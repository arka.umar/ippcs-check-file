﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonPostingtoICS.Models;
using CommonPostingtoICS.Helper.DBConfig;
using CommonPostingtoICS.Helper.FTP;
using CommonPostingtoICS.Helper.Base;
using System.IO;
using System.Reflection;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.IO;
using System.IO.Compression;
using CommonPostingtoICS.Helper.Util;

namespace CommonPostingtoICS.Models
{
    public class CSVCreation : BaseRepository<Common>
    {
        
        #region Singleton
        private CSVCreation() { }
        private static CSVCreation instance = null;
        public static CSVCreation Instance
        {
            get
            {
                if (instance == null)
                {
                    SetConfig();
                    instance = new CSVCreation();
                }

                return instance;
            }
        }
        #endregion

        public string generateCSV(IEnumerable<SendGRObj> DownloadList, string type)
        {
            string result = "";

            StringBuilder gr = new StringBuilder("");

            for (int i = 0; i < CsvCol.Count; i++)
            {
                //gr.Append(Quote(CsvCol[i].Text)); gr.Append(CSV_SEP);
                gr.Append(CsvCol[i].Text);
                if (i < (CsvCol.Count - 1))
                {
                    gr.Append(CSV_SEP);
                }
            }

            gr.Append(Environment.NewLine);

            foreach (SendGRObj d in DownloadList)
            {
                csvAddLine(gr, new string[] {
			                Equs(d.PROCESS_ID),
                            Equs(d.PROCESS_KEY),
                            Equs(d.SYSTEM_SOURCE),
                            Equs(d.USR_ID),
                            Equs(d.SEQ_NO),
                            //Equs(d.TRANS_ID),
                            //Equs(d.HEADER_INDEX),
                            Equs(d.POSTING_DT),
                            Equs(d.DOC_DT),
                            Equs(d.PROCESS_DT),
                            Equs(d.REF_NO),
                            Equs(d.MOVEMENT_TYPE),
                            Equs(d.SPECIAL_STOCK_TYPE),
                            Equs(d.MAT_DOC_DESC),
                            Equs(d.IM_PERIOD),
                            Equs(d.IM_YEAR),
                            Equs(d.KANBAN_ORDER_NO),
                            Equs(d.PROD_PURPOSE_CD),
                            Equs(d.SOURCE_TYPE),
                            Equs(d.MAT_NO),
                            Equs(d.ORI_MAT_NO),
                            Equs(d.MAT_DESC),
                            Equs(d.POSTING_QUANTITY),
                            Equs(d.PART_COLOR_SFX),
                            Equs(d.PACKING_TYPE),
                            Equs(d.UNIT_OF_MEASURE_CD),
                            Equs(d.ORI_UNIT_OF_MEASURE_CD),
                            Equs(d.BATCH_NO),
                            Equs(d.COIL_MAT_NO),
                            Equs(d.COIL_QTY),
                            Equs(d.PLANT_CD),
                            Equs(d.SLOC_CD),
                            Equs(d.DOCK_CD),
                            Equs(d.MAT_CURR),
                            Equs(d.VALUATION_CLASS),
                            Equs(d.PRICE_CALCULATION_TYPE),
                            Equs(d.ACTUAL_EXCHANGE_RATE),
                            Equs(d.CASE_EXPLOSION_FLAG),
                            Equs(d.SUPP_CD),
                            Equs(d.ORI_SUPP_CD),
                            Equs(d.PO_NO),
                            Equs(d.PO_DOC_TYPE),
                            Equs(d.PO_DOC_DT),
                            Equs(d.PO_ITEM_NO),
                            Equs(d.PO_MAT_PRICE),
                            Equs(d.PO_CURR),
                            Equs(d.PO_EXCHANGE_RATE),
                            Equs(d.GR_ORI_AMOUNT),
                            Equs(d.GR_LOCAL_AMOUNT),
                            Equs(d.PO_UNLIMITED_FLAG),
                            Equs(d.PO_TOLERANCE_PERCENTAGE),
                            Equs(d.PO_TAX_CD),
                            Equs(d.PO_INV_WO_GR_FLAG),
                            Equs(d.LIV_SPECIAL_TYPE),
                            Equs(d.LIV_SPECIAL_MAT_NO),
                            //Equs(d.SPECIAL_STOCK_VALUATION_FLAG),
                            //Equs(d.DELETION_FLAG),
                            Equs(d.GR_LEVEL),
                            Equs(d.RECEIVE_NO),
                            Equs(d.MAT_COMPLETE_FLAG),
                            Equs(d.AUTO_CREATED_FLAG),
                            Equs(d.REF_PROD_PURPOSE_CD),
                            Equs(d.REF_SOURCE_TYPE),
                            Equs(d.REF_MAT_NO),
                            Equs(d.REF_PLANT_CD),
                            Equs(d.REF_SLOC_CD),
                            Equs(d.SPECIAL_PROCUREMENT_TYPE_CD),
                            Equs(d.RAW_MATERIAL_FLAG),
                            Equs(d.DN_COMPLETE_FLAG),
                            Equs(d.REF_CANCEL_MAT_DOC_NO),
                            Equs(d.REF_CANCEL_MAT_DOC_YEAR),
                            Equs(d.REF_CANCEL_MAT_DOC_ITEM_NO),
                            Equs(d.INT_KANBAN_FLAG),
                            Equs(d.ARRIVAL_PLAN_DT),
                            Equs(d.OTHER_PROCESS_ID),
                            Equs(d.COMP_PRICE_CD_1),
                            Equs(d.COMP_PRICE_AMOUNT_1),
                            Equs(d.COMP_PRICE_CD_2),
                            Equs(d.COMP_PRICE_AMOUNT_2),
                            Equs(d.COMP_PRICE_CD_3),
                            Equs(d.COMP_PRICE_AMOUNT_3),
                            Equs(d.COMP_PRICE_CD_4),
                            Equs(d.COMP_PRICE_AMOUNT_4),
                            Equs(d.COMP_PRICE_CD_5),
                            Equs(d.COMP_PRICE_AMOUNT_5),
                            Equs(d.COMPLETE_FLAG),
                            Equs(d.REMARK_1),
                            Equs(d.REMARK_2),
                            //Equs(d.PROCESS_STS),
                            //Equs(d.STANDARD_PRICE),
                            Equs(d.VIN),
                            Equs(d.CREATED_BY),
                            Equs(d.CREATED_DT)});
                //Equs(d.CHANGED_BY),
                //Equs(d.CHANGED_DT)});
            }

            //string zipPath = CSTDDateUtil.GetCurrentDBDate().ToString("yyyyMMddHHmmss");

            string fileFormatNm = CommonDBHelper.Instance.getFilename(type);

            Byte[] bin = UTF8Encoding.UTF8.GetBytes(gr.ToString());
            string DownloadDirectory = "Temp/GR/" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "/";
            string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);

            System.IO.Directory.CreateDirectory(downdir);

            var pathfile = Path.Combine(downdir, fileFormatNm + ".csv");

            System.IO.File.WriteAllBytes(pathfile, bin);

            result = downdir + "|" + fileFormatNm + "|" + pathfile;

            return result;
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {

                    //_csv_sep = ConfigurationManager.AppSettings["CSV_SEPARATOR"];
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ",";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private List<CsvColumn> CsvCol = CsvColumn.Parse(
             "PROCESS_ID|C|20|0|PROCESS_ID;" +
             "PROCESS_KEY|C|65|0|PROCESS_KEY;" +
             "SYSTEM_SOURCE|C|20|0|SYSTEM_SOURCE;" +
             "USR_ID|C|20|0|USR_ID;" +
             "SEQ_NO|C|20|0|SEQ_NO;" +
            //"TRANS_ID|C|30|1|TRANS_ID;" +
            //"HEADER_INDEX|C|30|1|HEADER_INDEX;" +
             "POSTING_DT|C|30|0|POSTING_DT;" +
             "DOC_DT|C|30|0|DOC_DT;" +
             "PROCESS_DT|C|30|0|PROCESS_DT;" +
             "REF_NO|C|16|0|REF_NO;" +
             "MOVEMENT_TYPE|C|4|0|MOVEMENT_TYPE;" +
             "SPECIAL_STOCK_TYPE|C|1|1|SPECIAL_STOCK_TYPE;" +
             "MAT_DOC_DESC|C|255|1|MAT_DOC_DESC;" +
             "IM_PERIOD|C|2|1|IM_PERIOD;" +
             "IM_YEAR|C|4|1|IM_YEAR;" +
             "KANBAN_ORDER_NO|C|16|1|KANBAN_ORDER_NO;" +
             "PROD_PURPOSE_CD|C|5|0|PROD_PURPOSE_CD;" +
             "SOURCE_TYPE|C|1|0|SOURCE_TYPE;" +
             "MAT_NO|C|23|0|MAT_NO;" +
             "ORI_MAT_NO|C|23|0|ORI_MAT_NO;" +
             "MAT_DESC|C|40|0|MAT_DESC;" +
             "POSTING_QUANTITY|C|30|0|POSTING_QUANTITY;" +
             "PART_COLOR_SFX|C|2|0|PART_COLOR_SFX;" +
             "PACKING_TYPE|C|2|0|PACKING_TYPE;" +
             "UNIT_OF_MEASURE_CD|C|3|0|UNIT_OF_MEASURE_CD;" +
             "ORI_UNIT_OF_MEASURE_CD|C|5|0|ORI_UNIT_OF_MEASURE_CD;" +
             "BATCH_NO|C|10|1|BATCH_NO;" +
             "COIL_MAT_NO|C|23|1|COIL_MAT_NO;" +
             "COIL_QTY|C|20|1|COIL_QTY;" +
             "PLANT_CD|C|4|0|PLANT_CD;" +
             "SLOC_CD|C|6|0|SLOC_CD;" +
             "DOCK_CD|C|6|1|DOCK_CD;" +
             "MAT_CURR|C|3|0|MAT_CURR;" +
             "VALUATION_CLASS|C|4|1|VALUATION_CLASS;" +
             "PRICE_CALCULATION_TYPE|C|1|1|PRICE_CALCULATION_TYPE;" +
             "ACTUAL_EXCHANGE_RATE|C|30|0|ACTUAL_EXCHANGE_RATE;" +
             "CASE_EXPLOSION_FLAG|C|1|1|CASE_EXPLOSION_FLAG;" +
             "SUPP_CD|C|6|0|SUPP_CD;" +
             "ORI_SUPP_CD|C|6|0|ORI_SUPP_CD;" +
             "PO_NO|C|10|0|PO_NO;" +
             "PO_DOC_TYPE|C|4|0|PO_DOC_TYPE;" +
             "PO_DOC_DT|C|20|0|PO_DOC_DT;" +
             "PO_ITEM_NO|C|5|0|PO_ITEM_NO;" +
             "PO_MAT_PRICE|C|30|0|PO_MAT_PRICE;" +
             "PO_CURR|C|3|0|PO_CURR;" +
             "PO_EXCHANGE_RATE|C|30|1|PO_EXCHANGE_RATE;" +
             "GR_ORI_AMOUNT|C|30|0|GR_ORI_AMOUNT;" +
             "GR_LOCAL_AMOUNT|C|30|0|GR_LOCAL_AMOUNT;" +
             "PO_UNLIMITED_FLAG|C|1|0|PO_UNLIMITED_FLAG;" +
             "PO_TOLERANCE_PERCENTAGE|C|30|0|PO_TOLERANCE_PERCENTAGE;" +
             "PO_TAX_CD|C|2|0|PO_TAX_CD;" +
             "PO_INV_WO_GR_FLAG|C|1|1|PO_INV_WO_GR_FLAG;" +
             "LIV_SPECIAL_TYPE|C|1|1|LIV_SPECIAL_TYPE;" +
             "LIV_SPECIAL_MAT_NO|C|23|1|LIV_SPECIAL_MAT_NO;" +
            //"SPECIAL_STOCK_VALUATION_FLAG|C|1|1|SPECIAL_STOCK_VALUATION_FLAG;" +
            //"DELETION_FLAG|C|1|1|DELETION_FLAG;" +
             "GR_LEVEL|C|1|1|GR_LEVEL;" +
             "RECEIVE_NO|C|10|1|RECEIVE_NO;" +
             "MAT_COMPLETE_FLAG|C|1|0|MAT_COMPLETE_FLAG;" +
             "AUTO_CREATED_FLAG|C|1|1|AUTO_CREATED_FLAG;" +
             "REF_PROD_PURPOSE_CD|C|5|1|REF_PROD_PURPOSE_CD;" +
             "REF_SOURCE_TYPE|C|1|1|REF_SOURCE_TYPE;" +
             "REF_MAT_NO|C|23|1|REF_MAT_NO;" +
             "REF_PLANT_CD|C|4|1|REF_PLANT_CD;" +
             "REF_SLOC_CD|C|6|1|REF_SLOC_CD;" +
             "SPECIAL_PROCUREMENT_TYPE_CD|C|2|1|SPECIAL_PROCUREMENT_TYPE_CD;" +
             "RAW_MATERIAL_FLAG|C|1|1|RAW_MATERIAL_FLAG;" +
             "DN_COMPLETE_FLAG|C|1|0|DN_COMPLETE_FLAG;" +
             "REF_CANCEL_MAT_DOC_NO|C|20|1|REF_CANCEL_MAT_DOC_NO;" +
             "REF_CANCEL_MAT_DOC_YEAR|C|4|1|REF_CANCEL_MAT_DOC_YEAR;" +
             "REF_CANCEL_MAT_DOC_ITEM_NO|C|20|1|REF_CANCEL_MAT_DOC_ITEM_NO;" +
             "INT_KANBAN_FLAG|C|1|1|INT_KANBAN_FLAG;" +
             "ARRIVAL_PLAN_DT|C|20|1|ARRIVAL_PLAN_DT;" +
             "OTHER_PROCESS_ID|C|30|1|OTHER_PROCESS_ID;" +
             "COMP_PRICE_CD_1|C|1|1|COMP_PRICE_CD_1;" +
             "COMP_PRICE_AMOUNT_1|C|30|1|COMP_PRICE_AMOUNT_1;" +
             "COMP_PRICE_CD_2|C|1|1|COMP_PRICE_CD_2;" +
             "COMP_PRICE_AMOUNT_2|C|30|1|COMP_PRICE_AMOUNT_2;" +
             "COMP_PRICE_CD_3|C|1|1|COMP_PRICE_CD_3;" +
             "COMP_PRICE_AMOUNT_3|C|30|1|COMP_PRICE_AMOUNT_3;" +
             "COMP_PRICE_CD_4|C|1|1|COMP_PRICE_CD_4;" +
             "COMP_PRICE_AMOUNT_4|C|30|1|COMP_PRICE_AMOUNT_4;" +
             "COMP_PRICE_CD_5|C|1|1|COMP_PRICE_CD_5;" +
             "COMP_PRICE_AMOUNT_5|C|30|1|COMP_PRICE_AMOUNT_5;" +
             "COMPLETE_FLAG|C|1|0|COMPLETE_FLAG;" +
             "REMARK_1|C|200|1|REMARK_1;" +
             "REMARK_2|C|200|1|REMARK_2;" +
            //"PROCESS_STS|C|1|1|PROCESS_STS;" +
            //"STANDARD_PRICE|C|50|1|STANDARD_PRICE;" +
             "VIN|C|500|1|VIN;" +
             "CREATED_BY|C|20|0|CREATED_BY;" +
             "CREATED_DT|C|30|1|CREATED_DT;");
        //"CHANGED_BY|C|20|1|CHANGED_BY;" +
        //"CHANGED_DT|C|30|1|CHANGED_DT;");

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        /// <summary>
        /// put Equal Sign in front and quote char to prevent 'numerization' by Excel
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                //else
                //{
                //    s = "=\"" + s + "\"";
                //}
            }
            return s;
        }

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }


        }

        public override List<Common> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
