﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CommonPostingtoICS.Models
{
    public class SendGRObj
    {
        public String PROCESS_KEY { get; set; }
        public String PROCESS_ID { get; set; }
        public String SYSTEM_SOURCE { get; set; }
        public String USR_ID { get; set; }
        public String SEQ_NO { get; set; }
        public String TRANS_ID { get; set; }
        public String HEADER_INDEX { get; set; }
        public String POSTING_DT { get; set; }
        public String DOC_DT { get; set; }
        public String PROCESS_DT { get; set; }
        public String REF_NO { get; set; }
        public String MOVEMENT_TYPE { get; set; }
        public String SPECIAL_STOCK_TYPE { get; set; }
        public String MAT_DOC_DESC { get; set; }
        public String IM_PERIOD { get; set; }
        public String IM_YEAR { get; set; }
        public String KANBAN_ORDER_NO { get; set; }
        public String PROD_PURPOSE_CD { get; set; }
        public String SOURCE_TYPE { get; set; }
        public String MAT_NO { get; set; }
        public String ORI_MAT_NO { get; set; }
        public String MAT_DESC { get; set; }
        public String POSTING_QUANTITY { get; set; }
        public String PART_COLOR_SFX { get; set; }
        public String PACKING_TYPE { get; set; }
        public String UNIT_OF_MEASURE_CD { get; set; }
        public String ORI_UNIT_OF_MEASURE_CD { get; set; }
        public String BATCH_NO { get; set; }
        public String COIL_MAT_NO { get; set; }
        public String COIL_QTY { get; set; }
        public String PLANT_CD { get; set; }
        public String SLOC_CD { get; set; }
        public String DOCK_CD { get; set; }
        public String MAT_CURR { get; set; }
        public String VALUATION_CLASS { get; set; }
        public String PRICE_CALCULATION_TYPE { get; set; }
        public String ACTUAL_EXCHANGE_RATE { get; set; }
        public String CASE_EXPLOSION_FLAG { get; set; }
        public String SUPP_CD { get; set; }
        public String ORI_SUPP_CD { get; set; }
        public String PO_NO { get; set; }
        public String PO_DOC_TYPE { get; set; }
        public String PO_DOC_DT { get; set; }
        public String PO_ITEM_NO { get; set; }
        public String PO_MAT_PRICE { get; set; }
        public String PO_CURR { get; set; }
        public String PO_EXCHANGE_RATE { get; set; }
        public String GR_ORI_AMOUNT { get; set; }
        public String GR_LOCAL_AMOUNT { get; set; }
        public String PO_UNLIMITED_FLAG { get; set; }
        public String PO_TOLERANCE_PERCENTAGE { get; set; }
        public String PO_TAX_CD { get; set; }
        public String PO_INV_WO_GR_FLAG { get; set; }
        public String LIV_SPECIAL_TYPE { get; set; }
        public String LIV_SPECIAL_MAT_NO { get; set; }
        public String SPECIAL_STOCK_VALUATION_FLAG { get; set; }
        public String DELETION_FLAG { get; set; }
        public String GR_LEVEL { get; set; }
        public String RECEIVE_NO { get; set; }
        public String MAT_COMPLETE_FLAG { get; set; }
        public String AUTO_CREATED_FLAG { get; set; }
        public String REF_PROD_PURPOSE_CD { get; set; }
        public String REF_SOURCE_TYPE { get; set; }
        public String REF_MAT_NO { get; set; }
        public String REF_PLANT_CD { get; set; }
        public String REF_SLOC_CD { get; set; }
        public String SPECIAL_PROCUREMENT_TYPE_CD { get; set; }
        public String RAW_MATERIAL_FLAG { get; set; }
        public String DN_COMPLETE_FLAG { get; set; }
        public String REF_CANCEL_MAT_DOC_NO { get; set; }
        public String REF_CANCEL_MAT_DOC_YEAR { get; set; }
        public String REF_CANCEL_MAT_DOC_ITEM_NO { get; set; }
        public String INT_KANBAN_FLAG { get; set; }
        public String ARRIVAL_PLAN_DT { get; set; }
        public String OTHER_PROCESS_ID { get; set; }
        public String COMP_PRICE_CD_1 { get; set; }
        public String COMP_PRICE_AMOUNT_1 { get; set; }
        public String COMP_PRICE_CD_2 { get; set; }
        public String COMP_PRICE_AMOUNT_2 { get; set; }
        public String COMP_PRICE_CD_3 { get; set; }
        public String COMP_PRICE_AMOUNT_3 { get; set; }
        public String COMP_PRICE_CD_4 { get; set; }
        public String COMP_PRICE_AMOUNT_4 { get; set; }
        public String COMP_PRICE_CD_5 { get; set; }
        public String COMP_PRICE_AMOUNT_5 { get; set; }
        public String COMPLETE_FLAG { get; set; }
        public String REMARK_1 { get; set; }
        public String REMARK_2 { get; set; }
        public String PROCESS_STS { get; set; }
        public String STANDARD_PRICE { get; set; }
        public String VIN { get; set; }
        public String CREATED_BY { get; set; }
        public String CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public String CHANGED_DT { get; set; }
        public String SEND_FLAG { get; set; }
    }

    public class SendICSRes
    {
        //public string isResult { get; set; }
        [JsonProperty("PROCESS_ID")]
        public string PROCESS_ID { get; set; }
        [JsonProperty("MESSAGE_TEXT")]
        public string MESSAGE_TEXT { get; set; }
        public string IsResult { get; set; }
    }

    public class FormFile
    {
        public string Name { get; set; }

        public string ContentType { get; set; }

        public string FilePath { get; set; }

        public Stream Stream { get; set; }
    }
}
