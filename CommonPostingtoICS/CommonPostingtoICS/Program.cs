﻿
/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : IPPCS (Procurement Control System)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : 
 * Function Name    : 
 * Function Group   : 
 * Program Id       : 
 * Program Name     : 
 * Program Type     : Console Application
 * Description      : This Console is used for Common Batch IPPCS.
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Ridwan
 * Version          : 01.00.00
 * Creation Date    : 10/09/2020 16.10.00
 *                                                                                                          *
 * Update history		Re-fix date				Person in charge				Description					*
 *
 * Copyright(C) 2020 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using CommonPostingtoICS.Helper.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonPostingtoICS
{
    class Program
    {
        static void Main(string[] args)
        {
            string functionId = "SendGRtoICSbyPass"; //args[0]; //

            Assembly assembly = typeof(Program).Assembly; // in the same assembly!

            Type type = assembly.GetType("CommonPostingtoICS.AppCode." + functionId);
            BaseBatch batch = (BaseBatch)Activator.CreateInstance(type);
            batch.ExecuteBatch();
        }
    }
}
