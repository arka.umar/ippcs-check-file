﻿
/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : IPPCS (Procurement Control System)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : 51005
 * Function Name    : Send GR to ICS
 * Function Group   : IPPCS
 * Program Id       : 81001
 * Program Name     : Send GR to ICS
 * Program Type     : Console Application
 * Description      : This Console is used for Common Batch IPPCS.
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Ridwan
 * Version          : 01.00.00
 * Creation Date    : 10/09/2020 16.10.00
 *                                                                                                          *
 * Update history		Re-fix date				Person in charge				Description					*
 *
 * Copyright(C) 2020 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonPostingtoICS.Models;
using CommonPostingtoICS.Helper.DBConfig;
using CommonPostingtoICS.Helper.FTP;
using CommonPostingtoICS.Helper.Base;
using System.IO;
using System.Reflection;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.IO;
using System.IO.Compression;
using CommonPostingtoICS.Helper.Util;
using System.Configuration;
using System.Data.SqlClient;

namespace CommonPostingtoICS.AppCode
{
    public class SendGRtoICSbyPass : BaseBatch
    {
        public static ConnectionDescriptor connDesc = DBContextHelper.Instance.getConfiguration();

        public override void ExecuteBatch()
        {
            Console.WriteLine("start process Send GR to ICS");
            string loc = "SEND IPPCS GR ICS";
            string module = "5";
            string function = "51005";
            int flag = 0;

            Common getProc = new Common();
            getProc.MSG_TXT = "Start Send IPPCS / JPO GR To ICS";
            getProc.LOCATION = loc;
            getProc.PID = 0;
            getProc.MSG_ID = "MTOCSTD024I";
            getProc.MSG_TYPE = "I";
            getProc.MODULE_ID = module;
            getProc.FUNCTION_ID = function;
            getProc.USER_ID = "SYSTEM";
            getProc.PROCESS_STS = 0;
            Int64 PID = CommonDBHelper.Instance.CreateLog(getProc);

            try
            {
                IDBContext db = dbManager.GetContext();

                getProc.MSG_TXT = "Fetching IPPCS / JPO GR data";
                getProc.LOCATION = loc;
                getProc.PID = PID;
                getProc.MSG_ID = "MTOCSTD024I";
                getProc.MSG_TYPE = "I";
                getProc.MODULE_ID = module;
                getProc.FUNCTION_ID = function;
                getProc.PROCESS_STS = 0;
                getProc.USER_ID = "SYSTEM";
                PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                SqlConnection connect = new SqlConnection(connDesc.ConnectionString);
                Int32 countIPPCS = 0;
                Int32 countJPO = 0;
                try
                {
                    // Connect to database.
                    connect.Open();
                    Console.WriteLine("Database connection successful.");
                    SqlCommand comm1 = new SqlCommand("SELECT COUNT(1) FROM [dbo].[TB_T_GOOD_RECEIVE] WHERE ISNULL(SEND_FLAG, 'N') = 'N'"
                            + " AND SYSTEM_SOURCE = 'IPPCS'", connect);
                    countIPPCS = Convert.ToInt32(comm1.ExecuteScalar());
                    connect.Close();

                    connect.Open();
                    SqlCommand comm2 = new SqlCommand("SELECT COUNT(1) FROM [dbo].[TB_T_GOOD_RECEIVE] WHERE ISNULL(SEND_FLAG, 'N') = 'N'"
                            + " AND SYSTEM_SOURCE = 'JPO'", connect);
                    countJPO = Convert.ToInt32(comm2.ExecuteScalar());
                    connect.Close();

                }
                catch (Exception e)
                {
                    // Confirm unsuccessful connection and stop program execution.
                    Console.WriteLine("Database connection unsuccessful.");
                    System.Environment.Exit(0);

                }

                #region IPPCS
                if (countIPPCS > 0)
                {
                    getProc.MSG_TXT = "IPPCS : Inserting data into csv file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    #region create csv file
                    string resCSV = CSVCreationBypass.Instance.generateCSV("IPPCS");
                    #endregion

                    #region create zip file
                    getProc.MSG_TXT = "IPPCS : Compress csv file into zip file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    string ZipFilename = resCSV.Split('|')[0] + resCSV.Split('|')[1] + ".zip";
                    using (ZipArchive newFile = ZipFile.Open(ZipFilename, ZipArchiveMode.Create))
                    {
                        newFile.CreateEntryFromFile(resCSV.Split('|')[2], resCSV.Split('|')[1] + ".csv", CompressionLevel.Fastest);
                    }
                    #endregion

                    #region Call API
                    getProc.MSG_TXT = "IPPCS : Send zip file to GR API";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    Dictionary<string, object> data = new Dictionary<string, object>();

                    data["Username"] = "IPPCS.Admin";
                    data["SystemSource"] = "IPPCS";
                    data["File"] = new FormFile() { Name = Path.GetFileName(ZipFilename), ContentType = MimeUtil.GetMimeType(Path.GetExtension(ZipFilename)), FilePath = ZipFilename }; //ZipFilename;

                    var isResult = WebAPIUtil.RequestFile<SendICSRes>(data, "GRInterfaceIPPCS", null, "POST", PID);
                    #endregion
                    if (isResult.Contains("Success"))
                    {
                        getProc.MSG_TXT = "IPPCS : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        getProc.MSG_TXT = "IPPCS : Total: " + countIPPCS.ToString() + " record(s), has been send to ICS";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        #region Update data
                        getProc.MSG_TXT = "IPPCS : Update send flag";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        /*db.BeginTransaction();
                        dynamic args = new
                        {
                            type = "IPPCS"
                        };
                        db.Execute("UpdateGRData", args);
                        db.CommitTransaction();*/

                        try
                        {
                            SqlCommand cmd = new SqlCommand("UPDATE A SET A.SEND_FLAG = 'Y', A.CHANGED_BY = 'GR.posting', A.CHANGED_DT = GETDATE() FROM [dbo].[TB_T_GOOD_RECEIVE] A WHERE ISNULL(SEND_FLAG, 'N') = 'N' AND SYSTEM_SOURCE = 'IPPCS'", connect);
                            cmd.CommandTimeout = 0;

                            connect.Open();
                            cmd.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            flag = 1;

                            getProc.MSG_TXT = ex.Message;
                            getProc.LOCATION = loc;
                            getProc.PID = PID;
                            getProc.MSG_ID = "MTOCSTD024I";
                            getProc.MSG_TYPE = "I";
                            getProc.MODULE_ID = module;
                            getProc.FUNCTION_ID = function;
                            getProc.PROCESS_STS = 1;
                            getProc.USER_ID = "SYSTEM";
                            PID = CommonDBHelper.Instance.CreateLogFinish(getProc);
                        }
                        finally
                        {
                            connect.Close();
                        }
                        #endregion
                    }
                    else
                    {
                        flag = 1;

                        getProc.MSG_TXT = "IPPCS : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                    }

                    #region Delete file
                    Directory.Delete(resCSV.Split('|')[0], true);

                    #endregion
                }
                else
                {
                    //flag = 1;

                    getProc.MSG_TXT = "IPPCS : GR data not found";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                }
                #endregion


                #region JPO
                if (countJPO > 0)
                {
                    getProc.MSG_TXT = "JPO : Inserting data into csv file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    #region create csv file
                    string resCSV = CSVCreationBypass.Instance.generateCSV("JPO");
                    #endregion

                    #region create zip file
                    getProc.MSG_TXT = "JPO : Compress csv file into zip file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    string ZipFilename = resCSV.Split('|')[0] + resCSV.Split('|')[1] + ".zip";
                    using (ZipArchive newFile = ZipFile.Open(ZipFilename, ZipArchiveMode.Create))
                    {
                        newFile.CreateEntryFromFile(resCSV.Split('|')[2], resCSV.Split('|')[1] + ".csv", CompressionLevel.Fastest);
                    }
                    #endregion

                    #region Call API
                    getProc.MSG_TXT = "JPO : Send zip file to GR API";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    Dictionary<string, object> data = new Dictionary<string, object>();

                    data["Username"] = "JPO.Admin";
                    data["SystemSource"] = "JPO";
                    data["File"] = new FormFile() { Name = Path.GetFileName(ZipFilename), ContentType = MimeUtil.GetMimeType(Path.GetExtension(ZipFilename)), FilePath = ZipFilename }; //ZipFilename;

                    var isResult = WebAPIUtil.RequestFile<SendICSRes>(data, "GRInterfaceJPO", null, "POST", PID);
                    #endregion

                    if (isResult.Contains("Success"))
                    {
                        getProc.MSG_TXT = "JPO : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        getProc.MSG_TXT = "JPO : Total: " + countJPO.ToString() + " record(s), has been send to ICS";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        #region Update data
                        getProc.MSG_TXT = "JPO : Update send flag";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        /*db.BeginTransaction();
                        dynamic args = new
                        {
                            type = "JPO"
                        };
                        db.Execute("UpdateGRData", args);
                        db.CommitTransaction();*/

                        try
                        {
                            SqlCommand cmd = new SqlCommand("UPDATE A SET A.SEND_FLAG = 'Y', A.CHANGED_BY = 'GR.posting', A.CHANGED_DT = GETDATE() FROM [dbo].[TB_T_GOOD_RECEIVE] A WHERE ISNULL(SEND_FLAG, 'N') = 'N' AND SYSTEM_SOURCE = 'JPO'", connect);
                            cmd.CommandTimeout = 0;

                            connect.Open();
                            cmd.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            flag = 1;

                            getProc.MSG_TXT = ex.Message;
                            getProc.LOCATION = loc;
                            getProc.PID = PID;
                            getProc.MSG_ID = "MTOCSTD024I";
                            getProc.MSG_TYPE = "I";
                            getProc.MODULE_ID = module;
                            getProc.FUNCTION_ID = function;
                            getProc.PROCESS_STS = 1;
                            getProc.USER_ID = "SYSTEM";
                            PID = CommonDBHelper.Instance.CreateLogFinish(getProc);
                        }
                        finally
                        {
                            connect.Close();
                        }
                        #endregion
                    }
                    else
                    {
                        flag = 1;

                        getProc.MSG_TXT = "JPO : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                    }

                    #region Delete file
                    Directory.Delete(resCSV.Split('|')[0], true);

                    #endregion


                }
                else
                {
                    //flag = 1;

                    getProc.MSG_TXT = "JPO : GR data not found";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                }
                #endregion
                

                if (flag == 0)
                {
                    getProc.MSG_TXT = "Send IPPCS / JPO GR Process is finished successfully";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                    Console.WriteLine("process is finished succesfully");
                }
                else
                {
                    getProc.MSG_TXT = "Send IPPCS / JPO GR Process is finished with error";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 1;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                    Console.WriteLine("process is finished with error");
                }

            }
            catch (Exception ex)
            {
                getProc.MSG_TXT = ex.Message.ToString(); //"Send GR Process is finished with error";
                getProc.LOCATION = loc;
                getProc.PID = PID;
                getProc.MSG_ID = "MTOCSTD024I";
                getProc.MSG_TYPE = "I";
                getProc.MODULE_ID = module;
                getProc.FUNCTION_ID = function;
                getProc.PROCESS_STS = 1;
                getProc.USER_ID = "SYSTEM";
                PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                Console.WriteLine("process is finished with system error");
            }

            Console.WriteLine("end process Send GR to ICS");
        }

        public override List<Common> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
