﻿
/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : IPPCS (Procurement Control System)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : 51005
 * Function Name    : Send GR to ICS
 * Function Group   : IPPCS
 * Program Id       : 81001
 * Program Name     : Send GR to ICS
 * Program Type     : Console Application
 * Description      : This Console is used for Common Batch IPPCS.
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Ridwan
 * Version          : 01.00.00
 * Creation Date    : 10/09/2020 16.10.00
 *                                                                                                          *
 * Update history		Re-fix date				Person in charge				Description					*
 *
 * Copyright(C) 2020 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonPostingtoICS.Models;
using CommonPostingtoICS.Helper.DBConfig;
using CommonPostingtoICS.Helper.FTP;
using CommonPostingtoICS.Helper.Base;
using System.IO;
using System.Reflection;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.IO;
using System.IO.Compression;
using CommonPostingtoICS.Helper.Util;

namespace CommonPostingtoICS.AppCode
{
    public class SendGRtoICS : BaseBatch
    {
        public override void ExecuteBatch()
        {
            Console.WriteLine("start process Send GR to ICS");
            string loc = "SEND IPPCS GR ICS";
            string module = "5";
            string function = "51005";
            int flag = 0;

            Common getProc = new Common();
            getProc.MSG_TXT = "Start Send IPPCS / JPO GR To ICS";
            getProc.LOCATION = loc;
            getProc.PID = 0;
            getProc.MSG_ID = "MTOCSTD024I";
            getProc.MSG_TYPE = "I";
            getProc.MODULE_ID = module;
            getProc.FUNCTION_ID = function;
            getProc.USER_ID = "SYSTEM";
            getProc.PROCESS_STS = 0;
            Int64 PID = CommonDBHelper.Instance.CreateLog(getProc);

            CommonDBHelper Repo = CommonDBHelper.Instance;

            try
            {
                IDBContext db = dbManager.GetContext();

                getProc.MSG_TXT = "Fetching IPPCS / JPO GR data";
                getProc.LOCATION = loc;
                getProc.PID = PID;
                getProc.MSG_ID = "MTOCSTD024I";
                getProc.MSG_TYPE = "I";
                getProc.MODULE_ID = module;
                getProc.FUNCTION_ID = function;
                getProc.PROCESS_STS = 0;
                getProc.USER_ID = "SYSTEM";
                PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                IEnumerable<SendGRObj> DownloadList = CommonDBHelper.Instance.GetListGR("IPPCS").ToList();

                IEnumerable<SendGRObj> DownloadListJPO = CommonDBHelper.Instance.GetListGR("JPO").ToList();

                #region IPPCS
                if (DownloadList.Count() > 0)
                {
                    getProc.MSG_TXT = "IPPCS : Inserting data into csv file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    #region create csv file
                    string resCSV = CSVCreation.Instance.generateCSV(DownloadList, "IPPCS");
                    #endregion

                    #region create zip file
                    getProc.MSG_TXT = "IPPCS : Compress csv file into zip file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    string ZipFilename = resCSV.Split('|')[0] + resCSV.Split('|')[1] + ".zip";
                    using (ZipArchive newFile = ZipFile.Open(ZipFilename, ZipArchiveMode.Create))
                    {
                        newFile.CreateEntryFromFile(resCSV.Split('|')[2], resCSV.Split('|')[1] + ".csv", CompressionLevel.Fastest);
                    }
                    #endregion

                    #region Call API
                    getProc.MSG_TXT = "IPPCS : Send zip file to GR API";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    Dictionary<string, object> data = new Dictionary<string, object>();

                    data["Username"] = "IPPCS.Admin";
                    data["SystemSource"] = "IPPCS";
                    data["File"] = new FormFile() { Name = Path.GetFileName(ZipFilename), ContentType = MimeUtil.GetMimeType(Path.GetExtension(ZipFilename)), FilePath = ZipFilename }; //ZipFilename;

                    var isResult = WebAPIUtil.RequestFile<SendICSRes>(data, "GRInterfaceIPPCS", null, "POST", PID);
                    #endregion

                    if (isResult.Contains("Success"))
                    {
                        getProc.MSG_TXT = "IPPCS : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        getProc.MSG_TXT = "IPPCS : Total: " + DownloadList.Count().ToString() + " record(s), has been send to ICS";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        #region Update data
                        getProc.MSG_TXT = "IPPCS : Update send flag";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        db.BeginTransaction();
                        dynamic args = new
                        {
                            type = "IPPCS"
                        };
                        db.Execute("UpdateGRData", args);
                        db.CommitTransaction();
                        #endregion
                    }
                    else
                    {
                        flag = 1;

                        getProc.MSG_TXT = "IPPCS : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                    }

                    #region Delete file
                    Directory.Delete(resCSV.Split('|')[0], true);

                    //System.IO.DirectoryInfo di = new DirectoryInfo(downdir);

                    //foreach (FileInfo file in di.GetFiles())
                    //{
                    //    file.Delete();
                    //}
                    //foreach (DirectoryInfo dir in di.GetDirectories())
                    //{
                    //    dir.Delete(true);
                    //}
                    #endregion


                }
                else
                {
                    //flag = 1;

                    getProc.MSG_TXT = "IPPCS : GR data not found";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                }
                #endregion

                #region JPO
                if (DownloadListJPO.Count() > 0)
                {
                    getProc.MSG_TXT = "JPO : Inserting data into csv file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    #region create csv file
                    string resCSV = CSVCreation.Instance.generateCSV(DownloadListJPO, "JPO");
                    #endregion

                    #region create zip file
                    getProc.MSG_TXT = "JPO : Compress csv file into zip file";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    string ZipFilename = resCSV.Split('|')[0] + resCSV.Split('|')[1] + ".zip";
                    using (ZipArchive newFile = ZipFile.Open(ZipFilename, ZipArchiveMode.Create))
                    {
                        newFile.CreateEntryFromFile(resCSV.Split('|')[2], resCSV.Split('|')[1] + ".csv", CompressionLevel.Fastest);
                    }
                    #endregion

                    #region Call API
                    getProc.MSG_TXT = "JPO : Send zip file to GR API";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    Dictionary<string, object> data = new Dictionary<string, object>();

                    data["Username"] = "JPO.Admin";
                    data["SystemSource"] = "JPO";
                    data["File"] = new FormFile() { Name = Path.GetFileName(ZipFilename), ContentType = MimeUtil.GetMimeType(Path.GetExtension(ZipFilename)), FilePath = ZipFilename }; //ZipFilename;

                    var isResult = WebAPIUtil.RequestFile<SendICSRes>(data, "GRInterfaceJPO", null, "POST", PID);
                    #endregion

                    if (isResult.Contains("Success"))
                    {
                        getProc.MSG_TXT = "JPO : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        getProc.MSG_TXT = "JPO : Total: " + DownloadListJPO.Count().ToString() + " record(s), has been send to ICS";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        #region Update data
                        getProc.MSG_TXT = "JPO : Update send flag";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        db.BeginTransaction();
                        dynamic args = new
                        {
                            type = "JPO"
                        };
                        db.Execute("UpdateGRData", args);
                        db.CommitTransaction();
                        #endregion
                    }
                    else
                    {
                        flag = 1;

                        getProc.MSG_TXT = "JPO : Return Message: " + isResult;
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                    }

                    #region Delete file
                    Directory.Delete(resCSV.Split('|')[0], true);

                    //System.IO.DirectoryInfo di = new DirectoryInfo(downdir);

                    //foreach (FileInfo file in di.GetFiles())
                    //{
                    //    file.Delete();
                    //}
                    //foreach (DirectoryInfo dir in di.GetDirectories())
                    //{
                    //    dir.Delete(true);
                    //}
                    #endregion


                }
                else
                {
                    //flag = 1;

                    getProc.MSG_TXT = "JPO : GR data not found";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                }
                #endregion
                



                if (flag == 0)
                {
                    getProc.MSG_TXT = "Send IPPCS / JPO GR Process is finished successfully";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                    Console.WriteLine("process is finished succesfully");
                }
                else
                {
                    getProc.MSG_TXT = "Send IPPCS / JPO GR Process is finished with error";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 1;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                    Console.WriteLine("process is finished with error");
                }

            }
            catch (Exception ex)
            {
                getProc.MSG_TXT = ex.Message.ToString(); //"Send GR Process is finished with error";
                getProc.LOCATION = loc;
                getProc.PID = PID;
                getProc.MSG_ID = "MTOCSTD024I";
                getProc.MSG_TYPE = "I";
                getProc.MODULE_ID = module;
                getProc.FUNCTION_ID = function;
                getProc.PROCESS_STS = 1;
                getProc.USER_ID = "SYSTEM";
                PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                Console.WriteLine("process is finished with system error");
            }

            Console.WriteLine("end process Send GR to ICS");
        }


        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {

                    //_csv_sep = ConfigurationManager.AppSettings["CSV_SEPARATOR"];
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ",";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private List<CsvColumn> CsvCol = CsvColumn.Parse(
             "PROCESS_ID|C|20|0|PROCESS_ID;" +
             "PROCESS_KEY|C|65|0|PROCESS_KEY;" +
             "SYSTEM_SOURCE|C|20|0|SYSTEM_SOURCE;" +
             "USR_ID|C|20|0|USR_ID;" +
             "SEQ_NO|C|20|0|SEQ_NO;" +
             //"TRANS_ID|C|30|1|TRANS_ID;" +
             //"HEADER_INDEX|C|30|1|HEADER_INDEX;" +
             "POSTING_DT|C|30|0|POSTING_DT;" +
             "DOC_DT|C|30|0|DOC_DT;" +
             "PROCESS_DT|C|30|0|PROCESS_DT;" +
             "REF_NO|C|16|0|REF_NO;" +
             "MOVEMENT_TYPE|C|4|0|MOVEMENT_TYPE;" +
             "SPECIAL_STOCK_TYPE|C|1|1|SPECIAL_STOCK_TYPE;" +
             "MAT_DOC_DESC|C|255|1|MAT_DOC_DESC;" +
             "IM_PERIOD|C|2|1|IM_PERIOD;" +
             "IM_YEAR|C|4|1|IM_YEAR;" +
             "KANBAN_ORDER_NO|C|16|1|KANBAN_ORDER_NO;" +
             "PROD_PURPOSE_CD|C|5|0|PROD_PURPOSE_CD;" +
             "SOURCE_TYPE|C|1|0|SOURCE_TYPE;" +
             "MAT_NO|C|23|0|MAT_NO;" +
             "ORI_MAT_NO|C|23|0|ORI_MAT_NO;" +
             "MAT_DESC|C|40|0|MAT_DESC;" +
             "POSTING_QUANTITY|C|30|0|POSTING_QUANTITY;" +
             "PART_COLOR_SFX|C|2|0|PART_COLOR_SFX;" +
             "PACKING_TYPE|C|2|0|PACKING_TYPE;" +
             "UNIT_OF_MEASURE_CD|C|3|0|UNIT_OF_MEASURE_CD;" +
             "ORI_UNIT_OF_MEASURE_CD|C|5|0|ORI_UNIT_OF_MEASURE_CD;" +
             "BATCH_NO|C|10|1|BATCH_NO;" +
             "COIL_MAT_NO|C|23|1|COIL_MAT_NO;" +
             "COIL_QTY|C|20|1|COIL_QTY;" +
             "PLANT_CD|C|4|0|PLANT_CD;" +
             "SLOC_CD|C|6|0|SLOC_CD;" +
             "DOCK_CD|C|6|1|DOCK_CD;" +
             "MAT_CURR|C|3|0|MAT_CURR;" +
             "VALUATION_CLASS|C|4|1|VALUATION_CLASS;" +
             "PRICE_CALCULATION_TYPE|C|1|1|PRICE_CALCULATION_TYPE;" +
             "ACTUAL_EXCHANGE_RATE|C|30|0|ACTUAL_EXCHANGE_RATE;" +
             "CASE_EXPLOSION_FLAG|C|1|1|CASE_EXPLOSION_FLAG;" +
             "SUPP_CD|C|6|0|SUPP_CD;" +
             "ORI_SUPP_CD|C|6|0|ORI_SUPP_CD;" +
             "PO_NO|C|10|0|PO_NO;" +
             "PO_DOC_TYPE|C|4|0|PO_DOC_TYPE;" +
             "PO_DOC_DT|C|20|0|PO_DOC_DT;" +
             "PO_ITEM_NO|C|5|0|PO_ITEM_NO;" +
             "PO_MAT_PRICE|C|30|0|PO_MAT_PRICE;" +
             "PO_CURR|C|3|0|PO_CURR;" +
             "PO_EXCHANGE_RATE|C|30|1|PO_EXCHANGE_RATE;" +
             "GR_ORI_AMOUNT|C|30|0|GR_ORI_AMOUNT;" +
             "GR_LOCAL_AMOUNT|C|30|0|GR_LOCAL_AMOUNT;" +
             "PO_UNLIMITED_FLAG|C|1|0|PO_UNLIMITED_FLAG;" +
             "PO_TOLERANCE_PERCENTAGE|C|30|0|PO_TOLERANCE_PERCENTAGE;" +
             "PO_TAX_CD|C|2|0|PO_TAX_CD;" +
             "PO_INV_WO_GR_FLAG|C|1|1|PO_INV_WO_GR_FLAG;" +
             "LIV_SPECIAL_TYPE|C|1|1|LIV_SPECIAL_TYPE;" +
             "LIV_SPECIAL_MAT_NO|C|23|1|LIV_SPECIAL_MAT_NO;" +
             //"SPECIAL_STOCK_VALUATION_FLAG|C|1|1|SPECIAL_STOCK_VALUATION_FLAG;" +
             //"DELETION_FLAG|C|1|1|DELETION_FLAG;" +
             "GR_LEVEL|C|1|1|GR_LEVEL;" +
             "RECEIVE_NO|C|10|1|RECEIVE_NO;" +
             "MAT_COMPLETE_FLAG|C|1|0|MAT_COMPLETE_FLAG;" +
             "AUTO_CREATED_FLAG|C|1|1|AUTO_CREATED_FLAG;" +
             "REF_PROD_PURPOSE_CD|C|5|1|REF_PROD_PURPOSE_CD;" +
             "REF_SOURCE_TYPE|C|1|1|REF_SOURCE_TYPE;" +
             "REF_MAT_NO|C|23|1|REF_MAT_NO;" +
             "REF_PLANT_CD|C|4|1|REF_PLANT_CD;" +
             "REF_SLOC_CD|C|6|1|REF_SLOC_CD;" +
             "SPECIAL_PROCUREMENT_TYPE_CD|C|2|1|SPECIAL_PROCUREMENT_TYPE_CD;" +
             "RAW_MATERIAL_FLAG|C|1|1|RAW_MATERIAL_FLAG;" +
             "DN_COMPLETE_FLAG|C|1|0|DN_COMPLETE_FLAG;" +
             "REF_CANCEL_MAT_DOC_NO|C|20|1|REF_CANCEL_MAT_DOC_NO;" +
             "REF_CANCEL_MAT_DOC_YEAR|C|4|1|REF_CANCEL_MAT_DOC_YEAR;" +
             "REF_CANCEL_MAT_DOC_ITEM_NO|C|20|1|REF_CANCEL_MAT_DOC_ITEM_NO;" +
             "INT_KANBAN_FLAG|C|1|1|INT_KANBAN_FLAG;" +
             "ARRIVAL_PLAN_DT|C|20|1|ARRIVAL_PLAN_DT;" +
             "OTHER_PROCESS_ID|C|30|1|OTHER_PROCESS_ID;" +
             "COMP_PRICE_CD_1|C|1|1|COMP_PRICE_CD_1;" +
             "COMP_PRICE_AMOUNT_1|C|30|1|COMP_PRICE_AMOUNT_1;" +
             "COMP_PRICE_CD_2|C|1|1|COMP_PRICE_CD_2;" +
             "COMP_PRICE_AMOUNT_2|C|30|1|COMP_PRICE_AMOUNT_2;" +
             "COMP_PRICE_CD_3|C|1|1|COMP_PRICE_CD_3;" +
             "COMP_PRICE_AMOUNT_3|C|30|1|COMP_PRICE_AMOUNT_3;" +
             "COMP_PRICE_CD_4|C|1|1|COMP_PRICE_CD_4;" +
             "COMP_PRICE_AMOUNT_4|C|30|1|COMP_PRICE_AMOUNT_4;" +
             "COMP_PRICE_CD_5|C|1|1|COMP_PRICE_CD_5;" +
             "COMP_PRICE_AMOUNT_5|C|30|1|COMP_PRICE_AMOUNT_5;" +
             "COMPLETE_FLAG|C|1|0|COMPLETE_FLAG;" +
             "REMARK_1|C|200|1|REMARK_1;" +
             "REMARK_2|C|200|1|REMARK_2;" +
            //"PROCESS_STS|C|1|1|PROCESS_STS;" +
            //"STANDARD_PRICE|C|50|1|STANDARD_PRICE;" +
             "VIN|C|500|1|VIN;" +
             "CREATED_BY|C|20|0|CREATED_BY;" +
             "CREATED_DT|C|30|1|CREATED_DT;");
            //"CHANGED_BY|C|20|1|CHANGED_BY;" +
            //"CHANGED_DT|C|30|1|CHANGED_DT;");

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        /// <summary>
        /// put Equal Sign in front and quote char to prevent 'numerization' by Excel
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                //else
                //{
                //    s = "=\"" + s + "\"";
                //}
            }
            return s;
        }

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }


        }


        public override List<Common> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
