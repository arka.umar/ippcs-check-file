﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Toyota.UI.Forms
{
    public partial class Box : UserControl
    {
        public Box()
        {
            InitializeComponent();
        }

        public static void DrawButtonBorder(Graphics e, Rectangle rect, float width, Color CustomColor)
        {
            using (System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath())
            {
                gp.StartFigure();
                gp.AddLine(0, 0, 0, rect.Height -1);
                gp.AddLine(0, rect.Height - 1, rect.Width, rect.Height - 1);
                gp.AddLine(rect.Width - 1, rect.Height - 1, rect.Width - 1, 1);
                gp.AddLine(rect.Width, 0, 0, 0);
                gp.CloseAllFigures();
                using (Pen p1 = new Pen(Color.FromArgb(150, CustomColor), width))
                {
                    p1.LineJoin = LineJoin.Bevel;
                    e.DrawPath(p1, gp);
                }
            }
        }

        protected override void OnResize(EventArgs e)
        {
            this.Invalidate();
            base.OnResize(e);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            DrawButtonBorder(e.Graphics, this.ClientRectangle, 3, Color.FromArgb(192, 192, 192));
            base.OnPaint(e);
        }
    }
}
