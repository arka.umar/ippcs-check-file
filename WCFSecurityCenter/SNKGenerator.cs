﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Cryptography;

namespace WCFSecurityCenter
{
    public class SNKGenerator
    {

        private bool IsRequestValid(byte[] Token)
        {
            byte[] validToken;
            byte[] requestToken;
            bool notValid = false;

            ReleaseSNK();
            using (FileStream fs = new FileStream(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.RelativeSearchPath) + "\\scenter.snk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (SHA512Hash hash = new SHA512Hash())
                {
                    validToken = hash.ComputeHash(fs);
                }
            }
            using (MemoryStream ms = new MemoryStream(Token))
            {
                using (SHA512Hash hash = new SHA512Hash())
                {
                    requestToken = hash.ComputeHash(ms);
                }
            }
            if (requestToken.Length == validToken.Length)
            {
                for (int c = 0; c < requestToken.Length - 1; c++)
                {
                    if (validToken[c] != requestToken[c])
                    {
                        notValid = true;
                        break;
                    }
                }
            }

            if (notValid == true)
                return false;
            else
                return true;
        }

        internal bool ReleaseSNK()
        {
            bool close = false;
            IEnumerator<Dictionary<IntPtr, FileInfo>> list = VmcController.Services.DetectOpenFiles.GetOpenFilesEnumerator(System.Diagnostics.Process.GetCurrentProcess().Id);
            while (list.MoveNext())
            {
                var ret = list.Current.Values.Where(c => c.FullName.Contains("scenter.snk"));
                if (ret.Any() && ret != null)
                {
                    close = VmcController.Services.NativeMethods.CloseHandle(list.Current.Keys.ToList()[0]);
                    break;
                }
            }
            list.Dispose();
            return close;
        }
    }
}