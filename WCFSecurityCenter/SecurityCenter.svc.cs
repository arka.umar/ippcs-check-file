﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using Toyota.BussinesLayer;
using Toyota.Common.Web.Credential;
using System.Web;
using System.ServiceModel.Activation;

/**
 * 
 *  Web Service WCF to enable communicating from Front End to 
 *  OpenLDAP to Authenticate, Authorize, get position and to get other user's Info
 *  On the collection methods below, there is status for information
 *  
 *      Status Legend:
 *      =====================================================================================================  
 *      1. Deprecrated = This function has been not function, please see info on which new functions is used.
 *      2. Not updated yet = This function has been tested before and worked, 
 *                           but due to changes this function is not has been changed and tested yet, 
 *                           not recommended to use without proper testing.
 *      3. Active = This function is has been tested and currently in use.
 *      =====================================================================================================  
 * 
 * Author : Erwin 28/6/2013
 * 
 **/
namespace WCFSecurityCenter
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SecurityCenter" in code, svc and config file together.
    public class SecurityCenter : ISecurityCenter 
    {
        /**
         * 
         * Web Service Method for authenticating User
         * On AD and OpenLDAP.
         * Status : Active
         * 
         **/
        public bool Login(string UserName, string Password)
        {
            return Authentication.Login(UserName, Password);
        }

        /**
         * 
         * Web Service method to get Info and Authorization Details
         * from spesific User based on spesific System.
         * Status : Deprecated,see GetUserRole() and GetUserInfo()
         * 
         **/
        public string getRole(string UserName, string Password, string App)
        {
            return Authentication.getRole(UserName, Password, App);
        }
        

        /**
         * 
         * Web Service method to get user's info,
         * this service is update from deprecated getRole()
         * but not with Authorization Details info.
         * Status : Active.
         * 
         **/ 
        public string getUserInfo(string UserName, string Password)
        {
            return Authentication.getUserInfo(UserName, Password, "");
        }

        /**
         * 
         * Web Service method to get list of the system.
         * Status : Not updated yet.
         * 
         **/ 
        public List<string> appList(string UserName, string Password)
        {
            return Authentication.appList(UserName, Password);
        }

        /**
         * 
         * Web Service method to log out from Web Entrance
         * Status : Not updated yet.
         * 
         **/ 
        public bool LogOut(string UserName, string Password)
        {
            return false;
        }

        /**
         * 
         * Web Service method to get list of the email based on the usernames.
         * Status : Not updated yet.
         * 
         **/
        public string getEmail(string UserName, string Password, List<string> usernames)
        {
            return Authentication.getEmail(UserName, Password, usernames);
        }

        /**
         *
         * Web Service method to create the user
         * Status : Not updated yet.
         * 
         **/
        public bool regCreate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            return Authentication.regCreate(UserName, Password, CompanyName, Address, CompanyAddress, Email, ContactPerson, Telephone, Faximile, MobilePhone);
        }

        /**
         *
         * Web Service method to update the user's info
         * Status : Not updated yet.
         * 
         **/
        public bool regUpdate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            return Authentication.regUpdate(UserName, Password, CompanyName, Address, CompanyAddress, Email, ContactPerson, Telephone, Faximile, MobilePhone);
        }

        /**
         *
         * Web Service method to delete the user
         * Status : Not updated yet.
         * 
         **/
        public bool regDelete(string UserName, string Password, string UserEmail)
        {
            return Authentication.regDelete(UserName, Password, UserEmail);
        }

        /**
         *
         * Web Service method to check wether user is already exist
         * Status : Not updated yet.
         * 
         **/
        public bool regIsExist(string UserName, string Password, string UserEmail)
        {
            return Authentication.regIsExist(UserName, Password, UserEmail);
        }

        /**
         *
         * Web Service method to generate the user's email
         * Status : Not updated yet.
         * 
         **/
        public string genNewUser(string UserName, string Password, string Email)
        {
            return Authentication.genNewUser(UserName, Password, Email);
        }

        /**
         *
         * Web Service method to update the user's password
         * Status : Not updated yet.
         * 
         **/
        public string genNewPass(string UserName, string Password, string Email)
        {
            return Authentication.genNewPass(UserName, Password, Email);
        }


        /**
         *
         * Web Service method to check whether a default application has been assigned to spesific user
         * Status : Not updated yet.
         * 
         **/
        public bool IsDefaultAppExist(string UserName, string Password, string UserEmail)
        {
            return Authentication.IsDefaultAppExist(UserName, Password, UserEmail);
        }

        /**
         *
         * Web Service method to update the user's default application
         * Status : Active
         * 
         **/
        public bool UpdateDefaultApp(string UserName, string Password, string DefaultApplication)
        {
            return Authentication.UpdateDefaultApp(UserName, Password, DefaultApplication);
        }


        /**
         *
         * Web Service method to get user's default application info
         * Status : Active
         * 
         **/
        public string getDefaultApp(string UserName, string Password)
        {
            return Authentication.getDefaultApp(UserName, Password);
        }

        /**
         *
         * Web Service method to get employee based on some values
         * Status : Not Updated yet
         * 
         **/
        public string GetEmployeesData(string values)
        {
            return Authentication.GetEmployeesData(values);
        }

        /**
         *
         * Web Service method to get employee's info based on some ID
         * Status : Active
         * 
         **/
        public string GetEmployees(List<string> values)
        {
            return Authentication.GetEmployeesData(values);
        }

        /**
         *
         * Web Service method to get employee's info based on some ID
         * Status : Active
         * 
         **/
        public string GetEmployeeDatas(List<string> values)
        {
            return Authentication.GetEmployees(values);
        }

        /**
         *
         * Web Service method to get employee's ID max value, for wall
         * Status : Active
         * 
         **/
        public string GetEmployeesNum()
        {
            return Authentication.GetEmployeesNumber();
        }

        /**
         * 
         * Web Service method to get user's authorization details,
         * this service is update from deprecated getRole()
         * but not with user's common info.
         * Status : Active.
         * 
         **/ 
        public string getUserRole(string username, string app)
        {
            return Authentication.getUserRole(username, app);
        }

        /**
         * 
         * Web Service method to get organization struct, 
         * based on organization values thrown and level to get.
         * Status : Active.
         * 
         **/ 
        public string GetOrganizationStruct(string username, string password, string values, int level)
        {
            return Authentication.getOrganizationStruct(username, password, values, level);
        }

        /**
         * 
         * Web Service method to get user's info of the logged user
         * Status : Active.
         * 
         **/ 
        public string getUserLogged(string username)
        {
            return Authentication.getUserLogged(username);
        }

        /**
         * 
         * Web Service method to create user's info of the logged user
         * Status : Active.
         * 
         **/ 
        public bool crUserLogged(string username, string password, string info)
        {
            return Authentication.crUserLogged(username, password, info);
        }


        /**
         * 
         * Web Service method to change user's info of the logged user
         * Status : Active.
         * 
         **/ 
        public bool chUserLogged(string username, string password, string changes)
        {
            return Authentication.chUserLogged(username, password, changes);
        }


        /**
         * 
         * Web Service method to delete user's info of the logged user
         * Status : Active.
         * 
         **/ 
        public bool delUserLogged(string user, string password, string username)
        {
            return Authentication.delUserLogged(user, password, username);
        }


        /**
         * 
         * Web Service method to get user's authorization details
         * Status : Deprecated, see getUserRole()
         * 
         **/ 
        public string getRolePR(string user, string password, string App, string roleId)
        {
            return Authentication.getRolePR(user,password,App,roleId); ;
        }

        public string ChangePassword(string username, string currentPassword, string newPassword)
        {
            return Authentication.changePassword(username, currentPassword, newPassword);
        }

        public bool IsPasswordMustBeChanged(string username, string password)
        {
            return Authentication.IsPasswordMustBeChanged(username, password);
        }
    }
}
