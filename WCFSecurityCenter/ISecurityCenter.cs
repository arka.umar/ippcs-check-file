﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.Web;
using System.ServiceModel.Web;

namespace WCFSecurityCenter
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISecurityCenter" in both code and config file together.
    [ServiceContract(Namespace = "WCFSecurityCenter", SessionMode= SessionMode.Allowed)]
    public interface ISecurityCenter
    {
        [OperationContract]
        bool Login(string UserName, string Password);

        [OperationContract]
        string getRole(string UserName, string Password, string App);
        
        [OperationContract]
        string getUserInfo(string UserName, string Password);

        [OperationContract]
        List<string> appList(string UserName, string Password);

        [OperationContract]
        bool LogOut(string UserName, string Password);

        [OperationContract]
        string getEmail(string UserName, string Password, List<string> usernames);

        [OperationContract]
        bool regCreate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone);

        [OperationContract]
        bool regUpdate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone);

        [OperationContract]
        bool regDelete(string UserName, string Password, string UserEmail);

        [OperationContract]
        bool regIsExist(string UserName, string Password, string UserEmail);

        [OperationContract]
        string genNewUser(string UserName, string Password, string Email);

        [OperationContract]
        string genNewPass(string UserName, string Password, string Email);

        [OperationContract]
        bool IsDefaultAppExist(string UserName, string Password, string UserEmail);

        [OperationContract]
        bool UpdateDefaultApp(string UserName, string Password, string DefaultApplication);

        [OperationContract]
        string getDefaultApp(string UserName, string Password);

        [OperationContract]
        string GetEmployeesData(string values);

        [OperationContract]
        string GetOrganizationStruct(string Username, string Password, string values ,int level);

        [OperationContract]
        string getUserLogged(string username);

        [OperationContract]
        bool crUserLogged(string username, string password, string info);

        [OperationContract]
        bool chUserLogged(string username, string password, string changes);

        [OperationContract]
        bool delUserLogged(string user, string password, string username);

        [OperationContract]
        string getRolePR(string user, string password, string App, string roleId);

        [OperationContract]
        string GetEmployees(List<string> values);

        [OperationContract]
        string GetEmployeeDatas(List<string> values);

        [OperationContract]
        string GetEmployeesNum();

        [OperationContract]
        string getUserRole(string username, string app);

        [OperationContract]
        string ChangePassword(string username, string currentPassword, string newPassword);

        [OperationContract]
        bool IsPasswordMustBeChanged(string username, string password);
    }
}
