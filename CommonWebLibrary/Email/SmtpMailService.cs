﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

// niit.yudha - 16 april 2013

namespace Toyota.Common.Web.Email
{
    public class SmtpMailService
    {

        private static readonly HashSet<ISmtpMailService> stored = new HashSet<ISmtpMailService>();
        private static readonly object instance = new SmtpMailService();

        private SmtpMailService()
        {
            Assembly ass = Assembly.GetExecutingAssembly();
            Type itype;
            foreach (Type type in ass.GetExportedTypes())
            {
                itype = type.GetInterface("ISmtpMailService");
                if (itype != null)
                {
                    stored.Add((ISmtpMailService)Activator.CreateInstance(type));
                }
            }
        }

        public static ISmtpMailService GetInstanceOf<T>()
        {
            Type type = typeof(T);
            foreach (object obj in stored)
            {
                if (type.IsInstanceOfType(obj))
                { return (ISmtpMailService)obj; }
            }
            return null;
        }

    }
}
