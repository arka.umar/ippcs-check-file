﻿/*
 * Niit.Mulki
 * 27-11-2012
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;

namespace Toyota.Common.Web.Email
{
    public class MailSetting
    {
        private string userName;
        public string UserName
        {
            set { userName = value; }
            get { return userName; }
        }

        private string passwd;
        public string Password
        {
            set { passwd = value; }
            get { return passwd; }
        }

        private string host;
        public string Host
        {
            set { host = value; }
            get { return host; }
        }

        private int port;
        public int Port
        {
            set { port = value; }
            get { return port; }
        }

        private bool enableSSL;
        public bool EnableSSL
        {
            set { enableSSL = value; }
            get { return enableSSL; }
        }

        public MailSetting()
        { }
    }
}
