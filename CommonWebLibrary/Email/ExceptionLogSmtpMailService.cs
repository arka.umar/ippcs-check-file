﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc.Razor;
using System.IO;
using System.Configuration;
using System.Web;

// niit.yudha - 16 april 2013

namespace Toyota.Common.Web.Email
{
    public class ExceptionLogSmtpMailService : ISmtpMailService
    {
        
        private SmtpEmail mail = new SmtpEmail();
        private string[] mailTo;
        private string mailFrom;
        private string body;
        private string subject;
        private MailSetting setting;

        public ExceptionLogSmtpMailService()
        {
            this.setting = new MailSetting();
            this.GetMailSetting();
        }

        public bool Send(string[] args)
        {
            try
            {
                body = string.Format(body, args[0], args[1], args[2], args[7] + " - " + args[3] + " at " + args[6], args[4], args[5]);
                mail.IsBodyHtml = true; 
                mail.Setting = setting;
                mail.Subject = string.Format(subject, args[7]);
                mail.MailTo = mailTo;
                mail.MailFrom = mailFrom;
                mail.Body = body; 
                return mail.Send();
            }
            catch (Exception ex)
            { throw (ex); }
        }

        private string GetBody(string FileName)
        {
            string ret;
            using (StreamReader reader = File.OpenText(FileName))
            { ret = reader.ReadToEnd(); }
            return ret;
        } 

        public MailSetting GetMailSetting()
        {
            if (setting != null)
            { 
                string host = System.Configuration.ConfigurationManager.AppSettings["EmailHost"];
                string port = System.Configuration.ConfigurationManager.AppSettings["EmailPort"];
                string mailTos = System.Configuration.ConfigurationManager.AppSettings["ExceptionEmailTo"];
                subject = System.Configuration.ConfigurationManager.AppSettings["ExceptionEmailSubject"];
                String MailPath = String.Format(System.Configuration.ConfigurationManager.AppSettings["ExceptionEmailBody"], HttpContext.Current.Server.MapPath("~"));
                body = GetBody(MailPath);

                mailFrom = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"]; 
                mailTo = mailTos.Split(';');
                 
                if (string.IsNullOrEmpty(subject))
                    throw new ConfigurationErrorsException("App settings \'ExceptionEmailSubject\' cannot be null, please check at web.config.");

                if (mailTo.Length == 0)
                    throw new ConfigurationErrorsException("App settings \'ExceptionEmailTo\' cannot be null, please check at web.config.");

                if (string.IsNullOrEmpty(host) && string.IsNullOrEmpty(port) && string.IsNullOrEmpty(body) && string.IsNullOrEmpty(mailFrom))
                    throw new ConfigurationErrorsException("App settings \'EmailHost\' or \'EmailPort\' or \'ExceptionEmailBody\' or \'EmailFrom\' cannot be null, please check at web.config.");

                setting.Host = host;
                setting.Port = Convert.ToInt32(port);
            }
            return setting;
        }
         
    }
}
