﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Http;
using Toyota.Common.Web.Util.Converter;

namespace Toyota.Common.Web.Validation
{
    public class InputValidation
    {
        private StandardTypeConverter converter;
        private Dictionary<string,string> inputData;

        public InputValidation(Dictionary<string,string> inputData)
        {            
            Valid = false;
            converter = new StandardTypeConverter();
            if (inputData == null)
            {
                this.inputData = new Dictionary<string, string>();
            }
            else
            {
                this.inputData = inputData;
            }
        }

        public string Name { set; get; }
        public bool Valid { set; get; }

        public T Get<T>(string key)
        {
            if (inputData.ContainsKey(key))
            {
                return converter.ConvertValue<T>(inputData[key]);
            }

            object val = null;
            return (T) val;
        }
    }
}
