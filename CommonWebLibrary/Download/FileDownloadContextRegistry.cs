﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Download
{
    public class FileDownloadContextRegistry
    {
        private static FileDownloadContextRegistry instance = null;
        private IDictionary<string, FileDownloadContext> contextMap;

        private FileDownloadContextRegistry() {
            contextMap = new Dictionary<string, FileDownloadContext>();
        }

        public static FileDownloadContextRegistry GetInstance()
        {
            if (instance == null)
            {
                instance = new FileDownloadContextRegistry();
            }
            return instance;
        }

        public void Add(FileDownloadContext context)
        {
            string name = context.Name;
            if (contextMap.ContainsKey(name))
            {
                contextMap[name] = context;
            }
            else
            {
                contextMap.Add(name, context);
            }
        }

        public void Remove(string name)
        {
            contextMap.Remove(name);
        }

        public FileDownloadContext Get(string name)
        {
            if (contextMap.ContainsKey(name))
            {
                return contextMap[name];
            }
            return null;
        }
    }
}
