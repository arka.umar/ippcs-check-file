﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Download
{
    public class FileDownloadContext
    {
        public string Name { set; get; }
        public FileDownloadContextType Type { set; get; }
        public string Path { set; get; }
    }
}
