﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Download
{
    interface IApplicationIcon<T>
    {
        T GetIcon(string fileType);
    }
}
