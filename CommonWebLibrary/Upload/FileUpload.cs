﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Upload
{
    public class FileUpload
    {
        public long Id { set; get; }
        public string Group { set; get; }
        public string Key { set; get; }
        public string Qualifier { set; get; }
        public string Path { set; get; }
        public string FileName { set; get; }
        public string Description { set; get; }
        public DateTime CreationDate { set; get; }
    }
}
