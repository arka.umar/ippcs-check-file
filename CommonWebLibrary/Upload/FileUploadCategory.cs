﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Upload
{
    public class FileUploadCategory
    {
        public const string INCREMENTAL = "incremental";
        public string Group { set; get; }
        public string Key { set; get; }
        public string Qualifier { set; get; }
    }
}
