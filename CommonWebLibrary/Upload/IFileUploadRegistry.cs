﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Upload
{
    public interface IFileUploadRegistry
    {
        void Save(FileUpload file, User user);
        void Save(FileUpload[] files, User user);
        List<FileUpload> List(FileUploadCategory category);
        FileUpload GetById(long id);
        bool RemoveById(long id);
    }
}
