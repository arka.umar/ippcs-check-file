﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Upload
{
    public class FileUploadRegistry
    {
        private static IFileUploadRegistry instance = new DefaultFileUploadRegistry();

        public static IFileUploadRegistry GetInstance()
        {
            return instance;
        }
    }
}
