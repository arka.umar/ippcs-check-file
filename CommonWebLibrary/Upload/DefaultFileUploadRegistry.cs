﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Html.Helper.DevExpress;

namespace Toyota.Common.Web.Upload
{
    class DefaultFileUploadRegistry: IFileUploadRegistry
    {
        public void Save(FileUpload file, User user)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            SaveFile(db,file,user);            
            db.Close();
        }

        public void Save(FileUpload[] files, User user)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);            
            foreach (FileUpload file in files)
            {
                SaveFile(db,file,user);
            }            
            db.Close();
        }

        private void SaveFile(IDBContext db, FileUpload file, User user) {
            if (file.Qualifier.Equals(FileUploadCategory.INCREMENTAL))
            {
                int? incrementalQualifier = GetIncrementalQualifier(db, file.Group, file.Key);
                file.Qualifier = Convert.ToString(incrementalQualifier.Value);
            }            

            db.Execute("FileUpload_Save", new object[] {
                file.Group, file.Key, file.Qualifier, 
                file.Path, file.FileName, file.Description,
                user.Username, DateTime.Now
            });
        }

        private int? GetIncrementalQualifier(IDBContext db, string group, string key)
        {
            int? qualifier = db.SingleOrDefault<int?>("FileUpload_GetIncrementalQualifier", new object[] { group, key });
            return qualifier.HasValue ? (qualifier + 1) : 1;
        }

        public List<FileUpload> List(FileUploadCategory category)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<FileUpload> result = db.Fetch<FileUpload>("FileUpload_ListByCategory", new object[] {
                category.Group, category.Key, category.Qualifier
            });
            db.Close();
            return result;
        }

        public FileUpload GetById(long id)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            FileUpload file = db.SingleOrDefault<FileUpload>("FileUpload_GetById", new object[] { id });
            db.Close();
            return file;
        }

        public bool RemoveById(long id)
        {
            bool succeed = true;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                int affectedRow = db.Execute("FileUpload_RemoveById", new object[] { id });
                succeed = affectedRow > 0;
            }
            catch (Exception ex)
            {
                succeed = false;
            }
            finally
            {
                db.Close();
            }
            return succeed;
        }
    }
}
