﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.FTP
{
    class FTPModel
    {
        public string FtpIpAddress { get; set; }
        public string FtpPassword { get; set; }
        public string FtpUsername { get; set; }
    }
}
