﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.FTP
{
    interface IFTPUpload
    {
        bool ftpUpload(string directoryPath, string filename, string filepath, ref bool result, ref string errMessage);
    }
}
