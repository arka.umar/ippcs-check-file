﻿/*
 NIIT Mulki
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Configuration;

namespace Toyota.Common.Web.FTP
{
    public class FTPSettings
    {
        public string IP { set; get; }
        public string UserID { set; get; }
        public string Password { set; get; }
        public bool UsePassive { set; get; }
        public bool UseBinary { set; get; }
        public bool UseSSL { set; get; }
        public bool Hash { set; get; }
        public bool KeepAlive { set; get; }

        public FTPSettings(): this(true)
        {
        }

        public FTPSettings(bool commonConfiguration)
        {
            UseBinary = true;
            if (commonConfiguration)
            {
                List<ConfigurationItem> configs = ApplicationConfiguration.GetInstance().GetByKey("Ftp");
                foreach (ConfigurationItem cfg in configs)
                {
                    if (cfg.Qualifier.Equals("FtpIpAddress"))
                    {
                        IP = cfg.TextValue;
                    }
                    else if (cfg.Qualifier.Equals("FtpPassword"))
                    {
                        Password = cfg.TextValue;
                    }
                    else if (cfg.Qualifier.Equals("FtpUsername"))
                    {
                        UserID = cfg.TextValue;
                    }
                }
            }            
        }

        public string FtpPath(string pathSetting)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(pathSetting))
            {
                List<ConfigurationItem> configs = ApplicationConfiguration.GetInstance().GetByKey("FtpPath");

                if (configs.Count > 0)
                {
                    foreach (ConfigurationItem cfg in configs)
                    {
                        if (cfg.Qualifier.Equals(pathSetting))
                        {
                            result = cfg.TextValue;
                            return result;
                        }
                        else
                        {
                            result = string.Empty;
                        }
                    }
                    if (result == "")
                        throw new Exception("Configuration for " + pathSetting + " is not found in database");
                }
                else
                {
                    result = string.Empty;
                    throw new Exception("Configuration for Ftp Path is not found in database");
                }
            }

            return result;
        }
    }
}
