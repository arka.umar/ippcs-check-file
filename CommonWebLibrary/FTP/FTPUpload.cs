﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Collections;
using Toyota.Common.Web.FTP;

namespace Toyota.Common.Web.FTP
{
    public class FTPUpload
    {
        /* property FTP */
        private string ftpUri;
        private bool ftpPassive = true;
        private bool ftpBinary = true;
        private Boolean ftpSSL = false;
        private bool ftpHash = false;
        private string Directory;

        private string[] delimeterChars = { " ", @"/", @"\", @"//", @"\\" };
        private string tempPath = string.Empty;
        private bool isRoot = true;
        private FTPSettings setting;

        public FTPSettings Setting
        {
            get { return setting; }
            set { setting = value; }
        }

        public FTPUpload()
        {
            Setting = new FTPSettings();
        }

        public FTPUpload(bool commonConfiguration)
        {
            Setting = new FTPSettings(commonConfiguration);
        }
        /// <summary>
        /// Represents the FTP NLIST protocol method that gets a short listing of the files on an FTP server IsExist or Not.
        /// </summary>
        public bool IsDirectoryExist(string rootDir, string subdir)
        {
            string[] folders = DirectoryList(rootDir);
            if (folders != null)
            {
                if (rootDir.Equals("/"))
                {
                    return (folders.Length > 0) && (folders.Contains<string>(subdir));
                }
                else
                {
                    string[] rootFracs = rootDir.Split('/');
                    return (folders.Length > 0) && (folders.Contains<string>(rootFracs[rootFracs.Length - 2] + "/" + subdir) || folders.Contains<string>(subdir));
                }                
            }
            return false;
        }

        public bool directoryExists(string directory)
        {
            var ftpRequest = CreateFtpRequest("ftp://" + setting.IP + GetDirectoryPath(directory), WebRequestMethods.Ftp.ListDirectory);
            try
            {
                using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

                    /* Resource Cleanup */
            finally
            {
                ftpRequest = null;
            }
        }

        private string GetAbsolutePath(string path)
        {
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }
            return "ftp://" + setting.IP + path;
        }
        private bool IsDirectory(string path)
        {
            bool isdirectory = false;

            System.IO.FileAttributes fileattr = System.IO.File.GetAttributes(path);

            if ((fileattr & FileAttributes.Directory) != 0)
            {
                isdirectory = true;
            }

            return isdirectory;
        }
        /// <summary>
        /// Represents the FTP MKD protocol method creates a directory on an FTP server. 
        /// </summary>
        public void CreateDirectory(string directoryPath)
        {
            string s = String.Empty;
            Stream ftpStream = null;

            try
            {
                string[] folders = directoryPath.Split(delimeterChars, System.StringSplitOptions.RemoveEmptyEntries);
                string root = "/";
                for (int i = 0; i < folders.Length; i++)
                {
                    if (i > 0)
                    {
                        root = root + folders[i-1] + "/";
                    }
                    if (!IsDirectoryExist(root, folders[i]))
                    {
                        var reqFTP = CreateFtpRequest(GetAbsolutePath(root + "/" + folders[i]), WebRequestMethods.Ftp.MakeDirectory);
                        FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                        if (response.StatusCode == FtpStatusCode.PathnameCreated)
                        {
                            ftpStream = response.GetResponseStream();
                            ftpStream.Close();
                            response.Close();
                        }
                    }
                }
                //string path = string.Empty;
                //path = "/";
                //foreach (string folder in folders)
                //{
                //    tempPath = folder;
                //    if (!IsDirectoryExist(path))
                //    {
                //        var reqFTP = CreateFtpRequest("ftp://" + setting.IP + path + tempPath, WebRequestMethods.Ftp.MakeDirectory);
                //        FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                //        if (response.StatusCode == FtpStatusCode.PathnameCreated)
                //        {
                //            ftpStream = response.GetResponseStream();
                //            ftpStream.Close();
                //            response.Close();
                //        }
                //        isRoot = false;                       
                //    }
                //    path += folder + "/";                    
                //}
            }
            catch (Exception ex)
            {
                if (ftpStream != null)
                {
                    ftpStream.Close();
                    ftpStream.Dispose();
                }
                throw new Exception(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Rename Directory or Folder
        /// </summary>
        public bool FtpRenameDir(string directoryPath, string directoryName, string renameTo, ref string errMessage)
        {
            try
            {
                var reqFTP = CreateFtpRequest("ftp://" + setting.IP + GetDirectoryPath(directoryPath) + "/" + directoryName, WebRequestMethods.Ftp.Rename);
                reqFTP.RenameTo = renameTo;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Represents the FTP RMD protocol method that removes a directory. 
        /// </summary>
        public bool RemoveDirectory(string directoryPath, ref string errMessage)
        {
            try
            {
                var reqFTP = CreateFtpRequest("ftp://" + setting.IP + GetDirectoryPath(directoryPath), WebRequestMethods.Ftp.RemoveDirectory);

                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Represents the FTP RETR protocol method that is used to download a file from an FTP server. 
        /// </summary>
        public bool FtpDownload(string sourcePath, string destinationPath, ref string errMessage)
        {
            try
            {
                var reqFTP = CreateFtpRequest(sourcePath, WebRequestMethods.Ftp.DownloadFile);

                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();

                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                FileStream outputStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
 
                int bufferSize = 1024;
                int readCount;
                byte[] buffer = new byte[bufferSize];
 
                readCount = responseStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = responseStream.Read(buffer, 0, bufferSize);
                }
                string statusDescription = response.StatusDescription;
                responseStream.Close();
                outputStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return false;
            }
            return true;
        }

        public byte[] FtpDownloadByte(string sourcePath, ref string errMessage)
        {
            byte[] result;
            try
            {
                var reqFTP = CreateFtpRequest(sourcePath, WebRequestMethods.Ftp.DownloadFile);

                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();

                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);                

                using (MemoryStream memstream = new MemoryStream())
                {
                    reader.BaseStream.CopyTo(memstream);
                    result = memstream.ToArray();
                }
    
                responseStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                result = new byte[1024];
                errMessage = ex.Message;
                return result;
            }
            return result;
        }

        /// <summary>
        /// Represents the FTP STOR protocol method that uploads a file to an FTP server.
        /// </summary>
        public bool FtpUpload(string directoryPath, string filename, string filepath, ref string errMessage)
        {
            try
            {
                CreateDirectory(directoryPath);
                byte[] buffer = ReadFile(filepath + filename);

                Stream ftpstream = GetRequestStream(directoryPath, filename);
                ftpstream.Write(buffer, 0, buffer.Length);
                ftpstream.Close();
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Represents the FTP STOR protocol method that uploads a file bytes to an FTP server.
        /// </summary>
        public bool FtpUpload(string directoryPath, string filename, byte[] data, ref string errMessage)
        {
            try
            {
                CreateDirectory(directoryPath);
                Stream ftpstream = GetRequestStream(directoryPath, filename);
                ftpstream.Write(data, 0, data.Length);
                ftpstream.Close();
                return true;
            }
            catch (WebException ex)
            {
                errMessage = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Represents the FTP DELE protocol method that is used to delete a file on an FTP server. 
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="filename"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public bool FTPRemoveFile(string directoryPath, string filename, ref string errMessage)
        {
            try
            {
                var reqFTP = CreateFtpRequest("ftp://" + setting.IP + GetDirectoryPath(directoryPath) + "/" + filename, WebRequestMethods.Ftp.DeleteFile);

                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Rename file
        /// </summary>
        public bool FtpRenameFile(string directoryPath, string filename, string renameTo, ref string errMessage)
        {
            try
            {
                var reqFTP = CreateFtpRequest("ftp://" + setting.IP + GetDirectoryPath(directoryPath) + "/" + filename, WebRequestMethods.Ftp.Rename);
                reqFTP.RenameTo = renameTo;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Represents the FTP NLIST protocol method that gets a short listing of the files on an FTP server. 
        /// </summary>
        public string[] DirectoryList(string directoryPath)
        {
            StringBuilder result = new StringBuilder();
            WebResponse response = null;

            var reqFTP = CreateFtpRequest("ftp://" + setting.IP + GetDirectoryPath(directoryPath), WebRequestMethods.Ftp.ListDirectory);

            reqFTP.KeepAlive = false;
            reqFTP.UsePassive = false;
            response = reqFTP.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            string line = reader.ReadLine();
            while (line != null)
            {
                result.Append(line);
                result.Append("\n");
                line = reader.ReadLine();
            }

            return result.ToString().Split('\n');
        }

        /// <summary>
        /// 
        /// </summary>
        private Stream GetRequestStream(string directoryPath, string filename)
        {
            Directory = "ftp://" + setting.IP + GetDirectoryPath(directoryPath) + "/";
            string ftpfullpath = Directory + "/" + filename;
            var reqFTP = CreateFtpRequest(ftpfullpath, WebRequestMethods.Ftp.UploadFile);
            Stream ftpstream = reqFTP.GetRequestStream();
            return ftpstream;
        }

        /// <summary>
        /// Return byte of file
        /// </summary>
        private byte[] ReadFile(string Path)
        {
            byte[] buffer = null;
            using (FileStream fs = File.OpenRead(Path))
            {
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
            }
            return buffer;
        }
                
        /// <summary>
        /// Get directory path
        /// </summary>
        private string GetDirectoryPath(string directoryPath)
        {
            string[] folders = directoryPath.Split(delimeterChars, System.StringSplitOptions.RemoveEmptyEntries);
            string path = string.Empty;
            foreach (string folder in folders)
            {
                path += "/" + folder;
            }
            return path;
        }

        /// <summary>
        /// Create FTP Request
        /// </summary>
        private FtpWebRequest CreateFtpRequest(string uri, string ftpMethod)
        {
            var requestFTP = (FtpWebRequest)WebRequest.Create(new Uri(uri));

            requestFTP.Credentials = new NetworkCredential(setting.UserID, setting.Password);
            requestFTP.Method = ftpMethod;
            requestFTP.UseBinary = setting.UseBinary;
            requestFTP.EnableSsl = setting.UseSSL;
            requestFTP.UsePassive = setting.UsePassive;
            requestFTP.Proxy = new WebProxy();
            return requestFTP;

        }        

    }
}