SELECT 
    'All' AS Username,
   'All' AS Firstname,
   '' Lastname 
UNION ALL (
	SELECT 
	    Username,
		FirstName,
	    LastName
	FROM (
		SELECT 
			U.username AS Username,
			U.username AS FirstName,
			U.last_name AS LastName
		FROM TMMIN_ROLE.dbo.tb_m_user U
		UNION ALL
		SELECT DISTINCT UR.USERNAME AS Username,
			   UR.USERNAME AS FirstName,
			   ' ' AS LastName
		FROM IPPCS_QA.dbo.TB_R_VW_USER_ROLE UR
		WHERE ROLE_NAME LIKE 'Supplier%'
		OR    ROLE_NAME = 'System Administrator'
	) AS Source 
		EXCEPT 
		   SELECT 'ALL' AS Username,
		   'All' AS Firstname,
		   '' AS Lastname
)

