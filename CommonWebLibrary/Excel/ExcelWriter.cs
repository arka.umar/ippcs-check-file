﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Excel
{
    public class ExcelWriter
    {
        private readonly static IExcelWriter instance = new NPOIWriter();

        public static IExcelWriter GetInstance()
        {
            return instance;
        }
    }
}
