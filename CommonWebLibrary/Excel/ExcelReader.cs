﻿/*
 * niit.diko mulyadi 15 nov 2012
 */ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using System.Reflection;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using System.Diagnostics;
using System.IO;
using System.Globalization;

namespace Toyota.Common.Web.Excel
{
    public class ExcelReader 
    {
         

        public static string GetCell(string path, string SheetName, int row, int column)
        {
            object value = null;
            using (StreamReader input = new StreamReader(path))
            {
                IWorkbook workbook = new HSSFWorkbook(new POIFSFileSystem(input.BaseStream));
                IRow rowCheck = workbook.GetSheet(SheetName).GetRow(row - 1);
                if (null != rowCheck)
                {
                    value = rowCheck.GetCell(column - 1, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                }
                else
                {
                    value = String.Empty;
                }
            }
            return value.ToString();
        }

        public static ArrayList ReaderExcel(string path, string SheetName, int rowStart, int columnStart, int columnEnd)
        {
            try
            {
                ArrayList array = new ArrayList();
                
                //using (StreamReader input = new StreamReader(fileStream))
                //{
                    //IWorkbook workbook = new HSSFWorkbook(new POIFSFileSystem(fileStream));

                    //GAD
                FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
                 //FileStream fs = new FileStream(@"H:\Book2.xls", FileMode.Open, FileAccess.ReadWrite);
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fileStream);
                IWorkbook workbook = templateWorkbook;
                //HSSFSheet sheet = (HSSFSheet)templateWorkbook.GetSheet("Sheet1");
                fileStream.Close();
                
                //sheet.GetRow(0).GetCell(0).SetCellValue("Drago");
                //sheet.ForceFormulaRecalculation = true;
                //fs = new FileStream(@"H:\Book2.xls", FileMode.Open, FileAccess.ReadWrite);
                //templateWorkbook.Write(fs);
                //fs.Close();


                     //GAD


                    //if (null == workbook)
                    //{
                    //    Console.WriteLine(string.Format("Excel Workbook '{0}' could not be opened.", path));
                    //    return String.Empty;
                    //}
                    // calculates/updates the formulas on the given workbook
                    IFormulaEvaluator formulaEvaluator = new HSSFFormulaEvaluator(workbook);
                    //foreach (ISheet sheet in workbook)
                    //{

                    ISheet sheet = workbook.GetSheet(SheetName);
                    string value = String.Empty;

                    int rowStartTemp = rowStart - 1;
                    for (int i = rowStartTemp; i < rowStartTemp + 1; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        if (null != row)
                        {
                            int columnStartTemp = columnStart - 1;
                            int columnEndTemp = columnEnd - 1;
                            //ICell cell = row.GetCell(j);

                            for (int j = columnStartTemp; j <= columnEndTemp; j++)
                            {
                                //value = value + "," + "'" + sheet.GetRow(i).GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK) + "'";
                                //if(cell itu tidak sama dengan string/ tipe datanya itu numerik)
                                //{
                                //    if(cek lagi apakah tahun lebih besar daripada tahun skrg)
                                //    {
                                //        jika benar maka ambil DateTime nya
                                //    }
                                //}
                                if (row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).CellType == CellType.NUMERIC)
                                {
                                    if (HSSFDateUtil.IsCellDateFormatted(row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK)) == true)
                                    {
                                        value = value + ";" + "'" + row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).DateCellValue.ToString("yyyy-MM-dd") + "'";
                                    }
                                    else
                                    {
                                        value = value + ";" + "'" + Convert.ToString(row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK)).Replace(";", " ") + "'";
                                    }
                                    columnStartTemp++;
                                }
                                else if (row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).CellType == CellType.STRING)
                                {
                                    value = value + ";" + "'" + Convert.ToString(row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK)).Replace(";", " ") + "'";
                                    columnStartTemp++;
                                }
                                else if (row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).CellType == CellType.FORMULA)
                                {
                                    if (row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).CachedFormulaResultType == CellType.STRING)
                                    {
                                        value = value + ";" + "'" + Convert.ToString(row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue).Replace(";", " ") + "'";
                                    }
                                    else
                                    {
                                        value = value + ";" + "'" + Convert.ToString(row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).NumericCellValue).Replace(";", " ") + "'";
                                    }
                                    columnStartTemp++;
                                }
                                else if (row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).CellType == CellType.BLANK)
                                {
                                    //value = value + "," + "'" + "";
                                    value = value + ";" + "''";
                                    columnStartTemp++;
                                }

                                //HSSFDateUtil.IsCellDateFormatted(row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK));
                                //HSSFCell cell = row.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK).CellStyle;
                            }
                            if (!String.IsNullOrEmpty(value.Replace(";", "").Replace("'", "")))
                            {
                                rowStartTemp++;
                                array.Add(value);
                                value = String.Empty;
                            }
                        }
                    }
                    return array;
                //}
            }
            catch (Exception exc)
            {
                throw exc;
            }            
        }
         
    }
}

