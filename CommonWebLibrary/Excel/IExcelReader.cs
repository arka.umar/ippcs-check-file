﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Toyota.Common.Web.Excel
{
    public interface IExcelReader
    {
        ArrayList ReadExcel(string filePath, string sheetName, string colomnStart, string colomnEnd);
    }
}
