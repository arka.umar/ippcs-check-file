﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Excel
{
    public class ExcelReaderColumnName
    {
        public string ColumnName { set; get; }
        public string DataType { set; get; }
    }
}
