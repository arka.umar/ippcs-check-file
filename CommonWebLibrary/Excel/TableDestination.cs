﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Excel
{
    public class TableDestination
    {
        public string functionID { set; get; }
        public string processID { set; get; }
        public string tableFromTemp { set; get; }
        public string tableToAccess { set; get; }
        public string filePath { set; get; }
        public string sheetName { set; get; }
        public string rowStart { set; get; }
        public string columnStart { set; get; }
        public string columnEnd { set; get; }
    }
}
