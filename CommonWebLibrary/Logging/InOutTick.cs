﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Log;
using System.IO;
using System.Diagnostics;

namespace Toyota.Common.Web.Logging
{
    public class InOutTick : ITick
    {
        private string logPath;
        private string logFile;
        private bool initdone= false;
        private string logName;

        public InOutTick(string name = "Tick") {

            initdone = false;
            logName = name;
        }

        private void Init(string name)
        {
            DateTime d = DateTime.Now;
            logName = name;
            logPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
                    , "toyota"
                    , "IPPCS"
                    , d.Year.ToString(), d.Month.ToString(), d.Day.ToString());
            if (!Directory.Exists(logPath)) { Directory.CreateDirectory(logPath); }
            logFile = Path.Combine(logPath, logName + ".txt");
            File.AppendAllText(logFile, "\r\n");
            initdone = true;
        }

        private string LogFile
        {
            get
            {
                if (!initdone)
                {
                    Init(logName);
                }
                return logFile;
            }
        }
        private readonly string[] wop = new string[] { "    ", "In  ", "Out " };

        private int Log(string w, int Op = -1)
        {
            File.AppendAllText(LogFile, 
                DateTime.Now.ToString("HH:mm:ss.fff ")
                + wop[Op+1]
                + Tab(level) + w 
                + "\r\n");
            return 0;
        }

        private int pid = 0;

        private Stack<string> stak = null;
        private Stack<string> St
        {
            get
            {
                if (stak == null)
                {
                    stak = new Stack<string>();
                }
                return stak;
            }
        }

        private int level = 0;
        public int Level
        {
            get
            {
                return level;
            }

            set
            {
                level = value;
            }
        }

        private static string Tab(int n)
        {
            string r = "";
            for (int i = 0; i < n; i++)
            {
                r += "     ";
            }
            return r;
        }

        public void In(string w, params object[] x)
        {
            Op(0, string.Format(w, x));
        }

        public void Out(string w, params object[] x)
        {
            if (!string.IsNullOrEmpty(w))
            {
                w = string.Format(w, x);
                if (St.Contains(w))
                {
                    string y = null;
                    do
                    {
                        y = Op(1, w);
                    }
                    while (!y.Equals(x) && St.Count > 0);
                }
            }
            else
            {
                Op(1, w);
            }
        }

        public void Say(string w, params object[] x)
        {
            if (!initdone)
                Init(w);

            Op(-1, string.Format(w, x));
        }

        public string Op(int op = 0, string x = null)
        {
            string z = "";
            if (op == 0)
            {
                St.Push(x);
                z = x;

                pid = Log(x, 0);
                level++;

            }
            else if (op > 0)
            {
                if (St.Count > 0)
                    z = St.Pop();
                else
                    z = x;
                level--;
                Log(z, 1);
            }
            else
            {
                Log(x);
            }
            return z;
        }


        public void Trace(string w)
        {
            StackTrace t = new StackTrace(1, true);

            StackFrame[] f = t.GetFrames();
            StringBuilder sfs = new StringBuilder("\r\n");
            for (int i = 0; i < f.Length; i++)
            {
                StackFrame sf = f[i];
                System.Reflection.MethodBase m = sf.GetMethod();

                string cname = m.ReflectedType.FullName;
                if (cname.Contains(".InOutMark") || cname.Contains(".ITick") || cname.Contains(".Ticker"))
                {
                    continue;
                }

                sfs.AppendFormat("{0,4} {1} {2} \r\n", sf.GetFileLineNumber(), cname, m.Name);
            }

            Log(w + "\r\n" + sfs.ToString());
        }

        
    }
}
