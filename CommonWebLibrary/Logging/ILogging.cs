﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Logging
{
    public interface ILogging
    {
        void Write(EventType eventType, Exception ex);
        void Write(EventType eventType, string message);
    }
}
