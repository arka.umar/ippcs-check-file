﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Logging
{
    public interface ITick
    {
        void In(string w, params object[] x);
        int Level { get; set; }
        string Op(int op = 0, string x = null);
        void Out(string w = null, params object[] x);
        void Say(string w, params object[] x);
        void Trace(string w);
    }
    
}
