﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Logging
{
    public enum EventType
    {
        Warn,
        Error,
        Fatal,
        Debug,
    }
}
