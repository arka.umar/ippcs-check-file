﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Toyota.Common.Web.Util;

namespace Toyota.Common.Web.Logging
{
    public class log4netProvider : ILogging 
    {
        private log4net.ILog Logger;

        public log4netProvider()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            String URI = Namespace.InternalConfigurationResourceNamespace + ".EventLogAppender.xml";
            System.IO.Stream configStream = assembly.GetManifestResourceStream(URI);
            log4net.Config.XmlConfigurator.Configure(configStream);
            Logger = log4net.LogManager.GetLogger(typeof(log4netProvider));
        }
 
        public void Write(EventType eventType, Exception ex)
        {
            Write(eventType, ex.Message);
        }

        public void Write(EventType eventType, string message)
        {
            switch (eventType)
            {
                case EventType.Debug:
                    Logger.Debug( message);
                    break;
                case EventType.Error:
                    Logger.Error(message);
                    break;
                case EventType.Fatal:
                    Logger.Fatal(message);
                    break;
                case EventType.Warn:
                    Logger.Warn(message);
                    break;
            }
        }
    }
}
