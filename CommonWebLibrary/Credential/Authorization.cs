﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{

    [Serializable]
    public class Authorization
    {
        public List<AuthorizationDetail> AuthorizationDetails { set; get; } //
        public String ObjectClass { set; get; }
        public String BranchID { set; get; }
        public String AuthorizationID { set; get; }
        public String FileSentMaxSize { set; get; }
        public String AuthorizationName { set; get; }
        public String RoleSessionTimeOut { set; get; }
    }
      
}
