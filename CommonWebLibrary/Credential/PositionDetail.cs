﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    [Serializable]
    public class PositionDetail
    {
        public String UserName { set; get; }
        public String Line { set; get; }
        public String Position { set; get; }
        public String Group { set; get; }
        public String Section { set; get; }
        public String Department { set; get; }
        public String Division { set; get; }
        public String Directorate { set; get; }
        public String Branch { set; get; }
    }
}
