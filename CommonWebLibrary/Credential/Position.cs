﻿/*
    @Lufty
    Representing an employee division.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    [Serializable]
    public class Position
    {
        public String ID { set; get; }
        public String Name { set; get; }
        public Byte Class { set; get; }
    }
}
