﻿/*
    @Lufty
    Representing a user.
 * updated : 
 * 1. 22/11/2012 FID.Taufika | added positionID, SectionID, DivisionID, and DepartementID for user information (needed in Dataexch)
 * 2. 22/11/2012 nii.yudha | adding List of Role, RoleUser, RoleObject, RoleArea, FileType, FilteringArea and IsAuthorized
 * 3. 22/11/2012 nii.yudha | Replace positionID, SectionID, DivisionID, and DepartementID with Class
 * 4. 18/04/2013 nii.yudha | cleaning unused properties & fields, sync with OpenLdap Function

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Toyota.Common.Web.Util;
using System.Runtime.Remoting;
using System.Reflection;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Credential
{
    [Serializable]
    public class User
    {
        // '//' is OpenLDAP Fields
        public String ObjectClass { set; get; } //
        public String CN { set; get; } //
        public String SN { set; get; }  //
        public String NoReg { set; get; } //
        public String Email { set; get; } // 
        public String IsActive { set; get; } //
        public String LockFlag { set; get; } //
        public String mobileNo { set; get; } //
        public String ccCode { set; get; } //
        public String classId { set; get; } //
        public String DefaultApp { set; get; } //
        public String Username { set; get; } //
        public string CompanyID { set; get; } //
        public String GivenName { set; get; } //
        public String BranchName { set; get; } //
        public String LocationID { set; get; } //
        public string JobFunction { set; get; } //
        public string TelNo { set; get; } //
        public int UnlockTimeOut { set; get; } //
        public string CompanyName { set; get; } //
        public String Description { set; get; } //
        public List<RoleDetail> RoleIDDetails { set; get; } //
        public String IsProduction { set; get; } // 
        public String ActiveDirectoryFlag { set; get; }  //
        public List<PositionDetail> PositionDetails { set; get; } //
        public List<Authorization> Authorization { set; get; } // 
        public String AdsPath { set; get; } //

        public String LastName { set; get; }
        public String FirstName { set; get; }
        public Position Position { set; get; }
        public String PositionName { set; get; }
        public String PositionId { set; get; }
        public String PositionLevel { set; get; }
        
        public String Password { set; get; }
        public bool? PasswordMustBeChanged { set; get; }

        /* In Minutes */
        //public int RoleSessionTimeOut { set; get; } configure at web config
         
        private string fullName;
        public String FullName
        {
            set
            {
                fullName = value;
            }
            get
            {
                if (string.IsNullOrEmpty(fullName))
                {
                    return string.Format("{0} {1}", FirstName, LastName);
                }
                else
                {
                    return fullName;
                }
            }
        } //

        private List<Role> roles = null;
        public List<Role> Role {
            set { }
            get
            {
                if (roles == null)
                {
                    roles = new List<Role>();
                    if (RoleIDDetails != null)
                    {
                        Role role;
                        foreach(RoleDetail roleDetail in RoleIDDetails) {
                            role = new Role() { 
                                Name = String.Empty,
                                RoleID = Convert.ToInt32(roleDetail.RoleID)
                            };
                            //role.SessionTimeout = RoleSessionTimeOut;
                            roles.Add(role);
                        }
                    }
                }
                return roles;
            }
        } 
           
        public User()
        {
            string defaultTO = ApplicationContext.GetInstance().GetDefaultUnlockTimeout();
            if (!string.IsNullOrEmpty(defaultTO))
            {
                try
                { 
                    UnlockTimeOut = int.Parse(defaultTO);
                }
                catch (Exception ex)
                { throw ex; }
            }
            else
                UnlockTimeOut = int.MaxValue;
        }

        /// <summary>
        /// Check authorization control.
        /// </summary>
        /// <remarks>
        /// This is sample when calling IsAuthorized methods from View,
        /// How to use it?
        /// settings.ClienVisible = Model.GetModel<User>().IsAuthorized("Forecast", "btnSearch");
        /// </remarks>
        public int IsAuthorized(string ScreenID, string ControlName)
        {
            if (Authorization != null)
            { 
                var auth = (from z in Authorization
                            from a in z.AuthorizationDetails
                            where a.Object.ToLower() == ControlName.ToLower() &&
                                  a.Screen.ToLower() == ScreenID.ToLower() &&
                                  (a.Object_Auth.ToLower() != "null")
                            select a.Object_Auth).FirstOrDefault();

                if (auth != null && auth.Any())
                {
                    return Convert.ToInt32(auth);
                }
            }
            return -1;
        }

        /// <summary>
        /// Filtering Data by User Area
        /// </summary>
        /// <typeparam name="T">the Type data source that will be filtering</typeparam>
        /// <param name="DataSource">the data source that will be filtering</param>
        /// <remarks>The Control must contains "Value" property, furthermore will be ignore.
        /// Bind(Model.GetModel<User>().FilteringArea<SupplierName>(Model.GetModel<List<SupplierName>>(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption")).GetHtml()</remarks>
        /// <returns>If filtering doesn't match with Area, the result will be same with DataSource.</returns>
        public List<T> FilteringArea<T>(List<T> DataSource, string Key, string ScreenID, string ControlName)
        {
            if (DataSource != null && ScreenID != "" & ControlName != "" && Authorization != null)
            {
                List<string> comp = new List<string>();
                string auth;
                IEnumerable<string> listAuth;
                 
                Type type = DataSource.GetType();
                listAuth = (from authSource in Authorization
                            from item in authSource.AuthorizationDetails
                            where item.Screen.ToLower() == ScreenID.ToLower() &&
                                item.Object.ToLower() == ControlName.ToLower() &&
                                (item.AuthorizationGroup.ToLower() != "null")
                            select item.AuthorizationGroup);
                if ((listAuth != null) && (listAuth.Any()))
                {
                    auth = string.Join(";", listAuth.ToArray());
                    if (auth != null && auth.Length > 0)
                    {
                        comp = auth.Split(';').ToList();
                        if (DataSource.Count > 0)
                        {
                            if (DataSource.ElementAt(1) != null)
                            {
                                if (DataSource.ElementAt(1).GetType().GetProperty(Key) != null)
                                {
                                    var result = (from item in DataSource
                                                  where comp.Contains(item.GetType().GetProperty(Key).GetValue(item, null))
                                                  select item).ToList();
                                    return result;
                                }
                            }
                        }
                    }
                }   
            }
            return DataSource;
        }

        public static T Cast<T>(Dictionary<string, List<string>> Source)
        {
            T buffer = Activator.CreateInstance<T>();
            Type userType = typeof(T);
            PropertyInfo propertyOfKey;
            Type keyType;
            object objectCast;
            foreach (string key in Source.Keys)
            {
                propertyOfKey = userType.GetProperty(key, BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.IgnoreCase);
                if (propertyOfKey != null)
                {
                    if (key.Contains("details") || key.Contains("Details"))
                    {
                        keyType = Assembly.GetExecutingAssembly().GetExportedTypes().Where(item => key.ToLower().Substring(0, key.Length - 1) == item.Name.ToLower()).SingleOrDefault();
                        objectCast = InternalCast(keyType, Source[key]);
                        propertyOfKey.SetValue(buffer, objectCast, null);
                    }
                    else
                    {
                        if (propertyOfKey.PropertyType == typeof(int))
                        {
                            propertyOfKey.SetValue(buffer, Convert.ToInt32(Source[key][0]), null);
                        }
                        else
                            propertyOfKey.SetValue(buffer, Source[key][0], null);
                    }
                }
            }
            return buffer;
        }

        private static object InternalCast(Type keyType, List<string> Source)
        {
            ObjectHandle oList = (ObjectHandle)Activator.CreateInstance("mscorlib", "System.Collections.Generic.List`1[[" + keyType.FullName + ", " + keyType.AssemblyQualifiedName.Split(',')[1] + "]]");
            object listAuthorizationDetail = oList.Unwrap();
            object o;
            object oDetail;
            Type detailType;
            string[] listValues;
            PropertyInfo piDetail;
            string[] listDetails;
            Type listAuthorizationDetailType = listAuthorizationDetail.GetType();
            for (int i = 0; i < Source.Count; i++)
            {
                listDetails = Source[i].Split(',');
                o = Activator.CreateInstance(Assembly.GetExecutingAssembly().FullName, keyType.FullName);
                oDetail = ((ObjectHandle)o).Unwrap();
                detailType = ((ObjectHandle)o).Unwrap().GetType();
                foreach (string value in listDetails)
                {
                    listValues = value.Split('=');
                    piDetail = detailType.GetProperty(listValues[0], BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.IgnoreCase);
                    if (piDetail != null) piDetail.SetValue(oDetail, listValues[1], null);
                }
                listAuthorizationDetailType.InvokeMember("Add", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.Public, null, listAuthorizationDetail, new object[] { oDetail });
            }
            return listAuthorizationDetail;
        }

    } 
}
