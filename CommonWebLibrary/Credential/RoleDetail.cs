﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    [Serializable]
    public class RoleDetail
    {
        public String RoleID { set; get; }
    }
}
