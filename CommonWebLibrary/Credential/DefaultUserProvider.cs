﻿/*
    niit.yudha 5 nov 2012
    wrapper for getting data roles
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Credential;
using System.DirectoryServices;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Util.Converter;
using Newtonsoft.Json;

namespace Toyota.Common.Web.Credential
{
    public class DefaultUserProvider : IUserProvider<User, UserAD>, IDisposable
    {
        private readonly IDBContextManager dbManager;
        private IDBContext dbContext;
        private SearchResult sr;

        public bool IsAuthenticAD(string username, string password)
        {
            return new OpenLDAPAuthentication().IsAuthenticAD(username, password);
        }



        public DefaultUserProvider()
        {
            IProviderResolver providerResolver = ProviderResolver.GetInstance();
            dbManager = providerResolver.Get<IDBContextManager>();
        }
         
        public UserAD GetUser(string Username, string Password, string App)
        {
            User user = default(User);
            UserAD userAD = default(UserAD);

            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(Username, Password, App);
            if (sr != null)
            {
                userAD = new OpenLDAPAuthentication().readOpenLDAPAttrs(sr);
            }
            return userAD;
        }

        public List<User> GetUsers()
        {
            //dbContext = dbManager.GetContext(DBContextNames.DB_TMMIN_ROLE);
            //return dbContext.Fetch<User>("GetAllUser");
            //UserAD values = new UserAD();
            //values.properties.Add("isActive", new List<String> { "1" });
            //string json = json = JSON.ToString<Dictionary<string, List<string>>>(values.properties);

            //OpenLDAPAuthentication ldap = new OpenLDAPAuthentication();
            //List<UserAD> userADs = ldap.GetEmployeesData(json);
            //List<User> employee = new List<User>();
            //foreach (UserAD param in userADs)
            //{
            //    employee.Add(Toyota.Common.Web.Credential.User.Cast(param.properties));
            //}
            //return employee;

            dbContext = dbManager.GetContext(DBContextNames.DB_MASTER);
            List<User> users = dbContext.Fetch<User>("GetAllUser");
            dbContext.Close();
            return users;
        }

        public void Dispose()
        {
            dbContext.Close();
            GC.SuppressFinalize(this);
        }

        public bool ChgEmployeeAttr(string Username, string Password, string Branch, string attr, string OldVal, string NewVal)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(Username, Password);
            return new OpenLDAPAuthentication().changeEmployeeAttrs(sr, attr, NewVal, OldVal);
        }

        public int CrtEmployeeAttr(string Username, string Password, string Branch, string user)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(Username, Password);
            return new OpenLDAPAuthentication().createEmployee(sr, user);
        }

        public bool DelEmployee(string Username, string Password, string Branch, string user)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(Username, Password);
            return new OpenLDAPAuthentication().delEmployee(sr, user);
        }

        public bool IsAuthenticate(string username, string password)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(username, password);
            if (sr != null)
            {
                return true;
            }
            return false;
        }

        public List<string> AppList(string Username, string Password)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(Username, Password);
            return new OpenLDAPAuthentication().appListing(sr);
        }

        public UserAD GetEmailList(string Username, string Password, List<string> usernames)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(Username, Password);
            return new OpenLDAPAuthentication().emailListUsername(sr, usernames);
        }
         
        public bool RegisterCreate(string Username, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            return true;
        }

        public bool RegisterUpdate(string Username, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            return true;
        }

        public bool RegisterDelete(string Username, string Password, string UserEmail)
        {
            return true;
        }

        public bool RegisterIsExist(string Username, string Password, string UserEmail)
        {
            return true;
        }

        public UserAD GenerateNewUsername(string Username, string Password, string Email)
        {
            return null;
        }

        public UserAD GenerateNewPassword(string Username, string Password, string Email)
        {
            return null;
        }

        public bool IsDefaultApplicationExist(string Username, string Password, string UserEmail)
        {
            return true;
        }

        public bool UpdateDefaultApplication(string Username, string Password, string DefaultApplication)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(Username, Password);
            return new OpenLDAPAuthentication().SetDefaultApp(sr, DefaultApplication);
        }

        public UserAD GetDefaultApp(string UserName, string Password)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(UserName, Password);
            return new OpenLDAPAuthentication().GetDefaultApp(sr);
        }

        public List<UserAD> GetEmployeesData(string values)
        {
            return new OpenLDAPAuthentication().GetEmployeesData(values);
        }

        public List<UserAD> GetEmployeesData(List<string> values)
        {
            return new OpenLDAPAuthentication().GetEmployeesData(values);
        }

        public string getEmployeesNumber()
        {
            return new OpenLDAPAuthentication().GetEmployeesNumber();
        }

        public List<UserAD> GetOrganizationStruct(string UserName, string Password,string OrgId, string UnitId)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(UserName, Password);
            return new OpenLDAPAuthentication().GetOrganizationStruct(sr, OrgId,UnitId);
        }

        public List<UserAD> GetRoleChunk(string UserName, string Password, string AuthId, string UnitId)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(UserName, Password);
            return new OpenLDAPAuthentication().GetOrganizationStruct(sr, AuthId, UnitId);
        }

        public bool crUserLoggedInfo(string UserName, string Password, string info)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(UserName, Password);
            return new OpenLDAPAuthentication().crUserLoggedInfo(sr, info);
        }

        public bool chUserLoggedInfo(string UserName, string Password, string changes)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(UserName, Password);
            return new OpenLDAPAuthentication().chUserLoggedInfo(sr, changes);
        }

        public bool delUserLoggedInfo(string UserName, string Password, string username)
        {
            sr = new OpenLDAPAuthentication().IsAuthenticOpenLDAP(UserName, Password);
            return new OpenLDAPAuthentication().delUserLoggedInfo(sr, username);
        }

        public List<UserAD> GetUserLoggedInfo(string username)
        {
            return new OpenLDAPAuthentication().GetUserLoggedInfo(username);
        }

        public List<UserAD> GetUserRole(string username, string app)
        {
            return new OpenLDAPAuthentication().GetUserRole(username, app);
        }
    }
}
