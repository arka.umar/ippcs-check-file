﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    [Serializable]
    public class AuthorizationDetail
    { 
        public String AuthorizationGroup { set; get; }
        public String Object { set; get; }
        public String Action { set; get; }
        public String Screen { set; get; }
        public String Role { set; get; }        
        public String System { set; get; }
        public String Branch { set; get; }
        public string Object_Auth { get; set; }
        public string Action_Auth { get; set; } 
        public string Screen_Auth { get; set; } 
    }
}
