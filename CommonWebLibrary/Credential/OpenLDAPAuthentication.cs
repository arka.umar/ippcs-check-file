﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.DirectoryServices;
using System.Reflection;
using Newtonsoft.Json;
using Cryptography;
using System.IO;
using System.Threading;
using Toyota.Common.Web.Util;
using Toyota.Common.Util.Text;

namespace Toyota.Common.Web.Credential
{

    /*
    public class worker
    {
        private string args;

        public worker()
        {
        }

        public void doWork(object param)
        {
            args = (string)param;
            Thread.Sleep(50000);
            string openString = DateTime.Now.ToString() + " | Service Started";
            string path = @"D:\log.txt";

            if (File.Exists(path))
            {
                using (StreamWriter writer = File.AppendText(path))
                {
                    writer.WriteLine(openString);
                    //foreach (string item in args)
                    writer.Write(args);
                    writer.WriteLine("\n");
                    writer.Close();
                }
            }
            else
            {
                File.WriteAllText(path, openString);
                using (StreamWriter writer = File.AppendText(path))
                {
                    //foreach (string item in args)
                    writer.Write(args);
                    writer.WriteLine("\n");
                    writer.Close();
                }
            }
        }
    }
    */


    public class OpenLDAPAuthentication
    {
        public bool IsAuthenticAD(string Username, string Password)
        {
            String domain = "toyota";
            String domainAndUsername = domain + @"\" + Username;
            DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["AD"], domainAndUsername, Password);
            try
            {
                //Bind to the native AdsObject to force authentication.			
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(&(SAMAccountName=" + Username + ")(!(userAccountControl:1.2.840.113556.1.4.803:=2)))";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (Convert.ToBoolean(result.GetDirectoryEntry().InvokeGet("IsAccountLocked")))
                {
                    return false;
                }

                if (null == result)
                {
                    return false;
                }
                search.Dispose();
            }
            catch (Exception ex)
            {
                return false;
            }
            entry.Close();
            entry.Dispose();
            return true;
        }

        //for credential only
        public SearchResult IsAuthenticOpenLDAP(string Username, string Password)
        {
            //worker worker = new worker();
            //Thread workerThread = new Thread(worker.doWork);
            //workerThread.Start(Username);

            try
            {
                //Set the connection estabilishment
                DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
                entry.AuthenticationType = AuthenticationTypes.None;
                entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
                entry.Password = "DefaultPassword";

                //This is how to get the attribute's value from OpenLDAP
                DirectorySearcher nds = new DirectorySearcher(entry);
                nds.SearchScope = SearchScope.Subtree;
                nds.Filter = "(&(objectClass=employee)(cn=" + Username + "))";
                //nds.Filter = "(&(objectClass=employee)(cn=" + username + ")(branchName=" + Branch + "))";
                SearchResult sr = nds.FindOne();

                if (sr != null)
                {
                    //we check for the attribute flag
                    int lockFlag = int.Parse(sr.Properties["lockFlag"][0].ToString());
                    if (lockFlag != 1)
                    {
                        int isActive = int.Parse(sr.Properties["isActive"][0].ToString());
                        if (isActive == 1)
                        {
                            int ADFlag = int.Parse(sr.Properties["activeDirectoryFlag"][0].ToString());
                            if (ADFlag == 1)
                            {
                                if (IsAuthenticAD(Username, Password))
                                    Password = "DefaultPassword";
                                else
                                    return null;
                            }
                            string spesUserGroup = sr.GetDirectoryEntry().Parent.Properties["ou"][0].ToString();
                            string userGroup = sr.GetDirectoryEntry().Parent.Parent.Properties["ou"][0].ToString();
                            entry.Username = "cn=" + Username + ",ou=" + spesUserGroup + ",ou=" + userGroup + ",dc=maxcrc,dc=com";
                            entry.Password = Password;

                            nds.Filter = "(&(objectClass=employee)(cn=" + Username + "))";
                            sr = nds.FindOne();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                nds.Dispose();
                entry.Close();
                return sr;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //for authorization and shows employee other informations
        public SearchResult IsAuthenticOpenLDAP(string Username, string Password, string app)
        {
            /*
            worker worker = new worker();
            Thread workerThread = new Thread(worker.doWork);
            workerThread.Start(Username);
             * */
            try
            {
                //Set the connection estabilishment
                DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
                entry.AuthenticationType = AuthenticationTypes.None;
                entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
                entry.Password = "DefaultPassword";

                //This is how to get the attribute's value from OpenLDAP
                DirectorySearcher nds = new DirectorySearcher(entry);
                nds.SearchScope = SearchScope.Subtree;
                nds.Filter = "(&(objectClass=employee)(cn=" + Username + "))";
                SearchResult sr = nds.FindOne();

                if (sr != null)
                {
                    //we check for the attribute flag
                    int lockFlag = int.Parse(sr.Properties["lockFlag"][0].ToString());
                    if (lockFlag != 1)
                    {
                        int isActive = int.Parse(sr.Properties["isActive"][0].ToString());
                        if (isActive == 1)
                        {
                            int ADFlag = int.Parse(sr.Properties["activeDirectoryFlag"][0].ToString());
                            if (ADFlag == 1)
                            {
                                if (IsAuthenticAD(Username, Password))
                                    Password = "DefaultPassword";
                                else
                                    return null;
                            }
                            string spesUserGroup = sr.GetDirectoryEntry().Parent.Properties["ou"][0].ToString();
                            string userGroup = sr.GetDirectoryEntry().Parent.Parent.Properties["ou"][0].ToString();
                            entry.Username = "cn=" + Username + ",ou=" + spesUserGroup + ",ou=" + userGroup + ",dc=maxcrc,dc=com";
                            entry.Password = Password;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                nds.Dispose();
                entry.Close();
                return sr;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public bool delEmployee(SearchResult sr, string username)
        {
            // This is how to delete person of OpenLDAP could be done
            DirectorySearcher nds = new DirectorySearcher(sr.GetDirectoryEntry().Parent.Parent);
            nds.SearchScope = SearchScope.Subtree;
            nds.Filter = "(&(objectClass=employee)(cn=" + username + "))";
            SearchResult result = nds.FindOne();

            sr.GetDirectoryEntry().Children.Remove(result.GetDirectoryEntry());
            sr.GetDirectoryEntry().CommitChanges();
            sr.GetDirectoryEntry().Close();
            return true;
        }

        public List<string> appListing(SearchResult sr)
        {
            List<string> appList = new List<string>();
            DirectorySearcher nds = new DirectorySearcher(sr.GetDirectoryEntry().Parent);
            nds.SearchScope = SearchScope.Subtree;
            //nds.PropertiesToLoad.Add("AppName");
            nds.Filter = "(&(objectClass=APP)(AppName=*))";
            foreach (SearchResult app in nds.FindAll())
                appList.Add(app.Properties["AppName"][0].ToString());
            sr.GetDirectoryEntry().Close();
            nds.Dispose();
            return appList;
        }

        //we read based on Search Result
        public UserAD readOpenLDAPAttrs(SearchResult sr)
        {
            try
            {
                UserAD userAd = new UserAD();
                foreach (string property in sr.Properties.PropertyNames)
                {
                    List<string> values = new List<string>();
                    for (int index = 0; index < sr.Properties[property].Count; index++)
                    {
                        values.Add(sr.Properties[property][index].ToString());
                    }
                    userAd.properties.Add(property, values);
                }
                return userAd;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //we read based on Directory Entry
        public UserAD readOpenLDAPAttrs(DirectoryEntry sr)
        {
            try
            {
                UserAD userAd = new UserAD();
                foreach (string property in sr.Properties.PropertyNames)
                {
                    List<string> values = new List<string>();
                    for (int index = 0; index < sr.Properties[property].Count; index++)
                    {
                        values.Add(sr.Properties[property][index].ToString());
                    }
                    userAd.properties.Add(property, values);
                }
                return userAd;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean changeEmployeeAttrs(SearchResult sr, string attr, string NewVal, string OldVal)
        {
            try
            {
                DirectoryEntry employee = sr.GetDirectoryEntry();
                for (int index = 0; index < employee.Properties[attr].Count; index++)
                    if (OldVal.Equals(employee.Properties[attr][index].ToString()))
                        employee.Properties[attr][index] = NewVal;
                employee.CommitChanges();
                employee.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public int createEmployee(SearchResult sr, string EncrUser)
        {
            //This is how to get the attribute's value from OpenLDAP
            DirectorySearcher nds = new DirectorySearcher(sr.GetDirectoryEntry().Parent.Parent);
            nds.SearchScope = SearchScope.Subtree;
            nds.Filter = "(&(objectClass=employee))";
            SearchResult employee = nds.FindOne();
            Crypter cryp = new Crypter(new ConfigKeySym());
            UserAD user = (UserAD)JsonConvert.DeserializeObject(cryp.Decrypt(EncrUser), typeof(UserAD));
            int boole2 = 0;
            if (user != null)
            {
                if (user.properties.Count != 0)
                {
                    DirectoryEntry newEmployee = employee.GetDirectoryEntry().Children.Add("cn=" + user.properties["username"][0], "employee");
                    foreach (string prop in user.properties.Keys)
                    {
                        List<string> temp = user.properties[prop];
                        foreach (string value in temp)
                            newEmployee.Properties[prop].Add(value);
                    }
                    boole2 = 1;
                    newEmployee.CommitChanges();
                    newEmployee.Close();
                }
            }
            return boole2;
        }

        public UserAD emailListCompany(SearchResult sr, List<string> CompanyCodes)
        {
            UserAD user = new UserAD();

            DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
            entry.AuthenticationType = AuthenticationTypes.None;
            entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
            entry.Password = "DefaultPassword";

            DirectorySearcher nds = new DirectorySearcher(entry);
            nds.SearchScope = SearchScope.Subtree;
            foreach (string CompanyCode in CompanyCodes)
            {
                nds.Filter = "(&(objectClass=Company)(companyCode=" + CompanyCode + "))";
                SearchResult result = nds.FindOne();
                string companyID = result.Properties["companyId"][0].ToString();

                nds.Filter = "(&(objectClass=employee)(companyId=" + companyID + "))";
                SearchResultCollection Employees = nds.FindAll();
                foreach (SearchResult employee in Employees)
                {
                    string email = employee.Properties["email"][0].ToString();
                    string usernameResult = employee.Properties["username"][0].ToString();
                    user.properties.Add(usernameResult, new List<string> { email });
                }
                Employees.Dispose();
            }
            nds.Dispose();
            entry.Close();
            return user;
        }

        public UserAD emailListUsername(SearchResult sr, List<string> Usernames)
        {
            UserAD user = new UserAD();

            DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
            entry.AuthenticationType = AuthenticationTypes.None;
            entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
            entry.Password = "DefaultPassword";

            DirectorySearcher nds = new DirectorySearcher(entry);
            nds.SearchScope = SearchScope.Subtree;
            foreach (string Username in Usernames)
            {
                nds.Filter = "(&(objectClass=employee)(username=" + Username + "))";
                SearchResult Employee = nds.FindOne();
                string email = Employee.Properties["email"][0].ToString();
                string usernameResult = Employee.Properties["username"][0].ToString();
                user.properties.Add(usernameResult, new List<string> { email });
            }
            nds.Dispose();
            entry.Close();
            return user;
        }

        public UserAD GetDefaultApp(SearchResult sr)
        {
            UserAD user = new UserAD();
            try
            {
                DirectoryEntry employee = sr.GetDirectoryEntry();
                string DefaultApp = employee.Properties["DefaultApp"][0].ToString();
                user.properties.Add("app", new List<string> { DefaultApp });
                employee.Close();
            }
            catch (Exception e)
            {
                user = null;
            }
            return user;
        }

        public bool SetDefaultApp(SearchResult sr, string DefaultApplication)
        {
            try
            {
                DirectoryEntry employee = sr.GetDirectoryEntry();
                employee.Properties["DefaultApp"][0] = DefaultApplication;
                employee.CommitChanges();
                employee.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        /*
         * For WorkList purposes, to return employee data based on organization level id filtering.
         */
        public List<UserAD> GetEmployeesData(string param)
        {
            DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
            entry.AuthenticationType = AuthenticationTypes.None;
            entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
            entry.Password = "DefaultPassword";

            Dictionary<String, List<String>> parameters = JSON.ToObject<Dictionary<string, List<string>>>(param);

            List<UserAD> user = new List<UserAD>();
            DirectorySearcher nds = new DirectorySearcher(entry);
            nds.SearchScope = SearchScope.Subtree;
            string parameterVals = "";

            foreach (string value in parameters.Keys)
            {
                string key = value;
                string val = parameters[key][0].ToString();
                parameterVals = parameterVals + "(" + key + "=" + val + ")";
            }

            string searchQuery = "(&(objectClass=employee)" + parameterVals + ")";

            nds.Filter = searchQuery;
            SearchResultCollection Employees = nds.FindAll();
            foreach (SearchResult employee in Employees)
            {
                //if we got the children on the right parent T.T
                if (employee.GetDirectoryEntry().Parent.Properties.Contains("ou"))
                    if (employee.GetDirectoryEntry().Parent.Properties["ou"][0].ToString() == "External" ||
                        employee.GetDirectoryEntry().Parent.Properties["ou"][0].ToString() == "Internal")
                        user.Add(readOpenLDAPAttrs(employee));
            }
            Employees.Dispose();
            nds.Dispose();
            entry.Close();
            return user;
        }

        public List<UserAD> GetOrganizationStruct(SearchResult sr, string OrgId, string UnitId)
        {
            DirectoryEntry organizationTree = sr.GetDirectoryEntry().Parent.Parent.Parent;
            DirectorySearcher nds = new DirectorySearcher(organizationTree);
            nds.SearchScope = SearchScope.Subtree;
            List<UserAD> orgList = new List<UserAD>();

            string searchQuery = "(&(objectClass=OrganizationStructure)(" + OrgId + "=" + UnitId + "))";

            nds.Filter = searchQuery;
            SearchResultCollection OrganizationStructure = nds.FindAll();
            foreach (SearchResult orgStruct in OrganizationStructure)
            {
                orgList.Add(readOpenLDAPAttrs(orgStruct));
            }
            OrganizationStructure.Dispose();
            nds.Dispose();
            organizationTree.Close();

            return orgList;
        }

        public List<UserAD> GetRoleChunk(SearchResult sr, string AuthId, string UnitId)
        {
            DirectoryEntry organizationTree = sr.GetDirectoryEntry().Parent.Parent.Parent;
            DirectorySearcher nds = new DirectorySearcher(organizationTree);
            nds.SearchScope = SearchScope.Subtree;
            List<UserAD> orgList = new List<UserAD>();

            string searchQuery = "(&(objectClass=RolePR)(" + AuthId + "=" + UnitId + "))";

            nds.Filter = searchQuery;
            SearchResultCollection OrganizationStructure = nds.FindAll();
            foreach (SearchResult orgStruct in OrganizationStructure)
            {
                orgList.Add(readOpenLDAPAttrs(orgStruct));
            }
            OrganizationStructure.Dispose();
            nds.Dispose();
            organizationTree.Close();

            return orgList;
        }

        public bool crUserLoggedInfo(SearchResult sr, string info)
        {
            //This is how to get the attribute's value from OpenLDAP
            try
            {
                DirectorySearcher nds = new DirectorySearcher(sr.GetDirectoryEntry().Parent.Parent.Parent);
                nds.SearchScope = SearchScope.Subtree;
                nds.Filter = "(&(objectClass=ObjectclassGroup)(ou=UserLoginInfo))";
                SearchResult employee = nds.FindOne();
                int ID = int.Parse(employee.Properties["CountUserLogged"][0].ToString()) + 1;
                UserAD user = JsonConvert.DeserializeObject<UserAD>(info);
                bool returns = false;
                if (user != null)
                {
                    if (user.properties.Count != 0)
                    {
                        DirectoryEntry newEmployee = employee.GetDirectoryEntry().Children.Add("ID=" +ID, "UserLoggedInfo");
                        foreach (string prop in user.properties.Keys)
                        {
                            List<string> temp = user.properties[prop];
                            foreach (string value in temp)
                                newEmployee.Properties[prop].Add(value);
                        }
                        returns = true;
                        newEmployee.CommitChanges();
                        newEmployee.Close();
                    }
                }
                return returns;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<UserAD> GetUserLoggedInfo(string param)
        {
            try
            {
                DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
                entry.AuthenticationType = AuthenticationTypes.None;
                entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
                entry.Password = "DefaultPassword";

                DirectorySearcher nds = new DirectorySearcher(entry);
                nds.SearchScope = SearchScope.Subtree;
                nds.Filter = "(&(objectClass=UserLoggedInfo)(username=" + param + "))";
                SearchResultCollection srC = nds.FindAll();
                List<UserAD> infoList = new List<UserAD>();
                foreach (SearchResult info in srC)
                {
                    infoList.Add(readOpenLDAPAttrs(info));
                }
                srC.Dispose();
                nds.Dispose();
                entry.Close();

                return infoList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool delUserLoggedInfo(SearchResult sr, string username)
        {
            // This is how to delete person of OpenLDAP could be done
            DirectorySearcher nds = new DirectorySearcher(sr.GetDirectoryEntry().Parent.Parent.Parent);
            nds.SearchScope = SearchScope.Subtree;
            nds.Filter = "(&(objectClass=UserLoggedInfo)(username=" + username + "))";
            SearchResultCollection srC = nds.FindAll();
            foreach (SearchResult result in srC)
            {
                result.GetDirectoryEntry().Parent.Children.Remove(result.GetDirectoryEntry());
                result.GetDirectoryEntry().CommitChanges();
                result.GetDirectoryEntry().Close();
            }

            return true;
        }

        public Boolean chUserLoggedInfo(SearchResult sr, string changes)
        {
            try
            {
                UserAD logInfo = JsonConvert.DeserializeObject<UserAD>(changes);

                DirectorySearcher nds = new DirectorySearcher(sr.GetDirectoryEntry().Parent.Parent.Parent);
                nds.SearchScope = SearchScope.Subtree;
                nds.Filter = "(&(objectClass=UserLoggedInfo)(username=" + logInfo.properties["username"][0] + "))";
                DirectoryEntry srC = nds.FindOne().GetDirectoryEntry();

                foreach (string key in logInfo.properties.Keys)
                {
                    for (int index = 0; index < logInfo.properties[key].Count; index++)
                        if (srC.Properties.Contains(key))
                            srC.Properties[key][index] = logInfo.properties[key][index];
                        else
                            srC.Properties[key].Add(logInfo.properties[key][index]);
                }
                srC.CommitChanges();
                srC.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /*
         * for Wall Info,we divide it by chunkies. to get the employee
         */
        public List<UserAD> GetEmployeesData(List<string> ID)
        {
            try
            {
                DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
                entry.AuthenticationType = AuthenticationTypes.None;
                entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
                entry.Password = "DefaultPassword";

                List<UserAD> user = new List<UserAD>();
                DirectorySearcher nds = new DirectorySearcher(entry);
                nds.SearchScope = SearchScope.Subtree;

                foreach (string value in ID)
                {
                    string searchQuery = "(&(objectClass=employee)(ID=" + value + "))";
                    nds.Filter = searchQuery;

                    SearchResultCollection Employees = nds.FindAll();
                    if (Employees.Count == 0)
                        user.Add(null);
                    foreach (SearchResult employee in Employees)
                    {
                        //if we got the children on the right parent T.T
                        if (employee.GetDirectoryEntry().Parent.Properties.Contains("ou"))
                            if (employee.GetDirectoryEntry().Parent.Properties["ou"][0].ToString() == "External" ||
                                employee.GetDirectoryEntry().Parent.Properties["ou"][0].ToString() == "Internal")
                                user.Add(readOpenLDAPAttrs(employee));
                    }
                }
                nds.Dispose();
                entry.Close();
                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //get the user number on User parent
        public string GetEmployeesNumber()
        {
            DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
            entry.AuthenticationType = AuthenticationTypes.None;
            entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
            entry.Password = "DefaultPassword";

            List<UserAD> user = new List<UserAD>();
            DirectorySearcher nds = new DirectorySearcher(entry);
            nds.SearchScope = SearchScope.Subtree;
            string searchQuery = "(&(objectClass=ObjectclassGroup)(ou=user))";
            nds.Filter = searchQuery;

            SearchResult Number = nds.FindOne();
            string result = Number.Properties["CountNumber"][0].ToString();

            nds.Dispose();
            entry.Close();
            return result;
        }


        /*
       * Get Role based on USername and systemName
       */
        public List<UserAD> GetUserRole(string username, string app)
        {
            try
            {
                DirectoryEntry entry = new DirectoryEntry(ConfigurationManager.AppSettings["OpenLDAP"]);
                entry.AuthenticationType = AuthenticationTypes.None;
                entry.Username = "cn=DefaultUsername,dc=maxcrc,dc=com";
                entry.Password = "DefaultPassword";

                List<UserAD> user = new List<UserAD>();
                DirectorySearcher nds = new DirectorySearcher(entry);
                nds.SearchScope = SearchScope.Subtree;

                string searchQuery = "(&(objectClass=employee)(username=" + username + "))";//(memberOf=(ou=External,ou=User,dc=maxcrc,dc=com))";

                nds.Filter = searchQuery;
                SearchResultCollection Employees = nds.FindAll();
                foreach (SearchResult employee in Employees)
                {
                    DirectoryEntry parent = employee.GetDirectoryEntry().Parent;

                    //This is for IPPCS,because of role grouping
                    if (parent.Parent.Parent.Properties.Contains("SystemName"))
                    {
                        if (parent.Parent.Parent.Properties["SystemName"][0].ToString() == app)
                            if (parent.Properties["objectclass"][0].ToString() == "role" && parent.Properties.Contains("authorizationdetails"))
                                user.Add(readOpenLDAPAttrs(parent));
                    }

                    //This is for HRIS, coz of there isn't role grouping
                    if (parent.Parent.Properties.Contains("SystemName"))
                    {
                        if (parent.Parent.Properties["SystemName"][0].ToString() == app)
                            if (parent.Properties["objectclass"][0].ToString() == "role" && parent.Properties.Contains("authorizationdetails"))
                                user.Add(readOpenLDAPAttrs(parent));
                    }
                }
                Employees.Dispose();
                nds.Dispose();
                entry.Close();
                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}