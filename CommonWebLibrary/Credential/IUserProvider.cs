﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;

namespace Toyota.Common.Web.Credential
{
    public interface IUserProvider<T, F>
    {
        UserAD GetUser(string Username, string Password, string app); 
        bool ChgEmployeeAttr(string Username, string Password, string Branch, string attr, string OldVal, string NewVal);
        int CrtEmployeeAttr(string Username, string Password, string Branch, string user);
        bool DelEmployee(string Username, string Password, string Branch, string user);
        bool IsAuthenticate(string username, string password);
        List<string> AppList(string Username, string Password);
        UserAD GetEmailList(string Username, string Password, List<string> usernames);
        List<T> GetUsers();
        bool RegisterCreate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone);
        bool RegisterUpdate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone);
        bool RegisterDelete(string UserName, string Password, string UserEmail);
        bool RegisterIsExist(string UserName, string Password, string UserEmail);
        UserAD GenerateNewUsername(string UserName, string Password, string Email);
        UserAD GenerateNewPassword(string UserName, string Password, string Email);
        bool IsDefaultApplicationExist(string UserName, string Password, string UserEmail);
        bool UpdateDefaultApplication(string UserName, string Password,string DefaulatApplication);
        UserAD GetDefaultApp(string UserName, string Password);
        List<UserAD> GetEmployeesData(string values);
        List<UserAD> GetOrganizationStruct(string UserName, string Password, string OrgId, string UnitId);
        List<UserAD> GetUserLoggedInfo(string username);
        bool crUserLoggedInfo(string UserName, string Password, string info);
        bool chUserLoggedInfo(string UserName, string Password, string changes);
        bool delUserLoggedInfo(string UserName, string Password, string username);
        List<UserAD> GetRoleChunk(string UserName, string Password, string AuthId, string UnitId);
        List<UserAD> GetEmployeesData(List<string> values);
        string getEmployeesNumber();
        List<UserAD> GetUserRole(string username, string app);
        bool IsAuthenticAD(string username, string password);
    }
}
