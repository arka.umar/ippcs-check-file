﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    public class UserProvider
    {
        readonly static IUserProvider<User, UserAD> instance = new DefaultUserProvider();

        private UserProvider() { }

        public static IUserProvider<User, UserAD> GetInstance()
        {
            return instance;
        }
    }
}
