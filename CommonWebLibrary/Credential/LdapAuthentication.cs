﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.DirectoryServices;

namespace Toyota.Common.Web.Credential
{
    public class LdapAuthentication: IUserAuthenticator
    {
        private String _path;
        private String _filterAttribute;

        public LdapAuthentication()
        {
            _path = ConfigurationManager.AppSettings["LdapPath"];
        }

        public bool IsAuthentic(string Username, string Password)
        {
            String domain = ConfigurationManager.AppSettings["LdapDomain"];
            String domainAndUsername = domain + @"\" + Username;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, Password);

            try
            {	//Bind to the native AdsObject to force authentication.			
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                
                search.Filter = "(SAMAccountName=" + Username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();
                
                if (null == result)
                {
                    return false;
                }

                //Update the new path to the user in the directory.
                //_path = result.Path;
                //_filterAttribute = (String)result.Properties["cn"][0];
                search.Dispose();
            }
            catch (Exception ex)
            {
                return false;
            }
            entry.Close();
            entry.Dispose();
            return true;
        }
    }
}
