﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Batch
{
    public class BatchProcessor : SelectableDataModel
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public byte PriorityLevel { set; get; }
        public string PriorityName { set; get; }
        public string Command { set; get; }

        public override string SelectedKey
        {
            get
            {
                return "Id";
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Id;
            }
        }
    }
}
