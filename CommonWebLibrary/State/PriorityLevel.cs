﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.State
{
    public class PriorityLevel
    {
        public byte Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }

        public static PriorityLevel Highest()
        {
            return new PriorityLevel() { 
                Id = 1,
                Name = "Highest",
                Description = "Highest priority"
            };
        }
        public static PriorityLevel High()
        {
            return new PriorityLevel()
            {
                Id = 2,
                Name = "High",
                Description = "High priority"
            };
        }
        public static PriorityLevel Medium()
        {
            return new PriorityLevel()
            {
                Id = 3,
                Name = "Medium",
                Description = "Medium priority"
            };
        }
        public static PriorityLevel Low()
        {
            return new PriorityLevel()
            {
                Id = 4,
                Name = "Low",
                Description = "Low priority"
            };
        }
        public static PriorityLevel Lowest()
        {
            return new PriorityLevel()
            {
                Id = 5,
                Name = "Lowest",
                Description = "Lowest priority"
            };
        }
    }
}
