﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Ioc
{
    public interface IBootstrap
    {        
        void RegisterProvider<T>(Type implementation, bool singleton);
    }
}
