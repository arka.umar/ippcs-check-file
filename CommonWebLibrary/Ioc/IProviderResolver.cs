﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Util;

namespace Toyota.Common.Web.Ioc
{
    public interface IProviderResolver
    {
        T Get<T>();
    }
}
