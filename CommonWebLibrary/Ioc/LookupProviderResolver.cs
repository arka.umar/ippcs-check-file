﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Lookup;
using Toyota.Common.Web.Util;

namespace Toyota.Common.Web.Ioc
{
    [Serializable]
    class LookupProviderResolver: IProviderResolver
    {
        private ILookup lookup;
        private IDictionary<Type, Type> implementationMap;

        private LookupProviderResolver()
        {
            lookup = new SimpleLookup();
            implementationMap = new Dictionary<Type, Type>();
        }

        public void Register<T>(Type implementationType, bool singleton)
        {
            if (implementationType == null)
            {
                return;
            }

            if (singleton)
            {
                IList<T> result = lookup.GetAll<T>();
                if (result != null)
                {
                    foreach (T obj in result)
                    {
                        lookup.Remove(obj);
                    }
                }
                T objInstance = ReflectionUtil.Instantiate<T>(implementationType.AssemblyQualifiedName);
                if (objInstance != null)
                {
                    lookup.Add(objInstance);
                }
            }
            else
            {
                Type type = typeof(T);
                if (implementationMap.ContainsKey(type))
                {
                    implementationMap[type] = implementationType;
                }
                else
                {
                    implementationMap.Add(type, implementationType);
                }
            }
        }

        public T Get<T>()
        {
            T obj = lookup.Get<T>();
            if (obj == null)
            {
                Type type = typeof(T);
                if(implementationMap.ContainsKey(type)) {
                    obj = ReflectionUtil.Instantiate<T>(implementationMap[type].AssemblyQualifiedName);
                }
            }

            return obj;
        }

        public static LookupProviderResolver GetInstance()
        {
            return InstanceHolder.instance;
        }
        
        private static class InstanceHolder
        {
            public static readonly LookupProviderResolver instance = new LookupProviderResolver();
        }        
    }
}
