﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Ioc
{
    public class ProviderResolver
    {
        private readonly static IProviderResolver instance = LookupProviderResolver.GetInstance();
        
        private ProviderResolver() { }

        [System.Diagnostics.DebuggerStepThrough]
        public static IProviderResolver GetInstance()
        {
            return instance;
        }
    }
}
