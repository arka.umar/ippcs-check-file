﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Toyota.Common.Web.Util;
using System.IO;
using System.Xml;

namespace Toyota.Common.Web.Ioc
{
    public class IocServiceFactory
    {
        private static IocService service = null;
        
        public static IocService GetService()
        {
            if (service == null)
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                String URI = Namespace.InternalConfigurationResourceNamespace + ".GlobalConfiguration.xml";
                Stream stream = assembly.GetManifestResourceStream(URI);
                XmlTextReader reader = new XmlTextReader(stream);
            
                if (reader.ReadToFollowing("Ioc-Container"))
                {
                    string typeName = reader.ReadElementContentAsString();
                    service = ReflectionUtil.Instantiate<IocService>(typeName);
                }

                reader.Close();
            }            

            return service;
        }
    }
}
