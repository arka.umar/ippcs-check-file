﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Util;

namespace Toyota.Common.Web.Ioc
{
    public interface IocService
    {
        void Bind(Type iface, Type impl);
        void Bind(Type iface, Type impl, params MappedValue<string, object>[] arguments);
        T Get<T>();
    }
}
