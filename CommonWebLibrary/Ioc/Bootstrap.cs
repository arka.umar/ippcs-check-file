﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Ioc
{
    public class Bootstrap
    {
        private static IBootstrap instance = new LookupBootstrap();

        public static IBootstrap GetInstance()
        {
            return instance;
        }
    }
}
