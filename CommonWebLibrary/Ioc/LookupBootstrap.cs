﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Ioc
{
    class LookupBootstrap: IBootstrap
    {
        private LookupProviderResolver lookupProvider;

        public LookupBootstrap()
        {
            lookupProvider = LookupProviderResolver.GetInstance();
        }

        public void RegisterProvider<T>(Type implementation, bool singleton)
        {
            lookupProvider.Register<T>(implementation, singleton);
        }
    }
}
