﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Toyota.Common.Web.Util;

namespace Toyota.Common.Web.Ioc.Ninject
{
    public class NinjectIocService: IocService
    {
        private IKernel kernel;

        public NinjectIocService()
        {
            kernel = new StandardKernel();
        }

        public void Bind(Type iface, Type impl)
        {
            kernel.Bind(iface).To(impl);
        }

        public T Get<T>()
        {
            return kernel.TryGet<T>();
        }


        public void Bind(Type iface, Type impl, params Util.MappedValue<string, object>[] arguments)
        {
            if ((arguments != null) && (arguments.Length > 0))
            {
                MappedValue<string, object> arg = arguments[0];
                var theKernel = kernel.Bind(iface).To(impl).WithConstructorArgument(arg.Key, arg.Value);
                if (arguments.Length > 1)
                {
                    for (int i = 1; i < arguments.Length; i++)
                    {
                        arg = arguments[i];
                        theKernel.WithConstructorArgument(arg.Key, arg.Value);
                    }
                }
            }            
        }
    }
}
