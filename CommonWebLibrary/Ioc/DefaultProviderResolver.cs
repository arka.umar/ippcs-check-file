﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using System.Reflection;
using Toyota.Common.Web.Util;
using System.IO;
using System.Xml;
using System.Configuration;

namespace Toyota.Common.Web.Ioc
{
    class DefaultProviderResolver: IProviderResolver
    {
        private IocService iocService;
        private string configUri;
                
        public DefaultProviderResolver()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            configUri = Namespace.InternalConfigurationResourceNamespace + ".Providers.xml";
            iocService = IocServiceFactory.GetService();
        }
                
        public T Get<T>()
        {
            object objProvider = iocService.Get<T>();
            if (objProvider == null)
            {
                Bind(typeof(T), null);
                objProvider = iocService.Get<T>();
            }

            return (T) objProvider;
        }
                
        public T Get<T>(params MappedValue<string, object>[] arguments)
        {
            object objProvider = iocService.Get<T>();
            if (objProvider == null)
            {
                Bind(typeof(T), arguments);
                objProvider = iocService.Get<T>();
            }

            return (T)objProvider;
        }
                
        private void Bind(Type type, params MappedValue<string, object>[] arguments)
        {
            bool hasArgument = (arguments != null) && (arguments.Length > 0);
            string typeName = type.FullName;            

            string ifaceName = null;
            string implName = null;
            Type ifaceType;
            Type implType;
            string strTemp;
            bool allowOverriding = false;

            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(configUri);
            XmlTextReader reader = new XmlTextReader(stream);
            while (reader.ReadToFollowing("Provider"))
            {
                reader.MoveToAttribute("Interface");
                ifaceName = reader.ReadContentAsString();
                if (ifaceName.Equals(typeName))
                {
                    if(reader.MoveToAttribute("Implementation")) {
                        implName = reader.ReadContentAsString();
                    }                    
                    if(reader.MoveToAttribute("AllowOverriding")) {
                        strTemp = reader.ReadContentAsString();
                        allowOverriding = strTemp.Trim().ToLower().Equals("true");
                    }
                    
                    break;
                }
            }
            reader.Close();
            stream.Close();

            //if ((string.IsNullOrEmpty(ifaceName) && string.IsNullOrEmpty(implName)) || allowOverriding)
            //{
            //    stream = new FileStream(applicationConfigUri, FileMode.Open);
            //    reader = new XmlTextReader(stream);
            //    while (reader.ReadToFollowing("Provider"))
            //    {
            //        reader.MoveToAttribute("Interface");
            //        ifaceName = reader.ReadContentAsString();
            //        if (ifaceName.Equals(typeName))
            //        {
            //            if (reader.MoveToAttribute("Implementation"))
            //            {
            //                implName = reader.ReadContentAsString();
            //            }

            //            break;
            //        }
            //    }
            //    reader.Close();
            //    stream.Close();
            //}

            if (!string.IsNullOrEmpty(ifaceName) && !string.IsNullOrEmpty(implName))
            {
                ifaceType = Type.GetType(ifaceName);
                implType = Type.GetType(implName);
                if (!hasArgument)
                {
                    iocService.Bind(ifaceType, implType);
                }
                else
                {
                    iocService.Bind(ifaceType, implType, arguments);
                }              
            }            
        }
    }
}
