﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Toyota.Common.Web.K2Connector
{
    public class K2Setting
    {
        private static string _K2Server = Convert.ToString(ConfigurationManager.AppSettings["K2_SERVER"]);
        public static string K2Server
        { 
            get { return _K2Server; }
        }

        private static uint _K2SmoPort = Convert.ToUInt16(ConfigurationManager.AppSettings["K2_SMO_PORT"]);
        public static uint K2SmoPort
        {
            get { return _K2SmoPort; }
        }

        private static uint _K2WorkflowPort = Convert.ToUInt16(ConfigurationManager.AppSettings["K2_WORKFLOW_PORT"]);
        public static uint K2WorkflowPort
        {
            get { return _K2WorkflowPort; }
        }

        private static string _K2User = Convert.ToString(ConfigurationManager.AppSettings["K2_USER"]);
        public static string K2User
        {
            get { return _K2User; }
        }

        private static string _K2Password = Convert.ToString(ConfigurationManager.AppSettings["K2_PASSWORD"]);
        public static string K2Password
        {
            get { return _K2Password; }
        }

        private static string _K2Domain = Convert.ToString(ConfigurationManager.AppSettings["K2_DOMAIN"]);
        public static string K2Domain
        {
            get { return _K2Domain; }
        }

        private static string _K2Label = Convert.ToString(ConfigurationManager.AppSettings["K2_LABEL"]);
        public static string K2Label
        {
            get { return _K2Label; }
        }

    }
}
