﻿/**
 * Created by : Argyaputri
 * K2 helper for connect to K2
 * The method without open and close connection, need call Open() and Close() method
 * 
 * Process Full Name => (ProcessName\\WorkflowName) "workflow_test\\ProcessTest"
 * Username => (K2:My Domain\UserID) "K2:TOYOTA\mangestya.argyaputri"
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SourceCode.Workflow.Client;
using SourceCode.Hosting.Client.BaseAPI;

namespace Toyota.Common.Web.K2Connector
{
    public class K2Manager
    {
        private string server;
        private string userK2;
        private string password;
        private uint smoPort;
        private uint workflowPort;
        private string domain;
        private string label;
        private Connection Conn;

        public K2Manager()
        {
            this.server = K2Setting.K2Server;
            this.userK2 = K2Setting.K2User;
            this.password = K2Setting.K2Password;
            this.smoPort = K2Setting.K2SmoPort;
            this.workflowPort = K2Setting.K2WorkflowPort;
            this.domain = K2Setting.K2Domain;
            this.label = K2Setting.K2Label;
        }


        /** Method  : ConnectionSetup
         *  Purpose : Build the connection string
         **/
        public string ConnectionSetup()
        {
            SCConnectionStringBuilder strConn = new SCConnectionStringBuilder();
            strConn.Authenticate = true;
            strConn.Host = this.server;
            strConn.IsPrimaryLogin = true;
            strConn.SecurityLabelName = this.label;
            strConn.Port = this.workflowPort;
            strConn.Integrated = true;
            strConn.UserID = this.userK2;
            strConn.Password = this.password;
            strConn.WindowsDomain = this.domain;

            return strConn.ConnectionString;
        }


        /** Method  : Open
         *  Purpose : Open connection to K2
         **/
        public void Open()
        {
            Conn = new Connection();
            Conn.Open(this.server, ConnectionSetup());            
        }


        /** Method  : Close
         *  Purpose : Close connection to K2
         **/
        public void Close()
        {
            if (Conn != null)
            {
                Conn.Close();
            }
        }


        /** Method  : Impersonate
         *  Purpose : Impersonate a different user, the user associated with the connection
         *            Must have Impersonate rights on the workflow server
         *  Param   :
         *     @user => user name
        **/
        public void Impersonate(String User)
        {
            Conn.ImpersonateUser(User);
        }


        /** Method  : Revert
         *  Purpose : Revet connection back to original user
        **/
        public void Revert()
        {
            Conn.RevertUser();
        }


        /** Method  : StartProcessInstance
         *  Purpose : Start process instance, ex. for submit/register application
         *  Param   :
         *     @processName => Process name on K2 workflow, ex: "K2_CARS\CarsWorkflow"
         *     @folio => string folio, is unique for every application per submit/register
         *     @user => user name
         *     @datafield => data parameter to be initiated to workflow data field
         **/
        public bool StartProcessInstance(string processName, string folio, string user, Dictionary<String, String> datafield)
        {
            bool stat = false;

            try
            {
                Open();
                Impersonate(user); //impersonate user so originator is user

                // create process instance
                ProcessInstance procInstance = Conn.CreateProcessInstance(processName);

                // populate data fields
                foreach (String Keys in datafield.Keys)
                {
                    procInstance.DataFields[Keys].Value = datafield[Keys];
                }

                // set process folio
                procInstance.Folio = folio;

                // start the process
                Conn.StartProcessInstance(procInstance, false);
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
            finally
            {
                Close();
            }
            

            return stat;
        }


        /** Method  : Action
         *  Purpose : Execute client event action, ex. approve / reject (need open and close connection)
         *  Param   :
         *     @action => action name, ex. approve / reject
         *     @sn => serial number, based on k2 workflow server
         **/
        public void Action(string action, string sn)
        {
            try
            {
                WorklistItem wfItem = Conn.OpenWorklistItem(sn);
                if (wfItem != null)
                {
                    wfItem.Actions[action].Execute();
                }
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
        }


        /** Method  : Do Action
         *  Purpose : Example method to call method Action()
         *  Param   :
         *     @action => action name, ex. approve / reject
         *     @sn => serial number, based on k2 workflow server
         **/
        public void DoAction(string action, string sn, string user)
        {
            try
            {
                // open connection to K2
                Open();
                Impersonate(user);

                // do action
                Action(action, sn);
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
            finally
            {
                // close connection to K2
                Close();
            }
        }


        /** Method  : GetAction
         *  Purpose : Get action name that exist on workflow per serial number (need open and close connection)
         *  Param   :
         *     @sn => serial number, based on k2 workflow server
         **/
        public List<String> GetAction(string sn)
        {
            try
            {
                List<String> actions = new List<string>();
                WorklistItem worklistItem = Conn.OpenWorklistItem(sn);
                
                if (worklistItem != null)
                {
                    // write list action
                    foreach (SourceCode.Workflow.Client.Action action in worklistItem.Actions)
                    {
                        actions.Add(action.Name);
                    }
                }

                return actions;
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
        }


        /** Method  : GetAction
         *  Purpose : Get action name that exist on workflow per serial number and user (need open and close connection)
         *  Param   :
         *     @sn => serial number, based on k2 workflow server
         *     @user => user name
         **/
        public List<String> GetAction(string sn, string user)
        {
            List<String> actions = new List<string>();

            try
            {
                Impersonate(user);
                WorklistItem worklistItem = Conn.OpenWorklistItem(sn);

                if (worklistItem != null)
                {
                    // write list action
                    foreach (SourceCode.Workflow.Client.Action action in worklistItem.Actions)
                    {
                        actions.Add(action.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
            finally
            {
                Revert(); //revert user
            }

            return actions;
        }


        /** Method  : GetAllWorklist
         *  Purpose : Get all worklists per user
         *  Param   :
         *     @user => user name
         *     @processFullName => process full name, ex: "workflow_test\\ProcessTest"
         **/
        public List<WorkflowModel> GetAllWorklist(string user, string processFullName)
        {
            List<WorkflowModel> _worklists = new List<WorkflowModel>();
            try
            {
                // open connection to K2
                Open();

                Impersonate(user);

                // add criteria or filter worklist depend on process full name
                WorklistCriteria k2criteria = new WorklistCriteria();
                k2criteria.AddFilterField(WCField.ProcessFullName, WCCompare.Equal, processFullName);
                // open K2 worklist based on criteria
                Worklist k2Worklist = Conn.OpenWorklist(k2criteria);

                foreach (WorklistItem item in k2Worklist)
                {
                    WorkflowModel _Data = new WorkflowModel();
                    if (item.AllocatedUser.ToUpper() == label.ToUpper() + ":" + domain.ToUpper() + "\\" + user.ToUpper())
                    {
                        _Data.Folio = item.ProcessInstance.Folio;
                        _Data.SN = item.SerialNumber;
                        _Data.ProcessName = item.ProcessInstance.FullName;
                        _Data.StartDate = item.EventInstance.StartDate;
                        _Data.StatusInstance = item.ProcessInstance.Status1.ToString();
                        _Data.StatusWorklist = item.Status.ToString();
                        _Data.AllocatedUser = item.AllocatedUser;
                        _worklists.Add(_Data);
                    }
                }
                
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
            finally
            {
                Revert();
                Close();
            }

            return _worklists;
        }


        /** Method  : GetWorklist
         *  Purpose : Get worklist per user as array, folio as index and sn as value (need open and close connection)
         *  Param   :
         *     @user => user name
         **/
        public Dictionary<string, string> GetWorklist(string user)
        {
            Dictionary<string, string> worklist = new Dictionary<string,string>();

            try
            {
                Impersonate(user);
                Worklist k2Worklist = Conn.OpenWorklist(); //open K2 worklist

                foreach (WorklistItem item in k2Worklist)
                {
                    if (item.AllocatedUser.ToUpper() == label.ToUpper() + ":" + domain.ToUpper() + "\\" + user.ToUpper())
                    {
                        worklist.Add(item.ProcessInstance.Folio, item.SerialNumber);
                    }
                }                
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
            finally
            {
                Revert();
            }

            return worklist;
        }


        /** Method  : GetProcessID
         *  Purpose : Get process ID from serial number
         *  Param   :
         *     @SN => serial number, based on k2 workflow server
         **/
        public int GetProcessID(String SN)
        {
            return Convert.ToInt32(SN.Split('_')[0]);
        }


        /** Method  : UpdateDatafield
         *  Purpose : Update datafield on K2
         *  Param   :
         *     @datafield => datafield to be update
         *     @processID => K2 process ID
         **/
        public void UpdateDatafield(Dictionary<String, String> datafield, int processID)
        {
            ProcessInstance pi = Conn.OpenProcessInstance(processID);
            foreach (String Keys in datafield.Keys)
            {
                pi.DataFields[Keys].Value = datafield[Keys];
            }
            pi.Update();
        }


        /** Method  : DeleteProcess
         *  Purpose : Delete process on K2
         *  Param   :
         *     @processID => K2 process ID
         **/
        public void DeleteProcess(int processID)
        {
            SourceCode.Workflow.Management.WorkflowManagementServer wms = new SourceCode.Workflow.Management.WorkflowManagementServer();
            wms.Open(ConnectionSetup());
            
            SourceCode.Workflow.Management.ProcessInstances pi = wms.GetProcessInstances(processID);

            foreach (SourceCode.Workflow.Management.ProcessInstance var in pi)
            {
                wms.DeleteProcessInstances(var.ID, false);
            }

            wms.Connection.Close();            
        }


        /** Method  : SetUserStatus
         *  Purpose : Set user's status, not used / available / out of office (need open and close method)
         *  Param   :
         *     @user => user name
         *     @status => 1:  used, 2: in the office, 3: out of the office
         **/
        public void SetUserStatus(string user, int status)
        {
            try
            {
                switch (status)
                {
                    case 1:
                        Conn.SetUserStatus(user, UserStatuses.None);
                        break;
                    case 2:
                        Conn.SetUserStatus(user, UserStatuses.Available);
                        break;
                    case 3:
                        Conn.SetUserStatus(user, UserStatuses.OOF);
                        break;
                }
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
        }


        /** Method  : GetUserStatus
         *  Purpose : Get user's status = none / available / oof (need open and close method)
         *  Param   :
         *     @user => user name
         **/
        public string GetUserStatus(string user)
        {
            Impersonate(user);
            UserStatuses uStatus = Conn.GetUserStatus();
            Revert();

            return uStatus.ToString();
        }


        /** Method  : DelegateWorklistItem
         *  Purpose : Use this method to create a copy of the task for another user, the original destination still can see and do action.
         *  Param   :
         *     @sn => serial number
         *     @userDest => user name destination / attorney
         **/
        public void DelegateWorklistItem(string sn, string userDest)
        {
            WorklistItem worklistItem = Conn.OpenWorklistItem(sn, "asp", false);    // SourceCode.Workflow.Client
            if (worklistItem != null)
            {
                Destination dest = new Destination();   // SourceCode.Workflow.Client
                dest.DestinationType = DestinationType.User;
                dest.Name = userDest;
                worklistItem.Delegate(dest);
            }
        }


        /** Method  : RedirectWorklistItem
         *  Purpose : Use this method to move the current task to another user per serial number.
         *  Param   :
         *     @sn => serial number
         *     @userDest => user name grantor
         *     @userDest => user name destination / attorney
         **/
        public void RedirectWorklistItem(string sn, string userGrantor, string userDest)
        {
            try
            {
                Impersonate(userGrantor);
                WorklistItem worklistItem = Conn.OpenWorklistItem(sn);   // SourceCode.Workflow.Client

                if (worklistItem != null)
                {
                    worklistItem.Release();
                    worklistItem.Redirect(userDest);
                }
            }
            catch (Exception ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(ex.InnerException)) ? Convert.ToString(ex.InnerException) : ex.Message;
                throw new Exception(_ErrMsg);
            }
            finally
            {
                Revert();
            }
        }


        /** Method  : Redirect
         *  Purpose : Use this method to move the current task to another user per full process name.
         *  Param   :
         *     @fromUser => user name grantor
         *     @toUser => user name destination / attorney
         *     @processFullName => process full name, ex: "workflow_test\\ProcessTest"
         **/
        public List<string> Redirect(string fromUser, string toUser, string processFullName)
        {
            List<string> list = new List<string>();

            try
            {
                Open();
                Impersonate(fromUser);
                
                // add criteria or filter worklist depend on process full name
                WorklistCriteria k2criteria = new WorklistCriteria();
                k2criteria.AddFilterField(WCField.ProcessFullName, WCCompare.Equal, processFullName);
                // open K2 worklist based on criteria
                Worklist k2Worklist = Conn.OpenWorklist(k2criteria);

                foreach (WorklistItem i in k2Worklist)
                {
                    i.Redirect(toUser);
                    list.Add(i.ProcessInstance.Folio);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Close();
            }

            return list;
        }


        /** Method  : Redirect
         *  Purpose : Use this method to move the current task to another user per folio / refference number.
         *  Param   :
         *     @fromUser => user name grantor
         *     @toUser => user name destination / attorney
         *     @processFullName => process full name, ex: "workflow_test\\ProcessTest"
         *     @folio => list all folio to be redirect
         **/
        public List<string> Redirect(string fromUser, string toUser, string processFullName, List<string> folio)
        {
            List<string> list = new List<string>();

            try
            {
                Open();
                Impersonate(fromUser);

                // add criteria or filter worklist depend on process full name
                WorklistCriteria k2criteria = new WorklistCriteria();
                k2criteria.AddFilterField(WCField.ProcessFullName, WCCompare.Equal, processFullName);
                // open K2 worklist based on criteria
                Worklist k2Worklist = Conn.OpenWorklist(k2criteria);

                foreach (WorklistItem i in k2Worklist)
                {
                    if (folio.Contains(i.ProcessInstance.Folio))
                    {
                        i.Redirect(toUser);
                        list.Add(i.ProcessInstance.Folio);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Close();
            }

            return list;
        }

        public void ShareWorklist(DateTime start, String fromUser, String toUser, int userStatus)
        {
            try
            {
                Open();
                Impersonate(fromUser);

                UserStatuses uStatus = UserStatuses.Available;
                switch (userStatus)
                {
                    case 0:
                        uStatus = UserStatuses.OOF;
                        break;
                    case 1:
                        uStatus = UserStatuses.Available;
                        break;
                    default:
                        uStatus = UserStatuses.Available;
                        break;
                }


                SourceCode.Workflow.Client.Destination dest = new SourceCode.Workflow.Client.Destination();
                SourceCode.Workflow.Client.WorklistShare wlShare = new SourceCode.Workflow.Client.WorklistShare();
                SourceCode.Workflow.Client.WorkType wType = new SourceCode.Workflow.Client.WorkType("PCSWorkType");

                dest.Name = toUser;
                dest.DestinationType = SourceCode.Workflow.Client.DestinationType.User;

                wType.WorklistCriteria.Platform = "ASP";
                wType.Destinations.Add(dest);

                wlShare.ShareType = SourceCode.Workflow.Client.ShareType.OOF;
                wlShare.StartDate = start;
                wlShare.WorkTypes.Add(wType);

                Conn.ShareWorkList(wlShare);
                Conn.SetUserStatus(uStatus);

                Revert();                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Close();
            }
        }
        
    }
}
