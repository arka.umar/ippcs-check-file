﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.K2Connector
{
    [Serializable]
    public class WorkflowModel
    {
        public string Folio { get; set; }
        public string SN { get; set; }
        public string ProcessName { get; set; }
        public DateTime StartDate { get; set; }
        public string UserID { get; set; }
        public string StatusInstance { get; set; }
        public string StatusWorklist { get; set; }
        public string Action { get; set; }
        public string AllocatedUser { get; set; }
    }
}
