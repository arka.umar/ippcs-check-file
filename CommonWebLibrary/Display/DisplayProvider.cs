﻿/*
 
    DIKO
 
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Web.Display;

namespace Toyota.Common.Web.Display
{
    public class DisplayProvider
    {
        readonly IDBContextManager dbManager = new PetaPocoContextManager();
        public DisplayBehaviour GetDisplayMethod(string screenID)
        {
            if(!String.IsNullOrEmpty(screenID))
            {
                IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_COMMON_MASTER);
                DisplayBehaviour behaviour = dbContext.SingleOrDefault<DisplayBehaviour>("GetDisplayBehaviour", new object[] { screenID });
                return behaviour;
            }
            return null;
        }
    }
}
