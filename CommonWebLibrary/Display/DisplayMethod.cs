﻿/*
 
    DIKO
 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Display
{
    public class DisplayMethod
    {
        public String MethodID { set; get; }
        public String Name { set; get; }
    }
}
