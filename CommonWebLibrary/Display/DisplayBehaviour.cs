﻿/*
 
    DIKO
 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Display
{
    public class DisplayBehaviour
    {
        public String Screen_ID { set; get; }
        public String DisplayMethod { set; get; }
    }
}
