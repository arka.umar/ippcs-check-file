﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.SAPConnector
{
    [Serializable]
    public class PaymentSAPResult
    {

        public string DOC_NO { get; set; }
        public string DOC_YEAR { get; set; }
        public string CLEARING_NO { get; set; }
        public string CLEARING_DT { get; set; }
    }
}
