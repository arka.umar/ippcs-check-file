﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAP.Middleware.Connector;
using System.Diagnostics;


/*created by : niit. hendra wahyudi 
 date : 03-12-2012
 */

namespace Toyota.Common.Web.SAPConnector
{
    public class SAPConfigChange : IDestinationConfiguration
    {
        private string SAPUser;
        private string SAPPassword;
        

        public SAPConfigChange(string _SAPUser, string _SAPPassword)
        {
            this.SAPUser = _SAPUser;
            this.SAPPassword = _SAPPassword;
        }

        #region IDestinationConfiguration Members

        public bool ChangeEventsSupported()
        {
            return false;
        }

        //public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public RfcConfigParameters GetParameters(string destinationName)
        {
            if (destinationName == InitialSetting.SAPName)
            {
                RfcConfigParameters _RfcConfigParameters = new RfcConfigParameters();
                _RfcConfigParameters.Add(RfcConfigParameters.User, this.SAPUser);
                _RfcConfigParameters.Add(RfcConfigParameters.Password, this.SAPPassword);
                _RfcConfigParameters.Add(RfcConfigParameters.Client, InitialSetting.SAPClient);
                _RfcConfigParameters.Add(RfcConfigParameters.Language, InitialSetting.SAPLang);
                _RfcConfigParameters.Add(RfcConfigParameters.AppServerHost, InitialSetting.SAPAsHost);
                _RfcConfigParameters.Add(RfcConfigParameters.SystemNumber, InitialSetting.SAPSysn);
                _RfcConfigParameters.Add(RfcConfigParameters.PeakConnectionsLimit, InitialSetting.SAPMaxPoolSize);
                _RfcConfigParameters.Add(RfcConfigParameters.IdleTimeout, InitialSetting.SAPIdleTimeout);
                return _RfcConfigParameters;
            }
            else
                return null;
        }

        RfcDestinationManager.ConfigurationChangeHandler changeHandler;
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged
        {
            add
            {
                changeHandler = value;
            }
            remove
            {
                //do nothing
            }
        }
        public void removeDestination()
        {
            changeHandler("SAPNCO", new RfcConfigurationEventArgs(RfcConfigParameters.EventType.DELETED));
        }
        #endregion
    }



    public class SAPcommunication
    {
        RfcDestinationManager.ConfigurationChangeHandler changeHandler;
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged
        {
            add
            {
                changeHandler = value;
            }
            remove
            {
                //do nothing
            }
        }
        public void removeDestination()
        {
            changeHandler("SAPNCO", new RfcConfigurationEventArgs(RfcConfigParameters.EventType.DELETED));
        }
        private RfcDestination SapRfcDestination;
        private RfcRepository SapRfcRepository;

        private IDestinationConfiguration _config;

        private List<string> errs;

        public SAPcommunication()
        {
            SapRfcDestination = RfcDestinationManager.GetDestination("SAPNCO");
            SapRfcRepository = SapRfcDestination.Repository;
            errs = new List<string>();
        }
        public SAPcommunication(string _SAPUser, string _SAPPassword)
        {
            //<add NAME="SAPNCO" USER="sapdirect" PASSWD="toyota" CLIENT="300" LANG="EN" ASHOST="10.16.19.30" SYSNR="30" MAX_POOL_SIZE="10" IDLE_TIMEOUT="10" />
            errs = new List<string>();
            _config = new SAPConfigChange(_SAPUser, _SAPPassword);
            try
            {

                RfcDestinationManager.RegisterDestinationConfiguration(_config);
                SapRfcDestination = RfcDestinationManager.GetDestination("SAPNCO");
                SapRfcRepository = SapRfcDestination.Repository;
            }
            catch (RfcInvalidStateException e)
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("Invalid State " + e.Message);
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("Network Problem " + e.Message);
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("Bad Username or password " + e.Message);
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message// or an ABAP class-based exception..
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("The function module returned an ABAP exception " + e.Message);
            }
        }

        
        public string LastMessageText
        {
            get
            {
                if (errs == null || errs.Count  < 1) 
                    return null;
                
                return (errs[errs.Count - 1]);
            }
        }

        
        //get clearing Number

        public String GetClearingNo(String DocNumber, String DocYear)
        {

        
            String result = "";
            String fn = "ZBAPI_PAYMENT_STATUS"; //module name
            try
            {
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("INPUT"); //parameter name
                IRfcStructure _inputStructure;
                _inputStructure = SapRfcRepository.GetStructureMetadata("ZBAPI_PAYMENT_STATUS_INPUT").CreateStructure();// refference type
                _inputStructure.SetValue("DOC_NUMBER", DocNumber);
                _inputStructure.SetValue("DOC_YEAR", DocYear);
                _inputHeader.Append(_inputStructure);

                f.SetValue("INPUT", _inputHeader);           
                f.Invoke(SapRfcDestination);

                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("OUTPUT");
                if (BAPIResult.Count > 0)
                {
                    String sClearing_No = BAPIResult.GetString("CLEARING_NO");
               
                    return sClearing_No;
                }

            }
            catch (Exception ex)
            {
                string _x = ExToStr(ex);
            }
            finally
            {
                if (_config != null)
                {
                    RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                }
            }
            return result;
        }

        // get clearing Dt

        public String GetClearingDt(String DocNumber, String DocYear)
        {


            String result = "";
            String fn = "ZBAPI_PAYMENT_STATUS"; //module name
            try
            {
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("INPUT"); //parameter name
                IRfcStructure _inputStructure;
                _inputStructure = SapRfcRepository.GetStructureMetadata("ZBAPI_PAYMENT_STATUS_INPUT").CreateStructure();// refference type
                _inputStructure.SetValue("DOC_NUMBER", DocNumber);
                _inputStructure.SetValue("DOC_YEAR", DocYear);
                _inputHeader.Append(_inputStructure);

                f.SetValue("INPUT", _inputHeader);
                f.Invoke(SapRfcDestination);

                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("OUTPUT");
                if (BAPIResult.Count > 0)
                {
                    String sClearing_No = BAPIResult.GetString("CLEARING_DT");

                    return sClearing_No;
                }

            }
            catch (Exception ex)
            {
                string _x = ExToStr(ex);
            }
            finally
            {
                if (_config != null)
                {
                    RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                }
            }
            return result;
        }

        private string GetExStr(Exception ex)
        {
            if (ex != null)
                return string.Format("{0} [{1}] @{2}",
                    new object[] { ex.Message, ex.GetType().FullName, ex.Source });
            else
                return "";
        }

        private string ExToStr(Exception ex)
        {
            return GetExStr(ex) +
                (
                    (ex.InnerException != null)
                    ? ("\r\n\t" + GetExStr(ex.InnerException))
                    : ""
                ) + "\r\n" + ex.StackTrace;
        }


     
    }
}
