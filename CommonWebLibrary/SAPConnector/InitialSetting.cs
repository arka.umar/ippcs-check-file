﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

/*created by : niit. hendra wahyudi 
 date : 03-12-2012
 */

namespace Toyota.Common.Web.SAPConnector
{
    public class InitialSetting
    {


        private static string _SAPName = Convert.ToString(ConfigurationManager.AppSettings["SAP_NAME"]);
        public static string SAPName
        {
            get { return InitialSetting._SAPName; }
        }

        private static string _SAPClient = Convert.ToString(ConfigurationManager.AppSettings["SAP_CLIENT"]);
        public static string SAPClient
        {
            get { return InitialSetting._SAPClient; }
        }

        private static string _SAPLang = Convert.ToString(ConfigurationManager.AppSettings["SAP_LANG"]);
        public static string SAPLang
        {
            get { return InitialSetting._SAPLang; }
        }

        private static string _SAPAsHost = Convert.ToString(ConfigurationManager.AppSettings["SAP_ASHOST"]);
        public static string SAPAsHost
        {
            get { return InitialSetting._SAPAsHost; }
        }

        private static string _SAPSysn = Convert.ToString(ConfigurationManager.AppSettings["SAP_SYSN"]);
        public static string SAPSysn
        {
            get { return InitialSetting._SAPSysn; }
        }

        private static string _SAPMaxPoolSize = Convert.ToString(ConfigurationManager.AppSettings["SAP_MAX_POOL_SIZE"]);
        public static string SAPMaxPoolSize
        {
            get { return InitialSetting._SAPMaxPoolSize; }
        }

        private static string _SAPIdleTimeout = Convert.ToString(ConfigurationManager.AppSettings["SAP_IDLE_TIMEOUT"]);
        public static string SAPIdleTimeout
        {
            get { return InitialSetting._SAPIdleTimeout; }
        }



    }
}
