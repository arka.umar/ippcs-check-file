﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.SAPConnector
{

    [Serializable]
    public  class PaymentPostInput
    {
        public string DOC_NO { get; set; }
        public string DOC_YEAR { get; set; }


        public PaymentPostInput clone()
        {
            PaymentPostInput header = new PaymentPostInput();
            header.DOC_NO = DOC_NO;
            header.DOC_YEAR = DOC_YEAR;

            return header;
        }

    }

}
