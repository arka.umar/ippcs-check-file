﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Toyota.Common.Web;
using System.Collections.Specialized;
using Toyota.Common.Lookup;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using System.Reflection;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Validation;
using ServiceStack.Text;
using Toyota.Common.Web.Http;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using Toyota.Common.Web.Notification;
using Toyota.Common.Web.Html.Helper.DevExpress;
using System.Drawing;
using Toyota.Common.Web.MessageBoard;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Upload;
using Toyota.Common.Web.Messaging.Notification;
using Toyota.Common.Web.Lookup;
using Toyota.Common.Web.Email;
using Toyota.Common.Web.MVC.Filter;
using System.Net.Mail;
using Toyota.Common.Util.Text;
using Toyota.Common.Task.External;

namespace Toyota.Common.Web.MVC
{
    [UnlockFilter]
    [AuthorizationFilter]
    //[LoginValidation]
    [ElmahHandleError]
    public abstract class BaseController : Controller
    {

        protected string viewName;
        protected ModelHolder Model { private set; get; }
        protected Logging.ILogging Logger { private set; get; }
        protected bool CheckSessionState { set; get; }
        protected bool LogoutState { set; get; }
        protected string ScreenId { set; get; }
        protected string PageTitle { set; get; }
        protected static string UserID { set; get; }
        protected virtual void ValidateInput(InputValidation validation) { }
        private List<MappedValue<string, string>> ValidationKeys { set; get; }
        protected bool Blacklisted { set; get; }

        protected abstract void StartUp();
        [Obsolete("Init() is obsolete, please use StartUp() instead.")]
        protected abstract void Init();

        public BaseController(string pageTitle)
        {
            Model = new ModelHolder();
            Logger = Logging.Logging.GetInstance(ResourceSystemNames.APP_TITLE);
            LogoutState = false;
            CheckSessionState = true;
            PageTitle = pageTitle;
            string name = GetType().Name;
            ScreenId = name.Substring(0, name.Length - 10);
            ValidationKeys = new List<MappedValue<string, string>>();
            PageLayout = new StandardPageLayout();
        }

        [Obsolete("This constructor is obsolete, please use constructor with one argument instead")]
        public BaseController(string screenId, string pageTitle)
        {
            Model = new ModelHolder();
            Logger = Logging.Logging.GetInstance(ResourceSystemNames.APP_TITLE);
            CheckSessionState = true;
            LogoutState = false;
            PageTitle = pageTitle;
            ValidationKeys = new List<MappedValue<string, string>>();
            PageLayout = new StandardPageLayout();

            if (string.IsNullOrEmpty(screenId))
            {
                string name = GetType().Name;
                ScreenId = name.Substring(0, name.Length - 10);
            }
            else
            {
                ScreenId = screenId;
            }
        }

        protected string ApplicationUrl
        {
            get
            {
                string scheme = Request.Url.Scheme;
                if (SystemState.GetInstance().EnableSecuredUrlModification)
                {
                    scheme = "https";
                }

                string deploymentContext = ApplicationContext.GetInstance().GetDeploymentContext();
                if (!string.IsNullOrEmpty(deploymentContext))
                {
                    return string.Format("{0}://{1}/{2}", scheme, Request.ServerVariables["HTTP_HOST"], deploymentContext);
                }
                else
                {
                    return string.Format("{0}://{1}", scheme, Request.ServerVariables["HTTP_HOST"]);
                }
            }
        }

        protected User AuthorizedUser
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                return SessionState.GetAuthorizedUser();
            }
        }

        [System.Diagnostics.DebuggerStepThrough]
        protected virtual string GetViewName()
        {
            return viewName;
        }

        protected ILookup Lookup
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                if (Session == null)
                    return null;
                object objLookup = Session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP];
                if (objLookup != null)
                {
                    return (ILookup)objLookup;
                }
                return null;
            }
        }

        protected ISessionState SessionState
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                if (Lookup != null)
                {
                    return Lookup.Get<ISessionState>();
                }
                return null;
            }
        }

        protected ScreenMessageStack MessageStack
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                return Lookup.Get<ScreenMessageStack>();
            }
        }

        [System.Diagnostics.DebuggerStepThrough]
        protected void SetViewName(string viewName)
        {
            this.viewName = viewName;
        }
        [System.Diagnostics.DebuggerStepThrough]
        protected void AddValidationKey(string key)
        {
            ValidationKeys.Add(new MappedValue<string, string>()
            {
                Key = key,
                Value = key
            });
        }

        [System.Diagnostics.DebuggerStepThrough]
        protected void AddValidationKey(string mappingKey, string key)
        {
            ValidationKeys.Add(new MappedValue<string, string>()
            {
                Key = mappingKey,
                Value = key
            });
        }

        public string ValidateInput()
        {
            InputValidation validation = new InputValidation(HttpUtil.PackMappedRequestParameter(Request, ValidationKeys.ToArray()));
            string name = Request.Params["name"];
            if (!string.IsNullOrEmpty(name))
            {
                validation.Name = name;
                ValidateInput(validation);
            }

            string result = JsonSerializer.SerializeToString<InputValidation>(validation);
            return result;
        }

        public string UpdateScreenMessage()
        {
            string json = JsonSerializer.SerializeToString<ScreenMessageStack>(MessageStack);
            return json;
        }

        public string RequestProgress(string param)
        {
            if ((SessionState != null) && (SessionState.GetAuthorizedUser() != null))
            {
                List<string> prgNames = JsonSerializer.DeserializeFromString<List<string>>(param);
                Dictionary<string, int> progressMap = new Dictionary<string, int>();
                int progress;
                foreach (String name in prgNames)
                {
                    progress = UpdateProgress(name);
                    progressMap.Add(name, progress);
                }
                String jsonResult = JsonSerializer.SerializeToString<Dictionary<string, int>>(progressMap);
                return jsonResult;
            }

            return String.Empty;
        }

        protected virtual int UpdateProgress(string name)
        {
            return 0;
        }

        protected void Reset()
        {
            if (SessionState != null)
            {
                SessionState.SetAuthorizedUser(null);
                Lookup.Remove<MenuModel>();
            }
            Session.Clear();
        }

        protected override void Initialize(RequestContext requestContext)
        {
            //requestContext.HttpContext.Response.BufferOutput = true;
            HttpSessionStateBase session = requestContext.HttpContext.Session;
            IProviderResolver providerResolver = ProviderResolver.GetInstance();

            ILookup lookup = null;
            object objLookup = session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP];
            if (objLookup != null)
            {
                lookup = (ILookup)objLookup;
            }
            else
            {
                lookup = providerResolver.Get<ILookup>();
                lookup.Add(providerResolver.Get<ISessionState>());
                lookup.Add(new ScreenMessageStack());
                lookup.Add(new PreviousHandler());
                session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP] = lookup;
            }

            ISessionState systemState = lookup.Get<ISessionState>();
            User user = (User)requestContext.HttpContext.Session[ResourceSystemNames.__WEB_SERVICE_USER];
            if (user != null)
            {
                systemState.SetAuthorizedUser(user);
            }
            else
            {
                user = systemState.GetAuthorizedUser();
            }

            bool sessionIsValid = (user != null);
            if (!CheckSessionState)
            {
                sessionIsValid = true;
            }

            sessionIsValid = sessionIsValid && !Blacklisted;

            if (sessionIsValid)
            {
                PageLayout.ScreenWidth = requestContext.HttpContext.Request.Browser.ScreenPixelsWidth;
                PageLayout.ScreenHeight = requestContext.HttpContext.Request.Browser.ScreenPixelsHeight;

                PageModel pageModel = new PageModel();
                if (SystemState.GetInstance().EnableMenuAutoGeneration)
                {
                    MenuModel menuModel = lookup.Get<MenuModel>();

                    if (menuModel == null)
                    {
                        menuModel = new MenuModel();
                        lookup.Add(menuModel);
                    }
                    if ((menuModel.Menus == null) || (menuModel.Menus.Count == 0))
                    {
                        if (user != null)
                        {
                            IMenuProvider menuProvider = MenuProvider.GetInstance();
                            menuModel.Menus = menuProvider.GetAll(user);
                        }
                    }
                    pageModel.MenuItems = menuModel.Menus;
                    if (user != null)
                    {
                        Model.AddModel(user);
                    }

                }
                pageModel.MessageStack = lookup.Get<ScreenMessageStack>();
                Model.AddModel(pageModel);
                Init();
            }
            else
            {
                if (!requestContext.HttpContext.Response.IsRequestBeingRedirected)
                   requestContext.HttpContext.Response.Redirect("/");
            }

            base.Initialize(requestContext);
        }

        protected StandardPageLayout PageLayout { private set; get; }

        public virtual ActionResult Index()
        {
            ViewData["PageTitle"] = PageTitle;
            ViewData["ScreenId"] = ScreenId;
            ViewData["Title"] = ResourceSystemNames.APP_TITLE;
            ViewData["ApplicationUrl"] = ApplicationUrl;

            User user = SessionState.GetAuthorizedUser();
            if (user != null)
            {
                ViewData["Username"] = user.FullName;
            }

            PageLayout.AttachSettings(ViewData);
            ViewData["PageLayout"] = PageLayout;

            if (user != null)
            {
                StartUp();
            }

            ViewResult view = View(Model);
            if (!string.IsNullOrEmpty(GetViewName()))
            {
                view.ViewName = GetViewName();
            }
            return view;
        }

        protected void BaseHandleException(Exception ex, Logging.EventType eventType, bool NotifEmail = false)
        {
            string message = "IPPCS Application \r\n\r\n" +
                                    "USER ID: {0} \r\n" +
                                    "MESSAGE: {1}\r\n" +
                                    "SOURCE: {2}\r\n" +
                                    "TARGETSITE: {3}\r\n" +
                                    "STACKTRACE: {4}\r\n";
            string msg = "";
            string source = "";
            string targetSite = "";
            string stackTrace = "";
            if (ex.Message != null)
                msg = ex.Message;
            if (ex.Source != null)
                source = ex.Source;
            if (ex.TargetSite != null)
                targetSite = ex.TargetSite.ToString();
            if (ex.StackTrace != null)
                stackTrace = ex.StackTrace;
            message = string.Format(message, UserID, msg, source, targetSite, stackTrace);
            Logger.Write(eventType, message);

            if (NotifEmail)
            {
                ISmtpMailService mail = SmtpMailService.GetInstanceOf<ExceptionLogSmtpMailService>();
                mail.Send(new string[] { UserID, msg, source, targetSite, stackTrace, ex.GetType().FullName });
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.BufferOutput = true;

            base.OnActionExecuting(filterContext);
            if (ApplicationContext.GetInstance().GetUnlockFilterState())
            {
                if (!SessionState.GetUnlockState() && Lookup != null)
                {
                    PreviousHandler previous = Lookup.Get<PreviousHandler>();
                    if (previous != null)
                    {
                        previous.Add(new ActionExecuting()
                        {
                            Controller = (string)filterContext.RouteData.Values["controller"],
                            Action = (string)filterContext.RouteData.Values["action"]
                        });
                    }
                }
            }


        }

        //add by : fid.goldy
        //date  add  : 03/07/2013
        //protected override void OnActionExecuted(ActionExecutedContext filterContext)
        //{
        //    base.OnActionExecuted(filterContext);
        //    filterContext.HttpContext.
        //    //ResultExecutedContext

        //    filterContext.HttpContext.Response.BufferOutput = true;

        //}


        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);

        }
        //end of add by goldy

        public string NotificationAjax()
        {
            NotificationProvider nProvider = NotificationProvider.GetInstance();
            string notiStr = nProvider.GetAllNotification(SessionState.GetAuthorizedUser());
            return notiStr;
        }

        public string _ajax_ResizeGrid(string name, string width, string height)
        {
            Dictionary<string, Size> resizingGrids = PageLayout.ResizingGrids;
            Size size = new Size();
            if (!resizingGrids.ContainsKey(name))
            {
                resizingGrids.Add(name, size);
            }

            if (!string.IsNullOrEmpty(width))
            {
                size.Width = Convert.ToInt32(width);
            }
            if (!string.IsNullOrEmpty(height))
            {
                size.Height = Convert.ToInt32(height);
            }
            return MvcHtmlString.Create(String.Empty).ToHtmlString();
        }

        public string _ajax_GetUserLogin()
        {
            List<User> users = UserProvider.GetInstance().GetUsers();
            return JSON.ToString<List<User>>(users);
        }

        public ActionResult DownloadAttachment(string path)
        {
            FTPUpload ftp = new FTPUpload();
            string errorMessage = String.Empty;
            string realPath = path.Replace('$', '/');
            byte[] fileBytes = ftp.FtpDownloadByte("ftp://" + ftp.Setting.IP + "/" + realPath, ref errorMessage);
            if (string.IsNullOrEmpty(errorMessage))
            {
                string[] fractions = realPath.Split('/');
                return File(fileBytes, "application/force-download", fractions[fractions.Length - 1]);
            }
            return null;
        }

        protected string CreateAttachmentDownloadUrl(string path, string fileName)
        {
            if (!path.EndsWith("/"))
            {
                path += "/";
            }
            return ApplicationUrl + "/" + ScreenId + "/DownloadAttachment?path=" + path.Replace('/', '$') + fileName;
        }

        [SkipUnlockAction]
        public string _ajax_MessageBoard_LoadMessages(string boardName)
        {
            if (!string.IsNullOrEmpty(boardName))
            {
                IMessageBoardService msgBoardService = MessageBoardService.GetInstance();
                List<MessageBoardMessageGroup> messages = msgBoardService.List(boardName);
                FTPUpload ftp = new FTPUpload();
                FTPSettings ftpSetting = ftp.Setting;
                if (messages != null)
                {
                    List<MessageBoardAttachment> attachments;
                    foreach (MessageBoardMessageGroup g in messages)
                    {
                        attachments = msgBoardService.GetAttachments(boardName, g.GroupId);
                        if (attachments != null)
                        {
                            foreach (MessageBoardAttachment attachment in attachments)
                            {
                                attachment.Url = CreateAttachmentDownloadUrl(attachment.Path, attachment.Filename);
                            }
                            g.Attachments.AddRange(attachments);
                        }
                    }
                    return JSON.ToString<List<MessageBoardMessageGroup>>(messages);
                }
            }

            return JSON.ToString<List<MessageBoardMessageGroup>>(new List<MessageBoardMessageGroup>());
        }
        public string _ajax_MessageBoard_SaveMessage(string boardName, string uploadId, string recipients, string text, string groupId, bool notifyBySMS, bool notifyByEmail, bool notifyByWall)
        {
            string[] fracsRecipients = recipients.Split(',');
            MessageBoardMessage message;
            List<MessageBoardMessage> messageList = new List<MessageBoardMessage>();
            NotificationMessage notification;
            List<NotificationMessage> notificationList = new List<NotificationMessage>();
            User user = SessionState.GetAuthorizedUser();
            StringBuilder stringBuilder = new StringBuilder();
            List<MessageBoardAttachment> attachments;
            IMessageBoardService msgBoardService = MessageBoardService.GetInstance();
            StringBuilder emailBuilder = new StringBuilder();

            StringBuilder mailBodyBuilder = new StringBuilder();
            mailBodyBuilder.AppendLine("Dear {0},\n\n");
            mailBodyBuilder.AppendLine("You've got new message from {1}, \n");
            mailBodyBuilder.AppendLine("{2}");
            mailBodyBuilder.AppendLine("To see all message, please check screen <b>" + ApplicationUrl + "/" + ScreenId + "</b>\n\n");
            mailBodyBuilder.AppendLine("Best Regards,");
            mailBodyBuilder.AppendLine("Message Board");
            string mailBody = mailBodyBuilder.ToString();

            MailSetting mailSettings = new MailSetting();
            string mailHost = System.Configuration.ConfigurationManager.AppSettings["EmailHost"];
            string mailPort = System.Configuration.ConfigurationManager.AppSettings["EmailPort"];
            mailSettings.Host = System.Configuration.ConfigurationManager.AppSettings["EmailHost"];
            mailSettings.Port = Convert.ToInt32(mailPort);

            SmtpEmail mail = new SmtpEmail();
            mail.IsBodyHtml = true;
            mail.Subject = "New message at Message Board";
            mail.MailFrom = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"];

            string smsServerAddress = System.Configuration.ConfigurationManager.AppSettings["SMS_SERVER"];
            GSMClient.IClient smsSender = new GSMClient.Client(smsServerAddress, 13005);

            foreach (string rec in fracsRecipients)
            {
                message = new MessageBoardMessage()
                {
                    GroupId = Convert.ToInt32(groupId.Trim()),
                    BoardName = boardName,
                    Recipient = rec,
                    Text = text,
                    Date = DateTime.Now,
                    Sender = user.Username,
                    CarbonCopy = false
                };
                messageList.Add(message);

                if (notifyByWall)
                {
                    stringBuilder.Clear();
                    stringBuilder.AppendLine(message.Text);
                    attachments = msgBoardService.GetAttachments(message.BoardName, message.GroupId);
                    if (attachments != null)
                    {
                        stringBuilder.AppendLine("<br/><i>Attachments:</i> <br/>");
                        foreach (MessageBoardAttachment att in attachments)
                        {
                            att.Url = CreateAttachmentDownloadUrl(att.Path, att.Filename);
                            stringBuilder.AppendLine("# <a target='_blank' href='" + att.Url + "'><i>" + att.Filename + "</i></a><br/>");
                        }
                    }

                    notification = new NotificationMessage()
                    {
                        Author = message.Sender,
                        Recipient = message.Recipient,
                        ActionUrl = "",
                        Content = stringBuilder.ToString(),
                        PostDate = message.Date,
                        Title = "New Message from Message Board"
                    };
                    notificationList.Add(notification);
                }

                if (notifyByEmail)
                {
                    mail.Body = string.Format(mailBody, message.Recipient, message.Sender, message.Text);
                    mail.Setting = mailSettings;
                    mail.MailTo = new string[] { message.Sender + "@toyota.co.id" };
                    mail.Send();
                }
                else if (notifyBySMS)
                {
                    string SMSCommmandFormat = "ISMS.Send(+param: Number={0}, +param: Message={1})";
                    smsSender.Open();
                    smsSender.Send(string.Format(SMSCommmandFormat, "081310455283", text)); // harus ambil dari nomor telp usernya, tunggu daftar user dari sc
                    smsSender.Close();
                }
            }

            msgBoardService.Save(messageList.ToArray());
            if (notifyByWall)
            {
                NotificationService.GetInstance().Save(notificationList, user);
            }

            MessageBoardSessionCache cache = Lookup.Get<MessageBoardSessionCache>();
            if (cache != null)
            {
                cache.RemoveUploadCategory(uploadId);
            }

            return _ajax_MessageBoard_LoadMessages(boardName);
        }
        public string _ajax_MessageBoard_GetNewAttachments(string id)
        {
            MessageBoardSessionCache cache = Lookup.Get<MessageBoardSessionCache>();
            if (cache != null)
            {
                FileUploadCategory cachedCategory = cache.GetUploadCategory(id);
                if (cachedCategory != null)
                {
                    List<FileUpload> files = FileUploadRegistry.GetInstance().List(cachedCategory);
                    if ((files != null) && (files.Count > 0))
                    {
                        FTPUpload ftp = new FTPUpload();
                        FTPSettings ftpSetting = ftp.Setting;
                        List<MessageBoardAttachment> attachments = new List<MessageBoardAttachment>();
                        foreach (FileUpload f in files)
                        {
                            attachments.Add(new MessageBoardAttachment()
                            {
                                Description = f.Description,
                                Filename = f.FileName,
                                Id = f.Id,
                                Path = f.Path,
                                Url = "ftp://" + ftpSetting.IP + "/" + f.Path + "/" + f.FileName
                            });
                        }

                        return JSON.ToString<List<MessageBoardAttachment>>(attachments);
                    }
                }
            }
            return String.Empty;
        }

        [SkipUnlockAction]
        public string _ajax_MessageBoard_GetUnreadMessageCount(string boardName)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int? num = db.SingleOrDefault<int?>("MessageBoard_GetUnreadMessageCount", new object[] { boardName });
            return Convert.ToString(num);
        }
        public string _ajax_MessageBoard_MarkRead(string boardName)
        {
            User user = SessionState.GetAuthorizedUser();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            db.Execute("MessageBoard_MarkRead", new object[] { boardName, user.Username });
            return String.Empty;
        }
        public ActionResult _ajax_MessageBoard_RecipientListCallback(string id, string boardName)
        {
            MessageBoardSettings settings = new MessageBoardSettings();
            settings.Name = id;
            settings.BoardName = boardName;

            settings.DataSource = UserProvider.GetInstance().GetUsers();
            return PartialView("MessageBoard/MessageBoard_RecipientList", settings);
        }

        public ActionResult _ajax_FileUploadPartial(string settings)
        {
            FileUploadSettings settingModel = JSON.ToObject<FileUploadSettings>(settings);
            return PartialView("FileUpload/FileUpload", settingModel);
        }
        public ActionResult _ajax_FileUpload(string id, string category)
        {
            FileUploadCategory uploadCategory = JSON.ToObject<FileUploadCategory>(category);
            MessageBoardSessionCache cache = Lookup.Get<MessageBoardSessionCache>();
            if (cache != null)
            {
                FileUploadCategory cachedCategory = cache.GetUploadCategory(id);
                if (cachedCategory != null)
                {
                    if (!string.IsNullOrEmpty(cachedCategory.Group))
                    {
                        uploadCategory.Group = cachedCategory.Group;
                    }
                    if (!string.IsNullOrEmpty(cachedCategory.Key))
                    {
                        uploadCategory.Key = cachedCategory.Key;
                    }
                    if (!string.IsNullOrEmpty(cachedCategory.Qualifier))
                    {
                        uploadCategory.Qualifier = cachedCategory.Qualifier;
                    }
                }
            }
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles(id);
            if ((files != null) && (files.Length > 0))
            {
                string path;
                FTPUpload ftp = new FTPUpload();
                FileUpload fileInfo;
                User user = SessionState.GetAuthorizedUser();
                foreach (UploadedFile file in files)
                {
                    path = _GetFileUploadPath(id);
                    if (string.IsNullOrEmpty(path))
                    {
                        path = GetFileUploadPath(id);
                    }
                    if (!string.IsNullOrEmpty(path))
                    {
                        string errorMessage = String.Empty;
                        ftp.FtpUpload(path, file.FileName, file.FileBytes, ref errorMessage);
                        if (string.IsNullOrEmpty(errorMessage))
                        {
                            fileInfo = new FileUpload()
                            {
                                FileName = file.FileName,
                                Description = file.FileName,
                                Group = uploadCategory.Group,
                                Key = uploadCategory.Key,
                                Qualifier = uploadCategory.Qualifier,
                                Path = path
                            };
                            FileUploadRegistry.GetInstance().Save(fileInfo, user);
                        }
                    }
                }
            }
            return null;
        }
        public void _ajax_FileUpload_CacheUploadCategory(string id, string group, string key, string qualifier)
        {
            if (string.IsNullOrEmpty(id) || (string.IsNullOrEmpty(group) && string.IsNullOrEmpty(key) && string.IsNullOrEmpty(qualifier)))
            {
                return;
            }
            MessageBoardSessionCache cache = Lookup.Get<MessageBoardSessionCache>();
            if (cache == null)
            {
                cache = new MessageBoardSessionCache();
                Lookup.Add(cache);
            }
            FileUploadCategory category = cache.GetUploadCategory(id);
            if (category == null)
            {
                category = new FileUploadCategory()
                {
                    Group = group,
                    Key = key,
                    Qualifier = qualifier
                };
            }
            cache.SaveUploadCategory(id, category);
        }
        public string _ajax_FileUpload_Remove(string id)
        {
            if (id != null)
            {
                long dbId = Convert.ToInt64(id);
                FileUpload file = FileUploadRegistry.GetInstance().GetById(dbId);
                if (file != null)
                {
                    FTPUpload ftp = new FTPUpload();
                    string errorMessage = String.Empty;
                    bool succeed = ftp.FTPRemoveFile(file.Path, file.FileName, ref errorMessage);
                    if (succeed)
                    {
                        succeed = FileUploadRegistry.GetInstance().RemoveById(dbId);
                        if (succeed)
                        {
                            return "true";
                        }
                    }
                }
            }

            return "false";
        }

        private string _GetFileUploadPath(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                DateTime today = DateTime.Now;
                if (name.StartsWith("messageBoard_updAttachment_"))
                {
                    return string.Format("IPPCS/{0}/{1}", today.Year, "MessageBoard/Attachments");
                }
            }
            return null;
        }
        protected string GetFileUploadPath(string name) { return null; }

        public string _ajax_Notification_Load()
        {
            User user = SessionState.GetAuthorizedUser();
            if (user != null)
            {
                INotificationService service = NotificationService.GetInstance();
                List<NotificationMessage> notifications = service.ListByUser(user);
                if (notifications != null)
                {
                    notifications = NotificationService.GetInstance().SortByPostDate(notifications);
                }
                else
                {
                    notifications = new List<NotificationMessage>();
                }

                return JSON.ToString<List<NotificationMessage>>(notifications);
            }
            return String.Empty;
        }
        public string _ajax_Notification_MarkAsRead(long id)
        {
            NotificationService.GetInstance().MarkAsRead(id, true);
            return string.Empty;
        }

        public ActionResult LookupPart<T>(
           string screen,
           string control,
           string grid,
           string column,
           Func<List<T>> fn,
           string partView = "GridLookup/PartialGrid")
        {
            TempData["GridName"] = control;
            
            var u = Model.GetModel<User>();
            if (u == null) return PartialView(partView, null);
            List<T> data = fn();
            List<T> dataFiltered = (u != null) ? u.FilteringArea<T>(data, column, screen, grid) : new List<T>();
           
            ViewData[grid] = (dataFiltered.Count > 0) ? dataFiltered : data;
            if (string.IsNullOrEmpty(partView)) 
                return null;
            return PartialView(partView, ViewData[grid]);
        }
        //public string _ajax_GetBackgroundTaskProgress(string name)
        //{
        //    ObservableBackgroundTask task = (ObservableBackgroundTask)InternalBackgroundTaskQueue.GetInstance().Get(name);
        //    if (task != null)
        //    {
        //        return Convert.ToString(task.Progress);
        //    }
        //    return Convert.ToString(-1);
        //}
        //public string _ajax_ExecuteBackgroundTask(string name, string param)
        //{
        //    IDictionary<string, object> paramMap = null;
        //    if (!string.IsNullOrEmpty(param))
        //    {
        //        paramMap = JSON.ToObject<IDictionary<string, object>>(param);
        //    }
        //    InternalBackgroundTaskQueue.GetInstance().Execute(name, paramMap);
        //    return String.Empty;
        //}
    }
}
