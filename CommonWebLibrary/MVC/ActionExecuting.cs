﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MVC
{
    [Serializable]
    public class ActionExecuting
    {
        public string Controller { get; set; }
        public string Action { get; set; }

        public ActionExecuting()
        {}
    }
}
