﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MVC.Filter
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MarkAsAuthorizedControllerAttribute : Attribute
    { }
}
