﻿//Remarked by : fid.goldy
//Remarked date : 01072013
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web.Mvc;
//using System.Web;
//using Toyota.Common.Lookup;
//using Toyota.Common.Web.Email;
//using Toyota.Common.Web.Credential;

//namespace Toyota.Common.Web.MVC.Filter
//{
//    public class HandlingErrorAttribute : HandleErrorAttribute
//    {
//        private bool enabled;

//        public HandlingErrorAttribute()
//        {
//            enabled = ApplicationContext.GetInstance().GetHandlingErrorFilterState();
//        }

//        private ISessionState SystemState
//        {
//            [System.Diagnostics.DebuggerStepThrough]
//            get
//            {
//                object objLookup = HttpContext.Current.Session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP];
//                if (objLookup != null)
//                {
//                    return ((ILookup)objLookup).Get<ISessionState>();
//                }
//                return null;
//            }
//        }
        
//        public override void OnException(ExceptionContext filterContext)
//        {
//            if (!enabled) return;
//            if (filterContext.ExceptionHandled)
//                return;

//            string userName = "";
//            if (SystemState != null)
//            {
//                User user = SystemState.GetAuthorizedUser();
//                if (user != null)
//                { userName = user.Username; }
//                try
//                {
//                    ISmtpMailService mail = SmtpMailService.GetInstanceOf<ExceptionLogSmtpMailService>();
//                    mail.Send(new string[] { 
//                        string.Format("{0}, {1} / {2} bit", Environment.MachineName, Environment.OSVersion.VersionString, Environment.Is64BitOperatingSystem ? "64" : "32"),
//                        DateTime.Now.ToString(),
//                        userName, 
//                        filterContext.Exception.Message , 
//                        filterContext.Exception.Source, 
//                        filterContext.Exception.StackTrace,
//                        filterContext.Exception.TargetSite.Name,  
//                        filterContext.Exception.GetType().FullName });
//                }
//                catch (Exception ex) { }
//            } 
//        }
//    }
//}
