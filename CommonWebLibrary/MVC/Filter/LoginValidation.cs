﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Toyota.Common.Lookup;
using System.Web;
using Newtonsoft.Json;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.MVC.Filter
{
    public class LoginValidationAttribute : AuthorizeAttribute
    {
        private ILookup Lookup
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                object objLookup = HttpContext.Current.Session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP];
                if (objLookup != null)
                { return ((ILookup)objLookup); }
                return null;
            }
        }

        private ISessionState SessionState
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            { return Lookup.Get<ISessionState>(); }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            OpenLDAPService.SecurityCenterClient sc = new OpenLDAPService.SecurityCenterClient();
            User user = SessionState.GetAuthorizedUser();
            string UserLogged = sc.getUserLogged(user.FullName);
            sc.Close();
            bool notFound = false;
            if (!string.IsNullOrEmpty(UserLogged))
            {
                List<UserAD> userADUserLogged = JsonConvert.DeserializeObject<List<UserAD>>(UserLogged);
                foreach (UserAD uad in userADUserLogged)
                {
                    if ((uad.properties["sessionid"][0] == filterContext.RequestContext.HttpContext.Session.SessionID) &&
                        uad.properties["ipaddress"][0] == filterContext.RequestContext.HttpContext.Request.UserHostAddress)
                    {
                        notFound = true;
                        break;
                    }
                }
                if (!notFound)
                {
                    filterContext.Result = new EmptyResult(); // cancel the current action/response
                        System.Web.Mvc.UrlHelper Url;  Url = new System.Web.Mvc.UrlHelper(filterContext.RequestContext);
                        filterContext.RequestContext.HttpContext.Response.Redirect(Url.Action("Logout", "BaseLoginController"), true);
                }
            }
        }
    }
}
