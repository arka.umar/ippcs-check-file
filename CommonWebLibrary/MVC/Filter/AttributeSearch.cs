﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Toyota.Common.Web.MVC.Filter
{
    internal class AttributeSearch
    { 
        public static List<string> FindSpecificMethodAttribute(Type searchClass, Type attributeSearch, bool includeDerivedClass = false)
        {
            List<string> ret = new List<string>();
            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    foreach (Type t in a.GetTypes())
                    { 
                        if ((t.IsSubclassOf(searchClass) || t.IsAssignableFrom(searchClass)) && includeDerivedClass)
                        {
                            foreach (MethodInfo m in t.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly))
                            {
                                object[] cust = m.GetCustomAttributes(attributeSearch, true);
                                if (cust.Length > 0)
                                {
                                    ret.Add(t.Name + "." + m.Name);
                                }
                            }
                        }
                    }
                }
                catch
                { } 
            } 
            if (ret.Count == 0)
                return null;
            else
                return ret;
            //// error on production server when loop Assemblies, but fast perf
            //var methodNames = AppDomain.CurrentDomain.GetAssemblies().ToList()
            //                .SelectMany(assembly => assembly.GetTypes().Where(t => t.IsSubclassOf(searchClass) && includeDerivedClass))
            //                .SelectMany(t => t.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            //                .Where(m => m.GetCustomAttributes(attributeSearch, true).Length > 0)
            //                .Select(t => t.DeclaringType.FullName + "." + t.Name)
            //                .Distinct();
              
            //if (methodNames.Any())
            //{ return methodNames.ToList(); }
            //return null;
        }

        public static List<string> FindSpecificClassAttribute(Type searchClass, Type attributeSearch, bool includeDerivedClass = false)
        {
            List<string> ret = new List<string>();
            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    foreach (Type t in a.GetTypes())
                    {
                        if ((t.IsSubclassOf(searchClass) || t.IsAssignableFrom(searchClass)) && includeDerivedClass)
                        {
                            object[] cust = t.GetCustomAttributes(attributeSearch, true);
                            if (cust.Length > 0)
                            {
                                ret.Add(t.Name);
                            }
                        }
                    }
                }
                catch
                { } 
            } 
            if (ret.Count == 0)
                return null;
            else
                return ret;
            //// error on production server when loop Assemblies, but fast perf
            //var methodNames = AppDomain.CurrentDomain.GetAssemblies().ToList()
            //                .SelectMany(assembly => assembly.GetTypes().Where(t => t.IsSubclassOf(searchClass) && includeDerivedClass))
            //                .Where(m => m.GetCustomAttributes(attributeSearch, true).Length > 0)
            //                .Select(t => t.Name)
            //                .Distinct();

            //if (methodNames.Any())
            //{ return methodNames.ToList(); }
            //return null;
        }
 
    }
}
