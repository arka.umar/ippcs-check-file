﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using Toyota.Common.Lookup;
using Toyota.Common.Web.Credential;
using System.Web.Routing;
using System.Web.SessionState;
using Toyota.Common.Web.Util;
using System.Reflection;

namespace Toyota.Common.Web.MVC.Filter
{

 /*
 * niit.yudha - 23 may 2013 
 * Unlock screen
 */

    public class UnlockFilterAttribute : AuthorizeAttribute
    {
        private bool enabled;
        private string controllerName;
        private List<string> unlockIgnoreAction;
        private List<string> controllerNameClass;
        private List<string> excludeController;

        private ILookup Lookup
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                object objLookup = HttpContext.Current.Session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP];
                if (objLookup != null)
                { return ((ILookup)objLookup); }
                return null;
            }
        }

        private ISessionState SessionState
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            { return Lookup.Get<ISessionState>(); }
        }
 
        public UnlockFilterAttribute()
        {
            enabled = ApplicationContext.GetInstance().GetUnlockFilterState();
            FilterCollection filterMap = FilterCollection.GetInstance();
            if (excludeController == null)
            {
                excludeController = new List<string>();
                excludeController = filterMap.Get(typeof(SkipUnlockControllerAttribute).Name); 
            }  

            if (controllerNameClass == null)
            {
                controllerNameClass = filterMap.Get(typeof(MarkAsUnlockControllerAttribute).Name); 
                if (controllerNameClass != null)
                {
                    if (controllerNameClass.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(controllerNameClass[0]))
                            controllerName = controllerNameClass[0].Replace("Controller", "");
                    }
                }
            }
            else 
                controllerName = controllerNameClass[0].Replace("Controller", ""); 

            if (unlockIgnoreAction == null)
            {
                unlockIgnoreAction = new List<string>(); 
                List<string> finds = filterMap.Get(typeof(UnlockIgnoreActionAttribute).Name);
                if (finds != null)
                {
                    foreach (string name in finds)
                    {
                        string[] nm = name.Split('.');
                        unlockIgnoreAction.Add(nm[nm.Length - 1].ToLower());
                    }
                }
            }
        }

        private bool IsUnlock()
        {
            if (Lookup != null)
            { 
                User user = SessionState.GetAuthorizedUser();
                PreviousHandler handler = Lookup.Get<PreviousHandler>();
                if (handler != null && user != null)
                {
                    TimeSpan lastTimeSpan = handler.LastAction;
                    if (lastTimeSpan.Ticks > 0)
                    {
                        TimeSpan timerTimeOut = new TimeSpan(lastTimeSpan.Ticks).Add(TimeSpan.FromMinutes((double)user.UnlockTimeOut));
                        TimeSpan timerTimeNow = new TimeSpan(DateTime.Now.Ticks);
                        if (timerTimeNow > timerTimeOut)
                        { return true; }
                    }
                } 
            }
            return false;
        }
  
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!enabled) return;
            PreviousHandler handler = Lookup.Get<PreviousHandler>();
            if (handler.Count > 1 && excludeController.Count > 0)
            {
                if (excludeController.Contains(handler.Get(1).Controller))
                    return;
            }
            if (IsUnlock() && !SessionState.GetUnlockState())
            {
                if (!unlockIgnoreAction.Contains(filterContext.RouteData.Values["action"].ToString().ToLower()))
                {
                    if (!filterContext.RequestContext.HttpContext.Response.IsRequestBeingRedirected)
                    { 
                        filterContext.Result = new EmptyResult(); // cancel the current action/response
                        System.Web.Mvc.UrlHelper Url;  Url = new System.Web.Mvc.UrlHelper(filterContext.RequestContext);
                        filterContext.RequestContext.HttpContext.Response.Redirect(Url.Action("", controllerName), true);
                    }
                }
                SessionState.SetUnlockState(true);
            }
            else
            {
                if (SessionState.GetUnlockState() && filterContext.RouteData.Values["controller"].ToString().ToLower() != controllerName.ToLower())
                {
                    if (!filterContext.RequestContext.HttpContext.Response.IsRequestBeingRedirected)
                    {
                        filterContext.Result = new RedirectToRouteResult(new
                                                      RouteValueDictionary(new { controller = controllerName }));
                        filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);  
                    }
                }
            }
        }

    }
}
