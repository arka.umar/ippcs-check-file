﻿//Remarked by : fid.goldy
//Remarked date : 01072013
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web;
//using Toyota.Common.Lookup;
//using System.Web.Mvc;
//using Toyota.Common.Web.Credential;
//using System.IO;

//namespace Toyota.Common.Web.MVC.Filter
//{
//    [AttributeUsage(AttributeTargets.Class)]
//    public class FileHandlingErrorAttribute : HandleErrorAttribute
//    {
//        private bool enabled;

//        public FileHandlingErrorAttribute()
//        { enabled = ApplicationContext.GetInstance().GetHandlingErrorFilterState(); }

//        private ISessionState SystemState
//        {
//            [System.Diagnostics.DebuggerStepThrough]
//            get
//            {
//                object objLookup = HttpContext.Current.Session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP];
//                if (objLookup != null) 
//                    return ((ILookup)objLookup).Get<ISessionState>(); 
//                return null;
//            }
//        }

//        private string GetBody()
//        {
//            string ret;
//            String MailPath = String.Format(System.Configuration.ConfigurationManager.AppSettings["ExceptionFileBody"], HttpContext.Current.Server.MapPath("~"));
//            using (StreamReader reader = File.OpenText(MailPath))
//            { ret = reader.ReadToEnd(); }
//            return ret;
//        } 

//        public override void OnException(ExceptionContext filterContext)
//        {
//            if (!enabled) return;
//            if (filterContext.ExceptionHandled)
//                return;
//            if (SystemState != null)
//            { 
//                string userName = "";
//                User user = SystemState.GetAuthorizedUser();
//                if (user != null)
//                { userName = user.Username; }
//                try
//                {
//                    string path = HttpContext.Current.Server.MapPath("~") + @"\ErrorLog\" ;
//                    if (System.IO.Directory.Exists(path))
//                    {
//                        string body = GetBody();
//                        body = string.Format(body, new string[] { 
//                        string.Format("{0}, {1} / {2} bit", Environment.MachineName, Environment.OSVersion.VersionString, Environment.Is64BitOperatingSystem ? "64" : "32"),
//                        DateTime.Now.ToString(),
//                        userName, 
//                        filterContext.Exception.Message , 
//                        filterContext.Exception.Source, 
//                        filterContext.Exception.StackTrace,
//                        filterContext.Exception.TargetSite.Name,  
//                        filterContext.Exception.GetType().FullName });
//                        System.IO.File.WriteAllText(path + DateTime.Now.ToString("MM-dd-yyyy HH-mm-ss") + ".html", body);  
//                    }   
//                }
//                catch (Exception ex) { }
//            } 
//        }
//    }
//}
