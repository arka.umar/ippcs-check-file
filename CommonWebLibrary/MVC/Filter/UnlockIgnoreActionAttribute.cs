﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MVC.Filter
{
    [AttributeUsage(AttributeTargets.Method)]
    public class UnlockIgnoreActionAttribute : Attribute
    { }
}
