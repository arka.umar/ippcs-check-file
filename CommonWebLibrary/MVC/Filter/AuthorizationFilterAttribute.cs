﻿/*
 * niit.yudha - 8 nov 2012 
 * This class is used for authentication between View and Action. 
 * Please see on PCS database for detail information.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Toyota.Common.Web.Credential;
using System.Web;
using log4net.Repository.Hierarchy;
using Toyota.Common.Lookup;

namespace Toyota.Common.Web.MVC.Filter
{
    public class AuthorizationFilterAttribute : AuthorizeAttribute
    {
        private bool enabled;
        private User user;
        private string controllerName;
        private List<string> controllerNameClass;

        private ISessionState SystemState
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                object objLookup = HttpContext.Current.Session[ResourceSystemNames.SESSION_KEY_GLOBAL_LOOKUP];
                if (objLookup != null)
                {
                    return ((ILookup)objLookup).Get<ISessionState>();
                }
                return null;
            }
        }
         
        public AuthorizationFilterAttribute(string ControllerName)
        {
            enabled = ApplicationContext.GetInstance().GetAuthFilterState();
            this.controllerName = ControllerName; 
        }

        public AuthorizationFilterAttribute()
        {
            enabled = ApplicationContext.GetInstance().GetAuthFilterState();
            FilterCollection filterMap = FilterCollection.GetInstance();
            if (controllerNameClass == null) 
            {
                controllerNameClass = filterMap.Get(typeof(MarkAsAuthorizedControllerAttribute).Name);  
                if (controllerNameClass != null)
                {
                    if (controllerNameClass.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(controllerNameClass[0]))
                            controllerName = controllerNameClass[0].Replace("Controller", "");
                    }
                }
            }
            else
            {
                controllerName = controllerNameClass[0].Replace("Controller", "");
            }
        }

        /// <summary>
        /// Called before the action method is invoked,
        /// From this we can handle all action.
        /// </summary>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!enabled) return;
            if (SystemState != null)
            {
                user = SystemState.GetAuthorizedUser();
                if (user != null)
                {
                    if (user.Authorization != null)
                    {
                        string controller = filterContext.RouteData.Values["controller"].ToString();
                        string action = filterContext.RouteData.Values["action"].ToString();
                        var ret = (from auth in user.Authorization
                                   from f in auth.AuthorizationDetails
                                   where f.Screen.ToLower() == controller.ToLower()
                                    && ((f.Action == null || f.Action.ToLower() == "null" || f.Action.ToLower() == action.ToLower()) || f.Action == "") 
                                    && (f.Screen_Auth == "1") 
                                   select f).ToList();
                        // if act is 0, indicates that current user doesn't have permission. Redirect to Unauthorized view.
                        if (ret != null)
                        {
                            if (ret.Count == 0)
                            {
                                #region Debug region for testing 
                                
                                if (controller == "JunbikiPartMapping")
                                     return;
                                #endregion Debug region for testing
                                filterContext.Result = new RedirectToRouteResult(new
                                                        RouteValueDictionary(new { controller = controllerName }));
                                filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                            }
                        }
                    }
                }
            }
        }
         
    }
}
