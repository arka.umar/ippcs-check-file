﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MVC.Filter
{
    internal class FilterCollection
    {
        private IDictionary<string, List<string>> map;
        private static readonly FilterCollection instance = new FilterCollection();

        [System.Diagnostics.DebuggerStepThrough]
        public static FilterCollection GetInstance()
        {
            return instance;
        }

        public FilterCollection()
        {
            map = new Dictionary<string, List<string>>();
            List<string> finds;
            finds = FindClass(typeof(BaseController), typeof(MarkAsUnlockControllerAttribute));
            Add("MarkAsUnlockControllerAttribute", finds);
            finds = FindClass(typeof(BaseController), typeof(MarkAsAuthorizedControllerAttribute));
            Add("MarkAsAuthorizedControllerAttribute", finds);
            finds = FindMethod(typeof(BaseController), typeof(SkipUnlockActionAttribute));
            Add("SkipUnlockActionAttribute", finds);
            finds = FindClass(typeof(BaseController), typeof(SkipUnlockControllerAttribute));
            Add("SkipUnlockControllerAttribute", finds);
            finds = FindMethod(typeof(BaseController), typeof(UnlockIgnoreActionAttribute));
            Add("UnlockIgnoreActionAttribute", finds);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void Add(string Key, List<string> Value)
        {
            if (!string.IsNullOrEmpty(Key) && Value != null)
                map.Add(Key, Value);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public List<string> Get(string Key)
        {
            if (map.ContainsKey(Key))
                return map[Key];
            else
                return null;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void Remove(string Key)
        {
            map.Remove(Key);
        }

        private List<string> FindClass(Type searchClass, Type attributeSearch)
        {
            List<string> controllerNameClass = new List<string>();  
            List<string>  finds = AttributeSearch.FindSpecificClassAttribute(searchClass, attributeSearch, true);
            if (finds != null)
            {
                if (finds.Count > 0)
                {
                    finds.ForEach(c =>
                    {
                        controllerNameClass.Add(c.Replace("Controller", ""));
                    });
                }
            }
            return controllerNameClass;
        }

        private List<string> FindMethod(Type searchClass, Type attributeSearch)
        {
            List<string> controllerNameClass = new List<string>();
            List<string> finds = AttributeSearch.FindSpecificMethodAttribute(searchClass, attributeSearch, true);
            if (finds != null)
            {
                if (finds.Count > 0)
                {
                    finds.ForEach(c =>
                    {
                        controllerNameClass.Add(c.Replace("Controller", ""));
                    });
                }
            }
            return controllerNameClass;
        }
    }
}
