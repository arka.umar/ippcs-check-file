﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;
using System.Web;
using Toyota.Common.Lookup;
using Toyota.Common.Web.Database;
using System.Configuration;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.Util;
using System.Web.Mvc;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.MVC.Login;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Lookup;

namespace Toyota.Common.Web.MVC
{
    public abstract class FrontController : Controller 
    {
        public FrontController()
        {
            LoginControllerName = "Login";
        }

        protected override void Initialize(RequestContext requestContext)
        {
            /* Buat testing aja */
            //User user = new User()
            //{
            //    Username = "fid.lufty",
            //    FirstName = "lufty",
            //    LastName = "abdillah",
            //    Email = "fid.lufty@toyota.co.id",

            //};
            //Position p = new Position();
            //p.ID = "100";
            //p.Name = "SPG";
            //p.Class = 2;
            //user.Position = p;

            //systemState.SetAuthorizedUser(user);
            /* --------------- */

            ILoginValidator loginValidator = ProviderResolver.GetInstance().Get<ILoginValidator>();
            User user = loginValidator.Validate(requestContext.HttpContext.Session, requestContext.HttpContext.Request, new Dictionary<string, object>());
            requestContext.HttpContext.Session[ResourceSystemNames.__WEB_SERVICE_USER] = user;
            if (user != null)
            {
                //requestContext.HttpContext.Session.Timeout = user.RoleSessionTimeOut; //configure at web config
                LoginControllerName = "Dashboard";
            }

            base.Initialize(requestContext);
        }

        public string LoginControllerName { set; get; }

        //[System.Diagnostics.DebuggerStepThrough]
        public ActionResult Index()
        {
            StartUp();
            return RedirectToAction("Index", LoginControllerName);
        }

        protected abstract void StartUp();
    }
}