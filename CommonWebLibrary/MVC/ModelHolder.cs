﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Lookup;

namespace Toyota.Common.Web.MVC
{
    
    public class ModelHolder
    {        
        private ILookup modelLookup;

        [System.Diagnostics.DebuggerStepThrough]
        public ModelHolder()
        {
            modelLookup = new SimpleLookup();
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void AddModel(object model)
        {
            if (model == null)
            {
                return;
            }

            modelLookup.Remove(model);
            modelLookup.Add(model);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void RemoveModel<T>()
        {
            object obj = modelLookup.Get<T>();
            if (obj != null)
            {
                modelLookup.Remove(obj);
            }
        }

        [System.Diagnostics.DebuggerStepThrough]
        public T GetModel<T>()
        {
           return modelLookup.Get<T>();
        }
    }
}
