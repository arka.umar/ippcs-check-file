﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;

namespace Toyota.Common.Web.MVC
{
    public class TCDataTableModel
    {
        private Dictionary<string, SQLQueryModel> filteringQueries;
        private Dictionary<string, SQLQueryModel> sortingQueries;

        public TCDataTableModel()
        {
            filteringQueries = new Dictionary<string, SQLQueryModel>();
            sortingQueries = new Dictionary<string, SQLQueryModel>();
        }

        public SQLQueryModel ListingQuery { set; get; }
        
        public void AddFilteringQuery(SQLQueryModel model)
        {
            if (!filteringQueries.ContainsKey(model.Name))
            {
                filteringQueries.Add(model.Name, model);
            }
            else
            {
                filteringQueries[model.Name] = model;
            }
        }

        public void AddSortingQuery(SQLQueryModel model)
        {
            if (!sortingQueries.ContainsKey(model.Name))
            {
                sortingQueries.Add(model.Name, model);
            }
            else
            {
                sortingQueries[model.Name] = model;
            }
        }

        public List<SQLQueryModel> FilteringQueries
        {
            get
            {
                if (filteringQueries.Count > 0)
                {
                    return filteringQueries.Values.ToList();
                }
                return null;
            }
        }

        public List<SQLQueryModel> SortingQueries
        {
            get
            {
                if (sortingQueries.Count > 0)
                {
                    return sortingQueries.Values.ToList();
                }
                return null;
            }
        }
    }
}
