﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.MVC
{
    public interface ISessionState
    {
        bool HasAuthorizedUser();
        User GetAuthorizedUser();
        void SetAuthorizedUser(User user);
        void SetTimeout(int timeout);
        int GetTimeout();
        void SetUnlockState(bool state);
        bool GetUnlockState(); 
    }
}
