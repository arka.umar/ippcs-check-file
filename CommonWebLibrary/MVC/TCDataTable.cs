﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using System.Web;
using DevExpress.Web.Mvc;

namespace Toyota.Common.Web.MVC
{
    public class TCDataTable
    {
        public string Name { set; get; }
        public long CurrentPage { set; get; }
        public long PageSize { set; get; }
        public long PageCount { set; get; }
        public long DataCount { set; get; }
        public object Data { set; get; }
        public bool PagerVisible { set; get; }

        public static void FetchModel<T>(ModelHolder controllerModel, HttpRequestBase request, string queryName)
        {
            FetchModel<T>(controllerModel, request, queryName, null);
        }

        public static void FetchModel<T>(ModelHolder controllerModel, HttpRequestBase request, string queryName, object[] queryParam)
        {
            PageModel pageModel = controllerModel.GetModel<PageModel>();
            pageModel.AddDataTable(TCDataTable.CreateModel<T>(request, queryName, queryParam));
        }

        public static TCDataTable CreateModel<T>(HttpRequestBase request, string queryName)
        {
            return CreateModel<T>(request, queryName, null);
        }

        public static TCDataTable CreateModel<T>(HttpRequestBase request, string queryName, object[] queryParam)
        {
            string arg = DevExpressHelper.CallbackArgument;
            string[] argFractions = arg.Split('|');

            Dictionary<string, object> pagingParams = null;
            Dictionary<string, object> sortingParams = null;

            string argumentString;
            string loweredArgumentString;
            for(int i = 0; i < argFractions.Length; i++)
            {
                argumentString = argFractions[i];
                loweredArgumentString = argumentString.ToLower();
                if (loweredArgumentString.StartsWith("_grid_pagination"))
                {
                    if (argumentString.EndsWith(";"))
                    {
                        argumentString = argumentString.Remove(argumentString.Length - 1);
                    }

                    string[] argumentFracs = argumentString.Split(':');
                    string[] paramFracs = argumentFracs[1].Trim().Split('&');
                    pagingParams = new Dictionary<string, object>();
                    foreach (string p in paramFracs)
                    {
                        argumentFracs = p.Split('=');
                        pagingParams.Add(argumentFracs[0], argumentFracs[1]);
                    }
                    break;
                }
                else if (loweredArgumentString.StartsWith("sort"))
                {
                    sortingParams = new Dictionary<string, object>();
                    string strColumnNumber = argFractions[i + 1];
                    int columnNumber = Convert.ToInt32(strColumnNumber);
                    columnNumber = (columnNumber / 10) + (columnNumber % 10);
                    sortingParams.Add("ColumnNumber", columnNumber);
                    break;
                }
            }

            if (sortingParams != null)
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                IPagedData<T> pagedResult = db.FetchByPage<T>("Dummy_ListDCL", String.Empty, "order by [DELIVERY_NO]", 1, 10, queryParam);
                //List<T> resultList = db.Fetch<T>("Dummy_ListDCL", queryParam != null ? queryParam : new object[] { });
                db.Close();

                if (pagedResult != null)
                {
                    TCDataTable tableModel = new TCDataTable()
                    {
                        Name = "sampleGrid",
                        CurrentPage = pagedResult.GetCurrentPage(),
                        DataCount = pagedResult.GetTotal(),
                        PageCount = pagedResult.GetTotalPages(),
                        PageSize = pagedResult.GetTotalPerPage(),
                        Data = pagedResult.GetData()
                    };
                    return tableModel;
                }                
            }

            #region Paging
            if (pagingParams != null)
            {
                string operation = Convert.ToString(pagingParams["Op"]).ToLower();
                string name = Convert.ToString(pagingParams["_grid_Name"]);
                long currentPage = Convert.ToInt64(pagingParams["_grid_CurrentPage"]);
                long totalPage = Convert.ToInt64(pagingParams["_grid_PageTotal"]);
                bool pagerVisible = Convert.ToBoolean(pagingParams["_grid_PagerVisible"]);
                long pageSize = Convert.ToInt64(pagingParams["_grid_PageSize"]);

                if (operation.Equals("nextpage"))
                {
                    if (currentPage < (totalPage - 1))
                    {
                        currentPage++;
                    }                    
                }
                else if (operation.Equals("lastpage"))
                {
                    currentPage = totalPage - 1;
                }
                else if (operation.Equals("firstpage"))
                {
                    currentPage = 0;
                }
                else if (operation.Equals("previouspage"))
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                IPagedData<T> pagedData = db.FetchByPage<T>("Dummy_ListDCL", currentPage + 1, pageSize, queryParam != null ? queryParam : new object[] { });
                db.Close();

                if (pagedData != null)
                {                    
                    TCDataTable tableModel = new TCDataTable()
                    {
                        Name = name,
                        CurrentPage = currentPage,
                        DataCount = pagedData.GetTotal(),
                        PageCount = pagedData.GetTotalPages(),
                        PageSize = pagedData.GetTotalPerPage(),
                        Data = pagedData.GetData(),
                        PagerVisible = pagerVisible
                    };
                    return tableModel;
                }
            }
            #endregion

            return null;
        }
    }
}
