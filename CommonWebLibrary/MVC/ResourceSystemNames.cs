﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web
{
    public class ResourceSystemNames
    {
        public const string SESSION_KEY_GLOBAL_LOOKUP = "__BaseController_GlobalLookup";
        public const string SESSION_KEY_MENU = "__SessionData_Menu";
        public const string APP_TITLE = "TMMIN Portal";
        public const string __WEB_SERVICE_USER = "__ws_user";
        public const string __CRED_P = "__cred_p"; 
    }
}
