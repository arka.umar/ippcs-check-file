﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MVC
{
    [AttributeUsage(AttributeTargets.Method)]
    public class SkipUnlockActionAttribute : Attribute
    { }
}
