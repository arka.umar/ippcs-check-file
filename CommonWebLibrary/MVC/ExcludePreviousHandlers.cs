﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC.Filter;

namespace Toyota.Common.Web.MVC
{
    [Serializable]
    public class ExcludePreviousHandlers
    {
        private List<ActionExecuting> excludeAction;

        public ExcludePreviousHandlers()
        {
            excludeAction = new List<ActionExecuting>();
            List<string> actionLists = AttributeSearch.FindSpecificMethodAttribute(typeof(BaseController), typeof(SkipUnlockActionAttribute), true);
            if (actionLists != null)
            { 
                foreach (string name in actionLists)
                {
                    string[] nm = name.Split('.');
                    excludeAction.Add(new ActionExecuting() { Controller = nm[nm.Length - 2].Replace("Controller", ""), Action = nm[nm.Length - 1] });
                }
            }
        }

        public bool Contains(ActionExecuting item)
        { return excludeAction.Contains(item); }

        public bool Contains(Func<ActionExecuting, string> selector, string value)
        { return excludeAction.Select(selector).Contains(value); }

        [Obsolete("Replace with Contains(Func<ActionExecuting, string>, string value)")]
        public bool Contains(string Action)
        { return excludeAction.Select(p => p.Action).Contains(Action); }
       
    }
}
