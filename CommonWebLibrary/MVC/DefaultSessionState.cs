﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;
using System.Web;

namespace Toyota.Common.Web.MVC
{
    [Serializable]
    public class DefaultSessionState: ISessionState
    {
        private User user;
        private int timeout;
        private bool securedUrlModification;
        private bool autoGenerateMenu; 
        private bool unlockState; 

        public DefaultSessionState()
        {
            autoGenerateMenu = true;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public User GetAuthorizedUser()
        {
            return user;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void SetAuthorizedUser(User user)
        { 
           this.user = user;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public bool HasAuthorizedUser()
        {
            return (user != null);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public int GetTimeout()
        {
            return timeout;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void SetTimeout(int timeout)
        {
            this.timeout = timeout;
        }
        
        public void EnableSecuredUrlModification(bool enable)
        {
            this.securedUrlModification = enable;
        }

        public bool IsSecuredUrlModificationEnabled()
        {
            return securedUrlModification;
        }

        public void EnableMenuAutoGeneration(bool autoGenerate)
        {
            this.autoGenerateMenu = autoGenerate;
        }

        public bool IsMenuAutoGenerationEnabled()
        {
            return autoGenerateMenu;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void SetUnlockState(bool state)
        {
            unlockState = state;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public bool GetUnlockState()
        {
            return unlockState;
        }
          
    }
}
