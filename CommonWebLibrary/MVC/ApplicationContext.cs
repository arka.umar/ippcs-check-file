﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Toyota.Common.Web.MVC
{
    public class ApplicationContext
    {
        private static ApplicationContext instance = null;
        private string deploymentContext;
        private string defaultUnlockTimeout;

        private string unlockFilter;
        private string handlingErrorFilter;
        private string authFilter;

        private ApplicationContext() { }

        public static ApplicationContext GetInstance()
        {
            if (instance == null)
            {
                instance = new ApplicationContext();
            }
            return instance;
        }

        public string GetDeploymentContext()
        {
            if (string.IsNullOrEmpty(deploymentContext))
            {
                deploymentContext = ConfigurationManager.AppSettings["DeploymentContext"];                
            }
            return deploymentContext;
        }

        public string GetDefaultUnlockTimeout()
        {
            if (string.IsNullOrEmpty(defaultUnlockTimeout))
            {
                defaultUnlockTimeout = ConfigurationManager.AppSettings["DefaultUnlockTimeout"];
            }
            return defaultUnlockTimeout;
        }

        public bool GetUnlockFilterState()
        {
            bool ret = false;
            if (string.IsNullOrEmpty(unlockFilter))
            {
                unlockFilter = ConfigurationManager.AppSettings["UnlockFilterState"];
                if (unlockFilter == null) { unlockFilter = "false"; return false; }
                else
                { if (unlockFilter.Equals("true")) ret = true; }
            }
            else
            { if (unlockFilter.Equals("true")) ret = true; }
            return ret;
        }

        public bool GetHandlingErrorFilterState()
        {
            bool ret = false;
            if (string.IsNullOrEmpty(handlingErrorFilter))
            {
                handlingErrorFilter = ConfigurationManager.AppSettings["HandlingErrorFilterState"];
                if (handlingErrorFilter == null) { handlingErrorFilter = "false"; return false; }
                else
                { if (handlingErrorFilter.Equals("true")) ret = true; }
            }
            else
            { if (handlingErrorFilter.Equals("true")) ret = true; }
            return ret;
        }

        public bool GetAuthFilterState()
        {
            bool ret = false;
            if (string.IsNullOrEmpty(authFilter))
            {
                authFilter = ConfigurationManager.AppSettings["AuthFilterState"];
                if (authFilter == null) { authFilter = "false"; return false; }
                else
                { if (authFilter.Equals("true")) ret = true; }
            }
            else
            {  if (authFilter.Equals("true")) ret = true; }
            return ret;
        }
         
    }
}
