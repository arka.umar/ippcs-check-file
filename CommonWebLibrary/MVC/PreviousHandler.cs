﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Web.Mvc;

namespace Toyota.Common.Web.MVC
{
    [Serializable]
    public class PreviousHandler
    {
        private const int defaultMaxItem = 5;
        private List<ActionExecuting> previousList;
        private static ExcludePreviousHandlers excludeHandlers;

        public int MaxItem { get; set; }
        public TimeSpan LastAction { get; set; }
        public int Count { get { return previousList.Count; } }

        public PreviousHandler()
        {
            previousList = new List<ActionExecuting>();
            MaxItem = defaultMaxItem;
            if (excludeHandlers == null)
            { excludeHandlers = new ExcludePreviousHandlers(); }
        }

        /// <summary>
        /// Get recents action
        /// </summary>
        /// <param name="index">Last history is index 0</param>        
        public ActionExecuting Get(int index)
        {
            if (index >= previousList.Count) return null;
            return previousList[(previousList.Count - 1) - index];
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void SetLastActionTimeSpan()
        { 
            this.LastAction = new TimeSpan(DateTime.Now.Ticks);
        }
         
        public void Add(ActionExecuting item)
        {  
            if (!excludeHandlers.Contains(p => p.Action, item.Action))
            {
                previousList.Add(item);
                LastAction = new TimeSpan(DateTime.Now.Ticks);
                if (previousList.Count > MaxItem)
                {
                    if (previousList.Count > MaxItem + 1)
                    {
                        previousList.RemoveAt(0);
                    }
                }
            }  
        }

    }
}
