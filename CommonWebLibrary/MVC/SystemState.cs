﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MVC
{
    public class SystemState
    {
        private static SystemState instance = null;

        private SystemState() {
            EnableMenuAutoGeneration = true;
        }

        public static SystemState GetInstance() {
            if (instance == null)
            {
                instance = new SystemState();
            }

            return instance;
        }

        public bool EnableSecuredUrlModification { set; get; }
        public bool EnableMenuAutoGeneration { set; get; }
    }
}
