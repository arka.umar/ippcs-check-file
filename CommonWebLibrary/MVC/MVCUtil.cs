﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Toyota.Common.Web.Html;
using Toyota.Common.Web.Html.Helper;

namespace Toyota.Common.Web.MVC
{
    public class MVCUtil
    {
        public static Dictionary<string, string> UnpackFormCollection(FormCollection formCollection, IHtmlComponent[] components)
        {
            Dictionary<string, string> dataMap = new Dictionary<string, string>();
            string id;
            foreach (IHtmlComponent h in components)
            {
                id = HtmlComponentHelper.GenerateComponentId(h);
                dataMap.Add(h.GetId(), formCollection.Get(id));
            }

            return dataMap;
        }
    }
}
