﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MVC
{
    [Serializable]
    public class SelectableDataModel
    {
        public const char SELECTED_KEYS_SEPARATOR = ';';

        public bool Selected { set; get; }
        public virtual string SelectedKey 
        {            
            get
            {
                return String.Empty;
            }
        }

        public virtual string SelectedKeyValue
        {
            get
            {
                return String.Empty;
            }
        }
    }
}
