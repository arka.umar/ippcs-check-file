﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Html;

namespace Toyota.Common.Web.MVC
{
    public class LayoutPageModel
    {
        private Dictionary<string, HtmlArea> areas;

        public LayoutPageModel()
        {
            areas = new Dictionary<string,HtmlArea>();
        }

        public void RegisterArea(HtmlArea area)
        {
            string name = area.GetName();
            if (!areas.ContainsKey(name))
            {
                areas.Add(name, area);
            }
            else
            {
                areas[name] = area;
            }
        }

        public void RemoveArea(string name)
        {
            if (areas.ContainsKey(name))
            {
                areas.Remove(name);
            }
        }

        public void AddControl(string areaName, IHtmlComponent control)
        {
            if (areas.ContainsKey(areaName))
            {
                HtmlArea area = areas[areaName];
                area.AddControl(control);
            }
        }

        public IHtmlComponent GetControl(string areaName, string Id)
        {
            if (areas.ContainsKey(areaName))
            {
                return areas[areaName].GetControl(Id);
            }
            return null;
        }

        public IHtmlComponent GetControl(string areaName)
        {
            if (areas.ContainsKey(areaName))
            {
                IHtmlComponent[] components = areas[areaName].GetControls();
                if (components != null)
                {
                    return components[0];
                }
                return null;
            }
            return null;
        }

        public IHtmlComponent[] GetControls(string areaName)
        {
            if (areas.ContainsKey(areaName))
            {
                return areas[areaName].GetControls();
            }
            return null;
        }
    }
}
