﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Util;
using System.Reflection;
using System.Runtime.Remoting;
using System.Web;
using Newtonsoft.Json;
using Toyota.Common.Util.Text;
using System.IO;

namespace Toyota.Common.Web.MVC.Login
{
    public class OpenLDAPLoginValidator : ILoginValidator
    {
        public User Validate(HttpSessionStateBase session, HttpRequestBase request, IDictionary<string, object> parameters)
        {
            if (parameters == null || parameters.Count == 0)
            {
                return null;
            }
            string Username = parameters["Username"].ToString();
            string Password = parameters["Password"].ToString();
            string BranchID = parameters["BranchID"].ToString();

            if (string.IsNullOrEmpty(Username) && string.IsNullOrEmpty(Password) && string.IsNullOrEmpty(BranchID))
                throw new ArgumentNullException("Username, Password and BranchID.");

            OpenLDAPService.SecurityCenterClient svc = new OpenLDAPService.SecurityCenterClient();
            svc.Open();
            string userRole = svc.getUserRole(Username, "IPPCS");
            string userInfo = svc.getUserInfo(Username, Password);
            svc.Close();
            User user = null;
            if (!string.IsNullOrEmpty(userInfo))
            {
                /* @Lufty: Dicomment dulu karena CPU ngamuk
                user = JsonConvert.DeserializeObject<User>(result);*/
                Dictionary<string, List<string>> dataMapInfo = JSON.ToObject<Dictionary<string, List<string>>>(userInfo);
                user = User.Cast<User>(dataMapInfo);
                if (!string.IsNullOrEmpty(userRole))
                {
                    List<UserAD> dataMapRole = JsonConvert.DeserializeObject<List<UserAD>>(userRole);
                    user.Authorization = new List<Authorization>();
                    foreach (UserAD props in dataMapRole)
                    {
                        if (props.properties.Keys.Contains("authorizationdetails", StringComparer.InvariantCultureIgnoreCase)  )
                        {
                            Authorization auth = User.Cast<Authorization>(props.properties);
                            user.Authorization.Add(auth);
                        }
                    }
                }
            } 
            return user;
        }            
             
    }
}
