﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;
using System.Web;
using Toyota.Common.Lookup;

namespace Toyota.Common.Web.MVC.Login
{
    public interface ILoginValidator
    {
        User Validate(HttpSessionStateBase session, HttpRequestBase request, IDictionary<string, object> parameters);
    }
}
