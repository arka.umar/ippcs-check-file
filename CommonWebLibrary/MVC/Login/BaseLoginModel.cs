﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Toyota.Common.Web.MVC.Login
{
    public class BaseLoginModel
    {        
        public string Username { set; get; }
        public string Password { set; get; }
    }
}
