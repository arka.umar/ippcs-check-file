﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Web.Ioc;
using System.Web;

namespace Toyota.Common.Web.MVC.Login
{
    public class TmminRoleLoginValidator: ILoginValidator
    {
        public User Validate(HttpSessionStateBase session, HttpRequestBase request, IDictionary<string, object> parameters)
        {
            if (parameters == null)
            {
                return null;
            }

            string username = parameters.ContainsKey("Username") ? (string) parameters["Username"] : null;
            string sessionID = parameters.ContainsKey("SessionID") ? (string)parameters["SessionID"] : "Unknown";
            string browserName = parameters.ContainsKey("Browser.Name") ? (string)parameters["Browser.Name"] : "Unknown";
            string browserVersion = parameters.ContainsKey("Browser.Version") ? (string)parameters["Browser.Version"] : "Unknown";

            //User user = UserProvider.GetInstance().GetUser(username);
            User user = new User() { 
                Username = "fid.goldy",
                FullName = "Goldy",
                //UnlockTimeOut = 1
            };
            
            if (user != null)
            {
                //int timeOut = user.RoleSessionTimeOut * 60; //configure at web config
                if (user.Role != null)
                {
                    if (user.Role.Count > 0)
                    {
                        /// --- timeOut value is in second--- \\\
                        //timeOut = (from s in user.Role select s.SessionTimeout).Max(); //configure at web config
                    }
                }
                //user.RoleSessionTimeOut = Convert.ToInt32(Math.Round((double) (timeOut / 60), MidpointRounding.ToEven));
                /// --- Update Login ---\\\
                /// --- Update QUOTA on table TB_M_ROLE_USER --- \\\
                //IDBContextManager dbManager = ProviderResolver.GetInstance().Get<IDBContextManager>();
                //IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
                //dbContext.Execute("UpdateLogin", new object[] { user.Username });
                ///// --- Update Login History --- \\\
                //System.Web.HttpContext context = System.Web.HttpContext.Current;
                //string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                //if (String.IsNullOrEmpty(ipAddress))
                //{
                //    ipAddress = context.Request.ServerVariables["REMOTE_ADDR"];
                //}
                //int ret = dbContext.Execute("UpdateLoginHistory", new object[] { user.Username, sessionID, ipAddress, (browserName + " " + browserVersion) });
                //dbContext.Close();
            }

            return user;

        }
    }
}
