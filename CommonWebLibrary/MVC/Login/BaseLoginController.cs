﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Ioc;
using System.Web.Routing;
using System.Configuration;
using System.Web;

namespace Toyota.Common.Web.MVC.Login
{
    public abstract class BaseLoginController : BaseController
    {
        public BaseLoginController() : base("Login")
        {
            CheckSessionState = false;
            AuthorizedLandingControllerName = "Dashboard";
            LogoutLandingControllerName = "Home";
            CloseWindowAfterLogout = false;
            SupportSingleSignOn = false;
            Model.AddModel(new BaseLoginModel()
            {
                Username = String.Empty,
                Password = String.Empty
            });
        }

        public string AuthorizedLandingControllerName { set; get; }
        public string LogoutLandingControllerName { set; get; }
        public bool CloseWindowAfterLogout { set; get; }
        public bool SupportSingleSignOn { set; get; }
        public string AuthorizedLandingUrl { set; get; }

        [HttpPost]
        public ActionResult SubmitLogin(string Username, string Password)
        {
            bool authorized = false;

            ILoginValidator loginValidator = ProviderResolver.GetInstance().Get<ILoginValidator>();
            if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("Username", Username);
                parameters.Add("Password", Password);
                parameters.Add("BranchID", "IPPCS");
                parameters.Add("SessionID", Session.SessionID);
                parameters.Add("Browser.Name", Request.Browser.Browser);
                parameters.Add("Browser.Version", Request.Browser.Version);
                 
                User user = loginValidator.Validate(Session, Request, parameters);

                authorized = (user != null);
                if (authorized)
                {
                    //Session.Timeout = user.RoleSessionTimeOut; //configure at web config
                    SessionState.SetAuthorizedUser(user);
                }
            } 
           
            PrepareLogin();
            if (!authorized)
            {
                return RedirectToAction("Index");
            }

            if (string.IsNullOrEmpty(AuthorizedLandingUrl))
            {
                return RedirectToAction("Index", AuthorizedLandingControllerName, Model);
            }
            else
            {
                return Redirect(AuthorizedLandingUrl);
            }
        }

        /// <summary>
        /// Indicates that current user is being logged out from system
        /// </summary>
        public ActionResult Logout()
        {
            bool succeed = logout();
            if (!succeed)
            {
                return null;
            }

            string SSOUrl = ConfigurationManager.AppSettings["SSOLogoutUrl"];
            bool notifySSOApplication = !string.IsNullOrEmpty(SSOUrl) && SupportSingleSignOn;
            if (CloseWindowAfterLogout)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("<script type=\"text/javascript\">");
                if (notifySSOApplication)
                {
                    stringBuilder.AppendLine("  var request = new XMLHttpRequest(); ");
                    stringBuilder.AppendLine("  request.onreadystatechange = function() { }; ");
                    stringBuilder.AppendLine("  request.open('GET', '" + SSOUrl + "', true);");
                    stringBuilder.AppendLine("  request.send(''); ");
                }                
                stringBuilder.AppendLine("  window.close();");
                stringBuilder.AppendLine("</script>");

                return Content(stringBuilder.ToString());
            }
            
            return RedirectToAction("index", LogoutLandingControllerName);
        }

        public string StaticLogout()
        {
            bool succeed = logout();
            return Convert.ToString(succeed);
        }

        private bool logout()
        {
            try
            {
                /// --- inform basecontroller to execute logout with setting logoutstate to true --- \\\
                LogoutState = true;
                PrepareLogout();
                SessionState.SetAuthorizedUser(null);
                HttpContext.Session.Clear();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected virtual void PrepareLogout() { }
        protected virtual void PrepareLogin() { }
         
        public ActionResult RenderLogin()
        {
            Model.AddModel(new BaseLoginModel()
            {
                Username = String.Empty,
                Password = String.Empty
            });
            return PartialView("UserLogin", Model);
        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
        }
    }
}
