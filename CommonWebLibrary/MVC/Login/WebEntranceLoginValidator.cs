﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Toyota.Common.Web.Credential;
using Cryptography;

namespace Toyota.Common.Web.MVC.Login
{
    public class WebEntranceLoginValidator: ILoginValidator
    {
        private const string WEB_ENTRANCE_CODE = "SXdkcvXvFhtjdUhSD6NALuwY";
        private Crypter encrypter;
        private OpenLDAPLoginValidator ldapValidator;

        public WebEntranceLoginValidator()
        {
            encrypter = new Crypter(new ConfigKeySym());
            ldapValidator = new OpenLDAPLoginValidator();
        }

        public User Validate(HttpSessionStateBase session, HttpRequestBase request, IDictionary<string, object> parameters)
        {
            User user = null;
            string encryptedCredential = request["btrck"];
            if (!string.IsNullOrEmpty(encryptedCredential))
            {                
                string[] credentialFracs = encryptedCredential.Split('_');
                if ((credentialFracs != null) && (credentialFracs.Length == 3))
                {
                    string code = encrypter.Decrypt(credentialFracs[0].Replace(" ", "+"));
                    string username = encrypter.Decrypt(credentialFracs[1].Replace(" ", "+"));
                    string password = encrypter.Decrypt(credentialFracs[2].Replace(" ", "+"));
                    if (code.Equals(WEB_ENTRANCE_CODE))
                    {
                        user = ldapValidator.Validate(session, request, new Dictionary<string, object>() { 
                            { "Username", username}, {"Password", password}, { "BranchID", "xtc"}});
                        if (user != null)
                        { session[ResourceSystemNames.__CRED_P] = credentialFracs[2].Replace(" ", "+"); }
                    }
                }
            }
            return user;
        }
    }
}
