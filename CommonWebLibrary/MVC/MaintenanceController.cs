﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Html;
using System.Web.Mvc;
using Toyota.Common.Web.Html.Helper;
using System.Collections.Specialized;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.Http;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Validation;
using ServiceStack.Text;

namespace Toyota.Common.Web.MVC
{
    public abstract class MaintenanceController : BaseController
    {
        public const string AREA_SEARCH_PARAMETER = "__area_SEARCH_PARAMETER";
        public const string AREA_DATAGRID = "__area_DATAGRID";

        public MaintenanceController(string pageTitle)
            : base(pageTitle)
        {
            SetViewName("DefaultMaintenance");
            SearchParameterColumn = 3;
            Model.AddModel(new MaintenanceModel());
            UpdateAction = "Update";
            InsertAction = "Insert";
            DeleteAction = "Delete";
        }

        protected int SearchParameterColumn { set; get; }
        protected string UpdateAction { set; get; }
        protected string InsertAction { set; get; }
        protected string DeleteAction { set; get; }
        protected MappedValue<string, string>[] keyIdMap;

        protected LayoutPageModel LayoutModel
        {
            get
            {
                LayoutPageModel model = Model.GetModel<LayoutPageModel>();
                if (model == null)
                {
                    model = new LayoutPageModel();
                    Model.AddModel(model);
                }

                return model;
            }
        }

        protected override string GetViewName()
        {
            return "LayoutGenerator/" + viewName;
        }

        protected abstract void InitComponents();

        protected override void Init()
        {
            LayoutPageModel model = LayoutModel;
            model.RegisterArea(new HtmlArea(AREA_SEARCH_PARAMETER));
            model.RegisterArea(new HtmlArea(AREA_DATAGRID));
            InitComponents();
            ViewData["SearchParamColumn"] = SearchParameterColumn;
        }

        protected override void StartUp()
        {            
        }

        protected void AddSearchParameter(IHtmlComponent component)
        {
            LayoutModel.AddControl(AREA_SEARCH_PARAMETER, component);
            AddValidationKey(HtmlComponentHelper.GenerateComponentId(component), component.GetId());
        }

        protected void SetDataGrid(IHtmlComponent component)
        {
            LayoutModel.AddControl(AREA_DATAGRID, component);
        }

        protected IHtmlComponent GetDataGrid()
        {
            return LayoutModel.GetControl(AREA_DATAGRID);
        }

        public ActionResult SearchResultGridCallback()
        {
            Dictionary<string, string> paramMap = HttpUtil.PackRequestParameter(Request, null);
            if (paramMap.ContainsKey("Operation"))
            {
                string operation = paramMap["Operation"];
                if (!string.IsNullOrEmpty(operation))
                {
                    if (operation.Equals("Delete"))
                    {
                        if (paramMap.ContainsKey("SelectedKeys"))
                        {
                            string selectedKeyString = paramMap["SelectedKeys"];
                            if (!string.IsNullOrEmpty(selectedKeyString))
                            {
                                string[] fractions = selectedKeyString.Split('/');
                                if ((fractions == null) || (fractions.Length > 0))
                                {
                                    string[] keyFractions;
                                    List<string[]> selectedKeys = new List<string[]>();
                                    foreach (string frac in fractions)
                                    {
                                        keyFractions = frac.Split(SelectableDataModel.SELECTED_KEYS_SEPARATOR);
                                        if ((keyFractions == null) || (keyFractions.Length > 0))
                                        {
                                            selectedKeys.Add(keyFractions);
                                        }
                                    }
                                    PerformBulkDelete(selectedKeys);
                                }
                                else
                                {
                                    string[] keyFractions = selectedKeyString.Split(SelectableDataModel.SELECTED_KEYS_SEPARATOR);
                                    if ((keyFractions == null) || (keyFractions.Length > 0))
                                    {
                                        List<string[]> selectedKeys = new List<string[]>();
                                        selectedKeys.Add(keyFractions);
                                        PerformBulkDelete(selectedKeys);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            OnUpdatingSearchResult(paramMap);
            return PartialView("LayoutGenerator/DefaultMaintenanceDataGrid", Model);
        }
        public ActionResult DetailGridCallback(string keys)
        {
            Dictionary<string, string> keyMap = JsonSerializer.DeserializeFromString<Dictionary<string, string>>(keys);
            OnUpdatingDetailSearchResult(keyMap);
            ViewBag.KeyMap = keyMap;
            ViewBag.ScreenId = ScreenId;
            return PartialView("LayoutGenerator/DefaultMaintenanceDetailDataGrid", Model);
        }
        public ActionResult Update(FormCollection form)
        {
            if (keyIdMap == null)
            {
                HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
                keyIdMap = grid.GetMappedColumnKeyById().ToArray();
            }

            PerformUpdate(HttpUtil.PackMappedRequestParameter(Request, keyIdMap));
            return SearchResultGridCallback();
        }
        public ActionResult Insert(FormCollection form)
        {
            if (keyIdMap == null)
            {
                HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
                keyIdMap = grid.GetMappedColumnKeyById().ToArray();
            }

            PerformInsert(HttpUtil.PackMappedRequestParameter(Request, keyIdMap));
            return SearchResultGridCallback();
        }
        public ActionResult Delete(FormCollection form)
        {
            PerformDelete(HttpUtil.PackRequestParameter(Request));
            return SearchResultGridCallback();
        }

        public ActionResult UpdateDetail(FormCollection form, string keys)
        {
            if (keyIdMap == null)
            {
                HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
                keyIdMap = grid.DetailGrid.GetMappedColumnKeyById().ToArray();
            }

            PerformDetailUpdate(HttpUtil.PackMappedRequestParameter(Request, keyIdMap));
            return DetailGridCallback(keys);
        }
        public ActionResult InsertDetail(FormCollection form, string keys)
        {
            if (keyIdMap == null)
            {
                HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
                keyIdMap = grid.DetailGrid.GetMappedColumnKeyById().ToArray();
            }

            PerformDetailInsert(HttpUtil.PackMappedRequestParameter(Request, keyIdMap));
            return DetailGridCallback(keys);
        }
        public ActionResult DeleteDetail(FormCollection form, string keys)
        {
            PerformDetailDelete(HttpUtil.PackRequestParameter(Request));
            return DetailGridCallback(keys);
        }

        protected abstract void OnUpdatingSearchResult(Dictionary<string, string> requestParams);        
        protected abstract void PerformUpdate(Dictionary<string, string> requestParams);
        protected abstract void PerformDelete(Dictionary<string, string> requestParams);
        protected abstract void PerformBulkDelete(List<string[]> selectedKeys);
        protected abstract void PerformInsert(Dictionary<string, string> requestParams);

        protected abstract void OnUpdatingDetailSearchResult(Dictionary<string, string> requestParams);
        protected abstract void PerformDetailUpdate(Dictionary<string, string> requestParams);
        protected abstract void PerformDetailDelete(Dictionary<string, string> requestParams);
        protected abstract void PerformBulkDetailDelete(List<string[]> selectedKeys);
        protected abstract void PerformDetailInsert(Dictionary<string, string> requestParams);
    }
}
