﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Lookup;
using Toyota.Common.Web.MVC.Login;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Lookup;
using Toyota.Common.Web.MVC.Filter;
using Elmah;
using Toyota.Common.Task;
using Toyota.Common.Task.External;

namespace Toyota.Common.Web.MVC
{
    public abstract class MVCBaseApplication: HttpApplication
    {
         // Modified by : fid.goldy
         // Modified date : 01-07-2013
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new ElmahHandleErrorAttribute());
        }

        public void ErrorMail_Filtering(object sender, ExceptionFilterEventArgs e)
        {
            var httpException = e.Exception as HttpException;
            if (httpException != null && httpException.GetHttpCode() == 404)
            {
                e.Dismiss();
            }
        }
        // End Of Modified by : fid.goldy

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.IgnoreRoute("favicon.ico");

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            // set AppDomainAppID
            //RuntimeManager.Initialize(System.Configuration.ConfigurationManager.AppSettings["DomainIDName"]);

            IBootstrap bootstrap = Bootstrap.GetInstance();
            bootstrap.RegisterProvider<ILookup>(typeof(SimpleLookup), false);
            bootstrap.RegisterProvider<ISessionState>(typeof(DefaultSessionState), false);
            bootstrap.RegisterProvider<ILoginValidator>(typeof(TmminRoleLoginValidator), true);
            bootstrap.RegisterProvider<IDBContextManager>(typeof(PetaPocoContextManager), true);
            bootstrap.RegisterProvider<IApplicationConfiguration>(typeof(DbApplicationConfiguration), true);
            bootstrap.RegisterProvider<IMenuProvider>(typeof(DbMenuProvider), true);

            StartUp();
        }

        protected abstract void StartUp();        
    }
}
