﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Messaging;

namespace Toyota.Common.Web.MVC
{
    public class PageModel
    {
        private Dictionary<string, TCDataTable> dataTables;

        public PageModel()
        {
            dataTables = new Dictionary<string, TCDataTable>();
        }

        public List<MenuItem> MenuItems { set; get; }
        public ScreenMessageStack MessageStack { set; get; }

        public void AddDataTable(TCDataTable dataTable)
        {
            if (dataTable == null)
            {
                return;
            }

            if (dataTables.ContainsKey(dataTable.Name))
            {
                dataTables[dataTable.Name] = dataTable;
            }
            else
            {
                dataTables.Add(dataTable.Name, dataTable);
            }
        }

        public TCDataTable GetDataTable(string name)
        {
            if (dataTables.ContainsKey(name))
            {
                return dataTables[name];
            }
            return null;
        }
    }
}
