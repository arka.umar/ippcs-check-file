﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Drawing;

namespace Toyota.Common.Web.MVC
{
   public class StandardPageLayout
    {
       public StandardPageLayout()
       {
           ResizingGrids = new Dictionary<string, Size>();
           EstimatedGridRowHeightFactor = 100;
       }

       public static byte EstimatedGridRowHeightFactor { set; get; }

       public bool ShowLeftPane { set; get; }
       public bool ShowRightPane { set; get; }
       
       private bool useDockingLeftPane = false;
       public bool UseDockingLeftPane 
       {
           set
           {
               useDockingLeftPane = value;
               ShowLeftPane = useDockingLeftPane;
           }
           get { return useDockingLeftPane; }
       }

       private bool useDockingRightPane = false;
       public bool UseDockingRightPane 
       { 
           set {
            useDockingRightPane = value;
            ShowRightPane = useDockingRightPane;
           }
           get { return useDockingRightPane; }
       }
       
       public bool UseDockingMainPane { set; get; }       
       public bool UseSlidingLeftPane { set; get; }
       public bool UseSlidingRightPane { set; get; }
       public bool UseSlidingTopPane { set; get; }
       public bool UseSlidingBottomPane { set; get; }

       public bool UseMessageBoard { set; get; }
       public bool UseNotificationWall { set; get; }

       public void ShowSidePanes()
       {
           ShowLeftPane = true;
           ShowRightPane = true;
       }
       public void ActivateDockingFeature()
       {
           UseDockingLeftPane = true;
           UseDockingMainPane = true;
           UseDockingRightPane = true;
       }
       public void UseSlidingPanes()
       {
           UseSlidingTopPane = true;
           UseSlidingRightPane = true;
           UseSlidingBottomPane = true;
           UseSlidingLeftPane = true;
       }

       public Dictionary<string, Size> ResizingGrids { private set; get; }
              
       public int ScreenWidth { set; get; }
       public int ScreenHeight { set; get; }

       public void AttachSettings(ViewDataDictionary viewData)
       {
           viewData["ShowLeftPane"] = ShowLeftPane;
           viewData["ShowRightPane"] = ShowRightPane;
           viewData["UseDockingLeftPane"] = UseDockingLeftPane;
           viewData["UseDockingMainPane"] = UseDockingMainPane;
           viewData["UseDockingRightPane"] = UseDockingRightPane;
           viewData["UseSlidingTopPane"] = UseSlidingTopPane;
           viewData["UseSlidingLeftPane"] = UseSlidingLeftPane;
           viewData["UseSlidingBottomPane"] = UseSlidingBottomPane;
           viewData["UseSlidingRightPane"] = UseSlidingRightPane;
           viewData["UseMessageBoard"] = UseMessageBoard;
           viewData["UseNotificationWall"] = UseNotificationWall;
       }
    }
}
