﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Util;
using System.Web.UI.WebControls;

namespace Toyota.Common.Web.Html
{
    public interface IHtmlComponent
    {
        string GetId();
        string GetName();
        HtmlStyle GetStyle();
        void RegisterScript(string name, string script);
        void RegisterScript(HtmlScript script);
        HtmlScript[] GetScripts();
        HtmlScript GetScript(string name);
        void RemoveScript(string name);
        void SetCallback(HtmlCallback callback);
        HtmlCallback GetCallback();
        CompositeValue GetValue();
        void SetWidth(Unit width);
        Unit GetWidth();
        void SetEnabled(bool enabled);
        bool IsEnabled();
    }
}
