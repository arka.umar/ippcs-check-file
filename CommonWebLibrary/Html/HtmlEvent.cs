﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html
{
    public class HtmlEvent
    {
        public string Name { set; get; }
        public HtmlScript Script { set; get; }
    }
}
