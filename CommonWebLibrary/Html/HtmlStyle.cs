﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html
{
    public class HtmlStyle
    {
        private List<string> cssClasses;
        private Dictionary<string, string> cssStyles;

        public HtmlStyle()
        {
            cssClasses = new List<string>();
            cssStyles = new Dictionary<string, string>();
        }

        public void AddClass(string cssClass)
        {
            if (!cssClasses.Contains(cssClass))
            {
                cssClasses.Add(cssClass);
            }
        }

        public void RemoveClass(string cssClass)
        {
            cssClasses.Remove(cssClass);
        }

        public string[] GetClasses()
        {
            if (cssClasses.Count > 0)
            {
                return cssClasses.ToArray();
            }
            return null;
        }

        public void AddStyle(string key, string value)
        {
            if (!cssStyles.ContainsKey(key))
            {
                cssStyles.Add(key, value);
            }
            else
            {
                cssStyles[key] = value;
            }
        }

        public void RemoveStyle(string key)
        {
            if (cssStyles.ContainsKey(key))
            {
                cssStyles.Remove(key);
            }
        }

        override public string ToString()
        {
            if (cssStyles.Count > 0)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (string key in cssStyles.Keys)
                {
                    stringBuilder.Append(string.Format("{0}: {1}; ", key, cssStyles[key]));
                }

                return stringBuilder.ToString();
            }
            return String.Empty;
        }        
    }
}
