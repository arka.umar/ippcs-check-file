﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Toyota.Common.Web.Html
{
    public class HtmlLabelComponent : HtmlComponent
    {
        public HtmlLabelComponent(string id, string name, Type valueType, Unit width)
            : this(id, name, valueType, null, width)
        {
        }

        public HtmlLabelComponent(string id, string name, Type valueType)
            : this(id, name, valueType, null, Unit.Empty)
        {
        }

        public HtmlLabelComponent(string id, string name, Type valueType, object value, Unit width)
            : base(id, name, value, width)
        {

        }
    }
}
