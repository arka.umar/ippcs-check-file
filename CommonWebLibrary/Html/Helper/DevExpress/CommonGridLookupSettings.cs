﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.MVC;
using DevExpress.Utils;
using System.Web.UI;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{

    public class CommonGridLookupSettings
    {   
        public string Name { get; set; }
        public Unit Width { get; set; }
        public Unit Height { get; set; }
        public bool ReadOnly { get; set; }
        public bool Visible { get; set; }
        public bool Enabled { get; set; } 
        public bool ShowDescription { get; set; }
        public string ShowDescriptionText { get; set; }
        public Unit DescriptionWidth { get; set; }
        public bool ClientEnabled { get; set; }
        public CssStyleCollection Style { get; set; }
        public string Text { get; set; }
        public string TextChanged { get; set; }
        public string ValueChanged { get; set; }
        public string BeginCallback { get; set; }
        public string ButtonClick { get; set; }
        public GridSettings GridProperties { get; set; }
        
        public CommonGridLookupSettings()
        {
            GridProperties = new GridSettings();

            this.Enabled = true;
            this.Visible = true;
            this.ClientEnabled = true;
        }

        public class GridSettings : GridViewSettings
        {
            public GridSettings() { }

            public void PerformDefaultSettings()
            {
                Settings.ShowFilterRow = true;
                Settings.ShowFilterRowMenu = true;

                CommandColumn.Visible = true;
                CommandColumn.ClearFilterButton.Visible = true;
                CommandColumn.ShowSelectCheckbox = true;
                CommandColumn.HeaderStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                CommandColumn.Width = System.Web.UI.WebControls.Unit.Pixel(10);
                CommandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                Styles.Header.HorizontalAlign = HorizontalAlign.Center;
                Styles.AlternatingRow.Enabled = DefaultBoolean.True;

                SettingsDetail.AllowOnlyOneMasterRowExpanded = false;

                SettingsPager.PageSizeItemSettings.Visible = true;
                SettingsPager.PageSizeItemSettings.Items = new string[] { "10", "30", "50", "100", "500" };
                SettingsPager.PageSizeItemSettings.Caption = "Page size";
                SettingsPager.FirstPageButton.Visible = true;
                SettingsPager.LastPageButton.Visible = true;

                CustomCallback = (s, e) =>
                {
                    MVCxGridView grid = (MVCxGridView)s;
                    string[] parameters = e.Parameters.Split('&');
                    string[] pair;
                    double pageSize = SettingsPager.PageSize;
                    string key;
                    
                    foreach (string p in parameters)
                    {
                        pair = p.Trim().Split('=');
                        if (pair.Length == 2)
                        {
                            key = pair[0].Trim().ToLower();
                            if (key.Equals("pagesize"))
                            {
                                pageSize = Convert.ToDouble(pair[1].Trim());
                                break;
                            }
                        }
                    }

                    pageSize = Math.Round(pageSize);
                    if (pageSize < 2)
                    {
                        pageSize = 2;
                    }
                    grid.SettingsPager.PageSize = (int)pageSize;
                    grid.Settings.VerticalScrollableHeight = StandardPageLayout.EstimatedGridRowHeightFactor * (int)pageSize;

                    List<int> pageSizeItems = new List<int>();
                    pageSizeItems.AddRange(new int[] { 10, 30, 50, 100, (int)pageSize, 500 });
                    pageSizeItems.Sort();
                    List<string> stPageSizeItems = new List<string>();
                    foreach (int psize in pageSizeItems)
                    {
                        stPageSizeItems.Add(Convert.ToString(psize));
                    }
                    grid.SettingsPager.PageSizeItemSettings.Items = stPageSizeItems.ToArray();

                };
            }

            public Unit Width { get; set; }
            public Unit Height { get; set; }
            public object DataSource { get; set; }

        }
    }
}
