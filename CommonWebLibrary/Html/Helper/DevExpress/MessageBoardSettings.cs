﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MessageBoard;
using System.Web.UI.WebControls;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class MessageBoardSettings
    {
        public MessageBoardSettings()
        {
            Width = Unit.Pixel(350);
            Height = Unit.Pixel(500);
            Position = MessageBoardPosition.RIGHT;
        }

        public string Name { set; get; }
        public string BoardName { set; get; }
        public Unit Width { set; get; }
        public Unit Height { set; get; }
        public byte Position { set; get; }
        public object DataSource { set; get; }
    }
}
