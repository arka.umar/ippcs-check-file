﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class CommonGridViewVisibleColumnSelectorSettings
    {
        public CommonGridViewVisibleColumnSelectorSettings()
        {
            Columns = new Dictionary<string, string>();
        }

        public string Name { set; get; }
        public Dictionary<string, string> Columns { private set; get; }
    }
}
