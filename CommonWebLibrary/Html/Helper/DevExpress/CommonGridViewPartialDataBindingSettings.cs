﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class CommonGridViewPartialDataBindingSettings
    {
        public bool Enabled { set; get; }
        public long PageCount { set; get; }
        public long DataCount { set; get; }
        public long CurrentPage { set; get; }
        public bool CollapsiblePager { set; get; }
        public bool PagerVisible { set; get; }
    }
}
