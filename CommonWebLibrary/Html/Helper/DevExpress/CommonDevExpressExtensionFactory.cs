﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Messaging.Notification;
using System.IO;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class CommonDevExpressExtensionFactory
    {
        public HtmlHelper Helper { set; get; }

        public AjaxProgressBarExtension AjaxProgressBar(Action<AjaxProgressBarSettings> settings)
        {
            AjaxProgressBarSettings commonSettings = new AjaxProgressBarSettings();
            settings.Invoke(commonSettings);           
            
            return new AjaxProgressBarExtension(commonSettings);
        }

        public DockPanelExtension DockingPanel(string name, string title, string zone, Action content)
        {
            DockPanelSettings settings = new DockPanelSettings();
            settings.Name = "dk" + name;
            settings.PanelUID = "dkuid" + name;
            settings.HeaderText = title;
            settings.EnableAnimation = false;
            settings.OwnerZoneUID = zone;
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings.SetContent(content);
            settings.ShowCloseButton = false;
            settings.AllowResize = true;
            DockPanelExtension panel = new DockPanelExtension(settings);
            return panel;
        }

        public CommonGridViewExtension GridView(Action<CommonGridViewSettings> settings)
        {
            CommonGridViewSettings gridSettings = new CommonGridViewSettings();
            settings.Invoke(gridSettings);
            gridSettings.PerformDefaultSettings();
            return new CommonGridViewExtension(Helper,gridSettings);
        }

        public CommonGridViewVisibleColumnSelector VisibleColumnSelector(Action<CommonGridViewVisibleColumnSelectorSettings> settings)
        {
            CommonGridViewVisibleColumnSelectorSettings gridSettings = new CommonGridViewVisibleColumnSelectorSettings();
            settings.Invoke(gridSettings);

            return new CommonGridViewVisibleColumnSelector(gridSettings);
        }

        public CommonGridLookupExtension GridLookup(Action<CommonGridLookupSettings> settings)
        {
            CommonGridLookupSettings gridSettings = new CommonGridLookupSettings();
            settings.Invoke(gridSettings);
            gridSettings.GridProperties.PerformDefaultSettings();
            return new CommonGridLookupExtension(Helper, gridSettings);
        }

        public ButtonExtension MessageBoardOpenButton(Action<MessageBoardOpenButtonSettings> settings)
        {
            MessageBoardOpenButtonSettings buttonSettings = new MessageBoardOpenButtonSettings();
            settings.Invoke(buttonSettings);
            buttonSettings.ClientSideEvents.Click = "function(s,e) { _cmfunc_messageBoard_open('" + buttonSettings.BoardId + "'); }";
            buttonSettings.Images.Image.Url = new UrlHelper(Helper.ViewContext.RequestContext).Content("~/Content/Images/Common/chat-16.png");
            buttonSettings.ControlStyle.CssClass = "message-board-opener";
            return new ButtonExtension(buttonSettings, Helper.ViewContext);
        }
                
        public MessageBoardImageExtension MessageBoardOpenImage(Action<MessageBoardOpenImageSettings> settings) {            
            MessageBoardOpenImageSettings imgSettings = new MessageBoardOpenImageSettings();
            settings.Invoke(imgSettings);
            /*ImageEditSettings cmpSettings = new ImageEditSettings();
            cmpSettings.Name = imgSettings.Name;            
            UrlHelper urlHelper = new UrlHelper(Helper.ViewContext.RequestContext);
            switch (imgSettings.Dimension)
            {
                case ImageDimension.SQUARE_64:
                    cmpSettings.ImageUrl = urlHelper.Content("~/Content/Images/Common/chat-64.png");
                    break;
                case ImageDimension.SQUARE_16:
                    cmpSettings.ImageUrl = urlHelper.Content("~/Content/Images/Common/chat-16.png");
                    break;
                case ImageDimension.SQUARE_32:
                    cmpSettings.ImageUrl = urlHelper.Content("~/Content/Images/Common/chat-32.png");
                    break;
                default:
                    cmpSettings.ImageUrl = urlHelper.Content("~/Content/Images/Common/chat-24.png");
                    break;
            }
            cmpSettings.ControlStyle.Cursor = "pointer";
            cmpSettings.ControlStyle.CssClass = "message-board-opener";
            cmpSettings.Properties.ClientSideEvents.Click = "function(s,e) { __messageBoard_boardName_" + imgSettings.BoardId.Replace(' ','_') + " = '" + imgSettings.BoardName + "'; _cmfunc_messageBoard_reloadMessages('" + imgSettings.BoardId + "','" + imgSettings.BoardName + "'); _cmfunc_messageBoard_open('" + imgSettings.BoardId + "','" + imgSettings.BoardName + "'); }";
            
            return new ImageEditExtension(cmpSettings, Helper.ViewContext);*/

            return new MessageBoardImageExtension(Helper, imgSettings);
        }

        public MessageBoardExtension MessageBoard(Action<MessageBoardSettings> settings)
        {
            MessageBoardSettings msettings = new MessageBoardSettings();
            settings.Invoke(msettings);
            return new MessageBoardExtension(msettings, Helper);
        }

        public FileUploadExtension FileUpload(Action<FileUploadSettings> settings)
        {
            FileUploadSettings fsettings = new FileUploadSettings();
            settings.Invoke(fsettings);

            return new FileUploadExtension(Helper, fsettings);
        }

        public NotificationWallExtension NotificationWall(Action<NotificationWallSettings> settings) {
            NotificationWallSettings wsettings = new NotificationWallSettings();
            settings.Invoke(wsettings);
            return new NotificationWallExtension(Helper, wsettings);
        }
    }
}
