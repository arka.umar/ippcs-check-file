﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.Mvc;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class MessageBoardOpenButtonSettings : ButtonSettings
    {
        public string BoardId { set; get; }
        public string BoardName { set; get; }
    }
}
