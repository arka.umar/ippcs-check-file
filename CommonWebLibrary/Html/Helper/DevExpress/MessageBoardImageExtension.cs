﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class MessageBoardImageExtension
    {
        private HtmlHelper helper;
        private MessageBoardOpenImageSettings settings;

        public MessageBoardImageExtension(HtmlHelper helper, MessageBoardOpenImageSettings settings) {
            this.helper = helper;
            this.settings = settings;
        }

        public MvcHtmlString GetHtml()
        {
            return new MvcHtmlString(GetHtmlCode());
        }

        public void Render()
        {
            helper.ViewContext.Writer.WriteLine(GetHtml());
        }

        private string GetHtmlCode()
        {
            int size;
            switch (settings.Dimension)
            {
                case ImageDimension.SQUARE_128: size = 128; break;
                case ImageDimension.SQUARE_96: size = 96; break;
                case ImageDimension.SQUARE_72: size = 72; break;
                case ImageDimension.SQUARE_64: size = 64; break;
                case ImageDimension.SQUARE_32: size = 32; break;
                case ImageDimension.SQUARE_16: size = 16; break; 
                default: size = 24; break;
            }

            return "<div id=\"" + settings.Name + "\"><img/><span/></div>" +                    
                    "<script id='dxss_" + settings.Name + "' type='text/javascript'>" +
                    "   $('#" + settings.Name + "').commonInboxIcon({" +
                    "       boardName: '" + settings.BoardName + "'," +
                    "       size: " + Convert.ToString(size) + "," +
                    "       iconBaseName: 'message-board'," +
                    "       iconBaseUrl: '" + UrlHelper.GenerateContentUrl("~/Content/Images/Common", new HttpContextWrapper(HttpContext.Current)) + "'," +
                    "       onClicked: " + "function() { __messageBoard_boardName_" + settings.BoardId.Replace(' ', '_') + " = '" + settings.BoardName + "'; _cmfunc_messageBoard_open('" + settings.BoardId + "','" + settings.BoardName + "'); }, " +
                    "       countUnreadMessageOnServer: true," +
                    "       unreadMessageCounterUrl: _cmfunc_getActionUrl('_ajax_MessageBoard_GetUnreadMessageCount')," +
                    "       autoUpdateState: true" +
                    "   });" +
                    "</script>";
                    
        }
    }
}
