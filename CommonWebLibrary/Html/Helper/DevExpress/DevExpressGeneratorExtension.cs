﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Html;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DevExpress.Web.Mvc;
using System.Web;
using DevExpress.Web.Mvc.UI;
using DevExpress.Web.ASPxEditors;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using System.Web.UI;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public static class DevExpressGeneratorHelper
    {
        public static HtmlString cmDxComponent(this HtmlHelper html, IHtmlComponent component)
        {
            return cmRenderDevExpressComponent(component, true);
        }

        public static void cmRenderDxComponent(this HtmlHelper html, IHtmlComponent component)
        {
            cmRenderDevExpressComponent(component, false);
        }

        public static HtmlString cmRenderDevExpressComponent(IHtmlComponent component, bool htmlInstead)
        {
            ExtensionBase extension = cmGetDevExpressComponent(component);
            if (extension != null)
            {
                if (htmlInstead)
                {
                    return extension.GetHtml();
                }

                extension.Render();
            }

            return new HtmlString(String.Empty);
        }

        public static ExtensionBase cmGetDevExpressComponent(this HtmlHelper html, IHtmlComponent component)
        {
            return cmGetDevExpressComponent(component);
        }

        public static ExtensionBase cmGetDevExpressComponent(IHtmlComponent component)
        {
            if (component != null)
            {
                Type type = component.GetType();
                if (type == typeof(HtmlMemoComponent))
                {
                    MemoSettings settings = (MemoSettings) DevExpressUtil.ApplyBaseSettings(new MemoSettings(), component);
                    HtmlMemoComponent memoComponent = (HtmlMemoComponent) component;
                    settings.Properties.Rows = memoComponent.Rows;
                    MemoExtension memo = new MemoExtension(settings);   
                    memo.Bind(component.GetValue().Value);                 
                    return memo;
                }
                else if (type == typeof(HtmlBooleanComponent))
                {
                    CheckBoxSettings settings = (CheckBoxSettings)DevExpressUtil.ApplyBaseSettings(new CheckBoxSettings(), component);         
                    CheckBoxExtension checkbox = new CheckBoxExtension(settings);
                    checkbox.Bind(component.GetValue().Value);
                    return checkbox;
                }
                else if (type == typeof(HtmlButtonComponent))
                {
                    ButtonSettings settings = (ButtonSettings)DevExpressUtil.ApplyBaseSettings(new ButtonSettings(), component);
                    settings.Text = component.GetName();
                    ButtonExtension button = new ButtonExtension(settings);
                    return button;
                }
                else if (type == typeof(HtmlDataGridComponent))
                {
                    GridViewSettings settings = (GridViewSettings)DevExpressUtil.ApplyBaseSettings(new GridViewSettings(), component);                    
                    GridViewExtension gridView = new GridViewExtension(settings);
                    return gridView;
                }
                else if (type == typeof(HtmlGridOptionComponent))
                {

                }
                else if (type == typeof(HtmlHyperlinkComponent))
                {
                    HyperLinkSettings settings = (HyperLinkSettings) DevExpressUtil.ApplyBaseSettings(new HyperLinkSettings(), component); 
                    settings.Properties.Text = component.GetName();
                    HyperLinkExtension hyperlink = new HyperLinkExtension(settings);
                    return hyperlink;
                }
                else if (type == typeof(HtmlLabelComponent))
                {
                    LabelSettings settings = (LabelSettings)DevExpressUtil.ApplyBaseSettings(new LabelSettings(), component);
                    settings.Text = component.GetName();
                    LabelExtension label = new LabelExtension(settings);
                    return label;
                }
                else if (type == typeof(HtmlOptionComponent))
                {
                    HtmlOptionComponent option = (HtmlOptionComponent)component;
                    ComboBoxSettings settings = (ComboBoxSettings)DevExpressUtil.ApplyBaseSettings(new ComboBoxSettings(), component);
                    settings.Properties.TextField = option.TextField;
                    settings.Properties.ValueField = option.ValueField;
                    settings.Properties.ValueType = option.ValueType;
                    ComboBoxExtension comboBox = new ComboBoxExtension(settings);
                    comboBox.BindList(option.Items);
                    return comboBox;
                }
                else if (type == typeof(HtmlTextComponent))
                {
                    TextBoxSettings settings = (TextBoxSettings)DevExpressUtil.ApplyBaseSettings(new TextBoxSettings(), component);                    
                    TextBoxExtension textBox = new TextBoxExtension(settings);
                    textBox.Bind(component.GetValue().Value);
                    return textBox;
                }
                else if (type == typeof(HtmlDateComponent))
                {
                    DateEditSettings settings = (DateEditSettings)DevExpressUtil.ApplyBaseSettings(new DateEditSettings(), component);
                    DateEditExtension dateEdit = new DateEditExtension(settings);
                    dateEdit.Bind(component.GetValue().Value);
                    return dateEdit;
                }
            }
            return null;
        }
        
        public static MvcHtmlString MultipleComboBoxComponent(this HtmlHelper html, 
                                                              string comboName, 
                                                              string comboTabIndex,
                                                              string comboWidth,
                                                              string keyColumns,
                                                              int pagerSize,
                                                              string[] selectedColumns,
                                                              int[] columnsWidth,
                                                              string selectionFunction,
                                                              object callbackObject,
                                                              object dataSource)
        {
            //ChildActionExtensions.RenderAction(html, "controller", "action", params);
            /** set gridView settings **/
            GridViewSettings gridSettings = new GridViewSettings();
            
            gridSettings.Name = "GVInsideDropDown";
            gridSettings.Width = Unit.Pixel(800);
            gridSettings.KeyFieldName = keyColumns;
            gridSettings.Styles.Header.HorizontalAlign = HorizontalAlign.Center;
            gridSettings.CallbackRouteValues = callbackObject;
           

            gridSettings.CommandColumn.ShowSelectCheckbox = true;
            gridSettings.CommandColumn.Name = keyColumns;
            gridSettings.CommandColumn.Visible = true;
            gridSettings.CommandColumn.Width = Unit.Percentage(5);
            gridSettings.CommandColumn.ClearFilterButton.Visible = true;

            gridSettings.ClientSideEvents.SelectionChanged = selectionFunction;
            gridSettings.SettingsPager.PageSize = pagerSize;
            gridSettings.Settings.ShowVerticalScrollBar = true;
            gridSettings.Settings.ShowFilterRow = true;
            gridSettings.Settings.ShowFilterRowMenu = false;
            
            for (int i = 0; i < selectedColumns.Count(); i++)
            {
                gridSettings.Columns.Add(selectedColumns[i], selectedColumns[i], MVCxGridViewColumnType.TextBox).Width = Unit.Percentage(columnsWidth[i]);
            }
            
            /** set dropdown settings **/
            DropDownEditSettings dropDownSettings = new DropDownEditSettings();
            dropDownSettings.Name = comboName;
            dropDownSettings.TabIndex = short.Parse(comboTabIndex);
            dropDownSettings.Width = Unit.Pixel(int.Parse(comboWidth));
            dropDownSettings.Properties.DropDownWindowStyle.BackColor = System.Drawing.Color.FromArgb(0xEDEDED);
            
            //StringBuilder builder = new StringBuilder();
            //List<object> dummy = new List<object>();
            //foreach(object obj in dummy) {
            //   "<td>" + DataBinder.Eval(obj, "name kolom") + "<td>";
            //}
            //dropDownSettings.SetDropDownWindowTemplateContent(
            //    //<Script>
            //    //$("tableid").datatable({
            //    //     "bProcessing": true,
            //    //    "bServerSide": true,
            //    //    "sAjaxSource": callbackObject
            //    // });

                
            //);

               
            dropDownSettings.SetDropDownWindowTemplateContent(c =>
            {
                GridViewExtension grid = new GridViewExtension(gridSettings);
                grid.Bind(dataSource);
                grid.Render();
            });

            DropDownEditExtension dropDown = new DropDownEditExtension(dropDownSettings);           
            return dropDown.GetHtml();
        }

        public static MvcHtmlString comboboxGridComponent(this HtmlHelper html,
                                                          string comboName,
                                                          string comboTabIndex,
                                                          string comboWidth,
                                                          string keyColumns,
                                                          int pagerSize,
                                                          string[] columns,
                                                          int[] columnsWidth,
                                                          string selectionFunction,
                                                          string callbackController,
                                                          string callbackMethod,
                                                          object dataSource)
        {
            /** set dropdown settings **/
            DropDownEditSettings dropDownSettings = new DropDownEditSettings();
            dropDownSettings.Name = comboName;
            dropDownSettings.TabIndex = short.Parse(comboTabIndex);
            dropDownSettings.Width = Unit.Pixel(int.Parse(comboWidth));
            dropDownSettings.Properties.DropDownWindowStyle.BackColor = System.Drawing.Color.FromArgb(0xEDEDED);
            dropDownSettings.SetDropDownWindowTemplateContent(c =>
            {
                StringBuilder tableComponent = new StringBuilder();
                tableComponent.Append(" <script type=\"text/javascript\"> ");
                tableComponent.Append(" 	var oTable; ");
                tableComponent.Append("     var selected = new Array(); ");
                tableComponent.Append("     $(document).ready(function () { ");
                tableComponent.Append("         oTable = $(\"#GVInsideDropDown\").dataTable({ ");
                tableComponent.Append("             \"sPaginationType\": \"full_numbers\", ");
                tableComponent.Append("             \"bJQueryUI\": true, ");
                tableComponent.Append("             \"bProcessing\": true, ");
                tableComponent.Append("             \"bServerSide\": true, ");
                tableComponent.Append("             \"sAjaxSource\": '/DataExch/FilterRowPartial', ");
                tableComponent.Append("             \"fnServerData\": function (sSource, aoData, fnCallback) { ");
                tableComponent.Append("                 $.ajax({ ");
                tableComponent.Append("                     \"dataType\": 'json', ");
                tableComponent.Append("                     \"type\": \"POST\", ");
                tableComponent.Append("                     \"url\": sSource, ");
                tableComponent.Append("                     \"data\": aoData, ");
                tableComponent.Append("                     \"success\": fnCallback ");
                tableComponent.Append("                 }); ");
                tableComponent.Append("             }, ");
                tableComponent.Append("             \"fnRowCallback\": function (nRow, aData, iDisplayIndex) { ");
                tableComponent.Append("                 $('#GVInsideDropDown tbody tr').each(function () { ");
                tableComponent.Append("                     if (jQuery.inArray(aData[0], selected) != -1) { ");
                tableComponent.Append("                         $(this).addClass('row_selected'); ");
                tableComponent.Append("                     } ");
                tableComponent.Append("                 }); ");
                tableComponent.Append("                 return nRow; ");
                tableComponent.Append("             }, ");
                tableComponent.Append("             \"fnDrawCallback\": function (oSettings) { ");
                tableComponent.Append("                 $('#GVInsideDropDown tbody tr').each(function () { ");
                tableComponent.Append("                     var iPos = oTable.fnGetPosition(this); ");
                tableComponent.Append("                     if (iPos != null) { ");
                tableComponent.Append("                         var aData = oTable.fnGetData(iPos); ");
                tableComponent.Append("                         if (jQuery.inArray(aData[0], selected) != -1) ");
                tableComponent.Append("                             $(this).addClass('row_selected'); ");
                tableComponent.Append("                     } ");
                tableComponent.Append("                     $(this).click(function () { ");
                tableComponent.Append("                         var iPos = oTable.fnGetPosition(this); ");
                tableComponent.Append("                         var aData = oTable.fnGetData(iPos); ");
                tableComponent.Append("                         var iId = aData[0]; ");
                tableComponent.Append("                         is_in_array = jQuery.inArray(iId, selected); ");
                tableComponent.Append("                         if (is_in_array == -1) { ");
                tableComponent.Append("                             selected[selected.length] = iId; ");
                tableComponent.Append("                         } ");
                tableComponent.Append("                         else { ");
                tableComponent.Append("                             selected = jQuery.grep(selected, function (value) { ");
                tableComponent.Append("                                 return value != iId; ");
                tableComponent.Append("                             }); ");
                tableComponent.Append("                         } ");
                tableComponent.Append("                         if ($(this).hasClass('row_selected')) { ");
                tableComponent.Append("                             $(this).removeClass('row_selected'); ");
                tableComponent.Append("                         } ");
                tableComponent.Append("                         else { ");
                tableComponent.Append("                             $(this).addClass('row_selected'); ");
                tableComponent.Append("                         } ");
                tableComponent.Append("                         " + comboName + ".SetText(selected.toString()); ");
                tableComponent.Append("                     }); ");
                tableComponent.Append("                 }); ");
                tableComponent.Append("             } ");
                tableComponent.Append("         }); ");
                tableComponent.Append("     }); ");
                tableComponent.Append(" </script> ");
	
                tableComponent.Append(" <table cellpadding=\"0\" cellspacing=\"0\" border=\"1\" class=\"display\" "+
                                      " id=\"GVInsideDropDown\" style=\"width:850px\"> ");
                tableComponent.Append(" <thead> ");
                tableComponent.Append("     <tr> ");
                for (int i = 0; i < columns.Count(); i++)
                {
                    tableComponent.Append("         <th width=\"" + columnsWidth[i] + "%\">" + columns[i] + "</th> ");
                }

                tableComponent.Append("     </tr> ");
                tableComponent.Append(" </thead> ");
                tableComponent.Append(" <tbody> ");
                tableComponent.Append(" </tbody> ");
                tableComponent.Append(" </table> ");

                html.ViewContext.Writer.WriteLine(tableComponent.ToString());
            });

            DropDownEditExtension dropDown = new DropDownEditExtension(dropDownSettings);
            return dropDown.GetHtml();
        }
    }
}
