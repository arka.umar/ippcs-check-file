﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class FileUploadClientSideEvents: ClientSideEvents
    {
        public string UploadRequested { set; get; }
        public string FileUploadComplete { set; get; }
        public string BeforeUpload { set; get; }
        public string TextChanged { set; get; }
    }
}
