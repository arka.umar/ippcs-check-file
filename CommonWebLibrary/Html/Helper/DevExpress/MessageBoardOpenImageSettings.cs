﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class MessageBoardOpenImageSettings
    {
        public MessageBoardOpenImageSettings()
        {
            Dimension = ImageDimension.SQUARE_24;
        }

        public string Name { set; get; }
        public string BoardId { set; get; }
        public string BoardName { set; get; }
        public byte Dimension { set; get; }
    }
}
