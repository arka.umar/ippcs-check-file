﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class AjaxProgressBarSettings
    {
        public string Name { set; get; }
        public Unit Width { set; get; }
        public string ProcessName { set; get; }
    }
}
