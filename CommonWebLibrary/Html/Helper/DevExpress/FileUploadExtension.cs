﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class FileUploadExtension
    {
        private FileUploadSettings settings;
        private HtmlHelper helper;

        public FileUploadExtension(HtmlHelper helper, FileUploadSettings settings)
        {
            this.helper = helper;
            this.settings = settings;            
        }

        

        public MvcHtmlString GetHtml()
        {
            return PartialExtensions.Partial(helper, "FileUpload/FileUpload", settings);
        }

        public void Render()
        {
            helper.ViewContext.Writer.Write(PartialExtensions.Partial(helper, "FileUpload/FileUpload", settings));
        }
    }
}
