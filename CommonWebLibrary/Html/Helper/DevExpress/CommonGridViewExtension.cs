﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.MVC;
using System.Drawing;
using System.IO;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using Toyota.Common.Web.Database;
using DevExpress.Web.Mvc.UI;
using System.Reflection;
using System.Dynamic;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class CommonGridViewExtension
    {
        private CommonGridViewSettings settings;
        private GridViewExtension grid = null;

        public CommonGridViewExtension(HtmlHelper helper, CommonGridViewSettings settings)
        {
            Helper = helper;
            this.settings = settings;
            this.settings.ViewData = Helper.ViewData;
            ViewContext view = Helper.ViewContext;

            if (this.settings.PartialDataBinding.Enabled)
            {
                this.settings.SetPagerBarTemplateContent(c =>
                {
                    string callbackParams = "_grid_Name=" + this.settings.Name +
                                            "&_grid_CurrentPage=" + this.settings.PartialDataBinding.CurrentPage +
                                            "&_grid_PageTotal=" + this.settings.PartialDataBinding.PageCount +
                                            "&_grid_PagerVisible=" + this.settings.PartialDataBinding.PagerVisible +
                                            "&_grid_PageSize=" + this.settings.SettingsPager.PageSize;
                    string pagerControlClickAction = "function(s,e) {{ " + this.settings.Name + ".PerformCallback('_grid_Pagination:Op={0}&" + callbackParams + "'); }}";

                    TextWriter writer = Helper.ViewContext.Writer;
                    writer.WriteLine("<div id=\"" + this.settings.Name + "_pagerTemplate\" style=\"padding: 5px; position: relative;\">");

                    if (this.settings.PartialDataBinding.CollapsiblePager)
                    {
                        ButtonSettings toggleButtonSettings = new ButtonSettings()
                        {
                            Name = this.settings.Name + "_pagerToggleButton",
                            Text = this.settings.PartialDataBinding.PagerVisible ? "Hide Pager" : "Show Pager"
                        };
                        toggleButtonSettings.ClientSideEvents.Click = "function(s,e) { var pagerVisiblityHolder = $('#" + this.settings.Name + "_pagerVisibility'); var " + this.settings.Name + "_pager = $('#" + this.settings.Name + "_pagerTemplate_Content'); if(" + this.settings.Name + "_pager.is(':visible')) { " + this.settings.Name + "_pager.fadeOut('fast', 'swing', function() {}); " + this.settings.Name + "_pagerToggleButton.SetText('Show Pager'); pagerVisiblityHolder.val('" + Convert.ToString(false) + "');} else { " + this.settings.Name + "_pager.fadeIn('fast', 'swing', function() {}); " + this.settings.Name + "_pagerToggleButton.SetText('Hide Pager'); pagerVisiblityHolder.val('" + Convert.ToString(true) + "');} }";
                        ButtonExtension toggleButton = new ButtonExtension(toggleButtonSettings, Helper.ViewContext);
                        toggleButton.Render();
                    }

                    string pagerDisplayStyle = this.settings.PartialDataBinding.CollapsiblePager ? (this.settings.PartialDataBinding.PagerVisible ? String.Empty : "display:none;") + " position: absolute; padding: 5px; bottom: -45px; border: 1px solid #5693E1;" : String.Empty;
                    writer.WriteLine("<div id=\"" + this.settings.Name + "_pagerTemplate_Content\" style=\"" + pagerDisplayStyle + "\">");
                    writer.WriteLine("<table><tr>");
                    writer.WriteLine("<td>");
                    ButtonSettings firstButtonSettings = new ButtonSettings()
                    {
                        Name = this.settings.Name + "_pagerFirstButton",
                        Text = "First"
                    };
                    firstButtonSettings.ClientSideEvents.Click = string.Format(pagerControlClickAction, "FirstPage");
                    ButtonExtension firstButton = new ButtonExtension(firstButtonSettings, Helper.ViewContext);
                    firstButton.Render();
                    writer.WriteLine("</td>");
                    writer.WriteLine("<td>");
                    ButtonSettings prevButtonSettings = new ButtonSettings()
                    {
                        Name = this.settings.Name + "_pagerPrevButton",
                        Text = "Prev"
                    };
                    prevButtonSettings.ClientSideEvents.Click = string.Format(pagerControlClickAction, "PreviousPage");
                    ButtonExtension prevButton = new ButtonExtension(prevButtonSettings, Helper.ViewContext);
                    prevButton.Render();
                    writer.WriteLine("</td>");
                    writer.WriteLine("<td>");
                    writer.WriteLine(string.Format("&nbsp;&nbsp;Page {0} of {1}&nbsp;&nbsp;", settings.PartialDataBinding.CurrentPage + 1, this.settings.PartialDataBinding.PageCount));
                    writer.WriteLine("</td>");
                    writer.WriteLine("<td>");
                    ButtonSettings nextButtonSettings = new ButtonSettings()
                    {
                        Name = this.settings.Name + "_pagerNextButton",
                        Text = "Next"
                    };
                    nextButtonSettings.ClientSideEvents.Click = string.Format(pagerControlClickAction, "NextPage");
                    ButtonExtension nextButton = new ButtonExtension(nextButtonSettings, Helper.ViewContext);
                    nextButton.Render();
                    writer.WriteLine("</td>");
                    writer.WriteLine("<td>");
                    ButtonSettings lastButtonSettings = new ButtonSettings()
                    {
                        Name = this.settings.Name + "_pagerLastButton",
                        Text = "Last"
                    };
                    lastButtonSettings.ClientSideEvents.Click = string.Format(pagerControlClickAction, "LastPage");
                    ButtonExtension lastButton = new ButtonExtension(lastButtonSettings, Helper.ViewContext);
                    lastButton.Render();
                    writer.WriteLine("</td>");
                    writer.WriteLine("</tr></table>");

                    writer.WriteLine("</div>");
                    writer.WriteLine("</div>");
                });
            }
            grid = new GridViewExtension(settings, view);
        }

        public HtmlHelper Helper { set; get; }

        public MvcHtmlString GetHtml()
        {
            StringBuilder outputBuilder = new StringBuilder();
            outputBuilder.AppendLine(string.Format("<div id=\"{0}-container\" style=\"width: 100%;\">", settings.Name));
            outputBuilder.AppendLine("<div><input id=\"" + settings.Name + "_pagerVisibility\" type=\"hidden\" value=\"" + Convert.ToString(settings.PartialDataBinding.PagerVisible) + "\"/></div>");
            outputBuilder.AppendLine("<div>");
            outputBuilder.AppendLine(grid.GetHtml().ToHtmlString());
            outputBuilder.AppendLine("</div>");
            outputBuilder.AppendLine("</div>");
            return new MvcHtmlString(outputBuilder.ToString());
        }

        public void Render()
        {            
            grid.Render();
        }

        public CommonGridViewExtension Bind(object dataObject)
        { 
            grid.Bind(dataObject);
            return this;
        }

        public CommonGridViewExtension Bind(TCDataTable model)
        {
            if (model != null)
            {
                settings.PartialDataBinding.DataCount = model.DataCount;
                settings.PartialDataBinding.PageCount = model.PageCount;
                settings.PartialDataBinding.CurrentPage = model.CurrentPage;
                settings.PartialDataBinding.PagerVisible = model.PagerVisible;

                grid.Bind(model.Data);                
            }            

            return this;
        }
    }
}
