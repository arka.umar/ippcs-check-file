﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class ImageDimension
    {
        public const byte SQUARE_16 = 0;
        public const byte SQUARE_24 = 1;
        public const byte SQUARE_32 = 2;
        public const byte SQUARE_64 = 3;
        public const byte SQUARE_72 = 4;
        public const byte SQUARE_96 = 5;
        public const byte SQUARE_128 = 6;
    }
}
