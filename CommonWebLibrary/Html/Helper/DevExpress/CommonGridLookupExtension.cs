﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.Mvc;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class CommonGridLookupExtension
    {
        private CommonGridLookupSettings settings;

        public CommonGridLookupExtension(HtmlHelper helper, CommonGridLookupSettings settings)
        {
            Helper = helper;
            this.settings = settings;
            this.TempData = Helper.ViewContext.TempData;
            this.ViewData = Helper.ViewData;
            TempData["GridName"] = settings.Name;
            TempData["Grid" + settings.Name] = settings;
        }

        public HtmlHelper Helper { set; get; }

        public TempDataDictionary TempData { get; set; }

        public ViewDataDictionary ViewData { get; set; }
         
        public MvcHtmlString GetHtml()
        {
            MvcHtmlString result = PartialExtensions.Partial(Helper, "GridLookup/Index", settings);
            return result;
        }

        public void Render()
        {
            Helper.ViewContext.Writer.WriteLine(GetHtml());
        }

        public CommonGridLookupExtension Bind(object dataObject)
        {
            settings.GridProperties.DataSource = dataObject;
            return this;
        }
    }
}
