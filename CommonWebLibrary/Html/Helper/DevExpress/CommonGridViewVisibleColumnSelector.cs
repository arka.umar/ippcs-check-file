﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class CommonGridViewVisibleColumnSelector
    {
        private CommonGridViewVisibleColumnSelectorSettings settings;
        public CommonGridViewVisibleColumnSelector(CommonGridViewVisibleColumnSelectorSettings settings)
        {
            this.settings = settings;
        }

        public MvcHtmlString GetHtml()
        {
            StringBuilder stringBuilder = new StringBuilder();
            return new MvcHtmlString(stringBuilder.ToString());
        }
    }
}
