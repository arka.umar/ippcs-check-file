﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.Mvc;
using System.Web.UI.WebControls;
using Toyota.Common.Web.MVC;
using DevExpress.Utils;
using System.Web.Mvc;
using Toyota.Common.Web.Database;
using DevExpress.Web.ASPxGridView;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class CommonGridViewSettings: GridViewSettings
    {        
        public CommonGridViewSettings()
        {
            SettingsPager.PageSize = 10;
            PartialDataBinding = new CommonGridViewPartialDataBindingSettings();
        }

        public ViewDataDictionary ViewData { set; get; }
        public bool OnDemandPageSize { set; get; }
        public CommonGridViewPartialDataBindingSettings PartialDataBinding { set; get; }

        private void RecursifSetAutoFilterCondition(GridViewColumn e)
        {
            if (e.GetType() == typeof(MVCxGridViewColumn))
            {
                ((MVCxGridViewColumn)e).Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            else
            {
                foreach (GridViewColumn col in ((GridViewBandColumn)e).Columns)
                {
                    RecursifSetAutoFilterCondition(col);
                } 
            } 
        }

        public void PerformDefaultSettings()
        {
            Width = Unit.Percentage(100);  
            Styles.Header.HorizontalAlign = HorizontalAlign.Center;
            Styles.AlternatingRow.Enabled = DefaultBoolean.True;            
            
            SettingsDetail.AllowOnlyOneMasterRowExpanded = false;            
            CommandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

            SettingsPager.PageSizeItemSettings.Visible = true;
            if (!OnDemandPageSize)
            {
                SettingsPager.PageSizeItemSettings.Items = new string[] { "10", "30", "50", "100" };                
            }

            SettingsPager.PageSizeItemSettings.Caption = "Page size";
            SettingsPager.FirstPageButton.Visible = true;
            SettingsPager.LastPageButton.Visible = true;

            foreach (GridViewColumn column in Columns)
            {
                RecursifSetAutoFilterCondition(column);
            }

            CustomCallback = (s, e) => {
                MVCxGridView grid = (MVCxGridView)s;
                string[] parameters = e.Parameters.Split('&');
                string[] pair;
                double pageSize = SettingsPager.PageSize;
                string key;
                string argument;
                string[] argumentFractions;
                foreach(string p in parameters) {
                    pair = p.Trim().Split('=');
                    if (pair.Length == 2)
                    {
                        key = pair[0].Trim().ToLower();
                        if (key.Equals("pagesize"))
                        {
                            pageSize = Convert.ToDouble(pair[1].Trim());
                            break;
                        }
                        else if (key.Equals("hidecolumns") || key.Equals("showcolumns") || key.Equals("togglecolumns"))
                        {
                            argument = pair[1].Trim();
                            bool hideColumns = key.Equals("hidecolumns");
                            bool showColumns = key.Equals("showcolumns");
                            bool toggleColumns = key.Equals("togglecolumns");
                            if (!string.IsNullOrEmpty(argument))
                            {
                                argumentFractions = argument.Split(';');
                                object objColumn;
                                foreach (string arg in argumentFractions)
                                {
                                    objColumn = grid.Columns[arg];
                                    if (objColumn == null)
                                    {
                                        continue;
                                    }

                                    if (objColumn.GetType() == typeof(MVCxGridViewColumn))
                                    {
                                        MVCxGridViewColumn column = (MVCxGridViewColumn)objColumn;
                                        if (hideColumns)
                                        {
                                            column.Visible = false;
                                        }
                                        else if (showColumns)
                                        {
                                            column.Visible = true;
                                        } else if (toggleColumns)
                                        {
                                            column.Visible = !column.Visible;
                                        }
                                    }
                                    else if (objColumn.GetType() == typeof(MVCxGridViewBandColumn))
                                    {
                                        MVCxGridViewBandColumn column = (MVCxGridViewBandColumn)objColumn;
                                        if (hideColumns)
                                        {
                                            column.Visible = false;
                                        }
                                        else if (showColumns)
                                        {
                                            column.Visible = true;
                                        }
                                        else if (toggleColumns)
                                        {
                                            column.Visible = !column.Visible;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                pageSize = Math.Round(pageSize);
                if (pageSize < 2)
                {
                    pageSize = 2;
                }
                grid.SettingsPager.PageSize = (int) pageSize;
                grid.Settings.VerticalScrollableHeight = StandardPageLayout.EstimatedGridRowHeightFactor * (int)pageSize;
                if (OnDemandPageSize)
                {
                    List<int> pageSizeItems = new List<int>();
                    pageSizeItems.AddRange(new int[] { 10, 30, 50, 100, (int)pageSize, 500 });
                    pageSizeItems.Sort();
                    List<string> stPageSizeItems = new List<string>();
                    foreach (int psize in pageSizeItems)
                    {
                        stPageSizeItems.Add(Convert.ToString(psize));
                    }
                    grid.SettingsPager.PageSizeItemSettings.Items = stPageSizeItems.ToArray();
                }
            };
        }
    }
}
