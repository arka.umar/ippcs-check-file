﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.Mvc;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class DevExpressUtil
    {
        public static SettingsBase ApplyBaseSettings(SettingsBase settings, IHtmlComponent component)
        {
            settings.Name = HtmlComponentHelper.GenerateComponentId(component);
            settings.Width = component.GetWidth();
            settings.Enabled = component.IsEnabled();
            return settings;
        }
    }
}
