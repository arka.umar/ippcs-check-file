﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class MessageBoardPosition
    {
        public const byte LEFT = 0;
        public const byte RIGHT = 1;
    }
}
