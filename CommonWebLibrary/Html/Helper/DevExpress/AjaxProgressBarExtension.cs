﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.Mvc;
using System.Web.Mvc;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class AjaxProgressBarExtension
    {
        private ProgressBarExtension dxProgressBar;
        private StringBuilder script;

        public AjaxProgressBarExtension(AjaxProgressBarSettings settings)
        {
            ProgressBarSettings dxSettings = new ProgressBarSettings();
            dxSettings.Name = settings.Name;
            dxSettings.Width = settings.Width;
            dxProgressBar = new ProgressBarExtension(dxSettings);

            script = new StringBuilder();
            script.Append("<script type=\"text/javascript\">");
            script.Append("     GetProgressManager().Register(new Progress(\"" + settings.ProcessName + "\", function(position) { " + dxSettings.Name + ".SetPosition(position); }))");
            script.Append("</script>\n");
        }

        public MvcHtmlString GetHtml()
        {
            
            return new MvcHtmlString(script.ToString() + dxProgressBar.GetHtml().ToHtmlString());
        }
    }
}
