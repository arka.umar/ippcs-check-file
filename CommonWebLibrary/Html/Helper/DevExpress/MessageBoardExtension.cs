﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class MessageBoardExtension
    {
        private MessageBoardSettings settings;
        private HtmlHelper helper;

        public MessageBoardExtension(MessageBoardSettings settings, HtmlHelper helper)
        {
            this.settings = settings;
            this.helper = helper;
        }

        public MvcHtmlString GetHtml()
        {
            settings.DataSource = UserProvider.GetInstance().GetUsers();
            return PartialExtensions.Partial(helper, "MessageBoard/MessageBoard", settings);
        }

        public void Render()
        {
            helper.ViewContext.Writer.WriteLine(GetHtml());
        }
    }
}
