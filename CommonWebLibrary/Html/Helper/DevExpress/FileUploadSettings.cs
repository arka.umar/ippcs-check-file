﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Upload;

namespace Toyota.Common.Web.Html.Helper.DevExpress
{
    public class FileUploadSettings
    {
        public FileUploadSettings() 
        {
            ClientSideEvents = new FileUploadClientSideEvents();
            Category = new FileUploadCategory();
        }

        public string Name { set; get; }
        public Unit Width { set; get; }
        public bool ManualUploadTrigger { set; get; }
        public FileUploadClientSideEvents ClientSideEvents { private set; get; }
        public FileUploadCategory Category { private set; get; }
    }
}
