﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Toyota.Common.Web.Html.Helper.DevExpress;
using System.Web.Routing;
using System.Web.Mvc.Html;

namespace Toyota.Common.Web.Html.Helper
{
    public class CommonExtensionFactory
    {
        private readonly CommonDevExpressExtensionFactory dxFactory = new CommonDevExpressExtensionFactory();

        public CommonDevExpressExtensionFactory DevExpress()
        {
            return dxFactory;
        }

        private HtmlHelper helper;
        public HtmlHelper Helper 
        {
            set {
                helper = value;
                dxFactory.Helper = helper;
            }
            get
            {
                return helper;
            }
        }

        public void LoginBox(string controllerName)
        {
            ChildActionExtensions.RenderAction(Helper, "RenderLogin", controllerName);
        }
    }
}
