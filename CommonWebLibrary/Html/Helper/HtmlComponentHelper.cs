﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Toyota.Common.Web.Html.Helper
{
    public static class HtmlComponentHelper
    {
        private static readonly CommonExtensionFactory commonFactory = new CommonExtensionFactory();
        public static CommonExtensionFactory Common(this HtmlHelper html)
        {
            commonFactory.Helper = html;
            return commonFactory;
        }

        public static string cmComponentId(this HtmlHelper html, IHtmlComponent component)
        {
            if (component == null)
            {
                return String.Empty;
            }
            return cmComponentId(html, component.GetId(), component.GetType());
        }

        public static string cmComponentId(this HtmlHelper html, string Id, Type type)
        {
            return GenerateComponentId(Id, type);
        }

        public static string GenerateComponentId(IHtmlComponent component)
        {
            return GenerateComponentId(component.GetId(), component.GetType());
        }

        public static string GenerateComponentId(string Id, Type type)
        {
            if (!string.IsNullOrEmpty(Id))
            {
                string prefix = null;

                if (type == typeof(HtmlMemoComponent))
                {
                    prefix = "mo";
                }
                else if (type == typeof(HtmlBooleanComponent))
                {
                    prefix = "cbox";
                }
                else if (type == typeof(HtmlButtonComponent))
                {
                    prefix = "bt";
                }
                else if (type == typeof(HtmlDataGridComponent))
                {
                    prefix = "gr";
                }
                else if (type == typeof(HtmlGridOptionComponent))
                {
                    prefix = "gropt";
                }
                else if (type == typeof(HtmlHyperlinkComponent))
                {
                    prefix = "lnk";
                }
                else if (type == typeof(HtmlLabelComponent))
                {
                    prefix = "lbl";
                }
                else if (type == typeof(HtmlOptionComponent))
                {                    
                    prefix = "ddl";
                }
                else if (type == typeof(HtmlTextComponent))
                {
                    prefix = "txt";
                }
                else if (type == typeof(HtmlImageComponent))
                {
                    prefix = "img";
                }

                if (!string.IsNullOrEmpty(prefix))
                {
                    string ID = char.ToUpper(Id[0]) + Id.Substring(1);
                    return prefix + ID;
                }
            }
            return Id;
        }
    }
}
