﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Util;
using System.Web.UI.WebControls;

namespace Toyota.Common.Web.Html
{
    public class HtmlOptionComponent: HtmlComponent
    {
        public HtmlOptionComponent(string id, string name,Type valueType, Unit width)
            : this(id, name, valueType, null, width)
        {
        }

        public HtmlOptionComponent(string id, string name, Type valueType)
            : this(id, name, valueType, null, Unit.Empty)
        {
        } 

        public HtmlOptionComponent(string id, string name, Type valueType, object value, Unit width): base(id, name, value, width)
        {
            
        }

        public object Items { set; get; }
        public Type ValueType { set; get; }
        public String TextField { set; get; }
        public String ValueField { set; get; }
    }
}
