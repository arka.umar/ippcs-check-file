﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Toyota.Common.Web.Html
{
    public class HtmlUploadComponent: HtmlComponent
    {
        public const string MIME_IMAGE = "img";
        public const string MIME_PDF = "pdf";
        public const string MIME_TEXT = "txt";
        public const string MIME_MICROSOFT_EXCEL = "xls";
        public const string MIME_MICROSOFT_WORD = "doc";
        public const string MIME_MICROSOFT_POWERPOINT = "ppt";
        public const string MIME_UNKNOWN = "??";

        public HtmlUploadComponent(string id, string name, Type valueType, Unit width)
            : this(id, name, valueType, null, width)
        {
        }

        public HtmlUploadComponent(string id, string name, Type valueType)
            : this(id, name, valueType, null, Unit.Empty)
        {
        }

        public HtmlUploadComponent(string id, string name, Type valueType, object value, Unit width)
            : base(id, name, value, width)
        {

        }
        public string MimeType { set; get; }
    }
}
