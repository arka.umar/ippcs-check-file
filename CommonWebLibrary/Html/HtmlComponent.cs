﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Util;
using System.Web.UI.WebControls;

namespace Toyota.Common.Web.Html
{
    public class HtmlComponent : IHtmlComponent
    {
        protected string id;
        protected string name;
        private HtmlStyle style;
        private Dictionary<string, HtmlScript> scripts;
        private HtmlCallback callback;
        private CompositeValue value;
        private Unit width;
        private bool enabled;

        public HtmlComponent(string Id, string name, Unit width)
            : this(Id, name, null, width)
        {
        }

        public HtmlComponent(string Id, string name)
            : this(Id, name, null, Unit.Empty)
        {
        }

        public HtmlComponent(string Id, string name, object value, Unit width)
        {
            this.id = Id;
            this.name = name;
            this.style = new HtmlStyle();
            this.value = new CompositeValue();
            if (value != null)
            {
                this.value.Value = value;
            }
            this.value.Value = value;
            this.width = width != null ? width : 150;
            this.scripts = new Dictionary<string, HtmlScript>();
            this.enabled = true;
        }

        public string GetId()
        {
            return id;
        }

        public string GetName()
        {
            return name;
        }

        public HtmlStyle GetStyle()
        {
            return style;
        }

        public void RegisterScript(string name, string script)
        {
            RegisterScript(new HtmlScript()
            {
                Name = name,
                Script = script
            });
        }

        public void RegisterScript(HtmlScript script)
        {
            string name = script.Name;
            if (!scripts.ContainsKey(name))
            {
                scripts.Add(name, script);
            }
            else
            {
                scripts[name] = script;
            }
        }

        public HtmlScript[] GetScripts()
        {
            if (scripts.Count > 0)
            {
                return scripts.Values.ToArray<HtmlScript>();
            }
            return null;
        }

        public HtmlScript GetScript(string name)
        {
            if (scripts.ContainsKey(name))
            {
                return scripts[name];
            }
            return null;
        }

        public void RemoveScript(string name)
        {
            if (scripts.ContainsKey(name))
            {
                scripts.Remove(name);
            }
        }

        public void SetCallback(HtmlCallback callback)
        {
            this.callback = callback;
        }

        public HtmlCallback GetCallback()
        {
            return callback;
        }

        public CompositeValue GetValue()
        {
            return value;
        }

        public void SetWidth(Unit width)
        {
            this.width = width;
        }

        public Unit GetWidth()
        {
            return width;
        }

        public void SetEnabled(bool enabled)
        {
            this.enabled = enabled;
        }

        public bool IsEnabled()
        {
            return enabled;
        }
    }
}
