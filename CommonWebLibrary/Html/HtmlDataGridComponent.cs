﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Html.Helper.DevExpress;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Html.Helper;

namespace Toyota.Common.Web.Html
{
    public class HtmlDataGridComponent : HtmlComponent
    {
        private IEnumerable<object> dataSource;

        public HtmlDataGridComponent(string id, string name, Type valueType, Unit width)
            : this(id, name, valueType, null, width)
        {
        }

        public HtmlDataGridComponent(string id, string name, Type valueType)
            : this(id, name, valueType, null, Unit.Empty)
        {
        }

        public HtmlDataGridComponent(string id, string name, Type valueType, object value, Unit width)
            : base(id, name, value, width)
        {
            Columns = new List<IHtmlComponent>();
            PageSelections = new List<int>();
        }

        public List<IHtmlComponent> Columns { private set; get; }
        public IEnumerable<object> DataSource;
        public string KeyFieldName { set; get; }

        public List<int> PageSelections { private set; get; }
        public void AddPageSelection(params int[] pageSelections)
        {
            foreach (int page in pageSelections)
            {
                if (!PageSelections.Contains(page))
                {
                    PageSelections.Add(page);
                }
            }
        }

        public HtmlDataGridComponent DetailGrid { set; get; }

        public List<MappedValue<string,string>> GetMappedColumnKeyById()
        {
            List<MappedValue<string, string>> columnIds = new List<MappedValue<string, string>>();
            foreach (IHtmlComponent component in Columns)
            {
                columnIds.Add(new MappedValue<string, string>()
                {
                    Key = HtmlComponentHelper.GenerateComponentId(component),
                    Value = component.GetId()
                });
            }

            return columnIds;
        }
    }
}
