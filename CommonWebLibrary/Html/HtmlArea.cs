﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html
{
    public class HtmlArea
    {
        private string Id;
        private HtmlStyle style;
        private string name;
        private List<IHtmlComponent> controls;

        public HtmlArea(string name)
        {
            this.name = name;
            style = new HtmlStyle();
            controls = new List<IHtmlComponent>();
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }

        public void AddControl(IHtmlComponent control)
        {
            if (!controls.Contains(control))
            {
                controls.Add(control);
            }
        }

        public IHtmlComponent GetControl(string Id)
        {
            foreach (IHtmlComponent control in controls)
            {
                if (control.GetId().Equals(Id))
                {
                    return control;
                }
            }
            return null;
        }

        public void RemoveControl(string Id)
        {
            IHtmlComponent deleted = null;
            foreach (IHtmlComponent control in controls)
            {
                if (control.GetId().Equals(Id))
                {
                    deleted = control;
                    break;
                }
            }

            if (deleted != null)
            {
                controls.Remove(deleted);
            }
        }

        public IHtmlComponent[] GetControls()
        {
            if (controls.Count > 0)
            {
                return controls.ToArray();
            }
            return null;
        }

        public void SetId(string Id)
        {
            this.Id = Id;
        }

        public string GetId()
        {
            return Id;
        }

        public HtmlStyle GetStyle()
        {
            return style;
        }
    }
}
