﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html
{
    public class HtmlScript
    {
        public HtmlScript()
        {
            ExecuteWhenDocumentReady = false;
        }
        public string Name { set; get; }
        public string Script { set; get; }
        public bool ExecuteWhenDocumentReady { set; get; }
    }
}
