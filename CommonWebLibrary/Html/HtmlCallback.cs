﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Html
{
    public class HtmlCallback
    {
        public HtmlCallback()
        {
            Params = new Dictionary<string,object>();
        }

        public Dictionary<string, object> Params { set; get; }
        public string Controller { set; get; }
        public string Action { set; get; }
    }
}
