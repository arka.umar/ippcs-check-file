﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Util.Event
{
    public class ObservableEvent
    {
        private Dictionary<String, Object> Arguments;

        [System.Diagnostics.DebuggerStepThrough]
        public ObservableEvent(String Name)
        {
            this.Name = Name;
            Arguments = new Dictionary<String, Object>();
        }

        public String Name { set; get; }

        [System.Diagnostics.DebuggerStepThrough]
        public void PutArgument(String ArgumentName, Object ArgumentValue)
        {
            if (string.IsNullOrEmpty(ArgumentName))
            {
                return;
            }
            Arguments.Add(ArgumentName, ArgumentValue);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public Object GetArgument(String ArgumentName)
        {
            if (ArgumentName == null)
            {
                return null;
            }
            return Arguments[ArgumentName];
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void RemoveArgument(String ArgumentName)
        {
            if (string.IsNullOrEmpty(ArgumentName))
            {
                return;
            }
            Arguments.Remove(ArgumentName);
        }
    }
}
