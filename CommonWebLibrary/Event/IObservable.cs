﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Util.Event
{
    public interface IObservable
    {
        void AddObserver(IObserver observer);
        
        void ReleaseObserver(IObserver observer);
        
        void ReleaseAllObserver();

        Boolean HasObserver(IObserver observer);

        void NotifyObserver(ObservableEvent evt); 
    }
}
