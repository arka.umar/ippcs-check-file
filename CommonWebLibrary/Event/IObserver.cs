﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Util.Event
{
    public interface IObserver
    {
        void ObservedObjectChanged(ObservableEvent evt);
    }
}
