﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Util.Event
{
    public class SimpleObservable : IObservable
    {
        private List<IObserver> observers;

        public SimpleObservable()
        {
            observers = new List<IObserver>();
        }

        public void AddObserver(IObserver observer)
        {
            if (observer == null)
            {
                return;
            }

            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
        }

        public void ReleaseObserver(IObserver observer)
        {
            if (observer == null)
            {
                return;
            }
            observers.Remove(observer);
        }

        public void ReleaseAllObserver()
        {
            observers.Clear();
        }

        public bool HasObserver(IObserver observer)
        {
            if (observer == null)
            {
                return false;
            }

            return observers.Contains(observer);
        }

        public void NotifyObserver(ObservableEvent evt)
        {
            if (evt == null)
            {
                return;
            }

            if (observers.Count == 0)
            {
                return;
            }

            for(int i = observers.Count - 1; i >= 0; i--)
            {
                observers[i].ObservedObjectChanged(evt);    
            }
        }
    }
}
