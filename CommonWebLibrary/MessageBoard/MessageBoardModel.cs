﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MessageBoard
{
    public class MessageBoardModel
    {
        public MessageBoardModel()
        {
            MessageBoards = new List<MessageBoard>();
        }

        public List<MessageBoard> MessageBoards { private set; get; }

        public MessageBoard GetMessageBoard(string name)
        {
            foreach(MessageBoard board in MessageBoards) {
                if (board.Name.Equals(name))
                {
                    return board;
                }
            }

            return null;
        }
    }
}
