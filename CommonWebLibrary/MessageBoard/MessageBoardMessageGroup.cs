﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MessageBoard
{
    public class MessageBoardMessageGroup
    {
        public MessageBoardMessageGroup() {
            Attachments = new List<MessageBoardAttachment>();
            Recipients = new List<string>();
            CCRecipients = new List<string>();
        }
        
        public int GroupId { set; get; }
        public String BoardName { set; get; }
        public string Sender { set; get; }
        public List<string> Recipients { private set; get; }
        public DateTime Date { set; get; }
        public List<string> CCRecipients { private set; get; }
        public string DateInString
        {
            set { }
            get
            {
                return string.Format("{0:dd.MM.yy HH:mm:ss}", Date);
            }
        }
        public string Text { set; get; }
        public List<MessageBoardAttachment> Attachments { private set; get; }
    }
}
