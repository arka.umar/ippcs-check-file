﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.MessageBoard
{
    public interface IMessageBoardService
    {        
        void Save(params MessageBoardMessage[] messages);
        List<MessageBoardMessageGroup> List(string boardName);
        List<MessageBoardMessageGroup> ListByUser(string boardName, User user);
        List<MessageBoardAttachment> GetAttachments(string boardName, int messageId);
    }
}
