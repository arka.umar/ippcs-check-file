﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Upload;

namespace Toyota.Common.Web.MessageBoard
{
    public class MessageBoardSessionCache
    {
        private Dictionary<string, FileUploadCategory> uploadCategories;

        public MessageBoardSessionCache()
        {
            uploadCategories = new Dictionary<string, FileUploadCategory>();
        }

        public void SaveUploadCategory(string id, FileUploadCategory category) {
            if (uploadCategories.ContainsKey(id))
            {
                uploadCategories[id] = category;
            }
            else
            {
                uploadCategories.Add(id, category);
            }
        }

        public FileUploadCategory GetUploadCategory(string id) {
            if(uploadCategories.ContainsKey(id)) {
                return uploadCategories[id];
            }
            return null;
        }

        public void RemoveUploadCategory(string id)
        {
            if (uploadCategories.ContainsKey(id))
            {
                uploadCategories.Remove(id);
            }
        }
    }
}
