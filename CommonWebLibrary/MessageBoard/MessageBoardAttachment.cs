﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MessageBoard
{
    public class MessageBoardAttachment
    {
        public long Id { set; get; }
        public int GroupId { set; get; }
        public string BoardName { set; get; }
        public string Description { set; get; }
        public string Path { set; get; }
        public string Url { set; get; }
        public string Filename { set; get; }
    }
}
