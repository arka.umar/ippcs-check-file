﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MessageBoard
{
    public class MessageBoard
    {
        public MessageBoard(string name)
        {
            Messages = new List<MessageBoardMessageGroup>();
            Name = name;
        }

        public List<MessageBoardMessageGroup> Messages { set; get; }
        public string Name { private set; get; }
    }
}
