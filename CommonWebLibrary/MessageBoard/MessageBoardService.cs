﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MessageBoard
{
    public class MessageBoardService
    {
        private static IMessageBoardService instance = new DefaultMessageBoardService();

        public static IMessageBoardService GetInstance() {
            return instance;
        }
    }
}
