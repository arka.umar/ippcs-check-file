﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.FTP;

namespace Toyota.Common.Web.MessageBoard
{
    class DefaultMessageBoardService: IMessageBoardService
    {        
        public DefaultMessageBoardService()
        {         
        }

        public void Save(params MessageBoardMessage[] messages)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            foreach (MessageBoardMessage msg in messages)
            {
                db.Execute("MessageBoard_Insert", new object[] {
                    msg.GroupId ,msg.BoardName, msg.Sender, msg.Recipient, msg.Text, msg.Date, false, msg.CarbonCopy,msg.Sender, DateTime.Now
                });
            }
            db.Close();
        }

        public List<MessageBoardMessageGroup> List(string boardName)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<MessageBoardMessage> messages = db.Fetch<MessageBoardMessage>("MessageBoard_List", new object[] { boardName });
            db.Close();

            return PerformGrouping(messages);
        }

        public List<MessageBoardMessageGroup> ListByUser(string boardName, User user)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<MessageBoardMessage> messages = db.Fetch<MessageBoardMessage>("MessageBoard_ListByUser", new object[] { boardName, user.Username });
            db.Close();

            return PerformGrouping(messages);
        }

        public List<MessageBoardAttachment> GetAttachments(string boardName, int messageId)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<MessageBoardAttachment> attachments = db.Fetch<MessageBoardAttachment>("MessageBoard_GetAttachments", new object[] {boardName, messageId});
            db.Close();
            return (attachments != null) && (attachments.Count > 0) ? attachments : null;
        }

        private List<MessageBoardMessageGroup> PerformGrouping( List<MessageBoardMessage> messages)
        {
            if ((messages != null) && (messages.Count > 0))
            {
                Dictionary<int, List<MessageBoardMessage>> messageMap = new Dictionary<int, List<MessageBoardMessage>>();
                List<MessageBoardMessage> list;
                foreach (MessageBoardMessage msg in messages)
                {
                    if (!messageMap.ContainsKey(msg.GroupId))
                    {
                        messageMap.Add(msg.GroupId, new List<MessageBoardMessage>());
                    }
                    list = messageMap[msg.GroupId];
                    list.Add(msg);
                }

                List<MessageBoardMessageGroup> groupList = new List<MessageBoardMessageGroup>(messages.Count);
                MessageBoardMessageGroup group;
                foreach (int groupId in messageMap.Keys)
                {
                    list = messageMap[groupId];
                    group = new MessageBoardMessageGroup();
                    group.GroupId = groupId;
                    foreach (MessageBoardMessage msg in list)
                    {
                        group.Sender = msg.Sender;
                        group.BoardName = msg.BoardName;
                        group.Date = msg.Date;
                        if (msg.CarbonCopy)
                        {
                            group.CCRecipients.Add(msg.Recipient);
                        }
                        else
                        {
                            group.Recipients.Add(msg.Recipient);
                        }
                        group.Text = msg.Text;
                    }
                    groupList.Add(group);
                }

                return groupList;
            }

            return null;
        }
    }
}
