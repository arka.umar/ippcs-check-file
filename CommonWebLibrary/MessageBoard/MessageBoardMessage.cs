﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.MessageBoard
{
    public class MessageBoardMessage
    {
        public MessageBoardMessage()
        {
            Attachments = new List<MessageBoardAttachment>();
        }

        public long Id { set; get; }
        public int GroupId { set; get; }
        public String BoardName { set; get; }
        public string Sender { set; get; }
        public string Recipient { set; get; }
        public DateTime Date { set; get; }
        public bool CarbonCopy { set; get; }
        public string DateInString
        {
            set { }
            get
            {
                return string.Format("{0:dd.MM.yy HH:mm:ss}", Date);
            }
        }
        public string Text { set; get; }
        public List<MessageBoardAttachment> Attachments { set; get; }
    }
}
