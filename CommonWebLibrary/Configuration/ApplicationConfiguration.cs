﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Ioc;

namespace Toyota.Common.Web.Configuration
{
    public class ApplicationConfiguration
    {
        private readonly static IApplicationConfiguration instance = ProviderResolver.GetInstance().Get<IApplicationConfiguration>();
         
        private ApplicationConfiguration()
        { }

        public static IApplicationConfiguration GetInstance()
        {
            return instance;
        }

    }
}
