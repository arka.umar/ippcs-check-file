﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Configuration
{
    public class ConfigurationItem: SelectableDataModel
    {
        public string Key { set; get; }
        public string Qualifier { set; get; }
        public string Value { set; get; }
        public string TextValue { set; get; }
        public bool Prefetched { set; get; }

        public override string SelectedKey
        {
            get
            {
                return string.Format("Key{0}Qualifier", SELECTED_KEYS_SEPARATOR);
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return string.Format(Key + SELECTED_KEYS_SEPARATOR + Qualifier);
            }
        }

        public string GetSaveableDateFormat(DateTime date)
        {
            return string.Format("{0:dd/mm/yyyy}", date);
        }

        public int? GetValueAsInteger()
        {
            if (!string.IsNullOrEmpty(Value))
            {
                return Convert.ToInt32(Value);
            }

            return null;
        }
        public double? GetValueAsDouble()
        {
            if (!string.IsNullOrEmpty(Value))
            {
                return Convert.ToDouble(Value);
            }

            return null;
        }
        public decimal? GetValueAsDecimal()
        {
            if (!string.IsNullOrEmpty(Value))
            {                
                return Convert.ToDecimal(Value);
            }

            return null;
        }
        public byte? GetValueAsByte()
        {
            if (!string.IsNullOrEmpty(Value))
            {
                return Convert.ToByte(Value);
            }

            return null;
        }
        public bool? GetValueAsBoolean()
        {
            if (!string.IsNullOrEmpty(Value))
            {
                string lowerCase = Value.ToLower().Trim();
                bool state = lowerCase.Equals("true");
                return state;
            }

            return null;
        }
    }
}
