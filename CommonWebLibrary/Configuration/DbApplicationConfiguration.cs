﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Configuration
{
    public class DbApplicationConfiguration: IApplicationConfiguration
    {
        private readonly IDBContextManager dbManager;
        private Dictionary<string, List<ConfigurationItem>> prefetchedConfigs;

        public DbApplicationConfiguration()
        {
            IProviderResolver providerResolver = ProviderResolver.GetInstance();
            dbManager = providerResolver.Get<IDBContextManager>();
            prefetchedConfigs = new Dictionary<string, List<ConfigurationItem>>();
        }

        private void CachePrefetched(params ConfigurationItem[] configs)
        {
            if (configs == null)
            {
                return;
            }
            if (configs.Length > 0)
            {
                List<ConfigurationItem> items;
                bool exists;
                foreach (ConfigurationItem item in configs)
                {
                    if (!prefetchedConfigs.ContainsKey(item.Key))
                    {
                        items = new List<ConfigurationItem>();
                        items.Add(item);
                        prefetchedConfigs.Add(item.Key, items);
                    }
                    else
                    {
                        items = prefetchedConfigs[item.Key];
                        exists = false;
                        foreach (ConfigurationItem existingItem in items)
                        {
                            if (existingItem.Qualifier.Equals(item.Key))
                            {
                                exists = true;
                                break;
                            }
                        }
                        if (!exists)
                        {
                            items.Add(item);
                        }
                    }
                }
            }
        }
        private List<ConfigurationItem> GetPrefetchedByKey(string key)
        {
            if (prefetchedConfigs.ContainsKey(key))
            {
                return prefetchedConfigs[key];
            }
            return null;
        }
        private List<ConfigurationItem> GetPrefetchedByQualifier(string qualifier)
        {
            List<ConfigurationItem> result = new List<ConfigurationItem>();
            foreach (List<ConfigurationItem> items in prefetchedConfigs.Values)
            {
                foreach (ConfigurationItem item in items)
                {
                    if (item.Qualifier.Equals(qualifier))
                    {
                        result.Add(item);
                    }
                }
            }
            return result.Count > 0 ? result : null;
        }
        private ConfigurationItem GetPrefetched(string key, string qualifier)
        {
            List<ConfigurationItem> prefetched = GetPrefetchedByKey(key);
            if (prefetched != null)
            {
                foreach (ConfigurationItem item in prefetched)
                {
                    if (item.Qualifier.Equals(qualifier))
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public ConfigurationItem Get(string key, string qualifier)
        {
            ConfigurationItem prefetched = GetPrefetched(key, qualifier);
            if (prefetched != null)
            {
                return prefetched;
            }

            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            ConfigurationItem config = dbContext.SingleOrDefault<ConfigurationItem>("SelectConfiguration", new object[] { key, qualifier });
            dbContext.Close();

            CachePrefetched(config);
            return config;
        }
        public List<ConfigurationItem> GetByKey(string key)
        {
            List<ConfigurationItem> prefetched = GetPrefetchedByKey(key);
            if (prefetched != null)
            {
                return prefetched;
            }

            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            List<ConfigurationItem> result = dbContext.Fetch<ConfigurationItem>("SelectConfigurationByKey", new object[] {key});
            dbContext.Close();

            CachePrefetched(result.ToArray());
            return result;
        }
        public List<ConfigurationItem> GetByQualifier(string qualifier)
        {
            List<ConfigurationItem> prefetched = GetPrefetchedByQualifier(qualifier);
            if (prefetchedConfigs != null)
            {
                return prefetched;
            }

            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            List<ConfigurationItem> result = dbContext.Fetch<ConfigurationItem>("SelectConfigurationByQualifier", new object[] { qualifier });
            dbContext.Close();

            CachePrefetched(result.ToArray());
            return result;
        }
        public List<ConfigurationItem> GetAll()
        {
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            List<ConfigurationItem> result = dbContext.Fetch<ConfigurationItem>("SelectAllConfiguration");
            dbContext.Close();

            return result;
        }        

        public bool Save(params ConfigurationItem[] configs)
        {
            if ((configs != null) && (configs.Length > 0))
            {
                IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
                ConfigurationItem configItem;
                ConfigurationItem existingConfig;
                int len = configs.Length;
                int affectedRow = 0;
                for (int i = 0; i < len; i++)
                {
                    configItem = configs[i];
                    existingConfig = dbContext.SingleOrDefault<ConfigurationItem>("SelectConfiguration", new object[] {configItem.Key, configItem.Qualifier});
                    if (existingConfig == null)
                    {
                        affectedRow = dbContext.Execute(
                            "InsertConfiguration",new object[] {
                            configItem.Key,
                            configItem.Qualifier,
                            configItem.Value,
                            configItem.TextValue}
                        );
                    }
                    else
                    {
                        affectedRow = dbContext.Execute(
                           "UpdateConfiguration",new object[] {
                           configItem.Key,
                           configItem.Qualifier,
                           configItem.Value,
                           configItem.TextValue,
                           configItem.Key,
                           configItem.Qualifier}
                       );
                    }
                }

                dbContext.Close();
                return (affectedRow > 0);
            }

            return false;
        }

        public void Dispose() { }
        
    }
}
