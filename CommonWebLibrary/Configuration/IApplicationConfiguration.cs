﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Configuration
{
    public interface IApplicationConfiguration : IDisposable
    {
        ConfigurationItem Get(string key, string qualifier);
        List<ConfigurationItem> GetByKey(string key);
        List<ConfigurationItem> GetByQualifier(string qualifier);
        List<ConfigurationItem> GetAll();
        bool Save(params ConfigurationItem[] configs);
    }
}
