﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Ioc;

namespace Toyota.Common.Web.Menu
{
    public class MenuProvider
    {
        private static IMenuProvider provider = ProviderResolver.GetInstance().Get<IMenuProvider>();        

        private MenuProvider() {
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static IMenuProvider GetInstance()
        {
            return provider;
        }
    }
}
