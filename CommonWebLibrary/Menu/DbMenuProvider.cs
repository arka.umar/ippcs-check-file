﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;

namespace Toyota.Common.Web.Menu
{
    public class DbMenuProvider : IMenuProvider
    {
        public List<MenuItem> GetAll(User user)
        {
            IEnumerable<string> excepterScreen = new List<string>();
            if (user != null)
            {
                if (user.Authorization != null)
                {
                    excepterScreen = user.Authorization
                                        .SelectMany(s => s.AuthorizationDetails)
                                        .Where(item => (item.Screen != "") &&
                                            (item.Screen_Auth == "1"))
                                        .Select(item => item.Screen.ToLower().Trim())
                                        .Distinct()
                                        .OrderBy(s => s);
                }
            }
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var menus = db.Query<MenuItem>("GetAllMenu", new object[] { });
            if ((menus != null) && menus.Any())
            {
                Dictionary<byte, MenuItem> parents = new Dictionary<byte, MenuItem>();
                MenuItem parentMenu;
                foreach (var u in menus)
                {
                    if ((u.ParentId == null) || (u.ParentId == 0))
                    {
                        parents.Add(u.Id, u);
                    }
                    else
                    {
                        parentMenu = null;
                        parents.TryGetValue(u.ParentId.Value, out parentMenu);
                        if (parentMenu != null)
                        {
                            if (excepterScreen.Contains(u.ScreenId.ToLower().Trim()))
                            { parentMenu.Submenus.Add(u); } 
                        }
                        else
                        {
                            foreach (MenuItem m in parents.Values)
                            {
                                parentMenu = m.GetSubmenu(u.ParentId.Value);
                                if (parentMenu != null)
                                {
                                    if (excepterScreen.Contains(u.ScreenId.ToLower().Trim()))
                                    { parentMenu.Submenus.Add(u); break; }
                                }
                            }

                            //if (parentMenu == null)
                            //{
                            //    parents.Add(u.Id, u);
                            //}
                        }
                    }
                }

                List<MenuItem> lstMenu = new List<MenuItem>();
                lstMenu.AddRange(parents.Values);
                lstMenu.Sort();
                List<MenuItem> submenus;
                foreach (MenuItem menu in lstMenu.ToList())
                { 
                    if (menu.Submenus.Count == 0)
                    { lstMenu.Remove(menu); continue; }

                    submenus = menu.Submenus;
                    submenus.Sort(); 
                    foreach (MenuItem submenu in submenus.ToList())
                    { 
                        if (submenus.Count == 0)
                        { submenus.Remove(submenu); continue; }

                        if (submenu.Submenus.Count == 0 && submenu.Url == "#")
                        { submenus.Remove(submenu); continue; }

                        submenu.Submenus.Sort();
                    }
                }
                 
                // setelah di remove, pastiin lagi Menu paling atas yg 
                // tidak memiliki child untuk dihapus
                foreach (MenuItem menu in lstMenu.ToList())
                {
                    if (menu.Submenus.Count == 0 && menu.Url == "#")
                    { lstMenu.Remove(menu); continue; }
                }
                return lstMenu;
            }

            db.Close();
            return new List<MenuItem>();
        }
 
        private List<MenuItem> Except(List<MenuItem> MasterSource, User ExceptSource)
        {
            if (ExceptSource != null)
            {
                if (ExceptSource.Authorization != null)
                {
                    IEnumerable<string> excepterScreen = ExceptSource.Authorization
                                                            .SelectMany(s => s.AuthorizationDetails)
                                                            .Where(item => (item.Screen != "") &&
                                                                (item.Action.ToLower() == "null" || item.Action.ToLower() == null || item.Action == "") &&
                                                                (item.Object.ToLower() == "null" || item.Object.ToLower() == null || item.Object == "") &&
                                                                (item.AuthorizationGroup.ToLower() == "null" || item.AuthorizationGroup.ToLower() == null || item.AuthorizationGroup == ""))
                                                            .Select(item => item.Screen);

                     
                }
            }
            
            return MasterSource;
        }

        public bool Save(params MenuItem[] menus)
        {
            if ((menus != null) && (menus.Length > 0))
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                int cntMenu = menus.Length;
                int affectedRow = 0;
                MenuItem menu;
                MenuItem existingMenu;

                for (int i = 0; i < cntMenu; i++)
                {
                    menu = menus[i];

                    existingMenu = db.SingleOrDefault<MenuItem>("GetMenu", new object[] { menu.Id });
                    if (existingMenu == null)
                    {
                        affectedRow = db.Execute(
                            "InsertMenu", new object[] {
                            menu.ScreenId,
                            menu.Caption,
                            menu.Url,
                            menu.ParentId,
                            menu.Position}
                        );
                    }
                    else
                    {
                        affectedRow = db.Execute(
                            "UpdateMenu", new object[] {
                            menu.ScreenId,
                            menu.Caption,
                            menu.Url,
                            menu.ParentId,
                            menu.Position,
                            menu.Id}
                        );
                    }
                }

                db.Close();
                return (affectedRow > 0);
            }

            return false;
        }

    }
}
