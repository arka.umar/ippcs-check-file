﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Menu
{
    [Serializable]
    public class MenuModel
    {
        public MenuModel()
        {
            Menus = new List<MenuItem>();
        }

        public List<MenuItem> Menus { set; get; }
    }
}
