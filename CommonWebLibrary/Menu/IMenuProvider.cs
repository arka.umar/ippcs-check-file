﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Menu
{
    public interface IMenuProvider
    {
        List<MenuItem> GetAll(User user);
        bool Save(params MenuItem[] menus);
    }
}
