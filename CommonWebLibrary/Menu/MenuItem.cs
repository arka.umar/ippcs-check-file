﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Menu
{
    [Serializable]
    public class MenuItem : SelectableDataModel, IComparable<MenuItem>
    {
        public MenuItem()
        {
            Submenus = new List<MenuItem>();
        }

        public byte Id { set; get; }
        public string ScreenId { set; get; }
        public string Caption { set; get; }
        public string Url { set; get; }
        public byte? ParentId { set; get; }
        public byte? Position { set; get; }

        public override string SelectedKey
        {
            get
            {
                return "Id";
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Convert.ToString(Id);
            }
        }

        public List<MenuItem> Submenus { private set; get; }
        public MenuItem GetSubmenu(byte Id)
        {
            return GetSubmenu(this, Id);
        }

        /* In case we need full recursive searching on the future */
        public MenuItem GetSubmenu(MenuItem parent, byte Id)
        {
            List<MenuItem> submenus = parent.Submenus;
            if (submenus.Count > 0)
            {
                MenuItem found = null;
                foreach (MenuItem menu in submenus)
                {
                    if (menu.Id == Id)
                    {
                        found = menu;
                        break;
                    }
                }

                return found;
            }
            return null;
        }

        public int CompareTo(MenuItem other)
        {
            if ((Position != null) && (other.Position != null))
            {
                if (Position < other.Position)
                {
                    return -1;
                }
                if (Position > other.Position)
                {
                    return 1;
                }
            }
                        
            return 0;
        }
    }
}
