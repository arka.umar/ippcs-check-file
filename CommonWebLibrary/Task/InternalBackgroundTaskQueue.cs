﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.BackgroundTask
{
    public class InternalBackgroundTaskQueue
    {
        private static InternalBackgroundTaskQueue instance = null;
        private IDictionary<string, IBackgroundTask> tasks;        

        private InternalBackgroundTaskQueue() {
            tasks = new Dictionary<string, IBackgroundTask>();            
        }

        public static InternalBackgroundTaskQueue GetInstance()
        {
            if (instance == null)
            {
                instance = new InternalBackgroundTaskQueue();
            }            
            return instance;
        }        

        public void Add(IBackgroundTask task)
        {
            if(task != null) {
                if(!tasks.ContainsKey(task.GetName())) {
                    tasks.Add(task.GetName(), task);
                } else {
                    tasks[task.GetName()] = task;
                }       
            }            
        }

        public void Remove(string name)
        {
            if (tasks.ContainsKey(name))
            {
                tasks.Remove(name);
            }
        }
        
        public void Execute(string name, IDictionary<string, object> parameters) {
            IBackgroundTask task = Get(name);
            if (task != null)
            {
                task.Execute(parameters);
            }
        }

        public IBackgroundTask Get(string name)
        {
            if (tasks.ContainsKey(name))
            {
                return tasks[name];
            }
            return null;
        }
    }
}
