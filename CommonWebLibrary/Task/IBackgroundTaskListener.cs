﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.BackgroundTask
{
    public interface IBackgroundTaskListener
    {
        void BackgroundTaskChanged(BackgroundTaskEvent evt, IBackgroundTask task);
    }
}
