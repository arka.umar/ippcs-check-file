﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.BackgroundTask
{
    public enum BackgroundTaskEvent
    {
        STARTED, FINISHED, PROGRESS_CHANGED
    }
}
