﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Database.Petapoco;
using System.Reflection;
using System.Configuration;

namespace Toyota.Common.Web.Task
{
    public class BackgroundTaskService: IDisposable
    {
        private static BackgroundTaskService instance = null;
        private BackgroundTaskManager taskManager;

        private BackgroundTaskService() {
            ConnectionDescription dbDescription = new ConnectionDescription() { 
                Name = "PCS",
                ConnectionString = ConfigurationManager.AppSettings["BackgroundTaskConnectionString"],
                IsDefault = true
            };
            taskManager = new ExternalBackgroundTaskManager(dbDescription);
            TaskFolder = "C:\\Background_Task\\Tasks";
        }

        public static BackgroundTaskService GetInstance()
        {
            if (instance == null)
            {
                instance = new BackgroundTaskService();
            }
            return instance;
        }

        public string TaskFolder { private set; get; }
        public string GetTaskPath(string taskName)
        {
            return TaskFolder + "\\" + taskName + ".exe";
        }
                
        public void Register(Toyota.Common.Task.BackgroundTask task)
        {
            taskManager.Register(task);
        }

        public IList<ExternalBackgroundTask> GetRegisteredTasks()
        {
            List<ExternalBackgroundTask> result = new List<ExternalBackgroundTask>();

            IList<Common.Task.BackgroundTask> tasks = taskManager.SearchRegistryByType(TaskType.Immediate);
            if (tasks != null)
            {
                foreach (Common.Task.BackgroundTask t in tasks)
                {
                    result.Add((ExternalBackgroundTask) t);
                }
            }

            tasks = taskManager.SearchRegistryByType(TaskType.Delayed);
            if (tasks != null)
            {
                foreach (Common.Task.BackgroundTask t in tasks)
                {
                    result.Add((ExternalBackgroundTask)t);
                }
            }

            tasks = taskManager.SearchRegistryByType(TaskType.Periodic);
            if (tasks != null)
            {
                foreach (Common.Task.BackgroundTask t in tasks)
                {
                    result.Add((ExternalBackgroundTask)t);
                }
            }

            return result;
        }

        public IList<ExternalBackgroundTask> GetHistoryTasks()
        {
            List<ExternalBackgroundTask> result = new List<ExternalBackgroundTask>();

            IList<Common.Task.BackgroundTask> tasks = taskManager.SearchHistoryByType(TaskType.Immediate);
            if (tasks != null)
            {
                foreach (Common.Task.BackgroundTask t in tasks)
                {
                    result.Add((ExternalBackgroundTask)t);
                }
            }

            tasks = taskManager.SearchHistoryByType(TaskType.Delayed);
            if (tasks != null)
            {
                foreach (Common.Task.BackgroundTask t in tasks)
                {
                    result.Add((ExternalBackgroundTask)t);
                }
            }

            tasks = taskManager.SearchHistoryByType(TaskType.Periodic);
            if (tasks != null)
            {
                foreach (Common.Task.BackgroundTask t in tasks)
                {
                    result.Add((ExternalBackgroundTask)t);
                }
            }

            return result;
        }

        public void Dispose()
        {
            taskManager.Dispose();
        }
    }
}
