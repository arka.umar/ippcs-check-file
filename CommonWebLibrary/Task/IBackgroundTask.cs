﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.BackgroundTask
{
    public interface IBackgroundTask
    {
        string GetName();
        void AddListener(IBackgroundTaskListener listener);
        void RemoveListener(IBackgroundTaskListener listener);
        void Execute(IDictionary<string, object> parameters);        
    }
}
