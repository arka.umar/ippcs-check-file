﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Toyota.Common.Web.BackgroundTask
{
    public abstract class ObservableBackgroundTask: BackgroundTaskEventBroadcaster, IBackgroundTask
    {
        public const int PROGRESS_EXCEPTION = -255;

        private Thread thread;

        public bool Running { protected set; get; }

        private int progress;
        public int Progress {
            protected set {
                if (progress > 100)
                {
                    progress = 100;
                }
                else
                {
                    progress = value;
                }
            }
            get
            {
                return progress;
            }
        }

        public void Execute(IDictionary<string, object> parameters)
        {            
            progress = 0;
            ThreadPool.QueueUserWorkItem(new WaitCallback(ExecuteThreadWork), parameters);
            //thread = new Thread(new ParameterizedThreadStart(ExecuteThreadWork));
            //thread.Start(parameters);
        }

        private void ExecuteThreadWork(object param)
        {            
            IDictionary<string, object> parameters = (IDictionary<string, object>)param;
            Running = true;
            NotifyBackgroundTaskChanged(BackgroundTaskEvent.STARTED, this);
            DoWork(parameters);
            Running = false;
            NotifyBackgroundTaskChanged(BackgroundTaskEvent.FINISHED, this);
        }

        protected abstract void DoWork(IDictionary<string, object> parameters);
        public abstract string GetName();
    }
}
