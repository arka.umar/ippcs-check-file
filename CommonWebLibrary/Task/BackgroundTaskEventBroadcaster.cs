﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.BackgroundTask
{
    public class BackgroundTaskEventBroadcaster
    {
        private IList<IBackgroundTaskListener> listeners;

        public BackgroundTaskEventBroadcaster()
        {
            listeners = new List<IBackgroundTaskListener>();
        }

        public void AddListener(IBackgroundTaskListener listener)
        {
            if (!listeners.Contains(listener))
            {
                listeners.Add(listener);
            }
        }

        public void RemoveListener(IBackgroundTaskListener listener)
        {
            listeners.Remove(listener);
        }

        protected void NotifyBackgroundTaskChanged(BackgroundTaskEvent evt, IBackgroundTask task)
        {
            foreach (IBackgroundTaskListener listener in listeners)
            {
                listener.BackgroundTaskChanged(evt, task);
            }
        }
    }
}
