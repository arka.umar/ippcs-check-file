﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Messaging
{
    public class Message
    {
        public string MessageID { set; get; }
        public string MessageText { set; get; }
        public string MessageType { set; get; }
        public string Description { set; get; }
        public string Solution { set; get; }
        public string PIC1 { set; get; }
        public string PIC2 { set; get; }
        //public string CreatedBy { set; get; }
        //public DateTime CreatedDt { set; get; }
    }
}
