﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using System.Text.RegularExpressions;
using System.Collections;

namespace Toyota.Common.Web.Messaging
{
    public class Messages
    {
        private readonly static IMessages instance = new DbMessageMaster();

        private Messages() { }

        public static IMessages GetInstance()
        {
            return instance;
        }
    }
}
