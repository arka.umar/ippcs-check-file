﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Toyota.Common.Web.Messaging
{
    public interface IMessages
    { 
        List<Message> GetMessages();
    }
}
