﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using System.Text.RegularExpressions;
using System.Collections;

namespace Toyota.Common.Web.Messaging
{
    public class DbMessageMaster : IMessages
    {
        public List<Message> ListMessages;
         
        public DbMessageMaster()
        {
            ListMessages = GetAllMessage();
        }
         
        private List<Message> GetAllMessage()
        {
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            var messages = dbContext.Query<Message>("GetAllMessage");
            dbContext.Close();

            List<Message> lst = new List<Message>();
            foreach (var msg in messages)
            {
                lst.Add(new Message() 
                { 
                    MessageID = msg.MessageID,
                    MessageText = msg.MessageText,
                    MessageType = msg.MessageType
                });
            }
            
            return lst;
        }

        public List<Message> GetMessages()
        {
            return ListMessages;
        }
    }
}
