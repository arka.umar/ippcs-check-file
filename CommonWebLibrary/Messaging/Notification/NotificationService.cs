﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Messaging.Notification
{
    public class NotificationService
    {
        private static INotificationService service = new DefaultNotificationService();

        private NotificationService() { }

        public static INotificationService GetInstance()
        {
            return service;
        }
    }
}
