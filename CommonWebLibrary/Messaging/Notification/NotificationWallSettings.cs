﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Messaging.Notification
{
    public class NotificationWallSettings
    {
        public string Name { set; get; }
        public List<NotificationMessage> Messages { set; get; }
    }
}
