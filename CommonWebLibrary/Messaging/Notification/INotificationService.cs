﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Messaging.Notification
{
    public interface INotificationService
    {
        void Save(NotificationMessage notification, User user);
        void Save(List<NotificationMessage> notifications, User user);
        void Delete(NotificationMessage notification);
        void Delete(List<NotificationMessage> notifications);
        
        List<NotificationMessage> List();
        List<NotificationMessage> ListByPriority(byte priority);
        List<NotificationMessage> ListByRecipient(string recipient);
        List<NotificationMessage> ListByRecipientRole(int roleId);
        List<NotificationMessage> ListByUser(User user);
        void MarkAsRead(long id, bool read);

        List<NotificationMessage> SortByPriority(List<NotificationMessage> notifications);
        List<NotificationMessage> SortByPostDate(List<NotificationMessage> notifications);
    }
}
