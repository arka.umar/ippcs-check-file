﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Messaging.Notification
{
    class DefaultNotificationService: INotificationService
    {
        public List<NotificationMessage> ListByPriority(byte priority)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<NotificationMessage> messages = db.Fetch<NotificationMessage>("Notification_ListByPriority", new object[] { priority });
            db.Close();

            return messages;
        }

        public List<NotificationMessage> List()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<NotificationMessage> messages = db.Fetch<NotificationMessage>("Notification_List");
            db.Close();

            return messages;
        }

        public void Save(NotificationMessage notification, User user)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            db.Execute("Notification_Save", new object[] {
                GetLatestGroupId() + 1,
                notification.Title,
                notification.Content,
                notification.Author,
                notification.Recipient,
                notification.RecipientRoleId,
                notification.Priority,
                notification.ActionUrl,
                notification.PostDate,
                notification.Read,
                notification.ValidFrom,
                notification.ValidTo,
                user.Username,
                DateTime.Now
            });
            db.Close();
        }

        public void Save(List<NotificationMessage> notifications, User user)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            long groupId = GetLatestGroupId() + 1;
            foreach (NotificationMessage notification in notifications)
            {
                db.Execute("Notification_Save", new object[] {
                    GetLatestGroupId() + 1,
                    notification.Title,
                    notification.Content,
                    notification.Author,
                    notification.Recipient,
                    notification.RecipientRoleId,
                    notification.Priority,
                    notification.ActionUrl,
                    notification.PostDate,
                    notification.Read,
                    notification.ValidFrom,
                    notification.ValidTo,
                    user.Username,
                    DateTime.Now
                });
            }            
            db.Close();
        }

        public void Delete(NotificationMessage notification)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            db.Execute("Notification_Delete", new object[] { notification.Id });
            db.Close();
        }

        public void Delete(List<NotificationMessage> notifications)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            foreach (NotificationMessage notification in notifications)
            {
                db.Execute("Notification_Delete", new object[] { notification.Id });
            }            
            db.Close();
        }

        public List<NotificationMessage> ListByRecipient(string recipient)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<NotificationMessage> messages = db.Fetch<NotificationMessage>("Notification_ListByRecipient", new object[] { recipient });
            db.Close();

            return messages;
        }

        public List<NotificationMessage> ListByRecipientRole(int roleId)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<NotificationMessage> messages = db.Fetch<NotificationMessage>("Notification_ListByRecipientRole", new object[] { roleId });
            db.Close();

            return messages;
        }

        public List<NotificationMessage> ListByUser(User user)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<int> roleIds = new List<int>();

            foreach (Role role in user.Role)
            {
                roleIds.Add(role.RoleID);
            }

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<long> IdList = null;
            if (roleIds.Count > 0)
            {
                IdList = db.Fetch<long>("Notification_ListByUser", new object[] { user.Username, roleIds });
            }
            else
            {
                IdList = db.Fetch<long>("Notification_ListByUserNoRole", new object[] { user.Username });
            }
            
            List<NotificationMessage> result = null;
            if ((IdList != null) && (IdList.Count > 0))
            {
                result = db.Fetch<NotificationMessage>("Notification_ListById", new object[] { IdList });
            }
            db.Close();

            return result;
        }

        public List<NotificationMessage> SortByPriority(List<NotificationMessage> notifications)
        {
            notifications.Sort(new NotificationPriorityComparer());
            return notifications;
        }

        private long GetLatestGroupId()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            long? groupId = db.SingleOrDefault<long?>("Notification_LatestGroupId");
            db.Close();

            return groupId.HasValue ? groupId.Value + 1 : -1;
        }


        public List<NotificationMessage> SortByPostDate(List<NotificationMessage> notifications)
        {
            notifications.Sort(new NotificationPostDateComparer());
            return notifications;
        }


        public void MarkAsRead(long id, bool read)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            db.Execute("Notification_MarkRead", new object[] { read, id });
            db.Close();
        }
    }
}
