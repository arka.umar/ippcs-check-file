﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Toyota.Common.Web.Messaging.Notification
{
    public class NotificationWallExtension
    {
        private NotificationWallSettings settings;
        private HtmlHelper helper;

        public NotificationWallExtension(HtmlHelper helper, NotificationWallSettings settings)
        {
            this.settings = settings;
            this.helper = helper; 
        }

        public MvcHtmlString GetHtml()
        {
            return PartialExtensions.Partial(helper, "Messaging/Notification/NotificationWall", settings);
        }

        public void Render()
        {
            helper.ViewContext.Writer.WriteLine(GetHtml().ToHtmlString());
        }

        public NotificationWallExtension Bind(List<NotificationMessage> messages)
        {
            settings.Messages = messages;
            return this;
        }
    }
}
