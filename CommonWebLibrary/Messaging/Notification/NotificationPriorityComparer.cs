﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Messaging.Notification
{
    public class NotificationPriorityComparer: Comparer<NotificationMessage>
    {
        public override int Compare(NotificationMessage x, NotificationMessage y)
        {
            return y.Priority - x.Priority;
        }
    }
}
