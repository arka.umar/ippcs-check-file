﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Messaging.Notification
{
    public class NotificationMessage
    {
        public long Id { set; get; }
        public long GroupId { set; get; }
        public string Title { set; get; }
        public string Content { set; get; }
        public string Author { set; get; }
        public string Recipient { set; get; }
        public int RecipientRoleId { set; get; }
        public byte Priority { set; get; }
        public string ActionUrl { set; get; }
        public DateTime PostDate { set; get; }
        public string PostDateInString {
            get {
                if (PostDate != null)
                {
                    return string.Format("{0:dd.MM.yyyy}", PostDate);
                }
                return null;
            }
        }
        public bool Read { set; get; }
        public DateTime ValidFrom { set; get; }
        public DateTime ValidTo { set; get; }
    }
}