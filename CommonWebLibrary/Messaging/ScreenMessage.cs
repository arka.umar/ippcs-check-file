﻿/*
    @Lufty
    Representing a message on the screen.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Messaging
{
    [Serializable]
    public class ScreenMessage : IComparable<ScreenMessage>
    {        
        public const byte MESSAGE_TYPE_ERROR = 3;
        public const byte MESSAGE_TYPE_WARNING = 2;
        public const byte MESSAGE_TYPE_INFO = 1;
        public const byte MESSAGE_TYPE_SUCCESS = 0;
        
        public ScreenMessage(byte status, string message)
            : this(status, message, null)
        {
        }
        [System.Diagnostics.DebuggerStepThrough]
        public ScreenMessage(byte status, string message, string[] arguments)
        {
            this.Status = status;
            this.message = message;
            this.Arguments = arguments;
            this.Details = new List<ScreenMessage>();
        }        

        private string message;
        public string Message
        {
            set
            {
                message = value;
            }

            get
            {
                if (Arguments != null)
                {
                    return string.Format(message, Arguments);
                }
                return message;
            }
        }

        public byte Status { set; get; }
        public string[] Arguments { set; get; }
        public List<ScreenMessage> Details { set; get; }

        [System.Diagnostics.DebuggerStepThrough]
        public int CompareTo(ScreenMessage other)
        {
            if (Status < other.Status)
            {
                return -1;
            }
            if (Status > other.Status)
            {
                return 1;
            }

            return 0;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateError(string message, string[] arguments)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_ERROR, message, arguments);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateError(string message)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_ERROR, message, new string[] { });
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateInfo(string message, string[] arguments)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_INFO, message, arguments);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateInfo(string message)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_INFO, message, new string[] { });
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateWarning(string message, string[] arguments)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_WARNING, message, arguments);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateWarning(string message)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_WARNING, message, new string[] { });
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateSuccess(string message, string[] arguments)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_SUCCESS, message, arguments);
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static ScreenMessage CreateSuccess(string message)
        {
            return new ScreenMessage(ScreenMessage.MESSAGE_TYPE_SUCCESS, message, new string[] { });
        }
    }
}
