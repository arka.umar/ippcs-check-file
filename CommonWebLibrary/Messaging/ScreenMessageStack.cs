﻿/*
    @Lufty
    The holder for screen messages.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Messaging
{
    [Serializable]
    public class ScreenMessageStack
    {
        public List<ScreenMessage> Messages { private set; get; }
        public string Headline { set; get; }

        [System.Diagnostics.DebuggerStepThrough]
        public ScreenMessageStack()
        {
            Messages = new List<ScreenMessage>();
            Headline = "Error !!";
        }        

        [System.Diagnostics.DebuggerStepThrough]
        public void Push(ScreenMessage msg)
        {
            if (msg != null)
            {
                Messages.Add(msg);
            }
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void Remove(ScreenMessage msg)
        {
            if (msg != null)
            {
                Messages.Remove(msg);
            }
        }

        [System.Diagnostics.DebuggerStepThrough]
        public void Clear()
        {
            Messages.Clear();
        }
    }
}
