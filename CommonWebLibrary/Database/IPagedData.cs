﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public interface IPagedData<T>
    {
        long GetCurrentPage();
        long GetTotalPerPage();
        long GetTotalPages();
        long GetTotal();
        List<T> GetData();
    }
}
