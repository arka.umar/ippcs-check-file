﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public class NoConnectionStringFoundException : Exception
    {
        public NoConnectionStringFoundException() : base("No connection string found")
        {
        }
    }
}
