﻿/*
 * niit.yudha 16 nov 2012
 * Read files and get SQL statement
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Runtime.Caching; 

namespace Toyota.Common.Web.Database
{
    public class FileSQLStatementLoader : ISQLStatementLoader
    {

        private string SQLFolder;

        private Dictionary<string, string> replacements = null;
        private string Textuality(string name)
        {
            string key = "SQL." + name.ToLower();
            string sql = Memori.Get<string>(key);
            if (string.IsNullOrEmpty(sql))
            {
                string sqlFolderPath = ConfigurationManager.AppSettings["SQLFolder"];
                if (string.IsNullOrEmpty(Path.GetPathRoot(sqlFolderPath)))
                {
                    SQLFolder = AppDomain.CurrentDomain.BaseDirectory + sqlFolderPath;
                }
                if (Directory.Exists(SQLFolder))

                    foreach (string ext in new string[] { "txt", "sql" })
                    {
                        string fn = Path.Combine(SQLFolder, name) + "." + ext;
                        if (File.Exists(fn))
                        {
                            sql = File.ReadAllText(fn);
                            break;
                        }
                    }
            }
            else
            {
                Memori.Int("COUNT." + name.ToLower());
            }
            return sql; 
        }     
        public FileSQLStatementLoader()
        {
                      if (replacements == null) {
                replacements = Memori.Get<Dictionary<string, string>>("SQL_REPLACEMENTS_DICTIONARY");
                if (replacements == null)
                {
                    int kvcount = 0;
                    string SQL_REPLACEMENTS = Textuality("replacements");
                    if (!string.IsNullOrEmpty(SQL_REPLACEMENTS))
                    {
                        replacements = new Dictionary<string, string>();
                        
                        foreach (string s in SQL_REPLACEMENTS.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            string[] kv = s.Split(new string[] { "|", "\t", "=>" }, StringSplitOptions.RemoveEmptyEntries);
                            if (kv.Length == 2)
                            {
                                ++kvcount;
                                replacements.Add(kv[0], kv[1]);
                            }
                        }
                        Memori.Set("SQL.replacements", SQL_REPLACEMENTS);
                        Memori.Set("SQL_REPLACEMENTS_DICTIONARY", replacements);
                    }
                }
            }
        }

        /// <summary>
        /// Get SQL Statement 
        /// </summary>
        /// <param name="name">Represent the sql name</param>
        /// <returns></returns>
        public string GetSQLStatement(string name)
        {            
                      string key = "SQL." + name.ToLower();

            string sql = Textuality(name); 

            if (!string.IsNullOrEmpty(sql))
            {
                Memori.Set(key, sql);
            }
            /// treat sql with care 
            if (replacements != null && !string.IsNullOrEmpty(sql))
            {
                foreach (var kv in replacements)
                {
                    sql = sql.Replace(kv.Key, kv.Value);
                }
            }
            return sql; 

        }
    }
}
