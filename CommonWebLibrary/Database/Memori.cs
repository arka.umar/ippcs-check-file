﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching; 

namespace Toyota.Common.Web.Database
{
    public static class Memori
    {
        public static MemoryCache mem { get { return MemoryCache.Default; } }

        public static void Set(string key, object value, int ageSeconds = -300)
        {
            MemoryCache mem = MemoryCache.Default;
            if (value == null)
            {
                mem.Remove(key);
                return;
            }
            CacheItemPolicy cip = new CacheItemPolicy();
            if (ageSeconds < 0) /// sliding expiration 
            {
                cip.SlidingExpiration = TimeSpan.FromSeconds(ageSeconds * -1);
            }
            else if (ageSeconds > 0) /// absolute expiration 
            {
                cip.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddSeconds(ageSeconds));
            }
            else // never expire 
            {
                cip.AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration;
            }
            mem.Set(key, value, cip);
        }

        public static T Get<T>(string key)
        {
            MemoryCache mem = MemoryCache.Default; 

            CacheItem c = mem.GetCacheItem(key);
            if (c != null)
            {
                if (c.Value is T)
                    return (T)c.Value;
                else
                    return default(T);
            }
            else
                return default(T);
        }

        public static int Int(string key, int? val = null)
        {
            MemoryCache mem = MemoryCache.Default; 

            CacheItem ci = mem.GetCacheItem(key);
            Int32 y = 0;

            if (ci != null && ci.Value != null)
            {
                y = (Int32)ci.Value;
            }

            if (val.HasValue)
            {
                Int32 x = new Int32();
                x = val.Value;
                mem.Set(key, x, new CacheItemPolicy() { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });
                if (ci == null || ci.Value == null)
                {
                    y = x;
                }
            }
            else
            {
                Int32 z = new Int32();
                z = y + 1;
                mem.Set(key, z, new CacheItemPolicy() { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });
                y = z;
            }
            return y;
        }
    }
}
