﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public abstract class BaseSQLPaginator: SQLPaginator
    {
        public string GetTotalScript(string sql)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("select COUNT(*) from ( ");
            builder.AppendLine("    " + sql);
            builder.AppendLine(") as Result ");
            return builder.ToString();
        }

        public virtual string DecorateScript(string sql, long page, long pageSize)
        {
            return sql;
        }
    }
}
