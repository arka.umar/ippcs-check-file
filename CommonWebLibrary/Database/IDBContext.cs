﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;


namespace Toyota.Common.Web.Database
{
    public interface IDBContext: IDisposable
    {
        string GetName();

        IEnumerable<T> QueryStatement<T>(string sql, object[] args);
        IEnumerable<T> QueryStatement<T>(string sql, string prefixStatement, string suffixStatement, object[] args);
        IEnumerable<T> QueryStatement<T>(string sql);
        IEnumerable<T> QueryStatement<T>(string sql, string prefixStatement, string suffixStatement);

        IEnumerable<T> Query<T>(string sqlName, object[] args);
        IEnumerable<T> Query<T>(string sqlName, string prefixStatement, string suffixStatement, object[] args);
        IEnumerable<T> Query<T>(string sqlName);
        IEnumerable<T> Query<T>(string sqlName, string prefixStatement, string suffixStatement);        

        IPagedData<T> FetchByPage<T>(string sqlName, long pageNumber, long pageSize, object[] args);
        IPagedData<T> FetchByPage<T>(string sqlName, string prefixStatement, string suffixStatement, long pageNumber, long pageSize, object[] args);
        IPagedData<T> FetchByPage<T>(string sqlName, long pageNumber, long pageSize);
        IPagedData<T> FetchByPage<T>(string sqlName, string prefixStatement, string suffixStatement, long pageNumber, long pageSize);

        List<T> FetchStatement<T>(string sql, object[] args);
        List<T> FetchStatement<T>(string sql, string prefixStatement, string suffixStatement, object[] args);
        List<T> FetchStatement<T>(string sql);
        List<T> FetchStatement<T>(string sql, string prefixStatement, string suffixStatement);

        List<T> Fetch<T>(string sqlName, object[] args);
        List<T> Fetch<T>(string sqlName, string prefixStatement, string suffixStatement, object[] args);
        List<T> Fetch<T>(string sqlName);
        List<T> Fetch<T>(string sqlName, string prefixStatement, string suffixStatement);

        T SingleOrDefaultStatement<T>(string sql);
        T SingleOrDefaultStatement<T>(string sql, object[] args);
        T SingleOrDefaultStatement<T>(string sql, string prefixStatement, string suffixStatement, object[] args);

        T SingleOrDefault<T>(string sqlName);
        T SingleOrDefault<T>(string sqlName, object[] args);
        T SingleOrDefault<T>(string sqlName, string prefixStatement, string suffixStatement, object[] args);

        T ExecuteScalar<T>(string sqlName);
        T ExecuteScalar<T>(string sqlName, object[] args);
        T ExecuteScalar<T>(string sqlName, string prefixStatement, string suffixStatement, object[] args);

        int Execute(string sqlName);
        int Execute(string sqlName, object[] args);
        int Execute(string sqlName, string prefixStatement, string suffixStatement, object[] args);

        void BeginTransaction();
        void CommitTransaction();
        void Close();
    }
}
