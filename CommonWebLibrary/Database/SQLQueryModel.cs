﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public class SQLQueryModel
    {
        public string Name { set; get; }
        public object[] Parameters { set; get; }
    }
}
