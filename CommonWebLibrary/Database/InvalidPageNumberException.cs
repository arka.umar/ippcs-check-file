﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public class InvalidPageNumberException: Exception
    {
        public InvalidPageNumberException(): base("Invalid page number. Page number is a positive number starts from 1.") {
            
        }
    }
}
