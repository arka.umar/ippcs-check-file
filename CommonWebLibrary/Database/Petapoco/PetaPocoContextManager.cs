﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Configuration;

namespace Toyota.Common.Web.Database.Petapoco
{
    public class PetaPocoContextManager : IDBContextManager
    {
        private readonly Dictionary<string, string> connectionStrings;

        public PetaPocoContextManager()
        {
            connectionStrings = new Dictionary<string, string>();
            foreach (ConnectionStringSettings con in ConfigurationManager.ConnectionStrings)
            {
                connectionStrings.Add(con.Name, con.ConnectionString);
            }
        }

        public IDBContext GetContext(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            return new PetaPocoDBContext(name, this);
        }

        public void Close()
        {
            GC.SuppressFinalize(this);
        }
    }
}
