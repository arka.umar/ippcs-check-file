﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;

namespace Toyota.Common.Web.Database.Petapoco
{
    public class PetaPocoPagedData<T>: IPagedData<T>
    {
        private PetaPoco.Page<T> page;

        public PetaPocoPagedData(PetaPoco.Page<T> page)
        {   
            this.page = page;
        }

        public long GetCurrentPage()
        {
            return page.CurrentPage;
        }

        public long GetTotalPerPage()
        {
            return page.ItemsPerPage;
        }

        public long GetTotalPages()
        {
            return page.TotalPages;
        }

        public long GetTotal()
        {
            return page.TotalItems;
        }

        public List<T> GetData()
        {
            return page.Items;
        }
    }
}
