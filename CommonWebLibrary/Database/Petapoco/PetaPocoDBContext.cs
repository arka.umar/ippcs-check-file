﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Util.Configuration;

namespace Toyota.Common.Web.Database.Petapoco
{
    public class PetaPocoDBContext : IDBContext
    {
        private string name;
        private PetaPoco.Database db;
        private PetaPocoContextManager contextManager;

        private List<ISQLStatementLoader> loaders; 
        private SQLPaginator paginator;
 
        public PetaPocoDBContext(string name, PetaPocoContextManager contextManager)
        {
            this.name = name;
            this.contextManager = contextManager;
            db = new PetaPoco.Database(name);
            db.EnableAutoSelect = false;
            db.OpenSharedConnection();
            loaders = new List<ISQLStatementLoader>(); 
            loaders.Add(new XMLSQLStatementLoader());
            loaders.Add(new FileSQLStatementLoader());
            paginator = new SQL2008Paginator();
        }
        private string SQL(string name)
        {
            string result = null;
            
            foreach(ISQLStatementLoader l in loaders) {
                result = l.GetSQLStatement(name);
                if (!string.IsNullOrEmpty(result))
                    break;
            }
            return result; 
        }

        public IEnumerable<T> Query<T>(string sqlName)
        {
            return QueryStatement<T>(SQL(sqlName));
        }
        public IEnumerable<T> Query<T>(string sqlName, params object[] args)
        {
            return QueryStatement<T>(SQL(sqlName), args);
        }
        public IEnumerable<T> Query<T>(string sqlName, string prefixStatement, string suffixStatement)
        {
            return QueryStatement<T>(prefixStatement + " " + SQL(sqlName) + " " + suffixStatement);
        }
        public IEnumerable<T> Query<T>(string sqlName, string prefixStatement, string suffixStatement, params object[] args)
        {
            return QueryStatement<T>(prefixStatement + " " + SQL(sqlName) + " " + suffixStatement, args);
        }

        public IEnumerable<T> QueryStatement<T>(string sql, object[] args)
        {
            return db.Query<T>(sql, args);
        }
        public IEnumerable<T> QueryStatement<T>(string sql, string prefixStatement, string suffixStatement, object[] args)
        {
            return db.Query<T>(prefixStatement + " " + sql + " " + suffixStatement, args);
        }
        public IEnumerable<T> QueryStatement<T>(string sql)
        {
            return db.Query<T>(sql);
        }
        public IEnumerable<T> QueryStatement<T>(string sql, string prefixStatement, string suffixStatement)
        {
            return db.Query<T>(prefixStatement + " " + sql + " " + suffixStatement);
        }

        public IPagedData<T> FetchByPage<T>(string sqlName, long pageNumber, long itemNumber)
        {
            return FetchByPage<T>(sqlName, pageNumber, itemNumber, null);
        }
        public IPagedData<T> FetchByPage<T>(string sqlName, long pageNumber, long itemNumber, params object[] args)
        {
            string sql = SQL(sqlName); 
            PetaPoco.Page<T> page = new PetaPoco.Page<T>();
            page.TotalItems = SingleOrDefaultStatement<long>(paginator.GetTotalScript(sql));
            page.TotalPages = (page.TotalItems % itemNumber) == 0 ? page.TotalItems / itemNumber : (page.TotalItems / itemNumber) + 1;
            page.CurrentPage = pageNumber;
            page.ItemsPerPage = itemNumber;
            page.Items = FetchStatement<T>(paginator.DecorateScript(sql, pageNumber, itemNumber), args != null ? args : new object[] {});
            return new PetaPocoPagedData<T>(page);
        }
        public IPagedData<T> FetchByPage<T>(string sqlName, string prefixStatement, string suffixStatement, long pageNumber, long itemNumber)
        {
            return FetchByPage<T>(sqlName, prefixStatement, suffixStatement, pageNumber, itemNumber, null);
        }
        public IPagedData<T> FetchByPage<T>(string sqlName, string prefixStatement, string suffixStatement, long pageNumber, long itemNumber, params object[] args)
        {
            string sql = SQL(sqlName);
      
            PetaPoco.Page<T> page = new PetaPoco.Page<T>();
            page.TotalItems = SingleOrDefaultStatement<long>(paginator.GetTotalScript(sql));
            page.TotalPages = (page.TotalItems % itemNumber) == 0 ? page.TotalItems / itemNumber : (page.TotalItems / itemNumber) + 1;
            page.CurrentPage = pageNumber;
            page.ItemsPerPage = itemNumber;
            page.Items = FetchStatement<T>(paginator.DecorateScript(prefixStatement + " " + sql + " " + suffixStatement, pageNumber, itemNumber), args != null ? args : new object[] { });
            return new PetaPocoPagedData<T>(page);
        }
        
        public List<T> FetchStatement<T>(string sql, object[] args)
        {
            return db.Fetch<T>(sql, args);
        }
        public List<T> FetchStatement<T>(string sql, string prefixStatement, string suffixStatement, object[] args)
        {
            return db.Fetch<T>(prefixStatement + " " + sql + " " + suffixStatement, args);
        }
        public List<T> FetchStatement<T>(string sql)
        {
            return db.Fetch<T>(sql);
        }
        public List<T> FetchStatement<T>(string sql, string prefixStatement, string suffixStatement)
        {
            return db.Fetch<T>(prefixStatement + " " + sql + " " + suffixStatement);
        }

        public List<T> Fetch<T>(string sqlName)
        {
            string kn = "List." + typeof(T).Name + "." + sqlName; 

            List<T> ret = Memori.Get<List<T>>(kn);
            if (ret == null)
            {
                ret = FetchStatement<T>(SQL(sqlName));
                Memori.Set(kn, ret);
            }
            return ret;
        }
        public List<T> Fetch<T>(string sqlName, params object[] args)
        {
            return FetchStatement<T>(SQL(sqlName), args);
        }
        public List<T> Fetch<T>(string sqlName, string prefixStatement, string suffixStatement)
        {
            return FetchStatement<T>(prefixStatement + " " + SQL(sqlName)+ " " + suffixStatement);
        }
        public List<T> Fetch<T>(string sqlName, string prefixStatement, string suffixStatement, params object[] args)
        {
            return FetchStatement<T>(prefixStatement + " " + SQL(sqlName) + " " + suffixStatement, args);
        }

        public T SingleOrDefaultStatement<T>(string sql)
        {
            return SingleOrDefaultStatement<T>(sql, new object[] {});
        }
        public T SingleOrDefaultStatement<T>(string sql, object[] args)
        {
            return db.SingleOrDefault<T>(sql, args);
        }
        public T SingleOrDefaultStatement<T>(string sql, string prefixStatement, string suffixStatement, object[] args)
        {
            return db.SingleOrDefault<T>(prefixStatement + " " + sql + " " + suffixStatement, args);
        }

        public T SingleOrDefault<T>(string sqlName)
        {
            return SingleOrDefault<T>(sqlName, null);
        }
        public T SingleOrDefault<T>(string sqlName, params object[] args)
        {
            return SingleOrDefaultStatement<T>(SQL(sqlName), args);
        }
        public T SingleOrDefault<T>(string sqlName, string prefixStatement, string suffixStatement, params object[] args)
        {
            return SingleOrDefaultStatement<T>(prefixStatement + " " + SQL(sqlName) + " " + suffixStatement, args);
        }

        public T ExecuteScalar<T>(string sqlName)
        {            
            return ExecuteScalar<T>(sqlName, null);
        }
        public T ExecuteScalar<T>(string sqlName, params object[] args)
        {
            return db.ExecuteScalar<T>(SQL(sqlName), args != null ? args : new object[] { });
        }
        public T ExecuteScalar<T>(string sqlName, string prefixStatement, string suffixStatement, params object[] args)
        {
            return db.ExecuteScalar<T>(prefixStatement + " " + SQL(sqlName) + " " + suffixStatement, args != null ? args : new object[] { });
        }

        public int Execute(string sqlName)
        {
            return Execute(sqlName, null);
        }
        public int Execute(string sqlName, params object[] args)
        {
            return db.Execute(SQL(sqlName), args != null ? args : new object[] { });
        }
        public int Execute(string sqlName, string prefixStatement, string suffixStatement, params object[] args)
        {
            return db.Execute(prefixStatement + " " + SQL(sqlName) + " " + suffixStatement, args != null ? args : new object[] { });
        }

        public string GetName()
        {
            return name;
        }        
        public void Close()
        {
            db.CloseSharedConnection();
        }
        public void BeginTransaction()
        {
            db.BeginTransaction();
        }
        public void CommitTransaction()
        {
            db.CompleteTransaction();
        }

        public void Dispose()
        {
            Close();
            GC.SuppressFinalize(this);
        }
    }
}
