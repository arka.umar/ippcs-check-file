﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public class DBContextNames
    {
        public const string DB_TMMIN_ROLE = "TMMIN_ROLE";
        public const string DB_COMMON_MASTER = "COMMON_MASTER";
        public const string DB_PCS = "PCS";
        public const string DB_SCP = "SCP";
        public const string DB_WEB_PORTAL = "WEB_PORTAL_DB";
        public const string DB_WORKFLOW = "WORKFLOW";
        public const string DB_MASTER = "MASTER";
           
    }
}
