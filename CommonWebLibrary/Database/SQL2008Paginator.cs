﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public class SQL2008Paginator: BaseSQLPaginator
    {
        public override string DecorateScript(string sql,long page, long pageSize)
        {
            if (page < 1)
            {
                throw new InvalidPageNumberException();
            }

            long startIndex = (pageSize * (page - 1)) + 1;
            long endIndex = startIndex + pageSize - 1;

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("select * from ( ");
            builder.AppendLine("	select ");
            builder.AppendLine("		ROW_NUMBER() over (order by DELiVERY_NO) as RowNumber, * ");
            builder.AppendLine("	from ( ");
            builder.AppendLine("        " + sql);
            builder.AppendLine("	) as OriginalResult ");
            builder.AppendLine(") as PagedResult ");
            builder.AppendLine("where (PagedResult.RowNumber >= " + startIndex + ") and (PagedResult.RowNumber <= " + endIndex + ") ");
            builder.AppendLine("order by PagedResult.RowNumber ");
            return builder.ToString();
        }
    }
}
