﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;

namespace Toyota.Common.Web.Database
{
    public class DatabaseManager
    {
        private readonly static IDBContextManager instance = ProviderResolver.GetInstance().Get<IDBContextManager>();

        static DatabaseManager(){}

        DatabaseManager(){}

        public static IDBContextManager GetInstance()
        {
            return instance;
        }
    }
}
