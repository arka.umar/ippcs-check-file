﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database {

    public interface ISQLStatementLoader
    {
        string GetSQLStatement(string name);
    }
}
