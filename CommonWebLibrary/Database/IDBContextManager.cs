﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public interface IDBContextManager
    {
        IDBContext GetContext(string name);
        void Close();
    }
}
