﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public interface SQLPaginator
    {
        string DecorateScript(string sql, long page, long pageSize);
        string GetTotalScript(string sql);        
    }
}
