﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Database
{
    public class ConnectionStringNotFoundException : Exception
    {
        public ConnectionStringNotFoundException(string connectionName)
            : base(string.Format("Connection string named {0} not found",connectionName))
        {

        }
    }
}
