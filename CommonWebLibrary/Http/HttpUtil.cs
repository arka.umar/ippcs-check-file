﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using Toyota.Common.Web.Util;

namespace Toyota.Common.Web.Http
{
    public class HttpUtil
    {        
        public static Dictionary<string, string> PackMappedRequestParameter(HttpRequestBase request, params MappedValue<string,string>[] keys)
        {
            Dictionary<string, string> requestParams = new Dictionary<string, string>();
            NameValueCollection rqParams = request.Params;
            string[] paramKeys = rqParams.AllKeys;
            if ((keys != null) && (keys.Length > 0))
            {
                foreach (MappedValue<string,string> k in keys)
                {
                    if (paramKeys.Contains(k.Key))
                    {
                        requestParams.Add(k.Value, rqParams.Get(k.Key));
                    }
                }          
            }
            else
            {
                foreach (string key in rqParams.Keys)
                {
                    requestParams.Add(key, rqParams.Get(key));
                }
            }

            return requestParams;
        }
        
        public static Dictionary<string, string> PackRequestParameter(HttpRequestBase request, params string[] keys)
        {
            List<MappedValue<string, string>> mappedKeys = null;
            if ((keys != null) && (keys.Length > 0))
            {
                mappedKeys = new List<MappedValue<string, string>>();
                foreach (string k in keys)
                {
                    mappedKeys.Add(new MappedValue<string,string>() {
                        Key = k, Value = k
                    });
                }                
            }

            if (mappedKeys == null)
            {
                return PackMappedRequestParameter(request, null);
            }
            return PackMappedRequestParameter(request, mappedKeys.ToArray());
        }
    }
}
