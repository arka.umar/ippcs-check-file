﻿/*
 * NIIT Mulki, 10th Dec 2012
 * Log Monitoring
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Log
{
    public class LogDetail
    {
        public string ProcessID { set; get; }
        public int SequenceNumber { set; get; }
        public string MessageID { set; get; }
        public string MessageText { set; get; }
        public string MessageType { set; get; }
        public string Location { set; get; }
        public string CreatedUser { set; get; }
        public DateTime CreatedDate { set; get; }
    }
}
