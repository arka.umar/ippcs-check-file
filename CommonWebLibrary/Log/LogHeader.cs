﻿/*
 * NIIT Mulki, 10th Dec 2012
 * Log Monitoring
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Log
{
    public class LogHeader : SelectableDataModel
    {
        public string ProcessID { set; get; }
        public string FunctionID { set; get; }
        public string ModuleID { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public byte ProcessStatus { set; get; }
        public string UserName { set; get; }
        public string CreatedUser { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedUser { set; get; }
        public DateTime ChangedDate { set; get; }

        public override string SelectedKey
        {
            get
            {
                return string.Format("ProcessID");
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Convert.ToString(ProcessID);
            }
        }
    }
}
