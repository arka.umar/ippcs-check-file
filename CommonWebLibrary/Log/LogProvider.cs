﻿/*
 * NIIT Mulki, 10th Dec 2012
 * Log Monitoring
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Log
{
    public class LogProvider
    {
        private string Id;
        private string moduleId;
        private User user;

        public LogProvider(string Id, string moduleId, User user)
        {
            this.Id = Id;
            this.moduleId = moduleId;
            this.user = user;
        }

        public ILogSession CreateSession()
        {
            return new DefaultLogSession(moduleId, Id, user);           
        }

        public ILogSession CreateSession(bool isDirectly)
        {
            return new DefaultLogSession(moduleId, Id, user, isDirectly);
        }
    }
}
