﻿/*
 * NIIT Mulki, 10th Dec 2012
 * Log Monitoring
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using System.Text.RegularExpressions;

namespace Toyota.Common.Web.Log
{
    public class DefaultLogSession : ILogSession
    {
        private string moduleId;
        private string Id;
        private LogHeader header;
        private List<LogDetail> details;         
        private User user;
        private IMessages messages;

        private Int64 ProcessId;
        public Int64 GetProcessId()
        {
            return ProcessId;
        }

        private int SequenceNumber;

        public DefaultLogSession(string Id, string moduleId, User user)
        {
            Init(Id, moduleId, user);
        }

        public DefaultLogSession(string Id, string moduleId, User user, bool isDirectly)
        {
            if (isDirectly)
            {
                this.Id = Id;
                this.moduleId = moduleId;
                this.user = user;
                details = new List<LogDetail>();
                DateTime startDate = DateTime.Now;
                messages = Messages.GetInstance();
                header = new LogHeader
                {
                    ProcessID = Id,
                    FunctionID = Id,
                    ModuleID = moduleId,
                    StartDate = DateTime.Now,
                    CreatedUser = user.Username,
                    CreatedDate = DateTime.Now
                };
                
                header.EndDate = DateTime.Now;
                header.ProcessStatus = GetStatus();

                IDBContextManager dbManager = DatabaseManager.GetInstance();
                IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);

                try
                {
                    ProcessId = dbContext.ExecuteScalar<Int64>("InsertTableLogHeaders", new object[] {
                            header.FunctionID ,
                            header.ModuleID, 
                            header.StartDate, 
                            header.EndDate,
                            header.ProcessStatus,
                            header.CreatedUser
                        });
                }
                catch (Exception) { }
                finally { dbContext.Close(); }

                SequenceNumber = 0;
            }
            else
            {
                Init(Id, moduleId, user);
            }
        }

        private void Init(string Id, string moduleId, User user)
        {
            this.Id = Id;
            this.moduleId = moduleId;
            this.user = user;
            details = new List<LogDetail>();
            DateTime startDate = DateTime.Now;
            messages = Messages.GetInstance();
            header = new LogHeader
            {
                ProcessID = Id,
                FunctionID = Id,
                ModuleID = moduleId,
                StartDate = DateTime.Now,
                CreatedUser = user.Username,
                CreatedDate = DateTime.Now
            };
        }

        public void Commit()
        {
            if (header != null)
            {
                if (details != null)
                {
                    header.EndDate = DateTime.Now;
                    header.ProcessStatus = GetStatus();

                    IDBContextManager dbManager = DatabaseManager.GetInstance();
                    IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);

                    Int64 processID = dbContext.ExecuteScalar<Int64>("InsertTableLogHeaders", new object[] {
                            header.FunctionID ,
                            header.ModuleID, 
                            header.StartDate, 
                            header.EndDate,
                            header.ProcessStatus,
                            header.CreatedUser
                        });

                    details.ForEach(ld =>
                        {
                            dbContext.Execute("InsertTableLogDetails", new object[] {  
                            processID, 
                            ld.SequenceNumber,
                            ld.MessageID, 
                            ld.MessageType,
                            ld.MessageText,
                            ld.Location, 
                            ld.CreatedUser});
                        });

                    dbContext.Close();
                }
                details = null;
                header = null;
            }
        }

        public string GetId()
        {
            return Id;
        }

        public void Write(string messageID, object[] messageArgs)
        {
            string methodName = new System.Diagnostics.StackTrace().GetFrame(1).GetMethod().Name;
            Message msg = (from m in messages.GetMessages()
                           where m.MessageID == messageID
                           select m).SingleOrDefault();

            if (msg != null)
            {
                details.Add(new LogDetail()
                {
                    ProcessID = header.ProcessID,
                    SequenceNumber = details.Count + 1,
                    MessageText = GetMessage(msg.MessageText, messageArgs),
                    MessageID = msg.MessageID,
                    MessageType = msg.MessageType,
                    Location =  ToReadableString(methodName),
                    CreatedUser = user.Username,
                    CreatedDate = DateTime.Now
                });
            }
            else 
            {
                throw new Exception("Could not found Message ID!!");
            }
        }

        public void WriteDirectly(string messageID, object[] messageArgs, string location)
        {
            string methodName = new System.Diagnostics.StackTrace().GetFrame(1).GetMethod().Name;
            Message msg = (from m in messages.GetMessages()
                           where m.MessageID == messageID
                           select m).SingleOrDefault();

            if (msg != null)
            {
                IDBContextManager dbManager = DatabaseManager.GetInstance();
                IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);

                try
                {
                    dbContext.Execute("InsertTableLogDetails", new object[] {  
                        ProcessId, 
                        ++SequenceNumber,
                        msg.MessageID, 
                        msg.MessageType,
                        GetMessage(msg.MessageText, messageArgs),
                        location, 
                        user.Username});
                }
                catch (Exception) { }
                finally { dbContext.Close(); }
            }
            else
            {
                throw new Exception("Could not found Message ID!!");
            }
        }

        public string GetModuleId()
        {
            return moduleId;
        }

        public void SetStatus(byte status)
        {
            header.ProcessStatus = status;
        }

        public byte GetStatus()
        {
            return header.ProcessStatus;
        }
        private string ToReadableString(string str)
        {
            string buff = string.Empty;
            Array.ForEach(str.ToCharArray(), c =>
                    {
                        if (Convert.ToInt32(c) >= 65 && Convert.ToInt32(c) <= 90)
                        { buff += " " + c;}
                        else
                        { buff += c; }
                    }
                );
            return buff;
        }

        private string GetMessage(string messageID, object[] param)
        {
            Regex rxParams = new Regex(@"\{(\w*?)\}|\[(\w*?)\]", RegexOptions.Compiled);
            string hasil = string.Empty;
            int paramIndex = 0;
            hasil = rxParams.Replace(messageID, m =>
            {

                if (paramIndex < 0 || paramIndex >= param.Length)
                    throw new ArgumentOutOfRangeException(string.Format("Parameter '{0}' specified but only {1} parameters supplied (in `{2}`)", paramIndex, param.Length, messageID));
                string rep = (string)param[paramIndex];
                paramIndex += 1;

                return rep;
            });

            return hasil;
        }
    }
}
