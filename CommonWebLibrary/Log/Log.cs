﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Log
{
    public class Log : SelectableDataModel
    {
        public int Process_id { set; get; }
        public DateTime Process_dt { set; get; }
        public string Function_id { set; get; }
        public string Module_id { set; get; }
        public string Process_sts { set; get; }
        public string User_id { set; get; }
        public DateTime End_dt { set; get; }
        public string Remarks { set; get; }

        public override string SelectedKey
        {
            get
            {
                return string.Format("Process_id");
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Convert.ToString(Process_id);
            }
        }
    }
}
