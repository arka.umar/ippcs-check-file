﻿/*
 * NIIT Mulki, 10th Dec 2012
 * Log Monitoring
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Log
{
    public interface ILogSession
    {        
        string GetModuleId();
        string GetId();
        Int64 GetProcessId();
        void Write(string messageID, object[] messageArgs);
        void WriteDirectly(string messageID, object[] messageArgs, string location);
        void Commit();
        void SetStatus(byte status);
        byte GetStatus();
    }
}
