﻿/*
 NIIT Mulki
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Compression;

namespace Toyota.Common.Web.Compression
{
    public class ZipCompress
    {
        private readonly static ICompression instance = new IonicZip();

        public static ICompression GetInstance()
        {
            return instance;            
        }

    }
}
