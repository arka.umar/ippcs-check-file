﻿/*
 NIIT Mulki
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ionic.Zip;
using System.IO;

namespace Toyota.Common.Web.Compression
{
    public class IonicZip : ICompression
    {

        /// <summary>
        /// Compress files to zip file
        /// </summary>
        /// <param name="zipFileToCreate">Set file zip name and directory</param>
        public bool Compress(Dictionary<string, byte[]> byteContents, Stream outputStream)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    foreach (string entryName in byteContents.Keys) { ZipEntry e = zip.AddEntry(entryName, byteContents[entryName]); }
                    zip.Save(outputStream);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Compress files to zip file
        /// </summary>
        /// <param name="zipFileToCreate">Set file zip name and directory</param>
        public bool Compress(string[] fileNames, string zipFileToCreate, ref string msg)
        {

            string ZipFileToCreate = zipFileToCreate;
            try
            {
                using (ZipFile zip = new ZipFile())
                { 
                    foreach (String filename in fileNames)
                    {
                        Console.WriteLine("Adding {0}...", filename);
                        ZipEntry e = zip.AddFile(filename);
                        //e.Comment = "Added by Betrack.";
                    }
                    
                    zip.Save(ZipFileToCreate);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString();
                return false;                
            }
        }

        /// <summary>
        /// Decompress file zip
        /// </summary>
        /// <param name="zipFileToCreate">Set file zip name and directory</param>
        public bool Decompress(string zipFileLocName, string extractLocation, ref string msg)
        {
            if (!Directory.Exists(Path.GetDirectoryName(extractLocation)))
            {
                msg = "The directory does not exist!\n";
                return false;
            }
            if (!System.IO.File.Exists(zipFileLocName))
            {
                msg = "That zipfile not exists!\n";
                return false;
            }
            if (!zipFileLocName.EndsWith(".zip"))
            {
                msg = "The filename must end with .zip!\n";
                return false;
            }

            try
            {
                var options = new ReadOptions { StatusMessageWriter = System.Console.Out };
                using (ZipFile zip = ZipFile.Read(zipFileLocName))
                {
                    zip.ExtractAll(extractLocation);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString();
                return false;
            }
        }

        /// <summary>
        /// Compress files to zip using password
        /// </summary>
        /// <param name="zipFileToCreate">Set file zip name and directory</param>
        public bool Compress(string[] fileNames, string zipFileToCreate, string passwdZip, ref string msg)
        {
            if (!zipFileToCreate.EndsWith(".zip"))
            {
                msg = "The filename must end with .zip!\n";
                return false;
            }
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.Password = passwdZip;

                    foreach (String filename in fileNames)
                    {
                        Console.WriteLine("Adding {0}...", filename);
                        ZipEntry e = zip.AddFile(filename);
                        //e.Comment = "Added by Betrack.";
                    }
                    
                    zip.Comment =
                        String.Format("This zip archive was created by the CreateZip example application on machine '{0}'",
                        System.Net.Dns.GetHostName());

                    zip.Save(zipFileToCreate);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString();
                return false;
            }
        }

        /// <summary>
        /// Decompress Zip's File with password
        /// </summary>
        /// <param name="zipFileToCreate">Set file zip name and directory</param>
        public bool Decompress(string zipFileLocName, string extractLocation, string passwdZip, ref string msg)
        {
            if (!System.IO.Directory.Exists(zipFileLocName))
            {
                msg = "The directory does not exist!\n";
                return false;
            }
            if (System.IO.File.Exists(zipFileLocName))
            {
                msg = "That zipfile already exists!\n";
                return false;
            }
            if (!zipFileLocName.EndsWith(".zip"))
            {
                msg = "The filename must end with .zip!\n";
                return false;
            }

            try
            {
                using (ZipFile zip = ZipFile.Read(zipFileLocName))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.ExtractWithPassword(extractLocation, passwdZip);  // overwrite == true
                    }
                }
                return true;
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directoryToZip"></param>
        /// <param name="zipFileToCreate">Set file zip name and directory</param>
        public bool CompressDir(string directoryToZip, string zipFileToCreate, ref string msg)
        {
            if (!zipFileToCreate.EndsWith(".zip"))
            {
                msg = "The filename must end with .zip!\n";
                return false;
            }
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(directoryToZip);

                    zip.Comment =
                        String.Format("This zip archive was created by the CreateZip example application on machine '{0}'",
                        System.Net.Dns.GetHostName());

                    zip.Save(zipFileToCreate);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString();
                return false;
            }
        }

    }
}
