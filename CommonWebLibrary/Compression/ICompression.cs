﻿/*
 NIIT Mulki
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Toyota.Common.Web.Compression
{
    public interface ICompression
    {
        bool Compress(Dictionary<string, byte[]> byteContents, Stream outputStream);
        bool Compress(string[] fileNames, string zipFileToCreate, string passwdZip, ref string msg);
        bool Compress(string[] fileNames, string zipFileToCreate, ref string msg);
        bool Decompress(string zipFileLocName, string extractLocation, ref string msg);
    }
}
