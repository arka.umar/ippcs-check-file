﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using System.Transactions;
using Toyota.Common.Web.Credential;
using Toyota.Common.Util;
using Toyota.Common.Web.Util;
using Newtonsoft.Json;

namespace Toyota.Common.Web.Workflow
{
    public class WorkflowFunction
    {
        private static IDBContext DbWorkflow
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_WORKFLOW);
            }
        }

        private static IDBContext DbPcs
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);                    
            }
        }

        public static List<WorkflowModel> GetWorklist(string UserName)
        {
            List<WorkflowModel> workflowModel = new List<WorkflowModel>();
            IDBContext db = DbWorkflow;
            workflowModel = db.Fetch<WorkflowModel>("GetDisplayWorklist", new object[] { UserName });
            db.Close();

            return workflowModel;
        }


        public static List<WorkflowModel> GetDivisionClass(string FolioNo)
        {
            List<WorkflowModel> workflowModel = new List<WorkflowModel>();
            IDBContext db = DbWorkflow;
            workflowModel = db.Fetch<WorkflowModel>("GetDivisionClass", new object[] { FolioNo });
            db.Close();

            return workflowModel;
        }

        public static Boolean RegisterWorklist(string FolioNo, string ModuleCd, int StatusCd, string ProceedBy)
        {
            Boolean ResultProcess = false;
            OpenLDAPService.SecurityCenterClient sc = new OpenLDAPService.SecurityCenterClient();
            IDBContext db = DbWorkflow;
            try
            {
                UserAD values = new UserAD();
                string json, returns;

                IDBContext dbPCS = DbPcs;
                string userSubmit = dbPCS.ExecuteScalar<string>("GetUserSubmit", new object[] { });
                dbPCS.Close();

                sc.Open();
                returns = sc.GetEmployeeDatas(new string[] { "username=" + userSubmit });

                List<UserAD> userPosition = JsonConvert.DeserializeObject<List<UserAD>>(returns);

                String userClass = userPosition[0].properties["positionid"][0];

                TimeSpan ts = new TimeSpan(0, 30, 0);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {


                    db.Execute("RegisterWorklist", new object[] { FolioNo, ModuleCd, userSubmit });

                    sc.Close();

                    ResultProcess = true;

                    scope.Complete();
                    
                }
            }
            catch (Exception ex)
            {
                ResultProcess = false;
                sc.Close();

                PutLog(ExPlain(ex), ProceedBy, "RegisterWorklist");

                throw ex;
            }
            finally
            {
                db.Close();
            }

            return ResultProcess;
        }


        public static string ExPlain(Exception e)
        {
            string r = "";
            if (e != null)
            {
                r = r + e.Message + "\r\n" + e.StackTrace;
                if (e.InnerException != null)
                    r = r + "\r\n" + e.InnerException.Message;
            }
            return r;
        }


        public static Boolean RegisterWorklistPC(string FolioNo, string ModuleCd, int StatusCd, string UserSubmitScreen)
        {
            Boolean ResultProcess = false;
            IDBContext db = DbWorkflow;
            OpenLDAPService.SecurityCenterClient sc = new OpenLDAPService.SecurityCenterClient();
            try
            {
                UserAD values = new UserAD();
                string json, returns;


                String userSubmit = db.ExecuteScalar<string>("GetUserIcsUserMapping", new object[] { UserSubmitScreen });

                values.properties.Add("username", new List<String> { userSubmit });

                sc.Open();

                returns = sc.GetEmployeeDatas(new string[] { "username=" + userSubmit });

                List<UserAD> userPosition = JsonConvert.DeserializeObject<List<UserAD>>(returns);

                String userClass = userPosition[0].properties["positionid"][0];
                string UnitIdICS = userPosition[0].properties["sectionid"][0];

                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {
                    db.ExecuteScalar<string>("RegisterWorklistPC", new object[] { FolioNo, ModuleCd, UserSubmitScreen });

                    sc.Close();

                    ResultProcess = true;
                    db.Close();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ResultProcess = false;
                sc.Close();
                PutLog(ExPlain(ex), UserSubmitScreen, "RegisterWorklistPC");
                throw ex;
            }
            finally
            {
                db.Close();
            }

            return ResultProcess;
        }


        public static int ApproveWorklist(string FolioNo, string ModuleCd, string ProceedBy, ref string ApproveMessage, int silent =0)
        {
            int ResultProcess = -1;
            List<String> ApproverList = new List<String>();
            ApproverList = GetNextApproverWorklist(FolioNo, ModuleCd);

            Boolean ApproverStatus = false;

            foreach (String NextApprover in ApproverList)
            {
                if (NextApprover == ProceedBy)
                {
                    ApproverStatus = true;
                }
            }

            if (ApproverStatus)
            {
                IDBContext db = DbWorkflow;
                try
                {
                    TimeSpan ts = new TimeSpan(0, 30, 0);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                    {

                        ApprovalModel m = db.SingleOrDefault<ApprovalModel>("ApproveWorklist", new object[] { FolioNo, ModuleCd, ProceedBy, silent });
                        if (!(m == null))
                        {
                            ResultProcess = m.FunctionReturn;
                            ApproveMessage = m.FunctionMessage;

                            db.ExecuteScalar<string>("UpdateStatusPO", new object[] { FolioNo, ProceedBy, ResultProcess });
                        }
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    PutLog(ExPlain(ex), ProceedBy, "ApproveWorklist");
                    ResultProcess = -2;
                }
                finally
                {
                    db.Close();
                }
            }

            return ResultProcess;
        }

        public static int ApproveWorklistPC(string FolioNo, string ModuleCd, string ProceedBy, ref string ApproveMessage, int silent = 0)
        {
            int ResultProcess = -1;
            List<String> ApproverList = new List<String>();
            ApproverList = GetNextApproverWorklist(FolioNo, ModuleCd);

            Boolean ApproverStatus = false;

            foreach (String NextApprover in ApproverList)
            {
                if (NextApprover == ProceedBy)
                {
                    ApproverStatus = true;
                }
            }

            if (ApproverStatus)
            {
                IDBContext db = DbWorkflow;
                try
                {
                    TimeSpan ts = new TimeSpan(0, 30, 0);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                    {
                        ApprovalModel m = db.SingleOrDefault<ApprovalModel>("ApproveWorklist", new object[] { FolioNo, ModuleCd, ProceedBy, silent });
                        if (m != null)
                        {
                            ResultProcess = m.FunctionReturn;
                            ApproveMessage = m.FunctionMessage;
                            if (ResultProcess >= 0)
                            {
                                db.ExecuteScalar<string>("UpdateStatusPC", new object[] { FolioNo, ProceedBy, ResultProcess });
                                scope.Complete();
                            }
                        }
                        else
                            scope.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    PutLog(ExPlain(ex), ProceedBy, "ApproveWorklistPC");
                    ResultProcess = -2;
                }
                finally
                {
                    db.Close();
                }
            }

            return ResultProcess;
        }

        public static int GetPositionLevel(string FolioNo, string ModuleCd)
        {
            int ResultProcess = 0;
            IDBContext db = DbWorkflow;
            try
            {
                ResultProcess = db.ExecuteScalar<int>("GetPositionLevel", new object[] { FolioNo, ModuleCd });
            }
            catch (Exception ex)
            {
                PutLog(ExPlain(ex), "?", "GetPositionLevel");
                ResultProcess = -1;
            }
            finally
            {
                db.Close();
            }
            return ResultProcess;
        }

        public static string GetRegistrant(string FolioNo, string ModuleCd)
        {
            IDBContext DB = DbWorkflow;
            string registrant = DB.SingleOrDefault<string>("GetRegistrant", new object[] { FolioNo, ModuleCd });
            DB.Close();

            return registrant;
        }

        public static List<String> GetNextApproverWorklist(string FolioNo, string ModuleCd)
        {
            List<String> approverList = new List<String>();
            IDBContext DB = DbWorkflow;
            approverList = DB.Fetch<String>("GetNextApproverWorklist", new object[] { FolioNo, ModuleCd });
            DB.Close();

            return approverList;
        }

        public static int RejectWorklist(string FolioNo, string ModuleCd, string ProceedBy, string RejectType, int silent = 0)
        {
            int ResultProcess = -1;
            List<String> ApproverList = new List<String>();
            ApproverList = GetNextApproverWorklist(FolioNo, ModuleCd);

            Boolean ApproverStatus = false;
            foreach (String NextApprover in ApproverList)
            {
                if (NextApprover == ProceedBy)
                {
                    ApproverStatus = true;
                }
            }

            if (ApproverStatus)
            {
                IDBContext db = DbWorkflow;
                try
                {
                    TimeSpan ts = new TimeSpan(0, 30, 0);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                    {
                        string x = db.ExecuteScalar<string>("RejectWorklist", new object[] { FolioNo, ModuleCd, ProceedBy, RejectType, silent });
                        int.TryParse(x, out ResultProcess);

                        db.ExecuteScalar<string>("UpdateStatusPO", new object[] { FolioNo, ProceedBy, ResultProcess });

                        db.Close();
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    PutLog(ExPlain(ex), ProceedBy, "RejectWorklist");
                    ResultProcess = -2;
                }
                finally
                {
                    db.Close();
                }
            }
            return ResultProcess;
        }

        public static int RejectWorklistPC(string FolioNo, string ModuleCd, string ProceedBy, string RejectType, int silent = 0)
        {
            int ResultProcess = -1;
            List<String> ApproverList = new List<String>();
            ApproverList = GetNextApproverWorklist(FolioNo, ModuleCd);

            Boolean ApproverStatus = false;
            foreach (String NextApprover in ApproverList)
            {
                if (NextApprover == ProceedBy)
                {
                    ApproverStatus = true;
                }
            }

            if (ApproverStatus)
            {
                IDBContext db = DbWorkflow;
                try
                {
                    TimeSpan ts = new TimeSpan(0, 30, 0);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                    {
                        string x = db.ExecuteScalar<string>("RejectWorklist", new object[] { FolioNo, ModuleCd, ProceedBy, RejectType, silent });
                        int.TryParse(x, out ResultProcess);

                        db.ExecuteScalar<string>("UpdateStatusPC", new object[] { FolioNo, ProceedBy, ResultProcess });

                        db.Close();
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    PutLog(ExPlain(ex), ProceedBy, "RejectWorklistPC");
                    ResultProcess = -2;
                }
                finally
                {
                    db.Close();
                }
            }
            return ResultProcess;
        }

        public static int RedirectWorklist(string FolioNo, string ModuleCd, string ProceedBy, int PositionLevel, int silent = 0)
        {
            int ResultProcess = -1;
            int PositionApprover = GetPositionLevel(FolioNo, ModuleCd);

            Boolean RedirectStatus = false;

            if (PositionLevel <= PositionApprover)
            {
                RedirectStatus = true;
            }

            if (RedirectStatus)
            {
                IDBContext db = DbWorkflow;
                try
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
                    {
                        ResultProcess = Convert.ToInt32(db.ExecuteScalar<string>("RedirectWorklist", new object[] { FolioNo, ModuleCd, ProceedBy, PositionLevel, silent}));
                        
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    PutLog(ExPlain(ex), ProceedBy, "RedirectWorklist");
                    ResultProcess = -2;
                }
                finally
                {
                    db.Close();
                }
            }

            return ResultProcess;
        }

        public static Boolean DeleteWorklist(string FolioNo, string ModuleCd, string ProceedBy)
        {
            Boolean ResultProcess = false;
            IDBContext db = DbWorkflow;
            try
            {
                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {

                    db.ExecuteScalar<string>("DeleteWorklist", new object[] { FolioNo, ModuleCd, ProceedBy });
                    ResultProcess = true;
                    
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                PutLog(ExPlain(ex), ProceedBy, "DeleteWorklist");
                ResultProcess = false;
            }
            finally
            {
                db.Close();
            }
            return ResultProcess;
        }

        public static int CheckOffStatus(string Attorney)
        {
            int ResultProcess = 0;
            IDBContext db = DbWorkflow;
            try
            {
                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {

                    ResultProcess = Convert.ToInt32(db.ExecuteScalar<string>("CheckOffStatus", new object[] { Attorney }));
                    
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                PutLog(ExPlain(ex), "?", "CheckOffStatus");
                ResultProcess = -1;
            }
            finally
            {
                db.Close();
            }

            return ResultProcess;
        }

        public static String RegPOA(string PoaNo, string Grantor, string Attorney, string Reason, int OfficeStatus, DateTime ValidFrom, DateTime ValidTo, string PoaType, string UserName)
        {
            String ResultProcess = "";
            Boolean ResultMoveCopy = true;

            int CheckAttorney = CheckOffStatus(Attorney);

            if (DateTime.Now < ValidTo)
            {
                if (CheckAttorney == 0)
                {
                    IDBContext db = DbWorkflow;
                    try
                    {
                        TimeSpan ts = new TimeSpan(0, 30, 0);
                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                        {

                            db.ExecuteScalar<string>("RegPOA", new object[] { PoaNo, Grantor, Attorney, Reason, OfficeStatus, ValidFrom, ValidTo, PoaType, UserName });
                            
                            scope.Complete();
                        }

                        if ((DateTime.Now >= ValidFrom) && (DateTime.Now <= ValidTo))
                        {
                            ResultMoveCopy = MoveCopyWorklist(PoaNo, Grantor, Attorney, PoaType, UserName);
                            if (ResultMoveCopy == false)
                            {
                                ResultProcess = "Failed Move or Copy Worklist";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        PutLog(ExPlain(ex), "?", "RegPoA");
                        ResultProcess = ex.Message;
                    }
                    finally
                    {
                        db.Close();
                    }
                }
                else if (CheckAttorney > 0)
                {
                    ResultProcess = "POA is not allowed because attorney is out office";
                }
            }
            else
            {
                ResultProcess = "Valid to date must greater than today";
            }

            return ResultProcess;
        }

        public static Boolean MoveCopyWorklist(string PoaNo, string Grantor, string Attorney, string PoaType, string UserName)
        {
            Boolean ResultProcess = false;
            IDBContext db = DbWorkflow;
            try
            {
                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {

                    db.ExecuteScalar<string>("MoveCopyWorklist", new object[] { PoaNo, Grantor, Attorney, PoaType, UserName });
                    ResultProcess = true;
                   
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                PutLog(ExPlain(ex), UserName, "MoveCopyWorklist");
                ResultProcess = false;
            }
            finally
            {
                db.Close();
            }

            return ResultProcess;
        }

        public static Boolean RevokePOA(string PoaNo, string Grantor, string Attorney)
        {
            Boolean ResultProcess = false;
            IDBContext db = DbWorkflow;
            try
            {
                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {

                    db.ExecuteScalar<string>("RevokePOA", new object[] { PoaNo, Grantor, Attorney });
                    ResultProcess = true;
                    db.Close();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                PutLog(ExPlain(ex), "?", "RevokePoA");
                ResultProcess = false;
            }
            finally
            {
                db.Close();
            }
            return ResultProcess;
        }


        

        public static string SplitLines(string v, string separator = ",", int maxCol = 10)
        {
            if (string.IsNullOrEmpty(v)) return v;
            string x = "";
            int i, j = v.Length, k = 0, l = 0;
            do
            {
                i = k;
                k = v.IndexOf(separator, i) + 1;
                if (k > i)
                {
                    l++;
                    x = x + v.Substring(i, k - i);

                    if ((l % maxCol) == 0)
                    {
                        x = x + Environment.NewLine;
                    }
                }
            }
            while (i < j && k > i);
            if (i > 0)
                x = x + v.Substring(i, v.Length - i);
            else
            {
                if (k == 0)
                    x = v;
            }

            return x;
        }

        public static void PutLog(
                string what, 
                string who, 
                string where, 
                int    aPid = 0, 
                string aId = "MPCS00002INF", 
                string aTyp = "INF", 
                string aModule = "2", 
                string aFunction="25999", 
                string aStatus = "0")
        {
            using (IDBContext DB = DbWorkflow)
            {
                long pid = 0;
                string id = "MPCS00002INF";
                string typ = "INF";
                string module = "2";
                string func = "25001";
                string sts = "0";
                try
                {
                    long x = DB.ExecuteScalar<long>("PutLog", new object[] 
                        { what , who, where
                          , aPid
                          , aId, aTyp
                          , aModule, aFunction, aStatus });
                }
                finally
                {
                    DB.Close();
                }
            }
            
        }
    }
}
