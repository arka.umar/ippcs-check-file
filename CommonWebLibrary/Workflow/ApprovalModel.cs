﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Workflow
{
    public class ApprovalModel
    {
        public int FunctionReturn { get; set; }
        public string FunctionMessage { get; set; } 
    }
}
