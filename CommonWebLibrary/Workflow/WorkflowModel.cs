﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Workflow
{
    public class WorkflowModel
    {
        public string FOLIO_NO { set; get; }
        public int MODULE_CD { set; get; }
        public string MODULE_NAME { set; get; }
        public int STATUS_CD { set; get; }
        public string STATUS_NAME { set; get; }
        public string PROCEED_BY { set; get; }
        public string PROCEED_NAME { set; get; }

        public string CLASS { set; get; }
        public string UNIT_ID { set; get; }
        public string UNIT_LEVEL { set; get; }
    }
}
