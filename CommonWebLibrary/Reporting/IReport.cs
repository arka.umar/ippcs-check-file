﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Toyota.Common.Web.Reporting
{
    public interface IReport
    {
        string Name { get; set; }
        string Title { get; set; }
        string Path { get; set; }
        string ExportType { get; set; }
        DataTable DataSource { get; set; }
    }
}
