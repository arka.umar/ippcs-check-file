﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Reporting
{
    public interface IReportQueryRetriever
    {
        IEnumerable<ReportQuery> GetQueryByGroup(string group);
        IEnumerable<ReportQuery> GetQueryByCategory(string category);
        IEnumerable<ReportQuery> GetQuery(string group, string category);
    }
}
