﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Reporting
{
    public class TablePropertiesModel
    {
        public string FIELD_NAME { set; get; }
        public string DISPLAY_NAME { set; get; }
        public int ORDER_DISPLAY { set; get; }
        public bool PK { set; get; }
        public string DATA_TYPE { set; get; }
        public string CRITERIA { set; get; }
        public string COMPARISON { set; get; }
        public bool DISPLAY_OPTION { set; get; }
        public string SORT { set; get; }
        public string COMPARISONFROM { set; get; }
        public string COMPARISONTO { set; get; }
        public string CRITERIAFROM { set; get; }
        public string CRITERIATO { set; get; }

    }
}
