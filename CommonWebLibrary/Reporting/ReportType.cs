﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Reporting;

namespace Toyota.Common.Web.Reporting
{
    class ReportType
    {
        public ReportType(string exportTo, string reportFileName, string reportTitle) {
            this.ExportTo = exportTo;
            this.ReportFileName = reportFileName;
            this.ReportTitle = reportTitle;
        }
        public const string ExportToXLS = "Xls";
        public const string ExportToPDF = "Pdf";
        public const string ExportToCSV = "Csv";

        public static ReportType ExportReportXls(string namaFile, string reportTitle){
            return new ReportType(ReportType.ExportToXLS, namaFile, reportTitle);
        }

        public static ReportType ExportReportPdf(string namaFile, string reportTitle)
        { 
            return new ReportType(ReportType.ExportToPDF,namaFile,reportTitle);
        }

        public static ReportType ExportReportCsv(string namaFile, string reportTitle)
        { 
            return new ReportType(ReportType.ExportToCSV,namaFile,reportTitle);
        }

        public string ExportTo { set; get; }
        public string ReportFileName { set; get; }
        public string ReportTitle { set; get; }
    }
}
