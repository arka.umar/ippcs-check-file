﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Telerik.Reporting.Drawing;
using System.Drawing;
using System.Reflection;

namespace Toyota.Common.Web.Reporting
{
    public class TelerikReport : IReport, IDisposable
    {
        private string _name;
        private string _title;
        private string _path;
        private string _exportType;
        private DataTable _datasource;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public string Path
        {
            get
            {
                return _path;
            }
            set
            {
                _path = value;
            }
        }

        public string ExportType
        {
            get
            {
                return _exportType;
            }
            set
            {
                _exportType = value;
            }
        }

        public System.Data.DataTable DataSource
        {
            get
            {
                return _datasource;
            }
            set
            {
                _datasource = value;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                if (_datasource != null)
                {
                    _datasource.Dispose();
                    _datasource = null;
                }
        }

        ~TelerikReport()
        {
            Dispose(false);
        }

        private Telerik.Reporting.TextBox CreateTxtHeader(string FieldName, int i)
        {
            Telerik.Reporting.TextBox txtHead = new Telerik.Reporting.TextBox();
            txtHead.Style.VerticalAlign = VerticalAlign.Middle;
            txtHead.Value = FieldName;
            return txtHead;
        }

        private Telerik.Reporting.TextBox CreateTxtDetail(string FieldName, int i)
        {
            Telerik.Reporting.TextBox txtHead = new Telerik.Reporting.TextBox();
            txtHead.Value = "=[" + FieldName + "]";
            txtHead.TextWrap = false;
            txtHead.Style.VerticalAlign = VerticalAlign.Middle;
            return txtHead;
        }

        public Telerik.Reporting.Report GenerateReportDefinition(System.Data.DataTable dataSource)
        {
            Telerik.Reporting.Report report1 = new Telerik.Reporting.Report();
            report1.Style.Font.Size = Unit.Pixel(10);
            report1.DataSource = dataSource;
            int count = (dataSource.Columns.Count);

            Unit x = Unit.Inch(0);
            Unit y = Unit.Inch(0);

            SizeU size = new SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5), Unit.Inch(0.2));
            Telerik.Reporting.ReportItemBase[] headColumnList = new Telerik.Reporting.ReportItem[count];
            Telerik.Reporting.ReportItemBase[] detailColumnList = new Telerik.Reporting.ReportItem[count];
            for (int column = 0; (column < count); column++)
            {
                int j = dataSource.Columns[column].ColumnName.Length;
                string columnName = dataSource.Columns[column].ColumnName;
                string columnCaption = dataSource.Columns[column].Caption;
                Telerik.Reporting.TextBox header = this.CreateTxtHeader(columnCaption, column);
                header.Location = new Telerik.Reporting.Drawing.PointU(x, y);
                header.Size = size;
                headColumnList[column] = header;
                Telerik.Reporting.TextBox textBox = this.CreateTxtDetail(columnName, column);
                textBox.Location = new Telerik.Reporting.Drawing.PointU(x, y);
                textBox.Size = size;
                detailColumnList[column] = textBox;
                x += Telerik.Reporting.Drawing.Unit.Inch(1);
            }
            Telerik.Reporting.ReportHeaderSection reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
            reportHeaderSection1.Height = new Unit(0.4, Telerik.Reporting.Drawing.UnitType.Cm);
            reportHeaderSection1.Style.BackgroundColor = Color.LightGray;
            reportHeaderSection1.Items.AddRange(headColumnList);
            Telerik.Reporting.DetailSection detailSection1 = new Telerik.Reporting.DetailSection();
            detailSection1.Height = new Unit(0.4, UnitType.Cm);
            detailSection1.Items.AddRange(detailColumnList);
            report1.Items.Add(reportHeaderSection1);
            report1.Items.Add(detailSection1);
            return report1;
        }

        public RenderingResult ExportTo()
        {
            ReportProcessor reportProcessor = new ReportProcessor();

            Telerik.Reporting.ObjectDataSource objectDataSource = new Telerik.Reporting.ObjectDataSource();
            //objectDataSource.DataSource = GetCust(); // GetData returns a DataTable
            objectDataSource.DataSource = _datasource; // GetData returns a DataTable

            Telerik.Reporting.Report rpt = GenerateReportDefinition(_datasource);

            rpt.DataSource = objectDataSource;

            Telerik.Reporting.InstanceReportSource reportSource = new Telerik.Reporting.InstanceReportSource();
            reportSource.ReportDocument = rpt;

            RenderingResult result = reportProcessor.RenderReport(_exportType, reportSource, null);

            return result;
        }

    }
}
