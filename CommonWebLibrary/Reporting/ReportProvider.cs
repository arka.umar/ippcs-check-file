﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Telerik.Reporting.Processing;
using Telerik.Reporting;

namespace Toyota.Common.Web.Reporting
{
    public class ReportProvider
    {
        IDBContext db;

        public ReportProvider(string DBNames)
        {
            db = DatabaseManager.GetInstance().GetContext(DBNames);
        }

        public ReportProcessor ReportProcess(string Report, Dictionary<string, string> SubReport, string[] Query, ReportParameterCollection Params)
        {
            return null;
        }

        public RenderingResult RenderReport(string Report, Dictionary<string, string> SubReport, string[] Query, ReportParameterCollection Params)
        {
            return null;
        }

    }
}
