﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Toyota.Common.Web.Reporting
{
    public class ReportQuery
    {
        public string Category { get; set; }

        public string Group { get; set; }

        public string Statement { get; set; }

        public object[] Parameter { get; set; }

        public IEnumerable<TablePropertiesModel> DataSource { get; set; }

    }
}
