﻿/*
 *  NIIT Mulki
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using System.Data;
using System.Reflection;
using Toyota.Common.Web.Credential;
namespace Toyota.Common.Web.Reporting
{
    public class DBQueryRetrieval : IReportQueryRetriever
    {
        private IDBContextManager dbManager;

        private string sqlquery;

        public DBQueryRetrieval()
        {
            this.dbManager = DatabaseManager.GetInstance();
        }

        public IEnumerable<ReportQuery> GetQueryByGroup(string group)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ReportQuery> GetQueryByCategory(string category)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ReportQuery> GetQuery(string group, string category)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetQuery<T>()
        {
            IDBContext db = dbManager.GetContext(DBContextNames.DB_TMMIN_ROLE);
            IEnumerable<T> val = db.Query<T>(sqlquery);
            db.Close();
            return val;
        }

        public IEnumerable<RetrievalGroup> GetGroups()
        {
            IDBContext db = dbManager.GetContext(DBContextNames.DB_COMMON_MASTER);
            IEnumerable<RetrievalGroup> val = db.Query<RetrievalGroup>("GetRetrievalGroup");
            db.Close();
            return val;
        }

        public IEnumerable<RetrievalCategory> GetCategories(string groupID)
        {
            string groupid = string.Format("%{0}%", groupID);
            IDBContext db = dbManager.GetContext(DBContextNames.DB_COMMON_MASTER);
            IEnumerable<RetrievalCategory> val = db.Query<RetrievalCategory>("GetRetrievalCategory", new object[] { groupid });
            db.Close();
            return val;
        }

        public DataTable ListToDataTable<T>(IEnumerable<T> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (PropertyInfo pi in pis)
                    {
                        dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable GetProperties<T>(IEnumerable<T> data)
        {
            DataTable dt = new DataTable();

            Type t = typeof(T);

            PropertyInfo[] x = t.GetProperties();
            foreach (PropertyInfo xTmp in x)
            {
                dt.Columns.Add(xTmp.Name, typeof(string));
            }

            /* get Value and Data Type of T */

            //PetaPoco.PrimaryKeyAttribute isPK = (PetaPoco.PrimaryKeyAttribute)Attribute.GetCustomAttribute(t, typeof(PetaPoco.PrimaryKeyAttribute));

            T curr;

            IEnumerator<T> idata = data.GetEnumerator();
            while (idata.MoveNext())
            {

                object[] val = new object[dt.Columns.Count];
                curr = idata.Current;
                int l = 0;
                foreach (PropertyInfo xTmp in x)
                {

                    val[l] = t.GetProperty(xTmp.Name).GetValue(curr, BindingFlags.CreateInstance, null, null, null);
                    string dataType = xTmp.PropertyType.Name;

                    l += 1;
                }

                dt.Rows.Add(val);

            }


            TypeAttributes attrs = t.Attributes;
            
            return dt;
        }

        public DataTable SetQueryProperties(DataTable data)
        {
            DataTable datatable = new DataTable();

            datatable.Columns.Add("FieldName", typeof(string));
            datatable.Columns.Add("DisplayName", typeof(string));
            datatable.Columns.Add("OrderDisplay", typeof(int));
            datatable.Columns.Add("PK", typeof(bool));
            datatable.Columns.Add("DataType", typeof(string));
            datatable.Columns.Add("Criteria", typeof(string));
            datatable.Columns.Add("DisplayOption", typeof(bool));
            datatable.Columns.Add("Sort", typeof(string));

            DataRow row;

            //buat nyari PK
            //DataColumn[] colArr;
            //colArr = data.PrimaryKey;
            //for (int i = 0; i < colArr.Length; i++)
            //{
            //    string ass = colArr[i].ColumnName + colArr[i].DataType;
            //}

            int i = 0;
            foreach (DataColumn col in data.Columns)
            {
                i += 1;
                row = datatable.NewRow();
                row["FieldName"] = col.ColumnName;
                row["DisplayName"] = "";
                row["OrderDisplay"] = i;
                row["PK"] = col.Unique;
                row["DataType"] = col.DataType.Name;
                row["Criteria"] = "";
                row["DisplayOption"] = true;
                row["Sort"] = "Asc";
                datatable.Rows.Add(row);
            }

            return datatable;
        }

        public void UpdateQueryProperties(DataTable data, TablePropertiesModel editable)
        {
            DataRow[] row = data.Select("FIELD_NAME = '" + editable.FIELD_NAME + "'");

            data.Rows[0].BeginEdit();
            //row[0]["FieldName"] = editable.FieldName;
            row[0]["DISPLAY_NAME"] = editable.DISPLAY_NAME;
            //row[0]["PK"] = editable.PK;
            //row[0]["DataType"] = editable.DataType;
            row[0]["CRITERIA"] = editable.CRITERIA;
            row[0]["COMPARISON"] = editable.COMPARISON;
            row[0]["ORDER_DISPLAY"] = editable.ORDER_DISPLAY;
            row[0]["DISPLAY_OPTION"] = editable.DISPLAY_OPTION;
            row[0]["CRITERIAFROM"] = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(editable.CRITERIAFROM)); 
            row[0]["COMPARISONFROM"] = editable.COMPARISONFROM;
            row[0]["CRITERIATO"] = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(editable.CRITERIATO)); 
            row[0]["COMPARISONTO"] = editable.COMPARISONTO;
            row[0]["SORT"] = editable.SORT;
            data.Rows[0].EndEdit();
            data.AcceptChanges();
        }

        public DataTable QueryGenerate<T>(DataTable datasource, IEnumerable<T> datalist)
        {
            DataTable tmpdata = GetProperties<T>(datalist);

            DataRow[] rowfalse = datasource.Select("Displayoption = false", "OrderDisplay ASC");
            DataRow[] row = datasource.Select("Displayoption = true", "OrderDisplay ASC");
            string valQuery = "SELECT ";
            string[] fieldName = new string[row.Count()];
            string[] displayName = new string[row.Count()];
            string[] orderDisplay = new string[row.Count()];
            string[] filterDisplay = new string[row.Count()];
            int j = 0;
            foreach (DataRow r in row)
            {
                if (Convert.ToString(r[1]).Equals(string.Empty))
                    fieldName[j] = "[" + Convert.ToString(r[0]) + "]";
                else
                    fieldName[j] = "[" + Convert.ToString(r[0]) + "]" + " AS [" + Convert.ToString(r[1]) + "]";
                if (!Convert.ToString(r[1]).Equals(string.Empty))
                    displayName[j] = Convert.ToString(r[1]);
                else
                    displayName[j] = Convert.ToString(r[0]);
                orderDisplay[j] = Convert.ToString(r[0]) + " " + Convert.ToString(r[7]);

                if (!Convert.ToString(r[5]).Equals(string.Empty))
                    filterDisplay[j] = Convert.ToString(r[0]) + " " + Convert.ToString(r[5]);
                j += 1;
            }

            valQuery = valQuery + string.Join(", ", fieldName) + " FROM (" + sqlquery + ") as tabel";

            foreach (DataRow r in rowfalse)
            {
                tmpdata.Columns.Remove(Convert.ToString(r[0]));
            }

            string valFilterDisplay = "";
            for (int i = 0; i < filterDisplay.Length; i++)
            {
                string filter = filterDisplay[i];
                if (filter != null)
                    valFilterDisplay += filter + ", ";
            }
            if (valFilterDisplay.Equals(string.Empty))
                valFilterDisplay = "";
            else
                valFilterDisplay = valFilterDisplay.Substring(0, valFilterDisplay.Length - 2);

            string valOrderDisplay = string.Join(", ", orderDisplay);

            DataRow[] tmpdataSelect = tmpdata.Select(valFilterDisplay, valOrderDisplay);

            DataTable viewTable = tmpdata.Clone();
            for (int i = 0; i < row.Count(); i++)
            {
                viewTable.Columns[i].Caption = displayName[i];
            }
            for (int i = 0; i < tmpdataSelect.Length; i++)
            {
                
                viewTable.ImportRow(tmpdataSelect[i]);
            }

            return viewTable;
        }

        public string SQLQuery {
            get
            {
                return sqlquery;
            }
            set
            {
                sqlquery = value;
            }
        }

        public object Obj { set; get; }

        public IEnumerable<TablePropertiesModel> GetTableProperties()
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_TMMIN_ROLE);

            //var hood = data.Cast<IDictionary<string, object>>().ToDataSource();
            IEnumerable<object> QueryLogin = db.Query<object>("GetColumnNames", new object[] { "TB_M_USER" });
            //var hood = QueryLogin.Cast<IDictionary<string, object>>().ToDataSource();
            Type t;
            List<TablePropertiesModel> lst = new List<TablePropertiesModel>();
            foreach (object wew in QueryLogin)
            {
                lst.Add(new TablePropertiesModel
                {
                    FIELD_NAME = ((dynamic)((System.Dynamic.ExpandoObject)(wew))).ColumnName,
                    DATA_TYPE = ((dynamic)((System.Dynamic.ExpandoObject)(wew))).DataType
                });
            }

            //DataSourceCreator.ToDataSource(QueryLogin            //  //GenerateData().ToDataSource();
            return lst;
        }
    }
}
