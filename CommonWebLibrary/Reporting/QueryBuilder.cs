﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Toyota.Common.Web.Reporting
{
    public class QueryBuilder
    {
        private string TableName;
        private ArrayList Fields;
        private ArrayList Filters;
        private ArrayList VarValue;

        public QueryBuilder(string tableName)
        {
            TableName = tableName.Contains(' ') ? string.Format("({0}) tempTable", tableName) : tableName;
            Fields = new ArrayList();
            Filters = new ArrayList();
            VarValue = new ArrayList();
        }

        public void AddColumn(string columnName)
        {
            Fields.Add(columnName);
        }

        public void RemoveColumn(string columnName)
        {
            Fields.Remove(columnName);
        }

        public void AddFilter(string columnFilter, string filterComparison, string filterValue, string dataType, string databaseType)
        {
            if (filterComparison.ToLower().Contains("like") && filterValue.Contains(";"))
            {
                string[] _FilterValue = filterValue.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                string[] newFilterValue = new string[_FilterValue.Length];

                for (int i = 0; i < _FilterValue.Length; i++)
                {
                    newFilterValue[i] = columnFilter + " " + filterComparison + " '" + _FilterValue[i] + "'";
                }
                Filters.Add("(" + string.Join(" or ", newFilterValue) + ")");
            }
            else
            {
                switch (dataType.ToLower())
                {
                    case "string":
                        Filters.Add(columnFilter + " " + filterComparison + " " + "'" + filterValue + "'");
                        break;
                    case "datetime":
                        string columnfilter = string.Empty;
                        string filtervalue = string.Empty;

                        switch (databaseType.ToLower())
                        {
                            case "oracle.dataaccess.client":
                                filtervalue = "to_date('" + filterValue + "', 'mm/dd/yyyy')";
                                Filters.Add(columnFilter + " " + filterComparison + " " + "'" + filtervalue + "'");
                                break;
                            default:
                                columnfilter = "CONVERT(nvarchar(30), " + columnFilter + ", 101)";
                                filtervalue = "CONVERT(nvarchar(30), " + filterValue + ", 101)";
                                Filters.Add(columnfilter + " " + filterComparison + " " + "'" + filterValue + "'");

                                break;
                        }
                        //"CONVERT(nvarchar(30), " + columnFilter + ", 101)"
                        //Filters.Add(columnFilter + " " + filterComparison + " " + "'" + filterValue + "'");
                        break;
                    default:
                        Filters.Add(columnFilter + " " + filterComparison + " " + filterValue);
                        break;
                }
            }
        }

        public ArrayList ListColumns()
        {
            return Fields;
        }

        public string GetQuery()
        {
            string tmpString = string.Empty;

            tmpString = "SELECT ";

            if (Fields.Count == 0)
                throw new Exception("No Columns Selected");
            else
                tmpString += string.Join(",", ToStringArray(Fields.ToArray()));

            tmpString += " FROM " + TableName;

            if (Filters.Count != 0)
                tmpString += " WHERE " + string.Join(" AND ", ToStringArray(Filters.ToArray()));

            return tmpString;
        }

        private string EscapeString(string str)
        {
            string m_str = string.Empty;
            if (str != null)
            {
                if (str.Length > 0)
                {
                    m_str = str.Replace(@"\", @"\\");
                    m_str = m_str.Replace("'", @"\'");
                }
            }
            return m_str;
        }

        private string[] ToStringArray(object[] objValue)
        {
            string[] stringArray = new string[objValue.Length];

            for (int i = 0; i < objValue.Length; i++)
            {
                stringArray[i] = objValue[i].ToString();
            }
            return stringArray;
        }
    }
}
