﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Lookup;

namespace Toyota.Common.Web.Lookup
{
    public class SystemLookup
    {
        private static readonly ILookup lookup = new SimpleLookup();

        private SystemLookup() { }
        public static ILookup GetInstance()
        {
            return lookup;
        }
    }
}
