﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Util
{
    public class ProgressStatus
    {
        public const byte PROCESS_STATUS_SUCCESS = 0;
        public const byte PROCESS_STATUS_ERROR = 1;
        public const byte PROCESS_STATUS_SUCCESS_WITH_WARNING = 2;
        public const byte PROCESS_STATUS_SUSPENDED = 3;
        public const byte PROCESS_STATUS_ABORTED = 4;
        public const byte PROCESS_STATUS_PROCESSING = 5;

    }
}
