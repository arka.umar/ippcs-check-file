﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Toyota.Common.Web.Util
{
    public class ReflectionUtil
    {
        public static T Instantiate<T>(String typeName)
        {
            return Instantiate<T>(typeName, null);
        }
        public static T Instantiate<T>(String typeName, params object[] args)
        {
            Type type = Type.GetType(typeName);
            if ((args != null) && (args.Length > 0))
            {
                return (T)Activator.CreateInstance(type, args);
            }
            return (T) Activator.CreateInstance(type);
        }
    }
}
