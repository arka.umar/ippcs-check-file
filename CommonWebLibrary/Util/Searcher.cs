﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace Toyota.Common.Web.Util
{
    /* 28 jan 2013 niit.yudha */
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true, Name = "FullTrust")]
    public class Searcher
    {

        public delegate void StatusChangeEventArgs(Searcher.ScanStatus Status);

        private string _SearchPath;
        private ScanStatus _status;
        private SearchRoutine _searchroutine;
        private bool _IncludeSubfolder;

        private System.Threading.ManualResetEvent _ManualEvent;
        public delegate void SearchRoutine(string filename);
        public event EventHandler Finish;
        public event EventHandler Begin;
        public event StatusChangeEventArgs StatusChanging;

        /// <summary>
        /// Initializes a new instance of the <see cref="Searcher" /> class.
        /// </summary>
        [DebuggerStepThrough()]
        public Searcher()
        {
            Finish += Searcher_Finish;
            Begin += Searcher_Begin;
            this._ManualEvent = new System.Threading.ManualResetEvent(false);
            this._IncludeSubfolder = true;
            this._status = ScanStatus.Ready;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Searcher" /> class.
        /// </summary>
        /// <param name="path">The path.</param>
        [DebuggerStepThrough()]
        public Searcher(string path)
            : this()
        {
            this._SearchPath = path;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Searcher" /> class.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="searchroutine">The searchroutine.</param>
        [DebuggerStepThrough()]
        public Searcher(string path, SearchRoutine searchroutine)
            : this()
        {
            this._SearchPath = path;
            this._searchroutine = searchroutine;
        }

        /// <summary>
        /// Gets or sets the search path.
        /// </summary>
        /// <value>The search path.</value>
        public virtual string SearchPath
        {
            [DebuggerStepThrough()]
            get { return this._SearchPath; }
            [DebuggerStepThrough()]
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                else
                {
                    this._SearchPath = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [include subfolder].
        /// </summary>
        /// <value><c>true</c> if [include subfolder]; otherwise, <c>false</c>.</value>
        public bool IncludeSubfolder
        {
            [DebuggerStepThrough()]
            get { return this._IncludeSubfolder; }
            [DebuggerStepThrough()]
            set { this._IncludeSubfolder = value; }
        }

        /// <summary>
        /// Gets or sets the search call back.
        /// </summary>
        /// <value>The search call back.</value>
        public virtual SearchRoutine SearchCallBack
        {
            [DebuggerStepThrough()]
            get { return _searchroutine; }
            [DebuggerStepThrough()]
            set { this._searchroutine = value; }
        }

        /// <summary>
        /// Gets or sets the manual event.
        /// </summary>
        /// <value>The manual event.</value>
        public System.Threading.ManualResetEvent ManualEvent
        {
            [DebuggerStepThrough()]
            get { return this._ManualEvent; }
            [DebuggerStepThrough()]
            set { this._ManualEvent = value; }
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        [DebuggerStepThrough()]
        public virtual void Stop()
        {
            this._status = ScanStatus.Stopped;
            this.OnStatusChange();
        }

        /// <summary>
        /// Resumes this instance.
        /// </summary>
        [DebuggerStepThrough()]
        public void Resume()
        {
            this._status = ScanStatus.Running;
            this._ManualEvent.Set();
            this.OnStatusChange();
        }

        /// <summary>
        /// Suspends this instance.
        /// </summary>
        [DebuggerStepThrough()]
        public void Suspend()
        {
            this._status = ScanStatus.Pause;
            this._ManualEvent.Reset();
            this.OnStatusChange();
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        [DebuggerStepThrough()]
        public virtual void Start()
        {
            this.OnBegin();
            this.BeginStart();
            this.OnFinish();
        }

        [DebuggerStepThrough()]
        protected internal void BeginStart()
        {
            this._Start(_SearchPath);
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Searcher" /> is running.
        /// </summary>
        /// <value><c>true</c> if running; otherwise, <c>false</c>.</value>
        public virtual ScanStatus Status
        {
            [DebuggerStepThrough()]
            get { return this._status; }
        }

        /// <summary>
        /// start searching.
        /// </summary>
        /// <param name="searchPath">The search path.</param>
        [DebuggerStepThrough()]
        private void _Start(object searchPath)
        {
            string DirectoryPath = Path.GetDirectoryName(Convert.ToString(searchPath));
            string FileName = Path.GetFileName(Convert.ToString(searchPath));
            string[] Directories = null;
            string[] Files = null;
            if (DirectoryPath == null | FileName == null)
            {
                this.OnFinish();
                return;
            }
            if (this._status == ScanStatus.Stopped)
                return;
            try
            {
                Files = Directory.GetFiles(DirectoryPath, FileName);
                foreach (string file in Files)
                {
                    this._searchroutine.Invoke(file);
                    if (this._ManualEvent != null)
                    {
                        this._ManualEvent.WaitOne();
                    }
                }

            }
            catch (Exception e)
            {
            }
            if (_IncludeSubfolder)
            {
                try
                {
                    Directories = Directory.GetDirectories(DirectoryPath);
                    foreach (string SubDir in Directories)
                    {
                        this._searchroutine.Invoke(SubDir);
                        if (this._status == ScanStatus.Stopped)
                            return;
                        this._Start(Path.Combine(SubDir, FileName));
                    }

                }
                catch (Exception ex)
                {
                }
            }
        }

        /// <summary>
        /// Called when [begin].
        /// </summary>
        [DebuggerStepThrough()]
        protected virtual void OnBegin()
        {
            if (Begin != null)
            {
                Begin(this, null);
            }
        }

        /// <summary>
        /// Called when [finish].
        /// </summary>
        [DebuggerStepThrough()]
        protected virtual void OnFinish()
        {
            if (Finish != null)
            {
                Finish(this, null);
            }
        }

        /// <summary>
        /// Called when [status change].
        /// </summary>
        [DebuggerStepThrough()]
        protected virtual void OnStatusChange()
        {
            if (StatusChanging != null)
            {
                StatusChanging(this.Status);
            }
        }

        /// <summary>
        /// Handles the Begin event of the Searcher control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        [DebuggerStepThrough()]
        private void Searcher_Begin(object sender, System.EventArgs e)
        {
            this._status = ScanStatus.Running;
            if (this._ManualEvent != null)
            {
                this._ManualEvent.Set();
            }
            this.OnStatusChange();
        }

        /// <summary>
        /// Handles the Finish event of the Searcher control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        [DebuggerStepThrough()]
        private void Searcher_Finish(object sender, System.EventArgs e)
        {
            this._status = ScanStatus.Ready;
            if (this._ManualEvent != null)
            {
                this._ManualEvent.Reset();
            }
            this.OnStatusChange();
        }

        public enum ScanStatus
        {
            Ready = 0,
            Running = 1,
            Pause = 2,
            Stopped = 3
        }

    }
}
