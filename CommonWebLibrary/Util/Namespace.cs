﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Util
{
    public class Namespace
    {
        public static String DefaultNamespace
        {
            get
            {
                return "Toyota.Common.Web";
            }
        }

        public static String InternalResourceNamespace
        {
            get
            {
                return DefaultNamespace + "." + "resources";
            }
        }

        public static String InternalConfigurationResourceNamespace
        {
            get
            {
                return InternalResourceNamespace + "." + "config";
            }
        }

        public static String InternalSQLResourceNamespace
        {
            get
            {
                return InternalResourceNamespace + "." + "sql";
            }
        }
    }
}
