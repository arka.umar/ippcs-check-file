﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Web;
using System.Configuration;
using System.Web.SessionState;

namespace Toyota.Common.Web.Util
{
    public class RuntimeManager
    {

        private static string appDomainID = "";

        public static string CurrentID
        { get { return appDomainID; } }

        /// <summary>
        /// Replace AppDomainAppID at HttpRuntime to AppName while HttpApplication is being created.
        /// </summary>
        /// <param name="AppName">The App Domain App ID</param>
        /// <param name="restartDomain">Restart the domain if true</param>
        /// <returns>return the AppDomainAppID, if AppName null, it will be set to "DefaultDomainID"</returns>
        public static string Initialize(string AppName = "", bool restartRuntime = false)
        {

            FieldInfo runtimeInfo;

            if (restartRuntime)
            {
                HttpRuntime.UnloadAppDomain();
                //// force to Init
                runtimeInfo = typeof(HttpRuntime).GetField("_theRuntime",
                    BindingFlags.Static | BindingFlags.NonPublic); ;
                typeof(HttpRuntime).InvokeMember("Init", BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Instance, null, (HttpRuntime)runtimeInfo.GetValue(null), null);
            }

            runtimeInfo = typeof(HttpRuntime).GetField("_theRuntime",
                BindingFlags.Static | BindingFlags.NonPublic);
            HttpRuntime theRuntime = (HttpRuntime)runtimeInfo.GetValue(null);
            FieldInfo _appDomainAppId = typeof(HttpRuntime).GetField("_appDomainAppId",
                BindingFlags.Instance | BindingFlags.NonPublic);

            string appDomainID3;
            if (!string.IsNullOrEmpty(AppName))
                appDomainID3 = AppName;
            else
                appDomainID3 = "DefaultDomainID";

            _appDomainAppId.SetValue(theRuntime, appDomainID3);

            appDomainID = appDomainID3;
            return appDomainID3;
        }

    }
}
