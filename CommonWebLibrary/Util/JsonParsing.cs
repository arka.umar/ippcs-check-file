﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Toyota.Common.Web.Util
{
    public class JsonParsing
    {
        public static string DicToJson(Object objJson)
        {
            string json = JsonConvert.SerializeObject(objJson, Formatting.None);
            String regexPattern = @"\[(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\=*\!*)*([A-Za-z0-9\s])*\]";
            String regexStartDel = @"\{(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\=*\!*\""*\\*)*([A-Za-z0-9\s])*";

            Regex regex = new Regex(regexPattern);
            Regex regexDel = new Regex(regexStartDel);

            String jsonTemp;
            jsonTemp = json.ToString();
            jsonTemp = jsonTemp.Replace("\",\"", "#@#!#@#");
            jsonTemp = jsonTemp.Replace("\"", "");

            String jasonText;
            String jasonReplace;
            String jasonFinal = json;

            foreach (Match match in regex.Matches(jsonTemp))
            {
                if (match.Value.Contains(","))
                {
                    string[] jsonSplit = Regex.Split(match.Value, "!");
                    foreach (string JsonSplit in jsonSplit)
                    {
                        jasonText = JsonSplit.Replace("[", "[\"").Replace("]", "\"]").Replace("#@#", "\",\"");
                        jasonFinal = jasonFinal.Replace("\"]},{\"", "\",\"");
                        jasonReplace = jasonText.Replace("[", "[{").Replace("]", "\"]}]").Replace("=", "\":[\"").Replace(",", "\"],\"").Replace("\"\"],\"\"", "\"]},{\"");
                        jasonFinal = jasonFinal.Replace(jasonText, jasonReplace);
                    }

                }
            }

            String jsonStart;
            foreach (Match match in regexDel.Matches(jasonFinal))
            {
                jsonStart = match.Value + ":{";

                if (jasonFinal.Contains(jsonStart))
                {
                    jasonFinal = jasonFinal.Replace(jsonStart, "{");
                }
            }

            jasonFinal = jasonFinal.Replace("\",\"", "\"]},{\"");

            jasonFinal = jasonFinal.Replace("[\"", "\"").Replace("\"]", "\"");
            jasonFinal = jasonFinal.Replace("\"\"", "\"").Replace("}}", "}");
            return jasonFinal;
        }

        public static string DicToJson2(Object objJson)
        {
            string json = JsonConvert.SerializeObject(objJson, Formatting.None);
            String regexPattern = @"\[(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\=*\!*)*([A-Za-z0-9\s])*\]";
            String regexStartDel = @"\{(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\=*\!*\""*\\*)*([A-Za-z0-9\s])*";

            Regex regex = new Regex(regexPattern);
            Regex regexDel = new Regex(regexStartDel);

            String jsonTemp;
            jsonTemp = json.ToString();
            jsonTemp = jsonTemp.Replace("\",\"", "#@#!#@#");
            jsonTemp = jsonTemp.Replace("\"", "");

            String jasonText;
            String jasonReplace;
            String jasonFinal = json;

            foreach (Match match in regex.Matches(jsonTemp))
            {
                if (match.Value.Contains(","))
                {
                    string[] jsonSplit = Regex.Split(match.Value, "!");
                    foreach (string JsonSplit in jsonSplit)
                    {
                        jasonText = JsonSplit.Replace("[", "[\"").Replace("]", "\"]").Replace("#@#", "\",\"");
                        jasonFinal = jasonFinal.Replace("\"]},{\"", "\",\"");
                        jasonReplace = jasonText.Replace("[", "[{").Replace("]", "\"]}]").Replace("=", "\":[\"").Replace(",", "\"],\"").Replace("\"\"],\"\"", "\"]},{\"");
                        jasonFinal = jasonFinal.Replace(jasonText, jasonReplace);
                    }

                }
            }

            String jsonStart;
            foreach (Match match in regexDel.Matches(jasonFinal))
            {
                jsonStart = match.Value + ":{";

                if (jasonFinal.Contains(jsonStart))
                {
                    jasonFinal = jasonFinal.Replace(jsonStart, "{");
                }
            }

            jasonFinal = jasonFinal.Replace("\",\"", "\"]},{\"");

            jasonFinal = jasonFinal.Replace("\"\"", "\"").Replace("}}", "}");
            return jasonFinal;
        }

        public static Dictionary<String, List<String>> JsonToDic(string json)
        {
            Dictionary<String, List<String>> result = new Dictionary<string, List<string>>();

            json = json.Replace("\"", "");

            //String regexMultiS = @"\{(([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)](?:,))*(([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)])*}";
            //String regexMultiS2 = @"([A-Za-z0-9\s]*):\[({(([A-Za-z0-9\s]*)=((([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))(?:,))*(([A-Za-z0-9\s]*)=((([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)))*}(?:,))*{(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)(?:,))*(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*}]";
            //String regexMultiVal = @"\{(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)(?:,))*(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*}";

            String regexMultiS = @"\[{([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\:*\=*\!*\""*\:*\[*\]*)*([A-Za-z0-9\s])*";
            String regexMultiS2 = @"([A-Za-z0-9\s]*):\[{(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\:*\=*\!*\/*\:*\{*\}*)*([A-Za-z0-9\s])*\]";
            String regexMultiVal = @"\{(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\=*\!*\/*\:*)*([A-Za-z0-9\s])*\}";

            //String regexPattern = @"([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*|([A-Za-z0-9\s])*(\.)*([A-Za-z0-9\s])*\@([A-Za-z0-9\s])*((\.)([A-Za-z0-9\s])*)*)]";
            //String regexKey = @"([A-Za-z0-9\s]*):";
            //String regexVal = @"\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*|([A-Za-z0-9\s])*(\.)*([A-Za-z0-9\s])*\@([A-Za-z0-9\s])*((\.)([A-Za-z0-9\s])*)*)]";

            String regexPattern = @"([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\:*\=*\!*\/*\:*)*([A-Za-z0-9\s])*\]";
            String regexKey = @"([A-Za-z0-9\s]*):\[";
            String regexVal = @"\[(([A-Za-z0-9\s])*\.*\,*\#*\&*\@*\=*\!*\/*\:*)*([A-Za-z0-9\s])*\]";

            Regex regexMulti = new Regex(regexMultiS);
            Regex regexMulti2 = new Regex(regexMultiS2);
            Regex regexMultiValue = new Regex(regexMultiVal);
            Regex regex = new Regex(regexPattern);

            String jsonTemp, jsonReplace;
            String keyJson, valJson;

            jsonTemp = json;
            String jsonGroup;
            foreach (Match match in regexMulti.Matches(jsonTemp))
            {
                jsonGroup = match.Value + "}";

                if (jsonTemp.Contains(jsonGroup))
                {
                    jsonReplace = match.Value.Replace(":", "=").Replace("[", "").Replace("]", "");
                    jsonTemp = jsonTemp.Replace(match.Value, jsonReplace);
                }
            }

            foreach (Match match in regex.Matches(jsonTemp))
            {
                keyJson = Regex.Match(match.Value, regexKey).Groups[0].Value.Replace(":", "").Replace("[", "");
                valJson = Regex.Match(match.Value, regexVal).Groups[0].Value.Replace("[", "").Replace("]", "");

                result.Add(keyJson, new List<String> { valJson });
            }


            foreach (Match match in regexMulti2.Matches(jsonTemp))
            {
                List<String> tempVal = new List<string>();
                foreach (Match match2 in regexMultiValue.Matches(match.Value))
                {
                    tempVal.Add(match2.Value.Replace("{", "").Replace("}", ""));
                }

                keyJson = Regex.Match(match.Value, regexKey).Groups[0].Value.Replace(":", "").Replace("[", "");
                result.Add(keyJson, tempVal);
            }

            return result;
        }

        //public static string DicToJson(Object objJson)
        //{
        //    string json = JsonConvert.SerializeObject(objJson, Formatting.None);

        //    String regexPattern = @"\[([A-Za-z0-9\s]*)(\=+)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)((?:,)([A-Za-z0-9\s]*)(?:=)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*]";
        //    String regexPattern1 = @"\[([A-Za-z0-9\s]*)(\=+)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)((?:,)([A-Za-z0-9\s]*)(?:=)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*#@#";
        //    String regexPattern2 = @"\#@#([A-Za-z0-9\s]*)(\=+)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)((?:,)([A-Za-z0-9\s]*)(?:=)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*#@#";
        //    String regexPattern3 = @"\#@#([A-Za-z0-9\s]*)(\=+)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)((?:,)([A-Za-z0-9\s]*)(?:=)(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*]";

        //    Regex regex = new Regex(regexPattern);
        //    Regex regex1 = new Regex(regexPattern1);
        //    Regex regex2 = new Regex(regexPattern2);
        //    Regex regex3 = new Regex(regexPattern3);

        //    String jsonTemp;
        //    jsonTemp = json.ToString();
        //    jsonTemp = jsonTemp.Replace("\",\"", "#@##@#");
        //    jsonTemp = jsonTemp.Replace("\"", "");

        //    String jasonText;
        //    String jasonReplace;
        //    String jasonFinal = json;

        //    foreach (Match match in regex.Matches(jsonTemp))
        //    {
        //        jasonText = match.Value.Replace("[", "[\"").Replace("]", "\"]");
        //        jasonReplace = jasonText.Replace("[", "[{").Replace("]", "\"]}]").Replace("=", "\":[\"").Replace(",", "\"],\"").Replace("\",\"", "\"]},{\"");
        //        jasonFinal = jasonFinal.Replace(jasonText, jasonReplace);
        //    }

        //    foreach (Match match in regex1.Matches(jsonTemp))
        //    {
        //        jasonText = match.Value.Replace("[", "[\"").Replace("]", "\"]").Replace("#@#", "\",\"");
        //        jasonReplace = jasonText.Replace("[", "[{").Replace("]", "\"]}]").Replace("=", "\":[\"").Replace(",", "\"],\"").Replace("\"\"],\"\"", "\"]},{\"");
        //        jasonFinal = jasonFinal.Replace(jasonText, jasonReplace);
        //    }

        //    foreach (Match match in regex2.Matches(jsonTemp))
        //    {
        //        jasonText = match.Value.Replace("[", "[\"").Replace("]", "\"]").Replace("#@#", "\",\"");
        //        jasonFinal = jasonFinal.Replace("\"]},{\"", "\",\"");
        //        jasonReplace = jasonText.Replace("[", "[{").Replace("]", "\"]}]").Replace("=", "\":[\"").Replace(",", "\"],\"").Replace("\"\"],\"\"", "\"]},{\"");
        //        jasonFinal = jasonFinal.Replace(jasonText, jasonReplace);
        //    }

        //    foreach (Match match in regex3.Matches(jsonTemp))
        //    {
        //        jasonText = match.Value.Replace("[", "[\"").Replace("]", "\"]").Replace("#@#", "\",\"");
        //        jasonFinal = jasonFinal.Replace("\"]},{\"", "\",\"");
        //        jasonReplace = jasonText.Replace("[", "[{").Replace("]", "\"]}]").Replace("=", "\":[\"").Replace(",", "\"],\"").Replace("\"\"],\"\"", "\"]},{\"");
        //        jasonFinal = jasonFinal.Replace(jasonText, jasonReplace);
        //    }

        //    jasonFinal = jasonFinal.Replace("\",\"", "\"]},{\"");

        //    return jasonFinal;
        //}

        //public static Dictionary<String, List<String>> JsonToDic(string json)
        //{
        //    Dictionary<String, List<String>> result = new Dictionary<string, List<string>>();

        //    json = json.Replace("\"", "");

        //    String regexMultiS = @"\{(([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)](?:,))*(([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)])*}";
        //    String regexMultiS2 = @"([A-Za-z0-9\s]*):\[({(([A-Za-z0-9\s]*)=((([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))(?:,))*(([A-Za-z0-9\s]*)=((([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)))*}(?:,))*{(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)(?:,))*(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*}]";
        //    String regexMultiVal = @"\{(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)(?:,))*(([A-Za-z0-9\s]*)=(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*))*}";

        //    String regexPattern = @"([A-Za-z0-9\s]*):\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)]";
        //    String regexKey = @"([A-Za-z0-9\s]*):";
        //    String regexVal = @"\[(([A-Za-z0-9\s])*|([A-Za-z0-9\s])*\.([A-Za-z0-9\s])*(@([A-Za-z0-9\s])*)*.([A-Za-z0-9\s])*)]";

        //    Regex regexMulti = new Regex(regexMultiS);
        //    Regex regexMulti2 = new Regex(regexMultiS2);
        //    Regex regexMultiValue = new Regex(regexMultiVal);
        //    Regex regex = new Regex(regexPattern);

        //    String jsonTemp, jsonReplace;
        //    String keyJson, valJson;

        //    jsonTemp = json;
        //    foreach (Match match in regexMulti.Matches(jsonTemp))
        //    {
        //        jsonReplace = match.Value.Replace(":", "=").Replace("[", "").Replace("]", "");
        //        jsonTemp = jsonTemp.Replace(match.Value, jsonReplace);
        //    }

        //    foreach (Match match in regex.Matches(jsonTemp))
        //    {
        //        keyJson = Regex.Match(match.Value, regexKey).Groups[0].Value.Replace(":", "");
        //        valJson = Regex.Match(match.Value, regexVal).Groups[0].Value.Replace("[", "").Replace("]", "");

        //        result.Add(keyJson, new List<String> { valJson });
        //    }


        //    foreach (Match match in regexMulti2.Matches(jsonTemp))
        //    {
        //        List<String> tempVal = new List<string>();
        //        foreach (Match match2 in regexMultiValue.Matches(match.Value))
        //        {
        //            tempVal.Add(match2.Value.Replace("{", "").Replace("}", ""));
        //        }

        //        keyJson = Regex.Match(match.Value, regexKey).Groups[0].Value.Replace(":", "");
        //        result.Add(keyJson, tempVal);
        //    }

        //    return result;
        //}
    }
}
