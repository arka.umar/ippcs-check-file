﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Util
{
    public static class DictionaryDefaultValueHelper
    {
        public static TValue ValueOf<TKey, TValue>(this IDictionary<TKey, TValue> x, TKey key, TValue defaultValue)
        {
            if (x.ContainsKey(key))
            {
                return x[key];
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
