﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Util.Converter;

namespace Toyota.Common.Web.Util
{
    public class CompositeValue
    {
        private StandardTypeConverter converter;

        public CompositeValue()
        {
            converter = new StandardTypeConverter();
        }

        public object Value { set; get; }
        
        public T GetValue<T>()
        {
            return converter.ConvertValue<T>(Value);
        }
    }
}
