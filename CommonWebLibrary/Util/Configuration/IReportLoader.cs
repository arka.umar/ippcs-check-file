﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Util.Configuration
{
    public interface IReportLoader
    {
        string GetReport(string name);
    }
}
