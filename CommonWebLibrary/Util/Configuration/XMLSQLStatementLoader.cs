﻿/*

 diko
 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;
using Toyota.Common.Web.Database;
using Toyota.Common.Lookup;
using System.IO;

namespace Toyota.Common.Web.Util.Configuration
{
    public class XMLSQLStatementLoader: ISQLStatementLoader
    {
        List<string> sqlConfigNames;

        public XMLSQLStatementLoader()
        {
            GlobalConfiguration globalConfig = new GlobalConfiguration();
            sqlConfigNames = globalConfig.GetSqlQueryConfigurationNames();
        }

        public string GetSQLStatement(string name)
        {
            string result = null;
            string key = "SQL." + name.ToLower();
            if (!String.IsNullOrEmpty(name))
            {
                string sql = Memori.Get<string>(key);
                if (!string.IsNullOrEmpty(sql))
                {
                    Dictionary<string, string> replacements = Memori.Get<Dictionary<string, string>>("SQL_REPLACEMENTS_DICTIONARY");
                    if (replacements != null)
                    {
                        foreach (var kv in replacements)
                        {
                            sql = sql.Replace(kv.Key, kv.Value);
                        }
                    }
                    return sql;
                }
                Assembly assembly = Assembly.GetExecutingAssembly();
                Stream stream;
                foreach (string cfgName in sqlConfigNames)
                {
                    stream = assembly.GetManifestResourceStream(Namespace.InternalSQLResourceNamespace + "." + cfgName);
                    /// ---replace cursor with Linq, remove unused variable - niit.yudha 15 nov 2012--- \\\
                    using (StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        if (sr != null)
                        {
                            string content = sr.ReadToEnd();
                            IEnumerable<XElement> answer = XElement.Parse(content).Descendants("Sql").Where(node => node.Attribute("name").Value == name);
                            if (answer != null && answer.Any()) { result = answer.ElementAt(0).Value.Trim(); }
                        }
                    }
                    stream.Close();
                    if (!string.IsNullOrEmpty(result))
                    {
                        break;
                    }
                    /* --------------------------------------------------- */
                }
                
            }
            if (!string.IsNullOrEmpty(result)) Memori.Set(key, result); 
            return result;
        }
    }
}
