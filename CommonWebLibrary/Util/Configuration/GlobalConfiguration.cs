﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using System.Configuration;
using Toyota.Common.Web.Database;
using Toyota.Common.Lookup;
using System.Xml;

namespace Toyota.Common.Web.Util.Configuration
{
    public class GlobalConfiguration
    {
        private XDocument document;

        public GlobalConfiguration()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            String URI = Namespace.InternalConfigurationResourceNamespace + ".GlobalConfiguration.xml";
            Stream stream = assembly.GetManifestResourceStream(URI);
            document = XDocument.Load(stream);
            stream.Close();
        }

        public List<string> GetSqlQueryConfigurationNames()
        {
            XElement rootDB = document.Root.Element("Database");
            if (rootDB == null)
            {
                return null;
            }

            XElement ctxQueryNode = rootDB.Element("SqlQuery");
            if (ctxQueryNode == null)
            {
                return null;
            }

            var qQuery = from c in ctxQueryNode.Elements() select c;
            if(qQuery.Any())
            {
                List<string> lstSql = new List<string>();
                foreach (var name in qQuery)
                {
                    lstSql.Add(name.Value);
                }
                return lstSql;
            }
            return null;
        }
        public string GetDatabaseContextManager()
        {

            XElement rootDB = document.Root.Element("Database");
            if (rootDB == null)
            {
                return null;
            }

            XElement ctxManagerNode = rootDB.Element("ContextManager");
            if (ctxManagerNode == null)
            {
                return null;
            }

            
            return ctxManagerNode.Value;
        }

        public int? GetDatabasePoolSize()
        {
            XElement rootDB = document.Root.Element("Database");
            if (rootDB == null)
            {
                return null;
            }

            XElement poolNode = rootDB.Element("ConnectionPool");
            if (poolNode == null)
            {
                return null;
            }

            XElement sizeNode = poolNode.Element("Size");
            if (sizeNode == null)
            {
                return null;
            }
            
            return Convert.ToInt32(sizeNode.Value);
        }

        public ILookup GetLookup()
        {
            XElement node = document.Root.Element("Lookup");
            if (node == null)
            {
                return null;
            }

            return ReflectionUtil.Instantiate<ILookup>(node.Value);
        }

        public string GetLoginValidator()
        {
            XElement node = document.Root.Element("LoginValidator");
            if (node == null)
            {
                return null;
            }

            return node.Value;
        }
    }
}
