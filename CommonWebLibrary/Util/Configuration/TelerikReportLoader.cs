﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using System.Text.RegularExpressions;
using Toyota.Common.Web.Database;

namespace Toyota.Common.Web.Util.Configuration
{
    public class TelerikReportLoader : IReportLoader
    {
        protected string ReportFolder;
        protected List<string> files;
        private Searcher search;

        string _paramPrefix = "@";
        private ISQLStatementLoader FilestatementLoader;

        public TelerikReportLoader()
        {
            files = new List<string>();
            FilestatementLoader = new FileSQLStatementLoader();
            if (System.IO.Path.GetPathRoot(ConfigurationManager.AppSettings["ReportFolder"]) == "")
            {
                ReportFolder = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["ReportFolder"];
            }
            if (System.IO.Directory.Exists(ReportFolder))
            {
                search = new Searcher(ReportFolder + "\\*.trdx", SearchCallback);
                search.IncludeSubfolder = true;
                search.Start();
            }
            else
            {
                throw new System.IO.DirectoryNotFoundException(ReportFolder + " , please make sure that directory is exist.");
            }
        }

        private void SearchCallback(string filename)
        {
            if (System.IO.Path.GetExtension(filename) == ".trdx")
            {
                files.Add(filename);
            }
        }

        public string GetReport(string name)
        {
            //string result = "";
            string fileName = (from file in files
                               where System.IO.Path.GetFileNameWithoutExtension(file.ToLower()) == name.ToLower()
                               select file).SingleOrDefault();
            //if (fileName != null)
            //{
            //    using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
            //    {
            //        result = sr.ReadToEnd();
            //    }
            //}
            return fileName;
        }

        public Telerik.Reporting.Report Open(string templateName, string connectionString, string sqlQuery, params object[] args)
        {
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer = new ReportXmlSerializer();
            XmlReaderSettings settings;
            Telerik.Reporting.Report rpt;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(GetReport(templateName), settings))
            {
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            string SQLQuery = FilestatementLoader.GetSQLStatement(sqlQuery);

            if (args.Count() >= 1)
            {
                for (int i = 0; i < args.Count(); i++)
                {
                    if (SQLQuery.Contains("@" + i.ToString()))
                        SQLQuery = SQLQuery.Replace("@" + i.ToString(), args[i].ToString());
                }
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connectionString;
            sqlDataSource.SelectCommand = SQLQuery;

            return rpt;
        }

        static Regex rxParamsPrefix = new Regex(@"(?<!@)@\w+", RegexOptions.Compiled);
        public string CreateSQLString(string sql, params object[] args)
        {


            // Perform parameter prefix replacements
            if (_paramPrefix != "@")
                sql = rxParamsPrefix.Replace(sql, m => _paramPrefix + m.Value.Substring(1));
            sql = sql.Replace("@@", "@");		   // <- double @@ escapes a single @

            return sql;
        }
    }
}
