﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Util.Converter
{
    public class StandardTypeConverter
    {
        public T ConvertValue<T>(object value)
        {
            Type type = typeof(T);
            
            if (value == null)
            {
                return (T) value;
            }

            object returnValue = value;
            if (type == typeof(int))
            {
                returnValue = Convert.ToInt32(value);
            }
            else if (type == typeof(double))
            {
                returnValue = Convert.ToDouble(value);
            }
            else if (type == typeof(bool))
            {
                string strBool = Convert.ToString(value);
                if (!string.IsNullOrEmpty(strBool))
                {
                    strBool = strBool.ToLower();
                    if (strBool.Trim().Equals("true"))
                    {
                        returnValue = true;
                    }
                }
                returnValue = false;
            }
            else if (type == typeof(byte))
            {
                returnValue = Convert.ToByte(value);
            }
            else if (type == typeof(char))
            {
                returnValue = Convert.ToChar(value);
            }
            else if (type == typeof(DateTime))
            {
                returnValue = Convert.ToDateTime(value);
            }
            else if (type == typeof(decimal))
            {
                returnValue = Convert.ToDecimal(value);
            }

            return (T)returnValue;
        }
    }
}
