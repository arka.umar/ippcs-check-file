﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.Text;

namespace Toyota.Common.Web.Util.Converter
{
    public class JSON
    {
        [System.Diagnostics.DebuggerStepThrough]
        public static string ToString<T>(object obj)
        {
            if (obj != null)
            { return JsonSerializer.SerializeToString<T>((T)obj); }
            return String.Empty;
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static T ToObject<T>(string str)
        {
            object result = null;
            if (!string.IsNullOrEmpty(str))
            { result = JsonSerializer.DeserializeFromString<T>(str); } 
            return (T) result;
        }
    }
}
