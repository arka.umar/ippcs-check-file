﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * niit.yudha - 7 jan 2012
 * Get password score
 */

namespace Toyota.Common.Web.Util
{
    public class PasswordMetering
    {

        public static int GetScore(string Password)
        {
            const string sLetter = "abcdefghijklmnopqrstuvwxyz";
            const string sNumeric = "1234567890";
            const string sSymbol = @"~!@#$%^&*()_+{}|:<>?,./;'[]\-=";
            const string mLetterUpper = "[A-Z]";
            const string mLetterLower = "[a-z]";
            const string mNumeric = "[0-9]";
            const string mSymbol = @"[!@#$%*()_+^&}{:;?.]";
            const int PasswordLen = 8;

            int iLength = 0;
            int iNumeric = 0;
            int iLetterUpper = 0;
            int iLetterLower = 0;
            int iSymbol = 0;
            int iSymbolMid = 0;
            int iConsecutiveLetter = 0;
            int iConsecutiveNumeric = 0;
            int iConsecutiveSymbol = 0;
            int iMultiple = 0;
            int iSeqLetter = 0;
            int iSeqSymbol = 0;
            int iSeqNumeric = 0;
            int requirement = 0;
            int iScore = 0;
            List<string> Multiple = new List<string>();
            char prevChar = '\0';

            if (!string.IsNullOrEmpty(Password))
            {
                iLength = Password.Length;

                iNumeric = Counting(Password, mNumeric);
                iSymbol = Counting(Password, mSymbol);
                iLetterLower = Counting(Password, mLetterLower);
                iLetterUpper = Counting(Password, mLetterUpper);

                Password.ToList().ForEach(c =>
                {
                    // multiple
                    if (!Multiple.Contains(c.ToString()))
                    {
                        var q = (from i in Password.ToCharArray()
                                 where i == c
                                 select i).Count();
                        if (q > 1)
                        {
                            iMultiple += q;
                            Multiple.Add(c.ToString());
                        }
                    }

                    // consecutive
                    if (char.IsDigit(c) && char.IsDigit(prevChar))
                    {
                        iConsecutiveNumeric++;
                    }
                    else if (char.IsLetter(c) && char.IsLetter(prevChar))
                    {
                        iConsecutiveLetter++;
                    }
                    else if (char.IsSymbol(c) && char.IsSymbol(prevChar))
                    {
                        iConsecutiveSymbol++;
                    }
                    prevChar = c;
                });

                for (int i = 0; i < 23; i++)
                {
                    string fwd = sLetter.Substring(i, 3);
                    string rvs = strReverse(fwd);
                    if (Password.ToLower().IndexOf(fwd) != -1 || Password.ToLower().IndexOf(rvs) != -1)
                        iSeqLetter++;
                }

                for (int i = 0; i < 8; i++)
                {
                    string fwd = sNumeric.Substring(i, 3);
                    string rvs = strReverse(fwd);
                    if (Password.ToLower().IndexOf(fwd) != -1 || Password.ToLower().IndexOf(rvs) != -1)
                        iSeqNumeric++;
                }


                for (int i = 0; i < sSymbol.Length - 2; i++)
                {
                    string fwd = sSymbol.Substring(i, 2);
                    string rvs = strReverse(fwd);
                    if (Password.ToLower().IndexOf(fwd) != -1 || Password.ToLower().IndexOf(rvs) != -1)
                        iSeqSymbol++;
                }

                iSymbolMid = Counting(Password.Substring(1, iLength - 1), mSymbol);

                //// calculate score
                iScore = iLength * 4;

                if (iLetterLower > 0)
                    iScore += (iLength - iLetterLower) * 2;
                if (iLetterUpper > 0)
                    iScore += (iLength - iLetterUpper) * 2;
                if (iSymbolMid > 0)
                    iScore += iSymbolMid * 2;
                if (iNumeric > 0)
                    iScore += (iNumeric) * 4;
                if (iSymbol > 0)
                    iScore += (iSymbol) * 6;


                if (iLetterUpper > 0)
                    requirement++;
                if (iLetterLower > 0)
                    requirement++;
                if (iNumeric > 0)
                    requirement++;
                if (iSymbol > 0)
                    requirement++;
                if (iSymbolMid > 0)
                    requirement++;

                iScore += requirement * 2;

                //// deduction
                if (iConsecutiveLetter > 0)
                    iScore -= (iConsecutiveLetter) * 2;
                if (iConsecutiveNumeric > 0)
                    iScore -= (iConsecutiveNumeric) * 2;
                if (iConsecutiveSymbol > 0)
                    iScore -= (iConsecutiveSymbol) * 2;
                if (iMultiple > 0)
                    iScore -= (iMultiple / 2) - iMultiple;
                if (iSeqLetter > 0)
                    iScore -= iSeqLetter * 2;
                if (iSeqNumeric > 0)
                    iScore -= iSeqNumeric * 2;
                if (iNumeric == 0)
                    iScore -= (iLength);
            }
            return iScore;
        }

        [System.Diagnostics.DebuggerStepThrough]
        private static String strReverse(String str)
        {
            string newstring = "";
            for (int s = 0; s < str.Length; s++)
            {
                newstring = str[s] + newstring;
            }
            return newstring;
        }

        [System.Diagnostics.DebuggerStepThrough]
        private static int Counting(string input, string pattern)
        {
            System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(input, pattern);
            int i = 0;
            while (match.Success)
            {
                i++;
                match = match.NextMatch();
            }
            return i;
        }

    }
}
