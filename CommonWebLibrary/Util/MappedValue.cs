﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Util
{
    public class MappedValue<T, K>
    {
        public MappedValue()
        {

        }

        public MappedValue(T key, K value)
        {
            Key = key;
            Value = value;
        }
        public T Key { set; get; }
        public K Value { set; get; }
    }
}
