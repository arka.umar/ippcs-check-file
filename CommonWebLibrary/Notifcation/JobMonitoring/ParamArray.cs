﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public class ParamArray : Collection
    {
        public ParamArray()
        {
            map.Add("recipientcode", "-");
            map.Add("attachmentid", "-"); 
        }

        public override void Add(string key, string value)
        {
            if (key.ToLower() == "recipientcode")
                this.map[key] = value;
            else if (key.ToLower() == "attachmentid")
                this.map[key] = value;
            else
                base.Add(key, value);
        }
         
    }
}
