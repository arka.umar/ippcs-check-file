﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public class TaskService
    {
        private static ITask instance = new DefaultNotificationTask();

        public static ITask GetInstance()
        {
            return instance;
        }
    }
}
