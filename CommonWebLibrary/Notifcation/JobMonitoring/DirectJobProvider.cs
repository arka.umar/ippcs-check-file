﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using System.Configuration;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public class DirectJobProvider
    {
        //public static bool Execute(string functionID, string roleID, string contentID, ArgumentParameter parameter, string attachmentID = "-", string recipientCode = "-")
        public static bool Execute(ParamArray keyParameter, ArgumentParameter parameter)
        { 
            string path = null;
            string formatArg = "{0} {1} {2} {3} {4} {5} {6}";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var method = db.Query<int>("GetNotificationMethod", new object[] { keyParameter.Get("functionid"), keyParameter.Get("contentid") });
            db.Close();
            System.Diagnostics.ProcessStartInfo processInfo;
            if (method.Any())
            {
                foreach (int i in method)
                {
                    switch (i)
                    {
                        case 1:
                            path = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["EmailExecPath"];
                            break;
                        case 2:
                            path = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["SMSExecPath"];
                            break;
                        case 3:
                            
                            break;
                    }
                    if (!string.IsNullOrEmpty(path))
                    {
                        if (parameter != null)
                        {
                            processInfo = new System.Diagnostics.ProcessStartInfo(path, string.Format(formatArg, keyParameter.Get("functionid").Trim(), keyParameter.Get("roleid").Trim(), keyParameter.Get("contentid").Trim(), i, parameter.ToArgument().Trim(), keyParameter.Get("attachmentid"), keyParameter.Get("recipientcode")));
                            processInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            System.Diagnostics.Process.Start(processInfo);
                        }
                    }
                    else
                        throw new ArgumentNullException("Path cannot be null.");
                }
            }
            return true;
        }
    }
}

