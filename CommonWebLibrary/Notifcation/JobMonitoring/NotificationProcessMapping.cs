﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public class NotificationProcessMapping
    {
        public string ProcessID { get; set; }
        public string ProcessDescription { get; set; }
        public string RoleID { get; set; }
        public string NotificationMethod { get; set; }
        public string MethodDescription { get; set; }
        public string Path { get; set; }
        public int Month { get; set; }
        public int Date { get; set; }
        public string Day { get; set; }
        public TimeSpan Time { get; set; }
        public string Summary { get; set; }
    }
}
