﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Microsoft.Win32.TaskScheduler;
using Toyota.Common.Web.Database;
using System.Threading;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public class DefaultNotificationTask : ITask, IDisposable
    {
        private string folder;
        private List<NotificationProcessMapping> list;
        private Microsoft.Win32.TaskScheduler.TaskService task = new Microsoft.Win32.TaskScheduler.TaskService();
        private Microsoft.Win32.TaskScheduler.TaskFolder taskFolder; 
        private ManualResetEvent wait; 
        private int threadCounter;
        private const string joinString = "_";

        public DefaultNotificationTask()
        {
            folder = ConfigurationManager.AppSettings["TaskSchedulerFolder"];
            if (string.IsNullOrEmpty(folder))
                taskFolder = task.GetFolder(@"\");
            else
                taskFolder = task.GetFolder(folder);

            GetNotifications();
            task.BeginInit();
        }

        public void Delete(string ProcessID, string RoleID)
        {
            taskFolder.DeleteTask(ProcessID + RoleID);
        }

        public void DeleteAllTasks()
        { 
            NotificationProcessMapping map;
            wait = new ManualResetEvent(false);
            Thread thrCreateTask;
            TaskCollection collection = taskFolder.GetTasks();
            threadCounter = collection.Count;
            foreach (Microsoft.Win32.TaskScheduler.Task item in collection)
            {
                map = list.Where(x => item.Name.ToLower() == x.ProcessID.ToLower() + joinString + x.RoleID.ToLower()).SingleOrDefault();
                if (map != null)
                {
                    thrCreateTask = new Thread(new ParameterizedThreadStart(_DeleteTask));
                    thrCreateTask.Start(item.Name);
                }
            }
            if (threadCounter > 0)
                wait.WaitOne();
        }

        private void _DeleteTask(object Key)
        {
            taskFolder.DeleteTask((string)Key);
            if (Interlocked.Decrement(ref threadCounter) == 0)
                wait.Set();
        }

        public object Get(string ProcessID, string RoleID)
        {
            return (object)task.GetTask(ProcessID + joinString + RoleID);
        }

        public List<object> GetAllTasks()
        {
            return taskFolder.GetTasks().ToList<object>();
        }

        public bool Register(string ProcessID, string RoleID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("id-ID");
            string name = ProcessID.ToUpper() + joinString + RoleID.ToUpper();
            Microsoft.Win32.TaskScheduler.TaskDefinition td = task.NewTask();
            td.Settings.Enabled = true;
            td.Settings.Compatibility = Microsoft.Win32.TaskScheduler.TaskCompatibility.V2;

            NotificationProcessMapping d = list.Where(p => p.ProcessID.ToLower() == ProcessID.ToLower() && p.RoleID.ToLower() == RoleID.ToLower()).Select(i => i).SingleOrDefault();
            DateTime dtData;
            Trigger trg = null;
            if (d != null)
            {
                if (d.Month == 0 && d.Date == 0 && !string.IsNullOrEmpty(d.Day))
                {
                    string[] days = d.Day.Split(';');
                    if (days.Length > 0)
                    {
                        DaysOfTheWeek dayShort = DaysOfTheWeek.AllDays;
                        foreach (string day in days)
                        {
                            try
                            { dayShort = (DaysOfTheWeek)short.Parse(day); }
                            catch
                            { }
                            if (dayShort == DaysOfTheWeek.AllDays)
                            {
                                switch (day.ToLower())
                                {
                                    case "sunday":
                                        dayShort = DaysOfTheWeek.Sunday;
                                        break;

                                    case "monday":
                                        dayShort = DaysOfTheWeek.Monday;
                                        break;

                                    case "tuesday":
                                        dayShort = DaysOfTheWeek.Tuesday;
                                        break;

                                    case "wednesday":
                                        dayShort = DaysOfTheWeek.Wednesday;
                                        break;

                                    case "Thursday":
                                        dayShort = DaysOfTheWeek.Thursday;
                                        break;

                                    case "friday":
                                        dayShort = DaysOfTheWeek.Friday;
                                        break;

                                    case "saturday":
                                        dayShort = DaysOfTheWeek.Saturday;
                                        break;
                                }
                            }
                            dtData = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, d.Time.Hours, d.Time.Minutes, d.Time.Seconds);
                            trg = new WeeklyTrigger { WeeksInterval = 1, StartBoundary = dtData, DaysOfWeek = dayShort };
                        }
                    }
                }
                else if (d.Month != 0 && d.Date != 0 && string.IsNullOrEmpty(d.Day))
                {
                    dtData = new DateTime(DateTime.Now.Year, d.Month, d.Date, d.Time.Hours, d.Time.Minutes, d.Time.Seconds);
                    trg = new MonthlyTrigger { StartBoundary = dtData, MonthsOfYear = GetMonthsOfTheYear(d.Month), DaysOfMonth = new int[] { d.Date } };
                }
                else if (d.Month == 0 && d.Date == 0 && string.IsNullOrEmpty(d.Day))
                {
                    dtData = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, d.Time.Hours, d.Time.Minutes, d.Time.Seconds);
                    trg = new DailyTrigger { StartBoundary = dtData, DaysInterval = 1 };
                }
                else if (d.Month == 0 && d.Date != 0 && string.IsNullOrEmpty(d.Day))
                {
                    dtData = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, d.Time.Hours, d.Time.Minutes, d.Time.Seconds);
                    trg = new MonthlyTrigger { StartBoundary = dtData, MonthsOfYear = MonthsOfTheYear.AllMonths, DaysOfMonth = new int[]{d.Date} };
                }
                else
                { throw new InvalidDataException("Month or Date or Day"); }

                if (trg != null)
                {
                    td.Principal.LogonType = TaskLogonType.S4U;
                    td.Triggers.Add(trg);
                    td.RegistrationInfo.Description = "Start " + d.MethodDescription + " " + d.Summary;
                    td.RegistrationInfo.Author = "SYSTEM";
                    td.Actions.Add(new ExecAction(d.Path, ProcessID + RoleID));
                    taskFolder.RegisterTaskDefinition(name, td);
                }
                else
                { throw new InvalidOperationException("Cannot Register task because no triggers that are found."); }
                return true;
            }
            return false;
        }

        private MonthsOfTheYear GetMonthsOfTheYear(int value)
        {
            MonthsOfTheYear ret = MonthsOfTheYear.AllMonths;
            switch (value)
            { case 0: ret = MonthsOfTheYear.AllMonths; break; case 1: ret = MonthsOfTheYear.January; break; case 2: ret = MonthsOfTheYear.February; break; case 3: ret = MonthsOfTheYear.March; break; case 4: ret = MonthsOfTheYear.April; break; case 5: ret = MonthsOfTheYear.May; break; case 6: ret = MonthsOfTheYear.June; break; case 7: ret = MonthsOfTheYear.July; break; case 8: ret = MonthsOfTheYear.August; break; case 9: ret = MonthsOfTheYear.September; break; case 10: ret = MonthsOfTheYear.October; break; case 11: ret = MonthsOfTheYear.November; break; case 12: ret = MonthsOfTheYear.December; break; }
            return ret;
        }

        private List<NotificationProcessMapping> GetNotifications()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            list = db.Fetch<NotificationProcessMapping>("GetNotificationScheduler");
            db.Close();
            return list;
        }
         
        public void Dispose()
        {
            if (task != null)
            {
                task.EndInit();
                task.Dispose();
            }
        }
         
    }
}