﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public interface ITask
    { 
        bool Register(string ProcessID, string RoleID);
        object Get(string ProcessID, string RoleID);
        void Delete(string ProcessID, string RoleID);
        List<object> GetAllTasks();
        void DeleteAllTasks();
    }
}
