﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public interface IAction
    { 
        bool Execute(ArgumentParameter parameter);
    }
}
