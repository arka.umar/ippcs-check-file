﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;

namespace Toyota.Common.Web.Notifcation.JobMonitoring
{
    public class AttachmentProvider
    {
        
        private string attachID;
        private string functionID;
        private string roleID;

        public void Create(string functionID, string roleID)
        {
            attachID = DateTime.Now.ToString("yyyyMMddhhmmssfff");
            this.functionID = functionID;
            this.roleID = roleID;
        }
        
        public void Write(string path)
        {
            string fullpath = "";
            if (string.IsNullOrEmpty(path))
                return;

            if (System.IO.Path.GetDirectoryName(path) == "")
                fullpath = AppDomain.CurrentDomain.RelativeSearchPath + @"\" + path;
            else
                fullpath = path;

            if (System.IO.File.Exists(fullpath))
            {
                string filename = System.IO.Path.GetFileNameWithoutExtension(fullpath);
                string extension = System.IO.Path.GetExtension(fullpath);
                byte[] dataInBytes = System.IO.File.ReadAllBytes(fullpath);
                string dataInStr = Convert.ToBase64String(dataInBytes);
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("InsertAttachmentContent", new object[] { functionID, roleID, attachID, filename, dataInStr, extension });
                db.Close();
            }
        }

        public string Flush()
        {
            string ret = attachID;
            attachID = null;
            return ret;
        }
    }
}
