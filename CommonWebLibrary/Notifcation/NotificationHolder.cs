﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

namespace Toyota.Common.Web.Notification
{
    /// <summary>
    /// This class provides holder for Notification List.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <remarks>
    /// niit.yudha 11 sep 2012, Created.
    /// </remarks>
    public class NotificationHolder<T> : ReadOnlyCollectionBase, IDisposable, System.Collections.Specialized.INotifyCollectionChanged
    {

        public NotificationHolder()
        {
            
        }

        public NotificationHolder(IList sourceList)
        {
            InnerList.AddRange(sourceList);
        }

        public void Push(T obj)
        {
            base.InnerList.Add(obj);
        }
        
        public void Remove(T obj)
        {
            base.InnerList.Remove(obj);
        }
         
        public T this[int index]
        {
            get
            {
                return ((T)InnerList[index]);
            }
        }
        public int IndexOf(T value)
        {
            return (InnerList.IndexOf(value));
        }

        public bool Contains(T value)
        {
            return (InnerList.Contains(value));
        }

        public void Dispose()
        {
            base.InnerList.Clear();
        }
        public object[] ToArray()
        {
            return base.InnerList.ToArray();
        }

        public event System.Collections.Specialized.NotifyCollectionChangedEventHandler CollectionChanged;

    }

}
