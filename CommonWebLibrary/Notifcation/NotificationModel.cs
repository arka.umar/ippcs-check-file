﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;

namespace Toyota.Common.Web.Notification
{
    public class NotificationModel
    {
        string _id;
        string _title;
        string _description;
        
        public NotificationModel(string id, string title, string description)
        {
            _id = id;
            _title = title;
            _description = description;
        }

        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }
         
    }
}
