﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Web.Messaging;

namespace Toyota.Common.Web.Notification
{
    public class Notification
    {
        
        //public Notification(string id, string title, string description, string userNames, string recipient_level, int authentication_method, int notification_Level,string action_URL, byte status)
        public Notification(string id, string title, string description, byte notification_Level, string action_URL)
        {
            ID = id;
            Title = title;
            Description = description;
            Notification_Level = notification_Level;
            Action_URL = action_URL;
        }

        public Notification()
        {
            // TODO: Complete member initialization
        }

        public string ID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public byte Notification_Level { get; set; }
        
        public string Action_URL { get; set; }
         
        public static Notification Error(string id, string title, string description, string action_URL)
        {
            return new Notification(id, title, description, ScreenMessage.MESSAGE_TYPE_ERROR, action_URL);
        }

        public static Notification Warning(string id, string title, string description, string action_URL)
        {
            return new Notification(id, title, description, ScreenMessage.MESSAGE_TYPE_WARNING, action_URL);
        }

        public static Notification Info(string id, string title, string description, string action_URL)
        {
            return new Notification(id, title, description, ScreenMessage.MESSAGE_TYPE_INFO, action_URL);
        }
    }
}
