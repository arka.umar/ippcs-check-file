﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Credential;

namespace Toyota.Common.Web.Notification
{
    /// This class provides wrapper for Notifications.
    /// niit.yudha 11 sep 2012, Created.
    public class NotificationProvider : IDisposable
    {
        private static NotificationProvider instance = new NotificationProvider();
        IDBContextManager dbManager = DatabaseManager.GetInstance();
 
        public string result { get; set; }

        //private NotificationProvider()
        //{

        //}

        public static NotificationProvider GetInstance()
        {
            return instance;
        }

        public string GetAllNotification(User user)
        {
            const string SplitClause = "/split";
            string result = String.Empty;
            StringBuilder resultBuilder = new StringBuilder();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            var tempNotif = dbContext.Query<Notification>("GetTempNotification", new object[] {user.Username, "SUPPLIER_ADMINISTRATOR", user.Username, "SUPPLIER_ADMINISTRATOR"});
            if ((tempNotif != null) && tempNotif.Any())
            {
                foreach (var c in tempNotif)
                {
                    resultBuilder.Append(String.Format("{0}, {1}, {2}, {3}{4}", c.Title, c.Description, c.Notification_Level, c.Action_URL, SplitClause));
                }
            }
            dbContext.Close();
            result = resultBuilder.ToString();
            if (!String.IsNullOrEmpty(result))
            {
                ClearTempNotification();
                return result.Substring(0, resultBuilder.Length - SplitClause.Length).Trim();
            }
            else
            {
                return "none";
            }
        }
        
        public void Insert(string title, string description, string userName, string recipient_Roles, int authentication_Method, int notification_Level, string action_URL, string created_By, DateTime create_Dt, string changed_By, DateTime changed_Dt)
        {
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            dbContext.Execute("InsertTempNotification", new object[] {               
                    title,
                    description,
                    userName, 
                    recipient_Roles,
                    authentication_Method,
                    notification_Level,
                    action_URL,
                    created_By,
                    create_Dt,
                    changed_By,
                    changed_Dt
                });
            dbContext.Close();
        }

        public void ClearTempNotification()
        {
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            dbContext.Execute("ClearTempNotification", new object[]{});
            dbContext.Close();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}