﻿function scanDisplayMethod(display, document) {
    if (display == 'popup') 
    {
        $link = $(document).find(".inspectPopup");
        $link.each(function () {
            $url = $(this).attr("href");
            $(this).popupWindow({
                height: 500,
                width: 800,
                top: 50,
                left: 50,
                windowURL: $url,
                centerScreen: 1
            });
        });
    }
}