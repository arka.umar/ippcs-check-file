﻿//$timerNotif = $.timer(function () {
//    updateNotification();
//});

var Url;
var ScreenMessageError;
var ScreenMessageWarning;
var ScreenMessageInfo;
var ScreenMessageClickable;

var ImageError;
var ImageInfo;
var ImageWarning;

function initNotification(params) {
    Url = params["Url"];
    ScreenMessageError = params["ScreenMessageError"];
    ScreenMessageWarning = params["ScreenMessageWarning"];
    ScreenMessageInfo = params["ScreenMessageInfo"];
    ScreenMessageClickable = params["ScreenMessageClickable"];

        ImageError = params["ImageError"];
        ImageInfo = params["ImageInfo"];
        ImageWarning = params["ImageWarning"];

        //alert(ImageError);
    var Timer = params["Timer"];
    var Interval = params["Interval"];

    insertHtmlElements();
    updateNotification();
//    $timerNotif();
//    $timerNotif.set({ time: Interval, autostart: Timer });
}

function insertHtmlElements() {
    $body = $("body");
    $body.append(
        "<div id=\"notificationArea\" style=\"display:none\">" + 
                "<div id=\"sticky\">" +
			        "<a class=\"ui-notify-close ui-notify-cross\" href=\"#\">x</a>" +
                    "<div style=\"float:left;margin: auto 10px auto auto\"><img  height=\"40px\" src=\"~/Content/Images/error_icon.png\")\" alt=\"warning\" /></div>" +
			        "<h1>#{title}</h1>" +
			        "<p>#{text}</p>" +
		        "</div>" +

                "<div id=\"withIcon\">" +
			        "<a class=\"ui-notify-close ui-notify-cross\" href=\"#\">x</a>" +
			        "<div style=\"float:left;margin: auto 10px auto auto\"><img  height=\"40px\" src=\"~/Content/Images/warning_icon.png\")\" alt=\"warning\" /></div>" +
			        "<h1>#{title}</h1>" +
			        "<p>#{text}</p>" +
		        "</div>" +

                "<div id=\"themeroller\" class=\"ui-state-error\" style=\"padding:10px; -moz-box-shadow:0 0 6px #980000; -webkit-box-shadow:0 0 6px #980000; box-shadow:0 0 6px #980000;\">" +
			        "<a class=\"ui-notify-close\" href=\"#\"><span class=\"ui-icon ui-icon-close\" style=\"float:right\"></span></a>" +
			        "<span style=\"float:left;margin: auto 10px auto auto\" class=\"ui-icon ui-icon-alert\"></span>" +
			        "<h1>#{title}</h1>" +
			        "<p>#{text}</p>" +
			        "<p style=\"text-align:center\"><a class=\"ui-notify-close\" href=\"#\">Close Me</a></p>" +
		        "</div>" +

                "<div id=\"default5second\">" +
                "<div style=\"float:left;margin: auto 10px auto auto\"><img  height=\"40px\" src=\"~/Content/Images/info_icon.png\")\" alt=\"warning\" /></div>" +
			        "<h1>#{title}</h1>" +
			        "<p>#{text}</p>" +
		        "</div>" +

                "<div id=\"standard\">" +
			        "<a class=\"ui-notify-close ui-notify-cross\" href=\"#\">x</a>" +
			        "<h1>#{title}</h1>" +
			        "<p>#{text}</p>" +
		        "</div>" +
            "</div>");

            //alert($body.html());
}

function updateNotification() 
{
    $.get(Url, function (data) {
        if (data != 'none') {
            //alert(data);
            var arrayData = [];
            arrayData = data.split('/split');
            //alert(arrayData.length);
            for (var i = 0; i < arrayData.length; i++) {
                var baris1 = arrayData[i];
                //alert(baris1);
                var arrayDetail = [];
                arrayDetail = baris1.split(',');
                //alert(arrayDetail[2]);
                //alert(ScreenMessageError);
                //alert(arrayDetail[2].toString());
                //alert(ScreenMessageError);
                if (arrayDetail[2].toString() == ScreenMessageError) {
                    //create("sticky", { title: arrayDetail[0], text: arrayDetail[1] }, { expires: false }); expires guna nya untuk tahan agar ga close automatic
                    create("sticky", { title: arrayDetail[0], text: arrayDetail[1] }, { expires: 10000 });
                }
                else if (arrayDetail[2].toString() == ScreenMessageWarning) {
                    create("withIcon", { title: arrayDetail[0], text: arrayDetail[1] }, { expires: 17000 });
                }
                else if (arrayDetail[2].toString() == ScreenMessageInfo) {
                    create("default5second", { title: arrayDetail[0], text: arrayDetail[1] });
                }
                else if (arrayDetail[2].toString() == ScreenMessageClickable) {//for clickable
                    create("standard", { title: arrayDetail[0], text: arrayDetail[1] }, {
                        click: function (e, instance) {
                            alert(arrayDetail[3]);
                        }
                    });
                }
            }
        }
    },
                "html");

                function create(template, vars, opts) {
                    //alert($notificationArea);
                    return $notificationArea.notify("create", template, vars, opts);
                }

                $(function () {
                    $notificationArea = $("#notificationArea").notify();
                }); 
}