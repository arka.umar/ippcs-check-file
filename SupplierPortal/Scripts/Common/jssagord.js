﻿/* 
    Document   : jssagord
    Created on : Jan 16, 2013, 6:23:32 PM
    Author     : Lufty Abdillah
    Description: Chat-styled Message Board
*/

(function ($) {
    $.fn.jssagord = function (options) {
        var settings = $.extend({
            backendProcessing: false,
            messageReloadUrl: null,
            customRecipientInput: null,
            onPostingReply: function (data) { },
            onReloadMessages: function () { },
            onReloadNewAttachments: function () { },
            onAttachmentDeletion: function (id) { }
        }, options);
        var listWrapperHeight = 0;

        function scrollDown(element) {
            element.animate({ scrollTop: 10000 }, 500);
        }

        function reloadMessages(list) {
            if (settings.backendProcessing) {
                if (settings.messageReloadUrl != null) {
                    $.ajax({
                        url: settings.messageReloadUrl,
                        dataType: "text",
                        success: function (data) {
                            _reloadMessages(list, $.parseJSON(data));
                        }
                    });
                }
            } else {
                _reloadMessages(list, $.parseJSON(settings.onReloadMessages()));
            }
        }

        function _reloadMessages(list, messages) {
            list.empty();
            list.hide();
            $(messages).each(function (index, value) {
                if (value != null) {
                    var attachments = $(value.Attachments);
                    var attachmentStrings = "";
                    attachments.each(function (index, value) {
                        attachmentStrings += "<li id='" + value.Id + "'><a id='" + value.Id + "' href='" + value.Url + "'>" + value.Name + "</a>";
                    });
                    list.append(
                        "<li>\n" +
                        "   <div class=\"sender\">" + value.Sender + "</div>\n" +
                        "   <div class=\"date\">" + value.DateInString + "</div>\n" +
                        "   <div class=\"attachments\">\n" +
                        "       <ul>\n" + attachmentStrings + "</ul>\n" +
                        "   </div>\n" +
                        "   <div class=\"text\">" + value.Text + "</div>" +
                        "</li>\n"
                    );
                }
            });

            restructureMessageList(list);
            list.fadeIn("fast");
        }

        function restructureMessageList(list) {
            var messages = list.children("li");
            var counter = 0;
            messages.each(function () {
                counter++;
                var item = $(this);
                if ((counter % 2) == 0) {
                    item.addClass("even");
                } else {
                    item.addClass("odd");
                }

                var sender = item.children(".sender");
                var date = item.children(".date");
                item.prepend("<div class='message-header'/>");
                var messageHeader = item.children(".message-header");
                sender.appendTo(messageHeader);
                date.appendTo(messageHeader)
                messageHeader.append("<div id='attachments'>attachments</div>");
                var attachments = item.children(".attachments");
                attachments.hide();

                var attachmentSlider = messageHeader.children("#attachments");
                var hasNoAttachment = attachments.children("ul").length == 0;
                if (hasNoAttachment) {
                    attachmentSlider.text("no attachments");
                } else {
                    attachmentSlider.addClass("active");
                }

                date.hover(function (e) {
                    e.stopPropagation();
                    $(this).fadeOut("fast");
                    attachmentSlider.fadeIn("fast");
                }, function () { });

                attachmentSlider.hover(function () { }, function () {
                    $(this).fadeOut("fast");
                    date.fadeIn("fast");
                });

                attachmentSlider.click(function () {
                    if (!hasNoAttachment) {
                        if (attachments.is(":visible")) {
                            attachments.slideUp("fast");
                        } else {
                            attachments.slideDown("fast");
                        }
                    }
                });
            });
        }

        return this.each(function () {
            var element = $(this);
            var id = element.attr("id");
            element.addClass("jssagord");
            element.append("<div/>");

            var listWrapper = element.children("div");
            listWrapper.addClass("list-wrapper");
            listWrapper.append("<ul/>");

            var list = listWrapper.children("ul");
            list.addClass("list");
            element.append("<div id='" + id + "-chatbox' class='chatbox'/>");

            var chatBox = element.children(".chatbox");
            chatBox.hide();
            if (settings.customRecipientInput != null) {
                chatBox.prepend("<div class='recipient-form'><span id='label'>To:</span>&nbsp;&nbsp;" + settings.customRecipientInput + "</div>");
            } else {
                chatBox.prepend("<div class='recipient-form'><span id='label'>To:</span>&nbsp;&nbsp;<input id='text' type='text'/></div>");
            }
            console.log();

            chatBox.append("<textarea id='message' rows='2'/>");
            chatBox.append("<div id='options'/>");
            var chatBoxOptions = chatBox.children("#options");
            chatBoxOptions.append("Notification: &nbsp; &nbsp;<input id='sendBySMS' type='checkbox' value='SendBySMS'/>&nbsp;SMS");
            chatBoxOptions.append("&nbsp;&nbsp;<input id='sendByEmail' type='checkbox' value='SendBySMS'/>&nbsp;Email");
            var replyOptions_SendBySMS = chatBoxOptions.children("#sendBySMS");
            var replyOptions_SendByEmail = chatBoxOptions.children("#sendByEmail");
            chatBox.append("<div class='buttons'/>");
            var chatBoxButtons = chatBox.children(".buttons");
            chatBoxButtons.append("<input id='send' type='button' class='button' value='Send'/>");
            chatBoxButtons.append("<input id='attachment' type='button' class='button' value='Attachment'/>");
            chatBoxButtons.append("<input id='clear' type='button' class='button' value='Clear'/>");
            var replyRecipient = chatBox.children(".recipient-form").children("#text").filter(":first");
            var replyMessage = chatBox.children("#message");
            var replySendButton = chatBoxButtons.children("#send").filter(":first");
            var replyClearButton = chatBoxButtons.children("#clear").filter(":first");

            restructureMessageList(list);

            element.append("<div id='slider'>show</div>");
            var rootSlider = element.children("#slider");
            rootSlider.click(function () {
                $(this).fadeOut("fast", "swing", function () { });
                listWrapper.animate({
                    height: listWrapperHeight
                }, "fast", "swing", function () { });
                chatBox.fadeIn("fast", "swing", function () {
                    scrollDown(listWrapper);
                });
            });

            chatBox.prepend("<div id='slider'>hide</div>");
            var slider = chatBox.children("#slider");
            slider.click(function () {
                var attachmentSlider = chatBox.children("#attachment-form").children("#slider").filter(":first");
                attachmentSlider.click();
                chatBox.fadeOut("fast", "swing", function () { });
                listWrapperHeight = listWrapper.css("height");
                listWrapper.animate({
                    height: "90%"
                }, "fast", "swing", function () {
                    rootSlider.fadeIn("fast", "swing", function () { });
                    scrollDown(listWrapper);
                });
            });

            chatBox.prepend("<div id='attachment-form'/>");
            var attachmentForm = chatBox.children("#attachment-form");
            attachmentForm.hide();
            attachmentForm.css("height", element.height() / 2);
            attachmentForm.css("width", "100%");
            attachmentForm.css("bottom", 0);
            attachmentForm.css("left", 0);
            attachmentForm.prepend("<div id='slider'>close</div>");
            attachmentForm.append("<div id='form'/>");

            var attachmentUploadForm = attachmentForm.children("#form");
            attachmentUploadForm.append("<input type='file' id='attachment-file' size='" + (Math.round(element.outerWidth() / parseInt(element.css("font-size"), 10)) + 2) + "'>");
            attachmentUploadForm.append("<input type='button' id='send-attachment' value='send'>");
            attachmentForm.append("<div id='list'/>");

            var attachmentUploadList = attachmentForm.children("#list");
            attachmentUploadList.append("<ul>");
            attachmentUploadList.append("</ul>");

            var attachmentUploadSendButton = attachmentUploadForm.children("#send-attachment");
            attachmentUploadSendButton.click(function () {
                var data = settings.onReloadNewAttachments();
                var linkMap = $.parseJSON(data);
                var attachmentList = attachmentUploadList.children("ul").filter(":first");
                attachmentList.empty();
                $(linkMap).each(function (index, value) {
                    if (value != null) {
                        attachmentList.append(" <li id='" + value.Id + "'><a id='" + value.Id + "' href='" + value.Url + "'>" + value.Name + "</a>&nbsp;&nbsp;<span id='" + value.Id + "'>[x]</span></li>");
                    }
                });
                attachmentList.children("li").find("span").click(function () {
                    settings.onAttachmentDeletion($(this).attr("id"));
                });
            });

            var attachmentFormSlider = attachmentForm.children("#slider");
            attachmentFormSlider.click(function () {
                attachmentForm.animate({
                    bottom: 0
                }, 200);
                attachmentForm.fadeOut();
            });

            replySendButton.click(function () {
                var replyData = {
                    Recipients: replyRecipient.val(),
                    Message: replyMessage.val()
                };
                settings.onPostingReply(replyData);
                reloadMessages(list);
                scrollDown(listWrapper);
            });
            replyClearButton.click(function () {
                replyRecipient.val("");
                replyMessage.val("");
                replyOptions_SendByEmail.attr("checked", false);
                replyOptions_SendBySMS.attr("checked", false);
                attachmentUploadList.children("ul").empty();
            });

            var attachmentButton = chatBox.find(".buttons #attachment").filter(":first");
            attachmentButton.click(function () {
                attachmentForm.fadeIn("fast");
                attachmentForm.animate({
                    bottom: chatBox.outerHeight()
                }, 200);
            });

            reloadMessages(list);
            chatBox.show();
            slider.click();
        });
    }
})(jQuery)