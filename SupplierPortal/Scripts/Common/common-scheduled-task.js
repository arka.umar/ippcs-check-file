﻿var _cmconst_TASK_SHEDULER_PERIOD = 29000;
var _cmvar_scheduledTasks = {};
var _cmvar_scheduledTaskTimer = null;

function _cmfunc_initTaskScheduler() {
    _cmvar_scheduledTaskTimer = window.setInterval(function () {
        $.each(_cmvar_scheduledTasks, function (key, value) {
            value();
        });
    }, _cmconst_TASK_SHEDULER_PERIOD);
}

function _cmfunc_addScheduledTask(name, scheduledFunction) {
    _cmvar_scheduledTasks[name] = scheduledFunction;
}


/* Rapid Scheduled Task */
var _cmconst_RAPID_TASK_SHEDULER_PERIOD = 1000;
var _cmvar_rapidScheduledTasks = {};
var _cmvar_rapidScheduledTaskCount = 0;
var _cmvar_rapidScheduledTaskTimer = null;

function _cmfunc_initRapidTaskScheduler() {
    _cmvar_rapidScheduledTaskTimer = window.setInterval(function () {
        $.each(_cmvar_rapidScheduledTasks, function (key, value) {
            value();
        });
    }, _cmconst_RAPID_TASK_SHEDULER_PERIOD);
}

function _cmfunc_clearRapidTaskScheduler() {
    window.clearInterval(_cmvar_rapidScheduledTaskTimer);
}

function _cmfunc_addRapidScheduledTask(name, scheduledFunction) {
    _cmvar_rapidScheduledTasks[name] = scheduledFunction;
    _cmvar_rapidScheduledTaskCount++;
    if(_cmvar_rapidScheduledTaskCount == 1) {
        _cmfunc_initRapidTaskScheduler();
    }
}

function _cmfunc_removeRapidScheduledTask(name) {
    var map = {};
    $.each(_cmvar_rapidScheduledTasks, function (key, value) {
        if (key != name) {
            map[key] = value;
        } else {
            _cmvar_rapidScheduledTaskCount--;
        }
    });
    _cmvar_rapidScheduledTasks = map;

    if (_cmvar_rapidScheduledTaskCount == 0) {
        _cmfunc_clearRapidTaskScheduler();
    }
}