﻿var cmDialogOptions = {
    resizable: false,
    width: 300,
    height: 200,
    modal: false,
    buttons: {
        "Close": function () {
            $(this).dialog("close");
        }
    }
};

function cmOpenDialog(Id, properties) {
    cmDialogOptions.resizable = properties.resizable;
    cmDialogOptions.width = properties.width;
    cmDialogOptions.height = properties.height;
    cmDialogOptions.buttons = properties.buttons;
    $("#" + Id).dialog(cmDialogOptions);
}

function cmOpenDialog(Id, dgWidth, dgHeight) {
    cmDialogOptions.width = dgWidth;
    cmDialogOptions.height = dgHeight;
    $("#" + Id).dialog(cmDialogOptions);
}

function cmOpenDialog(Id) {
    cmDialogOptions.resizable = true;
    $("#" + Id).dialog(cmDialogOptions);
}

function cmOpenModalDialog(Id, dgWidth, dgHeight) {
    cmDialogOptions.modal = true;
    cmDialogOptions.width = dgWidth;
    cmDialogOptions.height = dgHeight;
    $("#" + Id).dialog(cmDialogOptions);
}

