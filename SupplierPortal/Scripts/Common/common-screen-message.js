﻿function ScreenMessageStack(containerId, url) {
    this.url = url;
    this.containerId = containerId;
}

ScreenMessageStack.prototype.update = function () {
    var container = $("#" + this.containerId);
    $.ajax({
        url: this.url,
        type: "GET",
        dataType: "json",
        success: function (data) {
            container.empty();
            var messages = data.Messages;
            if (messages.length > 0) {
                var list = container.children("ul");
                if ((typeof list == 'undefined') || (list.length == 0)) {
                    container.append("<ul/>");
                    list = container.children("ul");
                }
                $.each(messages, function () {
                    list.append("<li class=\"Error\">" + this["Message"] + "</li>");
                });
                list.jqErrorDisplayer();
            }
        }
    });
}