/*
	Error Message Displayer Plugin for jQuery
	Author: Lufty Abdillah
*/
(function ($) {
    $.fn.jqErrorDisplayer = function (options) {

        return this.each(function () {
            var CLASS_PREFIX = "jqErrorDisplayer";
            var CLASS_WRAPPER = CLASS_PREFIX + "-Wrapper";
            var CLASS_LIST_WRAPPER = CLASS_PREFIX + "-ListWrapper";
            var CLASS_LIST = CLASS_PREFIX + "-List";
            var CLASS_MESSAGES = CLASS_PREFIX + "-ListItem";
            var CLASS_MESSAGES_TEXT = CLASS_PREFIX + "-ListItem-Text";
            var CLASS_HEADLINE_WRAPPER = CLASS_PREFIX + "-HeadlineWrapper";
            var CLASS_HEADLINE = CLASS_PREFIX + "-Headline";
            var CLASS_HEADLINE_IMAGE = CLASS_PREFIX + "-HeadlineImage";
            var CLASS_DETAIL_SCROLLER = CLASS_PREFIX + "-DetailScroller";
            var CLASS_DETAIL_SCROLLER_WRAPPER = CLASS_PREFIX + "-DetailScrollerWrapper";
            var CLASS_DETAIL_CLOSER = CLASS_PREFIX + "-DetailCloser";

            var id = $(this).attr("id");
            var ID_WRAPPER = id + "-Wrapper";
            var ID_LIST_WRAPPER = id + "-MessageList-Wrapper";
            var ID_MESSAGES = id + "-List-Item";
            var ID_MESSAGES_TEXT = id + "-List-Item-Text";
            var ID_HEADLINE_WRAPPER = id + "-Headline-Wrapper";
            var ID_HEADLINE = id + "-Headline";
            var ID_HEADLINE_IMAGE = id + "-HeadlineImage";
            var ID_DETAIL_SCROLLER = id + "-DetailScroller";
            var ID_DETAIL_SCROLLER_WRAPPER = id + "-DetailScrollerWrapper";
            var ID_DETAIL_CLOSER = id + "-DetailCloser";

            $(this).addClass(CLASS_LIST);
            $(this).wrapAll(
				"<div id='" + ID_LIST_WRAPPER + "' class='" + CLASS_LIST_WRAPPER + "'/>"
			);
            $listWrapper = $(this).parent();
            $listWrapper.wrapAll(
				"<div id='" + ID_WRAPPER + "' class='" + CLASS_WRAPPER + "'/>"
			);

            $wrapper = $listWrapper.parent();
            $wrapper.append("<div id='" + ID_HEADLINE_IMAGE + "' class='" + CLASS_HEADLINE_IMAGE + "'/>");
            $wrapper.append(
				"<div id='" + ID_HEADLINE_WRAPPER + "' class='" + CLASS_HEADLINE_WRAPPER + "'>" +
				"	<div id='" + ID_HEADLINE + "' class='" + CLASS_HEADLINE + "'></div>" +
				"	<span id='" + ID_DETAIL_SCROLLER + "' class='" + CLASS_DETAIL_SCROLLER + "'>more</span>" +
				"</div>"
			);
            $wrapper.prepend("<span id='" + ID_DETAIL_CLOSER + "' class='" + CLASS_DETAIL_CLOSER + "'>close</span>");

            $detailCloser = $("#" + ID_DETAIL_CLOSER);
            $detailCloser.hide();
            $headlineWrapper = $("#" + ID_HEADLINE_WRAPPER);
            $headline = $("#" + ID_HEADLINE);
            $scroller = $("#" + ID_DETAIL_SCROLLER);

            $messages = $(this).children("li");
            if ($messages.length < 2) {
                $("#" + ID_DETAIL_SCROLLER).hide();
            }

            $messages.attr("id", ID_MESSAGES);
            $messages.addClass(CLASS_MESSAGES);
            $messages.each(function () {
                $(this).hover(function () {
                    $messages.filter(".jqErrorDisplayer-ListItem-Focused").removeClass("jqErrorDisplayer-ListItem-Focused");
                    $(this).addClass("jqErrorDisplayer-ListItem-Focused");
                }, function () {
                    $(this).removeClass("jqErrorDisplayer-ListItem-Focused");
                });
            });

            $headline.append($messages.filter(":first").text());
            $headlineImage = $("#" + ID_HEADLINE_IMAGE);

            $listWrapper.hide();
            $scroller.click(function () {
                $headlineWrapper.slideUp("fast", "swing", function () { });
                $listWrapper.slideDown("fast", "swing", function () { });
                $headlineImage.hide();
                $detailCloser.fadeIn("fast", "swing", function () { });
            });

            $detailCloser.click(function () {
                $headlineWrapper.slideDown("fast", "swing", function () { });
                $listWrapper.slideUp("fast", "swing", function () { });
                $headlineImage.show();
                $(this).fadeOut("fast", "swing", function () { });
            });
        });
    }
})(jQuery);