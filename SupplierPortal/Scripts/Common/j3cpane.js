﻿/* 
Three column layout with collapsible pane.
Author: Lufty Abdillah
*/

(function ($) {
    $.fn.j3cpane = function (options) {
    	var settings = $.extend({
    		hideLeftPane: false,
    		hideRightPane: false
    	}, options);
    
    	var leftPaneScrollerId = "j3cpane-leftScroller";
    	var rightPaneScrollerId = "j3cpane-rightScroller";
        var leftPaneScrollerImageId = "j3cpane-leftScrollerImage";
        var rightPaneScrollerImageId = "j3cpane-rightScrollerImage";
            
        var leftPaneHidden = false;
        var rightPaneHidden = false;
        
		/*
			position: left, right
		*/
		function appendScroller(pane, container, position) {
			var id = leftPaneScrollerId;
			var imageId = leftPaneScrollerImageId;
			var scrollerClass = "j3cpane-left-pane-scroller";			
			if(position == "right") {
				id = rightPaneScrollerId;
				imageId = rightPaneScrollerImageId;
				scrollerClass = "j3cpane-right-pane-scroller";
			}
			
            var body = $("body");
			body.append("<div id='" + id + "'/>");
            var scroller = body.children("#" + id);
            scroller.addClass(scrollerClass);
            scroller.hide();
            scroller.append("<div id='" + imageId + "'/>");
            
            scroller.click(function () {
            	if(position == "left") {
                	if (leftPaneHidden) {
                    	var leftPadding = 0;
                    	var step = settings.leftPaneWidth/6;
                    	var bound = settings.leftPaneWidth - step;
                        pane.show();
                    	while (leftPadding < settings.leftPaneWidth) {
                    		if(leftPadding < bound) {
                        		leftPadding += step;
                        	} else {
                        		leftPadding = settings.leftPaneWidth;
                        	}
                        	container.animate({
                            	paddingLeft: leftPadding + "px"
                        	}, 10, "swing", function () { });
                    	}
                    	
                    	leftPaneHidden = false;
                	} else {
                    	var leftPadding = settings.leftPaneWidth;
                    	var step = settings.leftPaneWidth/6;
                    	while (leftPadding > 0) {
                    		if(leftPadding >= step) {
                        		leftPadding -= step;
                        	} else {
                        		leftPadding = 0;
                        	}
                        	container.animate({
                            	paddingLeft: leftPadding + "px"
                        	}, 10, "swing", function () {
                                if(leftPadding == 0) {
                                    pane.hide();
                                }
                            });
                    	}
                    	
                    	leftPaneHidden = true;
                	}
                } else {
                	if (rightPaneHidden) {
                    	var rightPadding = 0;
                    	var step = settings.rightPaneWidth/6;
                    	var bound = settings.rightPaneWidth - step;
                        pane.show();
                    	while (rightPadding < settings.rightPaneWidth) {
                    		if(rightPadding < bound) {
                        		rightPadding += step;
                        	} else {
                        		rightPadding = settings.rightPaneWidth;
                        	}                            
                        	container.animate({
                            	paddingRight: rightPadding + "px"
                        	}, 10, "swing", function () { });
                    	}
                    
                    	rightPaneHidden = false;
                	} else {
                    	var rightPadding = parseInt(container.css("padding-right"), 10);
                    	var step = settings.leftPaneWidth/6;
                    	while (rightPadding > 0) {
                    		if(rightPadding >= step) {
                        		rightPadding -= step;
                        	} else {
                        		rightPadding = 0;
                        	}
                        	container.animate({
                            	paddingRight: rightPadding + "px"
                        	}, 100, "swing", function () { 
                                if(rightPadding == 0) {
                                    pane.hide();
                                }
                            });
                    	}
                    	rightPaneHidden = true;
                	}
                }
            });
            
            scroller.hover(
				function () { },
				function () { $(this).hide(); }
			);
			
			return scroller;
		}        

        return this.each(function () {           
			var container = $(this);
            var leftPane = $(".j3cpane-left");
            var rightPane = $(".j3cpane-right");
            var centerPane = $(".j3cpane-center");			
			
			/* Init custom styling */
			﻿settings.leftPaneWidth = leftPane.outerWidth();
        	settings.rightPaneWidth = rightPane.outerWidth();

            /* Add slider button to left and right pane */
            var leftPaneScroller = appendScroller(leftPane, container, "left");
            var rightPaneScroller = appendScroller(rightPane, container, "right");
			
			if(settings.hideLeftPane) {
				leftPaneScroller.trigger("click");
			}
			
			if(settings.hideRightPane) {
				rightPaneScroller.trigger("click");
			}
			
            $(document).mousemove(function (e) {
                if (e.pageX < 5) {		
                	var leftScroller = leftPaneScroller;
                	var scrollerImage = leftScroller.children("#" + leftPaneScrollerImageId);              	
                	if(leftPaneHidden) {
                		scrollerImage = leftScroller.children("#" + leftPaneScrollerImageId);   
                		scrollerImage.removeClass("j3cpane-left-pane-scroller-image");
                		scrollerImage.addClass("j3cpane-left-pane-scroller-image-restore");
                	} else {
                		scrollerImage.removeClass("j3cpane-left-pane-scroller-image-restore");
                		scrollerImage.addClass("j3cpane-left-pane-scroller-image");
                	}
                    leftScroller.css("top", e.pageY - (leftScroller.height() / 2));
                    leftScroller.fadeIn("fast", "swing");
                } else {
                    leftPaneScroller.hide();
                }

                if (e.pageX > $(window).innerWidth() - 5) {
                	var rightScroller = rightPaneScroller;     
                	var scrollerImage = rightScroller.children("#" + rightPaneScrollerImageId);             	    
                	if(rightPaneHidden) {
                		scrollerImage = rightScroller.children("#" + rightPaneScrollerImageId);  
                		scrollerImage.removeClass("j3cpane-right-pane-scroller-image");
                		scrollerImage.addClass("j3cpane-right-pane-scroller-image-restore");
                	} else {
                		scrollerImage.removeClass("j3cpane-right-pane-scroller-image-restore");
                		scrollerImage.addClass("j3cpane-right-pane-scroller-image");
                	}
                    rightScroller.css("top", e.pageY - (rightScroller.height() / 2));
                    rightScroller.fadeIn("fast", "swing");
                } else {
                    rightPaneScroller.hide();
                }
            });
        });
    }
})(jQuery)