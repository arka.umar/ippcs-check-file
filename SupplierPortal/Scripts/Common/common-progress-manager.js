﻿/* Progress object */
function Progress(progressName, callback) {
    var name = progressName;
    var complete = false;
    var position = 0;
    var updateFunction = callback;

    this.GetName = function () { return name; }
    this.SetComplete = function (status) { complete =  status}
    this.IsComplete = function () { return complete }
    this.SetPosition = function (currentPosition) { position = currentPosition; }
    this.GetPosition = function () { return position; }
    this.PerformUpdate = function () {
        updateFunction(position);
    }
}

/* Progress manager */
function ProgressManager(url, interval) {
    var updateUrl = url;
    var queue = new Array();
    var updateInterval = interval;
    var running = false;
    var updateTimer;

    var getProgress = function (name) {
        var progress;
        for (var i = queue.length - 1; i >= 0; i--) {
            progress = queue[i];
            if (progress.GetName() == name) {
                return progress;
            }
        }
    }

    var startUpdate = function () {
        updateTimer = setInterval(function () {
            var progressNames = new Array();
            var progress;
            for (var i = queue.length - 1; i >= 0; i--) {
                progressNames.push(queue[i].GetName());
            }

            $.ajax({
                url: updateUrl,
                type: "POST",
                data: "param=" + JSON.stringify(progressNames),
                dataType: "json",
                success: function (data) {
                    var progress;
                    $.each(data, function (key, val) {
                        progress = getProgress(key);
                        if ((progress != null) && (progress != undefined)) {
                            progress.SetPosition(val);
                            progress.PerformUpdate();
                            if (val == '100') {
                                GetProgressManager().Remove(progress.GetName());
                            }
                        }
                    })
                }
            });
        }, updateInterval);
    }

    this.Size = function () { return queue.length; }
    this.Register = function (progress) {
        queue.push(progress);
        if (!running && (queue.length > 0)) {
            startUpdate();
            running = true;
        }
    }
    this.Remove = function (name) {
        var progress;
        for (var i = queue.length - 1; i >= 0; i--) {
            progress = queue[i];
            if (progress.GetName() == name) {
                queue.splice(i, 1);
                break;
            }
        }

        if (queue.length == 0) {
            clearInterval(updateTimer);
        }
    }
    this.SetUpdateUrl = function (url) { updateUrl = url; }
    this.SetUpdateInterval = function (interval) { this.updateInterval = interval; }    
}

/* Single object for entire page */
var cmProgressManager = new ProgressManager("", 5000);

function GetProgressManager() {
    return cmProgressManager;
}



/* Common Progress Bar */
var __cmfunc_commonProgressBarContainer;
var __cmfunc_commonProgressBar;
var __cmfunc_commonProgressBar_Label;
function _cmfunc_openProgressBar() {
    __cmfunc_commonProgressBarContainer = $("#common-progress-bar-container");
    __cmfunc_commonProgressBar = __cmfunc_commonProgressBarContainer.children(".progress-bar:first");
    __cmfunc_commonProgressBar_Label = __cmfunc_commonProgressBarContainer.children(".label:first");

    __cmfunc_commonProgressBar.progressbar({
        value: false,
        change: function () {
            __cmfunc_commonProgressBar_Label.text("Processing ... " + __cmfunc_commonProgressBar.progressbar("value") + "%");
        },
        complete: function() {
            __cmfunc_commonProgressBar_Label.text("Done");
        }
    });

    $("#common-progress-bar-container").fadeIn();
}

function _cmfunc_closeProgressBar() {
    __cmfunc_commonProgressBarContainer.fadeOut();
}

function _cmfunc_setProgressBarValue(progressValue) {
    __cmfunc_commonProgressBar.progressbar("value", progressValue);
}