﻿function _cmfunc_messageBoard_initEvents(id) {
    $("html").click(function () {
        var board = $("#" + id);
        var openFlag = board.find("#messageBoard_openFlag_" + id);
        if (openFlag.val() === "true") {
            _cmfunc_messageBoard_close(id);
        }
    });
}

function _cmfunc_messageBoard_markRead(boardName) {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_MessageBoard_MarkRead"),
        dataType: "text",
        data: {
            boardName: boardName
        }
    });
}

function _cmfunc_messageBoard_open(id) {
    _cmfunc_messageBoard_open(id, null);
}

function _cmfunc_messageBoard_open(id, boardName) {
    _cmfunc_messageBoard_scrollDownMessagesById(id);
    if (boardName != null) {
        _cmfunc_messageBoard_setBoardName(id, boardName);
    }

    var element = $("#" + id);
    var openFlag = element.find("#messageBoard_openFlag_" + id);
    var right = parseInt(element.css("right"), 10);
    if (element.hasClass("right")) {
        element.animate({
            right: 30
        }, "fast", "swing", function () {
            openFlag.val("true");
            if (right < 0) {
                _cmfunc_messageBoard_reloadMessages(id, boardName);
            }            
            _cmfunc_messageBoard_markRead(boardName);
        });
    } else {
    element.animate({
        left: 30
    }, "fast", "swing", function () {
        openFlag.val("true");
        if (right < 0) {
            _cmfunc_messageBoard_reloadMessages(id, boardName);
        }        
        _cmfunc_messageBoard_markRead(boardName);
    });
    }    
}

function _cmfunc_messageBoard_close(id) {
    _cmfunc_messageBoard_hideReplyForm(id);

    var element = $("#" + id);
    var openFlag = element.find("#messageBoard_openFlag_" + id);    
    if (element.hasClass("right")) {
        element.animate({
            right: -($(document).width() / 2)
        }, "fast", "swing", function () { openFlag.val("false"); });
    } else {
        element.animate({
            left: -($(document).width() / 2)
        }, "fast", "swing", function () { openFlag.val("false"); });
    }
}

function _cmfunc_messageBoard_openAttachments(id, msgNumber) {
    var element = $("#" + id);
    var messageList = element.find(".message-wrapper .message-list").filter(":first");
    var item = messageList.find("#message-item-" + msgNumber).filter(":first");
    var attachments = item.children(".attachments:first");
    if(attachments.is(":visible")) {
        attachments.slideUp(100, "swing", function () { });
    } else {
        attachments.slideDown(100, "swing", function () { });
    }
    if (item.is(":last-child")) {
        _cmfunc_messageBoard_scrollDownMessagesById(id);
    }
}

function _cmfunc_messageBoard_reloadNewAttachments(id, uploaderId) {
    var board = $("#" + id);
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_MessageBoard_GetNewAttachments"),
        dataType: "json",
        data: {
            id: uploaderId
        },
        success: function (data) {
            var list = board.find(".uploaded-list .list:first");
            list.empty();
            if (data != '') {
                $(data).each(function (index, value) {
                    list.append("<li id=\"" + value.Id + "\"><a href=\"" + value.Url + "\" target=\"_blank\">" + value.Filename + "</a>&nbsp;&nbsp;<span class=\"attachment-removal\" onclick=\"_cmfunc_messageBoard_removeAttachment('" + id + "'," + value.Id + ");\">[ X ]</span></li>");
                });
            }
        }
    });
}

function _cmfunc_messageBoard_removeNewAttachments(boardId) {
    var board = $("#" + boardId);
    var list = board.find(".uploaded-list .list:first");
    list.children("li").each(function () {
        _cmfunc_messageBoard_removeAttachment(boardId, $(this).attr("id"));
    });
}

function _cmfunc_messageBoard_openRecipients(id, msgNumber) {
    var element = $("#" + id);
    var messageList = element.find(".message-wrapper .message-list").filter(":first");
    var item = messageList.find("#message-item-" + msgNumber).filter(":first");
    var recipients = item.children(".recipients:first");
    if (recipients.is(":visible")) {
        recipients.slideUp(100, "swing", function () { });
    } else {
        recipients.slideDown(100, "swing", function () { });
    }
    if (item.is(":last-child")) {
        _cmfunc_messageBoard_scrollDownMessagesById(id);
    }
}

function _cmfunc_messageBoard_hideReplyForm(id) {
    var element = $("#" + id);
    var form = element.children(".message-form:first");
    var messageWrapper = element.children(".message-wrapper:first");
    var boardButtons = element.children(".buttons:first");

    element.children(".attachment-form:first").fadeOut("fast", function () {
        form.fadeOut("fast");
        messageWrapper.animate({
            height: "100%"
        }, "fast", function () {
            boardButtons.show();
            messageWrapper.css("height", messageWrapper.outerHeight() - 30);
            _cmfunc_messageBoard_scrollDownMessages(messageWrapper);
        });
    });    
}

function _cmfunc_messageBoard_showReplyForm(id) {
    var element = $("#" + id);
    var form = element.children(".message-form:first");
    var messageWrapper = element.children(".message-wrapper:first");
    var boardButtons = element.children(".buttons:first");

    boardButtons.fadeOut("fast");
    messageWrapper.animate({
        height: "60%"
    }, "fast", function () {
        _cmfunc_messageBoard_scrollDownMessages(messageWrapper);
        _cmfunc_messageBoard_toggleAttachmentForm(id);
    });
    form.fadeIn("fast");
}

function _cmfunc_messageBoard_scrollDownMessagesById(id) {
    var element = $("#" + id);
    var messageWrapper = element.children(".message-wrapper:first");
    messageWrapper.animate({
        scrollTop: 2000
    }, "fast", function () { });
}

function _cmfunc_messageBoard_scrollDownMessages(wrapper) {
    wrapper.animate({
        scrollTop: 2000
    }, "fast", function () { });
}

function _cmfunc_messageBoard_updateRecipients(list, callback) {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_GetUserLogin"),
        dataType: "json",
        success: function (data) {
            $(data).each(function (index, value) {
                list.AddItem(value.FirstName + " " + value.LastName, value.Username);
            });
            callback(data);
        }
    });
}

function _cmfunc_messageBoard_updateRecipientText(list, dropdown) {
    var selectedItems = list.GetSelectedItems();
    var itemTexts = [];
    for (var i = 0; i < selectedItems.length; i++) {
        itemTexts.push(selectedItems[i].value);
    }
    dropdown.SetText(itemTexts.join(","));
}

function _cmfunc_messageBoard_toggleAttachmentForm(id) {
    var element = $("#" + id);
    var button = window['btAttachment_' + id];
    var form = element.children(".attachment-form:first");
    if (form.is(":visible")) {
        form.fadeOut("fast");
        button.SetText("Attachment");
    } else {
        form.fadeIn("fast");
        button.SetText("Hide Attachment");
    }    
}

function _cmfunc_messageBoard_removeAttachment(boardId, id) {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_FileUpload_Remove"),
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            if ($.trim(data) == "true") {
                var board = $("#" + boardId);
                var list = board.find(".uploaded-list .list:first");
                var removedItem = list.children("#" + id).filter(":first");
                removedItem.remove();
            }
        }
    });
}

/* for internal use only */
function _cmfunc_messageBoard_constructMessageList(id, data) {
    var element = $("#" + id);
    var messageList = element.find(".message-list:first");
    var messageWrapper = element.children(".message-wrapper:first");
    var noDataSpan = messageWrapper.children(".no-data");
    noDataSpan.text("No data to be displayed");
    messageList.empty();
    var jdata = $(data);
    if (jdata.length > 0) {
        noDataSpan.remove();
        var imageUrl;
        var attachmentCodes;
        var attachmentSliderClass;
        var attachments;
        $(data).each(function (index, value) {
            attachmentCodes = "";
            attachmentSliderClass = "";
            attachments = value.Attachments;
            if (attachments.length > 0) {
                imageUrl = _cmvar_ApplicationUrl + "/Content/Images/Common/attachment-16.png";
                attachmentSliderClass = "pointed";
                for (var i = 0; i < attachments.length; i++) {
                    attachmentCodes += "<li class=\"attachment-item\">" +
                                        "   <a id=" + attachments[i].Id + " href=" + attachments[i].Url + " target=\"_blank\">" + attachments[i].Description + "</a>" +
                                        "</li>"
                }
            } else {
                imageUrl = _cmvar_ApplicationUrl + "/Content/Images/Common/attachment-16-gray.png";
            }
            messageList.append(
                "<li id=\"message-item-" + value.GroupId + "\" class=\"message-item " + ((index % 2) == 0 ? "even" : "odd") + "\">" +
                "   <div class=\"sender\">" + value.Sender + "</div>" +
                "   <div class=\"date\">" + value.DateInString + "</div>" +
                "   <div class=\"recipient-info\"> " +
                "       <img alt=\"Recipients Detail\" class=\"pointed\" width=\"16\" height=\"16\" src=\"" + _cmvar_ApplicationUrl + "/Content/Images/Common/MySpace-16.png\" onclick=\"_cmfunc_messageBoard_openRecipients('" + id + "', " + (index + 1) + ")\"/>" +
                "   </div>" +
                "   <div class=\"recipients\">" +
                "       <div class=\"recipient-list to\"><span>To :</span> " + value.Recipients.join(", ") + "</div>" +
                "       <div class=\"recipient-list cc\"><span>cc :</span> " + value.CCRecipients.join(", ") + "</div>" +
                "   </div>" +
                "   <div class=\"attachments-slider\"> " +
                "       <img alt=\"Attachments\" class=\"" + attachmentSliderClass + "\" width=\"16\" height=\"16\" src=\"" + imageUrl + "\" onclick=\"_cmfunc_messageBoard_openAttachments('" + id + "', " + value.GroupId + ")\"/>" +
                "   </div>" +
                "   <div class=\"attachments\">" +
                "       <ul class=\"attachment-list\">" + attachmentCodes + "</ul>" +
                "   </div>" +
                "   <div class=\"text\">" + value.Text + "</div> " +
                "</li>"
            );
        });
    } else {
        messageWrapper.append(
            "<span class='no-data'>No data to be displayed</span>"
        );
    }

    messageList.children("li").last().effect("highlight", { color: "#FFC400" }, 2000);
}

function _cmfunc_messageBoard_reloadMessages(id, name) {
    var element = $("#" + id);
    var messageList = element.find(".message-list:first");
    messageList.empty();
    var messageWrapper = element.children(".message-wrapper:first");
    messageWrapper.children(".no-data").text("Loading ...");
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_MessageBoard_LoadMessages"),
        dataType: "json",
        data: {
            boardName: name
        },
        success: function (data) {
            _cmfunc_messageBoard_constructMessageList(id, data);
            _cmfunc_messageBoard_scrollDownMessagesById(id);
        }
    });
}

function _cmfunc_messageBoard_sendReply(id, uploadId, name, recipients, text, notifyBySMS, notifyByEmail, notifyByWall) {
    var board = $("#" + id);
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_MessageBoard_SaveMessage"),
        dataType: "json",
        data: {
            boardName: name,
            uploadId: uploadId,
            recipients: recipients,
            text: text,
            groupId: _cmfunc_messageBoard_getLastDisplayedMessageId(id),
            notifyBySMS: notifyBySMS,
            notifyByEmail: notifyByEmail,
            notifyByWall: notifyByWall
        },
        success: function (data) {
            var list = board.find(".uploaded-list .list:first");
            list.empty();
            _cmfunc_messageBoard_constructMessageList(id, data);
            _cmfunc_messageBoard_scrollDownMessagesById(id);
            _cmfunc_messageBoard_toggleAttachmentForm(id);
        }
    });
}

function _cmfunc_messageBoard_saveUploadCategory(id, group, key, qualifier) {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_FileUpload_CacheUploadCategory"),
        data: {
            id: id,
            group: group,
            key: key,
            qualifier: qualifier
        }
    });
}

function _cmfunc_messageBoard_getLastDisplayedMessageId(id) {
    var element = $("#" + id);
    var messageList = element.find(".message-list:first");
    var messageItems = messageList.find(".message-item");
    var lastId = messageItems.last().attr("id");
    if (lastId != undefined) {
        var numNextId = parseInt(lastId.substring(13, lastId.length), 10);
        return (numNextId + 1);
    }
    return 1;
}

function _cmfunc_messageBoard_setBoardName(id, boardName) {
    var element = $("#" + id);
    var boardNameHolder = element.find("#messageBoard_boardNameHolder_" + id).filter(":first");
    boardNameHolder.val(boardName);
}

function _cmfunc_messageBoard_getBoardName(id) {
    var element = $("#" + id);
    var boardNameHolder = element.find("#messageBoard_boardNameHolder_" + id).filter(":first");
    return boardNameHolder.val();
}