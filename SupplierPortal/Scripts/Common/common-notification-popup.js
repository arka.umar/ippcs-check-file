﻿var __cmfunc_EXPIRE_TIMEOUT = 10000;

function _cmfunc_initNotificationPopup() {
    $("body").append(
         '<div id="notification-popup-container" style="display:none">' +
            '<div id="empty">' +
                '<a class="ui-notify-cross ui-notify-close" href="#">x</a>' +
                '<a class="ui-notify-cross ui-notify-close" href="#"> | </a>' +
                '<a class="ui-notify-cross ui-notify-close mark-read" href="#" onclick="javascript:_cmfunc_popupMarkAsRead(#{id});">read</a>' +
                '<h1 style="border-bottom: 1px dotted #fff; padding-bottom: 5px; margin-bottom: 10px;">#{title}</h1>' +
                '<p>#{text}</p>' +
            '</div>' +
        '</div>'
    );
    $("#notification-popup-container").notify();  
}

function _cmfunc_popupNotifications() {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_Notification_Load"),
        dataType: "json",
        success: function (data) {
            var container = $("#notification-popup-container");
            $(data).each(function (index, value) {
                container.notify("create", "empty", {
                    title: value.Title,
                    text: value.Content,
                    id: value.Id
                }, {
                    queue: true,
                    speed: 500,
                    expires: 10000,
                    click: function (e, instance) {
                        window.open(value.ActionUrl, "_blank");
                    }
                });
            });
        }
    });
}
function _cmfunc_popupMarkAsRead(id) {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_Notification_MarkAsRead"),
        dataType: "text",
        data: { id: id}
    });
}