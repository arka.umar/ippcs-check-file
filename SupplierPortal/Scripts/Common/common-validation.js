﻿function InputValidator(url) {
    this.Url = url;
}

InputValidator.prototype.validate = function (name, values, callback) {
    var requestData = {};
    requestData["name"] = name;

    $.map(values, function (value, key) {
        requestData[key] = value;
    });    

    $.ajax({
        url: this.Url,
        type: "POST",
        data: requestData,
        dataType: "json",
        error: function () {
            var validation = new InputValidation();
            validation.defineError();
            callback(validation);
        },
        success: function (data) {
            var validation = new InputValidation();
            validation.Valid = data.Valid;
            callback(validation);
        }
    });
}


/* -------------------------------------------------------------------------------------------------- */

function InputValidation() {
    this.Status = "success";
    this.Valid = false;
}

InputValidation.prototype.defineError = function () {
    this.Status = "error";
}

InputValidation.prototype.defineSuccess = function () {
    this.Status = "success";
}