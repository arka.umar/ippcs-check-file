if (!String.format) {
    String.format = function () {
        var a = Object.assign([], arguments);
        var ax = [];
        for (var i = 1; i < arguments.length; i++) {
            ax.push(a[i])
        }
        return (a[0]).replace(/((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g,
	        function (m, str, index) {
	            if (str) {
	                return str.replace(/(?:{{)|(?:}})/g,
				        function (m) { return m[0]; });
	            } else {
	                if (index > arguments.length) {
	                    return '';
	                } return ax[parseInt(index)];
	            }
	        });
    };
}
function Msg(id) {
    if (window.localStorage) {
        var r = window.localStorage.getItem(id);
        if (r == null || r == undefined) {
            $.ajax({
                url: getActMethodUrl("Messages") + "?msgid=" + id
               , async: false
               , success: function (data) {
                   var ro = data.split("\n");
                   ro.forEach(function (a, i, x) {
                       var row = a.split("\t");
                       if (row.length > 1) {
                           console.log(row[0], ' = ', row[1]);
                           window.localStorage.setItem(row[0], row[1]);
                           if (row[0] === id)
                               r = row[1];
                       }
                   });
               }
            });
       }
       if (r!==null && r.match(/{[0-9]+}/g))
       {
         var a = Object.assign([], arguments);
         a[0] = r;
         r = String.format.apply(this, a);
       }
       return r;
    } else {
        return id;
    }
}