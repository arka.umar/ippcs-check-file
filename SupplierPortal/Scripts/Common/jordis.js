﻿/* 
Document    : jordis
Created on  : Dec 27, 2012, 8:21:13 PM
Author      : Lufty Abdillah
Description : A simple error displayer        
*/

(function ($) {
    $.fn.jordis = function (options) {
        var settings = $.extend({
            onItemClicked: function (id, content) {  }
        }, options);

        return this.each(function () {
            var list = $(this);
            var listItems = list.children("li");
            if ((listItems == undefined) || (listItems.length == 0)) {
                return;
            }
            var listItem;
            listItems.each(function (index) {
                listItem = $(this);
                listItem.addClass("jordis-item");
                listItem.css("padding", 5);
                listItem.css("padding-left", 7);
                listItem.css("cursor", "pointer");
                if ((index % 2) == 0) {
                    listItem.addClass("jordis-item-even");
                } else {
                    listItem.addClass("jordis-item-odd");
                }
                listItem.click(function (e) {
                    e.stopPropagation();
                    settings.onItemClicked(listItem.attr("id"), listItem.text());
                });
            });

            list.css("list-style", "none");
            list.css("margin", 10);
            list.css("padding", 0);
            var listId = list.attr("id");
            var windowHeight = $(window).height();
            list.addClass("jordis-list");
            list.wrapAll("<div id='" + listId + "-list-wrapper'/>");

            var listWrapper = list.parent();
            listWrapper.addClass("jordis-list-wrapper");
            listWrapper.css("position", "fixed");
            listWrapper.css("bottom", -listWrapper.outerHeight());
            listWrapper.css("left", 0);
            listWrapper.css("overflow", "auto");
            listWrapper.css("width", "100%");
            var listWrapperHeight = listWrapper.outerHeight();
            if (listWrapperHeight >= windowHeight) {
                listWrapper.css("height", windowHeight / 2);
            }
            listWrapper.wrapAll("<div id='" + listId + "-wrapper'/>");

            var wrapper = listWrapper.parent();
            wrapper.hide();
            wrapper.addClass("jordis-wrapper");
            wrapper.css("position", "fixed");
            wrapper.css("left", 0);
            wrapper.css("top", 0);
            wrapper.css("width", "100%");
            wrapper.css("height", "100%");
            wrapper.click(function (e) {
                e.stopPropagation();
                var listWrapperOffset = listWrapper.offset();
                if (e.pageY < listWrapperOffset.top) {
                    listWrapper.animate({ bottom: -listWrapper.outerHeight() }, "fast", "swing", function () {
                        wrapper.fadeOut("fast", "swing", function () { });
                    });
                }
            });

            wrapper.fadeIn("fast", "swing", function () {
                listWrapper.animate({ bottom: 0 }, "fast", "swing", function () { });
            });
        });
    }
})(jQuery)