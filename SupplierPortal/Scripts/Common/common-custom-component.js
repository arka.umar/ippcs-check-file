﻿function cmInitImagePreviewMenu(id) {
    var component = $("#" + id);
    component.addClass("image-preview");
    component.css("cursor", "pointer");

    component.wrapAll('<div class="image-preview-container "/>');
    var parent = component.parent();
    parent.append('<div class="image-preview-menu"/>');
    var previewMenu = parent.children(".image-preview-menu");
    previewMenu.hide();
    previewMenu.append(
        '<input type="button" value="Edit" class="image-preview-menu-button"/>' +
        '<input type="button" value="Delete" class="image-preview-menu-button"/>' +
        '<input type="button" value="View" class="image-preview-menu-button"/>' +
        '<span class="image-preview-close">X</span>'
    );

    component.click(function () {
        $(".image-preview-menu").hide();
        $(".image-preview").removeClass("image-preview-selected", "fast");
        $(this).addClass("image-preview-selected", "fast");
        var previewMenu = $(this).parent().children(".image-preview-menu");
        previewMenu.css("left", (component.width()) + "px");
        previewMenu.fadeIn();        
    });

    previewMenu.children(".image-preview-close").click(function () {
        previewMenu.fadeOut();
        component.removeClass("image-preview-selected", "fast");
    });
}