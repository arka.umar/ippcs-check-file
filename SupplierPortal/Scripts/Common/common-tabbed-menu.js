/*
Tabbed Menu Plugin for jQuery
Author: Lufty Abdillah
*/
(function ($) {
    $.fn.jqTabbedMenu = function (options) {
        var settings = $.extend({
            openedMenuId: null
        }, options);

        var subsubmenuTriggered = false;
        var wideScreen = $(document).width() > 1024;
        var ie7 = false;
        var ie8 = false;
        if ($.browser.msie) {
            var ieVersion = parseInt($.browser.version, 10);
            ie7 = (ieVersion >= 7) && (ieVersion < 8);
            ie8 = (ieVersion >= 8) && (ieVersion < 9);
        }

        var ID_MENU_WRAPPER = "MenuWrapper";
        var ID_MENU_LIST = "MenuList";
        var ID_MENU_LIST_WRAPPER = "MenuListWrapper";
        var ID_MENU = "Menu";
        var ID_MENU_ITEM = "MenuItem";
        var ID_MENU_SCROLLER_WRAPPER = "ScrollerWrapper";
        var ID_MENU_SCROLLER_LEFT = "MenuLeftScroller";
        var ID_MENU_SCROLLER_RIGHT = "MenuRightScroller";
        var ID_SUBMENU_WRAPPER = "SubmenuWrapper";
        var ID_SUBMENU_LIST = "SubmenuList";
        var ID_SUBMENU_LIST_WRAPPER = "SubmenuListWrapper";
        var ID_SUBMENU = "Submenu";
        var ID_SUBMENU_ITEM = "SubmenuItem";
        var ID_SUBMENU_SCROLLER_WRAPPER = "SubMenuScrollerWrapper";
        var ID_SUBMENU_SCROLLER_LEFT = "SubMenuLeftScroller";
        var ID_SUBMENU_SCROLLER_RIGHT = "SubMenuRightScroller";
        var ID_SUBSUBMENU_WRAPPER = "SubsubmenuWrapper";
        var ID_SUBSUBMENU_LIST = "SubsubmenuList";
        var ID_SUBSUBMENU_LIST_WRAPPER = "SubsubmenuListWrapper";
        var ID_SUBSUBMENU = "Subsubmenu";
        var ID_SUBSUBMENU_ITEM = "SubsubmenuItem";

        function menuClicked(menu, menuItem) {
            $menu.children("div#" + ID_SUBMENU_WRAPPER).hide();
            $menu.children("a#" + ID_MENU_ITEM).filter(".jqTabbedMenu-Menu-Selected").removeClass("jqTabbedMenu-Menu-Selected");
            menuItem.children("a").addClass("jqTabbedMenu-Menu-Selected");
            menuItem.children("div#" + ID_SUBMENU_WRAPPER).show();
        }

        function submenuClicked(menu, submenu) {
            $submenuItems = menu.find("a#" + ID_SUBMENU_ITEM);
            $lstSubsubmenu = submenu.find(".jqTabbedMenu-Subsubmenu");

            $submenuItems.each(function () {
                $(this).removeClass("jqTabbedMenu-Submenu-Focused");
                if (subsubmenuTriggered) {
                    $(this).filter(":not(.subsubmenuTriggered)").removeClass("jqTabbedMenu-Submenu-Selected");
                } else {
                    $(this).removeClass("subsubmenuTriggered");
                    if ($lstSubsubmenu.length == 0) {
                        $(this).removeClass("jqTabbedMenu-Submenu-Selected");
                    }
                }
            });

            if ($lstSubsubmenu.length == 0) {
                submenu.find(".jqTabbedMenu-Submenu-Item").addClass("jqTabbedMenu-Submenu-Selected");
            }

            subsubmenuTriggered = false;
        }

        function subsubmenuClicked(menu, subsubmenu) {
            $parent = subsubmenu.parent().parent().parent();
            $submenuItem = $parent.children(".jqTabbedMenu-Submenu-Item");
            $submenuItem.removeClass("jqTabbedMenu-Submenu-Focused-HasSubsubmenu");
            $submenuItem.removeClass("jqTabbedMenu-Submenu-Selected-Focused");
            $submenuItem.removeClass("jqTabbedMenu-Submenu-Focused");
            $submenuItem.addClass("jqTabbedMenu-Submenu-Selected");

            menu.find("a#" + ID_SUBMENU_ITEM).removeClass("subsubmenuTriggered");
            $submenuItem.addClass("subsubmenuTriggered");
            subsubmenuTriggered = true;

            $subsubmenuWrapper = subsubmenu.closest(".jqTabbedMenu-Subsubmenu-List-Wrapper");
            $subsubmenuWrapper.stop(true, true).slideUp('fast');
        }

        return this.each(function () {
            /* Ids */
            var id = $(this).attr("id");
            ID_MENU_WRAPPER = id.concat("-" + ID_MENU_WRAPPER);
            ID_MENU_LIST = id.concat("-" + ID_MENU_LIST);
            ID_MENU_LIST_WRAPPER = id.concat("-" + ID_MENU_LIST_WRAPPER);
            ID_MENU = id.concat("-" + ID_MENU);
            ID_MENU_ITEM = id.concat("-" + ID_MENU_ITEM);
            ID_MENU_SCROLLER_WRAPPER = id.concat("-" + ID_MENU_SCROLLER_WRAPPER);
            ID_MENU_SCROLLER_LEFT = id.concat("-" + ID_MENU_SCROLLER_LEFT);
            ID_MENU_SCROLLER_RIGHT = id.concat("-" + ID_MENU_SCROLLER_RIGHT);
            ID_SUBMENU_WRAPPER = id.concat("-" + ID_SUBMENU_WRAPPER);
            ID_SUBMENU_LIST = id.concat("-" + ID_SUBMENU_LIST);
            ID_SUBMENU_LIST_WRAPPER = id.concat("-" + ID_SUBMENU_LIST_WRAPPER);
            ID_SUBMENU = id.concat("-" + ID_SUBMENU);
            ID_SUBMENU_ITEM = id.concat("-" + ID_SUBMENU_ITEM);
            ID_SUBMENU_SCROLLER_WRAPPER = id.concat("-" + ID_SUBMENU_SCROLLER_WRAPPER);
            ID_SUBMENU_SCROLLER_LEFT = id.concat("-" + ID_SUBMENU_SCROLLER_LEFT);
            ID_SUBMENU_SCROLLER_RIGHT = id.concat("-" + ID_SUBMENU_SCROLLER_RIGHT);
            ID_SUBSUBMENU_WRAPPER = id.concat("-" + ID_SUBSUBMENU_WRAPPER);
            ID_SUBSUBMENU_LIST = id.concat("-" + ID_SUBSUBMENU_LIST);
            ID_SUBSUBMENU_LIST_WRAPPER = id.concat("-" + ID_SUBSUBMENU_LIST_WRAPPER);
            ID_SUBSUBMENU = id.concat("-" + ID_SUBSUBMENU);
            ID_SUBSUBMENU_ITEM = id.concat("-" + ID_SUBSUBMENU_ITEM);

            var menuScrollingStep = 200;
            var menuScrollingDuration = 500;

            /* Structuring */
            $(this).addClass("jqTabbedMenu-List");
            $(this).wrapAll("<div id='" + ID_MENU_WRAPPER + "' class='jqTabbedMenu-Wrapper'/>");
            $wrapper = $("#" + ID_MENU_WRAPPER);
            $menuList = $wrapper.children("ul").filter(":first");
            $menuList.wrapAll("<div id='" + ID_MENU_LIST_WRAPPER + "' class='jqTabbedMenu-List-Wrapper'/>");
            $menuListWrapper = $menuList.parent();
            $wrapper.append(
				"<div id='" + ID_MENU_SCROLLER_WRAPPER + "' class='jqTabbedMenu-Scroller-Wrapper'>" +
					"<span id='" + ID_MENU_SCROLLER_LEFT + "' class='jqTabbedMenu-Scroller-Left'>&laquo;</span>" +
					"<span id='" + ID_MENU_SCROLLER_RIGHT + "' class='jqTabbedMenu-Scroller-Right'>&raquo;</span>" +
				"</div>"
			);
            $scrollerWrapper = $wrapper.children("div#" + ID_MENU_SCROLLER_WRAPPER);
            $leftScroller = $scrollerWrapper.children("span#" + ID_MENU_SCROLLER_LEFT);
            $rightScroller = $scrollerWrapper.children("span#" + ID_MENU_SCROLLER_RIGHT);
            $leftScroller.click(function () {
                $menuListWrapper.animate({
                    scrollLeft: "-=" + menuScrollingStep + "px"
                }, {
                    "duration": menuScrollingDuration
                });
            });
            $rightScroller.click(function () {
                $menuListWrapper.animate({
                    scrollLeft: "+=" + menuScrollingStep + "px"
                }, {
                    "duration": menuScrollingDuration
                });
            });


            $menu = $menuList.children("li");
            $menu.each(function () {
                $(this).addClass($(this).attr("id").toLowerCase());
                $(this).addClass("jqTabbedMenu-Menu");
                $(this).attr("id", ID_MENU);
                $menuItem = $(this).children("a").filter(":first");
                $menuItem.attr("id", ID_MENU_ITEM);
                $menuItem.addClass("jqTabbedMenu-Menu-Item");

                $submenuList = $(this).children("ul");
                if (($submenuList != null) && (typeof ($submenuList) != 'undefined') && ($submenuList.length > 0)) {
                    $(this).children("a").append("<span id='" + id + "' class='jqTabbedMenu-Submenu-Marker'>&nabla;</span>");

                    $submenuList.wrapAll("<div id='" + ID_SUBMENU_LIST_WRAPPER + "'/>");
                    $submenuListWrapper = $(this).children("div").filter(":first");
                    $submenuListWrapper.addClass("jqTabbedMenu-Submenu-List-Wrapper");
                    $submenuListWrapper.wrapAll("<div id='" + ID_SUBMENU_WRAPPER + "'/>");
                    $submenuWrapper = $submenuListWrapper.parent();
                    $submenuWrapper.addClass("jqTabbedMenu-Submenu-Wrapper");
                    $submenuWrapper.append(
						"<div id='" + ID_SUBMENU_SCROLLER_WRAPPER + "' class='jqTabbedMenu-Submenu-Scroller-Wrapper'>" +
							"<span id='" + ID_SUBMENU_SCROLLER_LEFT + "' class='jqTabbedMenu-Submenu-Scroller-Left'>&laquo;</span>" +
							"<span id='" + ID_SUBMENU_SCROLLER_RIGHT + "' class='jqTabbedMenu-Submenu-Scroller-Right'>&raquo;</span>" +
						"</div>"
					);

                    $submenuScrollerWrapper = $submenuWrapper.children("div#" + ID_SUBMENU_SCROLLER_WRAPPER);
                    $submenuLeftScroller = $submenuScrollerWrapper.children("span#" + ID_SUBMENU_SCROLLER_LEFT);
                    $submenuRightScroller = $submenuScrollerWrapper.children("span#" + ID_SUBMENU_SCROLLER_RIGHT);
                    $submenuLeftScroller.click(function () {
                        $selectedMenu = $menuList.find("a.jqTabbedMenu-Menu-Selected").filter(":first");
                        $scrollingSubmenu = $selectedMenu.next().find("div#" + ID_SUBMENU_LIST_WRAPPER).filter(":first");
                        $scrollingSubmenu.animate({
                            scrollLeft: "-=" + menuScrollingStep + "px"
                        }, {
                            "duration": menuScrollingDuration
                        });
                    });
                    $submenuRightScroller.click(function () {
                        $selectedMenu = $menuList.find("a.jqTabbedMenu-Menu-Selected").filter(":first");
                        $scrollingSubmenu = $selectedMenu.next().find("div#" + ID_SUBMENU_LIST_WRAPPER).filter(":first");
                        $scrollingSubmenu.animate({
                            scrollLeft: "+=" + menuScrollingStep + "px"
                        }, {
                            "duration": menuScrollingDuration
                        });
                    });

                    $submenuList = $submenuListWrapper.children("ul").filter(":first");
                    $submenuList.attr("id", ID_SUBMENU_LIST);
                    $submenuList.addClass("jqTabbedMenu-Submenu-List");
                    if (wideScreen) {
                        $submenuList.addClass("jqTabbedMenu-Submenu-List-WideScreen");
                    }

                    $submenu = $submenuList.children("li");
                    $submenu.each(function () {
                        $(this).addClass($(this).attr("id").toLowerCase());
                        $(this).addClass("jqTabbedMenu-Submenu");
                        $(this).attr("id", ID_SUBMENU);
                        $submenuItem = $(this).children("a");
                        $submenuItem.addClass("jqTabbedMenu-Submenu-Item");
                        $submenuItem.each(function () {
                            $(this).attr("id", ID_SUBMENU_ITEM);
                        });

                        $(this).hover(
							function () {
							    subsubmenuTriggered = false;
							    $submenuItem = $menu.find("a#" + ID_SUBMENU_ITEM);
							    $lstSubsubmenu = $(this).find(".jqTabbedMenu-Subsubmenu");
							    if ($lstSubsubmenu.length > 0) {
							        $submenuItem.filter(".jqTabbedMenu-Submenu-Focused").removeClass("jqTabbedMenu-Submenu-Focused");
							        $submenuItem.filter(".jqTabbedMenu-Submenu-Focused-HasSubsubmenu").removeClass("jqTabbedMenu-Submenu-Focused-HasSubsubmenu");
							        $submenuItem.filter(".jqTabbedMenu-Submenu-Selected-Focused").removeClass("jqTabbedMenu-Submenu-Selected-Focused");

							        $submenuItems = $(this).find(".jqTabbedMenu-Submenu-Item");
							        $submenuItems.addClass("jqTabbedMenu-Submenu-Focused-HasSubsubmenu");
							        $selectedHasChildren = $(this).hasClass(".jqTabbedMenu-Submenu-Selected");
							        if ($selectedHasChildren != undefined) {
							            $submenuItems.addClass("jqTabbedMenu-Submenu-Selected-Focused");
							        }

							        $subsubmenuListWrapper = $(this).find(".jqTabbedMenu-Subsubmenu-List-Wrapper");
							        $subsubmenuListWrapper.stop(true, true).slideDown('fast');
							    } else {
							        $submenuItem.filter(".jqTabbedMenu-Submenu-Focused").removeClass("jqTabbedMenu-Submenu-Focused");
							        $(this).find(".jqTabbedMenu-Submenu-Item").addClass("jqTabbedMenu-Submenu-Focused");
							    }
							},
							function () {
							    $menu.find("a#" + ID_SUBMENU_ITEM).filter(".jqTabbedMenu-Submenu-Focused").removeClass("jqTabbedMenu-Submenu-Focused");
							    $submenuItem.filter(".jqTabbedMenu-Submenu-Focused-HasSubsubmenu").removeClass("jqTabbedMenu-Submenu-Focused-HasSubsubmenu");
							    $submenuItem.filter(".jqTabbedMenu-Submenu-Selected-Focused").removeClass("jqTabbedMenu-Submenu-Selected-Focused");
							    $(this).find(".jqTabbedMenu-Subsubmenu-List-Wrapper").stop(true, true).slideUp('fast');
							}
						);

                        $(this).click(function () {
                            submenuClicked($menu, $(this));
                        });

                        $subsubmenuList = $(this).children("ul");
                        if (($subsubmenuList != null) && ($subsubmenuList != 'undefined') && ($subsubmenuList.length > 0)) {
                            $subsubmenuList.addClass("jqTabbedMenu-Subsubmenu-List");
                            $submenuParent = $subsubmenuList.parents("ul");
                            if (wideScreen) {
                                $submenuParent.removeClass("jqTabbedMenu-Submenu-List-WideScreen");
                                $submenuParent.addClass("jqTabbedMenu-Submenu-List-Widescreen-hasSubmenu");
                            }

                            $subsubmenuList.attr("id", ID_SUBSUBMENU_LIST);
                            $subsubmenuList.wrapAll("<div id='" + ID_SUBSUBMENU_LIST_WRAPPER + "' class='jqTabbedMenu-Subsubmenu-List-Wrapper'/>");
                            $subsubmenuListWrapper = $subsubmenuList.parent();
                            /* Ugly hack for IE. This browser needs to be burnt down ! */
                            if ($.browser.msie) {
                                $submenuPosition = $(this).offset();
                                $subsubmenuListWrapper.css("position", "fixed");
                                $subsubmenuListWrapper.css("left", ($submenuPosition.left) + "px");
                                var submenuHeight = parseInt($(this).css("height"), 10);
                                $subsubmenuListWrapper.css("top", ($submenuPosition.top + submenuHeight) + "px");
                            }
                            /* -------------------------------------------------------- */

                            $subsubmenuListWrapper.wrapAll("<div id='" + ID_SUBSUBMENU_WRAPPER + "' class='jqTabbedMenu-Subsubmenu-Wrapper'/>");
                            $subsubmenuWrapper = $subsubmenuListWrapper.parent();
                            $subsubmenu = $subsubmenuList.children("li");
                            $subsubmenu.each(function () {
                                $(this).addClass($(this).attr("id").toLowerCase());
                                $(this).addClass("jqTabbedMenu-Subsubmenu");
                                $(this).attr("id", ID_SUBSUBMENU);
                                $subsubmenuItem = $(this).children("a");
                                $subsubmenuItem.attr("id", ID_SUBSUBMENU_ITEM);
                                $subsubmenuItem.addClass("jqTabbedMenu-Subsubmenu-Item");

                                $(this).click(function () {
                                    subsubmenuClicked($menu, $(this));
                                });
                            });
                            $submenuItem.append("<span class='jqTabbedMenu-Subsubmenu-Marker'>&raquo;</span>");
                            $subsubmenuWrapper.prepend($submenuItem);
                            $subsubmenuListWrapper.hide();
                        }
                    });
                } else {
                    var paddingTop = 0;
                    var paddingBottom = 0;
                    $submenuSample = $menu.has("ul").find("#" + ID_SUBMENU_LIST).filter(":first");
                    if (($submenuSample != null) && (typeof ($submenuSample) != 'undefined')) {
                        paddingTop = $submenuSample.css("padding-top");
                        paddingLeft = $submenuSample.css("padding-bottom");
                    }
                    $(this).append(
                        "<div class='jqTabbedMenu-Submenu-Wrapper' id='" + ID_SUBMENU_WRAPPER + "'>" +
                        "   <div id='" + ID_SUBMENU_LIST_WRAPPER + "' class='jqTabbedMenu-Submenu-List-Wrapper'>" +
                        "       <ul id='" + ID_SUBMENU_LIST + "' class='jqTabbedMenu-Submenu-List'>" +
                        "           <li class='jqTabbedMenu-Submenu'>&nbsp;&nbsp;</li>" +
                        "       </ul>" +
                        "   </div>" +
                        "</div>"
                    );
                    $menuItem = $(this).children("a");
                }
                $submenuList = $(this).children("div#" + ID_SUBMENU_WRAPPER);
                $submenuList.hide();

                $(this).click(function () {
                    menuClicked($menu, $(this));
                });

                $(this).hover(
					function () {
					    $menu.each(function () {
					        $(this).find("a#" + ID_MENU_ITEM).filter("a.jqTabbedMenu-Menu-Focused").removeClass("jqTabbedMenu-Menu-Focused");
					    });
					    $(this).children("a").addClass("jqTabbedMenu-Menu-Focused");
					},
					function () {
					    $menu.each(function () {
					        $(this).find("a#" + ID_MENU_ITEM).filter("a.jqTabbedMenu-Menu-Focused").removeClass("jqTabbedMenu-Menu-Focused");
					    });
					}
				);
            });

            /* Give all empty href initial value */
            var linkUrl;
            $(this).find("a").each(function () {
                linkUrl = $(this).attr("href");
                if ((linkUrl == null) || (linkUrl == '') || (typeof (linkUrl) === 'undefined')) {
                    $(this).attr("href", "#");
                }
            });

            /* Activate first menu */
            $openedMenu = $(this).find("li." + settings.openedMenuId.toLowerCase());
            if ($openedMenu.hasClass("jqTabbedMenu-Submenu")) {
                submenuClicked($menu, $openedMenu);
                var parent = $openedMenu.parents(".jqTabbedMenu-Menu");
                menuClicked($menu, parent);
            } else if ($openedMenu.hasClass("jqTabbedMenu-Subsubmenu")) {
                subsubmenuClicked($menu, $openedMenu);
                var parent = $openedMenu.parents(".jqTabbedMenu-Menu");
                menuClicked($menu, parent);
            }
        });
    }
})(jQuery);
