﻿/* 
    Document   : jstifall (common-notification-wall)
    Created on : Feb 28, 2013, 1:40:26 PM
    Author     : Lufty Abdillah
    Description: jQuery plugin for notification wall
*/

(function ($) {
    $.fn.jstifall = function (options) {
        var settings = $.extend({}, options);

        function reloadNotifications(list) {
            $.ajax({
                url: _cmfunc_getActionUrl("_ajax_Notification_Load"),
                dataType: "json",
                success: function (data) {
                    list.empty();
                    $(data).each(function (index, value) {
                        list.append(
                            "<li class=\"jstifall-list-item\">" +
                            "   <div class=\"jstifall-notification-item\">" +
                            "       <div class=\"title\">" +
                            "           <a href=\"" + value.ActionUrl + "\" target=\"_blank\">" + value.Title + "</a>" +
                            "       </div>" +
                            "       <div class=\"author\">" + value.Author + " @ </div>" +
                            "       <div class=\"date\">" + value.PostDateInString + "</div>" +
                            "       <div class=\"content\">" + value.Content + "</div>" +
                            "   </div>" +
                            "</li>"
                        );
                    });
                }
            });
        }

        return this.each(function () {
            var element = $(this);
            element.addClass("jstifall");
            var scrollStep = element.outerHeight() * 0.3;

            //element.append("<div class='jstifall-see-all-link'><a href='http://www.google.com'><span>&laquo;</span> see all <span>&raquo;</span></a></div>");
            var container = element.children("div:first");
            container.addClass("jstifall-container");
            container.css("height", element.innerHeight() - parseInt(element.css("padding-top"), 10) - parseInt(element.css("padding-bottom"), 10));
            var list = container.children("ul:first");
            list.addClass("jstifall-list");
            var listItems = list.children("li");
            listItems.each(function () {
                var item = $(this);
                item.addClass("jstifall-list-item");
                var notificationItem = item.children("div");
                notificationItem.addClass("jstifall-notification-item");
            });

            var authors = listItems.find(".author");
            authors.each(function () {
                var author = $(this);
                author.text(author.text() + " @ ");
            });

            /* Scroller */
            element.append("<div class='jstifall-up-scroller'>Up</div>");
            element.append("<div class='jstifall-down-scroller'>Down</div>");
            var upScroller = element.find(".jstifall-up-scroller");
            upScroller.click(function (e) {
                e.stopPropagation();
                container.animate({
                    scrollTop: "-=" + scrollStep + "px"
                }, "fast", "swing", function () { });
            });
            var downScroller = element.find(".jstifall-down-scroller");
            downScroller.click(function (e) {
                e.stopPropagation();
                container.animate({
                    scrollTop: "+=" + scrollStep + "px"
                }, "fast", "swing", function () { });
            });

            container.mousemove(function (e) {
                var position = element.offset();
                var height = element.outerHeight();

                if ((e.pageY > position.top) && (e.pageY < (position.top + 15))) {
                    upScroller.fadeIn("fast", "swing", function () { });
                } else {
                    upScroller.fadeOut("fast", "swing", function () { });
                }

                if ((e.pageY < (position.top + height)) && (e.pageY > ((position.top + height) - 15))) {
                    downScroller.fadeIn("fast", "swing", function () { });
                } else {
                    downScroller.fadeOut("fast", "swing", function () { });
                }
            });

            upScroller.click();

            _cmfunc_addScheduledTask(element.attr("id"), function () {
                var list = element.find(".jstifall-list");
                reloadNotifications(list);
            });

            reloadNotifications(list);
        });
    }
})(jQuery)