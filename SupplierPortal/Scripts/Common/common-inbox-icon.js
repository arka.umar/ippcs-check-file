/*
 * Common Inbox Icon
 * Author: Lufty Abdillah
 * 
 * A Simple jQuery plugin to add some decoration to the icon related to message inbox.
 */

(function ($) {
    $.fn.commonInboxIcon = function (options) {
        var settings = $.extend({
            boardName: "",
            size: 24,
            iconBaseUrl: "images",
            iconBaseName: "inbox",
            iconExtension: "png",
            unreadMessageCounter: function () { return 0; },
            countUnreadMessageOnServer: false,
            unreadMessageCounterUrl: "",
            onClicked: function () { },
            autoUpdateState: false,
            stateUpdateInterval: 29000
        }, options);

        function updateNewMessageCounter(parent) {
            var number = parent.children("span.common-inbox-icon-number");
            var newMessageCount = settings.unreadMessageCounter();
            if (settings.countUnreadMessageOnServer) {
                $.ajax({
                    url: settings.unreadMessageCounterUrl,
                    dataType: "text",
                    data: {
                        boardName: settings.boardName
                    },
                    success: function (data) {
                        newMessageCount = parseInt(data);
                        number.text(newMessageCount);
                        if (newMessageCount > 0) {
                            switch (settings.size) {
                                case 24:
                                    number.css("font-weight", "bold");
                                    number.css("font-size", "0.6em");
                                    number.css("padding", "1px 3px");
                                    if (newMessageCount < 10) {
                                        number.css("right", -5);
                                    } else if (newMessageCount < 100) {
                                        number.css("right", -9);
                                    } else {
                                        number.css("right", -12);
                                    }
                                    break;
                                case 32:
                                    number.css("font-weight", "bold");
                                    number.css("font-size", "0.8em");
                                    number.css("padding", "2px 5px");
                                    if (newMessageCount < 10) {
                                        number.css("right", -7);
                                    } else if (newMessageCount < 100) {
                                        number.css("right", -11);
                                    } else {
                                        number.css("right", -17);
                                    }
                                    break;
                                case 64:
                                    number.css("font-size", "1.1em");
                                    number.css("padding", "5px 10px");
                                    if (newMessageCount < 10) {
                                        number.css("right", -12);
                                    } else if (newMessageCount < 100) {
                                        number.css("right", -16);
                                    } else {
                                        number.css("right", -20);
                                    }
                                    break;
                                case 72:
                                    number.css("font-size", "1.2em");
                                    number.css("padding", "5px 10px");
                                    if (newMessageCount < 10) {
                                        number.css("right", -10);
                                    } else if (newMessageCount < 100) {
                                        number.css("right", -18);
                                    } else {
                                        number.css("right", -23);
                                    }
                                    break;
                                case 96:
                                    number.css("font-size", "1.5em");
                                    number.css("padding", "8px 13px");
                                    if (newMessageCount < 10) {
                                        number.css("right", -10);
                                    } else if (newMessageCount < 100) {
                                        number.css("right", -20);
                                    } else {
                                        number.css("right", -25);
                                    }
                                    break;
                                case 128:
                                    number.css("font-size", "2em");
                                    number.css("padding", "10px 15px");
                                    if (newMessageCount < 10) {
                                        number.css("right", -10);
                                    } else if (newMessageCount < 100) {
                                        number.css("right", -23);
                                    } else {
                                        number.css("right", -28);
                                    }
                                    break;
                                default: /* 16 */
                                    break;
                            }

                            number.show();
                        } else {
                            number.hide();
                        }
                    }
                });
            }
        }

        return this.each(function () {
            var element = $(this);
            if (settings.size < 16) {
                settings.size = 16;
            } else if (settings.size > 128) {
                settings.size = 128;
            }

            var iconUrl = settings.iconBaseUrl + "/" + settings.iconBaseName + "-" + settings.size + "." + settings.iconExtension;
            var hoverIconUrl = settings.iconBaseUrl + "/" + settings.iconBaseName + "-" + settings.size + "-bright." + settings.iconExtension;

            element.addClass("common-inbox-icon-container");
            element.addClass("common-inbox-icon-container-" + settings.size.toString());
            element.css("width", settings.size);
            element.css("height", settings.size);
            
            var image = element.children("img");
            image.addClass("common-inbox-icon-image");
            image.attr("alt", "");
            image.attr("src", iconUrl);
            image.mouseover(function () {
                $(this).attr("src", hoverIconUrl);
            });
            image.mouseout(function () {
                $(this).attr("src", iconUrl);
            });
            image.click(function () {
                settings.onClicked();
            });

            var number = element.children("span");
            number.addClass("common-inbox-icon-number");
            number.addClass("common-inbox-icon-number-" + settings.size.toString());
            
            if (settings.size > 16) {
                number.hide();
            }

            if (settings.autoUpdateState) {
                window.setInterval(function () {
                    updateNewMessageCounter(element);
                }, settings.stateUpdateInterval);
            } else {
                updateNewMessageCounter(element);
            }
        });
    }
})(jQuery)