﻿var _cmvar_ApplicationUrl = "appUrl";
var _cmvar_ScreenId = "screenId";

function _cmfunc_getScreenUrl() {
    return _cmvar_ApplicationUrl + "/" + _cmvar_ScreenId;
}

function _cmfunc_getActionUrl(action) {
    var prot = window.location.protocol;
    var su = _cmfunc_getScreenUrl();
    if (su.indexOf(prot) !== 1) {
        var pi = su.indexOf("://") +1;
        su = prot + su.substr(pi, su.length - pi + 1);
    }
    return su + "/" + action; 
    // return _cmfunc_getScreenUrl() + "/" + action;
}