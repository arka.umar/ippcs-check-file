﻿/* 
Document    : jslane
Created on  : Dec 26, 2012, 6:52:27 AM
Author      : Lufty Abdillah
Description : Sliding panes with fixed position
*/

(function ($) {
    $.jslane = {}

    $.jslane.open = function (orientation) {
        $("body").find(".jslane-floating-slider." + orientation).hide();
        var pane = $("body").find(".jslane-pane." + orientation).first();
        pane.show();
        if (orientation == "left") {
            pane.animate({ left: 0 }, 100, "swing", function () { });
        } else if (orientation == "right") {
            pane.animate({ right: 0 }, 100, "swing", function () { });
        } else if (orientation == "top") {
            pane.animate({ top: 0 }, 100, "swing", function () { });
        } else if (orientation == "bottom") {
            pane.animate({ bottom: 0 }, 100, "swing", function () { });
        }
    }

    $.jslane.close = function (orientation) {
        var pane = $("body").find(".jslane-pane." + orientation).first();
        var sliderCallback = function () {
            pane.hide();
            $("body").find(".jslane-floating-slider." + orientation).show();
        };

        if (orientation == "left") {
            pane.animate({ left: -parseInt(pane.outerWidth(), 10) }, 100, "swing", sliderCallback);
        } else if (orientation == "right") {
            pane.animate({ right: -parseInt(pane.outerWidth(), 10) }, 100, "swing", sliderCallback);
        } else if (orientation == "top") {
            pane.animate({ top: -parseInt(pane.outerHeight(), 10) }, 100, "swing", sliderCallback);
        } else if (orientation == "bottom") {
            pane.animate({ bottom: -parseInt(pane.outerHeight(), 10) }, 100, "swing", sliderCallback);
        }
    }

    $.jslane.isOpened = function (orientation) {
        var pane = $("body").find(".jslane-pane." + orientation);
        var offset = parseInt(pane.css(orientation), 10);
        return offset > 0;
    }

    $.jslane.toggleOpen = function (orientation) {
        if (!$.jslane.isOpened(orientation)) {
            $.jslane.open(orientation);
        } else {
            $.jslane.close(orientation);
        }
    }

    var mouseInsideLeftPane = false;
    var mouseInsideRightPane = false;
    var mouseInsideTopPane = false;
    var mouseInsideBottomPane = false;

    $.jslane.setMouseHoverState = function (orientation, hovering) {
        if (orientation == "left") { mouseInsideLeftPane = hovering; } else
            if (orientation == "right") { mouseInsideRightPane = hovering; } else
                if (orientation == "top") { mouseInsideTopPane = hovering; } else
                    if (orientation == "bottom") { mouseInsideBottomPane = hovering; }
    }

    $.jslane.hasMouseHovering = function () {
        return mouseInsideBottomPane || mouseInsideLeftPane || mouseInsideRightPane || mouseInsideTopPane;
    }

    $.fn.jslane = function (options) {
        var settings = $.extend({
            /* Orientation: left/right/top/bottom */
            orientation: "left",
            title: "Sliding Panel",
            width: 0,
            height: 0,
            sliderSize: 40
        }, options);

        var windowWidth = $(window).outerWidth();
        var windowHeight = $(window).outerHeight();

        if ((settings.orientation == "right") || (settings.orientation == "left")) {
            settings.width = options.width != undefined ? settings.width : 300;
            settings.height = options.height != undefined ? settings.height : (0.8 * windowHeight);
        } else {
            settings.width = options.width != undefined ? settings.width : (0.9 * windowWidth);
            settings.height = options.height != undefined ? settings.height : 200;
        }

        return this.each(function () {
            var pane = $(this);
            var id = pane.attr("id");
            var parent = $(this).parent();
            var content = pane.children("div");
            content.attr("id", "content");
            content.addClass("jslane-content");

            var borderRadius = 10;
            var inactiveFloatingOffset = settings.sliderSize - (settings.sliderSize / 4);

            pane.hide();
            pane.addClass("jslane-pane " + settings.orientation);
            pane.css("position", "fixed");
            pane.css("width", settings.width);
            pane.css("height", settings.height);
            pane.css("border-" + settings.orientation, "none");
            pane.prepend("<div id='title' class='jslane-title'/>");
            pane.prepend("<div id='jslane-slider' class='jslane-slider " + settings.orientation + "'/>");
            if ((settings.orientation == "left") || (settings.orientation == "right")) {
                pane.css(settings.orientation, -pane.outerWidth());
                var top = pane.css("top");
                if (top == "auto") {
                    pane.css("top", (windowHeight - settings.height) / 2);
                }
            } else {
                pane.css(settings.orientation, -pane.outerHeight());
                var left = pane.css("left");
                if (left == "auto") {
                    pane.css("left", (windowWidth - settings.width) / 2);
                }
            }

            var title = pane.children("#title");
            title.addClass(settings.orientation);
            title.append("<span id='text' class=\"text\">" + settings.title + "</span>");

            parent.append("<div id='jslane-floating-slider-" + id + "' class='jslane-floating-slider " + settings.orientation + "'/>");
            var floatingSlider = parent.children("#jslane-floating-slider-" + id);
            floatingSlider.css("position", "fixed");
            floatingSlider.css("cursor", "pointer");
            floatingSlider.append("<div id='icon' class='icon'/>");
            if ((settings.orientation == "left") || (settings.orientation == "right")) {
                floatingSlider.css("top", (windowHeight / 2) - (floatingSlider.outerHeight() / 2));
                floatingSlider.css(settings.orientation, 0);
                floatingSlider.css("width", settings.sliderSize);
                floatingSlider.css("height", settings.sliderSize * 2);
                floatingSlider.css(settings.orientation, -inactiveFloatingOffset);
                if (settings.orientation == "left") {
                    floatingSlider.css("border-top-right-radius", borderRadius);
                    floatingSlider.css("border-bottom-right-radius", borderRadius);
                    floatingSlider.css("-moz-border-radius-topright", borderRadius);
                    floatingSlider.css("-moz-border-radius-bottomright", borderRadius);
                    floatingSlider.css("-webkit-border-top-right-radius", borderRadius);
                    floatingSlider.css("-webkit-border-bottom-right-radius", borderRadius);
                } else {
                    floatingSlider.css("border-top-left-radius", borderRadius);
                    floatingSlider.css("border-bottom-left-radius", borderRadius);
                    floatingSlider.css("-moz-border-radius-topleft", borderRadius);
                    floatingSlider.css("-moz-border-radius-bottomleft", borderRadius);
                    floatingSlider.css("-webkit-border-top-left-radius", borderRadius);
                    floatingSlider.css("-webkit-border-bottom-left-radius", borderRadius);
                }
            } else {
                floatingSlider.css(settings.orientation, 0);
                floatingSlider.css("width", settings.sliderSize * 2);
                floatingSlider.css("height", settings.sliderSize);
                floatingSlider.css("left", (windowWidth / 2) - (floatingSlider.outerWidth() / 2));
                floatingSlider.css(settings.orientation, -inactiveFloatingOffset);
                if (settings.orientation == "bottom") {
                    floatingSlider.css("border-top-right-radius", borderRadius);
                    floatingSlider.css("border-top-left-radius", borderRadius);
                    floatingSlider.css("-moz-border-radius-topright", borderRadius);
                    floatingSlider.css("-moz-border-radius-topleft", borderRadius);
                    floatingSlider.css("-webkit-border-top-right-radius", borderRadius);
                    floatingSlider.css("-webkit-border-top-left-radius", borderRadius);
                } else {
                    floatingSlider.css("border-bottom-right-radius", borderRadius);
                    floatingSlider.css("border-bottom-left-radius", borderRadius);
                    floatingSlider.css("-moz-border-radius-bottomright", borderRadius);
                    floatingSlider.css("-moz-border-radius-bottomleft", borderRadius);
                    floatingSlider.css("-webkit-border-bottom-right-radius", borderRadius);
                    floatingSlider.css("-webkit-border-bottom-left-radius", borderRadius);
                }
            }

            var slider = pane.children("#jslane-slider");
            slider.css("position", "absolute");
            slider.css("cursor", "pointer");
            slider.append("<div id='icon' class='icon'/>");
            if ((settings.orientation == "left") || (settings.orientation == "right")) {
                slider.css("top", ((pane.outerHeight() / 2) - (slider.outerHeight() / 2)));
                slider.css("width", settings.sliderSize);
                slider.css("height", settings.sliderSize * 2);
                if (settings.orientation == "left") {
                    slider.css("right", -slider.outerWidth());
                    slider.css("border-top-right-radius", borderRadius);
                    slider.css("border-bottom-right-radius", borderRadius);
                    slider.css("-moz-border-radius-topright", borderRadius);
                    slider.css("-moz-border-radius-bottomright", borderRadius);
                    slider.css("-webkit-border-top-right-radius", borderRadius);
                    slider.css("-webkit-border-bottom-right-radius", borderRadius);
                } else {
                    slider.css("left", -slider.outerWidth());
                    slider.css("border-top-left-radius", borderRadius);
                    slider.css("border-bottom-left-radius", borderRadius);
                    slider.css("-moz-border-radius-topleft", borderRadius);
                    slider.css("-moz-border-radius-bottomleft", borderRadius);
                    slider.css("-webkit-border-top-left-radius", borderRadius);
                    slider.css("-webkit-border-bottom-left-radius", borderRadius);
                }
            } else {
                slider.css("width", settings.sliderSize * 2);
                slider.css("height", settings.sliderSize);
                slider.css("left", (pane.outerWidth() / 2) - (slider.outerWidth() / 2));
                if (settings.orientation == "bottom") {
                    slider.css("top", -slider.outerHeight());
                    slider.css("border-top-right-radius", borderRadius);
                    slider.css("border-top-left-radius", borderRadius);
                    slider.css("-moz-border-radius-topright", borderRadius);
                    slider.css("-moz-border-radius-topleft", borderRadius);
                    slider.css("-webkit-border-top-right-radius", borderRadius);
                    slider.css("-webkit-border-top-left-radius", borderRadius);
                } else {
                    slider.css("bottom", -slider.outerHeight());
                    slider.css("border-bottom-right-radius", borderRadius);
                    slider.css("border-bottom-left-radius", borderRadius);
                    slider.css("-moz-border-radius-bottomright", borderRadius);
                    slider.css("-moz-border-radius-bottomleft", borderRadius);
                    slider.css("-webkit-border-bottom-right-radius", borderRadius);
                    slider.css("-webkit-border-bottom-left-radius", borderRadius);
                }
            }

            floatingSlider.click(function (event) {
                event.stopPropagation();
                $.jslane.open(settings.orientation);
            });
            floatingSlider.hover(function (event) {
                event.stopPropagation();
                $.jslane.setMouseHoverState(settings.orientation, true);
                if (settings.orientation == "left") {
                    $(this).animate({ left: 0 }, 100, "swing", function () { });
                } else if (settings.orientation == "right") {
                    $(this).animate({ right: 0 }, 100, "swing", function () { });
                } else if (settings.orientation == "top") {
                    $(this).animate({ top: 0 }, 100, "swing", function () { });
                } else if (settings.orientation == "bottom") {
                    $(this).animate({ bottom: 0 }, 100, "swing", function () { });
                }
            }, function (event) {
                event.stopPropagation();
                $.jslane.setMouseHoverState(settings.orientation, false);
                if (settings.orientation == "left") {
                    $(this).animate({ left: -inactiveFloatingOffset }, 100, "swing", function () { });
                } else if (settings.orientation == "right") {
                    $(this).animate({ right: -inactiveFloatingOffset }, 100, "swing", function () { });
                } else if (settings.orientation == "top") {
                    $(this).animate({ top: -inactiveFloatingOffset }, 100, "swing", function () { });
                } else if (settings.orientation == "bottom") {
                    $(this).animate({ bottom: -inactiveFloatingOffset }, 100, "swing", function () { });
                }
            });

            slider.click(function (event) {
                event.stopPropagation();
                $.jslane.close(settings.orientation);
            });

            slider.hover(function () {
                title.addClass("hover");
                pane.addClass("hover");
            }, function () {
                title.removeClass("hover");
                pane.removeClass("hover");
            });

            pane.hover(function () {
                $.jslane.setMouseHoverState(settings.orientation, true);
            }, function () {
                $.jslane.setMouseHoverState(settings.orientation, false);
            });

            $("body").click(function (e) {
                e.stopPropagation();
                if (!$.jslane.hasMouseHovering()) {
                    $.jslane.close("left");
                    $.jslane.close("right");
                    $.jslane.close("top");
                    $.jslane.close("bottom");
                }
            });
        });
    }
})(jQuery)