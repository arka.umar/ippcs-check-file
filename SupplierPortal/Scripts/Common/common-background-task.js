﻿var _cmvar_BACKGROUND_TASK_POLLING_INTERVAL = 500;
var _cmvar_BACKGROUND_TASK_PROGRESS_EXCEPTION = -255;

function __cmfunc_updateBackgroundTaskProgress(taskName, finishCallback, errorCallback) {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_GetBackgroundTaskProgress"),
        data: { name: taskName },
        success: function (data) {
            var numData = parseInt(data, 10);
            alert(numData + ": " + numData >= 0);
            if (numData >= 0) {
                if (numData >= 100) {
                    _cmfunc_setProgressBarValue(numData);
                    setTimeout(function () {
                        _cmfunc_closeProgressBar();
                        if (finishCallback && (typeof (finishCallback) === "function")) {
                            finishCallback();
                        }
                    }, 1000);
                } else {
                    _cmfunc_setProgressBarValue(numData);
                    setTimeout(function () {
                        __cmfunc_updateBackgroundTaskProgress(taskName, finishCallback, errorCallback);
                    }, _cmvar_BACKGROUND_TASK_POLLING_INTERVAL);
                }
            } else {
                if (data == _cmvar_BACKGROUND_TASK_PROGRESS_EXCEPTION) {
                    _cmfunc_closeProgressBar();
                    if (errorCallback && (typeof (errorCallback) === "function")) {
                        errorCallback();
                    }
                }
            }
        }
    });
}

function _cmfunc_executeBackgroundTask(taskName, paramMap, finishCallback, errorCallback) {
    $.ajax({
        url: _cmfunc_getActionUrl("_ajax_ExecuteBackgroundTask"),
        data: {
            name: taskName,
            param: JSON.stringify(paramMap)
        },
        success: function (data) {
            setTimeout(function () {
                _cmfunc_openProgressBar();
                __cmfunc_updateBackgroundTaskProgress(taskName, finishCallback, errorCallback);
            }, _cmvar_BACKGROUND_TASK_POLLING_INTERVAL);
        }
    });
}

function _cmfunc_monitorBackgroundTask(taskName, finishCallback, errorCallback) {
    setTimeout(function () {
        _cmfunc_openProgressBar();
        __cmfunc_updateBackgroundTaskProgress(taskName, finishCallback, errorCallback);
    }, _cmvar_BACKGROUND_TASK_POLLING_INTERVAL);
}