﻿var _cmvar_EstimatedGridRowHeightFactor = 20;

function dxRecalculateGridPageSize(gridArray) {
    var winHeight = $(window).height();
    var winHeight = winHeight - 120 - (0.25 * winHeight);
    var pageSize = (winHeight / _cmvar_EstimatedGridRowHeightFactor) / gridArray.length;

    for (var i = 0; i < gridArray.length; i++) {
        gridArray[i].PerformCallback("PageSize=" + pageSize);
    }
}

function _cmfunc_initMessageBoard(props) {
    $("#" + props.id).jssagord({
        backendProcessing: true,
        messageReloadUrl: _cmfunc_getActionUrl(props.messageReloadAction)
    });
}