(function ($) {
    $.fn.jstabmenu = function (options) {
        var settings = $.extend({
            activeMenuId: null
        }, options);

        function styleListItem(listItem) {
            listItem.css("display", "inline");
            listItem.css("list-style", "none");
        }

        function styleAnchor(anchor) {
            anchor.css("text-decoration", "none");
        }

        return this.each(function () {
            var list = $(this);
            var listId = list.attr("id");
            list.addClass("jstabmenu-menu jstabmenu-list");
            list.wrapAll("<div id='" + listId + "-wrapper'/>");
            var wrapper = list.parent("#" + listId + "-wrapper");
            wrapper.addClass("jstabmenu-wrapper");
            wrapper.append("<div id='" + listId + "-menubar'/>");
            wrapper.append("<div id='" + listId + "-submenubar'/>");
            wrapper.append("<div id='" + listId + "-subsubmenubar'/>");

            var menubar = wrapper.children("#" + listId + "-menubar");
            menubar.addClass("jstabmenu-menubar");
            list.appendTo(menubar);
            var submenubar = wrapper.children("#" + listId + "-submenubar");
            submenubar.addClass("jstabmenu-submenubar");
            var subsubmenubar = wrapper.children("#" + listId + "-subsubmenubar");
            subsubmenubar.addClass("jstabmenu-subsubmenubar");
            subsubmenubar.hide();

            var menuItems = list.children("li");
            menuItems.each(function () {
                var menuItem = $(this);
                var menuItemId = menuItem.attr("id");
                menuItem.addClass("jstabmenu-menu-item");
                var menuAnchor = menuItem.children("a").filter(":first");
                menuAnchor.addClass("jstabmenu-anchor");
                var submenu = menuItem.children("ul").filter(":first");
                if ((submenu != undefined) && (submenu.length > 0)) {
                    menuAnchor.append("<span class='jstabmenu-child-marker'>&nabla;</span>");
                    submenu.addClass("jstabmenu-submenu jstabmenu-list");
                    submenu.wrapAll("<div class='jtabmenu-list-wrapper'/>");
                    var submenuListWrapper = submenu.parent();
                    submenuListWrapper.wrapAll("<div id='" + listId + "-submenu-wrapper'/>");
                    var submenuWrapper = submenuListWrapper.parent();
                    submenuWrapper.addClass("jstabmenu-submenu-wrapper");
                    submenuWrapper.addClass(menuItemId);
                    submenuWrapper.appendTo(submenubar);
                    submenuWrapper.hide();
                }

                if (menuItem.attr("id") == settings.activeMenuId) {
                    menuItem.addClass("selected");
                    submenubar.find("." + menuItemId).show();
                }

                var submenuItems = submenu.children("li");
                submenuItems.each(function () {
                    var submenuItem = $(this);
                    var submenuItemId = submenuItem.attr("id");
                    submenuItem.addClass("jstabmenu-submenu-item");
                    var submenuAnchor = submenuItem.children("a").filter(":first");
                    submenuAnchor.addClass("jstabmenu-anchor");
                    var subsubmenu = submenuItem.children("ul").filter(":first");
                    if ((subsubmenu != undefined) && (subsubmenu.length > 0)) {
                        submenuAnchor.append("<span class='jstabmenu-child-marker'>&nabla;</span>");
                        subsubmenu.addClass("jstabmenu-subsubmenu jstabmenu-list");
                        subsubmenu.wrapAll("<div id='" + listId + "-subsubmenu-wrapper'/>");
                        var subsubmenuWrapper = subsubmenu.parent("#" + listId + "-subsubmenu-wrapper");
                        subsubmenuWrapper.addClass("jstabmenu-subsubmenu-wrapper");
                        subsubmenuWrapper.addClass(submenuItemId);
                        subsubmenuWrapper.appendTo(subsubmenubar);
                        subsubmenuWrapper.hide();
                    }

                    if (submenuItem.attr("id") == settings.activeMenuId) {
                        submenuItem.addClass("selected");
                        submenubar.find("." + menuItemId).show();
                        if (!menuItem.hasClass("selected")) {
                            menuItem.addClass("selected");
                        }
                    }

                    var subsubmenuItems = subsubmenu.children("li");
                    subsubmenuItems.each(function () {
                        var subsubmenuItem = $(this);
                        subsubmenuItem.addClass("jstabmenu-subsubmenu-item");
                        var subsubmenuAnchor = subsubmenuItem.children("a").filter(":first");
                        subsubmenuAnchor.addClass("jstabmenu-anchor");
                        subsubmenuAnchor.attr("id", submenuItemId);

                        if (subsubmenuItem.attr("id") == settings.activeMenuId) {
                            if (!menuItem.hasClass("selected")) {
                                menuItem.addClass("selected");
                            }
                            submenubar.children("." + menuItemId).show();
                            var anchorId = subsubmenuAnchor.attr("id");
                            submenuItems.each(function () {
                                var item = $(this);
                                if (item.attr("id") == anchorId) {
                                    item.addClass("selected");
                                }
                            });
                            subsubmenuItem.addClass("selected");
                            subsubmenubar.slideUp("fast", "swing", function () { });
                        }
                    });
                });
            });

            submenubar.append("<div class='jtabmenu-submenu-scroller-right' id='rightScroller'>&raquo;</div>");
            submenubar.append("<div class='jtabmenu-submenu-scroller-left' id='leftScroller'>&laquo;</div>");
            var submenuRightScroller = submenubar.children(".jtabmenu-submenu-scroller-right");
            submenuRightScroller.mousedown(function () {
                $(this).addClass("pushed");
            });
            submenuRightScroller.mouseup(function () {
                $(this).removeClass("pushed");
            });
            var submenuLeftScroller = submenubar.children(".jtabmenu-submenu-scroller-left");
            submenuLeftScroller.mousedown(function () {
                $(this).addClass("pushed");
            });
            submenuLeftScroller.mouseup(function () {
                $(this).removeClass("pushed");
            });

            var submenuItemCollection = submenubar.find("li");
            var submenuWrapperCollection = submenubar.find(".jstabmenu-submenu-wrapper");
            var subsubmenuItemCollection = subsubmenubar.find("li");
            var subsubmenuWrapperCollection = subsubmenubar.find(".jstabmenu-subsubmenu-wrapper");

            wrapper.hover(function () { }, function (e) {
                e.stopPropagation();
                if (subsubmenubar.is(":visible")) {
                    subsubmenubar.slideUp("fast", "swing", function () { });
                }
                submenubar.find(".hover").removeClass("hover");
            });

            menuItems.each(function () {
                var menuItem = $(this);
                var menuAnchor = menuItem.children("a");
                var menuId = $(this).attr("id");
                menuAnchor.click(function (e) {
                    e.stopPropagation();
                    menuItems.filter(".selected").removeClass("selected");
                    menuItem.addClass("selected");
                    submenuWrapperCollection.not("." + menuId).hide();
                    submenuWrapperCollection.filter("." + menuId).show();
                });

                menuItem.hover(function (e) {
                    e.stopPropagation();
                    if (subsubmenubar.is(":visible")) {
                        subsubmenubar.slideUp("fast", "swing", function () { });
                    }
                }, function () { });

                if (menuId == settings.activeMenuId) {
                    menuItem.addClass("selected");
                    submenuWrapperCollection.not("." + menuId).hide();
                    submenuWrapperCollection.filter("." + menuId).show();
                }
            });
            var widthScroll = submenuRightScroller.outerWidth() + submenuLeftScroller.outerWidth() + 30;
            submenuItemCollection.each(function () {
                var submenuItem = $(this);
                var id = submenuItem.attr("id");
                var menuWrappers = subsubmenuWrapperCollection.filter("." + id);
                if (menuWrappers.length > 0) {
                    submenuItem.hover(function (e) {
                        e.stopPropagation();
                        submenubar.find(".hover").removeClass("hover");
                        subsubmenuWrapperCollection.not("." + id).hide();
                        subsubmenuWrapperCollection.filter("." + id).show();
                        if (!subsubmenubar.is(":visible")) {
                            subsubmenubar.slideDown("fast", "swing", function () { });
                        }
                    }, function () { });
                } else {
                    submenuItem.click(function (e) {
                        e.stopPropagation();
                        submenubar.find(".selected").removeClass("selected");
                        subsubmenuItemCollection.filter(".selected").removeClass("selected");
                        $(this).addClass("selected");
                    });
                    submenuItem.hover(function (e) {
                        e.stopPropagation();
                        submenubar.find(".hover").removeClass("hover");
                        if (subsubmenubar.is(":visible")) {
                            subsubmenubar.slideUp("fast", "swing", function () { });
                        }
                    }, function () { });
                }
            });

            subsubmenuItemCollection.each(function () {
                var subsubmenuItem = $(this);
                var subsubmenuAnchor = subsubmenuItem.children("a").filter(":first");

                subsubmenuAnchor.click(function (e) {
                    e.stopPropagation();
                    var anchorId = $(this).attr("id");
                    submenuItemCollection.each(function () {
                        var item = $(this);
                        item.removeClass("selected");
                        if (item.attr("id") == anchorId) {
                            item.addClass("selected");
                        }
                    });
                    subsubmenuItemCollection.filter(".selected").removeClass("selected");
                    subsubmenuItem.addClass("selected");
                    subsubmenubar.slideUp("fast", "swing", function () { });
                });

                subsubmenuAnchor.hover(function (e) {
                    e.stopPropagation();
                    var submenu = submenubar.find("#" + subsubmenuAnchor.attr("id")).addClass("hover");
                });
            });

            submenuRightScroller.click(function () {
                var subwrapper = submenuWrapperCollection.filter(":visible").filter(":first");
                var listWrapper = subwrapper.find(".jtabmenu-list-wrapper");
                var listItem = listWrapper.children("ul").children("li");
                if ((listItem != undefined) && (listItem.length > 0)) {
                    var subwrapperLeftOffset = subwrapper.offset().left + subwrapper.width();
                    var lastItemOffset = listItem.last().offset().left;
                    if (lastItemOffset + widthScroll > subwrapperLeftOffset) {
                        var currentLeft = parseInt(listWrapper.css("left"), 10);
                        var scrollDiff = (-1 * currentLeft) + (subwrapper.outerWidth(true) / 4);
                        // console.log('outerWidth: ', subwrapper.outerWidth(true), 'currentLeft: ', currentLeft, 'scrollDiff: ', scrollDiff, ' lastitemOffset: ', lastItemOffset);
                        if ((currentLeft + scrollDiff) < lastItemOffset) {
                            listWrapper.animate({
                                left: -scrollDiff
                            }, "fast", "swing", function () { });
                        }
                    }
                }
            });
            submenuLeftScroller.click(function () {
                var subwrapper = submenuWrapperCollection.filter(":visible").filter(":first");
                var listWrapper = subwrapper.find(".jtabmenu-list-wrapper");
                var listItem = listWrapper.children("ul").children("li");
                if ((listItem != undefined) && (listItem.length > 0)) {
                    var currentLeft = parseInt(listWrapper.css("left"), 10);
                    if (currentLeft < 0) {
                        var scrollDiff = subwrapper.outerWidth(true) / 4;
                        if (currentLeft < scrollDiff) {
                            scrollDiff = 0;
                        }

                        listWrapper.animate({
                            left: scrollDiff
                        }, "fast", "swing", function () { });
                    }
                }
            });
        });
    }
})(jQuery)