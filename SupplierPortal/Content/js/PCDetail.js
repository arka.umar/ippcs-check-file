var vParams = {}
var mode = "";

function getActMethodUrl(method) {
    return window.__base + "PCDetail/" + method; 
}
function OnBeginCallbackGrid(s, e) {
    var a = {};

    var moda = $('#PCDetailSearchBox').data("mode");

    var searchMode = !(moda == "view" || moda == "edit");
    a.PCNo = PCNoText.GetText();
    a.SupplierCode = (searchMode) 
        ? OIHSupplierOption.GetValue()
        : SupplierCodeText.GetText();
    a.EffectiveDate = EffectiveDateText.GetText();
    
    var iprop = 0;
    for (var p in a){ 
        vParams[p] = a[p];
        e.customArgs[p] = a[p];
    }
    // OnResize();
}
function OnEndCallbackGrid(s, e) {
    
}
function OnBeginCallbackGridItem(s, e) {
    var a = new Object();
    // a.PCNo = ItemPCNoText.GetText(); /// jangan cek PC_NO fid.rizki - 2020.10.16
    a.SupplierCode = SupplierCodeText.GetText();
    a.EffectiveDate = ItemEffectiveDateText.GetText();/// ttep cek kata yanes jagen cek effective_date  fid.rizki - 2020.10.16
    a.BusinessArea = ItemBusinesssAreaText.GetText(); 
    console.log('beginCallbackGrdiItem', a);
     for (var p in a) { 
        vParams[p] = a[p];
        e.customArgs[p] = a[p];
    }
}

function OnEndCallbackGridItem(s, e) {

}

function btnClearClick(s, e) {
    OIHSupplierOption.SetValue();
    GridOIHSupplierOption.UnselectRows();
    GridDetail.UnselectRows();
    GridDetail.ClearFilter();
    SupplierCodeText.SetValue();
    PCNoText.SetValue();
    EffectiveDateText.SetValue();
    SupplierNameText.SetValue();
    SupplierAddressText.SetValue();
    SupplierAttentionText.SetValue();
    PCDateText.SetValue();
}

function btnSearchClick(s, e) {
    GridDetail.ClearFilter();
    var a = {};
    var moda = $('#PCDetailSearchBox').data("mode");

    var searchMode = !(moda == "view" || moda == "edit");
    a.PCNo = PCNoText.GetText();
    if (a.PCNo.length < 17 && a.PCNo.length>0 ) 
        a.PCNo = a.PCNo + '%';
    a.SupplierCode = (searchMode) ? OIHSupplierOption.GetValue()
        : a.SupplierCode = SupplierCodeText.GetText();
    a.EffectiveDate = EffectiveDateText.GetText();

    $.ajax({
        type: "POST",
        url: "PCDetail/GetHeader",
        data: a,
        success: function (result) {
            try {
                var x = JSON.parse(result);
                PCNoText.SetValue(x.PCNo);
                console.log('Set EffectiveDateText: ', x.EffectiveDate);
                EffectiveDateText.SetText(x.EffectiveDate);
                if (searchMode)
                    OIHSupplierOption.SetValue(x.SupplierCode);
                else
                    SupplierCodeText.SetValue(x.SupplierCode);
                PCDateText.SetValue(x.PCDate);
                SupplierAttentionText.SetValue(x.SupplierAttention);
                SupplierAddressText.SetValue(x.SupplierAddress);
                SupplierNameText.SetValue(x.SupplierName);
            } catch (e) {
                console.log(e);
                if (result === "") {
                    console.log("Not found");
                    PCDateText.SetValue();
                    SupplierAttentionText.SetValue();
                    SupplierAddressText.SetValue();
                    SupplierNameText.SetValue();
                    PCNoText.SetValue(a.PCNo);
                    console.log('Set EffectiveDateText: ',a.EffectiveDate);
                    EffectiveDateText.SetText(a.EffectiveDate);
                    if (searchMode)
                        OIHSupplierOption.SetValue(a.SupplierCode);
                    else
                        SupplierCodeText.SetValue(a.SupplierCode);
                }
                else
                    console.log(result);
            }
        },
        error: function (result) {
            alert(result);
        }
    })


}

function btnAddToPCClick(s, e) {
    var selected = GridItem.GetSelectedRowCount();
    if (selected <= 0) {
        alert(Msg("MPCS01011ERR"));
        return;
    }

    var pcNo = ItemPCNoText.GetText();
    var item = '';
    var itemi = [];
    var cpMatNos = '';
    var cpsourceType = '';
    var cpprodPurpose = '';
    var cppacType = '';
    var cppartSfx = '';

    for (var index = GridItem.GetTopVisibleIndex() ; index < ((GridItem.GetPageIndex() * GridItem.cpPageSize) + GridItem.GetVisibleRowsOnPage()) ; index++) {
        if (GridItem.IsRowSelectedOnPage(index)) {
            cpMatNos += GridItem.cpMatNos[index - (GridItem.GetPageIndex() * GridItem.cpPageSize)] + "|";
            cpsourceType += GridItem.cpSourceType[index - (GridItem.GetPageIndex() * GridItem.cpPageSize)] + "|";
            cpprodPurpose += GridItem.cpProdPurpose[index - (GridItem.GetPageIndex() * GridItem.cpPageSize)] + "|";
            cppacType += GridItem.cpPacType[index - (GridItem.GetPageIndex() * GridItem.cpPageSize)] + "|";
            cppartSfx += GridItem.cpPartSfx[index - (GridItem.GetPageIndex() * GridItem.cpPageSize)] + "|";
        }
    }

    for (var inf = 0; inf < selected; inf++) {
        var mat = cpMatNos.split("|")[inf];
        var st = cpsourceType.split("|")[inf];
        var pp = cpprodPurpose.split("|")[inf];
        var pt = cppacType.split("|")[inf];
        var sf = cppartSfx.split("|")[inf];
        itemi.push(mat + "|" + st + "|" + pp + "|" + pt + "|" + sf);
        item = itemi.join(";");
    }

    var suppCd = SupplierCodeText.GetText();
    $.ajax({
        type: "POST",
        url: "PCDetail/PutDraftMaterial",
        data:
    {
        PCNo: pcNo,
        Items: item,
        SupplierCode: suppCd
    },
        success: function (result) {
            GridItem.UnselectRows();
            GridItem.UnselectFilteredRows();
            GridItem.PerformCallback();

            popupItem.Hide();
            btnSearchClick();

            window.localStorage.setItem(pcNo, null);
            alert("Add to PC " + result);


        },
        error: function (result) {
            alert("error updating Material Price");
        }
    });

    //GridItem.GetSelectedFieldValues('MaterialNo;SourceType;ProductionPurpose;PackingType;PartColorSuffix',
    //function (f) {
    //    var pcNo = ItemPCNoText.GetText();
    //    console.log(f);
    //    var item = "";
    //    var itemi = [];

    //    for (var inf = 0; inf < f.length; inf++) {
    //        var fi = f[inf];
    //        itemi.push(fi.join("|"))
    //    }
    //    item = itemi.join(";"); 

    //    var suppCd = SupplierCodeText.GetText();
    //    // var item = JSON.stringify(f);
    //    $.ajax({
    //        type: "POST",
    //        url: "PCDetail/PutDraftMaterial",
    //        data:
    //    {
    //        PCNo: pcNo,
    //        Items: item,
    //        SupplierCode: suppCd
    //    },
    //        success: function (result) {
    //            GridItem.UnselectRows();
    //            GridItem.UnselectFilteredRows();
    //            GridItem.PerformCallback();

    //            popupItem.Hide();
    //            btnSearchClick();

    //            window.localStorage.setItem(pcNo, null);
    //            alert("Add to PC " + result);


    //        },
    //        error: function (result) {
    //            alert("error updating Material Price");
    //        }
    //    })
    //});
}

function btnCloseClick(s, e) {
    Inga();
    popupItem.Hide();
    GridDetail.ClearFilter();
}

function popupItemCloseUp(s, e) {
    var pcNo = PCNoText.GetText();
    console.log('popupItemCloseUp');
    $.post("PCDetail/Lock", { PCNo: pcNo, Act: 1 },
        function (ret) {
    }); 
}
function btnAddClick(s, e) {
    mode = "ADD";
    var pcNo = PCNoText.GetText();
    $.post("PCDetail/Lock", { PCNo: pcNo }, function (ret) {
        if (ret === "SUCCESS") {
            popupItem.Show();
            Inga(1);
            GridItem.UnselectRows();
            GridItem.UnselectFilteredRows();
            GridItem.PerformCallback();
        } else {
            alert(ret);
        }
    }).fail(function () {
        alert('FAILED to LOCK ' + pcNo);
    });
}

function btnDeleteClick(s, e) {
    var  selected = GridDetail.GetSelectedRowCount();
    if (selected <= 0) {
        $.msgBox({ content: Msg("MPCS01011ERR"), type: 'info' })
        return;
    } else if (selected > 1) {
        $.msgBox({ content: Msg("MPCS01012ERR"), type: 'info' });
        return;
    }
    var pcNo = PCNoText.GetText();
    var matNo = null;
    var sourceType = null;
    var prodPurpose = null;
    var packType = null;
    var sfx = null;
    var i = 0;

    GridDetail.GetSelectedFieldValues('MaterialNo;SourceType;ProductionPurpose;PackingType;PartColorSuffix',
        function (f) {
            matNo = f[i][0];
            sourceType = f[i][1];
            prodPurpose = f[i][2];
            packType = f[i][3];
            sfx = f[i][4];
            /// if (confirm(Msg("MPCS01013CON") + " PC No: " + pcNo +" Material No: " + matNo)==1)
            $.msgBox({
                content: Msg("MPCS01013CON") + "<br/> PC No: " + pcNo +"<br/> Material No: " + matNo
                , type: "confirm"
                , title: ""
                , buttons: [{ value: "Ok" }, { value: "Cancel"}],
                success: function (r) {
                    if (r !== "Ok") return;
                    // console.log("Delete Material Price PCNo: " + pcNo + " MatNo: " + matNo);
                    $.ajax({
                        type: "POST",
                        url: "PCDetail/Delete",
                        data:
                            {
                                PCNo: pcNo,
                                MatNo: matNo,
                                SourceType: sourceType,
                                ProductionPurpose: prodPurpose,
                                PackingType: packType,
                                PartColorSuffix: sfx
                            },
                        success: function (result) {
                            //GridDetail.UnselectFilteredRows();
                            //GridDetail.UnselectRows();
                            //GridDetail.ClearFilter();

                            btnSearchClick();

                            $.msgBox({ content: result, title: "Delete" });
                        },
                        error: function (result) {
                            alert("Error DELETING: " + result);

                        }
                    });
                }
            });
            i++;
        });  
    
    
}
function Inga(t) {
    if (window.localStorage) {
        if (t) {
            GridItem.SelectRowsByKey(
            JSON.parse(window.localStorage.getItem(ItemPCNoText.GetText()))
            );
        }
        else {
            GridItem.GetSelectedFieldValues('MaterialNo',
                function (f) {
                    window.localStorage.setItem(ItemPCNoText.GetText(), JSON.stringify(f))
                });
        }
    }
}
function InitialMode(s, e) {
    var moda = $('#PCDetailSearchBox').data("mode");
  
    var searchMode = !(moda == "view" || moda == "edit")
    
    var ed = $('#daEffectiveDate').data("ed");
    ed = ed.substring(2);
    
    console.log("moda: ", moda, " searchMode: ", searchMode, " EffectiveDateText: ", ed);
    
    OIHSupplierOption.SetVisible(searchMode);
    SupplierCodeText.SetVisible(!searchMode);
    PCNoText.GetInputElement().readOnly = !searchMode;
    EffectiveDateText.GetInputElement().readOnly = !searchMode;
    
    EffectiveDateText.SetText(ed);
    btnSearch.SetEnabled(searchMode);
    btnClear.SetEnabled(searchMode);
}

function OIHSupplierOptionSelectionChanged(s, e) {
    SupplierCodeText.SetValue(s.GetValue());
}

function OnResize(s, e) {
    var gridH = window.innerHeight - document.getElementById("GridDetail").offsetTop;

    if (gridH >= 220) {
        GridDetail.SetHeight(gridH);
    } else {
        GridDetail.SetHeight(220);
    }
}

function btnPCClick(s, e) {
    window.location = __base + "PriceConfirmation";
}


function OnEffectiveDateInit(s, e) {
    var calendar = s.GetCalendar();
    calendar.owner = s;
    calendar.SetVisible(false);
}

function OnEffectiveDateDropdown(s, e) {
    var calendar = s.GetCalendar();
    var fastNav = calendar.fastNavigation;
    fastNav.activeView = calendar.GetView(0, 0);
    fastNav.Prepare();
    fastNav.GetPopup().popupVerticalAlign = 'Below';
    fastNav.GetPopup().ShowAtElement(s.GetMainElement())

    fastNav.OnOkClick = function () {
        var parentDateEdit = this.calendar.owner;
        var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
        parentDateEdit.SetDate(currentDate);
        parentDateEdit.HideDropDown();
    }

    fastNav.OnCancelClick = function () {
        var parentDateEdit = this.calendar.owner;
        parentDateEdit.HideDropDown();
    }
}

function OnGridDetailSelectionChanged(s) {
}