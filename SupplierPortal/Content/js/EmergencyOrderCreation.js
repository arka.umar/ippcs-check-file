
function getActMethodUrl(method, fun) {
    if (fun)
        return window.__base + fun + "/" + method;
    return window.__base + "EmergencyOrderCreation/" + method;
}

function BeginCallbackUpload(s, e) {
    if (txtOrderNo.GetText() != "" && (grlDockCode.GetText() != "" || txtDockCode.GetText() != "")
    && (grlSupplierCode.GetText() != "" || txtSupplierCode.GetText() != "") && txtSupplierPlant.GetText() != "") {
        $.ajax({
            type: "POST",
            url: getActMethodUrl("UploadAction"),
            data:
        {
            orderNo: txtOrderNo.GetText(),
            dockCode: (grlDockCode.GetText() != "") ? grlDockCode.GetText() : txtDockCode.GetText(),
            supplierCode: (grlSupplierCode.GetText() != "") ? grlSupplierCode.GetText() : txtSupplierCode.GetText(),
            supplierPlantCode: txtSupplierPlant.GetText()
        },
            success: function (result) {
                EOCDGridView.PerformCallback();
                alert("Upload process success");
            },
            error: function (result) {
                alert("Error when upload data");
            }
        });
    }
    else
        alert("Fields with marked (*) must be filled");
}

function coEOTSelectedIndexChanged(s, e) {
}

function BeginCallbackSaveHeader(s, e) {
    if (txtOrderNo.GetText() != "" && (grlDockCode.GetText() != "" || txtDockCode.GetText() != "")
    && (grlSupplierCode.GetText() != "" || txtSupplierCode.GetText() != "") && txtSupplierPlant.GetText() != ""
    && dtArrivalTime.GetText() != "") {
        $.ajax(
    {
        type: "POST",
        url: getActMethodUrl("SaveCancelHeader"),
        data:
        {
            orderNo: txtOrderNo.GetText(),
            dockCode: (grlDockCode.GetText() != "") ? grlDockCode.GetText() : txtDockCode.GetText(),
            supplierCode: (grlSupplierCode.GetText() != "") ? grlSupplierCode.GetText() : txtSupplierCode.GetText(),
            supplierPlantCode: txtSupplierPlant.GetText(),
            arrivalTime: dtArrivalTime.GetText(),
            statusButton: btnSaveHeader.GetText()
        },
        success: function (result) {
            if (result == "Data has been exist, please verify again.")
                alert(result);
            else {
                if (btnSaveHeader.GetText() == "Save") {
                    alert("Save Process success");
                    btnSubmit.SetEnabled(true);
                    btnNew1.SetEnabled(true);
                    btnDelete1.SetEnabled(true);
                    btnUpload.SetEnabled(true);

                    txtOrderNo.SetEnabled(false);
                    grlDockCode.SetVisible(false);
                    grlDockCode.SetEnabled(false);
                    grlDockCode.SetText("");
                    txtDockCode.SetVisible(true);
                    txtDockCode.SetEnabled(false);
                    txtDockCode.SetText(dockCodePost);
                    dtArrivalTime.SetEnabled(false);
                    grlSupplierCode.SetVisible(false);
                    grlSupplierCode.SetEnabled(false);
                    grlSupplierCode.SetText("");
                    txtSupplierCode.SetVisible(true);
                    txtSupplierCode.SetEnabled(false);
                    txtSupplierCode.SetText(supplierCodePost);
                    txtSupplierPlant.SetEnabled(false);

                    btnSaveHeader.SetText("Cancel");
                }
                else if (btnSaveHeader.GetText() == "Cancel") {
                    alert("Cancel Process Success");
                    btnSubmit.SetEnabled(false);
                    btnNew1.SetEnabled(false);
                    btnDelete1.SetEnabled(false);
                    btnUpload.SetEnabled(false);

                    txtOrderNo.SetEnabled(false);
                    grlDockCode.SetVisible(true);
                    grlDockCode.SetEnabled(true);
                    grlDockCode.SetText("");
                    txtDockCode.SetVisible(false);
                    txtDockCode.SetText("");
                    dtArrivalTime.SetEnabled(true);
                    dtArrivalTime.SetText("");
                    grlSupplierCode.SetVisible(true);
                    grlSupplierCode.SetEnabled(true);
                    grlSupplierCode.SetText("");
                    txtSupplierCode.SetVisible(false);
                    txtSupplierCode.SetText("");
                    txtSupplierPlant.SetEnabled(true);
                    txtSupplierPlant.SetText("");

                    btnSaveHeader.SetText("Save");
                }
            }
        },
        error: function (result) {
            alert("Error when update data");
        }
    });
    }
    else
        alert("Fields with marked (*) must be filled");
}

function BeginCallbackAddNew(s, e) {
    var flagInput = 0;
    if (EOCHSupplierGridLookup.GetText() == "") {
        alert("Please fill Supplier Code field");
    }
    else if (EOCHDockGridLookup.GetText() == "") {
        alert("Please fill Dock Code field");
    }
    else if (grlPartNo.GetText() == "") {
        alert("Please fill Part No field");
    }
    else if (txtPartBarcode.GetText() == "") {
        alert("Please fill Part Barcode field");
    }
    else if (txtPartName.GetText() == "") {
        alert("Part Name Is Empty, Please Wait until all detail data retrieved.")
    }
    else if (txtKanbanNo.GetText() == "") {
        alert("Unique No Is Empty, Please Wait until all detail data retrieved.")
    }
    else if ( !(coEOT.GetValue()) ) {
        alert("Emergency Order type is empty, pick one from selection");        
    }
    else {
        if (buildoutflag == "3") {
            if ((txtPiecesQty.GetText() == "0" || txtPiecesQty.GetText() == "")) {
                alert("Please fill Pieces Quantity field");
                flagInput = 1;
            }
            else if (txtOrderQty.GetText() == "" || txtOrderQty.GetText() == "0") {
                txtOrderQty.SetText(txtPiecesQty.GetText() / txtQtyPerContainer.GetText());
            }
        }
        else {
            if ((txtOrderQty.GetText() == "0" || txtOrderQty.GetText() == "")) {
                alert("Please fill Kanban Quantity field");
                flagInput = 1;
            }
            else if (txtPiecesQty.GetText() == "" || txtPiecesQty.GetText() == "0") {
                txtPiecesQty.SetText(txtQtyPerContainer.GetText() * txtOrderQty.GetText());
            }
        }

        if (flagInput == 0) {

            doAddNewPart();
        }
    }
}

function doAddNewPart(s, e) {
    $.ajax(
	{
	    type: "POST",
	    url: getActMethodUrl("AddNewPart"),
	    data:
		{
		    Supplier: EOCHSupplierGridLookup.GetText(),
		    DockCode: EOCHDockGridLookup.GetText(),
		    PartNo: grlPartNo.GetValue(),
		    PartName: txtPartName.GetText(),
		    KanbanNo: txtKanbanNo.GetText(),
		    OrderQty: txtOrderQty.GetText(),
		    //QtyPerContainer: ddlQtyPerContainer.GetText(),
		    QtyPerContainer: txtQtyPerContainer.GetText(),
		    PiecesQty: txtPiecesQty.GetText(),
		    BuildOutFlag: buildoutflag,
		    PartAddress: txtPartAddress.GetText(),
		    ImportirInfo1: txtImportirInfo1.GetText(),
		    ImportirInfo2: txtImportirInfo2.GetText(),
		    ImportirInfo3: txtImportirInfo3.GetText(),
		    ImportirInfo4: txtImportirInfo4.GetText(),
		    PartBarcode: txtPartBarcode.GetText(),
		    ProgressLaneNo: txtProgressLaneNo.GetText(),
		    ConveyanceNo: txtConveyanceNo.GetText(),
		    EOT: coEOT.GetValue()
		},
	    success: function (result) {

	        if (result.trim() === "Process succesfully") {
	            EOCDGridView.PerformCallback();
	            GridEOCHSupplierGridLookup.UnselectAllRowsOnPage();
	            GridEOCHSupplierGridLookup.ClearFilter();
	            GridEOCHDockGridLookup.UnselectAllRowsOnPage();
	            GridEOCHDockGridLookup.ClearFilter();
	            EOCHDockGridLookup.SetValue("");
	            EOCHSupplierGridLookup.SetValue("");

	            grlPartNo.SetValue("");
	            txtPartName.SetText("");
	            txtKanbanNo.SetText("");
	            //ddlQtyPerContainer.SetText("");
	            txtQtyPerContainer.SetText("");
	            txtOrderQty.SetText("0");
	            txtPiecesQty.SetText("");
	            coEOT.SetText("");
	            popupNew.Hide();
	        }
	        else {
	            alert(result);
	        }
	    },
	    error: function (result) {
	        alert("Error when update data");
	    }
	});

}

function CancelAddNew(s, e) {
    grlPartNo.SetValue("");
    txtPartName.SetText("");
    txtKanbanNo.SetText("");
    //ddlQtyPerContainer.SetText("");
    txtQtyPerContainer.SetText("");
    txtOrderQty.SetText("0");
    txtPiecesQty.SetText("");
    popupNew.Hide();
}



// ================ Emergency Order Creation Header Functions ==================
function OnBeginCallBackLookupPart(s, e) {
    e.customArgs["DockCode"] = EOCHDockGridLookup.GetValue();
    e.customArgs["SupplierCode"] = EOCHSupplierGridLookup.GetValue().split("-")[0];
    e.customArgs["SupplierPlant"] = EOCHSupplierGridLookup.GetValue().split("-")[1];
    e.customArgs["KanbanNo"] = kanbanNo;
}

function OnEOCHDownloadTemplateLinkClick(s, e) {
    $("#EOCMessageLabel").text("Download in progress, \nPlease wait...");
    popupEOCConfirmationUploadProgress.Show();

    $.fileDownload("EmergencyOrderCreation/DownloadTemplate/", {
        successCallback: function (url) {
            popupEOCConfirmationUploadProgress.Hide();
        },
        failCallback: function (responseHtml, url) {
            popupEOCConfirmationUploadProgress.Hide();
            alert(responseHtml);
        }
    });
}

function OnEOCHNewButtonClick(s, e) {
    GridEOCHSupplierGridLookup.UnselectAllRowsOnPage();
    GridEOCHSupplierGridLookup.ClearFilter();
    GridEOCHDockGridLookup.UnselectAllRowsOnPage();
    GridEOCHDockGridLookup.ClearFilter();
    EOCHDockGridLookup.SetValue("");
    EOCHSupplierGridLookup.SetValue("");

    grlPartNo.SetValue("");
    txtPartName.SetText("");
    txtKanbanNo.SetText("");
    txtQtyPerContainer.SetText("");
    txtOrderQty.SetText("0");
    txtPiecesQty.SetText("");

    txtPartAddress.SetText("");
    txtImportirInfo1.SetText("");
    txtImportirInfo2.SetText("");
    txtImportirInfo3.SetText("");
    txtImportirInfo4.SetText("");
    txtPartBarcode.SetText("");
    txtProgressLaneNo.SetText("");
    txtConveyanceNo.SetText("");
    CheckDockImportirInfo("");

    $("#BO_Text").css("display", "none");

    popupNew.Show();
}

function OnClickEOCHSupplierGridLookup(s, e) {
    dockCode = EOCHDockGridLookup.GetValue();
    supplierCode = EOCHSupplierGridLookup.GetValue().split("-")[0];
    supplierPlant = EOCHSupplierGridLookup.GetValue().split("-")[1];

    if (EOCHDockGridLookup.GetValue() != "" && EOCHSupplierGridLookup.GetValue() != "") {
        txtKanbanNo.SetEnabled(true);
        grlPartNo.SetEnabled(true);
        //ddlQtyPerContainer.SetEnabled(true);
        txtQtyPerContainer.SetEnabled(true);
        txtOrderQty.SetEnabled(true);
        txtPartBarcode.SetEnabled(true);

        kanbanNo = txtKanbanNo.GetText();
    }
    else {
        txtKanbanNo.SetEnabled(false);
        grlPartNo.SetEnabled(false);
        //ddlQtyPerContainer.SetEnabled(false);
        txtQtyPerContainer.SetEnabled(false);
        txtOrderQty.SetEnabled(false);
        txtPartBarcode.SetEnabled(false);

        kanbanNo = "";
    }

    GridgrlPartNo.PerformCallback();
}

function OnClickEOCHDockGridLookup(s, e) {
    dockCode = EOCHDockGridLookup.GetValue();
    supplierCode = EOCHSupplierGridLookup.GetValue().split("-")[0];
    supplierPlant = EOCHSupplierGridLookup.GetValue().split("-")[1];

    if (EOCHDockGridLookup.GetValue() != "" && EOCHSupplierGridLookup.GetValue() != "") {
        txtKanbanNo.SetEnabled(true);
        grlPartNo.SetEnabled(true);
        //ddlQtyPerContainer.SetEnabled(true);
        txtQtyPerContainer.SetEnabled(true);
        txtOrderQty.SetEnabled(true);
        txtPartBarcode.SetEnabled(true);

        kanbanNo = txtKanbanNo.GetText();
    }
    else {
        txtKanbanNo.SetEnabled(false);
        grlPartNo.SetEnabled(false);
        //ddlQtyPerContainer.SetEnabled(false);
        txtQtyPerContainer.SetEnabled(false);
        txtOrderQty.SetEnabled(false);
        txtPartBarcode.SetEnabled(false);

        kanbanNo = "";
    }

    CheckDockImportirInfo(dockCode);
    GridgrlPartNo.PerformCallback();
}

function FillPartDescription(s, e) {
    var partNoDetail = grlPartNo.GetValue();
    var supplierCodeDetail = EOCHSupplierGridLookup.GetValue().split("-")[0];
    var supplierPlantCodeDetail = EOCHSupplierGridLookup.GetValue().split("-")[1];
    var dockCodeDetail = EOCHDockGridLookup.GetValue();

    $.ajax({
        type: "POST",
        url: getActMethodUrl("GetPartDescription"),
        data: {
            PartNo: partNoDetail,
            SupplierCode: supplierCodeDetail,
            SupplierPlantCode: supplierPlantCodeDetail,
            DockCode: dockCodeDetail
        },
        success: function (result) {
            $.each(result, function (index, item) {
                if (item) {
                    txtPartName.SetText(item.PART_NAME);
                    txtKanbanNo.SetText(item.KANBAN_NO);
                    txtQtyPerContainer.SetText(String(item.PACK_PER_CONTAINER));
                    //ddlQtyPerContainer.AddItem(String(item.PACK_PER_CONTAINER), String(item.PACK_PER_CONTAINER));
                    //ddlQtyPerContainer.SetValue(String(item.PACK_PER_CONTAINER));
                    oldPcsKanban = String(item.PACK_PER_CONTAINER);

                    if (item.BUILD_OUT_FLAG == "3") {
                        txtOrderQty.SetEnabled(false);
                        txtPartBarcode.SetEnabled(false);
                        txtPiecesQty.SetEnabled(true);
                        //ddlQtyPerContainer.SetEnabled(false);
                        txtQtyPerContainer.SetEnabled(false);
                        buildoutflag = "3";
                    }
                    else {
                        txtOrderQty.SetEnabled(true);
                        txtPartBarcode.SetEnabled(true);
                        txtPiecesQty.SetEnabled(false);
                        //ddlQtyPerContainer.SetEnabled(true);            
                        txtQtyPerContainer.SetEnabled(false);
                        buildoutflag = "";
                    }


                    if (item.BO_PART == 1) {
                        $("#BO_Text").css("display", "inherit");
                    }
                    else {
                        $("#BO_Text").css("display", "none");
                    }

                }
            });
        },
        error: function (result) {
            console.log("Part Description not found");
        }
    });
}

function OnTextChangedtxtKanbanNo(s, e) {
    dockCode = EOCHDockGridLookup.GetValue();
    supplierCode = EOCHSupplierGridLookup.GetValue().split("-")[0];
    supplierPlant = EOCHSupplierGridLookup.GetValue().split("-")[1];
    kanbanNo = txtKanbanNo.GetText();

    $.ajax({
        type: "POST",
        url: getActMethodUrl("GetPartDescriptionByKanban"),
        data: {
            KanbanNo: kanbanNo,
            SupplierCode: supplierCode,
            SupplierPlantCode: supplierPlant,
            DockCode: dockCode
        },
        success: function (result) {
            if (typeof result == "undefined") return; 

            $.each(result, function (index, item) {
                if (item) {
                    txtPartName.SetText(item.PART_NAME);
                    grlPartNo.SetText(item.PART_NO);
                    txtQtyPerContainer.SetText(String(item.PACK_PER_CONTAINER));
                    oldPcsKanban = String(item.PACK_PER_CONTAINER);

                    if (item.BUILD_OUT_FLAG == "3") {
                        txtOrderQty.SetEnabled(false);
                        txtPartBarcode.SetEnabled(false);
                        txtPiecesQty.SetEnabled(true);
                        txtQtyPerContainer.SetEnabled(false);
                        buildoutflag = "3";
                    }
                    else {
                        txtOrderQty.SetEnabled(true);
                        txtPartBarcode.SetEnabled(true);
                        txtPiecesQty.SetEnabled(false);
                        txtQtyPerContainer.SetEnabled(false);
                        buildoutflag = "";
                    }

                    if (item.BO_PART == 1) {
                        $("#BO_Text").css("display", "inherit");
                    }
                    else {
                        $("#BO_Text").css("display", "none");
                    }

                }
            });
        },
        error: function (result) {
            console.log("Part Description by Kanban not found");
        }
    });
    GridgrlPartNo.PerformCallback();
}

function GetBOMessage() {
    $.ajax({
        type: "POST",
        url: getActMethodUrl("GetBOMessage"),
        data: {},
        success: function (result) {
            $("#BO_Text").html(result);
        },
        error: function (result) {
        }
    });
}

function CheckDockImportirInfo(dockCode) {
    if (dockCode != "") {
        $.ajax({
            type: "POST",
            url: getActMethodUrl("GetDockInformation"),
            data: {
                DockCode: dockCode
            },
            success: function (result) {
                if (result == "1") {
                    document.getElementById('trPartAddress').style.visibility = "visible";
                    document.getElementById('trImportirInfo1').style.visibility = "visible";
                    document.getElementById('trImportirInfo2').style.visibility = "visible";
                    document.getElementById('trImportirInfo3').style.visibility = "visible";
                    document.getElementById('trImportirInfo4').style.visibility = "visible";
                    document.getElementById('trPartBarcode').style.visibility = "visible";
                    document.getElementById('trProgressLaneNo').style.visibility = "visible";
                    document.getElementById('trConveyanceNo').style.visibility = "visible";
                }
                else {
                    document.getElementById('trPartAddress').style.visibility = "collapse";
                    document.getElementById('trImportirInfo1').style.visibility = "collapse";
                    document.getElementById('trImportirInfo2').style.visibility = "collapse";
                    document.getElementById('trImportirInfo3').style.visibility = "collapse";
                    document.getElementById('trImportirInfo4').style.visibility = "collapse";
                    document.getElementById('trPartBarcode').style.visibility = "collapse";
                    txtPartBarcode.SetText("1");
                    document.getElementById('trProgressLaneNo').style.visibility = "collapse";
                    document.getElementById('trConveyanceNo').style.visibility = "collapse";
                }
            },
            error: function (result) {
                console.log("Dock information not found");
            }
        });
    }
    else {
        document.getElementById('trPartAddress').style.visibility = "collapse";
        document.getElementById('trImportirInfo1').style.visibility = "collapse";
        document.getElementById('trImportirInfo2').style.visibility = "collapse";
        document.getElementById('trImportirInfo3').style.visibility = "collapse";
        document.getElementById('trImportirInfo4').style.visibility = "collapse";
        document.getElementById('trPartBarcode').style.visibility = "collapse";
        txtPartBarcode.SetText("1");
    }
}

function OnEOCHDeleteButtonClick(s, e) {
    var suppliers = '';
    var docks = '';
    var partnos = '';

    for (var index = EOCDGridView.GetTopVisibleIndex(); index < ((EOCDGridView.GetPageIndex() * EOCDGridView.cpPageSize) + EOCDGridView.GetVisibleRowsOnPage()); index++) {
        if (EOCDGridView.IsRowSelectedOnPage(index)) {
            suppliers += EOCDGridView.cpSupplierPlantCD[index - (EOCDGridView.GetPageIndex() * EOCDGridView.cpPageSize)] + ";";
            docks += EOCDGridView.cpDockCode[index - (EOCDGridView.GetPageIndex() * EOCDGridView.cpPageSize)] + ";";
            partnos += EOCDGridView.cpPartNo[index - (EOCDGridView.GetPageIndex() * EOCDGridView.cpPageSize)] + ";";
        }
    }

    if (partnos.length > 0) {
        suppliers = suppliers.substring(0, suppliers.length - 1);
        docks = docks.substring(0, docks.length - 1);
        partnos = partnos.substring(0, partnos.length - 1);

        $.ajax({
            type: "POST",
            url: getActMethodUrl("DeletePart"),
            data: {
                PartNos: partnos,
                Suppliers: suppliers,
                Docks: docks
            },
            success: function (result) {
                //alert("Success to delete part number");
                EOCDGridView.UnselectAllRowsOnPage();
                EOCDGridView.PerformCallback();
            },
            error: function (result) {
                alert("Error when update data");

            }
        });
    }
    else {
        alert("Please check part to process.");
    }
}

function OnEOCHSubmitButtonClick(s, e) {
    if (EOCDGridView.GetVisibleRowsOnPage() == 0) {
        alert("Please fill Part No at least one");
        return;
    }
    var eoid = '';
    EOCDGridView.GetSelectedFieldValues('SUPPLIER_PLANT_CD;DOCK_CD;PART_NO', 
        function (o) {
            if (o && o.length > 0) {
                for (var i = 0; i < o.length; i++) {
                    v = o[i];
                    eoid = eoid + ((i>0)? ';':'') + v[0].replace('-',',') + ',' + v[1] + ',' + v[2].replace(/-/g,'');
                }

                 $.ajax({
                    type: "POST",
                    url: getActMethodUrl("SubmitData"),
                    data: {
                        ArrivalDate: EOCHArrivalDate.GetText(),
                        ArrivalTime: EOCHArrivalTime.GetText(), 
                        EOID: eoid 
                    },
                    success: function (result) {
                        if (Object.prototype.toString.call(result).replace("[object ", "").replace("]", "") != 'String') {
                            var manifestNo = '';

                            var make = "<table width='100%' border='1' align='center'>";
                            make += "<tr><td>Manifest No</td><td>Order No</td></tr>";

                            $.each(result, function (index, item) {
                                if (item) {
                                    make += "<tr><td>" + item.MANIFEST_NO + "</td><td>" + item.ORDER_NO + "</td></tr>";
                                    manifestNo += item.MANIFEST_NO + ",";
                                }
                            });

                            if (manifestNo.length > 0) {
                                manifestNo = manifestNo.substring(0, manifestNo.length - 1);
                            }

                            make += "</table>";
                            $("#resultSuccess").html(make);

                            var link = '<ul><li><a href="' + getActMethodUrl("Index", "DailyOrder")
                            + '?ManifestNos=_manifestNos_"' +  
                            + '&TabActive=2">Please click following link to check on Order Inquiry</a></li></ul>'.replace('_manifestNos_', manifestNo);
                            $("#linkSuccess").html(link);

                            popupEOCSuccessSubmit.Show();
                            EOCDGridView.PerformCallback();
                        }
                        else {
                            alert("Error : " + result);
                        }
                    },
                    error: function (result) {
                        alert("Error : " + result);
                    }
                });
            }

           
        });


    
}

function OnEOCHCancelButtonClick(s, e) {

}


// ====================== Upload LPOP Confirmation function ======================
function OnEOCHUploadButtonClick(s, e) {
    popupEOCUploadConfirmation.Show();
}

function OnEOCOkButtonClick(s, e) {
    popupEOCUploadConfirmation.Hide();
    EOCFileUpload.UploadFile();
    EOCDGridView.PerformCallback();
}

function OnEOCCancelButtonClick(s, e) {
    popupEOCUploadConfirmation.Hide();
}

function OnEOCFileUploadComplete(s, e) {
    if (e.isValid) {
        filePath = e.callbackData;

        $("#EOCMessageLabel").text("Upload in progress, \nPlease wait...");
        popupEOCConfirmationUploadProgress.Show();

        $.ajax({
            type: "POST",
            url: getActMethodUrl("HeaderTabEOCConfirmation_Upload"),
            dataType: "json",
            data: {
                filePath: filePath
            },
            success: function (result) {
                var parameterValues = result.Status.split(';');
                if (parameterValues[0] == "true") {
                    popupEOCConfirmationUploadProgress.Hide();
                    OnEOCFileUploadValid(parameterValues[1], parameterValues[2], parameterValues[3], parameterValues[4]);
                }
                else if (parameterValues[0] == "false") {
                    popupEOCConfirmationUploadProgress.Hide();
                    OnEOCFileUploadInvalid(parameterValues[1], parameterValues[2], parameterValues[3], parameterValues[4]);
                }
            },
            error: function (result) {
                popupEOCConfirmationUploadProgress.Hide();
                alert("Error On EOCFileUpload File Upload Complete.");
            }
        });
    }
}

function OnEOCFileUploadValid(processId, functionId, temporaryTableUpload, temporaryTableValidate) {
    $.ajax({
        type: "POST",
        url: getActMethodUrl("UploadValid"),
        dataType: "json",
        data: {
            processId: processId,
            functionId: functionId,
            temporaryTableValidate: temporaryTableValidate
        },
        success: function (result) {
            EOCDGridView.PerformCallback();

            // clear temporary upload data
            OnEOCFileUploadCleared(processId, functionId, temporaryTableUpload, temporaryTableValidate);
        },
        error: function (result) {
            alert("Error On EOCFileUpload File Upload Valid.");
        }
    });
}

function OnEOCFileUploadInvalid(processId, functionId, temporaryTableUpload, temporaryTableValidate) {
    $("#EOCMessageLabel").text("Upload invalid, \nPlease wait...");

    $.fileDownload("EmergencyOrderCreation/DownloadInvalid/", {
        dataType: "text",
        data: {
            processId: processId,
            functionId: functionId,
            temporaryTableUpload: temporaryTableUpload
        },
        successCallback: function (url) {
            // clear temporary upload data
            OnEOCFileUploadCleared(processId, functionId, temporaryTableUpload, temporaryTableValidate);
            popupEOCConfirmationUploadProgress.Hide();
        },
        failCallback: function (responseHtml, url) {
            popupEOCConfirmationUploadProgress.Hide();
            alert("Error On EOCFileUpload File Upload Invalid.");
        }
    });
}

function OnEOCFileUploadCleared(processId, functionId, temporaryTableUpload, temporaryTableValidate) {
    $.ajax({
        type: "POST",
        url: getActMethodUrl("ClearAllFeedbackPartUpload"),
        data: {
            processId: processId,
            functionId: functionId,
            temporaryTableUpload: temporaryTableUpload,
            temporaryTableValidate: temporaryTableValidate
        },
        success: function (result) {

        },
        error: function (result) {
            alert("Error On EOCFileUpload File Upload Cleared.");
        }
    });
}
// ==========================================================================
// ================ Emergency Order Creation Detail Functions ==================


function OnEOCOrderInquiryButtonClick(s, e) {
    popupEOCSuccessSubmit.Hide();

}

function CalculatePieceQty(s, e) {
    txtPiecesQty.SetText(txtQtyPerContainer.GetText() * s.GetValue());
}

function CalculateOrderQty(s, e) {
    
    if (parseInt(txtQtyPerContainer.GetText()) <= parseInt(txtPiecesQty.GetText())) {
        alert("Pieces Qty cannot greater than Pcs/Kanban");
        txtQtyPerContainer.SetText(String(oldPcsKanban));
        
        txtPiecesQty.SetText("0");
    }
    else {
        txtQtyPerContainer.SetText(txtPiecesQty.GetText());
        
        txtOrderQty.SetText("1");
    }
}

function OnEditPartNoClick(s, e) {
    var i = parseInt(s.name.substring(15));
    if (typeof i === "undefined") return; 

   
    $("#BO_Text").css("display", "none");

    EOCDGridView.GetRowValues(i, 'SUPPLIER_PLANT_CD;DOCK_CD;PART_NO;PART_NAME;KANBAN_NO;'
        + 'QTY_PER_CONTAINER;ORDER_QTY;PIECES_QTY;KANBAN_PRINT_ADDRESS;IMPORTIR_INFO;'
        + 'IMPORTIR_INFO2;IMPORTIR_INFO3;IMPORTIR_INFO4;PART_BARCODE;PROGRESS_LANE_NO;'
        + 'CONVEYANCE_NO;EO_TYPE',
        function (o) {
            txtOrderQty.SetEnabled(true);
            if (o && o.length > 16) {
                EOCHSupplierGridLookup.SetValue(o[0]);
                CheckDockImportirInfo(0[1]);
                EOCHDockGridLookup.SetValue(o[1]);
                grlPartNo.SetValue(o[2]);
                txtPartName.SetText(o[3]);
                txtKanbanNo.SetText(o[4]);
                txtQtyPerContainer.SetText(o[5]);
                txtOrderQty.SetText(o[6].toString());
                txtPiecesQty.SetText(o[7]);
                txtPartAddress.SetText(o[8]);
                txtImportirInfo1.SetText(o[9]);
                txtImportirInfo2.SetText(o[10]);
                txtImportirInfo3.SetText(o[11]);
                txtImportirInfo4.SetText(o[12]);
                txtPartBarcode.SetText(o[13]);
                txtProgressLaneNo.SetText(o[14]);
                txtConveyanceNo.SetText(o[15]);
                coEOT.SetValue(o[16]);
            }
        })


    popupNew.SetHeaderText('Update Part');
    popupNew.Show();

}

   