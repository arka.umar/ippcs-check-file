function getActMethodUrl(method) {
    return window.__base + "PcToBeReleased/" + method;
}
function dmy(dx) {
    var ax = dx.split(/[\.\-\/]/);
    var d = new Array();

    d.yyyy = Number(ax[2]);
    d.mm = Number(ax[1]);
    d.dd = Number(ax[0]);

    if ((ax.length > 2) &&
        (d.yyyy != NaN && d.mm != NaN && d.dd != NaN)
         && d.yyyy >= 0 && d.yyyy < 9999
         && d.mm > 0 && d.mm < 13
         && d.dd > 0 && d.dd < 32)
        return new Date(d.yyyy, d.mm - 1, d.dd);
    else
        return null;
}

var sx = null;
function delayedSearch(v) {
    sx = setTimeout(function () {
        PcData.PerformCallback();
    }, 3000);
}

function Clear(s, e) {
    PCDate.SetText('');
    PCTo.SetText('');
    EffectiveDate.SetText('');
    EffectiveDateTo.SetText('');
    CurrentPIC.SetText(FullName);
    grlLPHeader.SetText("");
    grlLPHeaderPCNo.SetText("");

    EffectiveDate.SetText("");
    EffectiveDateTo.SetText("");
    PcData.PerformCallback();
}

function PcDetailGridCallback(s, e) {
    e.customArgs["PCNo"] = txtPCNO.GetText();
}
function BeginGridCallback(s, e) {
    e.customArgs["PCDate"] = PCDate.GetText();
    e.customArgs["PCTo"] = PCTo.GetText();
    e.customArgs["EffectiveDateTo"] = EffectiveDateTo.GetText();
    e.customArgs["EffectiveDate"] = EffectiveDate.GetText();

    e.customArgs["grlLPHeader"] = grlLPHeader.GetValue();
    e.customArgs["grlLPHeaderPCNo"] = grlLPHeaderPCNo.GetText();
    e.customArgs["CurrentPIC"] = CurrentPIC.GetText();

}

function ExportToXLS(s, e) {
    $.fileDownload("PcToBeReleased/DownloadExport", {
        data: {
            p_Pcnos: grlLPHeaderPCNo.GetText(),
            p_SuppCd: grlLPHeader.GetValue(),
            p_PcDateBegin: PCDate.GetText(),
            p_PcDateEnd: PCTo.GetText(),
            p_EfDateBegin: EffectiveDate.GetText(),
            p_EfDateEnd: EffectiveDateTo.GetText(),
            p_CurrPIC: CurrentPIC.GetText()
        },
        successCallback: function (url) {},
        failCallback: function (responseHtml, url) {
            alert(responseHtml);
        }
    });
}

function DownloadPDFPC(s, e) {
    var namaHL = s.name.substring(5);
    //var selectedIDs = $('#PcData').find('#PCNo' + namaHL).filter(':first');
    var selecteds = $('#PcData').find('#hl_1' + namaHL).filter(':first');

    $.fileDownload("PcToBeReleased/DownloadPdf", {
        data: {
            PcNo: selecteds.text()
        },
        successCallback: function (url) {},
        failCallback: function (responseHtml, url) {
            alert(responseHtml);
        }
    });
}

function ShowDetailPcToBeReleased(s, e) {
    var namaHL = s.name.substring(4);
    selectedIDs = $('#PcData').find('#hl_1' + namaHL).filter(':first');
    $("#lblPONo").text(selectedIDs.text());
    PCNo = s.GetValue().toString();
    popupRegulerDetail.Show();
}

function HideDetail(s, e) {
    popupRegulerDetail.Hide();
}

function OnCloseHandler(s, e) {

    $("#lblPONo").text(selectedIDs.text());

    PCNo = "";
    popupRegulerDetail.PerformCallback(PCNo);
}

function OnShowHandler(s, e) {
    $("#lblPONo").text(selectedIDs.text());
    popupRegulerDetail.PerformCallback(PCNo);
}

function OnBeginCallback(s, e) {

    $("#lblPONo").text(selectedIDs.text());

    e.customArgs["PCNo"] = PCNo;
    PCNo = null;
}

var tempPoNo = "";
var selectReject = 0;
var screenAction = 0;
var selectedPOs = "";
var selectedPOCount = 0;

function FirstStepReject(s, e) {
    selectReject = parseInt(document.getElementsByName("OPHRejectType")[0].value) + 1;
    if (selectReject == 1)
        selectReject = 2;
    else
        selectReject = 1;

    commentReject = '';
    commentReject = CommentTextReject.GetText();
    if (commentReject == '') {
        alert('Please fill up comment');
    } else {
        ActionPo('REJECT', commentReject);
        popupReject.Hide();
        CommentTextReject.SetText('');
    }
}

function FirstStepApprove(s, e) {
    ActionPo('APPROVE', '');
    popupApprove.Hide();
}

function ActionPo(ActionBut, commentAction) {
    PONoGrid = "";
    SHApprovalStatusGrid = "";

    if (screenAction == "0") {

        var allSelected = chckBox.GetChecked();
        var addi = "";
        for (var i = PcData.cpStartIndex; i <= PcData.cpEndIndex; i++) {
            if (allSelected || PcData.IsRowSelectedOnPage(i)) {
                PONoGrid += addi + PcData.cpPCNo[i];

                addi = ",";
            }
        }
    } else {
        PONoGrid = tempPoNo;
    }
    popupProgress.Show();
    $.ajax({
        type: "POST",
        url: "PcToBeReleased/ActionPurchaseOrder",
        data: {
            DocNoList: PONoGrid,
            ActionBut: ActionBut,
            commentAction: commentAction,
            RejectType: selectReject
        },
        success: function (result) {
            alert(result);
            popupProgress.Hide();
            PcData.UnselectAllRowsOnPage(chckBox.GetChecked());
            PcData.PerformCallback();
        },
        error: function (result) {
            alert("Error when update data");
        }
    });
}

function CheckValidationRedirect(s, e) {
    screenAction = 0;
    validationRedirect = "";
    validationRedirect = ValidateSelectedRows(PcData, e, 'Redirect');

    PONoGrid = selectedPOs;
    if (validationRedirect == "") {

        var allSelected = chckBox.GetChecked();
        if (allSelected) {
            $("#lblcommentRedirect").text('ALL ' + selectedPOCount + ' PC Listed');
        } else {
            $("#lblcommentRedirect").text(PONoGrid);
        }
        popupRedirect.Show();
    } else {
        alert(validationRedirect);
    }
}

function AddRegis(s, e) {
    PopupNewRegis.Show();
}

function CheckValidationReject(s, e) {
    screenAction = 0;
    validationReject = "";
    
    validationReject = ValidateSelectedRows(PcData, e, 'Reject');
    console.log(validationReject);
    PONoGrid = selectedPOs;

    if (validationReject == "") {
        var allSelected = chckBox.GetChecked();
        if (allSelected) {
            $("#lblcommentr").text('ALL ' + selectedPOCount + ' PC Listed');
        } else {
            $("#lblcommentr").text(PONoGrid);
        }
        popupReject.Show();
    } else {
        alert(validationReject);
    }
}

function FirstStepRedirect(s, e) {
    ActionPo('REDIRECT', '');
    popupRedirect.Hide();
}

function PcDataSelectAllCheckedChanged(s, e) {
    var b = s.GetChecked();
    if (b)
        PcData.SelectRows();
    else
        PcData.UnselectRows();
}

function GridSelectionChanged(s, e) {

    var unSelectedIndex = -1;
    for (var index = s.GetTopVisibleIndex(); index < ((s.GetPageIndex() * s.pageRowSize) + s.GetVisibleRowsOnPage()); index++) {
        if (!s.IsRowSelectedOnPage(index)) {
            unSelectedIndex = index;
            break;
        }
    }

    if (unSelectedIndex >= 0) {
        chckBox.SetChecked(false);
    }
}

function ValidateSelectedRows(s, e, aktion) {
    position = s.cpPosition;
    if (!position)
        return;

    var fname = "cp" + position.toUpperCase() + "ApprovalStatus";
    if (s[fname] == undefined)
        return 'Cannot find object to Validate';

    var m = s.cpStartIndex;
    var n = s.cpEndIndex;
    var k = 0;
    var PONos = new Array();
    var Approvals = new Array();
    var illegals = new Array();
    var ki = 0;
    var selc = 0;
    var addiPO = "";
    selectedPOs = "";
    selectedPOCount = 0;
    ErrorMsg = "";
    var allSelected = chckBox.GetChecked();

    for (var i = m; i <= n; i++) {
        if (allSelected || s.IsRowSelectedOnPage(i)) {
            PONos.push(s.cpPCNo[i]);

            var b = s[fname][i];
            console.log(fname + i + ':' + b); 

            Approvals.push(b);
            if (!(b == '1')) {
                illegals.push(s.cpPCNo[i]);
                ki++;
            } else {
                selectedPOs = selectedPOs + addiPO + s.cpPCNo[i];
                selectedPOCount++;
                selc++;
                addiPO = ",";
            }

            k++;
        }
    }

    // alert("Position : " + s.cpPosition);
    // alert("Position Abbr : " + s.cpPositionAbbr);

    if (ki > 0) {

        ErrorMsg = s.cpPositionAbbr;

        ErrorMsg = ErrorMsg + " cannot " + aktion + " PC No: ";
        var addi = "";
        for (var i = 0; i < ki; i++) {
            ErrorMsg = ErrorMsg + addi + illegals[i];
            addi = "; "
        }
    } else {
        if (k < 1)
            ErrorMsg = "Please choose Price Confirmation  to process";
    }
    return ErrorMsg;
}

var x = "";

function CheckValidationApprove(s, e) {
    screenAction = 0;
    validationApprove = "";
    
    validationApprove = ValidateSelectedRows(PcData, e, 'Approve');

    PONoGrid = selectedPOs;

    if (validationApprove == "") {
        var allSelected = chckBox.GetChecked();
        if (allSelected) {
            $("#lblcomment").text('ALL ' + selectedPOCount + ' PC Listed');
        } else {
            $("#lblcomment").text(PONoGrid);
        }
        popupApprove.Show();
    } else {
        alert(validationApprove);

    }
}

function BeginCallbackAddNewRegis(s, e) {
    $.ajax({
        type: "POST",
        url: "PcToBeReleased/AddRegisterPC",
        data: {
            FolioNo: txtFolioNo.GetText(),
            ModuleCd: txtModuleCd.GetText(),
            StatusCd: txtStatusCode.GetText(),
            UserSubmit: txtUserSubmit.GetText(),
        },
        success: function (result) {
            txtFolioNo.SetText("");
            txtModuleCd.SetText("");
            txtStatusCode.SetText("");
            txtUserSubmit.SetText("");
            alert(result);
            PcData.PerformCallback();
            PopupNewRegis.Hide();
        },
        error: function (result) {
            alert("Error when update data");
        }
    });
}

function ValidateSearchCriteria(silent) {
    var doalert = 1;
    if (silent)
        doalert = 0;
    var d1 = dmy(PCDate.GetText());
    var d2 = dmy(PCTo.GetText());
    if (!d1 || !d2) {
        if (doalert)
            alert("PC Date is not valid");
        return false;
    }

    if (d1 - d2 > 0) {
        if (doalert)
            alert("PC Date From Cannot be larger than PC Date to");
        return false;
    }

    d1 = dmy(EffectiveDate.GetText());
    d2 = dmy(EffectiveDateTo.GetText());
    if (d1 && d2) {
        if (!d1 || !d2) {
            if (doalert)
                alert("Effective Date is not valid");
            return false;
        }

        if (d1 - d2 > 0) {
            if (doalert)
                alert("'Effective Date From Cannot be larger than Effective Date to");
            return false;
        }
    } else if (d1 && !d2 || !d1 && d2) {
        if (doalert)
            alert("Effective Date is not valid");
        return false;
    }

    return true;
}
function btnSearchClick(s, e) {
    if (!ValidateSearchCriteria())
        return;

    PcData.PerformCallback();
}
