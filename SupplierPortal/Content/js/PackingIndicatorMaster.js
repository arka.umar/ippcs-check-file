function getActMethodUrl(method, fun) {
    if (fun)
        return window.__base + fun + "/" + method;
    return window.__base + "PackingIndicatorMaster/" + method;
}

function OnOIPClearButtonClick(s, e) {
    OIPMaterialNumber.SetText('');
    OIPDockCode.SetValue(null);
    OIPPackingType.SetValue(null);

    OnOIPSearchButtonClick(s, e);
}

function OnOIPSearchButtonClick(s, e) {
    searchMatNo = OIPMaterialNumber.GetValue();
    searchDockCd = OIPDockCode.GetValue();
    searchPackingType = OIPPackingType.GetValue();
    PackingIndicatorInquiry.PerformCallback();
}

function OnOIPViewBeginCallback(s, e) {
    e.customArgs["MaterialNo"] = searchMatNo;
    e.customArgs["DockCd"] = searchDockCd;
    e.customArgs["PackingType"] = searchPackingType;
}

function OnOIPViewEndCallback(s, e) {

}

function BehaviourAdd(s, e) {
    PackingIndicatorInquiry.UnselectAllRowsOnPage();
}

function BehaviourNormal() {
    PackingIndicatorInquiry.UnselectAllRowsOnPage();
}

function OnShowAddNewData(s, e) {
PackingIndicatorInquiry.PerformCallback();
}

function OIPDeleteClick() {
    var selectedKey = PackingIndicatorInquiry.GetSelectedKeysOnPage();
    if (selectedKey.length < 1) {
        $.msgBox("A single record must be selected to execute Delete operation.");
        return;
    }
    else {
        $.msgBox({
            type: "confirm",
            content: "Are you sure you want to delete the record ?",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    DeletePackingIndicatorMaster();
                }
            }
        });
    }
}

function DeletePackingIndicatorMaster() {
    var deleti = 0;
    var GridID = '';
    var mi = PackingIndicatorInquiry.GetTopVisibleIndex();
    var  mj = mi + PackingIndicatorInquiry.pageRowSize;
    var rowpage = PackingIndicatorInquiry.GetPageIndex() * PackingIndicatorInquiry.pageRowSize;
    for (var Index = mi; Index < mj; Index++) {
        if (PackingIndicatorInquiry.IsRowSelectedOnPage(Index)) {
            var d = PackingIndicatorInquiry.cpData[Index - rowpage];

            GridID += ((deleti > 0) ? ';' : '') + d[0] + '_' + d[1] + '_' + d[2] + '_' + d[3] + '_' + d[5] + '_' + d[7];
            ++deleti;
        }
    }

    $.ajax({
        type: "POST",
        url: getActMethodUrl("DeletePackingIndicatorMaster"),
        data:
            {
                GridId: GridID
            },
        success: function (result) {
            var hery = result.split('|');
            if (hery[0] == 'Error') {
                $.msgBox({ content: "Error happened: " + hery[1], type: "error" });
            }
            else {
                $.msgBox({ content: hery[1], type: 'info' });
            }
        },
        error: function (result) {
            $.msgBox({ content: "Error when deleting: " + result, type: 'error' });
        },
        complete: function (r) {
            PackingIndicatorInquiry.UnselectAllRowsOnPage();
            PackingIndicatorInquiry.PerformCallback();
        }
    });

}

function DownloadTemplateLink_Click(s, e) {
$("#EOCMessageLabel").text("Download in progress, \nPlease wait...");

$.fileDownload(getActMethodUrl("DownloadTemplate"), {
    successCallback: function (url) {

    },
    failCallback: function (responseHtml, url) {

        $.msgBox(responseHtml);
    }
    });
}

function OIPAddClick(){
    AddNewData.Show();
    ResetPopup();
    AddMode(true);
}

function onClickBtnSave(s, e) {
    $.msgBox({
        type: "confirm",
        content: "Are you sure you want to confirm all records?",
        buttons: [{ value: "Yes" }, { value: "No"}],
        success: function (result) {
            if (result !== "Yes") return;
            onSave();
        }
    });
}

function onSave() {
    var status = '';
    var lengtData = AddNewPartColorSuffix.GetValue().length;
    var emsg = "";

    if (AddEditMaterialNo.GetValue() == '')
    { emsg += "Material No should not be empty."; }

    if (AddNewSourceType.GetValue() == '')
    { emsg += "Source Type should not be empty." }

    if (AddNewProdPurpose.GetValue() == '')
    { emsg += "Prod Purpose should not be empty."; }

    if (AddNewPartColorSuffix.GetValue() == '')
    { emsg +="Part Color Suffix should not be empty." ; }

    if (lengtData > 2)
    { emsg += "Part Color Suffix max two chars"; }

    if (AddNewDockCd.GetText() == '')
    { emsg += "Dock Code should not be empty." ; }

    if (AddNewPackingType.GetText() == '')
    { emsg += "Packing Type should not be empty." ; }

    if (AddNewValidFrom.GetText() == '')
    { emsg +=  "Valid From should not be empty."; }

    if (emsg.length > 0) {
        $.msgBox({content: emsg});
        return;
    }
    $.ajax(
        {
            type: "POST",
            url: getActMethodUrl("AddNewPart"),
        data:
        {
            MatNo : AddEditMaterialNo.GetValue(),
            SourceType : AddNewSourceType.GetValue(),
            ProdPurpose : AddNewProdPurpose.GetValue(),
            ColorSuffix : AddNewPartColorSuffix.GetValue(),
            DockCode : AddNewDockCd.GetValue(),
            PackingType :  AddNewPackingType.GetValue(),
            ValidFrom : AddNewValidFrom.GetFormattedDate(),
            ValidTo : AddNewValidTo.GetFormattedDate(),
            status : PackingIndicatorInquiry.cpMode

        },
        success: function (result) {

        var hery = result.split('|');
            if (hery[0] == 'Error')
            {
                $.msgBox(hery[1].replaceAll(/(; )/g, "<br/>"));
            }
            else
            {
                    onClickBtnClose();

                    $.msgBox({ content: hery[1], type: 'info' });
                    BehaviourNormal();
            }
        },
        error: function (result) {
            $.msgBox(result);
        }
    });

}

function ResetPopup() {
    AddEditMaterialNo.SetValue(null);
    AddNewSourceType.SetValue(null);
    AddNewProdPurpose.SetValue(null);
    AddNewPartColorSuffix.SetValue(null);
    AddNewDockCd.SetValue(null);
    AddNewPackingType.SetValue(null);
    AddNewValidFrom.SetValue(null);
}

function onClickBtnClose(s, e) {
    AddNewData.Hide();
    PackingIndicatorInquiry.PerformCallback();
    BehaviourNormal();
    ResetPopup();
    OnOIPClearButtonClick(s, e);
}

function onClickBtnCancel(s, e) {
    $.msgBox({
        type: "confirm",
        content: "Are you sure you want to abort the operation ?",
        buttons: [{ value: "Yes" }, { value: "No"}],
        success: function (result) {
            if (result !== "Yes") return;
            AddNewData.Hide();
            PackingIndicatorInquiry.PerformCallback();
            BehaviourNormal();
            ResetPopup();

            OnOIPClearButtonClick(s, e);
        }
    });
}

function OIPEditClick() {
    var selectedKey = PackingIndicatorInquiry.GetSelectedKeysOnPage();

    if (selectedKey.length < 1) {
        $.msgBox("Please select a record to edit");
        return;
    }

    if (selectedKey.length > 1)
    {
        $.msgBox("Only one record can be edited at a time");
        return;
    }
    AddNewData.Show();
    AddMode(false);
    var rowpage = PackingIndicatorInquiry.GetPageIndex() * PackingIndicatorInquiry.pageRowSize;
    var mi = PackingIndicatorInquiry.GetTopVisibleIndex();
    var mj = mi + PackingIndicatorInquiry.pageRowSize;
    var d = [];

    for (var Index = mi; Index < mj; Index++)
    {
        if (PackingIndicatorInquiry.IsRowSelectedOnPage(Index)) {
            var d = PackingIndicatorInquiry.cpData[Index - rowpage];
            break;
        }
    }
    if (d && d.length > 8) {
        AddEditMaterialNo.SetValue(d[0]);
        AddNewSourceType.SetValue(d[1]);
        AddNewProdPurpose.SetValue(d[2]);
        AddNewPartColorSuffix.SetValue(d[3]);
        AddNewDockCd.SetValue(d[5]);

        AddNewPackingType.SetValue(d[6]);

        AddNewValidFrom.SetText(d[7]);
        AddNewValidTo.SetText(d[8]);

        $("input[name=AddNewValidTo]").attr("placeholder", d[8]);

        PackingIndicatorInquiry.Editing = "";
    }

}

function AddMode(b) {

    AddNewData.SetHeaderText(((b) ? "Add" : "Edit") + " Packing Indicator");
    PackingIndicatorInquiry.cpMode = (b) ? "new": "edit";

    AddEditMaterialNo.SetEnabled(b);
    AddNewSourceType.SetEnabled(b);
    AddNewProdPurpose.SetEnabled(b);
    AddNewPartColorSuffix.SetEnabled(b);
    AddNewDockCd.SetEnabled(b);

}

function OIPDownloadClick ()
{
searchMatNo = OIPMaterialNumber.GetValue();
searchDockCd = OIPDockCode.GetValue();
searchPackingType = OIPPackingType.GetValue();

    $.ajax({
        type: "POST",
        url: getActMethodUrl("CheckBeforeDownload"),
        data: {
            MatNo: searchMatNo,
            DockCd: searchDockCd,
            PackingType: searchPackingType
        },
        success: function(data) {
            var splitMsg = data.Result.split('|');
            if (splitMsg[0] == "E") {
                alert(splitMsg[1]);
            }
            else {

                PackingReportDownload(data.MAT_NO, data.DOCK_CD, data.PACKING_TYPE_CD);
            }
        },
        error: function(data) {
            alert("Download Packing Report Error");
        }
    });
}

function PackingReportDownload(MatNo, DockCd, PackingType)
{
    $("#IPMessageLabel").text("Download in progress,\nPlease wait...");
    LoadingPanel.Show();

    $.fileDownload(getActMethodUrl("DownloadPackingIndicatorMaster"), {
    data:{
            MatNo: MatNo,
            DockCd: DockCd,
            PackingType: PackingType
    },
    successCallback: function(url) {
        LoadingPanel.Hide();
    },
    failCallback: function(responseHtml, url) {
        LoadingPanel.Hide();
        alert(responseHtml);
    }
    });
}

function OnUploadButtonClick(s, e){
if (PackingIndicatorMasterUpload.GetText() == "")
        return alert("Can't upload data, please choose the file first");

    LoadingPanel.Show();
    PackingIndicatorMasterUpload.UploadFile();
}

function PackingIndicatorMasterUploadFileUploadCompleted(s, e) {
    if (!e.isValid) {
        alert("Upload invalid.");
        LoadingPanel.Hide();
        return;
    }

    filePath = e.callbackData;

    $.ajax({
        type: "POST",
        url: getActMethodUrl("saveDataUpload"),
        dataType: "json",
        async: false,
        data: {
            filePath: filePath
        },
        success: function (result) {
            var results = result.split('|');
            if (results[0] == "success") {

                $.msgBox({ content: "Data upload success", type: 'info' });
                PackingIndicatorInquiry.PerformCallback();
                LoadingPanel.Hide();
            } else {
                $.msgBox({ content: results[0], type: 'alert' });

                OnOIPClearButtonClick(s, e);
                LoadingPanel.Hide();

                var pid = results[1];
                $.fileDownload(getActMethodUrl("DownloadErr"),
                {
                    data: {pid: pid},
                    successCallback: function (url) {

                    },
                    failCallback: function (responseHtml, url) {
                        $.msgBox("Error while downloading Upload Error");
                    }
                });
            }
        },
        error: function(result){
            alert(result);
            OnOIPClearButtonClick(s, e);
            LoadingPanel.Hide();
        }

    });

}

function OnMaterialNoLookupClick(s, e) {
}

var matno = "";
function OnLookUpClick(s, e) {
    matNo = AddEditMaterialNo.GetValue().split("|")[0];
    AddEditMaterialNo.SetText(matNo);
}
$(document).ready(function () {
    $(document).on("click", "#AddEditMaterialNo", ResetAddForm);
});

function ResetAddForm() {
    AddEditMaterialNo.SetValue('');

    OnCBLSelectedIndexChanged();
}
function OnCBLSelectedIndexChanged(s, e) {
    s.UnselectAll();
    if (e.isSelected)
        s.SetSelectedIndex(e.index);
    else
        s.UnselectIndices(e.index);
}
