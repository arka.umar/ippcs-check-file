function getActMethodUrl(method, fun) {
    if (fun)
        return window.__base + fun + "/" + method;
    return window.__base + "SourceListMaintenance/" + method;
}
function OnOIPClearButtonClick(s, e) {
    OIPMaterialNo.SetText('');
    OIPSuppCD.SetText('');
    OIPSourceType.SetText('');
    OIPSuppName.SetText('');
    OIPProdPurpose.SetText('');
    OIPPartColorSuffix.SetText('');
    OIPMatDesc.SetText('');
    OIPBuyerCD.SetText('');
    a = 0;

    OnOIPSearchButtonClick(s, e);
}

function OnOIPSearchButtonClick(s, e) {
    SourceListInquiry.PerformCallback();
}

function OnOIPViewBeginCallback(s, e) {
    e.customArgs["MatNo"] = OIPMaterialNo.GetText();
    e.customArgs["SourceType"] = OIPSourceType.GetValue();
    e.customArgs["ProdPurpose"] = OIPProdPurpose.GetValue();
    e.customArgs["ColorSuffix"] = OIPPartColorSuffix.GetText();
    e.customArgs["MatDesc"] = OIPMatDesc.GetText();
    e.customArgs["SuppCD"] = OIPSuppCD.GetText();
    e.customArgs["SuppName"] = OIPSuppName.GetText();
    e.customArgs["BuyerCD"] = OIPBuyerCD.GetText();
    e.customArgs["Condition"] = 1
}

function OIPDeleteClick() {
    var selectedKey = SourceListInquiry.GetSelectedKeysOnPage();
    if (selectedKey.length < 1) {
        $.msgBox("A single record must be selected to execute Delete operation.");
        return;
    }
    else {
        $.msgBox({
            type: "confirm",
            content: "Are you sure you want to delete the record ?",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    DeleteSourceListMaintenance();
                    SourceListInquiry.UnselectAllRowsOnPage();
                    SourceListInquiry.PerformCallback();
                }
                if (result == "No") {
                    SourceListInquiry.UnselectAllRowsOnPage();
                    SourceListInquiry.PerformCallback();
                }
            }
        });
    }
}

function DeleteSourceListMaintenance() {
    var GridId = "";
    var rowpage = SourceListInquiry.GetPageIndex() * SourceListInquiry.cpPageSize;
    for (var Index = SourceListInquiry.GetTopVisibleIndex(); Index < ((rowpage) + SourceListInquiry.GetVisibleRowsOnPage()); Index++) {
        if (SourceListInquiry.IsRowSelectedOnPage(Index)) {
            var ii = Index - rowpage;
            GridId +=
                    SourceListInquiry.cpMaterialNo[ii] + "_" +
                    SourceListInquiry.cpSourceType[ii] + "_" +
                    SourceListInquiry.cpProdPurpose[ii] + "_" +
                    SourceListInquiry.cpPartColorSfx[ii] + "_" +
                    SourceListInquiry.cpSupplierCode[ii] + "_" +
                    SourceListInquiry.cpValidFrom[ii] + "_" +
                    SourceListInquiry.cpDeletionFlag[ii] + ";"
        }

    }
    if (GridId == "") {
        alert("No Data Selected !!!");
    }
    else {

        $.ajax(
            {
                type: "POST",
                url: getActMethodUrl("DeleteSourceListMaintenance"),
                data:
                {
                    GridId: GridId
                },
                success: function (result) {
                    var hery = result.split('|');
                    if (hery[0] == 'Error') {
                        $.msgBox(hery[1]);
                    }
                    else {

                        $.msgBox({ content: hery[1], type: 'info' });
                    }

                },
                error: function (result) {
                    $.msgBox(result);
                }
            });

        SourceListInquiry.UnselectAllRowsOnPage();
        SourceListInquiry.PerformCallback();
    }
}

function OnOIPViewEndCallback(s, e) {
    if (SourceListInquiry.GetPageCount() <= 0) {
        popupNoDataFound.Show();
    }
}

function BehaviourAdd(s, e) {
    SourceListInquiry.UnselectAllRowsOnPage();
}

function BehaviourNormal() {
    SourceListInquiry.UnselectAllRowsOnPage();
}

function OnShowAddNewData(s, e) {
    SourceListInquiry.PerformCallback();
}

function onClickBtnClose(s, e) {
    AddNewData.Hide();
    SourceListInquiry.PerformCallback();
    BehaviourNormal();
    AddNewMaterialNo.SetText('');
    AddEditSupplierCd.SetText('');
    AddNewSourceType.SetText('');
    AddNewProdPurpose.SetText('');
    AddNewPartColorSuffix.SetText('');
    AddNewValidFrom.SetText('');
    AddNewSuppName.SetText('');
    AddNewMatDesc.SetText('');
    OnOIPClearButtonClick(s, e);
}

function onClickBtnCancel(s, e) {
    $.msgBox({
        type: "confirm",
        content: "Are you sure you want to abort the operation ?",
        buttons: [{ value: "Yes" }, { value: "No"}],
        success: function (result) {
            if (result == "Yes") {
                AddNewData.Hide();
                SourceListInquiry.PerformCallback();
                BehaviourNormal();
                AddNewMaterialNo.SetText('');
                AddEditSupplierCd.SetText('');
                AddNewSuppName.SetText('');
                AddNewSourceType.SetText('');
                AddNewProdPurpose.SetText('');
                AddNewPartColorSuffix.SetText('');
                AddNewValidFrom.SetText('');
                AddNewSuppName.SetText('');
                AddNewMatDesc.SetText('');

                OnOIPClearButtonClick(s, e);
            }
            if (result == "No") {

            }
        }
    });
}

function DownloadTemplateLink_Click(s, e) {
    $("#EOCMessageLabel").text("Download in progress, \nPlease wait...");
    //popupConfirmationProgress.Show();

    $.fileDownload(getActMethodUrl("DownloadTemplate"), {
        successCallback: function (url) {
            //popupConfirmationProgress.Hide();
        },
        failCallback: function (responseHtml, url) {
            //popupConfirmationProgress.Hide();
            $.msgBox(responseHtml);
        }
    });
}

function onClickBtnSave(s, e) {

    $.msgBox({
        type: "confirm",
        content: "Are you sure you want to confirm all records?",
        buttons: [{ value: "Yes" }, { value: "No"}],
        success: function (result) {
            if (result == "Yes") {
                onSave()
            }
            if (result == "No") {

            }
        }
    });
}

function onSave() {

    var matNo = AddNewMaterialNo.GetText();
    var matNoSplit = matNo.split('|');

    var srcType = AddNewSourceType.GetText();
    var srcTypeSplit = srcType.split(':');

    var suppCd = AddEditSupplierCd.GetText();
    var suppCdSplit = suppCd.split('|');

    $.ajax({
        type: "POST",
        url: getActMethodUrl("AddNewPart"),
        data:
        {
            MatNo: matNoSplit[0],
            SourceType: srcTypeSplit[0],
            ProdPurpose: AddNewProdPurpose.GetText(),
            ColorSuffix: AddNewPartColorSuffix.GetText(),
            SuppCD: suppCdSplit[0],
            ValidFrom: AddNewValidFrom.GetText(),
            ValidTo: $("input[name=AddNewValidTo]").val()

        },
        success: function (result) {
            if (result == "Saving data is completed successfully") {
                onClickBtnClose();

                $.msgBox({ content: result, type: 'info' });
            }
            else {
                $.msgBox(result);
            }
        },
        error: function (result) {
            $.msgBox(result);
        }
    });
}
function OnUploadClick(s, e) {
    var validExt = ".xls, .xlsx";
    var filePath = SourceListUpload.GetText();
    var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
    var pos = validExt.indexOf(getFileExt);
    if (pos < 0) {
        $.msgBox("Invalid Excel file format. Please use download the correct template from this screen.");
        return false;
    } else {
        SourceListUpload.UploadFile();
        return true;
    }
}
function OnUploadComplete(s, e) {
    if (e.isValid) {
        LoadingPanel.Show();
        var fn = e.callbackData;
        $.ajax({
            type: "POST",
            url: getActMethodUrl("SourceListUpload"),
            dataType: "json",
            async: false,
            data: {
                filename: fn
            },
            success: function (result) {
                LoadingPanel.Hide();
                var parameterValues = result.Status.split(';');
                if (parameterValues[0] == "true") {
                    $.msgBox("Upload Success with Process ID " + parameterValues[1])
                    SourceListInquiry.PerformCallback();
                }
                else if (parameterValues[0] == "false") {
                    $.fileDownload(getActMethodUrl("DownloadErr"), {
                        data: {
                            filename: result.filename
                        },
                        successCallback: function (url) {
                        },
                        failCallback: function (responseHtml, url) {
                            alert(responseHtml);
                        }
                    });
                }
                else if (parameterValues[0] == "empty") {
                    alert("No Data Found in Uploaded Data.");
                }
                else if (parameterValues[0] == "error") {
                    alert("Error on upload process, see Process ID " + parameterValues[1]); 
                }
            },
            error: function (result) {
                LoadingPanel.Hide();
                alert("Error On Upload Process.");
            },
            complete: function () {
                LoadingPanel.Hide();
            }
        });
        
    }
}

function OnDownloadClick() {
    $.fileDownload(getActMethodUrl("Download"), {
        data: {
            MatNo: OIPMaterialNo.GetText()
            , SourceType: OIPSourceType.GetValue()
            , ProdPurpose: OIPProdPurpose.GetValue()
            , ColorSuffix: OIPPartColorSuffix.GetText()
            , MatDesc: OIPMatDesc.GetText()
            , SuppCD: OIPSuppCD.GetText()
            , SuppName:  OIPSuppName.GetText()
            , BuyerCD: OIPBuyerCD.GetText()
            , Condition: 1
        },
        successCallback: function (url) {
        },
        failCallback: function (responseHtml, url) {
            alert(responseHtml);
        }
    });
}