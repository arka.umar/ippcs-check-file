/// InvoiceAndPaymentInquiry moved from View/InvoiceAndPaymentInquiry/index.cshtml

function getActMethodUrl(method, fun) {
    if (fun)
        return window.__base + fun + "/" + method;
    return window.__base + "InvoiceAndPaymentInquiry/" + method;
}

function ValidationSearch() {
    var validate = '0';
    if (OIPUploadDateFromDate.GetText() == '') {
        alert("Please fill Upload Date From");
        validate = '1';
    } else if (OIPUploadDateToDate.GetText() == '') {
        alert("Please fill Upload Date To");
        validate = '1';
    } else if (OIPInvoiceDateFromDate.GetText() == '') {
        alert("Please fill Invoice Date From");
        validate = '1';
    } else if (OIPInvoiceDateToDate.GetText() == '') {
        alert("Please fill Invoice Date To");
        validate = '1';
    }

    return validate;
}

function ValidationPost() {
    var validate = '0';
    if (OIPPartnerBankGridLookup.GetText() == '') {
        alert("Please fill Partner Bank");
        validate = '1';
    } else if (OIPPostingDate.GetText() == '') {
        alert("Please fill Posting Date");
        validate = '1';
    } else if (OIPBaselineDate.GetText() == '') {
        alert("Please fill Baseline Date");
        validate = '1';
    } else if (OIPTermPaymentGridLookup.GetText() == '') {
        alert("Please fill Term of Payment");
        validate = '1';
    } else if (OIPPaymentMethodGridLookup.GetText() == '') {
        alert("Please fill Payment Method");
        validate = '1';
    }

    if (OIPWitholdingTaxGridLookup.GetText() != '') {
        if (OIPBaseAmount.GetText() == '' || OIPBaseAmount.GetText() == '0') {
            alert("Please fill Base Amount");
            validate = '1';
        }
    }

    if (OIPGLAccountGridLookup.GetText() != '') {
        if (OIPPPVAmount.GetText() == '' || OIPPPVAmount.GetText() == '0') {
            alert("Please fill PPV Amount");
            validate = '1';
        }
    }

    return validate;
}

function OIPDownloadClick(s, e) {
    if (ValidationSearch() == '0') {
        $("#IPMessageLabel").text("Download in progress,\nPlease wait...");
        popupProgress.Show();

        SupplierCode = OIPSupplierOption.GetText();
        UploadDateFrom = OIPUploadDateFromDate.GetText();
        UploadDateTo = OIPUploadDateToDate.GetText();
        InvoiceDateFrom = OIPInvoiceDateFromDate.GetText();
        InvoiceDateTo = OIPInvoiceDateToDate.GetText();
        InvoiceNo = OIPInvoiceNoText.GetText();
        StatusCode = OIPStatusOption.GetText();
        PlanPaymentDateFrom = OIPPaymentDateFromDate.GetText();
        PlanPaymentDateTo = OIPPaymentDateToDate.GetText();
        Automatic = OIPAutomaticOption.GetValue();

        $.fileDownload(getActMethodUrl("DownloadListCSVNew"), {
            data: {
                SupplierCode: SupplierCode,
                UploadDateFrom: UploadDateFrom,
                UploadDateTo: UploadDateTo,
                InvoiceDateFrom: InvoiceDateFrom,
                InvoiceDateTo: InvoiceDateTo,
                InvoiceNo: InvoiceNo,
                StatusCode: StatusCode,
                PlanPaymentDateFrom: PlanPaymentDateFrom,
                PlanPaymentDateTo: PlanPaymentDateTo,
                Automatic: Automatic
            },
            successCallback: function (url) {
                popupProgress.Hide();
            },
            failCallback: function (responseHtml, url) {
                popupProgress.Hide();
                alert(responseHtml);
            }
        });
        popupProgress.Hide();
    }
}

function OnOIPDownloadButtonClick(s, e) {
    $("#IPMessageLabel").text("Download in progress,\nPlease wait...");
    popupProgress.Show();

    $.fileDownload(getActMethodUrl("DownloadDetail"), {
        data: {
            InvoiceNo: InvoiceNo,
            InvoiceDate: InvoiceDateParam,
            SupplierCode: SupplierCodeParam
        },
        successCallback: function (url) {
            popupProgress.Hide();
        },
        failCallback: function (responseHtml, url) {
            popupProgress.Hide();
            alert(responseHtml);
        }
    });
    popupProgress.Hide();
}

function OIPPrintClick(s, e) {
    if (ValidationSearch() == '0') {
        $("#IPMessageLabel").text("Download in progress,\nPlease wait...");
        popupProgress.Show();

        var invoiceNos = '';
        var invoiceDates = '';
        var supplierCodes = '';
        var statusCD = '';
        var statusName = '';
        var messageError = '';
        for (var index = GridInvoiceAndPaymentInquiry.GetTopVisibleIndex(); index < ((GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize) + GridInvoiceAndPaymentInquiry.GetVisibleRowsOnPage()); index++) {
            if (GridInvoiceAndPaymentInquiry.IsRowSelectedOnPage(index)) {
                invoiceNos += GridInvoiceAndPaymentInquiry.cpInvoiceNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
                invoiceDates += GridInvoiceAndPaymentInquiry.cpInvoiceDateFormat[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
                supplierCodes += GridInvoiceAndPaymentInquiry.cpSupplierCode[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
                statusCD = GridInvoiceAndPaymentInquiry.cpStatusCd[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)];
                statusName = GridInvoiceAndPaymentInquiry.cpStatusName[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)];

                if (statusCD == '2' || statusCD == '-2') {
                    messageError += '';
                } else {
                    messageError += 'Invoice No: ' + GridInvoiceAndPaymentInquiry.cpInvoiceNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] +
                    ' with status ' + statusName;
                }
            }
        }
        if (messageError != '') {
            messageError += ' cannot be printed';
            alert(messageError);
        } else {
            if (invoiceNos.length > 0) {
                invoiceNos = invoiceNos.substring(0, invoiceNos.length - 1);
                if (invoiceDates.length > 0)
                    invoiceDates = invoiceDates.substring(0, invoiceDates.length - 1);
                if (supplierCodes.length > 0)
                    supplierCodes = supplierCodes.substring(0, supplierCodes.length - 1);

                $.ajax({
                    type: "POST",
                    url: getActMethodUrl("CheckBeforePrintCertificate"),
                    data: {
                        InvoiceNos: invoiceNos,
                        InvoiceDates: invoiceDates,
                        SupplierCodes: supplierCodes
                    },
                    success: function (resultmessage) {
                        if (resultmessage == "OK") {
                            $.fileDownload(getActMethodUrl("PrintCertificate"), {
                                    data: {
                                        InvoiceNos: invoiceNos,
                                        InvoiceDates: invoiceDates,
                                        SupplierCodes: supplierCodes
                                    },
                                    successCallback: function (url) {
                                        popupProgress.Hide();
                                    },
                                    failCallback: function (responseHtml, url) {
                                        popupProgress.Hide();
                                        alert(responseHtml);
                                    }
                                });
                            } else
                                alert(resultmessage);
                        },
                        error: function (resultmessage) {
                            alert('Print Certificate Error');
                        }
                    });
                } else {
                    alert("Please select invoice no");
                }
            }
            popupProgress.Hide();
        }
    }

    function OnOIPSearchButtonClick(s, e) {
        if (ValidationSearch() == '0') {
            SupplierCode = OIPSupplierOption.GetText();
            UploadDateFrom = OIPUploadDateFromDate.GetText();
            UploadDateTo = OIPUploadDateToDate.GetText();
            InvoiceDateFrom = OIPInvoiceDateFromDate.GetText();
            InvoiceDateTo = OIPInvoiceDateToDate.GetText();
            InvoiceNo = OIPInvoiceNoText.GetText();
            StatusCode = OIPStatusOption.GetText();
            PlanPaymentDateFrom = OIPPaymentDateFromDate.GetText();
            PlanPaymentDateTo = OIPPaymentDateToDate.GetText();
            Automatic = OIPAutomaticOption.GetValue();

            GridInvoiceAndPaymentInquiry.PerformCallback();
        }
        __firstLoad = false;
    }

function OnOIPClearButtonClick(s, e) {
	OIPSupplierOption.SetText(ViewData["GridSupplierCode"]);
	OIPUploadDateFromDate.SetText(ViewData["DateFrom"]);
	OIPUploadDateToDate.SetText(ViewData["DateTo"]);
	OIPInvoiceDateFromDate.SetText(ViewData["DateFrom"]);
	OIPInvoiceDateToDate.SetText(ViewData["DateTo"]);
	OIPInvoiceNoText.SetText('');
	OIPPaymentDateFromDate.SetText('');
	OIPPaymentDateToDate.SetText('');
	OIPStatusOption.SetText('');
	OIPAutomaticOption.SetText('');

	SupplierCode = OIPSupplierOption.GetText();
	UploadDateFrom = OIPUploadDateFromDate.GetText();
	UploadDateTo = OIPUploadDateToDate.GetText();
	InvoiceDateFrom = OIPInvoiceDateFromDate.GetText();
	InvoiceDateTo = OIPInvoiceDateToDate.GetText();
	InvoiceNo = OIPInvoiceNoText.GetText();
	StatusCode = OIPStatusOption.GetText();
	PlanPaymentDateFrom = OIPPaymentDateFromDate.GetText();
	PlanPaymentDateTo = OIPPaymentDateToDate.GetText();
	Automatic = OIPAutomaticOption.GetValue();
	GridInvoiceAndPaymentInquiry.PerformCallback();
}

function ShowInvoiceDetail(s, e) {
	var i = '';
	var index = '';
	i = s.name.substring(3);
	index = i - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize);

	InvoiceNo = s.GetValue();
	InvoiceDateParam = GridInvoiceAndPaymentInquiry.cpInvoiceDate[index];
	SupplierCodeParam = GridInvoiceAndPaymentInquiry.cpSupplierCode[index];

	popupInquiryDetail.Show();
	popupInquiryDetail.SetHeaderText('Invoice No : ' + InvoiceNo);
}

function OnOIPViewBeginCallback(s, e) {
	if (!__firstLoad) {
		OIPDownload.SetEnabled(false);
		OIPPrint.SetEnabled(false);
		OIPCancelInvoice.SetEnabled(false);
		OIPToggleButton.SetEnabled(false);
		OIPPostInvoice.SetEnabled(false);
		OIPReverseInvoice.SetEnabled(false);
		OIPSearchButton.SetEnabled(false);
		OIPClearButton.SetEnabled(false);
	}

	e.customArgs["SupplierCode"] = SupplierCode;
	e.customArgs["UploadDateFrom"] = UploadDateFrom;
	e.customArgs["UploadDateTo"] = UploadDateTo;
	e.customArgs["InvoiceDateFrom"] = InvoiceDateFrom;
	e.customArgs["InvoiceDateTo"] = InvoiceDateTo;
	e.customArgs["InvoiceNo"] = InvoiceNo;
	e.customArgs["StatusCode"] = StatusCode;
	e.customArgs["PlanPaymentDateFrom"] = PlanPaymentDateFrom;
	e.customArgs["PlanPaymentDateTo"] = PlanPaymentDateTo;
	e.customArgs["Automatic"] = OIPAutomaticOption.GetValue();
}

function OnOIPViewEndCallback(s, e) {
	if (!__firstLoad) {
		OIPDownload.SetEnabled(true);
		OIPPrint.SetEnabled(true);
		OIPCancelInvoice.SetEnabled(true);
		OIPToggleButton.SetEnabled(true);
		OIPPostInvoice.SetEnabled(true);
		OIPReverseInvoice.SetEnabled(true);
		OIPSearchButton.SetEnabled(true);
		OIPClearButton.SetEnabled(true);
	}
}

function OnBeginCallBackLookupPartnerBank(s, e) {
	e.customArgs["SupplierCode"] = SupplierCode;
	e.customArgs["Currency"] = Currency;
}

function OnOIPViewDetailBeginCallback(s, e) {
	e.customArgs["InvoiceNo"] = InvoiceNo;
	e.customArgs["SupplierCode"] = SupplierCodeParam;
	e.customArgs["InvoiceDate"] = InvoiceDateParam;
}

function OnShowHandler(s, e) {
	InvoiceAndPaymentDetailGrid.UnselectAllRowsOnPage();
	InvoiceAndPaymentDetailGrid.PerformCallback();
}

function OnOIPToggleButtonClick(s, e) {
	GridInvoiceAndPaymentInquiry.PerformCallback('ToggleColumns=OIPReceiveBand;OIPPostingBand;PaymentDocNo;SupplierName');
}

var StatusCdGlobal = '';
var InProgressGlobal = '';
function OIPPostInvoiceClick(s, e) {
	var supplierCode = '';
	var invoiceNos = '';
	var invoiceDate = '';
	var invoiceTaxNo = '';
	var invoiceTaxDate = '';
	var statusCD = '';
	var inProgress = '';
	var i = 0;

	for (var index = GridInvoiceAndPaymentInquiry.GetTopVisibleIndex(); index < ((GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize) + GridInvoiceAndPaymentInquiry.GetVisibleRowsOnPage()); index++) {
		if (GridInvoiceAndPaymentInquiry.IsRowSelectedOnPage(index)) {
			supplierCode += GridInvoiceAndPaymentInquiry.cpSupplierCode[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			Currency = GridInvoiceAndPaymentInquiry.cpCurrency[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)];
			invoiceNos += GridInvoiceAndPaymentInquiry.cpInvoiceNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceDate += GridInvoiceAndPaymentInquiry.cpInvoiceDate[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceTaxNo += GridInvoiceAndPaymentInquiry.cpInvoiceTaxNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceTaxDate += GridInvoiceAndPaymentInquiry.cpInvoiceTaxDate[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			statusCD += GridInvoiceAndPaymentInquiry.cpStatusCd[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			inProgress += GridInvoiceAndPaymentInquiry.cpInProgress[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";

			i = i + 1;
		}
	}

	if (i == 1) {
	    if (invoiceNos.length > 0) {
	        invoiceNos = invoiceNos.substring(0, invoiceNos.length - 1);
	        if (statusCD.length > 0)
	            statusCD = statusCD.substring(0, statusCD.length - 1);
	        if (inProgress.length > 0)
	            inProgress = inProgress.substring(0, inProgress.length - 1);
	        if (invoiceDate.length > 0)
	            invoiceDate = invoiceDate.substring(0, invoiceDate.length - 1);
	        if (supplierCode.length > 0)
	            supplierCode = supplierCode.substring(0, supplierCode.length - 1);

	        StatusCdGlobal = statusCD;
	        InProgressGlobal = inProgress;

	        $.ajax({
	            type: "POST",
	            url: getActMethodUrl("GetInvoicePaymentDetailPosting"),
	            data: {
	                SupplierInvoiceNo: invoiceNos,
	                SupplierCode: supplierCode,
	                InvoiceDate: invoiceDate
	            },
	            success: function (result) {
	                $.each(result, function (index, item) {
	                    if (item) {
	                        SupplierCode = item.SUPP_CD;
	                        OIPSupplierGridLookup.SetValue(item.SUPP_CD);
	                        OIPPartnerBankGridLookup.SetValue(item.SUPP_BANK_TYPE);
	                        txtSupplierInvoiceNo.SetText(invoiceNos);
	                        OIPAmount.SetText(item.AMT);
	                        if (item.POSTING_DT_STRING != "01.01.0001")
	                            OIPPostingDate.SetText(item.POSTING_DT_STRING);
	                        else
	                            OIPPostingDate.SetText(ViewData["DateTo"]);

	                        OIPInvoiceNote.SetText(item.INV_TEXT);
	                        if (item.INV_DT_STRING != "01.01.0001")
	                            OIPInvoiceDate.SetText(item.INV_DT_STRING);
	                        OIPHeaderText.SetText(item.INV_TAX_NO);
	                        if (item.BASELINE_DT_STRING != "01.01.0001")
	                            OIPBaselineDate.SetText(item.BASELINE_DT_STRING);
	                        OIPAssignment.SetText(item.ASSIGNMENT);
	                        OIPTaxDate.SetText(item.INV_TAX_DT_STRING);
	                        OIPWitholdingTaxGridLookup.SetText(item.WITHHOLDING_TAX_CD);
	                        OIPBaseAmount.SetText(item.BASE_AMT);
	                        OIPGLAccountGridLookup.SetText(item.GL_ACCOUNT);
	                        OIPPPVAmount.SetText(item.GL_INV_AMT);

	                        if (invoiceTaxNo != '') {
															// Start Changed by FID) Yudia (2022-04-09) : No Default Set for TAX_CD if null
	                            //if (item.TAX_CD == null) {
																	// Start Changed by FID) Yudia (2022-03-13) : Change Default from V1 to I2
																	/*
	                                OIPTaxGridLookup.SetValue('V1');
	                                GridOIPTaxGridLookup.SelectRowsByKey(new Array('V1'));
	                                */
																  /*
	                                OIPTaxGridLookup.SetValue('I2');
	                                GridOIPTaxGridLookup.SelectRowsByKey(new Array('I2'));
																	chkCalculateTax.SetChecked(true);
																	*/
																	// End Changed
	                            //} else {
	                                OIPTaxGridLookup.SetValue(item.TAX_CD);
	                                GridOIPTaxGridLookup.SelectRowsByKey(new Array(item.TAX_CD));
	                                chkCalculateTax.SetChecked(true);
	                            //}
	                        }
	                        if (item.PAY_TERM_CD == null) {
	                            OIPTermPaymentGridLookup.SetValue('ZD00');
	                            GridOIPTermPaymentGridLookup.SelectRowsByKey(new Array('ZD00'));
	                        } else {
	                            OIPTermPaymentGridLookup.SetValue(item.PAY_TERM_CD);
	                            GridOIPTermPaymentGridLookup.SelectRowsByKey(new Array(item.PAY_TERM_CD));
	                        }

	                        if (item.PAY_METHOD_CD == null) {
	                            OIPPaymentMethodGridLookup.SetValue('T');
	                            GridOIPPaymentMethodGridLookup.SelectRowsByKey(new Array('T'));
	                        } else {
	                            OIPPaymentMethodGridLookup.SetValue(item.PAY_METHOD_CD);
	                            GridOIPPaymentMethodGridLookup.SelectRowsByKey(new Array(item.PAY_METHOD_CD));
	                        }

	                        if ((item.PPV_EVIDENCE != null) && (item.PPV_EVIDENCE != "")) {
	                            $("#filename").html(item.PPV_EVIDENCE);
	                            $("#PPVEvidence").css("display", "inherit");
	                        } else {
	                            $("#filename").html('');
	                            $("#PPVEvidence").css("display", "none");
	                        }

	                        GridOIPPartnerBankGridLookup.PerformCallback();
	                        popupPost.Show();

	                        if (StatusCdGlobal != "3" && StatusCdGlobal != "-4" && StatusCdGlobal != "-5")
	                            OIPPostButton.SetEnabled(false);
	                        else
	                            OIPPostButton.SetEnabled(true);
	                    }
	                });
	            },
	            error: function (result) {
	                alert("Data not exist");
	            }
	        });
	    } else {
	        alert("Please select invoice no");
	    }
	} else if (i > 1) {
	    alert("Please select only ONE invoice no");
	} else if (i <= 0) {
	    alert("Please select invoice no");
	}
}

function OnOIPCancelButtonClick(s, e) {
	txtSupplierInvoiceNo.SetText('');
	OIPSupplierGridLookup.SetText('');
	OIPPostingDate.SetText(ViewData["DateFrom"]);
	OIPInvoiceDate.SetText(ViewData["DateFrom"]);
	OIPBaselineDate.SetText('');
	OIPTermPaymentGridLookup.SetText('');
	OIPPaymentMethodGridLookup.SetText('');
	OIPWitholdingTaxGridLookup.SetText('');
	OIPBaseAmount.SetText('');
	OIPPartnerBankGridLookup.SetText('');
	chkCalculateTax.SetChecked(false);
	OIPAmount.SetText('');
	OIPInvoiceNote.SetText('');
	OIPHeaderText.SetText('');
	OIPAssignment.SetText('');
	OIPTaxGridLookup.SetText('');
	OIPTaxDate.SetText('');
	OIPGLAccountGridLookup.SetText('');
	OIPPPVAmount.SetText('');
	OIPAutomaticOption.SetText('');

	GridOIPTermPaymentGridLookup.UnselectAllRowsOnPage();
	GridOIPTermPaymentGridLookup.ClearFilter();
	GridOIPPaymentMethodGridLookup.UnselectAllRowsOnPage();
	GridOIPPaymentMethodGridLookup.ClearFilter();
	GridOIPWitholdingTaxGridLookup.UnselectAllRowsOnPage();
	GridOIPWitholdingTaxGridLookup.ClearFilter();
	GridOIPPartnerBankGridLookup.UnselectAllRowsOnPage();
	GridOIPPartnerBankGridLookup.ClearFilter();
	GridOIPTaxGridLookup.UnselectAllRowsOnPage();
	GridOIPTaxGridLookup.ClearFilter();
	GridOIPGLAccountGridLookup.UnselectAllRowsOnPage();
	GridOIPGLAccountGridLookup.ClearFilter();

	popupPost.Hide();
}

function OnOIPSaveButtonClick(s, e) {
	var ppv = OIPPPVAmount.GetText();
	if ((ppv != null) && (ppv != '0') && (ppv != 0) && (ppv != "")) {
		var evidence = $("#filename").html();
		if ((evidence == null) || (evidence == "") || (evidence == undefined)) {
			alert('Please Upload PPV Evidence if PPV Amount is not 0 or empty');
			return;
		}
	}

	OIPPostButton.SetEnabled(false);
	OIPSaveButton.SetEnabled(false);
	OIPCancelButton.SetEnabled(false);
	popupPost.Show();

	$.ajax({
		type: "POST",
		url: getActMethodUrl("SaveInvoicePayment"),
		async: false,
		data: {
			SupplierInvoiceNo: txtSupplierInvoiceNo.GetText(),
			SupplierCode: OIPSupplierGridLookup.GetText(),
			PostingDate: "",
			InvoiceDate: OIPInvoiceDate.GetText(),
			BaselineDate: "",
			TermOfPayment: OIPTermPaymentGridLookup.GetText(),
			PaymentMethod: OIPPaymentMethodGridLookup.GetText(),
			WithHoldingTaxCode: OIPWitholdingTaxGridLookup.GetText(),
			BaseAmount: OIPBaseAmount.GetText(),
			PartnerBank: OIPPartnerBankGridLookup.GetText(),
			CalculateTax: chkCalculateTax.GetChecked(),
			Amount: OIPAmount.GetText(),
			InvoiceNote: OIPInvoiceNote.GetText(),
			HeaderText: OIPHeaderText.GetText(),
			Assignment: OIPAssignment.GetText(),
			TaxCode: OIPTaxGridLookup.GetText(),
			TaxDate: OIPTaxDate.GetText(),
			GLAccount: OIPGLAccountGridLookup.GetText(),
			PPVAmount: OIPPPVAmount.GetText()
		},
		success: function (result) {
			alert(result);
			SupplierCode = OIPSupplierOption.GetText();
			UploadDateFrom = OIPUploadDateFromDate.GetText();
			UploadDateTo = OIPUploadDateToDate.GetText();
			InvoiceDateFrom = OIPInvoiceDateFromDate.GetText();
			InvoiceDateTo = OIPInvoiceDateToDate.GetText();
			InvoiceNo = OIPInvoiceNoText.GetText();
			StatusCode = OIPStatusOption.GetText();
			PlanPaymentDateFrom = OIPPaymentDateFromDate.GetText();
			PlanPaymentDateTo = OIPPaymentDateToDate.GetText();

			GridInvoiceAndPaymentInquiry.UnselectAllRowsOnPage();
			GridInvoiceAndPaymentInquiry.PerformCallback();
			OIPPostButton.SetEnabled(true);
			OIPSaveButton.SetEnabled(true);
			OIPCancelButton.SetEnabled(true);
			popupPost.Hide();
		},
		error: function (result) {
			alert("Error when post data");
		}
	});
}

function OnOIPPostButtonClick(s, e) {
	var ppv = OIPPPVAmount.GetText();
	if ((ppv != null) && (ppv != '0') && (ppv != 0) && (ppv != "")) {
		var evidence = $("#filename").html();
		if ((evidence == null) || (evidence == "") || (evidence == undefined)) {
			alert('Please Upload PPV Evidence if PPV Amount is not 0 or empty');
			return;
		}
	}

	OIPPostButton.SetEnabled(false);
	OIPSaveButton.SetEnabled(false);
	OIPCancelButton.SetEnabled(false);
	var monthPostingDate = '';
	var jsDate = OIPPostingDate.GetDate();

	if (StatusCdGlobal == "3" || StatusCdGlobal == "-4" || StatusCdGlobal == "-5") {
		if (InProgressGlobal == "1") {
			alert("Please wait until Progress Status is Completed");
			OIPPostButton.SetEnabled(true);
			OIPSaveButton.SetEnabled(true);
			OIPCancelButton.SetEnabled(true);
		} else {
			if (ValidationPost() == '0') {
				$.ajax({
					type: "GET",
					url: getActMethodUrl("GetMonthPostingDate"),
					async: false,
					data: {},
					success: function (result) {
						monthPostingDate = result;
					},
					error: function (result) {
						alert("Error when get data");
					}
				});
				var postingMonth = parseInt(monthPostingDate) + 3;
				if (postingMonth > 12)
					postingMonth = postingMonth % 12;
				if (postingMonth != parseInt(jsDate.getMonth() + 1)) {
					alert("Posting Date Period is closed, Current Period is " + monthPostingDate);
					OIPPostButton.SetEnabled(true);
					OIPSaveButton.SetEnabled(true);
					OIPCancelButton.SetEnabled(true);
				} else {
					$.ajax({
						type: "POST",
						url: getActMethodUrl("PostInvoicePayment"),
						async: false,
						data: {
							SupplierInvoiceNo: txtSupplierInvoiceNo.GetText(),
							SupplierCode: OIPSupplierGridLookup.GetText(),
							PostingDate: OIPPostingDate.GetText(),
							InvoiceDate: OIPInvoiceDate.GetText(),
							BaselineDate: OIPBaselineDate.GetText(),
							TermOfPayment: OIPTermPaymentGridLookup.GetText(),
							PaymentMethod: OIPPaymentMethodGridLookup.GetText(),
							WithHoldingTaxCode: OIPWitholdingTaxGridLookup.GetText(),
							BaseAmount: OIPBaseAmount.GetText(),
							PartnerBank: OIPPartnerBankGridLookup.GetText(),
							CalculateTax: chkCalculateTax.GetChecked(),
							Amount: OIPAmount.GetText(),
							InvoiceNote: OIPInvoiceNote.GetText(),
							HeaderText: OIPHeaderText.GetText(),
							Assignment: OIPAssignment.GetText(),
							TaxCode: OIPTaxGridLookup.GetText(),
							TaxDate: OIPTaxDate.GetText(),
							GLAccount: OIPGLAccountGridLookup.GetText(),
							PPVAmount: OIPPPVAmount.GetText()
						},
						success: function (result) {
							alert(result);
							SupplierCode = OIPSupplierOption.GetText();
							UploadDateFrom = OIPUploadDateFromDate.GetText();
							UploadDateTo = OIPUploadDateToDate.GetText();
							InvoiceDateFrom = OIPInvoiceDateFromDate.GetText();
							InvoiceDateTo = OIPInvoiceDateToDate.GetText();
							InvoiceNo = OIPInvoiceNoText.GetText();
							StatusCode = OIPStatusOption.GetText();
							PlanPaymentDateFrom = OIPPaymentDateFromDate.GetText();
							PlanPaymentDateTo = OIPPaymentDateToDate.GetText();

							GridInvoiceAndPaymentInquiry.UnselectAllRowsOnPage();
							GridInvoiceAndPaymentInquiry.PerformCallback();
							OIPPostButton.SetEnabled(true);
							OIPSaveButton.SetEnabled(true);
							OIPCancelButton.SetEnabled(true);
							popupPost.Hide();
						},
						error: function (result) {
							alert("Error when post data");
						}
					});
				}
			} else {
				OIPPostButton.SetEnabled(true);
				OIPSaveButton.SetEnabled(true);
				OIPCancelButton.SetEnabled(true);
			}
		}
	} else {
		alert("Please select data with status Submitted, Reversed, or Error Posting");
		OIPPostButton.SetEnabled(true);
		OIPSaveButton.SetEnabled(true);
		OIPCancelButton.SetEnabled(true);
	}
}

function OnOIPCancelInvoiceClick(s, e) {
	var invoiceNos = '';
	var invoiceTaxNos = '';
	var statusCd = '';
	var suppCd = '';
	var invoiceDate = '';
	var inProgress = '';
	var l = 0;

	for (var index = GridInvoiceAndPaymentInquiry.GetTopVisibleIndex(); index < ((GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize) + GridInvoiceAndPaymentInquiry.GetVisibleRowsOnPage()); index++) {
		if (GridInvoiceAndPaymentInquiry.IsRowSelectedOnPage(index)) {
			invoiceTaxNos += GridInvoiceAndPaymentInquiry.cpInvoiceTaxNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceNos += GridInvoiceAndPaymentInquiry.cpInvoiceNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			statusCd += GridInvoiceAndPaymentInquiry.cpStatusCd[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			suppCd += GridInvoiceAndPaymentInquiry.cpSupplierCode[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceDate += GridInvoiceAndPaymentInquiry.cpInvoiceDate[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			inProgress = GridInvoiceAndPaymentInquiry.cpInProgress[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)];
			l++;
		}
	}

	if (l > 1) {
		alert('Please Select Only One Invoice To Be Cancelled');
		return;
	}

	if (invoiceNos.length > 0) {
		invoiceNos = invoiceNos.substring(0, invoiceNos.length - 1);
		if (statusCd.length > 0)
			statusCd = statusCd.substring(0, statusCd.length - 1);
		if (suppCd.length > 0)
			suppCd = suppCd.substring(0, suppCd.length - 1);
		if (invoiceDate.length > 0)
			invoiceDate = invoiceDate.substring(0, invoiceDate.length - 1);
		if (invoiceTaxNos.length > 0)
			invoiceTaxNos = invoiceTaxNos.substring(0, invoiceTaxNos.length - 1);

		if (statusCd === '1') {
			alert("Outstanding GR Cannot be Cancelled, use Re-Process Instead");
			return;
		}

		if (inProgress == "1" || inProgress == "3") {
			alert("Please wait until Progress Status is Completed");
		} else {
			if (confirm("Are you sure you want to cancel the selected invoice?")) {
				$.ajax({
					type: "POST",
					url: getActMethodUrl("CancelInvoice"),
					async: false,
					data: {
						SupplierInvoiceNo: invoiceNos,
						SupplierCode: suppCd,
						InvoiceDate: invoiceDate,
						StatusCd: statusCd,
						InvTaxNo: invoiceTaxNos
					},
					success: function (result) {
						alert(result);
						GridInvoiceAndPaymentInquiry.UnselectAllRowsOnPage();
						GridInvoiceAndPaymentInquiry.PerformCallback();
						popupPost.Hide();
					},
					error: function (result) {
						alert("Error when post data");
					}
				});
			}
		}
	} else {
		alert("Please select invoice no");
	}
}
function OnOIPReverseInvoiceClick(s, e) {
	var invoiceNos = '';
	var statusCd = '';
	var invoiceTaxNos = '';
	var inProgress = '';
	var supplierCode = '';
	var l = 0;

	for (var index = GridInvoiceAndPaymentInquiry.GetTopVisibleIndex(); index < ((GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize) + GridInvoiceAndPaymentInquiry.GetVisibleRowsOnPage()); index++) {
		if (GridInvoiceAndPaymentInquiry.IsRowSelectedOnPage(index)) {
			supplierCode += GridInvoiceAndPaymentInquiry.cpSupplierCode[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceTaxNos += GridInvoiceAndPaymentInquiry.cpInvoiceTaxNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceNos += GridInvoiceAndPaymentInquiry.cpInvoiceNoICS[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			statusCd += GridInvoiceAndPaymentInquiry.cpStatusCd[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			inProgress = GridInvoiceAndPaymentInquiry.cpInProgress[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)];
			l++;
		}
	}

	if (l > 1) {
		alert('Please Select Only One Invoice To Be Reversed');
		return;
	}

	if (invoiceNos.length > 0) {
		invoiceNos = invoiceNos.substring(0, invoiceNos.length - 1);
		if (statusCd.length > 0)
			statusCd = statusCd.substring(0, statusCd.length - 1);
		if (invoiceTaxNos.length > 0)
			invoiceTaxNos = invoiceTaxNos.substring(0, invoiceTaxNos.length - 1);
		if (supplierCode.length > 0)
			supplierCode = supplierCode.substring(0, supplierCode.length - 1);

		if (statusCd == '4') {
			if (inProgress == "1" || inProgress == "3") {
				alert("Please wait until Progress Status is Completed");
			} else {
				$.ajax({
					type: "POST",
					url: getActMethodUrl("ReverseInvoicePayment"),
					async: false,
					data: {
						InvoiceNo: invoiceNos,
						InvTaxNo: invoiceTaxNos,
						SupplierCode: supplierCode
					},
					success: function (result) {
						alert(result);
						GridInvoiceAndPaymentInquiry.UnselectAllRowsOnPage();
						GridInvoiceAndPaymentInquiry.PerformCallback();
					},
					error: function (result) {
						alert("Error when post data");
					}
				});
			}
		} else {
			alert("Please select invoice that has been posted");
		}
	} else {
		alert("Please select invoice no");
	}
}
function TextValueChangedMergeNewLineToSemiColon(s, e) {
	var t = s.GetText();
	t = t.replace(/\s+/g, ';');
	s.SetText(t);
}
function OnDetailStatusNameLinkClick(s, e) {
	var i = s.name.substring(23);
	var currentTime = $.now();
	var _frameId = "download" + currentTime;
	var _suppCode;
	_suppCode = $('#GridInvoiceAndPaymentInquiry').find('#OIPSupplierCodeLabel' + i).filter(':first');

	$("#errorDownloadContainer").append("<iframe id='" + _frameId + "' src='" + getActMethodUrl("DownloadError", "InvoiceCreation") + "?supplierCode=" + _suppCode.text() + "' style='visibility:collapse'></iframe>");
	var _theframe = document.getElementById(_frameId);
	_theframe.contentWindow.location.href = _theframe.src;
}

function OnOIPReProcessClick(s, e) {
	var invoiceNos = '';
	var statusCd = '';
	//var invoiceTaxNos = '';
	//var inProgress = '';
	var supplierCode = '';
	var l = 0;

	for (var index = GridInvoiceAndPaymentInquiry.GetTopVisibleIndex(); index < ((GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize) + GridInvoiceAndPaymentInquiry.GetVisibleRowsOnPage()); index++) {
		if (GridInvoiceAndPaymentInquiry.IsRowSelectedOnPage(index)) {
			supplierCode += GridInvoiceAndPaymentInquiry.cpSupplierCode[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			//invoiceTaxNos += GridInvoiceAndPaymentInquiry.cpInvoiceTaxNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			invoiceNos += GridInvoiceAndPaymentInquiry.cpInvoiceNo[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			//statusCd += GridInvoiceAndPaymentInquiry.cpStatusCd[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)] + ",";
			//inProgress = GridInvoiceAndPaymentInquiry.cpInProgress[index - (GridInvoiceAndPaymentInquiry.GetPageIndex() * GridInvoiceAndPaymentInquiry.cpPageSize)];
			l++;
		}
	}

	if (l > 1) {
		alert('Please Select Only One Invoice To Re Process');
		return;
	}

	//alert(statusCd);

	if (invoiceNos.length > 0) {
		$.ajax({
			type: "POST",
			url: getActMethodUrl("ReProcessInvoice"),
			async: false,
			data: {
				SupplierInvoiceNo: invoiceNos.split(',')[0],
				SuppCD: supplierCode.split(',')[0]
			},
			success: function (result) {
				alert(result);
				GridInvoiceAndPaymentInquiry.UnselectAllRowsOnPage();
				GridInvoiceAndPaymentInquiry.PerformCallback();
			},
			error: function (result) {
				alert("Ajax Error on Reprocess Data");
			}
		});
	} else {
		alert("Please Select One Invoice No To Re Process");
		return;
	}
}

function UploadEvidenceClick() {
	PPVEVFileUpload.Upload();
}

function UploadEvidenceInit(s, e) {}

function PPVEvidenceFileUploadComplete(s, e) {
	$.ajax({
		type: "POST",
		url: getActMethodUrl("GetUploadedTempPPVEvidenceFiles"),
		async: false,
		data: {
			supp_inv_no: txtSupplierInvoiceNo.GetText(),
			supp_cd: OIPSupplierGridLookup.GetText()
		},
		success: function (result) {
			$("#filename").html(result);
			$("#PPVEvidence").css("display", "inherit");
		},
		error: function (result) {}
	});
}

function DownloadTempPPV(Filepath) {
	$.fileDownload("InvoiceAndPaymentInquiry/DownloadPPVEvidence/", {
		data: {
			supp_inv_no: txtSupplierInvoiceNo.GetText(),
			supp_cd: OIPSupplierGridLookup.GetText(),
			filepath: $("#filename").html()
		},
		successCallback: function (url) {},
		failCallback: function (responseHtml, url) {
			alert(responseHtml);
		}
	});
}

function DeleteTempPPV() {
	$.ajax({
		type: "POST",
		url: getActMethodUrl("DeletePPVEvidence"),
		async: false,
		data: {
			supp_inv_no: txtSupplierInvoiceNo.GetText(),
			supp_cd: OIPSupplierGridLookup.GetText(),
			filepath: $("#filename").html()
		},
		success: function (result) {
			$("#filename").html('');
			$("#PPVEvidence").css("display", "none");
			alert(result);
		},
		error: function (result) {}
	});
}

function DownloadPPFEvidenceGrid(s, e) {
	var i = '';
	var supp_inv_no = '';
	var supp_cd = '';
	var filepath = '';

	i = s.name.substr(12);
	supp_inv_no = $('#GridInvoiceAndPaymentInquiry').find('#hl_' + i).filter(':first');
	supp_cd = $('#GridInvoiceAndPaymentInquiry').find('#OIPSupplierCodeLabel' + i).filter(':first');
	filepath = $('#GridInvoiceAndPaymentInquiry').find('#PPVFileEvidence' + i).filter(':first');

	$.fileDownload("InvoiceAndPaymentInquiry/DownloadPPVEvidence/", {
		data: {
			supp_inv_no: supp_inv_no.text(),
			supp_cd: supp_cd.text(),
			filepath: filepath.text()
		},
		successCallback: function (url) {},
		failCallback: function (responseHtml, url) {
			alert(responseHtml);
		}
	});
}
