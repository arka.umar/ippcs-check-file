var supplierCode = "";
var VPCNo = "";
var VSupplierCode = '';
var VEffectiveDate = '';
var VPCDate = '';
var VReleaseStatus = '';
var mode = "";
$(function() {
    $.getJSON("PriceConfirmation/CoverLetter", null, function(s) {
        console.log('CoverLetter: ', s);
        /// CoverLetterTemplate = s;
    });   

    $.getJSON("PriceConfirmation/CategoryList", null, function (s) {
        console.log('CategoryList ', s); 
        /// CategoryList = s;
    });
}); 
function getActMethodUrl(method, fun) {
    if (fun)
        return window.__base + fun + "/" + method;
    return window.__base + "PriceConfirmation/" + method;
}
function OnBeginCallbackGrid(s, e) {
    var a = {};
    a.PCNo = PCNoText.GetText();
    a.PCDateFrom = PCDate.GetText();
    a.PCDateTo = PCDateTo.GetText();
    a.SupplierCode = OIHSupplierOption.GetText();
    a.ReleaseStatus = ReleaseStatusCombo.GetValue();
    a.EffectiveDate = EffectiveDate.GetText();

    console.log("CallbackGrid");
    var iprop = 0;
    for (var p in a) {
        console.log(iprop++ , ' ' , p, ' :', a[p]);
        e.customArgs[p] = a[p];
    }   
}

function OnDownloadTemplateLinkClick(s, e) {
    var ParamSupplierCode = OIHSupplierOption.GetText();
    
    $.fileDownload(getActMethodUrl("Download"), {
        data: {
            SupplierCode: ParamSupplierCode,
            DockCode: ParamDockCode,
            PartNo: ParamPartNo
        },
        successCallback: function (url) {

        },
        failCallback: function (responseHtml, url) {
            $.msgBox({ content: responseHtml, type: "info", title: "Download" });
        }
    });
}



function btnClearClick(s, e) {
    PCDate.SetDate(null);
    PCDateTo.SetDate(null);
    ReleaseStatusCombo.SetValue("");
    PCNoText.SetValue("");
    EffectiveDate.SetText("");
    ReleaseStatusCombo.SetValue(null);
    OIHSupplierOption.SetValue();
    GridOIHSupplierOption.UnselectRows();
    GridDetail.UnselectRows();
    GridDetail.ClearFilter();
}

function btnSearchClick(s, e) {
    GridDetail.ClearFilter();
}

function BeginCallbackAddNew(s, e) {
    var iPCNo = InPCNo.GetValue();
    var iPCDate = InPCDate.GetText();
    var iCategory = InCategory.GetText(); 
    var iSupplierCode = InSupplierLookup.GetValue();
    var iEffectiveDate = InEffectiveDate.GetText();
   
    var iCover = InCoverLetter.GetValue(); 
    var iCategory = InCategory.GetValue();

    var emptyfields = []; 
    if (!iPCNo || iPCNo === "") emptyfields.push("PC No");
    if (!iPCDate || iPCDate === "") emptyfields.push("PC Date");
    if (!iSupplierCode || iSupplierCode === "") emptyfields.push("Supplier Code");
    if (!iEffectiveDate || iEffectiveDate === "") emptyfields.push("Effectve Date");
    if (!iCategory || iCategory === "") emptyfields.push("Category");
    if (!iCover || iCover === "") emptyfields.push("Cover Letter");

    if (emptyfields.length > 0) {
        $.msgBox({ content: Msg("MPCS01015ERR", emptyfields.join(", ")), type: "error", title: "Effective Date Checking" });
        
        return;
    }

    //FID.Ridwan: 2021-01-15 --> Add valdation if effective date <> current month + 1 than error
    // remark dulu as request ISTD.Yanes
    //var tday = new Date();
    //var curdate = ('0' + (tday.getMonth() + 1)).slice(-2);
    //var curryear = tday.getFullYear();
    //var curryeardt = curryear + "" + curdate;
    //var effDt = iEffectiveDate.substring(3, 5);
    //var effYr = iEffectiveDate.substring(6, 10);
    //var effYrDt = effYr + "" + effDt;

    //if ((effYrDt == curryeardt) || (effYrDt < curryeardt)) {
    //    $.msgBox({ content: "Effective date must be current month + 1.", type: "error", title: "Mandatory" });
        
    //    return;
    //}

    $.ajax(
    {
        type: "POST",
        url: "PriceConfirmation/Save",
        data:
        {
            PCNo: '',
            PCDate: iPCDate,
            Category: iCategory,
            SupplierCode: iSupplierCode,
            EffectiveDate: iEffectiveDate,
            CoverLetterBody: iCover,
            PrintFlag: 'N'
        },
        success: function (result) {
            CancelAddNew()
            if (result == "SUCCESS") {

                GridDetail.PerformCallback();
                $.msgBox({ content: "Saving Data Successfully", type: "info", title: "Save" });
            }
            else {
                $.msgBox({ content: result, title: "Save" });
            }
        },
        error: function (result) {
            $.msgBox({ content: "Error when update data", type: "Error", title: "Save" });
        }
    });
}
    
function CancelAddNew(s, e) {
    InPCNo.SetText("");
    InPCDate.SetText("");
    InCategory.SetText("");
    InSupplierLookup.SetText(""); 
    InEffectiveDate.SetText("");
    InCoverLetter.SetText("");

    GridInSupplierLookup.UnselectRows();

    popupNew.Hide();    
}

function btnSubmitClick(s, e) {
    var selected = GridDetail.GetSelectedRowCount();
    if (selected <= 0) {
        $.msgBox({ content: Msg("MPCS01011ERR"), type: 'info' })
        return;
    }
    var k = GridDetail.GetSelectedKeysOnPage();
    var pcNos = k.join(';');
    console.log('Submit: ', pcNos);
    $.ajax(
    {
        type: "POST",
        url: "PriceConfirmation/Submit",
        data:
        {
            PCNos: pcNos
        },
        success: function (result) {
            CancelAddNew()
            if (result == "SUCCESS") {
                GridDetail.UnselectFilteredRows();
                GridDetail.PerformCallback();
                $.msgBox({ content: "Submitted Successfully", type: "info", title: "Submit" });
            }
            else {
                console.log('Submit failed: ', result);
                $.msgBox({ content: result, title: "Save" });
            }
        },
        error: function (result) {
            $.msgBox({ content: "Error when Submit", type: "Error", title: "Submit" });
        }
    });
}

function btnAddToPCClick(s, e) {
    var selected = GridDetail.GetSelectedRowCount();
    if (selected <= 0) {
        $.msgBox({ content: Msg("MPCS01011ERR"), type: 'info' })
        return;
    } else if (selected > 1) {
        $.msgBox({ content: Msg("MPCS01024ERR"), type: 'info' });
        GridDetail.UnselectRows();
        GridDetail.UnselectFilteredRows();
        return;
    }
    var k = GridDetail.GetSelectedKeysOnPage()[0];
    $('#PCDetailForm>#fiMode').val('edit');
    $('#PCDetailForm>#fiPCNo').val(k);
    $('#PCDetailForm').submit(); 
}

function btnAddClick(s, e) {
    mode = "ADD";
    
    InPCNo.SetText("");
    InPCDate.SetText("");
    InCategory.SetText("");
    InSupplierLookup.SetText(""); 
    InEffectiveDate.SetText("");
    InCoverLetter.SetText("");

    InPCNo.SetEnabled(true); 
    InPCDate.SetEnabled(true);
    InCategory.SetEnabled(true); 
    InSupplierLookup.SetEnabled(true);
    InEffectiveDate.SetEnabled(true);
    InCoverLetter.SetEnabled(true); 

    popupNew.Show();
}

function btnDeleteClick(s, e) {
    var  selected = GridDetail.GetSelectedRowCount();
    if (selected <= 0) {
        $.msgBox({ content: Msg("MPCS01011ERR"), type: 'info' })
        return;
    } else if (selected > 1) {
        $.msgBox({ content: Msg("MPCS01012ERR"), type: 'info' });
        return; 
    }
    var k = GridDetail.GetSelectedKeysOnPage()[0];
    $.msgBox({ 
          content: Msg("MPCS01013CON") 
        , type: "confirm"
        , title: ""
        , buttons: [{ value: "Ok" }, { value: "Cancel"}],
        success: function (r) {
            if (r !== "Ok") return;
            $.ajax({
                type: "POST",
                url: "PriceConfirmation/Delete",
                data:
                    {
                        PCNo: k
                    },
                success: function (result) {
                    //GridDEtail.UnselectFilteredRows();
                    //GridDetail.PerformCallback();
                    btnSearchClick();
                    $.msgBox({ content: result, title: "Delete" });
                },
                error: function (result) {
                    $.msgBox({ content: "Error when update data", type: "error", title: "Delete" });
                }
            });
        }
    });
}

function btnDetailClick(s, e) {
    var selected = GridDetail.GetSelectedRowCount();
    if (selected <= 0) {
        $.msgBox({ content: Msg("MPCS01011ERR"), type: 'info' })
        return;
    } else if (selected > 1) {
        $.msgBox({ content: Msg("MPCS01024ERR"), type: 'info' });
        GridDetail.UnselectRows();
        return;
    }    
    var k = GridDetail.GetSelectedKeysOnPage()[0];
    $('#PCDetailForm>#fiMode').val('view');
    $('#PCDetailForm>#fiPCNo').val(k);
    $('#PCDetailForm').submit(); 
}

function InPCDateChanged(s, e) {
    console.log('InPCDateChanged: ', s);
    $.ajax({
        type: "GET", 
        url: "PriceConfirmation/NextPCNo",
        data: {
            PCDate: s.GetText()
        }, 
        success: function(r) {
            InPCNo.SetText(r);
        }, 
        error: function(r) {    
            InPCNo.SetText('');
        }
    });
}

function GetCategoryText() {
    var t = "";
    var v = InCategory.GetValue(); 
    for (var ica = 0; ica < window.CategoryList.length; ica++) {
        if (window.CategoryList[ica][0] === v) {
            t = window.CategoryList[ica][1];
            break;
        }
    }
    return t;
}

function GetEffectiveDate() {
    return $.format.date(InEffectiveDate.GetValue(), "dd MMMM yyyy");
}

function UpdateCoverLetter() {
    var coverlettertext = decodeURIComponent(window.CoverLetterTemplate);
    coverlettertext = coverlettertext.replace("{EffectiveDate}", GetEffectiveDate()).replace("{category}", GetCategoryText()).replace(/\|\|/g, "\r\n");
    coverlettertext = $('<textarea />').html(coverlettertext).text();
    InCoverLetter.SetValue(coverlettertext);  
}

function InEffectiveDateChanged(s, e) {
    /// change  cover letter value
    UpdateCoverLetter();
}

function InCategoryChanged(s, e) {
    UpdateCoverLetter();
}

function OnCoverLetterClick(s, e) {
    var pcno = s.mainElement.getAttribute('vi');
    var released = s.mainElement.getAttribute('released');
    if (released !== 'Y') {
        alert("PC not released yet");
        return;
    }
    $.fileDownload(getActMethodUrl("DownloadCover", "PCDownload"), {
        data: {
            PcNo: pcno
        },
        successCallback: function (url) {
        },
        failCallback: function (responseHtml, url) {
            alert(responseHtml);
        }
    });
    
}

function OnFullPcClick(s, e) {
    var pcno = s.mainElement.getAttribute('vi');
    var released = s.mainElement.getAttribute('released');
    if (released !== 'Y') {
        alert("PC not released yet");
        return;
    }
    
    $.fileDownload(getActMethodUrl("DownloadPdf", "PCDownload"), {
        data: {
            PcNo: pcno 
        },
        successCallback: function (url) {
        },
        failCallback: function (responseHtml, url) {
            alert(responseHtml);
        }
    });

}

function OnEffectiveDateInit(s, e) {
    var calendar = s.GetCalendar();
    calendar.owner = s;
    calendar.SetVisible(false);
}

function OnEffectiveDateDropdown(s, e) {
    var calendar = s.GetCalendar();
    var fastNav = calendar.fastNavigation;
    fastNav.activeView = calendar.GetView(0, 0);
    fastNav.Prepare();
    fastNav.GetPopup().popupVerticalAlign = 'Below';
    fastNav.GetPopup().ShowAtElement(s.GetMainElement())

    fastNav.OnOkClick = function () {
        var parentDateEdit = this.calendar.owner;
        var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
        parentDateEdit.SetDate(currentDate);
        parentDateEdit.HideDropDown();
    }

    fastNav.OnCancelClick = function () {
        var parentDateEdit = this.calendar.owner;
        parentDateEdit.HideDropDown();
    }
}

