/// moved from Views/InvoiceCreation/Index.cshtml\
var TaxCodeFix;
var TaxRate;

function getActMethodUrl(method, fun) {
    if (fun)
        return window.__base + fun + "/" + method;
    return window.__base + "InvoiceCreation/" + method;
}

function OnGoodsReceiptInquiryPopupBeginCallback(s, e) {

    e.customArgs["DockCode"] = dockCode;
    e.customArgs["RcvPlantCode"] = rcvPlantCode;
    e.customArgs["ManifestNo"] = manifestNo;

}

function OnGoodsReceiptInquiryPopupClosing(s, e) {
    GRIDGridView.PerformCallback();
}
function DisableButton() {
    ClearButton.SetEnabled(false);
    btnRefresh.SetEnabled(false);
    btnDownload.SetEnabled(false);
    PreviewInvoice.SetEnabled(false);
    CancelUploadedInvoiceButton.SetEnabled(false);
    UploadButton.SetEnabled(false);
    SearchData.SetEnabled(false);
}

function EnableButton() {
    ClearButton.SetEnabled(true);
    btnRefresh.SetEnabled(true);
    btnDownload.SetEnabled(true);
    PreviewInvoice.SetEnabled(true);
    CancelUploadedInvoiceButton.SetEnabled(true);
    UploadButton.SetEnabled(true);
    SearchData.SetEnabled(true);
}

function OnHyperLinkClick(manifestNo) {
    currentManifestNo = manifestNo;
    ICDetailGrid.PerformCallback();
    popupDetail.SetHeaderText("Manifest No: " + currentManifestNo);
}
function OnDetailGridBeginCallback(s, e) {
    e.customArgs["ManifestNo"] = currentManifestNo;
}

function OnDateChanged(s, e) {
    var myname = s.name;
    var mydate = s.date;
    if (!mydate) {
        return;
    }
    var yourname = "";
    var isFrom = myname.lastIndexOf("From");
    if (isFrom > 0) {
        yourname = myname.substr(0, isFrom) + "To";
    }
    else {
        yourname = myname.substr(0, myname.length - 2) + "From";
    }

    var oPair = eval(yourname);
    var yourdate = oPair.GetDate();

    if (yourdate == null) {
        oPair.SetDate(mydate);
        return;
    }
    if ((isFrom > 0 && mydate > yourdate)
         || (mydate < yourdate && isFrom < 0)) {
        oPair.SetDate(mydate);
        s.SetDate(mydate);
    }
}


function OnPreviewInvoiceCallback() {
    $.ajax({
        type: "POST",
        url: getActMethodUrl('PreviewInvoiceDocument'),

        data:
        {
            supplierCode: OIHSupplierOption.GetText(),
            manifests: Manifests,
            dateFrom: PeriodFrom.GetText(),
            dateTo: PeriodTo.GetText(),
            showOnlyUploaded: UploadedOnlyCheck.GetChecked(),
            invoicenos: invoices,
            documentDate: DocumentDate
        },
        success: function (result) {
            popupLoadingData.Hide();

            GridInvoiceTaxNo.MoveColumn(0);
            GridInvoiceTaxNo.UnselectFilteredRows();
            GridInvoiceTaxNo.UnselectAllRowsOnPage();


            //Add By Arka.Taufik 2022-03-22
            GridTaxCodeCombo.MoveColumn(0);

            if (result.toUpperCase() === "OK") {
                popupInvoicePreview.Show();
                TaxNoMandat();
                InvoiceListGridView.UnselectAllRowsOnPage();
                InvoiceListGridView.PerformCallback();
            }
            else {
                if (result.indexOf("in progress") > 0) {
                    $('#UploadProgressDiv').show();
                } else {
                    $('#UploadProgressDiv').hide();
                }

                alert(result);

                if (result == 'Invoice Will Be Paid Based on Last GR & Invoice Submission Date') {
                    popupInvoicePreview.Show();
                    TaxNoMandat();
                    InvoiceListGridView.UnselectAllRowsOnPage();
                    InvoiceListGridView.PerformCallback();
                }
            }
            EnableButton();
        },
        error: function (result) {
            popupProgress.Hide();
            alert("Error trying Preview Invoice Document");
            EnableButton();
        }

    });

}

function OnRowClickInvoiceNo(s, e) {

    var Manifest = txtManifestNo.GetText();
    var SelectionValue = "";

    if(e.visibleIndex == null) SelectionValue = InvoiceListGridView.cpInvoiceNumber[0];
    else SelectionValue = s.GetRowKey(e.visibleIndex);

    if(SelectionValue == null)
    {
        Manifest = Manifests;
        SelectionValue = "";
    }
    selectedInvoice = SelectionValue;

    popupLoadingData.Show();

    GridInvoiceTaxNo.MoveColumn(0);
    GridInvoiceTaxNo.UnselectRows();


    GridTaxCodeCombo.MoveColumn(0);

    $.ajax({
        type: "POST",
        url: getActMethodUrl('PopupGridInvoiceDetail'),
        async: false,
        data: {
            Manifest: Manifest,
            InvoiceNumber: SelectionValue,
            paramSupplierCode: OIHSupplierOption.GetText(),
            paramdockCode: (SelectionValue != "" ? "" : OIHDockOption.GetText()),
            paramorderNo: (SelectionValue != "" ? "" : txtOrderNo.GetText()),
            paramdfrom: (SelectionValue != "" ? "" : PeriodFrom.GetText()),
            paramdto: (SelectionValue != "" ? "" : PeriodTo.GetText()),
            paramonlyUploaded: (SelectionValue != "" ? false : UploadedOnlyCheck.GetChecked())
        },
        datatype: "json",
        traditional: true,
        success: function (result) {
            $.each(result, function (index, item) {
                if (item) {

                    if (item.InvoiceNo == "" || item.InvoiceNo == null) InvoiceNo.SetEnabled(true);
                    else InvoiceNo.SetEnabled(false);

                    if (item.InvoiceDate == "" || item.InvoiceDate == null) { deInvoiceDate.SetEnabled(true); }

                    if (item.InvoiceTaxNo == "" || item.InvoiceTaxNo == null) { InvoiceTaxNo.SetEnabled(true); }

                    if(item.InvoiceTaxDate == "" || item.InvoiceTaxDate == null) InvoiceTaxDate.SetEnabled(false);

                    InvoiceNo.SetText(item.InvoiceNo);
                    deInvoiceDate.SetText(item.InvoiceDate);

                    TaxAmount.SetValue("");
                    InvoiceTaxDate.SetValue(null);
                    TurnOver.SetText(String(item.TurnOver));
                    TotalManifest.SetText(String(item.TotalManifest));
                    TotalInvoiceAmount.SetText(String(item.TotalAmount));
                    InvoiceTaxNo.SetText(item.InvoiceTaxNo);

                    InvoiceTaxAmount.SetText(item.TurnOver * item.RetroTaxRate);
                    RetroTaxRate.SetText(item.RetroTaxRate * 100);
                    RetroAmount.SetText(item.RetroAmount);

                    //Add By Arka.Taufik 2022-03-22
                    TaxCodeCombo.SetValue(item.InvoiceTaxCd);
                    GridTaxCodeCombo.SelectRowsByKey(item.InvoiceTaxCd)
                    //Add By Arka.Taufik 2022-04-10
                    TaxCodeFix = item.InvoiceTaxCd;
                    TaxRate = item.RetroTaxRate * 100

                    RetroAmount.SetClientVisible(true);
                    $("#RetroLabel").show();
                    $("#LblCommand").show();

                    $("#LblCommand").html(item.CommandText);
                    icDocDate = item.DocDateString;
                    icCurrency = item.Currency;
                    icStampAmount = item.StampAmount;
                    icRetroAmount = item.RetroAmount;
                    icTotalAmount = item.TotalAmount;
                    icRetroDocNo = item.RetroDocNo;
                    icRetroRemaining = item.RetroRemaining;
                    InvoiceManifests = Manifest;

                    sumInTot();
                }
            });

            //
            $.ajax({
                type: "POST",
                url: getActMethodUrl('GetToleranceVal'), data: { },
                success: function (result) {
                    ToleranceValue.SetText(result);
                },
                error: function (result) { }
            });
            //

            GetRetroDisabledDate();
            popupLoadingData.Hide();
        },
        error: function (result) {
            alert("Data not exist");
        }
    });
}

function OnPreviewInvoiceClick(s, e) {

    DisableButton();

    if (   !ValidateSearchCriteria()
        || !ValidateOnlyOneSupplier()
        || !ValidateSinglePO()) {
        EnableButton();
        return;
    }

    Manifests = "";
    invoices = "";
    DocumentDate = "";

    Manifests = ListManifest.toString().replace(/,/g, ";");

    if(Manifests == "") {
        Manifests = null;
    }

    DocumentDate = ListDocumentDate.toString().replace(/,/g, ";");

    if (DocumentDate == "") {
        DocumentDate = null;
    }

    GridInvoiceTaxNo.MoveColumn(0);
    GridTaxCodeCombo.MoveColumn(0);
    OnPreviewInvoiceCallback();
}

function OnDetailGridEndCallback(s, e) {
    if (!popupDetail.IsVisible())
        popupDetail.Show();
}

function OnPopupPreviewBeginCallback(s, e) {
    PutArgs(e);
}


function RefreshPopUp(s, e) {
    var row = s.GetFocusedRowIndex ();

    if (row>=0) {
            s.GetRowValues(row, 'InvoiceNo;Status;InvoiceDate;InvoiceTaxDate;' +
                'InvoiceTaxNo;StampIncluded;TurnOver;InvoiceTax;RetroAmount;' +
                'TotalAmount;TotalManifest;Currency;Manifests;' +
                'RetroDocNo;RetroRemaining;TakenRate;StampAmount;TaxCodeCombo'
            , OnPopupGetRowValues);
    }
}

function OnPopupPreviewFocusedRowChanged(s, e) {
        RefreshPopUp(s, e);
}

function OnGetRowValues(values) {
    var a=values;
}

function OnPopupPreviewRowClick(s, e) {
}

function OnPopupPreviewEndCallback(s, e) {
    var r = s.GetVisibleRowsOnPage();

    if (r == 1) {
        var i = s.GetFocusedRowIndex();
        if (i < 0) {
            s.SetFocusedRowIndex(0);
        }
        else
            s.SetFocusedRowIndex(-1);
    }

    if (r == 0) {
        alert("There are no Invoice Status on Invoice List Grid, something is wrong");

        // popupInvoicePreview.Hide(); // no hiding - fid.ambar issue 0319 2020-09-14
    }
    else OnRowClickInvoiceNo(s, e);

}

function selManiTot(selMani, tot) {
    if (isNaN(selMani)) selMani = 0;

    $("#totalManifest").html(parseFloat(selMani));

    if (!isNaN(tot))
    {
        if ((tot-Math.floor(tot)) != 0) {
            var tot1= Math.floor(tot);
            var tot2 = (tot-tot1).toString().substr(2,2);
            $("#totalAmountStores").html((tot1 + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + '.' + tot2);
        }
        else {
            $("#totalAmountStores").html((tot + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
        }
    } else {
        $("#totalAmountStores").html('<!--NaN-->');
    }
}


function sumInTot() {
    var stamped = 0;
    var StampedAmount = 0;
    if (StampIncluded.GetChecked())
        stamped = 1;
    var aTurnOver = parseFloat(TurnOver.GetText());
    var aRetroAmount = parseFloat(RetroAmount.GetText());
    var aInvoiceTaxAmount = parseFloat(InvoiceTaxAmount.GetText());
    var InvAmount = aTurnOver + aRetroAmount + aInvoiceTaxAmount;

    //FID.Ridwan: 20210125 
    var aPPh23Amount = parseFloat(PPh23Amount.GetText());

    if (icCurrency == "IDR") {
        if (stamped == 1 ){
            StampedAmount = parseFloat(InvAmount) + parseFloat(icStampAmount) - (aPPh23Amount = aPPh23Amount ? aPPh23Amount : 0);
            TotalInvoiceAmount.SetText(StampedAmount);
        }
        else {
            TotalInvoiceAmount.SetText(parseFloat(InvAmount) - (aPPh23Amount = aPPh23Amount ? aPPh23Amount : 0));
        }
    }
}

function OnPopupGetRowValues(values) {
    /*
    0   InvoiceNo
    1   Status
    2   InvoiceDate
    3   InvoiceTaxDate
    4   InvoiceTaxNo
    5   StampIncluded
    6   TurnOver
    7   InvoiceTax
    8   RetroAmount
    9   TotalAmount
    10  TotalManifest
    11  Currency
    12  Manifests
    13  RetroDocNo
    14  RetroRemaining
    15  TakenRate
    16  StampAmount
    */

    var st = "";
    for(var i = 0; i < values.length; i++) {
        st = st  + "[" + i + "]: " + values[i] + "  ";
    }

    var takenRate = values[15];
    icCurrency = values[11];


}

function OnInvoiceListGridViewRowClick(s, e) {
    s.SelectRow();

}

function PutArgs(e) {
    DisableButton();

    if(CheckedManifest == "1" && invoices != "")
    {
        e.customArgs["manifests"] = Manifests;
        e.customArgs["invoices"] = invoices;
        e.customArgs["SupplierCode"] = OIHSupplierOption.GetText();
        e.customArgs["DockCode"] = null;
        e.customArgs["ManifestNo"] = null;
        e.customArgs["OrderNo"] = null;
        e.customArgs["DateFrom"] = null;
        e.customArgs["DateTo"] = null;
        e.customArgs["UploadedOnly"] = 0;
        CheckedManifest = "0";
    }
    else{
        e.customArgs["manifests"] = Manifests;
        e.customArgs["invoices"] = invoices;
        e.customArgs["SupplierCode"] = OIHSupplierOption.GetText();
        e.customArgs["DockCode"] = OIHDockOption.GetText();
        e.customArgs["ManifestNo"] = txtManifestNo.GetText();
        e.customArgs["OrderNo"] = txtOrderNo.GetText();
        e.customArgs["DateFrom"] = PeriodFrom.GetText();
        e.customArgs["DateTo"] = PeriodTo.GetText();
        e.customArgs["UploadedOnly"] = UploadedOnlyCheck.GetChecked();
    }
    EnableButton();
}

function OnGridDataBeginCallback(s, e) {
    var sel = s.GetSelectedKeysOnPage();
    var m = "";
    for(var i = 0; i<sel.length; i++) {
        var se = sel[i];
        if (i>0) se = ";" + se;
        m += se;
    }
    s['cpPONO'] = null;
    Manifests = m;
    PutArgs(e);
    EnableButton();
}

function OnGridDataEndCallback(s, e) {
    if (s.length == 0)
        return;

        var CountSelected = grdData.cpVisibleRowCount;
        var TotalAmount = grdData.cpSum;

        selManiTot(CountSelected, TotalAmount);
}

function MonthsBetween(d1, d2) {
    var months;
    months = (d1.getFullYear() - d2.getFullYear()) * 12;
    months -= d2.getMonth() + 1;
    months += d1.getMonth() + 1;
    return months;
}

function DaysBetween(d1, d2) {
    var d1ms = d1.getTime();
    var d2ms = d2.getTime();
    var dd = (d1ms - d2ms) / (1000*60*60*24);

    return dd;
}

function dmy(dx)  {
    var ax = dx.split(/[\.\-\/]/);
    var d = new Array();

    d.yyyy = Number(ax[2]);
    d.mm = Number(ax[1]);
    d.dd = Number(ax[0]);

    if ((ax.length > 2) &&
        (d.yyyy != NaN && d.mm != NaN && d.dd != NaN)
        && d.yyyy >= 0 && d.yyyy < 9999
        && d.mm > 0 && d.mm < 13
        && d.dd > 0 && d.dd < 32
        )
        return new Date(d.yyyy, d.mm-1, d.dd);
    else
        return null;
}

function OnPopupPreviewSelectionChanged(s, e) {
    var ino = s.GetSelectedFieldValues("InvoiceNo", OnGetRowValues);
}



function SelectionRowChanged(s, e) {
    var TotalAmount = 0;
    var CountSelected = grdData.GetSelectedRowCount();

    if (CountSelected == 0){

        CountSelected = s.cpVisibleRowCount;
        TotalAmount = s.cpSum;
    }
    else{
        //count if any selected grid
        for (var index = grdData.GetTopVisibleIndex(); index < ((grdData.GetPageIndex() * grdData.cpPageSize) + grdData.GetVisibleRowsOnPage()); index++) {
            if (grdData.IsRowSelectedOnPage(index))
            {
                if (TotalAmount == "")
                    TotalAmount = 0;
                TotalAmount = parseFloat(TotalAmount) + parseFloat(grdData.cpAmount[index - (grdData.GetPageIndex() * grdData.cpPageSize)]);
            }
        }
    }

    //alert(CountSelected);

    selManiTot(CountSelected, TotalAmount);

}


    function GetSelectedFieldValuesCallback(values) {

}

/*CheckDockCode*/
function UpdateSelectAllItemState() {
    IsAllSelected() ? checkListDockCode.SelectIndices([0]) : checkListDockCode.UnselectIndices([0]);
    IsAllSelected() ? checkListStatus.SelectIndices([0]) : checkListStatus.UnselectIndices([0]);
}
function IsAllSelected() {
    for (var i = 1; i < checkListDockCode.GetItemCount(); i++)
        if (!checkListDockCode.GetItem(i).selected)
            return false;
    return true;
}
function UpdateText() {
    var selectedItems = checkListDockCode.GetSelectedItems();
    cbDockCode.SetText(GetSelectedItemsText(selectedItems));
}
function SynchronizeListBoxValues(dropDown, args) {
    checkListDockCode.UnselectAll();
    var texts = dropDown.GetText().split(textSeparator);
    var values = GetValuesByTexts(texts);
    checkListDockCode.SelectValues(values);
    UpdateSelectAllItemState();
    UpdateText(); // for remove non-existing texts
}

function GetValuesByTexts(texts) {
    var actualValues = [];
    var item;
    for (var i = 0; i < texts.length; i++) {
        item = checkListDockCode.FindItemByText(texts[i]);
        if (item != null)
            actualValues.push(item.value);
    }
    return actualValues;
}

function OnGetManifestValue(v) {
    /*set manifest*/
    Manifests = ListManifest.toString().replace(/,/g, ";");

    $.fileDownload("InvoiceCreation/DownloadList/",{
        data :{
            supplierCode: OIHSupplierOption.GetText(),
            dockCode: OIHDockOption.GetText(),
            manifestNo: Manifests,
            orderNo: txtOrderNo.GetText(),
            periodFrom: PeriodFrom.GetText(),
            periodTo: PeriodTo.GetText(),
            uploadedOnly: UploadedOnlyCheck.GetChecked()
        },
        successCallback : function(url){
            popupProgress.Hide();
            EnableButton();
        },
        failCallback : function(responseHtml, url){
            popupProgress.Hide();
            EnableButton();
        }
    });
}

function OnGetManifestValueExcel(v) {
    /*set manifest*/
    Manifests = ListManifest.toString().replace(/,/g, ";");

    //$.fileDownload("InvoiceCreation/DownloadList/", {
    $.fileDownload("InvoiceCreation/DownloadListExcel/", {
        data: {
            supplierCode: OIHSupplierOption.GetText(),
            dockCode: OIHDockOption.GetText(),
            manifestNo: Manifests,
            orderNo: txtOrderNo.GetText(),
            periodFrom: PeriodFrom.GetText(),
            periodTo: PeriodTo.GetText(),
            uploadedOnly: UploadedOnlyCheck.GetChecked()
        },
        successCallback: function (url) {
            popupProgress.Hide();
            EnableButton();
        },
        failCallback: function (responseHtml, url) {
            popupProgress.Hide();
            EnableButton();
        }
    });
}
function btnDownloadClick()
{
    DisableButton();
    if (!ValidateSearchCriteria()) {
        EnableButton();
        return;
    }

    var mx = "";
    $("#DOMessageLabel").text("Download in progress,\nPlease wait...");
    popupProgress.Show();

    grdData.GetSelectedFieldValues("SupplierManifest", OnGetManifestValue);
}
function btnDownloadExcelClick() {
    DisableButton();
    if (!ValidateSearchCriteria()) {
        EnableButton();
        return;
    }

    var mx = "";
    $("#DOMessageLabel").text("Download in progress,\nPlease wait...");
    popupProgress.Show();

    grdData.GetSelectedFieldValues("SupplierManifest", OnGetManifestValueExcel);
}


function btnDownloadDetailDataClick()
{
    var mx = "";

    $("#DOMessageLabel").text("Download in progress,\nPlease wait...");
    popupProgress.Show();

    $.fileDownload("InvoiceCreation/DownloadInvoice/",{
        data :{
            supplierCode: OIHSupplierOption.GetText(),
            dockCode: OIHDockOption.GetText(),
            manifestNo: InvoiceManifests,
            orderNo: txtOrderNo.GetText(),
            periodFrom: PeriodFrom.GetText(),
            periodTo: PeriodTo.GetText(),
            uploadedOnly: UploadedOnlyCheck.GetChecked(),
            invoices : selectedInvoice
        },
        successCallback : function(url){
            popupProgress.Hide();
        },
        failCallback : function(responseHtml, url){
            popupProgress.Hide();
        }
    });
}

function btnSaveInvoiceDocumentClick(s, e) {
    icTaxNo = InvoiceTaxNo.GetText();
    icTaxAmount = InvoiceTaxAmount.GetText();
    icTurnOver = TurnOver.GetText();
    icTaxDt = InvoiceTaxDate.GetText();
    icToleranceVal = ToleranceValue.GetText();
    txByPassFlag = txBypassFlag.GetText();
    icWithholdingTaxAmt = PPh23Amount.GetText();
    icTransactionType = coTT.GetValue();

    //Add By Arka.Taufik 2022-03-22
    icTaxCodeCombo = TaxCodeCombo.GetValue();

    dexInvoiceDate = deInvoiceDate.GetValue()
    icRetroTaxRate = RetroTaxRate.GetValue()

    //Added by Ark.Taufik
    //aprlTaxRate = '0.11';
    //aprlTaxDate = new Date(2022, 03, 01);
    //if (icRetroTaxRate == aprlTaxRate){
    //    if (dexInvoiceDate < aprlTaxDate) {
    //        alert("Tax Rate 11% only valid from 01 April 2022");
    //        return;
    //    }
    //}

    //Added by FID.reggy
    //local variabel for selected E-Faktur Tax Amount
    var taxamount = TaxAmount.GetText();
    var invDate = deInvoiceDate.GetText();
    var iDateEmpty = (!invDate || (invDate.length < 1)) ? 1 : 0;
    var supp_cd = OIHSupplierOption.GetText();

    var isCoil = CoilSupplier.includes(";" + supp_cd + ";");

    //Added by Ark.Taufik : 2022-04-05
    if (icTaxCodeCombo == null || icTaxCodeCombo == "") { alert("Please fill Tax Code"); return; }
    //Added by FID.Yudia : 2022-04-10 -> Tax Code = V0 or I0 : not need fill Tax Invoice No
    var isTax0 = icTaxCodeCombo.includes("V0") || icTaxCodeCombo.includes("I0");

    if (InvoiceNo.GetText() == "") { alert("Please fill Invoice No"); return; }
    //Changed by FID.Yudia : 2022-04-10 -> Tax Code = V0 or I0 : not need fill Tax Invoice No
    if (InvoiceTaxNo.GetText() == "" && !isCoil && !isTax0) { alert("Please fill Invoice Tax No"); return; }
    //FID.Ridwan : 2021-03-03 -> transaction type cannot be null
    if (icTransactionType == null || icTransactionType == "") { alert("Please fill Transaction Type"); return; }

    if (icCurrency == "IDR")
    {
        if (Number(icTaxAmount) < 1 || icTaxNo.length < 1) {
            //Changed by FID.Yudia : 2022-04-10 -> Tax Code = V0 or I0 : not need fill Tax Invoice No
            if (!isCoil && !isTax0)
            {
                alert('Invoice Tax is mandatory for local currency (IDR)');
                return;
            }
        }

        if (!icTaxDt) {
            //Changed by FID.Yudia : 2022-04-10 -> Tax Code = V0 or I0 : not need fill Tax Invoice Date
            if (!isCoil && !isTax0) 
            {
                alert('Invoice Tax Date is mandatory for local currency (IDR)');
                return;
            }
        }
    }

    var range = 1;

    $.ajax(
        {
            type: "POST",
            url: getActMethodUrl('GetInvoiceDateRange'),
            data: {},
            async: false,
            success: function (result) {
                range = Number(result);
            },
            error: function (result) {

            }
        });
    //Changed by FID.Yudia : 2022-04-10 -> Tax Code = V0 or I0 : not need to check Invoice Tax Date  
    if (icDocDate && !isCoil && !isTax0) {
        if (MonthsBetween(dmy(icTaxDt), dmy(icDocDate)) < -range || MonthsBetween(dmy(icTaxDt), dmy(icDocDate)) > range) {
            alert('Invoice Tax is expired');
            return;
        }
    }
    //Changed by FID.Yudia : 2022-04-10 -> Tax Code = V0 or I0 : ByPass
    if (isCoil && isTax0 && (InvoiceTaxNo.GetText() == "")) {
        txByPassFlag = "Y"; // skip any tax validation when invoice NO is not mandatory
    }

    //Modified by fid.intan 2015-08-06
    if (txByPassFlag != "Y") {
        if (taxamount !== icTaxAmount) {
            if (Math.abs(taxamount - icTaxAmount) > icToleranceVal) {
                alert("Difference between Tax Amount and Invoice Tax Amount is more than tolerance amount. Please Select Another Tax No");
                return;
            }
            //alert('Tax Amount is Different with Invoice Tax Amount, Please Select Another Tax No');
            //return;
        }

        //FID.Ridwan : 20210107 --> add validation if by pass flag <> Y then check tax amt must be 10% of turn over amt
        if (Math.abs(taxamount - (icTurnOver * parseInt(icTaxCodeCombo))) > icToleranceVal) //(taxamount > ((icTurnOver * 10) / 100))
        {
            alert("Tax Amount must be " + parseInt(icTaxCodeCombo) + " % of Turn Over Amount. Please Select Another Tax No");
            return;
        }

    }

    if (iDateEmpty > 0)
    {
        alert('Invoice Date is mandatory');
        return;
    }

    if ((RetroAmount.GetText() != '0') && (CheckRetroDisabledDate() === 1)){
            return;
    }

    if(ASPxClientEdit.ValidateGroup('VG1') == true)
    {
        $("#DOMessageLabel").text("Saving Invoice Document ...");

        if (LockRef && LockRef.length > 1) {
            alert('Another process in progress, try again later');
            return;
        }

        LockRef = 1; // prevent doubleclick make double post

        popupProgress.Show();

        $.ajax({
            type: "POST",
            url: getActMethodUrl('SaveInvoiceDocument'),
            async: false,
            data:
            {
                invoiceNo: InvoiceNo.GetText(),
                manifests: InvoiceManifests,
                invoiceDate: deInvoiceDate.GetText(),
                invoiceTaxNo: InvoiceTaxNo.GetText(),
                invoiceTaxDate: InvoiceTaxDate.GetText(),
                stampFlag: StampIncluded.GetChecked(),
                stampAmount: icStampAmount,
                turnOver: icTurnOver,
                invoiceTaxAmt: parseInt(taxamount),
                totalManifest: TotalManifest.GetText(),
                retroDocNo: icRetroDocNo,
                invTotalAmt: icTotalAmount,
                dockCode: OIHDockOption.GetText(),
                orders: txtOrderNo.GetText(),
                dfrom: PeriodFrom.GetText(),
                dto: PeriodTo.GetText(),
                showUploaded: UploadedOnlyCheck.GetChecked(),
                retroAmount: icRetroAmount,
                retroRemaining: icRetroRemaining,
                withholdingTaxAmt: icWithholdingTaxAmt,
                transactionType: icTransactionType,
                isCoil: isCoil,
                supplierCode: OIHSupplierOption.GetText(),

                //Add By Arka.Taufik 2022-03-22
                TaxCode: icTaxCodeCombo,
                TaxRate: icRetroTaxRate
            },
            success: function (result) {
                popupProgress.Hide();
                alert(result);

                InvoiceTaxNo.SetValue("");
                InvoiceTaxDate.SetValue(null);
                TaxAmount.SetValue("");
                GridInvoiceTaxNo.MoveColumn(0);

                //Add By Arka.Taufik 2022-03-22
                TaxCodeCombo.SetValue("");
                GridTaxCodeCombo.MoveColumn(0);

                ListKeys = new Array();
                ListManifest = new Array();
                ListTotalAmount = new Array();
                ListInvoice = new Array();
                ListDocumentDate = new Array();

                if (result && result.indexOf("in progress") > 0)
                {
                    $('#UploadProgressDiv').show();
                    checkLock();
                    popupInvoicePreview.Hide();
                }
                else if (result && isCoil && result.indexOf("successfully") > 0) {
                    popupInvoicePreview.Hide();
                    alert('Coil supplier is not required to print Certificate');
                }
                else if (result && result.indexOf("successfully") > 0) {
                    var ino = InvoiceNo.GetText();
                    if(ino.length > 0)
                    {
                        $("#DOMessageLabel").text("Print Certificate ...");

                        $.ajax({
                            type: "POST",
                            url: getActMethodUrl('CheckBeforePrintCertificate'),
                            data:
                            {
                                InvoiceNos: ino,
                                InvoiceDates: deInvoiceDate.GetText(),
                                SupplierCodes: OIHSupplierOption.GetText()
                            },
                            success: function (resultmessage) {

                                if (resultmessage == "OK") {
                                    $.fileDownload(getActMethodUrl('PrintCertificate', "InvoiceAndPaymentInquiry"), {
                                        data: {
                                            InvoiceNos: ino,
                                            InvoiceDates: deInvoiceDate.GetText(),
                                            SupplierCodes: OIHSupplierOption.GetText()
                                        },
                                        successCallback: function (url) {
                                            popupProgress.Hide();
                                        },
                                        failCallback: function (responseHtml, url) {
                                            popupProgress.Hide();
                                            alert(responseHtml);
                                        }
                                    });
                                }
                                else
                                    alert(resultmessage);
                            },
                            error: function (resultmessage) {
                                alert('Print Certificate Error');
                            }
                        });

                        if(InvoiceListGridView.cpVisibleRowCount > 1)
                        {
                            InvoiceListGridView.PerformCallback();
                        }
                        else{
                            popupInvoicePreview.Hide();
                        }
                    }
                }
            },
            error: function (result) {
                popupProgress.Hide();
                alert("Error when Save Invoice Document");
            }
        });
    }
}

/*Supplier*/
function OnListBoxSelectionChanged(listBox, args) {
    if (args.index == 0)
        args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
    UpdateSelectAllItemState();
    UpdateText();
}

function GetSelectedItemsText(items) {
    var texts = [];
    for (var i = 0; i < items.length; i++)
        if (items[i].index != 0)
            texts.push(items[i].text);
    return texts.join(textSeparator);
}

function UploadInProgressCheck(fnOk, fnLock, fnErr) {
    $.ajax({
        type: "POST",
        url: getActMethodUrl('GetUploadInProgress'),
        data: {
            supplierCode: OIHSupplierOption.GetText()
        },
        success: function(result) {
            LockRef = result;

            if (LockRef.length >= 1) {
                if (LockRef.indexOf("C") >= 0) {
                    InProgressText(1);
                }
                else  {
                    InProgressText(0);
                }

                if (fnLock)
                    fnLock();
                $('#UploadProgressDiv').show();
            } else {
                if (fnOk)
                    fnOk();
                $('#UploadProgressDiv').hide();
            }
            EnableButton();
        },
        fail: function(result) {
            if (fnErr)
                fnErr();
            EnableButton();
        }
    });
}

function InProgressText(fMode) {
    var x = " "
    switch(fMode) {
        case 0:
            x = "Verification";
            break;
        case 1:
            x = "Cancellation";
            break;
        defaut:
            x = "Process";
            break;
    }
    $('#ProgressDivText').html(x  + " in progress");
}

function ValidateSearchCriteria(silent) {
    var doalert = 1;
    if (silent)
        doalert = 0;
    var d1 = dmy(PeriodFrom.GetText());
    var d2 = dmy(PeriodTo.GetText());
    if (!d1 || !d2) {
        if (doalert) alert("Document Date is not valid");
        return false;
    }

    if (d1-d2 > 0)
    {
        if (doalert) alert("Document Date To cannot be smaller than Document Date From");
        return false;
    }

    var supp= OIHSupplierOption.GetText();
    if ((!supp) || (supp.length <1)){
        if (doalert) {
            alert('Please Select Supplier')
            EnableButton();
        };
        return false;
    }


    return true;
}

function ValidateOnlyOneSupplier(silent) {
    var doalert = 1;
    if (silent)
        doalert = 0;

    var supp= OIHSupplierOption.GetText();
    if (supp)
    {
        if (supp.indexOf(";") > 1) {
            if (doalert) {
                alert('Select only one supplier');
                EnableButton();
            }
            return false;
        }
    }

    return true;
}
function btnReportSummaryClick()
{
    popupInvoiceReport.Hide();
    $("#DOMessageLabel").text("Download in progress,\nPlease wait...");
    popupProgress.Show();
    $.fileDownload("InvoiceCreation/DownloadGR_IR_Summary/", {
        data: {
            DocDate: dtmOutstandingAsOf.GetText()
        },
        successCallback: function (url) {
            popupProgress.Hide();
            EnableButton();
        },
        failCallback: function (responseHtml, url) {
            popupProgress.Hide();
            EnableButton();
        }
    });
    popupProgress.Hide();
}
function ValidateSinglePO(s) {
    var poNos = grdData['cpPONO'];
    var isNewInvoice = false;
    var retPOval = true;

    //FID.Ridwan : 20210414
    for (var index = grdData.GetTopVisibleIndex() ; index < ((grdData.GetPageIndex() * grdData.cpPageSize) + grdData.GetVisibleRowsOnPage()) ; index++) {
        if (grdData.IsRowSelectedOnPage(index)) {
            if (Number(grdData.cpProdMonth[index - (grdData.GetPageIndex() * grdData.cpPageSize)]) == 202104 || Number(grdData.cpProdMonth[index - (grdData.GetPageIndex() * grdData.cpPageSize)]) > 202104) {
                isNewInvoice = true;
            }
        }
    }

    if (!(poNos)) {
        alert('Select Invoice to preview');
        return false;
    }

    if (poNos.length > 1 && isNewInvoice) {
        alert('Cannot process invoice with more than 1 PO NO');

        retPOval = false;
    }
    //return (poNos.length===1);
    return (retPOval);
}

function DoSearch(silent) {
    grdData['cpPONO'] = null;
    if (!ValidateSearchCriteria(silent)) return;

    grdData.UnselectAllRowsOnPage();
    UploadInProgressCheck(null, null, null);

    //set manifest to default
    ListManifest = new Array();
    ListTotalAmount = new Array();
    ListInvoice = new Array();
    ListKeys = new Array();
    ListDocumentDate = new Array();
    grdData.GotoPage(0);
}

function SearchDataClick(s, e) {
    DisableButton();
    DoSearch();
}

function DoUpload() {
    LivUpload.Upload();
    InProgressText(0);
    $('#UploadProgressDiv').show();
}
function DoUpload_Excel() {
    LivUpload_Excel.Upload();
    InProgressText(0);
    $('#UploadProgressDiv').show();
}

function DoErr() {
    alert("Another Process in progress, try again later");
}

function UploadButtonClick(s, e) {
    if (!ValidateSearchCriteria() || !ValidateOnlyOneSupplier())
        return;
    var ih = document.getElementById("ihLIVUSuppCd");
    if (ih) {
        ih.value = OIHSupplierOption.GetText();
    }
    $.ajax({
        type: "POST",
        url: getActMethodUrl('CheckUploadProgress'),
        data:
        {
            supplierCode: OIHSupplierOption.GetText()
        },
        success: function (result) {
            if (result.length > 0) {
                alert(result);
                return;
            }
            else {
                DisableButton();
                UploadInProgressCheck(DoUpload, DoErr, function () { });
                InProgressText(0);
                $('#UploadProgressDiv').show();
            }
        },
        error: function (result) {
            console.log(result);
        }
    });
}
function UploadButtonClick_Excel(s, e) {

    if (!ValidateSearchCriteria() || !ValidateOnlyOneSupplier())
    {
        return;
    }

    $("#DOMessageLabel").text("Upload in progress,\nPlease wait...");
    popupProgress.Show();
        $.ajax({
            type: "POST",
            url: getActMethodUrl('CheckUploadProgress'),
            data:
        {
            supplierCode: OIHSupplierOption.GetText()
        },
        success: function (result) {
            if (result.length > 0) {
                alert(result);
                popupProgress.Hide();
                return;
            }
            else {
                DisableButton();
                UploadInProgressCheck(DoUpload_Excel, DoErr, function () { });
                InProgressText(0);
                $('#UploadProgressDiv').show();
            }
        },
        error: function (result) {
            console.log(result);
            popupProgress.Hide();
        }
    });
}

function CancelOnUpload(s,e){
    DisableButton();
    $.ajax({type: "POST",
        url: getActMethodUrl('CancelOnUpload'),
        data: {
            supplier: OIHSupplierOption.GetText()
        },
        success: function(result) {
            if (result == "1"){
                alert("Upload already running");
            }
            else{
                CancelUploadedInvoiceButtonClick();
            }
            EnableButton();
            grdData.UnselectFilteredRows();
        },
        fail: function(result) {
            alert('Fail to Cancel when Invoice Uploaded');
            EnableButton();
        }
    });
}

function CancelUploadedInvoiceButtonClick(s, e) {
    $('#DOMessageLabel').html("Cancel Data in progress<br />Please wait...");

    /*set manifest and invoice no*/
    Manifests = ListManifest.toString().replace(/,/g, ";");
    Invoices = ListInvoice.toString().replace(/,/g, ";");

    if (!ValidateSearchCriteria()|| !ValidateOnlyOneSupplier())
        return;

    if (!confirm("Cancel Invoice Upload can't be undone.\r\n\r\n Are you sure?"))
        return;

    InProgressText(1);
    $('#UploadProgressDiv').show();

    popupProgress.Show();
    $.ajax({type: "POST",
        url: getActMethodUrl('CancelUpload'),
        data: {
            supplier: OIHSupplierOption.GetText(),
            docks: OIHDockOption.GetText(),
            manifests: Manifests,
            orderNo: txtOrderNo.GetText(),
            dateFrom: PeriodFrom.GetText(),
            dateTo: PeriodTo.GetText(),
            invoiceNo: Invoices
        },
        success: function(result) {
            popupProgress.Hide();

            if (!(result === "Ok"))
                alert('Cancel Invoice Upload '+ '\r\n\r\n' +  result);

            delayedSearch(1);
            grdData.ClearFilter();

            UploadInProgressCheck(null, null, null);

        },
        fail: function(result) {
            alert('Fail to Cancel Invoice Upload');
            $('#UploadProgressDiv').hide();
            popupProgress.Hide();
        }
    });
}

function LivUploadInit(s, e) {
    var fu = document.getElementById("formUploadLIV");
    if (fu) {
        var qE = fu.querySelectorAll("input#ihLIVUSuppCd");
        if (qE.length <= 0) {
            var ih = document.createElement("input");
            ih.type = "hidden";
            ih.name = "supplierCode";
            ih.id = "ihLIVUSuppCd";
            ih.value = OIHSupplierOption.GetText();
            fu.appendChild(ih);
        } else {
            qE[0].value = OIHSupplierOption.GetText();
        }
    }
    $('#DownloadErrorDiv').hide();
}

function LivExcelUploadInit(s, e) {
    var fu = document.getElementById("formUploadLIVexcel");
    if (fu) {
        var qE = fu.querySelectorAll("input#ihSuppCd");
        if (qE.length <= 0) {
            var ih = document.createElement("input");
            ih.type = "hidden";
            ih.name = "supplierCode";
            ih.id = "ihSuppCd";
            ih.value = OIHSupplierOption.GetText();
            fu.appendChild(ih);
        } else {
            qE[0].value = OIHSupplierOption.GetText();
        }
    }
    $('#DownloadErrorDiv').hide();
}

function ShowDownloadError() {
    var currentTime = $.now();
    var _frameId = "download" + currentTime;
    var _suppCode = OIHSupplierOption.GetText();

    $("#errorDownloadContainer").append("<iframe id='" + _frameId + "' src='" + getActMethodUrl('DownloadError') + "?supplierCode=" + _suppCode + "' style='visibility:collapse'></iframe>");
    var _theframe = document.getElementById(_frameId);
    _theframe.contentWindow.location.href = _theframe.src;

}

function LivUploadFileUploadComplete(s, e) {

    if (e && e.errorText && e.errorText.length>1)
    {
        $('#UploadProgressDiv').hide();
        alert(e.errorText);
        if (e.errorText == "Invalid data") {
            ShowDownloadError();
            popupProgress.Hide();
        }
        popupProgress.Hide();
        return;
    }
    checkLock();
    InProgressText(0);
    $('#UploadProgressDiv').show();
    EnableButton();
    popupProgress.Hide();
}

function FillInvoiceListbox() {
    $.ajax({type: "POST",
        url: getActMethodUrl('GetSelectedInvoices'),
        data: {
            manifests: Manifests,
            supplierCode: OIHSupplierOption.GetText(),
            dockCode: OIHDockOption.GetText(),
            manifestNo: txtManifestNo.GetText(),
            orderNo: txtOrderNo.GetText(),
            dateFrom: PeriodFrom.GetText(),
            dateTo: PeriodTo.GetText(),
            uploadedOnly: UploadedOnlyCheck.GetChecked()
            },
        success: function(result) {

        },
        dataType: "json"});
}


function TextValueChangedMergeNewLineToSemiColon(s, e) {
    var t = s.GetText();
    t = t.replace(/\s+/g, ';');
    s.SetText(t);
}

function txtManifestNoValueChanged(s, e) {
    var t = s.GetText();
    t = t.replace(/\s+/g, ';');
    s.SetText(t);
}

function txtOrderNoValueChanged(s, e) {

}

function GetLastMonth() {
    var d = new Date();
    d.setDate(d.getDate() - 30);
    var curDate = d.getDate();
    var curMonth = d.getMonth() + 1;
    var curYear = d.getFullYear();

    return (curDate + "." + curMonth + "." + curYear);
}

function GetToday() {
    var d = new Date();
    var curDate = d.getDate();
    var curMonth = d.getMonth() + 1;
    var curYear = d.getFullYear();

    return (curDate + "." + curMonth + "." + curYear);
}

function OnDownloadErrorDivClick() {
    $('#DownloadErrorDiv').hide();
}

function OnClearClick() {
    DisableButton();
    PeriodFrom.SetText(GetLastMonth());
    PeriodTo.SetText(GetToday());
    txtManifestNo.SetText('');
    txtOrderNo.SetText('');
    OIHDockOption.SetText('');

    grdData.UnselectAllRowsOnPage();
    GridOIHDockOption.UnselectRows();
    UploadedOnlyCheck.SetChecked(false)
    grdData['cpPONO'] = null;

    if (SupplierCode == null || SupplierCode == "")
        OIHSupplierOption.SetText();
        GridOIHSupplierOption.UnselectRows();


    grdData.UnselectFilteredRows();

    //set manifest to default
    ListKeys = new Array();
    ListManifest = new Array();
    ListTotalAmount = new Array();
    ListInvoice = new Array();
    ListDocumentDate = new Array();
    grdData.GotoPage(0);
}


function popupPreviewCloseUp(s, e) {
    OnPopupGetRowValues([null, null, null, null, null
                        , null, null, null, null, null
                        , null, null, null
                        ]) ;

}

function OnpopupInvoicePreviewClosing(s, e) {
    grdData.UnselectFilteredRows();
    DoSearch();
}

function btnRefreshClick(s, e) {
    DisableButton();
    $.ajax({type: "POST",
        url: getActMethodUrl('Refresh'),
        data: {

        },
        success: function(result) {
            btnRefresh.SetEnabled(false);
            alert("Refreshing in progress");
            EnableButton();
        },
        fail: function(result) {
            EnableButton();
        }
    });
}

function StampIncludedCheckedChanged(s, e)
{
    sumInTot();
}
var tx = null;

function PollCheckLocked() {
    if (LockRef && LockRef.length > 0)
        checkLock();
}

function checkLock() {
    tx = setTimeout(
            function() {
                UploadInProgressCheck(null, PollCheckLocked, null)
            }
            , 60000); // must hold for at least for a minute
}

var sx = null;
function delayedSearch(v) {
    sx = setTimeout(function() {
            DoSearch(v);
        }, 3000);
}

function OnClosePopup(s, e){
    ListKeys = new Array();
    ListManifest = new Array();
    ListTotalAmount = new Array();
    ListInvoice = new Array();
    ListDocumentDate = new Array();

    popupInvoicePreview.Hide();

    grdData.UnselectFilteredRows();
}
function uniqueArray(a) {

        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }

    // usage
    if (a){}
    else {return null};
    var unique = new Array();
    Object.apply( a.filter( onlyUnique ), unique); // returns ['a', 1, 2, '1']

    return unique;
}


function OnGridChecked(s,e){
    var ValTotalAmount = 0;
    var ValSelectedAmount = 0;
    var found = -1;
    var maxRowOnPage = grdData.GetPageIndex() * grdData.cpPageSize;

    /*set manifest*/
    var sKeys = s.GetRowKey(e.visibleIndex);
    if (!sKeys) return;

    var key = sKeys.split('|');

    /*find manifest in array if any array then remove else add*/
    for (var i = 0; i < ListKeys.length; i++) {
        if (ListKeys[i] == sKeys) {
            found = i;
            break;
        }
    }

    /*set Total Amount*/
    var sManifest = "";
    var sAmount = "";
    var sInvoince = "";
    var sDocumentDate = "";
    for (var index = grdData.GetTopVisibleIndex(); index < maxRowOnPage + grdData.GetVisibleRowsOnPage(); index++) {
        if (grdData.IsRowSelectedOnPage(index))
        {
            if (key[0].toString() == grdData.cpManifest[index - maxRowOnPage]) {
                sManifest = grdData.cpManifest[index - maxRowOnPage];
                sAmount = grdData.cpAmount[index - maxRowOnPage];
                sInvoince = grdData.cpInvoiceNo[index - maxRowOnPage];
                sDocumentDate = grdData.cpDocumentDate[index - maxRowOnPage];
            }
        }
    }

    // format document date menjadi string 20191017 hari
    var documentDateStr = "";
    if (sDocumentDate != null && sDocumentDate !='') {
        var dayDocumentDate = sDocumentDate.getDate();
        var monthDocumentDate = sDocumentDate.getMonth()+1;
        var yearDocumentDate = sDocumentDate.getFullYear();

        if (dayDocumentDate < 10) {
            dayDocumentDate = "0" + dayDocumentDate;
        }

        if (monthDocumentDate < 10) {
            monthDocumentDate = "0" + monthDocumentDate;
        }

        documentDateStr = yearDocumentDate + "-" + monthDocumentDate + "-" + dayDocumentDate;
    }

    //console.log(sManifest + " - " + sAmount + " - " + sInvoince);

    /*Set List*/
    if (found == -1) {
        if ((sKeys != "") && (sKeys != null)) {
            ListKeys.push(sKeys);
        }

        if ((sManifest != "") && (sManifest != null)) {
            ListManifest.push(sManifest);
        }

        if ((sAmount != "") && (sAmount != null)) {
            ListTotalAmount.push(sAmount);
        }

        if ((sInvoince != "") && (sInvoince != null)) {
            ListInvoice.push(sInvoince);
        }
        if ((documentDateStr != "") && (documentDateStr != null)) {
            ListDocumentDate.push(documentDateStr);
        }

    }
    else {
        ListKeys.splice(found, 1);
        ListManifest.splice(found, 1);
        ListTotalAmount.splice(found, 1);
        ListInvoice.splice(found, 1);
        ListDocumentDate.splice(found, 1);
    }

    /*set value textbox with total amount*/
    if(ListTotalAmount.length == 0 || ListManifest == ""){
        ValSelectedAmount= grdData.cpVisibleRowCount;
        ValTotalAmount = grdData.cpSum;
    }
    else{
        ValSelectedAmount = ListTotalAmount.length;

        for (var ito = 0; ito < ValSelectedAmount; ito++) {
            ValTotalAmount += isNaN(parseFloat(ListTotalAmount[ito])) ? 0 : parseFloat(ListTotalAmount[ito]);
        }
    }

    var xpo = new Array();
    s.GetSelectedFieldValues("PONo", function (s) {
        s.forEach(function(a,i) {
            if (xpo.indexOf(a) == -1) xpo.push(a);
        });
    });
    s['cpPONO'] = xpo;

    //console.log(ValSelectedAmount + " - " + ValTotalAmount);
    selManiTot(ValSelectedAmount, ValTotalAmount);
}

function OnGridEnd(s, e) {
    ListKeys = ListKeys;
    ListManifest = ListManifest;
    ListTotalAmount = ListTotalAmount;
    ListInvoice = ListInvoice;

    /*set textbox amount with default value*/
    if(ListTotalAmount.length == 0 || ListTotalAmount.length == []){
        var ValSelectedAmount= grdData.cpVisibleRowCount;
        var ValTotalAmount = grdData.cpSum;

        selManiTot(ValSelectedAmount, ValTotalAmount);
    }
}

function SelectingAllRows(s,e){
    var isChecked = chcBox.GetChecked(true);
    for (var index = grdData.GetTopVisibleIndex(); index < ((grdData.GetPageIndex() * grdData.cpPageSize) + grdData.GetVisibleRowsOnPage()); index++) {
            grdData.SelectRowOnPage(index, isChecked);
        }
}

function btnReportClick(s, e) {
    popupInvoiceReport.Show();
}
function btnResportSummaryClick(s, e){
    popupInvoiceReport.Hide();
    $("#DOMessageLabel").text("Download in progress,\nPlease wait...");
    popupProgress.Show();
    $.fileDownload("InvoiceCreation/DownloadGR_IR_Summary/",{
        data :{
            DocDate: dtmOutstandingAsOf.GetText()
        },
        successCallback : function(url){
            popupProgress.Hide();
            EnableButton();
        },
        failCallback : function(responseHtml, url){
            popupProgress.Hide();
            EnableButton();
        }
    });
    popupProgress.Hide();
}
function btnReportDetailClick(s, e){
    popupInvoiceReport.Hide();
    $("#DOMessageLabel").text("Download in progress,\nPlease wait...");
    popupProgress.Show();
    $.fileDownload("InvoiceCreation/DownloadListCSVNew/",{
        data :{
            DocDate: dtmOutstandingAsOf.GetText()
        },
        successCallback : function(url){
            popupProgress.Hide();
            EnableButton();
        },
        failCallback : function(responseHtml, url){
            popupProgress.Hide();
            EnableButton();
        }
    });
    popupProgress.Hide();
}

function GetRetroDisabledDate() {
    $.ajax({
        type: "POST",
        url: getActMethodUrl('GetDisabledRetroDate'),
        data: {},
        async: false,
        success: function (result) {
            RetroDisabledDate.SetText(result);
        },
        error: function (result) {
            RetroDisabledDate.SetText("E");
        }
    });
}

//Add By Arka.Taufik 2022-03-22
function GetTaxList() {
    $.ajax({
        type: "POST",
        url: getActMethodUrl('GetTaxList'),
        data: {},
        async: false,
        success: function (result) {
            console.log(result)
        },
        error: function (result) {

        }
        
    });
}

function CheckRetroDisabledDate() {
    var DisabledDate = RetroDisabledDate.GetText() === "E" ? "01.07.2015" : RetroDisabledDate.GetText();

    if ((MonthsBetween(dmy(deInvoiceDate.GetText()), dmy(DisabledDate)) >= 0) ||
        (MonthsBetween(dmy(InvoiceTaxDate.GetText()), dmy(DisabledDate)) >= 0))
    {
        alert("Error : Retro must be posted before July 2015");
        return 1;
    }
    return 0;
}

function CheckInvoiceDateForRetro(s, e) {
    if ((InvoiceTaxDate.GetText() != null) && (InvoiceTaxDate.GetText() != "") && (RetroAmount.GetText() != '0')) {
        //CheckRetroDisabledDate();
    }
}

function GetInvTaxValue(s, e) {
    s.GetSelectedFieldValues("InvoiceTax", function (field) {
        TaxAmount.SetValue(field);
        //if (InvoiceTaxAmount.GetText() != field)
        //alert('Tax Amount is Different with Invoice Tax Amount, Please Select Another Tax No');
    });
    s.GetSelectedFieldValues("InvoiceTaxDate", function (field) {
        var d = new Date(field);
        InvoiceTaxDate.SetValue(d);
        /*if ((RetroAmount.GetText() != '0') && (deInvoiceDate.GetText() != "") && (deInvoiceDate.GetText() != null)) {
            CheckRetroDisabledDate();
        }*/
    });
    s.GetSelectedFieldValues("BypassFlag", function (field) {
        txBypassFlag.SetValue(field);
        //if (InvoiceTaxAmount.GetText() != field)
        //alert('Tax Amount is Different with Invoice Tax Amount, Please Select Another Tax No');
    });
}

function CheckDataClick(s, e) {

    PopupCheckManifest.Show();
}

function SearchDataManifestClick()
{
    if (txtManifestNoCheck.GetText() == '')
    {
        alert("Please fill in Manifest Number");

    }
    else { GRIDGridView.ClearFilter(); }

}
function OnGRIDGridViewBeginCallback(s, e) {
    e.customArgs["MANIFEST_NO"] = txtManifestNoCheck.GetText();
}

$(document).ready(function () {

    //Test
    //PeriodFrom.SetText('01.01.2000')
    //OIHSupplierOption.SetText(1)


    GridInvoiceTaxNo.MoveColumn(0);
    //Add By Arka.Taufik 2022-03-22
    GridTaxCodeCombo.MoveColumn(0);

    $('#PPh23Row').hide();

    $(document).on("click", "#btnCancelInvoiceTaxNo", function (e) {
        InvoiceTaxDate.SetValue(null);
        TaxAmount.SetValue("");
        InvoiceTaxNo.SetValue("");
        GridInvoiceTaxNo.MoveColumn(0);
    });

    //Add By Arka.Taufik 2022-03-22
    $('#btnCancelTaxCodeCombo').click(function () {

        //Update By Arka.Taufik 2022-04-10
        var ITA = parseInt((TurnOver.GetText() * parseInt(TaxRate)) * 0.01);

        GridTaxCodeCombo.SelectRowsByKey(TaxCodeFix)
        TaxCodeCombo.SetValue(TaxCodeFix);
        RetroTaxRate.SetValue(parseInt(TaxRate));
        InvoiceTaxAmount.SetText(ITA);
        TotalInvoiceAmount.SetText(parseInt(TurnOver.GetText()) + ITA)
    })

    //Add By Arka.Taufik 2022-04-10
    $('#btnOkTaxCodeCombo').click(function () {
        TaxCodeFix = TaxCodeCombo.GetValue()
        TaxRate = RetroTaxRate.GetValue()
    })
});

function OnGRIDManifestDetailLinkErrorPostingClick(s, e) {

    var index = s.name.substring(34);

    index = index;
    dockCode = GRIDGridView.cpDockCode[index];
    rcvPlantCode = GRIDGridView.cpRcvPlantCode[index];
    manifestNo = GRIDGridView.cpManifestNo[index];

    GoodsReceiptInquiryManifestDetailPopupErrorPosting.Show();

}
function OnGoodsReceiptInquiryManifestDetailPopupShownError(s, e) {
    GoodsReceiptInquiryManifestDetailPopupErrorPosting.SetHeaderText("Detail Error Manifest No " + manifestNo);
    GRIDEPGridView.PerformCallback();
}
function OnGRIDEPGridViewBeginCallback(s, e) {
    e.customArgs["DockCode"] = dockCode;
    e.customArgs["RcvPlantCode"] = rcvPlantCode;
    e.customArgs["ManifestNo"] = manifestNo;
}
function coTTSelectedIndexChanged(s, e) {
    console.log('SelectedIndexChanged:', s);

    //FID.Ridwan: 20210125
    var aaTurnOver = parseFloat(TurnOver.GetText());
    var aRetroAmount = parseFloat(RetroAmount.GetText());
    var aInvoiceTaxAmount = parseFloat(InvoiceTaxAmount.GetText());
    var InvAmount = aaTurnOver + aRetroAmount + aInvoiceTaxAmount;
    var stamped = 0;
    var StampedAmount = 0;
    if (StampIncluded.GetChecked())
        stamped = 1;

    if (stamped == 1) {
        StampedAmount = parseFloat(icStampAmount);
    }

    if (s.GetValue() == '3') {
        $('#PPh23Row').show();
        var aTurnOver = 0.0, aTaxRate = 0.0, PPh23Tax = 0.0;

        if (PPh23TaxRate !== null) {
            aTurnOver = parseFloat(TurnOver.GetText());
            aTaxRate = parseFloat(PPh23TaxRate);
            PPh23Tax = aTurnOver * (aTaxRate / 100);
            PPh23Amount.SetValue(PPh23Tax);

            //FID.Ridwan: 20210125
            TotalInvoiceAmount.SetText(parseFloat(InvAmount) + StampedAmount - parseFloat(PPh23Tax));
        } else {
            $.ajax({
                method: "GET",
                url: getActMethodUrl("PPh23TaxRate"),
                success: function (r) {
                    PPh23TaxRate = r;
                    aTurnOver = parseFloat(TurnOver.GetText());
                    aTaxRate = parseFloat(PPh23TaxRate);
                    PPh23Tax = aTurnOver * (aTaxRate / 100);
                    PPh23Amount.SetValue(PPh23Tax);

                    //FID.Ridwan: 20210125
                    TotalInvoiceAmount.SetText(parseFloat(InvAmount) + StampedAmount - parseFloat(PPh23Tax));
                }
            });
        }
    } else {
        $('#PPh23Row').hide();

        //FID.Ridwan: 20210125
        PPh23Amount.SetValue(0)
        TotalInvoiceAmount.SetText(parseFloat(InvAmount) + StampedAmount)
    }
}

function TaxNoMandat() {
    if (CoilSupplier.includes(";" + OIHSupplierOption.GetText() + ";"))
        $("#tax_no_mandat").hide();
    else
        $("#tax_no_mandat").show();
}


//Add By Arka.Taufik 2022-03-22
function GetTaxCodeCombo(a, b, c) {
    console.log('a', a)
    a.GetSelectedFieldValues("RATE", function (field) {
        RetroTaxRate.SetValue(field);
        InvoiceTaxAmount.SetText((TurnOver.GetValue() * parseInt(field)) / 100);
        TotalInvoiceAmount.SetText(parseInt(TurnOver.GetValue()) + ((TurnOver.GetValue() * parseInt(field)) / 100))
        coTTSelectedIndexChanged(coTT)
    });

}