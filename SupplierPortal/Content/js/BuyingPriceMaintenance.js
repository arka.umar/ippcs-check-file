function getActMethodUrl(method, fun) {
    if (fun)
        return window.__base + fun + "/" + method;
    return window.__base + "BuyingPriceMaintenance/" + method;
}
function OnOIPSearchButtonClick(s, e) {

    BuyingPriceInquiry.ClearFilter();
}
function OnOIPViewEndCallback(s, e) {
    if (BuyingPriceInquiry.GetPageCount() <= 0) {
        popupNoDataFound.Show();
    }
}
function OnOIPViewBeginCallback(s, e) {
    e.customArgs["SupplierCode"] = OISupplierCd.GetText();
    e.customArgs["MaterialNo"] = OIMatNo.GetText();
    e.customArgs["ReleaseSts"] = OIPReleaseOption.GetValue();
    e.customArgs["PrieType"] = OIPPriceTypeOption.GetText();
    //FID.Ridwan - 20210714
    e.customArgs["BuyerCd"] = OIBuyerCd.GetText();
    e.customArgs["MatDesc"] = OIMatDesc.GetText();
    e.customArgs["PriceStatus"] = OIPriceStatus.GetValue();
    e.customArgs["PackingType"] = OIPackingType.GetValue();
    e.customArgs["SourceData"] = OISourceData.GetValue();
    e.customArgs["WarpRefNo"] = OIWarpRefNo.GetText();
    e.customArgs["DeletionFlag"] = OIDeletionFlag.GetValue();
    e.customArgs["SourceType"] = OISourceType.GetValue();
    e.customArgs["PCNo"] = OIPCNo.GetText();
    e.customArgs["ProdPurpose"] = OIProdPurpose.GetValue();
    e.customArgs["CppFlag"] = OICppFlag.GetValue();
    e.customArgs["ValidFrom"] = OIValidFrom.GetText();
    e.customArgs["ValidTo"] = OIValidTo.GetText();
}
function OnOIPAddButtonClick() {
    ClearAddEdit();
    gScreenMode = "ADD";
    AddEditMaterialNo.SetEnabled(true);
    AddEditSupplierCd.SetEnabled(true);
    AddEditSourceType.SetEnabled(true);
    AddEditProdPurpose.SetEnabled(true);
    AddEditValidTo.SetEnabled(false);
    $("input[name=AddEditValidTo]").val('31.12.9999');
    AddEditPartColorSfx.SetEnabled(true);
    AddEditPackingType.SetEnabled(true);
    BuyingPriceAddedit.Show();
}
function SaveButtonClick(s, e) {
    var sPrice = AddEditPrice.GetText();
    var price = parseFloat(sPrice);
    var hasComma = false;
    
    if (sPrice.indexOf(',') > 0) hasComma = true;

    var currency = AddEditCurrency.GetText();
    if (currency == "IDR" && ((Math.floor(price) < price) || hasComma) ) {
        alert("Price in IDR must be a round number");        
        return;
    }
    if (currency !== "IDR" && hasComma) {
        alert("invalid decimal separator");
        return;
    }
        
    $.msgBox({
        type: "confirm",
        content: "Are you sure you want to confirm all records?",
        buttons: [{ value: "Yes" }, { value: "No"}],
        success: function (result) {
            if (result == "Yes") {
                onSave()
            }
            if (result == "No") {
            }
        }
    });
}
function onSave() {
    var BusinesArea = AddEditBisnisArea.GetValue();
    var MatNo = AddEditMaterialNo.GetText();
    $.ajax({
        type: "POST",
        url: getActMethodUrl("AddEditData"),
        data: {
            BUSINESS_AREA: BusinesArea,
            MATERIAL_NO: MatNo,
            MATERIAL_DESC: AddEditMaterialDesc.GetText(),
            PROD_PURPOSE: AddEditProdPurpose.GetValue(),
            SOURCE_TYPE: AddEditSourceType.GetValue(),
            SUPPLIER_CODE: AddEditSupplierCd.GetValue(),
            PART_COLOR_SFX: AddEditPartColorSfx.GetText(),
            PACKING_TYPE: AddEditPackingType.GetValue(),
            PRICE_STS: AddEditPriceStatus.GetValue(),
            PRICE: AddEditPrice.GetText(),
            CURR_CD: AddEditCurrency.GetText(),
            VALID_FROM: $("input[name='AddEditValidFrom']").val(),
            gScreenMode: gScreenMode
        },
        success: function (result) {
            var resValue = result.split('|');
            if (resValue[0] == 'Error') {
                $.msgBox(resValue[1]);
            }
            else {
                BuyingPriceAddedit.Hide()
                $.msgBox({ content: resValue[1], type: 'info' });
                BuyingPriceInquiry.UnselectAllRowsOnPage();
                BuyingPriceInquiry.PerformCallback();
            }
        },
        error: function (result) {
            $.msgBox(result);
        }
    })
}
function CancelButtonClick() {
    if (confirm('Are you sure to cancel this data ?')) {
        ClearAddEdit();
    }
}
function ClearAddEdit() {
    AddEditBisnisArea.SetValue('');
    AddEditMaterialNo.SetValue('');
    AddEditPriceStatus.SetValue('');
    AddEditMaterialDesc.SetValue('');
    AddEditPrice.SetValue('');
    AddEditProdPurpose.SetValue('');
    AddEditCurrency.SetValue('');
    AddEditSourceType.SetValue('');
    $("input[name='AddEditValidFrom']").val('');
    AddEditSupplierCd.SetValue('');
    $("input[name='AddEditValidTo']").val('');
    $("input[name='AddEditValidTo']").val('');
    AddEditPartColorSfx.SetValue('');
    AddEditPackingType.SetValue('');
}
function OnOIPEditButtonClick(s, e) {
    gScreenMode = "EDIT";
    var selectedrow = BuyingPriceInquiry.GetSelectedRowCount();
    if (selectedrow == 0) {
        $.msgBox("Please select at least one record");
        return;
    }
    if (selectedrow > 1) {        
        $.msgBox("Can only select one of data to be edited");
        return;
    }
    else if (confirm("Are you sure you want to edit the record?")) {
        var iTop = BuyingPriceInquiry.GetPageIndex() * BuyingPriceInquiry.cpPageSize;
        for (var i = BuyingPriceInquiry.GetTopVisibleIndex(); i < ((iTop) + BuyingPriceInquiry.GetVisibleRowsOnPage()); i++) {
            if (BuyingPriceInquiry.IsRowSelectedOnPage(i)) {
                var ii = i - iTop;
                paramMaterialNo = BuyingPriceInquiry.cpMaterialNo[ii];
                paramSupplieCd = BuyingPriceInquiry.cpSupplierCD[ii];
                paramSourceType = BuyingPriceInquiry.cpSourceType[ii];
                paramProdPurpose = BuyingPriceInquiry.cpProdPurpose[ii];
                paramValidFrom = BuyingPriceInquiry.cpValidFrom[ii];
                paramPartColorSfx = BuyingPriceInquiry.cpPartColorSFX[ii];
                paramPackingType = BuyingPriceInquiry.cppackingType[ii];
            }
        }


        $.ajax({
            type: "POST",
            url: getActMethodUrl("getByKey"),
            data: {
                MATERIAL_NO: paramMaterialNo,
                SUPPLIER_CODE: paramSupplieCd,
                SOURCE_TYPE: paramSourceType,
                PROD_PURPOSE: paramProdPurpose,
                VALID_FROM: paramValidFrom,
                PART_COLOR_SFX: paramPartColorSfx,
                PACKING_TYPE: paramPackingType
            },
            success: function (result) {
                var resultIsNotEmpty = false;
                if (result) {
                    if (result[0]) {
                        resultIsNotEmpty = true;
                    }
                }
                if(!resultIsNotEmpty) {
                    alert("Can't get data to edit");
                    return;
                }
                if (result[0].editable == "True") {
                    AddEditBisnisArea.SetValue(result[0].BUSINESS_AREA);
                    AddEditMaterialNo.SetValue(result[0].MATERIAL_NO);
                    AddEditPriceStatus.SetValue(result[0].PRICE_STS);
                    AddEditMaterialDesc.SetValue(result[0].MATERIAL_DESC);
                    AddEditPrice.SetValue(result[0].PRICE);
                    AddEditProdPurpose.SetValue(result[0].PROD_PURPOSE);
                    AddEditCurrency.SetValue(result[0].CURR_CD);
                    AddEditSourceType.SetValue(result[0].SOURCE_TYPE);
                    $("input[name='AddEditValidFrom']").val(result[0].VALID_FROM);
                    AddEditSupplierCd.SetValue(result[0].SUPPLIER_CODE);
                    $("input[name='AddEditValidTo']").val(result[0].VALID_DT_TO);
                    AddEditPartColorSfx.SetValue(result[0].PART_COLOR_SFX);
                    AddEditPackingType.SetValue(result[0].PACKING_TYPE);
                    SetScreenEdit();
                } else {
                    $.msgBox("The record is already released");
                }
            },
            error: function (result) {
            }
        })
    }
}
function SetScreenEdit() {
    AddEditMaterialNo.SetEnabled(false);
    AddEditSupplierCd.SetEnabled(false);
    AddEditSourceType.SetEnabled(false);
    AddEditProdPurpose.SetEnabled(false);
    AddEditValidTo.SetEnabled(false);
    AddEditValidFrom.SetEnabled(false);
    AddEditPartColorSfx.SetEnabled(false);
    AddEditPackingType.SetEnabled(false);
    BuyingPriceAddedit.Show();
}
function OnOIPDownloadButtonClick() {
    searchSupplieCd = OISupplierCd.GetText();
    searchMaterialNo = OIMatNo.GetText();
    searchReleaseStatus = OIPReleaseOption.GetValue();
    searchPriceType = OIPPriceTypeOption.GetText();

    PurchaseReportDownload(searchSupplieCd, searchMaterialNo, searchReleaseStatus, searchPriceType);

    /*$.ajax({
        type: "post",
        url: getActMethodUrl("CheckBeforeDownload"),
        data: {
            SUPPLIER_CD: searchSupplieCd,
            MATERIAL_NO: searchMaterialNo,
            RELEASE_STS: searchReleaseStatus,
            PRICE_TYPE: searchPriceType
        },
        success: function (data) {
            var splitMsg = data.split('|');
            if (splitMsg[0] == "E") {
                alert(splitMsg[1]);
            }
            else {
                PurchaseReportDownload(searchSupplieCd, searchMaterialNo, searchReleaseStatus, searchPriceType);
            }
        },
        error: function (data) {
            alert("Download Purchase Report Error");
        }
    });*/
}
function PurchaseReportDownload(searchSupplieCd, searchMaterialNo, searchReleaseStatus, searchPriceType) {
    $("#IPMessageLabel").text("Download in progress,\nPlease wait...");

    //FID.Ridwan - 20210714
    searchBuyerCd = OIBuyerCd.GetText();
    searchMatDesc = OIMatDesc.GetText();
    searchPriceStatus = OIPriceStatus.GetValue();
    searchPackingType = OIPackingType.GetValue();
    searchSourceData = OISourceData.GetValue();
    searchWarpRefNo = OIWarpRefNo.GetText();
    searchDeletionFlag = OIDeletionFlag.GetValue();
    searchSourceType = OISourceType.GetValue();
    searchPCNo = OIPCNo.GetText();
    searchProdPurpose = OIProdPurpose.GetValue();
    searchCppFlag = OICppFlag.GetValue();
    searchValidFrom = OIValidFrom.GetText();
    searchValidTo = OIValidTo.GetText();


    $.fileDownload(getActMethodUrl("DownloadBuyingPriceCSV"), {
        data: {
            SUPPLIER_CODE: searchSupplieCd,
            MATERIAL_NO: searchMaterialNo,
            RELEASE_STS: searchReleaseStatus,
            PRICE_TYPE: searchPriceType,
            WRAP_BUYER_CD: searchBuyerCd,
            MATERIAL_DESC: searchMatDesc,
            PRICE_STS: searchPriceStatus,
            PACKING_TYPE: searchPackingType,
            SOURCE_DATA: searchSourceData,
            WRAP_REF_NO: searchWarpRefNo,
            DELETION_FLAG: searchDeletionFlag,
            SOURCE_TYPE: searchSourceType,
            PC_NO: searchPCNo,
            PROD_PURPOSE: searchProdPurpose,
            CPP_FLAG: searchCppFlag,
            VALID_FROM: searchValidFrom,
            VALID_DT_TO: searchValidTo
        },
        successCallback: function (url) {
        },
        failCallback: function (responseHtml, url) {
            alert(responseHtml);
        }
    });
}
function OnOIPDeleteButtonClick() {
    var selectedKey = BuyingPriceInquiry.GetSelectedKeysOnPage();
    if (selectedKey.length < 1) {
        $.msgBox("A single record must be selected to execute Delete operation.");
        return;
    }
    else {
        $.msgBox({
            type: "confirm",
            content: "Are you sure you want to delete the record ?",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    DeleteBuyingPriceMaintenanceF();
                    BuyingPriceInquiry.UnselectAllRowsOnPage();
                    BuyingPriceInquiry.PerformCallback();
                }
                if (result == "No") {
                    BuyingPriceInquiry.UnselectAllRowsOnPage();
                    BuyingPriceInquiry.PerformCallback();
                }
            }
        });
    }
}
function DeleteBuyingPriceMaintenanceF() {
    var MatNoDel = "";
    var SuppCD = "";
    var SourceType = "";
    var prodPurposeCD = "";
    var validFrom = "";
    var partColorSfx = "";
    var packingType = "";
    var iTop = BuyingPriceInquiry.GetPageIndex() * BuyingPriceInquiry.cpPageSize;
    for (var index = BuyingPriceInquiry.GetTopVisibleIndex(); 
	     index < ((iTop) + BuyingPriceInquiry.GetVisibleRowsOnPage()); 
		 index++) {
        if (BuyingPriceInquiry.IsRowSelectedOnPage(index)) {
            var ii = index - iTop;
            MatNoDel += BuyingPriceInquiry.cpMaterialNo[ii] + ",";
            SuppCD += BuyingPriceInquiry.cpSupplierCD[ii] + ",";
            SourceType += BuyingPriceInquiry.cpSourceType[ii] + ",";
            prodPurposeCD += BuyingPriceInquiry.cpProdPurpose[ii] + ",";
            validFrom += BuyingPriceInquiry.cpValidFrom[ii] + ",";
            partColorSfx += BuyingPriceInquiry.cpPartColorSFX[ii] + ",";
            packingType += BuyingPriceInquiry.cppackingType[ii] + ",";
        }
    }
    $.ajax({
        type: "POST",
        url: getActMethodUrl("DeleteMasterBuyingPrice"),
        data: {
            MATERIAL_NO: MatNoDel,
            SUPPLIER_CODE: SuppCD,
            SOURCE_TYPE: SourceType,
            PROD_PURPOSE: prodPurposeCD,
            VALID_FROM: validFrom,
            PART_COLOR_SFX: partColorSfx,
            PACKING_TYPE: packingType
        },
        success: function (resultMessage) {
            $.msgBox(resultMessage);
        },
        error: function (resultMessage) {
            alert("Error when Delete data");
        }
    });
    BuyingPriceInquiry.UnselectAllRowsOnPage();
    BuyingPriceInquiry.PerformCallback();
}
function DownloadTemplate() {
    $.fileDownload(getActMethodUrl("downloadTemplate"), {
        data:
        {
        },
        successCallback: function (url) {
        },
        failCallback: function (responseHtml, url) {
        }
    });
}
function upload(s, e) {
    var validExt = ".xls, .xlsx";
    var filePath = BuyingPriceUpload.GetText();
    var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
    var pos = validExt.indexOf(getFileExt);
    if (pos < 0) {
        $.msgBox("Invalid Excel file format. Please use download the correct template from this screen.");
        return false;
    } else {
        BuyingPriceUpload.UploadFile();
        return true;
    }
}
function OnUploadComplete(s, e) {
    if (e.isValid) {
        filePath = e.callbackData;
        LoadingPanel.Show();
        $.ajax({
            type: "POST",
            url: getActMethodUrl("BuyingPriceUpload"),
            dataType: "json",
            async: false,
            data: {
                filePath: filePath
            },
            success: function (result) {
                var parameterValues = result.Status.split(';');
                if (parameterValues[0] == "true") {
                    $.msgBox("Upload Success with Process ID " + parameterValues[1])
                    BuyingPriceInquiry.PerformCallback();
                }
                else if (parameterValues[0] == "false") {
                    var msg = "Upload faield with Process ID " + parameterValues[1];
                    if (parameterValues.length > 2)
                        msg = msg + ". Error file: " + parameterValues[2];
                    $.msgBox(msg);
                    if (parameterValues[2]) {
                        $.fileDownload(getActMethodUrl("downloadTemplate"), {
                            data: { fileName: parameterValues[2] },
                            successCallback: function (url) {
                            },
                            failCallback: function (responseHtml, url) {
                            }
                        });
                    }
                }
                else if (parameterValues[0] == "empty") {
                    alert("No Data Found in Uploaded Data.");
                }

                LoadingPanel.Hide();
            },
            error: function (result) {
                alert("Error On Validate Upload Process.");
                LoadingPanel.Hide();
            }
        });
        LoadingPanel.Hide();
    }
}
function OnOIPClearButtonClick(s, e) {
    OISupplierCd.SetText('');
    OIMatNo.SetText('');
    OIPReleaseOption.SetText('');
    OIPPriceTypeOption.SetText('Draft Price');

    //FID.Ridwan - 20210714
    OIBuyerCd.SetText('');
    OIMatDesc.SetText('');
    OIPriceStatus.SetText('');
    OIPackingType.SetText('');
    OISourceData.SetText('');
    OIWarpRefNo.SetText('');
    OIDeletionFlag.SetText('');
    OISourceType.SetText('');
    OIPCNo.SetText('');
    OIProdPurpose.SetText('');
    OICppFlag.SetText('');
    OIValidFrom.SetText(''); //SetText(GetToday());
    OIValidTo.SetText(''); //SetText('31.12.9999');

    OnOIPSearchButtonClick(s, e);
}
function OnFullPcClick(s, e) {
    var pcno = s.mainElement.getAttribute('vi');
    var released = s.mainElement.getAttribute('released');
    if (released !== 'Y') {
        alert("PC not released yet");
        return;
    }

    $.fileDownload(getActMethodUrl("DownloadPdf", "PCDownload"), {
        data: {
            PcNo: pcno
        },
        successCallback: function (url) {
        },
        failCallback: function (responseHtml, url) {
            alert(responseHtml);
        }
    });

}

function OnOIPWARPButtonClick(s, e) {
    $.ajax({
        type: "POST",
        url: getActMethodUrl("WARP"),
        data: { },
        success: function (result) {
            $.msgBox("WARP validate done.");            
        },
        error: function (result) {
            $.msgBox("Error doing WARP");
        }
    });
}

//FID.Ridwan - 20210714
function OnDateChanged(s, e) {
    var myname = s.name;
    var mydate = s.date;
    if (!mydate) {
        return;
    }
    var yourname = "";
    var isFrom = myname.lastIndexOf("From");
    if (isFrom > 0) {
        yourname = myname.substr(0, isFrom) + "To";
    }
    else {
        yourname = myname.substr(0, myname.length - 2) + "From";
    }

    var oPair = eval(yourname);
    var yourdate = oPair.GetDate();

    if (yourdate == null) {
        oPair.SetDate(mydate);
        return;
    }
    if ((isFrom > 0 && mydate > yourdate)
         || (mydate < yourdate && isFrom < 0)) {
        oPair.SetDate(mydate);
        s.SetDate(mydate);
    }
}

function GetLastMonth() {
    var d = new Date();
    d.setDate(d.getDate() - 30);
    var curDate = d.getDate();
    var curMonth = d.getMonth() + 1;
    var curYear = d.getFullYear();

    return (curDate + "." + curMonth + "." + curYear);
}

function GetToday() {
    var d = new Date();
    var curDate = d.getDate();
    var curMonth = d.getMonth() + 1;
    var curYear = d.getFullYear();

    return (curDate + "." + curMonth + "." + curYear);
}

