﻿declare @@sql nvarchar(max), 
@@supplier varchar(15) = @0,
@@dateFrom varchar(10) = @1,
@@dateTo varchar(10) = @2


set @@sql = 'select MANIFEST_NO ManifestNo,ORDER_NO OrderNo, TOTAL_ITEM TotalItem, TOTAL_QTY TotalQty from TB_R_DAILY_ORDER_MANIFEST
where 1=1'

if(@@supplier <> '')
	set @@sql = @@sql + 'and Supplier_CD = ''' + @@supplier + ''' '
	
if(@@dateFrom <> '')
	set @@sql = @@sql + 'and ORDER_RELEASE_DT >= ''' + cast(@@dateFrom as varchar) + ''' '

if(@@dateTo <> '')
	set @@sql = @@sql + 'and ORDER_RELEASE_DT <= ''' + cast(@@dateTo as varchar) + ''' '
	
set @@sql = @@sql + 'order by [ORDER_RELEASE_DT]'

execute (@@sql)