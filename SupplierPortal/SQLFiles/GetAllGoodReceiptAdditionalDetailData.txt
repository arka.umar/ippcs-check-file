﻿DECLARE @@MANIFEST_NO VARCHAR(MAX) = @0
IF object_id('tempdb..#GRError') IS NOT NULL DROP TABLE #GRError

CREATE TABLE #GRError
( 
	parameter VARCHAR(200)
)

INSERT  INTO #GRError(parameter)
EXEC dbo.SplitString @@MANIFEST_NO, ';'

DECLARE @@params VARCHAR(MAX) = (select
                                    distinct 
                                        stuff((
                                                select DISTINCT ''''',''''' + u.parameter
                                                from #GRError u
                                                where u.parameter = parameter
                                                --order by u.PART_NO
                                                for xml path('')
                                        ),1,3,'') + '''''' as partlist
                                    from #GRError
                                    group by parameter)

DECLARE @@SQL NVARCHAR(MAX) = N'
		SELECT * 
		FROM OPENQUERY(IPPCS_TO_ICS,
			''SELECT 
				t.REF_NO AS MANIFEST_NO, 
				t.MAT_NO, t.SOURCE_TYPE, 
				t.PROD_PURPOSE_cD, 
				t.DOCK_CD, 
				t.PLANT_CD, 
				t.SLOC_CD, 
				t.SUPP_cD, 
				y.ERR_CD as ERR_ID, 
				y.ERR_MSG
		FROM tb_r_err_posting_h t,
			 tb_r_err_posting_d y
		Where t.err_id = y.err_id
		and t.ref_no IN (' + @@params + ')'')'

EXEC sp_executesql @@SQL

DROP TABLE #GRError