﻿declare @@reviseNo varchar(10)
select @@reviseNo = REVISE_NO from TB_R_DELIVERY_CTL_H where DELIVERY_NO = @0

update TB_R_DELIVERY_CTL_H set REVISE_NO = (@@reviseNo +1) , DELIVERY_REASON = @1, CHANGED_BY =  @2, CHANGED_DT = @3 where DELIVERY_NO = @0