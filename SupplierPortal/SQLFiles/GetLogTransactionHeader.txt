﻿declare @@sql nvarchar(max), 
		@@moduleID varchar(10)=@0,
		@@dateFrom varchar(10) = @1, 
		@@dateTo varchar(10) = @2,
		@@functionID int = @3,
		@@processStatus varchar = @4
		
set @@sql = 'select
				   [LOG_ID_H] LogIDH
				  ,[MODULE_ID] ModuleID
				  ,[FUNCTION_ID] FunctionID
				  ,[START_DATE] StartDate
				  ,[END_DATE] EndDate
				  ,[LOG_STATUS] LogStatus
			from TB_M_LOG_TRANSACTION_H
			where 1=1 '
			
if(@@moduleID <> '')
	set @@sql = @@sql + 'and Module_ID = '+ cast(@@moduleID as varchar) + ' '

if(@@dateFrom <> '')
	set @@sql = @@sql + 'and Created_Date >= '''+ cast(@@dateFrom as varchar) + ''' '

if(@@dateTo <> '')
	set @@sql = @@sql + 'and Created_Date <= '''+ cast(@@dateTo as varchar) + ''' '

if(@@functionID <> '')
	set @@sql = @@sql + 'and Function_ID = ' + cast(@@functionID as varchar)+ ' '

if(@@processStatus <> '')
	set @@sql = @@sql + 'and Process_Status = ' + cast(@@processStatus as varchar)+ ' '

execute (@@sql)
