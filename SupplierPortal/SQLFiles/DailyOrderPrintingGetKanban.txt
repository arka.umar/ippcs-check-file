﻿SELECT r.KANBAN_ID, CASE WHEN GETDATE() < m.ARRIVAL_PLAN_DT AND r.RECEIVING_FLG = '0'  THEN 'Ordered' ELSE CASE WHEN r.RECEIVING_FLG = '1' THEN 'Completed' ELSE 'Shortage' END END  as STATUS FROM TB_R_DAILY_ORDER_PART p
INNER JOIN TB_R_DAILY_ORDER_MANIFEST m ON m.MANIFEST_NO = p.MANIFEST_NO
LEFT JOIN TB_R_RECEIVING_D r ON r.MANIFEST_NO = p.MANIFEST_NO AND r.PART_NO = p.PART_NO
WHERE r.KANBAN_ID IS NOT NULL AND r.RECEIVING_FLG IS NOT NULL AND m.ORDER_NO = @0 AND p.PART_NO = @1 AND p.KANBAN_NO = @2