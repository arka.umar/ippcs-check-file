﻿DECLARE 
 @@MatNo		varchar (max) = @0
,@@SourceType	varchar (max) = @1
,@@ProdPurpose	varchar (max) = @2
,@@ColorSuffix	varchar (max) = @3
,@@DockCode		varchar (max) = @4
,@@PackingType	varchar (max) = @5
,@@ValidFrom	varchar (max) = @6
,@@ValidTo		varchar (max) = null
,@@username     varchar(20) = @8 
,@@valid_dt_fr  datetime=null
,@@valid_dt_to  datetime=null
,@@MSG VARCHAR(MAX) = ''
, @@DB_NAME VARCHAR(50) = NULL 
, @@dblink_ics_new VARCHAR(50) =NULL
, @@sql varchar(max) = null

DECLARE @@tb_t_mat TABLE (MAT_NO VARCHAR(100))

select @@dblink_ics_new = SERVERNAME, @@DB_NAME = DBNAME 
from (
    select
     SERVERNAME= coalesce(
     (SELECT SYSTEM_VALUE FROM [dbo].[TB_M_SYSTEM]
     WHERE FUNCTION_ID = 'LINKED_SERVER'
     AND SYSTEM_CD = 'NEW_ICS_DB')
        ,'[BB071_DB]')
    ,DBNAME = COALESCE(
    (SELECT top 1 SYSTEM_VALUE
    FROM   [dbo].[TB_M_SYSTEM]
    WHERE  FUNCTION_ID = 'DB_NAME'
	  AND SYSTEM_CD = 'ICS_DB_NAME')
    ,'[BB071_DB]')
  ) x;

-- FID.Ridwan: 2021-03-18 -> add validation mat no exists ini New ICS
select @@sql = Convert(varchar(max), '') +
    'SELECT MAT_NO FROM OPENQUERY('
    + @@dblink_ics_new+ ',
    ''  select mat_no 
        from ' + @@DB_NAME + '.dbo.TB_M_MATERIAL m
        where 1 = 1
		and m.DELETION_FLAG <> ''''Y''''
		and m.MAT_NO = 
        ''''' + @@MatNo + '''''
        ;
        '')';

insert into @@tb_t_mat (mat_no)
exec (@@sql);

if (select count(1) from @@tb_t_mat) = 0
BEGIN
    select @@MSG = replace('Material No: {0} doesn''t exist or deletion flag = Y in MM Master', '{0}', @@MatNo);
    RAISERROR(@@MSG, 16, 2)
    Return;
END

IF(not EXISTS(SELECT TOP 1 1 from TB_M_DOCK where DOCK_CD =@@DockCode))
BEGIN
	RAISERROR('Dock Code does not exist on TB_M_DOCK', 16, 2)
	Return;
END
IF(not EXISTS(SELECT TOP 1 1 from TB_M_PACKING_TYPE where PACKING_TYPE_CD =@@PackingType))
BEGIN
	RAISERROR('Packing Type not exist on TB_M_PACKING_TYPE', 16, 2)
	Return;
END

if len(@@ValidFrom)<10 or isdate(substring(@@ValidFrom,7,4)+'-' + substring(@@ValidFrom,4,2) + '-' + substring(@@ValidFrom,1,2)) = 0
BEGIN
	RAISERROR('Invalid data type of Valid From. Should be "dd.mm.yyyy".', 16, 2)
	Return;
END

select @@valid_dt_fr = dbo.tryCONVERTdate(@@ValidFrom)
     , @@valid_dt_to = CONVERT(DATETIME,'99991231', 112)

if @@valid_dt_fr is null 
begin
	RAISERROR('Invalid date "Valid from"', 16, 2)
	Return;
end 

IF exists(
	SELECT 1
	FROM TB_M_PACKING_INDICATOR PCI
	WHERE MAT_NO = @@MatNo 
	AND PCI.SOURCE_TYPE = @@SourceType 
	AND PCI.PROD_PURPOSE_CD = @@ProdPurpose 
	AND PCI.PART_COLOR_SFX = @@ColorSuffix 
	AND PCI.DOCK_CD = @@DockCode 
	AND PCI.PACKING_TYPE_CD = @@PackingType
	AND PCI.VALID_DT_FR = @@VALID_DT_FR
	)
BEGIN
	SELECT @@MSG = 'Data with combination; Mat No : '+@@MatNo
    +', Source Type : '+@@SourceType
    +', Prod Purpose: '+@@ProdPurpose
    +', Part Color Suffix: '+@@ColorSuffix
    +', Dock Code: '+@@DockCode
    +', Packing type: ' + @@PackingType 
    +', Valid From: ' + @@ValidFrom
    +'; already exists'
	RAISERROR(@@MSG, 16, 2)
	Return;
END

-- INVALIDATE old data 
UPDATE TB_M_PACKING_INDICATOR SET 
   VALID_DT_TO = DATEADD(DAY, -1, @@VALID_DT_FR)
   , CHANGED_BY = @@username, CHANGED_DT = GETDATE() 
WHERE MAT_NO = @@MatNo 
AND SOURCE_TYPE= @@SourceType
AND PROD_PURPOSE_CD = @@ProdPurpose
AND PART_COLOR_SFX = @@ColorSuffix
AND DOCK_CD = @@DockCode 
AND @@valid_dt_fr BETWEEN VALID_DT_FR AND VALID_DT_TO; 

-- new 
INSERT INTO [dbo].[TB_M_PACKING_INDICATOR](
MAT_NO, SOURCE_TYPE, PROD_PURPOSE_CD, PART_COLOR_SFX, DOCK_CD, PACKING_TYPE_CD, VALID_DT_FR, VALID_DT_TO, CREATED_BY, CREATED_DT
)
VALUES (
 @@MatNo, @@SourceType, @@ProdPurpose, @@ColorSuffix, @@DockCode, @@PackingType, @@valid_dt_fr, @@valid_dt_to, @@username, GETDATE()
); 

