DECLARE
  @@production_month varchar(6) = @0
, @@TIMING varchar(10) = @1
, @@DateNow datetime
, @@DateNext datetime
, @@ThisMonth varchar(6)
, @@NextMonth varchar(6)

SET @@DateNow = getdate()
SET @@DateNext = dateadd(month, 1, getdate())
SET @@ThisMonth = cast(year(@@DateNow) AS VARCHAR(4)) + RIGHT('00'+CONVERT(VARCHAR(2), month(@@DateNow)),2)
SET @@NextMonth = cast(year(@@DateNext) AS VARCHAR(4)) +  RIGHT('00'+CONVERT(VARCHAR(2), month(@@DateNext)),2)
select @@TIMING = CASE NULLIF(@@TIMING,'')
      WHEN 'Tentative' then 'T'
      WHEN 'Firm' THEN 'F'
      ELSE NULL
      END;

SELECT (SELECT DateName(mm,
            DATEADD(mm,CAST((select RIGHT(C.PRODUCTION_MONTH,2)) as int),-1)))
            + '-'
            + (select left(C.PRODUCTION_MONTH,4)) AS ID
     , CASE isnull(C.LPOP_TIMING, 'T')
         WHEN 'T' THEN 'Tentative'
         WHEN 'F' THEN 'Firm'
       END AS LPOP_Type
     , RTRIM(C.[APPROVE_BY]) AS ApproveBy
     , C.[APPROVE_DT] AS ApproveDate
     , C.[FEEDBACK_DUE_DATE] as FeedbackDueDate
     , C.[LAST_REMINDING_DATE] as LastRemindingDate
     , C.[REVISED_FLAG] AS RevisedFlag
     , C.[REVISED_BY] AS RevisedBy
     , C.[REVISED_DT] AS RevisedDate
     , C.[PO_CREATION_STATUS] AS PO_Status
     , C.[PO_CREATED_BY] AS POBy
     , C.[PO_CREATED_DT] AS PODate
     , CASE WHEN ISNULL(B.[PROD_MONTH], '') <> '' THEN 1 ELSE 0 END
       AS CompletenessCheck
     , C.[COMPLETENESS_CHECK_DT] AS CompletenessDate
     , C.[CREATED_BY] AS CreatedBy
     , C.[CREATED_DT] CreatedDate
     , C.[CHANGED_BY]
     , C.[CHANGED_DT]
     ,CASE WHEN
        (SELECT COUNT(H.PRODUCTION_MONTH)
           FROM TB_R_PO_H H
          WHERE H.PRODUCTION_MONTH = C.PRODUCTION_MONTH
           AND 'F' = C.LPOP_TIMING
        ) > 0
        THEN 1
        ELSE 0
        END IsPO
FROM [TB_R_LPOP_CONFIRMATION] C
    LEFT JOIN (
        SELECT DISTINCT PROD_MONTH FROM TB_R_LPOP_NOTIFICATION
    ) B
    ON B.PROD_MONTH = C.PRODUCTION_MONTH
WHERE ISNULL(C.SOURCE_TYPE,'') <> 'SPRN'
  AND ((    NULLIF(@@production_month,'') is null
       and C.PRODUCTION_MONTH BETWEEN @@ThisMonth AND @@NextMonth)
   or (NULLIF(@@production_month,'') is not null
       and c.PRODUCTION_MONTH = @@production_month
       and isnull(C.[DELETION_FLAG], 0) != 1
      )
      )
 AND (NULLIF(@@TIMING,'') IS NULL
   OR @@TIMING = C.LPOP_TIMING)
