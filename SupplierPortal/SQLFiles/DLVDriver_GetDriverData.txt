﻿DECLARE @@sql VARCHAR(MAX)

	SET @@sql = ' SELECT DRIVER_ID, LP_CD, LOG_PARTNER_NAME AS LP_NAME, DRIVER_NAME, D.CREATED_BY, D.CREATED_DT, D.CHANGED_BY, D.CHANGED_DT
				  FROM TB_M_DLV_DRIVER D
				  LEFT JOIN TB_M_LOGISTIC_PARTNER L
				  ON D.LP_CD = L.LOG_PARTNER_CD 
				  WHERE 1 = 1 '
				  
	IF(@0 <> '')
	BEGIN
		SET @@sql = @@sql + ' AND LP_CD IN (' + @0 + ')'
	END
	
	IF(@1 <> '')
	BEGIN
		SET @@sql = @@sql + ' AND DRIVER_NAME LIKE ''%' + @1 + '%'' '
	END	
	
	SET @@sql = @@sql + 'ORDER BY CREATED_DT, CHANGED_DT'	  

EXEC(@@sql)	
 