SELECT [NOTIFICATION_ID] as Id
      ,[GROUP_ID] as GroupId
      ,[TITLE] as Title
      ,[CONTENT] as Content
      ,[AUTHOR] as Author
      ,[RECIPIENT] as Recipient
      ,[RECIPIENT_ROLE_ID] as RecipientRoleId
      ,[PRIORITY] as Priority
      ,[ACTION_URL] as ActionUrl
      ,[POST_DATE] as PostDate
      ,[READ_FLAG] as [Read]
  FROM [dbo].[TB_R_NOTIFICATION]
  WHERE [PRIORITY] = @0
  AND (CONVERT(VARCHAR(8), GETDATE(), 112) BETWEEN CONVERT(VARCHAR(8), VALID_FROM, 112) AND CONVERT(VARCHAR(8), VALID_TO, 112))