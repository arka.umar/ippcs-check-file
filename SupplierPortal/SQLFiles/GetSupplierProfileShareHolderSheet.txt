﻿Declare @@sql varchar(max),
        @@SuppCd varchar(max) = REPLACE(@0,';',''','''),
		@@SuppCdLdap varchar(max) = REPLACE(@1,';',''',''')
	
		
set @@sql = '
	SELECT 
		A.SUPPLIER_CD,
		(select top 1 substring(d.SUPP_NAME,1, case when patindex(''%(%'',d.SUPP_NAME)-1>0 
														then patindex(''%(%'',d.SUPP_NAME)-1
														else len(d.SUPP_NAME) end ) 
		 from TB_M_SUPPLIER_ICS d where d.SUPP_CD=a.SUPPLIER_CD) SUPPLIER_NAME,
		A.ITEM_NO,
		A.SHAREHOLDER as SHAREHOLDER_NAME,
		B.COUNTRY_NAME,
		A.SHARE_PERCENTAGE,
		A.CONFIRM_BY_SUPPLIER,
		CONVERT(VARCHAR(20), A.CONFIRM_DT_SUPPLIER, 104) + '' '' + CONVERT(VARCHAR(20), A.CONFIRM_DT_SUPPLIER, 114) CONFIRM_DT_SUPPLIER,
		A.CREATED_BY,
		CONVERT(VARCHAR(20), A.CREATED_DT, 104) + '' '' + CONVERT(VARCHAR(20), A.CREATED_DT, 114) CREATED_DT,
		A.CHANGED_BY,
		CONVERT(VARCHAR(20), A.CHANGED_DT, 104) + '' '' + CONVERT(VARCHAR(20), A.CHANGED_DT, 114) CHANGED_DT  
	FROM TB_M_SUPPLIER_SHAREHOLDER A
		LEFT JOIN TB_M_COUNTRY B 
			ON A.COUNTRY_CD = B.COUNTRY_CD
	WHERE 1=1 '

if(@@SuppCd <> '')
	set @@sql = @@sql + 'AND A.SUPPLIER_CD  IN ('''+ @@SuppCd +''') '
	
	
if(@@SuppCdLdap <> '')
	set @@sql = @@sql + 'AND A.SUPPLIER_CD  IN ('''+ @@SuppCdLdap +''') '

execute (@@sql)