﻿Declare @@sql varchar(max),
  @@DELIVERY_NO VARCHAR(max)= @0
	   
set @@sql =  'SELECT DELIVERY_NO, PROCESS_ID, ROUTE_MASTER_DESC, ROUTE_PRICE_DESC, LP_MASTER_DESC, SYSTEM_DESC, CREATED_BY, CREATED_DT FROM TB_T_DLV_ERROR_POSTING WHERE 1 = 1 '

if(@@DELIVERY_NO <> '')
	set @@sql = @@sql + 'and DELIVERY_NO = '+ cast(@@DELIVERY_NO as varchar) +' '

if(@@DELIVERY_NO <> '')
	execute (@@sql)

	