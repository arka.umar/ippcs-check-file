﻿DECLARE @@sql NVARCHAR(MAX) = ''


SET @@sql = '
			SELECT
					PACK_MONTH, 
					SUPPLIER_CD + ''-'' + S_PLANT_CD AS SS_PLANT_CD, 
					S.SUPPLIER_NAME,
					CAR_CD CFC,
					CASE WHEN (F.FEEDBACK_STATUS = 0) THEN
						''NG''
					WHEN (F.FEEDBACK_STATUS = 1) THEN
						''OK''
					ELSE
						'' ''
					END AS KEIHENVOLUME,
					
					CASE WHEN (ISNULL(FEEDBACK_BY, '''') = '''')
					THEN '' ''
					ELSE ''CONFIRMED''
					END AS CONFIRMATION,
					CASE WHEN (ISNULL(LOCK, '''') = '''')
					THEN ''UNLOCK''
					ELSE ''LOCK''
					END AS LOCK,
					FEEDBACK_BY,
					FEEDBACK_DT,
					ISNULL(DOWNLOADED_BY, '''') AS DOWNLOADED_BY

				FROM TB_R_KEIHEN_FEEDBACK F 
					LEFT JOIN TB_M_SUPPLIER S 
					ON F.SUPPLIER_CD = S.SUPPLIER_CODE AND F.S_PLANT_CD = S.SUPPLIER_PLANT_CD 
				WHERE 1=1 '

			IF(ISNULL(@0, '') != '')
			BEGIN
				SET @@sql = @@sql + ' AND SUPPLIER_CD = '''+@0+''' '
			END
			IF(ISNULL(@1, '') != '')
			BEGIN
				SET @@sql = @@sql + 'AND S_PLANT_CD = '''+@1+''''
			END
			IF(ISNULL(@2, '') != '')
			BEGIN
				SET @@sql = @@sql + 'AND PACK_MONTH = '''+@2+''''
			END


SET @@sql = @@sql + '
			GROUP BY F.PACK_MONTH,
				F.S_PLANT_CD,
				SUPPLIER_CD + ''-'' + S_PLANT_CD, 
				S.SUPPLIER_NAME, 
				SUPPLIER_CD,
				F.FEEDBACK_STATUS,
				LOCK,
				FEEDBACK_BY,
				FEEDBACK_DT,
				CAR_CD,
				DOWNLOADED_BY'
				
			;

exec sp_executesql @@sql


/*DECLARE @@sql NVARCHAR(MAX) = ''

SET @@sql = '
			SELECT 
				PACK_MONTH, 
				SS_PLANT_CD, 
				SUPPLIER_NAME,
				CAR_CD CFC,
				CASE WHEN (FEEDBACK_STATUS = TOTALDATA) --all data are OK
					THEN ''OK''
					WHEN (FEEDBACK_STATUS = 0) --belum ada feedback
					THEN '' ''
					WHEN (FEEDBACK_STATUS < TOTALDATA) --ada salah satu feedback yg NG
					THEN ''NG''
					END AS KEIHENVOLUME,
				CASE WHEN (ISNULL(FEEDBACK_BY, '''') = '''')
					THEN '' ''
					ELSE ''CONFIRMED''
					END AS CONFIRMATION,
				CASE WHEN (ISNULL(LOCK, '''') = '''')
					THEN ''UNLOCK''
					ELSE ''LOCK''
				END AS LOCK,
			
			
			FEEDBACK_BY, FEEDBACK_DT
			FROM
			(
			SELECT 
				NTABLE.PACK_MONTH,
				NTABLE.SS_PLANT_CD, 
				NTABLE.SUPPLIER_NAME, 
				CAR_CD,
				SUM(NTABLE.FEEDBACK_STATUS) AS FEEDBACK_STATUS,
				COUNT(1) TOTALDATA,
				LOCK,
				FEEDBACK_BY,
				FEEDBACK_DT 
			FROM
				(SELECT
					PACK_MONTH, 
					SUPPLIER_CD + ''-'' + S_PLANT_CD AS SS_PLANT_CD, 
					SUPPLIER_CD,
					CAR_CD,
					S.SUPPLIER_NAME,
					CASE
						WHEN (F.REPLY = 0) --reply OK dihitung 1
						THEN	1
						WHEN (ISNULL(F.REPLY, '''') = '''')
						THEN	0 --null berarti belum ada feedback
						WHEN (F.REPLY = 1) --reply NG dihitung -1
						THEN	-1 
						END 
					FEEDBACK_STATUS,
					LOCK,
					FEEDBACK_BY,
					FEEDBACK_DT 
				FROM TB_R_KEIHEN_FEEDBACK F 
					LEFT JOIN TB_M_SUPPLIER S 
					ON F.SUPPLIER_CD = S.SUPPLIER_CODE
				WHERE 1=1 '

			IF(ISNULL(@0, '') != '')
			BEGIN
				SET @@sql = @@sql + ' AND SUPPLIER_CD = '''+@0+''' '
			END
			IF(ISNULL(@1, '') != '')
			BEGIN
				SET @@sql = @@sql + 'AND S_PLANT_CD = '''+@1+''''
			END
			IF(ISNULL(@2, '') != '')
			BEGIN
				SET @@sql = @@sql + 'AND PACK_MONTH = '''+@2+''''
			END


SET @@sql = @@sql + '
				)AS NTABLE
			GROUP BY NTABLE.PACK_MONTH,
				NTABLE.SS_PLANT_CD, 
				NTABLE.SUPPLIER_NAME, 
				CAR_CD,
				LOCK,
				FEEDBACK_BY,
				FEEDBACK_DT
			) NNTABLE';

exec sp_executesql @@sql*/