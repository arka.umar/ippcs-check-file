﻿DECLARE @@INV_NO varchar(max) = @0,
	@@PARTNER_BANK varchar(max) = @1,
	@@BASELINE_DATE varchar(max) = @2,
	@@TAX_CODE varchar(max) = @3, 
	@@TERM_PAYMENT varchar(max) = @4, 
	@@PAYMENT_METHOD varchar(max) =@5,
	@@INV_NOTES varchar(max) = @6,
	@@CHANGED_BY varchar(10)= @7

UPDATE TB_R_DLV_INV_UPLOAD 
	SET 
	PARTNER_BANK_KEY = @@PARTNER_BANK,
	CHANGED_BY = @@CHANGED_BY,
	BASELINE_DT = @@BASELINE_DATE,
	PAYMENT_TERM_CD = @@TERM_PAYMENT,
	PAYMENT_METHOD_CD = @@PAYMENT_METHOD,
	CHANGED_DT = GETDATE() 
WHERE LP_INV_NO = @@INV_NO

UPDATE TB_R_DLV_INV_UPLOAD 
	SET INV_TAX_CD =  @@TAX_CODE,
	INV_NOTES = @@INV_NOTES
WHERE LP_INV_NO = @@INV_NO AND CONDITION_CATEGORY ='H'