﻿declare @@logid varchar(10)=@0,
		@@Processid varchar(20)=@1,
		@@ManifestNo varchar(20)=@2
		

select LOG_ID_H as logID, 
PROCESS_ID as processID, 
MANIFEST_NO as manifestNo,
CREATED_BY as createdBy, 
CREATED_DATE as createdDate 
from TB_R_LOG_TRANSACTION_H

where

((LOG_ID_H=@@logid AND isnull(@@logid,'') <> '') or (isnull(@@logid,'') = '')) 
and ((PROCESS_ID=@@Processid AND isnull(@@Processid,'') <> '') or (isnull(@@Processid,'') = '')) 
and ((MANIFEST_NO=@@ManifestNo AND isnull(@@ManifestNo,'') <> '') or (isnull(@@ManifestNo,'') = '')) 