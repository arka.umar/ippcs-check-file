﻿--- InsertSourceListMaintenance

set nocount on;

DECLARE
  @@MatNo       varchar(max) = @0
, @@SourceType  varchar(max) = @1
, @@ProdPurpose varchar(max) = @2
, @@ColorSuffix varchar(max) = @3
, @@SuppCD      varchar(max) = @4
, @@ValidFrom   varchar(max) = @5
, @@ValidTo     varchar(max) = @6
, @@creator     varchar(20)  = @7
, @@PID         bigint       = 0
, @@VF          DATETIME     = null
, @@VT          DATETIME     = null
, @@exists      int          = 0
, @@MSG         VARCHAR(MAX) = ''
, @@log         varchar(8000) = ''
, @@msgt        varchar(500) = '{0}'
, @@valid_fr    datetime
, @@valid_to    datetime
, @@currdate    datetime
, @@prevdate    datetime
, @@prevSuppCD  varchar(6) = NULL 
;

SELECT top 1 @@msgt=MESSAGE_TEXT from TB_M_MESSAGE where MESSAGE_ID = 'MPCS00003ERR'

if (@@MatNo = '' OR @@MatNo is null)
BEGIN
    SELECT @@MSG = replace(@@msgt,'{0}','MATERIAL_NO');
    RAISERROR(@@MSG, 16, 2)
    return;
END

if (@@SuppCD = '' OR @@SuppCD is null)
BEGIN
    SELECT @@MSG = replace(@@msgt,'{0}', 'Supplier Code');
    RAISERROR(@@MSG, 16, 2)
    return;
END

if (@@SourceType = '' OR @@SourceType is null)
BEGIN
    SELECT @@MSG = replace(@@msgt,'{0}', 'Source Type')
    RAISERROR(@@MSG, 16, 2)
    return;
END

if (@@ProdPurpose = '' OR @@ProdPurpose is null)
BEGIN
    SELECT @@MSG = replace(@@msgt,'{0}', 'Prod Purpose')
    RAISERROR(@@MSG, 16, 2)
    return;
END

if (@@ValidFrom = '' OR @@ValidFrom is null)
BEGIN
    SELECT @@MSG = replace(@@msgt,'{0}', 'Valid From')
    RAISERROR(@@MSG, 16, 2)
    return;
END

if (@@ColorSuffix = '' OR @@ColorSuffix is null)
BEGIN
    SELECT @@MSG = replace(@@msgt,'{0}', 'Part Color Suffix')
    RAISERROR(@@MSG, 16, 2)
    return;
END

select @@msgt = coalesce(
(SELECT top 1 MESSAGE_TEXT from TB_M_MESSAGE where MESSAGE_ID = 'MPCS00031ERR')
, '{0} is not in {1} format');

-- dd.mm.yyyy
-- 1234567890
if len(@@validFrom)<10 or isdate(substring(@@ValidFrom,7,4) + '-' + substring(@@validFrom, 4,2) + '-' + substring(@@validFrom, 1,2) ) = 0
BEGIN
    select @@msg = replace(replace(@@msgt, '{0}', 'Valid From'), '{1}', 'DATE');
    RAISERROR(@@MSG, 16, 2)
    Return;
END

-- SELECT CONVERT(VARCHAR(50), GETDATE(), 104) --dd.MM.yyyy

select @@valid_fr = dbo.tryCONVERTDATe(@@VALIDFROM)
    , @@VALID_TO = dbo.tryCONVERTDATE(@@ValidTo);
if @@valid_fr is null or @@valid_to is null 
begin
    SELECT @@MSG = 'DATE CONVERT ERROR ' + COALESCE(@@VALIDFROM, '') + ' ' + COALESCE(@@VALIDTO, ''); 
    RAISERROR(@@MSG, 16, 2);
    RETURN;
end; 

select @@currdate =  DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
SELECT @@prevdate = DATEADD(DAY,-1, dateadd(month, datediff(month, 0, @@valid_fr), 0));

SELECT TOP 1 @@VF = VALID_DT_FR, @@VT = VALID_DT_TO, @@exists = 1, @@prevSuppCD = SUPP_CD
FROM TB_M_SOURCE_LIST
WHERE MAT_NO = @@MatNo
AND SOURCE_TYPE = @@SourceType
AND PROD_PURPOSE_CD = @@ProdPurpose
AND PART_COLOR_SFX = @@ColorSuffix
ORDER BY VALID_DT_TO desc, VALID_DT_FR desc

if @@exists =1
begin
    if datediff(day, @@vf, @@valid_fr)<= 0
    begin
        SET @@MSG =
                (SELECT 'Material No: ' + COALESCE( @@MatNo ,'')
                + ', Source Type: ' + coalesce(@@SourceType ,'')
                + ', Prod Purpose: ' + coalesce(@@ProdPurpose ,'')
                + ', Supplier Code: ' + coalesce(@@prevSuppCD ,'')
                + ', Color Suffix: ' + coalesce(@@ColorSuffix ,'')
                + ' and Valid From: ' + coalesce(convert(varchar(10), @@Valid_Fr ,121),'')
                + ' already exists')
        RAISERROR(@@MSG, 16, 2)
        Return;
    end
    else IF datediff(day, @@valid_fr, @@currdate) >=0
    begin
        SELECT @@msg='Valid from ' + convert(varchar, @@valid_fr, 104)
              + ' must be greater than current month ' + convert(varchar(10), @@currdate, 104);
        RAISERROR(@@MSG, 16, 2)
        Return;
    end;
end;

select @@log=coalesce(@@MatNo, ' ')
    + ' ' + coalesce(@@SourceType, ' ')
    + ' ' + coalesce(@@ProdPurpose, ' ')
    + ' ' + coalesce(@@ColorSuffix, ' ')
    + ' ' + coalesce(@@suppCd, ' ')
    + ' ' + coalesce(Convert(varchar(10), @@vf, 121),' ') + '-'
    + ' ' + coalesce(convert(varchar(10), @@vt, 121), ' ') + ' '

 IF @@vf is not null and @@vt is not null and @@VF < @@VALID_FR
 BEGIN
    UPDATE X SET
         VALID_DT_TO =   @@prevdate
       , CHANGED_BY = @@creator
       , CHANGED_DT = getdate()
    FROM TB_M_SOURCE_LIST X
    WHERE MAT_NO = @@MatNo
    AND PROD_PURPOSE_CD = @@ProdPurpose
    AND SOURCE_TYPE = @@SourceType
    AND SUPP_CD = @@prevSuppCD
    AND PART_COLOR_SFX = @@ColorSuffix
    AND VALID_DT_FR = @@VF
    AND VALID_DT_TO = @@VT;
    if @@@ROWCOUNT > 0
        select @@log = @@log + ' *VALID_DT_TO ' + convert(varchar(10), @@prevdate, 121);
end;

MERGE INTO TB_M_SOURCE_LIST s
USING (SELECT
 MAT_NO=@@MatNo
,SOURCE_TYPE=@@SourceType
,PROD_PURPOSE_CD=@@ProdPurpose
,PART_COLOR_SFX=@@ColorSuffix
,SUPP_CD=@@SuppCD
,VALID_DT_FR=@@Valid_fr -- NEXT MONTH TANGGAL 1
,VALID_DT_TO=convert(datetime, '99991231', 112)
) l
ON L.MAT_NO = S.MAT_NO
AND L.SOURCE_TYPE =S.SOURCE_TYPE
AND L.PROD_PURPOSE_CD = S.PROD_PURPOSE_CD
AND L.PART_COLOR_SFX = S.PART_COLOR_SFX
AND L.SUPP_CD = S.SUPP_CD
AND L.VALID_DT_FR = S.VALID_DT_FR
WHEN NOT MATCHED THEN
INSERT(MAT_NO,
SOURCE_TYPE,
PROD_PURPOSE_CD,
PART_COLOR_SFX,
SUPP_CD,
VALID_DT_FR,
VALID_DT_TO,
CREATED_BY,
CREATED_DT
) VALUES(
L.MAT_NO,
L.SOURCE_TYPE,
L.PROD_PURPOSE_CD,
L.PART_COLOR_SFX,
L.SUPP_CD,
L.VALID_DT_FR,
L.VALID_DT_TO,
@@CREATOR,
GETDATE());

if @@@ROWCOUNT > 0
    select @@log = @@log + ' +VALID_DT_FR ' + CONVERT(VARCHAR(10), @@VALID_FR, 121);

exec PutLog @@log, @@creator, 'InsertSourceList',  @@pid output, 'MPCS0001INF', '00000', 0;