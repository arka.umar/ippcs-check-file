﻿/*fid.indrawan Jul-2013 */
DECLARE
	@@sqlQuery AS VARCHAR(MAX)

IF object_id('tempdb..#tmpsplitStringDockCD') IS NOT NULL DROP TABLE #tmpsplitStringDockCD

CREATE TABLE #tmpsplitStringDockCD
(
	column1 VARCHAR(MAX)
)

INSERT INTO #tmpsplitStringDockCD(column1)
EXEC dbo.SplitString @3, ';'

SET @@sqlQuery = 'SELECT DISTINCT TOP 100 PERCENT B.PART_NO AS MatNo, 
										B.PART_NAME AS MaterialDesc, 
										A.SUPPLIER_CD AS SupplierCd, 
										A.DOCK_CD AS DockCd, 
										substring(A.ORDER_NO,1,6) as ProdMonth
				   FROM [dbo].TB_R_DAILY_ORDER_MANIFEST A INNER JOIN 
						[dbo].TB_R_DAILY_ORDER_PART B ON A.MANIFEST_NO = B.MANIFEST_NO LEFT OUTER JOIN
						[dbo].TB_T_COMPLETENESS_CHECK_RESULT C ON B.PART_NO = C.PART_NO
				   WHERE 
						 substring(A.ORDER_NO,1,6) = isnull('''+@0+''','''')'

				IF(@1<>'')
				BEGIN
					SET @@sqlQuery = @@sqlQuery + 'AND B.PART_NO = isnull('''+@1+''','''')'
				END

				IF(@2<>'')
				BEGIN
					SET @@sqlQuery = @@sqlQuery + ' AND B.PART_NAME = isnull('''+@2+''','''')'
				END

				IF(@3<>'')
				BEGIN
					SET @@sqlQuery = @@sqlQuery + 'AND ((A.DOCK_CD IN (select column1 from #tmpsplitStringDockCD) AND isnull('''+@3+''', '''') <> '''') or (isnull('''+@3+''', '''') = ''''))'
				END

SET @@sqlQuery = @@sqlQuery + 'ORDER BY ProdMonth DESC'
EXEC(@@sqlQuery)