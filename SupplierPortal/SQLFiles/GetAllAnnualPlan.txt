﻿/* Get all data from annual plan header table (tb_r_annual_plan_h). fid.salman 07-feb-2013 */
/* Update By : FID/Lita 04-Apr-2013 (Add Parameter) */


declare @@sql nvarchar(max), 
		@@year varchar(10) = @0,
		@@version varchar(10) = @1,
		@@suppliers varchar(50)=@2,
		@@annualPlanOf varchar(50)=@3
		
set @@suppliers = REPLACE(@@suppliers,';',''',''');
		
set @@sql = '
	SELECT H.[SUPPLIER_CD]
		  ,H.[PRODUCTION_YEAR]
		  ,H.[VERSION]
		  ,H.[FEEDBACK_BY]
		  ,H.[FEEDBACK_DT]
		  ,H.[FEEDBACK_STATUS]
		  ,H.[NOTICE_ID]
		  ,H.[CHANGED_BY]
		  ,H.[CHANGED_DT]
		  ,MAX(D.CATEGORY_CD) AS ANNUAL_PLAN_OF
		  ,(
			SELECT COUNT(*) 
			FROM [TB_T_ANNUAL_CROSSTAB_D] D2 
			WHERE 
				D2.AP_YEAR = H.PRODUCTION_YEAR
				AND D2.VERSION = H.VERSION 
				AND D2.SUPPLIER_CD = H.SUPPLIER_CD
			) AS TOTAL_RECORD
	FROM [dbo].[TB_T_ANNUAL_CROSSTAB_H] H
	JOIN
		[dbo].[TB_T_ANNUAL_CROSSTAB_D] D
		ON H.PRODUCTION_YEAR = D.AP_YEAR
		AND H.VERSION = D.VERSION
	WHERE 1=1 ';
			
if(isnull(@@year,'') <> '')
	set @@sql = @@sql + 'AND H.PRODUCTION_YEAR = ''' + @@year + ''' '

if(isnull(@@version,'') <> '')
	set @@sql = @@sql + 'AND H.VERSION = '''+ @@version + ''' '

if(isnull(@@suppliers,'') <> '')
	set @@sql = @@sql + 'AND H.SUPPLIER_CD in (''' + @@suppliers + ''') '

SET @@sql = @@sql + '
	GROUP BY
		H.[SUPPLIER_CD]
		,H.[PRODUCTION_YEAR]
		,H.[VERSION]
		,H.[FEEDBACK_BY]
		,H.[FEEDBACK_DT]
		,H.[FEEDBACK_STATUS]
		,H.[NOTICE_ID]
		,H.[CHANGED_BY]
		,H.[CHANGED_DT]'

if(isnull(@@annualPlanOf,'') <> '')
	set @@sql = @@sql + 'HAVING MAX(D.CATEGORY_CD) = ''' + @@annualPlanOf + '''  '
			
execute (@@sql)
