﻿select TOP 1
	DeliverNo = h.DELIVERY_NO,
	RevisionNo = h.REVISE_NO,
	PickUpDate = convert(VARCHAR(10), h.PICKUP_DT, 104),
	ROUTERATE = h.ROUTE + ' - ' + h.RATE,
	DriverName = h.DRIVER_NAME,
	DriverManifest = '',
	DownloadBy = h.DOWNLOAD_BY,
	DownloadTime = h.DOWNLOAD_DT,
	LogisticPartner = h.LP_CD,
	ApprovalStatus = '',
	DeliveryStatus = h.DELIVERY_STS,
	ArrivalAt = convert(VARCHAR(10), d.ARRIVAL_GATE_DT, 104) ,
	LPConfirmation = '',
	Notice = '',
	DeliveryReason = '',
	ReviseBy = h.CHANGED_BY,
	ReviseDate = h.CHANGED_DT,
	Approver = h.APPROVE_BY,
	ApprovalDate = h.APPROVE_DT,
	Status = '',
    DeliveryBy = '',
    DeliveryDate = getdate(),
    InvoiceBy = h.INVOICE_BY,
    InvoiceDate = h.INVOICE_DT,
    Requisitioner = h.REQUISITIONER_BY,
    RequisitionDate = h.REQUISITIONER_DT,
    LPName = ''
from TB_R_DELIVERY_CTL_H h
inner join TB_R_DELIVERY_CTL_D d on h.DELIVERY_NO=d.DELIVERY_NO
Where h.PUSH_TO_WEB_FLAG='1' and  h.DELIVERY_NO=@0
order by d.DOCK_CD,d.STATION,d.ARRIVAL_PLAN_DT