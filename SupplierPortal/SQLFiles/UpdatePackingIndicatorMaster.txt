﻿DECLARE 
  @@MatNo		varchar (max) = @0
, @@PackingType	varchar (max) = @1
, @@ValidFrom	varchar (max) = @2
, @@username    varchar(20) = @3
, @@source_type varchar(1) = @4 
, @@prod_purpose_cd varchar(5) = @5 
, @@part_color_sfx varchar(2) = @6 
, @@dock_cd varchar(6) = @7
, @@VAlid_dt_fr datetime =null;

DECLARE @@MSG VARCHAR(MAX) = ''
set @@valid_dt_fr = dbo.tryCONVERTdate(@@ValidFrom);
if @@valid_dt_fr is null 
BEGIN
	set @@MSG = (select 'Invalid data type of Valid From. Should be "dd.mm.yyyy".' )
	RAISERROR(@@MSG, 16, 2)
	Return;
END

UPDATE [dbo].[TB_M_PACKING_INDICATOR]
SET PACKING_TYPE_CD = @@PackingType, 
	CHANGED_BY = @@Username, 
	CHANGED_DT =GETDATE()
WHERE MAT_NO = @@MatNo
	AND SOURCE_TYPE = @@source_type
    AND PROD_PURPOSE_CD = @@prod_purpose_cd
	AND	PART_COLOR_SFX = @@part_color_sfx
	AND DOCK_CD = @@dock_cd 
    AND VALID_DT_FR= @@VALID_DT_FR
