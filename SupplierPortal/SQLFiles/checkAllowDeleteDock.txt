﻿DECLARE @@tableDock TABLE (dock VARCHAR(max))
DECLARE @@rowCount INT = 0;
DECLARE @@allowed INT = 0;

INSERT INTO @@tableDock SELECT s FROM [dbo].fnSplitString(@0,',')

SELECT @@rowCount = COUNT(1) FROM TB_R_DAILY_ORDER_MANIFEST M
	WHERE EXISTS (SELECT 'x' FROM @@tableDock t WHERE M.DOCK_CD = t.dock)

IF(@@rowCount > 0)
BEGIN
	SET @@allowed = @@allowed + 1;
END

SELECT @@rowCount = COUNT(1) FROM TB_R_DELIVERY_CTL_D D
	WHERE EXISTS (SELECT 'x' FROM @@tableDock t WHERE D.DOCK_CD = t.dock)

IF(@@rowCount > 0)
BEGIN
	SET @@allowed = @@allowed + 1;
END

SELECT @@rowCount = COUNT(1) FROM TB_R_GR_IR I
	WHERE EXISTS (SELECT 'x' FROM @@tableDock t WHERE I.DOCK_CD = t.dock)

IF(@@rowCount > 0)
BEGIN
	SET @@allowed = @@allowed + 1;
END

SELECT @@rowCount = COUNT(1) FROM TB_R_LPOP L
	WHERE EXISTS (SELECT 'x' FROM @@tableDock t WHERE L.DOCK_CD = t.dock)

IF(@@rowCount > 0)
BEGIN
	SET @@allowed = @@allowed + 1;
END

IF(@@allowed > 0)
BEGIN
	SELECT 'NOT ALLOWED'
END
ELSE
BEGIN
	SELECT 'ALLOWED'
END
