﻿INSERT INTO TB_R_DELIVERY_CTL_MANIFEST 
(DELIVERY_NO,
DOCK_CD,
STATION,
MANIFEST_NO,
MANIFEST_TYPE,
CREATED_BY,
CREATED_DT,
CHANGED_BY,
CHANGED_DT)
VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8)