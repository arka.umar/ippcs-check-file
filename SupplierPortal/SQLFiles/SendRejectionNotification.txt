﻿-- SendRejectionNotification

DECLARE @@folio VARCHAR(MAX) = @0,
	@@comment VARCHAR(MAX) = @1, 
	@@userName VARCHAR(50) = @2,
	@@rejectType INT = @3,
	@@module INT = @4;

EXEC dbo.sp_RejectionNotification @@module, @@rejectType, @@folio,  @@userName, @@comment;