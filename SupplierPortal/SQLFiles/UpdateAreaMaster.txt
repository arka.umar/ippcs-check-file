
	BEGIN
	
		UPDATE TB_M_AREA_MASTER SET 
		AREA_NM=  @1
		,PLANT=@2
		,PIC_NAME=@3
		,EMAIL_TO=@4
		,EMAIL_CC=@5
		,EMAIL_BCC=@6
	    ,TELEPHONE=@7
  		,MOBILE=@8
		,CREATED_BY=@9
		,CHANGED_DT=getdate()
		WHERE AREA_CD=@0

		select 'ProcessSuccesfully'
 END