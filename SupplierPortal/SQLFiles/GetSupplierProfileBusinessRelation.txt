﻿Declare @@sql varchar(max),
        @@SuppCd varchar(max) = REPLACE(@0,';',''',''')
	
		
set @@sql = 'SELECT A.SUPPLIER_CD,
(select top 1 substring(d.SUPP_NAME,1, case when patindex(''%(%'',d.SUPP_NAME)-1>0 then patindex(''%(%'',d.SUPP_NAME)-1
  else len(d.SUPP_NAME) end ) from TB_M_SUPPLIER_ICS d where d.SUPP_CD=a.SUPPLIER_CD )SUPPLIER_NAME
  ,A.ITEM_NO,
  CONFIRM_BY_SUPPLIER,
CONFIRM_DT_SUPPLIER,
A.COMPANY_NAME,A.CONTENT_OF_COOPERATION,A.COMPANY_CATEGORY
,A.CHANGED_BY,A.CHANGED_DT,A.CREATED_BY,A.CREATED_DT  
 FROM TB_M_SUPPLIER_BUSINESS_RELATION A
	where 1=1 '

if(@@SuppCd <> '')
	set @@sql = @@sql + 'and A.SUPPLIER_CD  IN ('''+ @@SuppCd +''') '


execute (@@sql)