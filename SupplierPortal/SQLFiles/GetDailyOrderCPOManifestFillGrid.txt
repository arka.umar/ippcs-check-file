﻿declare @@sql nvarchar(max), 
		@@orderReleaseDate varchar(10) = @0,
		@@supplierCode varchar(15) = @1
		
SELECT [MANIFEST_NO] Manifest_Number,
		[ORDER_NO] Order_Number, 
		[SUPPLIER_PLANT] Supplier_Plant, 
		[RCV_PLANT_CD] Receive_Plant_Code, 
		[DOCK_CD] DockCode,
		[TOTAL_ITEM], 
		[TOTAL_QTY], 
		[ARRIVAL_PLAN_DT], 
		[NOTIFY_ID]
FROM [dbo].[TB_R_DAILY_ORDER_MANIFEST]
	where [TB_R_DAILY_ORDER_MANIFEST].ORDER_RELEASE_DT=@@orderReleaseDate
		and [TB_R_DAILY_ORDER_MANIFEST].SUPPLIER_CD=@@supplierCode
		and [TB_R_DAILY_ORDER_MANIFEST].ORDER_TYPE='2'
