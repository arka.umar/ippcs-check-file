select
HR.CERTIFICATE_ID CertificateID,
convert(varchar(max),isnull(Rtrim(HR.SUPP_CD),'')) SupplierCode,
Sup.SUPP_CD SupplierCode,
convert(varchar(max),isnull(Rtrim(Sup.SUPP_NAME),'')) SupplierName,
HR.SUPP_INV_NO InvoiceNo,
parsename(convert(varchar(50),CAST(HR.INV_AMT_TOTAL as money),3),2) InvoiceAmount,
convert(varchar(max),HR.INV_TAX_NO) InvoiceTaxNo,
parsename(convert(varchar(50),CAST(HR.INV_TAX_AMT as money),3),2) InvoiceTaxAmount,
HR.CREATED_DT ReceiveDate,
-- CONVERT(VARCHAR(10), HR.SUBMIT_DT, 104) as SubmitDate,
HR.SUBMIT_DT as SubmitDate,
HR.SUBMIT_BY SubmitBy,
HR.CURR_CD Currency,
HR.STATUS_CD Status,
HR.REASON Notice
from TB_H_INV_RECEIVING HR
inner join TB_M_SUPPLIER_ICS Sup
on Sup.SUPP_CD = HR.SUPP_CD
--added fid.deny 2015-06-30
WHERE CAST(HR.SUBMIT_DT AS DATE) = CAST(GETDATE() AS DATE)
--end of added
order by HR.CREATED_DT desc