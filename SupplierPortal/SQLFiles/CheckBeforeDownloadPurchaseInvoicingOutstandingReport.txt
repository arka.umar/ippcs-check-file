DECLARE
	@@vendorCode VARCHAR(100) = @0,
	@@vendorName VARCHAR(100) = @1,
	@@periodFromDt VARCHAR(100) = @2,
	@@periodToDt VARCHAR(100) = @3,
	@@countData INT

SELECT
	@@countData = COUNT(1)
FROM (select PO_NO=max(a.PO_NO),	
		   Category=max(a.Category),	
		   Condition_type= max(a.Condition_type),	
		   Currency = MAX(a.Currency),																																																																							
		   SuppCode=MAX(a.SuppCode),
		   SuppName=MAX(a.SuppName),																																																																												
		   GR_IR_AMT=sum(a.GR_IR_AMT)
    from (
		SELECT PO_NO=max(g.PO_NO),	
			Category=max(x.Category),	
			Condition_type= max(g.COMP_PRICE_CD),	
			Currency = MAX(g.ITEM_CURR),																																																																							
			SuppCode=MAX(g.SUPPLIER_CD),
			SuppName=MAX(si.SUPP_NAME),																																																																												
			GR_IR_AMT=max(cast(GR_IR_AMT as float))	
			FROM [dbo].TB_R_GR_IR g
			INNER JOIN TB_M_SUPPLIER_ICS si ON g.SUPPLIER_CD=si.SUPP_CD
			LEFT JOIN dbo.[TB_T_INV_UPLOAD] t																																																																											
						ON t.SUPP_CD = g.SUPPLIER_CD 																																																																										
						AND t.PO_NO = g.PO_NO																																																																										
						AND t.MAT_DOC_NO = g.MAT_DOC_NO																																																																										
						AND t.COMP_PRICE_CD = g.COMP_PRICE_CD																																																																										
						AND t.MAT_DOC_ITEM = CONVERT(VARCHAR, g.ITEM_NO)																																																																										
						AND t.INV_REF_NO = G.INV_REF_NO																																																																									
				CROSS APPLY (VALUES(CASE WHEN LEFT(g.PO_NO,2) = 45 THEN 'Component'
									 WHEN  LEFT(g.PO_NO,2) = 11 THEN 'Service Part'
									 ELSE 'Steel Sheet' END)) As x(Category)																																																																											
				WHERE 1=1	
				and((@@vendorCode  IS NULL OR LEN(@@vendorCode) < 1) OR g.SUPPLIER_CD IN (select s from dbo.fnSplitString(@@vendorCode, ',')))
				AND (NULLIF(@@vendorName, '') IS NULL OR si.SUPP_NAME LIKE '%' + @@vendorName + '%')
				AND	(((@@periodFromDt IS NULL) OR (@@periodToDt IS NULL)) OR (g.DOC_DT BETWEEN @@periodFromDt AND @@periodToDt))
				AND g.CANCEL_FLAG = 0
					--AND (g.STATUS_CD in (-3, -1, 1))																																																																												
					--AND g.PLANT_CD='3000' AND g.SLOC_CD='5000'
				and g.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
				GROUP BY G.INV_REF_NO, g.SUPPLIER_CD
	)a
	GROUP BY a.SuppCode)GR
left JOIN (
select 
	SuppCode, 
    SuppName = max(SuppName),
	Category= max(Category),
	Condition_type = max(Condition_type),
    Currency = max(Currency),
	InvOutstandingInq = sum(cast(InvoiceAmount as float)), 
	PO_NO=max(PO_NO)
from (
	SELECT Qry.*																																																																										
		FROM(																																																																										
			SELECT 
			    PO_NO = MAX(tu.PO_NO),	
				Category=max(x.Category),
				Condition_type= max(tu.COMP_PRICE_CD),	
				Currency = MAX(tu.CURR_CD),																																																																										
				SuppCode=MAX(tu.SUPP_CD),	
				SuppName=MAX(si.SUPP_NAME),																																																																							
				InvoiceAmount=MAX(tu.INV_AMT_TOTAL),
				InvDt = MAX(tu.INV_DT)																																																																						
			FROM TB_R_INV_UPLOAD tu																																																																									
			INNER JOIN TB_M_SUPPLIER_ICS si ON tu.SUPP_CD=si.SUPP_CD																																																																									
			LEFT JOIN (																																																																									
				select SUPP_INVOICE_NO,SUPP_CD,INV_AMT=SUM(INV_AMT)																																																																								
				from TB_T_INV_GL_ACCOUNT 																																																																								
				where  TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE')																																																																								
				group by SUPP_INVOICE_NO,SUPP_CD --modif by alira.agi 2015-04-27																																																																								
			)t ON tu.SUPP_INV_NO=t.SUPP_INVOICE_NO and tu.SUPP_CD=t.SUPP_CD --modif by alira.agi 2015-04-27	
			CROSS APPLY (VALUES(CASE WHEN LEFT(tu.PO_NO,2) = 45 THEN 'Component'
						 WHEN  LEFT(tu.PO_NO,2) = 11 THEN 'Service Part'
						 ELSE 'Steel Sheet' END)) As x(Category)																																																																								
			WHERE 1=1																																																																																																																																					
				and TU.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')	
				AND ((tu.SUPP_CD =@@vendorCode) AND isnull(@@vendorCode,'') <> '') or (isnull(@@vendorCode,'') = '') 																																																																									
			    AND tu.INV_DT BETWEEN CONVERT(VARCHAR(10),CAST(@@periodFromDt AS DATETIME),23) AND CONVERT(VARCHAR(10),CAST(@@periodToDt AS DATETIME),23)																																																																																																																																																											
				and STATUS_CD not in (4,5)																																																																								
			GROUP by tu.SUPP_CD																																																																									
		)Qry																																																																										
		UNION																																																																										
		SELECT Qry.*																																																																										
		FROM(																																																																										
			SELECT
				PO_NO = MAX(z.PO_NO),	
				Category=max(x.Category),	
				Condition_type= max(COMP_PRICE_CD),	
				Currency = MAX(th.CURRENCY_CD),																																																																							
				SuppCode=MAX(th.SUPPLIER_CD),
				SuppName=MAX(si.SUPP_NAME),																																																																												
				InvoiceAmount=MAX(cast(th.INV_AMT as float)),	
				InvDt = MAX(th.INV_DT)																																																																							
			FROM TB_R_INV_H th																																																																									
			INNER JOIN TB_M_SUPPLIER_ICS si ON th.SUPPLIER_CD=si.SUPP_CD																																																																									
			INNER JOIN TB_R_INV_TAX tx ON th.INV_NO=tx.INVOICE_NO																																																																									
			left JOIN TB_R_INV_D z ON th.INV_NO = z.INV_NO																																																																									
			LEFT JOIN (																																																																									
				select INV_NO,INV_AMT=SUM(cast(INV_AMT as float))																																																																								
				from TB_R_INV_GL_ACCOUNT 																																																																								
				where TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE') 																																																																								
				group by INV_NO																																																																								
			)t ON th.INV_NO=t.INV_NO	
			CROSS APPLY (VALUES(CASE WHEN LEFT(z.PO_NO,2) = 45 THEN 'Component'
						 WHEN  LEFT(z.PO_NO,2) = 11 THEN 'Service Part'
						 ELSE 'Steel Sheet' END)) As x(Category)																																																																											
			WHERE 
				Z.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
				and((@@vendorCode IS NULL OR LEN(@@vendorCode) < 1) OR th.SUPPLIER_CD IN (select s from dbo.fnSplitString(@@vendorCode, ',')))
				AND (NULLIF(@@vendorName, '') IS NULL OR si.SUPP_NAME LIKE '%' + @@vendorName + '%')
				AND	(((@@periodFromDt IS NULL)  OR  (@@periodToDt IS NULL)) OR (th.INV_DT BETWEEN @@periodFromDt AND @@periodToDt))
				and (ACCT_DOC_NO = 'null' or ACCT_DOC_NO is null)																																																																						
			GROUP BY th.SUPP_INV_NO																																																																								
		)Qry																																																																										
		WHERE	1 = 1--
)a group by SuppCode		
)tuth
on tuth.PO_NO = GR.PO_NO

IF @@countData > 0
BEGIN
	SELECT 'S|Success';
END
ELSE
BEGIN
	SELECT 'E|MSTD00150ERR: No Data Found'
END