﻿IF EXISTS(SELECT '' FROM TB_R_DAILY_ORDER_WEB 
			where Convert(varchar(8),ORDER_RELEASE_DT,112)=Convert(varchar(8),@0,112) AND 
				SUPPLIER_CD = @1 AND 
				SUPPLIER_PLANT_CD = @2 AND
				RCV_PLANT_CD = @4 AND 
				DOWNLOAD_FLAG=1)
BEGIN
	update TB_R_DAILY_ORDER_WEB set 
		DOWNLOAD_FLAG = 0,
		DOWNLOADED_BY=null,
		DOWNLOADED_DT=null
	where Convert(varchar(8),ORDER_RELEASE_DT,112)=Convert(varchar(8),@0,112) AND 
		SUPPLIER_CD = @1 AND 
		SUPPLIER_PLANT_CD = @2 AND
		RCV_PLANT_CD = @4
END
ELSE
BEGIN
	DECLARE
		@@PROC_ID VARCHAR(12),
		@@PROCESS_ID BIGINT,
		@@LAST_PROCESS_ID VARCHAR(12),
		@@TODAY VARCHAR(8),
		@@NOW DATETIME,
		@@CURR_NO INT,
		@@CURR_PROCESS_ID VARCHAR(12),
		@@SEQ_NO_LOG BIGINT,
		@@LOCATION VARCHAR(100),
		@@MSG_LOG VARCHAR(MAX);

	SET @@LOCATION = 'Order Inquiry Screen - UnDownload Flag Order';
	SET @@SEQ_NO_LOG = 1;
	SET @@NOW = SYSDATETIME();
	SELECT @@PROCESS_ID = dbo.fn_get_last_process_id();
	
	EXEC dbo.spInsertLogHeader @@PROCESS_ID, 21005, 2, @@NOW, @@NOW, 5, 'SYSTEM';
	
	-- ** INSERT TO LOG DETAIL - START LOG **
	EXEC dbo.spInsertLogDetail @@PROCESS_ID, 'MPCS00002INF', 'INF', 
		'Process download order PDF is started', @@LOCATION, 'SYSTEM', @@SEQ_NO_LOG;

	SET @@MSG_LOG = 'Order Release Date ' + Convert(varchar(8),@0,112) + ' and Supplier Code ' + @1 + ' and Supplier Plant ' + @2 + ' and Rcv Plant Code ' + @4 + ' is succesfully UndownloadedFlag';
	EXEC dbo.spInsertLogDetail @@PROCESS_ID, 'MPCS00005INF', 'INF', @@MSG_LOG, @@LOCATION, 'SYSTEM';
		
	EXEC dbo.spInsertLogDetail @@PROCESS_ID, 'MPCS00003INF', 'INF', 
		'Process download order PDF is finished', @@LOCATION, 'SYSTEM';
		
	UPDATE dbo.TB_R_LOG_H 
	SET END_DATE = SYSDATETIME(), PROCESS_STATUS = 1
	WHERE PROCESS_ID = @@PROCESS_ID;
END