﻿DECLARE
	@@PROC_ID VARCHAR(12),
	@@PROCESS_ID BIGINT,
	@@LAST_PROCESS_ID VARCHAR(12),
	@@TODAY VARCHAR(8),
	@@NOW DATETIME,
	@@CURR_NO INT,
	@@CURR_PROCESS_ID VARCHAR(12)
SELECT @@TODAY = CONVERT(VARCHAR(8), GETDATE(), 112);

--GET LAST PROCESS ID
SELECT @@LAST_PROCESS_ID = ISNULL(MAX(PROCESS_ID), '1') FROM dbo.TB_R_LOG_H
WHERE SUBSTRING(CAST(PROCESS_ID AS VARCHAR(12)), 1, 8) = @@TODAY;

IF @@LAST_PROCESS_ID = '1'
BEGIN
	SELECT @@CURR_NO = 1;
END
ELSE
BEGIN
	SELECT @@CURR_NO = CAST(SUBSTRING(@@LAST_PROCESS_ID,9,LEN(@@LAST_PROCESS_ID)) AS INT) + 1;
END;

--GET NEXT PROCESS ID
SELECT @@CURR_PROCESS_ID = RIGHT(REPLICATE('0', 4)+ CAST(@@CURR_NO AS VARCHAR(4)), 4);
SELECT @@PROC_ID = @@TODAY + @@CURR_PROCESS_ID;
SELECT @@PROCESS_ID = CAST(@@PROC_ID AS BIGINT);

insert into TB_R_LOG_H(PROCESS_ID, 
					   FUNCTION_ID, 
					   MODULE_ID, 
					   START_DATE, 
					   END_DATE, 
					   PROCESS_STATUS,
					   CREATED_BY, 
					   CREATED_DATE)
VALUES  ( @@PROCESS_ID ,
          @0 ,
          @1 ,
          @2 ,
          @3,
          @4,
          @5,
          GETDATE()
        )
        
SELECT @@PROCESS_ID