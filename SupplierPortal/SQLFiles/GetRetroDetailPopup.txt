﻿-- =================================================
-- Author: niit.arie
-- Create date: 26 Oct 2013
-- Update date: 26 Oct 2013
-- Description: Search popup detail by RETRO_DOC_NO
-- =================================================

SELECT DISTINCT [RETRO_DOC_NO] AS RetroDocNo
	,[MAT_NO] AS MaterialNo
	,[PART_COLOR_SFX] AS PartColorSfx
	,[QUANTITY] AS Quantity
	,[NEW_MAT_PRICE] AS NewMaterialPrice
    ,[OLD_MAT_PRICE] AS OldMaterialPrice
    ,[PC_NO] AS PcNo
	,[PC_DT] AS PcDate
FROM [dbo].[TB_R_RETRO_D]
WHERE
	[RETRO_DOC_NO] = @0