SELECT [UPLOAD_ID] as 'Id'
      ,[GROUP] as 'Group'
      ,[KEY] as 'Key'
      ,[QUALIFIER] as 'Qualifier'
      ,[PATH] as 'Path'
      ,[FILENAME] as 'Filename'
      ,[DESCRIPTION] as 'Description'
  FROM [dbo].[TB_R_FILE_UPLOAD]
  where ([UPLOAD_ID] = @0)