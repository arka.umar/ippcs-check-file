﻿DECLARE @@sql NVARCHAR(MAX) = ''


SET @@sql = '
			SELECT
					Count(1) AS TOTAL
				FROM TB_R_KEIHEN_FEEDBACK F 
				WHERE 1=1 '

			IF(ISNULL(@0, '') != '')
			BEGIN
				SET @@sql = @@sql + ' AND SUPPLIER_CD = '''+@0+''' '
			END
			IF(ISNULL(@1, '') != '')
			BEGIN
				SET @@sql = @@sql + 'AND S_PLANT_CD = '''+@1+''''
			END
			IF(ISNULL(@2, '') != '')
			BEGIN
				SET @@sql = @@sql + 'AND PACK_MONTH = '''+@2+''''
			END


exec sp_executesql @@sql