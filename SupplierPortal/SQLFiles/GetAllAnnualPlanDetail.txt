﻿/* Get all data from annual plan detail table (tb_t_from_apps_D). fid.salman 14-feb-2013 */

declare @@sql nvarchar(max),
 
		@@year varchar(10) = @0,
		@@version varchar(10) = @1,
		@@supplier varchar(50)=@2
		
set @@sql = '
	SELECT
		  [AP_YEAR]
		  ,[VERSION]
		  ,[PART_NO]
		  ,[COLOR_SUFFIX]
		  ,[PART_NO_SUFFIX]
		  ,[CAR_FAMILY_CODE]
		  ,[KANBAN_SIZE]
		  ,[SUPPLIER_CD]
		  ,[SUPPLIER_PLANT_CD]
		  ,[SUPPLIER_NAME]
		  ,[PART_NAME]
		  ,[PART_MATCH_KEY]
		  ,[PLANT_CD]
		  ,[MONTH1]
		  ,[MONTH2]
		  ,[MONTH3]
		  ,[MONTH4]
		  ,[MONTH5]
		  ,[MONTH6]
		  ,[MONTH7]
		  ,[MONTH8]
		  ,[MONTH9]
		  ,[MONTH10]
		  ,[MONTH11]
		  ,[MONTH12]
		  ,[MONTH13]
		  ,[MONTH14]
		  ,[MONTH15]
		  ,[MONTH16]
		  ,[MONTH17]
		  ,[MONTH18]
		  ,[MONTH19]
		  ,[MONTH20]
		  ,[MONTH21]
		  ,[MONTH22]
		  ,[MONTH23]
		  ,[MONTH24]
		  ,[MONTH25]
		  ,[MONTH26]
		  ,[MONTH27]
		  ,[MONTH28]
		  ,[MONTH29]
		  ,[MONTH30]
		  ,[MONTH31]
		  ,[MONTH32]
		  ,[MONTH33]
		  ,[MONTH34]
		  ,[MONTH35]
		  ,[MONTH36]
	FROM [TB_R_ANNUAL_PLAN_D]
	WHERE 1=1 ';
			
if(isnull(@@year,'') <> '')
	set @@sql = @@sql + 'AND AP_YEAR = ''' + @@year + ''' '

if(isnull(@@version,'') <> '')
	set @@sql = @@sql + 'AND VERSION = '''+ @@version + ''' '

if(isnull(@@supplier,'') <> '')
	set @@sql = @@sql + 'AND SUPPLIER_CD = ''' + @@supplier + ''' '
	
execute (@@sql)
