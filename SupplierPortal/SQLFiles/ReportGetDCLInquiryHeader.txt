﻿	SELECT 
		TOP 1 
		a.DELIVERY_NO  as DeliveryNo,
		a.PICKUP_DT as PickupDate,a.REVISE_NO as ReviseNo,
		a.ROUTE as RouteCode, c.LOG_PARTNER_NAME as LPName, a.DOWNLOAD_BY as DownloadBy, 
		a.DRIVER_NAME as DriverName, a.RATE as Truck,GETDATE() as DownloadDate
		from 
		TB_R_DELIVERY_CTL_H as a LEFT OUTER JOIN
		TB_R_DELIVERY_CTL_MANIFEST as b 
		on a.DELIVERY_NO = b.DELIVERY_NO LEFT OUTER JOIN
		TB_M_LOGISTIC_PARTNER as c 
		on a.LP_CD = c.LOG_PARTNER_CD 
		WHERE a.DELIVERY_NO = '@0'