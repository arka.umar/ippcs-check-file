if exists(select * from TB_M_SUPPLIER_EQUIPMENT
	where SUPPLIER_CD=@0 and ITEM_NO=@1)
BEGIN
	update TB_M_SUPPLIER_EQUIPMENT set 
	[MACHINE_NAME]=@2
           ,[DETAIL_TYPE]=@3
           ,[TONAGE]=@4
           ,[UNIT]=@5
           ,[BRAND_NAME]=@6
           ,[QTY]=@7
           ,[YEAR_BUYING]=@8
           ,[YEAR_MAKING]=@9
           ,[REMARK]=@10
      ,CHANGED_BY=@11
      ,CHANGED_DT=getdate()
	where SUPPLIER_CD=@0 and ITEM_NO=@1
END
else
BEGIN

INSERT INTO TB_M_SUPPLIER_EQUIPMENT
         ([SUPPLIER_CD]
           ,[MACHINE_NAME]
           ,[DETAIL_TYPE]
           ,[TONAGE]
           ,[UNIT]
           ,[BRAND_NAME]
           ,[QTY]
           ,[YEAR_BUYING]
           ,[YEAR_MAKING]
           ,[REMARK]
           ,[CREATED_BY]
           ,[CREATED_DT]
           ,[CHANGED_BY]
           ,[CHANGED_DT]
		   ,CONFIRM_FLAG)
VALUES  ( @0 , -- SUPPLIER_CD 
          @2 , 
          @3 , 
          @4 , 
          @5 , 
          @6 ,
		  @7 ,
		  @8 ,
		  @9 ,
		  @10 ,
		  @11 ,
          getdate(),
		  @11,
		  getdate(),
		  0

        )

END