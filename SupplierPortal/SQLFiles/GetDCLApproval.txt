﻿Declare @@sql varchar(max),
        @@dateFrom varchar(10) = @0, 
		@@dateTo varchar(10) = @1


set @@sql =  'SELECT a.ROUTE,
		CASE 
			WHEN a.APPROVAL_STATUS = ''WAITING APPROVAL'' THEN ''True''
			ELSE ''False''
		END AS APPROVAL_STATUS_EDIT,
      CASE WHEN a.APPROVAL_STATUS IS NULL THEN ''-'' ELSE a.APPROVAL_STATUS END AS APPROVAL_STATUS,
      CASE WHEN a.REQUEST_STATUS IS NULL THEN ''-'' ELSE a.REQUEST_STATUS END AS REQUEST_STATUS,
      a.MAX_APPROVE_DURATION, a.PICK_UP_DATE, a.LP_CD, p.LOG_PARTNER_NAME AS LP_NAME, a.TRIP_NO, a.DELIVERY_REASON, a.REQUEST_DT,
	  a.DELIVERY_NO, a.DELIVERY_STATUS
FROM TB_R_LP_APPROVAL_H a
	INNER JOIN TB_M_LOGISTIC_PARTNER p ON p.LOG_PARTNER_CD = a.LP_CD
	WHERE 1=1 '

if((@@dateFrom <> '')and(@@dateTo <> ''))
	set @@sql = @@sql + 'and a.PICK_UP_DATE >= '''+ cast(@@dateFrom as varchar) + ''' and a.PICK_UP_DATE <= '''+ cast(@@dateTo as varchar) + ''' '

set @@sql = @@sql + ' ORDER BY a.CREATED_DT DESC'

execute (@@sql)

