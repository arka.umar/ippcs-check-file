﻿declare @@sql nvarchar(max), 
		@@orderReleaseDate datetime = @0,
		@@supplierCode varchar(15) = @1,
		@@supplierPlantCD varchar(15) = @2,
		@@orderType char(1) = @3,
		@@rcvPlantCode char(1) = @5

CREATE TABLE #tmpSplitStringDockLDAP
( 
	column1 VARCHAR(MAX)
)

INSERT  INTO #tmpSplitStringDockLDAP(column1)
EXEC dbo.SplitString @4, ''
	
SELECT OM.MANIFEST_NO,
	OM.ORDER_NO, 
	OM.SUPPLIER_PLANT,
	OM.RCV_PLANT_CD RECEIVE_PLANT_CODE, 
	OM.[DOCK_CD] DOCK_CODE,
	OM.[TOTAL_ITEM] TOTAL_ITEM, 
	OM.[TOTAL_QTY] TOTAL_QTY, 
	OM.[ARRIVAL_PLAN_DT] ARRIVAL_PLAN_DT,
	PRINT_FLAG=ISNULL(OM.PRINT_FLAG,'0'),
	RECEIVE_FLAG=ISNULL(OM.[RECEIVE_FLAG],'0'),
	M.DOWNLOAD_FLAG
FROM [dbo].[TB_R_DAILY_ORDER_MANIFEST] OM with(nolock)

inner join TB_R_DAILY_ORDER_WEB M
ON M.ORDER_RELEASE_DT = OM.CREATED_DT 
		AND M.ORDER_TYPE = OM.ORDER_TYPE
		AND M.SUPPLIER_CD = OM.SUPPLIER_CD
		AND M.SUPPLIER_PLANT_CD = OM.SUPPLIER_PLANT
		AND M.RCV_PLANT_CD = OM.RCV_PLANT_CD

where ISNULL(CANCEL_FLAG,'0') = '0'
and --CONVERT(VARCHAR(8),OM.ORDER_RELEASE_DT,112)=CONVERT(VARCHAR(8),@@orderReleaseDate,112)
	OM.CREATED_DT=@@orderReleaseDate
	and OM.SUPPLIER_CD=@@supplierCode
	and OM.SUPPLIER_PLANT = @@supplierPlantCD
	and OM.RCV_PLANT_CD = @@rcvPlantCode
	and OM.ORDER_TYPE=@@orderType
	and ((OM.DOCK_CD IN(select column1 from #tmpSplitStringDockLDAP) AND isnull(@4,'') <> '') or isnull(@4,'') = '')