﻿DECLARE @@sql varchar(max),
		@@DELIVERY_NO varchar(max) = @0,
		@@ORDER_NO varchar(max) = @1,
		@@RCV_PLANT_CD varchar(max) = @2,
		@@DOCK_CD varchar(max) = @3,
		@@SUPPLIER_CD varchar(max) = @4,
		@@KANBAN_NO varchar(max) = @5,
		@@MANIFEST_NO varchar(max) = @6,
		@@ROUTE varchar(max) = @7,
		@@RIT as varchar(max) = @8,
		@@P_LANE_SEQ varchar(max) = @9,
		@@DATE_FROM varchar(max) = @10,
		@@DATE_TO varchar(max) = @11,
		@@STATUS varchar(max) = @12

SET @@sql = 'SELECT ROW_NUMBER() over(order by MANIFEST_NO) as Row, KANBAN_ID, KANBAN_NO, PART_NAME, PART_NO,  SUPPLIER_CD, SUPPLIER_NAME, ORDER_NO, MANIFEST_NO, ROUTE_RATE, P_LANE_SEQ, ARRIVAL_PLAN_DT, ARRIVAL_ACTUAL_DT, SUM([ORDER]) as [ORDER], SHORTAGE, REMARK, RCV_PLANT_CD, RCV_DOCK_CD, QTY_PER_CONTAINER, SHORTAGE as RECEIVE FROM (
	SELECT 
	d.KANBAN_ID,
	a.KANBAN_NO,
	a.PART_NAME,
	a.PART_NO,
	b.SUPPLIER_CD,
	a.SUPPLIER_NAME,
	b.ORDER_NO,
	a.MANIFEST_NO,
	b.P_LANE_SEQ,
	SUBSTRING(b.SUB_ROUTE_CD,1,4) + '' - '' + b.SUB_ROUTE_SEQ as ROUTE_RATE,
	b.ARRIVAL_PLAN_DT,
	b.ARRIVAL_ACTUAL_DT,
	a.ORDER_PLAN_QTY_LOT as [ORDER],
	(SELECT COUNT(c.KANBAN_ID) FROM TB_R_RECEIVING_D c WHERE c.MANIFEST_NO = a.MANIFEST_NO AND c.RECEIVING_FLG = 0 AND c.ORDER_NO = b.ORDER_NO AND c.PART_NO = a.PART_NO) as SHORTAGE,
	e.REMARK,
	a.RCV_PLANT_CD,
	a.DOCK_CD as RCV_DOCK_CD,
	a.QTY_PER_CONTAINER
	FROM TB_R_DAILY_ORDER_PART a
	LEFT JOIN TB_R_DAILY_ORDER_MANIFEST b ON b.MANIFEST_NO = a.MANIFEST_NO
	INNER JOIN TB_R_RECEIVING_D d ON d.MANIFEST_NO = a.MANIFEST_NO AND d.PART_NO = a.PART_NO AND d.ORDER_NO = b.ORDER_NO
	LEFT JOIN TB_R_KANBAN_REMARK e ON e.KANBAN_NO = a.KANBAN_NO AND e.MANIFEST_NO = a.MANIFEST_NO AND e.ORDER_NO = b.ORDER_NO AND e.PART_NO = a.PART_NO
	WHERE a.ORDER_PLAN_QTY_LOT > 0 AND d.RECEIVING_FLG = 0 '

	--AND b.ORDER_NO = @0 AND b.RCV_PLANT_CD = @1 AND b.DOCK_CD = @2 AND b.SUPPLIER_CD = @3 AND b.P_LANE_CD = @4 AND a.KANBAN_NO = @5 AND ORDER_PLAN_QTY_LOT > 0
	IF(@@DELIVERY_NO<>'')
	BEGIN
		SET @@sql = @@sql + ' AND b.ORDER_NO = '''+@@DELIVERY_NO+''''
	END
	
	if(@@DELIVERY_NO = '' AND @@ORDER_NO <> '')
	BEGIN
		SET @@sql = @@sql + ' AND b.ORDER_NO = '''+@@ORDER_NO+''''
	END

	IF(@@RCV_PLANT_CD<>'')
	BEGIN
		SET @@sql = @@sql + ' AND a.RCV_PLANT_CD = '''+@@RCV_PLANT_CD+''''
	END

	IF(@@DOCK_CD <> '')
	BEGIN
		SET @@sql = @@sql + ' AND a.DOCK_CD = '''+@@DOCK_CD+''''
	END

	IF(@@SUPPLIER_CD <> '')
	BEGIN
		SET @@sql = @@sql + ' AND b.SUPPLIER_CD = '''+@@SUPPLIER_CD+''''
	END
	
	IF(@@KANBAN_NO <> '')
	BEGIN
		SET @@sql = @@sql + ' AND a.KANBAN_NO = '''+@@KANBAN_NO+''''
	END

	IF(@@MANIFEST_NO <> '')
	BEGIN
		SET @@sql = @@sql + ' AND a.MANIFEST_NO = '''+@@MANIFEST_NO+''''
	END

	IF(@@ROUTE <> '')
	BEGIN
		SET @@sql = @@sql + ' AND SUBSTRING(b.SUB_ROUTE_CD,1,4) = '''+@@ROUTE+''''
	END

	IF(@@P_LANE_SEQ <> '')
	BEGIN
		SET @@sql = @@sql + ' AND b.P_LANE_SEQ = '''+@@P_LANE_SEQ+''''
	END

	IF(@@DATE_FROM <> '' AND @@DATE_TO = '')
	BEGIN
		SET @@sql = @@sql + ' AND b.ARRIVAL_PLAN_DT BETWEEN '''+@@DATE_FROM+''' AND '''+GETDATE()+''''
	END

	IF(@@DATE_FROM <> '' AND @@DATE_TO <> '')
	BEGIN
		SET @@sql = @@sql + ' AND b.ARRIVAL_PLAN_DT BETWEEN '''+@@DATE_FROM+''' AND '''+@@DATE_TO+''''
	END

	IF(@@RIT <> '')
	BEGIN
		SET @@sql = @@sql + ' AND b.SUB_ROUTE_SEQ = '''+@@RIT+''''
	END

	if(@@STATUS <> '')
	BEGIN
		SET @@sql = @@sql + ' AND (CASE WHEN (SELECT COUNT(c.KANBAN_ID) FROM TB_R_RECEIVING_D c WHERE c.MANIFEST_NO = a.MANIFEST_NO AND c.RECEIVING_FLG = 1 AND c.PART_NO = a.PART_NO) = 0 AND GETDATE() <= b.ARRIVAL_PLAN_DT THEN ''Ordered'' ELSE CASE WHEN (SELECT COUNT(c.KANBAN_ID) FROM TB_R_RECEIVING_D c WHERE c.MANIFEST_NO = a.MANIFEST_NO AND c.RECEIVING_FLG = 1 AND c.PART_NO = a.PART_NO) < a.ORDER_PLAN_QTY_LOT THEN ''Shortage'' ELSE ''Completed'' END END) = '''+@@STATUS+''''
	END

SET @@sql = @@sql +')tbl
GROUP BY KANBAN_ID, KANBAN_NO, PART_NAME, PART_NO, MANIFEST_NO, SUPPLIER_NAME, SUPPLIER_CD, ORDER_NO, ROUTE_RATE, P_LANE_SEQ, ARRIVAL_PLAN_DT, ARRIVAL_ACTUAL_DT, SHORTAGE, REMARK, RCV_PLANT_CD, RCV_DOCK_CD, QTY_PER_CONTAINER'

IF(@@ORDER_NO <> '' OR @@RCV_PLANT_CD <> '' OR @@DOCK_CD <> '' OR @@SUPPLIER_CD <> '' OR @@KANBAN_NO <> '' OR @@MANIFEST_NO <> '' OR @@ROUTE <> '' OR @@P_LANE_SEQ <> '' OR @@DATE_FROM <> '' OR @@DATE_TO <> '' OR @@RIT<> '' OR @@STATUS <> '' OR @@DELIVERY_NO <> '')
BEGIN
	execute(@@sql);
END
