﻿DECLARE
	@@vendorCode VARCHAR(100) = @0,
	@@vendorName VARCHAR(100) = @1,
	@@periodFromDt VARCHAR(100) = @2,
	@@periodToDt VARCHAR(100) = @3

SELECT DISTINCT
        SUPPLIER_CODE = x.SUPPLIER_CD,
        SUPPLIER_NAME = x.SUPP_NAME,
        CATEGORY = x.CATEGORY,
        GOOD_RECEIPT_ACTUAL = x.GOOD_RECEIPT_ACTUAL,
        INVOICING_ACTUAL = x.INV_AMT,
        InvOutstandingCre = y.GR_IR_AMT,
        InvOutstandingInq = InvoiceAmount,
        INVOICING_OUTSTANDING =  y.GR_IR_AMT + InvoiceAmount,
        INVOICING_TOTAL = y.GR_IR_AMT + InvoiceAmount + x.INV_AMT,
        DIFFERENT = x.GOOD_RECEIPT_ACTUAL - y.GR_IR_AMT + InvoiceAmount + x.INV_AMT,
        [PERCENT] = CASE WHEN x.GOOD_RECEIPT_ACTUAL = 0 THEN NULL 
						 ELSE (x.GOOD_RECEIPT_ACTUAL - (y.GR_IR_AMT + InvoiceAmount + x.INV_AMT)) / x.GOOD_RECEIPT_ACTUAL / x.GOOD_RECEIPT_ACTUAL
					END
FROM (           
			SELECT PO_NO, SUPPLIER_CD, SUPP_NAME, CATEGORY, 
			SUM(GR_IR_AMT * PO_DETAIL_PRICE) [GOOD_RECEIPT_ACTUAL], SUM(INV_AMT) [INV_AMT]
			FROM (
				SELECT grir.SUPPLIER_CD,
					   sics.SUPP_NAME,
					   CASE WHEN ISNUMERIC(invd.COMP_PRICE_CD) = 0 THEN 0
							ELSE invd.COMP_PRICE_CD
					   END COMP_PRICE_CD,
					   invh.CURRENCY_CD,
					   invh.SUPP_INV_NO,
					   invh.INV_DT,
					   invd.TAX_CD,
					   invd.PO_NO,
					   x.CATEGORY,
					   CASE WHEN ISNUMERIC(grir.GR_IR_AMT) = 0 THEN 0
							ELSE grir.GR_IR_AMT
					   END GR_IR_AMT,
					   CASE WHEN ISNUMERIC(grir.PO_DETAIL_PRICE) = 0 THEN 0
							ELSE grir.PO_DETAIL_PRICE
					   END PO_DETAIL_PRICE,
					   --grir.DOC_DT,
					   CASE WHEN ISNUMERIC(invh.INV_AMT) = 0 THEN 0
							ELSE invh.INV_AMT
					   END INV_AMT
				FROM [dbo].TB_R_GR_IR grir
				JOIN [dbo].[TB_R_INV_H] invh ON invh.INV_NO = grir.INV_NO
					AND invh.SUPPLIER_CD = grir.SUPPLIER_CD
				OUTER APPLY (
					SELECT DISTINCT PO_NO, INV_NO, COMP_PRICE_CD, TAX_CD
					FROM TB_R_INV_D ad WHERE invh.INV_NO = ad.INV_NO
				) invd  
				JOIN TB_M_SUPPLIER_ICS sics ON sics.SUPP_CD = grir.SUPPLIER_CD
				CROSS APPLY (VALUES(CASE WHEN LEFT(INVD.PO_NO,2) = 45 THEN 'Component'
											WHEN  LEFT(INVD.PO_NO,2) = 11 THEN 'Service Part'
											ELSE 'Steel Sheet' END)) As x(CATEGORY)
				WHERE 1=1
					AND (NULLIF(@@vendorCode, '') IS NULL OR invh.SUPPLIER_CD = @@vendorCode)
					AND (NULLIF(@@vendorName, '') IS NULL OR sics.SUPP_NAME = @@vendorName)
					AND invd.PO_NO NOT LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
					AND grir.STATUS_CD IN (4,5)
					AND CAST(grir.DOC_DT as date) BETWEEN CAST(@@periodFromDt as date) AND CAST(@@periodToDt as date)
			) tx
			GROUP BY SUPPLIER_CD, SUPP_NAME, CATEGORY, PO_NO, /*DOC_DT,*/ CURRENCY_CD                     
        ) x
        LEFT JOIN (
			SELECT PO_NO, SUPPLIER_CD, SUM(GR_IR_AMT) GR_IR_AMT
			FROM (
				SELECT
					g.PO_NO,
					g.INV_REF_NO,                                                
					g.SUPPLIER_CD,
					g.CANCEL_FLAG,
					CASE WHEN ISNUMERIC(g.GR_IR_AMT) = 0 THEN 0
						ELSE g.GR_IR_AMT
					END GR_IR_AMT,
					g.LIV_SEND_FLAG
				FROM [dbo].TB_R_GR_IR g
				LEFT JOIN dbo.[TB_T_INV_UPLOAD] t ON t.SUPP_CD = g.SUPPLIER_CD
					AND t.PO_NO = g.PO_NO
					AND t.MAT_DOC_NO = g.MAT_DOC_NO
					AND t.COMP_PRICE_CD = g.COMP_PRICE_CD
					AND t.MAT_DOC_ITEM = CONVERT(VARCHAR, g.ITEM_NO)
					AND t.INV_REF_NO = G.INV_REF_NO
				LEFT JOIN TB_M_SUPPLIER_ICS SICS ON SICS.SUPP_CD = g.SUPPLIER_CD
				WHERE 1=1
					AND (NULLIF(@@vendorCode, '') IS NULL OR sics.SUPP_CD = @@vendorCode)
					AND (NULLIF(@@vendorName, '') IS NULL OR sics.SUPP_NAME = @@vendorName)
					AND g.CANCEL_FLAG = 0
					AND CAST(g.DOC_DT as date) BETWEEN CAST(@@periodFromDt as date) AND CAST(@@periodToDt as date)
					AND g.PO_NO NOT LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')                        
            ) txx
			GROUP BY PO_NO, SUPPLIER_CD				
        ) y ON y.PO_NO = x.PO_NO AND y.SUPPLIER_CD = x.SUPPLIER_CD
        LEFT JOIN (
				SELECT SuppCode, SUM(InvoiceAmount) InvoiceAmount
				FROM (			
					SELECT Qry.*
					FROM (
							SELECT
									SuppCode = tu.SUPP_CD,
									InvoiceAmount = SUM(tu.INV_AMT_TOTAL)
							FROM TB_R_INV_UPLOAD tu
							--LEFT JOIN (
							--		SELECT SUPP_INVOICE_NO, SUPP_CD, INV_AMT=SUM(INV_AMT)
							--		FROM TB_T_INV_GL_ACCOUNT
							--		WHERE TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE')
							--		GROUP BY SUPP_INVOICE_NO, SUPP_CD
							--)t ON tu.SUPP_INV_NO=t.SUPP_INVOICE_NO and tu.SUPP_CD=t.SUPP_CD
							LEFT JOIN TB_M_SUPPLIER_ICS SICS ON SICS.SUPP_CD = tu.SUPP_CD
							WHERE 1=1
									AND TU.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
									AND ((@@vendorCode IS NULL OR LEN(@@vendorCode) < 1) OR tu.SUPP_CD IN (select s from dbo.fnSplitString(@@vendorCode, ',')))
									AND (NULLIF(@@vendorName, '') IS NULL OR SICS.SUPP_NAME LIKE '%' + @@vendorName + '%')
									AND (((@@periodFromDt IS NULL)  OR  (@@periodToDt IS NULL)) OR (tu.INV_DT BETWEEN @@periodFromDt AND @@periodToDt))
							GROUP by tu.SUPP_CD
					) Qry
					UNION
					SELECT Qry.*                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
					FROM(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
							SELECT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
									SuppCode = th.SUPPLIER_CD,
									InvoiceAmount = SUM(th.INV_AMT)
							FROM TB_R_INV_H th
							INNER JOIN TB_R_INV_TAX tx ON th.INV_NO=tx.INVOICE_NO
							LEFT JOIN TB_R_INV_D z ON th.INV_NO = z.INV_NO
							--LEFT JOIN (
							--		SELECT INV_NO,INV_AMT=SUM(cast(INV_AMT as float))
							--		FROM TB_R_INV_GL_ACCOUNT
							--		WHERE TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE')
							--		GROUP BY INV_NO
							--)t ON th.INV_NO=t.INV_NO
							LEFT JOIN TB_M_SUPPLIER_ICS SICS ON SICS.SUPP_CD = th.SUPPLIER_CD
							WHERE 1=1
									AND Z.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
									AND ((@@vendorCode IS NULL OR LEN(@@vendorCode) < 1) OR th.SUPPLIER_CD IN (select s from dbo.fnSplitString(@@vendorCode, ',')))
									AND (NULLIF(@@vendorName, '') IS NULL OR SICS.SUPP_NAME LIKE '%' + @@vendorName + '%')
									AND (((@@periodFromDt IS NULL)  OR  (@@periodToDt IS NULL)) OR (th.INV_DT BETWEEN @@periodFromDt AND @@periodToDt))
									AND (ACCT_DOC_NO = 'null' or ACCT_DOC_NO is null)
							GROUP BY th.SUPP_INV_NO, th.SUPPLIER_CD
					) Qry
				) txxx
				GROUP BY SuppCode
        ) f ON f.SuppCode = x.SUPPLIER_CD

/*
DECLARE
	@@vendorCode VARCHAR(100) = @0,
	@@vendorName VARCHAR(100) = @1,
	@@periodFromDt VARCHAR(100) = @2,
	@@periodToDt VARCHAR(100) = @3

SELECT
	NO = ROW_NUMBER() OVER(ORDER BY x.SuppCode asc), 
	SUPPLIER_CODE = x.SuppCode,
	SUPPLIER_NAME = max(SuppName),
	CATEGORY = max(Category),
	GOOD_RECEIPT_ACTUAL = ISNULL(MAX(ISNULL(c.TOTAL_QTY,0)),0) * ISNULL(MAX(ISNULL(d.PO_MAT_PRICE,0)),0),
	INVOICING_ACTUAL = max(Cast(Actual as float)),
	InvOutstandingCre = cast(max(y.GR_IR_AMT) as float),
	InvOutstandingInq = cast(max(InvOutstanding) as float),
	INVOICING_OUTSTANDING =  ISNULL(MAX(ISNULL(y.GR_IR_AMT,0)),0) + ISNULL(MAX(ISNULL(InvOutstanding,0)),0),
	INVOICING_TOTAL = ISNULL(MAX(ISNULL(y.GR_IR_AMT,0)),0) + ISNULL(MAX(ISNULL(InvOutstanding,0)),0) + ISNULL(MAX(ISNULL(Actual,0)),0),
	DIFFERENT = (ISNULL(MAX(ISNULL(c.TOTAL_QTY,0)),0) * ISNULL(MAX(ISNULL(d.PO_MAT_PRICE,0)),0))- (ISNULL(MAX(ISNULL(y.GR_IR_AMT,0)),0) + ISNULL(MAX(ISNULL(InvOutstanding,0)),0) + ISNULL(MAX(ISNULL(Actual,0)),0)),
	[PERCENT] = ISNULL(CASE WHEN ISNULL(MAX(ISNULL(c.TOTAL_QTY,0)),0) * ISNULL(MAX(ISNULL(d.PO_MAT_PRICE,0)),0) = 0 THEN NULL 
	             ELSE ((ISNULL(MAX(ISNULL(c.TOTAL_QTY,0)),0) * ISNULL(MAX(ISNULL(d.PO_MAT_PRICE,0)),0))- (ISNULL(MAX(ISNULL(y.GR_IR_AMT,0)),0) + ISNULL(MAX(ISNULL(InvOutstanding,0)),0) + ISNULL(MAX(ISNULL(Actual,0)),0)))/ ISNULL(MAX(ISNULL(c.TOTAL_QTY,0)),0) * ISNULL(MAX(ISNULL(d.PO_MAT_PRICE,0)),0)/ISNULL(MAX(ISNULL(c.TOTAL_QTY,0)),0) * ISNULL(MAX(ISNULL(d.PO_MAT_PRICE,0)),0)
				 END,0)
FROM (
	SELECT
		  PO_NO = max(INVD.PO_NO),
		  SUPP_INV_NO = INVH.SUPP_INV_NO,
		  SuppCode = SICS.SUPP_CD,
		  SuppName = max(SICS.[SUPP_NAME]),
		  Category = max(x.Category),
		  Actual = max(Cast(INVH.INV_AMT as float))
	FROM [dbo].[TB_M_SUPPLIER_ICS] SICS
	JOIN [dbo].[TB_R_INV_H] INVH ON INVH.SUPPLIER_CD = SICS.SUPP_CD
	JOIN [dbo].[TB_R_INV_D] INVD ON INVD.INV_NO = INVH.INV_NO
	LEFT JOIN (
		select INV_NO,INV_AMT = SUM(cast(INV_AMT as float))
		from TB_R_INV_GL_ACCOUNT
		where TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE')
		group by INV_NO
	)t ON INVH.INV_NO = t.INV_NO
	CROSS APPLY (VALUES(CASE WHEN LEFT(INVD.PO_NO,2) = 45 THEN 'Component'
							 WHEN  LEFT(INVD.PO_NO,2) = 11 THEN 'Service Part'
							 ELSE 'Steel Sheet' END)) As x(Category)
	WHERE
		INVD.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
		and ((@@vendorCode is null OR @@vendorCode = '') OR (INVH.SUPPLIER_CD IN (SELECT s FROM dbo.fnSplitString(@@vendorCode, ';'))))
		AND (NULLIF(@@vendorName, '') IS NULL OR SICS.SUPP_NAME LIKE '%' + @@vendorName + '%')
		AND CONVERT(DATE, INVH.INV_DT) BETWEEN CONVERT(VARCHAR(10),CAST(@@periodFromDt AS DATETIME),23) AND CONVERT(VARCHAR(10),CAST(@@periodToDt AS DATETIME),23)
		and INVH.ACCT_DOC_NO <> 'NULL'
		and STATUS_CD  in (4,5)
	GROUP BY INVH.SUPP_INV_NO, SICS.SUPP_CD
) x
LEFT JOIN(
	SELECT PO_NO, SUM(cast(TOTAL_QTY as int)) AS TOTAL_QTY FROM [dbo].TB_R_DAILY_ORDER_MANIFEST
	GROUP BY PO_NO
)c on c.PO_NO = x.PO_NO
LEFT JOIN(
	SELECT PO_NO, SUM(cast(PO_MAT_PRICE as float)) as PO_MAT_PRICE FROM [dbo].TB_R_PO_D
	GROUP BY PO_NO
)d on d.PO_NO = x.PO_NO
LEFT JOIN (
	SELECT PO_NO=max(z.PO_NO), z.SUPPLIER_CD, GR_IR_AMT = SUM(z.GR_IR_AMT)
	FROM (
		SELECT
			PO_NO = max(g.PO_NO),
			G.INV_REF_NO,						
			SUPPLIER_CD,
			CANCEL_FLAG = MAX(CANCEL_FLAG),
			GR_IR_AMT = max(cast(GR_IR_AMT as float)),
			LIV_SEND_FLAG = MAX(LIV_SEND_FLAG)
		FROM [dbo].TB_R_GR_IR g
		LEFT JOIN dbo.[TB_T_INV_UPLOAD] t ON t.SUPP_CD = g.SUPPLIER_CD
											AND t.PO_NO = g.PO_NO
											AND t.MAT_DOC_NO = g.MAT_DOC_NO
											AND t.COMP_PRICE_CD = g.COMP_PRICE_CD
											AND t.MAT_DOC_ITEM = CONVERT(VARCHAR, g.ITEM_NO)
											AND t.INV_REF_NO = G.INV_REF_NO
		LEFT JOIN TB_M_SUPPLIER_ICS SICS ON SICS.SUPP_CD = g.SUPPLIER_CD
		WHERE 1=1
			and((@@vendorCode  IS NULL OR LEN(@@vendorCode) < 1) OR g.SUPPLIER_CD IN (select s from dbo.fnSplitString(@@vendorCode, ',')))
			AND (NULLIF(@@vendorName, '') IS NULL OR SICS.SUPP_NAME LIKE '%' + @@vendorName + '%')
			AND	(((@@periodFromDt IS NULL) OR (@@periodToDt IS NULL)) OR (g.DOC_DT BETWEEN @@periodFromDt AND @@periodToDt))
			AND g.CANCEL_FLAG = 0
			and g.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
		GROUP BY  G.INV_REF_NO, g.SUPPLIER_CD
	)z
	GROUP BY SUPPLIER_CD
) y ON y.PO_NO = x.PO_NO
LEFT JOIN (
	SELECT suppCode, SUM(CAST(InvoiceAmount AS FLOAT)) InvOutstanding
	from (
		SELECT Qry.*
		FROM (
			SELECT
				SuppCode = MAX(tu.SUPP_CD),
				InvoiceAmount = MAX(tu.INV_AMT_TOTAL)
			FROM TB_R_INV_UPLOAD tu
			LEFT JOIN (
				select SUPP_INVOICE_NO,SUPP_CD,INV_AMT=SUM(INV_AMT)
				from TB_T_INV_GL_ACCOUNT
				where  TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE')
				group by SUPP_INVOICE_NO,SUPP_CD
			)t ON tu.SUPP_INV_NO=t.SUPP_INVOICE_NO and tu.SUPP_CD=t.SUPP_CD
			LEFT JOIN TB_M_SUPPLIER_ICS SICS ON SICS.SUPP_CD = tu.SUPP_CD
			WHERE 1=1
				and TU.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
				and((@@vendorCode IS NULL OR LEN(@@vendorCode) < 1) OR tu.SUPP_CD IN (select s from dbo.fnSplitString(@@vendorCode, ',')))
				AND (NULLIF(@@vendorName, '') IS NULL OR SICS.SUPP_NAME LIKE '%' + @@vendorName + '%')
				AND	(((@@periodFromDt IS NULL)  OR  (@@periodToDt IS NULL)) OR (tu.INV_DT BETWEEN @@periodFromDt AND @@periodToDt))
			GROUP by tu.SUPP_CD
		) Qry
		UNION
		SELECT Qry.*																																																																										
		FROM(																																																																										
			SELECT																																																																									
				SuppCode = MAX(th.SUPPLIER_CD),
				InvoiceAmount=MAX(th.INV_AMT)
			FROM TB_R_INV_H th
			INNER JOIN TB_R_INV_TAX tx ON th.INV_NO=tx.INVOICE_NO
			left JOIN TB_R_INV_D z ON th.INV_NO = z.INV_NO
			LEFT JOIN (
				select INV_NO,INV_AMT=SUM(cast(INV_AMT as float))
				from TB_R_INV_GL_ACCOUNT
				where TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE')
				group by INV_NO
			)t ON th.INV_NO=t.INV_NO
			LEFT JOIN TB_M_SUPPLIER_ICS SICS ON SICS.SUPP_CD = th.SUPPLIER_CD
			WHERE
				Z.PO_NO not LIKE (SELECT CONFIG_VALUE + '%'  FROM dbo.TB_M_Spot_config_system WHERE CONFIG_NAME = 'PO_PREFIX_COD')
				and((@@vendorCode IS NULL OR LEN(@@vendorCode) < 1) OR th.SUPPLIER_CD IN (select s from dbo.fnSplitString(@@vendorCode, ',')))
				AND (NULLIF(@@vendorName, '') IS NULL OR SICS.SUPP_NAME LIKE '%' + @@vendorName + '%')
				AND	(((@@periodFromDt IS NULL)  OR  (@@periodToDt IS NULL)) OR (th.INV_DT BETWEEN @@periodFromDt AND @@periodToDt))
				and (ACCT_DOC_NO = 'null' or ACCT_DOC_NO is null)
			GROUP BY th.SUPP_INV_NO
		)Qry
	) tb
	GROUP BY SuppCode
) f ON f.SuppCode = x.SuppCode
GROUP BY x.SuppCode
*/