﻿DECLARE @@rowIndex AS INT,
		@@rowCountt AS INT,
		@@errorMessage AS VARCHAR(MAX),
		@@id AS VARCHAR(max),
		@@processId AS VARCHAR(max)
		
SET @@rowIndex = 1;

SELECT  ROW_NUMBER() OVER ( ORDER BY ID ASC ) AS SEQ_NO , *
	INTO #TempErrorUpload
FROM dbo.TB_T_CED_ERROR_UPLOAD 
WHERE FUNCTION_ID = @0 AND PROCESS_ID = @1

SELECT @@rowCountt = COUNT(*)
FROM #TempErrorUpload 

WHILE @@rowIndex <= @@rowCountt
BEGIN
	SELECT 
		@@errorMessage = ERRORS_MESSAGE,
		@@processId = PROCESS_ID FROM #TempErrorUpload WHERE SEQ_NO = @@rowIndex;
	
	SET @@id = SUBSTRING(@@errorMessage, CHARINDEX('[', @@errorMessage) + 1,CHARINDEX(']', @@errorMessage) - CHARINDEX('[', @@errorMessage) - 1);
	
	UPDATE dbo.TB_T_CED_EMERGENCY_ORDER_UPLOAD 
	SET ERRORS_MESSAGE_CONCAT = @@errorMessage
	WHERE PROCESS_id = @@processId
	AND ID = @@id
		
	SET @@rowIndex = @@rowIndex + 1;
END

DROP TABLE #TempErrorUpload

SELECT 
	SUPPLIER_CODE=SUPPLIER_CD,
	SUPPLIER_PLANT,
	DOCK_CODE=DOCK_CD,
	UNIQUE_NO=KANBAN_NO,
	PART_NO,
	ORDER_LOT_SZ,
	PROD_DATE,
	ERRORS_MESSAGE_CONCAT 
FROM dbo.TB_T_CED_EMERGENCY_ORDER_UPLOAD