﻿DECLARE @@sql VARCHAR (MAX),

@@PR_PO_MONTH VARCHAR(12) = @0,
@@LP_CD VARCHAR (MAX) = @1,
@@STATUS VARCHAR (MAX) = @2,
@@CHANGED_BY VARCHAR (30) = @3,
@@CHANGED_DT VARCHAR (MAX) = @4,
@@CREATED_BY VARCHAR (30) = @5,
@@CREATED_DT VARCHAR (MAX) = @6,
@@LP_CD2 VARCHAR (4) = @7,
@@LP_NM VARCHAR (30) = @8,
@@QTY VARCHAR (11) = @9,
@@ROUTE_CD VARCHAR (5) = @10,
@@ROUTE_NM VARCHAR (30) = @11,
@@SOURCE VARCHAR (20) = @12,
@@STATUS2 VARCHAR (20) = @13


SET @@sql ='SELECT
prr.PROD_MONTH,
prr.[LP_CD],
prr.ROUTE_CD,
prr.PR_QTY,
prr.SOURCE,
prr.PR_STATUS_FLAG,
prr.CREATED_BY,
prr.CREATED_DT,
prr.CHANGED_BY,
prr.CHANGED_DT,
TB_M_DLV_ROUTE.ROUTE_NAME,
TB_M_DLV_ROUTE.ROUTE_CD,
TB_M_LOGISTIC_PARTNER.LOG_PARTNER_NAME AS LP_NAME,
TB_M_SYSTEM.SYSTEM_VALUE AS STATUS

FROM
TB_T_PR_RETRIEVAL prr

LEFT JOIN TB_M_DLV_ROUTE ON TB_M_DLV_ROUTE.ROUTE_CD = prr.ROUTE_CD

LEFT JOIN TB_M_LOGISTIC_PARTNER ON TB_M_LOGISTIC_PARTNER.LOG_PARTNER_CD=prr.LP_CD

LEFT JOIN TB_M_SYSTEM ON TB_M_SYSTEM.SYSTEM_CD = prr.PR_STATUS_FLAG 

WHERE 1=1'

if(@@PR_PO_MONTH <> '')
	set @@sql = @@sql + 'AND prr.PROD_MONTH LIKE ''%' + @@PR_PO_MONTH + '%'''

if(@@LP_CD <> '')

	set @@sql = @@sql + 'AND prr.LP_CD IN ('+ cast (@@LP_CD as varchar (max)) +') '
	--set @@sql = @@sql + 'AND prr.LP_CD LIKE ''%' + @@LP_CD + '%'''

if(@@STATUS <> '')
	set @@sql = @@sql + 'AND TB_M_SYSTEM.SYSTEM_VALUE IN ('+ cast (@@STATUS as varchar (max)) +') '
	--set @@sql = @@sql + 'AND TB_M_SYSTEM.SYSTEM_VALUE LIKE ''%' + @@STATUS + '%'''
	
if(@@ROUTE_CD <> '')
	set @@sql = @@sql + 'AND prr.ROUTE_CD LIKE ''%' + @@ROUTE_CD + '%'''
	
if(@@ROUTE_NM <> '')
	set @@sql = @@sql + 'AND TB_M_DLV_ROUTE.ROUTE_NAME LIKE ''%' + @@ROUTE_NM + '%'''

if(@@CHANGED_BY <> '')
	set @@sql = @@sql + 'AND prr.CHANGED_BY LIKE ''%' + @@CHANGED_BY + '%'''

if(@@CHANGED_DT <> '')
	set @@sql = @@sql + 'AND  CONVERT (VARCHAR (10),prr.CHANGED_DT,111)  LIKE ''%' + @@CHANGED_DT + '%'''
	
if(@@CREATED_BY <> '')
	set @@sql = @@sql + 'AND prr.CREATED_BY LIKE ''%' + @@CREATED_BY + '%'''
	
if(@@CREATED_DT <> '')
	set @@sql = @@sql + 'AND CONVERT (VARCHAR (10),prr.CREATED_DT,111) LIKE ''%' + @@CREATED_DT + '%'''
	
if(@@LP_NM <> '')
	set @@sql = @@sql + 'AND TB_M_LOGISTIC_PARTNER.LOG_PARTNER_NAME LIKE ''%' + @@LP_NM + '%'''
	
if(@@QTY <> '')
	set @@sql = @@sql + 'AND prr.PR_QTY LIKE ''%' + @@QTY + '%'''
	
if(@@SOURCE <> '')
	set @@sql = @@sql + 'AND prr.SOURCE LIKE ''%' + @@SOURCE + '%'''

if(@@LP_CD2 <> '')
	set @@sql = @@sql + 'AND prr.LP_CD LIKE ''%' + @@LP_CD2 + '%'''

if(@@STATUS2 <> '')
	set @@sql = @@sql + 'AND TB_M_SYSTEM.SYSTEM_VALUE LIKE ''%' + @@STATUS2 + '%'''
	
set @@sql = @@sql + 'ORDER BY prr.CREATED_DT DESC, prr.CHANGED_DT DESC'

if (@@PR_PO_MONTH <> '') OR (@@LP_CD <> '') OR (@@STATUS <> '')
OR (@@CHANGED_BY <> '') OR (@@CHANGED_DT <> '') OR (@@CREATED_BY <> '') OR (@@CREATED_DT <> '') 
OR (@@LP_NM <> '')OR (@@QTY <> '') OR (@@LP_CD2 <> '')OR (@@STATUS2 <> '')
OR (@@ROUTE_CD <> '')OR (@@ROUTE_NM <> '')OR (@@SOURCE <> '')

execute (@@sql)