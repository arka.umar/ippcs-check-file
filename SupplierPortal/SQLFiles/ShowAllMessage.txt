SELECT
    ROW_NUMBER() OVER(ORDER BY CREATED_DT DESC) as NO,
    MESSAGE_ID as MessageID,
    TITLE as MessageTitle,
    BODY as MessageBody,
    FILE_URL as Filename,
    CONVERT(VARCHAR(10), MAX_RETENTION_DATE, 101)  as MaxRetDate,
    SENT_TO as MessageTo,
    PASSWORD as FilePassword,
    TYPE as MessageType,
    DOWNLOADED_USERNAME as DownloadUsername,
    CREATED_BY as MessageFrom,
    CREATED_DT  as MessageDate
    FROM TB_R_DATA_EXCHANGE
    WHERE DELETED_FLAG = 0
    AND TYPE = @0
    AND SENT_TO LIKE @1
    AND CREATED_BY LIKE @2
    ORDER BY CREATED_DT DESC