IF (   (SELECT COUNT(1) C 
        FROM TB_M_PRICE_CONFIRMATION 
        WHERE PC_NO = @PCNo AND SUPP_CD = @SupplierCode
        ) <= 0
    OR  NULLIF(@PCNo,'') IS NULL
    )
BEGIN
    INSERT INTO dbo.TB_M_PRICE_CONFIRMATION
      (PC_NO, SUPP_CD, EFFECTIVE_DT, PC_DT, 
      PRINT_FLAG, COVER_LETTER_BODY_CONTENT, 
      RETRO_DT_TO, RELEASE_STATUS, CREATED_BY, 
      CREATED_DT)
    VALUES
      (@PCNo, @SupplierCode, @EffectiveDate, @PCDate, 
      @PrintFlag, @CoverLetterBody, 
      @RetroDate, @ReleaseStatus, @CreatedBy, 
      GETDATE());
END
ELSE 
BEGIN
    UPDATE p SET
      EFFECTIVE_DT = @EffectiveDate
    , PC_DT = @PCDate
    , PRINT_FLAG = @PrintFlag
    , COVER_LETTER_BODY_CONTENT  = @CoverLetterBody
    , RETRO_DT_TO = @RetroDate
    , RELEASE_STATUS = @ReleaseStatus 
    , CHANGED_BY = @ChangedBy
    , CHANGED_DT = GETDATE() 
    FROM TB_M_PRICE_CONFIRMATION p 
    WHERE PC_NO = @PCNo 
      AND SUPP_CD = @SupplierCode 
END
if @@@ROWCOUNT > 0 
begin
    SELECT convert(varchar(20), 'SUCCESS') result
end
else
begin 
    select convert(varchar(20), 'FAIL') result
end