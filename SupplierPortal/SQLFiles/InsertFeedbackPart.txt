﻿/* Insert data for supplier feedback part (TB_R_LPOP, TB_R_SUPPLIER_FEEDBACK). fid.salman 14-feb-2013 */ 

UPDATE TB_R_LPOP
SET
	DOCK_CD = @5,
	N_VOLUME = @6,
	N_1_VOLUME = @7,
	N_2_VOLUME = @8,
	N_3_VOLUME = @9
WHERE
	((PART_NO=@0 AND isnull(@0,'') <> '') or (isnull(@0,'') = '')) AND 
	((PACK_MONTH=@1 AND isnull(@1,'') <> '') or (isnull(@1,'') = '')) AND 
	((SUPPLIER_CD=@2 AND isnull(@2,'') <> '') or (isnull(@2,'') = '')) AND 
	((S_PLANT_CD=@3)) AND
	((VERS=@4 AND isnull(@4,'') <> '') or (isnull(@4,'') = ''))

UPDATE TB_R_SUPPLIER_FEEDBACK	
SET
	N_FA = @10,
	N_1_FA = @11,
	N_2_FA = @12,
	N_3_FA = @13,
	N_MONTH = @14,
	N_1_MONTH = @15,
	N_2_MONTH = @16,
	N_3_MONTH = @17
WHERE
	((PART_NO=@0 AND isnull(@0,'') <> '') or (isnull(@0,'') = '')) AND 
	((PRODUCTION_MONTH=@1 AND isnull(@1,'') <> '') or (isnull(@1,'') = '')) AND 
	((SUPPLIER_CD=@2 AND isnull(@2,'') <> '') or (isnull(@2,'') = '')) AND
	((FORECAST_TYPE=@4 AND isnull(@4,'') <> '') or (isnull(@4,'') = ''))