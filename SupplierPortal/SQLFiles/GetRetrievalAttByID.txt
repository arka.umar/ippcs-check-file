declare @@mySql varchar(max),
		@@tampung varchar(max)

set @@mySql = '
select distinct b.*, a.* from 
TB_M_POSITION_LEVEL a 
inner join dbo.TB_M_POSITION b
on a.POSITION_ID=b.POSITION_ID 
where a.CLASS in 
    (select c.ATTORNEY from TB_M_POSITION_LEVEL a
     join dbo.TB_M_POSITION b 
     on a.POSITION_ID=b.POSITION_ID inner join dbo.TB_M_POA_MAPPING c
     on a.CLASS = c.CLASS
     where b.USERNAME=''' + @0 + ''')'

select @@tampung = c.SAME_DIV from TB_M_POSITION_LEVEL a
     join dbo.TB_M_POSITION b 
     on a.POSITION_ID=b.POSITION_ID inner join dbo.TB_M_POA_MAPPING c
     on a.CLASS = c.CLASS
     where b.USERNAME=@0

IF (@@tampung='N')
BEGIN
	EXEC( @@mySql)
END
ELSE
BEGIN
	set @@mySql = @@mySql + 'and b.DIVISION_ID  = (select DIVISION_ID from  dbo.TB_M_POSITION 
												 where USERNAME=''' + @0 + ''')'
	EXEC (@@mySql)
END
