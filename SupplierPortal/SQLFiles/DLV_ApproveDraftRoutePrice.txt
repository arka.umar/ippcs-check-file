DECLARE @@GRID VARCHAR(MAX) = @0, 
		@@CREATED_BY VARCHAR (20) = @1

EXEC [SP_DLV_ApproveDraftRoutePrice] @@GRID, @@CREATED_BY;