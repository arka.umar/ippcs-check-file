﻿DECLARE 
	@@categoryCd AS VARCHAR(1) = @0
	,@@apYear AS VARCHAR(4) = @1
	,@@version AS VARCHAR(3) = @2	

SELECT 
	[AP_YEAR]
    ,[VERSION]
    ,[PART_NO]
    ,[COLOR_SUFFIX]
    ,[PART_NO_SFX]
    ,[CAR_FAMILY_CD]
    ,[KANBAN_SIZE]
    ,[SUPPLIER_CD]
    ,[SUPPLIER_PLANT_CD]
    ,[SUPPLIER_NAME]
    ,[PART_NAME]
    ,[PART_MATCH_KEY]
    ,[PLANT_CD]
    ,[CATEGORY_CD]
    ,[MONTH1]
    ,[MONTH2]
    ,[MONTH3]
    ,[MONTH4]
    ,[MONTH5]
    ,[MONTH6]
    ,[MONTH7]
    ,[MONTH8]
    ,[MONTH9]
    ,[MONTH10]
    ,[MONTH11]
    ,[MONTH12]
    ,[MONTH13]
    ,[MONTH14]
    ,[MONTH15]
    ,[MONTH16]
    ,[MONTH17]
    ,[MONTH18]
    ,[MONTH19]
    ,[MONTH20]
    ,[MONTH21]
    ,[MONTH22]
    ,[MONTH23]
    ,[MONTH24]
    ,[MONTH25]
    ,[MONTH26]
    ,[MONTH27]
    ,[MONTH28]
    ,[MONTH29]
    ,[MONTH30]
    ,[MONTH31]
    ,[MONTH32]
    ,[MONTH33]
    ,[MONTH34]
    ,[MONTH35]
    ,[MONTH36]
FROM TB_T_ANNUAL_CROSSTAB_D
WHERE
	CATEGORY_CD = @@categoryCd
	AND AP_YEAR = @@apYear
	AND VERSION = @@version