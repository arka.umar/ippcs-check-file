﻿DECLARE @@system_id VARCHAR(5) = @0,
		@@role_id VARCHAR(5) = @1,
		@@screen_id VARCHAR(5) = @2,
		@@screen_auth VARCHAR(5) = @3,
		@@object_id VARCHAR(5) = @4,
		@@object_auth VARCHAR(5) = @5,
		@@action_id VARCHAR(5) = @6,
		@@action_auth VARCHAR(5) = @7,
		@@authgroup VARCHAR(5) = @8,
		@@username varchar(30) = @9,
		@@mode varchar(4) = @10,
		@@sql nvarchar(max) = ''

SET @@screen_auth = CASE @@screen_auth WHEN '-1' THEN NULL ELSE @@screen_auth END
SET @@object_auth = CASE @@object_auth WHEN '-1' THEN NULL ELSE @@object_auth END
SET @@action_auth = CASE @@action_auth WHEN '-1' THEN NULL ELSE @@action_auth END

IF(@@mode = 'Add')
BEGIN
	DECLARE @@max_id BIGINT = CONVERT(BIGINT, (SELECT * FROM OPENQUERY([10.16.25.100], 'SELECT ISNULL(MAX(AUTHORIZATION_ID), 0) + 1 FROM [OpenLDAP].[dbo].TB_M_AUTHORIZATION_DETAIL')))

	INSERT OPENQUERY([10.16.25.100], 
	' SELECT
		AUTHORIZATION_ID,
		BRANCH_ID,
		SYSTEM_ID,
		ROLE_ID,
		SCREEN_ID,
		SCREEN_AUTH,
		ACTION_ID,
		ACTION_AUTH,
		[OBJECT_ID],
		OBJECT_AUTH,
		AUTHORIZATION_GROUP_ID,
		CREATED_BY,
		CREATED_DT
	FROM [OpenLDAP].[dbo].TB_M_AUTHORIZATION_DETAIL')
	SELECT @@max_id,
		1000,
		@@system_id,
		@@role_id,
		@@screen_id,
		@@screen_auth,
		@@action_id,
		@@action_auth,
		@@object_id,
		@@object_auth,
		NULLIF(@@authgroup, ''),
		@@username,
		GETDATE()
END
ELSE
BEGIN
	SET @@sql = 
	'
		UPDATE OPENQUERY([10.16.25.100], '' 
				select screen_auth,
					   object_auth,
					   action_auth,
					   authorization_group_id,
					   changed_by,
					   changed_dt
				from [OpenLDAP].[dbo].TB_M_AUTHORIZATION_DETAIL
				where system_id = ' + ISNULL(NULLIF(@@system_id, ''), 'NULL') + '
					  and role_id = ' + ISNULL(NULLIF(@@role_id, ''), 'NULL') + '
					  and screen_id = ' + ISNULL(NULLIF(@@screen_id, ''), 'NULL') + '
					  and ((object_id = ' + ISNULL(NULLIF(@@object_id, ''), 'NULL') + ' AND ' + ISNULL(NULLIF(@@object_id, ''), '-2') +' <> -2) OR (' + ISNULL(NULLIF(@@object_id, ''), '-2') + ' = -2 AND ISNULL(object_id, 0) = 0))
					  and ((action_id = ' + ISNULL(NULLIF(@@action_id, ''), 'NULL') + ' AND ' + ISNULL(NULLIF(@@action_id, ''), '-2') +' <> -2) OR (' + ISNULL(NULLIF(@@action_id, ''), '-2') + ' = -2 AND ISNULL(action_id, 0) = 0))
		 '') SET screen_auth = ' + ISNULL(NULLIF(@@screen_auth, ''), 'NULL') + ',
				 action_auth = ' + ISNULL(NULLIF(@@action_auth, ''), 'NULL') + ',
				 object_auth = ' + ISNULL(NULLIF(@@object_auth, ''), 'NULL') + ',
				 changed_by = ''' + ISNULL(NULLIF(@@username, ''), 'NULL') + ''',
				 changed_dt = ''' + CONVERT(VARCHAR(50), GETDATE(), 120) + '''
	'
	EXEC sp_executesql @@sql
END

DELETE OPENQUERY([10.16.25.100], 'SELECT * FROM [OpenLDAP].[dbo].TB_T_AUTHORIZATION_DETAIL')
SELECT 'success'