﻿EXEC SP_addUpdateUserProcess
					@0, --branch,
					@1, --noreg,  
					@2, --username,  
					@3, --password,  
					@4, --passwordexpirationdate,  
					@5, --userexpirationdate,
                    @6, --firstname,  
					@7, --lastname,  
					@8, --fullname,  
					@9, --telpno, 
					@10, --mobileno,  
					@11, --classid,  
					@12, --jobfunction,
                    @13, --cccode,  
					@14, --companyid,  
					@15, --locationid,  
					@16, --isproduction,  
					@17, --maxlogin,  
					@18, --quota,  
					@19, --defaultapp,
                    @20, --email,  
					@21, --multiplelogin,  
					@22, --passwordchange, 
					@23, --activedirectory,  
					@24, --locked,  
					@25, --isactive,
                    @26, --notification,  
					@27, --deletecheck, 
					@28, --User,	
					@29 --"new or edit"