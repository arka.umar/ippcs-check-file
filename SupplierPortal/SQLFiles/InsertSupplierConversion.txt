DECLARE @@VSuppName VARCHAR(50);

IF  exists(select * from TB_M_SUPPLIER_CONVERSION 
	where (SUPPLIER_CODE_TMMIN =@0 and SUPPLIER_PLANT_TMMIN=@1  and SUPPLIER_CODE_ADM = @2 and SUPPLIER_PLANT_ADM = @03) OR (SUPPLIER_CODE_ADM = @2 and SUPPLIER_PLANT_ADM = @03)   )

	BEGIN
	 select 'Alreadyexists'
	END
ELSE

	BEGIN
	SELECT TOP 1 @@VSuppName = SUPPLIER_NAME  FROM TB_M_SUPPLIER WHERE SUPPLIER_CODE = @0

	SET @@VSuppName = @@VSuppName

		INSERT INTO TB_M_SUPPLIER_CONVERSION
				   (SUPPLIER_CODE_TMMIN --1
				   ,SUPPLIER_NAME
				   ,SUPPLIER_PLANT_TMMIN
				   ,SUPPLIER_CODE_ADM
				   ,SUPPLIER_PLANT_ADM
				   ,CREATED_BY
				   ,CREATED_DATE
				   )
		VALUES  ( @0 , 
				  @@VSuppName,
				  @1 , 
				  @2 , 
				  @3 , 
				  @4 , 
				 getdate()
				  
				   
				)
	    select 'ProcessSuccesfully'
   END