﻿-- =================================================
-- Author: niit.arie
-- Create date: 07 Nov 2013
-- Update date: 07 Nov 2013
-- Description: Query Grid Detail Supplier Performace Tab TWS
-- =================================================


DECLARE @@SupplierCode VARCHAR(20) = @0
        ,@@ProdMonth VARCHAR(20) = @1
        ,@@ProdMonthTo VARCHAR(20) = @2
        ,@@DockCode VARCHAR(20) = @3

SELECT 
	T.PROD_MONTH
	,T.SUPPLIER_CD
	,T.SUPPLIER_NAME
	,T.QTY_CLAIM
	,T.CREATED_BY
	,T.CREATED_DT
	,T.CHANGED_BY
	,T.CHANGED_DT
FROM
(
	SELECT 
		LEFT(CONVERT(VARCHAR(24), ST.PROD_MONTH,112),6) AS PROD_MONTH
		,ST.SUPPLIER_CD
		,S.SUPP_NAME AS SUPPLIER_NAME
		,ST.QTY_CLAIM
		,ST.CREATED_BY
		,ST.CREATED_DT
		,ST.CHANGED_BY
		,ST.CHANGED_DT
	FROM TB_R_SUPPLIER_TWC ST
		LEFT JOIN TB_M_SUPPLIER_ICS S
			ON S.SUPP_CD = ST.SUPPLIER_CD

	WHERE ST.PROD_MONTH BETWEEN @@ProdMonth AND @@ProdMonthTo

		AND ((@@SupplierCode IS NULL or LEN(@@SupplierCode) < 1)
		OR ST.SUPPLIER_CD IN (SELECT item FROM dbo.fnSplit(@@SupplierCode, ';'))
		)

		--AND ((@@DockCode IS NULL or LEN(@@DockCode) < 1)
		--OR ST.RECEIVING_AREA IN (SELECT item FROM dbo.fnSplit(@@DockCode, ';'))
		--)
) AS T

ORDER BY T.PROD_MONTH, T.SUPPLIER_CD, T.SUPPLIER_NAME