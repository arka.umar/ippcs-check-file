﻿declare @@RPlantCode varchar(10) = @0, 
		@@DockCode varchar(10) = @1,
		@@SupplierCode varchar(15) = @2,
		@@dateFrom Datetime = @3, 
		@@dateTo Datetime = @4

SELECT 
	R_PLANT_CD PlantCode,
	DOCK_CD DockCode,
	SUPPLIER_CD SupplierCode,
	SUPPLIER_NAME SupplierName,
	VALID_FROM ValidFrom,
	VALID_TO ValidTo,
	EXCLUDE_FLAG ExcludeFlag,
	CREATED_BY CreatedBy,
	CREATED_DT CreatedDate,
	CHANGED_BY ChangedBy,
	CHANGED_DT ChangedDate
FROM TB_M_LPOP_SUPPLIER_MAPPING
WHERE
	((R_PLANT_CD=@@RPlantCode AND isnull(@@RPlantCode,'') <> '') or (isnull(@@RPlantCode,'') = ''))
	AND ((DOCK_CD=@@DockCode AND isnull(@@DockCode,'') <> '') or (isnull(@@DockCode,'') = ''))
	AND ((SUPPLIER_CD=@@SupplierCode AND isnull(@@SupplierCode,'') <> '') or (isnull(@@SupplierCode,'') = ''))
	AND CONVERT(VARCHAR(8), VALID_FROM, 112)>=CONVERT(VARCHAR(8), @@dateFrom, 112)
	AND CONVERT(VARCHAR(8), VALID_TO, 112)<=CONVERT(VARCHAR(8), @@dateTo, 112)