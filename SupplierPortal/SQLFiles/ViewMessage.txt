SELECT  MESSAGE_ID as MessageID,
    TITLE as MessageTitle,
    BODY as MessageBody,
    FILE_URL as Filename,
    CONVERT(VARCHAR(10), MAX_RETENTION_DATE, 101) as MaxRetDate,
    SENT_TO as MessageTo,
    PASSWORD as FilePassword,
    TYPE as MessageType,
    EXPIRED_FLAG as ExpiredFlag,
    NOTIFY_DOWNLOAD_FLAG as NotifyDownloadFlag,
    CREATED_BY as MessageFrom,
    CREATED_DT  as MessageDate
    FROM TB_R_DATA_EXCHANGE
    WHERE MESSAGE_ID = @0