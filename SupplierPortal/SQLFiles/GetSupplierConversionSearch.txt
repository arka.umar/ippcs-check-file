﻿Declare @@sql varchar(max),
		@@SupplierCodeTmmin varchar(max) = @0,
		@@SupplierPlantTmmin varchar(max) = @1,
		@@SupplierCodeADM varchar(max) = @2,
		@@SupplierPlantADM varchar(max) = @3
		
		
set @@sql =  ' SELECT	
				[SUPPLIER_CODE_TMMIN]
				,b.[SUPPLIER_NAME]
				,[SUPPLIER_PLANT_TMMIN]
				,SUPPLIER_CODE_TMMIN + ''-'' + SUPPLIER_PLANT_TMMIN AS  SUPPLIER_CODE_PLANT_TMMIN
				,[SUPPLIER_CODE_ADM]
				,[SUPPLIER_PLANT_ADM]
      
  FROM [TB_M_SUPPLIER_CONVERSION] a
  join TB_M_SUPPLIER b on a.SUPPLIER_CODE_TMMIN = b.SUPPLIER_CODE
   AND  A.SUPPLIER_PLANT_TMMIN = B.SUPPLIER_PLANT_CD
   WHERE 1=1 '

		
if(@@SupplierCodeTmmin <> '')
	set @@sql = @@sql + ' and SUPPLIER_CODE_TMMIN LIKE ''%' + @@SupplierCodeTmmin +'%'''

if(@@SupplierPlantTmmin <> '')
	set @@sql = @@sql + ' and SUPPLIER_PLANT_TMMIN LIKE ''%' + @@SupplierPlantTmmin +'%'''

if(@@SupplierCodeADM <> '')
	set @@sql = @@sql + ' and SUPPLIER_CODE_ADM LIKE ''%' + @@SupplierCodeADM +'%'''

if(@@SupplierPlantADM <> '')
	set @@sql = @@sql + ' and SUPPLIER_PLANT_ADM LIKE ''%' + @@SupplierPlantADM +'%'''
	
	
	execute (@@sql)