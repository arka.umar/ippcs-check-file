﻿DECLARE @@sql varchar(max),
        @@SuppCd varchar(max) = REPLACE(@0,';',''','''), 
		@@PartNo varchar(max) = REPLACE(REPLACE(@1,';',''','''),'-',''),
		@@DockCode varchar(max) = REPLACE(@2,';',''',''') ,
		@@DockCodeLdap varchar(max) = REPLACE(@4,',',''',''')
		
SET @@sql =  '
			SELECT 
				LEFT(A.PART_NO,5) + ''-'' + SUBSTRING(A.PART_NO,6,5) + ''-'' + RIGHT(A.PART_NO,2) AS PartNo
				,a.PART_NO
				,a.SUPPLIER_CD, a.SUPPLIER_PLANT
				,a.SUPPLIER_CD + ''-'' + a.SUPPLIER_PLANT as  SupplierCodePlantCode
				,c.SUPPLIER_NAME
				,DOCK_CD
				,KANBAN_NO
				,PART_NAME
				,PCK_TYPE
				,KANBAN_QTY
				,SAFETY_PCS
				,PRMCFC
				,PRMPCT
				,PRMMS
				,PRMXS
				,PRMCD
				,LINE_LEAD_TIME
				,a.CREATED_BY
				,a.CREATED_DT
				,a.CHANGED_BY
				,a.CHANGED_DT
				,a.START_DT
				,a.END_DT
			FROM dbo.TB_M_PART_INFO A
			INNER JOIN TB_M_SUPPLIER C ON c.SUPPLIER_CODE=a.SUPPLIER_CD AND C.SUPPLIER_PLANT_CD=a.SUPPLIER_PLANT
			WHERE 1=1 '

if (@3 <>'')
	set @@sql = @@sql + ' and A.SUPPLIER_CD =''+ @3 +'''

if (@@DockCodeLdap <>'')
	set @@sql = @@sql + ' and A.DOCK_CD IN ('''+ @@DockCodeLdap +''')'


if(@@SuppCd <> '')
	set @@sql = @@sql + 'and A.SUPPLIER_CD +''-''+ A.SUPPLIER_PLANT IN ('''+ @@SuppCd +''') '

if(@@PartNo <> '')
	set @@sql = @@sql + 'and A.PART_NO IN ('''+  @@PartNo +''') '
		
if(@@DockCode <> '')
	set @@sql = @@sql + 'and A.DOCK_CD IN ('''+  @@DockCode +''') '

execute (@@sql)