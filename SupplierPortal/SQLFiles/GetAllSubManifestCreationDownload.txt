﻿IF OBJECT_ID('tempdb..#tempSupplier') IS NOT NULL
	DROP TABLE #tempSupplier

IF OBJECT_ID('tempdb..#tempSubSupplier') IS NOT NULL
	DROP TABLE #tempSubSupplier

CREATE TABLE #tempSupplier (supplier VARCHAR(20))

CREATE TABLE #tempSubSupplier (subsupcode VARCHAR(20))

INSERT INTO #tempSupplier (supplier)
EXEC dbo.SplitString @0
	,'-'

INSERT INTO #tempSubSupplier (subsupcode)
EXEC dbo.SplitString @2
	,'-'


IF(ISNULL(@4, '') = '')
BEGIN
		SELECT M.MANIFEST_NO
			,CONVERT(VARCHAR, M.CREATED_DT, 104) AS MANIFEST_NO_DT
			/*,M.SUPPLIER_CD
			,ISNULL(M.SUB_SUPP_CD,'') AS SUB_SUPP_CD*/
			,(M.SUPPLIER_CD + '-' + M.SUPPLIER_PLANT) AS SUPPLIER_CD
			,(CASE WHEN ISNULL(M.SUB_SUPP_CD,'') <> '' THEN (M.SUB_SUPP_CD + '-' + M.SUB_SUPP_PLANT) ELSE '' END) AS SUB_SUPP_CD
			,ISNULL(M.SUB_ROUTE_CD,'') AS SUB_ROUTE_CD
			,ISNULL(M.DOCK_CD,'') AS DOCK_CD
			,P.PART_NO
			,P.QTY_PER_CONTAINER
			,P.ITEM_NO
			,P.ORDER_QTY
			,P.REMAIN_RECEIVE_QTY AS REMAIN_QTY
			,'' AS DELIVERY_QTY --P.DELIVERY_QTY
		FROM TB_R_PRA_DAILY_ORDER_MANIFEST M
		JOIN TB_R_PRA_DAILY_ORDER_PART P ON M.MANIFEST_NO = P.MANIFEST_NO
		WHERE (
				(
					ISNULL(@0, '') <> ''
					AND EXISTS (
						SELECT ''
						FROM #tempSupplier P
						WHERE P.supplier = M.SUPPLIER_CD
						)
					)
				OR (ISNULL(@0, '') = '')
				)
			AND (
				(
					ISNULL(@2, '') <> ''
					AND EXISTS (
						SELECT ''
						FROM #tempSubSupplier D
						WHERE D.subsupcode = M.SUB_SUPP_CD
						)
					)
				OR (ISNULL(@2, '') = '')
				)
			AND (
				(
					(
						(M.[SUPPLIER_CD]) = @3
						AND isnull(@3, '') <> ''
						)
					OR (
						(M.[SUB_SUPP_CD]) = @3
						AND isnull(@3, '') <> ''
						)
					)
				OR ((isnull(@3, '') = ''))
				)
			/*AND (
				(
					ISNULL(@1, '') <> ''
					AND M.MANIFEST_NO = RTRIM(LTRIM(@1))
					)
				OR (ISNULL(@1, '') = '')
				)*/
			AND M.MANIFEST_NO = RTRIM(LTRIM(@1))
END
ELSE IF (ISNULL(@4, '') <> '')
BEGIN

	
		SELECT M.MANIFEST_NO
			,CONVERT(VARCHAR, M.CREATED_DT, 104) AS MANIFEST_NO_DT
			/*,M.SUPPLIER_CD
			,ISNULL(M.SUB_SUPP_CD,'') AS SUB_SUPP_CD*/
			,(M.SUPPLIER_CD + '-' + M.SUPPLIER_PLANT) AS SUPPLIER_CD
			,(CASE WHEN ISNULL(M.SUB_SUPP_CD,'') <> '' THEN (M.SUB_SUPP_CD + '-' + M.SUB_SUPP_PLANT) ELSE '' END) AS SUB_SUPP_CD
			,ISNULL(M.SUB_ROUTE_CD,'') AS SUB_ROUTE_CD
			,ISNULL(M.DOCK_CD,'') AS DOCK_CD
			,P.PART_NO
			,P.QTY_PER_CONTAINER
			,P.ITEM_NO
			,P.ORDER_QTY
			,ISNULL(P.REMAIN_RECEIVE_QTY,0) + ISNULL(L.ORDER_QTY,0) AS REMAIN_QTY
			,L.ORDER_QTY AS DELIVERY_QTY
			,'' AS DELIVERY_QTY --P.DELIVERY_QTY
		FROM TB_R_PRA_DAILY_ORDER_MANIFEST M
		JOIN TB_R_PRA_DAILY_ORDER_PART P ON M.MANIFEST_NO = P.MANIFEST_NO
		JOIN TB_R_DAILY_ORDER_MANIFEST K ON M.MANIFEST_NO = K.REFF_NO
		JOIN TB_R_DAILY_ORDER_PART L ON K.MANIFEST_NO = L.MANIFEST_NO
									 AND P.PART_NO = L.PART_NO
									 AND P.ITEM_NO = L.ITEM_NO
									 
		
		WHERE (
				(
					ISNULL(@0, '') <> ''
					AND EXISTS (
						SELECT ''
						FROM #tempSupplier P
						WHERE P.supplier = M.SUPPLIER_CD
						)
					)
				OR (ISNULL(@0, '') = '')
				)
			AND (
				(
					ISNULL(@2, '') <> ''
					AND EXISTS (
						SELECT ''
						FROM #tempSubSupplier D
						WHERE D.subsupcode = M.SUB_SUPP_CD
						)
					)
				OR (ISNULL(@2, '') = '')
				)
			AND (
				(
					(
						(M.[SUPPLIER_CD]) = @3
						AND isnull(@3, '') <> ''
						)
					OR (
						(M.[SUB_SUPP_CD]) = @3
						AND isnull(@3, '') <> ''
						)
					)
				OR ((isnull(@3, '') = ''))
				)
			/*AND (
				(
					ISNULL(@1, '') <> ''
					AND M.MANIFEST_NO = RTRIM(LTRIM(@1))
					)
				OR (ISNULL(@1, '') = '')
				)*/
			AND M.MANIFEST_NO = RTRIM(LTRIM(@1))
			AND K.MANIFEST_NO = RTRIM(LTRIM(@4))

END

IF OBJECT_ID('tempdb..#tempSupplier') IS NOT NULL
	DROP TABLE #tempSupplier

IF OBJECT_ID('tempdb..#tempSubSupplier') IS NOT NULL
	DROP TABLE #tempSubSupplier