﻿DECLARE @@SolvedSum INT,
		@@SolvedCount INT,
		@@ManifestNo VARCHAR(16) = @0,
		@@UserName VARCHAR(50) = @1

SELECT @@SolvedSum = SUM(ISNULL(CAST(SOLVED_FLAG AS int), 0)) 
						FROM TB_R_PROBLEM_PART WHERE MANIFEST_NO = @@ManifestNo
SELECT @@SolvedCount = COUNT(*) FROM TB_R_PROBLEM_PART WHERE MANIFEST_NO = @@ManifestNo

IF @@SolvedCount=@@SolvedSum 
	BEGIN
		UPDATE TB_R_DAILY_ORDER_MANIFEST SET 
			PROBLEM_FLAG = 0, 
			CHANGED_BY = @@UserName, 
			CHANGED_DT = GETDATE()
		WHERE MANIFEST_NO=@@ManifestNo
	END



--------------------------------------------------------------------
-- August , 8th 2018
-- NEW QUERY for checking whether the manifest was updated or not 
-- check manifest_receive_flag = 2 or not
-- if the manifest failed to be updated ( manifest_receive_flag != 2)
-- Re-Update manifest (logic query same as before)
--------------------------------------------------------------------

IF(EXISTS(SELECT 'Temp' FROM TB_R_DAILY_ORDER_MANIFEST WHERE MANIFEST_NO = @@ManifestNo AND MANIFEST_RECEIVE_FLAG <> '2'))
BEGIN
	       
		UPDATE TB_R_DAILY_ORDER_MANIFEST SET
			MANIFEST_RECEIVE_FLAG=2,
			RECEIVE_FLAG=1,
			RECEIVE_DT = GETDATE(),
			ARRIVAL_ACTUAL_DT = GETDATE(),
			DROP_STATUS_FLAG = 1,
			DROP_STATUS_DT = GETDATE(),
			CHANGED_BY = @@UserName,
			CHANGED_DT = getdate()
		WHERE MANIFEST_NO=@@ManifestNo
              

	
		UPDATE TB_R_DAILY_ORDER_PART SET
			RECEIVED_STATUS = 1,
			CHANGED_BY = @@UserName,
			CHANGED_DT = getdate()
		WHERE MANIFEST_NO=@@ManifestNo	

END