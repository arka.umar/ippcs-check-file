﻿Declare 
	   @@sql varchar(max),
       @@DELIVERY_NO VARCHAR(max)= @0,
	   @@DELIVERY_TYPE VARCHAR(max) = @1,
	   @@LP_CD VARCHAR(max) = @2,
	   @@ROUTE VARCHAR(max) = @3,
	   @@DELIVERY_STS VARCHAR(max) = @4,
	   @@ROUTE_DATE VARCHAR(max) = @5,
	   @@ROUTE_DATE_TO VARCHAR(max) = @6,
	   @@PO_NO VARCHAR(max) = @7

  set @@sql =  'SELECT 
	a.DELIVERY_NO, 
	CASE SUBSTRING(a.DELIVERY_NO,1,1) WHEN ''A'' THEN ''Additional'' WHEN ''R'' THEN ''Regular''  ELSE ''Non TLMS'' END AS DELIVERY_TYPE,
	a.LP_CD,
	c.LOG_PARTNER_NAME as LP_NAME,
	a.ROUTE + '' - '' + a.RATE AS ROUTE_RATE,
	a.PICKUP_DT,
	a.DELIVERY_STS,
	MAX(b.ARRIVAL_ACTUAL_DT) ARRIVAL,
	MAX(b.DEPARTURE_ACTUAL_DT) DEPARTURE,
	e.PO_NO,
	--CASE WHEN a.[DELIVERY_STS] = ''Posted'' OR a.[DELIVERY_STS] = ''Canceled'' THEN e.PO_NO ELSE '''' END as PO_NO,
	MAX(d.SERVICE_DOC_NO) as MATDOC_NO,
	(SELECT TOP 1 inv.ACCT_DOC_NO FROM TB_R_DLV_INV_H inv WHERE inv.LP_INV_NO = f.LP_INV_NO ORDER BY CREATED_DT DESC) AS ACCT_DOC_NO,
	(SELECT TOP 1 inv.ACCT_DOC_YEAR FROM TB_R_DLV_INV_H inv WHERE inv.LP_INV_NO = f.LP_INV_NO ORDER BY CREATED_DT DESC) AS ACCT_DOC_YEAR,
	(SELECT TOP 1 inv.PAYMENT_DOC_NO FROM TB_R_DLV_INV_H inv WHERE inv.LP_INV_NO = f.LP_INV_NO ORDER BY CREATED_DT DESC) AS CLEAR_DOC_NO,
	MAX(d.DOC_DT) as POSTING_DT,
	MAX(d.CREATED_BY) as PIC,
	f.LP_INV_NO as INV_NO,
	f.CREATED_DT as INV_DT,
	f.CREATED_BY as INV_POSTING_BY,
	CASE WHEN a.DELIVERY_STS = ''Posted'' THEN '''' ELSE MAX(d.SA_CANCEL_REFF_NO) END as MATDOC_NO2,
	CASE WHEN a.DELIVERY_STS = ''Posted'' THEN NULL ELSE MAX(d.CHANGED_DT) END as CANCEL_DT,
	CASE WHEN a.DELIVERY_STS = ''Posted'' THEN '''' ELSE MAX(d.CHANGED_BY) END as PIC2,
	--MAX(a.SA_NOTES) as SA_NOTES
	CASE WHEN (MAX(b.DEPARTURE_PLAN_DT) < GETDATE() AND MAX(a.SA_NOTES) IS NULL AND a.DELIVERY_STS = ''Arrived'') OR (a.DELIVERY_STS = ''Downloaded'' AND MAX(b.ARRIVAL_PLAN_DT) < GETDATE() AND MAX(a.SA_NOTES) IS NULL ) OR (a.DELIVERY_STS = ''Downloaded'' AND LEFT(a.DELIVERY_NO, 1) IN (''A'', ''N'') AND MAX(a.SA_NOTES) IS NULL) THEN ''yes'' ELSE ''no'' END +''|''+CASE WHEN MAX(a.SA_NOTES) IS NULL THEN '''' ELSE MAX(a.SA_NOTES) END as SA_NOTES
	FROM TB_R_DELIVERY_CTL_H a
	LEFT JOIN TB_M_SYSTEM s ON s.SYSTEM_CD = ''TRANS_CD_SA_POSTING''
	LEFT JOIN TB_R_DELIVERY_CTL_D b ON b.DELIVERY_NO = a.DELIVERY_NO
	LEFT JOIN TB_M_LOGISTIC_PARTNER c ON c.LOG_PARTNER_CD = a.LP_CD
	LEFT JOIN TB_R_DLV_SERVICE_DOC d ON d.REFF_NO = a.DELIVERY_NO AND d.TRANSACTION_CD = s.SYSTEM_VALUE
	LEFT JOIN TB_R_DLV_PO_H e ON e.PROD_MONTH = CAST(YEAR(a.PICKUP_DT) as VARCHAR(4))+dbo.FN_VARMONTH(a.PICKUP_DT) AND SUBSTRING(e.PO_NO, 1,3) = a.LP_CD AND e.RELEASED_FLAG = ''Y''
	LEFT JOIN TB_R_DLV_INV_UPLOAD f ON f.PO_NO = e.PO_NO AND f.DELIVERY_NO = a.DELIVERY_NO AND f.STATUS_CD NOT IN (''-3'')
	LEFT JOIN TB_R_DLV_PO_ITEM g ON g.ROUTE_CD = a.[ROUTE] AND g.PO_NO = e.PO_NO
	WHERE 1 = 1 AND c.ACTIVE_FLAG = 2 '

  if(@@DELIVERY_NO <> '')
	set @@sql = @@sql + 'and a.[DELIVERY_NO] LIKE '+ cast(@@DELIVERY_NO as varchar(max)) +' '

  if(@@DELIVERY_TYPE <> '')
	set @@sql = @@sql + 'and LEFT(a.[DELIVERY_NO],1) IN ('+ cast(@@DELIVERY_TYPE as varchar(max)) +') '
	--set @@sql = @@sql + 'and a.[DELIVERY_TYPE] IN ('+ cast(@@DELIVERY_TYPE as varchar(max)) +') '

  if(@@LP_CD <> '')
	set @@sql = @@sql + 'and a.[LP_CD] IN ('+ cast (@@LP_CD as varchar(max)) +') '

  if(@@ROUTE <> '')
	set @@sql = @@sql + 'and a.[ROUTE] IN ('+ cast (@@ROUTE as varchar(max)) + ') '

  if(@@DELIVERY_STS <> '')
	set @@sql = @@sql + 'and a.[DELIVERY_STS] IN ('+ cast (@@DELIVERY_STS as varchar(max)) + ') '

  if(@@PO_NO <> '')
	set @@sql = @@sql + 'and e.[PO_NO] LIKE '+ cast (@@PO_NO as varchar(max)) + ' '
	
  if(@@ROUTE_DATE <> '' AND @@ROUTE_DATE_TO <>'')
	set @@sql = @@sql + 'and (a.[PICKUP_DT] BETWEEN '+@@ROUTE_DATE+' AND '+@@ROUTE_DATE_TO+' )'

  set @@sql = @@sql + 'GROUP BY a.DELIVERY_NO, a.DELIVERY_TYPE, a.LP_CD, a.ROUTE, a.RATE, a.PICKUP_DT, a.DELIVERY_STS, c.LOG_PARTNER_NAME, e.PO_NO, f.LP_INV_NO, f.CREATED_BY, f.CREATED_DT '
  set @@sql = @@sql + 'ORDER BY a.PICKUP_DT '

  if(@@DELIVERY_NO <> '' OR @@DELIVERY_TYPE <> '' OR @@LP_CD <> '' OR @@ROUTE <> '' OR @@DELIVERY_STS <> '')
	execute (@@sql)