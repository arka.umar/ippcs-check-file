﻿SELECT 
	d.DELIVERY_NO, 
	d.STATION, 
	d.DOCK_CD, 
	h.ROUTE, 
	h.RATE, 
	h.REVISE_NO,
	d.ARRIVAL_ACTUAL_DT, 
	d.ARRIVAL_PLAN_DT, 
	d.ARRIVAL_GAP,  
	d.ARRIVAL_STATUS, 
	ARRIVAL_STATUS_DESC = case d.ARRIVAL_STATUS 
							--when 2 then 'Delay Waiting'
							--when 1 then 'Delay LP'
							when 99 then 'Advanced'
							when 3 then 'OnTime'
							when 2 then 'Delay'
							when 1 then 'Delay'
							else 'None'
						end,
	ARRIVAL_STATUS_IMAGE = case d.ARRIVAL_STATUS 
							when 99 then '~/Content/Images/biggreendot.png'
							when 3 then '~/Content/Images/biggreendot.png'
							when 2 then '~/Content/Images/bigreddot2.jpg'
							when 1 then '~/Content/Images/bigreddot2.jpg'
							else '~/Content/Images/biggraydot2.jpg'
						end,
	d.DEPARTURE_ACTUAL_DT, 
	d.DEPARTURE_PLAN_DT, 
	d.DEPARTURE_GAP, 
	d.DEPARTURE_STATUS,  
	DEPARTURE_STATUS_DESC = case d.DEPARTURE_STATUS 
							--when 2 then 'Delay Waiting'
							--when 1 then 'Delay LP'
							when 99 then 'Advanced'
							when 3 then 'OnTime'
							when 2 then 'Delay'
							when 1 then 'Delay'
							else 'None'
						end,
	DEPARTURE_STATUS_IMAGE = case d.DEPARTURE_STATUS 
							when 99 then '~/Content/Images/biggreendot.png'
							when 3 then '~/Content/Images/biggreendot.png'
							when 2 then '~/Content/Images/bigreddot2.jpg'
							when 1 then '~/Content/Images/bigreddot2.jpg'
							else '~/Content/Images/biggraydot2.jpg'
						end,
	REMAINING = 
		case 
			when (DATEDIFF(SECOND,ARRIVAL_ACTUAL_DT,DEPARTURE_PLAN_DT)) < 0 then 
				'00:00' 
			else
				RIGHT('0' + CAST(DATEDIFF(SECOND,ARRIVAL_ACTUAL_DT,DEPARTURE_PLAN_DT)/3600 AS VARCHAR(3)),2) + ':' + 
				RIGHT('0' + CAST(
					case when RIGHT('0' + CAST((DATEDIFF(SECOND,ARRIVAL_ACTUAL_DT,DEPARTURE_PLAN_DT) % 60) AS VARCHAR(2)), 2) <> 0 
						then ((DATEDIFF(SECOND,ARRIVAL_ACTUAL_DT,DEPARTURE_PLAN_DT) % 3600) / 60) + 1 else (DATEDIFF(SECOND,ARRIVAL_ACTUAL_DT,DEPARTURE_PLAN_DT) % 3600) / 60 end					 
				AS VARCHAR(2)), 2)				
				end,
	CurrentTime = isnull(d.DEPARTURE_ACTUAL_DT,getdate())
	--REMAINING = right('00' + convert(varchar,DATEPART(HOUR,d.REMAINING)),2) + ':' + 
	--	right('00' + convert(varchar,
	--		case when DATEPART(SECOND,d.REMAINING) > 0 then DATEPART(MINUTE,d.REMAINING) + 1 else DATEPART(MINUTE,d.REMAINING) end),2)
FROM DBO.[TB_R_DELIVERY_CTL_D] d
inner join DBO.[TB_R_DELIVERY_CTL_H] h on d.DELIVERY_NO = h.DELIVERY_NO
where d.DELIVERY_NO=@0 and d.[DOCK_CD]=@1