﻿UPDATE dbo.TB_M_PART_BO
	SET QTY_PER_BOX = @5 , -- QTY_PER_BOX - int
		INACTIVE_FLAG = @6 , -- INACTIVE_FLAG - char(1)
		CHANGED_BY = @7 , -- CREATED_BY - varchar(20)
		CHANGED_DT = GETDATE()
	WHERE
		PART_NO = @0 AND -- PART_NO - varchar(15)
		SUPPLIER_CD = LEFT(@1, 4) AND -- SUPPLIER_CD - varchar(4)
		SUPPLIER_PLANT = RIGHT(@1, 1) AND -- SUPPLIER_PLANT - char(1)
		DOCK_CD = @2 -- DOCK_CD - varchar(2)

SELECT 'Success update the data.'
