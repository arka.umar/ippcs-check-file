DECLARE
@@apYear AS VARCHAR(4) = @0
,@@version AS VARCHAR(3) = @1


SELECT 
PRODUCTION_YEAR, 
[VERSION],
COUNT(*)AS TOTAL_RECORDS,
MAX(CREATED_BY) AS CREATED_BY , 
ISNULL(CONVERT(varchar(10),max(CREATED_DT),104),'')+'   '+ISNULL(CONVERT(varchar(5),max(CREATED_DT),108),'') AS CREATED_DT,
MAX(APPROVE_BY) AS APPROVE_BY, 
ISNULL(CONVERT(varchar(10),max(APPROVE_DT),104),'')+'   '+ISNULL(CONVERT(varchar(5),max(APPROVE_DT),108),'') AS APPROVE_DT
FROM TB_T_ANNUAL_CROSSTAB_H 
where
((PRODUCTION_YEAR='' + @@apYear + '' AND ISNULL('' + @@apYear + '', '') <> '') or (ISNULL('' + @@apYear + '', '') = ''))   AND
((VERSION='' + @@version + '' AND ISNULL('' + @@version + '', '') <> '') or (ISNULL('' + @@version + '', '') = ''))
GROUP BY PRODUCTION_YEAR, [VERSION] 


