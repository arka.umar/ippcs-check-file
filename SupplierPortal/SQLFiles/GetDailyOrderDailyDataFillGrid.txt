﻿declare @@sql nvarchar(max), 
		@@supplier varchar(15) = @0,
		@@dockCode varchar(15) = @1,
		@@manifestNo varchar(15) = @2,
		@@dateFrom varchar(10) = @3,
		@@dateTo varchar(10) = @4,
		@@parameter varchar(10) = @5
		
set @@sql = 'SELECT OM.ORDER_RELEASE_DT ORDER_RELEASE_DT, OM.SUPPLIER_CD SUPPLIER_CD, 
			COUNT(OM.Manifest_NO) AS TOTAL_MANIFEST,
			SUM(OM.[TOTAL_QTY]) AS ORDER_PLAN_QTY,
			OM.ORDER_TYPE, OM.SUPPLIER_PLANT SUPPLIER_PLANT_CD,
			SUM(OP.REMAINING_ITEM) REMANING_QTY
			FROM TB_R_DAILY_ORDER_MANIFEST OM
			INNER JOIN (SELECT (SUM(ORDER_QTY)-SUM(RECEIVED_QTY)) REMAINING_ITEM, MANIFEST_NO FROM TB_R_DAILY_ORDER_PART GROUP BY MANIFEST_NO) OP
			ON OM.MANIFEST_NO = OP.MANIFEST_NO
			where 1=1'
			
if(@@supplier <> '')
	set @@sql = @@sql + 'and Supplier_CD = ''' + @@supplier + ''' '

if(@@dockCode <> '')
	set @@sql = @@sql + 'and Dock_CD = '''+ @@dockCode + ''' '

if(@@manifestNo <> '')
	set @@sql = @@sql + 'and Manifest_No = ''' + @@manifestNo + ''' '

if(@@dateFrom <> '')
	set @@sql = @@sql + 'and ORDER_RELEASE_DT >= ''' + cast(@@dateFrom as varchar) + ''' '

if(@@dateTo <> '')
	set @@sql = @@sql + 'and ORDER_RELEASE_DT <= ''' + cast(@@dateTo as varchar) + ''' '

set @@sql = @@sql + 'and OM.Order_Type = ''' + cast(@@parameter as varchar) + '''
					 and OM.MANIFEST_NO <>'''' 
				     and OM.[ORDER_RELEASE_DT] is not null
				     group by OM.[SUPPLIER_CD], OM.[ORDER_TYPE], OM.[ORDER_RELEASE_DT], OM.[SUPPLIER_PLANT]
				     order by [ORDER_RELEASE_DT]'
execute (@@sql)

