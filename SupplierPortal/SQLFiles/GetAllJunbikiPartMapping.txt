﻿
SELECT j.[KATASHIKI_CD] as [KatashikiCode] 
      ,j.[KATASHIKI_SFX] as [KatashikiSuffix] 
      ,j.[PART_NO] as [PartNo]
      ,j.[SUPPLIER_CD] as [SupplierCode]
      ,j.[SUPPLIER_PLANT] as [SupplierPlant]
      ,j.[DOCK_CD] as [DockCode] 
      ,j.[ITEM_QTY] as [ItemQty] 
      ,j.[VALID_FROM] as [ValidFrom]
      ,j.[VALID_TO] as [ValidTo]
      ,j.[CREATED_BY] as [CreatedBy]
      ,j.[CREATED_DT] as [CreatedDate]
      ,j.[CHANGED_BY] as [ChangedBy]
      ,j.[CHANGED_DT] as [ChangedDate]
  FROM [dbo].[TB_M_JUNBIKI_PART_MAPPING] AS j

WHERE DATEDIFF(DAY, GETDATE(), j.VALID_FROM) <= 0 AND DATEDIFF(DAY, GETDATE(), j.VALID_TO) >= 0