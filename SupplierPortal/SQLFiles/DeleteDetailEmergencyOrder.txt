﻿Delete TB_T_EMERGENCY_ORDER_PART
where SessionID=@0 AND 
	UserID=@1 AND 
	SUPPLIER_CD=@2 AND 
	SUPPLIER_PLANT=@3 AND 
	DOCK_CD=@4 AND 
	PART_NO=REPLACE(@5,'-','')