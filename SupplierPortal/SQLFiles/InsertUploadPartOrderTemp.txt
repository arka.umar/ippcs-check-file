if exists(select * from TB_T_UPLOAD_ORDER_PART 
		  where Order_No=@0 
				and DOCK_CD=@1			
				and SUPPLIER_CD=@2
				and SUPPLIER_PLANT=@3
				and Part_NO=@4)
	BEGIN
		update TB_T_UPLOAD_ORDER_PART
			set ORDER_PLAN_QTY = @6,
				QTY_PER_CONTAINER = @7
		where Order_No=@0
				and DOCK_CD=@1
				and SUPPLIER_CD=@2 
				and SUPPLIER_PLANT=@3
				and Part_NO=@4
	END
else
	BEGIN
		INSERT INTO TB_T_UPLOAD_ORDER_PART 
		(
			ORDER_NO,
			DOCK_CD,
			SUPPLIER_CD,
			SUPPLIER_PLANT,
			PART_NO,
			PART_NAME,
			ORDER_PLAN_QTY,
			QTY_PER_CONTAINER
		)
		VALUES (@0,@1,@2,@3,@4,@5,@6,@7)
	END