﻿BEGIN TRY
	DECLARE @@T AS TABLE (PID BIGINT)
	DECLARE @@pid AS BIGINT = 0

	INSERT @@T
	EXEC dbo.sp_PutLog 'Saving feedback', @0, 'FD_FEEDBACK_CD.Process', @@pid OUTPUT, 'MPCS00004INF', 'INF', 3, '31301';

	IF EXISTS(SELECT DOCUMENT_NO FROM TB_R_REC_H WHERE DOCUMENT_NO = @1)
	BEGIN
		IF ((SELECT COUNT(1) FROM TB_R_REC_H WHERE DOCUMENT_NO = @1 AND ((FD_FEEDBACK_CD IS NULL) OR (FD_FEEDBACK_CD = ''))) = 0)
			SELECT 'failed' AS MSTATUS, 'TMMIN has input feedback' AS MFEEDBACK
		ELSE
		BEGIN
			EXEC dbo.SP_SaveFeedbackTMMINOK @0, @1, @2
			SELECT 'success' AS MSTATUS, 'TMMIN has been saved' AS MFEEDBACK
		END
	END ELSE
		SELECT 'failed' AS MSTATUS, 'TMMIN has not been saved' AS MFEEDBACK
	
	INSERT @@T
	EXEC dbo.sp_PutLog 'Saving feedback finish', @0, 'SUPP_FEEDBACK_CD.Finish', @@pid OUTPUT, 'MPCS00003INF', 'INF', 3, '31301';

END TRY
BEGIN CATCH

	DECLARE @@log VARCHAR(MAX) = 'Error when saving : ' + ERROR_MESSAGE();
	
	INSERT @@T
	EXEC dbo.sp_PutLog @@log , @0, 'SUPP_AMOUNT.Process', @@pid OUTPUT, 'MPCS00008ERR', 'ERR', 3, '31301';
	
	SELECT 'failed' AS MSTATUS, 'Error when saving feedback' AS MFEEDBACK

END CATCH