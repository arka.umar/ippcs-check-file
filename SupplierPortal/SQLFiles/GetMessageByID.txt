﻿SELECT MESSAGE_ID MessageID
      ,MESSAGE_TEXT MessageText
      ,MESSAGE_TYPE MessageType
      ,[DESCRIPTION] Description
      ,SOLUTION Solution
      ,PIC1
      ,PIC2
      ,CREATED_BY CreatedBy
      ,CREATED_DT CreatedDt
  FROM TB_M_MESSAGE
  WHERE Message_id=@0