﻿-- =================================================
-- Author: niit.arie
-- Create date: 30 Dec 2013
-- Update date: 30 Dec 2013
-- Description: Query Insert Supplier Performace Status
-- =================================================



DECLARE @@ProductionMonth Datetime = @0
	,@@SupplierCode VARCHAR(10) = @1
	,@@UserCreated VARCHAR(50) = @2

IF EXISTS (SELECT 1 FROM TB_R_SUPPLIER_PERFORMANCE_STATUS 
	WHERE PROD_MONTH  = @@ProductionMonth
		AND SUPPLIER_CD = @@SupplierCode)
BEGIN
	--SELECT 'UPDATE DATA'
	UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
		SET EDIT_FLAG = 1 ,
			CHANGED_BY = @@UserCreated,
			CHANGED_DT = GETDATE()			
	WHERE PROD_MONTH = @@ProductionMonth 
		AND SUPPLIER_CD = @@SupplierCode

	SELECT 'Success to Released Data'	
END

ELSE
BEGIN
	--SELECT 'INSERT DATA'
	INSERT INTO TB_R_SUPPLIER_PERFORMANCE_STATUS
	        ( PROD_MONTH ,
	          SUPPLIER_CD ,
	          RELEASE_FLAG ,
	          EDIT_FLAG ,
	          CREATED_BY ,
	          CREATED_DT ,
	          CHANGED_BY ,
	          CHANGED_DT
	        )
	VALUES  ( @@ProductionMonth ,
	          @@SupplierCode ,
	          1 ,
			  0 ,
	          @@UserCreated ,
	          GETDATE() ,
	          @@UserCreated ,
	          GETDATE()
	        )

	SELECT 'Success to Released Data'
END
