﻿select 
       AP.APPROVER_DT Date
      ,AP.APPROVER Name
      ,AP.COMMENT Comment
	  ,AP.STATUS Status
      from dbo.TB_R_APPROVAL_PROCESS AP 
	  --inner join TMMIN_ROLE.dbo.TB_M_USER U ON U.USERNAME=AP.APPROVER 
WHERE 
      AP.POSITION='SH' and AP.REF_NO=@0