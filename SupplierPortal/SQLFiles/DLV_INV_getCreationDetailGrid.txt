﻿Declare @@sql varchar(max),
  @@DELIVERY_NO VARCHAR(max)= @0

set @@sql =  'SELECT CS.COMP_PRICE_TEXT as COMP_PRICE_CD, a.ITEM_CURR, a.PO_DETAIL_PRICE 
				FROM TB_R_DLV_GR_IR a 
					LEFT JOIN TB_M_CALC_SCHEME CS ON CS.COMP_PRICE_CD = a.COMP_PRICE_CD 
				WHERE a.CONDITION_CATEGORY = ''H'' '

if(@@DELIVERY_NO <> '')
	set @@sql = @@sql + 'and [REFF_NO] IN ('+ cast(@@DELIVERY_NO as varchar) +') '

set @@sql = @@sql + 'ORDER BY a.SERVICE_DOC_NO, a.ITEM_NO '

if(@@DELIVERY_NO <> '')
	execute (@@sql)