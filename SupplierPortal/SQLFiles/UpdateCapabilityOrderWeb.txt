﻿update TB_R_DAILY_ORDER_WEB set 
	REPLY_CAPABILITY = @3,
	REPLIED_BY=@4,
	REPLIED_DT=getdate(),
	LOCK_STATUS=1
where Convert(varchar(8),ORDER_RELEASE_DT,112)=Convert(varchar(8),@0,112) AND 
	SUPPLIER_CD = @1 AND 
	SUPPLIER_PLANT_CD = @2 AND
	RCV_PLANT_CD = @5