﻿Declare @@sql varchar(max),
		@@AreaCode varchar(max) = @0
		
		
set @@sql =  ' SELECT [AREA_CD]
      ,[AREA_NM]
      ,[PLANT]
	  ,B.[PLANT_NM]
      ,[PIC_NAME]
      ,[EMAIL_TO]
      ,[EMAIL_CC]
      ,[EMAIL_BCC]
      ,[TELEPHONE]
      ,[MOBILE]
  FROM [TB_M_AREA_MASTER] A
   JOIN TB_M_PLANT B ON A.PLANT = B.PLANT_CD
 WHERE 1=1 '

		
if(@@AreaCode <> '')
	set @@sql = @@sql + ' and AREA_CD = ''' + @@AreaCode +''''
	execute (@@sql)