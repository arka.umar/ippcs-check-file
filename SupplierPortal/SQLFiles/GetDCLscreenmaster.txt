﻿use PCS_DB

Declare @@sql varchar(max),
        @@dateFrom varchar(10) = @0, 
		@@dateTo varchar(10) = @1


set @@sql =  'SELECT [Date]
			 ,[Route]
			 ,RIGHT (''0''+LTRIM(CONVERT(VARCHAR(2),Rate)),2) AS [Rate]
			 ,[TYPE]
			 ,[LPCode]
			 ,[DRIVER_NAME]
			 ,[STATUS]
			 ,[TRUCK_NO]
		     ,[SKID_NO]
	FROM [dbo].[TB_R_LP_ROUTE]
	where 1=1 ORDER BY Date DESC'

if(@@dateFrom <> '')
	set @@sql = @@sql + 'and Date >= '''+ cast(@@dateFrom as varchar) + ''' '

if(@@dateTo <> '')
	set @@sql = @@sql + 'and Date <= '''+ cast(@@dateTo as varchar) + ''' '

execute (@@sql)