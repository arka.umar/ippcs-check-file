﻿-- =================================================
-- Author: riani	
-- Create date: 10 Oct 2022
-- Update date: 10 Oct 2022
-- Description: Query Insert Data in Supplier Performace Non Manufacturing
-- =================================================



DECLARE @@TabActive INT = @0
	,@@IntReturn INT = 1

	,@@ProductionMonth DATE = @1
	,@@SupplierCode VARCHAR(12) = @2
	--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
	,@@PartNo VARCHAR(15) = @3
	,@@QtyDefect FLOAT = @4
	,@@Problem NVARCHAR(MAX) = @5
	,@@ReceivingArea VARCHAR(100) = @6
	,@@CreatedBy VARCHAR(100) = @7

	--delivery
	,@@SupplierMonth FLOAT = @8
	,@@Delay VARCHAR(MAX) =@9
	,@@Shortage FLOAT = @10
	,@@Incorect FLOAT = @11

	--SPD
	,@@Ori FLOAT = @12
	,@@Adj FLOAT = @13
	,@@Receive FLOAT = @14
	,@@Quality FLOAT = @15
	,@@Workability FLOAT = @16

	--TWS
	,@@Claim FLOAT = @17
	,@@Cost FLOAT = @18
	,@@TriY FLOAT = @19

	--safety
	,@@Fatal FLOAT = @20
	,@@Disabilty FLOAT = @21
	,@@LostOrgan FLOAT = @22
	,@@Absent FLOAT = @23
	,@@AbsentPeriode FLOAT = @24
	,@@SmallInjured FLOAT = @25
	,@@TotalWorkers FLOAT = @26
	--modif by alira.agi 2015-04-15
	--,@@Judgment VARCHAR(1) = @27
	,@@Judgment VARCHAR(20) = @27
	--part receive
	,@@QtyReceive FLOAT = @28

	,@@DProblem VARCHAR(50) = @29
	,@@QProblem VARCHAR(50) = @30
	,@@WProblem VARCHAR(50) = @31

	--service part quality replace all ng to int
	--modif by alira.agi 2015-05-26
	,@@SPQtyReceive FLOAT = @32
	--,@@NGQuality VARCHAR(100) = @33
	,@@NGQuality int = @33
	,@@DescQuality VARCHAR(100) = @34
	,@@Time1 VARCHAR(20) = @35
	--,@@NGWorkAbility VARCHAR(100) = @36
	,@@NGWorkAbility int = @36
	,@@DescWorkAbility VARCHAR(100) = @37
	,@@Time2 VARCHAR(20) = @38
	----add by alira.agi 2015-05-26 add column for Delivery
	,@@NGDelivery int = @39
	,@@DescDelivery VARCHAR(100) = @40
	,@@TIME3 VARCHAR(20) = @41

	,@@partName VARCHAR(50) = @42
	,@@ShortageQty int = @43 --shortage tab
	,@@ModelCode VARCHAR(50) = @44

	,@@MisspartQty VARCHAR(5) = @45
	,@@MinLineStop VARCHAR(5) = @46
	,@@cripple VARCHAR(5) = @47
	,@@delayFreq VARCHAR(5) = @48
	,@@supplyPerMonth VARCHAR(5) = @49
	,@@CO2Reduction VARCHAR(5) = @50
	,@@CO2Emission VARCHAR(5) = @51
	,@@ProdYear varchar(5)=@52
	,@@Q1Safety VARCHAR(50) =@53
    ,@@Q2Safety VARCHAR(50) =@54
    ,@@Q3Safety VARCHAR(50) =@55
    ,@@Q4Safety VARCHAR(50) =@56
    ,@@TargetSafety VARCHAR(50) =@57
    ,@@AchievementSafety VARCHAR(50) =@58
    ,@@Q1Quality VARCHAR(50) =@59
    ,@@Q2Quality VARCHAR(50) =@60
    ,@@Q3Quality VARCHAR(50) =@61
    ,@@Q4Quality VARCHAR(50) =@62
    ,@@TargetQuality VARCHAR(50) =@63
    ,@@AchievementQuality VARCHAR(50) =@64
    ,@@Q1Delivery VARCHAR(50) =@65
    ,@@Q2Delivery VARCHAR(50) =@66
    ,@@Q3Delivery VARCHAR(50) =@67
    ,@@Q4Delivery VARCHAR(50) =@68
    ,@@TargetDelivery VARCHAR(50) =@69
    ,@@AchievementDelivery VARCHAR(50) =@70
    ,@@Q1Environment VARCHAR(50) =@71
    ,@@Q2Environment VARCHAR(50) =@72
    ,@@Q3Environment VARCHAR(50) =@73
    ,@@Q4Environment VARCHAR(50) =@74
    ,@@TargetEnvironment VARCHAR(50) =@75
    ,@@AchievementEnvironment VARCHAR(50) =@76
--check supplier code is not exist
IF @@SupplierCode <> ''
BEGIN
	IF NOT EXISTS (SELECT 1 FROM TB_M_SUPPLIER_ICS WHERE SUPP_CD = @@SupplierCode)
	BEGIN 
		SET @@IntReturn = 11
	END
END

--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
--check part number is not exist
--IF @@IntReturn = 1
--BEGIN
--	IF @@PartNo <> ''
--	BEGIN
--		IF NOT EXISTS (SELECT 1 FROM TB_M_PART_INFO WHERE PART_NO = @@PartNo 
--					  AND SUPPLIER_CD = @@SupplierCode AND DOCK_CD = @@ReceivingArea)
--		BEGIN
--			SET @@IntReturn = 22
--		END
--	END
--END

--check dock code is not exist
--IF @@IntReturn = 1
--BEGIN
--	IF @@ReceivingArea <> ''
--	BEGIN
--		IF NOT EXISTS (SELECT 1 FROM TB_M_DOCK WHERE DOCK_CD = @@ReceivingArea)
--		BEGIN
--			SET @@IntReturn = 33
--		END
--	END
--END

IF @@IntReturn = 1
BEGIN

	IF(@@TabActive = 14) --shortage Non Manufacturing grid --tambahan
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_SHORTAGE_NON_MANUFACTURING
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,RCV_AREA
						,SHORTAGE_QTY
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@ReceivingArea
						,@@ShortageQty
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update SHORTAGE NON MANUFACTURING COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING
						SET SHORTAGE_NON_MANUFACTURING = 1,CHANGED_DT=GETDATE(),CHANGED_BY = @@CreatedBy
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING 
						set COMPLETE_FLAG = 1,CHANGED_DT=GETDATE(),CHANGED_BY = @@CreatedBy
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY_NON_MANUFACTURING = 1 and SHORTAGE_NON_MANUFACTURING = 1 and MISSPART_NON_MANUFACTURING = 1 and ENVIRONMENT=1 and KPI_PROCESS =1													

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING (
						PRODUCTION_MONTH
						,SUPPLIER_CD
						,RELEASE_FLAG
						,EDIT_FLAG
						,COMPLETE_FLAG
						,QUALITY_NON_MANUFACTURING
						,SHORTAGE_NON_MANUFACTURING
						,MISSPART_NON_MANUFACTURING
						,ENVIRONMENT
						,KPI_PROCESS
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						)
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							0, -- quality Non manufacturing
							1, -- shortage non manufacturing
							0, -- misspart non manufacturing
							0, -- environment
							0, -- kpi process						
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				IF(EXISTS(SELECT 1 FROM TB_R_SUPPLIER_QUALITY_NON_MANUFACTURING WHERE 
				  PROD_MONTH = @@ProductionMonth AND SUPPLIER_CD = @@SupplierCode 
				  AND PART_NO = @@PartNo AND PROBLEM_DESC = RTRIM(LTRIM(@@Problem))
				  AND RECEIVING_AREA = @@ReceivingArea
				  ))
				  BEGIN
			RAISERROR('duplicate', 16,1);
		END

		
				IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
				begin
				INSERT INTO TB_R_SUPPLIER_QUALITY_NON_MANUFACTURING
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,QTY_DEFECT
						,PROBLEM_DESC
						,RECEIVING_AREA
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						,MODEL_CD)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@QtyDefect
						,@@Problem
						,@@ReceivingArea
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE()
						,@@ModelCode);
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update QUALITY NON MANUFACTURING COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING
						SET QUALITY_NON_MANUFACTURING = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING 
						set COMPLETE_FLAG = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY_NON_MANUFACTURING = 1 and SHORTAGE_NON_MANUFACTURING = 1 and MISSPART_NON_MANUFACTURING = 1 and ENVIRONMENT=1 and KPI_PROCESS =1													

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING (
						PRODUCTION_MONTH
						,SUPPLIER_CD
						,RELEASE_FLAG
						,EDIT_FLAG
						,COMPLETE_FLAG
						,QUALITY_NON_MANUFACTURING
						,SHORTAGE_NON_MANUFACTURING
						,MISSPART_NON_MANUFACTURING
						,ENVIRONMENT
						,KPI_PROCESS
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						)
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, -- release_flag
							0, -- edit_flag
							0, -- complete_flag
							1, -- quality Non manufacturing
							0, -- shortage non manufacturing
							0, -- misspart non manufacturing
							0, -- environment
							0, -- kpi process						
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL
						)

						end				
		end
				SELECT @@TabActive;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	


	END
	-----------------------------------------------------------------------------------------
	ELSE IF(@@TabActive = 15) --misspart Non Manufacturing grid --tambahan
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_MISSPART_NON_MANUFACTURING
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,RCV_AREA
						,MISSPART_QTY
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@ReceivingArea
						,@@MisspartQty
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update MISSPART NON MANUFACTURING COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING
						SET MISSPART_NON_MANUFACTURING = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING 
						set COMPLETE_FLAG = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY_NON_MANUFACTURING = 1 and SHORTAGE_NON_MANUFACTURING = 1 and MISSPART_NON_MANUFACTURING = 1 and ENVIRONMENT=1 and KPI_PROCESS =1													

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING (
						PRODUCTION_MONTH
						,SUPPLIER_CD
						,RELEASE_FLAG
						,EDIT_FLAG
						,COMPLETE_FLAG
						,QUALITY_NON_MANUFACTURING
						,SHORTAGE_NON_MANUFACTURING
						,MISSPART_NON_MANUFACTURING
						,ENVIRONMENT
						,KPI_PROCESS
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						)
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, -- release_flag
							0, -- edit_flag
							0, -- complete_flag
							0, -- quality Non manufacturing
							0, -- shortage non manufacturing
							1, -- misspart non manufacturing
							0, -- environment
							0, -- kpi process						
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status

					IF(EXISTS(SELECT 1 FROM TB_R_SUPPLIER_QUALITY_NON_MANUFACTURING WHERE 
				  PROD_MONTH = @@ProductionMonth AND SUPPLIER_CD = @@SupplierCode 
				  AND PART_NO = @@PartNo AND PROBLEM_DESC = RTRIM(LTRIM(@@Problem))
				  AND RECEIVING_AREA = @@ReceivingArea
				  ))
				  BEGIN
			RAISERROR('duplicate', 16,1);
		END

		
				IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
				begin
				INSERT INTO TB_R_SUPPLIER_QUALITY_NON_MANUFACTURING
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,QTY_DEFECT
						,PROBLEM_DESC
						,RECEIVING_AREA
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						,MODEL_CD)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@QtyDefect
						,@@Problem
						,@@ReceivingArea
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE()
						,@@ModelCode);
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update QUALITY NON MANUFACTURING COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING
						SET QUALITY_NON_MANUFACTURING = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING 
						set COMPLETE_FLAG = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY_NON_MANUFACTURING = 1 and SHORTAGE_NON_MANUFACTURING = 1 and MISSPART_NON_MANUFACTURING = 1 and ENVIRONMENT=1 and KPI_PROCESS =1													

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING (
						PRODUCTION_MONTH
						,SUPPLIER_CD
						,RELEASE_FLAG
						,EDIT_FLAG
						,COMPLETE_FLAG
						,QUALITY_NON_MANUFACTURING
						,SHORTAGE_NON_MANUFACTURING
						,MISSPART_NON_MANUFACTURING
						,ENVIRONMENT
						,KPI_PROCESS
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						)
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, -- release_flag
							0, -- edit_flag
							0, -- complete_flag
							1, -- quality Non manufacturing
							0, -- shortage non manufacturing
							0, -- misspart non manufacturing
							0, -- environment
							0, -- kpi process						
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL
						)

						end				
		end

				SELECT @@TabActive;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	


	END
	--------------------------------------	

	ELSE IF(@@TabActive = 16) --Environment grid --tambahan
	BEGIN		
		declare @@prodMonth VARCHAR(7) = '';

		SET @@prodMonth = REPLACE(LEFT(CAST(@@ProductionMonth AS VARCHAR), 7), '-', '')


		IF(
				(SELECT 'TRUE' WHERE DATEADD(DAY, -1 , (DATEADD(MONTH, 2, CAST(@@prodMonth + '01' AS DATE))))
				< GETDATE())
				= 'TRUE')
			BEGIN
				SELECT @@Judgment = 'Not Submit'
			END
			ELSE IF((SELECT 'TRUE' WHERE CONVERT(date, @@prodMonth + '10', 112) > GETDATE())
				= 'TRUE')
			BEGIN
				SELECT @@Judgment = 'OnTime'
			END
			ELSE IF(
				(SELECT 'TRUE' WHERE GETDATE()
					BETWEEN CONVERT(date, @@prodMonth + '01', 112)
					AND DATEADD(MONTH, 1, CAST(@@prodMonth + '11' AS DATE)))
					= 'TRUE'
				)
			BEGIN
				SELECT @@Judgment = 'OnTime'
			END
			ELSE
			BEGIN
				SELECT @@Judgment = 'Delay'
			END
				INSERT INTO TB_R_SUPPLIER_ENVIRONMENT
						(PROD_MONTH
						,SUPPLIER_CD
						,CO2_REDUCTION
						,CO2_EMISSION						
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@CO2Reduction
						,@@CO2Emission						
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update MISSPART NON MANUFACTURING COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING
						SET ENVIRONMENT = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING 
						set COMPLETE_FLAG = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY_NON_MANUFACTURING = 1 and SHORTAGE_NON_MANUFACTURING = 1 and MISSPART_NON_MANUFACTURING = 1 and ENVIRONMENT=1 and KPI_PROCESS =1													

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING (
						PRODUCTION_MONTH
						,SUPPLIER_CD
						,RELEASE_FLAG
						,EDIT_FLAG
						,COMPLETE_FLAG
						,QUALITY_NON_MANUFACTURING
						,SHORTAGE_NON_MANUFACTURING
						,MISSPART_NON_MANUFACTURING
						,ENVIRONMENT
						,KPI_PROCESS
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						)
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, -- release_flag
							0, -- edit_flag
							0, -- complete_flag
							0, -- quality Non manufacturing
							0, -- shortage non manufacturing
							0, -- misspart non manufacturing
							1, -- environment
							0, -- kpi process						
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status									

				SELECT @@TabActive;				
	END
	--------------------------------------	
	ELSE IF(@@TabActive = 17) --Environment Master grid --tambahan
	BEGIN				
				INSERT INTO TB_M_SUPPLIER_ENVIRONMENT
						(PROD_YEAR
						,SUPPLIER_CD
						,CO2_REDUCTION
						,CO2_EMISSION
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProdYear
						,@@SupplierCode
						,@@CO2Reduction
						,@@CO2Emission
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());									

				SELECT @@TabActive;				
	END
	--------------------------------------	
	ELSE IF(@@TabActive = 18) --KPI grid --tambahan
	BEGIN		
		
				INSERT INTO TB_R_SUPPLIER_KPI
						(PROD_MONTH
						,SUPPLIER_CD
						,Q1_SAFETY
						,Q2_SAFETY
						,Q3_SAFETY
						,Q4_SAFETY
						,TARGET_SAFETY
						,ACHIEVEMENT_SAFETY
						,Q1_QUALITY
						,Q2_QUALITY
						,Q3_QUALITY
						,Q4_QUALITY
						,TARGET_QUALITY
						,ACHIEVEMENT_QUALITY
						,Q1_DELIVERY
						,Q2_DELIVERY
						,Q3_DELIVERY
						,Q4_DELIVERY
						,TARGET_DELIVERY
						,ACHIEVEMENT_DELIVERY
						,Q1_ENVIRONMENT
						,Q2_ENVIRONMENT
						,Q3_ENVIRONMENT
						,Q4_ENVIRONMENT
						,TARGET_ENVIRONMENT
						,ACHIEVEMENT_ENVIRONMENT
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@Q1Safety
						,@@Q2Safety
						,@@Q3Safety
						,@@Q4Safety
						,@@TargetSafety
						,@@AchievementSafety
						,@@Q1Quality
						,@@Q2Quality
						,@@Q3Quality
						,@@Q4Quality
						,@@TargetQuality
						,@@AchievementQuality
						,@@Q1Delivery
						,@@Q2Delivery
						,@@Q3Delivery
						,@@Q4Delivery
						,@@TargetDelivery
						,@@AchievementDelivery
						,@@Q1Environment
						,@@Q2Environment
						,@@Q3Environment
						,@@Q4Environment
						,@@TargetEnvironment
						,@@AchievementEnvironment					
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update MISSPART NON MANUFACTURING COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING
						SET KPI_PROCESS = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING 
						set COMPLETE_FLAG = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY_NON_MANUFACTURING = 1 and SHORTAGE_NON_MANUFACTURING = 1 and MISSPART_NON_MANUFACTURING = 1 and ENVIRONMENT=1 and KPI_PROCESS =1													

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING (
						PRODUCTION_MONTH
						,SUPPLIER_CD
						,RELEASE_FLAG
						,EDIT_FLAG
						,COMPLETE_FLAG
						,QUALITY_NON_MANUFACTURING
						,SHORTAGE_NON_MANUFACTURING
						,MISSPART_NON_MANUFACTURING
						,ENVIRONMENT
						,KPI_PROCESS
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						)
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, -- release_flag
							0, -- edit_flag
							0, -- complete_flag
							0, -- quality Non manufacturing
							0, -- shortage non manufacturing
							0, -- misspart non manufacturing
							0, -- environment
							1, -- kpi process						
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status									

				SELECT @@TabActive;				
	END
	--------------------------------------	
	ELSE --tabindex 0
	BEGIN
		--modif by alira.agi 2015-04-15
		--add validate part no
		--if exists (select '' from TB_M_PART_INFO where PART_NO = LTRIM(RTRIM(@@PartNo))) 
		   --and 
		
		--check duplicate
		IF(EXISTS(SELECT 1 FROM TB_R_SUPPLIER_QUALITY_NON_MANUFACTURING WHERE 
				  PROD_MONTH = @@ProductionMonth AND SUPPLIER_CD = @@SupplierCode 
				  AND PART_NO = @@PartNo AND PROBLEM_DESC = RTRIM(LTRIM(@@Problem))
				  AND RECEIVING_AREA = @@ReceivingArea
				  )
			)
		BEGIN
			RAISERROR('duplicate', 16,1);
		END

		
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_QUALITY_NON_MANUFACTURING
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,QTY_DEFECT
						,PROBLEM_DESC
						,RECEIVING_AREA
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						,MODEL_CD)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@QtyDefect
						,@@Problem
						,@@ReceivingArea
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE()
						,@@ModelCode);
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update QUALITY NON MANUFACTURING COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING
						SET QUALITY_NON_MANUFACTURING = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING 
						set COMPLETE_FLAG = 1,CHANGED_BY = @@CreatedBy,CHANGED_DT = GETDATE()
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY_NON_MANUFACTURING = 1 and SHORTAGE_NON_MANUFACTURING = 1 and MISSPART_NON_MANUFACTURING = 1 and ENVIRONMENT=1 and KPI_PROCESS =1													

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS_NON_MANUFACTURING (
						PRODUCTION_MONTH
						,SUPPLIER_CD
						,RELEASE_FLAG
						,EDIT_FLAG
						,COMPLETE_FLAG
						,QUALITY_NON_MANUFACTURING
						,SHORTAGE_NON_MANUFACTURING
						,MISSPART_NON_MANUFACTURING
						,ENVIRONMENT
						,KPI_PROCESS
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						)
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, -- release_flag
							0, -- edit_flag
							0, -- complete_flag
							1, -- quality Non manufacturing
							0, -- shortage non manufacturing
							0, -- misspart non manufacturing
							0, -- environment
							0, -- kpi process						
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				SELECT @@TabActive;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	

	END

END 
ELSE
BEGIN
	SELECT @@IntReturn
END