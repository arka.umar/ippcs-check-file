﻿select orderPart.[PART_NO] PartNo, masterPart.[PART_NAME] PartName, orderPart.[ORDER_QTY] OrderQty,
       orderPart.[QTY_PER_CONTAINER] PCSKanban
from TB_R_DAILY_ORDER_PART orderPart
INNER JOIN TB_R_DAILY_ORDER_MANIFEST m on orderPart.MANIFEST_NO=m.MANIFEST_NO
left join TB_M_PART masterPart on masterPart.PART_NO = orderPart.PART_NO
where m.ORDER_NO=@0 and m.SUPPLIER_CD=@1 and m.DOCK_CD=@2 and m.SUPPLIER_PLANT=@3