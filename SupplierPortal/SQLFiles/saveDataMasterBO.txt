﻿IF((SELECT COUNT(1) FROM dbo.TB_M_PART_INFO 
	WHERE PART_NO = @0 AND SUPPLIER_CD + '-' + SUPPLIER_PLANT = @1
	AND DOCK_CD = @2 AND KANBAN_NO = @3) != 1)
BEGIN
	SELECT 'Part not found.'
	RETURN
END

IF(
	(SELECT COUNT(1) FROM TB_M_PART_BO 
	WHERE
		PART_NO = @0 AND
		SUPPLIER_CD + '-' + SUPPLIER_PLANT = @1 AND
		DOCK_CD = @2 /*AND KANBAN_NO = @3*/) = 0
  )
BEGIN
	INSERT INTO dbo.TB_M_PART_BO
			( PART_NO ,
			  SUPPLIER_CD ,
			  SUPPLIER_PLANT ,
			  DOCK_CD ,
			  KANBAN_NO ,
			  PART_NAME ,
			  QTY_PER_BOX ,
			  INACTIVE_FLAG ,
			  CREATED_BY ,
			  CREATED_DT ,
			  CHANGED_BY ,
			  CHANGED_DT
			)
	VALUES  ( @0 , -- PART_NO - varchar(15)
			  LEFT(@1, 4) , -- SUPPLIER_CD - varchar(4)
			  RIGHT(@1, 1) , -- SUPPLIER_PLANT - char(1)
			  @2 , -- DOCK_CD - varchar(2)
			  @3 , -- KANBAN_NO - varchar(5)
			  @4 , -- PART_NAME - varchar(40)
			  @5 , -- QTY_PER_BOX - int
			  @6 , -- INACTIVE_FLAG - char(1)
			  @7 , -- CREATED_BY - varchar(20)
			  GETDATE() , -- CREATED_DT - datetime
			  '' , -- CHANGED_BY - varchar(20)
			  ''  -- CHANGED_DT - datetime
			)

	SELECT 'Success save the data.'
END
ELSE
BEGIN
	SELECT 'Fail when save, data already exist.'
END