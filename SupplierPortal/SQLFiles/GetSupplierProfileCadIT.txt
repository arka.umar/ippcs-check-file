﻿Declare @@sql varchar(max),
        @@SuppCd varchar(max) = REPLACE(@0,';',''',''')
	
		
set @@sql = '
	SELECT 
		A.SUPPLIER_CD,
		(select top 1 substring(d.SUPP_NAME,1, case when patindex(''%(%'',d.SUPP_NAME)-1>0 then patindex(''%(%'',d.SUPP_NAME)-1
		else len(d.SUPP_NAME) end ) from TB_M_SUPPLIER_ICS d where d.SUPP_CD=a.SUPPLIER_CD )SUPPLIER_NAME,
		CONFIRM_BY_SUPPLIER,
		CONFIRM_DT_SUPPLIER,
		A.CAD_TYPE,
		A.ITEM_NO,
		ISNULL(CONVERT(VARCHAR(MAX), TERMINAL_AMOUNT), '''') as TERMINAL_AMOUNT,
		CAM_COMPLETED,
		ENGINEER,
		ISNULL(CONVERT(VARCHAR(MAX), INTERNET_BANDWITH), '''') as INTERNET_BANDWITH,
		OPERATOR,

		convert(varchar(max), CAST(TERMINAL_AMOUNT as money), -1) as TERMINAL_AMOUNT_F,
		convert(varchar(max), CAST(ENGINEER as money), -1) as ENGINEER_F,
		convert(varchar(max), CAST(OPERATOR as money), -1) as OPERATOR_F,

		PRINTER,
		PRINTER_SIZE,
		A.CHANGED_BY,
		A.CHANGED_DT,
		A.CREATED_BY,
		A.CREATED_DT  
  FROM TB_M_SUPPLIER_CAD_IT A
	where 1=1 '

if(@@SuppCd <> '')
	set @@sql = @@sql + 'and A.SUPPLIER_CD  IN ('''+ @@SuppCd +''') '


execute (@@sql)