DECLARE @@sql VARCHAR(MAX)

SET @@sql = '
		SELECT ROUTE_CD
			  , ROUTE_CD_CONV
			  , ROUTE_CONV_NAME
			  , LP_CD
			  , VALID_FROM
			  , VALID_TO
			  , CREATED_BY
			  , CREATED_DT
			  , CHANGED_BY
			  , CHANGED_DT
		FROM TB_M_ROUTE_CONVERSION
		  WHERE 1=1
	'

IF (@0 <> '')
BEGIN
	SET @@sql = @@sql + ' AND ROUTE_CD = ''' + @0 + ''''
END

IF (@1 <> '')
BEGIN
	SET @@sql = @@sql + ' AND ROUTE_CD_CONV = ''' + @1 + ''''
END

EXEC(@@sql)