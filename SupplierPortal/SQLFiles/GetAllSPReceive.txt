﻿-- =================================================
-- Author: niit.arie
-- Create date: 12 Dec 2013
-- Update date: 12 Dec 2013
-- Description: Query Grid Detail Supplier Performace Tab Part Receive
-- modif by alira.agi 2015-04-13
-- =================================================


DECLARE @@SupplierCode VARCHAR(20) = @0
        ,@@ProdMonth VARCHAR(20) = @1
        ,@@ProdMonthTo VARCHAR(20) = @2
        ,@@DockCode VARCHAR(20) = @3

DECLARE @@from DATETIME = CONVERT(DATETIME, @@ProdMOnth)
DECLARE @@to DATETIME = CONVERT(DATETIME, @@ProdMOnthTo)
SELECT 
	PROD_MONTH,
	SUPP_CD as SUPPLIER_CD,
	SUPP_NAME as SUPPLIER_NAME,
	SUM(QTY) as QTY_RECEIVE,
	RECEIVING_AREA
FROM TB_R_SUPPLIER_PART_RECEIVED
WHERE (CONVERT(INT, LEFT(PROD_MONTH, 2)) >= MONTH(@@from) AND (CONVERT(INT, LEFT(PROD_MONTH, 2)) <= MONTH(@@to)))
	  AND (RIGHT(PROD_MONTH, 4) >= YEAR(@@from) AND RIGHT(PROD_MONTH, 4) <= YEAR(@@to))
	  AND ((SUPP_CD = @@SupplierCode AND ISNULL(@@SupplierCode, '') <> '') OR ISNULL(@@SupplierCode, '') = '')
GROUP BY
	PROD_MONTH,
	SUPP_CD,
	SUPP_NAME,
	RECEIVING_AREA

ORDER BY PROD_MONTH, SUPP_CD, SUPP_NAME


