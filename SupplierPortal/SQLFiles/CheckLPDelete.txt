﻿DECLARE @@sql VARCHAR(MAX)

IF(@1 = 'A')
BEGIN
	set @@sql = 'SELECT COUNT (1) AS result FROM TB_M_ROUTE_PRICE WHERE LP_CD IN (' + @0 + ')'
END

IF(@1 = 'B')
BEGIN
	set @@sql = 'SELECT COUNT (1) AS result FROM TB_R_DELIVERY_CTL_H WHERE LP_CD IN (' + @0 + ')'
END

IF(@1 = 'C')
BEGIN
	set @@sql = 'SELECT COUNT (1) AS result FROM TB_T_PR_RETRIEVAL WHERE LP_CD IN (' + @0 + ')'
END

IF(@1 = 'D')
BEGIN
	set @@sql = 'SELECT COUNT (1) AS result FROM TB_R_DLV_PR_H WHERE LP_CD IN (' + @0 + ')'
END

IF(@1 = 'E')
BEGIN
	set @@sql = 'SELECT COUNT (1) AS result FROM TB_R_DLV_PO_H WHERE LP_CD IN (' + @0 + ')'
END

IF(@1 = 'F')
BEGIN
	set @@sql = 'SELECT COUNT (1) AS result FROM TB_T_DLV_SA_RAW WHERE LP_CD IN (' + @0 + ')'
END

IF(@1 = 'G')
BEGIN
	set @@sql = 'SELECT COUNT (1) AS result FROM TB_R_DLV_GR_IR WHERE LP_CD IN (' + @0 + ')'
END

EXEC(@@sql)

