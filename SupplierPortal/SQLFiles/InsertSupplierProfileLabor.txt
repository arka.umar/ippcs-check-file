DECLARE @@NEW_UNION VARCHAR(100) = @18
DECLARE @@UNION VARCHAR(MAX) = @4

IF(UPPER(@19) = 'OTHER')
BEGIN
	IF(ISNULL(@@NEW_UNION, '') <> '')
	BEGIN
		IF(NOT EXISTS(SELECT 'x' FROM TB_M_SUPPLIER_UNION WHERE UNION_NAME = LTRIM(RTRIM(@@NEW_UNION))))
		BEGIN
			INSERT INTO TB_M_SUPPLIER_UNION (UNION_NAME, CREATED_BY, CREATED_DT)
			SELECT @@NEW_UNION, @15, GETDATE()
		END 

		SELECT @@UNION = UNION_CD FROM TB_M_SUPPLIER_UNION WHERE UNION_NAME = @@NEW_UNION
	END
END

if exists(select * from TB_M_SUPPLIER_LABOR 
	where SUPPLIER_CD=@0 and ITEM_NO=@1)
BEGIN
	update TB_M_SUPPLIER_LABOR set 
	   [AVAILABILITY]=@2
           ,[SINGLE_PLURAL]=@3
           ,[UNION_CD] = @@UNION
           ,[HISTORY_STRIKE_3]=@5
		   ,[HISTORY_STRIKE_2]=@16
		   ,[HISTORY_STRIKE_1]=@17
           ,[COLLECTIVE_LABOR_AGREEMENT]=@6
           ,[COMMUNICATION_MEDIA]=@7
           ,[PENSION_PROGRAM]=@8
           ,[BONUS]=@9
		   ,[SALARY_INCREASE]=@10
           ,[MEAL_TRANSPORT_ALLOWANCE]=@11
           ,[MEDICAL_ALLOWANCE]=@12
           ,[THR]=@13
           ,[OVERTIME]=@14
		   ,CHANGED_BY=@15
           ,CHANGED_DT=getdate()
	where SUPPLIER_CD=@0 and ITEM_NO=@1
END
else
BEGIN

INSERT INTO TB_M_SUPPLIER_LABOR
       (	[SUPPLIER_CD]
           ,[AVAILABILITY]
           ,[SINGLE_PLURAL]
           ,[UNION_CD]
           ,[HISTORY_STRIKE_3]
           ,[COLLECTIVE_LABOR_AGREEMENT]
           ,[COMMUNICATION_MEDIA]
           ,[PENSION_PROGRAM]
           ,[BONUS]
		   ,[SALARY_INCREASE]
           ,[MEAL_TRANSPORT_ALLOWANCE]
           ,[MEDICAL_ALLOWANCE]
           ,[THR]
           ,[OVERTIME]
           ,[CREATED_BY]
           ,[CREATED_DT]
           ,[CHANGED_BY]
           ,[CHANGED_DT]
		   ,CONFIRM_FLAG
		   ,[HISTORY_STRIKE_2]
		   ,[HISTORY_STRIKE_1])
VALUES  ( @0 , -- SUPPLIER_CD 
          @2 , 
          @3 , 
          @@UNION , 
          @5 , 
          @6 ,
          @7 ,
		  @8,
		  @9,
		  @10,
		  @11,
		  @12,
		  @13,
		  @14,
		  @15,
		  getdate(),
		  @15,
		  getdate(),
		  0,
		  @16,
		  @17
        )

END