SELECT TOP 1 [ORDER_NO] OrderNo
      ,[DOCK_CD] DockCode
      ,[SUPPLIER_CD] SupplierCode
      ,[SUPPLIER_PLANT] SupplierPlant
	  ,ARRIVAL_PLAN_DT ArrivalTime
FROM [TB_R_DAILY_ORDER_MANIFEST]
where ORDER_NO=@0
           and SUPPLIER_CD=@1
           and DOCK_CD=@2
           and SUPPLIER_PLANT=@3