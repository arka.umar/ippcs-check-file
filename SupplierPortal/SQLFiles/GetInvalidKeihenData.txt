﻿SELECT
	PACK_MONTH as Production_Month,
	PART_NO as Part_Number_12_Digit,
	DOCK_CD as Dock_Code,
	SUPPLIER_CD as Supplier_Code,
	S_PLANT_CD as Supplier_Plant_Code,
	CAR_CD as CFC,
	N_1_QTY_SIGN as Quantity_Sign,
	N_1_QTY_GAP as Quantity,
	[ERROR_MESSAGE]
FROM TB_T_KEIHEN_UPLOAD
WHERE PROCESS_ID = @0