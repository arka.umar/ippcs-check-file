﻿--Updated by   : Septareo sagita
--Updated Date : 7 Maret 2014
 
DECLARE @@Query VARCHAR (MAX) 
SET @@Query = 
'SELECT 
	  ROW_NUMBER() OVER(ORDER BY MANIFEST_NO ASC) AS ID
	  ,MANIFEST_NO			AS ManifestNo
      ,ROUTE_CD				AS RouteCode
	  ,ROUTE_SEQ			AS RouteSeq
      ,PICKUP_DT			AS PickupDate
      ,LP_CD				AS LpCode
      ,TRUCK_TYPE			AS TruckType
      ,VOLUME_TARGET		AS VolTarget
      ,EFFICIENCY_TARGET	AS EffTarget
      ,UTILIZATION			AS Utilization
      ,PERCENTAGE			AS Percentage
      ,STATUS				AS Cstatus
  FROM TB_V_CAPACITY_PLAN ' + @0
EXEC ( @@Query ) 