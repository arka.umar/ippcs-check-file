SELECT
    ROW_NUMBER() OVER(ORDER BY M.USERNAME ASC, K.DIVISION_ID, K.DEPARTMENT_ID ASC) as no,
    M.USERNAME as username,
    M.FIRST_NAME as firstname,
    M.LAST_NAME as lastname,
    M.EMAIL as email,
    ( SELECT D.DIVISION_NAME
    FROM TB_M_DIVISION D
    WHERE D.DIVISION_ID = K.DIVISION_ID) as division,
    ( SELECT E.DEPARTMENT_NAME
    FROM TB_M_DEPARTMENT E
    WHERE E.DEPARTMENT_ID = K.DEPARTMENT_ID) as departement
    FROM TB_M_USER M, TB_M_POSITION K
    WHERE M.USERNAME = K.USERNAME
    AND (M.FIRST_NAME LIKE @0 OR M.LAST_NAME LIKE @1)
    ORDER BY M.USERNAME ASC, K.DIVISION_ID, K.DEPARTMENT_ID ASC