﻿DECLARE @@SupplierCode VARCHAR(20) = @0
        ,@@ProdMonth VARCHAR(20) = @1
        ,@@ProdMonthTo VARCHAR(20) = @2

if(@@ProdMonthTo=' 23:59:59') set 	@@ProdMonthTo=''		
SELECT 
	T.PROD_YEAR
	,T.SUPPLIER_CD
	,T.SUPPLIER_NAME
	,T.CO2_REDUCTION
	,T.CO2_EMISSION
	,T.CREATED_BY
	,T.CREATED_DT
	,T.CHANGED_BY
	,T.CHANGED_DT
FROM 
(
	SELECT DISTINCT
		SH.PROD_YEAR
		,SH.SUPPLIER_CD
		,S.SUPP_NAME AS SUPPLIER_NAME
		,SH.CO2_REDUCTION
		,SH.CO2_EMISSION	
		,SH.CREATED_BY
		,SH.CREATED_DT
		,SH.CHANGED_BY
		,SH.CHANGED_DT
	FROM TB_M_SUPPLIER_ENVIRONMENT SH
		LEFT JOIN dbo.TB_M_SUPPLIER_ICS S
			ON S.SUPP_CD = SH.SUPPLIER_CD
	
	WHERE CAST(SH.PROD_YEAR AS BIGINT) BETWEEN CAST(LEFT(ltrim(rtrim(@@ProdMonth)),4) AS BIGINT) AND CAST(LEFT(ltrim(rtrim(@@ProdMonthTo)),4) AS BIGINT)

		AND ((@@SupplierCode IS NULL or LEN(@@SupplierCode) < 1)
		OR SH.SUPPLIER_CD IN (SELECT item FROM dbo.fnSplit(@@SupplierCode, ';'))
		)

) AS T

ORDER BY CAST(T.PROD_YEAR AS BIGINT), T.SUPPLIER_CD, T.SUPPLIER_NAME
