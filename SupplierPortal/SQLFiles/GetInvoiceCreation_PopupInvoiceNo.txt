﻿-- GetInvoiceCreation_PopupInvoiceNo
DECLARE @@suppCode VARCHAR(MAX);
DECLARE @@dockCode VARCHAR(MAX);
DECLARE @@manifests VARCHAR(MAX);
DECLARE @@invoicenos VARCHAR(MAX); 
DECLARE @@orders VARCHAR(100);
DECLARE @@dFrom DATETIME; 
DECLARE @@dTo DATETIME; 
DECLARE @@status VARCHAR(MAX);
DECLARE @@showUploaded INT;

SET @@suppCode = @0;
SET @@dockCode = @1;
SET @@manifests = @2;
SET @@orders = @3;
SET @@dFrom = CONVERT(DATETIME, NULLIF(@4,''), 104);
SET @@dTo = CONVERT(DATETIME, NULLIF(@5,''), 104);
SET @@showUploaded = @6;
SET @@invoicenos = @7;

SELECT t.SUPP_INV_NO as InvoiceNo
		,s.STATUS_NAME as StatusName

FROM dbo.[TB_R_GR_IR] g
LEFT JOIN dbo.[TB_T_INV_UPLOAD] t 
	    ON t.SUPP_CD = g.SUPPLIER_CD 
	    AND t.PO_NO = g.PO_NO
	    AND t.PO_ITEM_NO = g.PO_ITEM_NO
	    AND t.MAT_DOC_NO = g.MAT_DOC_NO
	    AND t.COMP_PRICE_CD = g.COMP_PRICE_CD
	    AND t.MAT_DOC_ITEM = CONVERT(VARCHAR, g.ITEM_NO)
	    AND t.INV_REF_NO = G.INV_REF_NO
INNER JOIN TB_M_INV_STATUS s
	ON s.STATUS_CD = g.STATUS_CD
	
	WHERE ((@@invoicenos IS NULL OR LEN(@@invoicenos) < 1) 
		 OR  ISNULL(t.SUPP_INV_NO, '') IN (SELECT ISNULL(item,'') FROM dbo.fnSplit(@@invoicenos, ';'))
		 )

	AND ((@@suppCode  IS NULL OR LEN(@@suppCode) < 1)
		  OR g.SUPPLIER_CD IN 
			(SELECT item FROM dbo.fnSplit(@@suppCode, ';'))
		  )

	AND ((@@dockCode IS NULL or LEN(@@dockCode) < 1)
		 OR g.DOCK_CD IN 
			(SELECT item FROM dbo.fnSplit(@@dockCode, ';'))
		)

	AND ((@@manifests IS NULL OR LEN(@@manifests) < 1) 
		 OR  g.INV_REF_NO in (SELECT item FROM dbo.fnSplit(@@manifests, ';'))
		 )

	AND ((@@orders IS NULL OR LEN(@@orders) < 1)
		OR g.KANBAN_ORDER_NO IN (SELECT item FROM dbo.fnSplit(@@orders, ';'))
		)

	AND	(((@@dFrom IS NULL)  OR  (@@dTo IS NULL))
        OR (g.DOC_DT BETWEEN @@dFrom AND @@dTo) 
		)

	AND (g.STATUS_CD in (-3, -1, 1)			
		)

	AND (ISNULL(@@showUploaded, 0) = 0 
		  OR (@@showUploaded = 1 AND t.SUPP_INV_NO IS NOT NULL))
		 AND g.LIV_SEND_FLAG = 'N'
	AND g.CANCEL_FLAG = 0

	AND g.PO_NO not like (SELECT CONFIG_VALUE + '%' FROM TB_M_SPOT_CONFIG_SYSTEM WHERE CONFIG_NAME = 'PO_PREFIX_COD')

	GROUP BY s.STATUS_NAME, t.SUPP_INV_NO
	ORDER BY 
		t.SUPP_INV_NO
		,s.STATUS_NAME

