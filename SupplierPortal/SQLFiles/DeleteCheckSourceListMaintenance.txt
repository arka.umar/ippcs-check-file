﻿with a
as (
	select 
	 MatNo          = @0
	,SourceType   	= @1
	,ProdPurpose  	= @2
	,ColorSuffix   	= @3
	,SuppCD      	= @4
	,ValidFrom   	= dbo.tryconvertdate(@5) 
	,DeletionFlag	= @6	
)
, b as (
select count(distinct (l.MAT_NO+l.SOURCE_TYPE+l.PROD_PURPOSE_CD+l.PART_COLOR_SFX+l.SUPP_CD+convert(varchar(8),l.valid_dt_fr,112)))  source_Lists
	, count(distinct(i.po_no+'_'+i.po_item_no)) po_iTEMS	
from a 
left join TB_M_SOURCE_LIST l 
	on l.MAT_NO = a.MatNo
	and l.SOURCE_TYPE = a.SourceType
	and l.PROD_PURPOSE_CD = a.ProdPurpose
	and l.PART_COLOR_SFX = a.ColorSuffix
	and l.SUPP_CD = a.SuppCD
	and l.VALID_DT_FR = a.ValidFrom 
left join (
	SELECT i.po_no, i.po_item_no, i.MAT_NO, i.SOURCE_TYPE, i.PROD_PURPOSE_CD, i.PART_COLOR_SFX, h.SUPPLIER_CD, i.DELIVERY_DT
	FROM tb_r_po_item i  
	JOIN TB_R_PO_H H ON h.PO_NO=i.PO_NO
) i on i.MAT_NO = l.MAT_NO
	and i.SOURCE_TYPE = l.SOURCE_TYPE 
	and i.PROD_PURPOSE_CD = l.PROD_PURPOSE_CD
	and i.PART_COLOR_SFX = l.PART_COLOR_SFX
	and i.SUPPLIER_CD = l.SUPP_CD
) 
select REF=case when b.source_Lists < 1 then -1 else b.po_iTEMS end 
 from b;