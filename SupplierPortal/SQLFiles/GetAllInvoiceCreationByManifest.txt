-- GetAllInvoiceCreationByManifest 

DECLARE @@manifests VARCHAR(MAX);

SET @@manifests = @0;
select 
	i.ICS_MAT_NO [MaterialNumber]
	
	, i.PACKING_TYPE [PackingType]
	, i.PART_COLOR_SFX [SuffixColour]
	, i.INV_REF_NO AS [SupplierManifest]
	, i.MAT_DESC as [MaterialDescription]
	, i.DOC_DT as [DocumentDate]
	, i.DOCK_CD as [DockCode]
	
	, i.MOVEMENT_QTY [ICSQty]
	, i.PO_DETAIL_PRICE [PartPrice]
	, 0 as [Curr]
	, i.ITEM_CURR AS [Currency]
	, i.GR_IR_AMT [Amount]
	
FROM [dbo].[TB_R_GR_IR] i
WHERE i.INV_REF_NO IN (SELECT item FROM dbo.fnSplit(@@manifests, ';'))
  AND (i.STATUS_CD in (-3, -1, 1))
  AND i.LIV_SEND_FLAG = 'N'
  AND i.CANCEL_FLAG = 0
