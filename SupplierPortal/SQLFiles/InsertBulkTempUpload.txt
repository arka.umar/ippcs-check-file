﻿/* Insert bulk excel file into temp upload table (fid.salman id.80218) 11-apr-2013 */

DECLARE 
	@@tempTable AS VARCHAR(MAX) = @0,
	@@filePath AS VARCHAR(MAX) = @1,
	@@sheetName AS VARCHAR(MAX) = @2,

	@@sqlQuery AS VARCHAR(MAX)

SET @@sqlQuery = 'INSERT INTO ' + @@tempTable + ' ' +
                 'SELECT * FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'',
				 ''Excel 8,0;Database=' + @@filePath + ''',''SELECT * FROM [' + @@sheetName + '$]'')'

EXECUTE (@@sqlQuery)