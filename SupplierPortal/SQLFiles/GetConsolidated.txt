declare @@sql nvarchar(max), 
		@@Year varchar(10) = @0,
		@@Versions varchar(10) = @1,
		@@Sources varchar(50)=@2
		
set @@sql = '
			select 
				Production_Year ProductionYear,
				Version,
				Source,
	            Created_Date UploadDate
			from TB_T_PULL_FROM_APPS_H 
			where 1=1 '
			
if(@@Year <> '')
	set @@sql = @@sql + 'and Production_Year = ''' + @@Year + ''' '

if(@@Versions <> '')
	set @@sql = @@sql + 'and Version = '''+ @@Versions + ''' '

if(@@Sources <> '')
	set @@sql = @@sql + 'and Source = ''' + @@Sources + ''' '

execute (@@sql)

