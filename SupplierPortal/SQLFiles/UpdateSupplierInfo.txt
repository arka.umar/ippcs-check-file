﻿UPDATE TB_M_SUPPLIER_INFO
        SET 
          SP_INFO1 = @4 ,
          SP_INFO2 = @5,
          SP_INFO3 = @6,
          PRINT_CD = @7
        WHERE 
        SUPPLIER_CD = @0
          AND SUPPLIER_PLANT_CD = @1 
          AND PART_NO = @2
          AND DOCK_CODE = @3