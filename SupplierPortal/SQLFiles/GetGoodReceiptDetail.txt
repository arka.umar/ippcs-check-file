﻿SELECT No=ROW_NUMBER() OVER(ORDER BY tb.PART_NO ASC)
	  ,tb.[MANIFEST_NO]
      ,tb.[PART_NO]
      ,tb.PART_NAME
      ,tb.[ORDER_QTY]
	  ,tb.[RECEIVED_QTY]
      ,RemainingQty=tb.[ORDER_QTY]-tb.[RECEIVED_QTY]
      ,tb.[RECEIVED_STATUS]
      ,tb.[SHORTAGE_QTY]
      ,tb.[MISSPART_QTY]
      ,tb.[DAMAGE_QTY]
      ,SupplierMaster=''
      ,case ISNULL(tb.[MASTER_SUPPLIER_EROR_FLAG], 0) when '0' then '' else 'X' end SupplierMaster
      ,case ISNULL(tb.[MATERIAL_MASTER_ERROR_FLAG], 0) when '0' then '' else 'X' end MaterialMaster
      ,case ISNULL(tb.[BUYING_PRICE_ERROR_FLAG], 0) when '0' then '' else 'X' end PriceMaster
      ,StandardPrice=''
      ,case ISNULL(tb.[POSTING_PERIOD_ERROR], 0) when '0' then '' else 'X' end PostingPeriod
      ,ExtWarehouse=''
      ,NoPurchaseOrder=''
      ,case ISNULL(tb.[PROCESS_ERROR], 0) when '0' then '' else 'X' end ProcessLocked
      ,Notice=''
      ,Status=''
FROM TB_R_DAILY_ORDER_PART tb
where tb.MANIFEST_NO=@0