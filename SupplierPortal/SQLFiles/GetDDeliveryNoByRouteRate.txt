﻿declare @@sql nvarchar(max), 
		@@route char(4) = @0,
		@@rate char(2) = @1,
		@@pickupDate varchar(10) = @2

set @@sql = 'SELECT DELIVERY_NO AS DeliveryNo, LP_CD AS LPCode
				 FROM TB_R_DELIVERY_CTL_H
				 WHERE ROUTE = ''' + @@route + ''' AND RATE = ' + @@rate + ' AND PICKUP_DT = ''' + @@pickupDate + ''' '

execute (@@sql)