select
	MESSAGE_ID as Id,
	GROUP_ID as GroupId,
	BOARD_NAME as BoardName,
	SENDER as Sender,
	RECIPIENT as Recipient,
	TEXT as Text,
	SENT_DATE as Date,
	CC as CarbonCopy
 from tb_r_message_board
 where board_name = @0