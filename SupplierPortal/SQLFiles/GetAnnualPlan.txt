declare @@sql nvarchar(max), 
		@@Year varchar(10) = @0,
		@@Versions varchar(10) = @1,
		@@Sources varchar(50)=@2
		
set @@sql = '
			select 
				AP_YEAR ProductionYear,
				VERSION_CD,
				AP_OF,
	            Created_Date UploadDate
			from TB_T_PULL_FROM_APPS_H 
			where 1=1 '
			
if(@@Year <> '')
	set @@sql = @@sql + 'and Production_Year = ''' + @@Year + ''' '

if(@@Versions <> '')
	set @@sql = @@sql + 'and VERSION_CD = '''+ @@Versions + ''' '

if(@@Sources <> '')
	set @@sql = @@sql + 'and AP_OF = ''' + @@Sources + ''' '

execute (@@sql)

