﻿select
        Function_ID functionID,
		100 ProcessID,
		Table_From_Temp tableFromTemp,
        Table_To_Access tableToAccess,
        File_Path filePath,
		Sheet_Name sheetName,
		Row_Start rowStart,
        Column_Start columnStart,
        Column_End columnEnd      
    from TB_M_UPLOAD_DESTINATION
    where Function_ID = @0
