﻿-- =================================================
-- Author: niit.arie
-- Create date: 09 Nov 2013
-- Update date: 09 Nov 2013
-- Description: Query Insert Data in Supplier Performace
-- =================================================



DECLARE @@TabActive INT = @0
	,@@IntReturn INT = 1

	,@@ProductionMonth DATE = @1
	,@@SupplierCode VARCHAR(12) = @2
	--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
	,@@PartNo VARCHAR(15) = @3
	,@@QtyDefect FLOAT = @4
	,@@Problem NVARCHAR(MAX) = @5
	,@@ReceivingArea VARCHAR(100) = @6
	,@@CreatedBy VARCHAR(100) = @7

	--delivery
	,@@SupplierMonth FLOAT = @8
	,@@Delay VARCHAR(MAX) =@9
	,@@Shortage FLOAT = @10
	,@@Incorect FLOAT = @11

	--SPD
	,@@Ori FLOAT = @12
	,@@Adj FLOAT = @13
	,@@Receive FLOAT = @14
	,@@Quality FLOAT = @15
	,@@Workability FLOAT = @16

	--TWS
	,@@Claim FLOAT = @17
	,@@Cost FLOAT = @18
	,@@TriY FLOAT = @19

	--safety
	,@@Fatal FLOAT = @20
	,@@Disabilty FLOAT = @21
	,@@LostOrgan FLOAT = @22
	,@@Absent FLOAT = @23
	,@@AbsentPeriode FLOAT = @24
	,@@SmallInjured FLOAT = @25
	,@@TotalWorkers FLOAT = @26
	--modif by alira.agi 2015-04-15
	--,@@Judgment VARCHAR(1) = @27
	,@@Judgment VARCHAR(20) = @27
	--part receive
	,@@QtyReceive FLOAT = @28

	,@@DProblem VARCHAR(50) = @29
	,@@QProblem VARCHAR(50) = @30
	,@@WProblem VARCHAR(50) = @31

	--service part quality replace all ng to int
	--modif by alira.agi 2015-05-26
	,@@SPQtyReceive FLOAT = @32
	--,@@NGQuality VARCHAR(100) = @33
	,@@NGQuality int = @33
	,@@DescQuality VARCHAR(100) = @34
	,@@Time1 VARCHAR(20) = @35
	--,@@NGWorkAbility VARCHAR(100) = @36
	,@@NGWorkAbility int = @36
	,@@DescWorkAbility VARCHAR(100) = @37
	,@@Time2 VARCHAR(20) = @38
	----add by alira.agi 2015-05-26 add column for Delivery
	,@@NGDelivery int = @39
	,@@DescDelivery VARCHAR(100) = @40
	,@@TIME3 VARCHAR(20) = @41

	,@@partName VARCHAR(50) = @42
	,@@ShortageQty int = @43 --shortage tab
	,@@ModelCode VARCHAR(50) = @44

	,@@MisspartQty VARCHAR(5) = @45
	,@@MinLineStop VARCHAR(5) = @46
	,@@cripple VARCHAR(5) = @47
	,@@delayFreq VARCHAR(5) = @48
	,@@supplyPerMonth VARCHAR(5) = @49

--check supplier code is not exist
IF @@SupplierCode <> ''
BEGIN
	IF NOT EXISTS (SELECT 1 FROM TB_M_SUPPLIER_ICS WHERE SUPP_CD = @@SupplierCode)
	BEGIN 
		SET @@IntReturn = 11
	END
END

--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
--check part number is not exist
--IF @@IntReturn = 1
--BEGIN
--	IF @@PartNo <> ''
--	BEGIN
--		IF NOT EXISTS (SELECT 1 FROM TB_M_PART_INFO WHERE PART_NO = @@PartNo 
--					  AND SUPPLIER_CD = @@SupplierCode AND DOCK_CD = @@ReceivingArea)
--		BEGIN
--			SET @@IntReturn = 22
--		END
--	END
--END

--check dock code is not exist
--IF @@IntReturn = 1
--BEGIN
--	IF @@ReceivingArea <> ''
--	BEGIN
--		IF NOT EXISTS (SELECT 1 FROM TB_M_DOCK WHERE DOCK_CD = @@ReceivingArea)
--		BEGIN
--			SET @@IntReturn = 33
--		END
--	END
--END

IF @@IntReturn = 1
BEGIN

	IF (@@TabActive = 1)
	BEGIN
		--delivery
		--add by alira.agi 2015-04-16 cek receivingarea/DOCK_CD
				IF  EXISTS (SELECT 1 FROM TB_M_DOCK WHERE DOCK_CD = @@ReceivingArea)
				begin
					INSERT INTO TB_R_SUPPLIER_DELIVERY
						(PROD_MONTH
						,SUPPLIER_CD
						,QTY_PART_SUPPLY_MONTH
						,QTY_DELAY
						,RECEIVING_AREA
						--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
						,PART_NO
						,QTY_INCORRECT
						,QTY_SHORTAGE
						,PROBLEM_DESC
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@SupplierMonth
						,@@Delay
						,@@ReceivingArea
						--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
						,@@PartNo
						,@@Incorect
						,@@Shortage
						,@@Problem
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());			

					-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) =  LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update DELIVERY COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
						SET DELIVERY = 1
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) =  LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS 
						set COMPLETE_FLAG = 1
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) =  LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1 
						--modif by alira.agi 2015-05-27

						and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 
						

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							0, -- quality
							1, --delivery
							0,--safety
							0, --service_part
							0, --warranty_claim
							0, --part_received
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL,
							0, --SERVICE_PART_QUALITY
							0, --SHORTAGE
							0, --MISPART
							0, --ONTIME
							0, --LINESTOP
							0, --CRIPPLE
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				end
				else
				begin
						select 33
				end
					SELECT @@TabActive;		
	END
	IF (@@TabActive = 2)
	BEGIN
		--SELECT 'Safety'		
		--SET JUDGEMENT
		declare @@prodMonth VARCHAR(7) = '';

		SET @@prodMonth = REPLACE(LEFT(CAST(@@ProductionMonth AS VARCHAR), 7), '-', '')


		IF(
				(SELECT 'TRUE' WHERE DATEADD(DAY, -1 , (DATEADD(MONTH, 2, CAST(@@prodMonth + '01' AS DATE))))
				< GETDATE())
				= 'TRUE')
			BEGIN
				SELECT @@Judgment = 'Not Submit'
			END
			ELSE IF((SELECT 'TRUE' WHERE CONVERT(date, @@prodMonth + '10', 112) > GETDATE())
				= 'TRUE')
			BEGIN
				SELECT @@Judgment = 'OnTime'
			END
			ELSE IF(
				(SELECT 'TRUE' WHERE GETDATE()
					BETWEEN CONVERT(date, @@prodMonth + '01', 112)
					AND DATEADD(MONTH, 1, CAST(@@prodMonth + '11' AS DATE)))
					= 'TRUE'
				)
			BEGIN
				SELECT @@Judgment = 'OnTime'
			END
			ELSE
			BEGIN
				SELECT @@Judgment = 'Delay'
			END



		INSERT INTO TB_R_SUPPLIER_SAFETY_H
			(PROD_MONTH
			,SUPPLIER_CD
			,QTY_FATAL
			,QTY_DISABILITY
			,QTY_LOST_ORGAN
			,QTY_ABSENT
			,QTY_ABSENT_PERIOD
			,QTY_SMALL_INJURED
			,QTY_TOTAL_WORKERS_MH
			,JUDGEMENT
			,CREATED_BY
			,CREATED_DT
			,CHANGED_BY
			,CHANGED_DT)
		VALUES
			(@@ProductionMonth
			,@@SupplierCode
			,@@Fatal
			,@@Disabilty
			,@@LostOrgan
			,@@Absent
			,@@AbsentPeriode
			,@@SmallInjured
			,@@TotalWorkers
			,@@Judgment
			,@@CreatedBy
			,GETDATE()
			,@@CreatedBy
			,GETDATE());
		
		-- startregion update complete flag performance status
		if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
				begin

				-- Update SAFETY COLUMN
				UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
				SET [SAFETY] = 1
				WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

				--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
				update TB_R_SUPPLIER_PERFORMANCE_STATUS 
				set COMPLETE_FLAG = 1
				where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
				and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1 
				--modif by alira.agi 2015-05-27
				and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

				--and PART_RECEIVED=1

				end
			else
				begin

				
				-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
				insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
				values 
				(
					(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
					@@SupplierCode,
					0, --release_flag
					0, --edit_flag
					0, --complete_flag
					0, -- quality
					0, --delivery
					1,--safety
					0, --service_part
					0, --warranty_claim
					0, --part_received
					'SYSTEM',
					GETDATE(),
					NULL,
					NULL,
					--add by alira.agi 2015-05-27
					0, --SERVICE_PART_QUALITY
					0, --SHORTAGE
					0, --MISPART
					0, --ONTIME
					0, --LINESTOP
					0, --CRIPPLE
					NULL,
					NULL
				)

				end
		-- endregion update complete flag performance status
			SELECT @@TabActive;
	END

	IF (@@TabActive = 3)
	BEGIN
		--SELECT 'Service Part'
		INSERT INTO TB_R_SUPPLIER_SPD
			(PROD_MONTH
			,SUPPLIER_CD
			--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
			--open remarks by alira.agi 2015-06-01
			--,PART_NO
			,PART_NO
			,QTY_ORI
			,QTY_ADJ
			,QTY_RECEIVE
			,QTY_NG_QUALITY
			,QTY_NG_WORKABILITY
			,DELIVERY_PROBLEM
			,QUALITY_PROBLEM
			,WORKABIILITY_PROBLEM
			,CREATED_BY
			,CREATED_DT
			,CHANGED_BY
			,CHANGED_DT)
		VALUES
			(@@ProductionMonth
			,@@SupplierCode
			--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
			--,@@PartNo
			--open remark part no to '' for inline edit
			,''--isnull(@@PART_NO,'')
			,@@Ori
			,@@Adj
			,@@Receive
			,@@Quality
			,@@Workability
			,@@DProblem
			,@@QProblem
			,@@WProblem
			,@@CreatedBy
			,GETDATE()			
			,@@CreatedBy
			,GETDATE());

		-- startregion update complete flag performance status
		if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
				begin

				-- Update SERVICE PART COLUMN
				UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
				SET SERVICE_PART = 1
				WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

				--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
				update TB_R_SUPPLIER_PERFORMANCE_STATUS 
				set COMPLETE_FLAG = 1
				where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
				and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1 
				--modif by alira.agi 2015-05-27

				and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

				--and PART_RECEIVED=1

				end
			else
				begin
				-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
				insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
				values 
				(
					(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
					@@SupplierCode,
					0, --release_flag
					0, --edit_flag
					0, --complete_flag
					0, -- quality
					0, --delivery
					0,--safety
					1, --service_part
					0, --warranty_claim
					0, --part_received
					'SYSTEM',
					GETDATE(),
					NULL,
					NULL,
					--add by alira.agi 2015-05-27
					0, --SERVICE_PART_QUALITY
					0, --SHORTAGE
					0, --MISPART
					0, --ONTIME
					0, --LINESTOP
					0, --CRIPPLE
					NULL,
					NULL

				)

				end

		-- endregion update complete flag performance status

		SELECT @@TabActive;
	END

	IF (@@TabActive = 4)
	--begin try 
	BEGIN
		--SELECT 'TWC'
		INSERT INTO TB_R_SUPPLIER_TWC
			(PROD_MONTH
			,SUPPLIER_CD
			,QTY_CLAIM
			,COST_SHARE
			,QTY_3Y
			,CREATED_BY
			,CREATED_DT
			,CHANGED_BY
			,CHANGED_DT)
		 VALUES
			(@@ProductionMonth
			,@@SupplierCode
			,cast(@@Claim as decimal(10,3))
			,cast (@@Cost as decimal(4,2))
			,cast (@@TriY as bigint)
			,@@CreatedBy
			,GETDATE()			
			,@@CreatedBy
			,GETDATE());

		-- startregion update complete flag performance status
		if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
				begin

				-- Update WARRANTY CLAIM COLUMN
				UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
				SET WARRANTY_CLAIM = 1
				WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  
				and SUPPLIER_CD=@@SupplierCode

				--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
				update TB_R_SUPPLIER_PERFORMANCE_STATUS 
				set COMPLETE_FLAG = 1
				where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
				and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1 
				and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 
				--and PART_RECEIVED=1

				end
			else
				begin
				-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
				insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
				values 
				(
					(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
					@@SupplierCode,
					0, --release_flag
					0, --edit_flag
					0, --complete_flag
					0, -- quality
					0, --delivery
					0,--safety
					0, --service_part
					1, --warranty_claim
					0, --part_received
					'SYSTEM',
					GETDATE(),
					NULL,
					NULL,
					0, --SERVICE_PART_QUALITY
					0, --SHORTAGE
					0, --MISPART
					0, --ONTIME
					0, --LINESTOP
					0, --CRIPPLE
					NULL,
					NULL
				)

				end
		-- endregion update complete flag performance status
		SELECT @@TabActive;
	END
	--end try
	--begin catch
	--		select ERROR_MESSAGE()+',' + cast(error_line() as varchar(100))
	--end catch

	IF (@@TabActive = 5)
	BEGIN
		INSERT INTO TB_R_SUPPLIER_QUALITY_RECEIVE
				(PROD_MONTH
				,SUPPLIER_CD
				--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
				--,PART_NO
				,QTY_RECEIVE
				,RECEIVING_AREA
				,CREATED_BY
				,CREATED_DT
				,CHANGED_BY
				,CHANGED_DT)
			VALUES
				(@@ProductionMonth
				,@@SupplierCode
				--commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
				--,@@PartNo
				,@@QtyReceive
				,@@ReceivingArea
				,@@CreatedBy
				,GETDATE()
				,@@CreatedBy
				,GETDATE());

		-- startregion update complete flag performance status
		if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
				begin

				-- Update PART RECEIVED COLUMN
				UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
				SET PART_RECEIVED = 1
				WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

				--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
				update TB_R_SUPPLIER_PERFORMANCE_STATUS 
				set COMPLETE_FLAG = 1
				where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
				and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1
				--modif by alira.agi 2015-05-27

				and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 
				--and PART_RECEIVED=1

				end
			else
				begin
				-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
				insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
				values 
				(
					(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
					@@SupplierCode,
					0, --release_flag
					0, --edit_flag
					0, --complete_flag
					0, -- quality
					0, --delivery
					0,--safety
					0, --service_part
					0, --warranty_claim
					1, --part_received
					'SYSTEM',
					GETDATE(),
					NULL,
					NULL,
					--add by alira.agi 2015-05-27
					0, --SERVICE_PART_QUALITY
					0, --SHORTAGE
					0, --MISPART
					0, --ONTIME
					0, --LINESTOP
					0, --CRIPPLE
					NULL,
					NULL
				)

				end
		-- endregion update complete flag performance status
		SELECT @@TabActive;
	END

	--------------------------------------
	IF (@@TabActive = 6)
	BEGIN
		--if exists (select '' from TB_M_PART_INFO where PART_NO = LTRIM(RTRIM(@@PartNo))) 
		--begin

			INSERT INTO TB_R_SUPPLIER_SP_QUALITY
					(PROD_MONTH
					,SUPPLIER_CD
					,PART_NO
					,QTY_RECEIVE
					,NG_QUALITY
					,DESC_QUALITY
					,TIME1
					,NG_WORKABILITY
					,DESC_WORKABILITY
					,TIME2
					,CREATED_BY
					,CREATED_DT
					,CHANGED_BY
					,CHANGED_DT
					--add by alira.agi 2015-05-26
					,NG_DELIVERY
					,DESC_DELIVERY
					,TIME3
					)
				VALUES
					(@@ProductionMonth
					,@@SupplierCode
					,@@PartNo
					,@@SPQtyReceive
					,@@NGQuality
					,@@DescQuality
					,@@Time1
					,@@NGWorkAbility
					,@@DescWorkAbility
					,@@Time2
					,@@CreatedBy
					,GETDATE()
					,@@CreatedBy
					,GETDATE()
					--add by alira.agi 2015-05-26
					,@@NGDelivery
					,@@DescDelivery
					,@@TIME3);
	
			-- startregion update complete flag performance status
				if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
					begin

					-- Update QUALITY COLUMN
					UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
					--modif by alira.agi 2015-05-27
					--SET QUALITY = 1
					SET SERVICE_PART_QUALITY = 1
					WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

					--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
					update TB_R_SUPPLIER_PERFORMANCE_STATUS 
					set COMPLETE_FLAG = 1
					where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
					and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1
					--modif by alira.agi 2015-05-27
					and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 
					 --and PART_RECEIVED=1

					end
				else
					begin
					-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
					insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
					values 
					(
						(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
						@@SupplierCode,
						0, --release_flag
						0, --edit_flag
						0, --complete_flag
						0, -- quality
						0, --delivery
						0,--safety
						0, --service_part
						0, --warranty_claim
						0, --part_received
						'SYSTEM',
						GETDATE(),
						NULL,
						NULL,
						--add by alira.agi 2015-05-27
						1, --SERVICE_PART_QUALITY
						0, --SHORTAGE
						0, --MISPART
						0, --ONTIME
						0, --LINESTOP
						0, --CRIPPLE
						NULL,
						NULL
					)

					end
				-- endregion update complete flag performance status
		--end
		--else if not exists (select '' from TB_M_PART_INFO where PART_NO = LTRIM(RTRIM(@@PartNo))) 
		--begin
		--	SELECT 22		
		--end		
		SELECT @@TabActive;
	END

	-----------------------------------------------------------------------------------------
	IF(@@TabActive = 7) --shortage grid --tambahan
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_SHORTAGE
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,RCV_AREA
						,SHORTAGE_QTY
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@ReceivingArea
						,@@ShortageQty
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update QUALITY COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
						SET SHORTAGE = 1
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS 
						set COMPLETE_FLAG = 1
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1			
						--modif by alira.agi 2015-05-27

						and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

						--and PART_RECEIVED=1

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							0, -- quality
							0, --delivery
							0,--safety
							0, --service_part
							0, --warranty_claim
							0, --part_received
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL,
							--add by alira.agi 2015-05-27
							0, --SERVICE_PART_QUALITY
							1, --SHORTAGE
							0, --MISPART
							0, --ONTIME
							0, --LINESTOP
							0, --CRIPPLE
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status

				SELECT @@TabActive;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	


	END
	-----------------------------------------------------------------------------------------
	IF(@@TabActive = 8) --misspart grid --tambahan
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_MISSPART
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,RCV_AREA
						,MISSPART_QTY
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@ReceivingArea
						,@@MisspartQty
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update MISSPART COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
						SET MISPART = 1
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS 
						set COMPLETE_FLAG = 1
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1			
						--modif by alira.agi 2015-05-27

						and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

						--and PART_RECEIVED=1

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							0, -- quality
							0, --delivery
							0,--safety
							0, --service_part
							0, --warranty_claim
							0, --part_received
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL,
							--add by alira.agi 2015-05-27
							0, --SERVICE_PART_QUALITY
							0, --SHORTAGE
							1, --MISPART
							0, --ONTIME
							0, --LINESTOP
							0, --CRIPPLE
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				SELECT @@TabActive;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	


	END
	--------------------------------------




	-----------------------------------------------------------------------------------------
	IF(@@TabActive = 10) --LINESTOP grid --tambahan
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_LINESTOP
						(PROD_MONTH
						,SUPPLIER_CD
						,RCV_AREA
						,MIN_LINE_STOP
						,PROBLEM
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@ReceivingArea
						,@@MinLineStop
						,@@Problem
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update MISSPART COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
						SET LINESTOP = 1
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS 
						set COMPLETE_FLAG = 1
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1			
						--modif by alira.agi 2015-05-27

						and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							0, -- quality
							0, --delivery
							0,--safety
							0, --service_part
							0, --warranty_claim
							0, --part_received
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL,
							--add by alira.agi 2015-05-27
							0, --SERVICE_PART_QUALITY
							0, --SHORTAGE
							0, --MISPART
							0, --ONTIME
							1, --LINESTOP
							0, --CRIPPLE
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				SELECT @@TabActive;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	


	END
	--------------------------------------


	-----------------------------------------------------------------------------------------
	IF(@@TabActive = 9) --ontime grid --tambahan
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_ONTIME
						(PROD_MONTH
						,SUPPLIER_CD
						,RCV_AREA
						,DELAY_FREQ
						,SUPPLY_PER_MONTH
						,PROBLEM
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@ReceivingArea
						,@@delayFreq
						,@@supplyPerMonth
						,@@Problem
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update MISSPART COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
						SET ONTIME = 1
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS 
						set COMPLETE_FLAG = 1
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1			
						--modif by alira.agi 2015-05-27

						and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							0, -- quality
							0, --delivery
							0,--safety
							0, --service_part
							0, --warranty_claim
							0, --part_received
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL,
							--add by alira.agi 2015-05-27
							0, --SERVICE_PART_QUALITY
							0, --SHORTAGE
							0, --MISPART
							1	 , --ONTIME
							0, --LINESTOP
							0, --CRIPPLE
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				SELECT 12;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	


	END
	--------------------------------------



	-----------------------------------------------------------------------------------------
	IF(@@TabActive = 11) --CRIPPLE grid --tambahan
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_CRIPPLE
						(PROD_MONTH
						,SUPPLIER_CD
						,RCV_AREA
						,UNIT_CRIPPLE
						,PROBLEM
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@ReceivingArea
						,@@cripple
						,@@Problem
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update MISSPART COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
						SET CRIPPLE = 1
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS 
						set COMPLETE_FLAG = 1
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1			
						--modif by alira.agi 2015-05-27

						and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							0, -- quality
							0, --delivery
							0,--safety
							0, --service_part
							0, --warranty_claim
							0, --part_received
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL,
							--add by alira.agi 2015-05-27
							0, --SERVICE_PART_QUALITY
							0, --SHORTAGE
							0, --MISPART
							0, --ONTIME
							0, --LINESTOP
							1, --CRIPPLE
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				SELECT 12;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	


	END
	--------------------------------------



	-----------------------------------------------------------------------------------------
	IF(@@TabActive = 12) --comment grid --tambahan
	BEGIN
		
				INSERT INTO TB_R_SUPPLIER_TMMIN_COMMENT
						(PROD_MONTH
						,SUPPLIER_CD
						,COMMENT
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@Problem
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE());
	
				SELECT 12;
		

	END
	--------------------------------------




	ELSE --tabindex 0
	BEGIN
		--modif by alira.agi 2015-04-15
		--add validate part no
		--if exists (select '' from TB_M_PART_INFO where PART_NO = LTRIM(RTRIM(@@PartNo))) 
		   --and 
		
		--check duplicate
		IF(EXISTS(SELECT 1 FROM TB_R_SUPPLIER_QUALITY WHERE 
				  PROD_MONTH = @@ProductionMonth AND SUPPLIER_CD = @@SupplierCode 
				  AND PART_NO = @@PartNo AND PROBLEM_DESC = RTRIM(LTRIM(@@Problem))
				  AND RECEIVING_AREA = @@ReceivingArea
				  )
			)
		BEGIN
			RAISERROR('duplicate', 16,1);
		END

		
		IF EXISTS (SELECT 1 FROM TB_M_RECEIVING_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin
				INSERT INTO TB_R_SUPPLIER_QUALITY
						(PROD_MONTH
						,SUPPLIER_CD
						,PART_NO
						,PART_NAME
						,QTY_DEFECT
						,PROBLEM_DESC
						,RECEIVING_AREA
						,CREATED_BY
						,CREATED_DT
						,CHANGED_BY
						,CHANGED_DT
						,MODEL_CD)
					VALUES
						(@@ProductionMonth
						,@@SupplierCode
						,@@PartNo
						,@@partName
						,@@QtyDefect
						,@@Problem
						,@@ReceivingArea
						,@@CreatedBy
						,GETDATE()
						,@@CreatedBy
						,GETDATE()
						,@@ModelCode);
	
				-- startregion update complete flag performance status
					if exists ( select '' from TB_R_SUPPLIER_PERFORMANCE_STATUS where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode)
						begin

						-- Update QUALITY COLUMN
						UPDATE TB_R_SUPPLIER_PERFORMANCE_STATUS
						SET QUALITY = 1
						WHERE LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode

						--IF ALREADY COMPLETED, THEN UPDATE AS COMPLETED
						update TB_R_SUPPLIER_PERFORMANCE_STATUS 
						set COMPLETE_FLAG = 1
						where LEFT(CONVERT(VARCHAR, PRODUCTION_MONTH, 112),6) = LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6)  and SUPPLIER_CD=@@SupplierCode
						and QUALITY = 1 and DELIVERY = 1 and [SAFETY] = 1 and SERVICE_PART=1 and WARRANTY_CLAIM =1 AND SERVICE_PART_QUALITY = 1			
						--modif by alira.agi 2015-05-27

						and SHORTAGE = 1 AND MISPART = 1 AND ONTIME = 1 AND LINESTOP = 1 AND CRIPPLE = 1 

						--and PART_RECEIVED=1

						end
					else
						begin
						-- Insert to TB_R_SUPPLIER_PERFORMANCE_STATUS
						insert into TB_R_SUPPLIER_PERFORMANCE_STATUS 
						values 
						(
							(select CONVERT(DATETIME, CAST(LEFT(CONVERT(VARCHAR, @@ProductionMonth, 112),6) AS VARCHAR) + '01', 112)  ),
							@@SupplierCode,
							0, --release_flag
							0, --edit_flag
							0, --complete_flag
							1, -- quality
							0, --delivery
							0,--safety
							0, --service_part
							0, --warranty_claim
							0, --part_received
							'SYSTEM',
							GETDATE(),
							NULL,
							NULL,
							--add by alira.agi 2015-05-27
							0, --SERVICE_PART_QUALITY
							0, --SHORTAGE
							0, --MISPART
							0, --ONTIME
							0, --LINESTOP
							0, --CRIPPLE
							NULL,
							NULL
						)

						end
					-- endregion update complete flag performance status
				SELECT @@TabActive;
		end
		else if not EXISTS (SELECT 1 FROM RCV_AREA WHERE RCV_AREA = @@ReceivingArea)
		begin 
			select 33
		end	

	END

END 
ELSE
BEGIN
	SELECT @@IntReturn
END