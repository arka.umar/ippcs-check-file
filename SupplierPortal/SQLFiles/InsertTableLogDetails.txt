﻿insert into TB_R_LOG_D(PROCESS_ID, 
					   SEQUENCE_NUMBER, 
					   MESSAGE_ID, 
					   MESSAGE_TYPE, 
					   MESSAGE, 
					   LOCATION, 
					   CREATED_BY, 
					   CREATED_DATE)
values(@0,@1,@2,@3,@4,@5,@6,GETDATE())