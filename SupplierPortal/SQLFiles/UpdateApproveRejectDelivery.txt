﻿DECLARE @@sql nvarchar(max),
		@@sql2 nvarchar(max),
		@@actionType varchar(10) = @0,
		@@deliveryNo char(13) = @1,
		@@deliveryStatus char(1) = @2,
		@@route varchar(15) = @3,
		@@tripNo nvarchar(2) = @4,
		@@logisticPartnetCode nvarchar(4) = @5,
		@@supplierCode nvarchar(4) = @6,
		@@supplierPlantCode char(1) = @7,		
		@@actionBy nvarchar(50) = @8
		
IF (@@actionType = 'Approve')
BEGIN 
	SET @@sql = '
		UPDATE TB_R_LP_APPROVAL_H
			SET APPROVAL_STATUS = ''Approved By ' + @@actionBy + ''',
				APPROVED_BY = ''' + @@actionBy + ''',
				APPROVAL_DT = GETDATE(),
				REQUEST_STATUS = (CASE WHEN MODIFY_STATUS IS NULL THEN ''As Requested'' ELSE ''Modified'' END)
		WHERE 
			DELIVERY_NO = ''' + @@deliveryNo + ''' 
	'

	SET @@sql2 = '
		UPDATE TB_R_DELIVERY_CTL_H 
			SET APPROVE_BY = ''' + @@actionBy + ''',
				APPROVE_DT = GETDATE(),
				REJECT_BY = NULL,
				REJECT_DT = NULL,
				PUSH_TO_WEB_FLAG = 1
		WHERE DELIVERY_NO = ''' + @@deliveryNo + '''
	'
END ELSE IF (@@actionType = 'UnApprove')
BEGIN
	SET @@sql = '
		UPDATE TB_R_LP_APPROVAL_H 
			SET APPROVAL_STATUS = ''Waiting Approval'',
				APPROVED_BY = NULL,
				APPROVAL_DT = NULL,
				REQUEST_STATUS = ''Un-Approved By ' + @@actionBy + '''
		WHERE 
			DELIVERY_NO = ''' + @@deliveryNo + ''' 
	'

	SET @@sql2 = '
		UPDATE TB_R_DELIVERY_CTL_H 
			SET APPROVE_BY = NULL,
				APPROVE_DT = NULL,
				REJECT_BY = NULL,
				REJECT_DT = NULL,
				PUSH_TO_WEB_FLAG = 0
		WHERE DELIVERY_NO = ''' + @@deliveryNo + '''
	'
END ELSE BEGIN
	SET @@sql = '
		UPDATE TB_R_LP_APPROVAL_H
			SET APPROVAL_STATUS = ''Rejected By ' + @@actionBy + ''',
				REJECTED_BY = ''' + @@actionBy + ''',
				REJECTION_DT = GETDATE(),
				REQUEST_STATUS = ''Rejected''
		WHERE 
			DELIVERY_NO = ''' + @@deliveryNo + ''' 
	'

	SET @@sql2 = '
		UPDATE TB_R_DELIVERY_CTL_H 
			SET APPROVE_BY = NULL,
				APPROVE_DT = NULL,
				REJECT_BY = ''' + @@actionBy + ''',
				REJECT_DT = GETDATE(),
				PUSH_TO_WEB_FLAG = 0
		WHERE DELIVERY_NO = ''' + @@deliveryNo + '''
	'
END

execute (@@sql+@@sql2)