﻿declare @@manifestNo varchar(20)=@0
--set @@manifestNo='1101238125'
select LOG_ID_H as logID, 
SEQ_NO as seqNo, 
POSTING_DT as postingDate, 
PROCESS_DT as processDate, 
MANIFEST_NO as manifestNo, 
KANBAN_ORER_NO as kanbanNo, PROD_PURPOSE_CD as prodPurpose
from TB_R_LOG_TRANSACTION_D
where MANIFEST_NO=@@manifestNo