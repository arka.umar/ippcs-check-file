/*
	created by niit.diko
*/

declare @@sql nvarchar(max), 
		@@supplierCode varchar(50)=@0, 
		@@productionYear varchar(10) = @1, 
		@@versi varchar(10) = @2
		
set @@sql = '   select 
					supplier.SUPPLIER_NAME SupplierName,
					crossTab.SUPPLIER_CD SupplierCode,
					crossTab.PRODUCTION_YEAR ProductionYear,
					crossTab.VERSICD [Version],
					crossTab.CREATED_DT UploadDate,
					crossTab.REMARK ReplyFeedback,
					crossTab.CHANGED_DT LastUpdate,
					left(chat.COMMENT_CONTENT, 7) + ''.'' [Messages]
				from TB_R_ANNUAL_CROSSTAB_H crossTab
					join TB_R_CHAT chat on chat.CHAT_ID = crossTab.CHAT_ID
					join TB_M_SUPPLIER supplier on supplier.SUPPLIER_CODE = crossTab.SUPPLIER_CD
			    where 1=1 '
			
if(@@supplierCode <> '')
	set @@sql = @@sql + 'and crossTab.Supplier_CD = '''+ @@supplierCode + ''' '

if(@@productionYear <> '')
	set @@sql = @@sql + 'and crossTab.Production_Year = ''' + @@productionYear + ''' '

if(@@versi <> '')
	set @@sql = @@sql + 'and crossTab.VERSICD = '''+ @@versi + ''' '

execute (@@sql)
