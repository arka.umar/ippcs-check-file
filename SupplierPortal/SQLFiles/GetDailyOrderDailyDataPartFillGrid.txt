﻿declare @@sql nvarchar(max), 
		@@manifestNo varchar(10) = @0
		
set @@sql = '
			SELECT 
				A.[PART_NO] Part_Number, 
				B.PART_NAME Part_Name, 
				[ORDER_QTY] Order_Qty, 
				[PCS_PER_CONTAINER] Qty_Per_Container
			FROM [dbo].[TB_R_DAILY_ORDER_PART] A
				LEFT JOIN [dbo].TB_M_PART B ON B.PART_NO=B.PART_NO
			where 1=1 '


execute (@@sql)