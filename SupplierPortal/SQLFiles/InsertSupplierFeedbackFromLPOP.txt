﻿/* 
 * ====================================================
 *                 MODIFICATION HISTORY
 * ====================================================
 * Created By	: niit.mulki
 * Created Date	: 25.03.2013
 * Description	: Compare data each month and then insert to SupplierFeedback
 * ====================================================
 * Changed By   : fid.salman
 * Changed Date : 24.05.2013
 * Description  :
 * ====================================================
 */

DECLARE
	@@DID as varchar(4) = @0,
	@@Vers as char(1) = @1,
	@@PackMonth as varchar(6) = @2,
	@@PackPreviousMonth as varchar(12),
	@@HasRows INT
		
SET @@PackPreviousMonth = replace(convert(varchar(7), dateadd(MONTH, (-1), CONVERT(datetime, @@PackMonth + '01')), 120), '-', '')

SELECT @@HasRows = COUNT(*) FROM dbo.TB_R_SUPPLIER_FEEDBACK WHERE PRODUCTION_MONTH=@@PackMonth AND FORECAST_TYPE = @@Vers

IF(@@HasRows <= 0)
BEGIN
	--IF EXISTS (SELECT 1 FROM TB_R_LPOP_SUM as L WHERE D_ID = @@DID AND PACK_MONTH = @@PackMonth AND VERS = @@Vers)
		IF EXISTS (SELECT 1 FROM TB_R_LPOP_SUM as L WHERE D_ID = @@DID AND PACK_MONTH = @@PackMonth AND VERS = @@Vers)
			BEGIN
				INSERT INTO TB_R_SUPPLIER_FEEDBACK
					(SUPPLIER_CD, SUPPLIER_PLANT
					,PART_NAME, PART_NO
					,PRODUCTION_MONTH, FORECAST_TYPE
					,N_FA, N_1_FA
					,N_2_FA, N_3_FA, D_ID, DOCK_CD
					,CREATED_BY, CREATED_DT,N_FREQ,N_1_FREQ,N_2_FREQ,N_3_FREQ)
					SELECT SUPPLIER_CD
					  ,S_PLANT_CD
					  ,PART_NAME
					  ,PART_NO
					  ,PACK_MONTH
					  ,@@Vers
					  ,convert(varchar(15), 
						CASE 
						WHEN
							ISNULL((SELECT
									SUM(CONVERT(decimal(15, 2), isnull(N_1_VOLUME, 0)))  
								FROM TB_R_LPOP_SUM
								WHERE  PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH = @@PackPreviousMonth
										AND VERS='F'
										), 0) = 0 THEN 0
						ELSE
						  CONVERT(decimal(15, 0),(SUM(CONVERT(decimal(15,2), isnull(N_VOLUME, 0))) - 
							 (SELECT ISNULL(
								 (SELECT
									SUM(CONVERT(decimal(15, 2), isnull(N_1_VOLUME, 0)))  
								  FROM TB_R_LPOP_SUM 
									WHERE PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0))
							 )
						  / CONVERT(decimal(15,2), 
							(SELECT ISNULL((SELECT
									SUM(CONVERT(decimal(15, 2), isnull(N_1_VOLUME, 0)))  
								FROM TB_R_LPOP_SUM
								WHERE  PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0))
								) * 100)
						END
					  ) + '%'
					  as N
					  ,convert(varchar(15), 
						CASE 
						WHEN
							ISNULL((SELECT
								SUM(CONVERT(decimal(15, 2), isnull(N_2_VOLUME, 0)))  
								FROM TB_R_LPOP_SUM
								WHERE  PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0) = 0 THEN 0
						ELSE
						  CONVERT(decimal(15, 0),(SUM(CONVERT(decimal(15,2), isnull(N_1_VOLUME, 0))) - 
							 (SELECT ISNULL(
								 (SELECT
								  SUM(CONVERT(decimal(15, 2), isnull(N_2_VOLUME, 0)))  
								  FROM TB_R_LPOP_SUM 
									WHERE PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0))
							 )
						  / CONVERT(decimal(15,2), 
							(SELECT ISNULL((SELECT
								SUM(CONVERT(decimal(15, 2), isnull(N_2_VOLUME, 0)))  
								FROM TB_R_LPOP_SUM
								WHERE  PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0))
								) * 100)
						END
					  ) + '%'
					  as N1
					  ,convert(varchar(15), 
						CASE 
						WHEN
							ISNULL((SELECT
								SUM(CONVERT(decimal(15, 2), isnull(N_3_VOLUME, 0)))  
								FROM TB_R_LPOP_SUM
								WHERE  PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0) = 0 THEN 0
						ELSE
						  CONVERT(decimal(15, 0),(SUM(CONVERT(decimal(15,2), isnull(N_2_VOLUME, 0))) - 
							 (SELECT ISNULL(
								 (SELECT
								  SUM(CONVERT(decimal(15, 2), isnull(N_3_VOLUME, 0)))  
								  FROM TB_R_LPOP_SUM 
									WHERE PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0))
							 )
						  / CONVERT(decimal(15,2), 
							(SELECT ISNULL((SELECT
								SUM(CONVERT(decimal(15, 2), isnull(N_3_VOLUME, 0)))  
								FROM TB_R_LPOP_SUM
								WHERE  PART_NO = L.PART_NO
										AND DOCK_CD = L.DOCK_CD
										AND PACK_MONTH =  @@PackPreviousMonth
										AND VERS='F'), 0))
								) * 100)
						END
					  ) + '%'
					  as N2
					  ,'0%' as N3
					  ,CASE WHEN SUM(N_FREQ) IS NOT NULL 
								 AND SUM(N_1_FREQ) IS NOT NULL
								 AND SUM(N_2_FREQ) IS NOT NULL
								 AND SUM(N_3_FREQ) IS NOT NULL
						THEN 'SPEX' ELSE D_ID END D_ID
					  ,DOCK_CD
					  ,'System' as CreatedBy
					  ,GETDATE() as CreatedDate
					  ,SUM(N_FREQ) N_FREQ
					  ,SUM(N_1_FREQ) N_1_FREQ
					  ,SUM(N_2_FREQ) N_2_FREQ
					  ,SUM(N_3_FREQ) N_3_FREQ
				  FROM TB_R_LPOP_SUM as L
				  WHERE 
					D_ID = @@DID 
					AND PACK_MONTH = @@PackMonth
					AND VERS = @@Vers

				GROUP BY PART_NO
					,PART_NAME
					  ,PACK_MONTH
					  ,VERS
					  ,D_ID
					  ,DOCK_CD
					  ,SUPPLIER_CD
					  ,S_PLANT_CD
		END
	END