﻿Declare @@sql varchar(max),
        @@SuppCd varchar(max) = REPLACE(@0,';',''',''')
	
		
set @@sql = 'SELECT A.SUPPLIER_CD,
(select top 1 substring(d.SUPP_NAME,1, case when patindex(''%(%'',d.SUPP_NAME)-1>0 then patindex(''%(%'',d.SUPP_NAME)-1
  else len(d.SUPP_NAME) end ) from TB_M_SUPPLIER_ICS d where d.SUPP_CD=a.SUPPLIER_CD )SUPPLIER_NAME
  ,A.ITEM_NO,
  CONFIRM_BY_SUPPLIER,
CONFIRM_DT_SUPPLIER
,a.MACHINE_NAME,A.DETAIL_TYPE, ISNULL(CONVERT(VARCHAR(MAX), A.TONAGE), '''') as TONAGE,A.UNIT,A.BRAND_NAME,A.QTY,A.YEAR_BUYING,A.YEAR_MAKING,A.REMARK
,A.CHANGED_BY,A.CHANGED_DT,A.CREATED_BY,A.CREATED_DT,

convert(varchar(max), CAST(A.TONAGE as money), -1) as TONAGE_F,
convert(varchar(max), CAST(A.UNIT as money), -1) as UNIT_F,
convert(varchar(max), CAST(A.QTY as money), -1) as QTY_F

 FROM TB_M_SUPPLIER_EQUIPMENT A
	where 1=1 '

if(@@SuppCd <> '')
	set @@sql = @@sql + 'and A.SUPPLIER_CD  IN ('''+ @@SuppCd +''') '


execute (@@sql)