Declare @@sql varchar(max),
		@@OrderNo varchar(max) =@0,
		@@OrderDate varchar(max)=@1,
		@@PlantCD varchar(max) =@2,
		@@DockCd varchar(max)=@3

IF object_id('tempdb..#TempDockCode') IS NOT NULL DROP TABLE #TempDockCode
CREATE TABLE #TempDockCode
( 
	DOCK_CODE VARCHAR(100)
)

INSERT  INTO #TempDockCode(DOCK_CODE)
EXEC dbo.SplitString @@DockCd, ','
		
set @@sql =  ' SELECT  [SUPPLIER_CODE]
      ,[SUPPLIER_PLANT]
      ,[SHIPPING_DOCK]
      ,[COMPANY_CODE]
      ,[RCV_PLANT_CODE]
      ,[DOCK_CODE]
      ,[ORDER_NO]
      ,[CAR_FAMILY_CODE]
      ,[RE_EXPORT_CODE]
      ,[PART_NO]
      ,[KNBN_PRN_ADDRESS]
      ,[ORDER_TYPE]
      ,[ORDER_RELEASE_DATE]
      ,[ORDER_RELEASE_TIME]
      ,[KNBN_NO]
      ,[QTY_PER_CONTAINER]
      ,[ORDER_PLAN_QTY]
      ,[ORDER_PLAN_QTY_LOT]
      ,[BUILD_OUT_FLAG]
      ,[BUILD_OUT_QTY]
      ,[AICO_CEPT_FLAG]
      ,[SUPPLIER_NAME]
      ,[PARTS_NAME]
      ,[ORDER_MODE]
      ,[SHIPPING_DATE]
      ,[ARRIVAL_DATE]
      ,[MR_GRP_CODE]
      ,[MR_ORDER_SEQ]
      ,[MR_DATE]
      ,[SR_GRP_CODE]
      ,[SR_SEQ]
      ,[SR_DATE]
      ,[FRST_X_DOC_CODE]
      ,[FST_X_PLANT_CODE]
      ,[FST_X_SHIPPING_DOCK_CODE]
      ,[FST_X_DOCK_ARRIVAL_DATE]
      ,[FST_X_DOCK_DEPART_DATE]
      ,[FST_X_DOCK_ROUTE_GRP_CODE]
      ,[FST_X_DOCK_SEQ_NO]
      ,[FST_ROUTE_DATE]
      ,[SND_X_DOC_CODE]
      ,[SND_X_PLANT_CODE]
      ,[SND_X_SHIPPING_DOCK_CODE]
      ,[SND_X_DOCK_ARRIVAL_DATE]
      ,[SND_X_DOCK_DEPART_DATE]
      ,[SND_X_DOCK_ROUTE_GRP_CODE]
      ,[SND_X_DOCK_SEQ_NO]
      ,[SND_ROUTE_DATE]
      ,[TRD_X_DOC_CODE]
      ,[TRD_X_PLANT_CODE]
      ,[TRD_X_SHIPPING_DOCK_CODE]
      ,[TRD_X_DOCK_ARRIVAL_DATE]
      ,[TRD_X_DOCK_DEPART_DATE]
      ,[TRD_X_DOCK_ROUTE_GRP_CODE]
      ,[TRD_X_DOCK_SEQ_NO]
      ,[TRD_ROUTE_DATE]
      ,[MANIFEST_PRN_POINT_CODE]
      ,[MANIFEST_PRN_DATE_PLANT]
      ,[MANIFEST_PRN_DATE_LOCAL]
      ,[KNBN_PRN_POINT_CODE]
      ,[KNBN_PRN_DATE_PLANT]
      ,[KNBN_PRN_DATE_LOCAL]
      ,[CONVEYANCE_ROUTE]
      ,[FERRY_PORT_ARRIVAL_DATE]
      ,[FERRY_PORT_DEPART_DATE]
      ,[STATUS_FLAG]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[FILENAME]
  FROM [TB_T_ORDERPLAN_TMP_KCI]
 WHERE 1=1 '


 if @@OrderNo <> ''
set @@sql = @@sql + 'AND ORDER_NOlike ''%'+@@OrderNo+'%'''
if @@OrderDate <> ''
set @@sql = @@sql + 'AND CONVERT(date,CREATED_DT) = CONVERT(date,'''+@@OrderDate+''')'
if @@PlantCd <> ''
set @@sql = @@sql + 'AND RCV_PLANT_CODE like ''%'+@@PlantCd+'%'''
if @@DockCd <>''
set @@sql = @@sql + ' AND [DOCK_CODE] IN (SELECT DOCK_CODE FROM #TempDockCode)'
	
	execute (@@sql)