﻿declare @@q varchar(max) = null

select @@q = 
'SELECT '''' Val, ''All'' Text
UNION ALL
SELECT Val, Text
 FROM OPENQUERY('+x.servername+', 
''SELECT PROD_PURPOSE_CD Val, PROD_PURPOSE_NAME Text
  FROM ' + x.dbname + '.dbo.TB_M_PROD_PURPOSE 
  group by PROD_PURPOSE_CD, PROD_PURPOSE_NAME ''
)'
from (
	select 
	 SERVERNAME= coalesce(
	 (SELECT SYSTEM_VALUE FROM [dbo].[TB_M_SYSTEM] 
	 WHERE FUNCTION_ID = 'LINKED_SERVER' 
	 AND SYSTEM_CD = 'NEW_ICS_DB')
		,'[BB071_DB]')
	,DBNAME = COALESCE(
	(SELECT top 1 SYSTEM_VALUE
	FROM   [dbo].[TB_M_SYSTEM]
	WHERE  FUNCTION_ID = 'DB_NAME'AND SYSTEM_CD = 'ICS_DB_NAME')
	,'[BB071_DB]') 
) x ; 
exec (@@q);
