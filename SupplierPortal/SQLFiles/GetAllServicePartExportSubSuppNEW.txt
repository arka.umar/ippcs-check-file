﻿IF OBJECT_ID('tempdb..#tmpSplitStringSupplier') IS NOT NULL
        DROP TABLE #tmpSplitStringSupplier

IF OBJECT_ID('tempdb..#tmpSplitStringDock') IS NOT NULL
        DROP TABLE #tmpSplitStringDock

IF OBJECT_ID('tempdb..#tmpSplitStringManifest') IS NOT NULL
        DROP TABLE #tmpSplitStringManifest

IF OBJECT_ID('tempdb..#manifestTemp') IS NOT NULL
        DROP TABLE #manifestTemp

IF OBJECT_ID('tempdb..#tmpSplitStringSubSupplier') IS NOT NULL
        DROP TABLE #tmpSplitStringSubSupplier

IF OBJECT_ID('tempdb..#tmpSplitStringManifest') IS NOT NULL
        DROP TABLE #tmpSplitStringManifest

IF OBJECT_ID('tempdb..#tmpSplitStringDockLDAP') IS NOT NULL
        DROP TABLE #tmpSplitStringDockLDAP

CREATE TABLE #tmpSplitStringDockLDAP (column1 VARCHAR(8000))

CREATE UNIQUE CLUSTERED INDEX Idx_DockLDAP ON #tmpSplitStringDockLDAP (column1);

INSERT INTO #tmpSplitStringDockLDAP (column1)
EXEC dbo.SplitString @11
        ,''

IF OBJECT_ID('tempdb..#tmpSplitStringSubSuppLDAP') IS NOT NULL
        DROP TABLE #tmpSplitStringSubSuppLDAP

CREATE TABLE #tmpSplitStringSubSuppLDAP (column1 VARCHAR(8000))

CREATE UNIQUE CLUSTERED INDEX Idx_DockLDAP ON #tmpSplitStringSubSuppLDAP (column1);

INSERT INTO #tmpSplitStringSubSuppLDAP (column1)
EXEC dbo.SplitString @12
        ,''

CREATE TABLE #tmpSplitStringSupplier (column1 VARCHAR(MAX))

CREATE TABLE #tmpSplitStringDock (column1 VARCHAR(MAX))

CREATE TABLE #tmpSplitStringManifest (column1 VARCHAR(MAX))

CREATE TABLE #manifestTemp (manifestNo VARCHAR(30))

CREATE TABLE #tmpSplitStringSubSupplier (column1 VARCHAR(MAX))

INSERT INTO #tmpSplitStringSupplier (column1)
EXEC dbo.SplitString @0
        ,''

INSERT INTO #tmpSplitStringDock (column1)
EXEC dbo.SplitString @1
        ,''

INSERT INTO #tmpSplitStringManifest (column1)
EXEC dbo.SplitString @6
        ,''

INSERT INTO #tmpSplitStringSubSupplier (column1)
EXEC dbo.SplitString @9
        ,''
         
INSERT INTO #manifestTemp
SELECT DISTINCT DOM.MANIFEST_NO
FROM TB_R_PRA_DAILY_ORDER_MANIFEST AS DOM WITH (NOLOCK)
INNER JOIN dbo.TB_R_PRA_DAILY_ORDER_PART OP ON DOM.MANIFEST_NO = OP.MANIFEST_NO
INNER JOIN TB_M_SUPPLIER S ON DOM.SUPPLIER_CD = S.SUPPLIER_CODE
        AND DOM.SUPPLIER_PLANT = S.SUPPLIER_PLANT_CD
WHERE DOM.ORDER_TYPE = @5
        AND (
                (
                        (
                                (DOM.[SUPPLIER_CD] + '-' + DOM.[SUPPLIER_PLANT]) IN (
                                        SELECT column1
                                        FROM #tmpSplitStringSupplier
                                        )
                                AND isnull(@0, '') <> ''
                                )
                        OR (
                                (DOM.[SUB_SUPP_CD] + '-' + DOM.[SUB_SUPP_PLANT]) IN (
                                        SELECT column1
                                        FROM #tmpSplitStringSubSupplier
                                        )
                                AND isnull(@9, '') <> ''
                                )
                        )
                OR (
                        (isnull(@0, '') = '')
                        AND (isnull(@9, '') = '')
                        )
                )
        AND (
                (
                        DOM.[DOCK_CD] IN (
                                SELECT column1
                                FROM #tmpSplitStringDock
                                )
                        AND isnull(@1, '') <> ''
                        )
                OR (isnull(@1, '') = '')
                )
        AND (
                (
                        (
                                (DOM.[SUPPLIER_CD]) = @10
                                AND isnull(@10, '') <> ''
                                )
                        OR (
                                (DOM.[SUB_SUPP_CD]) = @10
                                AND isnull(@10, '') <> ''
                                )
                        )
                OR ((isnull(@10, '') = ''))
                )
        AND (
                (
                        DOM.[DOCK_CD] IN (
                                SELECT column1
                                FROM #tmpSplitStringDockLDAP
                                )
                        AND isnull(@11, '') <> ''
                        )
                OR (isnull(@11, '') = '')
                )
        AND (
                (
                        DOM.MANIFEST_NO LIKE '%'+ RTRIM(LTRIM(@2)) +'%' 
                        AND isnull(@2, '') <> ''
                        )
                OR (isnull(@2, '') = '')
                )
        AND
        --((CONVERT(DATE,DOM.ORDER_RELEASE_DT,112) BETWEEN CAST(CONVERT(VARCHAR(8),CAST(@3 AS DATETIME),112) AS DATE) AND CAST(CONVERT(VARCHAR(8),CAST(@4 AS DATETIME),112) AS DATE))) AND
        (
                (
                        CONVERT(VARCHAR(8), DOM.ORDER_RELEASE_DT, 112) BETWEEN CONVERT(VARCHAR(8), CAST(@3 AS DATETIME), 112)
                                AND CONVERT(VARCHAR(8), CAST(@4 AS DATETIME), 112)
                        )
                        OR (@3 = '') OR (@3 IS NULL) OR (@4 = '') OR (@4 IS NULL)
                )
        AND (
                (
                        DOM.MANIFEST_NO IN (
                                SELECT column1
                                FROM #tmpSplitStringManifest
                                )
                        AND isnull(@6, '') <> ''
                        )
                OR (isnull(@6, '') = '')
                )
        AND ISNULL(DOM.DELETION_FLAG, 'N') = 'N'
        AND ISNULL(DOM.CANCEL_FLAG, '0') = '0'
        AND (
                (
                        OP.PART_NO LIKE '%' + RTRIM(LTRIM(@7)) + '%'
                        AND ISNULL(@7, '') <> ''
                        )
                OR (ISNULL(@7, '') = '')
                )

IF (@8 = 'Build Out') --jika build_out_flag di TB_R_PRA_DAILY_ORDER_PART in (2, 3) maka sudah pasti build out, selain itu check dulu ke tb_m_part_bo
BEGIN
        SELECT DISTINCT DOM.MANIFEST_NO
                ,DOM.ORDER_RELEASE_DT
                ,DOM.ORDER_NO
                ,DOM.DOCK_CD
                ,DOM.SUPPLIER_CD
                ,DOM.SUPPLIER_PLANT
                ,DOM.TOTAL_QTY
                ,DOM.PRINT_FLAG
                ,CASE 
                        WHEN ISNULL(DOM.SUB_SUPP_CD, '') <> ''
                                THEN ISNULL(SSP.SUB_SUPP_NAME, ' ')
                        ELSE S.SUPPLIER_NAME
                        END AS SUPPLIER_NAME
                ,DOM.ARRIVAL_PLAN_DT
                ,RECEIVE_FLAG = ISNULL(DOM.RECEIVE_FLAG, '0')
                ,FTP_FLAG = ISNULL(DOM.FTP_FLAG, '0')
                ,DOWNLOAD_FLAG = ISNULL(DOM.DOWNLOAD_FLAG, '0')
                ,DOM.DOWNLOADED_BY
                ,DOM.DOWNLOADED_DT
                ,ISNULL(DOM.SUB_SUPP_CD, '') SUB_SUPP_CD
                ,ISNULL(DOM.SUB_SUPP_PLANT, '') SUB_SUPP_PLANT
                ,SUM(ISNULL(OP.RECEIVE_QTY,0)) AS DELIVERY_QTY
                ,SUM(ISNULL(OP.REMAIN_RECEIVE_QTY,0)) AS REMAIN_QTY
                ,ISNULL(DOM.COMPLETION_STS,'0') AS COMPLETION_STS
                ,(SUM(ISNULL(OP.ORDER_QTY,0))) - (SUM(ISNULL(OP.REMAIN_RECEIVE_QTY,0))) AS SUBMANIFEST_QTY
                --,CASE WHEN M.MANIFEST_RECEIVE_FLAG <> 0 
                --        THEN SUM(M.TOTAL_QTY)
                --ELSE 0
                --END AS RECEIVE_QTY
                ,SUM(ISNULL(OP.RECEIVE_QTY,0)) AS RECEIVE_QTY
        FROM TB_R_PRA_DAILY_ORDER_MANIFEST AS DOM WITH (NOLOCK)
        INNER JOIN dbo.TB_R_PRA_DAILY_ORDER_PART OP ON DOM.MANIFEST_NO = OP.MANIFEST_NO
        INNER JOIN TB_M_SUPPLIER S ON DOM.SUPPLIER_CD = S.SUPPLIER_CODE
                AND DOM.SUPPLIER_PLANT = S.SUPPLIER_PLANT_CD
        LEFT JOIN TB_M_SPEX_SUB_SUPPLIER SSP ON SSP.SUB_SUPP_CD = DOM.SUB_SUPP_CD
                AND SSP.SUB_SUPP_PLANT = DOM.SUB_SUPP_PLANT
                --LEFT JOIN (
                --                        SELECT REFF_NO, SUM(TOTAL_QTY) AS TOTAL_QTY
                --                        FROM TB_R_DAILY_ORDER_MANIFEST M
                --                        WHERE ISNULL(M.MANIFEST_RECEIVE_FLAG, 0) <> 0
                --                        GROUP BY REFF_NO
                --                        ) M ON M.REFF_NO = DOM.MANIFEST_NO--AND DOM.RECEIVE_FLAG <> 0 
        WHERE EXISTS (
                        SELECT ''
                        FROM #manifestTemp mt
                        WHERE mt.manifestNo = DOM.MANIFEST_NO
                        )
                AND (
                        ISNULL(OP.BUILD_OUT_FLAG, '') IN (
                                '2'
                                ,'3'
                                )
                        OR EXISTS (
                                SELECT ''
                                FROM dbo.TB_M_PART_BO BO
                                INNER JOIN dbo.TB_M_SUPPLIER SS ON SS.SUPPLIER_NAME = OP.SUPPLIER_NAME
                                WHERE BO.PART_NO = OP.PART_NO
                                        AND BO.SUPPLIER_PLANT = OP.SUPPLIER_PLANT
                                        AND BO.DOCK_CD = OP.DOCK_CD
                                        AND SS.SUPPLIER_CODE = BO.SUPPLIER_CD
                                        AND BO.INACTIVE_FLAG = '0'
                                )
                        )
        --AND ISNULL(OP.BUILD_OUT_FLAG, '') IN ('', '0', '1')
        GROUP BY DOM.MANIFEST_NO
                ,DOM.ORDER_RELEASE_DT
                ,DOM.ORDER_NO
                ,DOM.DOCK_CD
                ,DOM.SUPPLIER_CD
                ,DOM.SUPPLIER_PLANT
                ,DOM.TOTAL_QTY
                ,DOM.PRINT_FLAG
                ,DOM.SUB_SUPP_CD
                ,SSP.SUB_SUPP_NAME
                ,S.SUPPLIER_NAME
                ,DOM.ARRIVAL_PLAN_DT
                ,DOM.RECEIVE_FLAG
                ,DOM.FTP_FLAG
                ,DOM.DOWNLOAD_FLAG
                ,DOM.DOWNLOADED_BY
                ,DOM.DOWNLOADED_DT
                ,DOM.SUB_SUPP_CD
                ,DOM.SUB_SUPP_PLANT
                ,DOM.COMPLETION_STS
                --,M.MANIFEST_RECEIVE_FLAG
                --,M.TOTAL_QTY  
                
        ORDER BY DOM.MANIFEST_NO ASC
END
ELSE
        IF (@8 = 'Reguler') --jika reguler, maka build_out_flag di TB_R_PRA_DAILY_ORDER_PART sama dengan in (null, '', 1) dan tidak ada di tb_m_part_bo
        BEGIN
                SELECT DISTINCT DOM.MANIFEST_NO
                        ,DOM.ORDER_RELEASE_DT
                        ,DOM.ORDER_NO
                        ,DOM.DOCK_CD
                        ,DOM.SUPPLIER_CD
                        ,DOM.SUPPLIER_PLANT
                        ,DOM.TOTAL_QTY
                        ,DOM.PRINT_FLAG
                        ,CASE 
                                WHEN ISNULL(DOM.SUB_SUPP_CD, '') <> ''
                                        THEN ISNULL(SSP.SUB_SUPP_NAME, ' ')
                                ELSE S.SUPPLIER_NAME
                                END AS SUPPLIER_NAME
                        ,DOM.ARRIVAL_PLAN_DT
                        ,RECEIVE_FLAG = ISNULL(DOM.RECEIVE_FLAG, '0')
                        ,FTP_FLAG = ISNULL(DOM.FTP_FLAG, '0')
                        ,DOWNLOAD_FLAG = ISNULL(DOM.DOWNLOAD_FLAG, '0')
                        ,DOM.DOWNLOADED_BY
                        ,DOM.DOWNLOADED_DT
                        ,ISNULL(DOM.SUB_SUPP_CD, '') SUB_SUPP_CD
                        ,ISNULL(DOM.SUB_SUPP_PLANT, '') SUB_SUPP_PLANT
                        ,SUM(ISNULL(OP.RECEIVE_QTY,0)) AS DELIVERY_QTY
                        ,SUM(ISNULL(OP.REMAIN_RECEIVE_QTY,0)) AS REMAIN_QTY
                        ,ISNULL(DOM.COMPLETION_STS,'0') AS COMPLETION_STS
                        ,(SUM(ISNULL(OP.ORDER_QTY,0))) - (SUM(ISNULL(OP.REMAIN_RECEIVE_QTY,0))) AS SUBMANIFEST_QTY                
                --,CASE WHEN M.MANIFEST_RECEIVE_FLAG <> 0 
                --        THEN SUM(M.TOTAL_QTY)
                --ELSE 0
                --END AS RECEIVE_QTY
                ,SUM(ISNULL(OP.RECEIVE_QTY,0)) AS RECEIVE_QTY
                FROM TB_R_PRA_DAILY_ORDER_MANIFEST AS DOM WITH (NOLOCK)
                INNER JOIN dbo.TB_R_PRA_DAILY_ORDER_PART OP ON DOM.MANIFEST_NO = OP.MANIFEST_NO
                INNER JOIN TB_M_SUPPLIER S ON DOM.SUPPLIER_CD = S.SUPPLIER_CODE
                        AND DOM.SUPPLIER_PLANT = S.SUPPLIER_PLANT_CD
                LEFT JOIN TB_M_SPEX_SUB_SUPPLIER SSP ON SSP.SUB_SUPP_CD = DOM.SUB_SUPP_CD
                        AND SSP.SUB_SUPP_PLANT = DOM.SUB_SUPP_PLANT
                --LEFT JOIN TB_R_DAILY_ORDER_MANIFEST AS M ON M.REFF_NO = DOM.MANIFEST_NO --AND DOM.RECEIVE_FLAG <> 0 
                --LEFT JOIN (
                --                        SELECT REFF_NO, SUM(TOTAL_QTY) AS TOTAL_QTY
                --                        FROM TB_R_DAILY_ORDER_MANIFEST M
                --                        WHERE ISNULL(M.MANIFEST_RECEIVE_FLAG, 0) <> 0
                --                        GROUP BY REFF_NO  
                --                        ) M ON M.REFF_NO = DOM.MANIFEST_NO--AND DOM.RECEIVE_FLAG <> 0 
                WHERE EXISTS (
                                SELECT ''
                                FROM #manifestTemp mt
                                WHERE mt.manifestNo = DOM.MANIFEST_NO
                                )
                        AND (
                                ISNULL(OP.BUILD_OUT_FLAG, '') IN (
                                        ''
                                        ,'0'
                                        ,'1'
                                        )
                                AND NOT EXISTS (
                                        SELECT ''
                                        FROM dbo.TB_M_PART_BO BO
                                        INNER JOIN dbo.TB_M_SUPPLIER SS ON SS.SUPPLIER_NAME = OP.SUPPLIER_NAME
                                        WHERE BO.PART_NO = OP.PART_NO
                                                AND BO.SUPPLIER_PLANT = OP.SUPPLIER_PLANT
                                                AND BO.DOCK_CD = OP.DOCK_CD
                                                AND SS.SUPPLIER_CODE = BO.SUPPLIER_CD
                                                AND BO.INACTIVE_FLAG = '0'
                                        )
                                )
                --AND ISNULL(OP.BUILD_OUT_FLAG, '') IN ('', '0', '1')
                GROUP BY DOM.MANIFEST_NO
                        ,DOM.ORDER_RELEASE_DT
                        ,DOM.ORDER_NO
                        ,DOM.DOCK_CD
                        ,DOM.SUPPLIER_CD
                        ,DOM.SUPPLIER_PLANT
                        ,DOM.TOTAL_QTY
                        ,DOM.PRINT_FLAG
                        ,DOM.SUB_SUPP_CD
                        ,SSP.SUB_SUPP_NAME
                        ,S.SUPPLIER_NAME
                        ,DOM.ARRIVAL_PLAN_DT
                        ,DOM.RECEIVE_FLAG
                        ,DOM.FTP_FLAG
                        ,DOM.DOWNLOAD_FLAG
                        ,DOM.DOWNLOADED_BY
                        ,DOM.DOWNLOADED_DT
                        ,DOM.SUB_SUPP_CD
                        ,DOM.SUB_SUPP_PLANT
                        ,DOM.COMPLETION_STS
                        --,M.MANIFEST_RECEIVE_FLAG 
                        --,M.TOTAL_QTY 
                ORDER BY DOM.MANIFEST_NO ASC
        END
        ELSE
        BEGIN
                SELECT DISTINCT DOM.MANIFEST_NO
                        ,DOM.ORDER_RELEASE_DT
                        ,DOM.ORDER_NO
                        ,DOM.DOCK_CD
                        ,DOM.SUPPLIER_CD
                        ,DOM.SUPPLIER_PLANT
                        ,DOM.TOTAL_QTY
                        ,DOM.PRINT_FLAG
                        ,CASE 
                                WHEN ISNULL(DOM.SUB_SUPP_CD, '') <> ''
                                        THEN ISNULL(SSP.SUB_SUPP_NAME, ' ')
                                ELSE S.SUPPLIER_NAME
                                END AS SUPPLIER_NAME
                        ,DOM.ARRIVAL_PLAN_DT
                        ,RECEIVE_FLAG = ISNULL(DOM.RECEIVE_FLAG, '0')
                        ,FTP_FLAG = ISNULL(DOM.FTP_FLAG, '0')
                        ,DOWNLOAD_FLAG = ISNULL(DOM.DOWNLOAD_FLAG, '0')
                        ,DOM.DOWNLOADED_BY
                        ,DOM.DOWNLOADED_DT
                        ,ISNULL(DOM.SUB_SUPP_CD, '') SUB_SUPP_CD
                        ,ISNULL(DOM.SUB_SUPP_PLANT, '') SUB_SUPP_PLANT
                        ,SUM(ISNULL(OP.RECEIVE_QTY,0)) AS DELIVERY_QTY
                        ,SUM(ISNULL(OP.REMAIN_RECEIVE_QTY,0)) AS REMAIN_QTY
                        ,ISNULL(DOM.COMPLETION_STS,'0') AS COMPLETION_STS
                        ,(SUM(ISNULL(OP.ORDER_QTY,0))) - (SUM(ISNULL(OP.REMAIN_RECEIVE_QTY,0))) AS SUBMANIFEST_QTY
                --,CASE WHEN M.MANIFEST_RECEIVE_FLAG <> 0 
                --        THEN SUM(M.TOTAL_QTY)
                --ELSE 0
                --END AS RECEIVE_QTY
                ,SUM(ISNULL(OP.RECEIVE_QTY,0)) AS RECEIVE_QTY
                FROM TB_R_PRA_DAILY_ORDER_MANIFEST AS DOM WITH (NOLOCK)
                INNER JOIN dbo.TB_R_PRA_DAILY_ORDER_PART OP ON DOM.MANIFEST_NO = OP.MANIFEST_NO
                INNER JOIN TB_M_SUPPLIER S ON DOM.SUPPLIER_CD = S.SUPPLIER_CODE
                        AND DOM.SUPPLIER_PLANT = S.SUPPLIER_PLANT_CD
                LEFT JOIN TB_M_SPEX_SUB_SUPPLIER SSP ON SSP.SUB_SUPP_CD = DOM.SUB_SUPP_CD
                        AND SSP.SUB_SUPP_PLANT = DOM.SUB_SUPP_PLANT
                --LEFT JOIN TB_R_DAILY_ORDER_MANIFEST AS M ON M.REFF_NO = DOM.MANIFEST_NO --AND DOM.RECEIVE_FLAG <> 0 
                --LEFT JOIN (
                --                        SELECT REFF_NO, SUM(TOTAL_QTY) AS TOTAL_QTY
                --                        FROM TB_R_DAILY_ORDER_MANIFEST M
                --                        WHERE ISNULL(M.MANIFEST_RECEIVE_FLAG, 0) <> 0  
                --                        GROUP BY REFF_NO
                --                        ) M ON M.REFF_NO = DOM.MANIFEST_NO--AND DOM.RECEIVE_FLAG <> 0 
                WHERE EXISTS (
                                SELECT ''
                                FROM #manifestTemp mt
                                WHERE mt.manifestNo = DOM.MANIFEST_NO
                                )
                GROUP BY DOM.MANIFEST_NO
                        ,DOM.ORDER_RELEASE_DT
                        ,DOM.ORDER_NO
                        ,DOM.DOCK_CD
                        ,DOM.SUPPLIER_CD
                        ,DOM.SUPPLIER_PLANT
                        ,DOM.TOTAL_QTY
                        ,DOM.PRINT_FLAG
                        ,DOM.SUB_SUPP_CD
                        ,SSP.SUB_SUPP_NAME
                        ,S.SUPPLIER_NAME
                        ,DOM.ARRIVAL_PLAN_DT
                        ,DOM.RECEIVE_FLAG
                        ,DOM.FTP_FLAG
                        ,DOM.DOWNLOAD_FLAG
                        ,DOM.DOWNLOADED_BY
                        ,DOM.DOWNLOADED_DT
                        ,DOM.SUB_SUPP_CD
                        ,DOM.SUB_SUPP_PLANT
                        ,DOM.COMPLETION_STS
                        --,M.MANIFEST_RECEIVE_FLAG
                        --, M.TOTAL_QTY 
                ORDER BY DOM.MANIFEST_NO ASC
        END