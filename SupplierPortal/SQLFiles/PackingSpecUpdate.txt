﻿--Cretaed by   : Septareo sagita
--Created Date : 7 Maret 2014 

 UPDATE TB_M_PACKING_SPEC
 SET SUPPLIER_CD		=@1
      ,SUPPLIER_PLANT	=@2
      ,SHIPPING_DOCK	=@3
      ,COMPANY_CD		=@4
      ,RCV_PLANT_CD		=@5
      ,DOCK_CD			=@6
      ,PART_NO			=@7
      ,PALETTE_TYPE		=@8
      ,PALETTE_LENGTH	=@9
      ,PALETTE_WIDTH	=@10
      ,PALETTE_HEIGHT	=@11
      ,CHANGED_BY		=@12
      ,CHANGED_DATE		=GETDATE()
 WHERE PACKING_SPEC_ID	=@0