﻿insert into TB_R_LOG_H(PROCESS_ID, 
					   FUNCTION_ID, 
					   MODULE_ID, 
					   START_DATE, 
					   END_DATE, 
					   PROCESS_STATUS, 
					   CREATED_BY, 
					   CREATED_DATE, 
					   CHANGED_BY, 
					   CHANGED_DATE)
values(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9)