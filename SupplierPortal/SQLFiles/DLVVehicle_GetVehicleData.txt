﻿DECLARE @@sql VARCHAR(MAX)

	SET @@sql = ' SELECT VEHICLE_ID, LP_CD, LOG_PARTNER_NAME AS LP_NAME, VEHICLE_NO, V.CREATED_BY, V.CREATED_DT, V.CHANGED_BY, V.CHANGED_DT
				  FROM TB_M_DLV_VEHICLE V
				  LEFT JOIN TB_M_LOGISTIC_PARTNER L
				  ON V.LP_CD = L.LOG_PARTNER_CD 
				  WHERE 1 = 1 '
				  
	IF(@0 <> '')
	BEGIN
		SET @@sql = @@sql + ' AND LP_CD IN (' + @0 + ')'
	END
	
	IF(@1 <> '')
	BEGIN
		SET @@sql = @@sql + ' AND VEHICLE_NO LIKE ''%' + @1 + '%'' '
	END	
	
	SET @@sql = @@sql + 'ORDER BY CREATED_DT, CHANGED_DT'	  

EXEC(@@sql)	
 
