declare
@@DoorCode  AS varchar(5)=@0,
@@DockCode AS varchar(3)=@1,
@@PhysicalDock AS varchar(3)=@2,
@@PhysicalStation AS varchar(1)=@3,
@@UsedFlag AS varchar(1) =@4,
@@USED INT = NULL;

select @@USED= case when @@UsedFlag = 'Y' THEN 1 WHEN @@UsedFlag = 'N' THEN 0 ELSE NULL END ;

select 
DOOR_CD_TLMS ,
DOCK_CD,
PHYSICAL_DOCK,
PHYSICAL_STATION,
USED_FLAG,
CREATED_BY,
CREATED_DT,
CHANGED_BY,
CHANGED_DT
from TB_M_DELIVERY_STATION_MAPPING
where 
       ((NULLIF(@@DoorCode,'') IS NULL)        OR (DOOR_CD_TLMS=  @@DoorCode) ) 
and    ((NULLIF(@@DockCode,'') IS NULL)        OR (DOCK_CD=  @@DockCode) ) 
and    ((NULLIF(@@PhysicalDock,'') IS NULL)    OR (PHYSICAL_DOCK=  @@PhysicalDock) ) 
and    ((NULLIF(@@PhysicalStation,'') IS NULL) OR (PHYSICAL_STATION=  @@PhysicalStation) ) 
and    ((NULLIF(@@USED,'') IS NULL)            OR (USED_FLAG=  @@USED) ) 
