select
        menu_id as Id,
        screen_id as ScreenId,
        caption as Caption,
        url as Url,
        parent_id as ParentId,
        position as Position
    from tb_m_menu
    where (menu_id = @0)
