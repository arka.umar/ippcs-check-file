﻿	DECLARE @@total int

	CREATE TABLE #username 
	(
		s varchar(200)
	)

	INSERT INTO #username SELECT s AS item FROM [dbo].fnSplitString(@0,';') option (maxrecursion 0);

	CREATE TABLE #role
	(
		s varchar(200)
	)
	INSERT INTO #role SELECT s AS item FROM [dbo].fnSplitString(@1,';') option (maxrecursion 0);

	CREATE TABLE #system
	(
		s varchar(200)
	)
	INSERT INTO #system SELECT item FROM [dbo].fnSplit(@2,';') option (maxrecursion 0);

	SELECT 
		@@total = COUNT(1)
	FROM [10.16.25.100].OpenLDAP.dbo.TB_M_AUTHORIZATION_MAPPING M 
		INNER JOIN [10.16.25.100].OpenLDAP.dbo.TB_M_ROLE R
	ON M.ROLE_ID = R.ROLE_ID
	INNER JOIN [10.16.25.100].OpenLDAP.dbo.TB_M_SYSTEM S
	ON M.SYSTEM_ID = S.SYSTEM_ID

	WHERE
		(
			(ISNULL(@0, '') <> '' AND EXISTS(SELECT s AS item FROM #username a WHERE a.s = M.USERNAME )) 
		OR
			(ISNULL(@0, '') = '')
		)
	AND
	(
		(
			(ISNULL(@1, '') <> '' AND EXISTS(SELECT s AS item FROM #role a WHERE a.s = R.ROLE_NAME )) 
		OR
			(ISNULL(@1, '') = '')
		)
	
	)
	AND
	(
		(
			(ISNULL(@2, '') <> '' AND EXISTS(SELECT s AS item FROM #system a WHERE a.s = R.SYSTEM_ID )) 
		OR
			(ISNULL(@2, '') = '')
		)
	
	)

	SELECT @@total

	IF OBJECT_ID('tempdb..#username') IS NOT NULL 
		DROP TABLE #username

		IF OBJECT_ID('tempdb..#role') IS NOT NULL 
		DROP TABLE #role

		IF OBJECT_ID('tempdb..#system') IS NOT NULL 
	DROP TABLE #system
	