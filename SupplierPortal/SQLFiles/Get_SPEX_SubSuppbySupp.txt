﻿SELECT
	CASE WHEN ISNULL(m.SUB_SUPP_CD, '') <> ''
		THEN ISNULL(m.SUB_SUPP_CD, '') + ISNULL(m.SUB_SUPP_PLANT, '')
		ELSE ISNULL(m.SUPPLIER_CD, '') + ISNULL(m.SUPPLIER_PLANT, '')
	END
FROM TB_R_DAILY_ORDER_MANIFEST m
	LEFT JOIN TB_M_SPEX_SUB_SUPPLIER ssp ON ssp.SUB_SUPP_CD = m.SUB_SUPP_CD AND ssp.SUB_SUPP_PLANT = m.SUB_SUPP_PLANT
WHERE m.MANIFEST_NO = @0 AND
	CONVERT(VARCHAR(8),m.CREATED_DT,112) = @1 AND
	m.SUPPLIER_CD = @2 AND
	m.SUPPLIER_PLANT = @3