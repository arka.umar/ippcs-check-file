﻿Declare @@sql varchar(max),

		@@PickupDate varchar(20) = @0,
        @@LPCode nvarchar(4) = @1, 
		@@LPName nvarchar(30) = @2,
		@@routeEdit varchar(15) = @3,
		@@tripNoEdit nvarchar(2) = @4,
		@@deliveryNo char(13) = @5,
		@@deliveryStatus char(1) = @6,
		@@route varchar(15) = @7,
		@@tripNo nvarchar(2) = @8,
		@@logisticPartnetCode nvarchar(4) = @9,
		@@supplierCode nvarchar(4) = @10,
		@@supplierPlantCode char(1) = @11,
		@@changeBy nvarchar(50) = @12

set @@sql = '
		UPDATE TB_R_LP_APPROVAL_H
			SET LP_CD = ''' + @@LPCode + ''',
				MODIFY_STATUS = ''M'',
				CHANGED_BY = ''' + @@changeBy + ''',
				CHANGED_DT = GETDATE()
		WHERE 
			DELIVERY_NO = ''' + @@deliveryNo + ''' 
			'

set @@sql = @@sql + ';
				UPDATE TB_R_DELIVERY_CTL_H
					SET LP_CD = ''' + @@LPCode + ''',
						CHANGED_BY = ''' + @@changeBy + ''',
						CHANGED_DT = GETDATE()
				WHERE DELIVERY_NO = ''' + @@deliveryNo + ''';
			'

execute (@@sql)








