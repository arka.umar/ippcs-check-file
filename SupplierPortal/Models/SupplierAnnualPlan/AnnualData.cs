﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.SupplierAnnualPlan
{
    public class AnnualData
    {
        //public bool Check { set; get; }
        public int ProductionYear { set; get; }
        public string Version { set; get; }
        public DateTime UploadDate { set; get; }
        public string ReplyFeedback { set; get; }
        public DateTime LastUpdate { set; get; }
        public string Messages { set; get; }
    }
}