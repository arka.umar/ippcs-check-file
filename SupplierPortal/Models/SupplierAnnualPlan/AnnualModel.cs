﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierAnnualPlan
{
    public class AnnualModel
    {        
        public List<AnnualData> AnnualDatas { set; get; }

        public AnnualModel()
        {
            AnnualDatas = new List<AnnualData>();
        }
    }
}