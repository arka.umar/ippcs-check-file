﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SubManifestCreationSPEX
{
    public class SubManifestCreationSPEX
    {
        public string MANIFEST_NO { get; set; } //DOM
        public string MANIFEST_NO_DT { get; set; } //DOM
        public string SUB_ROUTE_CD { get; set; } //DOM
        public string DOCK_CD { get; set; } //DOM
        public string SUPPLIER_CD { get; set; } //DOM
        public string SUB_SUPP_CD { get; set; } //DOM
        public string PART_NO { get; set; } //DOP
        public string QTY_PER_CONTAINER { get; set; } //DOP
        public string ITEM_NO { get; set; } //DOP
        public string ORDER_QTY { get; set; } //DOP
        public string REMAIN_QTY { get; set; } //DOP
        public string DELIVERY_QTY { get; set; } //DOP

        public string DATA_ID { get; set; }
        public string QTY_DELIVERY { get; set; }
        public string CASE_NO { get; set; }
        public string KANBAN_ID { get; set; }

        public string PROCESS_ID { get; set; }
        public string ETD { get; set; }
        public string SUPPLIER_REF_1 { get; set; }
        public string SUPPLIER_REF_2 { get; set; }
        public string USER_ID { get; set; }
    }

    public class SMCListError
    {
        public string DATA_ID { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string QTY_DELIVERY { get; set; }
        public string CASE_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string ETD { get; set; }
        public string SUPPLIER_REF_1 { get; set; }
        public string SUPPLIER_REF_2 { get; set; }
        public string MESSAGE { get; set; }
    }

    public class ComboRoute
    {
        public string Route { set; get; }
    }

    public class SubManifestCreationSPEXDownload
    {
        public string MANIFEST_NO { get; set; } //DOM
        public string MANIFEST_NO_DT { get; set; } //DOM
        public string SUPPLIER_CD { get; set; } //DOM
        public string SUB_SUPP_CD { get; set; } //DOM
        public string DOCK_CD { get; set; } //DOM
        public string SUB_ROUTE_CD { get; set; } //DOM

        public string PART_NO { set; get; }
        public string QTY_PER_CONTAINER { set; get; }
        public string ITEM_NO { set; get; }
        public string ORDER_QTY { get; set; } //DOP
        public string REMAIN_QTY { get; set; } //DOP
        //public string DELIVERY_QTY { get; set; } //DOP
    }
}