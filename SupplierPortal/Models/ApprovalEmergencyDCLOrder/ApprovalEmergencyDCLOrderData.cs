﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ApprovalEmergencyDCLOrder
{
    public class ApprovalEmergencyDCLOrderData
    {
        private string _deliveryStatus = "";
        
        public string ROUTE { get; set; }
        public bool APPROVAL_STATUS_EDIT { get; set; }
        public string APPROVAL_STATUS { get; set; }
        public string REQUEST_STATUS { get; set; }
        public string MAX_APPROVE_DURATION { get; set; }
        public DateTime PICK_UP_DATE { get; set; }
        public DateTime REQUEST_DT { get; set; }
        public string DOCK { get; set; }
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        public string TRIP_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string REASON { get; set; }
        public Nullable<DateTime> ARRIVAL_DATE { get; set; }
        public string ARRIVAL_TIME { get; set; }
        public Nullable<DateTime> DEPT_DATE { get; set; }
        public string DEPT_TIME { get; set; }
        public string DELIVERY_NO { get; set; }
        public string DELIVERY_STATUS { 
            get { return _deliveryStatus; }
            set { _deliveryStatus = (value == null || value.Trim() == "") ? "" : value.ToLower().Trim().Substring(0, 1); }
        }
        public string DELIVERY_STATUS_STRING
        {
            get { return _deliveryStatus != "" ? (_deliveryStatus == "n" ? "New" : "Daily") : ""; }
            set { }
        }
        public string SUPPLIER_PLANT_STRING
        {
            get { return SUPPLIER_CD + " - " + SUPPLIER_PLANT_CD; }
            set { }
        }
    }

    public class DeliveryLPApprovalData
    {
        public string DELIVERY_NO { get; set; }
        public string ROUTE { get; set; }
        public string RATE { get; set; }
        public string ORIGIN_CODE { get; set; }
        public Nullable<DateTime> ORIGIN_ARRIVAL_TIME { get; set; }
        public Nullable<DateTime> ORIGIN_DEPARTURE_TIME { get; set; }
        public string DESTINATION_CD { get; set; }
        public Nullable<DateTime> DESTINATION_ARRIVAL_TIME { get; set; }
        public Nullable<DateTime> DESTINATION_DEPARTURE_TIME { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
    }

    public class DeliveryCTLOrderData
    {
        public string DELIVERY_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string ORDER_TYPE { get; set; }
        public DateTime ORDER_RELEASE_DT { get; set; }
        public string ROUTE { get; set; }
        public string RATE { get; set; }
        public string STATUS { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public int TOTAL_ITEM { get; set; }
        public int TOTAL_QTY { get; set; }
        public int ERROR_QTY { get; set; }
        public DateTime ARRIVAL_PLAN_DT { get; set; }
        public DateTime ARRIVAL_ACTUAL_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        //public string CHANGED_DT_STRING { get { return CHANGED_DT.ToString(); } }
        //public string CREATED_DT_STRING { get { return CREATED_DT.ToString(); } }
        //public string ARRIVAL_ACTUAL_DT_STRING { get { return ARRIVAL_ACTUAL_DT.ToString(); } }
        //public string ARRIVAL_PLAN_DT_STRING { get { return ARRIVAL_PLAN_DT.ToString(); } }
        //public string ORDER_RELEASE_DT_STRING { get { return ORDER_RELEASE_DT.ToString(); } }
    }

    public class ArrivalDepartureData {
        public DateTime ARRIVAL_DATE { get; set; }
        public DateTime DEPT_DATE { get; set; }
    }
}