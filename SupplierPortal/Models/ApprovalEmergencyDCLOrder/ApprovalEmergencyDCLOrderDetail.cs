﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ApprovalEmergencyDCLOrder
{
    public class ApprovalEmergencyDCLOrderDetail
    {
        public string PickUpDate { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string Route { get; set; }
        public string TripNo { get; set; }
        public string OrderNo { get; set; }
        public string DockCode { get; set; }
    }
}