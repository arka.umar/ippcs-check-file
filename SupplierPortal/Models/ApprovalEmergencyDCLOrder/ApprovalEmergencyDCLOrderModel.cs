﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Portal.Models.ApprovalEmergencyDCLOrder
{
    public class ApprovalEmergencyDCLOrderModel
    {
        public DataTable DataAE { set; get; }
        public List<ApprovalEmergencyDCLOrderData> Apps { get; set; }
        public List<ApprovalEmergencyDCLOrderDetail> AppDetails { get; set; }
        public List<DeliveryLPApprovalData> PopUp { get; set; }

        public ApprovalEmergencyDCLOrderModel()
        {
            Apps = new List<ApprovalEmergencyDCLOrderData>();
            AppDetails = new List<ApprovalEmergencyDCLOrderDetail>();
            PopUp = new List<DeliveryLPApprovalData>();
        }
    }
}