﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.KanbanReceivingManifestLevel
{
    public class KanbanReceivingManifestLevelModel
    {
        public List<KanbanReceivingManifestLevelData> KanbanReceivingManifestLevelListData { get; set; }
        public KanbanReceivingManifestLevelModel()
        {
            KanbanReceivingManifestLevelListData = new List<KanbanReceivingManifestLevelData>();
        }
    }//t
}