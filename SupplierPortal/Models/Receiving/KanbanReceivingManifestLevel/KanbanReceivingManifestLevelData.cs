﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.KanbanReceivingManifestLevel
{
    public class KanbanReceivingManifestLevelData
    {//t
        public int ROW_NUM { get; set; }
        public string KANBAN_ID { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public int ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string RECEIVING_FLG { get; set; }
        public int ORDER { get; set; }
        public int RECEIVE { get; set; }
        public int SHORTAGE { get; set; }
        public string KANBAN_NO { get; set; }
    }
}