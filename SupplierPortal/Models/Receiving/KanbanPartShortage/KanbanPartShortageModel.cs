﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.KanbanPartShortage
{
    public class KanbanPartShortageModel
    {//t
        public List<KanbanPartShortageData> KanbanPartShortageListData { get; set; }

        public KanbanPartShortageModel()
        {
            KanbanPartShortageListData = new List<KanbanPartShortageData>();
        }
    }
}


