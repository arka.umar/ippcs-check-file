﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;


namespace Portal.Models.Receiving.KanbanPartShortage
{
    public class KanbanPartShortageData
    {
        //t
        public int Row { get; set; }
        public string KANBAN_ID { get; set; }
        public string KANBAN_NO { get; set; }
        public string PART_NAME { get; set; }
        public string PART_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO {get; set;}
        public string P_LANE_SEQ { get; set; }
        public string ROUTE_RATE { get; set; }
        public DateTime ARRIVAL_PLAN_DT { get; set; }
        public DateTime ARRIVAL_ACTUAL_DT { get; set; }
        public int ORDER { get; set; }
        public int SHORTAGE { get; set; }
        public string REMARK { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string RCV_DOCK_CD { get; set; }
        public int QTY_PER_CONTAINER { get; set; }
        public int RECEIVE { get; set; }
        public int REMARKED { get; set; }
    }
}