﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.KanbanPartShortage
{
    public class KanbanPartShortageRemark
    {//t
        public string KANBAN_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string PART_NO { get; set; }
        public string REMARK { get; set; }
        public int ERROR { get; set; }
        public string MESSAGE { get; set; }
    }
}