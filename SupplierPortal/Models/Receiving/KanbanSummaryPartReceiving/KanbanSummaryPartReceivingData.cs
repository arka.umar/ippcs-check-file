﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Receiving.KanbanSummaryPartReceiving
{
    public class KanbanSummaryPartReceivingData
    {
        public string KANBAN_ID { get; set; }
        public string KANBAN_NO { get; set; }
        public string PART_NAME { get; set; }
        public string PART_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }//t
        public string STATUS { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string P_LANE_SEQ { get; set; }
        public DateTime ARRIVAL_PLAN_DT { get; set; }
        public DateTime ARRIVAL_ACTUAL_DT { get; set; }
        public int ORDER { get; set; }
        public int RECEIVED { get; set; }
        public string ROUTE_RATE { get; set; }
        public int QTY_PER_CONTAINER { get; set; }
        public int REMARKED { get; set; }

    }


    public class KanbanSummaryPartReceivingDataDownload
    {
        public string KANBAN_ID { get; set; }
        public string KANBAN_NO { get; set; }
        public string PART_NAME { get; set; }
        public string PART_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string STATUS { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string P_LANE_SEQ { get; set; }
        public string ARRIVAL_PLAN_DT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public int ORDER { get; set; }
        public int RECEIVED { get; set; }
        public string ROUTE_RATE { get; set; }
        public int QTY_PER_CONTAINER { get; set; }

    }

    public class p_lane_data
    {
        public string P_LANE_SEQ { get; set; }
    }

    public class SupplierData
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
    }

    public class RouteData
    {
        public string ROUTE { get; set; }
    }

    public class StatusData
    {
        public string STATUS { get; set; }
    }

    public class DockData
    {
        public string DOCK_CD { get; set; }
        public string DOCK_NM { get; set; }
    }
}