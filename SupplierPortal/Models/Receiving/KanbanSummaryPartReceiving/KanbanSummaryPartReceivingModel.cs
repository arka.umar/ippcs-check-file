﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.KanbanSummaryPartReceiving
{
    public class KanbanSummaryPartReceivingModel
    {
       public List<KanbanSummaryPartReceivingData> KanbanSummaryPartReceivingListData { get; set; }

        public KanbanSummaryPartReceivingModel()
        {
            KanbanSummaryPartReceivingListData = new List<KanbanSummaryPartReceivingData>();
        }
    }
}//t