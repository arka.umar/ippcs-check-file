﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;
namespace Portal.Models.Receiving.ReceivingProgress
{
    public class ReceivingProgressData
    {
        public string P_LANE_SEQ { get; set; }
        public int TOTAL { get; set; }
        public int UNPROGRESS { get; set; }
        public int PROGRESS { get; set; }
        public string COLOR { get; set; }
    }
}//t