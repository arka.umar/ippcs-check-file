﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.KanbanReceiving
{
    public class KanbanReceivingModel
    {//t
        public List<KanbanReceivingData> KanbanReceivingListData { get; set; }
        
        public KanbanReceivingModel()
        {
            KanbanReceivingListData = new List<KanbanReceivingData>();
        }
    }

    public class KanbanReceivingDownloadModel
    {
        public List<KanbanReceivingDataDownload> KanbanReceivingListDownloadData { get; set; }
        public KanbanReceivingDownloadModel()
        {
            KanbanReceivingListDownloadData = new List<KanbanReceivingDataDownload>();
        }
    }
}