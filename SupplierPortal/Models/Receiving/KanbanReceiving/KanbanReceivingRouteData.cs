﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Receiving.KanbanReceiving
{
    public class KanbanReceivingRouteData
    {//t
        public string ORDER_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string ROUTE_RATE { get; set; }
        
    }
}