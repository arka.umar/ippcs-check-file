﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Receiving.KanbanReceiving
{
    public class KanbanReceivingDetailForMobile
    {//t
        public string DOCK_CD {get; set;}
        public string KANBAN_ID {get; set;}
        public string RECEIVING_FLG {get; set;}
    }
}