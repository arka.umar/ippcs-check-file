﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Receiving.KanbanReceiving
{
    public class KanbanReceivingData
    {//t
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string ROUTE_RATE { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public int ORDER_PLAN_QTY_LOT { get; set; }
        public int RECEIVED { get; set; }
        public int SHORTAGE { get; set; }
        public DateTime ARRIVAL_ACTUAL_DT { get; set; }
        public string STATUS { get; set; }
        public string P_LANE_CD { get; set; }
        public string KANBAN_NO { get; set; }
        public string REMARK { get; set; }
        //public int? REMARKED { get; set; }
    }


    public class KanbanReceivingDataDownload
    {
        public string KANBAN_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string P_LANE_NO { get; set; }
        public string ROUTE { get; set; }
        public string RIT { get; set; }
        public string ARRIVAL_PLAN_DT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string STATUS { get; set; }
        public string UPDATE { get; set; }
    }


    public class KanbanReceivingDataHeaderUpload
    {
        public string PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public int UPDATE_COUNT { get; set; }
        public int UPLOADED_COUNT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
    }
}