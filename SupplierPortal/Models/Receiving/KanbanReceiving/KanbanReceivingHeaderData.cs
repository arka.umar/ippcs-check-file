﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.KanbanReceiving
{
    public class KanbanReceivingHeaderData
    {//t
        public string DELIVERY_NO { get; set; }
        public string TRUCK_STATION { get; set; }
        public string ROUTE { get; set; }
        public string RATE { get; set; }
        public string ARRIVAL_PLAN_DT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string DEPARTURE_PLAN_DT { get; set; }
        public string DEPARTURE_ACTUAL_DT { get; set; }
        public string DOCK_CD { get; set; }
        public string MANIFEST_NO { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string P_LANE_CD { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
    }
}