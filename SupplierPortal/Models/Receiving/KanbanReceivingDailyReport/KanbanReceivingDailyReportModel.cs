﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;
namespace Portal.Models.Receiving.KanbanReceivingDailyReport
{
    public class KanbanReceivingDailyReportModel
    {//t
        public List<KanbanReceivingDailyReportData> KanbanReceivingListData { get; set; }
        public KanbanReceivingDailyReportModel()
        {
            KanbanReceivingListData = new List<KanbanReceivingDailyReportData>();
        }
    }
}