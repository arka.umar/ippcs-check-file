﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;
namespace Portal.Models.Receiving.KanbanReceivingDailyReport
{
    public class KanbanReceivingDailyReportData
    {//t
        public DateTime PROCESS_DT { get; set; }
        public string PRODUCTION_DT { get; set; }
        public string DATE { get; set; }
        public string MONTH { get; set; }
        public string YEAR { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string KANBAN_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public DateTime ARRIVAL_PLAN_DT { get; set; }
        public int QTY_PER_CONTAINER { get; set; }
        public int SHORTAGE_KBN { get; set; }
        public int SHORTAGE_PCS { get; set; }
    }
}