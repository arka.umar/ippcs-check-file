﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.TruckReceiving
{
    public class TruckReceivingHeader
    {
        public string DELIVERY_NO { get; set; }
        public string TRUCK_STATION { get; set; }
        public string ROUTE { get; set; }
        public string RATE { get; set; }
        public string ARRIVAL_PLAN_DT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string DEPARTURE_PLAN_DT { get; set; }//t
        public string DEPARTURE_ACTUAL_DT { get; set; }
        public string DOCK_CD { get; set; }
    }
}