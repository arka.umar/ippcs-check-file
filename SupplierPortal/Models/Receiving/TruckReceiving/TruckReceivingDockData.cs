﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.TruckReceiving
{
    public class TruckReceivingDockData
    {
        public string DOCK_CD { get; set; }
        public string DOCK_NM { get; set; }
    }
}//t