﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.TruckReceiving
{
    public class TruckReceivingDetailModel
    {
        public List<TruckReceivingDetail> truckReceivingListData { get; set; }

        public TruckReceivingDetailModel()
        {
            truckReceivingListData = new List<TruckReceivingDetail>();
        }
    }
}//t