﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Receiving.TruckReceiving
{
    public class TruckReceivingDetail
    {
        public string P_LANE_CD { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string MANIFEST_NO { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string DOCK_NM { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public int TOTAL_ITEM { get; set; }//t
        public int COMPLETED { get; set; }
        public int INCOMPLETE { get; set; }
        public string STATUS { get; set; }
        public string REMARK { get; set; }
        public string ORDER_NO { get; set; }
        public int ERROR { get; set; }
        public string MESSAGE { get; set; }
        public int REMARKED { get; set; }

        public string DELIVERY_NO { get; set; }
        
    }
}