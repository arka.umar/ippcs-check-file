﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceAmountDifference
{
    public class InvoiceAmountDifferenceData
    {
       // public string SupplierCodePlantCode { set; get; }
        //
        public string SUPP_CODE { set; get; }
        public string INV_NO { set; get; }
        public double INV_AMOUNT { set; get; }
        public double SYSTEM_INV_AMOUNT { set; get; }
        public double DIFF_INV_AMOUNT { set; get; }
        public string TAX_INV_NO { set; get; }
        public double TAX_AMOUNT { set; get; }
        public double SYSTEM_TAX_AMOUNT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
}