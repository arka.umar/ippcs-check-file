﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceAmountDifference
{
    public class InvoiceAmountDifferenceModel
    {
        public List<InvoiceAmountDifferenceData> InvoiceAmountDifferenceDatas { set; get; }

        public InvoiceAmountDifferenceModel()
        {
            InvoiceAmountDifferenceDatas = new List<InvoiceAmountDifferenceData>();
        }
    }
}