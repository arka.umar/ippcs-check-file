﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.ProblemCategory
{
    public class ProblemCategoryData
    {
        public string PROBLEM_CATEGORY_CD	{ set; get; }
        public string PROBLEM_CATEGORY_NAME	{ set; get; }
    }
}