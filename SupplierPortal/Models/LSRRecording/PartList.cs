﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class PartList
    {
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string QTY { get; set; }
        public string REGISTER_DATE { get; set; }
        public string REGISTER_BY { get; set; }
        public string REASON { get; set; }
        public string PACKAGE_CONDITION { get; set; }
        public string REPAIR_CODE { get; set; } 
    }
}