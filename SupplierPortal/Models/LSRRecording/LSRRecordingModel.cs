﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingModel
    {
        public List<LSRRecordingHeader> LSRRecordingHeaders { set; get; }
        public List<LSRRecordingDetail> LSRRecordingDetails { set; get; }
        public List<LSRRecordingList> LSRRecordingLists { set; get; }
        public List<LSRRecordingCreation> LSRRecordingCreationList { set; get; }
        public List<LSRRecordingCreationDetails> LSRRecordingCreationDetailsList { set; get; }
        public List<LSRRecordingPartListModel> LSRRecordingPartListModelList { set; get; }

        public List<LSRRecordingPartListTemp> PartListTemp { set; get; }

        public LSRRecordingModel()
        {
            LSRRecordingHeaders = new List<LSRRecordingHeader>();
            LSRRecordingDetails = new List<LSRRecordingDetail>();
            LSRRecordingLists = new List<LSRRecordingList>();
            LSRRecordingCreationList = new List<LSRRecordingCreation>();
            LSRRecordingCreationDetailsList = new List<LSRRecordingCreationDetails>();
            LSRRecordingPartListModelList = new List<LSRRecordingPartListModel>();

            PartListTemp = new List<LSRRecordingPartListTemp>();
        }
    }
}