﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingList
    {
        public String PartNo { set; get; }
        public String PartName { set; get; }
        public DateTime FoundDate { set; get; }
        public String FoundBy { set; get; }
        public int Qty { set; get; }
        public string Reason { set; get; }
        public string Condition { set; get; }
        public string RepairCode { set; get; }
    }
}