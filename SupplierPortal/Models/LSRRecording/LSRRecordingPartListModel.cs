﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingPartListModel
    {
        public String MANIFEST_NO { get; set; }
        //public String PLANT_CD { get; set; }
	    public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
	    public Int32 ORDER_QTY { get; set; }
	    public Int32 RECEIVED_QTY { get; set; }

        public Int32 QTY { get; set; }
	    public Int32 SHORTAGE_QTY { get; set; }
	    public Int32 MISSPART_QTY { get; set; }
	    public Int32 DAMAGE_QTY { get; set; }

        //public String RECEIVED_STATUS { get; set; }
        //public String SUPPLIER_PLANT { get; set; }
        //public String SUPPLIER_DOCK { get; set; }
        //public String KANBAN_PRNT_ADDRESS { get; set; }
        //public String KANBAN_NO { get; set; }
	    public Int32 PCS_PER_CONTAINER { get; set; }
        //public Boolean? DELETION_FLAG { get; set; }
        //public DateTime? DELETION_DT { get; set; }
        //public Boolean? DP_DELIVERY_FLAG { get; set; }
        //public DateTime? DP_DELIVERY_DT { get; set; }
        //public Boolean? DP_DROP_STATUS_FLAG { get; set; }
        //public DateTime? DP_DROP_STATUS_DT { get; set; }
        //public Boolean? DP_RECEIVE_FLAG { get; set; }
        //public DateTime? DP_RECEIVE_DT { get; set; }
        public String AREA_NAME { get; set; }
	    //public DateTime FOUND_DT { get; set; }
        //public String NG_STATUS { get; set; }
        public String CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        //public String CHANGED_BY { get; set; }
        //public DateTime? CHANGED_DT { get; set; }
    }
}