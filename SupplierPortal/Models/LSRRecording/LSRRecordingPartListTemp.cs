﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingPartListTemp
    {
        public Int32 NO_INDEX { get; set; }
        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public String AREA_NAME { get; set; }
        public Int32 QTY { get; set; }
        public String REASON { get; set; }
    }
}