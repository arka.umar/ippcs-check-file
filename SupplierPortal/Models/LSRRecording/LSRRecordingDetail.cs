﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingDetail
    {
        public int No { set; get; }
        public string PartNo { set; get; }
        public string FactoryCode { get; set; }
        public string RepairCode { get; set; }
        public int Qty { get; set; }
        public string Description { get; set; }
        public DateTime ReplacementServiceDate { get; set; }
        public string MovementType { get; set; }
        public string CostCenterCode { get; set; }
        public string PartNotoICS { get; set; }
        public string Source { get; set; }
        public string PartName { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string UniqueNo { get; set; }
        public string DockCode { get; set; }
        public string SupplierPlantCode { get; set; }
        public DateTime DateInputKanban { get; set; }
        public string ModuleType { get; set; }
    }

}