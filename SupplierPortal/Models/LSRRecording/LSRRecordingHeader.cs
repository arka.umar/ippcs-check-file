﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingHeader
    {
        public int No { get; set; }
        public string LSRNo {get;set;}
        public string Line { get; set; }
        public string ShippingRequestNo { get; set; }
        public string Model { get; set; }
        public string Case { get; set; }
        public string PIC { get; set; }
        public DateTime LSRDate { get; set; }
        public string Status { get; set; }
        public string PlantCode { get; set; }
        public string StorageLocation { get; set; }
        public string MatDocNo { get; set; }
        public DateTime PostingDate { get; set; }
        public string Notice { get; set; }
    }
}