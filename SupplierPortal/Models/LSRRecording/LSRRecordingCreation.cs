﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingCreation
    {
        public DateTime LSRDate { set; get; }
        public string Line { set; get; }
        public string ShipingRequestNo { set; get; }
        public string Model { set; get; }
        public string CaseModelNo { set; get; }
        public string Source { set; get; }
        public string Status { set; get; }
        public string IsuedBy { set; get; }
        public DateTime IsuedDate { set; get; }

    }
}