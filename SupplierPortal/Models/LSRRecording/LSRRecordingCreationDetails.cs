﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingCreationDetails
    {
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public string Reason { set; get; }
        public string Condition { set; get; }
        public string RepairCode { set; get; }
    }
}