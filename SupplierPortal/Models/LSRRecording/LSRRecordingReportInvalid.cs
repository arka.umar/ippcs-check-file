﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LSRRecording
{
    public class LSRRecordingReportInvalid
    {
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string ORDER_QTY { get; set; }
        public string RECEIVED_QTY { get; set; }
        public string QTY { get; set; }
        public string SHORTAGE_QTY { get; set; }
        public string MISSPART_QTY { get; set; }
        public string DAMAGE_QTY { get; set; }
        public string PCS_PER_CONTAINER { get; set; }
        public string AREA_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string ERRORS_MESSAGE_CONCAT { get; set; }
    }
}