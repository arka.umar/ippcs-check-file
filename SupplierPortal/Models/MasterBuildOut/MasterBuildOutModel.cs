﻿using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.Models.MasterBuildOut
{
    public class MasterBuildOutModel
    { 
    }

    public class BOList
    {
        public string PART_NO { get; set; }
        public string SUPPLIER { get; set; }
		public string DOCK_CODE { get; set; }
        public string UNIQUE_NO { get; set; }
        public string PART_NAME { get; set; }
        public string QTY_PER_BOX { get; set; }
        public string INACTIVE_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
    }

    public class BOListError
    {
        public string PART_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string DOCK_CODE { get; set; }
        public string KANBAN_NO { get; set; }
        public string QTY_PER_BOX { get; set; }
        public string MESSAGE { get; set; }
    }

    public class partList
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string KANBAN_NO { get; set; }
    }

    public class supplierList
    {
        public string SUPPLIER { get; set; }
        public string SUPPLIER_NAME { get; set; }
    }

    public class dockList
    {
        public string DOCK_CD { get; set; }
        public string DOCK_NAME { get; set; }
    }
}