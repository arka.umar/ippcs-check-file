﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ProblemReport
{
    public class ProblemReportModel
    {
        public List<ProblemReport> ProblemReports { get; set; }
        public List<ProblemReportStatus> ProblemReportStatus { get; set; }

        public ProblemReportModel()
        {
            ProblemReports = new List<ProblemReport>();
            ProblemReportStatus = new List<ProblemReportStatus>(); 
        }

    }
}