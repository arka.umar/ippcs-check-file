﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ProblemReport
{
    public class ProblemReport
    {
        public String PR_CD { get; set; }
        //public String PR_DATE { get; set; }
        public String SUB_MANIFEST_NO { get; set; }
        public String MANIFEST_NO { get; set; }
        public String KANBAN_ID { get; set; }
        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public String PROBLEM_QTY { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT_CD { get; set; }
        public String SUB_SUPPLIER_CD { get; set; }
        public String SUB_SUPPLIER_PLANT_CD { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PROBLEM_DESCRIPTION { get; set; }        
        public String PROBLEM_ORIGIN_CD { get; set; }
        public String STATUS { get; set; }
        /*public String PROBLEM_CATEGORY_CD { get; set; }
        public String PROBLEM_SUB_ORIGIN { get; set; }
        public String DOCK_CD { get; set; }
        public String COST_CENTER { get; set; }
        public String CREATED_BY { get; set; }
        public String CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public String CHANGED_DT { get; set; }
        public String PROBLEM_REPORT_TYPE { get; set; }*/
    }
}
