﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DeliveryPRApproval
{
    public class DeliveryPRMaster
    {
        public string PRNumber { get; set; }
        public string statusSH { get; set; }
        public string statusDpH { get; set; }
        public string statusDH { get; set; }
        public string CurrentPIC { get; set; }
        public string PRDate { get; set; }
        public string Trucking { get; set; }
        public string ReleaseDateSH { get; set; }
        public string ReleaseDateDpH { get; set; }
        public string ReleaseDateDH { get; set; }
        public string Notice { get; set; }
        public string PDF { get; set; }

    }
}