﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DeliveryPRApproval
{
    public class DeliveryPRModel
    {
        public List<DeliveryPRMaster> DeliveryPRApproval { get; set; }
        public DeliveryPRModel()
        {
            DeliveryPRApproval = new List<DeliveryPRMaster>();
        }   
    }
}