﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PartDamageRecording
{
    public class PartDamageRecordingModel
    {
        public List<PartDamageRecordingData> PartDamageRecordingDatas { set; get; }

        public PartDamageRecordingModel() {
            PartDamageRecordingDatas = new List<PartDamageRecordingData>();
        }
    }
}