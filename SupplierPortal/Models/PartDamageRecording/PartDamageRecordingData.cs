﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PartDamageRecording
{
    public class PartDamageRecordingData
    {
        public string PlantName { set; get; }
        public string Line { set; get; }
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
        public string PartReceiveStatus { set; get; }
        public string PartErrorQty { set; get; }
        public string Status { set; get; }
        public string FoundedDate { set; get; }
        public string UniqueNo { set; get; }

    }
}