﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackPart
{
    public class ReportGetAllFeedbackPartModel
    {
        public String D_ID { set; get; }
        public String PACK_MONTH { set; get; }
        public String PART_NO { set; get; }
        public String PART_NAME { set; get; }
        public String VERS { set; get; }
        public String SUPPLIER_CD { set; get; }
        public String S_PLANT_CD { set; get; }
        public String DOCK_CD { set; get; }
        public Int32 N_VOLUME { set; get; }
        public string N_FREQ { get; set; }
        public String N_FA { set; get; }
        public Boolean? N_REPLY { set; get; }
        public String N_REPLY_PLANT_CAP { get; set; }
        public String N_REPLY_RAW_COMP { get; set; }
        public String N_REPLY_RAW_MAT { get; set; }
        public String N_REPLY_QTY_SUPPLY_REFF { get; set; }
        public Int32 N_1_VOLUME { set; get; }
        public string N_1_FREQ { get; set; }
        public String N_1_FA { set; get; }
        public Boolean? N_1_REPLY { set; get; }
        public String N_1_REPLY_PLANT_CAP { get; set; }
        public String N_1_REPLY_RAW_COMP { get; set; }
        public String N_1_REPLY_RAW_MAT { get; set; }
        public String N_1_REPLY_QTY_SUPPLY_REFF { get; set; }
        public Int32 N_2_VOLUME { set; get; }
        public string N_2_FREQ { get; set; }
        public String N_2_FA { set; get; }
        public Boolean? N_2_REPLY { set; get; }
        public String N_2_REPLY_PLANT_CAP { get; set; }
        public String N_2_REPLY_RAW_COMP { get; set; }
        public String N_2_REPLY_RAW_MAT { get; set; }
        public String N_2_REPLY_QTY_SUPPLY_REFF { get; set; }
        public Int32 N_3_VOLUME { set; get; }
        public string N_3_FREQ { get; set; }
        public String N_3_FA { set; get; }
        public Boolean? N_3_REPLY { set; get; }
        public String N_3_REPLY_PLANT_CAP { get; set; }
        public String N_3_REPLY_RAW_COMP { get; set; }
        public String N_3_REPLY_RAW_MAT { get; set; }
        public String N_3_REPLY_QTY_SUPPLY_REFF { get; set; }
    }
}