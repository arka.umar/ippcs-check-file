﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackPart
{
    public class SupplierFeedbackPartParameter
    {
        public String PART_NO { set; get; }
        public String SUPPLIER_CODE { set; get; }
        public String SUPPLIER_PLANT_CD { set; get; }
        public String PRODUCTION_MONTH { set; get; }
        public String VERSION { set; get; }
        public String PARENT { set; get; }
        public String LOCK { set; get; }
    }
}