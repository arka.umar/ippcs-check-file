﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackPart
{
    public class SupplierFeedbackPartModel
    {
        public SupplierFeedbackPartModel()
        {
            SupplierFeedbackParts = new List<SupplierFeedbackPartDetail>();
            
        }

        public List<SupplierFeedbackPartDetail> SupplierFeedbackParts { set; get; }
    }
}