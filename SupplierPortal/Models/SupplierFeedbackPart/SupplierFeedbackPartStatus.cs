﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackPart
{
    public class SupplierFeedbackPartStatus
    {
        public Boolean IS_N_REPLIED { get; set; }
        public Boolean IS_N_1_REPLIED { get; set; }
        public Boolean IS_N_2_REPLIED { get; set; }
        public Boolean IS_N_3_REPLIED { get; set; }

        public String N_STATUS { get; set; }
        public String N_1_STATUS { get; set; }
        public String N_2_STATUS { get; set; }
        public String N_3_STATUS { get; set; }

        public Boolean IS_CONFIRMED { get; set; }
        public Boolean OVERALL_N_STATUS { get; set; }

        public String LOCK { get; set; }
    }
}