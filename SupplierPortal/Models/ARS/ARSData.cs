﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ARS
{
    public class ARSData
    {
      public string PART_NO { set; get; }
      public string PART_NAME { set; get; }
      public string DOCK_CD { set; get; }
      public string SUPPLIER_CD { set; get; }
      public string SUPPLIER_NAME { set; get; }
      public string SUPPLIER_PLANT { set; get; }
      public Int32 PART_SER_NO { set; get; }
      public string KBN_NO { set; get; }
      public string PC_ADDRESS { set; get; }
      public string PC_RACK_CD { set; get; }
      public string PC_RACK_NO { set; get; }
      public string PC_ADDRESS_FINAL { set; get; }
      public string TEMP_ADDRESS { set; get; }
      public string TEMP_RACK_CD { set; get; }
      public string TEMP_RACK_NO { set; get; }
      public string TEMP_ADDRESS_FINAL { set; get; }
      public string SUP_ADDRESS { set; get; }
      public string SUP_RACK_CD { set; get; }
      public string SUP_RACK_NO { set; get; }
      public string SUP_ADDRESS_FINAL { set; get; }
      public string JUN_ADDRESS { set; get; }
      public string JUN_RACK_CD { set; get; }
      public string JUN_RACK_NO { set; get; }
      public string JUN_ADDRESS_FINAL { set; get; }
      public DateTime? CURRENT_DT { set; get; }
      public string SHIFT { set; get; }
      public string CREATED_BY { set; get; }
      public DateTime? CREATED_DT { set; get; }
      public string CHANGED_BY { set; get; }
      public DateTime? CHANGED_DT { set; get; }
    }
}