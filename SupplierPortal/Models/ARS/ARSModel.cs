﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ARS
{
    public class ARSModel
    {
        public List<ARSData> ARSDatas { set; get; }

        public ARSModel()
        {
            ARSDatas = new List<ARSData>();
        }
    }
}