﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderCompare
{
    public class OrderCompare
    {
        public DateTime PRODUCTION_DATE { get; set; }
        public string ARRIVAL_SEQ { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER { get; set; }
        public string PART_NO { get; set; }
        public int IPPCS_ORDER_QTY { get; set; }
        public int ORDER_QTY_KCI { get; set; }
        
    }
}