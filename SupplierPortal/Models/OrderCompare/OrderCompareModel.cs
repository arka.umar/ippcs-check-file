﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderCompare
{
    public class OrderCompareModel
    {
        public List<OrderCompare> OrderCompare { get; set; }
        public OrderCompareModel()
        {
            OrderCompare = new List<OrderCompare>();
        }

    }
}