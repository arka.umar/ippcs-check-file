﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderCompareSummary
{
    public class OrderCompareSummary
    {
        public int countAll { get; set; }
        public int countMatch { get; set; }
        public int countDiff { get; set; }

    }
}