﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GoodsReceiptCancellation
{
    public class GoodsReceiptCancellation
    {
        public string GRSlipNo { set; get; }
        public string GRSlipDateFrom { set; get; }
        public string GRSlipDateTo { set; get; }
        public string HeaderManifestNo { set; get; }

        /*  public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
        public string RouteRad { set; get; }
        public string ManifestNo { set; get; }
        public string OrderNo { set; get; }
        public string Status { set; get; }
        public string DockCode { set; get; }
        public string Po_No { set; get; }
        public string ArrivalTimePlanned { set; get; }
        public string ArrivalTimeActual { set; get; }
        public string Doc_No { set; get; }
        public string GRCreatedTime { set; get; }
        public string GRSlipDocNo { set; get; }
        public string GRSlipCreatedTime { set; get; }
        public double TotalItem{ set; get; }
        public double TotalQuantity { set; get; }
        public string Message { set; get; }
        public string InvoiceNo { set; get; }
        public string InvoiceCreatedTime { set; get; }
        */
        
    }
}