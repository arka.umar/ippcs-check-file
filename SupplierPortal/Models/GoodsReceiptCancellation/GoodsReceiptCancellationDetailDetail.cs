﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GoodsReceiptCancellation
{
    public class GoodsReceiptCancellationDetailDetail
    {


        public string ManifestNo { set; get; }
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
        public long OrderQty { set; get; }
        public long ReceivedQty { set; get; }
        public int RevisedQty { set; get; }
    }
}