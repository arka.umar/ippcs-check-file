﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.GoodsReceiptCancellation
{
    public class GoodsReceiptCancellationModel
        
    {
        public List<GoodsReceiptCancellationDetail> GoodsReceiptCancellationDetails { set; get; }
        public List<GoodsReceiptCancellation> GoodsReceiptCancellations { set; get; }
        public List<GoodsReceiptCancellationDetailDetail> GoodsReceiptCancellationDetailDetails { set; get; }

        public GoodsReceiptCancellationModel()
        { 
           GoodsReceiptCancellationDetails = new List<GoodsReceiptCancellationDetail>();
           GoodsReceiptCancellations = new List<GoodsReceiptCancellation>();
           GoodsReceiptCancellationDetailDetails = new List<GoodsReceiptCancellationDetailDetail>();
        }
    }
}

