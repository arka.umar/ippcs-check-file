﻿using System;

namespace Portal.Models.UserMaintenance 
{
    public class CompanyData
    {
        public string COMPANY_ID { get; set; }
        public string COMPANY_NAME { get; set; }
    
    }

    public class BranchData
    {
        public string BRANCH_ID { get; set; }
        public string BRANCH_NAME { get; set; }
    }


    public class ClassData
    {
        public string CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }
    }

    public class LocationData
    {
        public string LOCATION_ID {get; set;}
        public string LOCATION_NAME {get; set;}
    }

    public class DefaultAppData
    {
        public string SYSTEM_NAME {get; set; }
    }

    public class JobFunctionData
    {
        public string JOB_FUNCTION_ID { get; set; }
        public string JOB_FUNCTION_NAME { get; set; }
    }

    public class UMErrorListDownload
    { 
        public string BRANCH_ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string PASSWORD_EXPIRATION_DATE { get; set; }
        public string USER_EXPIRATION_DATE { get; set; }
        public string ACTIVE_DIRECTORY_FLAG { get; set; }
        public string LOCK_FLAG { get; set; }
        public string ISACTIVE_FLAG { get; set; }
        public string NOTIFICATION_FLAG { get; set; }
        public string NO_REG { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string TEL_NO { get; set; }
        public string MOBILE_NO { get; set; }
        public string CLASS_ID { get; set; }
        public string JOB_FUNCTION_ID { get; set; }
        public string CC_CODE { get; set; }
        public string COMPANY_ID { get; set; }
        public string LOCATION_ID { get; set; } 
        public string ISPRODUCTION { get; set; }
        public string MAX_LOGIN { get; set; } 
        public string QUOTA { get; set; }
        public string DELETED_FLAG { get; set; }
        public string DEFAULT_APP { get; set; }
        public string EMAIL { get; set; }
        public string MULTIPLE_LOGIN { get; set; }
        public string PASSWORD_MUST_BE_CHANGED { get; set; }
        public string MSGG { get; set; }
        

    
    }

}
