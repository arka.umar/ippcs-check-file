﻿using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.Models.UserMaintenance
{ 
	public class UserMaintenanceModel
    {
		public List<UserMaintenanceData> UserMaintenanceDatas {get; set;}
        
        //public List<UserMaintenanceAuthData> UserMaintenanceAuthDatas { get; set; }
        public List<CompanyData> CompanyDatas { get; set; }

		public UserMaintenanceModel()
        {
			UserMaintenanceDatas = new List<UserMaintenanceData>();
           // UserMaintenanceAuthDatas = new List<UserMaintenanceAuthData>();
            CompanyDatas = new List<CompanyData>();
		}
    
    }
}