﻿using System;


namespace Portal.Models.UserMaintenance
{
    public class UserMaintenanceData { 
        
        public int BRANCH_ID { get; set;}
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public DateTime PASSWORD_EXPIRATION_DATE { get; set; }
        public DateTime USER_EXPIRATION_DATE { get; set; }
        public int ACTIVE_DIRECTORY_FLAG { get; set; }
        public int LOCK_FLAG { get; set; }
        public int ISACTIVE_FLAG { get; set; }
        public int NOTIFICATION_FLAG { get; set; }
        public string NO_REG { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string FULL_NAME { get; set; }
        public string TEL_NO { get; set; }
        public string MOBILE_NO { get; set; }
        public string CLASS_ID { get; set; }
        public int JOB_FUNCTION_ID { get; set; }
        public string CC_CODE { get; set; }
        public int COMPANY_ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ISPRODUCTION { get; set; }
        public int MAX_LOGIN { get; set; }
        public int QUOTA { get; set; }
        public int DELETED_FLAG { get; set; }
        public string DEFAULT_APP { get; set; }
        public int ID { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public DateTime CHANGED_DATE { get; set; }
        public String CHANGED_BY { get; set; }
        public String COMPANY_NAME { get; set; }

        public string EMAIL { get; set; }
        public DateTime LAST_LOGON { get; set; }
        public int MULTIPLE_LOGIN { get; set; }
        public int PASSWORD_MUST_BE_CHANGED { get; set; }



        
    
    }



    public class UserMaintenanceDataTemp
    {
        public string BRANCH_ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string PASSWORD_EXPIRATION_DATE { get; set; }
        public string USER_EXPIRATION_DATE { get; set; }
        public string ACTIVE_DIRECTORY_FLAG { get; set; }
        public string LOCK_FLAG { get; set; }
        public string ISACTIVE_FLAG { get; set; }
        public string NOTIFICATION_FLAG { get; set; }
        public string NO_REG { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string TEL_NO { get; set; }
        public string MOBILE_NO { get; set; }
        public string CLASS_ID { get; set; }
        public string JOB_FUNCTION_ID { get; set; }
        public string CC_CODE { get; set; }
        public string COMPANY_ID { get; set; }
        public string LOCATION_ID { get; set; }
        public string ISPRODUCTION { get; set; }
        public string MAX_LOGIN { get; set; }
        public string QUOTA { get; set; }
        public string DELETED_FLAG { get; set; }
        public string DEFAULT_APP { get; set; }
        public string ID { get; set; }
        public string COMPANY_NAME { get; set; }
        public string EMAIL { get; set; }
        public string MULTIPLE_LOGIN { get; set; }
        public string PASSWORD_MUST_BE_CHANGED { get; set; }
    }


}