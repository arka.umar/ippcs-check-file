﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderPlan
{
    public class OrderPlanModel
    {
        public List<OrderPlanData> orderPlanDatas { get; set; }
        public OrderPlanModel()
        {
            orderPlanDatas = new List<OrderPlanData>();
        }

    }
}