﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Dock
{
    public class DockData
    {
        public bool Check { set; get; }
        public string PlantCD { set; get; }
        public string DockCode { set; get; }
        public string DockName { set; get; }
        public int STATUS_DOCK { set; get; }

        public String DOCK_CD { set; get; }
        public String PLANT_CD { set; get; }
        public String DOCK_PLANT_CD { set; get; }
        public String DOCK_NM { set; get; }
        public String PLANT_NM { set; get; }
        public String EMAIL_TO { get; set; }
        public String EMAIL_CC { get; set; }
        public string Email { get; set; }
        public string Emailcc { get; set; }
    }
}