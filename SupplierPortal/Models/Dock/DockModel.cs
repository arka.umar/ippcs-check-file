﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Dock
{
    public class DockModel
    {
        public List<DockData> DocksData { set; get; }
        public List<DockData> DockDbModel { set; get; }
        public List<PlantDockDats> PlantsData { set; get; }
        public List<DockData> Emailmodel { get; set; }
        public List<DockData> Emailcc { get; set; }

        public DockModel()
        {
            DocksData = new List<DockData>();
            DockDbModel = new List<DockData>();
            PlantsData = new List<PlantDockDats>();
            Emailmodel = new List<DockData>();
            Emailcc = new List<DockData>();
        }
    }
}