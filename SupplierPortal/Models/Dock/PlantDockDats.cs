﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Dock
{
    public class PlantDockDats
    {
        public bool Checking { set; get; }
        public string DockCode { get; set; }
        public string DockName { get; set; }
        public string PlantCode { set; get; }
        public string PlantName { set; get; }
        public string STATUS_DOCK { set; get; }
    }
}