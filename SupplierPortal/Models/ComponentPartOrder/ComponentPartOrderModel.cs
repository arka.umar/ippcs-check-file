﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ComponentPartOrder
{
    public class ComponentPartOrderModel
    {
        public ComponentPartOrderModel()
        {
            CPOHeaders = new List<ComponentPartOrderHeader>();
            CPODetails = new List<ComponentPartOrderDetail>();
        }
        public List<ComponentPartOrderHeader> CPOHeaders { set; get; }
        public List<ComponentPartOrderDetail> CPODetails { set; get; }
    }
}