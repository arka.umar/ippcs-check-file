﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ComponentPartOrder
{
    public class ComponentPartOrderHeader
    {
        public string PRNo { set; get; }
        public string ItemPR { set; get; }
        public string PartNo { set; get; }
        public string PartDesc { set; get; }
        public int Qty { set; get; }
        public int RemainQty { set; get; }
        public string UOM { set; get; }
        public DateTime DeliveredDate { set; get; }
        public string PlantCode { set; get; }
        public string sLockCode { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierSearchTerm { set; get; }
        public double Price { set; get; }
        public string ProdPurpose { set; get; }
        public string SourceType { set; get; }
        public string PackingType { set; get; }
        public string SuffixColor { set; get; }
        public DateTime PRCreationDate { set; get; }
        public string Requistioner { set; get; }
        public string POItemNo { set; get; }
        public DateTime POItemCreationDate { set; get; }
        public string PurchaseOrder { set; get; }
        public string PONo { set; get; }
        public Boolean DeletionFlag { set; get; }

        public string PRType { set; get; }
        public string PlanDeliveryDate { set; get; }
        public string RcvPlantCode { set; get; }
        public string DockCode { set; get; }
        public string Status { set; get; }
        public string Notice { set; get; }
        
    }
}