﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ComponentPartOrder
{
    public class ComponentPartOrderDetail
    {
        public string PRNo { set; get; }
        public string ItemNo { set; get; }
        public string PartNo { set; get; }
        public string MatNo { set; get; }
        public string ProdPurpose { set; get; }
        public string SourceType { set; get; }
        public string UOM { set; get; }
        public string MatDescription { set; get; }
        public string PartName { set; get; }
        public int OrderQty { set; get; }
        public string SpecialProcurementType { set; get; }
        public string PackingType { set; get; }
        public string Suffix { set; get; }
    }
}