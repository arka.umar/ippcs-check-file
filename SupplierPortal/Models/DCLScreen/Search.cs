﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLScreen
{
    public class Search
    {
        public DateTime TermFrom { set; get; }
        public DateTime TermTo { set; get; }
    }
}