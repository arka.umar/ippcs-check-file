﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLScreen
{
    public class DCLScreenMaster
    {
        public DateTime TermFrom { set; get; }
        public DateTime TermTo { set; get; }
        public DateTime date { get; set; }
        public string Route { get; set; }
        public string Rate { get; set; }
        public string TYPE { get; set; }
        public string LPCode { get; set; }
        public string DRIVER_NAME { get; set; }
        //public string STATUS { get; set; }
        public string DownloadBy { get; set; }
        public DateTime DownloadTime { get; set; }
        public string TRUCK_NO { get; set; }
        public string SKID_NO { get; set; }
        public string Download { get; set; }
        public string Capability { get; set; }
       
    }
}