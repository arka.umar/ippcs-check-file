﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLScreen
{
    public class DCLScreenDetail
    {
        public string PickUpDate { get; set; }
        public string Company { get; set; }
        public string DriverName { get; set; }
        public string RouteName { get; set; }
        public string TripNo { get; set; }
        public string Type { get; set; }
        public string Barcode { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string ArrivalPlan { get; set; }
        public string ArrivalActual { get; set; }
        public string DeparturePlan { get; set; }
        public string DepartureActual { get; set; }
        public string OrderNo { get; set; }
        public string Check { get; set; }
        public string DockCode { get; set; }
        public string Skid { get; set; }
       
    }
}