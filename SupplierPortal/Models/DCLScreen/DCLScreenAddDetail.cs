﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLScreen
{
    public class DCLScreenAddDetail
    {
        public string AddPickUpDate { get; set; }
        public string AddCompany { get; set; }
        public string AddDriverName { get; set; }
        public string AddRouteName { get; set; }
        public string AddTripNo { get; set; }
        public string AddType { get; set; }
        public string AddBarcode { get; set; }
        public string AddSuppCode { get; set; }
        public string AddSuppName { get; set; }
        public string AddArrivalPlan { get; set; }
        public string AddArrivalActual { get; set; }
        public string AddDeparturePlan { get; set; }
        public string AddDepartureActual { get; set; }
        public string AddOrderNo { get; set; }
        public string AddCheck { get; set; }
        public string AddDockCode { get; set; }
        public string AddSkid { get; set; }
       
    }
}