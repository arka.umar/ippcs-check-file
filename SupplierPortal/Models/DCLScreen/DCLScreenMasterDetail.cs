﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLScreen
{
    public class DCLScreenMasterDetail
    {
        public DateTime date { get; set; }
        public string Route { get; set; }
        public int rate { get; set; }
        public string TYPE { get; set; }
        public string LPCode { get; set; }
        public string DRIVER_NAME { get; set; }
        public string Barcode { get; set; }
        public string STATUS { get; set; }
        public string TRUCK_NO { get; set; }
        public string SKID_NO { get; set; }
        public string SUPPCD { get; set; }
        public string SUPPPLANTCD { get; set; }
        public string RCVCOMPDOCKCD { get; set; }
        public string ORDDATE { get; set; }
        public string ORDSEQ { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string ARRVDATETIME { get; set; }
        public string DPTDATETIME { get; set; }
    }
}