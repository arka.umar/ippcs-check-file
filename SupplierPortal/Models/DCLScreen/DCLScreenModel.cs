﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLScreen
{
    public class DCLScreenModel
    {
       
        public List<DCLScreenDetail> DCLScreenDetails { get; set; }
        
        public List<DCLScreenAddDetail> DCLScreenAddDetails { get; set; }
        public List<DCLScreenMaster> DCLScreenMain { get; set; }
        public List<DCLScreenAddMaster> DCLScreenAddMain { get; set; }


        public List<DCLScreenMasterDetail> DCLScreenMainDetail { get; set; }
        
        public List<DCLScreenCombo> Combo { get; set; }
        public DCLScreenModel()
        {
          
            DCLScreenDetails = new List<DCLScreenDetail>();
            
            DCLScreenAddDetails = new List<DCLScreenAddDetail>();
            DCLScreenMain = new List<DCLScreenMaster>();
            DCLScreenAddMain = new List<DCLScreenAddMaster>();
            DCLScreenMainDetail = new List<DCLScreenMasterDetail>();

            Combo = new List<DCLScreenCombo>();
        }
    }
}