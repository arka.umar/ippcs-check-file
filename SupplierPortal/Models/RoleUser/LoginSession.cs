﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;

namespace Portal.Models.RoleUser
{
    public class LoginSession : SelectableDataModel
    {
        public string Username { set; get; }
        public string SessionID { set; get; }
        public DateTime LoginTime { set; get; }
        public string IPAddress { set; get; }
        public string Browser { set; get; }

        public override string SelectedKey
        {
            get
            {
                return "Username;";
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Convert.ToString(Username) + ";";
            }
        }
    }
}