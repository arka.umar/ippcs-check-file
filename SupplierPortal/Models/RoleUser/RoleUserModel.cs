﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.Reporting;

namespace Portal.Models.RoleUser
{
    public class RoleUserModel
    {
        public List<LoginSession> GetLoginSessions { set; get; }

        public DataTable GetLoginSession { set; get; }

        public RoleUserModel()
        {
            GetLoginSessions = new List<LoginSession>();
        }
    }
}