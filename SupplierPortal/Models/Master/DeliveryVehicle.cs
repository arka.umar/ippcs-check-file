﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
  
namespace Portal.Models.Master
{
    public class DeliveryVehicleModel
    {
        public List<DeliveryVehicle> DeliveryVehicle { get; set; }
        public List<GridLP> GridLP { get; set; }

        public DeliveryVehicleModel()
        {
            DeliveryVehicle = new List<DeliveryVehicle>();
            GridLP = new List<GridLP>();
        }

    }

    public class DeliveryVehicle
    {
        public string LP_CD { set; get; }
        public string LP_NAME { set; get; }
        public string VEHICLE_ID { set; get; }
        public string VEHICLE_NO { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime CHANGED_DT { set; get; }
    }

    public class GridLP
    {
        public string LOG_PARTNER_CD{ set; get; }
        public string LOG_PARTNER_NAME { set; get; }
    }

    public class CheckCount
    {
        public int count { get; set; }
    }
}