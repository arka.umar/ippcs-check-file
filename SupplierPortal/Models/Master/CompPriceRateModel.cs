﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Master
{
    public class CompPriceRateModel
    {
        public List<MasterCompPriceRate> MasterCompPricePub { get; set; }
        public CompPriceRateModel()
        {
            MasterCompPricePub = new List<MasterCompPriceRate>();
        }
    }
    public class MasterCompPriceRate
    {
        public string COMP_PRICE_CD { get; set; }
        public string CONDITION_RULE { get; set; }
        public decimal COMP_PRICE { get; set; }
        public string TAX_CD { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
    }

    public class MasterCompPrice
    {
        public string COMP_PRICE_CD { get; set; }
        public string COMP_PRICE_DESC { get; set; }
    }

    public class MasterTaxComboList
    { 
        public string TAX_CD { get; set; }
        public string TAX_NAME { get; set; }
    }

}