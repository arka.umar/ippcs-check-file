﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
  
namespace Portal.Models.Master
{
    public class DeliveryDriverModel
    {
        public List<DeliveryDriver> DeliveryDriver { get; set; }
        public List<GLTrucking> GLTrucking { get; set; }

        public DeliveryDriverModel()
        {
            DeliveryDriver = new List<DeliveryDriver>();
            GLTrucking = new List<GLTrucking>();
        }
    }

        public class DeliveryDriver
        {
            public string LP_CD { set; get; }
            public string LP_NAME { set; get; }
            public string DRIVER_ID { set; get; }
            public string DRIVER_NAME { set; get; }
            public string CREATED_BY { set; get; }
            public DateTime CREATED_DT { set; get; }
            public string CHANGED_BY { set; get; }
            public DateTime CHANGED_DT { set; get; }
        }

        public class GLTrucking
        {
            public string LOG_PARTNER_CD { set; get; }
            public string LOG_PARTNER_NAME { set; get; }
        }

        public class CountTruck
        {
            public int CountLP { get; set; }
        }
}
