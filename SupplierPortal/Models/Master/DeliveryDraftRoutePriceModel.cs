﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Master
{
    public class DeliveryDraftRoutePriceModel
    {
        public List<DeliveryDraftRoutePriceData> DeliveryDraftRoutePriceListData { get; set; }

        public List<DeliveryDraftRoutePriceDataRoute> DeliveryDraftRoutePriceListRoute { get; set; }

        public List<DeliveryDraftRoutePriceDataTrucking> DeliveryDraftRoutePriceListTrucking { get; set; }

        public DeliveryDraftRoutePriceModel()
        {

            DeliveryDraftRoutePriceListData = new List<DeliveryDraftRoutePriceData>();

            DeliveryDraftRoutePriceListRoute = new List<DeliveryDraftRoutePriceDataRoute>();

            DeliveryDraftRoutePriceListTrucking = new List<DeliveryDraftRoutePriceDataTrucking>();
        }
    }
}