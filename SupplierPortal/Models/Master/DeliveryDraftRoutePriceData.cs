﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Master
{
    public class DeliveryDraftRoutePriceData
    {
        public string DATA_KEY { get; set; }
        public string CHILD_ROUTE { get; set; }
        public string PARENT_ROUTE { get; set; }
        public Decimal PARENT_ROUTE_PRICE { get; set; }
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        public string PROPOSED_BY { get; set; }
        public DateTime PROPOSED_DT { get; set; }
        public string APPROVED_BY { get; set; }
        public DateTime APPROVED_DT { get; set; }
        public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public string VALID_FROM_DATE { get; set; }
    }

    public class DeliveryDraftRoutePriceDataRoute
    {

        public string ROUTE_CD { get; set; }
        public string ROUTE_NAME { get; set; }

    }

    public class DeliveryDraftRoutePriceDataTrucking
    {
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }

    }
}