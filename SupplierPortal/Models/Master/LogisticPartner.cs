﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
  
namespace Portal.Models.Master
{
    public class LogisticPartnerModel
    { 
        public List<LogisticPartner> LogisticPartner { get; set; }
        public List<PayMethod> PayMethod { get; set; }
        public List<PayTerm> PayTerm { get; set; }

        public LogisticPartnerModel()
        {
            LogisticPartner = new List<LogisticPartner>();
            PayMethod = new List<PayMethod>();
            PayTerm = new List<PayTerm>();
        } 
    }
    
    public class LogisticPartner
    {
        public string LOG_PARTNER_CD { set; get; }
        public string LOG_PARTNER_NAME { set; get; }
        public string ACTIVE_FLAG { set; get; }
        public string LOG_PARTNER_ADDRESS { set; get; }
        public string CITY { set; get; }
        public string POSTAL_CD { set; get; }
        public string PHONE_NUMBER { set; get; }
        public string FAX_NUMBER { set; get; }
        public string CREATED_BY { get; set; }
        public string INVALID_SYNCH_FLAG { get; set; }
        public string NPWP { set; get; }
        public string PAYMENT_METHOD_CD { set; get; }
        public string PAYMENT_TERM_CD { set; get; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public string USER_POSITION { get; set; }
    }

    public class CheckResult
    {
        public int result { get; set; }
    }

    public class PayMethod
    {
        public string PAY_METHOD_CD { set; get; }
        public string PAY_METHOD_NAME { set; get; }
    }

    public class PayTerm
    {
        public string PAY_TERM_CD { set; get; }
        public string PAY_TERM_NAME { set; get; }
    }

    public class UserPosition
    {
        public string USER_POSITION { get; set; }
    }
     
}