﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Master
{
    public class Priority
    {
        public byte Level { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
    }
}