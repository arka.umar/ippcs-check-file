﻿using System;


namespace Portal.Models.AuthorizationMapping
{
    public class AuthorizationData
    {
        public int BRANCH_ID { get; set; }
        public int SYSTEM_ID { get; set; }
        public string SYSTEM_NAME { get; set; }
        public string USERNAME { get; set; }
        public int ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DATE { get; set; }
    }

    public class UploadAM
    {
        public string USERNAME { get; set; }
        public string ROLE_ID { get; set; }
    
    }

}