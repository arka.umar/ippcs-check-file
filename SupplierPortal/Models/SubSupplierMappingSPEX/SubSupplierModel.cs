﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SubSupplierMappingSPEX
{
    public class SubSupplierModel
    {
        public string SUPP_CD { get; set; }
        public string SUPP_PLANT { get; set; }
        public string SUPP_PLANT_CD { get; set; }
        public string SUPP_NAME { get; set; }
        public string SUB_SUPP_CD { get; set; }
        public string SUB_SUPP_PLANT { get; set; }
        public string SUB_SUPP_PLANT_CD { get; set; }
        public string SUB_SUPP_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
    }
}