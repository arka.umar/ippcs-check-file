﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SubSupplierMappingSPEX
{
    public class SubSupplierMappingModel
    {
        public List<SubSupplierModel> SubSupplierData { get; set; }
        public SubSupplierMappingModel()
        {
            SubSupplierData = new List<SubSupplierModel>();
        }

    }
}