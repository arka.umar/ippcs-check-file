﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.MasterDataCompletenessCheck
{
    public class MDCompletenessCheckData
    {
        public string RecordId { set; get; }
        public string PoNo { set; get; }
        public string PoItemNo { set; get; }
        public string DockCd { set; get; }
        public string SuppCd { set; get; }
        public string MatNo { set; get; }
        public string ProdPurposeCd { set; get; }
        public string SourceType { set; get; }
        public string PartColorSfx { set; get; }
        public string MaterialDesc { set; get; }
        public string SupplierCd { set; get; }
        public string ProdMonth { set; get; }
        public string MatMast { set; get; }
        public string MastDock { set; get; }
        public string MatDockMap { set; get; }
        public int QtyN { set; get; }
        public int QtyN1 { set; get; }
        public int QtyN2 { set; get; }
        public int QtyN3 { set; get; }
        public string PlantCd { set; get; }
        public string SlocCd { set; get; }
        public string PaymentMethodCd { set; get; }
        public string PaymentTermCd { set; get; }
        public string SpecialProcurementTypeCd { set; get; }
        public string UnitOfMeasureCd { set; get; }
        public string PackingType { set; get; }
        public string System { set; get; }
        public string MatPrice { set; get; }
        public string PoCurr { set; get; }
        public string ValStatus { set; get; }
        public string Message { set; get; }
    }

    //---=== Add by Rahmat (Arkamaya)
    public class MDCompletenessCheckListData
    {
        public int ROW_ID {get; set;}
	    public string PRODUCTION_MONTH {get; set;}
	    public string PART_NO {get; set;}
	    public string PART_NAME {get; set;}
	    public int ORDER_QTY {get; set;}
	    public int DAMAGE_QTY {get; set;}
	    public int MISSPART_QTY {get; set;}
	    public int SHORTAGE_QTY {get; set;}
	    public string FD_EXCHANGE_RATE {get; set;}
	    public string FD_IM_PERIOD {get; set;}
	    public string FD_STANDARD_COST {get; set;}
	    public string PCD_DOCK_CODE {get; set;}
	    public string PCD_MATERIAL_MASTER {get; set;}
	    public string PCD_ROUTING {get; set;}
	    public string PCD_PART_LIST {get; set;}
	    public string PCD_OTHERS {get; set;}
	    public string PAD_DOCK_CODE {get; set;}
	    public string PAD_MATERIAL_MASTER {get; set;}
	    public string PAD_MATDOCK {get; set;}
	    public string PAD_LOCATION_CODE {get; set;}
	    public string PAD_OTHERS {get; set;}
	    public string PUD_MATERIAL_PRICE {get; set;}
	    public string PUD_STANDARD_PRICE {get; set;}
	    public string PUD_PO {get; set;}
	    public string PUD_SOURCE_LIST {get; set;}
	    public string PUD_SUPPLIER_MASTER {get; set;}
	    public string PUD_OTHERS {get; set;}
	    public string PVD_MATERIAL_PRICE {get; set;}
	    public string PVD_PACKING_TYPE {get; set;}
	    public string PVD_OTHERS {get; set;}
	    public string ISTD_SYSTEM {get; set;}
	    public string ISTD_OTHERS {get; set;}
    }

}