﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.MasterDataCompletenessCheck
{
    public class MasterDataCompletenessCheckModel
    {
        public List<MDCompletenessCheckData> MDCompletenessCheckDatas { set; get; }
        public List<MDCompletenessCheckListData> MDCompletenessCheckListData { set; get; }

        public MasterDataCompletenessCheckModel()
        {
            MDCompletenessCheckDatas = new List<MDCompletenessCheckData>();
            MDCompletenessCheckListData = new List<MDCompletenessCheckListData>();
        }
    }
}