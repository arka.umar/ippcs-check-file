﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models.DCLReceiving
{
    public class DCLReceivingData
    {
        /*
        public string No { get; set; }
        public string Station { get; set; }
        public DateTime RouteDate { get; set; }
        public string ArrivalPlan { get; set; }
        public string ActualArrival { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }
        public string Date { get; set; }
        public string Dock { get; set; }
        public string RouteName { get; set; }
        public string TripNo { get; set; }
        public string ArrivalTimeStatus { get; set; }
        public string ArrivalStatus { get; set; }
        public string GapArr { get; set; }
        public string DeparturePlan { get; set; }
        public string ActualDeparture { get; set; }
        public string DepartureStatus { get; set; }
        public string GapDep { get; set; }
         */

        public string No { get; set; }
        public DateTime RouteDate { get; set; }
        public string Dock { get; set; }
        public string Station { get; set; }
        public string RouteName { get; set; }
        public string TripNo { get; set; }
        public string ArrivalPlan { get; set; }
        public string ActualArrival { get; set; }
        public string Status { get; set; }
        public string GAPARRV { get; set; }
        public string DeparturePlan { get; set; }
        public string ActualDeparture { get; set; }
        public string StatusDept { get; set; }
        public string GAPDEPT { get; set; }
        public string LPNAME { get; set; }
        public string Notif { get; set; }
        public int Months { get; set; }

        //[Required] -->
        //[Display(Name = "Delivery No")]
        //public string No { get; set; }
    }
}