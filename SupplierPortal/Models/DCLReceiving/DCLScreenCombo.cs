﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLReceiving
{
    public class DCLScreenCombo
    {
    
        public string LPCode { get; set; }
        public string LPName { get; set; }
        public string Route { get; set; }
        public string TripNo { get; set; }
        public string OrderNo { get; set; }
        public string SuppCode { get; set; }
        public string Reason { get; set; }
    }
}