﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLReceiving
{
    public class DCLReceivingModel
    {
        public List<DCLReceivingData> DCLReceivings { get; set; }
        public List<DCLScreenCombo> Combo { get; set; }
        public List<DCLReceivingDb> DCLReceivingGet { get; set; }

        public DCLReceivingModel()
        {
            DCLReceivings = new List<DCLReceivingData>();
            Combo = new List<DCLScreenCombo>();
            DCLReceivingGet = new List<DCLReceivingDb>();
        }
    }
}