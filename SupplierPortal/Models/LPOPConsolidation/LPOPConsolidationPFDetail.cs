﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConsolidation
{
    public class LPOPConsolidationPFDetail
    {
        public string D_ID{ get; set; }
        public string Vers { get; set; }
        public string Rev_No { get; set; }
        public string Comp_Code { get; set; }
        public string R_Plant_Code { get; set; }
        public string Dock_Code { get; set; }
        public string Supp_Code { get; set; }
        public string Ship_Code { get; set; }
        public string Ord_Type { get; set; }
        public string Pack_Month { get; set; }
        public string Car_Code { get; set; }
        public string Rex_Code { get; set; }
        public string Src  { get; set; }
        public string Part_No { get; set; }
        public string Order_Type { get; set; }
        public string Order_Lot_Cz { get; set; }
        public string Kanban_Number { get; set; }
        public string Parts_Matching_Key { get; set; }
        public string N_Volume { get; set; }
        public string AICO_Cept { get; set; }
        public string N1_Vol { get; set; }
        public string AICO_Cept1 { get; set; }
        public string N2_Vol { get; set; }
        public string AICO_Cept2 { get; set; }
        public string N3_Vol { get; set; }
        public string AICO_Cept3 { get; set; }
        public string N_D01 { get; set; }														
        public string N_D02 { get; set; }													
        public string N_D03 { get; set; }														
        public string N_D04 { get; set; }														
        public string N_D05 { get; set; }														
        public string N_D06 { get; set; }														
        public string N_D07 { get; set; }														
        public string N_D08 { get; set; }														
        public string N_D09 { get; set; }														
        public string N_D10 { get; set; }														
        public string N_D11 { get; set; }														
        public string N_D12 { get; set; }														
        public string N_D13 { get; set; }														
        public string N_D14 { get; set; }														
        public string N_D15 { get; set; }														
        public string N_D16 { get; set; }														
        public string N_D17 { get; set; }														
        public string N_D18 { get; set; }														
        public string N_D19 { get; set; }														
        public string N_D20 { get; set; }														
        public string N_D21 { get; set; }														
        public string N_D22 { get; set; }														
        public string N_D23 { get; set; }														
        public string N_D24 { get; set; }														
        public string N_D25 { get; set; }														
        public string N_D26 { get; set; }														
        public string N_D27 { get; set; }														
        public string N_D28 { get; set; }														
        public string N_D29 { get; set; }														
        public string N_D30 { get; set; }														
        public string N_D31 { get; set; }														
        public string N1_D01 { get; set; }														
        public string N1_D02 { get; set; }												
        public string N1_D03 { get; set; }												
        public string N1_D04 { get; set; }													
        public string N1_D05	{ get; set; }													
        public string N1_D06	{ get; set; }												
        public string N1_D07	{ get; set; }												
        public string N1_D08	 { get; set; }												
        public string N1_D09	 { get; set; }													
        public string N1_D10	 { get; set; }													
        public string N1_D11	 { get; set; }													
        public string N1_D12	 { get; set; }													
        public string N1_D13	 { get; set; }													
        public string N1_D14	{ get; set; }												
        public string N1_D15	 { get; set; }													
        public string N1_D16	{ get; set; }												
        public string N1_D17	{ get; set; }													
        public string N1_D18	 { get; set; }													
        public string N1_D19	 { get; set; }													
        public string N1_D20	 { get; set; }													
        public string N1_D21	 { get; set; }													
        public string N1_D22	 { get; set; }													
        public string N1_D23	 { get; set; }													
        public string N1_D24 { get; set; }												
        public string N1_D25 { get; set; }														
        public string N1_D26 { get; set; }														
        public string N1_D27 { get; set; }														
        public string N1_D28 { get; set; }														
        public string N1_D29 { get; set; }														
        public string N1_D30 { get; set; }
	    public string N1_D31 { get; set; }											
        public string System_Date	{ get; set; }													
        public string Series { get; set; }														
        public string Life_Cycle { get; set; }														
        public string BA_Sign { get; set; }														
        public string BA_Quantity { get; set; }
        public string RU_Previous { get; set; }
        public string RU_Present { get; set; }									
        public string Supplier_Name{ get; set; }
        public string Part_Name { get; set; }
        public string Created_By { get; set; }														
        public string Created_Date { get; set; }													
        public string Changed_By { get; set; }
        public string Changed_Date { get; set; }
    }
}