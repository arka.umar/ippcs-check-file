﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConsolidation
{
    public class LPOPConsilidation
    {
        public string D_ID {get;set;}
        public string VERS {get;set;}
        public string REV_NO {get;set;}
        public string COMP_CD {get;set;}
        public string R_PLANT_CD {get;set;}
        public string DOCK_CD {get;set;}
        public string S_PLANT_CD {get;set;}
        public string SHIP_CD {get;set;}
        public string ORD_TYPE {get;set;}
        public string PACK_MONTH {get;set;}
        public string CAR_CD {get;set;}
        public string REC_CD {get;set;}
        public string SRC {get;set;}
        public string PART_NO {get;set;}
        public string ORDER_TYPE {get;set;}
        public string ORDER_LOT_SZ {get;set;}
        public string KANBAN_NUMBER {get;set;}
        public string N_VOLUME {get;set;}
        public string AICO_CEPT {get;set;}
        public string N_1_VOLUME {get;set;}
        public string AICO_CEPT1 {get;set;}
        public string N_2_VOLUME {get;set;}
        public string AICO_CEPT2 {get;set;}
        public string N_3_VOLUME {get;set;}
        public string AICO_CEPT3 {get;set;}
        public string N_D01 {get;set;}
        public string N_D02 {get;set;}
        public string N_D03 {get;set;}
        public string N_D04 {get;set;}
        public string N_D05 {get;set;}
        public string N_D06 {get;set;}
        public string N_D07 {get;set;}
        public string N_D08 {get;set;}
        public string N_D09 {get;set;}
        public string N_D10 {get;set;}
        public string N_D11 {get;set;}
        public string N_D12 {get;set;}
        public string N_D13 {get;set;}
        public string N_D14 {get;set;}
        public string N_D15 {get;set;}
        public string N_D16 {get;set;}
        public string N_D17 {get;set;}
        public string N_D18 {get;set;}
        public string N_D19 {get;set;}
        public string N_D20 {get;set;}
        public string N_D21 {get;set;}
        public string N_D22 {get;set;}
        public string N_D23 {get;set;}
        public string N_D24 {get;set;}
        public string N_D25 {get;set;}
        public string N_D26 {get;set;}
        public string N_D27 {get;set;}
        public string N_D28 {get;set;}
        public string N_D29 {get;set;}
        public string N_D30 {get;set;}
        public string N_D31 {get;set;}
        public string N_1_D01 {get;set;}
        public string N_1_D02 {get;set;}
        public string N_1_D03 {get;set;}
        public string N_1_D04 {get;set;}
        public string N_1_D05 {get;set;}
        public string N_1_D06 {get;set;}
        public string N_1_D07 {get;set;}
        public string N_1_D08 {get;set;}
        public string N_1_D09 {get;set;}
        public string N_1_D10 {get;set;}
        public string N_1_D11 {get;set;}
        public string N_1_D12 {get;set;}
        public string N_1_D13 {get;set;}
        public string N_1_D14 {get;set;}
        public string N_1_D15 {get;set;}
        public string N_1_D16 {get;set;}
        public string N_1_D17 {get;set;}
        public string N_1_D18 {get;set;}
        public string N_1_D19 {get;set;}
        public string N_1_D20 {get;set;}
        public string N_1_D21 {get;set;}
        public string N_1_D22 {get;set;}
        public string N_1_D23 {get;set;}
        public string N_1_D24 {get;set;}
        public string N_1_D25 {get;set;} 
        public string N_1_D26 {get;set;}
        public string N_1_D27 {get;set;}
        public string N_1_D28 {get;set;}
        public string N_1_D29 {get;set;}
        public string N_1_D30 {get;set;}
        public string N_1_D31 {get;set;}
        public string SYSTEM_DATE {get;set;}
        public string SERIES {get;set;}
        public string LIFE_CYCLE {get;set;}
        public string B_A_SIGN {get;set;}
        public string B_A_QUANTITY {get;set;}
        public string R_U_PREVIOUS {get;set;}
        public string R_U_PRESENT {get;set;}
        public string SUPPLIER_CD {get;set;}
        public string SUPPLIER_NAME {get;set;}
        public string PART_NAME {get;set;}
        public string DUMMY {get;set;}
        public string TERM {get;set;}
        public string CREATED_BY {get;set;}
        public string CREATED_DT {get;set;}
        public string CHANGED_BY {get;set;}
        public string CHANGED_DT { get; set; }
    }
}