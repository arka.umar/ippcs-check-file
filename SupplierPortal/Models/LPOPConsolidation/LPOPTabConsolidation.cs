﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConsolidation
{
    public class LPOPTabConsolidation
    {
        public string GetsudoMonth { get; set; }
        public string DataSource { get; set; }
        public string Timing { get; set; }
        public string Download { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Records { get; set; }
        public string Status { get; set; }
        public string Notice { get; set; }
        public string ConsolidateBy { get; set; }
        public DateTime ConsolidateDate { get; set; }
    }
}