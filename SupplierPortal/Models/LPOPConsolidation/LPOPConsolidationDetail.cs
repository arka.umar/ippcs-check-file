﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConsolidation
{
    public class LPOPConsolidationDetail
    {
        public String D_ID { get; set; }
        public String VERS { get; set; }
        public String REV_NO { get; set; }
        public String COMP_CD { get; set; }
        public String R_PLANT_CD { get; set; }
        public String DOCK_CD { get; set; }
        public String S_PLANT_CD { get; set; }
        public String SHIP_CD { get; set; }
        public String ORD_TYPE { get; set; }
        public String PACK_MONTH { get; set; }
        public String CAR_CD { get; set; }
        public String REC_CD { get; set; }
        public String SRC { get; set; }
        public String PART_NO { get; set; }
        public String ORDER_TYPE { get; set; }
        public String ORDER_LOT_SZ { get; set; }
        public String KANBAN_NUMBER { get; set; }
        public String N_VOLUME { get; set; }
        public String AICO_CEPT { get; set; }
        public String N_1_VOLUME { get; set; }
        public String AICO_CEPT1 { get; set; }
        public String N_2_VOLUME { get; set; }
        public String AICO_CEPT2 { get; set; }
        public String N_3_VOLUME { get; set; }
        public String AICO_CEPT3 { get; set; }
        public String N_D01 { get; set; }
        public String N_D02 { get; set; }
        public String N_D03 { get; set; }
        public String N_D04 { get; set; }
        public String N_D05 { get; set; }
        public String N_D06 { get; set; }
        public String N_D07 { get; set; }
        public String N_D08 { get; set; }
        public String N_D09 { get; set; }
        public String N_D10 { get; set; }
        public String N_D11 { get; set; }
        public String N_D12 { get; set; }
        public String N_D13 { get; set; }
        public String N_D14 { get; set; }
        public String N_D15 { get; set; }
        public String N_D16 { get; set; }
        public String N_D17 { get; set; }
        public String N_D18 { get; set; }
        public String N_D19 { get; set; }
        public String N_D20 { get; set; }
        public String N_D21 { get; set; }
        public String N_D22 { get; set; }
        public String N_D23 { get; set; }
        public String N_D24 { get; set; }
        public String N_D25 { get; set; }
        public String N_D26 { get; set; }
        public String N_D27 { get; set; }
        public String N_D28 { get; set; }
        public String N_D29 { get; set; }
        public String N_D30 { get; set; }
        public String N_D31 { get; set; }
        public String N_1_D01 { get; set; }
        public String N_1_D02 { get; set; }
        public String N_1_D03 { get; set; }
        public String N_1_D04 { get; set; }
        public String N_1_D05 { get; set; }
        public String N_1_D06 { get; set; }
        public String N_1_D07 { get; set; }
        public String N_1_D08 { get; set; }
        public String N_1_D09 { get; set; }
        public String N_1_D10 { get; set; }
        public String N_1_D11 { get; set; }
        public String N_1_D12 { get; set; }
        public String N_1_D13 { get; set; }
        public String N_1_D14 { get; set; }
        public String N_1_D15 { get; set; }
        public String N_1_D16 { get; set; }
        public String N_1_D17 { get; set; }
        public String N_1_D18 { get; set; }
        public String N_1_D19 { get; set; }
        public String N_1_D20 { get; set; }
        public String N_1_D21 { get; set; }
        public String N_1_D22 { get; set; }
        public String N_1_D23 { get; set; }
        public String N_1_D24 { get; set; }
        public String N_1_D25 { get; set; }
        public String N_1_D26 { get; set; }
        public String N_1_D27 { get; set; }
        public String N_1_D28 { get; set; }
        public String N_1_D29 { get; set; }
        public String N_1_D30 { get; set; }
        public String N_1_D31 { get; set; }
        public String SYSTEM_DATE { get; set; }
        public String SERIES { get; set; }
        public String LIFE_CYCLE { get; set; }
        public String B_A_SIGN { get; set; }
        public String B_A_QUANTITY { get; set; }
        public String R_U_PREVIOUS { get; set; }
        public String R_U_PRESENT { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PART_NAME { get; set; }
        public String DUMMY { get; set; }
        public String TERM { get; set; }
        public String CREATED_BY { get; set; }
        public String CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public String CHANGED_DT { get; set; }
    }
}