﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConsolidation
{
    public class LPOPConsolidationMaster
    {
      
        public string GetsudoMonth { get; set; }
        public string DataSource { get; set; }
        public string Timing { get; set; }
        public string Download { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Records { get; set; }
        public string Status { get; set; }
        public string Notice { get; set; }
        public DateTime Consolidation_Date { get; set; }
    }
}