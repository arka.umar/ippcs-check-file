﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConsolidation
{
    public class LPOPConsolidationModel
    {
       
        public List<LPOPTabConsolidation> LPOPConsolidations { get; set; }
        public List<LPOPTabConfirmation> LPOPConsolidationConfirmations { get; set; }

        public List<LPOPConfirmationDetail> LPOPConfirmationDetails { set; get; }
        public List<LPOPConsolidationTNQCDetail> LPOPConsolidationTNQCDetails { set; get; }
        public List<LPOPConsolidationNCSDetail> LPOPConsolidationNCSDetails { set; get; }
        public List<LPOPConsolidationFCSTDetail> LPOPConsolidationPFDetails { set; get; }
        public List<LPOPConfirmationAbnormalityDetail> LPOPConfirmationAbnormalityDetail { set; get; }
        public List<LPOPConfirmationReportDetail> LPOPConfirmationReportDetail { set; get; }

        public LPOPConsolidationModel()
        {
            LPOPConsolidations = new List<LPOPTabConsolidation>();
            LPOPConsolidationConfirmations = new List<LPOPTabConfirmation>();
            LPOPConsolidationTNQCDetails = new List<LPOPConsolidationTNQCDetail>();
            LPOPConsolidationNCSDetails = new List<LPOPConsolidationNCSDetail>();
            LPOPConsolidationPFDetails = new List<LPOPConsolidationFCSTDetail>();
            LPOPConfirmationDetails = new List<LPOPConfirmationDetail>();
            LPOPConfirmationAbnormalityDetail = new List<LPOPConfirmationAbnormalityDetail>();
            LPOPConfirmationReportDetail = new List<LPOPConfirmationReportDetail>();
        }
    }
}