﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConsolidation
{
    public class LPOPPurchaseOrder
    {
        public string dockCode { set; get; }
        public string suppCode { set; get; }
        public string matNo { set; get; }
        public string prodPurposeCode { set; get; }
        public string sourceType { set; get; }
        public string matDesc { set; get; }
        public string prodMonth { set; get; }
        public string partColorSfx { set; get; }
        public string qtyN { set; get; }
        public string qtyN1 { set; get; }
        public string qtyN2 { set; get; }
        public string qtyN3 { set; get; }
    }
}