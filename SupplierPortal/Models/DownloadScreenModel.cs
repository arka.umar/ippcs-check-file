﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

namespace Portal.Models
{
    public class DownloadScreenModel
    {
        public DownloadScreenModel()
        {
            DownloadScreens = new List<DownloadScreen>();
        }

        public List<DownloadScreen> DownloadScreens { set; get; }
    }

    public class DownloadScreen
    {
        public string Path { set; get; }
        public string FileName { set; get; }
        public string FileType { set; get; }
        public byte[] Icon { set; get; }
        public string FileSize { set; get; }
        public int Complete { set; get; }
        public DateTime DownloadDate { set; get; }
    }
}