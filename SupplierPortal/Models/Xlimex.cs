﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NPOI.HPSF;
using NPOI.SS;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.Util;
using NPOI.DDF;
using NPOI.Util.Collections;
using System.IO;
using System.Reflection;

namespace Portal.Models
{
    public static class Xlimex
    {
        public static List<T> Read<T>(string inputFile, string columns, int skipRows = 1)
        {
            List<T> l = new List<T>();

            Type t = typeof(T);
            List<ColInfo> map = null;
            List<string> cols = null; 
            MapCols(t, columns, ref map, ref cols);

            ISheet sheet;
            using (var stream = new FileStream(inputFile, FileMode.Open))
            {
                stream.Position = 0;

                IWorkbook wb = new HSSFWorkbook(stream);
                sheet = wb.GetSheetAt(0);
                IRow headerRow = sheet.GetRow(0);
                int cellCount = headerRow.LastCellNum;
                IFormulaEvaluator objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook)wb);
                DataFormatter objDefaultFormat = new DataFormatter();

                for (int i = (sheet.FirstRowNum + skipRows); i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    if (row == null) continue;
                    if (row.Cells.All(d => d.CellType == CellType.BLANK)) continue;
                    int mi = 0;
               
                    T li = (T)Activator.CreateInstance(t);
                    for (int j = row.FirstCellNum; j < cellCount; j++)
                    {
                        if (mi < map.Count && map[mi].Index >= 0)
                        {
                            ICell c = row.GetCell(j);
                            string coType = map[mi].Type;
                            PropertyInfo pinfo = map[mi].Info;
                            if (c == null || c.CellType == CellType.BLANK) continue;

                            if ("String".Equals(coType))
                            {
                                string s = null;
                                if (c.CellType == CellType.STRING)
                                    s = c.StringCellValue;
                                else if (c.CellType == CellType.NUMERIC)
                                {
                                    if (HSSFDateUtil.IsCellDateFormatted(c))
                                        s = c.DateCellValue.ToString(c.CellStyle.GetDataFormatString().Replace("m", "M"));
                                    else 
                                        s = Convert.ToInt64(c.NumericCellValue).ToString();
                                }
                                else if (c.CellType == CellType.FORMULA)
                                {
                                    objFormulaEvaluator.Evaluate(c); // This will evaluate the cell, And any type of cell will return string value
                                    s = objDefaultFormat.FormatCellValue(c, objFormulaEvaluator);
                                }
                                pinfo.SetValue(li, s, null);
                                
                            }
                            else if ("DateTime".Equals(coType))
                            {
                                DateTime? dt = null;
                                if (c.CellType == CellType.NUMERIC)
                                    dt = c.DateCellValue;
                                else if (c.CellType == CellType.STRING || c.CellType == CellType.FORMULA)
                                    dt = c.StringCellValue.ToDateTime();

                                pinfo.SetValue(li, dt, null);
                            }
                            else if (coType.StartsWith("Int"))
                            {
                                if (c.CellType == CellType.NUMERIC)
                                {
                                    if (coType.EndsWith("64"))
                                        pinfo.SetValue(li, Convert.ToInt64(c.NumericCellValue), null);
                                    else
                                        pinfo.SetValue(li, Convert.ToInt32(c.NumericCellValue), null);
                                }
                                else if (c.CellType == CellType.STRING)
                                {
                                    Int64 i64 = 0;
                                    if (Int64.TryParse(c.StringCellValue, out i64))
                                        pinfo.SetValue(li, i64, null);
                                }

                            }
                            else if (" Double Single Decimal".IndexOf(coType) > 0)
                            {
                                if (c.CellType == CellType.NUMERIC)
                                {
                                    pinfo.SetValue(li, c.NumericCellValue, null);
                                }
                                else
                                {
                                    Double dou = 0.00;
                                    if (Double.TryParse(c.StringCellValue, out dou))
                                        pinfo.SetValue(li, dou, null);
                                }
                            }
                            else if ("Boolean".Equals(coType))
                            {
                                if (c.CellType == CellType.BOOLEAN)
                                {
                                    pinfo.SetValue(li, c.BooleanCellValue, null);
                                }
                                else
                                {
                                    bool b = false;
                                    if (Boolean.TryParse(c.StringCellValue, out b))
                                        pinfo.SetValue(li, b, null);
                                }
                            }

                        }
                        mi++;
                    }
                    l.Add(li);
                }
            }
            return l;
        }

        public static void Write<T>(string template, string output, string columns, string rownoColumn, List<T> li, int startRow = 1)
        {
            Type t = typeof(T);
            List<ColInfo> map = null;
            List<string> cols = null;
            
            MapCols(t, columns, ref map, ref cols);
            int Starten = 0;
            int Enden = map.Count;

            List<string> columnRowNo = null;
            if (!string.IsNullOrEmpty(rownoColumn) && cols.IndexOf(rownoColumn) < 0) 
                MapCols(t, rownoColumn, ref map, ref columnRowNo); 
            
            ISheet sheet;
            IWorkbook wb;
            using (var stream = new FileStream(template, FileMode.Open))
            {
                stream.Position = 0;

                wb = new HSSFWorkbook(stream);
                sheet = wb.GetSheetAt(0);
                IRow headerRow = sheet.GetRow(0);
                int cellCount = headerRow.LastCellNum;

                HSSFFont font = (HSSFFont)wb.CreateFont();
                font.FontHeightInPoints = 10;
                font.FontName = "Arial";

                HSSFCellStyle border = (HSSFCellStyle)wb.CreateCellStyle();
                border.SetFont(font);
                border.BorderLeft = BorderStyle.THIN;
                border.BorderTop = BorderStyle.THIN;
                border.BorderRight = BorderStyle.THIN;
                border.BorderBottom = BorderStyle.THIN;

                if (cellCount < cols.Count)
                {
                    for (int i = cellCount; i < cols.Count; i++ )
                    {
                        ICell coname = headerRow.CreateCell(i, CellType.STRING);
                        coname.SetCellValue(cols[i]);
                    }
                };



                for (int findStarten = 0; findStarten < Enden; findStarten++)
                {
                    if (map[findStarten].Index >= 0) { Starten = findStarten; break; }
                }
                int findEnden = Enden; 
                while (findEnden > Starten) 
                {
                    if (map[findEnden-1].Index >= 0) { Enden = findEnden; break; }
                    --findEnden;
                }
                int rowNoColi = -1;
                for (int i = 0; i < map.Count; i++)
                {
                    if (string.Compare(map[i].Name, rownoColumn, true) == 0)
                    {
                        rowNoColi = i;
                        break;
                    }
                }
                    
                for (int ri = 0; ri < li.Count; ri++)
                {
                    T o = li[ri];
                    int rowi = ri + startRow;
                    if (rowi > 65535) /// limitation of xls -1997
                        break;
                    
                    if (rowNoColi >= 0)
                    {
                        object vo = map[rowNoColi].Info.GetValue(o, null);
                        if (vo != null)
                        {
                            int rowNo = Convert.ToInt32(vo);
                            rowi = rowNo + startRow;
                        }
                    }
                    IRow r = sheet.GetRow(rowi);

                    if (r == null)
                        r = sheet.CreateRow(rowi);
                    
                    for (int ci = Starten; ci < Enden; ci++)
                    {
                        if (map[ci].Index < 0) continue;
                        ICell sel = r.GetCell(ci);
                        CellType ct = CellType.STRING;
                        string sv = null;
                        DateTime? dv = null;
                        double? ddv = null;
                        bool? bv = null;
                        long? lv = null;   
                        
                        switch (map[ci].Type)
                        {
                            case "Boolean": ct = CellType.BOOLEAN; bv = (bool) map[ci].Info.GetValue(o, null);  break;
                            case "DateTime": ct = CellType.NUMERIC; dv = (DateTime)map[ci].Info.GetValue(o, null);  break;
                            
                            case "Int64":
                            case "Int32": ct = CellType.NUMERIC; lv = Convert.ToInt64(map[ci].Info.GetValue(o, null)); break;
                            case "Number":
                            case "Decimal":
                            case "Single":
                            case "Double": ct = CellType.NUMERIC; ddv = (double)map[ci].Info.GetValue(o, null); break;

                            default: ct = CellType.STRING; sv = (string)map[ci].Info.GetValue(o, null); break;
                        }

                        if (sel == null)
                        {
                            map[ci].Info.GetValue(o, null);

                            sel = r.CreateCell(ci, ct);
                            sel.CellStyle = border;
                           
                        }
                        else
                        {
                            sel.SetCellType(ct);
                        }

                        switch (map[ci].Type)
                        {
                            case "Boolean": sel.SetCellValue(bv.Value); break;
                            case "DateTime": sel.SetCellValue(dv.Value); break;

                            case "Int64":
                            case "Int32": sel.SetCellValue((double)lv); break;
                            case "Number":
                            case "Decimal":
                            case "Single":
                            case "Double": sel.SetCellValue(ddv.Value); break;

                            default: sel.SetCellValue(sv); break;
                        }


                    }
                }
                for (int ci = Starten; ci < Enden; ci++)
                    try
                    {
                        sheet.AutoSizeColumn(ci);
                    }
                    catch
                    {
                    }
            } 
            using (FileStream fo = new FileStream(output, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite, 10240))
            {
                wb.Write(fo);
            }
        }

        public static void MapCols(Type t, string columns, ref List<ColInfo> map, ref List<string> cols)
        {
            PropertyInfo[] pi = t.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            if (map == null)
            {
                map = new List<ColInfo>();
            }            
            cols = columns.Split(new char[] { ',', ' ', ';', '|' }).ToList();
            for (int ci = 0; ci < cols.Count; ci++)
            {
                int n = -1;
                for (int pj = 0; pj < pi.Length && n < 0; pj++)
                {
                    if (string.Compare(pi[pj].Name, cols[ci], true) == 0)
                    {
                        n = pj;
                    }
                }
                if (n > -1)
                {
                    Type nulable = Nullable.GetUnderlyingType(pi[n].PropertyType);
                    map.Add(new ColInfo()
                    {
                        Index = n,
                        Info = pi[n],
                        Name = pi[n].Name,
                        Type = (nulable == null) ? pi[n].PropertyType.Name : nulable.Name,
                        Nullable = (nulable != null)
                    });
                }
                else
                {
                    map.Add(new ColInfo() { Index = n });
                }
            }
        }

    }


    public class ColInfo
    {
        public PropertyInfo Info { get; set; }
        public string Name { get; set; }
        public int Index { get; set; }
        public string Type { get; set; }
        public bool Nullable { get; set; }
    }
}