﻿using System;
using System.Collections.Generic;


namespace Portal.Models.SupplierEmailMaster
{
    public class SupplierEmailMasterModel {
        
        public List<SupplierEmailMaster> EmailDatas { get; set; }

        public SupplierEmailMasterModel()
        {
            EmailDatas = new List<SupplierEmailMaster>();
        }
    }




    public class SupplierEmailMaster {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string POSITION_CD { get; set; }
        public int AREA_CD { get; set;}
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        //public string CHANGED_BY { get; set;}
        //public DateTime CHANGED_DT { get; set; }
    }


    public class SupplierEmailMasterErrorList
    {
        public string SUPPLIER_CODE { get; set; }
        public string AREA_CODE { get; set; }
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public string POSITION { get; set; }
        public string MSGG { get; set; }

        //public string CHANGED_BY { get; set;}
        //public DateTime CHANGED_DT { get; set; }
    }
}