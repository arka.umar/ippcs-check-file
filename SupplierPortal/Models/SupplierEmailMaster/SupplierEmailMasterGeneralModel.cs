﻿using System;

namespace Portal.Models.SupplierEmailMaster
{
    public class SupplierLookup {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
    }

    public class PositionLookup {
        public string POSITION_CODE { get; set; }
        public string POSITION_NAME { get; set; }
    }
}