﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationGridReportSum
    {
        public string SUPPLIER_CD { get; set; }
        public string STATUS { get; set; }
        public string TMMIN_CURR { get; set; }

        public int TMMIN_AMOUNT { get; set; }
        public int TMMIN_TAX { get; set; }
        public int TMMIN_TOTAL { get; set; }
        public string SUPP_CURR { get; set; }
        public int SUPP_AMOUNT { get; set; }

        public int SUPP_TAX { get; set; }

        public int SUPP_TOTAL { get; set; }
        public int DIFF_SUM { get; set; }
        public string PROGRESS { get; set; }
        public string FEEDBACK_SUM { get; set; }

    }
}