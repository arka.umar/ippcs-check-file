﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationDetailModel
    {
        public List<SupplierReconciliationGrid> SRDetail { get; set; }
        public SupplierReconciliationDetailModel()
        {
            SRDetail = new List<SupplierReconciliationGrid>();
        }
    }
}