﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationGridSumModel
    {
        public List<SupplierReconciliationGridSum> SRGridSum { get; set; }

        public SupplierReconciliationGridSumModel()
        {
            SRGridSum = new List<SupplierReconciliationGridSum>();
        }
    }
}