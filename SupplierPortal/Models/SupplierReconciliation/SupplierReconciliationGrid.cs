﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationGrid
    {
        public string SUPPLIER_CD { get; set; }
        public string DOCUMENT_NO { get; set; }
        public DateTime POSTING_DATE { get; set; }
        public string DOCUMENT_DESC { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string ITEM_CURR { get; set; }
        public int SUPP_AMOUNT { get; set; }
        public int FD_AMOUNT { get; set; }
        public int DIFFERENT { get; set; }
        public int PROCESS_ID { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string STATUS { get; set; }
        /*public string INVOICED { get; set; }
        public string OSGR { get; set; }
        public string ERRORGR { get; set; }
        public string NA { get; set; }*/
        public string SUPP_FEEDBACK_CD { get; set; }
        public string SUPP_FEEDBACK_DESC { get; set; }
        public string FD_FEEDBACK_DESC { get; set; }
        public string FD_FEEDBACK_CD { get; set; }
        public string PROGRESS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        public int COUNT_DATA { get; set; }
        /*public string ManifestNumber { set; get; }
        public string InvoiceNumber { set; get; }
        public string Amount { set; get; }*/


    }

    public class TotalAmount
    {
        public int idrSupplier { get; set; }
        public int idrTMMIN { get; set; }
        public int idrDiff { get; set; }
        public string itemCurr { get; set; }
    }

    public class LastReconcileMonth
    {
        public string lastReconcile { get; set; }
    }
}