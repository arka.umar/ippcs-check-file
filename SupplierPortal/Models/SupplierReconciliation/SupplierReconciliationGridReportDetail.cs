﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationGridReportDetail
    {
        public string MANIFEST_NO { get; set; }
        public DateTime POSTING_DATE { get; set; }
        public string INVOICE_NO { get; set; }
        public string ITEM_CURR { get; set; }
        public int AMOUNT { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        /*public string ManifestNumber { set; get; }
        public string InvoiceNumber { set; get; }
        public string Amount { set; get; }*/


    }
}