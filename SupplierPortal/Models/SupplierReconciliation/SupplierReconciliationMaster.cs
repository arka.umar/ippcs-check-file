﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationMaster
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
    }
}