﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationMasterName
    {
        public string SupplierCd { get; set; }
        public string SupplierNm { get; set; }
    }
}