﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationModel
    {
        public List<SupplierReconciliationMaster> SRMaster { get; set; }
        public SupplierReconciliationModel()
        {
            SRMaster = new List<SupplierReconciliationMaster>();
        }
    }
}