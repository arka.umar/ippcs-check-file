﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationGridDetailModel
    {
        public List<SupplierReconciliationGridDetail> SRGridDetail { get; set; }

        public SupplierReconciliationGridDetailModel()
        {
            SRGridDetail = new List<SupplierReconciliationGridDetail>();
        }
    }
}