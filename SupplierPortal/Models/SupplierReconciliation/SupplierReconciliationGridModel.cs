﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierReconciliation
{
    public class SupplierReconciliationGridModel
    {
        public List<SupplierReconciliationGrid> SRGrid { get; set; }

        public SupplierReconciliationGridModel()
        {
            SRGrid = new List<SupplierReconciliationGrid>();
        }
    }
}