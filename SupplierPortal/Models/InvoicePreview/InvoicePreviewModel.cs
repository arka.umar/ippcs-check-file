﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoicePreview
{
    public class InvoicePreviewModel
    {
        public List<InvoicePreview> InvoicePreviews { set; get; }
        public List<InvoicePreview> InvoicePreviewsII { set; get; }
        
        public InvoicePreviewModel()
        {
            InvoicePreviews = new List<InvoicePreview>();
            InvoicePreviewsII = new List<InvoicePreview>();
        }
    }
}
