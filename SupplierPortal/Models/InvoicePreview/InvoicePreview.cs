﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoicePreview
{
    public class InvoicePreview
    {
        public string InvoiceNo { set; get; }
        public DateTime InvoiceDate { get; set; }
        public double TurnOver { get; set; }
        public int TotalManifest { get; set; }
        public Boolean StampIncluded { get; set; }
        public double TotalInvoiceAmount { get; set; }
        public Boolean DownloadDetail { get; set; }
        public string TaxInvoiceNo { get; set; }
        public DateTime TaxInvoiceDate { set; get; }
        public double TaxAmount { get; set; }
        public double RetroAmount { get; set; }
    }
}
