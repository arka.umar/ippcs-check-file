﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Junbiki
{
    public class JunbikiMaster
    {
        public string Supplier { set; get; }
        public DateTime OrderDate { set; get; }
        public DateTime OrderDateTo { set; get; }
        public int SequenceNo { set; get; }
       // public DateTime OrderDate { set; get; }
        public string ManifestNo { set; get; }
        public string OrderNo { set; get; }
        public string ShiftCode { set; get; }
        public string Dock { set; get; }
        public int TotalItem { set; get; }
        public int TotalQty { set; get; }
        public string DownIcon { set; get; }
        public string NoticeIcon { set; get; }
    }
}