﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Junbiki
{
    public class JunbikiDetail
    {
    
        public string PartName { set; get; }
        public string Manifest { set; get; }
        public string PartNo { set; get; }
        public string Suffix { set; get; }
        public string Status { set; get; }
        public string Color { set; get; }
        public string Katashiki { set; get; }
        public string Modelz { set; get; }
        public int Qty { set; get; }
    }
}