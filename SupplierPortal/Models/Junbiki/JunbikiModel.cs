﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Junbiki
{
    public class JunbikiModel
    {
        public List<JunbikiMaster> GetJunbikiMaster { set; get; }
        public List<JunbikiDetail> GetJunbikiDetail { set; get; }
        public JunbikiModel() 
        {
            GetJunbikiMaster = new List<JunbikiMaster>();
            GetJunbikiDetail = new List<JunbikiDetail>();
        }
    }
}