﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.RoutePrice
{
    public class RoutePriceModel
    {
        public List<RoutePriceData> RoutePriceDatas { get; set; }
         
        public RoutePriceModel()
        {
            RoutePriceDatas = new List<RoutePriceData>();
        }
    }
}