﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DeliveryPurchase
{
    public class DeliveryPurchaseModel
    {
         public List<DeliveryPurchaseMaster> DeliveryPurchase { get; set; }
         public DeliveryPurchaseModel()
        {
            DeliveryPurchase = new List<DeliveryPurchaseMaster>();
        }   
    }
}