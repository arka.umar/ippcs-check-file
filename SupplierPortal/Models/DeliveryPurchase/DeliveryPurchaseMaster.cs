﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DeliveryPurchase
{
    public class DeliveryPurchaseMaster
    {
        public string PRNo { get; set; }
        public string RouteConvertion { get; set; }
        public string RouteCode { get; set; }
        public string RouteName { get; set; }
        public string Trucking { get; set; }
        public string Qty { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ChangeDate { get; set; }
        public string ChangeBy { get; set; }
    }
}