﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceTaxCreationByMonth
{
    public class InvoiceTaxCreationByMonthModel
    {
        public List<InvoiceTaxCreationByMonthDetail> InvoiceTaxCreationByMonthDetails { set; get; }

        public List<InvoiceTaxCreationByMonth> InvoiceTaxCreationByMonths { set; get; }

        public InvoiceTaxCreationByMonthModel()
        {
            InvoiceTaxCreationByMonthDetails = new List<InvoiceTaxCreationByMonthDetail>();
            InvoiceTaxCreationByMonths = new List<InvoiceTaxCreationByMonth>();
        }
    }
}