﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceTaxCreationByMonth
{
    public class InvoiceTaxCreationByMonthDetail
    {
        public string Editors { set; get; }
        public string InvoiceTaxNo { set; get; }
        public DateTime InvoiceTaxDate { set; get; }
        public string InvoiceNo { set; get; }
        public DateTime InvoiceDate { set; get; }
        public string TurnOverCurrency { set; get; }
        public string TurnOverAmount { set; get; }
        public string InvoiceTaxAmount { set; get; }
        public string Total { set; get; }
        public string TaxStatus { set; get; }
    }
}