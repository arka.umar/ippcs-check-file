﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceTaxCreationByMonth
{
    public class InvoiceTaxPopupModel
    {
        //public List<InvoiceCreationDetail> InvoiceCreationDetails { set; get; }

        public List<InvoiceTaxPopup> InvoiceTaxCreationPopups { set; get; }


        public InvoiceTaxPopupModel()
        {
            //InvoiceCreationDetails = new List<InvoiceCreationDetail>();
            InvoiceTaxCreationPopups = new List<InvoiceTaxPopup>();

        }
    }
}