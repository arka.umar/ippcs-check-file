﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceAndPaymentInquiry
{
    public class InvoiceAndPaymentDetail
    {
        public string ManifestNo { get; set; }
        public string MAT_NO { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public string MAT_TEXT { get; set; }
        public DateTime DOC_DT { get; set; }
        public string DOCK_CD { get; set; }
        public string QTY { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
        public double Amount { get; set; }        
    }
}