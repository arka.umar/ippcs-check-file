﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceAndPaymentInquiry
{
    public class InvoiceAndPaymentInquiry
    {
        public string SUPP_CD { get; set; }
	    public string SUPP_INV_NO { get; set; }
        public DateTime POSTING_DT { get; set; }
        public string POSTING_DT_STRING { get { return POSTING_DT.ToString("dd.MM.yyyy"); } }
        public DateTime BASELINE_DT { get; set; }
        public string BASELINE_DT_STRING { get { return BASELINE_DT.ToString("dd.MM.yyyy"); } }
        public DateTime INV_DT { get; set; }
        public string INV_DT_STRING { get { return INV_DT.ToString("dd.MM.yyyy"); } }
	    public string PAY_METHOD_CD { get; set; }
	    public string PAY_TERM_CD { get; set; }
	    public string SUPP_BANK_TYPE { get; set; }
        public double AMT { get; set; }
	    public string INV_TAX_NO { get; set; }
	    public string INV_TEXT { get; set; }
	    public string ASSIGNMENT { get; set; }
	    public string TAX_CD { get; set; }
        public DateTime INV_TAX_DT { get; set; }
        public string INV_TAX_DT_STRING { get { return INV_TAX_DT.ToString("dd.MM.yyyy"); } }
	    public string WITHHOLDING_TAX_CD { get; set; }
        public double BASE_AMT { get; set; }
	    public string GL_ACCOUNT { get; set; }
        public double GL_INV_AMT { get; set; }
        public string GL_COST_CENTER { get; set; }
        public string GL_TAX_CD { get; set; }
        public string PPV_EVIDENCE { get; set; }
    }
}