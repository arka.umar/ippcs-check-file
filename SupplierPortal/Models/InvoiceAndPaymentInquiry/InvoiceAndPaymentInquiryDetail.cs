﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceAndPaymentInquiry
{
    public class InvoiceAndPaymentInquiryDetail
    {
        public string InvoiceNo { set; get; }
        public DateTime? InvoiceDate { set; get; }
        public string InvoiceNoICS { set; get; }
        public string SAPDocNo { set; get; }
        public string Currency { set; get; } 
        public double InvoiceAmount { set; get; }
        public string STATUS_CD { set; get; }
        public string STATUS_NAME { set; get; }
        public string SuppCode { set; get; }
        public string SuppName { set; get; }
        public string InvoiceTaxNo { set; get; }
        public DateTime? InvoiceTaxDate { set; get; }
        public double InvoiceTaxAmount { set; get; }
        public DateTime? PlannedPaymentDate { set; get; }
        public DateTime? PaymentDate { set; get; }
        public string PaymentDocNo { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? SUBMIT_DT { set; get; } 
        public string SUBMIT_BY { set; get; }
        public DateTime? POSTING_DT { set; get; } 
        public string POSTING_BY { set; get; }
        public string IN_PROGRESS { set; get; }
        public double TurnOver { set; get; }
        public string PPV_EVIDENCE { get; set; }
        public double PPV_AMT { set; get; }
        public string AUTOMATIC { set; get; }
        public string TRANSACTION_TYPE { set; get; } 

    }
}