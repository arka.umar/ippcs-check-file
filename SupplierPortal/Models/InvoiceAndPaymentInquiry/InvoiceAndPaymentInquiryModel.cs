﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceAndPaymentInquiry
{
    public class InvoiceAndPaymentInquiryModel
    {
        public List<InvoiceAndPaymentInquiryDetail> InvoiceAndPaymentInquiryDetails { set; get; }
        public List<InvoiceAndPaymentInquiry> InvoiceAndPaymentInquirys { set; get; }
        public List<InvoiceAndPaymentDetail> InvoiceAndPaymentDetails { get; set; }


        public InvoiceAndPaymentInquiryModel()
        {
            InvoiceAndPaymentInquiryDetails = new List<InvoiceAndPaymentInquiryDetail>();
            InvoiceAndPaymentInquirys = new List<InvoiceAndPaymentInquiry>();
            InvoiceAndPaymentDetails = new List<InvoiceAndPaymentDetail>();
        }
    }
}