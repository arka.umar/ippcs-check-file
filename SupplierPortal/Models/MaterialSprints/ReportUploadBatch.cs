﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.MaterialSprints
{
    public class ReportUploadBatch
    {
        public string ProcessID { set; get; }
        public string SequenceNumber { set; get; }
        public string Message { set; get; }
        public string MessageID { set; get; }
        public string Location { set; get; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
    }
}