﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.MaterialSprints
{
    public class MaterialSprintsModel
    {
        public List<MaterialSprints> MaterialSprintsCollection { get; set; }

        public MaterialSprintsModel() {
            MaterialSprintsCollection = new List<MaterialSprints>();
        }
    }
}