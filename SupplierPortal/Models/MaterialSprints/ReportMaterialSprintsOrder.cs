﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.MaterialSprints
{
    public class ReportMaterialSprintsOrder
    {
        public string ORDER_NO { get; set; }
        public string SUPPLIER { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public DateTime ARRIVAL_PLAN { get; set; }
        public int ITEM_NO { get; set; }
        public string MATERIAL_NO { get; set; }
        public string SPEC { get; set; }
        public string SIZE { get; set; }
        public int QTY_PER_CASE { get; set; }
        public int SEQ_NO { get; set; }
        public string BATCH { get; set; }
        public int QTY_SHEET { get; set; }
        public decimal WEIGHT { get; set; }
        public string MILLS_CERT_NO { get; set; }
        public string MANIFEST_NO { get; set; }

    }
}
