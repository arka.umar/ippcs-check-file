﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.MaterialSprints
{
    public class MaterialSprints
    {
        public int ITEM_NO { get; set; }
        public string MATERIAL_NO { get; set; }
        public string DESCRIPTION { get; set; }
        public string ORDER_NO { get; set; }
        public string SLOC_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string LINE_CD { get; set; }
        public string MATERIAL_ADDRESS { get; set; }
        public string MAKER { get; set; }
        public string SIZE { get; set; }
        public string SPEC { get; set; }
        public string ARRIVAL_PLAN_DATE { get; set; }
        public DateTime? PLAN_ORDER_DATE { get; set; }
        public int? PLAN_ORDER_CYCLE_SEQ { get; set; }
        public TimeSpan? PLAN_ORDER_TIME { get; set; }
        public DateTime? PLAN_RECEIVING_DATE { get; set; }
        public int? PLAN_RECEIVING_CYCLE_SEQ { get; set; }
        public TimeSpan? PLAN_RECEIVING_TIME { get; set; }
        public int? QTY_ORI_MAT { get; set; }
        public int? QTY_PER_CASE { get; set; }
        public int? TTL_KANBAN { get; set; }
        public int? PLAN_ORDER_QTY { get; set; }
        public int? ACT_TTL_KBN { get; set; }
        public int? ACT_ORD_QTY { get; set; }
        public int? STOCK { get; set; }
        public int? STATUS { get; set; }
        public string STATUS_STR { get; set; }
        public string ORDER_NO_KBN_SEQ { get; set; }
        public string MANIFEST_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string SEQ_NO { get; set; }

        public string COMP_CD { get; set; }
        public string PLANT_CD { get; set; }
        public string BATCH { get; set; }
        public int? QTY { get; set; }
        public decimal? WEIGHT { get; set; }
        public bool BATCH_FLAG { get; set; }
        public string MILLS_CERT_NO { get; set; }


    }
}