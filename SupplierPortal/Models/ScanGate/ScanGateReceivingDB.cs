﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.ScanGate
{
    public class ScanGateReceivingDB
    {
        //
        // GET: /ScanGateReceivingDB/
        public string ROUTE { get; set; }
        public string RATE { get; set; }
        public DateTime PLAN_IN_DT { set; get; }
        public DateTime ACTUAL_IN_DT { set; get; }
        public TimeSpan GAP_IN { set; get; }
        //public long ARRIVAL_GAP_TICKS
        //{
        //    set { ARRIVAL_GAP = TimeSpan.FromTicks(value); }
        //    get { return ARRIVAL_GAP.Ticks; }
        //}
        public string STATUS_IN { set; get; }
        public string STATUS_IN_IMAGE { set; get; }
        public DateTime PLAN_OUT_DT { set; get; }
        public DateTime ACTUAL_OUT_DT { set; get; }
        public TimeSpan GAP_OUT { set; get; }
        public string STATUS_OUT { set; get; }
        public string STATUS_OUT_IMAGE { set; get; }
        //public long DEPARTURE_GAP_TICKS
        //{
        //    set { DEPARTURE_GAP = TimeSpan.FromTicks(value); }
        //    get { return DEPARTURE_GAP.Ticks; }
        //}
        //public string DELIVERY_NO { set; get; }
        //public string STATION { set; get; }
        //public string DOCK_CD { set; get; }
        //public string ROUTE { get; set; }
        //public string RATE { get; set; }
        //public DateTime ARRIVAL_ACTUAL_DT { set; get; }
        //public DateTime ARRIVAL_PLAN_DT { set; get; }
        //public TimeSpan ARRIVAL_GAP { set; get; }
        ////public long ARRIVAL_GAP_TICKS
        ////{
        ////    set { ARRIVAL_GAP = TimeSpan.FromTicks(value); }
        ////    get { return ARRIVAL_GAP.Ticks; }
        ////}
        //public int ARRIVAL_STATUS { set; get; }
        //public string ARRIVAL_STATUS_DESC { get; set; }
        //public string ARRIVAL_STATUS_IMAGE { get; set; }
        //public DateTime DEPARTURE_ACTUAL_DT { set; get; }
        //public DateTime DEPARTURE_PLAN_DT { set; get; }
        //public TimeSpan DEPARTURE_GAP { set; get; }
        ////public long DEPARTURE_GAP_TICKS
        ////{
        ////    set { DEPARTURE_GAP = TimeSpan.FromTicks(value); }
        ////    get { return DEPARTURE_GAP.Ticks; }
        ////}
        //public int DEPARTURE_STATUS { set; get; }
        //public string DEPARTURE_STATUS_DESC { get; set; }
        //public string DEPARTURE_STATUS_IMAGE { get; set; }
        //public DateTime CurrentTime { set; get; }
        //public string REMAINING { set; get; }
        //public int REVISE_NO { set; get; }
    }

    public class ScanGate
    {
        public string SCAN_CD { get; set; }
        public string SCAN_GATE { get; set; }
    }
}
