﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.ScanGate
{
    public class ScanGateReceivingModel 
    {
        
        //public List<ScanGateReceivingData> ScanGateReceivings { get; set; }
        public List<ScanGateScreenCombo> Combo { get; set; }
        public List<ScanGateReceivingDB> ScanGateReceivingGet { get; set; }

        public ScanGateReceivingModel()
        {
            //ScanGateReceivings = new List<ScanGateReceivingData>();
            Combo = new List<ScanGateScreenCombo>();
            ScanGateReceivingGet = new List<ScanGateReceivingDB>();
        }
    }
}
