﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.JunbikiPartMapping
{
    public class JunbikiPartMappingDetail
    {
        public string KatashikiCode { get; set; }
        public string KatashikiSuffix { get; set; }
        public string PartNo { get; set; }

        public string PartName { get; set; } 
        public string SupplierCode { get; set; }
        public string SupplierPlant { get; set; }
        
        public string DockCode { get; set; }

        public int ItemQty { get; set; }

        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }

        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDate { set; get; }
    }
}