﻿using System;
using System.Collections.Generic;

namespace Portal.Models.JunbikiPartMapping
{
    public class JunbikiPartMappingModel
    {
        public List<JunbikiPartMappingDetail> Detail { get; set; }
    }
}