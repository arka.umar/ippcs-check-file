﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ICSUserMapping
{
    public class ICSUserMappingModel
    {
        public List<ICSUserMapping> ICSUSerMappingList { set; get; }

        public ICSUserMappingModel()
        {
            ICSUSerMappingList = new List<ICSUserMapping>();
        }
    }
}