﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ICSUserMapping
{
    public class ICSUserMapping
    {
        public string ICSUserName { set; get; }
        public string LDAPUserName { set; get; }
        public string CreatedBy {set;get;}
        public DateTime CreatedDate { set; get; }
        public string ChangeBy { set; get; }
        public DateTime ChangeDate { set; get; }

    }
}