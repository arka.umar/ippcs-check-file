﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogTransaction
{
    public class logTransactionDetailMaster
    {
        public string logID {get; set;} 
        public string seqNo {get; set;}
        public DateTime  postingDate {get; set;}
        public DateTime  processDate {get; set;}
        public string manifestNo {get; set;}
        public string kanbanNo  {get; set;}
        public string prodPurpose { get; set; }
    }
}