﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogTransaction
{
    public class logTransactionDetailModel
    {
        public List<logTransactionDetailMaster> LogTransactionMonitoring { get; set; }
        public logTransactionDetailModel()
        {
            LogTransactionMonitoring = new List<logTransactionDetailMaster>();
        }   
    }
}