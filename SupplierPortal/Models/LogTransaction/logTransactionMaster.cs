﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogTransaction
{
    public class logTransactionMaster
    {
        public string logID {get; set;} 
        public string processID {get; set;}
        public string manifestNo {get; set;}
        public string createdBy {get; set;}
        public DateTime createdDate { get; set; }
        public string logDetailID { get; set; }
        public string seqNo { get; set; }
        public DateTime postingDate { get; set; }
        public DateTime processDate { get; set; }
        public string manifestNumber { get; set; }
        public string kanbanNo { get; set; }
        public string prodPurpose { get; set; }

    }
 }