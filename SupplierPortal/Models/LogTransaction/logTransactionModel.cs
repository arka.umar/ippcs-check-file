﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogTransaction
{
    public class logTransactionModel
    {
        public List<logTransactionMaster> logTransaction { get; set; }
        public logTransactionModel()
        {
            logTransaction = new List<logTransactionMaster>();
        }
    }
}