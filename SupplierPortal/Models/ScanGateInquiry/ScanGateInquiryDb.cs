﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ScanGateInquiry
{
    public class ScanGateInquiryDb
    {
        public string DELIVERY_NO { set; get; }
        public DateTime PICKUP_DT { set; get; }
        public string LP_CD  { set; get; }
        public string ROUTE_RATE { set; get; }
        public string LOG_PARTNER_NAME { get; set; }
        public string DRIVER_NAME { set; get; }
        public string VEHICLE_NO { set; get; }
        public string CODE_IN { set; get; }
        public DateTime? PLAN_IN_DT { set; get; }
        public DateTime? ACTUAL_IN_DT { set; get; }
        public TimeSpan? GAP_IN { set; get; }
        public string STATUS_IN { set; get; }
        public string STATUS_IMAGE_IN { set; get; }
        public string CODE_OUT { set; get; }
        public DateTime? PLAN_OUT_DT { set; get; }
        public DateTime? ACTUAL_OUT_DT { set; get; }
        
        //added fid.deny 2015-05-19
        public TimeSpan? GAP_OUT { set; get; }
        public string STATUS_OUT { set; get; }
        public string STATUS_IMAGE_OUT { set; get; }
        //end of added fid.deny 2015-05-19
    }
}