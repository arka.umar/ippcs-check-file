﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ScanGateInquiry
{
    public class ScanGateInquiry
    {
        public string DeliverNo { set; get; }
        public string PickUpDate { set; get; }
        public string RouteRate { set; get; }
        public string DriverName { set; get; }
        //public string DriverManifest { set; get; }
        //public string DownloadBy { set; get; }
        //public DateTime DownloadTime { set; get; }
        //public string ApprovalStatus { set; get; }
        //public string DeliveryStatus { set; get; }
        //public string ArrivalAt { set; get; }
        //public string LPConfirmation { set; get; }

        //public string Notice { set; get; }
        //public string DeliveryReason { set; get; }
        //public string ReviseBy { set; get; }
        //public DateTime ReviseDate { set; get; }
        //public string Approver { set; get; }
        //public DateTime ApprovalDate { set; get; }
        //public int RevisionNo { set; get; }

        //public string Status { set; get; }
        //public string DeliveryBy { set; get; }
        //public DateTime DeliveryDate { set; get; }
        //public string InvoiceBy { set; get; }
       // public DateTime InvoiceDate { set; get; }
        //public string Requisitioner { set; get; }
        //public DateTime RequisitionDate { set; get; }
        public string LPName { set; get; }
        public string LogisticPartner { set; get; }
    }

    public class ScanGateInquiryMainUploadDriver
    {
        public List<ScanGateInquiryUploadDriver> MainList { get; set; }

        public ScanGateInquiryMainUploadDriver()
        {
            MainList = new List<ScanGateInquiryUploadDriver>();
        }
    }

    public class ScanGateInquiryUploadDriver
    {
        public string DeliveryNo { get; set; }
        public string Route { get; set; }
        public string Rate { get; set; }
        public string PickupDate { get; set; }
        public string VehicleNo { get; set; }
        public string DriverName { get; set; }
    }

}