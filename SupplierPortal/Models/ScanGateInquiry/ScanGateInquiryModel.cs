﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ScanGateInquiry
{
    public class ScanGateInquiryModel
    {
        //public List<ScanGateInquiry> ScanGateInquiryRegionals { set; get; }
        public List<ScanGateInquiryDb> ScanGateInquiryDbModel { set; get; }
        public ScanGateInquiryModel()
        {
            //ScanGateInquiryRegionals = new List<ScanGateInquiry>();
            ScanGateInquiryDbModel = new List<ScanGateInquiryDb>();
        }
    }

    public class DriverNameList
    {
        public string DriverId { set; get; }
        public string DriverName { set; get; }
    }


    public class VehicleNoList
    {
        public string VehicleId { set; get; }
        public string VehicleNo { set; get; }
        public string BodyNo { set; get; }
    }

    public class GateCode
    {
        public string GATE_CODE { set; get; }
        public string COMPANY_CODE { set; get; }
        public string PLANT_CD { set; get; }
        public string DOCK_CD { set; get; }
        public string AREA { set; get; }
        public string DESCRIPTION { set; get; }
    }
}