﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.TPPostingToICSScreen
{
    public class TPPostingToICSScreenDetail
    {

        public string CycleSeq { get; set; }
        public string KanbanID { get; set; }
        public string DocCode { get; set; }
        public string PrintStatus { get; set; }
        public string ShopStatus { get; set; }
        public string ShopNo { get; set; }
        public DateTime PrintDate { get; set; }
        public string DeliveryNo { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public int PostToICS { get; set; }
        public string CancelQty { get; set; }
        public string CancelledQty { get; set; }
        public string Flag { get; set; }
        public string By { get; set; }
        public DateTime CancellationDate { get; set; }
    }
}