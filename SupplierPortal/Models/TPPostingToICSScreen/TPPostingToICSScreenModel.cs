﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.TPPostingToICSScreen
{
    public class TPPostingToICSScreenModel
    {
        public List<TPPostingToICSScreenDetail> TPPostingToICSScreenDetails { get; set; }

        public TPPostingToICSScreenModel()
        {
            TPPostingToICSScreenDetails = new List<TPPostingToICSScreenDetail>();
        }
    }
}