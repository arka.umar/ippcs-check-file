﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.CounterReceiving
{
    public class Rejectmodel
    {
        //public List<InvoiceCreationDetail> InvoiceCreationDetails { set; get; }

        public List<Reject> Rejects { set; get; }


        public Rejectmodel()
        {
            //InvoiceCreationDetails = new List<InvoiceCreationDetail>();
            Rejects = new List<Reject>();
            
        }
    }
}