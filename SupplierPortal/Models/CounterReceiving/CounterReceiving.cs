﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.CounterReceiving
{
    public class CounterReceiving
    {
        public string CertificateID { set; get; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string InvoiceNo { get; set; }
        public string Currency { get; set; }
        public string InvoiceAmount { get; set; }
        public string InvoiceTaxNo { get; set; }
        public string InvoiceTaxAmount { get; set; }
        public string InvoiceTaxType { set; get; }
        public string ReceiveDate { set; get; }
        public DateTime SubmitDate { set; get; }
        public string SubmitBy { set; get; }
        public string Status { set; get; }
    }
}