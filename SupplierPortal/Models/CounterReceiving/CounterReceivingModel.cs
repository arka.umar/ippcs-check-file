﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.CounterReceiving
{
    public class CounterReceivingModel
    {
        public List<CounterReceivingDetail> CounterReceivingDetails { get; set; }
        public List<CounterReceiving> CounterReceivings { get; set; }
        public CounterReceivingModel()
        {
            CounterReceivingDetails = new List<CounterReceivingDetail>();
            CounterReceivings = new List<CounterReceiving>();
        }
    }
}