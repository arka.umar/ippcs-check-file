﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ManifestReceivingScreen
{
    public class ManifestReceivingScreen
    {
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string RCV_PLANT_CD { set; get; }
        public int RECEIVED_STATUS { get; set; }
        public int PROBLEM_FLAG { get; set; }
     }
}