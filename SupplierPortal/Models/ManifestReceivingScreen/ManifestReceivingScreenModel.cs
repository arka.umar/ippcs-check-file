﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ManifestReceivingScreen
{
    public class ManifestReceivingScreenModel
    {
        public List<ManifestReceivingScreenDetail> ManifestReceivingScreenDetails { set; get; }

        public List<ManifestReceivingScreen> ManifestReceivingScreens { set; get; }

        public ManifestReceivingScreenModel()
        {
            ManifestReceivingScreenDetails = new List<ManifestReceivingScreenDetail>();
            ManifestReceivingScreens = new List<ManifestReceivingScreen>();
        }
    }
}