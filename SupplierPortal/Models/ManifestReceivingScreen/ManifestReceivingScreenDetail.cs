﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ManifestReceivingScreen
{
    public class ManifestReceivingScreenDetail
    {
        public int No { set; get; }
        public string ITEM_NO { set; get; }
        public string PART_NO { set; get; }
        public string KANBAN_NO { set; get; }
        public string PART_NAME { set; get; }
        public string KANBAN_PRNT_ADDRESS { set; get; }
        public int ORDER_QTY { set; get; }
        public int ALREADY_RECEIVED_QTY { set; get; }
        public int RECEIVED_QTY { set; get; }
        public int RECEIVED_STATUS { set; get; }
        public int SHORTAGE_QTY { set; get; }
        public int MISSPART_QTY { set; get; }
        public int DAMAGE_QTY { set; get; }
        public string Notice { set; get; }
        public int PROBLEM_FLAG { set; get; }
        public int ORDER_PLAN_QTY_LOT { set; get; }
    }
}