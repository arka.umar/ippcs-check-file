﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.SupplierPerformance
{
    public class SPSafety
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public float QTY_FATAL { get; set; }
        public float QTY_DISABILITY { get; set; }
        public float QTY_LOST_ORGAN { get; set; }
        public float QTY_ABSENT { get; set; }
        public float QTY_ABSENT_PERIOD { get; set; }
        public float QTY_SMALL_INJURED { get; set; }
        public float QTY_TOTAL_WORKERS_MH { get; set; }
        public string JUDGEMENT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPSafetyError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        public string Fatal { get; set; }
        public string Disability { get; set; }
        public string LostOrgant { get; set; }
        public string Absent { get; set; }
        public string AbsentPeriode { get; set; }
        public string SmallInjured { get; set; }
        public string TotalWorkers { get; set; }
        public string Judgment { get; set; }
        public string ErrorMessage { get; set; }		
    }
}