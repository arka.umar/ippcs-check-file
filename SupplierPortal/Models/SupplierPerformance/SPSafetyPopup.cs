﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.SupplierPerformance
{
    public class SPSafetyPopup
    {
        public string STOP_6 { get; set; }
        public string STOP_NAME { get; set; }
        public float PHF_A_PREV { get; set; }
        public float PHF_B_PREV { get; set; }
        public float PHF_C_PREV { get; set; }
        public string PHF_A_NEXT { get; set; }
        public string PHF_B_NEXT { get; set; }
        public string PHF_C_NEXT { get; set; }
        public float COUNT_A_PERM { get; set; }
        public float COUNT_A_TEMP { get; set; }
        public float COUNT_B_PERM { get; set; }
        public float COUNT_B_TEMP { get; set; }
        public float COUNT_C_PERM { get; set; }
        public float COUNT_C_TEMP { get; set; }
    }
}