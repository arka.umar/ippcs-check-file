﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformance
{
    public class SPShortage
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string RCV_AREA { get; set; }
        public int? SHORTAGE_QTY { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPShortageError
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string RECEIVING_AREA { get; set; }
        public string SHORTAGE { get; set; }
        public string ErrorMessage { get; set; }

    }
}
