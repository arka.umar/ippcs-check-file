﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Portal.Models.SupplierPerformance
{
    public class SPUploadError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        public string PartNo { get; set; }
        public float QtyDefect { get; set; }
        public string Problem { get; set; }
        public string ReceivingArea { get; set; }
    }

    //    public float SupplyPerMonth	 { get; set; }
    //    public string Delay	 { get; set; }
    //    public float Shortage	 { get; set; }
    //    public float Incorrect	 { get; set; }
    //    public string Probem { get; set; }

    //    public float Fatal { get; set; }
    //    public float Disability { get; set; }
    //    public float LostOrgant { get; set; }
    //    public float Absent { get; set; }
    //    public float AbsentPeriode { get; set; }
    //    public float SmallInjured { get; set; }
    //    public float TotalWorkers { get; set; }
    //    public string Judgment { get; set; }
		
    //    public float Ori { get; set; }
    //    public float Adj { get; set; }
    //    public float QtyReceive { get; set; }
    //    public float NGQuality  { get; set; }
    //    public float NGWorkability { get; set; }
		
    //    public float Claim { get; set; }
    //    public float CostShare { get; set; }
    //    public float _3YVol { get; set; }
    //    public string ErrorMessage { get; set; }
    //}
}