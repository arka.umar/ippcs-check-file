﻿using System;
using System.Collections.Generic;
using System.Web;


namespace Portal.Models.SupplierPerformance
{
    public class SPModel
    {
        public List<SPQuality> ListSPQuality { set; get; }
        public List<SPDelivery> ListSPDelivery { set; get; }
        public List<SPSafety> ListSPSafety { set; get; }
        public List<SPSafetyPopup> ListSPSafetyPopup { set; get; }
        public List<SPTWS> ListSPTWC { set; get; }
        public List<SPSPD> ListSPSPD { set; get; }
        public List<SPReceive> ListSPReceive { set; get; }
        public List<SPMonthYear> ListSPMonthYear { set; get; }

        public SPModel()
        {
            ListSPQuality = new List<SPQuality>();
            ListSPDelivery = new List<SPDelivery>();
            ListSPSafety = new List<SPSafety>();
            ListSPSafetyPopup = new List<SPSafetyPopup>();
            ListSPTWC = new List<SPTWS>();
            ListSPSPD = new List<SPSPD>();
            ListSPReceive = new List<SPReceive>();
            ListSPMonthYear = new List<SPMonthYear>();
        }
    }

    public class SPMonthYear
    {
        public string MonthYear { get; set; }
    }
    //add riani (20221019)
    public class SPYear
    {
        public string Year { get; set; }
    }
    //-------------//
}