﻿                                                                                                                                                                                                        using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.SupplierPerformance
{
    public class SPSPD
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        //public string PART_NO { get; set; }
        //public string PART_NAME { get; set; }
        public float QTY_ORI { get; set; }
        public float QTY_ADJ { get; set; }
        public float QTY_RECEIVE { get; set; }
        public float QTY_NG_QUALITY { get; set; }
        public float QTY_NG_WORKABILITY { get; set; }
        //public string DELIVERY_PROBLEM { get; set; }
        //public string    QUALITY_PROBLEM { get; set; }
        //public string WORKABIILITY_PROBLEM { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPSPDError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        //public string PartNo { get; set; }
        public string Ori { get; set; }
        public string Adj { get; set; }
        public string QtyReceive { get; set; }
        public string NGQuality { get; set; }
        public string NGWorkability { get; set; }
        public string ErrorMessage { get; set; }
    }
}