﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformance
{
    public class SPLineStop
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string RCV_AREA { get; set; }
        public int? MIN_LINE_STOP { get; set; }
        public string PROBLEM { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }


    public class SPLineStopError
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string RECEIVING_AREA { get; set; }
        public string MIN_LINE_STOP { get; set; }
        public string PROBLEM { get; set; }
        public string ErrorMessage { get; set; }
    }
}
