﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformance
{
    public class SPOntime
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PROBLEM { get; set; }
        public string RCV_AREA { get; set; }
        public int? SUPPLY_PER_MONTH { get; set; }
        public int? DELAY_FREQ { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPOntimeError
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string RECEIVING_AREA { get; set; }
        public string DELAY_FREQ { get; set; }
        public string SUPPLY_PER_MONTH { get; set; }
        public string PROBLEM { get; set; }
        public string ErrorMessage { get; set; }
    }
}
