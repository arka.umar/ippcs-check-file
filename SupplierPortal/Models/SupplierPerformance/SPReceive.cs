﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformance
{
    public class SPReceive
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        //commented by FID.Yusuf { get; set; } 27 OKTOBER 2014 { get; set; } Recomended by ISTD.Mangestya Argyaputri
        //public string PART_NO { get; set; }
        //public string PART_NAME { get; set; }
        public float QTY_RECEIVE { get; set; }
        public string RECEIVING_AREA { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPReceiveError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        //commented by FID.Yusuf { get; set; } 27 OKTOBER 2014 { get; set; } Recomended by ISTD.Mangestya Argyaputri
        //public string PartNo { get; set; }
        public string QtyReceive { get; set; }
        public string ReceivingArea { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class SPReceiveReportTMMIN
    {
        public string ROW_NO { get; set; }
        public string PROD_MONTH { get; set; }
        public string SUPP_CD { get; set; }
        public string SUPP_NAME { get; set; }

        public string RECEIVING_AREA { get; set; }
        public string QTY { get; set; }
        public string TOTAL_QTY { get; set; }
    }

    public class SPReceiveReportTMMINPivot
    {
        public SPReceiveReportTMMINPivot()
        {
            if (this.Assy_Str_1 == null) this.Assy_Str_1 = "0";
            if (this.Casting_Part == null) this.Casting_Part = "0";
            if (this.Finish_Part == null) this.Finish_Part = "0";
            if (this.Mach_Str_1 == null) this.Mach_Str_1 = "0";
            if (this.Other_Dock == null) this.Other_Dock = "0";
            if (this.Pack_Str_1 == null) this.Pack_Str_1 = "0";
            if (this.PAD_Str_1 == null) this.PAD_Str_1 = "0";
            if (this.Paint_Str_1 == null) this.Paint_Str_1 = "0";
            if (this.Weld_Str_1 == null) this.Weld_Str_1 = "0";
            if (this.Pack_Str_2 == null) this.Pack_Str_2 = "0";
            if (this.Assy_Krw == null) this.Assy_Krw = "0";
            if (this.Pack_Krw == null) this.Pack_Krw = "0";
            if (this.PAD_Krw == null) this.PAD_Krw = "0";
            if (this.Paint_Krw == null) this.Paint_Krw = "0";
            if (this.Weld_Krw == null) this.Weld_Krw = "0";
            if (this.Assy_Krw_2 == null) this.Assy_Krw_2 = "0";
            if (this.PAD_CY_Krw_2 == null) this.PAD_CY_Krw_2 = "0";
            if (this.PAD_Krw_2 == null) this.PAD_Krw_2 = "0";
            if (this.Paint_Krw_2 == null) this.Paint_Krw_2 = "0";
            if (this.PLD_Krw_2 == null) this.PLD_Krw_2 = "0";
            if (this.Stamp_Krw_2 == null) this.Stamp_Krw_2 = "0";
            if (this.Weld_Krw_2 == null) this.Weld_Krw_2 = "0";
            if (this.Engine_Krw_3 == null) this.Engine_Krw_3 = "0";
            if (this.Packing_Krw_3 == null) this.Packing_Krw_3 = "0";
            if (this.PAD_CY_Warehouse_Krw_3 == null) this.PAD_CY_Warehouse_Krw_3 = "0";
            if (this.PAD_Krw_3 == null) this.PAD_Krw_3 = "0";
            if (this.Fukoku == null) this.Fukoku = "0";
            if (this.GKD == null) this.GKD = "0";
            if (this.IGP == null) this.IGP = "0";
            if (this.Sugity == null) this.Sugity = "0";
            if (this.TDW == null) this.TDW = "0";
            if (this.WEP == null) this.WEP = "0";
        }

        public string ROW_NO { get; set; }
        public string PROD_MONTH { get; set; }
        public string SUPP_CD { get; set; }
        public string SUPP_NAME { get; set; }
        public string Assy_Str_1 { get; set; }
        public string Casting_Part { get; set; }
        public string Finish_Part { get; set; }
        public string Mach_Str_1 { get; set; }
        public string Other_Dock { get; set; }
        public string Pack_Str_1 { get; set; }
        public string PAD_Str_1 { get; set; }
        public string Paint_Str_1 { get; set; }
        public string Weld_Str_1 { get; set; }
        public string Pack_Str_2 { get; set; }
        public string Assy_Krw { get; set; }
        public string Pack_Krw { get; set; }
        public string PAD_Krw { get; set; }
        public string Paint_Krw { get; set; }
        public string Weld_Krw { get; set; }
        public string Assy_Krw_2 { get; set; }
        public string PAD_CY_Krw_2 { get; set; }
        public string PAD_Krw_2 { get; set; }
        public string Paint_Krw_2 { get; set; }
        public string PLD_Krw_2 { get; set; }
        public string Stamp_Krw_2 { get; set; }
        public string Weld_Krw_2 { get; set; }
        public string Engine_Krw_3 { get; set; }
        public string Packing_Krw_3 { get; set; }
        public string PAD_CY_Warehouse_Krw_3 { get; set; }
        public string PAD_Krw_3 { get; set; }
        public string Fukoku { get; set; }
        public string GKD { get; set; }
        public string IGP { get; set; }
        public string Sugity { get; set; }
        public string TDW { get; set; }
        public string WEP { get; set; }
        public string TOTAL_QTY { get; set; }
    }
}