﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.SupplierPerformance
{
    public class SPKPI
    {
        public string PROD_MONTH { get; set; }       
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string Q1_SAFETY { get; set; }
        public string Q2_SAFETY { get; set; }
        public string Q3_SAFETY { get; set; }
        public string Q4_SAFETY { get; set; }
        public string TARGET_SAFETY { get; set; }
        public string ACHIEVEMENT_SAFETY { get; set; }
        public string Q1_QUALITY { get; set; }
        public string Q2_QUALITY { get; set; }
        public string Q3_QUALITY { get; set; }
        public string Q4_QUALITY { get; set; }
        public string TARGET_QUALITY { get; set; }
        public string ACHIEVEMENT_QUALITY { get; set; }

        public string Q1_DELIVERY { get; set; }
        public string Q2_DELIVERY { get; set; }
        public string Q3_DELIVERY { get; set; }
        public string Q4_DELIVERY { get; set; }
        public string TARGET_DELIVERY { get; set; }
        public string ACHIEVEMENT_DELIVERY { get; set; }

        public string Q1_ENVIRONMENT { get; set; }
        public string Q2_ENVIRONMENT { get; set; }
        public string Q3_ENVIRONMENT { get; set; }
        public string Q4_ENVIRONMENT { get; set; }
        public string TARGET_ENVIRONMENT { get; set; }
        public string ACHIEVEMENT_ENVIRONMENT { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPKPIError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        public string Q1_SAFETY { get; set; }
        public string Q2_SAFETY { get; set; }
        public string Q3_SAFETY { get; set; }
        public string Q4_SAFETY { get; set; }
        public string TARGET_SAFETY { get; set; }
        public string ACHIEVEMENT_SAFETY { get; set; }
        public string Q1_QUALITY { get; set; }
        public string Q2_QUALITY { get; set; }
        public string Q3_QUALITY { get; set; }
        public string Q4_QUALITY { get; set; }
        public string TARGET_QUALITY { get; set; }
        public string ACHIEVEMENT_QUALITY { get; set; }

        public string Q1_DELIVERY { get; set; }
        public string Q2_DELIVERY { get; set; }
        public string Q3_DELIVERY { get; set; }
        public string Q4_DELIVERY { get; set; }
        public string TARGET_DELIVERY { get; set; }
        public string ACHIEVEMENT_DELIVERY { get; set; }

        public string Q1_ENVIRONMENT { get; set; }
        public string Q2_ENVIRONMENT { get; set; }
        public string Q3_ENVIRONMENT { get; set; }
        public string Q4_ENVIRONMENT { get; set; }
        public string TARGET_ENVIRONMENT { get; set; }
        public string ACHIEVEMENT_ENVIRONMENT { get; set; }
        public string ErrorMessage { get; set; }
    }
}