﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.SupplierPerformance
{
    public class SPTWS
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public float QTY_CLAIM { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPTWSError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        public string Claim { get; set; }
        public string CostShare { get; set; }
        public string _3YVol { get; set; }
        public string ErrorMessage { get; set; }
    }
}