﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.SupplierPerformance
{
    public class SPEnvironmentMaster
    {
        public string PROD_YEAR { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string CO2_REDUCTION { get; set; }
        public string CO2_EMISSION { get; set; }    
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class SPEnvironmentMasterError
    {
        public string ProductionYear { get; set; }
        public string SupplierCode { get; set; }
        public string CO2Reduction { get; set; }
        public string CO2Emission { get; set; }
        public string ErrorMessage { get; set; }
    }
}