﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformance
{
    public class SPWorkAbility
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        //remark by alira.agi 2015-05-26
        //public float QTY_RECEIVE { get; set; }
        public int QTY_RECEIVE { get; set; }
        //remark by alira.agi 2015-05-26
        //public string NG_QUALITY { get; set; }
        public int? NG_QUALITY { get; set; }
        public string DESC_QUALITY { get; set; }
        public string TIME1 { get; set; }
        //remark by alira.agi 2015-05-26
        //public string NG_WORKABILITY { get; set; }
        public int? NG_WORKABILITY { get; set; }
        public string DESC_WORKABILITY { get; set; }
        public string TIME2 { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public int? NG_DELIVERY { get; set; }
        public string DESC_DELIVERY { get; set; }
        public string TIME3 { get; set; }
    }


    public class SPWorkAbilityError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        public string PartNo { get; set; }
        public string QtyReceive { get; set; }
        public string NGQuality { get; set; }
        public string DescQuality { get; set; }
        public string ErrorMessage { get; set; }
    }
}