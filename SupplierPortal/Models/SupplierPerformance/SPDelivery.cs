﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace Portal.Models.SupplierPerformance
{
    public class SPDelivery
    {
        public string PROD_MONTH { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        //commented by FID.Yusuf, 27 OKTOBER 2014, Recomended by ISTD.Mangestya Argyaputri
        //public string PART_NO { get; set; }
        //public string PART_NAME { get; set; }
        public float QTY_SUPPLY_MONTH { get; set; }
        public float QTY_DELAY { get; set; }
        public string RECEIVING_AREA { get; set; }
        public float QTY_SHORTAGE { get; set; }
        public float QTY_INCORRECT { get; set; }
        public string PROBLEM_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT{ get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
    }

    public class SPDeliveryError
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        //commented by FID.Yusuf, 27 OKTOBER 2014, Recomended by ISTD.Mangestya Argyaputri
        //public string PartNo { get; set; }
        public string SupplyPerMonth { get; set; }
        public string delay { get; set; }
        public string ReceivingArea { get; set; }
        public string shortage { get; set; }
        public string incorrect { get; set; }
        public string probem { get; set; }
        public string ErrorMessage { get; set; }
    }
}