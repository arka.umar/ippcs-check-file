﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SuppConv
{
    public class SuppConvData
    {
       public string MANIFEST_NO { set; get; }
       public string ORDER_NO { set; get; }
       public string PART_NO { set; get; }
       public string PART_NAME { set; get; }
       public string QTY_PER_CONTAINER { set; get; }
       public string ORDER_QTY { set; get; }
       public DateTime? ARRIVAL_PLAN_DT { set; get; }
       public DateTime? ARRIVAL_ACTUAL_DT { set; get; }
       public string SUPPLIER_CODE_PLANT_TMMIN { set; get; }
       public string SUPPLIER_CODE_TMMIN { set; get; }
        
       public string SUPPLIER_NAME { set; get; }
       public string SUPPLIER_PLANT_TMMIN { set; get; }
       public string SUPPLIER_CODE_ADM { set; get; }
       public string SUPPLIER_PLANT_ADM { set; get; }
    

       public string CREATED_BY { set; get; }
       public DateTime? CREATED_DT { set; get; }
       public string CHANGED_BY { set; get; }
       public DateTime? CHANGED_DT { set; get; }
    }
    public class SuppConvDataDownload
    {
        
        public string SUPPLIER_CODE_TMMIN { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string SUPPLIER_PLANT_TMMIN { set; get; }
        public string SUPPLIER_CODE_ADM { set; get; }
        public string SUPPLIER_PLANT_ADM { set; get; }


       
    }
    public class SuppConvDataUpload
    {

        public string SUPPLIER_CODE_TMMIN { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string SUPPLIER_PLANT_TMMIN { set; get; }
        public string SUPPLIER_CODE_ADM { set; get; }
        public string SUPPLIER_PLANT_ADM { set; get; }



    }
    public class SuppConvDataUploadError
    {

        public string SUPPLIER_CODE_TMMIN { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string SUPPLIER_PLANT_TMMIN { set; get; }
        public string SUPPLIER_CODE_ADM { set; get; }
        public string SUPPLIER_PLANT_ADM { set; get; }
        public string MESSAGE { set; get; }



    }
}