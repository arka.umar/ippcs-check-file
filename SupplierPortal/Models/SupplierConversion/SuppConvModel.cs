﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SuppConv
{
    public class SuppConvModel
    {
        public List<SuppConvData> SuppConvDatas { set; get; }

        public SuppConvModel()
        {
            SuppConvDatas = new List<SuppConvData>();
        }
    }
}