﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.RoleMaster
{
    public class RoleData
    {
        public string SYSTEM_NAME { get; set; }
        public string ROLE_ID { get;set; }
        public string ROLE_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DATE { get; set; }
    }

    public class gridSystem 
    {
        public string SYSTEM_ID { get; set; }
        public string SYSTEM_NAME { get; set; }
    }
}

