﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.RoleMaster
{
    public class RoleModel
    {
        public List<RoleData> RoleDatas { set; get; }

        public RoleModel()
        {
            RoleDatas = new List<RoleData>();
        }

    }
}
