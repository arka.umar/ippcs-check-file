﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using Toyota.Common.Web.Database;
using System.Reflection; 

namespace Portal.Models
{
    public class Sing : IDisposable
    {
        private static Sing _me = null;
        public static Sing Me
        {
            get
            {
                if (_me == null)
                    _me = new Sing();
                return _me;
            }
        }
        
        private MemoryCache _mem = null;

        public MemoryCache Mem
        {
            get
            {
                if (_mem == null)
                {
                    _mem = MemoryCache.Default;
                }
                return _mem;
            }
        }
        IDBContext _db = null;
        public IDBContext DB
        {
            get
            {
                if (_db == null)
                {
                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                List<string> cacheKeys = Mem.Select(a => a.Key).ToList();
                foreach (string k in cacheKeys)
                {
                    IDisposable d = Mem[k] as IDisposable;
                    if (d != null) d.Dispose();
                    Mem.Remove(k);
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Set(string key, object value, int ageSeconds = -300)
        {
            if (value == null)
            {
                Mem.Remove(key);
                return;
            }
            CacheItemPolicy cip = new CacheItemPolicy();

            if (ageSeconds < 0) /// sliding expiration 
            {
                cip.SlidingExpiration = TimeSpan.FromSeconds(ageSeconds * -1);
            }
            else if (ageSeconds > 0) /// absolute expiration 
            {
                cip.AbsoluteExpiration = new DateTimeOffset(DateTime.Now, TimeSpan.FromSeconds(ageSeconds));
            }
            else // never expire 
            {
                cip.AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration;
            }
            Mem.Set(key, value, cip);
        }

        public int GetInt(string key)
        {
            CacheItem ci = Mem.GetCacheItem(key);
            Int32 y = 0;

            if (ci != null && ci.Value != null)
            {
                y = (Int32) ci.Value;
            }
            
            return y;
        }

        public int SetInt(string key, int? val = null)
        {
            CacheItem ci = Mem.GetCacheItem(key);
            Int32 y = 0;

            if (ci != null && ci.Value != null)
            {
                y = (Int32)ci.Value;
            }

            if (val.HasValue)
            {
                Int32 x = new Int32();
                x = val.Value;
                Mem.Set(key, x, new CacheItemPolicy() { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });
                if (ci == null || ci.Value == null)
                {
                    y = x;
                }
            }
            else
            {
                Int32 z = new Int32();
                z = y + 1;
                Mem.Set(key, z, new CacheItemPolicy() { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });
                y = z;
            }
            return y;
        }
             
        public T Get<T>(string key)
        {
            string tipe = typeof(T).Name;

            if (string.IsNullOrEmpty(key))
                key = tipe;

            CacheItem c = Mem.GetCacheItem(key);
            if ((c != null)
                && (c.Value.GetType().Equals(tipe)
                    || tipe.Equals("Object")
                   )
               )
            {
                if (!key.StartsWith("COUNT.")) SetInt("COUNT." + key); 
                return (T)c.Value;
            }
            return default(T);
        }

        public string AppData
        {
            get
            {
                string p = Get<string>("AppData");
                if (!string.IsNullOrEmpty(p)) return p;

                p = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "ToYoTa", "Portal");
                if (!Directory.Exists(p))
                    Directory.CreateDirectory(p);
                Set("AppData", p);
                return p;
            }
        }

        public string AppBuild
        {
            get
            {
                string b = Get<string>("AppBuild");
                if (!string.IsNullOrEmpty(b)) return b;

                DateTime dt= File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location);
                b = dt.ToString("yyyy-MM-dd HH:mm:ss");
                Set("AppBuild", b);
                return b; 
            }
        }

        public string AppStarted
        {
            get
            {
                string d = Get<string>("AppStarted");
                if (string.IsNullOrEmpty(d))
                {
                    d = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    Set("AppStarted", d);
                }
                return d;
            }
        }

        public long LogID
        {
            get; set; 
        }

        public string GetSaveFileName(string serviceName, string Parameters = "", string ext = ".txt")
        {
            return Path.Combine(Sing.Me.AppData, 
                    (serviceName + 
                       ((!string.IsNullOrEmpty(Parameters)) ? "("  + Parameters + ")" : "" )
                      ).SanitizeFilename() 
                    + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") 
                    + (string.IsNullOrEmpty(ext) ? ".json" : ext));
        }

        Configu _cfg = null;

        public Configu Cfg
        {
            get
            {
                if (_cfg == null)
                {
                    _cfg = Get<Configu>(typeof(Configu).Name);
                    if (_cfg == null)
                    _cfg = new Configu();
                    Set(typeof(Configu).Name, _cfg);
                }
                return _cfg;
            }
        }

        MessageHolder _meho = null;
        public MessageHolder Msg 
        {
            get
            {
                if (_meho == null)
                {
                    _meho = Get<MessageHolder>("ListMessages");
                    if (_meho == null)
                        _meho = new MessageHolder();
                    Set("ListMessages", _meho); 
                }
                return _meho;
            }
        }
    }
}