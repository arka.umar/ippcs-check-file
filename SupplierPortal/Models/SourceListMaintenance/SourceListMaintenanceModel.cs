﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SourceListMaintenance
{
    public class SourceListMaintenanceModel
    {
        public List<SourceListMaintenance> sourceList { get; set; }

        public SourceListMaintenanceModel()
        {
            sourceList = new List<SourceListMaintenance>();
        }
    }
}