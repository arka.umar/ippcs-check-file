﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SourceListMaintenance
{
    public class SourceListMaintenance
    {
        public string MAT_NO { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string SOURCE_NAMEE { get; set; }
        public string PROD_PURPOSE_CD { get; set; }
        public string PROD_PURPOSE_NAME { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public string MAT_DESC { get; set; }
        public string SUPP_CD { get; set; }
        public string SUPP_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string DELETION_FLAG { get; set; }
        public string VALID_DT_FR { get; set; }
        public string VALID_DT_TO { get; set; }

        public string SUPP_CD_BUYER { get; set; }
        public string BUYER_CD { get; set; }

        public string materialLookup { get; set; }

        public string ERR_MSG { get; set; }
        public int? SEQ { get; set; }
        public long? CRC { get; set; }
    }
}