﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.POA
{
    public class POAlist
    {
        public POAlist()
        {
            POAlistmodel = new List<POAmaster>();
        }

        public List<POAmaster> POAlistmodel { get; set; }
    }
}