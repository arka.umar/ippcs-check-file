﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.POA
{
    public class POAmaster
    {

        public string New { get; set; }
        public string Edit { get; set; }
        public string Delete { get; set; }
        public string POA_NUMBER { get; set; }
        public string GRANTOR { get; set; }
        public string ATTORNEY { get; set; }
        public string REASON { get; set; }
        public string  OFFICE_STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
    }
}