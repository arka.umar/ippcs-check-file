﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;
namespace Portal.Models.StationMappingMaster
{
    public class StationMappingModel
    {
       
        public List<StationMappingMaster> StationMappingMaster { get; set; }
        public List<StationMappingMasterTemp> TempGridContent { get; set; }
        public StationMappingModel()
        {
            StationMappingMaster = new List<StationMappingMaster>();
            TempGridContent = new List<StationMappingMasterTemp>();
        }   
    }

    public class DoorCodeCheckExists
    {
        public int resultCount { get; set; }
    }
}