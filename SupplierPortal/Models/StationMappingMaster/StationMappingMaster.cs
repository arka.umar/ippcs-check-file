﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.StationMappingMaster
{
    public class StationMappingMaster
    {
        public string DOOR_CD_TLMS { get; set; }
        public string DOCK_CD { get; set; }
        public string PHYSICAL_DOCK { get; set; }
        public string PHYSICAL_STATION { get; set; }
        public string USED_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
    }
}