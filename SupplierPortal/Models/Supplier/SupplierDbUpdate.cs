﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class SupplierDbUpdate
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME{ set; get; }
        public string CONFIRM_BY_TMMIN { set; get; }
        public DateTime? CONFIRM_DT_TMMIN { set; get; }
        public Boolean General { set; get; }
        public Boolean Address { set; get; }
        public Boolean ContactPerson { set; get; }
        public Boolean ShareHolder { set; get; }
        public Boolean Customer { set; get; }
        public Boolean Financial { set; get; }
        public Boolean ManPower { set; get; }
        public Boolean Labor { set; get; }
        public Boolean CadIt { set; get; }
        public Boolean PurchasedPart { set; get; }
        public Boolean Award { set; get; }
        public Boolean Product { set; get; }
        public Boolean BusinessRelation { set; get; }
        public Boolean Equipment { set; get; }
        public Boolean Export { set; get; }

        public String GeneralPic { set; get; }
        public String AddressPic { set; get; }
        public String ContactPersonPic { set; get; }
        public String ShareHolderPic { set; get; }
        public String CustomerPic { set; get; }
        public String FinancialPic { set; get; }
        public String ManPowerPic { set; get; }
        public String LaborPic { set; get; }
        public String CadItPic { set; get; }
        public String PurchasedPartPic { set; get; }
        public String AwardPic { set; get; }
        public String ProductPic { set; get; }
        public String BusinessRelationPic { set; get; }
        public String EquipmentPic { set; get; }
        public String ExportPic { set; get; }

    }
}