﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierCADIT
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string CAD_TYPE { set; get; }
        public int ITEM_NO { set; get; }
        public string TERMINAL_AMOUNT { set; get; }
        public Boolean CAM_COMPLETED { set; get; }
        public int ENGINEER { set; get; }
        public string INTERNET_BANDWITH { set; get; }
        public int OPERATOR { set; get; }
        public Boolean PRINTER { set; get; }
        public string PRINTER_SIZE { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }

        public string TERMINAL_AMOUNT_F { set; get; }
        public string ENGINEER_F { set; get; }
        public string OPERATOR_F { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierCADITSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string TERMINAL_AMOUNT { set; get; }
        public string ENGINEER { set; get; }
        public string INTERNET_BANDWITH { set; get; }
        public string OPERATOR { set; get; }
        public string PRINTER { set; get; }
        public string CAM_COMPLETED { set; get; }
        public string PRINTER_SIZE { set; get; }
        public string CAD_TYPE { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }

    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierCADITErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string TERMINAL_AMOUNT { set; get; }
        public string ENGINEER { set; get; }
        public string INTERNET_BANDWITH { set; get; }
        public string OPERATOR { set; get; }
        public string PRINTER { set; get; }
        public string CAM_COMPLETED { set; get; }
        public string PRINTER_SIZE { set; get; }
        public string CAD_TYPE { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}