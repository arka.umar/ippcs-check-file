﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierContactPerson
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string ITEM_NO { set; get; }
        public string POSITION_CD { set; get; }
        public string POSITION_NAME { set; get; }
        public string AREA_CD { set; get; }
        public string AREA_NAME { set; get; }
        public string NAME { set; get; }
        public string TELEPHONE { set; get; }
        public string MOBILE { set; get; }
        public string EMAIL { set; get; }
        public Boolean TMCLUB { set; get; }
        public Boolean BUYER { set; get; }
        public Boolean SPTT { set; get; }
        public Boolean ED { set; get; }
        public Boolean PCD { set; get; }
        public Boolean QD { set; get; }
        public Boolean PAD_LOGISTIC { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierContactPersonSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string POSITION_NAME { set; get; }
        public string AREA_NAME { set; get; }
        public string NAME { set; get; }
        public string TELEPHONE { set; get; }
        public string MOBILE { set; get; }
        public string EMAIL { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierContactPersonErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string POSITION_NAME { set; get; }
        public string AREA_NAME { set; get; }
        public string NAME { set; get; }
        public string TELEPHONE { set; get; }
        public string MOBILE { set; get; }
        public string EMAIL { set; get; }
        public string ERROR_MESSAGE { get; set; }
    }
    #endregion
}