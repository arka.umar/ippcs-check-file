﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierManPower
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public int ITEM_NO { set; get; }
        public string YEAR { set; get; }
        public int ENGINEERING { set; get; }
        public int PRODUCTION_CONTROL { set; get; }
        public int PRODUCTION_OPERATOR { set; get; }
        public int QUALITY_ASSURANCE { set; get; }
        public int OTHER { set; get; }
        public int PLANT_DIRECT { set; get; }
        public int NON_PLANT { set; get; }
        public int OTHER_WORKPLACE { set; get; }
        public int STATUS_PERMANENT { set; get; }
        public int STATUS_TEMPORER { set; get; }
        public int EDUCATION_S1_S2 { set; get; }
        public int EDUCATION_D3 { set; get; }
        public int EDUCATION_HIGH_SCHOOL { set; get; }
        public int NATIONALITY_EXPATRIATE { set; get; }
        public int NATIONALITY_WNI { set; get; }
        public int TOTAL_EMPLOYEE { set; get; }
        public string AVERAGE_AGE { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }

        public int ENGINEERING_F { set; get; }
        public int PRODUCTION_CONTROL_F { set; get; }
        public int PRODUCTION_OPERATOR_F { set; get; }
        public int QUALITY_ASSURANCE_F { set; get; }
        public int OTHER_F { set; get; }
        public int PLANT_DIRECT_F { set; get; }
        public int NON_PLANT_F { set; get; }
        public int OTHER_WORKPLACE_F { set; get; }
        public int STATUS_PERMANENT_F { set; get; }
        public int STATUS_TEMPORER_F { set; get; }
        public int EDUCATION_S1_S2_F { set; get; }
        public int EDUCATION_D3_F { set; get; }
        public int EDUCATION_HIGH_SCHOOL_F { set; get; }
        public int NATIONALITY_EXPATRIATE_F { set; get; }
        public int NATIONALITY_WNI_F { set; get; }
        public int TOTAL_EMPLOYEE_F { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierManPowerSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { get; set; }
        public string YEAR { set; get; }
        public string ENGINEERING { set; get; }
        public string PRODUCTION_CONTROL { set; get; }
        public string PRODUCTION_OPERATOR { set; get; }
        public string QUALITY_ASSURANCE { set; get; }
        public string LABOR_SUPPLY { set; get; }
        public string PLANT_DIRECT { set; get; }
        public string NON_PLANT { set; get; }
        public string STATUS_PERMANENT { set; get; }
        public string STATUS_TEMPORER { set; get; }
        public string EDUCATION_S1_S2 { set; get; }
        public string EDUCATION_D3 { set; get; }
        public string EDUCATION_HIGH_SCHOOL { set; get; }
        public string NATIONALITY_EXPATRIATE { set; get; }
        public string NATIONALITY_WNI { set; get; }
        public string TOTAL_EMPLOYEE { set; get; }
        public string AVERAGE_AGE { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierManPowerErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { get; set; }
        public string YEAR { set; get; }
        public string ENGINEERING { set; get; }
        public string PRODUCTION_CONTROL { set; get; }
        public string PRODUCTION_OPERATOR { set; get; }
        public string QUALITY_ASSURANCE { set; get; }
        public string LABOR_SUPPLY { set; get; }
        public string PLANT_DIRECT { set; get; }
        public string NON_PLANT { set; get; }
        public string STATUS_PERMANENT { set; get; }
        public string STATUS_TEMPORER { set; get; }
        public string EDUCATION_S1_S2 { set; get; }
        public string EDUCATION_D3 { set; get; }
        public string EDUCATION_HIGH_SCHOOL { set; get; }
        public string NATIONALITY_EXPATRIATE { set; get; }
        public string NATIONALITY_WNI { set; get; }
        public string TOTAL_EMPLOYEE { set; get; }
        public string AVERAGE_AGE { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}