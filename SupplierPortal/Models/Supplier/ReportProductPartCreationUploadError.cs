﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class ReportProductPartCreationUploadError
    {
        public String SupplierCode { get; set; }
        public String ProductName { get; set; }
        public String AutomotiveCustomerList { get; set; }
        public DateTime StartProduction { get; set; }
        public Double MarketShare { get; set; }
        public String Biggest { get; set; }
        public Double ShareOfCompetitor { get; set; }
        
        public String ERRORS_MESSAGE_CONCAT { set; get; }
    }
}