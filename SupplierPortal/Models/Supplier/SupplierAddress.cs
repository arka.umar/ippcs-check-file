﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierAddress
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string ITEM_NO { set; get; }
        public string LOCATION_FUNCTION_CD { set; get; }
        public string LOCATION_FUNCTION_NAME { set; get; }
        public string BUILDING { set; get; }
        public string FLOOR_BLOCK { set; get; }
        public string STREET { set; get; }
        public string CITY { set; get; }
        public string POSTAL_CODE { set; get; }
        public string TELEPHONE { set; get; }
        public string FAX { set; get; }
        public string SPEED_DIAL { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierAddressSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { get; set; }
        public string LOCATION_FUNCTION_NAME { set; get; }
        public string BUILDING { set; get; }
        public string FLOOR_BLOCK { set; get; }
        public string STREET { set; get; }
        public string CITY { set; get; }
        public string POSTAL_CODE { set; get; }
        public string TELEPHONE { set; get; }
        public string FAX { set; get; }
        public string SPEED_DIAL { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierAddressErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string LOCATION_FUNCTION_NAME { set; get; }
        public string BUILDING { set; get; }
        public string FLOOR_BLOCK { set; get; }
        public string STREET { set; get; }
        public string CITY { set; get; }
        public string POSTAL_CODE { set; get; }
        public string TELEPHONE { set; get; }
        public string FAX { set; get; }
        public string SPEED_DIAL { set; get; }
        public string ERROR_MESSAGE { get; set; }
    }
    #endregion
}