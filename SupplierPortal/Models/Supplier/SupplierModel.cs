﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class SupplierModel
    {
        private List<SupplierInfo> supplierList;

        public SupplierModel()
        {
            SupplierInfos = new List<SupplierInfo>();
            SupplierGenerals = new List<SupplierGeneral>();
            SupplierAddresss = new List<SupplierAddress>();
            SupplierContactPersons = new List<SupplierContactPerson>();
            SupplierShareHolders = new List<SupplierShareHolder>();
            SupplierCustomers = new List<SupplierCustomer>();
            SupplierFinancials = new List<SupplierFinancial>();
            SupplierManPowers = new List<SupplierManPower>();
            SupplierLabors = new List<SupplierLabor>();
            SupplierCADITs = new List<SupplierCADIT>();
            SupplierPurchasedParts = new List<SupplierPurchasedPart>();
            SupplierAwards = new List<SupplierAward>();
            SupplierProducts = new List<SupplierProduct>();
            SupplierBusinessRelations = new List<SupplierBusinessRelation>();
            SupplierEquipments = new List<SupplierEquipment>();
            SupplierExports = new List<SupplierExport>();
            SupplierNames = new List<SupplierName>();
            SupplierPlans = new List<SupplierPlan>();
            SupplierDbUpdate = new List<SupplierDbUpdate>();
        }

        public List<SupplierInfo> SupplierInfos { set; get; }
        public List<SupplierGeneral> SupplierGenerals { set; get; }
        public List<SupplierAddress> SupplierAddresss { set; get; }
        public List<SupplierContactPerson> SupplierContactPersons { set; get; }
        public List<SupplierShareHolder> SupplierShareHolders { set; get; }
        public List<SupplierCustomer> SupplierCustomers { set; get; }
        public List<SupplierFinancial> SupplierFinancials { set; get; }
        public List<SupplierManPower> SupplierManPowers { set; get; }
        public List<SupplierLabor> SupplierLabors { set; get; }
        public List<SupplierCADIT> SupplierCADITs { set; get; }
        public List<SupplierPurchasedPart> SupplierPurchasedParts { set; get; }
        public List<SupplierAward> SupplierAwards { set; get; }
        public List<SupplierProduct> SupplierProducts { set; get; }
        public List<SupplierBusinessRelation> SupplierBusinessRelations { set; get; }
        public List<SupplierEquipment> SupplierEquipments { set; get; }
        public List<SupplierExport> SupplierExports { set; get; }
        public List<SupplierName> SupplierNames { set; get; }
        public List<SupplierPlan> SupplierPlans { set; get; }
        public List<SupplierDbUpdate> SupplierDbUpdate { set; get; }
    }
}