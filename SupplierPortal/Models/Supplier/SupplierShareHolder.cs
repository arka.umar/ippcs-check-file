﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierShareHolder
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public int ITEM_NO { set; get; }
        public string COUNTRY_CD { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string SHAREHOLDER { set; get; }
        public string CURRENCY_CD { set; get; }
        public string CURRENCY_NAME { set; get; }
        public string AMOUNT { set; get; }
        public string SHARE_PERCENTAGE { set; get; }
        public String SHARE_PERCENTAGE_PERCENT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierShareHolderSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string SHAREHOLDER_NAME { set; get; }
        public string SHARE_PERCENTAGE { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidData
    public class SupplierShareHolderErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string SHAREHOLDER_NAME { set; get; }
        public string SHARE_PERCENTAGE { set; get; }
        public string ERROR_MESSAGE { get; set; }
    }
    #endregion
}