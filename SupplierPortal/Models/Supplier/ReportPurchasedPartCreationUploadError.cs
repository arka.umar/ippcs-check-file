﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class ReportPurchasedPartCreationUploadError
    {
        public String SupplierCode { get; set; }
        public String PartName { get; set; }
        public String CountryName { get; set; }
        public String SourceName { get; set; }

        public String ERRORS_MESSAGE_CONCAT { set; get; }
    }
}