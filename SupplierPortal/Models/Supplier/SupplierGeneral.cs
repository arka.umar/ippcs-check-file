﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierGeneral
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string COMPANY_STATUS { set; get; }
        public string CAPITAL_CURRENCY_CD { set; get; }
        public string CURRENCY_NAME { set; get; }
        public string CAPITAL_AMOUNT { set; get; }
        public string CAPITAL_AMOUNT_F { set; get; }
        public string YEAR_ESTABLISH { set; get; }
        public string YEAR_OPERATION { set; get; }
        public string WEBSITE { set; get; }
        public Boolean RELATION_1ST_TIER { set; get; }
        public Boolean RELATION_2ND_3RD_TIER { set; get; }
        public Boolean CONSUMABLE_AND_FACILITY { set; get; }
        public Boolean GROUP_TOYOTA { set; get; }
        public Boolean GROUP_ADM { set; get; }
        public Boolean GROUP_HMMI { set; get; }
        public Boolean CONFIRM_FLAG { set; get; }
        public string MODEL_INVOLVE_CD { set; get; }
        public string MODEL_NAME { set; get; }
        public string TMMIN_PIC { set; get; }
        public string MAIN_CONTACT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
        public string LAST_UPDATE_BY { set; get; }
        public DateTime? LAST_UPDATE_DT { set; get; }

        public String CONFIRM_FLAG_DB { set; get; }
        public String CONFIRM_FLAG_HURUF { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierGeneralSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string COMPANY_STATUS { set; get; }
        public string WEBSITE { set; get; }
        public string CAPITAL_CURRENCY_CD { set; get; }
        public double CAPITAL_AMOUNT { set; get; }
        public string YEAR_ESTABLISH { set; get; }
        public string YEAR_OPERATION { set; get; }
        public string MODEL_INVOLVE_NAME { set; get; }
        public string TMMIN_PIC { set; get; }
        public string MAIN_CONTACT { set; get; }
        public string RELATION_1ST_TIER { set; get; }
        public string RELATION_2ND_3RD_TIER { set; get; }
        public string CONSUMABLE_AND_FACILITY { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierGeneralErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { get; set; }
        public string COMPANY_STATUS { set; get; }
        public string WEBSITE { set; get; }
        public string CAPITAL_CURRENCY_CD { set; get; }
        public string CAPITAL_AMOUNT { set; get; }
        public string YEAR_ESTABLISH { set; get; }
        public string YEAR_OPERATION { set; get; }
        public string MODEL_INVOLVE_NAME { set; get; }
        public string TMMIN_PIC { set; get; }
        public string MAIN_CONTACT { set; get; }
        public string RELATION_1ST_TIER { set; get; }
        public string RELATION_2ND_3RD_TIER { set; get; }
        public string CONSUMABLE_AND_FACILITY { set; get; }
        public string ERROR_MESSAGE { get; set; }
    }
    #endregion
}