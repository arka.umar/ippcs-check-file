﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierLabor
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public int ITEM_NO { set; get; }
        public Boolean AVAILABILITY { set; get; }
        public String SINGLE_PLURAL { set; get; }
        public String UNION_NAME{ set; get; }
        public string UNION_CD { set; get; }
        public String HISTORY_STRIKE_1 { set; get; }
        public String HISTORY_STRIKE_2 { set; get; }
        public String HISTORY_STRIKE_3 { set; get; }
        public Boolean COLLECTIVE_LABOR_AGREEMENT { set; get; }
        public String COMMUNICATION_MEDIA { set; get; }
        public Boolean PENSION_PROGRAM { set; get; }
        public string BONUS { set; get; }
        public string SALARY_INCREASE { set; get; }
        public Boolean MEAL_TRANSPORT_ALLOWANCE { set; get; }
        public Boolean MEDICAL_ALLOWANCE { set; get; }
        public Boolean THR { set; get; }
        public Boolean OVERTIME { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierLaborSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string COMMUNICATION_MEDIA { set; get; }
        public string UNION_NAME { set; get; }
        public string SINGLE_PLURAL { set; get; }
        public string BONUS { set; get; }
        public string SALARY_INCREASE { set; get; }
        public string HISTORY_STRIKE_3 { set; get; }
        public string HISTORY_STRIKE_2 { set; get; }
        public string HISTORY_STRIKE_1 { set; get; }
        //public string AVAILABILITY { set; get; }
        public string PENSION_PROGRAM { set; get; }
        public string THR { set; get; }
        public string OVERTIME { set; get; }
        public string COLLECTIVE_LABOR_AGREEMENT { set; get; }
        public string MEAL_TRANSPORT_ALLOWANCE { set; get; }
        public string MEDICAL_ALLOWANCE { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierLaborErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string COMMUNICATION_MEDIA { set; get; }
        public string UNION_NAME { set; get; }
        public string SINGLE_PLURAL { set; get; }
        public string BONUS { set; get; }
        public string SALARY_INCREASE { set; get; }
        public string HISTORY_STRIKE_3 { set; get; }
        public string HISTORY_STRIKE_2 { set; get; }
        public string HISTORY_STRIKE_1 { set; get; }
        //public string AVAILABILITY { set; get; }
        public string PENSION_PROGRAM { set; get; }
        public string THR { set; get; }
        public string OVERTIME { set; get; }
        public string COLLECTIVE_LABOR_AGREEMENT { set; get; }
        public string MEAL_TRANSPORT_ALLOWANCE { set; get; }
        public string MEDICAL_ALLOWANCE { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}