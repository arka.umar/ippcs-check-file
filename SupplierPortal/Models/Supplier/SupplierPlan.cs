﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class SupplierPlan
    {
        public string SUPPLIER_PLANT_CD { set; get; }
        public string SUPPLIER_PLANT_NAME { set; get; }

    }
}