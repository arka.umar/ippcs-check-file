﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class SupplierInfo
    {
        public int SupplierCode { set; get; }
        public string SupplierName { set; get; }
        public string MainContactPerson { set; get; }
        public string YearOfEstablishment { set; get; }
        public string YearOfOperation { set; get; }
        public string LastUpdated { set; get; }
        public byte[] Status { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDate { set; get; }
        public string General { set; get; }
        public string Address { set; get; }
        public string ContactPerson { set; get; }
        public string ShareHolder { set; get; }
        public string Customer { set; get; }
        public string Financial { set; get; }
        public string ManPower { set; get; }
        public string Labor { set; get; }
        public string CADIT { set; get; }
        public string PurchasedPart { set; get; }
        public string Award { set; get; }
        public string Product { set; get; }
        public string BusinessRelation { set; get; }
        public string Equipment { set; get; }
        public string Export { set; get; }
    }
}