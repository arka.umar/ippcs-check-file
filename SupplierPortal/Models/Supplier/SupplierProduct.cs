﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierProduct
    {
       public string SUPPLIER_CD { set; get; }
       public string SUPPLIER_NAME { set; get; }
       public string CONFIRM_BY_SUPPLIER { set; get; }
       public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
       public string ITEM_NO { set; get; }
       public string PRODUCT_NAME { set; get; }
       public string AUTOMOTIVE_CUSTOMER_LIST { set; get; }
       public string AUTO_CUSTOMER_NAME { set; get; }  
       public string START_PRODUCTION { set; get; }
       public string MARKET_SHARE { set; get; }
       public string MARKET_SHARE_PERCENT { set; get; }
       public string BIGGEST { set; get; }
       public string SHARE_OF_COMPETITOR { set; get; }
       public string SHARE_OF_COMPETITOR_PERCENT { set; get; }
       public string CREATED_BY { set; get; }
       public DateTime? CREATED_DT { set; get; }
       public string CHANGED_BY { set; get; }
       public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierProductSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string PRODUCT_NAME { set; get; }
        public string AUTOMOTIVE_CUSTOMER_NAME { set; get; }
        public string START_PRODUCTION { set; get; }  
        public string MARKET_SHARE { set; get; }
        public string BIGGEST_COMPETITOR { set; get; }
        public string SHARE_OF_COMPETITOR { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierProductErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string PRODUCT_NAME { set; get; }
        public string AUTOMOTIVE_CUSTOMER_NAME { set; get; }
        public string START_PRODUCTION { set; get; }
        public string MARKET_SHARE { set; get; }
        public string BIGGEST_COMPETITOR { set; get; }
        public string SHARE_OF_COMPETITOR { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}