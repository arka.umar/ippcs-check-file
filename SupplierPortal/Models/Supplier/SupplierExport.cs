﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierExport
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string ITEM_NO { set; get; }
        public string COUNTRY_CD { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string MAJOR_PRODUCT { set; get; }
        public Boolean VIA_TMMIN { set; get; }
        public string SHARE_SALES { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierExportSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string MAJOR_PRODUCT { set; get; }
        public string VIA_TMMIN { set; get; }
        public string SHARE_SALES { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierExportErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string MAJOR_PRODUCT { set; get; }
        public string VIA_TMMIN { set; get; }
        public string SHARE_SALES { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}