﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class SupplierICS
    {
        public string    SUPP_CD { get; set; } // *
        public string    SUPP_NAME { get; set; } // *

        public string    PAYMENT_METHOD_CD { get; set; } // *
        public string    PAYMENT_TERM_CD { get; set; } // *

        public string    CREATED_BY { get; set; } // *
        public DateTime  CREATED_DT { get; set; } // *
        
        public string    SUPP_ADDR { get; set; }
        public string    SUPP_CITY { get; set; }
        public string    POSTAL_CD { get; set; } 

        public string    SUPP_EMAIL_ADDR1 { get; set; } 
        public string    SUPP_EMAIL_ADDR2 { get; set; } 
        
        public string    SUPP_SEARCH_TERM { get; set; } 
        
        public string    ORD_CURR { get; set; }
        public int       FREQ_SENDING { get; set; } 
        public string    FREQUENCIES_REMARK { get; set; } 
        public string    DELETION_FLAG { get; set; } 
        public string    SUPP_ATTENTION { get; set; } 
        public string    SUPP_ATTENTION_POSITION { get; set; } 
        public string    PHONE_NO { get; set; } 
        public string    FAX_NO { get; set; } 
        public string    INCOTERM { get; set; } 
        public string    SUPP_ATTENTION_2 { get; set; } 
        public string    SUPP_ATTENTION_POSITION_2 { get; set; } 
        
        public string    CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; } 
    }
}