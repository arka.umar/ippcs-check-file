﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierPurchasedPart
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string PART_NO { set; get; }
        public string PART_NAME { set; get; }
        public string COUNTRY_CD { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string SOURCE_STATUS { set; get; }
        public string SOURCE_NAME { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierPurchasedPartSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string PART_NO { set; get; }
        public string PART_NAME { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string SOURCE_STATUS { set; get; }
        public string SOURCE_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierPurchasedPartErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string PART_NAME { set; get; }
        public string COUNTRY_NAME { set; get; }
        public string SOURCE_STATUS { set; get; }
        public string SOURCE_NAME { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}