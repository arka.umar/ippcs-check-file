﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierFinancial
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string ITEM_NO { set; get; }
        public double SALES_AMOUNT_ACHIEVEMENT { set; get; }
        public double SALES_AMOUNT_FORECAST { set; get; }
        public string ACCT_PERIOD_FROM { set; get; }
        public string ACCT_PERIOD_TO { set; get; }
        public string OPERATING_PROFIT_ACHIEVEMENT { set; get; }
        public string OPERATING_PROFIT_FORECAST { set; get; }
        public double SALES_TO_TMMIN_ACHIEVEMENT { set; get; }
        public double SALES_TO_TMMIN_FORECAST { set; get; }
        public double SALES_TO_ADM_ACHIEVEMENT { set; get; }
        public double SALES_TO_ADM_FORECAST { set; get; }
        public string SALES_TO_OTHER_ACHIEVEMENT { set; get; }
        public string SALES_TO_OTHER_FORECAST { set; get; }
        public string ORDINARY_PROFIT_ACHIEVEMENT { set; get; }
        public string ORDINARY_PROFIT_FORECAST { set; get; }
        public string OPERATING_PROFIT_ACHIEVEMENT_PERCENT { set; get; }
        public string OPERATING_PROFIT_FORECAST_PERCENT { set; get; }
        public string SALES_TO_TMMIN_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_TMMIN_FORECAST_PERCENT { set; get; }
        public string SALES_TO_ADM_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_ADM_FORECAST_PERCENT { set; get; }
        public string SALES_TO_OTHER_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_OTHER_FORECAST_PERCENT { set; get; }
        public string ORDINARY_PROFIT_ACHIEVEMENT_PERCENT { set; get; }
        public string ORDINARY_PROFIT_FORECAST_PERCENT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }

        public string SALES_AMOUNT_ACHIEVEMENT_F { set; get; }
        public string SALES_AMOUNT_FORECAST_F { set; get; }
        public string OPERATING_PROFIT_ACHIEVEMENT_F { set; get; }
        public string OPERATING_PROFIT_FORECAST_F { set; get; }
        public string SALES_TO_TMMIN_ACHIEVEMENT_F { set; get; }
        public string SALES_TO_TMMIN_FORECAST_F { set; get; }
        public string SALES_TO_ADM_ACHIEVEMENT_F { set; get; }
        public string SALES_TO_ADM_FORECAST_F { set; get; }
        public string SALES_TO_OTHER_ACHIEVEMENT_F { set; get; }
        public string SALES_TO_OTHER_FORECAST_F { set; get; }
        public string ORDINARY_PROFIT_ACHIEVEMENT_F { set; get; }
        public string ORDINARY_PROFIT_FORECAST_F { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierFinancialSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string ACCT_PERIOD_FROM { set; get; }
        public string ACCT_PERIOD_TO { set; get; }
        public string SALES_AMOUNT_ACHIEVEMENT { set; get; }
        public string SALES_AMOUNT_FORECAST { set; get; }
        public string OPERATING_PROFIT_ACHIEVEMENT { set; get; }
        public string OPERATING_PROFIT_ACHIEVEMENT_PERCENT { set; get; }
        public string OPERATING_PROFIT_FORECAST { set; get; }
        public string OPERATING_PROFIT_FORECAST_PERCENT { set; get; }
        public string SALES_TO_TMMIN_ACHIEVEMENT { set; get; }
        public string SALES_TO_TMMIN_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_TMMIN_FORECAST { set; get; }
        public string SALES_TO_TMMIN_FORECAST_PERCENT { set; get; }
        public string SALES_TO_ADM_ACHIEVEMENT { set; get; }
        public string SALES_TO_ADM_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_ADM_FORECAST { set; get; }
        public string SALES_TO_ADM_FORECAST_PERCENT { set; get; }
        public string SALES_TO_OTHER_ACHIEVEMENT { set; get; }
        public string SALES_TO_OTHER_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_OTHER_FORECAST { set; get; }
        public string SALES_TO_OTHER_FORECAST_PERCENT { set; get; }
        public string ORDINARY_PROFIT_ACHIEVEMENT { set; get; }
        public string ORDINARY_PROFIT_ACHIEVEMENT_PERCENT { set; get; }
        public string ORDINARY_PROFIT_FORECAST { set; get; }
        public string ORDINARY_PROFIT_FORECAST_PERCENT { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierFinancialErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string ACCT_PERIOD_FROM { set; get; }
        public string ACCT_PERIOD_TO { set; get; }
        public string SALES_AMOUNT_ACHIEVEMENT { set; get; }
        public string SALES_AMOUNT_FORECAST { set; get; }
        public string OPERATING_PROFIT_ACHIEVEMENT { set; get; }
        public string OPERATING_PROFIT_ACHIEVEMENT_PERCENT { set; get; }
        public string OPERATING_PROFIT_FORECAST { set; get; }
        public string OPERATING_PROFIT_FORECAST_PERCENT { set; get; }
        public string SALES_TO_TMMIN_ACHIEVEMENT { set; get; }
        public string SALES_TO_TMMIN_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_TMMIN_FORECAST { set; get; }
        public string SALES_TO_TMMIN_FORECAST_PERCENT { set; get; }
        public string SALES_TO_ADM_ACHIEVEMENT { set; get; }
        public string SALES_TO_ADM_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_ADM_FORECAST { set; get; }
        public string SALES_TO_ADM_FORECAST_PERCENT { set; get; }
        public string SALES_TO_OTHER_ACHIEVEMENT { set; get; }
        public string SALES_TO_OTHER_ACHIEVEMENT_PERCENT { set; get; }
        public string SALES_TO_OTHER_FORECAST { set; get; }
        public string SALES_TO_OTHER_FORECAST_PERCENT { set; get; }
        public string ORDINARY_PROFIT_ACHIEVEMENT { set; get; }
        public string ORDINARY_PROFIT_ACHIEVEMENT_PERCENT { set; get; }
        public string ORDINARY_PROFIT_FORECAST { set; get; }
        public string ORDINARY_PROFIT_FORECAST_PERCENT { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}