﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierEquipment
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string ITEM_NO { set; get; }
        public string MACHINE_NAME { set; get; }
        public string DETAIL_TYPE { set; get; }
        public string TONAGE { set; get; }
        public int UNIT { set; get; }
        public string BRAND_NAME { set; get; }
        public int QTY { set; get; }
        public string YEAR_BUYING { set; get; }
        public string YEAR_MAKING { set; get; }
        public string REMARK { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }

        public string TONAGE_F { set; get; }
        public string UNIT_F { set; get; }
        public string QTY_F { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierEquipmentSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string MACHINE_NAME { set; get; }
        public string DETAIL_TYPE { set; get; }
        public string TONAGE { set; get; }
        public string UNIT { set; get; }
        public string BRAND_NAME { set; get; }
        public string QTY { set; get; }
        public string YEAR_BUYING { set; get; }
        public string YEAR_MAKING { set; get; }
        public string REMARK { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierEquipmentErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string MACHINE_NAME { set; get; }
        public string DETAIL_TYPE { set; get; }
        public string TONAGE { set; get; }
        public string UNIT { set; get; }
        public string BRAND_NAME { set; get; }
        public string QTY { set; get; }
        public string YEAR_BUYING { set; get; }
        public string YEAR_MAKING { set; get; }
        public string REMARK { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}