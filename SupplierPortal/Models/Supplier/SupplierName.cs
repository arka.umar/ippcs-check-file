﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    public class SupplierName
    {
        public string SUPPLIER_CODE { set; get; }
        public string SUPPLIER_PLANT_CD { set; get; }
        public string SUPPLIER_ABBREVIATION { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string SUPPLIER_CODE_NAME { set; get; }
        public string SUPPLIER_CODE_PLANT { set; get; }
    }

    public class SupplierNameSPEX
    {
        public string SUPPLIER_CODE_PLANT { set; get; }
        public string SUB_SUPPLIER_MAIN { set; get; }
        public string SUB_SUPPLIER_CODE { set; get; }
        public string SUB_SUPPLIER_PLANT_CD { set; get; }
        public string SUB_SUPPLIER_ABBREVIATION { set; get; }
        public string SUB_SUPPLIER_NAME { set; get; }
        public string SUB_SUPPLIER_CODE_NAME { set; get; }
        public string SUB_SUPPLIER_CODE_PLANT { set; get; }
    }
}