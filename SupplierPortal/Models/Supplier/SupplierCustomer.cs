﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Supplier
{
    #region GridModel
    public class SupplierCustomer
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public DateTime? CONFIRM_DT_SUPPLIER { set; get; }
        public string ITEM_NO { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string START_OF_SUPPLY { set; get; }
        public string SHARE_OF_SALES { set; get; }
        public int CUSTOMER_CATEGORY_CD { set; get; }
        public string CATEGORY_NAME { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadModel
    public class SupplierCustomerSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string START_OF_SUPPLY { set; get; }
        public string SHARE_OF_SALES { set; get; }
        public string CUSTOMER_CATEGORY_NAME { set; get; }
        public string CONFIRM_BY_SUPPLIER { set; get; }
        public string CONFIRM_DT_SUPPLIER { set; get; }
        public string CREATED_BY { set; get; }
        public string CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public string CHANGED_DT { set; get; }
    }
    #endregion

    #region DownloadInvalidDataModel
    public class SupplierCustomerErrorSheet
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string ITEM_NO { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string START_OF_SUPPLY { set; get; }
        public string SHARE_OF_SALES { set; get; }
        public string CUSTOMER_CATEGORY_NAME { set; get; }
        public string ERROR_MESSAGE { set; get; }
    }
    #endregion
}