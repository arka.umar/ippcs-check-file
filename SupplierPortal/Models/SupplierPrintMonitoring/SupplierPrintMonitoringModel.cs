﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPrintMonitoring
{
    public class SupplierPrintMonitoringModel
    {
        public List<SupplierPrintMonitoring> SupplierPrintMonitorings { get; set; }
        public SupplierPrintMonitoringModel()
        {
            SupplierPrintMonitorings = new List<SupplierPrintMonitoring>();
        }
    }
}