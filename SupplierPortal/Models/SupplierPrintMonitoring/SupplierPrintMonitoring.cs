﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPrintMonitoring
{
    public class SupplierPrintMonitoring
    {
        public DateTime ORDER_RELEASE_DT { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_CD_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string PLANT_NM { get; set; }
        public Int32 DOWNLOADED { get; set; }
        public string DOWNLOADED_BY { get; set; }
        public Int32 PRINTED { get; set; }
        public string PRINTED_BY { get; set; }
        public Int32 TOTAL_MANIFEST { get; set; }
        public DateTime SCHEDULE_PRINT_DT { get; set; }
        public string STATUS { get; set; }
    }
}