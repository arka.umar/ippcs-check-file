﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.IMPeriod
{
    public class IMPeriod
    {
        public int NO { get; set; }
        public int IMPERIOD { get; set; }
        public int IMYEAR { get; set; }
        public DateTime shiftdate { get; set; }
        public string shifttime { get; set; }
        public DateTime correctiondate { get; set; }
        public string correctiontime { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        
    }
}