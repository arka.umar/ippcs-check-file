﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.BuyingPriceMaintenance
{
    public class BuyingPriceMaintenance
    {
        public int NO { get; set; }
        public string PROCESS_ID { get; set; }
        public string BUSINESS_AREA { get; set; }
        public string MATERIAL_NO {get;set;}
        public string MATERIAL_DESC { get; set; }
        public string PACKING_TYPE { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public string VALID_FROM { get; set; }
        public string PRICE { get; set; }
        public string DELETION_FLAG { get; set; }

        public string CURR_CD { get; set; }
        public string PRICE_STS { get; set; }
        public string RELEASE_STS { get; set; }
        public string PC_NO { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string PROD_PURPOSE { get; set; }
        public string UOM { get; set; }
        public string MM_DF { get; set; }
        public string VALID_DT_TO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string RELEASE_DT { get; set; }
        public string PRICE_TYPE { get; set; }
        public string SOURCE_DATA { get; set; }
        public string DRAFT_DF { get; set; }
        public string WRAP_REF_NO { get; set; }
        public string CPP_FLAG { get; set; }
        public string WRAP_BUYER_CD { get; set; }

        public string SYSTEM_CD { get; set; }
        public string SYSTEM_REMARK { get; set; }

        public string editable { get; set; }

    }

    public class BuyingPriceMaintenanceCSV
    {
        public string MATERIAL_NO { get; set; }
        public string MATERIAL_DESC { get; set; }
        public string PACKING_TYPE { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public string VALID_FROM { get; set; }
        public string PRICE { get; set; }
        public string CURR_CD { get; set; }
        public string PRICE_STS { get; set; }
        public string RELEASE_STS { get; set; }
        public string PC_NO { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string PROD_PURPOSE { get; set; }
        public string UOM { get; set; }
        public string MM_DF { get; set; }
        public string VALID_DT_TO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string RELEASE_DT { get; set; }
        public string PRICE_TYPE { get; set; }
        public string SOURCE_DATA { get; set; }
        public string DRAFT_DF { get; set; }
        public string WRAP_REF_NO { get; set; }
        public string CPP_FLAG { get; set; }
        public string WRAP_BUYER_CD { get; set; }
        public string BUSINESS_AREA { get; set; }
    }
}