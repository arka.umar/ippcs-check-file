﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.BuyingPriceMaintenance
{
    public class BuyingPriceUpload
    {
        public string MAT_NO { get; set; }
        public string PROD_PURPOSE_CD { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string SUPP_CD { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public string PRICE_STATUS { get; set; }
        public decimal PRICE_AMT { get; set; }
        public string CURR_CD { get; set; }
        public DateTime VALID_DT_FR { get; set; }
        public DateTime VALID_DT_TO { get; set; }
        public string ERR_MSG { get; set; }
        public int ROW_NO { get; set; }
        public string STATUS { get; set; }
    }
}