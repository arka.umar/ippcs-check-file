﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace Portal.Models.PrintDocCode
{
    public class PrintDockCode
    {
        public string DOCK_CD { get; set; }
        public string PLANT_CD {get; set;}
        public string DOCK_NM { get; set; }
        public string DockCode { get; set; }
    }
}