﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogTransactionMonitoring
{
    public class LogTransactionMonitoringMaster
    {
        public string LogIDH { set; get; }
        public string ModuleID { set; get; }
        public string FunctionID { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public string LogStatus { set; get; }
        public int LogIDD { set; get; }
        public string SequenceNumber { set; get; }
        public string Type { set; get; }
        public string Location { set; get; }
        public string Message { set; get; }
    }
}