﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogTransactionMonitoring
{
    public class LogTransactionMonitoringDetailsModel
    {
        public List<LogTransactionMonitoringMaster> LogTransactionMonitoringDetails { get; set; }
        public LogTransactionMonitoringDetailsModel()
        {
            LogTransactionMonitoringDetails = new List<LogTransactionMonitoringMaster>();

        }
   
    }

}