﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogTransactionMonitoring
{
    public class LogTransactionMonitoringModel
    {
        public List<LogTransactionMonitoringMaster> LogTransactionMonitoring { get; set; }
        public LogTransactionMonitoringModel()
        {
            LogTransactionMonitoring = new List<LogTransactionMonitoringMaster>();
        }   
    }


}