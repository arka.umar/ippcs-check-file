﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Upload;
using Toyota.Common.Web.Messaging.Notification;

namespace Portal.Models.WallAnnouncement
{
    public class WallAnnouncementModel
    {
        public List<User> Recipients { set; get; }
        public List<FileUpload> UploadedFiles { set; get; }
        public string Email { set; get; }
        //public List<NotificationMessage> Notification { get; set; }
    }

    public class EmailRec
    {
        public string Email_Rec { get; set; }
    }
}