﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmationApprovalByPC
{
    public class PriceConfirmationApprovalByPC
    {
        public string PCNo { set; get; }
        public string SupplierName { set; get; }
        public string Status { set; get; }
        public DateTime EffectiveDate { get; set; }
        public DateTime PCDate { get; set; }
        public string Year { set; get; }
        public string SHApprovalStatus { set; get; }
        public string DPHApprovalStatus { set; get; }
        public string DHApprovalStatus { set; get; }
        public string StatusApprove { set; get; }
        public string StatusReject { set; get; }
    }
}