﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmationApprovalByPC
{
    public class PriceConfirmationApprovalByPCModel
    {
        public List<PriceConfirmationApprovalByPC> PriceConfirmationApprovalByPCs { set; get; }

        public List<PriceConfirmationApprovalByPCDetail> PriceConfirmationApprovalByPCDetails { set; get; }

        public PriceConfirmationApprovalByPC PriceConfirmation { set; get; }

   

        public PriceConfirmationApprovalByPCModel()
        {
            PriceConfirmationApprovalByPCDetails = new List<PriceConfirmationApprovalByPCDetail>();
            PriceConfirmationApprovalByPCs = new List<PriceConfirmationApprovalByPC>();
        }
    }
}