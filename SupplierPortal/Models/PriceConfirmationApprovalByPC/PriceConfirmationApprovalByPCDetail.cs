﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmationApprovalByPC
{
    public class PriceConfirmationApprovalByPCDetail
    {
        public int No { set; get; }
        public int ApprovalNo { set; get; }
        public string MaterialNo { set; get; }
        public string MaterialDescription { set; get; }
        public string PackType { set; get; }
        public string UOM { set; get; }
        public string Currency { set; get; }
        //public string CurrentPrice { set; get; }
        //public string NewPrice { set; get; }
        public string PriceStatus { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal NewPrice { get; set; }
        public string Remarks { set; get; }
        //public string penyatadatadisablenyecuy { get; set; }
    }
}