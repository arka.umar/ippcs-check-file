﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.VatInH
{
    public class VatInH
    {
       
        public string TAX_INVOICE_NO { set; get; }
        public string SUPPLIER_CODE { set; get; }
        public double VAT_PRICE { set; get; }
        public DateTime TAX_INVOICE_DT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime? CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime? CHANGED_DT { set; get; }

    }
}