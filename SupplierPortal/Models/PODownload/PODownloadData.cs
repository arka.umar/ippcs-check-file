﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PODownload
{
    public class PODownloadData
    {
        public string ProductionMonth { get; set; }
        public string PONo { get; set; }
        public string PO_TYPE { get; set; }
        public string PoTypeName { get; set;  }
        public DateTime PODate { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string Confirmation { get; set; }
        public string LockSts { get; set; }
        public string DownloadSts { get; set; }
        public string DownloadBy { get; set; }
        public DateTime? DownloadDt { get; set; }
        public string Notice { get; set; }
        public string PDFDownload { set; get; }
        public string CREATED_BY { get; set; }
    }
}