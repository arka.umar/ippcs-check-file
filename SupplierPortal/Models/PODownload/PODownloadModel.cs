﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PODownload
{
    public class PODownloadModel
    {
        public List<PODownloadData> PODownloadDatas = new List<PODownloadData>();

        public PODownloadModel()
        {
            PODownloadDatas = new List<PODownloadData>();
        }
    }
}