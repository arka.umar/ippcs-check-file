﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ApprovalModel
    {
        public List<ApprovalData> ApprovalDatas { get; set; }

        public ApprovalModel()
        {
            ApprovalDatas = new List<ApprovalData>();
        }
    }
}