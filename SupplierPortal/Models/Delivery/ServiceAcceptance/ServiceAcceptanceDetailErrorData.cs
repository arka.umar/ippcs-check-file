﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ServiceAcceptanceDetailErrorData
    {
        public string DELIVERY_NO { get; set; }
        public double PROCESS_ID{get; set;}
        public string ROUTE_MASTER_DESC {get; set;}
        public string ROUTE_PRICE_DESC {get; set;}
        public string LP_MASTER_DESC {get; set;}
        public string SYSTEM_DESC {get; set;}
        public string CREATED_BY {get; set;}
        public DateTime CREATED_DT {get; set;}
    }
}