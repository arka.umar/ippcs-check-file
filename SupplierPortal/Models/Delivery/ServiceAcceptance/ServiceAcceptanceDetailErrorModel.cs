﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ServiceAcceptanceDetailErrorModel
    {
        public List<ServiceAcceptanceDetailErrorData> ServiceAcceptanceDetailErrorDatas { get; set; }

        public ServiceAcceptanceDetailErrorModel()
        {
            ServiceAcceptanceDetailErrorDatas = new List<ServiceAcceptanceDetailErrorData>();
        }
    }
}