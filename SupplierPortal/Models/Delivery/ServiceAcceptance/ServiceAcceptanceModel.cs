﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ServiceAcceptanceModel
    {
        public List<ServiceAcceptanceData> ServiceAcceptanceDatas { get; set; }

        public ServiceAcceptanceModel()
        {
            ServiceAcceptanceDatas = new List<ServiceAcceptanceData>();
        }
    }
}