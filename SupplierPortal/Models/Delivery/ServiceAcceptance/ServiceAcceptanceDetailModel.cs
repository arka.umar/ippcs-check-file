﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ServiceAcceptanceDetailModel
    {
        public List<ServiceAcceptanceDetailData> ServiceAcceptanceDetailDatas { get; set; }

        public ServiceAcceptanceDetailModel()
        {
            ServiceAcceptanceDetailDatas = new List<ServiceAcceptanceDetailData>();
        }
    }
}