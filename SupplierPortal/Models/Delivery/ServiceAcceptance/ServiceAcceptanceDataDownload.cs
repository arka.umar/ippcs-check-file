﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ServiceAcceptanceDataDownload
    {
        public string DELIVERY_NO { get; set; }
        public string DELIVERY_TYPE { get; set; }
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        public string ROUTE_RATE { get; set; }
        public string DELIVERY_STS { get; set; }
        
        public string PICKUP_DT { get; set; }
        public string ARRIVAL { get; set; }
        public string DEPARTURE { get; set; }

        public string PO_NO { get; set; }
        public string MATDOC_NO { get; set; }
        
        public string POSTING_DT { get; set; }
        
        public string CANCEL_REFF_NO { get; set; }
        
        public string CANCEL_DT { get; set; }
        
        public string PIC { get; set; }
        public string PIC2 { get; set; }
        public string INV_NO { get; set; }
        
        public string INV_DT { get; set; }
        
        public string INV_POSTING_BY { get; set; }
        public string SA_NOTES { get; set; }

    }
}