﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ServiceAcceptanceData
    {
        public string DELIVERY_NO { get; set; }
        public string DELIVERY_TYPE { get; set; }
        public string DELIVERY_TYPE_DESC { get; set; }
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        public string ROUTE_RATE { get; set; }
        public string ROUTE_NAME { get; set; }
        public string DELIVERY_STS { get; set; }
        public DateTime PICKUP_DT { get; set; }
        public DateTime ARRIVAL { get; set; }
        public DateTime DEPARTURE { get; set; }
        public string PO_NO { get; set; }
        public string MATDOC_NO { get; set; }

        public string ACCT_DOC_NO { get; set; }
        public string ACCT_DOC_YEAR { get; set; }
        public string CLEAR_DOC_NO { get; set; }

        public DateTime POSTING_DT { get; set; }
        public string MATDOC_NO2 { get; set; }
        public DateTime CANCEL_DT { get; set; }
        public string PIC { get; set; }
        public string PIC2 { get; set; }
        public string INV_NO { get; set; }
        public DateTime INV_DT { get; set; }
        public string INV_POSTING_BY { get; set; }
        public string SA_NOTES { get; set; }


        public DateTime PLAN_DT { get; set; }
        public DateTime ACTUAL_DT { get; set; }
        public string DOCK_CD { get; set; }

        public double PROCESS_ID { get; set; }
        public string ROUTE_MASTER_DESC { get; set; }
        public string ROUTE_PRICE_DESC { get; set; }
        public string LP_MASTER_DESC { get; set; }
        public string SYSTEM_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }

   }

    public class ServiceAcceptanceStatus
    {
        public int id { get; set; }
        public string DELIVERY_STS { get; set; }
    }

}