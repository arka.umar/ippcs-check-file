﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.ServiceAcceptance
{
    public class ServiceAcceptanceDetailData
    {
        public string DELIVERY_NO { get; set; }
        public string ROUTE_RATE { get; set; }
        public DateTime PLAN_DT { get; set; }
        public DateTime ACTUAL_DT { get; set; }
        public string MATDOC_NO { get; set; }
        public DateTime POSTING_DT { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime INVOICE_DT { get; set; }
        public string PIC { get; set; }
        public string MATDOC_NO2 { get; set; }
        public DateTime CANCEL_DT { get; set; }
        public string PIC2 { get; set; }
    }
}