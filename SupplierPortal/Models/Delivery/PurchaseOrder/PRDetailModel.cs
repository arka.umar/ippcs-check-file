﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PRDetailModel_PO
    {
        public List<PRDetailData_PO> PRDetail_PODatas { get; set; }

        public PRDetailModel_PO()
        {
            PRDetail_PODatas = new List<PRDetailData_PO>();
        }

    }
}