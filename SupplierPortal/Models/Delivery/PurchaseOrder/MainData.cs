﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class MainData
    {
        public string   PO_NUMBER           { get; set; }
        public string   PRODUCTION_MONTH    { get; set; }
        public string   CODE                { get; set; }
        public string   LOG_PARTNER_NAME    { get; set; }
        public string   TOTAL_ROUTE         { get; set; }
        public string   REMAINING_ROUTE     { get; set; }
        public string   PR_NUMBER           { get; set; }
        public string   STATUS              { get; set; }
        public string   CREATE_BY           { get; set; }
        public DateTime CREATE_DT           { get; set; }
        public string   CHANGE_BY           { get; set; }
        public DateTime CHANGE_DT           { get; set; }
        public string LP_CD { get; set; }

    }

    public class PODataStatus
    {
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

    }


    public class CheckProgress
    {
        public string messageLog { get; set; }
    }


}
