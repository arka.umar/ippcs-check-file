﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class MainModel
    {
        public List<PODataStatus> PODataStatus { get; set; }
        public List<MainData> MainDatas { get; set; }

        public MainModel()
        {
            MainDatas = new List<MainData>();

            PODataStatus = new List<PODataStatus>();
        }

    }
}
