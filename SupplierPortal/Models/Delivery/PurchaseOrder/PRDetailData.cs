﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PRDetailData_PO
    {
            public string PR_NO { get; set; }
            public string CREATED_DT { get; set; }
            public string LP_CD { get; set; }
            public string LOG_PARTNER_NAME { get; set; }
            public string EFFECTIVE_DT { get; set; }
            public string PR_STATUS_FLAG { get; set; }
            public string PR_STATUS_DESC { get; set; }
            public string ROUTE_CD { get; set; }
            public string ROUTE_NAME { get; set; }
            public string PR_QTY { get; set; }
            public string PR_SEQUENCE { get; set; }
            public Decimal PRICE { get; set; }
            public string PO_NO { get; set; }
    }
}