﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PODetailModel
    {
        public List<PODetailData> PODetailDatas { get; set; }

        public PODetailModel()
        {
            PODetailDatas = new List<PODetailData>();
        }
    }
}