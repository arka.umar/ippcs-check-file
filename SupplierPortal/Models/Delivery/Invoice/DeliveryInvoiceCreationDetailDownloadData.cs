﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationDetailDownloadData
    {
        public string REFF_NO { get; set; }
        public string LP_INV_NO { get; set; }
        public string COMP_PRICE_TEXT {get; set;}
        public decimal VALUE {get; set;}
        public decimal AMOUNT { get; set; }
        public int TRANSACTION_QTY { get; set; }
    }
}