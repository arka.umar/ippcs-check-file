﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCancelData
    {
        public string STATUS { get; set; }
        public string TYPE { get; set; }
        public string MESSAGE { get; set; }

        public string ERROR { get; set; }
    }
}