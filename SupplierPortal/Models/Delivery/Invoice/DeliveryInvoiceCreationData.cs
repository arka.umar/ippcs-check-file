﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationData
    {
        public string DATA_KEY { get; set; }
        public string DELIVERY_NO { get; set; }
        public string PO_NO { get; set; }
        public string CURRENCY { get; set; }
        public Decimal AMOUNT { get; set; }
        public DateTime DOCUMENT_DT { get; set; }
        public string INVOICE_NO { get; set; }
        public string UPLOAD_BY { get; set; }
        public DateTime UPLOAD_DT { get; set; }
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        public string STATUS { get; set; }

        public string COMP_PRICE_CD { get; set; }
        public string ITEM_CURR { get; set; }
        public Decimal PO_DETAIL_PRICE { get; set; }


        public string LP_INV_NO { get; set; }
        public string STATUS_CD { get; set; }

        public string TYPE { get; set; }
        public string MESSAGE { get; set; }

        public int cnt { get; set; }
    }

    /*public class DeliveryInvoiceCreationDataDetail
    {
        public string COMP_PRICE_CD {get; set;}
        public string ITEM_CURR {get; set;}
        public string PO_DETAIL_PRICE { get; set; }
    }*/

    //Add By Arka.Taufik 2022-03-22
    public class DeliveryInvoiceCreationTax
    {
        public string TAX_CD { get; set; }
        public string TAX_NAME { get; set; }
        public string RATE { get; set; }
    }
}