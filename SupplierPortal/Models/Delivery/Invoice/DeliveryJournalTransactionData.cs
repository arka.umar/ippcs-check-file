﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryJournalTransactionData
    {
        public String DATA_KEY { get; set; } 

        //char(15)
        public String DOC_NO { get; set; }

        //char(4)
        public String DOC_YEAR { get; set; }

        //char(5)
        public String DOC_ITEM { get; set; }

        //varchar(10)
        public DateTime DOC_DT { get; set; }

        //varchar(4)
        public String TRANSACTION_CD { get; set; }

        //varchar(5)
        public String CONDITION_TYPE { get; set; }

        //varchar(1)
        public String DEBIT_CREDIT_INDICATOR { get; set; }

        //varchar(10)
        public String GL_ACCOUNT { get; set; }

        //varchar(4)
        public String ROUTE_CD { get; set; }

        //int
        public Int32 QTY { get; set; }

        //char(15)
        public Decimal AMT_DOC { get; set; }

        //varchar(3)
        public String DOC_CURR { get; set; }

        //varchar(15)
        public Decimal AMT_LOCAL { get; set; }

        //varchar(3)
        public String LOCAL_CURR { get; set; }

        //char(11)
        public String PO_NO { get; set; }

        //char(2)
        public String TAX_CD { get; set; }

        //char(7)
        public Decimal EXCHANGE_RATE { get; set; }

        //varchar(10)
        public String COST_CENTER_CD { get; set; }

        //varchar(2)
        public String DOC_TYPE { get; set; }

        //varchar(1)
        public String GL_BALANCE_FLAG { get; set; }

        //varchar(24)
        public String WBS_NO { get; set; }

        //char(3)
        public String UOM { get; set; }

        //char(50)
        public String ITEM_TEXT { get; set; }

        //
        public Int64 PROCESS_ID { get; set; }

        //
        public String CREATED_BY { get; set; }

        //
        public DateTime CREATED_DT { get; set; }

        //
        public String CHANGED_BY { get; set; }

        //
        public DateTime CHANGED_DT { get; set; }

        //TB_M_SYSTEM
        public string DOC_TYPE_CD { get; set; }

        public string DOC_TYPE_VALUE { get; set; }

        public string POSTING_STS { get; set; }
    }
}