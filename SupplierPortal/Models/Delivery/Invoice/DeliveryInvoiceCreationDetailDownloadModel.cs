﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationDetailDownloadModel
    {
        public List<DeliveryInvoiceCreationDetailDownloadData> DeliveryInvoiceCreationDetailListData { get; set; }

        //public List<DeliveryInvoiceCreationDataDetail> DeliveryInvoiceCreationDataDetail { get; set; }

        public DeliveryInvoiceCreationDetailDownloadModel()
        {
            DeliveryInvoiceCreationDetailListData = new List<DeliveryInvoiceCreationDetailDownloadData>();

            //DeliveryInvoiceCreationDataDetail = new List<DeliveryInvoiceCreationDataDetail>();
        }
    }
}