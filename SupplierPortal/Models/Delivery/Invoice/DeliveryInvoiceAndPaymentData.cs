﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;


namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceAndPaymentData
    {
        public string DATA_KEY { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime INVOICE_DT { get; set; }
        public string CURRENCY { get; set; }
        public Decimal TOTAL_AMOUNT { get; set; }
        public Decimal AMOUNT_BEFORE { get; set; }
        public Decimal TAX_AMOUNT { get; set; }
        public string STATUS { get; set; }
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        public DateTime? PLAN { get; set; }
        public DateTime? ACTUAL { get; set; }
        public string PROGRESS_STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }

        public string SUBMIT_BY { get; set; }
        public DateTime? SUBMIT_DT { get; set; }
        public string POSTING_BY { get; set; }
        public DateTime? POSTING_DT { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string PAY_DOC_NO { get; set; }

        
        public string DELIVERY_NO {get; set;}
        public string ROUTE_CD {get; set;}
        public string ROUTE_NAME { get; set; }
        public DateTime DOC_DT {get; set;}
        public string COMP_PRICE_CD { get; set; }
        public Decimal QTY {get; set;}
        public Decimal PRICE_AMT {get; set;}
        public string PAYMENT_CURR {get; set;}
        public Decimal TAX_AMT { get; set; }

        //NEW METHOD
        public string PARTNER_BANK {get; set;}
        public string INVOICE_DATE {get; set;}
        public string HEADER_TEXT {get; set;}
        public string BASELINE_DATE {get; set;}
        public string TAX_CODE {get; set;}
        public string TERM_PAYMENT {get; set;}
        public string TAX_DATE {get; set;}
        public string PAYMENT_METHOD {get; set;}
        public string POSTING_DATE { get; set; }
        public string INV_NOTES { get; set; }


    }

    public class DeliveryInvoiceAndPaymentDetailData
    {
        public string DELIVERY_NO { get; set; }
        public string ROUTE_CD { get; set; }
        public string ROUTE_NAME { get; set; }
        public DateTime DOC_DT { get; set; }
        public string COMP_PRICE_CD { get; set; }
        public Decimal QTY { get; set; }
        public Decimal PRICE_AMT { get; set; }
        public string PAYMENT_CURR { get; set; }
        public Decimal TAX_AMT { get; set; }
    }

    public class CheckProgress
    {
        public string messageLog { get; set; }
    }

    public class CheckResult
    {
        public string result { get; set; }
    }
}