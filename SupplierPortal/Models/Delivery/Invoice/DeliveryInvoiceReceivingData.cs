﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceReceivingData
    {
        public string DELIVERY_NO { get; set; }
        public string CERTIFICATE_ID { get; set; }
        public string CURRENCY { get; set; }
        public Decimal INVOICE_AMOUNT { get; set; }
        public Decimal INVOICE_TAX_AMOUNT { get; set; }
        public string INVOICE_TAX { get; set; }
        public DateTime DOCUMENT_DT { get; set; }
        public string INVOICE_NO { get; set; }
        public string SUBMIT_BY { get; set; }
        public DateTime SUBMIT_DT { get; set; }
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        public string STATUS { get; set; }
        public string NOTICE { get; set; }
    }
}