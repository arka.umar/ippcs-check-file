﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class InvoiceDocumentData
    {
        public string TRUCKING { get; set; }
        public string INVOICE_NO { get; set; }
        public string INVOICE_AMOUNT { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string TAX_NO { get; set; }
        public string TAX_AMOUNT { get; set; }

    }
}
