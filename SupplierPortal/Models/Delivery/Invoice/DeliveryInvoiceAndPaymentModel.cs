﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceAndPaymentModel
    {
        public List<DeliveryInvoiceAndPaymentData> DeliveryInvoiceAndPaymentListData { get; set; }

        public List<DeliveryInvoiceAndPaymentDetailData> DeliveryInvoiceAndPaymentListDetailData { get; set; }

        public DeliveryInvoiceAndPaymentModel()
        {
            DeliveryInvoiceAndPaymentListData = new List<DeliveryInvoiceAndPaymentData>();

            DeliveryInvoiceAndPaymentListDetailData = new List<DeliveryInvoiceAndPaymentDetailData>();
        }
    }
}