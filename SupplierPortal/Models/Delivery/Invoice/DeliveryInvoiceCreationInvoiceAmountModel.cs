﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationInvoiceAmountModel
    {
        public List<DeliveryInvoiceCreationInvoiceAmountData> DeliveryInvoiceCreationListTotalAmount { get; set; }
        public DeliveryInvoiceCreationInvoiceAmountModel()
        {
            DeliveryInvoiceCreationListTotalAmount = new List<DeliveryInvoiceCreationInvoiceAmountData>();
        }
    }
}