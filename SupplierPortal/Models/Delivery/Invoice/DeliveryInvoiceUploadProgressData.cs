﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;


namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceUploadProgressData
    {
        public string PROCESS_ID { get; set; }
        public int SEQUENCE_NUMBER { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string MESSAGE { get; set; }
        public string LOCATION { get; set; }
        public int COUNT { get; set; }
        public string PROCESS_STATUS { get; set; }
    }
}