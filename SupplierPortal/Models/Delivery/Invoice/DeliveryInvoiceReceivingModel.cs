﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceReceivingModel
    {
        public List<DeliveryInvoiceReceivingData> DeliveryInvoiceReceivingListData { get; set; }

        public DeliveryInvoiceReceivingModel()
        {
            DeliveryInvoiceReceivingListData = new List<DeliveryInvoiceReceivingData>();
        }
    }
}