﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryJournalPostHeaderData
    {
        //char(15)
        public string DOC_NO { get; set; }
        
        //char(4)
        public string DOC_YEAR { get; set; }

        //char(8) yyyymmdd
        public DateTime DOC_DT { get; set; }

        //char(8) yyyymmdd
        public DateTime POSTING_DT { get; set; }

        //char(25)
        public string DOC_HEADER_TXT { get; set; }

        //char(16)
        public string REFF_NO { get; set; }

        //char(4)
        public string PAYMENT_TERM_CD { get; set; }

        //varchar(10)
        public string LP_CD { get; set; }

        //char(10)
        public string CUSTOMER_CD { get; set; }

        //char(4)
        public string PAYMENT_METHOD_CD { get; set; }

        //char(4)
        public string PARTNER_BANK_KEY { get; set; }

        //char(10)
        public string ACCT_DOC_NO { get; set; }

        //char(4)
        public string ACCT_DOC_YEAR { get; set; }

        //varchar(2)
        public string HEADER_TYPE { get; set; }

        //varchar(MAX)
        public string ERROR_POSTING { get; set; }

        public string POSTING_STS { get; set; }

        //
        public String CREATED_BY { get; set; }

        //
        public DateTime CREATED_DT { get; set; }

        //
        public String CHANGED_BY { get; set; }

        //
        public DateTime CHANGED_DT { get; set; }

        //TB_M_SYSTEM
        public string DOC_TYPE_CD { get; set; }

        public string DOC_TYPE_VALUE { get; set; }
    }
}