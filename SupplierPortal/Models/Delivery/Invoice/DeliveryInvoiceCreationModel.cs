﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationModel
    {
        public List<DeliveryInvoiceCreationData> DeliveryInvoiceCreationListData { get; set; }

        //public List<DeliveryInvoiceCreationDataDetail> DeliveryInvoiceCreationDataDetail { get; set; }

        public DeliveryInvoiceCreationModel()
        {
            DeliveryInvoiceCreationListData = new List<DeliveryInvoiceCreationData>();

            //DeliveryInvoiceCreationDataDetail = new List<DeliveryInvoiceCreationDataDetail>();
        }
    }
}