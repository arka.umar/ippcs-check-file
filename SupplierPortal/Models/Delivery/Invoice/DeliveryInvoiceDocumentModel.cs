﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class InvoiceDocumentModel
    {
        public List<InvoiceDocumentData> InvoiceDocumentDatas { get; set; }

        public InvoiceDocumentModel()
        {
            InvoiceDocumentDatas = new List<InvoiceDocumentData>();
        }
    }
}