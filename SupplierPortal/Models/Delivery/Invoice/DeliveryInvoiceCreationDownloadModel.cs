﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationDownloadModel
    {
        public List<DeliveryInvoiceCreationDownloadData> DeliveryInvoiceCreationListData { get; set; }

        //public List<DeliveryInvoiceCreationDataDetail> DeliveryInvoiceCreationDataDetail { get; set; }

        public DeliveryInvoiceCreationDownloadModel()
        {
            DeliveryInvoiceCreationListData = new List<DeliveryInvoiceCreationDownloadData>();

            //DeliveryInvoiceCreationDataDetail = new List<DeliveryInvoiceCreationDataDetail>();
        }
    }
}