﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryJournalData
    {
        public List<DeliveryJournalPostHeaderData> DJPostHeader { get; set; }
        public List<DeliveryJournalPostDetailData> DJPostDetail { get; set; }
        public List<DeliveryJournalTransactionData> DJTransData { get; set; }
        public List<DeliveryJournalSystemMaster> DJSysData { get; set; }
        public List<DeliveryJournalSystemMaster> DJStatusData { get; set; }
    }

    public class DeliveryJournalSystemMaster
    {
        public String SYSTEM_CD { get; set; }
        public String SYSTEM_VALUE { get; set; }
    }
}