﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationDownloadData
    {
        public string ClientID { get; set; }
        public string LP_CD { get; set; }
        public string PO_NO { get; set; } 
        public int PO_ITEM_NO { get; set; }
        public string ROUTE_CD { get; set; }
        public string REFF_NO { get; set; }
        public string COMP_PRICE_CD { get; set; }
        public string DOC_DT { get; set; } 
        public Decimal TRANSACTION_QTY { get; set; }
        public string LP_INV_NO { get; set; } 
        public string PO_DETAIL_PRICE { get; set; } 
        public string TRANSACTION_CURR { get; set; }
        public string AMOUNT { get; set; }

    }
}