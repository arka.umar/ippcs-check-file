﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;


namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceData
    {
    }

    public class DeliveryInvoiceDataLogPartner
    {
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }

    }

    public class DeliveryInvoiceDataTermofPayment
    {
        public string PAY_TERM_CD { get; set; }
        public string PAY_TERM_NAME { get; set; }

    }

    public class DeliveryInvoiceDataPaymentMethod
    {
        public string PAY_METHOD_CD { get; set; }
        public string PAY_METHOD_NAME { get; set; }
    }

    public class DeliveryInvoiceDataWithholdingTax
    {
        public string WITHHOLDING_TAX_CD { get; set; }
        public string WITHHOLDING_TAX_NAME { get; set; }
    }

    public class DeliveryInvoiceDataGLAccount
    {
        public string GL_ACCOUNT { get; set; }
	    public string TRANSACTION_NAME { get; set; }
	    public string COST_CENTER { get; set; }
        public string TAX_CD { get; set; }

    }

    public class DeliveryInvoiceDataTax
    {
        public string TAX_CD { get; set; }
        public string TAX_NAME { get; set; }
    }

    public class DeliveryinvoiceDataPartnerBank
    {
        public string SUPP_BANK_TYPE { get; set; }
        public string SUPP_BANK_KEY { get; set; }
        public string SUPP_ACCOUNT { get; set; }
    }

    public class DeliveryInvoiceDataStatus
    {
        public string STATUS_CD { get; set; }
        public string STATUS_NAME { get; set; }
    }

}