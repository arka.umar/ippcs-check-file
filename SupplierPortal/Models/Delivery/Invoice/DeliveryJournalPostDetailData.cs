﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryJournalPostDetailData
    {
        //char(15)
        public string DOC_NO { get; set; }

        //char(4)
        public string DOC_YEAR { get; set; }

        //char(5)
        public string DOC_ITEM_NO { get; set; }

        //char(5)
        public string CONDITION_TYPE { get; set; }

        //char(10)
        public string GL_ACCOUNT { get; set; }

        //char(10)
        public string ROUTE_CD { get; set; }

        //char(4)
        public string DOC_QTY { get; set; }

        //char(10)
        public string DOC_AMT { get; set; }

        //char(2)
        public string DOC_CURR_CD { get; set; }

        //char(1)
        public string PO_NO { get; set; }

        //char(23)
        public string TAX_CD { get; set; }

        //char(13)
        public string EXCHANGE_RATE { get; set; }

        //char(15)
        public string COST_CENTER_CD { get; set; }

        //char(3)
        public string ITEM_TEXT { get; set; }

        //char(15)
        public string TRANSACTION_NO { get; set; }

        //char(3)
        public string WBS_NO { get; set; }

        //char(11)
        public string UOM { get; set; }

        //
        public String CREATED_BY { get; set; }

        //
        public DateTime CREATED_DT { get; set; }

        //
        public String CHANGED_BY { get; set; }

        //
        public DateTime CHANGED_DT { get; set; }
    }
}