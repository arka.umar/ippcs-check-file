﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceCreationInvoiceAmountData
    {
        public int DLV_COUNT { get; set; }
        public decimal TOTAL { get; set; }
        public decimal TAX_AMT { get; set; }
        public decimal TAX { get; set; }
        public string LP_INV_NO { get; set; }
        public string INV_DT { get; set; }
        public string INV_TEXT { get; set; }
        public string INV_TAX_DT { get; set; }
        public string DLV_NO { get; set; }

        //Add By Arka.Taufik 2022-03-22
        public string TAX_CD { get; set; }
        public string TAX_RATE { get; set; }
    }
}