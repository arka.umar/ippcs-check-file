﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.Invoice
{
    public class DeliveryInvoiceModel
    {
        public List<DeliveryInvoiceData> DeliveryInvoiceListData { get; set; }

        public List<DeliveryInvoiceDataLogPartner> DeliveryInvoiceListTrucking { get; set; }

        public List<DeliveryInvoiceDataTermofPayment> DeliveryInvoiceListPayofTerm { get; set; }

        public List<DeliveryInvoiceDataPaymentMethod> DeliveryInvoiceListPayMethod { get; set; }

        public List<DeliveryInvoiceDataWithholdingTax> DeliveryInvoiceListWithholdingTax { get; set; }

        public List<DeliveryInvoiceDataGLAccount> DeliveryInvoiceListGLAccount { get; set; }

        public List<DeliveryInvoiceDataTax> DeliveryInvoiceListTax { get; set; }

        public List<DeliveryinvoiceDataPartnerBank> DeliveryInvoiceListPartnerBank { get; set; }
        
        public List<DeliveryInvoiceDataStatus> DeliveryInvoiceListStatus { get; set; }

        public DeliveryInvoiceModel()
        {
            DeliveryInvoiceListData = new List<DeliveryInvoiceData>();

            DeliveryInvoiceListTrucking = new List<DeliveryInvoiceDataLogPartner>();

            DeliveryInvoiceListPayofTerm = new List<DeliveryInvoiceDataTermofPayment>();

            DeliveryInvoiceListPayMethod = new List<DeliveryInvoiceDataPaymentMethod>();

            DeliveryInvoiceListWithholdingTax = new List<DeliveryInvoiceDataWithholdingTax>();

            DeliveryInvoiceListGLAccount = new List<DeliveryInvoiceDataGLAccount>();

            DeliveryInvoiceListTax = new List<DeliveryInvoiceDataTax>();

            DeliveryInvoiceListPartnerBank = new List<DeliveryinvoiceDataPartnerBank>();

            DeliveryInvoiceListStatus = new List<DeliveryInvoiceDataStatus>();
        }

    }

    

}