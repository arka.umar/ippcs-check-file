﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.POApproval
{
    public class POApprovalModel
    {
        public List<POApprovalData> POApprovalDatas { get; set; }

        public List<PODataStatus> PODataStatus { get; set; }

        public POApprovalModel()
        {
            POApprovalDatas = new List<POApprovalData>();

            PODataStatus = new List<PODataStatus>();
        }
    }
}