﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.POApproval
{
    public class PODetailData
    {
            public string PO_NO { get; set; }
            public string CREATED_DT { get; set; }
            public string PO_STATUS_FLAG { get; set; }
            public string ROUTE_CD { get; set; }
            public string ROUTE_NAME { get; set; }
            public Decimal PRICE { get; set; }
            public string PO_QTY { get; set; }
            public string LOG_PARTNER_NAME { get; set; }
            public string PO_CURR { get; set; }
            public Decimal AMOUNT { get; set; }
    }
}

