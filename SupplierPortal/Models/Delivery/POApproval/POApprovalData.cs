﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.POApproval
{
    public class POApprovalData
    {
        public string PO_NO { get; set; }
        public string PROD_MONTH { get; set; }
        public string PR_STATUS_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public DateTime SH_RELEASE { get; set; }
        public DateTime DPH_RELEASE { get; set; }
        public DateTime DH_RELEASE { get; set; }
        public string SH_APPROVED_DT { get; set; }
        public string DPH_APPROVED_DT { get; set; }
        public string DH_APPROVED_DT { get; set; }
        public string SH_RELEASE_BY { get; set; }
        public string DPH_RELEASE_BY { get; set; }
        public string DH_RELEASE_BY { get; set; }
        public string REJECTED_BY { get; set; }
        public string REJECTED_POSITION { get; set; }
        public DateTime REJECTED_DT { get; set; }
        public string LP_CD { get; set; }
        public string LOG_PARTNER_NAME { get; set; }
    }

    public class PODataStatus
    {
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

    }
}