﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PurchaseOrder
{

    public class PurchaseOrderPRDetailModel
    {
        public List<PurchaseOrderPRDetailData> PurchaseOrderPRDetailDatas { get; set; }

        public PurchaseOrderPRDetailModel()
        {
            PurchaseOrderPRDetailDatas = new List<PurchaseOrderPRDetailData>();
        }

    }
}