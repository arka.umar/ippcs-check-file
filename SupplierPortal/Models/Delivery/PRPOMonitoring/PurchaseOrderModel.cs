﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PurchaseOrderModel
    {
        public List<PurchaseOrderData> PurchaseOrderDatas { get; set; }

        public List<DataStatusPR> DataStatusPR { get; set; }
 
        public PurchaseOrderModel()
        {
            PurchaseOrderDatas = new List<PurchaseOrderData>();
            DataStatusPR = new List<DataStatusPR>();
        }

    }

}