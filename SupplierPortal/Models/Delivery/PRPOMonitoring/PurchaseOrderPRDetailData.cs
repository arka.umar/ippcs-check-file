﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PurchaseOrderPRDetailData
    {
        public string PR_NO { get; set; }
        public String CREATED_DT { get; set; }
        public string LP_CD { get; set; }
        public string LOG_PARTNER_NAME { get; set; }
        public string PR_STATUS { get; set; }
        public string ROUTE_CD { get; set; }
        public string ROUTE_NAME { get; set; }
        public string PR_QTY { get; set; }
    }
}