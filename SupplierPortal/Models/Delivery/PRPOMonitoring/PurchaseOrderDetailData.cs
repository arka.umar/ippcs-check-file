﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PurchaseOrderDetailData
    {
        public string PR_SH_APPROVED_BY { get; set; }
        public string PR_SH_APPROVED_DT { get; set; }
        public string PR_DPH_APPROVED_BY { get; set; }
        public string PR_DPH_APPROVED_DT { get; set; }
        public string PR_DH_APPROVED_BY { get; set; }
        public string PR_DH_APPROVED_DT { get; set; }
        public string PO_SH_APPROVED_BY { get; set; }
        public string PO_SH_APPROVED_DT { get; set; }
        public string PO_DPH_APPROVED_BY { get; set; }
        public string PO_DPH_APPROVED_DT { get; set; }
        public string PO_DH_APPROVED_BY { get; set; }
        public string PO_DH_APPROVED_DT { get; set; }
    }
}