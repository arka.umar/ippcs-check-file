﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PurchaseOrderData
    {
        public string PR_NO                 { get; set; }        
        public string PR_SH_APPROVED_BY2     { get; set; }
        public string PR_SH_APPROVED_DT     { get; set; }
        public string PR_DPH_APPROVED_BY2    { get; set; }
        public string PR_DPH_APPROVED_DT    { get; set; }
        public string PR_DH_APPROVED_BY2     { get; set; }
        public string PR_DH_APPROVED_DT     { get; set; }
        public string PR_REJECTED_BY        { get; set; }
        public string PR_REJECTED_POSITION { get; set; }
        public DateTime PR_REJECTED_DT      { get; set; }
        public DateTime PR_CREATED_DT         { get; set; }
        public string PR_CREATED_BY  { get; set; }
        public string PR_REMARK             { get; set; }

        public string PO_NO                 { get; set; }
        public string PO_SH_APPROVED_BY2     { get; set; }
        public string PO_SH_APPROVED_DT     { get; set; }
        public string PO_DPH_APPROVED_BY2    { get; set; }
        public string PO_DPH_APPROVED_DT    { get; set; }
        public string PO_DH_APPROVED_BY2     { get; set; }
        public string PO_DH_APPROVED_DT     { get; set; }
        public string PO_REJECTED_BY       { get; set; }
        public string PO_REJECTED_POSITION { get; set; }
        public DateTime PO_REJECTED_DT     { get; set; }
        public DateTime PO_CREATED_DT { get; set; }
        public string PO_CREATED_BY { get; set; }
        public string PO_REMARK             { get; set; }

        public string PDF                    { get; set; }
        public string LP_CD                 { get; set; }
        public string LOG_PARTNER_NAME      { get; set; }

        public DateTime PR_SH_APPROVED_DT2 { get; set; }
        public DateTime PR_DPH_APPROVED_DT2 { get; set; }
        public DateTime PR_DH_APPROVED_DT2 { get; set; }
        public DateTime PO_SH_APPROVED_DT2 { get; set; }
        public DateTime PO_DPH_APPROVED_DT2 { get; set; }
        public DateTime PO_DH_APPROVED_DT2 { get; set; }

        public string PO_STATUS { get; set; }
        public string PR_STATUS { get; set; }
        
    }

    public class DataStatusPR
    {
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

    }
}