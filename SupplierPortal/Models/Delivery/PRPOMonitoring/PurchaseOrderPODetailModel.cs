﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PurchaseOrder
{

    public class PurchaseOrderPODetailModel
    {
        public List<PurchaseOrderPODetailData> PurchaseOrderPODetailDatas { get; set; }

        public PurchaseOrderPODetailModel()
        {
            PurchaseOrderPODetailDatas = new List<PurchaseOrderPODetailData>();
        }

    }
}