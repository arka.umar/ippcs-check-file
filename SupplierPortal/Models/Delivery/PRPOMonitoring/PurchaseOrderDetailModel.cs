﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PurchaseOrder
{
    public class PurchaseOrderDetailModel
    {
        public List<PurchaseOrderDetailData> PurchaseOrderDetailDatas { get; set; }

        public PurchaseOrderDetailModel()
        {
            PurchaseOrderDetailDatas = new List<PurchaseOrderDetailData>();
        }

    }

}