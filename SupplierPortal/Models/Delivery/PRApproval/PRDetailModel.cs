﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PRApproval
{
    public class PRDetailModel
    {
        public List<PRDetailData> PRDetailDatas { get; set; }

        public PRDetailModel()
        {
            PRDetailDatas = new List<PRDetailData>();
        }

    }
}