﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.PRApproval
{
    public class PRApprovalModel
    {
        public List<PRApprovalData> PRApprovalDatas { get; set; }

        public List<PRDataStatus> PRDataStatus { get; set; }

        public PRApprovalModel()
        {
            PRApprovalDatas = new List<PRApprovalData>();

            PRDataStatus = new List<PRDataStatus>();
        }
    }
}