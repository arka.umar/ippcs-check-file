﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Delivery.DeliveryPurchaseRequest
{
    public class DeliveryPurchaseRequestModel
    {
        
        public List<DeliveryPurchaseRequestData> DeliveryPurchaseRequestListData { get; set; }

        public List<DeliveryPurchaseRequestDataRoute> DeliveryPurchaseRequestDataRoute { get; set; }

        public List<DeliveryPurchaseRequestDataStatus> DeliveryPurchaseRequestDataStatus { get; set; }

        public List<DeliveryPurchaseRequestDataLogPartner> DeliveryPurchaseRequestDataLogPartner { get; set; }
        
        public List<getLog> getLogData { get; set; }

        public DeliveryPurchaseRequestModel()
        {
            
            DeliveryPurchaseRequestListData = new List<DeliveryPurchaseRequestData>();

            DeliveryPurchaseRequestDataRoute = new List<DeliveryPurchaseRequestDataRoute>();

            DeliveryPurchaseRequestDataStatus = new List<DeliveryPurchaseRequestDataStatus>();

            DeliveryPurchaseRequestDataLogPartner = new List<DeliveryPurchaseRequestDataLogPartner>();

            getLogData = new List<getLog>();
            
        }


    }


}