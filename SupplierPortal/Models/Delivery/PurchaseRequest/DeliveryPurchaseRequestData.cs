﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Delivery.DeliveryPurchaseRequest
{
    public class DeliveryPurchaseRequestData
    {
        public string DATA_KEY { get; set; }
        public string PROD_MONTH { get; set; }
        public string ROUTE_CD { get; set; }
        public string LP_CD { get; set; }
        public string SOURCE { get; set; }
        public string PR_QTY { get; set; }
        public string PR_STATUS_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public string ITEM_PR_FLAG { get; set; }
        public string STATUS { get; set; }
        public string ROUTE_NAME { get; set; }
        public string LP_NAME { get; set; }
       
    }

    public class DeliveryPurchaseRequestDataRoute
    {
        
        public string ROUTE_CD { get; set; }
        public string ROUTE_NAME { get; set; }

    }

    public class DeliveryPurchaseRequestDataStatus
    {
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

    }

    public class DeliveryPurchaseRequestDataLogPartner
    {
        public string LP_CD { get; set; }
        public string LP_NAME { get; set; }
        
    }

    public class getPid
    {
        public int pid { get; set; }
    }

    public class CheckResult
    {
        public int result { get; set; }
    }

    public class CheckProgress
    {
        public string messageLog { get; set; }
    }

    public class getLog
    {
        public string PID { get; set; }
        public int SEQUENCE_NUMBER { get; set; }
        public string MESSAGE { get; set; }
    }
}