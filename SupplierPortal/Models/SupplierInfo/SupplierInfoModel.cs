﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierInfo
{
    public class SupplierInfoModel
    {
        public List<SupplierInfoData> SupplierDatas { set; get; }

        public SupplierInfoModel()
        {
            SupplierDatas = new List<SupplierInfoData>();
        }
    }
}