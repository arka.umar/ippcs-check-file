﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierInfo
{
    public class SupplierInfoData
    {
        //public bool Check { set; get; }
        //public string Action { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierPlantCd { set; get; }
        public string SupplierCodePlantCode { set; get; }
        public string SupplierName { set; get; }
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public string DockCode { set; get; }
        public string SpInfo1 { set; get; }
        public string SpInfo2 { set; get; }
        public string SpInfo3 { set; get; }
        public string PrintCode { set; get; }
    }
}