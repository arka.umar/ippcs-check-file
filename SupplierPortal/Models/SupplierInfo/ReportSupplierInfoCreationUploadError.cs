﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierInfo
{
    public class ReportSupplierInfoCreationUploadError
    {
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT_CD { get; set; }
        public String DOCK_CODE { get; set; }
        public String PART_NO { get; set; }
        public String SP_INFO1{ get; set; }
        public String SP_INFO2 { get; set; }
        public String SP_INFO3 { get; set; }
        public String PRINT_CD { get; set; }
        public String ERRORS_MESSAGE_CONCAT { set; get; }
    }
}