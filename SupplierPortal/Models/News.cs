﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;


namespace Portal.Models
{
    public class News : SelectableDataModel
    {
        public long Id { set; get; }        
        public string Title { set; get; }
        public string Content { set; get; }

        public override string SelectedKey
        {
            get
            {
                return "Id;";
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Convert.ToString(Id) + ";";
            }
        }
    }
}