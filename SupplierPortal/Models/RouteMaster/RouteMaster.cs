﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.RouteMaster
{
    public class RouteMaster
    {
        public string routeCode { get; set; }
        public string routeName { get; set; }
        public string SourceData { get; set; }
        public string INVALID_SYNCH_FLAG {get; set;}
        public string INVALID_SYNCH_DT { get; set; }
        public string CreatedBy { get; set; }
        public DateTime createdDate { get; set; }
        public string ChangeBy { get; set; }
        public DateTime changeDate { get; set; }
    }
}