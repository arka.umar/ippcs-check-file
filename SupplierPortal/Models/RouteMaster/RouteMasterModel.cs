﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.RouteMaster
{
    public class RouteMasterModel
    {
        public List<RouteMaster> RouteMaster { get; set; }

        public RouteMasterModel()
        {
            RouteMaster = new List<RouteMaster>();
          
        }   
    }
}