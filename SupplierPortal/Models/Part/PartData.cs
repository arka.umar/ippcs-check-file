﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Part
{
    public class PartData
    {
        public bool Check { set; get; }
        public string Actionnya { set; get; }
        public string SupplierCodePlantCode { set; get; }
        public string PART_NO { set; get; }
        public string PART_NAME { set; get; }
        public string REC_COMP_CD { set; get;}
        public string REC_PLANT_CD { set; get;}
        public string SUPPLIER_CD { set; get; }
        public string REC_DOCK_CD { set; get;}
        public string INHOUSE_ROUTING { set; get; }
        public string SUPPLIER_PLANT { set; get; }
        public string SUPPLIER_SHIPPING {set; get;}
        public DateTime TIME_FROM { set; get; }
        public DateTime TIME_TO { set; get; }
        public string LIFE_CYCLE_CD { set; get; }
        public string TEAM_MEMBER { set; get; }
        public string SERIES_NAME { set; get; }
        public string ORDER_METHOD { set; get; }
        public string BC_REF_FLAG { set; get; }
        public string KANBAN_NO { set; get; }
        public int KANBAN_QTY { set; get; }
        public string VENDOR_SHARE_FLAG { set; get; }
        public string VENDOR_SHARE_VALUE { set; get; }
        public int PC_LEAD_TIME { set; get; }
        public int PC_SAFETY_TIME { set; get; }
        public int LINE_LEAD_TIME { set; get; }
        public int LINE_SAFETY_TIME { set; get; }
        public int SAFETY_PCS { set; get; }
        public string PCK_TYPE { set; get; }
        public int PART_WEIGHT { set; get; }
        public int PACK_PER_CONTAINER { set; get; }
        public int AUDIO_FLAG { set; get; }
       
    }


}