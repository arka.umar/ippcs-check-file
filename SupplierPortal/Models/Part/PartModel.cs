﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Part
{
    public class PartModel
    {
        public List<PartData> PartDatas { set; get; }

        public PartModel()
        {
            PartDatas = new List<PartData>();
        }
    }
}