﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;

namespace Portal.Models
{
    public static class ExceptionalHelper
    {
        public static long PutLog(this Exception ex, string msg, string uid, string loc="", long pid =0, 
             string id= "INF", string typ ="inf", string module="", string func="", int sts = 0 ) 
        {
            return PutLog(ExPlain(ex, msg), uid, loc, pid, id, typ, module, func, sts);
        }

        public static long PutLog(string msg, string uid, string loc = "", long pid = 0,
          string id = "INF", string typ = "INF", string module = "",
          string func = "", int sts = 0)
        {
            long r = Sing.Me.DB.ExecuteScalar<long>("PutLog",
                    new object[] { msg, uid, loc, pid
                    , id, typ, module, func, sts}
                );
            return r;
        }

        public static int PutLogStatus(long pid=0, int sts= 0, string uid="") {
            return Sing.Me.DB.FetchStatement<int>("update tb_r_log_h set process_status=@0, changed_by = coalesce(NULLIF(@1,''), changed_by), changed_dt = getdate(), end_dt=getdate() where process_id=@2; SELECT @@@ROWCOUNT",
                new object[] { sts, uid, pid }).FirstOrDefault();
        }
        

        public static string ExPlain(Exception e, string message)
        {
            StringBuilder b = new StringBuilder(message ?? "");
            if (e != null)
            {
                
                var iex = e;
                string st = e.StackTrace;
                int loops = 0;
                while (iex != null && (loops++<7))
                {
                    b.AppendLine(iex.Message);
                    if (iex is SqlException)
                    {
                        SqlException qex = iex as SqlException;
                        if (qex != null)
                        {
                            b.AppendLine(" Number: " + qex.Number);
                            b.AppendLine("Source: " + qex.Source);
                            b.AppendLine("Proc:" + qex.Procedure);
                            b.AppendLine("Line:" + qex.LineNumber);
                        }
                    }
                    iex = iex.InnerException;
                }
                b.AppendLine(st);
            }
            return b.ToString();
        }
    }
}