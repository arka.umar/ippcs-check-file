﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderPlanPartial
{
    public class OrderPlanPartialModel
    {
        public List<OrderPlanPartialData> orderPlanPartialDatas { get; set; }
        public OrderPlanPartialModel()
        {
           
            orderPlanPartialDatas = new List<OrderPlanPartialData>();
        }

    }
}