﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderPlanPartial
{
    public class OrderPlanPartialData
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SHIPPING_DOCK { get; set; }
        public string COMPANY_CODE { get; set; }
        public string RCV_PLANT_CODE { get; set; }
        public string DOCK_CODE { get; set; }
        public string ORDER_NO { get; set; }
        public string CAR_FAMILY_CODE { get; set; }
        public string RE_EXPORT_CODE { get; set; }
        public string PART_NO { get; set; }
        public string KNBN_PRN_ADDRESS { get; set; }
        public string ORDER_TYPE { get; set; }
        public string ORDER_RELEASE_DATE { get; set; }
        public string ORDER_RELEASE_TIME { get; set; }
        public string KNBN_NO { get; set; }
        public string QTY_PER_CONTAINER { get; set; }
        public string ORDER_PLAN_QTY { get; set; }
        public string ORDER_PLAN_QTY_LOT { get; set; }
        public string BUILD_OUT_FLAG { get; set; }
        public string BUILD_OUT_QTY { get; set; }
        public string AICO_CEPT_FLAG { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PARTS_NAME { get; set; }
        public string ORDER_MODE { get; set; }
        public string SHIPPING_DATE { get; set; }
        public string ARRIVAL_DATE { get; set; }
        public string MR_GRP_CODE { get; set; }
        public string MR_ORDER_SEQ { get; set; }
        public string MR_DATE { get; set; }
        public string SR_GRP_CODE { get; set; }
        public string SR_SEQ { get; set; }
        public string SR_DATE { get; set; }
        public string FRST_X_DOC_CODE { get; set; }
        public string FST_X_PLANT_CODE { get; set; }
        public string FST_X_SHIPPING_DOCK_CODE { get; set; }
        public string FST_X_DOCK_ARRIVAL_DATE { get; set; }
        public string FST_X_DOCK_DEPART_DATE { get; set; }
        public string FST_X_DOCK_ROUTE_GRP_CODE { get; set; }
        public string FST_X_DOCK_SEQ_NO { get; set; }
        public string FST_ROUTE_DATE { get; set; }
        public string SND_X_DOC_CODE { get; set; }
        public string SND_X_PLANT_CODE { get; set; }
        public string SND_X_SHIPPING_DOCK_CODE { get; set; }
        public string SND_X_DOCK_ARRIVAL_DATE { get; set; }
        public string SND_X_DOCK_DEPART_DATE { get; set; }
        public string SND_X_DOCK_ROUTE_GRP_CODE { get; set; }
        public string SND_X_DOCK_SEQ_NO { get; set; }
        public string SND_ROUTE_DATE { get; set; }
        public string TRD_X_DOC_CODE { get; set; }
        public string TRD_X_PLANT_CODE { get; set; }
        public string TRD_X_SHIPPING_DOCK_CODE { get; set; }
        public string TRD_X_DOCK_ARRIVAL_DATE { get; set; }
        public string TRD_X_DOCK_DEPART_DATE { get; set; }
        public string TRD_X_DOCK_ROUTE_GRP_CODE { get; set; }
        public string TRD_X_DOCK_SEQ_NO { get; set; }
        public string TRD_ROUTE_DATE { get; set; }
        public string MANIFEST_PRN_POINT_CODE { get; set; }
        public string MANIFEST_PRN_DATE_PLANT { get; set; }
        public string MANIFEST_PRN_DATE_LOCAL { get; set; }
        public string KNBN_PRN_POINT_CODE { get; set; }
        public string KNBN_PRN_DATE_PLANT { get; set; }
        public string KNBN_PRN_DATE_LOCAL { get; set; }
        public string CONVEYANCE_ROUTE { get; set; }
        public string FERRY_PORT_ARRIVAL_DATE { get; set; }
        public string FERRY_PORT_DEPART_DATE { get; set; }
        public int STATUS_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string FILENAME { get; set; }
    }
}