﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RetroConfirmation
{
    public class RetroConfirmationDetail
    {
        public string RetroDocNo { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string TMMIN { get; set; }
        public string Supplier { get; set; }
        public string InvoiceNo { get; set; }
        public string Status { get; set; }
        public string Notice { get; set; }
        public int Upload { get; set; }
        public int Download { get; set; }
    }
}