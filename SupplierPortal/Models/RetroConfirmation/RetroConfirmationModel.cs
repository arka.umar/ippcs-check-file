﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RetroConfirmation
{
    public class RetroConfirmationModel
    {
        public List<RetroConfirmationDetail> RetroConfirmationDetails {get; set;}
        public List<RetroConfirmation> RetroConfirmations { get; set; }
        public List<RetroApprovaldetail> RetroApprovalDetails { get; set; }

        public RetroConfirmationModel()
        {
            RetroConfirmationDetails = new List<RetroConfirmationDetail>();
            RetroConfirmations = new List<RetroConfirmation>();
            RetroApprovalDetails = new List<RetroApprovaldetail>();
        
        }
    }
}