﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RetroConfirmation
{
    public class RetroConfirmation
    {
        public string supplierCode { get; set; }
        public string Name { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime To { get; set; }
        public string RetroDocNo { get; set; }
        public string StatusRetro { get; set; }
    }
}