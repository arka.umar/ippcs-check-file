﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RetroConfirmation
{
    public class RetroApprovaldetail
    {
        public string RetroDocNo {get; set;}
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string PICFinance { get; set; }
        public DateTime DateFinance { get; set; }
        public string PICSupplier { get; set; }
        public DateTime DateSupplier { get; set; }
        public string PICPUD { get; set; }
        public DateTime DatePUD { get; set; }
    }
}