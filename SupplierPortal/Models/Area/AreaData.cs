﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Area
{
    public class AreaData
    {
       public string AREA_CD { set; get; }
       public string AREA_NM { set; get; }
       public string PLANT { set; get; }
       public string PLANT_NM { set; get; }
       public string PIC_NAME { set; get; }
       public string EMAIL_TO { set; get; }
       public string EMAIL_CC { set; get; }
       public string EMAIL_BCC{ set; get; }
       public string TELEPHONE { set; get; }
       public string MOBILE { set; get; }
      
       public string CREATED_BY { set; get; }
       public DateTime? CREATED_DT { set; get; }
       public string CHANGED_BY { set; get; }
       public DateTime? CHANGED_DT { set; get; }
    }
}