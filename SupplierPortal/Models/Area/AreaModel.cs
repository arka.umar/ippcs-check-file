﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Area
{
    public class AreaModel
    {
        public List<AreaData> AreaDatas { set; get; }

        public AreaModel()
        {
            AreaDatas = new List<AreaData>();
        }
    }
}