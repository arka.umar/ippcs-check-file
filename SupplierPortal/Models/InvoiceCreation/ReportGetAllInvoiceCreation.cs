﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceCreation
{
    public class ReportGetAllInvoiceCreation
    {
        public int ClientID { get; set; }
        public string SupplierCode { get; set; }
        public string PoNumber { get; set; }
        public string PoItemNo { get; set; }
        public string NewKanbanMaterialNumber { get; set; }
        public string IcsMaterialNumber { get; set; }
        public string ProdPurpose { get; set; }
        public string SourceType { get; set; }
        public string PackingType { get; set; }
        public string SuffixColour { get; set; }
        public string MaterialDescription { get; set; }
        public string MatDoc { get; set; }
        public string MatDocYear { get; set; }
        public int ItemNo { get; set; }
        public string CompPrice { get; set; }
        public string SupplierManifest { get; set; }
        public DateTime? DocumentDate { get; set; }
        public decimal IcsQty { get; set; }
        public string OrderNumber { get; set; }
        public string DockCode { get; set; }
        public string SupplierInvoiceNo { get; set; }
        public decimal SuppQty { get; set; }
        public decimal Price { get; set; }
        public string Curr { get; set; }
        public decimal Amount { get; set; }
        public string Remarks { get; set; }
    }
    public class ReportGetAllInvoiceCreationExcel
    {
        public int ClientID { get; set; }
        public string SupplierCode { get; set; }
        public string PoNumber { get; set; }
        public string PoItemNo { get; set; }
        public string NewKanbanMaterialNumber { get; set; }
        public string MaterialNumber { get; set; }
        public string ProdPurpose { get; set; }
        public string SourceType { get; set; }
        public string PackingType { get; set; }
        public string SuffixColour { get; set; }
        public string MaterialDescription { get; set; }
        public string MatDocNo { get; set; }
        // public string MatDocYear { get; set; }
        public int ItemNo { get; set; }
        public string CompPrice { get; set; }
        public string SupplierManifest { get; set; }
        public string DocumentDate { get; set; }
        public decimal Quantity { get; set; }
        public string OrderNumber { get; set; }
        public string DockCode { get; set; }
        public string InvoiceNo { get; set; }
        public decimal SupplierQuantity { get; set; }
        public string Price { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
    }
}