﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceCreation
{
    public class InvoiceCreationDetail
    {
        public string MaterialNumber { set; get; }
        public int RowNumber { set; get; }
        public string PackingType { set; get; }
        public string SuffixColour { set; get; }
        public string MaterialDescription { set; get; }
        public string SupplierManifest { set; get; }
        public DateTime DocumentDate { set; get; }
        public string DockCode { set; get; }
        public int ICSQty { set; get; }
        public decimal PartPrice { set; get; }
        public decimal Curr { set; get; }
        public string Currency { set; get; } 
        public decimal Amount { set; get; }
       
    }
}