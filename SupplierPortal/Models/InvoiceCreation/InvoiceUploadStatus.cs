﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceCreation
{
    public class InvoiceUploadStatus
    {
        public long ProcessId { get; set; }
        public string SuppCode { get; set; }
        public string InvoiceNo { get; set; }
        public string ExistingInvoiceNo { get; set; }
        public string SupplierManifest { get; set; }
        public int? StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}