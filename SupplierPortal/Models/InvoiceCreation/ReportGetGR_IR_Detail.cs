﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceCreation
{
    public class ReportGetGR_IR_Detail
    {
        public string SUPPLIER_CD { get; set; }
        public string PERIOD { get; set; }
        public DateTime DOC_DT { get; set; }
        public string MAT_DOC_NO { get; set; }
        public string INV_REF_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string MAT_NO { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public int MOVEMENT_QTY { get; set; }
        public string ITEM_CURR { get; set; }
        public decimal PO_DETAIL_PRICE { get; set; }
        public decimal GR_IR_AMT { get; set; } 
    }
}