﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceCreation
{
    public class InvoiceCreationPreview
    {
        public string DataKey { get; set; }
        public string InvoiceNo { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }

        public DateTime? InvoiceDate { get; set; }
        public DateTime? InvoiceTaxDate { get; set; }
        public DateTime DocDate { get; set; }
        public string DocDateString { get { return DocDate.ToString("dd.MM.yyyy"); } }
        public string InvoiceTaxNo { get; set; }

        public string Currency { get; set; }

        public decimal RetroTaxRate { get; set; }
        public decimal TurnOver { get; set; }
        public decimal InvoiceTax { get; set; }
        //Add By Arka.Taufik 2022-03-22
        public string InvoiceTaxCd { get; set; }
        public decimal RetroAmount { get; set; }
        public int StampIncluded { get; set; }
        public int StampAmount { get; set; }
        public decimal PPVAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public string RetroDocNo { get; set; }
        public int TakenRate { get; set; }
        public decimal? RetroRemaining { get; set; }
        public string Manifests { get; set; }
        public int TotalManifest { get; set; }
        public string CommandText { get; set; }
        public string BypassFlag { get; set; }

        public string DispFormat
        {
            get
            {
                if (Currency.Equals("IDR"))
                {
                    return Currency + " #,##0";
                }
                else
                {
                    return Currency + " #,##0.00";
                }
            }
        }

    }
}
