﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.InvoiceCreation
{
    public class InvoiceCreation
    {
        public string MANIFEST_NO { set; get; }
        public string ORDER_TYPE { set; get; }
        public string RCV_PLANT_CD { set; get; }
        public string SUPPLIER_CD { set; get; }
        public string DOCK_CD { set; get; }
        public string TOTAL_ITEM { set; get; }
        public string TOTAL_QTY { set; get; }
        public string MANIFEST_RECEIVE_FLAG { set; get; }
        public string SupplierManifest { set; get; }
        public int RowNumber { set; get; }
        public string PONo { set; get; }
        public string Currency { set; get; }
        public double Amount { set; get; }
        public string OrderNo { set; get; }
        public string DockCode { set; get; }
        public string SuppCode { set; get; }
        public string SuppName { set; get; }
        public DateTime? DocumentDate { set; get; }

        public string InvoiceNo { set; get; }
        public string InvoiceTaxType { set; get; }
        public string Status { set; get; }
        public string StatusName { set; get; }

        public string CreatedBy { set; get; }
        public DateTime? CreatedDate { set; get; }
    }
    public class InvoiceCreationErrorPosting
    {
        public string MANIFEST_NO { get; set; }

        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public Int32 ORDER_QTY { get; set; }
        public Int32 RECEIVED_QTY { get; set; }
        public Int32 REMAINING_QTY { get; set; }
        public String RECEIVED_STATUS { get; set; }
        public Int32 DAMAGE_QTY { get; set; }
        public Int32 MISSPART_QTY { get; set; }
        public Int32 SHORTAGE_QTY { get; set; }
        public string DESCRIPTION { get; set; }
        public string AREA { get; set; }
        public string DIVISION { get; set; }
        public string GROUP { get; set; }
        public string REMARK { get; set; }

        public string FD_EXCHANGE_RATE { get; set; }
        public string FD_IM_PERIOD { get; set; }
        public string FD_STANDARD_COST { get; set; }

        public string PCD_DOCK_CODE { get; set; }
        public string PCD_MATERIAL_MASTER { get; set; }
        public string PCD_ROUTING { get; set; }
        public string PCD_PART_LIST { get; set; }
        public string PCD_OTHERS { get; set; }

        public string PAD_MATERIAL_MASTER { get; set; }
        public string PAD_DOCK_CODE { get; set; }
        public string PAD_MATDOCK { get; set; }
        public string PAD_LOCATION_CODE { get; set; }
        public string PAD_OTHERS { get; set; }

        public string PUD_MATERIAL_PRICE { get; set; }
        public string PUD_STANDARD_PRICE { get; set; }
        public string PUD_PO { get; set; }
        public string PUD_SOURCE_LIST { get; set; }
        public string PUD_SUPPLIER_MASTER { get; set; }
        public string PUD_OTHERS { get; set; }

        public string PVD_MATERIAL_PRICE { get; set; }
        public string PVD_PACKING_TYPE { get; set; }
        public string PVD_OTHERS { get; set; }

        public string ISTD_SYSTEM { get; set; }
        public string ISTD_OTHERS { get; set; }
    }
    //Add By Arka.Taufik 2022-03-22
    public class InvoiceCreationTax
    {
        public string TAX_CD { get; set; }
        public string TAX_NAME { get; set; }
        public string RATE { get; set; }
    }
}