﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Html.Helper.DevExpress;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace Portal.Models.InvoiceCreation
{
    public class InvoiceCreationModel
    {
        public List<InvoiceCreation> InvoiceCreationLists { get; set; }
        public List<InvoiceCreationErrorPosting> InvoiceCreationErrorPosting { get; set; }

        public List<InvoiceCreationDetail> InvoiceCreationDetails { get; set; }

        public List<InvoiceCreationPreview> Preview { get; set; }
        public InvoiceCreationPreview InvoicePreview { get; set; }
        private Dictionary<string, string> filter = new Dictionary<string, string>();
        private GridViewModel gvm = null;

        public InvoiceCreationModel()
        {
            InvoiceCreationLists = new List<InvoiceCreation>();
            InvoiceCreationDetails = new List<InvoiceCreationDetail>();
            InvoiceCreationErrorPosting = new List<InvoiceCreationErrorPosting>();
        }

        public string PackingType { get; set; }
        public string SuffixColour { get; set; }
        public string MaterialDescription { get; set; }
        public string SupplierManifest { get; set; }
        public DateTime DocumentDate { get; set; }
        public string DockCode { get; set; }
        public int ICSQty { get; set; }
        public decimal PartPrice { get; set; }
        public decimal Curr { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }


        public string LockReff { get; set; }

        public GridViewModel GetGridViewModel()
        {

            if (gvm == null)
            {
                Debug.WriteLine("new GridViewModel");
                gvm = new GridViewModel();
                gvm.KeyFieldName = "SupplierManifest";
                gvm.Columns.Add("SupplierManifest");
                gvm.Columns.Add("PONo");
                gvm.Columns.Add("Currency");
                gvm.Columns.Add("Amount");
                gvm.Columns.Add("OrderNo");
                gvm.Columns.Add("DockCode");
                gvm.Columns.Add("SuppCode");
                gvm.Columns.Add("DocumentDate");
                gvm.Columns.Add("InvoiceNo");
                gvm.Columns.Add("RowNumber");
                gvm.FilterExpression = "";

            }
            // DebugGridViewModel();
            return gvm;
        }


        public void DebugGridViewModel()
        {
            // Trace("InvoiceCreationModel");
            Debug.WriteLine("GridViewModel");
            Debug.WriteLine("\t FilterExpression : " + (gvm.FilterExpression ?? "null"));
            Debug.WriteLine("\t pager");
            if (gvm.Pager == null)
            {
                Debug.WriteLine("\t\t Pager is null");

            }
            else
            {
                Debug.WriteLine("\t\t Size " + gvm.Pager.PageSize + "  Index " + gvm.Pager.PageIndex);
            }
            Debug.WriteLine("\t columns");
            if (gvm.Columns == null)
            {
                Debug.WriteLine("\t\t Columns is null");
            }
            else
            {
                Debug.WriteLine("\t\t ColumnCount=" + gvm.Columns.Count);
                foreach (var c in gvm.Columns)
                {
                    Debug.WriteLine("\t\t Column : " + c.FieldName);
                }

            }
            Debug.WriteLine("\t SortedColumns ");
            if (gvm.SortedColumns == null)
            {
                Debug.WriteLine("\t\tSortedColumns is null");
            }
            else
            {
                Debug.WriteLine("\t\tSortedColumns.Count = " + gvm.SortedColumns.Count);
                foreach (var columnState in gvm.SortedColumns)
                {
                    Debug.WriteLine("\t\tField : " + columnState.FieldName);
                    if ((gvm.Columns != null) && (gvm.Columns[columnState.FieldName] == null))
                    {
                        Debug.WriteLine(string.Format("\t\tColumns[{0}] is null", columnState.FieldName));
                    }


                }
            }

        }

        public void SetGridViewModel(GridViewModel g)
        {
            gvm = g;
        }

        public GridViewModel Gridviewmodel
        {
            get { return gvm; }
            set { gvm = value; }
        }

        public Dictionary<string, string> Filter
        {
            get
            {
                return filter;
            }
        }

    }
}