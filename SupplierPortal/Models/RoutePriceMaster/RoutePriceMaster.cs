﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RoutePriceMaster
{
    public class RoutePriceMaster
    {
        public string ROUTE_CD { get; set; }
        public string ROUTE_NAME { get; set; }
        public string LP_CD { get; set; }
        public int PRICE { get; set; }
        public bool VALID_FLAG { get; set; }
        public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
    }

    public class CheckResult
    {
        public int result { get; set; }
    }

}