﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RoutePriceMaster
{
    public class RoutePriceMasterModel
    {
        public List<RoutePriceMaster> RoutePriceMaster { get; set; }
        public RoutePriceMasterModel()
        {
            RoutePriceMaster = new List<RoutePriceMaster>();
        }   
    }
}