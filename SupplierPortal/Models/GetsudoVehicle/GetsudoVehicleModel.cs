﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GetsudoVehicle
{
    public class GetsudoVehicleModel
    {
        public List<GetsudoVehicleDetail> GetsudoVehicleDetails { get; set; }
        public List<GetsudoVehicle> GetsudoVehicles { get; set; }
        public GetsudoVehicleModel()
        {
            GetsudoVehicleDetails = new List<GetsudoVehicleDetail>();
            GetsudoVehicles = new List<GetsudoVehicle>();
        }
    }
}