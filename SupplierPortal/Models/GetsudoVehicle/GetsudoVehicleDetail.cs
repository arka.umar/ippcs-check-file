﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GetsudoVehicle
{
    public class GetsudoVehicleDetail
    {

        public int No { get; set; }
        public DateTime ProductionMonth { get; set; }
        public string ProductionType { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Download { get; set; }

    }
}