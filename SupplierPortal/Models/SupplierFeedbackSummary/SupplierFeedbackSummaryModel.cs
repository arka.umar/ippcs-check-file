﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierFeedbackSummaryModel
    {
        public List<SupplierFeedbackSummary> SupplierFeedbackSummary { set; get; }
        public List<SupplierFeedbackSummaryDetails> SupplierFeedbackSummaryDetails { set; get; }

        public SupplierFeedbackSummaryModel()
        {
            SupplierFeedbackSummary = new List<SupplierFeedbackSummary>();
            SupplierFeedbackSummaryDetails = new List<SupplierFeedbackSummaryDetails>();
        }
    } 
}