﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierFeedbackForecastOrderListHeader
    {

        public SupplierFeedbackForecastOrderListHeader()
        {
            supplierFeedbackForecastOrders = new List<SupplierFeedbackForecastOrderListHeader>();
        }

        public List<SupplierFeedbackForecastOrderListHeader> supplierFeedbackForecastOrders { set; get; }

    }
}