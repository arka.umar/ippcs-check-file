﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierFeedbackSummary
    {
        public String SUPPLIER_CODE { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PRODUCTION_MONTH { get; set; }
        public String TIMING { get; set; }
        public String SUPPLIER_PLANT_CODE { get; set; }
        public Boolean? N_MONTH { get; set; }
        public Boolean? N_1_MONTH { get; set; }
        public Boolean? N_2_MONTH { get; set; }
        public Boolean? N_3_MONTH { get; set; }
        public Boolean IS_CONFIRMED { get; set; }
        public String CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public Boolean LOCK_STATUS { get; set; }
        public String REPLY_BY { get; set; }
        public DateTime? REPLY_DT { get; set; }
    }

    public class SupplierFeedbackSummaryDownload
    {
        public String SUPPLIER_CODE { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PRODUCTION_MONTH { get; set; }
        public String TIMING { get; set; }
        public String SUPPLIER_PLANT_CODE { get; set; }
        public String N_MONTH { get; set; }
        public String N_1_MONTH { get; set; }
        public String N_2_MONTH { get; set; }
        public String N_3_MONTH { get; set; }
        public String IS_CONFIRMED { get; set; }
        public String CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public String LOCK_STATUS { get; set; }
        public String REPLY_BY { get; set; }
        public DateTime? REPLY_DT { get; set; }
    }
}