﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierFeedbackSummaryDetails
    {
        public int OrderID { get; set; }
        public string SupplierCode { set; get; }
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public string DockNo { set; get; }
        public int NQty { set; get; }
        public string NFA { set; get; }
        public string NROK { set; get; }
        public int N1Qty { set; get; }
        public string N1FA { set; get; }
        public string N1ROK { set; get; }
        public string NRNOK { set; get; }
        public string N1RNOK { set; get; }
        public string N2RNOK { set; get; }
        public string N3RNOK { set; get; }
        public int N2Qty { set; get; }
        public string N2FA { set; get; }
        public string N2ROK { set; get; }
        public int N3Qty { set; get; }
        public string N3FA { set; get; }
        public string N3ROK { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDate { set; get; }
    }
}