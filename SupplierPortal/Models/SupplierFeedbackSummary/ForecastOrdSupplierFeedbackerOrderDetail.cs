﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierFeedbackForecastOrderDetail
    {
        public int OrderID { get; set; }
        //public string PartNo { set; get; }
        //public string PartName { set; get; }
        //public string DockNo { set; get; }
        //public int NQty { set; get; }
        //public string NFA { set; get; }
        //public bool NROK { set; get; }
        //public int N1Qty { set; get; }
        //public string N1FA { set; get; }
        //public bool N1ROK { set; get; }
        //public string NRNOK { set; get; }
        //public string N1RNOK { set; get; }
        //public string N2RNOK { set; get; }
        //public string N3RNOK { set; get; }
        //public int N2Qty { set; get; }
        //public string N2FA { set; get; }
        //public bool N2ROK { set; get; }
        //public int N3Qty { set; get; }
        //public string N3FA { set; get; }
        //public bool N3ROK { set; get; }
        //public string CreatedBy { set; get; }
        //public DateTime CreatedDate { set; get; }
        //public string ChangedBy { set; get; }
        //public DateTime ChangedDate { set; get; }
        public string PartNo { get; set; }
        public string SuppPlantCode { get; set; }
        public string PartName { get; set; }
        public string ReceiveArea { get; set; }
        public int KanbanSize { get; set; }
        public int SFX { get; set; }
        public int NQty { get; set; }
        public int N1Qty { get; set; }
        public int N2Qty { get; set; }
        public int N3Qty { get; set; }
        public int Remarks { get; set; }
        public int Message { get; set; }
    }
}