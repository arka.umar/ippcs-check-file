﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierFeedbackForecastOrderListDetail
    {

        public SupplierFeedbackForecastOrderListDetail()
        {
            supplierFeedbackForecastOrderDetails = new List<SupplierFeedbackForecastOrderDetail>();
        }

        public List<SupplierFeedbackForecastOrderDetail> supplierFeedbackForecastOrderDetails { set; get; }

    }
}