﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierList
    {
        public SupplierList()
        {
            supplierlistmodel = new List<SupplierFeedbackSummary>();
        }

        public List<SupplierFeedbackSummary> supplierlistmodel { get; set; }
    }
}