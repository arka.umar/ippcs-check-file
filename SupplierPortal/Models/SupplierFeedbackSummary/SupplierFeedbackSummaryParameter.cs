﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierFeedbackSummary
{
    public class SupplierFeedbackSummaryParameter
    {
        public String SUPPLIER_CODE { set; get; }
        public String SUPPLIER_PLANT_CD { set; get; }
        public String PRODUCTION_MONTH { set; get; }
        public String VERSION { set; get; }
    }
}