﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderMonitoring
{
    public class OrderMonitoringDetail
    {
        public string PROCESS_ID { get; set; }
        public string SEQUENCE_NUMBER { get; set; }
        public string MESSAGE_ID { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string MESSAGE { get; set; }
    }
}