﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderMonitoring
{
    public class OrderMonitoring
    {
        public string PLANT_CD { get; set; }
        public string PLANT_NM { get; set; }
        public string Status1 { get; set; }
        public DateTime? StartDate1 { get; set; }
        public DateTime? EndDate1 { get; set; }
        public string Status2 { get; set; }
        public DateTime? StartDate2 { get; set; }
        public DateTime? EndDate2 { get; set; }
        public string Status3 { get; set; }
        public DateTime? StartDate3 { get; set; }
        public DateTime? EndDate3 { get; set; }

        public string Category { get; set; }
        public string DataControlBy { get; set; }
        public DateTime? DataControlDt { get; set; }
        public string DataSysVal { get; set; }
        public string DataStatus { get; set; }
        public string PDFControlBy { get; set; }
        public DateTime? PDFControlDt { get; set; }
        public string PDFSysVal { get; set; }
        public string PDFStatus { get; set; }
    }
}