﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.OrderMonitoring
{
    public class OrderMonitoringModel
    {
        public List<OrderMonitoring> OrderMonitorings { get; set; }
        public OrderMonitoringModel()
        {
            OrderMonitorings = new List<OrderMonitoring>();
        }

    }
}