﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.NewSystem
{
    public class NewSystem
    {
        public string FUNCTION_ID { get; set; }
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string SYSTEM_REMARK { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

    }
}

