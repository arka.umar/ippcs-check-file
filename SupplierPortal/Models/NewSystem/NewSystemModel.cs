﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.NewSystem
{
    public class NewSystemModel
    {
        public List<NewSystem> NewSystemList { set; get; }
         
        public NewSystemModel()
        {
            NewSystemList = new List<NewSystem>();
        }
    }
}

