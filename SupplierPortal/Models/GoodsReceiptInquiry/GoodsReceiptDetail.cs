﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GoodsReceiptInquiry
{
    public class GoodsReceiptDetail
    {
        public string MANIFEST_NO { set; get; }
        public int No { set; get; }
        public string PART_NO { set; get; }
        public string PART_NAME { set; get; }
        public int ORDER_QTY { set; get; }
        public int RemainingQty { set; get; }
        public int RECEIVED_QTY { set; get; }
        public string RECEIVED_STATUS { set; get; }
        //public int QtyError { set; get; }
        public int DAMAGE_QTY { set; get; }
        public int MISSPART_QTY { set; get; }
        public int SHORTAGE_QTY { set; get; }
        public string SupplierMaster { set; get; }
        public string MaterialMaster { set; get; }
        public string PriceMaster { set; get; }
        public string StandardPrice { set; get; }
        public string PostingPeriod { set; get; }
        public string ExtWarehouse { set; get; }
        public string NoPurchaseOrder { set; get; }
        public string ProcessLocked { set; get; }
        public string Notice { set; get; }
        public string Status { set; get; }
    }
}