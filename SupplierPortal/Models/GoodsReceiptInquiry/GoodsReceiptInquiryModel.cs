﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GoodsReceiptInquiry
{
    public class GoodsReceiptInquiryModel
    {
        public List<GoodsReceiptInquiryData> GoodsReceiptInquiryList { get; set; }
        public List<GoodsReceiptManifestStatus> GoodsReceiptManifestStatusList { get; set; }
        public List<GoodsReceiptInquiryData> GoodsReceiptInquirys { get; set; }
        public List<GoodsReceiptDetail> GoodsReceiptDetails { get; set; }
        public List<GoodsReceiptErrorPIC> GoodsReceiptErrorPICs { get; set; }

        public GoodsReceiptInquiryModel()
        {
            GoodsReceiptInquiryList = new List<GoodsReceiptInquiryData>();
            GoodsReceiptManifestStatusList = new List<GoodsReceiptManifestStatus>();
            GoodsReceiptInquirys = new List<GoodsReceiptInquiryData>();
            GoodsReceiptDetails = new List<GoodsReceiptDetail>();
            GoodsReceiptErrorPICs = new List<GoodsReceiptErrorPIC>();
        }
    }
}