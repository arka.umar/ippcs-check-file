﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GoodsReceiptInquiry
{
    public class GoodsReceiptManifestStatus
    {
        public String STATUS_CODE { get; set; }
        public String STATUS_NAME { get; set; }
    }
}