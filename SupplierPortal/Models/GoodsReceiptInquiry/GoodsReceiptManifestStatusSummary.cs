﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GoodsReceiptInquiry
{
    public class GoodsReceiptManifestStatusSummary
    {
        public Int32 ORDERED_SUMMARY { get; set; }
        public Int32 DELIVERED_SUMMARY { get; set; }
        public Int32 SCANNED_SUMMARY { get; set; }
        public Int32 APPROVED_SUMMARY { get; set; }
        public Int32 POSTED_SUMMARY { get; set; }
        public Int32 ERROR_POSTING_SUMMARY { get; set; }
        public Int32 INVOICED_SUMMARY { get; set; }
        public Int32 PAID_SUMMARY { get; set; }
        public Int32 CANCELLED_SUMMARY { get; set; }
    }
}