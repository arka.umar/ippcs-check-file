﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GoodsReceiptInquiry
{
    public class GoodsReceiptErrorPIC
    {
        public string ERROR_PIC_TITLE { set; get; }
        public string ERROR_PIC_NAME { set; get; }
        public string ERROR_PIC_DIVISION { set; get; }
        public string ERROR_PIC_PHONE { set; get; }
        public string ERROR_PIC_EXT { set; get; }
        public string ERROR_PIC_EMAIL { set; get; }
    }
}