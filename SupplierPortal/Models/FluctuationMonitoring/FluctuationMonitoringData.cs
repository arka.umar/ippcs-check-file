﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models.Supplier;

namespace Portal.Models.FluctuationMonitoring
{
    public class FluctuationMonitoringData
    {
        public List<FluctuationSystemMapping> FMSystem { get; set; }
        public List<FluctuationData> FMGridData { get; set; }
        public List<SupplierName> FMSupplier { get; set; }
    }

    public class FluctuationSystemMapping
    {
        public String FLUCTUATION_CD { get; set; }
        public String FLUCTUATION_TEXT { get; set; }
    }
}