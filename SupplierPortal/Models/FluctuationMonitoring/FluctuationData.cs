﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.FluctuationMonitoring
{
    public class FluctuationData
    {
        public String FORECAST_TYPE { get; set; }
        public String PRODUCTION_MONTH { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public String DOCK_CD { get; set; }
        public String N_VOLUME { get; set; }
        public String N_FA { get; set; }
        public String T_N_FA { get; set; }
        public String N_FLUCTUATION { get; set; }
        public String T_N_FLUCTUATION { get; set; }
        public String N_1_VOLUME { get; set; }
        public String N_1_FA { get; set; }
        public String T_N_1_FA { get; set; }
        public String N_1_FLUCTUATION { get; set; }
        public String T_N_1_FLUCTUATION { get; set; }
        public String N_2_VOLUME { get; set; }
        public String N_2_FA { get; set; }
        public String T_N_2_FA { get; set; }
        public String N_2_FLUCTUATION { get; set; }
        public String T_N_2_FLUCTUATION { get; set; }
        public String N_3_VOLUME { get; set; }
        public String N_3_FA { get; set; }
        public String T_N_3_FA { get; set; }
        public String N_3_FLUCTUATION { get; set; }
        public String T_N_3_FLUCTUATION { get; set; }
    }
}