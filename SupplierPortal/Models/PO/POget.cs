﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PO
{
    public class POget
    {
        public string PONo { set; get; }
        public DateTime PODate { set; get; }
        public string POType { set; get; }
        public string SHApprover { set; get; }
        public DateTime SHApprovalDate { set; get; }
        public string SHApprovalStatus { set; get; }
        public string DPHApprover { set; get; }
        public DateTime  DPHApprovalDate { set; get; }
        public string DPHApprovalStatus { set; get; }
        public string DHApprovalStatus { set; get; }
        public string DHApprover { set; get; }
        public string CreatedBy { set; get; }

        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDate { set; get; }
        // public DateTime RequiredDate { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
        public string TotalAmount { set; get; }
        public string CurrencyCode { set; get; }
        public string ExchangeRate { set; get; }

        public DateTime DHApprovalDate { set; get; }
        public string NoticeStatus { set; get; }

        public string StatusName { set; get; }

    }
}