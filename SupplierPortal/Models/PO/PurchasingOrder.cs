﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.PO
{
    public class PurchasingOrder
    {
        public int No { set; get; }
        public string PONo { set; get; }
        public DateTime PODate { set; get; }
        public string POType { set; get; }
        public string SHApprover { set; get; }
        public DateTime SHApprovalDate { set; get; }
        public string SHApprovalStatus { set; get; }
        public string DPHApprover { set; get; }
        public DateTime DPHApprovalDate { set; get; }
        public string DPHApprovalStatus { set; get; }
        public string DHApprovalStatus { set; get; }
        public string DHApprover { set; get; }
        public string CreatedBy { set; get; }
        public string CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public string ChangedDate { set; get; }
        // public DateTime RequiredDate { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
        // public string TotalAmount { set; get; }
        public decimal TotalAmount { get; set; } 
        public string CurrencyCode { set; get; }
        public string ExchangeRate { set; get; }
        public DateTime DHApprovalDate { set; get; }
        public string NoticeStatus { set; get; }
        
        public string SHApprovalStatusPic { set; get; }
        public string DPHApprovalStatusPic { set; get; }
        public string DHApprovalStatusPic { set; get; }
        
        public string Notice { set; get; }
        public string Otentik { set; get; }
        public string PDFDownload { set; get; }

        public string CurrPIC { set; get; }
        public string CurrPICName { set; get; }
        public string SHName { set; get; }
        public string DPHName { set; get; }
        public string DHName { set; get; }

    }


}