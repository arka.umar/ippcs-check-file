﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PO
{
    public class POApprovalModel
    {
        public List<PurchasingOrder> PurchasingOrders = new List<PurchasingOrder>();

        public List<PurchasingOrderPart> PurchasingOrderParts = new List<PurchasingOrderPart>();

        public List<ApprovalOverview> ApprovalSH = new List<ApprovalOverview>();
        public List<ApprovalOverview> ApprovalDpH = new List<ApprovalOverview>();
        public List<ApprovalOverview> ApprovalDH = new List<ApprovalOverview>();
        public POApprovalModel()
        {
            PurchasingOrders = new List<PurchasingOrder>();
            PurchasingOrderParts = new List<PurchasingOrderPart>();
            ApprovalSH = new List<ApprovalOverview>();
            ApprovalDpH = new List<ApprovalOverview>();
            ApprovalDH = new List<ApprovalOverview>();
        }

    }
}