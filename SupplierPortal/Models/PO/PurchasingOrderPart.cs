﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PO
{
    public class PurchasingOrderPart
    {
        public Int32 No { set; get; }
        public string PONo { set; get; }
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public string ColorSuffix { set; get; }
        public string PackingType { set; get; }
        public string Qty { set; get; }
        // public string RequiredDate { set; get; }
        public decimal Price { set; get; }
        public string CreatedBy { set; get; }
        public string CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public string ChangedDate { set; get; }
        public decimal Amount { set; get; }
        public string CurrencyCode { set; get; }
    }
}