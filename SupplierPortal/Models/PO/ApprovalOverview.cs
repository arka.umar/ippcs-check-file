﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.PO
{
    public class ApprovalOverview 
    {

        public string Name { set; get; }
        public string Status { set; get; }
        public DateTime Date { set; get; }
        public string Comment { set; get; }
    }
}
