﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrderPrinting
{
    public class DailyOrderPrintingModel
    {
        public List<DailyOrderPrinting> RegularOrders { get; set; }
        public List<DailyOrderPrinting> EmergencyOrders { get; set; }
        public List<DailyOrderPrinting> ComponentPartOrders { get; set; }
        public List<DailyOrderPrinting> JunbikiOrders { get; set; }
        public List<DailyOrderPrinting> ProblemPartOrders { get; set; }
        public List<DailyOrderPrinting> ServicePartExport { get; set; }
        public DailyOrderPrintingModel()
        {
            RegularOrders = new List<DailyOrderPrinting>();
            EmergencyOrders = new List<DailyOrderPrinting>();
            ComponentPartOrders = new List<DailyOrderPrinting>();
            JunbikiOrders = new List<DailyOrderPrinting>();
            ProblemPartOrders = new List<DailyOrderPrinting>();
            ServicePartExport = new List<DailyOrderPrinting>();
        }
    }

    public class DailyOrderPrintingModelSPEX
    {
        public List<DailyOrderPrintingSPEX> ServicePartExport { get; set; }
        public DailyOrderPrintingModelSPEX()
        {
            ServicePartExport = new List<DailyOrderPrintingSPEX>();
        }
    }

    public class DailyOrderPrintingModelCPO
    {
        public List<DailyOrderPrintingCPO> ComponentPartOrders { get; set; }
        public DailyOrderPrintingModelCPO()
        {
            ComponentPartOrders = new List<DailyOrderPrintingCPO>();
        }
    }
}