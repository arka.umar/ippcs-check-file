﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrderPrinting
{
    public class DailyOrderPrintingDetail
    {
        public string KANBAN_ID { get; set; }
        public string STATUS { get; set; }
    }
}