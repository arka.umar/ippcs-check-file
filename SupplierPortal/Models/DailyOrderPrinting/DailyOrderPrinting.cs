﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrderPrinting
{
    public class DailyOrderPrinting
    {
        public String MANIFEST_NO { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String DOCK_CD { get; set; }
        public String ORDER_NO { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String ORDER_TYPE { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public DateTime? ROUTE_DT { get; set; }
        public String ROUTE_NO { get; set; }
        public DateTime? KANBAN_PRINT_DT { get; set; }
        public String LOGISTIC_PARTNER { get; set; }
        public String RCV_PLANT_CD { get; set; }
        public String PLANT_NM { get; set; }
        public int PRINT_FLAG { get; set; }
        public String PRINTED_BY { get; set; }
        public int TOTAL_QTY { get; set; }
        public int TOTAL_ITEM { get; set; }
        public int ORDER_PLAN_QTY_LOT { get; set; }
        public int TOTAL_SKID { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public String FTP_FLAG { get; set; }
       
    }

    public class DailyOrderPrintingSPEX
    {
        public String MANIFEST_NO { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String DOCK_CD { get; set; }
        public String ORDER_NO { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String ORDER_TYPE { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public DateTime? ROUTE_DT { get; set; }
        public String ROUTE_NO { get; set; }
        public DateTime? KANBAN_PRINT_DT { get; set; }
        public String LOGISTIC_PARTNER { get; set; }
        public String RCV_PLANT_CD { get; set; }
        public String PLANT_NM { get; set; }
        public int PRINT_FLAG { get; set; }
        public String PRINTED_BY { get; set; }
        public int TOTAL_QTY { get; set; }
        public int TOTAL_ITEM { get; set; }
        public int ORDER_PLAN_QTY_LOT { get; set; }
        public int TOTAL_SKID { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public String FTP_FLAG { get; set; }
        public String SUB_SUPP_CD { get; set; }
        public String SUB_SUPP_PLANT { get; set; }
        public String SUB_SUPP_CD_PLANT { get; set; }
    }

    public class DailyOrderPrintingCPO
    {
        public String MANIFEST_NO { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String DOCK_CD { get; set; }
        public String ORDER_NO { get; set; }
        public String ORDER_TYPE { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public DateTime? ROUTE_DT { get; set; }
        public String ROUTE_NO { get; set; }
        public DateTime? KANBAN_PRINT_DT { get; set; }
        public String LOGISTIC_PARTNER { get; set; }
        public String RCV_PLANT_CD { get; set; }
        public String PLANT_NM { get; set; }
        public int PRINT_FLAG { get; set; }
        public int TOTAL_ITEM { get; set; }
        public int TOTAL_QTY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public String PRINTED_BY { get; set; }
        public int ORDER_PLAN_QTY_LOT { get; set; }
        public int TOTAL_SKID { get; set; }        
        public String FTP_FLAG { get; set; }

    }
}
