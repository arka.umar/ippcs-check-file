﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrderPrinting
{
    public class ReportEmergencyOrder
    {
        public String ORDER_NO { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PLANT_NM { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String DOCK_CD { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public int PRINT_FLAG { get; set; }
    }

    public class ReportSPEXOrder
    {
        public String ORDER_NO { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUB_SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PLANT_NM { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String DOCK_CD { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public int PRINT_FLAG { get; set; }
    }

    public class ReportCPOOrder
    {
        public String ORDER_NO { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PLANT_NM { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String DOCK_CD { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public int PRINT_FLAG { get; set; }
    }

    public class ReportSPEXv2Order
    {
        public String ORDER_NO { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUB_SUPP_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PLANT_NM { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String DOCK_CD { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public int PRINT_FLAG { get; set; }
    }

}