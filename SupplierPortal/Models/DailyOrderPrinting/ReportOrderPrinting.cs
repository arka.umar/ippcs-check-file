﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrderPrinting
{
    public class ReportOrderPrinting
    {
        public DateTime? ROUTE_DT { get; set; }
        public String ROUTE_NO { get; set; }
        public DateTime? KANBAN_PRINT_DT { get; set; }
        public String DOCK_CD { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PLANT_NM { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String LOGISTIC_PARTNER { get; set; }
        public String ORDER_NO { get; set; }
        public int PRINT_FLAG { get; set; }
    }
}