﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrderPrinting
{
    public class DailyOrderPrintingSchedule
    {
        public string No { set; get; }
        public string PRINTING_TIME { set; get; }
    }
}