﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrderPrinting
{
    public class DailyOrderPrintingDetailModel
    {
        public List<DailyOrderPrintingDetail> orderListData { get; set; }

        public DailyOrderPrintingDetailModel()
        {
            orderListData = new List<DailyOrderPrintingDetail>();
        }
    }
}