﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLInquiry
{
    public class DCLRouteDestination
    {
        public string SupplierCode { get; set; }
        public string ArrivalPlan { get; set; }
        public string ArrivalActual { get; set; }
        public string ArrivalStatus { set; get; }
        public string DeparturePlan { get; set; }
        public string DepartureActual { get; set; }
        public string DepartureStatus { set; get; }
        public string Status { get; set; }
    }
}