﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Web.Mvc;

namespace Portal.Models.DCLInquiry
{
    public class DCLInquiryModel
    {
        public GridViewModel GvModel { get; set; } //added change//
        public GridViewModel GvModelAdditional { get; set; } //added change//

        public List<DCLInquiry> DCLInquiryRegionals { set; get; }
        public List<DCLInquiryDb> DCLInquiryAdditionals { set; get; }
        public List<DCLInquiryDb> DCLInquiryDbModel { set; get; }
        public List<DCLSupplier> DCLSupplier { set; get; }
        public List<DCLPickUp> DCLPickUp { set; get; }
        public List<DCLRouteDestination> DCLRouteDestinations { set; get; }

        public DCLInquiryModel()
        {
            DCLInquiryRegionals = new List<DCLInquiry>();
            DCLInquiryAdditionals = new List<DCLInquiryDb>();
            DCLInquiryDbModel = new List<DCLInquiryDb>();
            DCLSupplier = new List<DCLSupplier>();
            DCLPickUp = new List<DCLPickUp>();
            DCLRouteDestinations = new List<DCLRouteDestination>();
        }
    }
}