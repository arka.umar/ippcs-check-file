﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLInquiry
{
    public class DCLMessageStatus
    { 
        public string StatEmpty { get; set; }
        public string StatNew { get; set; }
        public string StatRead { get; set; }
        public string StatClose { get; set; }
    }
}