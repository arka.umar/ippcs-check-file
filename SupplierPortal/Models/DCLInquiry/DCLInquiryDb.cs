﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLInquiry
{
    public class DCLInquiryDb
    {
        public string DELIVERY_NO { set; get; }
        public string REVISE_NO { set; get; }
        public DateTime PICKUP_DT { set; get; }
        public string ROUTE_RATE { set; get; }
        public string LP_CD { set; get; }
        public string LOG_PARTNER_NAME { get; set; }
        public string DRIVER_NAME { set; get; }
        public string DELIVERY_STS { set; get; }
        public string DEPARTURE_STATUS { set; get; }
        public string ARRIVAL_STATUS { set; get; }  
        public string CONFIRM_STS { set; get; }
        public string REQUISITIONER_BY { set; get; }
        public DateTime REQUISITIONER_DT { set; get; }
        public string DOWNLOAD_BY { set; get; }
        public DateTime DOWNLOAD_DT { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime CHANGED_DT { set; get; }
        public string INVOICE_BY { set; get; }
        public DateTime INVOICE_DT { set; get; }
        public string Notif { set; get; }
        public string DriverManifest { get; set; }
        public string DELIVERY_REASON { get; set; }
        public string APPROVE_BY { set; get; }
        public DateTime APPROVE_DT { set; get; }
        public string VEHICLE_NO { set; get; }
        public string MAP { set; get; }
    }
}