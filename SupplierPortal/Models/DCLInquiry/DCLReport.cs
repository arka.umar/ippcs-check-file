﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLInquiry
{
    public class DCLReport
    {
        public string DeliveryNo { get; set; }
        public DateTime PickupDate { get; set; }
        public string RouteCode { get; set; }
        public string LPName { get; set; }
        public string DriverName { get; set; }
        public string Truck { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string DockCode { get; set; }
        public DateTime ArrivalPlan { get; set; }
        public DateTime ArrivalActual { get; set; }
        public DateTime DepartualPlan { get; set; }
        public DateTime DepartureActual { get; set; }
        public string Station { get; set; }
        public string ManifestNo { get; set; }
    }
}