﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PostToICS
{
    public class PostToICSModel
    {
        public List<PostToICSData> PostToICSs { get; set; }

        public PostToICSModel()
        {
            PostToICSs = new List<PostToICSData>();
        }
    }
}