﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PostToICS
{
    public class PostToICSData
    {
        public string InvoiceNo { get; set; }
        public string SupplierCode { get; set; }
        public string PlantCode { get; set; }
        public string DockCode { get; set; }
        public string PartNumber { get; set; }
        public string PartName { get; set; }
        public int OrderQty { get; set; }
        public int OrderQtyLot { get; set; }
        public int QtyPerContainer { get; set; }
        public DateTime DropStatusDate { get; set; }
        public int ReceivedQty { get; set; }
        public int ICSFlag { get; set; }
        public int CancelQty { get; set; }
        public int CancelledQty { get; set; }
        public string CancellationFlag { get; set; }
        public string CancellationBy { get; set; }
        public DateTime CancellationDate { get; set; }
    }
}