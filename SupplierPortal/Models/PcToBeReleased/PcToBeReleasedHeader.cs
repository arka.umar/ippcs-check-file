﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Portal.Models.PcToBeReleased
{
    public class PcToBeReleasedHeader
    {
        public string PONo { get; set; }
        public string Action { get; set; }
        public string SH { get; set; }
        public string DPH { get; set; }
        public string DH { get; set; }
        public string PCNO { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string DownloadBy {get; set;}
        public string DownloadSts {get; set;}
        public string PCFile { get; set; }
        public string Confirmation { get; set; }
        public string Notice { get; set; }
        public DateTime DownloadDt { get; set; }
        public DateTime PCDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}