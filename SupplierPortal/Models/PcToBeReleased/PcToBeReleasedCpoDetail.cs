﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PcToBeReleased
{
    public class PcToBeReleasedCpoDetail
    {
        public string Action { get; set; }
        public string SH { get; set; }
        public string DPH { get; set; }
        public string DH { get; set; }
        public string Description { get; set; }
        public DateTime PODate { get; set; }
        public string Year { get; set; }
    }
}