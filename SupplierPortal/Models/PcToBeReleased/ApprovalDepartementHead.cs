﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PcToBeReleased
{
    public class ApprovalDepartementHead
    {
        public int No { set; get; }
        public string Name { set; get; }
        public string Status { set; get; }
        public DateTime Date { set; get; }
        public string Comment { set; get; }
    }
}