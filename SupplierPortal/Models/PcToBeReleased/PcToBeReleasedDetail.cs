﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PcToBeReleased
{
    public class PcToBeReleasedDetail
    {
        public string Action { get; set; }
        public string SH { get; set; }
        public string DPH { get; set; }
        public string DH { get; set; }
        public string PCNO { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public DateTime PCDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; } 
        public DateTime EffectiveDate { get; set; }
        public DateTime tglSH { get; set; }
        public string Description { get; set; }
        public DateTime PODate { get; set; }
        public string Year { get; set; }
        public string ViewDetaile { get; set; }
        public string Notice { get; set; }
        
        public string SHApprovalStatus { set; get; }
        public string DPHApprovalStatus { set; get; }
        public string DHApprovalStatus { set; get; }

        public string SHApprovalStatusIcon { set; get; }
        public string DPHApprovalStatusIcon { set; get; }
        public string DHApprovalStatusIcon { set; get; }

        public DateTime? SHDate { get; set; }
        public string SHName { get; set; }
        public DateTime? DPHDate { get; set; }
        public string DPHName { get; set; }
        public DateTime? DHDate { get; set; }
        public string DHName { get; set; }
        public string Status { set; get; }
        public string CurrPIC { set; get; }
        public string CurrPICFullName { set; get; }
        public string ReleaseByUserName { set; get; }
        public string ReleaseByFullName { set; get; } 
        public string PDFDownload { set; get; }
    }
}