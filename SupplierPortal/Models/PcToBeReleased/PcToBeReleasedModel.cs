﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PcToBeReleased
{
    public class PcToBeReleasedModel
    {
        public List<PcToBeReleasedDetail> PcToBeaReleasedDetails = new List<PcToBeReleasedDetail>();
        public List<PcToBeReleasedCpoDetail> PcToBeReleasedCpoDetails = new List<PcToBeReleasedCpoDetail>();
        public List<ApprovalSectionHead> ApprovalSectionHeadList = new List<ApprovalSectionHead>();
        public List<ApprovalDivisionHead> ApprovalDivisionHeadList = new List<ApprovalDivisionHead>();
        public List<ApprovalDepartementHead> ApprovalDepartementHeadList = new List<ApprovalDepartementHead>();
        public List<PcToBeReleasedHeader> PcToBeReleasedHeaders = new List<PcToBeReleasedHeader>();
        public PcToBeReleasedModel()
        {
            PcToBeaReleasedDetails = new List<PcToBeReleasedDetail>();
            PcToBeReleasedCpoDetails = new List<PcToBeReleasedCpoDetail>();
            ApprovalSectionHeadList = new List<ApprovalSectionHead>();
            ApprovalDivisionHeadList = new List<ApprovalDivisionHead>();
            ApprovalDepartementHeadList = new List<ApprovalDepartementHead>();
            PcToBeReleasedHeaders = new List<PcToBeReleasedHeader>();
        }
    }
}