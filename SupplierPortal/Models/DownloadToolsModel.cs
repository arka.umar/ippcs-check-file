﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.Credential;

namespace Portal.Models
{
    public class DownloadToolsModel
    {
        public DataTable DataDownloads { set; get; }

        public DataTable DataUsers { set; get; }

        public DataTable DataGroups { set; get; }

        public List<RetrievalGroup> RetrievalGroups { set; get; }

        public List<RetrievalGroup> RetrievalCategories { set; get; }

        public DownloadToolsModel()
        {
            RetrievalGroups = new List<RetrievalGroup>();
            RetrievalCategories = new List<RetrievalGroup>();
        }
    }
}