﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLAndon
{
    public class DCLAndonPickUp
    {
        public string ReceiveingDock { set; get; }
        public string Station { set; get; }
        public string ArrivalPlan { set; get; }
        public string ArrivalActual { set; get; }
        public string ArrivalStatus { set; get; }
        public string ArrivalGap { set; get; }

        public string DeparturePlan { set; get; }
        public string DepartureActual { set; get; }
        public string DepartureStatus { set; get; }
        public string DepartureGap { set; get; }

        public string CarriedManifest { set; get; }
        public string ManifestType { set; get; }
    }
}