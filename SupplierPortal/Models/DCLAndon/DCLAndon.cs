﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLAndon
{
    public class DCLAndon
    {
        public string No { get; set; }
        public DateTime RouteDate { get; set; }
        public string Dock { get; set; }
        public string Station { get; set; }
        public string RouteName { get; set; }
        public string TripNo { get; set; }
        public string ArrivalPlan { get; set; }
        public string ActualArrival { get; set; }
        public string Status { get; set; }
        public string GAPARRV { get; set; }
        public string DeparturePlan { get; set; }
        public string ActualDeparture { get; set; }
        public string StatusDept { get; set; }
        public string GAPDEPT { get; set; }
        public string LPNAME { get; set; }
        public string Notif { get; set; }
        public int Months { get; set; }
    }
}