﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLAndon
{
    public class DCLAndonSupplier
    {
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
    }
}