﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLAndon
{
    public class DCLAndonModel
    {
        public List<DCLAndon> DCLAndons { set; get; }
        public List<DCLAndonCombo> Combonye { set; get; }

        public DCLAndonModel()
        {
            DCLAndons = new List<DCLAndon>();
            Combonye = new List<DCLAndonCombo>();
        }
    }
}