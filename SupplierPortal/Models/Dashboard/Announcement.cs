﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Portal.Models.Dashboard
{
    public class Announcement
    {
        public string AnnouncementID { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public int AnnouncementTypeID { set; get; }
        public DateTime ExpiredOut { set; get; }
        public string Phone { set; get; }
        public bool EmailFlag { set; get; }
        public string RecipientEmail { set; get; }
        public string UserDivision { set; get; }
        public DateTime CreateDate { set; get; }
    }
}
