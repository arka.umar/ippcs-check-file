﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Dashboard
{
    public class AnnouncementType
    {
        public string AnnouncementTypeID { set; get; }
        public string TypeDescription { set; get; }
        public string TypeIcon { set; get; }
    }
}