﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Messaging.Notification;

namespace Portal.Models.Dashboard
{
    public class DashboardModel
    {
        public DashboardModel()
        {
            Notifications = new List<NotificationMessage>();
        }

        public List<NotificationMessage> Notifications { private set; get; }
    }
}