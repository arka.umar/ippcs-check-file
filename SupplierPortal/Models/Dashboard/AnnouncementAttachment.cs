﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Portal.Models.Dashboard
{
    class AnnouncementAttachment
    {
        public string AnnouncementID { set; get; }
        public string SeqNo { set; get; }
        public string FileURL { set; get; }
    }
}
