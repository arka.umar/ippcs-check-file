﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    public class Notification
    {
        public string NotificationID { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public string NotificationLevel { set; get; }
        public string ActionURL { set; get; }
        public DateTime CreateDate { set; get; }
    }
}
