﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.TransactionMaster
{
    public class TransactionMasterModel
    {
        public List<TransactionMaster> TransactionMaster { get; set; }
        public TransactionMasterModel()
        {
            TransactionMaster = new List<TransactionMaster>();
        }   
    }
}