﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.TransactionMaster
{
    public class TransactionMaster
    {
        public string TRANSACTION_CD { get; set; }
        public string TRANSACTION_NAME { get; set; }
        public string TRANSACTION_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string SIGN { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
    }
}