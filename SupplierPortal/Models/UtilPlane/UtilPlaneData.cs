﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.UtilPlane
{
    public class UtilPlaneData
    {
       public string SupplierCodePlantCode { set; get; }
       public string SUPPLIER_CD { set; get; }
       public string SUPPLIER_NAME { set; get; }
       public string SUPPLIER_PLANT { set; get; }
       public string DOCK_CD { set; get; }
       public string DOCK_NM { set; get; }
       public string SHIPPING_DOCK{ set; get; }
       public string RCV_PLANT_CD { set; get; }
       public string PLANT_NM { set; get; }
       public string PAT_PL_FLAG { set; get; }
       public string CREATED_BY { set; get; }
       public DateTime? CREATED_DT { set; get; }
       public string CHANGED_BY { set; get; }
       public DateTime? CHANGED_DT { set; get; }
    }
}