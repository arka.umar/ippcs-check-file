﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.UtilPlane
{
    public class UtilPlaneModel
    {
        public List<UtilPlaneData> UtilPlaneDatas { set; get; }

        public UtilPlaneModel()
        {
            UtilPlaneDatas = new List<UtilPlaneData>();
        }
    }
}