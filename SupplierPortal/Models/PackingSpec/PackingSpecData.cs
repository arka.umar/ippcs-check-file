﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PackingSpec
{
    public class PackingSpecData
    {
        public Int32 PackingSpecId { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierPlant { set; get; }
        public string ShippingDock { set; get; }
        public string CompanyCode { set; get; }
        public string RcvPlantCode { set; get; }
        public string DockCode { set; get; }
        public string PartNo { set; get; }
        public string PalleteType { set; get; }
        public Int32 PalleteLength { set; get; }
        public Int32 PalleteWidth { set; get; }
        public Int32 PalleteHeight { set; get; }
        public double PalleteVolume { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime? ChangedDate { set; get; }
    }
}