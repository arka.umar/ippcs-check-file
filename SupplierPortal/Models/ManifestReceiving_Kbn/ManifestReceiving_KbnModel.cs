﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ManifestReceiving_Kbn
{
    public class ManifestReceiving_KbnModel
    {
        public List<ManifestReceiving_KbnDetail> ManifestReceiving_KbnDetails { set; get; }

        public List<ManifestReceiving_Kbn> ManifestReceivingKbns { set; get; }

        public ManifestReceiving_KbnModel()
        {
            ManifestReceiving_KbnDetails = new List<ManifestReceiving_KbnDetail>();
            ManifestReceivingKbns = new List<ManifestReceiving_Kbn>();
        }
    }
}