﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ManifestReceiving_Kbn
{
    public class ManifestReceiving_KbnDetail
    {
        public int No { set; get; }
        public string ITEM_NO { set; get; }
        public string PART_NO { set; get; }
        public string KANBAN_NO { set; get; }
        public string PART_NAME { set; get; }
        public string KANBAN_PRNT_ADDRESS { set; get; }
        public int ORDER_QTY { set; get; }
        public int ALREADY_RECEIVED_QTY { set; get; }
        public int RECEIVED_QTY { set; get; }
        public int RECEIVED_KBN { get; set; }
        public int RECEIVED_STATUS { set; get; }
        public int SHORTAGE_QTY { set; get; }
        public int SHORTAGE_KBN { get; set; }
        public int MISSPART_QTY { set; get; }
        public int MISSPART_KBN { get; set; }
        public int DAMAGE_QTY { set; get; }
        public int DAMAGE_KBN { get; set; }
        public int PCS_PER_KBN { get; set; }
        public int NO_OF_KBN { get; set; }
        public string Notice { set; get; }
        public int PROBLEM_FLAG { set; get; }
    }
}