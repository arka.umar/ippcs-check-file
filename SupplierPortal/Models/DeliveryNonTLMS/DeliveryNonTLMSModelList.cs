﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DeliveryNonTLMS
{
    public class DeliveryNonTLMSModelList
    {
        public DeliveryNonTLMSModelList()
        {
            Collection = new List<DeliveryNonTLMSModel>();
        }

        public List<DeliveryNonTLMSModel> Collection { set; get; }
    }
}