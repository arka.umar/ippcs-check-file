﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DeliveryNonTLMS
{
    public class DeliveryNonTLMSModel
    {
        public List<DeliveryNonTLMSGridContent> GridContent { get; set; }
        public List<DeliveryNonTLMSData> Apps { get; set; }
        public List<DeliveryNonTLMSTempGridContent> TempGridContent { get; set; }
        public List<DeliveryNonTLMSExeptionDate> ExeptionGridContent { get; set; }

        public DeliveryNonTLMSModel()
        {
            GridContent = new List<DeliveryNonTLMSGridContent>();
            Apps = new List<DeliveryNonTLMSData>();
            TempGridContent = new List<DeliveryNonTLMSTempGridContent>();
            ExeptionGridContent = new List<DeliveryNonTLMSExeptionDate>();
        }
    }

    public class DeliveryNonTLMSGridContent
    {
        public string Route { get; set; }
        public int TRIP_PER_DAY { get; set; }
        public int TRIP_NO { get; set; }
        public string LP_CD { get; set; }
        public DateTime PICKUP_DT { get; set; }
        public string LOGISTIC_POINT_CD { get; set; }
        public string LOGISTIC_PLANT_CD { get; set; }
        public string LOGISTIC_POINT_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string STATION { get; set; }
        public DateTime ARRIVAL_LOG_POINT_TIME { get; set; }
        public DateTime DEPARTURE_LOG_POINT_TIME { get; set; }
        public string ORDER_NO { get; set; }
        public string PUBLISH_FLAG { get; set; }
        public string PUBLISH_FLAG_NAME { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class DeliveryNonTLMSData
    {
        public string Route { get; set; }
        public int TRIP_PER_DAY { get; set; }
        public int TRIP_NO { get; set; }
        public string LP_CD { get; set; }
        public DateTime PICKUP_DT { get; set; }
        public string LOGISTIC_POINT_CD { get; set; }
        public string LOGISTIC_PLANT_CD { get; set; }
        public string LOGISTIC_POINT_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string STATION { get; set; }
        public DateTime ARRIVAL_LOG_POINT_TIME { get; set; }
        public DateTime DEPARTURE_LOG_POINT_TIME { get; set; }
        public string ORDER_NO { get; set; }
        public string PUBLISH_FLAG { get; set; }
        public string PUBLISH_FLAG_NAME { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class DeliveryNonTLMSTempGridContent
    {
        public string Route { get; set; }
        public int TripDay { get; set; }
        public int TripNo { get; set; }
        public string LPCode { get; set; }
        public DateTime PickUpDate { get; set; }
        public string LogPointCode { get; set; }
        public string LogPlantCode { get; set; }
        public string DockCode { get; set; }
        public string Station { get; set; }
        public string LogPointName { get; set; }
        public DateTime ArrivalLogisticPoint { get; set; }
        public DateTime DepartureLogPoint { get; set; }
        public string OrderNo { get; set; }
        public string IsPublish { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class DeliveryNonTLMSCheckExists
    {
        public int resultCount { get; set; }
    }

    public class DeliveryNonTLMSNormalisasi
    {
        public int Result { get; set; }
    }

    public class DeliveryNonTLMSExeptionDate
    {
        public DateTime EXEPTION_DATE { get; set; }
    }
}