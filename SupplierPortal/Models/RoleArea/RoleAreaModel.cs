﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RoleArea
{
    public class RoleAreaModel
    {
        public List<RoleAreaData> RoleAreaDatas { set; get; }

        public RoleAreaModel()
        {
            RoleAreaDatas = new List<RoleAreaData>();
        }

    }
}
