﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.RoleArea
{
    public class RoleAreaData : Controller
    {
        public bool Check { set; get; }
        public Int32 AreaID { set; get; }
        public string AreaName { set; get; }
        public string Data { set; get; }
        public int RoleObjectID { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDate { set; get; }

    }
}
