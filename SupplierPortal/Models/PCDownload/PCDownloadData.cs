﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PCDownload
{
    public class PCDownloadData
    {
        public string PCNo { get; set; }
        public string SupplierCode { get; set; }
        public DateTime PCDate { get; set; }
        public string Confirmation { get; set; }
        public string LockSts { get; set; }
        public string DownloadSts { get; set; }
        public string DownloadBy { get; set; }
        public DateTime ? DownloadDt { get; set; }
        public string SupplierName { get; set; }
        public string Notice { get; set; }
        public string PDFDownload { set; get; }
        public string CREATED_BY { get; set; }
    }
}