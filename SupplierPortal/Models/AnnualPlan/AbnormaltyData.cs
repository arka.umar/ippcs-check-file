﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AbnormaltyData
    {
        public string PartNo { get; set; }
        public string SupplierCode { get; set; }
        public string Source { get; set; }
        public string ErrorSuppCode { get; set; }
        public string ErrorMM { get; set; }
        public string ErrorPriceMst { get; set; }
        public string ChekPartNew { get; set; }
        public string ChekPartObsolete { get; set; }
        public string CheckPartEci { get; set; }
        public int QuantityPrev { get; set; }
        public int QuantityCurrent { get; set; }
        public int QuantityGap { get; set; }
        public string SourcePrev { get; set; }
        public string SourceCuurent { get; set; }
        public string DeleteFlag { get; set; }
        public string Notice { get; set; }

    }
}