﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanParameter
    {
        public String APYear { set; get; }
        public String Version { set; get; }
        public String SupplierCode { set; get; }
    }
}