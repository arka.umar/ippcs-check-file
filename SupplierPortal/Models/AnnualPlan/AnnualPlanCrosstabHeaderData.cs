﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanCrosstabHeaderData
    {
        public String SUPPLIER_CD { get; set; }
	    public String PRODUCTION_YEAR { get; set; }
	    public String VERSION { get; set; }
	    public String FEEDBACK_BY { get; set; }
	    public DateTime? FEEDBACK_DT { get; set; }
	    public Boolean? FEEDBACK_STATUS { get; set; }
	    public String NOTICE_ID { get; set; }

        public Int32 TOTAL_RECORD { get; set; }
        public String ANNUAL_PLAN_OF { get; set; }

	    public String CREATED_BY { get; set; }
	    public DateTime CREATED_DT { get; set; }
	    public String CHANGED_BY { get; set; }
	    public DateTime? CHANGED_DT { get; set; }
    }
}