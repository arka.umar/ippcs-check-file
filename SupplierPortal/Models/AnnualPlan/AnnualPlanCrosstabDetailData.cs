﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanCrosstabDetailData
    {
        public string AP_YEAR { get; set; }
        public string VERSION { get; set; }
        public string PART_NO { get; set; }
        public string COLOR_SUFFIX { get; set; }
        public string PART_NO_SFX { get; set; }
        public string CAR_FAMILY_CD { get; set; }
        public string KANBAN_SIZE { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PART_NAME { get; set; }
        public string PART_MATCH_KEY { get; set; }
        public string PLANT_CD { get; set; }
        public string CATEGORY_CD { get; set; }
        public string MONTH1 { get; set; }
        public string MONTH2 { get; set; }
        public string MONTH3 { get; set; }
        public string MONTH4 { get; set; }
        public string MONTH5 { get; set; }
        public string MONTH6 { get; set; }
        public string MONTH7 { get; set; }
        public string MONTH8 { get; set; }
        public string MONTH9 { get; set; }
        public string MONTH10 { get; set; }
        public string MONTH11 { get; set; }
        public string MONTH12 { get; set; }
        public string MONTH13 { get; set; }
        public string MONTH14 { get; set; }
        public string MONTH15 { get; set; }
        public string MONTH16 { get; set; }
        public string MONTH17 { get; set; }
        public string MONTH18 { get; set; }
        public string MONTH19 { get; set; }
        public string MONTH20 { get; set; }
        public string MONTH21 { get; set; }
        public string MONTH22 { get; set; }
        public string MONTH23 { get; set; }
        public string MONTH24 { get; set; }
        public string MONTH25 { get; set; }
        public string MONTH26 { get; set; }
        public string MONTH27 { get; set; }
        public string MONTH28 { get; set; }
        public string MONTH29 { get; set; }
        public string MONTH30 { get; set; }
        public string MONTH31 { get; set; }
        public string MONTH32 { get; set; }
        public string MONTH33 { get; set; }
        public string MONTH34 { get; set; }
        public string MONTH35 { get; set; }
        public string MONTH36 { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}