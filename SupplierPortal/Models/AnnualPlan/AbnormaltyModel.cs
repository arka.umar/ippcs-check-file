﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AbnormaltyModel
    {
        public List<AbnormaltyData> AbormaltyData { get; set; }

        public AbnormaltyModel()
        {
            AbormaltyData = new List<AbnormaltyData>();
        }
    }
}