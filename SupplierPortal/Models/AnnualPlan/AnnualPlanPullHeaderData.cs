﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanPullHeaderData
    {
        public String PLANNING_TYPE { get; set; }
	    public String AP_YEAR { get; set; }
	    public String VERSION { get; set; }
	    public Int32 TOTAL_RECORD { get; set; }
	    public String APPROVE_BY { get; set; }
	    public DateTime? APPROVE_DT { get; set; }
	    public String CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
    }
}