﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanModel
    {
        public List<AnnualPlanHeaderData> AnnualPlanHeaderList { set; get; }
        public List<AnnualPlanDetailData> AnnualPlanDetailList { set; get; }

        public AnnualPlanModel()
        {
            AnnualPlanHeaderList = new List<AnnualPlanHeaderData>();
            AnnualPlanDetailList = new List<AnnualPlanDetailData>();
        }
    }
}