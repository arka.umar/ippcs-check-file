﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanHeaderData
    {
        public string AP_YEAR { get; set; }
        public string VERSION { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public int TOTAL_RECORD { get; set; }
        public Boolean? SUPPLIER_CD_ERROR { get; set; }
        public Boolean? SUPPLIER_MASTER_ERROR { get; set; }
        public Boolean? NEW_PART { get; set; }
        public Boolean? OBSOLATE_PART { get; set; }
        public string FEEDBACK_BY { get; set; }
        public DateTime? FEEDBACK_DT { get; set; }
        public Boolean? LOCK_FLAG { get; set; }
        public string NOTICE_ID { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}