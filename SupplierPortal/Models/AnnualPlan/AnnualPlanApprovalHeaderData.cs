﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanApprovalHeaderData
    {
        public string PRODUCTION_YEAR { get; set; }
        public string VERSION { get; set; }
        public int TOTAL_RECORDS { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string APPROVE_BY { get; set; }
        public string APPROVE_DT { get; set; }
         }
    }
