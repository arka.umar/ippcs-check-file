﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanApprovalModel
    {
        public List<AnnualPlanApprovalHeaderData> AnnualPlanApprovalHeaderList { set; get; }
        public List<AnnualPlanApprovalDetailData> AnnualPlanApprovalDetailList { set; get; }

        public AnnualPlanApprovalModel()
        {
 
            AnnualPlanApprovalDetailList = new List<AnnualPlanApprovalDetailData>();
        }
    }
}