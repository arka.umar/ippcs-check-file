﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanAPPSModel
    {
        public List<AnnualPlanPullHeaderData> AnnualPlanPullHeaderList { set; get; }
        public List<AnnualPlanPullDetailData> AnnualPlanPullDetailList { set; get; }

        public List<AnnualPlanCrosstabHeaderData> AnnualPlanCrosstabHeaderList { set; get; }
        public List<AnnualPlanCrosstabDetailData> AnnualPlanCrosstabDetailList { set; get; }

        public AnnualPlanAPPSModel()
        {
            AnnualPlanPullDetailList = new List<AnnualPlanPullDetailData>();

            AnnualPlanCrosstabHeaderList = new List<AnnualPlanCrosstabHeaderData>();
            AnnualPlanCrosstabDetailList  = new List<AnnualPlanCrosstabDetailData>();
        }
    }
}