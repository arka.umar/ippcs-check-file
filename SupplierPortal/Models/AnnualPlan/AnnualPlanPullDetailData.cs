﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AnnualPlan
{
    public class AnnualPlanPullDetailData
    {
        public String AP_YEAR { get; set; } 
        public String CATEGORY_CD { get; set; } 
        public String VERSION_CD { get; set; } 
        public String EDITION { get; set; } 
        public String PLANT_CD { get; set; }
        public String CAR_FAMILY_CD { get; set; } 
        public String PART_NO { get; set; } 
        public String COLOR_SFX { get; set; } 
        public String AP_OF { get; set; } 
        public String PART_MATCHING_KEY { get; set; }
        public String SUPPLIER_CD { get; set; } 
        public String SUPPLIER_PLANT_CD { get; set; } 
        public String SUPPLIER_NAME { get; set; } 
        public String PART_NAME { get; set; } 
        public Int32 KANBAN_SIZE { get; set; }
        public String PROD_MONTH { get; set; } 
        public Int32 VOLUME { get; set; } 
        public Int32 FINAL_VOLUME { get; set; } 
        public String VENDOR_SHARE_GROUP_NO { get; set; } 
        public String VENDOR_SHARE_TYPE { get; set; }
        public String VENDOR_SHARE_VALUE { get; set; } 
        public String CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
    }
}