﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogMonitoring
{
    public class LogMonitoringDetailsModel
    {
        public List<LogMonitoringMaster> LogMonitoringDetails { get; set; }
        public LogMonitoringDetailsModel()
        {
            LogMonitoringDetails = new List<LogMonitoringMaster>();

        }
   
    }

}