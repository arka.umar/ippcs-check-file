﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogMonitoring
{
    public class LogMonitoringMaster
    {
        public string ProcessID { set; get; }
        public DateTime ProcessDate { set; get; }
        public DateTime ProcessDateTo { set; get; }
        public string FunctionID { set; get; }
        public string FunctionNM { set; get; }
        public string ProcessStatus { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public string SequenceNumber { set; get; }
        public string MessageType { set; get; }
        public string Message { set; get; }
        public string MessageSplit { set; get; }
        public string UserID { set; get; }
        public string MessageID { set; get; }
        public string Location { set; get; }
        public string createdby { get; set; }
        public DateTime createddate { get; set; }
    }


    public class StatusType
    {
        public string StatusTypeName { get; set; }
    }
}