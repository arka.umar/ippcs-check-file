﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogMonitoring
{
    public class StatusMaster
    {
        public string StatusCode { get; set; }
        public string StatusDesc { get; set; }
    }
}