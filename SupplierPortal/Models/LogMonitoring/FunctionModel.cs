﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogMonitoring
{
    public class FunctionModel
    {
        public List<functionMaster> FunctionLog { get; set; }
        public FunctionModel()
        {
            FunctionLog = new List<functionMaster>();
        }   
    }
}