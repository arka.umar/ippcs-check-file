﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LogMonitoring
{
    public class StatusModel
    {
        public List<StatusMaster> StatusLog { get; set; }
        public StatusModel()
        {
            StatusLog = new List<StatusMaster>();
        }   
    }
}