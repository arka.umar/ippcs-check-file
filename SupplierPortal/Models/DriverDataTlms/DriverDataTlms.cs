﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DriverDataTlms
{
    public class DriverDataTlms
    {
        //public int DRIVERDTID { get; set; }
        public DateTime APPLTERMFROM { get; set; }
        public DateTime APPLTERMTO { get; set; }
        public string RTEGRPCD { get; set; }
        public DateTime RTEDATE { get; set; }
        public string RUNSEQ { get; set; }
        public string LOGPTCD { get; set; }
        public string DOCKCD { get; set; }
        public string PLANTCD { get; set; }
        public string INBOUTBFLAG { get; set; }
        public string DRIVERACCSEQ { get; set; }
        public string LOGPARTNERCD { get; set; }
        public DateTime ARRVDATETIME { get; set; }
        public DateTime DPTDATETIME { get; set; }
        public int TRAVELDISTKM { get; set; }
        public string DOORCD { get; set; }
    }
}