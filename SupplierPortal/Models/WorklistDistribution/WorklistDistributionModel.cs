﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.WorklistDistribution
{
    public class WorklistDistributionModel
    {
        public List<WorklistDistribution> WorklistDistributionList { set; get; }

        public WorklistDistributionModel()
        {
            WorklistDistributionList = new List<WorklistDistribution>();
        }
    }
}