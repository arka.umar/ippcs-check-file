﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.WorklistDistribution
{
    public class WorklistDistribution
    {
        public string FolioCD { set; get; }
        public int ModuleCD { set; get; }
        public string Destination { set; get; }
        public string ModuleName { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangeBy { set; get; }
        public DateTime ChangeDate { set; get; }
    }
}