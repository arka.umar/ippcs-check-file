﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PurchaseReport
{
    public class PurchaseReportInvoiceOutstanding
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string CATEGORY { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string CURRENCY { get; set; }
        public double TOTAL_PURCHASE { get; set; }
    }
}