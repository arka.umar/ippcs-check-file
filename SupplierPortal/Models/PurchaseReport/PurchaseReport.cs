﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PurchaseReport
{
    public class PurchaseReport
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PERIOD_FROM { get; set; }
        public string PERIOD_TO { get; set; }
        public string CATEGORY { get; set; }
        public double GOOD_RECEIPT_ACTUAL { get; set; }
        public double INVOICING_ACTUAL { get; set; }
        public double INVOICING_OUTSTANDING { get; set; }
        public double INVOICING_TOTAL { get; set; }
        public double DIFFERENT { get; set; }
        public double PERCENT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string PO_NO { get; set; }
        public string POSTING_DATE { get; set; }
        public string LOCAL_CURR { get; set; }
        public double AMOUNT_DOC { get; set; }
        public double AMOUNT_LOCAL { get; set; }
        public string CURRENCY { get; set; }
        public string TOTAL_PURCHASE { get; set; }
        public string INVOICE_NO { get; set; }
        public string INVOICE_DATE { get; set; }
        public string INVOICE_NET_AMOUNT { get; set; }
        public string PPV_AMOUNT { get; set; }
        public string STATUS { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string PAYMENT_DOC_NO { get; set; }

    }
}