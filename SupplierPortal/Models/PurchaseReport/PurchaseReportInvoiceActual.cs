﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PurchaseReport
{
    public class PurchaseReportInvoiceActual
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string CATEGORY { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string INVOICE_NO { get; set; }
        public string INVOICE_DATE_STR { get; set; }
        public string CURRENCY { get; set; }
        public double INVOICE_NET_AMOUNT { get; set; }
        public double PPV_AMOUNT { get; set; }
        public double TOTAL_PURCHASE { get; set; }
        public string STATUS { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string PAYMENT_DOC_NO { get; set; }
        public DateTime INVOICE_DATE { get; set; }
    }
}