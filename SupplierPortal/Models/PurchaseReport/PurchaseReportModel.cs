﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PurchaseReport
{
    public class PurchaseReportModel
    {
        public List<PurchaseReport> purchaseReports { get; set; }
        public List<PurchaseReportGoodReceipt> purchaseReportGoodReceipts { get; set; }
        public List<PurchaseReportInvoiceActual> purchaseReportInvoiceActuals { get; set; }
        public List<PurchaseReportInvoiceOutstanding> purchaseReportInvoiceOutstandings { get; set; }

        public PurchaseReportModel() {
            purchaseReports = new List<PurchaseReport>();
            purchaseReportGoodReceipts = new List<PurchaseReportGoodReceipt>();
            purchaseReportInvoiceActuals = new List<PurchaseReportInvoiceActual>();
            purchaseReportInvoiceOutstandings = new List<PurchaseReportInvoiceOutstanding>();
        }
    }
}