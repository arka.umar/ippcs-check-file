﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PurchaseReport
{
    public class PurchaseReportDownload
    {
        public string NO { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string CATEGORY { get; set; }
        public double GOOD_RECEIPT_ACTUAL { get; set; }
        public double INVOICING_ACTUAL { get; set; }
        public double INVOICING_OUTSTANDING { get; set; }
        public double INVOICING_TOTAL { get; set; }
        public double DIFFERENT { get; set; }
        public double PERCENT { get; set; }
    }
}