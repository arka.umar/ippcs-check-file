﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PurchaseReport
{
    public class PurchaseReportGoodReceipt
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string CATEGORY { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string PO_NO { get; set; }
        public string POSTING_DATE_STR { get; set; }
        public string LOCAL_CURR { get; set; }
        public double AMOUNT_DOC { get; set; }
        public double AMOUNT_LOCAL { get; set; }
        public double SUM_AMOUNT_DOC { get; set; }
        public double SUM_AMOUNT_LOCAL { get; set; }
        public DateTime POSTING_DATE { get; set; }
    }
}