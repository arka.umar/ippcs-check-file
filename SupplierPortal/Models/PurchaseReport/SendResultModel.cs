﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PurchaseReport
{
    public class SendResultModel
    {
        public string Result { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string Desc { get; set; }
    }
}