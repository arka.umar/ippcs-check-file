﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RetroInquiry
{
    public class RetroInquiryDetailPopup
    {
        public string RetroDocNo { get; set; }
        public string MaterialNo { get; set; }
        public string PartColorSfx { get; set; }
        public float Quantity { get; set; }
        public double NewMaterialPrice { get; set; }
        public double OldMaterialPrice { get; set; }
        public string PcNo { get; set; }
        public DateTime? PcDate { get; set; }
    }
}