﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Portal.Models.RetroInquiry
{
    public class RetroInquiryModel
    {

        //public List<RetroInquiryDetail> RetroInquiryDetail = new List<RetroInquiryDetail>();
        public List<RetroInquiryDetail> RetroInquiryDetail { set; get; }
        public List<RetroInquiryDetailPopup> RetroInquiryDetailPopup { get; set; }

        public RetroInquiryModel()
        {
            RetroInquiryDetail = new List<RetroInquiryDetail>();
            RetroInquiryDetailPopup = new List<RetroInquiryDetailPopup>();
        }
    }
}