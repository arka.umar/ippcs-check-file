﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RetroInquiry
{
    public class RetroInquiryDetail
    {
        public string RETRO_DOC_NO { get; set; }
        public DateTime? PERIOD_FROM { get; set; }
        public DateTime? PERIOD_TO { get; set; }
        public string CURRENCY_CD { get; set; }
        public string RETRO_AMOUNT { get; set; }
        public string RETRO_RATE { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string REMAINING_INV_NO { get; set; }
        public DateTime? REMAINING_INV_DATE { get; set; }
        public string REMAINING_AMOUNT { get; set; }
        public string RETRO_REMAINING_AMOUNT { get; set; }
        public string RETRO_STATUS { get; set; }
        public int NOTICE { get; set; }
    }
}