﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Web.ASPxUploadControl;
using Portal.Controllers.Upload;

namespace Portal.Models
{
    public class UploadModel
    {
        public List<string> UploadCondition { set; get; }
        public static readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".pdf", ".xls", ".csv" },
            MaxFileSize = 20971520
        };

        public static int FileInputCount
        {
            get
            {
                UploadControlFilesStorage storage = HttpContext.Current.Session["Storage"] as UploadControlFilesStorage;
                if (storage != null)
                    return storage.FileInputCount;
                return 1;
            }
        }
    }


}