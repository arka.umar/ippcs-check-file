﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.Globals
{
    public  class LogisticPartner
    {
        public string LPCode { set; get; }
        public string LPName { set; get; }
    }

    public class SupplierReconciliation
    {
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
    }

    public class SupplierReconciliationPopup
    {
        public string SupplierCd { set; get; }
        public string SupplierNm { set; get; }
    }
        
    public class Supplier
    {
        public string SupplierCode { set; get; }
        public string SupplierPlantCD { set; get; }
        public string SupplierAbbreviation { set; get; }
        public string SupplierName { set; get; }
        public string SUPPLIER_CODE_NAME { set; get; }
        public string SupplierCodePlant { set; get; }

        public String SUPPLIER_CODE { get; set; }
        public String SUPPLIER_NAME { get; set; }
    }

    public class Reconcile
    {
        public string SupplierCode {get; set;}
        public string SupplierName { get; set; }
        public string VendorName { set; get; }
        public string DocumentNumber { set; get; }
        public string PostingDate { set; get; }
        public string DocumentType { set; get; }
        public string Curr { set; get; }
        public string SupplierAmount { set; get; }
        public string TMMINAmount { set; get; }
        public string Different { set; get; }
    }

    public class PCNo
    {
        public string PCNO { set; get; }
    }

    public class Manifest
    {
        public string ManifestNo { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
        public string DockCode { set; get; }
    }
    public class Dock
    {
        public string DockCode { set; get; }
        public string PlantCD { set; get; }
        public string DockName { set; get; }
        public string Email { get; set; }
        public string Emailcc { get; set; }

        public String DOCK_CODE { set; get; }
        public String PLANT_CD { set; get; }
        public String DOCK_NAME { set; get; }
        public String EMAIL_TO { get; set; }
        public String EMAIL_CC { get; set; }
    }
    public class DockPlant
    {
        public string DockCode { set; get; }
        public string PlantCD { set; get; }
        public string DockName { set; get; }
        public string PlantName { set; get; }
        public string DockCD { set; get; }
    }
    public class RcvArea
    {
        public string RCV_AREA { get; set; }
        public string RCV_PLANT { get; set; }
    }

    public class TMMINModel
    {
        public string MODEL_CD { get; set; }
    }


    class Plant
    {
        public string PlantCode { set; get; }
        public string PlantName { set; get; }
       
    }

    public class Part
    {
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public string KanbanNo { set; get; }
        public string BO_PART { set; get; }
    }

    class RouteRate
    {
        public string DeliveryNo { set; get; }
        public string LpCode { set; get; }
        public string PickupDate { set; get; }
        public string Routerate { set; get; }

    }

    public class ExistingDeliveryNoList
    {
        public string DeliveryNo { set; get; }
        public string LPCode { set; get; }
    }

    public class Currency
    {
        public string CURRENCY_CD { set; get; }
        public string CURRENCY_NAME { set; get; }
    }

    public class Union
    {
        public string UNION_CD { set; get; }
        public string UNION_NAME { set; get; }
    }

    public class LocationFunction
    {
        public string LOCATION_FUNCTION_CD { set; get; }
        public string LOCATION_FUNCTION_NAME { set; get; }
    }

    public class SupplierPosition
    {
        public string POSITION_CD { set; get; }
        public string POSITION_NAME { set; get; }
    }

    public class SupplierArea
    {
        public string AREA_CD { set; get; }
        public string AREA_NAME { set; get; }
    }

    public class SupplierICS
    {
        public string SUPP_CD { set; get; }
        public string SUPP_NAME { set; get; }
        public string PAYMENT_METHOD_CD { set; get; }
        public string PAYMENT_TERM_CD { set; get; }
        public string suppLookUp { set; get; }
    }

    public class MaterialICS
    {
        public string MAT_NO { get; set; }
        public string MAT_DESC { get; set; }
    }

    public class CountryArea
    {
        public string COUNTRY_CD { set; get; }
        public string COUNTRY_NAME { set; get; }
    }
    public class CustomerCategory
    {
        public string CATEGORY_CD { set; get; }
        public string CATEGORY_NAME { set; get; }
    }
    public class InvoiceTaxType
    {
        public string INVOICE_TAX_TYPE_CD { set; get; }
        public string INVOICE_TAX_TYPE_NAME { set; get; }
    }
    public class Tier
    {
        public string TIER_CD { set; get; }
        public string TIER_NAME { set; get; }
    }
    public class SupplyType
    {
        public string SUPPLY_TYPE_CD { set; get; }
        public string SUPPLY_TYPE_NAME { set; get; }
    }
    public class Automotive
    {
        public string AUTO_CUSTOMER_CD { set; get; }
        public string AUTO_CUSTOMER_NAME { set; get; }
    }
    public class TermOfPayment
    {
        public string PAY_TERM_CD { set; get; }
        public string PAY_TERM_NAME { set; get; }
    }
    public class PaymentMethod
    {
        public string PAY_METHOD_CD { set; get; }
        public string PAY_METHOD_NAME { set; get; }
    }
    public class InvoiceStatus
    {
        public string STATUS_CD { set; get; }
        public string STATUS_NAME { set; get; }
    }
    public class PartnerBank
    {
        public string SUPP_BANK_TYPE { set; get; }
        public string SUPP_BANK_KEY { set; get; }
        public string SUPP_ACCOUNT { set; get; }
        public string SUPP_BANK_COUNTRY { set; get; }
    }

    public class WithHoldingTax
    {
        public string WITHHOLDING_TAX_CD { set; get; }
        public string WITHHOLDING_TAX_NAME { set; get; }
        public string WITHHOLDING_TAX_DESC { set; get; }
        public double RATE { set; get; }
    }
    public class Tax
    {
        public string TAX_CD { set; get; }
        public string TAX_NAME { set; get; }
        public double RATE { set; get; }
    }
    public class GLAccount
    {
        public string GL_ACCOUNT { set; get; }
        public string TRANSACTION_NAME { set; get; }
        public string COST_CENTER { set; get; }
        public string TAX_CD { set; get; }
    }

    public class Route
    {
        public string ROUTE { set; get; }
        public string ROUTE_NAME { set; get; }
    }

    public class Transaction
    {
        public string TRANSACTION_CD { set; get; }
        public string TRANSACTION_NAME { set; get; }
    }

    public class DockList
    {
        public string DOCK_CODE { set; get; }
        public string PLANT_CD { set; get; }
        public string DOCK_NAME { set; get; }
        public string EMAIL_TO { get; set; }
        public string EMAIL_CC { get; set; }
    }

    public class DockListError
    {
        public string DOCK_CODE { set; get; }
        public string PLANT_CD { set; get; }
        public string DOCK_NAME { set; get; }
        public string EMAIL_TO { get; set; }
        public string EMAIL_CC { get; set; }
        public string MESSAGE { get; set; }
    }

    //add riani (20221020)
    public class SysMaster
    {
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }
    }
}