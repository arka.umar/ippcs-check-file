﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Globals
{
    public class ComboData
    {
        public string Val { get; set; } 
        public string Text { get; set; }
    }
}