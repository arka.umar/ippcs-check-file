﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ChatDownload
{
    public class ChatData
    {
        public int ChatId { set; get; }
        public string ProcessType { set; get; }
        public string UserId { set; get; }
        public string CommentTo { set; get; }
        public string CommentCc { set; get; }
        public string Participant { set; get; }
        public string CommentContent { set; get; }
        public string CommentDt { set; get; }
        public string FileUploadFlag { set; get; }
        public string FileUploadName { set; get; }
        public string FileUploadPath { set; get; }
        public string DeletionFlag { set; get; }
        public string ChangedBy { set; get; }
        public DateTime StartDt { set; get; }
        public DateTime EndDt { set; get; }
    }
}