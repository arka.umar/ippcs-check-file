﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ChatDownload
{
    public class ChatDownloadModel
    {
        public List<ChatData> ChatDatas { set; get; }

        public ChatDownloadModel()
        {
            ChatDatas = new List<ChatData>();
        }

    }
}