﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ConsolidatedLPOP
{
    public class MasterCompletenessResult
    {
        public string RecordID	{get; set;}				
        public string    PONumber {get; set;}										
         public string   POItemNo	{get; set;}							
         public string   DockCode	{get; set;}								
         public string   SupplierCode	{get; set;}									
         public string   MaterialNo		{get; set;}						
         public string   ProdPurposeCD	{get; set;}								
          public string  SourceType		{get; set;}						
        public string    PartColorSFX		{get; set;}						
         public string   MaterialDescription	{get; set;}		
        public string    ProdMonth {get; set;}	
         public string   Qty_n			{get; set;}		
         public string   Qty_n1			{get; set;}			
         public string   Qty_n2			{get; set;}			
          public string  Qty_n3				{get; set;}		
          public string  PlantCode		{get; set;}						
         public string   SlocCode			{get; set;}				 
         public string   PaymentMethodCode	{get; set;}														
          public string  PaymentTermCode	{get; set;}												
         public string   MatPrice			{get; set;}				
        public string    ValStats	{get; set;}	
        public string    Message		{get; set;}	
    }
}

																			
																																																																																									
				
