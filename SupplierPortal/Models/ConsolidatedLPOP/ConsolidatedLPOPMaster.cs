﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ConsolidatedLPOP
{
    public class ConsolidatedLPOPMaster
    {
      
        public string ID { get; set; }
        public string Production_Year { get; set; }
        public string Production_Month { get; set; }
        public string LPOP_Type { get; set; }
        public string PO_Number { get; set; }
        public DateTime Created_Time { get; set; }
        
    }
}