﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ConsolidatedLPOP
{
    public class ConsolidatedLPOPModel
    {
       
        public List<ConsolidatedLPOPMaster> ConsolidatedLPOPs { get; set; }
        
        public List<ConsolidatedTNQCDetail> ConsolidatedTNQCDetails { set; get; }
       
        public List<MasterCompletenessResult> MasterCompletenessResults { get; set; }

        
        public ConsolidatedLPOPModel()
        {
            
            ConsolidatedLPOPs = new List<ConsolidatedLPOPMaster>();
            ConsolidatedTNQCDetails = new List<ConsolidatedTNQCDetail>();
            
            MasterCompletenessResults = new List<MasterCompletenessResult>();
        }
    }
}