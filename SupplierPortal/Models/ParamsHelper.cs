﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection; 
using System.Collections; 
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.IO;
using System.Text.RegularExpressions; 
using System.Web.Mvc;
namespace Portal.Models
{
    public static class ParamsHelper
    {
        public static T  AsObject<T>(this NameValueCollection nv)  
        { 
            PropertyInfo[] properties = typeof(T).GetProperties();
            Type otype = typeof(T);
            object o = Activator.CreateInstance(otype);
            var k = nv.Keys;
            List<string> li = new List<string>();
            foreach (var ki in k)
                li.Add(ki.ToString());

            foreach (PropertyInfo property in properties)
            {
                if (li.Contains(property.Name))
                {
                    object v = nv[property.Name];
                    property.SetValue(o, v, null);
                }
                else
                {
                    Type pt = property.GetType();
                    object po = null;
                    if(pt.IsValueType)
                    {
                        po = Activator.CreateInstance(pt);
                    }
                    property.SetValue(o, po, null);
                }
            }
            return ((T)o);
        }

        public static DateTime ToDateTime(this string datum, string fmt = "dd.MM.yyyy")
        {
            DateTime d = new DateTime(1900, 1, 1);
            if (!DateTime.TryParseExact(datum, fmt
                , CultureInfo.InvariantCulture
                , DateTimeStyles.None
                , out d))
                d = new DateTime(1899, 12, 31);
            return d;
        }

        public static string DateReFormat(this string datum, string fromFormat, string toFormat)
        {
            DateTime d = new DateTime(1900, 1, 1);
            if (!DateTime.TryParseExact(datum, fromFormat
                    , CultureInfo.InvariantCulture
                , DateTimeStyles.None, out d))
            {
                return null;
            }
            return d.ToString(toFormat, CultureInfo.InvariantCulture);
        }

        public static string ToSQLDate(this string datum)
        {
            return DateReFormat(datum, "dd.MM.yyyy", "yyyy-MM-dd");
        }
        public static bool MonthValid(this string monum)
        {
            DateTime d;
            return (!string.IsNullOrEmpty(monum) 
                && DateTime.TryParseExact("01." +monum, "dd.MM.yyyy"
                    , CultureInfo.InvariantCulture
                    , DateTimeStyles.None, out d));           
        }


        public static bool DateValid(this string datum, string fmt)
        {
            DateTime d;
            return (!string.IsNullOrEmpty(datum)
                  && DateTime.TryParseExact(datum, fmt
                    , CultureInfo.InvariantCulture
                    , DateTimeStyles.None, out d));           
        }

        public static string SanitizeFilename(this string insane)
        {
            Regex regEx = new Regex("[^A-z0-9\\-_\\.]+");
            Regex rX = new Regex("[`\\s\\^\\]\\[]+");
            return Regex.Replace(regEx.Replace(rX.Replace(insane, "_"), "_"), @"[\s|_]+", "_");
        }

        public static string AsJson(this object o)
        {
            return (o == null) ? null : (new System.Web.Script.Serialization.JavaScriptSerializer()).Serialize(o);
        }

        /// <summary>
        /// Encodes a string to be represented as a string literal. The format
        /// is essentially a JSON string.
        /// 
        /// The string returned includes outer quotes 
        /// Example Output: "Hello \"Rick\"!\r\nRock on"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EncodeJs(string s)
        {
            StringBuilder sb = new StringBuilder();
            
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\'':
                        sb.Append("\\\'");
                        break;
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }           

            return sb.ToString();
        }

        public static string Lxiv(long l)
        {
            const byte BASE = 64;
            const string A = "0123456789etaoinshrdlucmfwypvbgkjqxzETAOINSHRDLUCMFWYPVBGKJQXZ-_#";
            StringBuilder x = new StringBuilder("");
            byte n;
            do
            {
                n = (byte)(l % BASE);
                if (n < BASE)
                    x.Insert(0, A.Substring(n, 1));
                l = (l - n) / BASE;
            }
            while (l > 0);
            return x.ToString();
        }

        public static string Unique(string n)
        {
            string fx = "";
            int i = 0;
            do
            {
                fx = Path.Combine(Path.GetDirectoryName(n),
                        Path.GetFileNameWithoutExtension(n)
                        + Lxiv((int)Math.Floor(DateTime.Now.TimeOfDay.TotalMilliseconds) + (i++))
                        + Path.GetExtension(n));
            } while (File.Exists(fx));
            return (fx);
        }

        public static string ReMoveFile(string n)
        {
            string newname = n;
            if (!File.Exists(n)) return n;

            try
            {
                newname = Unique(n);
                File.Move(n, newname);
            }
            catch (Exception ex)
            {
                newname = null;
            }
            return newname;
        }


    }
}