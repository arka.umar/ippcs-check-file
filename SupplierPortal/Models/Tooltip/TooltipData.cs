﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.Tooltip
{
    public class TooltipData : Controller
    {
        public bool Check { set; get; }
        public Int32 TooltipID { set; get; }
        public string Description { set; get; }
        public string UserName { set; get; }
        public string Message { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDate { set; get; }
    }
}