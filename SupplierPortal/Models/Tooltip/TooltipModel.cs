﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Tooltip
{
    public class TooltipModel
    {
        public List<TooltipData> TooltipDatas { set; get; }
        
        public TooltipModel()
        {
            TooltipDatas = new List<TooltipData>();
        }
    }
}