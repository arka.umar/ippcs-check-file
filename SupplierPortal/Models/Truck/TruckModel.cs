﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Truck
{
    public class TruckModel
    {
        public List<TruckData> Trucks { set; get; }
        public List<ManifestPartData> ManifestParts { set; get; }

        public TruckModel()
        {
            Trucks = new List<TruckData>();
            ManifestParts = new List<ManifestPartData>();
        }
    }
}