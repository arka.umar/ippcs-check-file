﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Truck
{
    public class TruckData
    {
        public string DELIVERY_NO { set; get; }
        public DateTime PICKUP_DT { set; get; }
        public string ROUTERATE { set; get; }
        public string LP_CD { set; get; }
        public string LOG_PARTNER_NAME { set; get; }
        public string DRIVER_NAME { set; get; }
        public string DOCK_CD { set; get; }
        public string STATION { set; get; }
        public string ARRIVAL_PLAN_DT { set; get; }
        public string ARRIVAL_ACTUAL_DT { set; get; }
        public string ARRIVAL_STATUS { set; get; }
        //public TimeSpan ARRIVAL_GAP { set; get; }
        public string ARRIVAL_GAP { set; get; }

        public string DEPARTURE_PLAN_DT { set; get; }
        public string DEPARTURE_ACTUAL_DT { set; get; }
        public string DEPARTURE_STATUS { set; get; }
        //public TimeSpan DEPARTURE_GAP { set; get; }
        public string DEPARTURE_GAP { set; get; }
        public string NOTICE { set; get; }
    }
}
