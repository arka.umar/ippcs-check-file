﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Truck
{
    public class ManifestPartData
    {
        public string MANIFEST_NO { set; get; }
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string PART_NO { set; get; }
        public string PARTS_NAME { set; get; }        
    }
}
