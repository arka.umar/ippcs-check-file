﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DetailForecast
{
    public class DetailForecast
    {
        public string Part_No {get;set;}
        public string Part_Name {get;set;}
        public string RCV_Area {get;set;}
        public string Kanban_Size {get;set;}
        public string SFX {get;set;}
        public string N_QTY {get;set;}
        public string N_Plus_One_QTY {get;set;}
        public string N_Plus_Two_QTY {get;set;}
        public string N_Plus_Three_QTY {get;set;}
        public string Remark {get;set;}
        public string Message {get;set;}
    }
}