﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DetailForecast
{
    public class DetailForecastModel
    {
        public List<DetailForecast> Details { get; set; }
        public DetailForecastModel()
        {
            Details = new List<DetailForecast>();
        }
    }
}