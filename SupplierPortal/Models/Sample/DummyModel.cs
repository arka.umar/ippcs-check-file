﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Sample
{
    public class DummyModel : SelectableDataModel
    {
        public string Name { set; get; }
        public byte[] AttachmentPreview { set; get; }
        public override string SelectedKey
        {
            get
            {
                return "Name";
            }
        }
        public override string SelectedKeyValue
        {
            get
            {
                return Name;
            }
        }
    }
}