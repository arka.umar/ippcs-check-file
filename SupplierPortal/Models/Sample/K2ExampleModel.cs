﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.K2Connector;

namespace Portal.Models.Sample
{
    public class K2ExampleModel
    {
        public List<WorkflowModel> Worklists { get; set; }

        public List<WorkflowModel> Worklists2 { get; set; }

        public K2ExampleModel()
        {
            Worklists = new List<WorkflowModel>();
            Worklists2 = new List<WorkflowModel>();
        }
    }
}