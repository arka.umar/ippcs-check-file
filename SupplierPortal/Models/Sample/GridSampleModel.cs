﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Configuration;

namespace Portal.Models.Sample
{
    public class GridSampleModel
    {
        public List<ConfigurationItem> Configurations { set; get; }
        public GridSampleModel()
        {
            Configurations = new List<ConfigurationItem>();
        }
    }
}