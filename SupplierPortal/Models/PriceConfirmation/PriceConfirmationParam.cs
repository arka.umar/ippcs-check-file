﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Portal.Models.PriceConfirmation
{
    public class PriceConfirmationParam
    {
        public string PCNo { get; set; }
        public string PCDateFrom { get; set; }
        public string PCDateTo { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string ReleaseStatus { get; set; }
        public string EffectiveDate { get; set; }
        public string BusinessArea { get; set; }
        public string Username { get; set; }
        public string Noreg { get; set; } 

        public void Validate(int detail=0)
        {
            if (!string.IsNullOrEmpty(PCNo) && PCNo.Length > 21)
                throw new Exception("PC No max length is 21 chars");
            const string df = "dd.MM.yyyy";
            if (!string.IsNullOrEmpty(PCDateFrom) && !PCDateFrom.DateValid(df)) 
                throw new Exception(string.Format("Invalid date format for {0} The format is DD.MM.YYYY", "PC Date From")) ; 
            if  (!string.IsNullOrEmpty(PCDateTo) && !PCDateTo.DateValid(df))
                throw new Exception(string.Format("Invalid date format for {0} The format is DD.MM.YYYY", "PC Date To")) ;
            if (!string.IsNullOrEmpty(EffectiveDate) && !EffectiveDate.MonthValid())
                throw new Exception(string.Format("Invalid date format for {0} The format is MM.YYYY", "Effective Date"));
            if (!string.IsNullOrEmpty(SupplierCode) && (SupplierCode.Length > 6 && detail==0))
                throw new Exception("Supplier Code max length is 6 chars");
        }
    }
}