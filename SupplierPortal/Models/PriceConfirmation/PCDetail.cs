﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmation
{
    public class PCDetail
    {
        public string PCNo { get; set; }
        public int ItemNo { get; set; }        
        public string MaterialNo { get; set; }
        public string MaterialName  { get; set; }
        public string SourceType { get; set; }
        public string ProductionPurpose { get; set; }
        public string PackingType { get; set; }
        public string PartColorSuffix { get; set; }
        public decimal? CurrentPrice { get; set; }
        public decimal? NewPrice { get; set; }
    }
}