﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmation
{
    public class PriceConfirmationModel
    {
        public string Mode { get; set; }
        public PriceConfirmation Detail { get; set; }
        public List<PriceConfirmation> Rows { get; set; }

        public List<PCDetail> Material { get; set; }
        public List<PCDetail> Draft { get; set; }
        public string Message { get; set; }

        public PriceConfirmationModel()
        {
            Detail = new PriceConfirmation();
            Rows = new List<PriceConfirmation>();
            
            Material = new List<PCDetail>();
            Draft = new List<PCDetail>(); 
        }
    }
}