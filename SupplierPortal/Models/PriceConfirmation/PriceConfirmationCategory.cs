﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmation
{
    public class PriceConfirmationCategory
    {
        public string FunctionId { get; set; }
        public string SystemCode { get; set; }
        public string SystemValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedDate { get; set; }
    }
}