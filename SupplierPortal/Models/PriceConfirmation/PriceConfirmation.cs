﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmation
{
    public class PriceConfirmation
    {
        public string PCNo { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string BusinessArea { get; set; } 
        public DateTime EffectiveDate { get; set; }
        public DateTime PCDate { get; set; }
        public string PrintFlag { get; set; }
        public string CoverLetterBody { get; set; }
        public DateTime? RetroDate { get; set; }
        public string ReleaseStatus { get; set; }
        public string Category { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public DateTime? ChangedDate { get; set; }

        public string SupplierAttention { get; set; }
        public string SupplierAddress { get; set; }
        public string SubmitSts { get; set; }
        public string PartCount { get; set; }
    }
}