﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PriceConfirmation
{
    public class PCDetailHead
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierAttention { get; set; }
        public string SupplierAddress { get; set; }
        public string PCNo { get; set; }
        public DateTime? PCDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
    }
}