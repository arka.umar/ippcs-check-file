﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierMapping
{
    public class SupplierMappingData
    {
        public string PlantCode { set; get; }
        public string DockCode { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierName	{ set; get; }
        public DateTime ValidFrom { set; get; }
        public DateTime ValidTo { set; get; }
        public int ExcludeFlag	{ set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDate { set; get; }

    }
}