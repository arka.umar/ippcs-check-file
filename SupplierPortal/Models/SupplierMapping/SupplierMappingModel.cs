﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierMapping
{
    public class SupplierMappingModel
    {
        public List<SupplierMappingData> SupplierMappings { set; get; }

        public SupplierMappingModel()
        {
            SupplierMappings = new List<SupplierMappingData>();
        }
    }
}