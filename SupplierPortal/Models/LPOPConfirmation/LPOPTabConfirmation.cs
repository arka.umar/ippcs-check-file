﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConfirmation
{
    public class LPOPTabConfirmation
    {
        public string ID { get; set; }
        public string LPOP_Type { get; set; }
        public string Download { get; set; }
        public string Abnormality { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDate { get; set; }
        public DateTime FeedbackDueDate { get; set; }
        public DateTime LastRemindingDate { get; set; }
        public string RevisedBy { get; set; }
        public DateTime RevisedDate { get; set; }
        public string POBy { get; set; }
        public DateTime PODate { get; set; }
        public string CompletenessBy { get; set; }
        public DateTime CompletenessDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsPO { get; set; }
    }
}