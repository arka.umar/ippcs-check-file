﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConfirmation
{
    public class LPOPConfirmationAbnormalityDetail
    {
        public string PACK_MONTH { get; set; }
        public string D_ID { get; set; }
        public string VERS { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string N_VOLUME { get; set; }
        public string N_1_VOLUME { get; set; }
        public string N_2_VOLUME { get; set; }
        public string N_3_VOLUME { get; set; }

        public string PCD_MATERIAL_MASTER { get; set; }
        public string PCD_ROUTING { get; set; }
        public string PCD_PART_LIST { get; set; }
        public string PCD_OTHERS { get; set; }

        public string PAD_DOCK_CODE { get; set; }
        public string PAD_MATDOCK { get; set; }
        public string PAD_LOCATION_CODE { get; set; }
        public string PAD_OTHERS { get; set; }

        public string PUD_MATERIAL_PRICE { get; set; }
        public string PUD_STANDARD_PRICE { get; set; }
        public string PUD_PO { get; set; }
        public string PUD_SOURCE_LIST { get; set; }
        public string PUD_SUPPLIER_MASTER { get; set; }
        public string PUD_OTHERS { get; set; }

        public string PVD_PACKING_TYPE { get; set; }
        public string PVD_OTHERS { get; set; }

        public string ISTD_SYSTEM { get; set; }
        public string ISTD_OTHERS { get; set; }
    }
}