﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.Models.LPOPConfirmation
{
    public class LPOPConfirmationReportUnion
    {
        public string ACTION { set; get; }
        public string D_ID { set; get; }
        public string VERS { set; get; }
        public string REV_NO { set; get; }
        public string COMP_CD { set; get; }
        public string R_PLANT_CD { set; get; }
        public string DOCK_CD { set; get; }
        public string S_PLANT_CD { set; get; }
        public string SHIP_CD { set; get; }
        public string ORD_TYPE { set; get; }
        public string PACK_MONTH { set; get; }
        public string CAR_CD { set; get; }
        public string REC_CD { set; get; }
        public string SRC { set; get; }
        public string PART_NO { set; get; }
        public string ORDER_TYPE { set; get; }
        public string ORDER_LOT_SZ { set; get; }
        public string KANBAN_NUMBER { set; get; }
        public string N_VOLUME { set; get; }
        public string AICO_CEPT { set; get; }
        public string N_1_VOLUME { set; get; }
        public string AICO_CEPT1 { set; get; }
        public string N_2_VOLUME { set; get; }
        public string AICO_CEPT2 { set; get; }
        public string N_3_VOLUME { set; get; }
        public string AICO_CEPT3 { set; get; }
        public string N_D01 { set; get; }
        public string N_D02 { set; get; }
        public string N_D03 { set; get; }
        public string N_D04 { set; get; }
        public string N_D05 { set; get; }
        public string N_D06 { set; get; }
        public string N_D07 { set; get; }
        public string N_D08 { set; get; }
        public string N_D09 { set; get; }
        public string N_D10 { set; get; }
        public string N_D11 { set; get; }
        public string N_D12 { set; get; }
        public string N_D13 { set; get; }
        public string N_D14 { set; get; }
        public string N_D15 { set; get; }
        public string N_D16 { set; get; }
        public string N_D17 { set; get; }
        public string N_D18 { set; get; }
        public string N_D19 { set; get; }
        public string N_D20 { set; get; }
        public string N_D21 { set; get; }
        public string N_D22 { set; get; }
        public string N_D23 { set; get; }
        public string N_D24 { set; get; }
        public string N_D25 { set; get; }
        public string N_D26 { set; get; }
        public string N_D27 { set; get; }
        public string N_D28 { set; get; }
        public string N_D29 { set; get; }
        public string N_D30 { set; get; }
        public string N_D31 { set; get; }
        public string N_1_D01 { set; get; }
        public string N_1_D02 { set; get; }
        public string N_1_D03 { set; get; }
        public string N_1_D04 { set; get; }
        public string N_1_D05 { set; get; }
        public string N_1_D06 { set; get; }
        public string N_1_D07 { set; get; }
        public string N_1_D08 { set; get; }
        public string N_1_D09 { set; get; }
        public string N_1_D10 { set; get; }
        public string N_1_D11 { set; get; }
        public string N_1_D12 { set; get; }
        public string N_1_D13 { set; get; }
        public string N_1_D14 { set; get; }
        public string N_1_D15 { set; get; }
        public string N_1_D16 { set; get; }
        public string N_1_D17 { set; get; }
        public string N_1_D18 { set; get; }
        public string N_1_D19 { set; get; }
        public string N_1_D20 { set; get; }
        public string N_1_D21 { set; get; }
        public string N_1_D22 { set; get; }
        public string N_1_D23 { set; get; }
        public string N_1_D24 { set; get; }
        public string N_1_D25 { set; get; }
        public string N_1_D26 { set; get; }
        public string N_1_D27 { set; get; }
        public string N_1_D28 { set; get; }
        public string N_1_D29 { set; get; }
        public string N_1_D30 { set; get; }
        public string N_1_D31 { set; get; }
        public DateTime SYSTEM_DATE { set; get; }
        public string SERIES { set; get; }
        public string LIFE_CYCLE { set; get; }
        public string B_A_SIGN { set; get; }
        public string B_A_QUANTITY { set; get; }
        public string R_U_PREVIOUS { set; get; }
        public string R_U_PRESENT { set; get; }
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_NAME { set; get; }
        public string PART_NAME { set; get; }
        public string DUMMY { set; get; }
        public string TERM { set; get; }
        public string CREATED_BY { set; get; }
        public DateTime CREATED_DT { set; get; }
        public string CHANGED_BY { set; get; }
        public DateTime CHANGED_DT { set; get; }
    }
}
