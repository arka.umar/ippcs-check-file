﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.LPOPConfirmation
{
    public class LPOPConfirmationModel
    {

        public List<LPOPTabConfirmation> LPOPConsolidationConfirmations { get; set; }
        public List<LPOPConfirmationDetail> LPOPConfirmationDetails { set; get; }
        public LPOPConfirmationModel()
        {
            LPOPConsolidationConfirmations = new List<LPOPTabConfirmation>();
            LPOPConfirmationDetails = new List<LPOPConfirmationDetail>();
        }
    }
}