﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DataExch
{
    public class InboxOutboxPage
    {
        public InboxOutboxPage()
        {
            InboxOutboxWithPage = new List<InboxOutboxMsg>();
        }

        public List<InboxOutboxMsg> InboxOutboxWithPage { get; set; }
    }
}