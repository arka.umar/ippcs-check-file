﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DataExch
{
    public class UserDataExch
    {
        public String Username { set; get; }
        public String RegistrationNumber { set; get; }
        public String FirstName { set; get; }
        public String LastName { set; get; }
        public int SentMaxFileSize { set; get; }
        public String Email { set; get; }
        public string Division { set; get; }
        public string Departement { set; get; }
        public String PositionID { set; get; }
        public String SectionID { set; get; }
        public String DivisionID { set; get; }
        public String DepartementID { set; get; }
    }
}