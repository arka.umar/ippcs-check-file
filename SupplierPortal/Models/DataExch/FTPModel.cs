﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DataExch
{
    public class FTPModel
    {
        public string FtpIpAddress { get; set; }
        public string FtpPassword { get; set; }
        public string FtpUsername { get; set; }
    }
}