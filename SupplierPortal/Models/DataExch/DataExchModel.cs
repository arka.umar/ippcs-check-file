﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections;
using Portal.Models.DataExch;

namespace Portal.Models
{
    public class DataExchModel
    {
        public int MessageID { get; set; }
        public int No { get; set; }
        [Required]
        public String MessageTitle { get; set; }
        public String MessageBody { get; set; }
        public String Filename { get; set; }
        public String MaxRetDate { get; set; }
        public String MessageTo { get; set; }
        public String FilePassword { get; set; }
        public int MessageType { get; set; }
        public String MessageFrom { get; set; }
        public DateTime MessageDate { get; set; }
        public String DownloadUsername { get; set; }

        public String savedMaxRetDate { get; set; }
        public bool ExpiredFlag { get; set; }
        public bool NotifyDownloadFlag { get; set; }
        
        public String ErrorTitle { get; set; }
        public String ErrorBody { get; set; }
        public String ErrorFilename { get; set; }
        public String ErrorRetDate { get; set; }
        public String ErrorSendTo { get; set; }
        public String ErrorSendMail { get; set; }
        
        public String UserName { get; set; }
        public String lookupName { get; set; }
        public List<UserDataExch> LookUpUserList { get; set; }        
    }
}