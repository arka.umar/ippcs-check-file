﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DataExch
{
    public class InboxOutboxMsg
    {
        public InboxOutboxMsg()
        {
            InboxOutboxMsgModel = new List<DataExchModel>();
        }

        public List<DataExchModel> InboxOutboxMsgModel { get; set; }
    }
}