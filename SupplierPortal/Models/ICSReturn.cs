﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models
{
    public class ICSResult<T>
    {
        public string status { get; set; }
        public T value { get; set; }
    }
}