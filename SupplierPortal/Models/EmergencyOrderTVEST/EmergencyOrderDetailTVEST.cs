﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.EmergencyOrderTVEST
{
    public class EmergencyOrderDetailTVEST          
    {
        public string SUPPLIER_CD { set; get; }
        public string SUPPLIER_PLANT { set; get; }
        public string SUPPLIER_PLANT_CD { set; get; }
        public string DOCK_CD { set; get; }
        public string PART_NO { set; get; }
        public string PART_NAME { set; get; }
        public string KANBAN_NO { set; get; }
        public int QTY_PER_CONTAINER { set; get; }
        public int ORDER_QTY { set; get; }
        public int PIECES_QTY { set; get; }
        public string KANBAN_PRINT_ADDRESS { set; get; }
        public string IMPORTIR_INFO { set; get; }
        public string IMPORTIR_INFO2 { set; get; }
        public string IMPORTIR_INFO3 { set; get; }
        public string IMPORTIR_INFO4 { set; get; }
        public string PART_BARCODE { set; get; }
        public string PROGRESS_LANE_NO { set; get; }
        public string CONVEYANCE_NO { set; get; }
        public DateTime? PROD_DATE { get; set; }
    }
}