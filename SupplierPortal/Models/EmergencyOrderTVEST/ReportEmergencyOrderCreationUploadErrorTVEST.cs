﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.EmergencyOrderTVEST
{
    public class ReportEmergencyOrderCreationUploadErrorTVEST
    {
        public String SUPPLIER_CODE { get; set; }
        public String SUPPLIER_PLANT { get; set; }
        public String DOCK_CODE { get; set; }
        public String UNIQUE_NO { get; set; }
        public String PART_NO { get; set; }
        public String PROD_DATE { get; set; }
        public Int32 ORDER_LOT_SZ { get; set; }

        public String ERRORS_MESSAGE_CONCAT { set; get; }
    }
}