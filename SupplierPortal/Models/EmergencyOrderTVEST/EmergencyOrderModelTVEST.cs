﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.EmergencyOrderTVEST
{
    public class EmergencyOrderModelTVEST
    {
        public EmergencyOrderModelTVEST()
        {
            EmergencyOrders = new List<EmergencyOrderDetailTVEST>();
        }
        public List<EmergencyOrderDetailTVEST> EmergencyOrders { set; get; }
    }
}