﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models;
using Toyota.Common.Web.Database;
using System.Reflection;
using System.Diagnostics;

namespace Portal.Models
{
    public static class Dbutil
    {
        public static IDBContext Context() {
            return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        }

        public static string GetValue(string sql, object[] args = null)
        {
            if (string.IsNullOrEmpty(sql)) return null;
            string datakey = "q(" + sql + ((args != null) ? ("," + args.AsJson().Replace(",", ";")) : "") + ")";
            string v= Memori.Get<string>(datakey) ;
            if (v==null) 
            {
                try
                {
                    IDBContext db = Context();
                    v = db.SingleOrDefault<string>(sql, args);
                    db.Close();
                    Memori.Set(datakey, v);
                }
                catch (Exception ex)
                {
                    StackTrace st = new StackTrace(1, true);
                    StackFrame[] f = st.GetFrames();
                    string cname = null;
                    if (f.Length > 1)
                    {
                        cname = f[1].GetMethod().ReflectedType.FullName;
                    }
                    ex.PutLog("", cname?? "DbUtil.GetValue", datakey, 0, "MPCS0002ERR", "ERR", "0", "00000", 2);
                }
            }
            return v;
        }

        public static List<T> GetList<T>(string sql, object[] args=null)
        {
            if (string.IsNullOrEmpty(sql)) return null;
            string typename = (typeof(T)).Name;
            string datakey = "List<" + typename + ">" + sql;
            if (args != null)
            {
                datakey = datakey+ "(" + args.AsJson().Replace(",", ";") + ")";
            }
            List<T> t = Memori.Get<List<T>>(datakey);
            if (t == null)
            {
                try
                {
                    IDBContext db = Context();
                    t = db.Fetch<T>(sql, args);
                    db.Close();
                    Memori.Set(datakey, t);
                }
                catch (Exception ex)
                {
                    StackTrace st = new StackTrace(1, true);
                    StackFrame[] f = st.GetFrames();
                    string cname = null;
                    if (f.Length > 1)
                    {
                        cname = f[1].GetMethod().ReflectedType.FullName;
                    }
                    ex.PutLog("", cname??"DbUtil.GetList", datakey, 0, "MPCS0002ERR", "ERR", "0", "00000", 2);
                }
            }
            return t;
        }

    }
}