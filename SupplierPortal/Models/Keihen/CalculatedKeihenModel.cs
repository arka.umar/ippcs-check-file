﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Keihen
{
    public class CalculatedKeihenModel
    {
        public string PACK_MONTH { get; set; }
        public string APPROVED { get; set; }
        public string APPROVED_BY { get; set; }
        public string APPROVED_DT { get; set; }
        public string FEEDBACK_REMINDER_START { get; set; }
        public string FEEDBACK_REMINDER_END { get; set; }
    }

    public class CalculatedKeihenData
    {
        public List<CalculatedKeihenModel> CModel { get; set; }
    }

    public class CalculatedKeihenUploadModel
    {
        public string ACTION { get; set; }
        public string PACK_MONTH { get; set; }
        public string PART_NO_12_DIGITS { get; set; }
        public string PART_NAME { get; set; }
        public string DOCK_CODE { get; set; }
        public string CFC { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANT_CODE { get; set; }
        public string ORIGINAL_VOLUME { get; set; }
        public string NEW_VOLUME { get; set; }
        public string FLUCTUATION { get; set; }
    }

    public class CalculatedKeihenUploadInvalidModel
    {
        public string ACTION { get; set; }
        public string PACK_MONTH { get; set; }
        public string PART_NO_12_DIGITS { get; set; }
        public string PART_NAME { get; set; }
        public string DOCK_CODE { get; set; }
        public string CFC { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANT_CODE { get; set; }
        public string ORIGINAL_VOLUME { get; set; }
        public string NEW_VOLUME { get; set; }
        public string FLUCTUATION { get; set; }
        public string ERROR_MESSAGE { get; set; }
    }
}