﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Keihen
{
    public class KeihenModel
    {
        public string PACK_MONTH { get; set; }
        public string STATUS_CD { get; set; }
        public string APPROVED_BY { get; set; }
        public string APPROVED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CAR_CD { get; set; }
    }

    public class KeihenUploadModel
    {
        public string Production_Month { get; set; }
        public string Part_Number_12_Digit { get; set; }
        public string Dock_Code { get; set; }
        public string CFC { get; set; }
        public string Supplier_Code { get; set; }
        public string Supplier_Plant_Code { get; set; }
        public string Quantity_Sign { get; set; }
        public string Quantity { get; set; }
        public string Received_By { get; set; }
        public string Received_Date { get; set; }
        //public string Status { get; set; }
        //public string Approved_By { get; set; }
        //public string Approved_Date { get; set; }
    }

    public class KeihenDataInvalidModel
    {
        public string Production_Month { get; set; }
        public string Part_Number_12_Digit { get; set; }
        public string Dock_Code { get; set; }
        public string CFC { get; set; }
        public string Supplier_Code { get; set; }
        public string Supplier_Plant_Code { get; set; }
        public string Quantity_Sign { get; set; }
        public string Quantity { get; set; }
        public string error_message { get; set; }
    }

    public class KeihenData
    {
        public List<KeihenModel> KModel { get; set; }
    }
}