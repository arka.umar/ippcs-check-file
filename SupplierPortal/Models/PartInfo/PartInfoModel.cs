﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PartInfo
{
    public class PartInfoModel
    {
        public List<PartInfoData> PartInfoDatas { set; get; }

        public PartInfoModel()
        {
            PartInfoDatas = new List<PartInfoData>();
        }
    }
}