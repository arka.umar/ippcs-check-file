﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PartInfo
{
    public class PartInfoData
    {
       public string PartNo { set; get; }
       public string PART_NO { set; get; }
       public string SupplierCodePlantCode { set; get; }
       public string SUPPLIER_CD { set; get; }

       public string SUPPLIER_NAME { set; get; }
       public string SUPPLIER_PLANT { set; get; }
       public string DOCK_CD { set; get; }
       public string KANBAN_NO { set; get; }
       public string PART_NAME { set; get; }
       public string PCK_TYPE { set; get; }
       public string KANBAN_QTY { set; get; }
       public string SAFETY_PCS { set; get; }
       public string PRMCFC { set; get; }
       public string PRMPCT { set; get; }
       public string PRMMS { set; get; }
       public string PRMXS { set; get; }
       public string PRMCD { set; get; }
       public string LINE_LEAD_TIME { set; get; }
       public string CREATED_BY { set; get; }
       public DateTime? CREATED_DT { set; get; }
       public string CHANGED_BY { set; get; }
       public DateTime? CHANGED_DT { set; get; }
       public DateTime? START_DT { set; get; }
       public DateTime? END_DT { set; get; }
    }
}