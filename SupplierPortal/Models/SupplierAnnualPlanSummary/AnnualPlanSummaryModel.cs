﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierAnnualPlanSummary
{
    public class AnnualPlanSummaryModel
    {
        public List<AnnualPlanHeaderData> AnnualPlanHeaderList { set; get; }
        public List<AnnualPlanDetailData> AnnualPlanDetailList { set; get; }

        public List<AnnualPlanCrosstabHeaderData> AnnualPlanCrosstabHeaderList { set; get; }
        public List<AnnualPlanCrosstabDetailData> AnnualPlanCrosstabDetailList { set; get; }

        public AnnualPlanSummaryModel()
        {
            AnnualPlanHeaderList = new List<AnnualPlanHeaderData>();
            AnnualPlanDetailList = new List<AnnualPlanDetailData>();

            AnnualPlanCrosstabHeaderList = new List<AnnualPlanCrosstabHeaderData>();
            AnnualPlanCrosstabDetailList  = new List<AnnualPlanCrosstabDetailData>();
        }
    }
}