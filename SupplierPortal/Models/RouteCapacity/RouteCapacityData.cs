﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RouteCapacity
{
    public class RouteCapacityData
    {
        public Int32 RouteId { set; get; }
        public string RouteCode { set; get; }
        public string TruckType { set; get; }
        public double VolumeTarget { set; get; }
        public double EfficiencyTarget { set; get; }
        public DateTime TimeControlFrom { set; get; }
        public DateTime TimeControlTo { set; get; }


        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime? ChangedDate { set; get; }
    }
}