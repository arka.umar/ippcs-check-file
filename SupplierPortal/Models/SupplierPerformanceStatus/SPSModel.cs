﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformanceStatus
{
    public class SPSModel
    {
        public List<SPSRelease> ListSPSReleased { set; get; }

        public SPSModel()
        {

            ListSPSReleased = new List<SPSRelease>();
        }
    }

    public class SPSubCommonGraph
    {
        public double JAN { get; set; }
        public double FEB { get; set; }
        public double MAR { get; set; }
        public double APR { get; set; }
        public double MAY { get; set; }
        public double JUN { get; set; }
        public double JUL { get; set; }
        public double AGS { get; set; }
        public double SEP { get; set; }
        public double OCT { get; set; }
        public double NOV { get; set; }
        public double DEC { get; set; }
        public double TARGET_TMMIN { get; set; }
    }

    public class SPSubCommonGraphYAxis
    {
        public double YAxis { get; set; }
    }

    public class SPSubCommonPieChart
    {
        public string DATA { get; set; }
        public double PERCENTAGE { get; set; }
    }

    public class SPSubCommonPieColor
    {
        public string COLOR_TYPE { get; set; }
        public string COLOR { get; set; }
    }

    public class SPTMMINTarget
    {
        public string QUALITY { get; set; }
        public string SHORTAGE { get; set; }
        public string MISSPART { get; set; }
        public string ONTIME { get; set; }
        public string LINESTOP { get; set; }
        public string CRIPPLE { get; set; }
        public string DDD { get; set; }
        public string SPQUALITY { get; set; }
        public string WORKABILITY { get; set; }
        //add riani (20230209)
        public string QUALITY_NM { get; set; }
        public string SHORTAGE_NM { get; set; }
        public string MISSPART_NM { get; set; }
        //--------------------------//

    }
}