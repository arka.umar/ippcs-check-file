﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformanceStatus
{
    public class SPSRelease
    {
        public string ProductionMonth { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        //public int Quality { get; set; }
        //public int Delivery { get; set; }
        //public int Safety { get; set; }
        //public int ServicePart { get; set; }
        //public int Warranty { get; set; }
        //public int PartReceive { get; set; }
        public int ReleaseFlag { get; set; }
        public int EditFlag { get; set; }
        public int CompleteFlag { get; set; }


   


    }
}