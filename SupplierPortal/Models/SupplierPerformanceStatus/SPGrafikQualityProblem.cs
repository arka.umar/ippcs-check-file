﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SupplierPerformanceStatus
{
    public class SPGrafikQualityProblem
    {
        private string supplierCode;

        public string SUPPLIER_CD
        {
            get { return supplierCode; }
            set { supplierCode = value; }
        }

        private string yyyy;

        public string YYYY
        {
            get { return yyyy; }
            set { yyyy = value; }
        }

        private double jan;

        public double JAN
        {
            get { return jan; }
            set { jan = value; }
        }
        private double feb;

        public double FEB
        {
            get { return feb; }
            set { feb = value; }
        }
        private double mar;

        public double MAR
        {
            get { return mar; }
            set { mar = value; }
        }
        private double apr;

        public double APR
        {
            get { return apr; }
            set { apr = value; }
        }
        private double may;

        public double MAY
        {
            get { return may; }
            set { may = value; }
        }
        private double jun;

        public double JUN
        {
            get { return jun; }
            set { jun = value; }
        }
        private double jul;

        public double JUL
        {
            get { return jul; }
            set { jul = value; }
        }
        private double ags;

        public double AGS
        {
            get { return ags; }
            set { ags = value; }
        }
        private double sep;

        public double SEP
        {
            get { return sep; }
            set { sep = value; }
        }
        private double oct;

        public double OCT
        {
            get { return oct; }
            set { oct = value; }
        }
        private double nov;

        public double NOV
        {
            get { return nov; }
            set { nov = value; }
        }
        private double dec;

        public double DEC
        {
            get { return dec; }
            set { dec = value; }
        }

        private string targetTMMIN;

        public string TARGET_TMMIN
        {
            get { return targetTMMIN; }
            set { targetTMMIN = value; }
        }
    }
}