﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.KeihenFeedback
{
    public class KeihenFeedbackModel
    {
        //public List<SupplierName> suppliers { get; set; }
        //public List<SupplierPlant> suppliersPlant { get; set; }
        public List<keihenHeaderData> keihenData { get; set; }
        public List<detailKeihen> detailKeihen { get; set; }

        public KeihenFeedbackModel()
        {
            keihenData = new List<keihenHeaderData>();
            detailKeihen = new List<detailKeihen>();
        }
    }


    public class DataDetailFeedbackModel
    {
        public List<detailKeihenTemporary> detailTemporary { get; set; }
        public string overallMode {get;set;}

        public DataDetailFeedbackModel()
        {
            //string overall;

            detailTemporary = new List<detailKeihenTemporary>();
            overallMode = "";

            //public string overallMode {get;set;}
            //overallMode = new string();

        }
    }
    

    public class SupplierName
    {
        public string SUPPLIER_CODE { set; get; }
        public string SUPPLIER_PLANT_CD { set; get; }
        //public string SUPPLIER_ABBREVIATION { set; get; }
        public string SUPPLIER_NAME { set; get; }
        //public string SUPPLIER_CODE_NAME { set; get; }
        //public string SUPPLIER_CODE_PLANT { set; get; }
    }

    public class SupplierPlant
    {
        public string SUPPLIER_PLANT_CD { set; get; }
        public string SUPPLIER_PLANT_NAME { set; get; }
    }

    public class KeihenResume 
    {
        public String SS_PLANT_CD { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String PACK_MONTH { get; set; }
        public String CFC { get; set; } //car code
        public String KEIHENVOLUME { get; set; }
        public String CONFIRMATION { get; set; }
        public String LOCK { get; set; }
        public String FEEDBACK_BY { get; set; }
        public DateTime FEEDBACK_DT { get; set; }
        public String DOWNLOADED_BY { get; set; }
    }

    public class keihenHeaderData
    {
        public String PACK_MONTH { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String S_PLANT_CD { get; set; }
        public String SS_PLANT_CD { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String FEEDBACK_STATUS { get; set; }
        public Boolean? LOCK { get; set; }
        public String DOWNLOADED_BY { get; set; }
        public DateTime? DOWNLOADED_DT { get; set; }
        public String FEEDBACK_BY { get; set; }
        public DateTime? FEEDBACK_DT { get; set; }
    }

    public class keihenRevision
    {
        public String PACK_MONTH { get; set; }
        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String S_PLANT_CD { get; set; }
        public String DOCK_CD { get; set; }
        public String N_1_ORIGINAL { get; set; }
        public String N_1_NEW { get; set; }
        public String FLUCTUATION { get; set; }
        public String REPLY { get; set; }
        public String PLANT_CAP { get; set; }
        public String RAW_COMP { get; set; }
        public String RAW_MAT { get; set; }
        public String QTY_SUPP_REFF { get; set; }

    }


    public class detailKeihen
    {
        public String DOCK_CD { get; set; }
        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public String SUPPLIER_CD { get; set; }
        
        public String N_1_ORIGINAL { get; set; }
        public String N_1_NEW { get; set; }
        public String FLUCTUATION { get; set; }
        public String REPLY { get; set; }
        public String PLANT_CAP { get; set; }
        public String RAW_COMP { get; set; }
        public String RAW_MAT { get; set; }
        public String QTY_SUPP_REFF { get; set; }
        public String PACK_MONTH { get; set; }
        public String LOCK { get; set; }
    }


    public class downloaddetailKeihen
    {
        public String PACK_MONTH { get; set; }
        public String DOCK_CD { get; set; }
        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String S_PLANT_CD { get; set; }
        public String ORIGINAL { get; set; }
        public String NEW { get; set; }
        public String FLUCTUATION { get; set; }
        public String REPLY { get; set; }
        public int PLANT_CAP { get; set; }
        public int RAW_COMP { get; set; }
        public int RAW_MAT { get; set; }
        public String QTY_SUPP_REFF { get; set; }
        
    }

    public class downloaddetailKeihenError
    {
        public String PACK_MONTH { get; set; }
        public String DOCK_CD { get; set; }
        public String PART_NO { get; set; }
        public String PART_NAME { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String S_PLANT_CD { get; set; }
        public String ORIGINAL { get; set; }
        public String NEW { get; set; }
        public String FLUCTUATION { get; set; }
        public String REPLY { get; set; }
        public String PLANT_CAP { get; set; }
        public String RAW_COMP { get; set; }
        public String RAW_MAT { get; set; }
        public String QTY_SUPP_REFF { get; set; }
        public String ERROR_DESCRIPTION { get; set; }
    }


    public class detailKeihenTemporary
    {
        public String PACK_MONTH { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT { get; set; }
        public String DOCK_CD { get; set; }
        public String PART_NO { get; set; }
        public String REPLY { get; set; }
        public String PLANT_CAP { get; set; }
        public String RAW_COMP { get; set; }
        public String RAW_MAT { get; set; }
        public String QTY_SUPP_REFF { get; set; }
        public String OVERALLMODE { get; set; }
        public Int64 PROCESS_ID { get; set; }
        public String FEEDBACK_BY { get; set; }
        //public String PART_NAME { get; set; }
        //public String N_1_ORIGINAL { get; set; }
       //public String N_1_NEW { get; set; }
        //public String FLUCTUATION { get; set; }
        //public String LOCK { get; set; }
    }



}