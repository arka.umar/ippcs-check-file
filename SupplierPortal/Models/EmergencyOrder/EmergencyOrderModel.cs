﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.EmergencyOrder
{
    public class EmergencyOrderModel
    {
        public EmergencyOrderModel()
        {
            EmergencyOrders = new List<EmergencyOrderDetail>();
        }
        public List<EmergencyOrderDetail> EmergencyOrders { set; get; }
    }
}