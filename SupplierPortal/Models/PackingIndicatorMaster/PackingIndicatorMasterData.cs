﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Portal.Models.PackingIndicatorMaster
{
    public class PackingIndicatorMasterData 
    {
        //TB_M_PACKING_INDICATOR
        public string MAT_NO { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string PROD_PURPOSE { get; set; }
        public string PROD_PURPOSE_CD { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public string MAT_DESC { get; set; }
        public string DOCK_CD { get; set; }
        public string PACKING_TYPE_NAME { get; set; }
        public string PACKING_TYPE_NAMEE { get; set; }
        public string VALID_DT_FR { get; set; }
        public string VALID_DT_TO { get; set; }
        //public DateTime? VALID_DT_FR { get; set; }
        //public DateTime? VALID_DT_TO { get; set; }

        //TB_M_PACKING_TYPE
        public string PACKING_TYPE_CD { get; set; }

        //base Model
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
        
        public string ERR_MSG { get; set; }
        public string Result { get; set; }

        public string KEYF { get; set; }
        public int? SEQ { get; set; }
    }
}