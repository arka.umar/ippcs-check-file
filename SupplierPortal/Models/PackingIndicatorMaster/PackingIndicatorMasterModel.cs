﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.PackingIndicatorMaster
{
    public class PackingIndicatorMasterModel
    {
        public List<PackingIndicatorMasterData> PackingIndicatorMasterDatas { get; set; }

        public PackingIndicatorMasterModel()
        {
            PackingIndicatorMasterDatas = new List<PackingIndicatorMasterData>();
        }
    }
}