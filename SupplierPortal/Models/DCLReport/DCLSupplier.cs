﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLReport
{
    public class DCLSupplier
    {
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
 
    }
}