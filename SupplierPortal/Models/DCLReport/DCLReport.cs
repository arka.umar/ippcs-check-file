﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Database;

namespace Portal.Models.DCLReport
{
    public class DCLReport
    {
        public string DeliverNo { set; get; }
        public string PickUpDate { set; get; }
        public string RouteRate { set; get; }
        public string DriverName { set; get; }
        public string DriverManifest { set; get; }
        public string DownloadBy { set; get; }
        public DateTime DownloadTime { set; get; }
        public string LogisticPartner { set; get; }
        public string ApprovalStatus { set; get; }
        public string DeliveryStatus { set; get; }
        public string ArrivalAt { set; get; }
        public string LPConfirmation { set; get; }

        public string Notice { set; get; }
        public string DeliveryReason { set; get; }
        public string ReviseBy { set; get; }
        public DateTime ReviseDate { set; get; }
        public string Approver { set; get; }
        public DateTime ApprovalDate { set; get; }
        public int RevisionNo { set; get; }

        public string Status { set; get; }
        public string DeliveryBy { set; get; }
        public DateTime DeliveryDate { set; get; }
        public string InvoiceBy { set; get; }
        public DateTime InvoiceDate { set; get; }
        public string Requisitioner { set; get; }
        public DateTime RequisitionDate { set; get; }
        public string LPName { set; get; }
    }

    public class SyncCheck
    {
        public string Job_Name { set; get; }
        public string Job_Status { set; get; }
        public string Job_Message { set; get; }
        public string Outcome { set; get; }//added 2015-03-19
        public string Last_Step_Message { set; get; } //added 2015-05-08
    }

    //public class CheckResult
    //{
    //    public int result { get; set; }
    //}

    public class DeliveryType
    {
        public string DeliveryTypeName { get; set; }
    }

    public class OkamochiHeader
    {
        public string No { get; set; }
        public string Route { get; set; }
        public string AllDock { get; set; }
        public string SDock { get; set; }
        public string Terminal { get; set; }
        public string Arr { get; set; }
        public string ArrFri { get; set; }
        public string DepFri { get; set; }
        public string Dep { get; set; }
        public string Logp { get; set; }
        public string Cycle { get; set; }
        public string R { get; set; }
        public string Trip { get; set; }
    }

    public class OkamochiDetail
    {
        public string SupplierCD { get; set; }
        public string SupplierAbbr { get;set; }
        public string Dock { get; set; }
        public string Ord { get; set; }
    }

    public class TruckSheet
    {
        public string PDock { get; set; }
        public string Route { get; set; }
        public string Del_No { get; set; }
        public string Door_CD { get; set; }
        public string Physical_Station { get; set; }
        public string Arrvdatetime { get; set; }
        public string Dptdatetime { get; set; }
        public string ArrvFri { get; set; }
        public string DeptFri { get; set; }
        public string LogpartnerCD { get; set; }
        public string Supplier { get; set; }
        public string RouteCode { get; set; }
        public string Supp { get; set; }
        public string SuppCD { get; set; }
    }

    public class Plane
    {
        public string P_LANE { get; set; }
        public string RCVDOCK { get; set; }
        public string SUPPID { get; set; }
        public string SUP_ABBR { get; set; }
        public string PLANE_IN { get; set; }
    }


    public class EmptyJobCard
    {
        /*public string ID_ROUTE { get; set; }
        public string ID_SUPPLIER { get; set; }
        public string ROUTE_CD { get; set; }
        public string ROUTE_TRIP { get; set; }
        public string CSUPLLOGPTCD { get; set; }
        public string CSUPLPLNTCD { get; set; }
        public string CSUPLDCKCD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string LNUMPLT { get; set; }*/
        public string CONCTOCRTE { get; set; }
        public string CONCTOCRTETRP { get; set; }
        public string CONCTODRTEDATE2 { get; set; }
        public string RTEGRPCD { get; set; }
        public string RUNSEQ { get; set; }
        public string RTEDATE2 { get; set; }
        public string SUPPCD { get; set; }
        public string SUPPPLANTCD { get; set; }
        public string SUPPDOCKCD { get; set; }
        public string NUMPALLETE { get; set; }
        public string ORDDATE { get; set; }
        public string ORDSEQ { get; set; }
        public string RCVCOMPDOCKCD { get; set; }
        public string DOORCD { get; set; }

        public string SROUTE { get; set; }
        public string SSTATION { get; set; }
        public string SDOCK { get; set; }
        public string SSUPPLIER { get; set; }
        public string SABBR { get; set; }
        public string SNUM { get; set; }
    }


    public class DeliveryControlList
    {
        public string DeliveryNo { get; set; }
        public string PickupDate { get; set; }
        public string RouteCode { get; set; }
        public string Rate { get; set; }
        public string LPCode { get; set; }
        public string LPName { get; set; }
        public string DELIVERY_NO { get; set; }
        public string LogisticPoint { get; set; }
        public string SupplierName { get; set; }
        public string ArrivalPlan { get; set; }
        public string DeparturePlan { get; set; }
        public string OrderNo { get; set; }
        public string DockCode { get; set; }
        //public string JUMLAH { get; set; }
    }

    /*public class ChangeOverList
    {
        public string CHANGING_POINT { get; set; }
        public string RTEDATE { get; set; } 
        public string SUPPCD { get; set; }
        public string SUPPPLANTCD { get; set; }
        public string curr_RCVCOMPDOCKCD { get; set; }
        public string curr_RTEGRPCD { get; set; }
        public string curr_RUNSEQ { get; set; }
        public string curr_ORDDATE { get; set; }
        public string curr_ORDSEQ { get; set; }
        public string curr_ENDOFORDER { get; set; }
        public string curr_UNLOADSTRTIME { get; set; }
        public string curr_UNLOADENDTIME { get; set; }
        public string curr_LOGPARTNERCD { get; set; }
        public string next_RCVCOMPDOCKCD { get; set; }
        public string next_RTEGRPCD { get; set; }
        public string next_RUNSEQ { get; set; }
        public string next_ORDDATE { get; set; }
        public string next_ORDSEQ { get; set; } 
        public string next_ENDOFORDER { get; set; }
        public string next_UNLOADSTRTIME { get; set; }
        public string next_UNLOADENDTIME { get; set; }
        public string next_LOGPARTNERCD { get; set; }

    }*/


    public class ChangeOverList
    {
        public string CHANGING_POINT { get; set; }
        public string RTEDATE { get; set; }
        public string SUPPCD { get; set; }
        public string SUPPPLANTCD { get; set; }
        public string RCVCOMPDOCKCD { get; set; }
        public string RTEGRPCD { get; set; }
        public string RUNSEQ { get; set; }
        public string ORDDATE { get; set; }
        public int ORDSEQ { get; set; }
        public string ENDOFORDER { get; set; }
        public DateTime UNLOADSTRTIME { get; set; }
        public DateTime UNLOADENDTIME { get; set; }
        public string LOGPARTNERCD { get; set; }
        public string STATUS { get; set; }
    }

    /*
    public class runSeqPerRoute {
        public string RUNSEQ { get; set; }
    }

    public class listSupp
    {
        public string SUPPCD { get; set; }
    }
    */
    /*public class distinctChangeOver
    {
        public string SUPPCD { get; set; }
        public string SUPPPLANTCD { get; set; }
        public string RCVCOMPDOCKCD { get; set; } 
    }*/

    public class compareDock {
        public string keys { get; set; }
        public string RCVCOMPDOCKCD { get; set; }


    }

}