﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DCLReport
{
    public class DCLReportModel
    {
        public List<DCLReport> DCLInquiryRegionals { set; get; }
        public List<DCLReportDb> DCLInquiryAdditionals { set; get; }
        public List<DCLReportDb> DCLInquiryDbModel { set; get; }
        public List<DCLSupplier> DCLSupplier { set; get; }
        public List<DCLPickUp> DCLPickUp { set; get; }
        public List<DCLRouteDestination> DCLRouteDestinations { set; get; }
        public List<SyncCheck> SyncCheck { set; get; }

        public DCLReportModel()
        {
            DCLInquiryRegionals = new List<DCLReport>();
            DCLInquiryAdditionals = new List<DCLReportDb>();
            DCLInquiryDbModel = new List<DCLReportDb>();
            DCLSupplier = new List<DCLSupplier>();
            DCLPickUp = new List<DCLPickUp>();
            DCLRouteDestinations = new List<DCLRouteDestination>();
            SyncCheck = new List<SyncCheck>();
        }
    }
}