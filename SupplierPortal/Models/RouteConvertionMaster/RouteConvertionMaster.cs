﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RouteConvertionMaster
{
    public class RouteConvertionMaster
    {
        public string ROUTE_CD_CONV { get; set; }
        public string ROUTE_CD { get; set; }
        public string ROUTE_CONV_NAME { get; set; }
        public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
        public string LP_CD { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public string CHANGED_BY { get; set; }
    }

    public class CheckResult
    {
        public int result { get; set; }
    }

}