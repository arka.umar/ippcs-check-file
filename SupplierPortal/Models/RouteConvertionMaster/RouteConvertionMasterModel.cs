﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.RouteConvertionMaster
{
    public class RouteConvertionMasterModel
    {
        public List<RouteConvertionMaster> RouteConvertionMaster { get; set; }
        public RouteConvertionMasterModel()
        {
            RouteConvertionMaster = new List<RouteConvertionMaster>();
        }   
    }

}