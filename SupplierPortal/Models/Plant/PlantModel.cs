﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Plant
{
    public class PlantModel
    {
        public List<PlantData> PlantsData { set; get; }
        public List<PlantData> PlantsDBModel { set; get; }

        public PlantModel()
        {
            PlantsData = new List<PlantData>();
            PlantsDBModel = new List<PlantData>();
        }
    }
}