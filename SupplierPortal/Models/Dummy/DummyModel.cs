﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models.DCLInquiry;

namespace Portal.Models.Dummy
{
    public class DummyModel
    {
        public List<DCLInquiryDb> Inquiries { set; get; }
    }
}