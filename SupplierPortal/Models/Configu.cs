﻿using System;
using System.Configuration;
using System.Reflection; 

namespace Portal.Models
{
    public class Configu
    {
        public string DB { get; set; }
        public int Debug { get; set; }
        
        public Configu()
        {
            foreach(PropertyInfo prop in this.GetType().GetProperties())
            {
                string pt = prop.PropertyType.Name;
                string pn = prop.Name;
                string v = ConfigurationManager.AppSettings[pn];
                if (string.Compare("string", pt, true) == 0)
                    prop.SetValue(this, v, null);
                else if (pt.StartsWith("int", StringComparison.InvariantCultureIgnoreCase))
                    prop.SetValue(this, Convert.ToInt32(v), null);
            }   
        }

            
    }
}
