﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Messaging; 
using Toyota.Common.Web.Log; 

namespace Portal.Models
{
    public class MessageHolder
    {
        public List<Message> messages = null; 
        public MessageHolder()
        {
            IMessages mm = new DbMessageMaster();
            messages = mm.GetMessages();   
        }

        public string Text(string msgid, params object[] p)
        {
            Message msg = messages.Where(a => a.MessageID.Equals(msgid)).FirstOrDefault();
           
            if (msg == null) 
                return null;
            else 
                return string.Format(msg.MessageText, p);
        }

        public string Text(string msgid)
        {
            Message msg = messages.Where(a => a.MessageID.Equals(msgid)).FirstOrDefault();
            if (msg == null)
                return null;
            else
                return msg.MessageText;
        }

        public string Delimited(string msgid=null)
        {
            return string.Join("\r\n", messages
                .Where(a=> string.IsNullOrEmpty(msgid) || msgid.Equals(a.MessageID))
                .Select(a => a.MessageID + "\t" + a.MessageText));
        }

        
       
    }
}