﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.CapacityPlanning
{
    public class CapacityPlanning
    {
        public int ID { get; set; }
        public string DeliveryNo { set; get; }
        public string RouteId { set; get; }
        public string RouteCode { set; get; }
        public string RouteSeq { set; get; }
        public DateTime PickupDate { set; get; }
        public string LpCode { set; get; }
        public string TruckType { set; get; }
        public double VolTarget { set; get; }
        public double EffTarget { set; get; }
        public double Utilization { set; get; }
        public double Percentage { set; get; }
        public string Cstatus { set; get; }

        public string status { set; get; }
        public double value { set; get; }
        public string name { set; get; }


    }
}