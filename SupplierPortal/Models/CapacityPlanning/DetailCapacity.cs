﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Portal.Models.CapacityPlanning
{
    public class DetailCapacity
    {
        //detail dipisah karna untuk urutan pada field-field download.
        // selain itu karna atribut kosong yang tidak terselect juga akan ikut ter-export di excel.
        //for detail planning

        //administrasi
        public int ID { get; set; }
        public string DeliveryNo { set; get; }
        public string ManifestNo { set; get; }
        public string CompanyCode { set; get; }
        public string RCVPlant { set; get; }
        public string DockCode { set; get; }
        public string SpCode { set; get; }
        public string SpPlantCode { set; get; }
        public string ShippingDock { set; get; }

        //pallete info
        public string PartNo { set; get; }
        public string PalleteType { set; get; }
        public Int32 PalleteLength { set; get; }
        public Int32 PalleteWidth { set; get; }
        public Int32 PalleteHeight { set; get; }
        public double PalleteVolume { set; get; }

        //route info
        public string RouteCode { set; get; }
        public double VolTarget { set; get; }
        public double EffTarget { set; get; }
        public string RouteSeq { set; get; }
        public DateTime PickupDate { set; get; }

        //transaksi kanban
        public double QtyLot { set; get; }
        public string KanbanNo { set; get; }
        public string SpName { set; get; }
        public string PartName { set; get; }
        public double OrderQty { set; get; }
        public double OrderM3 { set; get; }

    }
}