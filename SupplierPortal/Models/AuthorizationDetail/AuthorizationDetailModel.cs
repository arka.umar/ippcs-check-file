﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AuthorizationDetail
{
    public class AuthorizationDetailModel
    {
        public List<RoleLookup> RoleLookupList { get; set; }
        public List<ScreenLookup> ScreenLookupList { get; set; }
        public List<SystemLookup> SystemLookupList { get; set; }
        public List<ActionLookup> ActionLookupList { get; set; }
        public List<ObjectLookup> ObjectLookupList { get; set; }
        public List<AuthGroupLookup> AuthGroupLookup { get; set; }
        public List<AuthorizationDetailData> AuthDetailData { get; set; }
    }

    public class RoleLookup
    {
        public string DATAKEY { get; set; }
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
    }

    public class ScreenLookup
    {
        public string DATAKEY { get; set; }
        public string SCREEN_ID { get; set; }
        public string SCREEN_NAME { get; set; }
        public string SCREEN_AUTH { get; set; }
    }

    public class SystemLookup
    {
        public string DATAKEY { get; set; }
        public string SYSTEM_ID { get; set; }
        public string SYSTEM_NAME { get; set; }
    }

    public class ActionLookup
    {
        public string DATAKEY { get; set; }
        public string ACTION_ID { get; set; }
        public string ACTION_NAME { get; set; }
        public string ACTION_AUTH { get; set; }
    }

    public class ObjectLookup
    {
        public string DATAKEY { get; set; }
        public string OBJECT_ID { get; set; }
        public string OBJECT_NAME { get; set; }
        public string OBJECT_AUTH { get; set; }
    }

    public class AuthGroupLookup
    {
        public string DATAKEY { get; set; }
        public string AUTHORIZATION_GROUP_ID { get; set; }
        public string AUTHORIZATION_GROUP_NAME { get; set; }
    }

    public class AuthorizationDetailError
    {
        public string system_id { get; set; }
        public string role_id { get; set; }
        public string screen_id { get; set; }
        public string action_id { get; set; }
        public string object_id { get; set; }
        public string authorization_group_id { get; set; }
        public string screen_auth { get; set; }
        public string action_auth { get; set; }
        public string object_auth { get; set; }
        public string message { get; set; }
    }

    public class AuthorizationDetailData
    {
        public string system_id { get; set; }
        public string system_name { get; set; }
        public string role_id { get; set; }
        public string role_name { get; set; }
        public string screen_id { get; set; }
        public string screen_name { get; set; }
        public string screen_auth { get; set; }
        public string object_id { get; set; }
        public string object_name { get; set; }
        public string object_auth { get; set; }
        public string action_id { get; set; }
        public string action_name { get; set; }
        public string action_auth { get; set; }
        public string authorization_group_id { get; set; }
        public string authorization_group_name { get; set; }
        public string created_by { get; set; }
        public string created_dt { get; set; }
        public string changed_by { get; set; }
        public string changed_dt { get; set; }
        public string message { get; set; }
    }
}