﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.EmergencyDCLOrderCreation
{
    public class LogisticPartnerData
    {
        
        public string LPCode { get; set; }
        public string LPName { get; set; }
        
    }
}