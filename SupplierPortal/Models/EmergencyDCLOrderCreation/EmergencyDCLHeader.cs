﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.EmergencyDCLOrderCreation
{
    public class EmergencyDCLHeader
    {
        public string DELIVERY_NO { set; get; }
        public DateTime PICKUP_DT { set; get; }
        public string LP_CD { set; get; }
        public string Route { set; get; }
        public string Rate { set; get; }
        public string DELIVERY_REASON { set; get; }
    }
}