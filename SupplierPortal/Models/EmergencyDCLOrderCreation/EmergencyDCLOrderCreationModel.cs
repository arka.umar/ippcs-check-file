﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Portal.Models.Globals;

namespace Portal.Models.EmergencyDCLOrderCreation
{
    public class EmergencyDCLOrderCreationModel
    {
        public DataTable EmergencyData { set; get; }

        public List<EmergencyDCLOrderCreationData> Emergencys { get; set; }
        public List<EmergencyDCLOrderCreationDataGrid> EmergencysGrid { get; set; }
        public List<LogisticPartnerData> LogisticPartnerData { get; set; }
        public List<EmergencyGridContent> GridContent { get; set; }
        public EmergencyDCLHeader Header { get; set; }
        //public List<Portal.Models.Globals.Dock> DockGrid { set; get; }
        public List<Portal.Models.Globals.Supplier> GridSupplier { set; get; }
        public List<Portal.Models.Dock.DockData> ListDock { set; get; }

        public EmergencyDCLOrderCreationModel()
        {
            Emergencys = new List<EmergencyDCLOrderCreationData>();
            EmergencysGrid = new List<EmergencyDCLOrderCreationDataGrid>();
            LogisticPartnerData = new List<LogisticPartnerData>();
            GridContent = new List<EmergencyGridContent>();
            Header = new EmergencyDCLHeader();
            ListDock = new List<Portal.Models.Dock.DockData>();
            GridSupplier = new List<Portal.Models.Globals.Supplier>();
        }
    }
}