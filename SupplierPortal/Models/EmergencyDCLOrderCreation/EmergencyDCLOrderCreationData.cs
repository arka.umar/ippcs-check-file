﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models.EmergencyDCLOrderCreation
{
    public class EmergencyDCLOrderCreationData
    {
        public string PickUpDate { get; set; }
        //public string LPCode { get; set; }
        //public string LPName { get; set; }
        [Required(ErrorMessage="Route is required")]
        public string Route { get; set; }
        [Required(ErrorMessage = "Rate is required")]
        public string TripNo { get; set; }
        public string OrderNo { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string Dock { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalTime { get; set; }
        public string Reason { get; set; }
        public string DeliveryNo { get; set; }
    }
}