﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.EmergencyDCLOrderCreation
{
    public class EmergencyGridContent
    {
        public string ManifestNo { set; get; }
        public string DockCode { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierName { set; get; }
        public string Remark { set; get; }
    }
}