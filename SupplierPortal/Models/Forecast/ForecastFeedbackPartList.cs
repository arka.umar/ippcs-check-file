﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models.SupplierFeedbackPart;

namespace Portal.Models.Forecast
{
    public class ForecastFeedbackPartList
    {

        public ForecastFeedbackPartList()
        {
            Collection = new List<SupplierFeedbackPartDetail>();
        }

        public List<SupplierFeedbackPartDetail> Collection { set; get; }

    }
}