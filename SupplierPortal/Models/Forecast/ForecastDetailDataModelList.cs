﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Forecast
{
    public class ForecastDetailDataModelList
    {

        public ForecastDetailDataModelList()
        {
            Collection = new List<ForecastDetailDataModel>();
        }

        public List<ForecastDetailDataModel> Collection { set; get; }

    }
}