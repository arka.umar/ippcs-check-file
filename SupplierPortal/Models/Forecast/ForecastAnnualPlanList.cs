﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.Models.Forecast
{
    public class ForecastAnnualPlanList
    {
        public ForecastAnnualPlanList()
        {
            Collection = new List<Portal.Models.AnnualPlan.AnnualPlanDetailData>();
        }

        public List<Portal.Models.AnnualPlan.AnnualPlanDetailData> Collection { set; get; }
    } 
}