﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.Models.Forecast
{
    public class ForecastSupplierPlanList
    {
        public ForecastSupplierPlanList()
        {
            Collection = new List<Portal.Models.Supplier.SupplierPlan>();
        }

        public List<Portal.Models.Supplier.SupplierPlan> Collection { set; get; }
    }
}