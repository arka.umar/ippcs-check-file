﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models.Supplier;

namespace Portal.Models.Forecast
{
    public class ForecastSupplierList
    {        
        public ForecastSupplierList()
        {
            Collection = new List<Portal.Models.Supplier.SupplierName>();
        }

        public List<Portal.Models.Supplier.SupplierName> Collection { set; get; }
    } 
}