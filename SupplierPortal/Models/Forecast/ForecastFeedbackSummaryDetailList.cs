﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models.SupplierFeedbackSummary;

namespace Portal.Models.Forecast
{
    public class ForecastFeedbackSummaryDetailList
    {        
        public ForecastFeedbackSummaryDetailList()
        {
            Collection = new List<Portal.Models.SupplierFeedbackSummary.SupplierFeedbackSummaryDetails>();
        }

        public List<Portal.Models.SupplierFeedbackSummary.SupplierFeedbackSummaryDetails> Collection { set; get; }
    } 
}