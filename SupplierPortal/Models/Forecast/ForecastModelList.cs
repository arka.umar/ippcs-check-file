﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Web.MVC;

namespace Portal.Models.Forecast
{
    public class ForecastModelList
    {
        
        public ForecastModelList()
        {
            Collection = new List<ForecastModel>();
        }

        public List<ForecastModel> Collection { set; get; }

    }
}