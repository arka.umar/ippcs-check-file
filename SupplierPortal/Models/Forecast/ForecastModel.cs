﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.Forecast
{
    public class ForecastModel
    {
        public String PRODUCTION_MONTH { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String LPOP_T_STATUS { get; set; }
        public String LPOP_T_FILE { get; set; }
        public String LPOP_F_STATUS { get; set; }
        public String LPOP_F_FILE { get; set; }
        public String PO_STATUS { get; set; }
        public String PO_FILE_PATH { get; set; }
        public Boolean? T_FEEDBACK_REPLY { get; set; }
        public Boolean? T_FEEDBACK_STATUS { get; set; }
        public Boolean? T_LOCK_STATUS { get; set; }
        public String T_LOCK_CHANGED_BY { get; set; }
        public DateTime? T_LOCK_CHANGED_DT { get; set; }
        public Boolean? F_FEEDBACK_REPLY { get; set; }
        public Boolean? F_FEEDBACK_STATUS { get; set; }
        public Boolean? F_LOCK_STATUS { get; set; }
        public String F_LOCK_CHANGED_BY { get; set; }
        public DateTime? F_LOCK_CHANGED_DT { get; set; }
        public String T_LPOP_DOWNLOADER { get; set; }
        public DateTime? T_LPOP_DOWNLOAD_DT { get; set; }
        public String F_LPOP_DOWNLOADER { get; set; }
        public DateTime? F_LPOP_DOWNLOAD_DT { get; set; }
        public String T_FEEDBACK_USER { get; set; }
        public DateTime? T_FEEDBACK_DT { get; set; }
        public String F_FEEDBACK_USER { get; set; }
        public DateTime? F_FEEDBACK_DT { get; set; }
        public String PO_DOWNLOAD_USER { get; set; }
        public DateTime? PO_DOWNLOAD_DT { get; set; }

        public String PO_NO { get; set; }
        public DateTime? RELEASE_DT { get; set; }

        public String NOTICE { get; set; }
        public Boolean? DELETION_FLAG { get; set; }

        public String CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

    }
}