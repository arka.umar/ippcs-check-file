﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models.SupplierFeedbackSummary;

namespace Portal.Models.Forecast
{
    public class ForecastFeedbackSummaryList
    {        
        public ForecastFeedbackSummaryList()
        {
            Collection = new List<Portal.Models.SupplierFeedbackSummary.SupplierFeedbackSummary>();
        }

        public List<Portal.Models.SupplierFeedbackSummary.SupplierFeedbackSummary> Collection { set; get; }
    } 
}