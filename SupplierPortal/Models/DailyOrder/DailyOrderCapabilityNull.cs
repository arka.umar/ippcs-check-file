﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class DailyOrderCapabilityNull
    {
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String ORDER_TYPE { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT_CD { get; set; }       
    }
}