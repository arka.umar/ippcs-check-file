﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class DailyOrder
    {
        public String MANIFEST_NO { get; set; }
        public DateTime? ORDER_RELEASE_DT { get; set; }
        public String ORDER_TYPE { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_PLANT_CD { get; set; }
        public String SUPPLIER_CD_PLANT { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String RCV_PLANT_CD { get; set; }
        public String PLANT_NM { get; set; }
        public String DOCK_CD { get; set; }
        public Int32 TOTAL_MANIFEST { get; set; }
        public Int32 ORDER_PLAN_QTY { get; set; }
        public Int32 REMAINING_QTY { get; set; }
        public String DOWNLOAD_FLAG { get; set; }
        public String REPLY_CAPABILITY { get; set; }
        public String REPLIED_BY { get; set; }
        public DateTime? REPLIED_DT { get; set; }
        public String DOWNLOADED_BY { get; set; }
        public DateTime? DOWNLOADED_DT { get; set; }
        public String CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public String NOTICE_ID { get; set; }
        public Int16 LOCK_STATUS { get; set; }
        public String FTP_FLAG { get; set; }
    }
}
