﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class PartDetailManifest
    {
        public string MANIFEST_NO { set; get; }
        public string ITEM_NO { set; get; }
        public string PART_NO { set; get; }
        public string PartName { set; get; }
        public Int32 ORDER_QTY { set; get; }
        public Int32 ORDER_PLAN_QTY_LOT { set; get; }
        public string KANBAN_NO { set; get; }
        //public string QtyLot { set; get; }
        //public string QtyPerContainer { set; get; }
        //public string CancelQty { set; get; }
        public Int32 QTY_PER_CONTAINER { set; get; }
        public Int32 RECEIVED_QTY { set; get; }
        public Int32 SHORTAGE_QTY { set; get; }
        public Int32 MISSPART_QTY { set; get; }
        public Int32 DAMAGE_QTY { set; get; }
        public string Notice { set; get; }

        // 2018-12-31
        public String RECEIVE_QTY { get; set; }
        public String REMAIN_QTY { get; set; }

        //2019-01-10
        public string MANIFEST_ITEM_NO { set; get; }

        //2019-02-04
        public String SUBMANIFEST_QTY { get; set; } 
    }
}