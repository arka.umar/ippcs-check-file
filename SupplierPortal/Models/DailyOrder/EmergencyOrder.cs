﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class EmergencyOrder
    {
        public DateTime Order_Release_Date0 { get; set; }
        public string Supplier_code0 { get; set; }
        public int Total_Manifest0 { get; set; }
        public int Order_Plan_Qty0 { get; set; }
        public int Total_Record0 { get; set; }
        public string Download_Flag0 { get; set; }
        public string Download0 { get; set; }
        public string Message0 { get; set; }
    }
}