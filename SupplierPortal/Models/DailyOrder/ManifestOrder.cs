﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class ManifestOrder
    {
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public Int32 REMANING_QTY { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string RECEIVE_PLANT_CODE { get; set; }
        public string DOCK_CODE { get; set; }
        public Int32 TOTAL_ITEM { get; set; }
        public Int32 TOTAL_QTY { get; set; }
        public DateTime ARRIVAL_PLAN_DT { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public string PRINT_FLAG { get; set; }
        public string DOWNLOAD_FLAG { get; set; }
    }

    
}