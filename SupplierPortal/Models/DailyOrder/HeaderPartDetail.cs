﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class HeaderPartDetail 
    {
        public string OrderNumber { set; get; }
        public string ReceivingPlant { set; get; }
        public string SupplierPlant { set; get; }
        public string DockCode { set; get; }
    }
}