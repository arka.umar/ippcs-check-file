﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class NonDailyOrder
    {
        public string MANIFEST_NO { get; set; }
        public DateTime ORDER_RELEASE_DT { get; set; }
        public string SUPPLIER_PLANT_CD { get { return SUPPLIER_CD + "-" + SUPPLIER_PLANT; } }
        public string SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string TOTAL_QTY { get; set; }
        public string PRINT_FLAG { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public DateTime ARRIVAL_PLAN_DT { get; set; }
        public String FTP_FLAG { get; set; }
        public String DOWNLOAD_FLAG { get; set; }
        public String DOWNLOADED_BY { get; set; }
        public DateTime DOWNLOADED_DT { get; set; }
        public string EO_TYPE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }

    public class NonDailyOrderSPEXSubSupp
    {
        public string MANIFEST_NO { get; set; }
        public DateTime ORDER_RELEASE_DT { get; set; }
        public string SUPPLIER_PLANT_CD {
            get
            {
                return SUPPLIER_CD + "-" + SUPPLIER_PLANT;
            }
        }
        public string SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string TOTAL_QTY { get; set; }
        public string PRINT_FLAG { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public DateTime ARRIVAL_PLAN_DT { get; set; }
        public String FTP_FLAG { get; set; }
        public String DOWNLOAD_FLAG { get; set; }
        public String DOWNLOADED_BY { get; set; }
        public DateTime DOWNLOADED_DT { get; set; }
        public string SUB_SUPP_CD { get; set; }
        public string SUB_SUPP_PLANT { get; set; }
        public string SUB_SUPPLIER_PLANT_CD {
            get
            {
                return (SUB_SUPP_CD != "" && SUB_SUPP_PLANT != "")
                    && (SUB_SUPP_CD != null && SUB_SUPP_PLANT != null)
                    ? SUB_SUPP_CD + "-" + SUB_SUPP_PLANT : " ";
            }
        }

        // 2018-12-31
        public String DELIVERY_QTY { get; set; }
        public String REMAIN_QTY { get; set; }
        public String COMPLETION_STS { get; set; }

        //2019-02-04
        public String SUBMANIFEST_QTY { get; set; }
        public String RECEIVE_QTY { get; set; }
    }


}