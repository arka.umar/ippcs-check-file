﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class ReportGetAllDailyOrderModel
    {
        public DateTime ORDER_RELEASE_DT { set; get; }
        public String SUPPLIER_CD_PLANT { set; get; }
        public String SUPPLIER_NAME { set; get; }
        public String DOCK_CD { set; get; }
        public Int32 TOTAL_MANIFEST { set; get; }
        public Int32 ORDER_PLAN_QTY { set; get; }
        public String DOWNLOAD_FLAG { set; get; }
        public String REPLY_CAPABILITY { set; get; }
        public String REPLIED_BY { set; get; }
        public DateTime REPLIED_DT { set; get; }
        public String DOWNLOADED_BY { set; get; }
        public DateTime DOWNLOADED_DT { set; get; }        
    }
}