﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class ReportGetAllNonDailyOrderModel
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_PLANT_CD { get { return SUPPLIER_CD + "-" + SUPPLIER_PLANT; } }
        public string SUPPLIER_PLANT { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_PLAN_QTY { get; set; }
        public DateTime RECEIVE_DT { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string UNIQUE_NO { get; set; }
        
        
        //public string TOTAL_QTY { get; set; }
        //public string PRINT_FLAG { get; set; }
        //public string RECEIVE_FLAG { get; set; }
        //public DateTime ARRIVAL_PLAN_DT { get; set; }
    }



    public class ReportGetAllNonDailyOrderModelEO
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_PLANT_CD { get { return SUPPLIER_CD + "-" + SUPPLIER_PLANT; } }
        public string SUPPLIER_PLANT { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_PLAN_QTY { get; set; }
        public string RECEIVE_DT { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string UNIQUE_NO { get; set; }
        public string BUILD_OUT_FLAG { get; set; }

        //public string TOTAL_QTY { get; set; }
        //public string PRINT_FLAG { get; set; }
        //public string RECEIVE_FLAG { get; set; }
        //public DateTime ARRIVAL_PLAN_DT { get; set; }
    }

    public class ReportGetAllNonDailyOrderModelSPEX
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_PLANT_CD { get { return SUPPLIER_CD + "-" + SUPPLIER_PLANT; } }
        public string SUPPLIER_PLANT { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_PLAN_QTY { get; set; }
        public string RECEIVE_DT { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string UNIQUE_NO { get; set; }
        public string BUILD_OUT_FLAG { get; set; }

        //public string TOTAL_QTY { get; set; }
        //public string PRINT_FLAG { get; set; }
        //public string RECEIVE_FLAG { get; set; }
        //public DateTime ARRIVAL_PLAN_DT { get; set; }
    }

    public class ReportGetAllNonDailyOrderModelSPEXSubSupplier
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_PLAN_QTY { get; set; }
        public string RECEIVE_DT { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string UNIQUE_NO { get; set; }
        public string BUILD_OUT_FLAG { get; set; }

        //public string SUB_SUPP_CD { get; set; }
        //public string SUB_SUPP_PLANT { get; set; }
        //{ get { return SUB_SUPP_CD + "-" + SUB_SUPP_PLANT; } }

        //public string TOTAL_QTY { get; set; }
        //public string PRINT_FLAG { get; set; }
        //public string RECEIVE_FLAG { get; set; }
        //public DateTime ARRIVAL_PLAN_DT { get; set; }
    }

    public class ReportGetAllNonDailyOrderModelCPO
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_PLAN_QTY { get; set; }
        public string RECEIVE_DT { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string UNIQUE_NO { get; set; }
        public string BUILD_OUT_FLAG { get; set; }
    }

    public class SPEXDailyOrderRAWData
    {
        public string DATA { get; set; }
        public int ORDER_QTY { get; set; }
        public int LVL { get; set; }
    }

    public class CPODailyOrderRAWData
    {
        public string DATA { get; set; }
        public int ORDER_QTY { get; set; }
        public int LVL { get; set; }
    }
     
    public class ReportPraSPEXDailyOrder
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_PLAN_QTY { get; set; }
        public string RECEIVE_DT { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string UNIQUE_NO { get; set; }
        public string BUILD_OUT_FLAG { get; set; }
        public string DELIVERY_QTY { get; set; }
        public string RECEIVE_QTY { get; set; }
        public string STATUS { get; set; }

    }

    public class ReportPraSPEXDailyOrderNew
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; } 
        public string ORDER_QTY { get; set; }
        public string SUBMANIFEST_QTY { get; set; }
        public string REMAIN_QTY { get; set; }
        public string SORTING_OR_BINNING_QTY { get; set; } 
         
    }

    public class ReportPraSPEXDailyOrderNewDetail
    {
        public string MANIFEST_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string SUB_MANIFEST_NO { get; set; }  
    }

     




}