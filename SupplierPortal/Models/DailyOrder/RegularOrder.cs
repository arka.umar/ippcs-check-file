﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class RegularOrder
    {
        public DateTime OrderReleaseDateRegular { get; set; }
        public string SupplierCodeReguler { get; set; }
        public int OrderPlanQtyReguler { get; set; }
        public int TotalManifestReguler { get; set; }
        public int RemainingQtyReguler { get; set; }

        public string DownloadFlagReguler { get; set; }
        public string DownloadReguler { get; set; }
        public string CapabilityReguler { get; set; }
        public string NotifReguler { get; set; }      
        public string DownloadByReguler { set; get; }
        public string DownloadDateReguler { set; get; }
        public string CapabilityDateReguler { set; get; }
        public string CapabilityByReguler { set; get; }

    }
}