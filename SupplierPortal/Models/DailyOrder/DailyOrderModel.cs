﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
    public class DailyOrderModel
    {
        public List<DailyOrder> RegularOrders { get; set; }
        public List<NonDailyOrder> EmergencyOrders { get; set; }
        public List<NonDailyOrder> ComponentPartOrders { get; set; }
        public List<NonDailyOrder> JunbikiOrders { get; set; }
        public List<NonDailyOrder> ProblemPartOrders { get; set; }
        public List<NonDailyOrder> ServicePartExport { get; set; }
        public List<NonDailyOrderSPEXSubSupp> ServicePartExportSubSupp { get; set; }
        public List<ManifestOrder> ManifestOrders { get; set; }
        public List<PartDetailManifest> PartDetails { get; set; }
        public DailyOrderModel()
        {
            RegularOrders = new List<DailyOrder>();
            EmergencyOrders = new List<NonDailyOrder>();
            ComponentPartOrders = new List<NonDailyOrder>();
            JunbikiOrders = new List<NonDailyOrder>();
            ProblemPartOrders = new List<NonDailyOrder>();
            ManifestOrders = new List<ManifestOrder>();
            PartDetails = new List<PartDetailManifest>();
            ServicePartExport = new List<NonDailyOrder>();
            ServicePartExportSubSupp = new List<NonDailyOrderSPEXSubSupp>();
        }

    }
}