﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.DailyOrder
{
   public class ComponentPartOrder
   {
        public DateTime OrderReleaseDateCPO { get; set; }
        public string SupplierCodeCPO { get; set; }
        public int TotalManifestCPO { get; set; }
        public int OrderPlanQtyCPO { get; set; }
        public int RemainingQtyCPO { get; set; }
        public string DownloadFlagCPO { get; set; }
        public string DownloadCPO { get; set; }
        public string CapabilityCPO { get; set; }
        public string NotifCPO { get; set; }

        public string DownloadByCPO { set; get; }
        public string DownloadDateCPO { set; get; }
        public string CapabilityByCPO { set; get; }
        public string CapabilityDateCPO { set; get; }
    }
}