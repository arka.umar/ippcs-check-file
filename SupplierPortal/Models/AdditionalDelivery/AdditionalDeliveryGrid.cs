﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AdditionalDelivery
{
    public class AdditionalDeliveryGridData
    {
        public List<AdditionalDeliveryGridContent> GridContent { get; set; }
    }

    public class AdditionalDeliverySourceGridData
    {
        public List<AdditionalDeliverySourceGridContent> GridSourceContent { get; set; }
    }

    public class AdditionalDeliveryDestinationGridData
    {
        public List<AdditionalDeliveryDestinationGridContent> GridDestinationContent { get; set; }
    }
}