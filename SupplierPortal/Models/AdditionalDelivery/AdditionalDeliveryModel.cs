﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Portal.Models.Globals;

namespace Portal.Models.AdditionalDelivery
{
    public class AdditionalDeliveryModel
    {
        public List<AdditionalDeliverySourceGridContent> GridSourceContent { get; set; }
        public List<AdditionalDeliveryDestinationGridContent> GridDestinationContent { get; set; }
        public List<AdditionalDeliveryGridContent> GridContent { get; set; }
        public List<PopupAdditionalDeliveryGridContent> GridContentPopup { get; set; }
        public List<PopupAdditionalDeliveryGridOrderPartContent> GridOrderPartContent { get; set; }
        
        public AdditionalDeliveryModel()
        {
            GridSourceContent = new List<AdditionalDeliverySourceGridContent>();
            GridDestinationContent = new List<AdditionalDeliveryDestinationGridContent>();
            GridContent = new List<AdditionalDeliveryGridContent>();
            GridContentPopup = new List<PopupAdditionalDeliveryGridContent>();
            GridOrderPartContent = new List<PopupAdditionalDeliveryGridOrderPartContent>();
        }
    }

    public class AdditionalDeliveryExtraRouteRate
    {
        public string extra_rate { get; set; }
    }

}