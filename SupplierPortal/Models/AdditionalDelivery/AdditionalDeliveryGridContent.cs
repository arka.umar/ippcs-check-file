﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AdditionalDelivery
{ 
    public class AdditionalDeliveryGridContent
    {
        public string OrderNo { set; get; }
        public string ManifestNo { set; get; }
        public string OrderType { set; get; }
        public DateTime OrderReleaseDate { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierPlant { set; get; }
        public string SupplierName { set; get; }
        public string Route { set; get; }
        public string Rate { set; get; }
        public string ReceivePlantCode { set; get; }
        public string ReceiveDockCode { set; get; }
        public int TotalItem { set; get; }
        public int TotalQty { set; get; }
        public int ErrorQty { set; get; }
        public DateTime ArrivalPlanDate { set; get; }
        public Nullable<DateTime> ArrivalActualDate { set; get; }
    }

    public class DockCompanyClass
    {
        public string DockCode { set; get; }
        public string DockPlant { set; get; }
        public string DockCompany { set; get; }
    }

    public class ResultClass
    {
        public string ResultProccess { set; get; }
    }

    public class AdditionalDeliverySourceGridContent
    {
        public string TrackingPoint { set; get; }
        public string TrackingType { set; get; }
        public Nullable<DateTime> ArrivalTime { set; get; }
        public Nullable<DateTime> DepartureTime { set; get; }
    }

    public class AdditionalDeliveryDestinationGridContent
    {
        public string TrackingPoint { set; get; }
        public string TrackingType { set; get; }
        public Nullable<DateTime> ArrivalTime { set; get; }
        public Nullable<DateTime> DepartureTime { set; get; }
    }

    public class PopupAdditionalDeliveryGridContent
    {
        public string OrderNo { set; get; }
        public string ManifestNo { set; get; }
        public string OrderType { set; get; }
        public DateTime OrderReleaseDate { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierPlant { set; get; }
        public string SupplierName { set; get; }
        public string Route { set; get; }
        public string Rate { set; get; }
        public string ReceivePlantCode { set; get; }
        public string ReceiveDockCode { set; get; }
        public int TotalItem { set; get; }
        public int TotalQty { set; get; }
        public int ErrorQty { set; get; }
        public DateTime ArrivalPlanDate { set; get; }
        public Nullable<DateTime> ArrivalActualDate { set; get; }
    }

    public class PopupAdditionalDeliveryGridOrderPartContent
    {
        public string ManifestNo { set; get; }
        public string SupplierCode { set; get; }
        public string ShippingDock { set; get; }
        public string SupplierPlant { set; get; }
        public string RcvPlantCode { set; get; }
        public string DockCode { set; get; }
        public string PartNo { set; get; }
        public string PartName { set; get; }
        public int QtyPerContainer { set; get; }
        public int OrderQty { set; get; }
    }
}