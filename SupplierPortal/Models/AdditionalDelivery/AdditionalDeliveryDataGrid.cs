﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AdditionalDelivery
{
    public class AdditionalDeliveryDataGrid
    { 
        public string DockCode { get; set; }
        public string DetailOrderNo { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        
        public DateTime ArrTime { get; set; }
        public DateTime DeptSuppTime { get; set; }

        public DateTime ArrPlan { get; set; }
        public DateTime DeptPlan { get; set; }
    }
}