﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.AdditionalDelivery
{
    public class ComboDelivery
    {
        public string DeliveryNo { set; get; }
    }
    public class ComboRate
    {
        public string Rate { set; get; }
    }
    public class ComboRoute
    {
        public string Route { set; get; }
    }
}