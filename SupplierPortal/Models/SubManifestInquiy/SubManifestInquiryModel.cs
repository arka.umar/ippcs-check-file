﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SubManifestInquiry
{
    public class SubManifest
    {  
        public string MANIFEST_NO { get; set; }
        public string SUB_MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string SUPPLIER { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUB_SUPPLIER { get; set; } 
        public string DOCK_CODE { get; set; }
        public string SUB_ROUTE_CD { get; set; } 
        public string ETD { get; set; }
        public string ETA { get; set; }
        public string QTY { get; set; }
        public string SM_DATE { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public string STATUS { get; set; }
        public string UPLOAD_NO { get; set; }
        public string REF_NO { get; set; }
        public string CANCEL_BY { get; set; }
        public string CANCEL_DT { get; set; }
        public string FTP_FLAG { get; set; }
    }

    public class SubManifestDetail
    {
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string DELIVERY_QTY { get; set; } 

    }

    public class SubManifestReport
    {
        public string MANIFEST_NO { get; set; }
        public string SUB_MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string SUPPLIER { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUB_SUPPLIER { get; set; }
        public string DOCK_CODE { get; set; }
        public string SUB_ROUTE_CD { get; set; }
        public string ETD { get; set; }
        public string RELEASE { get; set; }
        public string SM_DATE { get; set; }
        public string STATUS { get; set; }
        public string UPLOAD_NO { get; set; }
        public string REF_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string DELIVERY_QTY { get; set; }
    }
}