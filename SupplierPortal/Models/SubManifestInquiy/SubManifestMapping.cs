﻿using Portal.Models.SubManifestInquiry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.SubManifestInquiry
{

    
    public class SubManifestMapping
    {
        public List<SubManifest> SubManifestHeaderList { get; set; }
        public SubManifestMapping() {
            SubManifestHeaderList = new List<SubManifest>();           
        }
    }
}