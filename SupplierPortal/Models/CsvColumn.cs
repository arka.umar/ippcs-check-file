﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models
{
    public class CsvColumn
    {
        public string Text { get; set; }
        public string Column { get; set; }
        public string DataType { get; set; }
        public int Len { get; set; }
        public int Mandatory { get; set; }

        public static List<CsvColumn> Parse(string v)
        {
            List<CsvColumn> l = new List<CsvColumn>();

            string[] x = v.Split(';');


            for (int i = 0; i < x.Length; i++)
            {
                string[] y = x[i].Split('|');
                CsvColumn me = null;
                if (y.Length >= 4)
                {
                    int len = 0;
                    int mandat = 0;
                    int.TryParse(y[2], out len);
                    int.TryParse(y[3], out mandat);
                    me = new CsvColumn()
                    {
                        Text = y[0],
                        DataType = y[1],
                        Len = len,
                        Mandatory = mandat
                    };
                    l.Add(me);
                }

                if (y.Length > 4 && me != null)
                {
                    me.Column = y[4];
                }
            }
            return l;
        }
    }
}