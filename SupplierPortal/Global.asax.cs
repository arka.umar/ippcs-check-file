﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.MVC.Login;

/* namespace for background process */
using Portal.Controllers.Dummy;
using Toyota.Common.Web.Download;
using DevExpress.Web.Mvc;

namespace Portal
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : MVCBaseApplication
    {
        protected override void StartUp()
        {
            ThemeAssembly.ThemesProviderEx.Register();
            SystemState.GetInstance().EnableSecuredUrlModification = true;
            Bootstrap.GetInstance().RegisterProvider<ILoginValidator>(typeof(WebEntranceLoginValidator), false);

            //InternalBackgroundTaskQueue backgroundTaskQueue = InternalBackgroundTaskQueue.GetInstance();
            //backgroundTaskQueue.Add(new DummyTask());
            //backgroundTaskQueue.Add(new GoodsReceiptPostingTask());
            //backgroundTaskQueue.Add(new LPOPPostingTask());
            //backgroundTaskQueue.Add(new CompletenessCheckTask());


            ModelBinders.Binders.DefaultBinder = new DevExpressEditorsBinder(); 

            FileDownloadContextRegistry fileDownloadContextRegistry = FileDownloadContextRegistry.GetInstance();
            fileDownloadContextRegistry.Add(new FileDownloadContext()
            { 
                Name = "dailyorder", 
                Path = "Portal/DailyOrderPrinting", 
                Type = FileDownloadContextType.FTP
            });
            fileDownloadContextRegistry.Add(new FileDownloadContext()
            {
                Name = "emergencyorder",
                Path = "Portal/EmergencyOrder",
                Type = FileDownloadContextType.FTP
            });
            fileDownloadContextRegistry.Add(new FileDownloadContext()
            {
                Name = "cpoorder",
                Path = "Portal/ComponentPartOrder",
                Type = FileDownloadContextType.FTP
            });
            fileDownloadContextRegistry.Add(new FileDownloadContext()
            {
                Name = "servicepartexport",
                Path = "Portal/ServicePartExport",
                Type = FileDownloadContextType.FTP
            });
            fileDownloadContextRegistry.Add(new FileDownloadContext()
            {
                Name = "junbikiorder",
                Path = "Portal/JunbikiOrder",
                Type = FileDownloadContextType.FTP
            });
            fileDownloadContextRegistry.Add(new FileDownloadContext()
            {
                Name = "problempartorder",
                Path = "Portal/ProblemPartOrder",
                Type = FileDownloadContextType.FTP
            });
        }        
    }
}