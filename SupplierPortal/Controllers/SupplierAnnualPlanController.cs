﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.SupplierAnnualPlan;

namespace Portal.Controllers.Upload
{
    public class SupplierAnnualPlanController : BaseController
    {
         public SupplierAnnualPlanController()
            : base("Supplier Annual Plan")
        { 
        }
      
        protected override void StartUp()
         {
         }

        protected override void Init()
        {
            //AnnualModel model = new AnnualModel();
            //for (int i = 0; i < 21; i++)
            //{
            //    model.AnnualDatas.Add(new AnnualData()
            //    {
                    
            //        ProductionYear = 1991+i,
            //        Versi = "RAP",
            //        UploadDate = DateTime.Now,
            //        ReplyFeedback = "RF" + (i + 1),
            //        LastUpdate = DateTime.Now,
            //        Messages = "MSG" + (i + 1),

            //    });
            //}
            //Model.AddModel(model);

            AnnualModel model = new AnnualModel();
            model.AnnualDatas = SupplierAnnualPlan();
            Model.AddModel(model);
        }

        protected List<AnnualData> SupplierAnnualPlan()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<AnnualData> LogList = new List<AnnualData>();
            var QueryLog = db.Query<AnnualData>("GetSupplierAnnualPlan", new object[] { "", "", "" });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new AnnualData()
                    {
                        ProductionYear = q.ProductionYear,
                        Version = q.Version,
                        UploadDate = q.UploadDate,
                        ReplyFeedback = q.ReplyFeedback,
                        LastUpdate = q.LastUpdate,
                        Messages = q.Messages
                    });
                }
            }
            db.Close();
            return LogList;
        }

        public ActionResult ReloadGrid(string supplierCode, string productionYear, string annualPlanVersion)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            AnnualModel model = Model.GetModel<AnnualModel>();
            model.AnnualDatas = db.Fetch<AnnualData>("GetSupplierAnnualPlan", new object[] { supplierCode, productionYear, annualPlanVersion });

            model.AnnualDatas = model.AnnualDatas;
            db.Close();
            return PartialView("GridIndex", Model);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("HeaderSupplierAnnualPlan", Model);
        }

        public ActionResult PartialUpload()
        {
            return PartialView("UploadSupplierAnnualPlan", Model);
        }
    }
}
