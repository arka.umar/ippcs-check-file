﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Compression;
using DevExpress.Web.ASPxUploadControl;

using Toyota.Common.Web.Html;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Configuration;
using System.Web.UI.WebControls;
using System.Net;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.Notification;
using Toyota.Common.Web.SAPConnector;

namespace Portal.Controllers
{
    public class SAPController : BaseController
    {
        public SAPController()
            : base("Sytem Application And Product")
        { 
        
        }

        private  IDBContextManager dbManager;
        private IDBContext dbContext;
        
        protected override void Init()
        {
            IProviderResolver providerResolver = ProviderResolver.GetInstance();
            IApplicationConfiguration appConfig = providerResolver.Get<IApplicationConfiguration>();
            dbManager = providerResolver.Get<IDBContextManager>();

            dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            dbContext.Execute("InsertSAP", new object[] {"0100008076", "2012"});  // insert log history call SAP
            dbContext.Close();

            SAPcommunication SAPS = new SAPcommunication();
            
            string ResultClearingDt = SAPS.GetClearingDt("0100008076", "2012");  // call clearing Dt
            string ResultClearingNo = SAPS.GetClearingNo("0100008076", "2012");  // call clearing No
           

             
            
            
        }

        protected override void StartUp()
        {
            
        }


    }
}
