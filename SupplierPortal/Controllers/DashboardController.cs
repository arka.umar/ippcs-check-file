﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Notification;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using System.IO;
using Portal.Models.Dashboard;
using Toyota.Common.Web.Messaging.Notification;
using Toyota.Common.Web.State;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Upload;

namespace Portal.Controllers
{
    public class DashboardController : BaseController
    {
        public DashboardController()
            : base("Dashboard")
        {
            PageLayout.UseNotificationWall = true;
            PageLayout.UseDockingLeftPane = true;
            PageLayout.UseDockingRightPane = true;
        }
        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            
        }
    }
}
