﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Portal.Models.RoleUser;
using System.Globalization;

namespace Portal.Controllers.RoleUser
{
    public class LoginSessionController : BaseController
    {
        public LoginSessionController()
            : base("User History Login")
        {
            
        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            ViewData["StartDate"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString();
            ViewData["EndDate"] = DateTime.Now.ToShortDateString();
            

            RoleUserModel mdl = new RoleUserModel();
            string un;
            DateTime startdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime enddate = DateTime.Now;
            //if (Request.Params["username"] != null)
            //{
            //    un = !string.IsNullOrEmpty(Request.Params["username"]) ? Request.Params["username"] : string.Empty;
            //}
            //else
            un = string.Empty;

            mdl.GetLoginSessions = GetLoginSession(un, startdate, enddate);
            Model.AddModel(mdl);
        }

        public ActionResult LoginSessionPartial()
        {
            RoleUserModel mdl = new RoleUserModel();
            string un = !string.IsNullOrEmpty(Request.Params["username"]) ? Request.Params["username"] : string.Empty;
            DateTime startdate = DateTime.ParseExact(Request.Params["StartDate"], "yyyy/M/d", CultureInfo.InvariantCulture);
            DateTime enddate = DateTime.ParseExact(Request.Params["EndDate"], "yyyy/M/d", CultureInfo.InvariantCulture);

            mdl.GetLoginSessions = GetLoginSession(un, startdate, enddate);
            Model.AddModel(mdl);
            return PartialView("LoginSessionPartial", Model);
        }

        protected List<LoginSession> GetLoginSession(string userName, DateTime startDate, DateTime endDate)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_TMMIN_ROLE);
            List<LoginSession> newsList = new List<LoginSession>();
            string startdate = String.Format("{0:yyyy/M/d}", startDate);
            string enddate = String.Format("{0:yyyy/M/d}", endDate);
            string un;
            un = string.Format("%{0}%", userName);
            var QueryLogin = db.Query<LoginSession>("GetLoginSessions", new object[] { 
                                        un, 
                                        startdate,
                                        enddate
                                    });
            if (QueryLogin.Any())
            {
                foreach (var q in QueryLogin)
                {
                    newsList.Add(new LoginSession()
                    {
                        Username = q.Username,
                        SessionID = q.SessionID,
                        LoginTime = q.LoginTime,
                        IPAddress = q.IPAddress,
                        Browser = q.Browser
                    });
                }
            }
            db.Close();
            return newsList;
        }
    }
}
