﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;
using System.Web.Mvc;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Task;
using Toyota.Common.Task.External;

namespace Portal.Controllers
{
    public class JobMonitoringController:BaseController
    {
        public JobMonitoringController()
            : base("Job Monitoring")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {            
        }

        public ActionResult PartialHeaderJobMonitoring()
        {
            return PartialView("PartialHeaderJobMonitoring");
        }

        public ActionResult PartialRegistryGrid()
        {            
            IList<ExternalBackgroundTask> tasks = BackgroundTaskService.GetInstance().GetRegisteredTasks();
            return PartialView("PartialRegistryGrid",tasks);
        }

        public ActionResult PartialHistoryGrid()
        {
            IList<ExternalBackgroundTask> tasks = BackgroundTaskService.GetInstance().GetHistoryTasks();
            return PartialView("PartialHistoryGrid", tasks);
        }

        public ActionResult PartialQueueGrid()
        {
            IList<ExternalBackgroundTask> tasks = BackgroundTaskService.GetInstance().GetRegisteredTasks();
            return PartialView("PartialQueueGrid", tasks);
        }
    }
}