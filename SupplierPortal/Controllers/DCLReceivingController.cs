﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Media;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;
using Portal.Models;
using Portal.Models.PO;
using Portal.Models.DCLReceiving;
using Toyota.Common.Web.Credential;
using System.Xml;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Security.Principal;
using System.Net.Sockets;
using Cryptography;
using System.Configuration;
using System.Xml.Serialization;
using Portal.Models.ScanGate;


//using Toyota.Common.Web.Notification;

namespace Portal.Controllers
{
    [SkipUnlockController]
    public class DCLReceivingController : BaseController
    {
        //
        // GET: /DCLReceiving/

        public DCLReceivingController()
            : base("DCL Receiving")
        {
        }

        //private Queue<string> _downloadUrls = new Queue<string>();

        //private void downloadFile(IEnumerable<string> urls)
        //{
        //    foreach (var url in urls)
        //    {
        //        _downloadUrls.Enqueue(url);
        //    }

            // Starts the download
            //btnGetDownload.Text = "Downloading...";
            //btnGetDownload.Enabled = false;
            //progressBar1.Visible = true;
            //lblFileName.Visible = true;

        //    DownloadFile();
        //}

        //private void DownloadFile()
        //{
        //    if (_downloadUrls.Any())
        //    {
        //        WebClient client = new WebClient();
        //        client.DownloadProgressChanged += client_DownloadProgressChanged;
        //        client.DownloadFileCompleted += client_DownloadFileCompleted;

        //        var url = _downloadUrls.Dequeue();
        //        string FileName = url.Substring(url.LastIndexOf("/") + 1,
        //                    (url.Length - url.LastIndexOf("/") - 1));

        //        client.DownloadFileAsync(new Uri(url), "C:\\Test4\\" + FileName);
        //        lblFileName.Text = url;
        //        return;
        //    }

        //    // End of the download
        //    btnGetDownload.Text = "Download Complete";
        //}

        //private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        // handle error scenario
        //        throw e.Error;
        //    }
        //    if (e.Cancelled)
        //    {
        //        // handle cancelled scenario
        //    }
        //    DownloadFile();
        //}

        //void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        //{
        //    double bytesIn = double.Parse(e.BytesReceived.ToString());
        //    double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
        //    double percentage = bytesIn / totalBytes * 100;
        //    progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());
        //}
        
        protected override void StartUp()
        {


        //    DirectoryInfo di = Directory.CreateDirectory("C:\\tempIPPCS\\");
        //    System.Security.AccessControl.DirectorySecurity dSec = di.GetAccessControl();
        //    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
        //    //dSec.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(@"LV38PCE00081461\ASPNET", System.Security.AccessControl.FileSystemRights.FullControl, System.Security.AccessControl.AccessControlType.Allow));
        //    dSec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.CreateDirectories | FileSystemRights.CreateFiles, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
        //    di.SetAccessControl(dSec);


        //    //DirectorySecurity sec = Directory.GetAccessControl(path);
        //    //// Using this instead of the "Everyone" string means we work on non-English systems.
        //    //SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
        //    //sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
        //    //Directory.SetAccessControl(path, sec);

        //    //DirectorySecurity sec = Directory.GetAccessControl("C:\\tempIPPCS\\");
        //    //foreach (FileSystemAccessRule acr in sec.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        //    //{
        //    //    Console.WriteLine("{0} | {1} | {2} | {3} | {4}", acr.IdentityReference.Value, acr.FileSystemRights, acr.InheritanceFlags, acr.PropagationFlags, acr.AccessControlType);
        //    //}

        //    string remoteUri = Server.MapPath("~") + @"\Content\Sounds\";
        //     //string remoteUri = "https://ppc-trial.toyota.co.id/Content/Sounds/";

        //     if (remoteUri != null)
        //    {
        //     string myStringWebResource = null;
        //     string[] fileName = 
        //     {
        //     "ARV Advance.wav",
        //     "ARV Delay.wav",
        //     "ARV OnTime.wav", 
        //     "Double Scan.wav", 
        //     "DPT Advance.wav", 
        //     "DPT Delay.wav",
        //     "DPT OnTime.wav",
        //     "Failure.wav", 
        //     "Sirine.wav",
        //     "Success.wav"
        //     };
             
        //     WebClient myWebClient = new WebClient();

            
             

        //     for (int i=0 ; i < fileName.Count() ; i++)
        //     {

                 
        //         myStringWebResource = remoteUri + fileName[i];
        //         //FileIOPermission ioPerm = new FileIOPermission(FileIOPermissionAccess.Write,"C:\\tempIPPCS\\" + fileName[i]);

        //         myWebClient.DownloadFile(myStringWebResource, "C:\\tempIPPCS\\" + fileName[i]);
                
               
        //     }
             
        //}
        //   else
        //   {
        //                throw new Exception(remoteUri);
        //   }

            
        }
        protected override void Init()
        {
            int flag = 0;
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = true;

            DateTime arrAct= Convert.ToDateTime("1900-01-01 12:02:00.000");
            DCLReceivingModel mdl = new DCLReceivingModel();
            for (int i = 1; i <= 40; i++)
            {
                if (i == 1 || i == 4 || i == 7 || i == 10 || i == 13 || i == 17 || i == 20)
                {
                    if (i < 10)
                    {
                        /*mdl.DCLReceivings.Add(new DCLReceivingData()
                            {

                                No = "" + i,
                                Station = "4",
                                TripNo = "0" + i,
                                RouteDate = DateTime.Now,
                                ArrivalPlan = "13:10",
                                ActualArrival = "13:06",
                                Status = "Delay",
                                Time = "22",
                                Date = i + "/10/2012",
                                Dock = "4B",
                                RouteName = "RK35-04",
                                ArrivalStatus = "Delay",
                                GapArr = "" + i,
                                DeparturePlan = "13:17",
                                ActualDeparture = "13:35",
                                DepartureStatus = "Delay",
                                GapDep = "" + i
                            }
                        );
                    }
                    else
                    {
                            mdl.DCLReceivings.Add(new DCLReceivingData()
                            {

                                No = "" + i,
                                Station = "4",
                                TripNo = "" + i,
                                RouteDate = DateTime.Now,
                                ArrivalPlan = "13:10",
                                ActualArrival = "13:06",
                                Status = "Delay",
                                Time = "22",
                                Date = i + "/10/2012",
                                Dock = "4B",
                                RouteName = "RK35-04",
                                ArrivalStatus = "Delay",
                                GapArr = "" + i,
                                DeparturePlan = "13:17",
                                ActualDeparture = "13:35",
                                DepartureStatus = "Delay",
                                GapDep = "" + i
                            }
                        );
                    }
                }
                else
                {
                    if (i < 10)
                    {
                        mdl.DCLReceivings.Add(new DCLReceivingData()
                        {
                            No = "" + i,
                            Station = "4",
                            TripNo = "0" + i,
                            RouteDate = DateTime.Now,
                            ArrivalPlan = "13:10",
                            ActualArrival = "13:06",
                            Status = "Delay",
                            Time = "22",
                            Date = i + "/10/2012",
                            Dock = "4R",
                            RouteName = "DK09-03",
                            ArrivalStatus = "On Time",
                            GapArr = "" + i,
                            DeparturePlan = "13:17",
                            ActualDeparture = "13:35",
                            DepartureStatus = "On Time",
                            GapDep = "" + i
                        }
                    );
                    }
                    else
                    {
                                mdl.DCLReceivings.Add(new DCLReceivingData()
                                {
                                    No = "" + i,
                                    Station = "4",
                                    TripNo = "" + i,
                                    RouteDate = DateTime.Now,
                                    ArrivalPlan = "13:10",
                                    ActualArrival = "13:06",
                                    Status = "Delay",
                                    Time = "22",
                                    Date = i + "/10/2012",
                                    Dock = "4R",
                                    RouteName = "DK09-03",
                                    ArrivalStatus = "On Time",
                                    GapArr = "" + i,
                                    DeparturePlan = "13:17",
                                    ActualDeparture = "13:35",
                                    DepartureStatus = "Delay",
                                    GapDep = "" + i
                                }
                           );*/
                        if ( flag  == 0)
                        {
                            mdl.DCLReceivings.Add(new DCLReceivingData()
                                    {

                                        //No = "" + i,
                                        //Station = "4",
                                        //TripNo = "" + i,
                                        //RouteDate = DateTime.Now,
                                        ArrivalPlan = "arrAct",
                                        ActualArrival = "arrAct",
                                        //Status = "Delay",
                                        ////Time = "22",
                                        ////Date = i + "/10/2012",
                                        //Dock = "4R",
                                        //RouteName = "DK09-03",
                                        ////ArrivalStatus = "On Time",
                                        ////GapArr = "" + i,
                                        DeparturePlan = "13:17",
                                        ActualDeparture = "13:35",
                                        //DepartureStatus = "Delay",
                                        //GapDep = "" + i

                                    });
                            flag = 1;
                                
                        }
                        else
                        {
                            //return flag();
                        }
                    }
                }
            }

            Model.AddModel(mdl);
             

             DCLReceivingModel mdls = new DCLReceivingModel();
            mdls.DCLReceivings = DCLget();
            mdls.Combo = dclGetCombo();
            //Model.AddModel(new Search { });
            Model.AddModel(mdls);
        }

        protected List<DCLReceivingDb> dclGetRecDb()
        {
            DCLReceivingModel mdl = new DCLReceivingModel();
            List<DCLReceivingDb> DCLReceivingGet = new List<DCLReceivingDb>();
            for (int i = 1; i <= 5; i++)
            {
                DCLReceivingGet.Add(new DCLReceivingDb()
                {
                    ARRIVAL_ACTUAL_DT = Convert.ToDateTime(" "),
                    ARRIVAL_PLAN_DT = Convert.ToDateTime(" "),
                    DEPARTURE_ACTUAL_DT = Convert.ToDateTime(" "),
                    DEPARTURE_PLAN_DT = Convert.ToDateTime(" "),
                    //public string DEPARTURE_GAP { set; get; }
                    //DEPARTURE_GAP = Convert.ToDateTime(" ")

                });
            }
            return DCLReceivingGet;
        }
        protected List<DCLScreenCombo> dclGetCombo()
        {
            DCLReceivingModel mdl = new DCLReceivingModel();
            List<DCLScreenCombo> combo = new List<DCLScreenCombo>();
            for (int i = 1; i <= 5; i++)
            {
                combo.Add(new DCLScreenCombo()
                {
                    LPCode = "MML" + i,
                    LPName = "Mitra mandiri logistik",
                    Route = "RS2" + i,
                    TripNo = "2",
                    SuppCode = "AT Indonesia" + i,
                    OrderNo = "201212" + i + "02",
                    Reason = "Emergency Order"

                });
            }
            return combo;
        }

        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_open_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_close_icon.png";

            if (val % 3 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 3 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 3 == 2)
            {
                img = imageBytes3;
            }


            return img;
        }
        //public ContentResult upgetreceiving(string DeliveryNo, DateTime DEPARTURE_ACTUAL_DT, DateTime ARRIVAL_ACTUAL_DT)
        //{
        //    IDBContext dataconn = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    int hasil = dataconn.Execute("GetDCLReceivingUpdt", new object[] { DeliveryNo, DEPARTURE_ACTUAL_DT, ARRIVAL_ACTUAL_DT });

        //    return Content(hasil.ToString());
        //}
        protected DCLReceivingDb GetReceiving(string DELIVERY_NO, string DOCK_CD)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            
            DCLReceivingDb QueryLog = db.SingleOrDefault<DCLReceivingDb>("GetDCLReceivingUpdt", new object[] { DELIVERY_NO, DOCK_CD });

            db.Close();
            return QueryLog;
        }
        protected string ModifReceiving(string DELIVERY_NO, string DOCK_CD)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            
            string QueryLog = db.ExecuteScalar<string>("GetDCLReceivingModif", new object[] { DELIVERY_NO, DOCK_CD, AuthorizedUser.Username });

            // Added by WOT, 15/06/2015 [start]
            db.Fetch<ScanGateReceivingDB>("SP_TC_InsertDeliveryKanbanRoom", new object[] { AuthorizedUser.Username, DOCK_CD, DELIVERY_NO }).FirstOrDefault();
            // Added by WOT, 15/06/2015 [end]

            db.Close();
            return QueryLog;
        }

        private static T Deserialize<T>(string e)
        {
            e = e.Replace("\0", "");
            XmlSerializer des = new XmlSerializer(typeof(T));
            T ret;
            using (StringReader s = new StringReader(e))
            {
                XmlReader reader = XmlReader.Create(s);
                ret = (T)des.Deserialize(reader);
            }
            return ret;
        }

        //private bool SendingDCLToSplitterMonitoring(DCLReceivingDb e)
        //{
        //    string ip = System.Configuration.ConfigurationManager.AppSettings["SplitterMonitorIP"];
        //    string port = System.Configuration.ConfigurationManager.AppSettings["SplitterMonitorPort"];
        //    if (!string.IsNullOrEmpty(ip) && !string.IsNullOrEmpty(port))
        //    {
        //        TcpClient client = new TcpClient(ip, Convert.ToInt32(port));
        //        Stream stream = client.GetStream();
        //        Crypter sym = new Crypter(new ConfigKeySym());
        //        XmlSerializer serial = new XmlSerializer(typeof(DCLReceivingDb));
        //        StringWriter streamW = new StringWriter();
        //        XmlWriter writer = XmlWriter.Create(streamW);
        //        serial.Serialize(writer, e);
        //        byte[] encrypt;
        //        string data = streamW.ToString();
        //        encrypt = ASCIIEncoding.ASCII.GetBytes(sym.Encrypt(data));
        //        client.SendBufferSize = encrypt.Length;
        //        stream.Write(encrypt, 0, encrypt.Length);
        //        client.Close(); 
        //        return true;
        //    }
        //    else
        //    {
        //        throw new ArgumentNullException("AppSettings \'SplitterMonitorIP\' and \'SplitterMonitorPort\' cannot be null."); 
        //    }
                
        //}

        [HttpPost]
        public ActionResult GetReceivingData(string txtDeliveryNo, string txtDockCode)
        {
            DCLReceivingDb QueryLogView = GetReceiving(txtDeliveryNo, txtDockCode); 
            return PartialView("DCLTruckArrivalPartial", QueryLogView);
        }
        public string ModifReceivingData(string txtDeliveryNo, string txtDockCode)
        {
            return ModifReceiving(txtDeliveryNo, txtDockCode);
            
        }
        public string CheckDeliveryData(string txtDeliveryNo, string txtDockCode)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string ResultQuery = "";
            try
            {
                ResultQuery = db.ExecuteScalar<string>("CheckDeliveryData", new object[] { txtDeliveryNo, txtDockCode });
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            db.Close();
            return (ResultQuery == null ? " " : ResultQuery);
        }

        protected List<DCLReceivingData> DCLget()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<DCLReceivingData> DCLListAdd = new List<DCLReceivingData>();
            var QueryLog = db.Query<DCLReceivingData>("GetDCLReceiving", new object[] { "", "" });
            int count = 0;
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    DCLListAdd.Add(new DCLReceivingData()
                    {
                       No = q.No,
                       RouteDate  = q.RouteDate,
                       Dock =q.Dock,
                       Station = q.Station,
                       RouteName = q.RouteName,
                       TripNo = q.TripNo,
                       ArrivalPlan = q.ArrivalPlan,
                       ActualArrival = q.ActualArrival,
                       Status = q.Status,
                       GAPARRV = q.GAPARRV,
                       DeparturePlan = q.DeparturePlan,
                       ActualDeparture = q.ActualDeparture,
                       StatusDept = q.StatusDept ,
                       GAPDEPT = q.GAPDEPT,
                       LPNAME = q.LPNAME,
                       Notif = SetNoticeIconString(count)
                       
                    });
                    count++;
                }
            }
            db.Close();
            return DCLListAdd;
        }
        public ActionResult PartialDetilRecevingMores(string DELIVERY_NO)
        {
            DCLReceivingDb DeliveryDetail = new DCLReceivingDb();
            DeliveryDetail.ARRIVAL_STATUS_IMAGE = "~/Content/Images/biggraydot2.jpg";
            DeliveryDetail.DEPARTURE_STATUS_IMAGE = "~/Content/Images/biggraydot2.jpg";
            
            return PartialView("DCLTruckArrivalPartial", DeliveryDetail);
        }
        public ActionResult DCLScreenLogisticPartner() 
        {
            return PartialView("DCLScreenLogisticPartner", Model); 
        } 

        public ActionResult PartialHeader()
        {
            return PartialView("HeaderDCLReceiving", Model);
        }
        public ActionResult PartialHeaderTop()
        {
            return PartialView("HeaderTopDCLReceiving", Model);
        }
        public ActionResult PartialHeaderRight()
        {
            return PartialView("HeaderRightDCLReceiving", Model);
        }

        public ActionResult DCLReceivingPartial()
        {
            return PartialView("DCLReceivingPartial", Model);
        }
        public ActionResult DCLScanBarcodePartial()
        {
            return PartialView("DCLScanBarcodePartial", Model);
        }
        public ActionResult DeliveryInformationDetail()
        {
            return PartialView("DeliveryInformationDetail", Model);
        }

        public ActionResult TimingInformationDetail()
        {
            return PartialView("TimingInformationDetail", Model);
        }
        public ActionResult TimingIndicatorDetail()
        {
            return PartialView("TimingIndicatorDetail", Model);
        }
        public ActionResult DCLScanBarcodeDetailPartial()
        {
            return PartialView("DCLScanBarcodeDetailPartial", Model);
        }
        public ActionResult DCLReceivingBarcodePartial()
        {
            return PartialView("DCLReceivingBarcodePartial", Model);
        }
        public ActionResult DCLDockandRate()
        {
            return PartialView("DCLDockandRate", Model);
        }

        public static SoundPlayer PlayWav(string val)
        {
            string locFile;
            string serverFile = "";
            switch (val.ToLower())
            {
                case "arradvance":
                    locFile = @"C:\tempIPPCS\ARV Advance.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/ARV Advance.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "arrdelay":
                    locFile = @"C:\tempIPPCS\ARV Delay.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/ARV Delay.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "arrontime":
                    locFile = @"C:\tempIPPCS\ARV OnTime.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/ARV OnTime.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "doublescan":
                    locFile = @"C:\tempIPPCS\Double Scan.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/Double Scan.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "depadvance":
                    locFile = @"C:\tempIPPCS\DPT Advance.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/DPT Advance.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "depdelay":
                    locFile = @"C:\tempIPPCS\DPT Delay.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/DPT Delay.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "depontime":
                    locFile = @"C:\tempIPPCS\DPT OnTime.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/DPT OnTime.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "failure":
                    locFile = @"C:\tempIPPCS\Failure.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/Failure.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "sirine":
                    locFile = @"C:\tempIPPCS\Sirine.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/Sirine.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                case "success":
                    locFile = @"C:\tempIPPCS\Success.wav";
                    serverFile = System.Web.HttpContext.Current.Server.MapPath("~/Content/Sounds/Success.mp3");
                    if (System.IO.File.Exists(locFile))
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(locFile);
                        return objSoundPlayer;
                    }
                    else
                    {
                        SoundPlayer objSoundPlayer = new SoundPlayer(serverFile);
                        return objSoundPlayer;
                    } 
                default:
                    throw new Exception("Sound File doesn't exist"); 
            }
        }
        public ActionResult GetFile()
        {
            string file = Server.MapPath("~/Content/Sounds/Success.wav");
            if (System.IO.File.Exists(file))
                return File("~/Content/Sounds/Success.wav", "audio/x-wav", "Success.wav");
            else
                return RedirectToAction("Index");
        }
        //public static void ForceDownload(this HttpResponse Response, string virtualPath, string fileName)
        //{
        //    Response.Clear();
        //    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
        //    Response.WriteFile(virtualPath);
        //    Response.ContentType = "";
        //    Response.End();
        //}

        //public string GetVisitorIpAddress()
        //{
        //    string stringIpAddress;
        //    stringIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (stringIpAddress == null) //may be the HTTP_X_FORWARDED_FOR is null
        //    {
        //        stringIpAddress = Request.ServerVariables["REMOTE_ADDR"];//we can use REMOTE_ADDR
        //    }
        //    return stringIpAddress;
        //}
        //public string GetLanIPAddress()
        //{
        //    //Get the Host Name
        //    string stringHostName = Dns.GetHostName();
        //    //Get The Ip Host Entry
        //    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
        //    //Get The Ip Address From The Ip Host Entry Address List
        //    IPAddress[] arrIpAddress = ipHostEntries.AddressList;
        //    return arrIpAddress[arrIpAddress.Length - 1].ToString();
        //}
    }
}
