using System;   
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Configuration;

namespace Portal.Controllers
{
    public interface ITick
    {
        int Level { get; set; }

        /// <summary>
        /// Base operation: Say,Lip,In,Out method call Op 
        /// </summary>
        /// <param name="op">-1 Say 0 In 1 Out</param>
        /// <param name="x"></param>
        /// <returns></returns>
        string Op(int op = 0, string x = null);
        void Trace(string w);

        /// <summary>
        ///  walk In - indent 
        /// </summary>
        /// <param name="w"></param>
        /// <param name="x"></param>
        void In(string w, params object[] x);
        

        /// <summary>
        /// walk Out dedent previous (multi) In 
        /// </summary>
        /// <param name="w"></param>
        /// <param name="x"></param>
        void Out(string w = null, params object[] x);

        /// <summary>
        /// Say something
        /// </summary>
        /// <param name="w"></param>
        /// <param name="x"></param>
        void Say(string w, params object[] x);
        
        /// <summary>
        /// List Parameter, behave the same as Say, but with param list and method name
        /// </summary>
        /// <param name="w">LIst Parameter: 
        /// format: MethodName params1,params2,...,paramsN
        /// output:
        ///     MethodName
        ///         params1: value1
        ///         params2: value2
        ///         ...
        ///         paramsN: valueN
        /// </param>
        /// <param name="x">list values to match parameter name</param>
        void Lip(string w, params object[] x);
        
    }

    public class InOutTick : ITick
    {
        private string logPath;
        private string logFile;
        private bool initdone = false;
        private string logName;

        public InOutTick(string name = "Tick")
        {
            initdone = false;
            logName = name;
        }

        private void Init(string name)
        {
            DateTime d = DateTime.Now;
            logName = name;
            logPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
                    , ConfigurationManager.AppSettings["LdapDomain"]
                    , ConfigurationManager.AppSettings["DeploymentContext"] 
                    , d.Year.ToString(),  d.Month.ToString("00"), d.Day.ToString("00"));
            if (!Directory.Exists(logPath)) 
            {
                Directory.CreateDirectory(logPath); 
            }
            logFile = Path.Combine(logPath, logName + ".txt");
            
            initdone = true;
        }

        private string LogFile
        {
            get
            {
                if (!initdone)
                {
                    Init(logName);
                }
                return logFile;
            }
        }
        private readonly string[] wop = new string[] { "    ", "In  ", "Out " };

        private DateTime LastLog = new DateTime(1753, 12, 31);
        private int Log(string w, int Op = -1)
        {

            DateTime d = DateTime.Now;
            string ts = new string(' ', 9);           

            if (Math.Floor((d - LastLog).TotalSeconds) > 0)
            {
                LastLog = d;
                ts = d.ToString("HH:mm:ss ");
            }

            File.AppendAllText(LogFile,
                ts
                + wop[Op + 1]
                + Tab(level) + w
                + "\r\n");
            return 0;
        }

        private int pid = 0;

        private Stack<string> stak = null;
        private Stack<string> St
        {
            get
            {
                if (stak == null)
                {
                    stak = new Stack<string>();
                }
                return stak;
            }
        }

        private int level = 0;
        public int Level
        {
            get
            {
                return level;
            }

            set
            {
                level = value;
            }
        }

        private static string Tab(int n)
        {
            return new string(' ', 4*n);
        }

        public void In(string w, params object[] x)
        {
            Op(0, string.Format(w, x));
        }

        public void Out(string w, params object[] x)
        {
            if (!string.IsNullOrEmpty(w))
            {
                w = string.Format(w, x);
                if (St.Contains(w))
                {
                    string y = null;
                    do
                    {
                        y = Op(1, w);
                    }
                    while (!y.Equals(x) && St.Count > 0);
                }
            }
            else
            {
                Op(1, w);
            }
        }

        public void Say(string w, params object[] x)
        {
            if (!initdone)
            {
                Init(w);
                return;
            }           

            Op(-1, string.Format(w, x));
        }

        public void Lip(string w, params object[] x)
        {
            int j = w.IndexOf(" ");
            string n = "";
            if (j > 1 )
            {
                n = w.Substring(0, j);
                w = w.Substring(j + 1, w.Length - (j+1));
            }
            string [] a = w.Split(',');

            StringBuilder b = new StringBuilder(n);
            b.Append("\r\n");

            j = x.Length;
            int k = a.Length;
            if (k > j) 
                 k = j;

            for (int i = 0; i < k; i++)
            {
                b.Append(Tab(3));
                b.Append(a[i]);
                b.Append(": ");
                if (x[i] is string)
                {
                    b.Append("\"" + x[i] + "\"");
                }
                else if (x[i] is DateTime)
                {
                    DateTime ? t = x[i] as DateTime?;
                    if (t != null)
                        b.Append(t.Value.ToString("yyyy.MM.dd HH:mm:ss.ttt"));
                }
                else 
                {
                    b.Append(x[i]);
                }
                b.Append("\r\n");
            }
            
            Op(-1, b.ToString());
        }

        public string Op(int op = 0, string x = null)
        {
            string z = "";
            if (op == 0)
            {
                St.Push(x);
                z = x;

                pid = Log(x, 0);
                level++;

            }
            else if (op > 0)
            {
                if (St.Count > 0)
                    z = St.Pop();
                else
                    z = x;
                level--;
                Log(z, 1);
            }
            else
            {
                Log(x);
            }
            return z;
        }


        public void Trace(string w)
        {
            Log(w + "\r\n" + StackTracer.Get(w));
        }
    }


    public static class StackTracer
    {
        public static string Get(string w)
        {
            StackTrace t = new StackTrace(1, true);

            StackFrame[] f = t.GetFrames();
            StringBuilder sfs = new StringBuilder("\r\n");
            for (int i = 0; i < f.Length; i++)
            {
                StackFrame sf = f[i];
                System.Reflection.MethodBase m = sf.GetMethod();

                string cname = m.ReflectedType.FullName;
                if (cname.Contains(".InOutMark") || cname.Contains(".ITick") || cname.Contains(".Ticker") || cname.Contains("StackTracer"))
                {
                    continue;
                }

                sfs.AppendFormat("{0,4} {1} {2} \r\n", sf.GetFileLineNumber(), cname, m.Name);
            }
            return sfs.ToString();
        }
    }


    public class IdleNoTick : ITick
    {
        public int Level
        {
            get
            {
                return 0;
            }
            set
            {
                
            }
        }

        public string Op(int op = 0, string x = null)
        {
            return null;
        }

        public void Trace(string w)
        {
            
        }

        public void In(string w, params object[] x)
        {
            
        }

        public void Out([System.Runtime.InteropServices.OptionalAttribute][System.Runtime.InteropServices.DefaultParameterValueAttribute(null)]string w, params object[] x)
        {
            
        }

        public void Say(string w, params object[] x)
        {
            
        }

        public void Lip(string w, params object[] x)
        {
            
        }
    }
}
