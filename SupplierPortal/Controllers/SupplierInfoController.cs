﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.SupplierInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using Portal.Models.Dock;
namespace Portal.Controllers
{

    public class SupplierInfoController : BaseController
    {
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        /// <summary>
        /// If the current response is a FileResult (an MVC base class for files) then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }
       

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }


        List<Supplier> _supplierModel = null;
        private List<Supplier> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<Supplier>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<PartData> _partModel = null;
        private List<PartData> _PartModel
        {
            get
            {
                if (_partModel == null)
                    _partModel = new List<PartData>();

                return _partModel;
            }
            set
            {
                _partModel = value;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/SupplierInfo/UploadTemp/");
                return _uploadDirectory;
            }
        }


        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private Int32 _processId = 0;
        private Int32 ProcessId
        {
            get
            {
                if (_processId == 0)
                    _processId = Math.Abs(Environment.TickCount);

                return _processId;
            }
        }

        

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();

            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = DbContext.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            finally
            {
                DbContext.Close();
            }

            return String.Join(",", columnNameArray.ToArray());
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        public SupplierInfoController()
            : base("Supplier Info Master Maintenance Screen")
        {
            CookieName = "fileDownload";
            CookiePath = "/";
         
        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            IDBContext db = DbContext;

            SupplierInfoModel modelSuppInfo = new SupplierInfoModel();
            //modelSuppInfo.SupplierDatas = getSuppInfo("", "");
            //Model.AddModel(GetAllSupplier());
            Model.AddModel(modelSuppInfo);
           // ViewData["GridSupplier"] = GetAllSupplier();
            ViewData["SupplierGridLookup"] = GetAllSupplier();
            ViewData["DockGridLookup"] = GetAllDock();
            ViewData["PartGridLookup"] = GetAllPart("", "", "", "");
            //List<SupplierName> supplierModel = new List<SupplierName>();
            //supplierModel = DbContext.Fetch<SupplierName>("GetSupplierCombobox", new object[] { });
            //ViewData["GridSupplier"] = supplierModel;

            //List<PartData> partModel = new List<PartData>();
            //partModel = DbContext.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
            //ViewData["GridPart"] = partModel;

            //List<SupplierName> supplierModel = new List<SupplierName>();
            //supplierModel = db.Fetch<SupplierName>("GetSupplierCombobox", new object[] { });
           // ViewData["GridSupplier"] = supplierModel;
            //DbContext.Close();
        }

        public JsonResult GetPartDescription(string PartNo, string SupplierCode, string SupplierPlantCode, string DockCode)
        {
            List<SupplierPart> supplierPart = new List<SupplierPart>();

            try
            {
                supplierPart = DbContext.Query<SupplierPart>("GetPartDescription", new object[] { PartNo, SupplierCode, SupplierPlantCode, DockCode }).ToList();
            }
            catch (Exception exc) { throw exc; }
            finally { DbContext.Close(); }

            //return new JsonResult
            //{
            //    Data = new
            //    {
            //        PartNo = supplierPart.PART_NO,
            //        PartName = supplierPart.PART_NAME,
            //        KanbanNo = supplierPart.KANBAN_NO,
            //        PackPerContainer = supplierPart.PACK_PER_CONTAINER
            //    }
            //};
            return Json(supplierPart);
        }
    
        //string vSupCode = "";
            //string vSupPlantCd = "";
            //if (suppCdParam != "")
            //{
            //    string result = string.Empty;
            //    char[] splitStr = { '-' };
            //    string[] supps = suppCdParam.Split(splitStr);
            //    vSupCode = supps[0].Trim();
            //    vSupPlantCd = supps[1].Trim();
            //}
        protected List<SupplierInfoData> getSuppInfo(string suppCdParam, string partNoParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierInfoData> listTooltipDatas = new List<SupplierInfoData>();
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierInfo", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "EmergencyOrderCreation", "EOCHDockGridLookup");

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }
            listTooltipDatas = db.Query<SupplierInfoData>("GetSupplierInfo", new object[] { suppCdParam, partNoParam, SupplierCodeLDAP, DockCodeLDAP }).ToList();
          
            //listTooltipDatas = db.Query<SupplierInfoData>("GetSupplierInfo", new object[] { vSupCode, vSupPlantCd, partNoParam }).ToList();
             db.Close();
            //if (queryLog.Any())
            //{
            //    foreach (var q in queryLog)
            //    {
            //        listTooltipDatas.Add(new SupplierInfoData
            //        {
            //            SupplierCode = q.SupplierCode ,
            //            SupplierPlantCd = q.SupplierPlantCd,
            //            SupplierName=q.SupplierName,
            //            PartNo = q.PartNo,
            //            PartName=q.PartName,
            //            DockCode = q.DockCode,
            //            SpInfo1 = q.SpInfo1,
            //            SpInfo2 = q.SpInfo2,
            //            SpInfo3 = q.SpInfo3,
            //            PrintCode = q.PrintCode
            //        });
            //    }

            //}
           // db.Close();
            return listTooltipDatas;
        }

        public ActionResult PartialSupplierDatas()
        {
            string SuppCd = String.IsNullOrEmpty(Request.Params["SupplierCode1"]) ? "" : Request.Params["SupplierCode1"];
            string PartNo =  String.IsNullOrEmpty(Request.Params["PartNo1"]) ? "" : Request.Params["PartNo1"];

            SupplierInfoModel model = Model.GetModel<SupplierInfoModel>();
            model.SupplierDatas = getSuppInfo(SuppCd,  PartNo.Replace("-",""));

            Model.AddModel(model);

            return PartialView("PartialSupplierInfoData", Model);
        }


          public ActionResult PartialHeader()
        {
            #region Required model in partial page : for SupplierInfoHeaderPartial
            ViewData["GridSupplier"] = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierInfo", "OIHSupplierOption");
            //GetAllSupplier();
             ViewData["GridPart"] = GetAllPart();

            //Model.AddModel(_SupplierModel);
            //Model.AddModel(_PartModel);
            #endregion

             return PartialView("PartialSupplierInfoHeader", Model);
        }

        public ActionResult PartialButton()
        {
            return PartialView("PartialSupplierInfoDataButtonUpGrid", Model);
        }

        #region Combobox Header Supplier
        public ActionResult PartialHeaderSupplierLookupGridSupplierInfo()
        {
            //IDBContext db = DbContext;

            //TempData["GridName"] = "OIHSupplierOption";
            //List<Supplier> supplierModel = new List<Supplier>();
            //supplierModel = DbContext.Fetch<Supplier>("GetAllSupplierGrid", new object[] { });
            //ViewData["GridSupplier"] = supplierModel;

            //db.Close();

            //return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);

            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierInfo", "OIHSupplierOption");

            TempData["GridName"] = "OIHSupplierOption";
            _supplierModel = suppliers;
           return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }


        private List<Supplier> GetAllSupplier()
        {
            List<Supplier> SISupplierModel = new List<Supplier>();
            IDBContext db = DbContext;
            SISupplierModel = db.Fetch<Supplier>("GetAllSupplierGrid", new object[] { });            
            db.Close();

            return SISupplierModel;
        }
        //private List<Supplier> GetAllSupplier()
        //{

        //    IDBContext db = DbContext;

        //     List<Supplier> supplierModelList = new List<Supplier>();
            

        //    try
        //    {
        //        supplierModelList = DbContext.Fetch<Supplier>("GetAllSupplierGrid", new object[] { "" });
        //    }
        //    catch (Exception exc) { throw exc; }
        //    db.Close();

        //    return supplierModelList;
        //}

        //private List<SupplierName> GetAllSupplier()
        //{
        //    List<SupplierName> supplierModel = new List<SupplierName>();
        //    supplierModel = DbContext.Fetch<SupplierName>("GetSupplierCombobox", new object[] { });
        //    DbContext.Close();

        //    return supplierModel;
        //}

        public PartialViewResult PartialComboBoxGrid()
        {
            
            IDBContext db = DbContext;

            List<Part> partModel = new List<Part>();
            partModel = DbContext.Fetch<Part>("GetAllSupplierPartGrid", new object[] { "", "", "", "" });
            ViewData["GridPart"] = partModel;

            db.Close();

            return PartialView("PartialComboBoxGrid", ViewData["GridPart"]);
        }
        #endregion

        private List<Part> GetAllPart(string SupplierCode, string SupplierPlant, string DockCode, string KanbanNo)
        {
            List<Part> partModelList = new List<Part>();

            try
            {
                //partModelList = DbContext.Fetch<Part>("GetAllPartNoGrid", new object[] { SupplierCode, SupplierPlant, DockCode });

                partModelList = DbContext.Fetch<Part>("GetAllSupplierPartGrid", new object[] { SupplierCode, SupplierPlant, DockCode, KanbanNo });
            }
            catch (Exception exc) { throw exc; }
            finally { DbContext.Close(); }

            return partModelList;
        }

        public ActionResult PartialPartGridLookup()
        {
            #region Required model in partial page : for grlPartNo
            TempData["GridName"] = "grlPartNo";
            ViewData["PartGridLookup"] = GetAllPart(
                    Request.Params["SupplierCode"],
                    Request.Params["SupplierPlant"],
                    Request.Params["DockCode"],
                    Request.Params["KanbanNo"]);
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["PartGridLookup"]);
        }

        public ActionResult PartialHeaderDockGridLookupEmergencyOrderCreation()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "SICHDockGridLookup";
            ViewData["DockGridLookup"] = GetAllDock();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["DockGridLookup"]);
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> PartIndoDockModel = new List<DockData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return PartIndoDockModel;
        }
       
        #region Combobox Header Part
        public ActionResult PartialHeaderPartLookupGridSupplierInfo()
        {
            IDBContext db = DbContext;

            TempData["GridName"] = "OIHPartOption";
            List<PartData> partModel = new List<PartData>();
            partModel = db.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
            ViewData["GridPart"] = partModel;

            db.Close();

            return PartialView("GridLookup/PartialGrid", ViewData["GridPart"]);
        }

        public PartialViewResult PartialGLPart()
        {
            IDBContext db = DbContext;

            TempData["GridName"] = "glPartNo";
            List<PartData> partModel = new List<PartData>();
            partModel = db.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
            ViewData["GridPart"] = partModel;

            db.Close();

            return PartialView("GridLookup/PartialGrid", ViewData["GridPart"]);
        }
        private List<PartData> GetAllPart()
        {
            IDBContext db = DbContext;

            List<PartData> partModel = new List<PartData>();
            partModel = db.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
            db.Close();

            return partModel;
        }
        #endregion

        public ActionResult PartialHeaderSupplierGridLookup()
        {
            //#region Required model in partial page : for grlSuppCd
            //TempData["GridName"] = "grlSuppCd";
            //ViewData["SupplierGridLookup"] = GetAllSupplier();
            //#endregion

            //return PartialView("GridLookup/PartialGrid", ViewData["SupplierGridLookup"]);

            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierInfo", "grlSuppCd");

            TempData["GridName"] = "grlSuppCd";
            _supplierModel = suppliers;
            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }


        //public ActionResult SupplierCodePartialLookupForm()
        //{
        //    try
        //    {
        //        TempData["GridName"] = "grlSuppCd";
        //        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

        //        var QueryPlantDock = db.Query<SupplierName>("GetSupplierCombobox");

        //        db.Close();
        //        ViewData["GridSupplier"] = QueryPlantDock;
        //    }
        //    catch (Exception explantcode)
        //    {
        //        Console.WriteLine(explantcode.Message + explantcode.Source + explantcode.StackTrace + explantcode.TargetSite);
        //        return RedirectToAction("ShowError", explantcode.Message);
        //    }
        //    return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);
        //}

        public ActionResult AddNewPart(string Supplier, string DockCode, string PartNo, string SpInfo1, string SpInfo2, string SpInfo3, string PrintCd)
        {
            string ResultQuery = "Process succesfully";
            string[] Suppliers = Supplier.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");

            if (SpInfo1 == "") { SpInfo1 = null; }
            if (SpInfo2 == "") { SpInfo2 = null; }
            if (SpInfo3 == "") { SpInfo3 = null; }
            if (PrintCd == "") { PrintCd= null; }
           IDBContext db = DbContext;
            try
            {
                db.Execute("InsertSupplierInfo", new object[] 
                {
                   SupplierCode,
                   SupplierPlant,
                   PartNo,
                   DockCode,
                   SpInfo1,
                   SpInfo2,
                   SpInfo3,
                   PrintCd,
                   AuthorizedUser.Username
                });
                TempData["GridName"] = "grlSuppCd";
               db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
         
        [HttpPost]
        public void SIFileUpload_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("SIFileUpload", ValidationSettings, SIFileUploadConfirmation_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void SIFileUploadConfirmation_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Math.Abs(System.Environment.TickCount) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        #region UPLOAD CONFIRMATION - ACTION
        [HttpPost]
        public ActionResult HeaderTabSIConfirmation_Upload(string filePath)
        {
            ProcessAllSIConfirmationUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }
        #endregion

        [HttpPost]
        public void ClearAllFeedbackPartUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            try
            {
                // clear data in temporary uploaded table.
                DbContext.Execute("ClearFeedbackPartUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception exc)
            {
                throw new Exception();
            }
            finally { DbContext.Close(); }
        }


        #region UPLOAD CONFIRMATION - RESULT
        public void UploadValid(String processId, String functionId, String temporaryTableValidate)
        {
            DbContext.ExecuteScalar<string>("UpdateSupplierInfoUpload",
                            new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            AuthorizedUser.Username,
                            Session.SessionID
                        });

            DbContext.Close();
        }

        [HttpGet]
        public void DownloadInvalid(String processId, String functionId, String temporaryTableUpload)
        {

            string fileName = "SupplierInfoUploadInvalid" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportSupplierInfoCreationUploadError> qry = db.Query<ReportSupplierInfoCreationUploadError>("ReportGetSupplierInfoUploadError", new object[] { functionId, processId });

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(qry, "SupplierInfoInvalid");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }


        [HttpGet]
        public void DownloadSheet(string suppCdParam, string partNoParam)
        {

            string fileName = "SupplierInfo" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";

            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierInfo", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "EmergencyOrderCreation", "EOCHDockGridLookup");

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }
            IEnumerable<SupplierInfoData> listGeneral = db.Query<SupplierInfoData>("GetSupplierInfo", new object[] { suppCdParam, partNoParam, SupplierCodeLDAP, DockCodeLDAP });

            IExcelWriter exporter = ExcelWriter.GetInstance();
            exporter.Append(listGeneral, "PartInfo");
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();
        }


        public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/SupplierInfo/UploadTemp/SupplierInfo_Template.xls"));
          byte[] documentBytes = StreamFile(filePath);
          return File(documentBytes, "application/vnd.ms-excel", "SupplierInfo_Template.xls");
            
          //Response.Clear();
          //Response.ClearContent();
          //Response.ClearHeaders();
          //Response.ContentType = "application/vnd.ms-excel";
          //Response.Cache.SetCacheability(HttpCacheability.Private);
          //Response.Expires = 0;
          //Response.Buffer = true;

          //Response.AddHeader("Content-Disposition", "SupplierInfo_Template.xls");
          //Response.BinaryWrite(documentBytes);
          //Response.Flush(); Response.End();     
            //string fileName = "SupplierInfo_Template.xls";

            //// Update download user and status
            //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ////IEnumerable<ReportGetAllEmergencyOrderPartModel> qry = db.Fetch<ReportGetAllEmergencyOrderPartModel>("ReportGetAllFeedbackPart", new object[] { arrivalTime, supplierCode, supplierPlantCode, dockCode});
            //DataTable dt = new DataTable();
            //dt.Columns.Add("Supplier Code", typeof(string));
            //dt.Columns.Add("Supplier Plant", typeof(string));
            //dt.Columns.Add("Dock Code", typeof(string));
            //dt.Columns.Add("Part Number", typeof(string));
            //dt.Columns.Add("SP Info 1", typeof(string));
            //dt.Columns.Add("SP Info 2", typeof(string));
            //dt.Columns.Add("SP Info 3", typeof(string));
            //dt.Columns.Add("Print Code", typeof(string));
            //IExcelWriter exporter = new NPOIWriter();
            
            //byte[] hasil = exporter.Write(dt, "SupplierInfo");
            //Response.Clear();
            ////Response.ContentType = result.MimeType;
            //Response.Cache.SetCacheability(HttpCacheability.Private);
            //Response.Expires = -1;
            //Response.Buffer = true;

            //Response.ContentType = "application/octet-stream";
            //Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            //Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            //Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            //Response.BinaryWrite(hasil);
            //Response.End();
        }

        private SupplierInfoModel ProcessAllSIConfirmationUpload(string filePath)
        {
            SupplierInfoModel _UploadedSupplierInfoModel = new SupplierInfoModel();

            // table destination properties
            _TableDestination.functionID = "61001";
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "SupplierInfo";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "46";
            _TableDestination.tableFromTemp = "TB_T_SUPPLIER_INFO_UPLOAD";
            _TableDestination.tableToAccess = "TB_T_SUPPLIER_INFO_VALIDATE";

            LogProvider logProvider = new LogProvider("61001", "6", AuthorizedUser);
            ILogSession sessionLPOP = logProvider.CreateSession();

            OleDbConnection excelConn = null;

            try
            {
                sessionLPOP.Write("MPCS00002INF", new object[] { "Process Upload has been started" });

                // clear temporary upload table.
                DbContext.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableFromTemp });
                DbContext.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties = Excel 8.0";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                excelCommand = new OleDbCommand(@"select " +
                                    _TableDestination.functionID + " as FUNCTION_ID, " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @" *
                                from [" + _TableDestination.sheetName + "$]", excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");

                    bulkCopy.ColumnMappings.Add("Supplier Code", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("Supplier Plant", "SUPPLIER_PLANT_CD");
                    bulkCopy.ColumnMappings.Add("Part Number", "PART_NO");
                    bulkCopy.ColumnMappings.Add("Dock Code", "DOCK_CODE");
                    bulkCopy.ColumnMappings.Add("SP Info 1", "SP_INFO1");
                    bulkCopy.ColumnMappings.Add("SP Info 2", "SP_INFO2");
                    bulkCopy.ColumnMappings.Add("SP Info 3", "SP_INFO3");
                    bulkCopy.ColumnMappings.Add("Print Code", "PRINT_CD");
                #endregion
                    
                    try
                    {
                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        sessionLPOP.Write("MPCS00003ERR", new string[] { ex.Source, ex.Message, "", "" });
                        Console.WriteLine(ex.Message);
                    }
                }

                String columnNameList = GetColumnName();

                // validate data in temporary uploaded table.
                DbContext.Execute("ValidateSupplierInfoUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });

                // check for invalid data.
                try
                {
                    _ErrorValidationList = DbContext.Fetch<ErrorValidation>("GetErrorMessageUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });
                }
                catch (Exception exc)
                {
                    sessionLPOP.Write("MPCS00003ERR", new string[] { exc.Source, exc.Message, "", "" });
                    throw exc;
                }

                if (_ErrorValidationList.Count == 0)
                {
                    // insert data from temporary uploaded table into temporary validated table.
                    DbContext.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] { _TableDestination.tableToAccess, columnNameList, columnNameList, _TableDestination.tableFromTemp });

                    // read uploaded data from temporary validated table to transcation table.
                    //_UploadedLPOPConsolidationModel = GetAllLPOPConsolidationUpload(productionMonth, version, _TableDestination.processID, _TableDestination.functionID);

                    // set process validation status.
                    TempData["IsValid"] = "true;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    sessionLPOP.Write("MPCS00003INF", new object[] { @"Insert upload success info into table log detail.
                                                                        Insert data from temporary uploaded table into temporary validated table.
                                                                        Read uploaded data from temporary validated table to transcation table.
                                                                        set process validation status...!!!" });
                }
                else
                {

                    // load previous data for supplier feedback part model
                    //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail = GetAllLPOPConsolidationUpload(productionMonth, version);

                    // set process validation status.
                    TempData["IsValid"] = "false;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;

                    sessionLPOP.Write("MPCS00003ERR", new object[] { @"Insert upload fail info into table log detail
                                                                        set process validation status...!!!" });
                }
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                sessionLPOP.Write("MPCS00003ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });

                // load previous data for supplier feedback part model
                //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail= GetAllFeedbackPart(productionMonth, supplierCode, supplierPlanCode, version);
            }
            finally
            {
                sessionLPOP.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                sessionLPOP.Commit();
                DbContext.Close();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }

            return _UploadedSupplierInfoModel;
        }

        #region CRUD
        public ActionResult OnUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] SupplierInfoData supplierInfo)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User user = SessionState.GetAuthorizedUser();
                    string partNo = (string)ViewData["PartNo"];
                    IDBContext db = DbContext;
                    db.Execute("UpdateSupplierInfo", new object[] {               
                        supplierInfo.SupplierCode, 
                        supplierInfo.SupplierPlantCd,
                        supplierInfo.PartNo,
                        supplierInfo.DockCode,
                        supplierInfo.SpInfo1,
                        supplierInfo.SpInfo2,
                        supplierInfo.SpInfo3,
                        supplierInfo.PrintCode
                    });
                    db.Close();
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = ex.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";
                ViewData["SupplierTable"] = supplierInfo;
            }

            SupplierInfoModel modelSuppInfo = new SupplierInfoModel();
            modelSuppInfo.SupplierDatas = getSuppInfo("", "");
            Model.AddModel(GetAllSupplier());
            Model.AddModel(modelSuppInfo); ;

            return PartialView("PartialSupplierInfoData", Model);
        }

        public ActionResult OnInsert([ModelBinder(typeof(DevExpressEditorsBinder))] SupplierInfoData supplierInfo)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User user = SessionState.GetAuthorizedUser();
                    IDBContext db = DbContext;
                    db.Execute("InsertSupplierInfo", new object[] {               
                        supplierInfo.SupplierCode, 
                        supplierInfo.SupplierPlantCd,
                        supplierInfo.PartNo,
                        supplierInfo.DockCode,
                        supplierInfo.SpInfo1,
                        supplierInfo.SpInfo2,
                        supplierInfo.SpInfo3,
                        supplierInfo.PrintCode
                    });
                    db.Close();
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = ex.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";
                ViewData["SupplierTable"] = supplierInfo;
            }

            SupplierInfoModel modelSuppInfo = new SupplierInfoModel();
            modelSuppInfo.SupplierDatas = getSuppInfo("", "");
            Model.AddModel(GetAllSupplier());
            Model.AddModel(modelSuppInfo); ;

            return PartialView("PartialSupplierInfoData", Model);
        }

        public ActionResult OnDelete([ModelBinder(typeof(DevExpressEditorsBinder))] SupplierInfoData supplierInfo)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User user = SessionState.GetAuthorizedUser();
                    IDBContext db = DbContext;
                    db.Execute("DeleteSupplierInfo", new object[] {               
                        supplierInfo.SupplierCode, 
                        supplierInfo.SupplierPlantCd,
                        supplierInfo.PartNo,
                        supplierInfo.DockCode,
                        supplierInfo.SpInfo1,
                        supplierInfo.SpInfo2,
                        supplierInfo.SpInfo3,
                        supplierInfo.PrintCode
                    });
                    db.Close();
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = ex.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";
                ViewData["SupplierTable"] = supplierInfo;
            }

            SupplierInfoModel modelSuppInfo = new SupplierInfoModel();
            modelSuppInfo.SupplierDatas = getSuppInfo("", "");
            Model.AddModel(GetAllSupplier());
            Model.AddModel(modelSuppInfo); ;

            return PartialView("PartialSupplierInfoData", Model);
        }

        public ActionResult OnBulkDelete(string gridKeyField)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string[] supplierinfo = gridKeyField.Split(new string[] {";"}, StringSplitOptions.RemoveEmptyEntries);
            string[] supplierinfopart;

            for (int i = 0; i < supplierinfo.Length; i++) {
                supplierinfopart = supplierinfo[i].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);


                db.Execute("DeleteSupplierInfo", new object[] {               
                        supplierinfopart[0], 
                        supplierinfopart[1],
                        supplierinfopart[3].Replace("-",""),
                        supplierinfopart[2]
                    });

            }

                return PartialView("PartialSupplierInfoData", Model);
        }
        #endregion
    }
}
