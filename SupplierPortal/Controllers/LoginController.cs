﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC.Login;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.Database;
using Cryptography;
using Toyota.Common.Web.Util;

namespace Portal.Controllers
{
    public class LoginController : BaseLoginController
    {
        public LoginController()
        {
            CloseWindowAfterLogout = true;
            SupportSingleSignOn = true;
        }

        public override ActionResult Index()
        {
            ViewData["Title"] = "Login";
            ViewData["Section.Partner.Title"] = "All Highlander models feature a standard 50/50 split third-row seat and rear climate control. Generous standard features on the Highlander grade include front and rear air conditioning; eight-way adjustable driver’s seat; power door locks and windows with driver's window jam protection and auto-up/down feature;";
            
            return base.Index();
        }       

        protected override void StartUp()
        {
        }

        protected override void Init()
        {            
        }
    }
}
