﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.GetsudoVehicle;

namespace Portal.Controllers
{
    public class GetsudoVehicleController : BaseController
    {
        int i = 0;
        public GetsudoVehicleController()
            : base("Upload Production Planning")
        {
            
        }
        
        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            GetsudoVehicleModel getsudoMdl = new GetsudoVehicleModel();
            List<GetsudoVehicleDetail> dtlGetsudo = new List<GetsudoVehicleDetail>();
            for (; i < 4; i++)
            {
                if (i == 1)
                {
                    dtlGetsudo.Add(new GetsudoVehicleDetail()
                     {
                         No = i,
                         ProductionMonth = Convert.ToDateTime("1/1/2012"),
                         ProductionType = "Firm",
                         Status = "Downloaded",
                         CreatedBy = "userPCD1",
                         CreatedDate = DateTime.Now,
                         Download = ""
                     });
                }
                else if (i == 2)
                {
                    dtlGetsudo.Add(new GetsudoVehicleDetail()
                    {
                        No = i,
                        ProductionMonth = Convert.ToDateTime("2/1/2012"),
                        ProductionType = "Firm",
                        Status = "Not Downloaded",
                        CreatedBy = "userPCD2",
                        CreatedDate = DateTime.Now,
                        Download = ""
                    });
                }
                else if (i == 3)
                {
                    dtlGetsudo.Add(new GetsudoVehicleDetail()
                    {
                        No = i,
                        ProductionMonth = Convert.ToDateTime("3/1/2012"),
                        ProductionType = "Firm",
                        Status = "Not Downloaded",
                        CreatedBy = "userPCD3",
                        CreatedDate = DateTime.Now,
                        Download = ""
                    });
                }
            }
            getsudoMdl.GetsudoVehicleDetails=dtlGetsudo;
            Model.AddModel(getsudoMdl);
         
        }

        public ActionResult ReloadGrid(string cbProdMonth, string cbProdPlanType, string upSelectFile)
        {
            string monthInput = (Convert.ToDateTime(cbProdMonth.Substring(4, 20)).ToString("MM"));
            string yearInput = (Convert.ToDateTime(cbProdMonth.Substring(0, 20)).ToString("yyyy"));
            if (DateTime.Today.Year <= Convert.ToInt32(yearInput))
            {
                if (DateTime.Today.Month <= Convert.ToInt32(monthInput))
                {
                    GetsudoVehicleModel getsudoMdl = Model.GetModel<GetsudoVehicleModel>();
                    List<GetsudoVehicleDetail> dtlGetsudo = getsudoMdl.GetsudoVehicleDetails;

                    dtlGetsudo.Add(new GetsudoVehicleDetail()
                    {
                        No = i,
                        ProductionMonth = Convert.ToDateTime(cbProdMonth.Substring(4, 20)),
                        ProductionType = cbProdPlanType,
                        Status = "Not Downloaded",
                        CreatedBy = "userPCD" + i,
                        CreatedDate = DateTime.Now,
                    });
                }
                else
                {
                    MessageStack.Push(ScreenMessage.CreateError("Month Error", null));
                }
            }

            else
            {
                MessageStack.Push(ScreenMessage.CreateError("Month Error", null));
            }
            //return PartialView("GetsudoVehicleGrid", Model);
            return null;
        }

        public ActionResult PartialHeaderGetSudoVehicle()
        {
            return PartialView("HeaderGetSudoVehicle", Model);
        }
    }
}
