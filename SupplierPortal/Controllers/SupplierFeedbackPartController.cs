﻿using System;
using System.Runtime.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Threading;
using System.Data;
using System.Xml;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Excel;
using Portal.Models.SupplierFeedbackPart;
using Portal.ExcelUpload;
using Portal.Controllers.Upload;
using System.Data.OleDb;
using System.Data.SqlClient;
using Portal.Models.Forecast;
using Toyota.Common.Web.Upload;
using Toyota.Common.Web.MessageBoard;
using System.Diagnostics;
using Toyota.Common.Web.Log;

using Toyota.Common.Web.FTP;
using Portal.Models;
using System.Reflection;

namespace Portal.Controllers
{
    public class SupplierFeedbackPartController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        //private IDBContext DbContextLdap
        //{
        //    get
        //    {
        //        return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_LDAP_OPEN);
        //    }
        //}

        public SupplierFeedbackPartController()
            : base("Supplier Feedback Input Screen")
        {
        }

        #region Model properties
        SupplierFeedbackPartModel _supplierFeedbackPartModel = null;
        private SupplierFeedbackPartModel _SupplierFeedbackPartModel
        {
            get
            {
                if (_supplierFeedbackPartModel == null)
                    _supplierFeedbackPartModel = new SupplierFeedbackPartModel();

                return _supplierFeedbackPartModel;
            }
            set
            {
                _supplierFeedbackPartModel = value;
            }
        }

        SupplierFeedbackPartParameter _supplierFeedbackPartParameterModel = null;
        private SupplierFeedbackPartParameter _SupplierFeedbackPartParameterModel
        {
            get
            {
                if (_supplierFeedbackPartParameterModel == null)
                    _supplierFeedbackPartParameterModel = new SupplierFeedbackPartParameter();

                return _supplierFeedbackPartParameterModel;
            }
            set
            {
                _supplierFeedbackPartParameterModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
        }

        #region Supplier Feedback Part controller
        #region View controller
        public ActionResult PartialFooterSupplierFeedbackPartForecast()
        {
            #region Required model in partial page
            #region Model for SupplierFeedbackPartFooterPartial_Forecast

            #endregion
            #endregion

            return PartialView("SupplierFeedbackPartFooterPartial_Forecast", Model);
        }

        public ActionResult PartialGridSupplierFeedbackPartForecast()
        {
            #region Required model in partial page
            #region Model for SupplierFeedbackPartGridPartial_Forecast
            _SupplierFeedbackPartParameterModel.PRODUCTION_MONTH = Request.Params["ProductionMonth"];
            _SupplierFeedbackPartParameterModel.SUPPLIER_CODE = Request.Params["SupplierCode"];
            _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD = Request.Params["SupplierPlanCode"];
            _SupplierFeedbackPartParameterModel.VERSION = Request.Params["Version"];
            _SupplierFeedbackPartParameterModel.PARENT = "1";
            _SupplierFeedbackPartParameterModel.LOCK = Request.Params["Lock"];

            if (_SupplierFeedbackPartParameterModel.LOCK == "false")
            {
                _SupplierFeedbackPartParameterModel.LOCK = "0";
            }
            else
            {
                _SupplierFeedbackPartParameterModel.LOCK = "1";
            }


            Model.AddModel(_SupplierFeedbackPartParameterModel);

            _SupplierFeedbackPartModel.SupplierFeedbackParts = GetAllFeedbackPart(_SupplierFeedbackPartParameterModel.PRODUCTION_MONTH, _SupplierFeedbackPartParameterModel.SUPPLIER_CODE, _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD, _SupplierFeedbackPartParameterModel.VERSION, _SupplierFeedbackPartParameterModel.LOCK);
            Model.AddModel(_SupplierFeedbackPartModel);
            #endregion
            #endregion

            return PartialView("SupplierFeedbackPartGridPartial_Forecast", Model);
        }

        public ActionResult PartialGridUploadEvidenceSupplierFeedbackPartForecast()
        {
            #region Required model in partial page
            #region Model for SupplierFeedbackPartGridUploadEvidencePartial_Forecast

            #endregion
            #endregion

            return PartialView("SupplierFeedbackPartGridUploadEvidencePartial_Forecast", Model);
        }

        [HttpPost]
        public ActionResult PartialGridSupplierFeedbackPartForecast_Upload(String productionMonth, String supplierCode, String supplierPlanCode, String version, String FeedbackLock, String filePath)
        {
            #region Required model in partial page: SupplierFeedbackPartGridPartial_Forecast
            _SupplierFeedbackPartParameterModel.PRODUCTION_MONTH = productionMonth;
            _SupplierFeedbackPartParameterModel.SUPPLIER_CODE = supplierCode;
            _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD = supplierPlanCode;
            _SupplierFeedbackPartParameterModel.VERSION = version;
            _SupplierFeedbackPartParameterModel.LOCK = FeedbackLock;
            _SupplierFeedbackPartParameterModel.PARENT = "1";
            Model.AddModel(_SupplierFeedbackPartParameterModel);

            _SupplierFeedbackPartModel = ProcessAllFeedbackPartUpload(productionMonth, supplierCode, supplierPlanCode, version, filePath);
            Model.AddModel(_SupplierFeedbackPartModel);
            #endregion

            //return PartialView("SupplierFeedbackPartGridPartial_Forecast", Model);

            String status = Convert.ToString(TempData["IsValid"]);
            String view = PartialViewToString(this, "SupplierFeedbackPartGridPartial_Forecast", Model);

            return Json(
                new
                {
                    Status = status,
                    View = view
                });
        }

        public static string PartialViewToString(Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
            }

            controller.ViewData.Model = model;

            using (StringWriter stringWriter = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                return stringWriter.GetStringBuilder().ToString();
            }
        }
        //private static string RenderViewToString(this Controller controller, string viewName, object model)
        //{
        //    using (var writer = new StringWriter())
        //    {
        //        var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
        //        controller.ViewData.Model = model;
        //        var viewCxt = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, writer);
        //        viewCxt.View.Render(viewCxt, writer);
        //        return writer.ToString();
        //    }
        //}

        //public string RenderRazorViewToString(string viewName, object model)
        //{
        //    ViewData.Model = model;
        //    using (var sw = new StringWriter())
        //    {
        //        var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
        //        var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
        //        viewResult.View.Render(viewContext, sw);
        //        viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
        //        return sw.GetStringBuilder().ToString();
        //    }
        //}
        #endregion

        #region Database controller
        private Boolean IsUpdateValid(string[] updateParameterList)
        {
            Boolean valid = true;
            char[] fieldSeparator = { '|' };

            for (int i = 0; i < updateParameterList.Length; i++)
            {
                string[] fieldList = updateParameterList[i].Split(fieldSeparator);
                if (
                    fieldList[14] == "null" ||
                    fieldList[15] == "null" ||
                    fieldList[16] == "null" ||
                    fieldList[17] == "null"
                    )
                    valid = false;
            }

            return valid;
        }

        public static DataTable ToDataTable(List<SupplierFeedbackPartDetail> items)
        {
            DataTable dataTable = new DataTable("SPFTMP");

            PropertyInfo[] Props = items.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (SupplierFeedbackPartDetail item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public JsonResult UpdateFeedbackPart(List<SupplierFeedbackPartDetail> parameterList)
        {
            string FUNCTION_ID = "11009";
            string LOCATION = "SupplierFeedback.Update";
            string MODULE_ID = "1";
            Int64 PROCESS_ID = 0;
            string resultMessage = "SUCCESS";

            IDBContext db = DbContext;

            if (parameterList == null)
                parameterList = new List<SupplierFeedbackPartDetail>();

            if (parameterList.Count <= 0)
                return Json("Input Data Not Found or Missing, Please Refresh Page and Try Again");

            try
            {
                PROCESS_ID = db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                                "Update Supplier Feedback Data Started for Supplier " + 
                                                                parameterList[0].SUPPLIER_CD + ", Plant " + 
                                                                parameterList[0].S_PLANT_CD + ", Pack Month " +
                                                                parameterList[0].PACK_MONTH + ", Forecast Type " + 
                                                                parameterList[0].VERS,
                                                                AuthorizedUser.Username,
                                                                LOCATION,
                                                                PROCESS_ID,
                                                                "MSG001INF",
                                                                "INF",
                                                                MODULE_ID,
                                                                FUNCTION_ID,
                                                                0
                                                            });
            }
            catch (Exception excp)
            {
                resultMessage = "Error When Generate Process ID : " + excp.Message;
            }
            finally
            {
                db.Close();
            }

            if (!resultMessage.Equals("SUCCESS"))
                return Json(resultMessage);

            parameterList.ForEach(x => x.PROCESS_ID = PROCESS_ID);
            parameterList.ForEach(x => x.FUNCTION_ID = FUNCTION_ID);
            //parameterList.ForEach(x => x.USERNAME = AuthorizedUser.Username);

            DataTable ds = ConvertToDataTable(parameterList);

            using (SqlBulkCopy bcp = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
            {
                bcp.BulkCopyTimeout = 30;
                bcp.DestinationTableName = "TB_T_SUPPLIER_FEEDBACK_VALIDATE";
                bcp.ColumnMappings.Clear();
                bcp.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                bcp.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                bcp.ColumnMappings.Add("D_ID", "D_ID");
                bcp.ColumnMappings.Add("PACK_MONTH", "PACK_MONTH");
                bcp.ColumnMappings.Add("PART_NO", "PART_NO");
                bcp.ColumnMappings.Add("SUPPLIER_CD", "SUPPLIER_CD");
                bcp.ColumnMappings.Add("DOCK_CD", "DOCK_CD");
                bcp.ColumnMappings.Add("S_PLANT_CD", "S_PLANT_CD");
                bcp.ColumnMappings.Add("VERS", "VERSION");
                bcp.ColumnMappings.Add("N_VOLUME", "N_VOLUME");
                bcp.ColumnMappings.Add("N_1_VOLUME", "N_1_VOLUME");
                bcp.ColumnMappings.Add("N_2_VOLUME", "N_2_VOLUME");
                bcp.ColumnMappings.Add("N_3_VOLUME", "N_3_VOLUME");
                bcp.ColumnMappings.Add("N_FA", "N_FA");
                bcp.ColumnMappings.Add("N_1_FA", "N_1_FA");
                bcp.ColumnMappings.Add("N_2_FA", "N_2_FA");
                bcp.ColumnMappings.Add("N_3_FA", "N_3_FA");
                bcp.ColumnMappings.Add("N_MONTH", "N_MONTH");
                bcp.ColumnMappings.Add("N_1_MONTH", "N_1_MONTH");
                bcp.ColumnMappings.Add("N_2_MONTH", "N_2_MONTH");
                bcp.ColumnMappings.Add("N_3_MONTH", "N_3_MONTH");
                bcp.ColumnMappings.Add("NOTICE", "ERRORS_MESSAGE_CONCAT");
                bcp.ColumnMappings.Add("N_REPLY_PLANT_CAP", "N_PLANT_CAP");
                bcp.ColumnMappings.Add("N_1_REPLY_PLANT_CAP", "N_1_PLANT_CAP");
                bcp.ColumnMappings.Add("N_2_REPLY_PLANT_CAP", "N_2_PLANT_CAP");
                bcp.ColumnMappings.Add("N_3_REPLY_PLANT_CAP", "N_3_PLANT_CAP");
                bcp.ColumnMappings.Add("N_REPLY_RAW_COMP", "N_RAW_COMP");
                bcp.ColumnMappings.Add("N_1_REPLY_RAW_COMP", "N_1_RAW_COMP");
                bcp.ColumnMappings.Add("N_2_REPLY_RAW_COMP", "N_2_RAW_COMP");
                bcp.ColumnMappings.Add("N_3_REPLY_RAW_COMP", "N_3_RAW_COMP");
                bcp.ColumnMappings.Add("N_REPLY_RAW_MAT", "N_RAW_MAT");
                bcp.ColumnMappings.Add("N_1_REPLY_RAW_MAT", "N_1_RAW_MAT");
                bcp.ColumnMappings.Add("N_2_REPLY_RAW_MAT", "N_2_RAW_MAT");
                bcp.ColumnMappings.Add("N_3_REPLY_RAW_MAT", "N_3_RAW_MAT");
                bcp.ColumnMappings.Add("N_REPLY_QTY_SUPPLY_REFF", "N_QTY_SUPPLY_REFF");
                bcp.ColumnMappings.Add("N_1_REPLY_QTY_SUPPLY_REFF", "N_1_QTY_SUPPLY_REFF");
                bcp.ColumnMappings.Add("N_2_REPLY_QTY_SUPPLY_REFF", "N_2_QTY_SUPPLY_REFF");
                bcp.ColumnMappings.Add("N_3_REPLY_QTY_SUPPLY_REFF", "N_3_QTY_SUPPLY_REFF");

                try
                {
                    bcp.WriteToServer(ds);
                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Success Perform Bulk Insert to TB_T_SUPPLIER_FEEDBACK_VALIDATE",
                                                    AuthorizedUser.Username,
                                                    LOCATION,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });
                }
                catch (Exception ex)
                {
                    resultMessage = ex.Message;
                }
                finally
                {
                    bcp.Close();
                    db.Close();
                }
            }

            if (!resultMessage.Equals("SUCCESS"))
                return Json(resultMessage);

            string PACK_MONTH = parameterList[0].PACK_MONTH;
            string SUPPLIER_CD = parameterList[0].SUPPLIER_CD;
            string S_PLANT_CD = parameterList[0].S_PLANT_CD;
            string VERS = parameterList[0].VERS;
            string D_ID = parameterList[0].D_ID;

            try
            {
                int updated = db.ExecuteScalar<int>("UpdateFeedbackPart", new object[] { 
                                                        PROCESS_ID, 
                                                        FUNCTION_ID, 
                                                        AuthorizedUser.Username,
                                                        MODULE_ID,
                                                        FUNCTION_ID
                                                    });

                if ((resultMessage == "SUCCESS") && (updated == parameterList.Count))
                {
                    db.ExecuteScalar<string>("UpdateFeedbackSummary",
                        new object[] { 
                            PACK_MONTH,   // production month
                            SUPPLIER_CD,   // supplier cd
                            S_PLANT_CD,   // supplier plant cd
                            VERS,   // version
                            D_ID,   // d id
                            AuthorizedUser.Username
                        });

                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                            "Success Update Supplier Feedback Data for Supplier " + 
                            parameterList[0].SUPPLIER_CD + ", Plant " + 
                            parameterList[0].S_PLANT_CD + ", Pack Month " +
                            parameterList[0].PACK_MONTH + ", Forecast Type " + 
                            parameterList[0].VERS,
                            AuthorizedUser.Username,
                            LOCATION,
                            PROCESS_ID,
                            "MSG001INF",
                            "SUC",
                            MODULE_ID,
                            FUNCTION_ID,
                            2
                        });
                }
                else
                {
                    throw new Exception("Cannot Update Partial Data, Please Make Sure all Data are Not Empty");
                }
            }
            catch (Exception exc)
            {
                resultMessage = exc.Message;
                string unlockmsg = ErrorFeedbackUnlock(PACK_MONTH, SUPPLIER_CD, S_PLANT_CD, VERS);
                if (unlockmsg.Length > 0)
                {
                    resultMessage = resultMessage + ", " + unlockmsg;
                }
            }
            finally
            {
                db.Close();
            }

            try
            {
                db.ExecuteScalar<int>("DeleteTemporaryFeedback", new object[] { PROCESS_ID, FUNCTION_ID, AuthorizedUser.Username, MODULE_ID, FUNCTION_ID });
            }
            catch (Exception exc)
            {
                resultMessage = exc.Message;
            }
            finally
            {
                db.Close();
            }

            return Json(resultMessage);
        }

        private string ErrorFeedbackUnlock(String productionMonth, String supplierCode, String supplierPlanCode, String version)
        {
            String resultMessage = "";

            if ((productionMonth != "" && productionMonth != null) || (supplierCode != "" && supplierCode != null) || (supplierPlanCode != "" && supplierPlanCode != null) || (version != "" && version != null))
            {
                IDBContext db = DbContext;
                try
                {
                    resultMessage += db.ExecuteScalar<string>("UnlockForecast", new object[] { productionMonth, supplierCode, supplierPlanCode, version, AuthorizedUser.Username });
                }
                catch (Exception exc) { resultMessage = exc.Message; }
                finally { db.Close(); }

                resultMessage = "";
            }
            else
            {
                resultMessage = "Unlocking Failed";
            }

            return resultMessage;
        }
        public string GetLdapCompanyID()
        {
            //IDBContext dbLdap = DbContextLdap;
            String resultMessage = "";
            try
            {
                resultMessage = DbContext.ExecuteScalar<string>("GetLdapCompanyID", new object[] { AuthorizedUser.Username });
            }
            catch (Exception exc) { throw exc; }
            finally { DbContext.Close(); }

            return (resultMessage.ToString());
        }
        private List<SupplierFeedbackPartDetail> GetAllFeedbackPart(String productionMonth, String supplierCode, String supplierPlanCode, String version, String feedbacklock, String partNo = "")
        {
            List<SupplierFeedbackPartDetail> feedbackPartModel = new List<SupplierFeedbackPartDetail>();

            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();

            string CompanyID = "", noreg = "";
            noreg = user.NoReg;
            if (noreg == null)
            {
                noreg = "0";
            }
            CompanyID = GetLdapCompanyID();
            IDBContext db = DbContext;
            try
            {
                if (noreg == "0" && Convert.ToInt32(CompanyID) > 1)
                {
                    feedbackPartModel = db.Fetch<SupplierFeedbackPartDetail>("GetAllFeedbackPartSupp", new object[] { productionMonth, supplierCode, supplierPlanCode, version, feedbacklock, partNo });
                }
                else
                {
                    feedbackPartModel = db.Fetch<SupplierFeedbackPartDetail>("GetAllFeedbackPart", new object[] { productionMonth, supplierCode, supplierPlanCode, version, feedbacklock, partNo });
                }
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return feedbackPartModel;
        }
        #endregion

        #region Upload ftp controller
        #region Upload properties
        public static List<string> Files
        {
            get
            {
                UploadControlFilesStorage storage = System.Web.HttpContext.Current.Session["Storage"] as UploadControlFilesStorage;
                if (storage != null)
                    return storage.Files;
                return new List<string>();
            }
        }

        public static int FileInputCount
        {
            get
            {
                UploadControlFilesStorage storage = System.Web.HttpContext.Current.Session["Storage"] as UploadControlFilesStorage;
                if (storage != null)
                    return storage.FileInputCount;
                return 2;
            }
        }
        #endregion

        #region Upload request
        [HttpPost]
        public void SFPGUEFileUpload_CallbackRouteValues()
        {
            try
            {
                UploadedFile[] files = UploadControlExtension.GetUploadedFiles("SFPGUEFileUpload", ValidationSettingsEvidence, SFPGUEFileUpload_FileUploadComplete);
                if (files.Length == 0)
                {
                    return;
                }

                string msg = string.Empty;
                FTPUpload vFtp = new FTPUpload();
                ForecastDataCache forecastDataCache = Lookup.Get<ForecastDataCache>();
                List<FileUpload> fileUploadList = new List<FileUpload>();
                Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();

                // create directory in ftp
                //vFtp.CreateDirectory("/Portal/SupplierFeedbackPart/");
                string fileName = null;
                string pathDownload = vFtp.Setting.FtpPath("SupplierFeedback");
                for (int i = 0; i < files.Length; i++)
                {
                    fileName = Math.Abs(System.Environment.TickCount) + files[i].FileName;
                    //vFtp.FtpUpload("/Portal/SupplierFeedback/", fileName, files[i].FileBytes, ref msg);
                    vFtp.FtpUpload(vFtp.Setting.FtpPath("SupplierFeedback"), fileName, files[i].FileBytes, ref msg);
                   
                    if (!string.IsNullOrEmpty(forecastDataCache.UpcomingUploadMessageBoardName))
                    {
                        FileUpload fileInfo = new FileUpload()
                        {
                            FileName = fileName,
                            Description = fileName,
                            Group = "MessageBoard",
                            Key = forecastDataCache.UpcomingUploadMessageBoardName,
                            Qualifier = FileUploadCategory.INCREMENTAL,
                            //Path = "/Portal/SupplierFeedback/" + fileName
                            Path = pathDownload + fileName
                        };
                        fileUploadList.Add(fileInfo);
                    }
                }
                FileUploadRegistry.GetInstance().Save(fileUploadList.ToArray(), user);

                MessageBoardMessage message;
                List<MessageBoardMessage> messageList = new List<MessageBoardMessage>();
                foreach (FileUpload fileUpload in fileUploadList)
                {
                    message = new MessageBoardMessage()
                    {
                        GroupId = Convert.ToInt32(fileUpload.Qualifier),
                        BoardName = forecastDataCache.UpcomingUploadMessageBoardName,
                        Recipient = "All",
                        //Text = "Supplier feedback file uploaded: <a target='_blank' href='ftp://" + vFtp.Setting.IP + "/Portal/DataExchange/" + fileName + "'>" + fileName + "</a>",
                        Text = "Supplier feedback file uploaded: <a target=' _blank' href='" + CreateAttachmentDownloadUrl(pathDownload, fileName) + "'>" + fileName + "</a>",
                        Date = DateTime.Now,
                        Sender = user.Username
                    };
                    messageList.Add(message);
                }
                MessageBoardService.GetInstance().Save(messageList.ToArray());
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        protected readonly ValidationSettings ValidationSettingsEvidence = new ValidationSettings
        {
            //AllowedFileExtensions = new string[] { ".xls", ".xlsx", ".pdf", ".doc", ".docx", ".txt", ".jpg", ".gif", ".png" },
            AllowedFileExtensions = new string[] { ".xls", ".xlsx" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true,
            
        };
        #endregion

        //[HttpPost]
        //public ActionResult SFPGUEFileUpload_FileUploadComplete(String updateParameter)
        //{
        //    UpdateFeedbackPart(updateParameter);

        //    return Json(
        //        new
        //        {
        //        });
        //}

        protected void SFPGUEFileUpload_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }
        #endregion

        #region Upload database controller
        #region Upload properties
        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/SupplierFeedbackPart/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private long _processId = 0;
        private long ProcessId
        {
            get
            {
                if (_processId == 0)
                {
                    IDBContext db = DbContext;

                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); }

                    return _processId;

                    //_processId = Math.Abs(Environment.TickCount);
                }

                return _processId;
            }
        }

        private Int32 _seqNo = 0;
        private Int32 SeqNo
        {
            get
            {
                return ++_seqNo;
            }
        }
        #endregion

        private ArrayList GetUploadedFile()
        {
            ArrayList fileRowList = new ArrayList();

            try
            {
                // read uploaded excel file
                fileRowList = ExcelReader.ReaderExcel(
                    _TableDestination.filePath,
                    _TableDestination.sheetName,
                    Convert.ToInt32(_TableDestination.rowStart),
                    Convert.ToInt32(_TableDestination.columnStart),
                    Convert.ToInt32(_TableDestination.columnEnd));

                // if read success then remove temporary file
                if (fileRowList.Count > 0)
                {
                    if (System.IO.File.Exists(_TableDestination.filePath))
                        System.IO.File.Delete(_TableDestination.filePath);
                }
            }
            catch (Exception exc)
            {
                throw new Exception("GetUploadedFile:" + exc.Message);
            }

            return fileRowList;
        }

        private String GetColumnName()
        {
            IDBContext db = DbContext;

            ArrayList columnNameArray = new ArrayList();

            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            finally
            {
                db.Close();
            }

            return String.Join(",", columnNameArray.ToArray());
        }

        private String GetColumnValue(String fileRow, Int32 fileColumnCount)
        {
            ArrayList columnValueArray = new ArrayList();
            String[] dumpArray = new String[fileColumnCount];

            try
            {
                // create cleaned (from unused field) from uploaded excel file
                Array.Copy(fileRow.Split(';'), 1, dumpArray, 0, fileColumnCount);

                // create column value parameter     
                columnValueArray.Add("'" + _TableDestination.functionID + "'");    // function id
                columnValueArray.Add("'" + _TableDestination.processID + "'");     // process id

                columnValueArray.Add(dumpArray[0]);    // d id
                columnValueArray.Add(dumpArray[1]);    // pack month
                columnValueArray.Add(dumpArray[2]);    // part no
                columnValueArray.Add(dumpArray[3]);    // part name

                columnValueArray.Add(dumpArray[4]);    // version
                columnValueArray.Add(dumpArray[5]);    // supplier code
                columnValueArray.Add(dumpArray[6]);    // supplier plant code
                columnValueArray.Add(dumpArray[7]);    // dock code

                columnValueArray.Add(dumpArray[8]);     // n volume
                columnValueArray.Add(dumpArray[11]);    // n + 1 volume
                columnValueArray.Add(dumpArray[14]);    // n + 2 volume
                columnValueArray.Add(dumpArray[17]);    // n + 3 volume

                columnValueArray.Add(dumpArray[9]);    // n fa
                columnValueArray.Add(dumpArray[12]);    // n + 1 fa
                columnValueArray.Add(dumpArray[15]);    // n + 2 fa
                columnValueArray.Add(dumpArray[18]);    // n + 3 fa

                columnValueArray.Add(dumpArray[10]);    // n reply
                columnValueArray.Add(dumpArray[13]);    // n + 1 reply
                columnValueArray.Add(dumpArray[16]);    // n + 2 reply
                columnValueArray.Add(dumpArray[19]);    // n + 3 reply
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnValue:" + exc.Message);
            }

            return String.Join(",", columnValueArray.ToArray());
        }

        /// <summary>
        /// Main process to upload feedback part data.
        /// </summary>
        /// <param name="partNo"></param>
        /// <param name="productionMonth"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        /// <param name="version"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private SupplierFeedbackPartModel ProcessAllFeedbackPartUpload(String productionMonth, String supplierCode, String supplierPlanCode, String version, String filePath)
        {
            IDBContext db = DbContext;

            SupplierFeedbackPartModel _UploadedSupplierFeedbackPartModel = new SupplierFeedbackPartModel();

            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();

            string CompanyID = "", noreg = "";
            noreg = user.NoReg;
            if (noreg == null)
            {
                noreg = "0";
            }
            CompanyID = GetLdapCompanyID();

            // table destination properties
            _TableDestination.functionID = "111";
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "SupplierFeedbackData";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "20";
            _TableDestination.tableFromTemp = "TB_T_SUPPLIER_FEEDBACK_UPLOAD";
            _TableDestination.tableToAccess = "TB_T_SUPPLIER_FEEDBACK_VALIDATE";

            LogProvider logProvider = new LogProvider("101", "111", AuthorizedUser);
            ILogSession sessionA = logProvider.CreateSession(true);

            OleDbConnection excelConn = null;

            try
            {
                // insert upload process info into table log header.
                //LogProvider logProvider = new LogProvider("111", "SFPU", AuthorizedUser);
                //ILogSession sessionA = logProvider.CreateSession();
                //sessionA.Write("MPCS00001ERR", new object[] { "Test Parameter 1", "Test Parameter 2", "Test Parameter 3" });
                //sessionA.Write("MPCS00001INF", new object[] { "Penginputan data telah berhasil, silahkan mencoba untuk bertahan" });
                //sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);

                //sessionA.Commit();

                sessionA.WriteDirectly("MPCS00002INF", new object[] { "Process Upload" }, "Supplier Feedback Part.Upload Process");

                sessionA.WriteDirectly("MPCS00004INF", new object[] { "Clear upload temporary table" }, "Supplier Feedback Part.Clear Temporary Table" );

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableFromTemp });
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                sessionA.WriteDirectly("MPCS00002INF", new object[] { "Bulk copy" }, "Supplier Feedback Part.Bulk Copy");

                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;IMEX=1;ImpoerMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();
                //string value = "'FCST','NCS','TNQC'";
                excelCommand = new OleDbCommand("SELECT" + " " +_TableDestination.functionID + " " +" as FUNCTION_ID, " + " "
                                               
                                               +  _TableDestination.processID  + " " +" as PROCESS_ID, " + " "
                                               + " D_ID,"
                                               + "PACK_MONTH,"
                                               + "PART_NO,"
                                               + "PART_NAME,"
                                               + "VERS AS VERSION,"
                                               + "SUPPLIER_CD,"
                                               + "S_PLANT_CD,"
                                               + "DOCK_CD,"
                                               + "N_VOLUME,"
                                               + "N_FA,"
                                               + "N_REPLY AS N_MONTH ,"
                                               + "IIf(IsNull(N_REPLY_PLANT_CAP), Null, CStr(N_REPLY_PLANT_CAP)) AS N_PLANT_CAP,"
                                               + "IIf(IsNull(N_REPLY_RAW_COMP),Null, CStr(N_REPLY_RAW_COMP)) AS N_RAW_COMP,"
                                               + "IIf(IsNull(N_REPLY_RAW_MAT), Null, CStr(N_REPLY_RAW_MAT)) AS N_RAW_MAT,"
                                               + "IIf(IsNull(N_REPLY_QTY_SUPPLY_REFF), Null, CStr(N_REPLY_QTY_SUPPLY_REFF)) AS N_QTY_SUPPLY_REFF,"
                                               + "N_1_VOLUME,"
                                               + "N_1_FA,"
                                               + "N_1_REPLY AS N_1_MONTH,"
                                               + "IIf(IsNull(N_1_REPLY_PLANT_CAP), Null, CStr(N_1_REPLY_PLANT_CAP)) AS N_1_PLANT_CAP,"
                                               + "IIf(IsNull(N_1_REPLY_RAW_COMP),Null, CStr(N_1_REPLY_RAW_COMP)) AS N_1_RAW_COMP,"
                                               + "IIf(IsNull(N_1_REPLY_RAW_MAT), Null, CStr(N_1_REPLY_RAW_MAT)) AS N_1_RAW_MAT,"
                                               + "IIf(IsNull(N_1_REPLY_QTY_SUPPLY_REFF), Null, CStr(N_1_REPLY_QTY_SUPPLY_REFF)) AS N_1_QTY_SUPPLY_REFF,"
                                               + "N_2_VOLUME," 
                                               + "N_2_FA,"
                                               + "N_2_REPLY AS N_2_MONTH,"
                                               + "IIf(IsNull(N_2_REPLY_PLANT_CAP), Null, CStr(N_2_REPLY_PLANT_CAP)) AS N_2_PLANT_CAP,"
                                               + "IIf(IsNull(N_2_REPLY_RAW_COMP),Null, CStr(N_2_REPLY_RAW_COMP)) AS N_2_RAW_COMP,"
                                               + "IIf(IsNull(N_2_REPLY_RAW_MAT), Null, CStr(N_2_REPLY_RAW_MAT)) AS N_2_RAW_MAT,"
                                               + "IIf(IsNull(N_2_REPLY_QTY_SUPPLY_REFF), Null, CStr(N_2_REPLY_QTY_SUPPLY_REFF)) AS N_2_QTY_SUPPLY_REFF,"
                                               + "N_3_VOLUME,"
                                               + "N_3_FA,"
                                               + "N_3_REPLY AS N_3_MONTH,"
                                               + "IIf(IsNull(N_3_REPLY_PLANT_CAP), Null, CStr(N_3_REPLY_PLANT_CAP)) AS N_3_PLANT_CAP,"
                                               + "IIf(IsNull(N_3_REPLY_RAW_COMP),Null, CStr(N_3_REPLY_RAW_COMP)) AS N_3_RAW_COMP,"
                                               + "IIf(IsNull(N_3_REPLY_RAW_MAT), Null, CStr(N_3_REPLY_RAW_MAT)) AS N_3_RAW_MAT,"
                                               + "IIf(IsNull(N_3_REPLY_QTY_SUPPLY_REFF), Null, CStr(N_3_REPLY_QTY_SUPPLY_REFF)) AS N_3_QTY_SUPPLY_REFF"
                                               + " "
                                               + "FROM [" + _TableDestination.sheetName + "$] ", excelConn);
                                 
                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);

                dtPatterns = DeleteEmptyRows(dtPatterns);

                ds.Tables.Add(dtPatterns);

                //using (SqlBulkCopy bulkCopy = new SqlBulkCopy("Server=10.16.20.230;Database=PCS_DB;User ID=K2;Password=K2pass!;Trusted_Connection=false;MultipleActiveResultSets=true;"))
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //bulkCopy.DestinationTableName = "dbo.TB_T_SUPPLIER_FEEDBACK_UPLOAD_JKS";
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("D_ID", "D_ID");
                    bulkCopy.ColumnMappings.Add("PACK_MONTH", "PACK_MONTH");
                    bulkCopy.ColumnMappings.Add("PART_NO", "PART_NO");
                    bulkCopy.ColumnMappings.Add("PART_NAME", "PART_NAME");
                    bulkCopy.ColumnMappings.Add("VERSION", "VERSION");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CD", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("S_PLANT_CD", "S_PLANT_CD");
                    bulkCopy.ColumnMappings.Add("DOCK_CD", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("N_VOLUME", "N_VOLUME");
                    bulkCopy.ColumnMappings.Add("N_1_VOLUME", "N_1_VOLUME");
                    bulkCopy.ColumnMappings.Add("N_2_VOLUME", "N_2_VOLUME");
                    bulkCopy.ColumnMappings.Add("N_3_VOLUME", "N_3_VOLUME");
                    bulkCopy.ColumnMappings.Add("N_FA", "N_FA");
                    bulkCopy.ColumnMappings.Add("N_1_FA", "N_1_FA");
                    bulkCopy.ColumnMappings.Add("N_2_FA", "N_2_FA");
                    bulkCopy.ColumnMappings.Add("N_3_FA", "N_3_FA");
                    bulkCopy.ColumnMappings.Add("N_MONTH", "N_MONTH");
                    bulkCopy.ColumnMappings.Add("N_1_MONTH", "N_1_MONTH");
                    bulkCopy.ColumnMappings.Add("N_2_MONTH", "N_2_MONTH");
                    bulkCopy.ColumnMappings.Add("N_3_MONTH", "N_3_MONTH");
                    bulkCopy.ColumnMappings.Add("N_PLANT_CAP", "N_PLANT_CAP");
                    bulkCopy.ColumnMappings.Add("N_1_PLANT_CAP", "N_1_PLANT_CAP");
                    bulkCopy.ColumnMappings.Add("N_2_PLANT_CAP", "N_2_PLANT_CAP");
                    bulkCopy.ColumnMappings.Add("N_3_PLANT_CAP", "N_3_PLANT_CAP");
                    bulkCopy.ColumnMappings.Add("N_RAW_COMP", "N_RAW_COMP");
                    bulkCopy.ColumnMappings.Add("N_1_RAW_COMP", "N_1_RAW_COMP");
                    bulkCopy.ColumnMappings.Add("N_2_RAW_COMP", "N_2_RAW_COMP");
                    bulkCopy.ColumnMappings.Add("N_3_RAW_COMP", "N_3_RAW_COMP");
                    bulkCopy.ColumnMappings.Add("N_RAW_MAT", "N_RAW_MAT");
                    bulkCopy.ColumnMappings.Add("N_1_RAW_MAT", "N_1_RAW_MAT");
                    bulkCopy.ColumnMappings.Add("N_2_RAW_MAT", "N_2_RAW_MAT");
                    bulkCopy.ColumnMappings.Add("N_3_RAW_MAT", "N_3_RAW_MAT");
                    bulkCopy.ColumnMappings.Add("N_QTY_SUPPLY_REFF", "N_QTY_SUPPLY_REFF");
                    bulkCopy.ColumnMappings.Add("N_1_QTY_SUPPLY_REFF", "N_1_QTY_SUPPLY_REFF");
                    bulkCopy.ColumnMappings.Add("N_2_QTY_SUPPLY_REFF", "N_2_QTY_SUPPLY_REFF");
                    bulkCopy.ColumnMappings.Add("N_3_QTY_SUPPLY_REFF", "N_3_QTY_SUPPLY_REFF");

                    try
                    {
                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        sessionA.WriteDirectly("MPCS00002ERR", new string[] { ex.Message, "", "", "" }, "Supplier Feedback Part.Bulk Copy");
                        Console.WriteLine(ex.Message);
                    }
                }

                sessionA.WriteDirectly("MPCS00003INF", new object[] { "Bulk copy" }, "Supplier Feedback Part.Bulk Copy");

                String columnNameList = GetColumnName();

                sessionA.WriteDirectly("MPCS00004INF", new object[] { "Validate Feedback Part Upload" }, "Supplier Feedback Part.Validate Feedback Part Upload");

                // validate data in temporary uploaded table.
                db.Execute("ValidateFeedbackPartUpload", new object[] { _TableDestination.functionID, _TableDestination.processID, supplierCode, supplierPlanCode, productionMonth, version });

                sessionA.WriteDirectly("MPCS00004INF", new object[] { "Get Error Message Upload" }, "Supplier Feedback Part.Get Error Message Upload");

                // check for invalid data.
                try
                {
                    _ErrorValidationList = db.Fetch<ErrorValidation>("GetErrorMessageUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });
                }
                catch (Exception exc)
                {
                    sessionA.WriteDirectly("MPCS00002ERR", new string[] { exc.Message, "", "", "" }, "Supplier Feedback Part.Get Error Message Upload");
                    throw exc;
                }

                string matching = "";

                if (_ErrorValidationList.Count == 0)
                    matching = db.SingleOrDefault<string>("MatchingValidation", new object[] { productionMonth, supplierCode, supplierPlanCode, version, _TableDestination.processID, _TableDestination.functionID,CompanyID });

                if ((_ErrorValidationList.Count == 0) && (matching == ""))
                {
                    sessionA.WriteDirectly("MPCS00004INF", new object[] { "Insert upload data from temporary table into destination table" }, "Supplier Feedback Part.Insert Temporary to Destination Table Upload");

                    // insert data from temporary uploaded table into temporary validated table.
                    db.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] { _TableDestination.tableToAccess, columnNameList, columnNameList, _TableDestination.tableFromTemp });

                    sessionA.WriteDirectly("MPCS00004INF", new object[] { "Get all uploaded data from destination table" }, "Supplier Feedback Part.Get Uploaded Data From Destination Table");

                    // read uploaded data from temporary validated table to transcation table.
                    _UploadedSupplierFeedbackPartModel = GetAllFeedbackPartUpload(productionMonth, supplierCode, supplierPlanCode, version, _TableDestination.processID, _TableDestination.functionID);

                    // set process validation status.
                    TempData["IsValid"] = "true;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                }
                else
                {
                    sessionA.WriteDirectly("MPCS00008ERR", new object[] { "Validate Feedback Part Upload" }, "Supplier Feedback Part.Validate Feedback Part Upload");

                    sessionA.WriteDirectly("MPCS00004INF", new object[] { "Get previous feedback part data" }, "Supplier Feedback Part.Get Previous Feedback Part Data");

                    // load previous data for supplier feedback part model
                    _UploadedSupplierFeedbackPartModel.SupplierFeedbackParts = GetAllFeedbackPart(productionMonth, supplierCode, supplierPlanCode, version, FeedbackLock);

                    // set process validation status.
                    if ((matching != "") && (_ErrorValidationList.Count == 0))
                        TempData["IsValid"] = "notmatch;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    else
                        TempData["IsValid"] = "false;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                }
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                sessionA.WriteDirectly("MPCS00002ERR", new string[] { exc.Message, "", "", "" }, "Supplier Feedback Part.Process Upload");

                // load previous data for supplier feedback part model
                _UploadedSupplierFeedbackPartModel.SupplierFeedbackParts = GetAllFeedbackPart(productionMonth, supplierCode, supplierPlanCode, version, FeedbackLock);

                // set download as invalid type
                TempData["IsValid"] = "invalid;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
            }
            finally
            {
                sessionA.WriteDirectly("MPCS00003INF", new object[] { "Process Upload" }, "Supplier Feedback Part.Upload Process");

                sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                sessionA.Commit();
                db.Close();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }

            return _UploadedSupplierFeedbackPartModel;
        }

        [HttpPost]
        public void ClearAllFeedbackPartUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            IDBContext db = DbContext;

            try
            {
                // clear data in temporary uploaded table.
                db.Execute("ClearFeedbackPartUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                db.Close();
            }
        }

        /// <summary>
        /// Get all of uploaded feedback part data.
        /// </summary>
        /// <param name="partNo"></param>
        /// <param name="productionMonth"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        private SupplierFeedbackPartModel GetAllFeedbackPartUpload(String productionMonth, String supplierCode, String supplierPlanCode, String version, String processId, String functionId)
        {
            IDBContext db = DbContext;
            
            SupplierFeedbackPartModel _FilteredSupplierFeedbackPartModel = new SupplierFeedbackPartModel();
                        
            try
            {
                _FilteredSupplierFeedbackPartModel.SupplierFeedbackParts = db.Fetch<SupplierFeedbackPartDetail>("GetAllFeedbackPartUpload", new object[] { productionMonth, supplierCode, supplierPlanCode, version, processId, functionId });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            //SupplierFeedbackPartModel _UploadedSupplierFeedbackPartModel = new SupplierFeedbackPartModel();

            //try
            //{
            //    _UploadedSupplierFeedbackPartModel.SupplierFeedbackParts = db.Fetch<SupplierFeedbackPartDetail>("GetAllFeedbackPartUpload", new object[] { productionMonth, supplierCode, supplierPlanCode, version, processId, functionId });
            //}
            //catch (Exception exc) { throw exc; }
            //finally { db.Close(); }
                        
            //SupplierFeedbackPartModel _FilteredSupplierFeedbackPartModel = new SupplierFeedbackPartModel();
            //SupplierFeedbackPartDetail _FilteredSupplierFeedbackPartDetail = new SupplierFeedbackPartDetail();

            //_FilteredSupplierFeedbackPartModel.SupplierFeedbackParts.Clear();

            //_SupplierFeedbackPartModel.SupplierFeedbackParts = GetAllFeedbackPart(productionMonth, supplierCode, supplierPlanCode, version);
            //if (_SupplierFeedbackPartModel.SupplierFeedbackParts.Count > 0)
            //{
            //    foreach (SupplierFeedbackPartDetail _ComparedSupplierFeedbackPartDetail in _SupplierFeedbackPartModel.SupplierFeedbackParts)
            //    {
            //        _FilteredSupplierFeedbackPartDetail = null;
            //        _FilteredSupplierFeedbackPartDetail = _UploadedSupplierFeedbackPartModel.SupplierFeedbackParts.Find(
            //                supplierFeedbackPartDetail =>
            //                    supplierFeedbackPartDetail.D_ID == _ComparedSupplierFeedbackPartDetail.D_ID &&
            //                    supplierFeedbackPartDetail.PART_NO == _ComparedSupplierFeedbackPartDetail.PART_NO &&
            //                    supplierFeedbackPartDetail.SUPPLIER_CD == _ComparedSupplierFeedbackPartDetail.SUPPLIER_CD &&
            //                    supplierFeedbackPartDetail.S_PLANT_CD == _ComparedSupplierFeedbackPartDetail.S_PLANT_CD &&
            //                    supplierFeedbackPartDetail.VERS == _ComparedSupplierFeedbackPartDetail.VERS &&
            //                    supplierFeedbackPartDetail.DOCK_CD == _ComparedSupplierFeedbackPartDetail.DOCK_CD
            //                );
            //        if (_FilteredSupplierFeedbackPartDetail != null)
            //        {
            //            _ComparedSupplierFeedbackPartDetail.N_REPLY = _FilteredSupplierFeedbackPartDetail.N_REPLY;
            //            _ComparedSupplierFeedbackPartDetail.N_1_REPLY = _FilteredSupplierFeedbackPartDetail.N_1_REPLY;
            //            _ComparedSupplierFeedbackPartDetail.N_2_REPLY = _FilteredSupplierFeedbackPartDetail.N_2_REPLY;
            //            _ComparedSupplierFeedbackPartDetail.N_3_REPLY = _FilteredSupplierFeedbackPartDetail.N_3_REPLY;
            //        }

            //        _FilteredSupplierFeedbackPartModel.SupplierFeedbackParts.Add(_ComparedSupplierFeedbackPartDetail);
            //    }
            //}

            return _FilteredSupplierFeedbackPartModel;
        }

        [HttpPost]
        public void SFPFFFileUpload_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("SFPFFFileUpload", ValidationSettings, SFPFFFileUpload_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void SFPFFFileUpload_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }
        #endregion

        #region Download controller
        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else if (pi.PropertyType == typeof(Int32?))
                            dt.Columns.Add(pi.Name, typeof(Int32));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /// <summary>
        /// Download template using local file and dummy data. for testing only.
        /// </summary>
        //public void DownloadTemplate()
        //{
        //    try
        //    {
        //        FileInfo templateFile = new FileInfo(Server.MapPath("~/Views/SupplierFeedbackPart/UploadTemplate/SupplierFeedbackPart_Template.xls"));
        //        FileStream myFile = new FileStream(templateFile.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        //        BinaryReader br = new BinaryReader(myFile);
        //        try
        //        {
        //            Response.AddHeader("Accept-Ranges", "bytes");
        //            Response.Buffer = false;
        //            long fileLength = myFile.Length;
        //            long startBytes = 0;

        //            int pack = 10240; //10K bytes
        //            int sleep = (int)Math.Floor((double)(1000 * pack / 1024000)) + 1;
        //            if (Request.Headers["Range"] != null)
        //            {
        //                Response.StatusCode = 206;
        //                string[] range = Request.Headers["Range"].Split(new char[] { '=', '-' });
        //                startBytes = Convert.ToInt64(range[1]);
        //            }
        //            Response.AddHeader("Content-Length", (fileLength - startBytes).ToString());
        //            if (startBytes != 0)
        //            {
        //                Response.AddHeader("Content-Range", string.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength));
        //            }
        //            Response.AddHeader("Connection", "Keep-Alive");
        //            Response.ContentType = "application/octet-stream";
        //            Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(templateFile.Name, System.Text.Encoding.UTF8));

        //            br.BaseStream.Seek(startBytes, SeekOrigin.Begin);
        //            int maxCount = (int)Math.Floor((double)((fileLength - startBytes) / pack)) + 1;

        //            for (int i = 0; i < maxCount; i++)
        //            {
        //                if (Response.IsClientConnected)
        //                {
        //                    Response.BinaryWrite(br.ReadBytes(pack));
        //                    Thread.Sleep(sleep);
        //                }
        //                else
        //                {
        //                    i = maxCount;
        //                }
        //            }
        //        }
        //        catch
        //        {
        //            return;
        //        }
        //        finally
        //        {
        //            br.Close();
        //            myFile.Close();
        //        }
        //    }
        //    catch
        //    {
        //        return;
        //    }
        //    return;
        //}

        /// <summary>
        /// Download template using telerik.
        /// </summary>
        /// <param name="partNo"></param>
        /// <param name="productionMonth"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        public void DownloadTemplate(String productionMonth, String supplierCode, String supplierPlanCode, String version, String partNo = "")
        {
            string fileName = "SupplierFeedbackPart" + (version == "T" ? "Tentative" : "Firm") + productionMonth + supplierCode + supplierPlanCode + ".xls";

            // Update download user and status
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportGetAllFeedbackPartModel> qry = db.Fetch<ReportGetAllFeedbackPartModel>("ReportGetAllFeedbackPart", new object[] { productionMonth, supplierCode, supplierPlanCode, version, partNo });

            IExcelWriter exporter = new NPOIWriter();

            byte[] hasil = exporter.Write(ConvertToDataTable(qry), "SupplierFeedbackData");

            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        /// <summary>
        /// Generate excel file contain invalid data from data upload validation process.
        /// </summary>
        /// <param name="partNo"></param>
        /// <param name="productionMonth"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        public void DownloadInvalid(String productionMonth, String supplierCode, String supplierPlanCode, String version, String processId, String functionId)
        {
            string fileName = "SupplierFeedbackPartInvalid" + (version == "T" ? "Tentative" : "Firm") + productionMonth + supplierCode + supplierPlanCode + ".xls";

            // Update download user and status
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportGetAllFeedbackPartInvalidModel> qry = db.Fetch<ReportGetAllFeedbackPartInvalidModel>("ReportGetAllFeedbackPartInvalid", new object[] { functionId, processId });
            db.Close();

            //int exec= db.Execute("ReportGetAllFeedbackPartInvalid", new object[] { functionId, processId });

            IExcelWriter exporter = new NPOIWriter();

            byte[] hasil = exporter.Write(ConvertToDataTable(qry), "SupplierFeedbackData");
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion
        #endregion

        private IDBContextManager DBManager;
        private FTPUpload vFtp = new FTPUpload();
        private const string fileServerPath = "/Portal/DataExchange/";
        private string downloadedPath = "";
        private string FeedbackLock;

        public ActionResult Download()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/SupplierFeedbackPart/UploadTemp/PCS-Supplier_Feedback_Form_for_NG_Feedback.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "PCS-Supplier_Feedback_Form_for_NG_Feedback.xls");

            return PartialView("SupplierFeedbackPartGridUploadEvidencePartial_Forecast", Model);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        public String convertToString(String var)
        {
            String res = "";
            if (!String.IsNullOrEmpty(var))
            {
                res = var;
            }
            return res;
        }

        public DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            List<DataRow> drList = new List<DataRow>();
            foreach (DataRow dr in formattedTable.Rows)
            {
                int count = dr.ItemArray.Length;
                int nullcounter = 0;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])) || dr.ItemArray[i].ToString().Length <= 0)
                    {
                        nullcounter++;
                    }
                }

                if (nullcounter == count - 2)
                {
                    drList.Add(dr);
                }
            }

            for (int i = 0; i < drList.Count; i++)
            {
                formattedTable.Rows.Remove(drList[i]);
            }
            formattedTable.AcceptChanges();

            return formattedTable;
        }
    }
}
