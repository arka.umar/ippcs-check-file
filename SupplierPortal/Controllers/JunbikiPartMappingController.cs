﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml;
using DevExpress.Web.Mvc;
using Portal.Models;
using Portal.Models.Dock;
using Portal.Models.Globals;
using Portal.Models.Plant;
using Portal.Models.JunbikiPartMapping;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Reporting;

namespace Portal.Controllers
{
    public class JunbikiPartMappingController : BaseController
    {

        IDBContext db = null;
        IDBContext DB
        {
            get
            {
                if (db == null)
                {
                    db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return db;
            }
        }

        public JunbikiPartMappingController()
            : base("Junbiki Part Mapping")
        {
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            JunbikiPartMappingModel j = new JunbikiPartMappingModel();
            j.Detail = // new List<JunbikiPartMappingDetail>();
                DB.Query<JunbikiPartMappingDetail>("GetAllJunbikiPartMapping")
                  .ToList();
            
            Model.AddModel(j);
        }

        public ActionResult JunbikiPartPartial()
        {
            return PartialView("JunbikiPartMappingPartial", Model);
        }

        public ActionResult JunbikiPartRoutes()
        {
            return null; //  PartialView("JunbikiPartMappingPartial", Model);
        }
    }
}
