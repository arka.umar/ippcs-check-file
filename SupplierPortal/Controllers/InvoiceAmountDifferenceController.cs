﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.PartInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;
using Portal.Models.Dock;
using Portal.Models.UtilPlane;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.Plant;
using Portal.Models.InvoiceAmountDifference;
using Portal.Models.VatInH;

namespace Portal.Controllers
{
    public class InvoiceAmountDifferenceController : BaseController
    {

        public InvoiceAmountDifferenceController()
            : base("Invoice Amount Difference : ")
        {
        }

        protected override void StartUp()
        {
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<SupplierName> _supplierModel = null;
        private List<SupplierName> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierName>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<PartData> _partModel = null;
        private List<PartData> _PartModel
        {
            get
            {
                if (_partModel == null)
                    _partModel = new List<PartData>();

                return _partModel;
            }
            set
            {
                _partModel = value;
            }
        }


        protected override void Init()
        {

            IDBContext db = DbContext;

            InvoiceAmountDifferenceModel modelInfo = new InvoiceAmountDifferenceModel();
            modelInfo.InvoiceAmountDifferenceDatas = getPartInfo("", "", "");
            Model.AddModel(modelInfo);

        }

        public ActionResult PartialHeader()
        {
            //List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "InvoiceAmountDifference", "OIHSupplierOptionUtilPlane");
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAmountDifference", "OIHSupplierOptionUtilPlane");
            
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            ViewData["GridPart"] = GetAllPart();
            ViewData["GridDock"] = GetAllDock();
            ViewData["GridRcv"] = GetAllTax();

            return PartialView("PartialHeader");

        }
        public ActionResult PartialDetail()
        {

            InvoiceAmountDifferenceModel model = Model.GetModel<InvoiceAmountDifferenceModel>();
            model.InvoiceAmountDifferenceDatas = getPartInfo(Request.Params["SupplierCode"]
                                        , Request.Params["InvNumber"], Request.Params["TaxNumber"]);

            Model.AddModel(model);

            return PartialView("PartialDetail", Model);
        }

        protected List<InvoiceAmountDifferenceData> getPartInfo(string SupplierCode, string InvNumber, string TaxNumber)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<InvoiceAmountDifferenceData> listTooltipDatas = new List<InvoiceAmountDifferenceData>();
            
            listTooltipDatas = db.Query<InvoiceAmountDifferenceData>("GetInvoiceAmountDifference", new object[] { SupplierCode, InvNumber, TaxNumber }).ToList();

            db.Close();
            return listTooltipDatas;
        }

        public ActionResult PartialHeaderSupplierLookupGridUtilPlane()
        {
            TempData["GridName"] = "UtilPlaneHSupplierGridLookup";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "InvoiceAmountDifference", "OIHSupplierOptionUtilPlane");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);

        }

        public ActionResult PartialHeaderRcvPlantGridHeaderUtilPlane()
        {
            TempData["GridName"] = "UtilPlaneHRcvPlantGridLookup";
            List<VatInH> docks = GetAllTax();
            ViewData["GridRcv"] = docks;


            //List<VatInH> suppliers = Model.GetModel<User>().FilteringArea<VatInH>(GetAllTax(), "TAX_INVOICE_NO", "InvoiceAmountDifference", "UtilPlaneHRcvPlantGridLookup");
            //if (suppliers.Count > 0)
            //{
            //    ViewData["GridSupplier"] = suppliers;
            //}
            //else
            //{
            //    ViewData["GridSupplier"] = GetAllTax();
            //}


            return PartialView("GridLookup/PartialGrid", ViewData["GridRcv"]);
        }
        private List<VatInH> GetAllTax()
        {
            List<VatInH> PartIndoDockModel = new List<VatInH>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<VatInH>("GetAllTaxNumber", new object[] { });
            db.Close();

            return PartIndoDockModel;

        }
        private List<PartData> GetAllPart()
        {
            IDBContext db = DbContext;

            List<PartData> partModel = new List<PartData>();
            partModel = db.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
            db.Close();

            return partModel;

        }

        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> SISupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            SISupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return SISupplierModel;
        }

        private List<SupplierICS> GetAllSupplierICS()
        {
            List<SupplierICS> lstSupplierICS = new List<SupplierICS>();
            IDBContext db = DbContext;
            lstSupplierICS = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });
            db.Close();

            return lstSupplierICS;
        }

        public ActionResult PartialPopupUtilPlane()
        {
            ViewData["GridPart"] = GetAllPart();
            ViewData["GridDock"] = GetAllDock();
            ViewData["GridRcv"] = GetAllTax();

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "InvoiceAmountDifference", "OIHSupplierOptionUtilPlane");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }
            return PartialView("PopupNew", null);
        }

        public ActionResult PartialHeaderPartLookupGridUtilPlane()
        {
            TempData["GridName"] = "OIHPartOptionUtilPlane";
            ViewData["GridPart"] = GetAllPart();

            return PartialView("GridLookup/PartialGrid", ViewData["GridPart"]);
        }

        public ActionResult PartialHeaderDockGridHeaderUtilPlane()
        {
            //TempData["GridName"] = "OIHDockCodeUtilPlane";
            //List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DOCK_CODE", "InvoiceAmountDifference", "OIHDockCodeUtilPlane");

            //ViewData["GridDock"] = docks;

            //return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);
            TempData["GridName"] = "OIHSupplierOptionUtilPlane";

           // List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPPLIER_CODE", "InvoiceAmountDifference", "OIHSupplierOptionUtilPlane");
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAmountDifference", "OIHSupplierOptionUtilPlane");
                        
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);
        }

        public ActionResult PartialGridLookUpDockUtilPlane()
        {
            TempData["GridName"] = "UtilPlaneHDockGridLookup";
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DOCK_CODE", "InvoiceAmountDifference", "UtilPlaneHDockGridLookup");

            ViewData["GridDock"] = docks;

            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);

        }


        private List<DockData> GetAllDock()
        {
            List<DockData> PartIndoDockModel = new List<DockData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return PartIndoDockModel;
        }

        /*
        Date: January 8, 2019
        Note: update code for deploy

        start
        */
        public ActionResult AddNewInvoiceAmountDifference(string SupplierCode, string InvoiceNumber, string InvoiceAmount, string IppcsInvoiceAmount,
            string DiffInvoiceAmount, string TaxInvoiceNumber, string TaxAmount, string IppcsTaxAmount, string User)
        {
            string ResultQuery = "";


            User = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                string CheckData;


                CheckData = db.ExecuteScalar<string>("InsertInvoiceAmountDifference", new object[]
                {
                   SupplierCode,
                   InvoiceNumber,
                   InvoiceAmount,
                   IppcsInvoiceAmount,
                   DiffInvoiceAmount,
                   TaxInvoiceNumber,
                   TaxAmount,
                   IppcsTaxAmount,
                   User
                });

                if (CheckData == "Alreadyexists")
                {
                    ResultQuery = " Tax Data Already Exists, Please Input Other Data";
                }
                else
                {
                    TempData["GridName"] = "PartialDetailUtilPlane";
                    ResultQuery = " Process succesfully..";
                }

                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }


        /* ------ End ------- */

        public ActionResult DeleteInvoiceAmountDifference(string SupplierCode, string TaxNumber, string User)
        {
            string ResultQuery = "Process succesfully";

            User = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteInvoiceAmountDifference", new object[]
                {
                   SupplierCode,
                   TaxNumber,
                   User

                });
                TempData["GridName"] = "PartialDetailUtilPlane";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

    }
}