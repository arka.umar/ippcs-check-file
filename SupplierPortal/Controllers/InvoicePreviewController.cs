﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.InvoicePreview;
using System.Threading;

namespace Portal.Controllers
{
    public class InvoicePreviewController : BaseController
    {
        public InvoicePreviewController()
            : base("Invoice Preview")
        {
        }
        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            InvoicePreviewModel mdl = new InvoicePreviewModel();

            mdl.InvoicePreviews.Add(new InvoicePreview
            {
                InvoiceNo = "1234567878787" ,
                InvoiceDate = DateTime.Now,
                TurnOver = 1,
                TotalManifest =2 ,
                StampIncluded = true,
                TotalInvoiceAmount = 3,
                DownloadDetail = true,
                TaxInvoiceNo = "9090909898989",
                TaxInvoiceDate = DateTime.Now,
                TaxAmount = 5,
                RetroAmount = 6
            });
            mdl.InvoicePreviewsII.Add(new InvoicePreview
            {
                InvoiceNo = "1234567878788",
                InvoiceDate = DateTime.Now,
                TurnOver = 1,
                TotalManifest = 2,
                StampIncluded = true,
                TotalInvoiceAmount = 3,
                DownloadDetail = true,
                TaxInvoiceNo = "9090909898989",
                TaxInvoiceDate = DateTime.Now,
                TaxAmount = 5,
                RetroAmount = 6
            });
            Model.AddModel(mdl);
        }

        public ActionResult InvoiceCreationPartial()
        {
            return PartialView("InvoiceCreationPartial", Model);
        }
        public ActionResult InvoicePreviewIIPartial()
        {
            return PartialView("InvoicePreviewIIPartial", Model);
        }
        public ActionResult InvoicePreviewIPartial()
        {
            return PartialView("InvoicePreviewIPartial", Model);
        }
        public ActionResult InvoicePreviewPopupPartial()
        {
            return PartialView("InvoicePreviewPopupPartial", Model);
        }
    }
}
