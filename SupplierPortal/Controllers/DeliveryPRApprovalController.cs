﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.DeliveryPRApproval;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
namespace Portal.Controllers
{
    public class DeliveryPRApprovalController : BaseController
    {
        public DeliveryPRMaster modelExport;

        public DeliveryPRApprovalController()
            : base("DeliveryPRApproval", "Delivery PR Approval")
           
       {

       }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            DeliveryPRModel mdl = new DeliveryPRModel();

            mdl.DeliveryPRApproval = DeliveryPRMaster();
            Model.AddModel(mdl);
           
        }

        public ActionResult Detail()
        {
            return PartialView("Detail");
        }

        public ActionResult HeaderSearch()
        {
            return PartialView("HeaderSearch");
        }

        public ActionResult popupdetail()
        {
            return PartialView("gridpopup");
        }

        public ActionResult popupsearch()
        {
            return PartialView("Headerpopup");
        }

        protected List<DeliveryPRMaster> DeliveryPRMaster()
        {
            List<DeliveryPRMaster> LogList = new List<DeliveryPRMaster>();

            LogList.Add(new DeliveryPRMaster()
            {
                PRNumber = "PR001",
                statusSH = "1",
                statusDpH = "1",
                statusDH = "1",
                CurrentPIC="Ondi Simamora",
                PRDate = "2013.09.21 18:32",
                Trucking="Mitra Mandiri Logistic",
                ReleaseDateSH = "2013.09.21 18:32",
                ReleaseDateDH = "2013.09.21 18:32",
                ReleaseDateDpH = "2013.09.21 18:32",
                Notice="1",
                
            });
            return LogList;

        }

      

       
    }
}
