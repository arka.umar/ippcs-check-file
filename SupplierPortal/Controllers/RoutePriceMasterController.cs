﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RoutePriceMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class RoutePriceMasterController : BaseController
    {
       public RoutePriceMaster modelExport;

       public RoutePriceMasterController()
           : base("RoutePriceMaster", "Route Price Master")
           
       {

       }

        
        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
            ViewData["LogisticPartnerDataSearch"] = GetAllLogisticPartners();
        }

        public ActionResult Back()
        {
            return RedirectToAction("Index", "RoutePriceMaster");
        }

        public ActionResult IndexHeader()
        {

            return PartialView("IndexHeader", null);
        }

        public ActionResult RoutePriceMasterTypeLookupCallback()
        {
            TempData["Grid"] = "RoutePriceMaster";
            return PartialView("GridLookup/PartialGrid", null);
        }

        public ActionResult LogisticPartnerLookupCallback()
        {
            TempData["Grid"] = "LogisticPartner";
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public List<LogisticPartner> GetAllLogisticPartners()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();
            db.Close();
            return listLogisticPartner;
        }

        public ActionResult GridRoutePriceMaster(string Route_cd, string LP_Cd)
        {
            Route_cd = ((Route_cd == null) ? "" : Route_cd);
            LP_Cd = ((LP_Cd == null) ? "" : LP_Cd);

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            RoutePriceMasterModel rpm = new RoutePriceMasterModel();
            rpm.RoutePriceMaster = db.Query<RoutePriceMaster>("GetRoutePriceMasterData", new object[] { Route_cd.ToUpper(), LP_Cd }).ToList<RoutePriceMaster>();
            Model.AddModel(rpm);

            db.Close();

            return PartialView("GridRoutePriceMaster", Model);
        }

        public ActionResult LPPartialAdditional()
        {
            ViewData["LogisticPartnerData"] = ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult LPPartialAdditionalSearch()
        {
            ViewData["LogisticPartnerDataSearch"] = ViewData["LogisticPartnerDataSearch"] = GetAllLogisticPartners();
            return PartialView("grlLPHeaderSearch", ViewData["LogisticPartnerDataSearch"]);
        }

        public ContentResult AddingRoutePrice(string Route_Cd, string Route_Name, string LP_Cd, string Price, string Valid_From, string Valid_To)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isExistsRoute = 0;
            int isExistsLP = 0;
            int isError = 0;
            string errorMessage = "";



            try
            {
                //---=== Check if exists on TB_M_DELIVERY_ROUTE GetValidationRoutePriceStat
                CheckResult QueryLog = db.SingleOrDefault<CheckResult>("GetRoutePriceMasterDataCheck", new object[] { 
                    "R", Route_Cd
                });
                isExistsRoute = QueryLog.result;
                //---=== Check if exists on TB_M_LOGISTIC_PARTNER
                QueryLog = db.SingleOrDefault<CheckResult>("GetRoutePriceMasterDataCheck", new object[] { 
                    "L", LP_Cd
                });
                isExistsLP = QueryLog.result;

                if ((isExistsRoute == 1) && (isExistsLP == 1))
                {
                    db.Execute("SetMasterRoutePrice", new object[] { 
                            Route_Cd.ToUpper(), Route_Name, LP_Cd, Price, Valid_From, Valid_To, AuthorizedUser.Username
                        });
                    errorMessage = "Master data saved.";
                }
                else
                {
                    isError = 1;
                    errorMessage = ((isExistsRoute==0)?"Route":"");
                    errorMessage = ((isExistsLP == 0) ? ((errorMessage != "") ? errorMessage + " and LP" : "LP") : errorMessage);
                    errorMessage = "Error : " + errorMessage + " not exists on master table.";
                }
                
            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = "Failed on saving process.";
            }   

            return Content(((isError==1)?"Error|":"Sucess|")+errorMessage);
        }

    }

    }

