﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Xml;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.ServiceModel;

using Portal.Models;
using Portal.Models.GoodsReceiptInquiry;
using Portal.Models.Globals;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Html.Helper.DevExpress;
using Toyota.Common.Web.Util.Converter;
using Toyota.Common.Util.Text;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Service;
using Toyota.Common.Web.Log;
using Toyota.Common.Task;
using Toyota.Common.Task.External;

using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using Toyota.Common.Web.Task;



namespace Portal.Controllers
{
    public class GoodsReceiptInquiryController : BaseController
    {
         
        private IDBContext DbContext
        {
            get
            { 
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public GoodsReceiptInquiryController()
            : base("Goods Receipt Monitoring")
        {
            PageLayout.UseMessageBoard = true;
        }

        #region Parameter Properties
        List<Dock> _dockModel = null;
        private List<Dock> _DockModel
        {
            get
            {
                if (_dockModel == null)
                    _dockModel = new List<Dock>();

                return _dockModel;
            }
            set
            {
                _dockModel = value;
            }
        }

        List<Supplier> _supplierModel = null;
        private List<Supplier> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<Supplier>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<GoodsReceiptManifestStatus> _manifestStatusModel = null;
        private List<GoodsReceiptManifestStatus> _ManifestStatusModel
        {
            get
            {
                if (_manifestStatusModel == null)
                    _manifestStatusModel = new List<GoodsReceiptManifestStatus>();

                return _manifestStatusModel;
            }
            set
            {
                _manifestStatusModel = value;
            }
        }

        GoodsReceiptManifestStatusSummary _manifestStatusSummaryModel = null;
        private GoodsReceiptManifestStatusSummary _ManifestStatusSummaryModel
        {
            get
            {
                if (_manifestStatusSummaryModel == null)
                    _manifestStatusSummaryModel = new GoodsReceiptManifestStatusSummary();

                return _manifestStatusSummaryModel;
            }
            set
            {
                _manifestStatusSummaryModel = value;
            }
        }
        
        List<GoodsReceiptInquiryData> _goodsReceiptInquiryDataModel = null;
        private List<GoodsReceiptInquiryData> _GoodsReceiptInquiryDataModel
        {
            get
            {
                if (_goodsReceiptInquiryDataModel == null)
                    _goodsReceiptInquiryDataModel = new List<GoodsReceiptInquiryData>();

                return _goodsReceiptInquiryDataModel;
            }
            set
            {
                _goodsReceiptInquiryDataModel = value;
            }
        }

        List<GoodsReceiptInquiryManifestDetailData> _goodsReceiptInquiryManifestDetailDataModel = null;
        private List<GoodsReceiptInquiryManifestDetailData> _GoodsReceiptInquiryManifestDetailDataModel
        {
            get
            {
                if (_goodsReceiptInquiryManifestDetailDataModel == null)
                    _goodsReceiptInquiryManifestDetailDataModel = new List<GoodsReceiptInquiryManifestDetailData>();

                return _goodsReceiptInquiryManifestDetailDataModel;
            }
            set
            {
                _goodsReceiptInquiryManifestDetailDataModel = value;
            }
        }
        #endregion
        
        protected override void StartUp() { }

        [Obsolete]
        protected override void Init()
        {
            PageLayout.UseSlidingRightPane = true;
            PageLayout.UseSlidingLeftPane = true;
        }

        #region Goods Receipt Inquiry Controllers
        #region View Controllers
        public ActionResult PartialHeaderGoodsReceiptInquiry()
        {
            #region Required model for [Dock lookup]
            List<Dock> DockCodes = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DockCode", "GoodsReceiptInquiry", "GRIHDockLookup");
            _DockModel = DockCodes;
            //_DockModel = GetAllDock();
            Model.AddModel(_DockModel);
            #endregion

            #region Required model for [Supplier lookup]
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "GoodsReceiptInquiry", "GRIHSupplierLookup");

            if (suppliers.Count > 0)
            {
                _SupplierModel = suppliers;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }

            Model.AddModel(_SupplierModel);
            #endregion

            #region Required model for [Status lookup]
            _ManifestStatusModel = GetAllManifestStatus();
            Model.AddModel(_ManifestStatusModel);
            #endregion

            return PartialView("GoodsReceiptInquiryHeaderPartial", Model);
        }

        public ActionResult PartialHeaderDockLookupGoodsReceiptInquiry()
        {
            #region Required model for [Dock lookup]
            TempData["GridName"] = "GRIHDockLookup";
            List<Dock> DockCodes = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DockCode", "GoodsReceiptInquiry", "GRIHDockLookup");
            _DockModel = DockCodes;
            //_DockModel = GetAllDock();
            #endregion

            return PartialView("GridLookup/PartialGrid", _DockModel);
        }

        public ActionResult PartialHeaderSupplierLookupGoodsReceiptInquiry()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "GRIHSupplierLookup";

            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "GoodsReceiptInquiry", "GRIHSupplierLookup");

            if (suppliers.Count > 0)
            {
                _SupplierModel = suppliers;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialHeaderStatusLookupGoodsReceiptInquiry()
        {
            #region Required model for [Status lookup]
            TempData["GridName"] = "GRIHStatusLookup";
            _ManifestStatusModel = GetAllManifestStatus();
            #endregion

            return PartialView("GridLookup/PartialGrid", _ManifestStatusModel);
        }

        public ActionResult PartialDetailGoodsReceiptInquiry()
        {
            DateTime currentDate = DateTime.Now;

            // get current user login detail
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "GoodsReceiptInquiry", "GRIHSupplierLookup");
            
            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {
                /*
                 * Man ini yg soal supplier plant code juga ya
                 */

                supplierCode = suppliers[0].SUPPLIER_CODE;
            }
            
            #region Required model for [Goods receipt inquiry gridview]
            //edit riani (20220906) --> ganti ke system master
            /*
            // default value for manifest receive flag is 1 (scanned ok)
            _GoodsReceiptInquiryDataModel = GetAllGoodsReceiptInquiry(
                String.IsNullOrEmpty(Request.Params["ArrivalTimeFrom"]) ? currentDate.AddDays(-1).ToString("yyyy-MM-dd") : Request.Params["ArrivalTimeFrom"],
                String.IsNullOrEmpty(Request.Params["ArrivalTimeTo"]) ? currentDate.ToString("yyyy-MM-dd") : Request.Params["ArrivalTimeTo"],
                String.IsNullOrEmpty(Request.Params["DockCode"]) ? "" : Convert.ToString(Request.Params["DockCode"]).Replace(";", ","),
                
                //String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ","),
                String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode,

                String.IsNullOrEmpty(Request.Params["RcvPlantCode"]) ? "" : Request.Params["RcvPlantCode"],
                String.IsNullOrEmpty(Request.Params["RouteRate"]) ? "" : Request.Params["RouteRate"],
                String.IsNullOrEmpty(Request.Params["ManifestNo"]) ? "" : Request.Params["ManifestNo"],
                String.IsNullOrEmpty(Request.Params["ManifestReceiveFlag"]) ? "" : GetManifestReceiveFlagCodeList(Request.Params["ManifestReceiveFlag"]),
                String.IsNullOrEmpty(Request.Params["OrderNo"]) ? "" : Request.Params["OrderNo"]
                );
            */
            _GoodsReceiptInquiryDataModel = SysMaster_GetAllGoodsReceiptInquiry(
                String.IsNullOrEmpty(Request.Params["ArrivalTimeFrom"]) ? currentDate.AddDays(-1).ToString("yyyy-MM-dd") : Request.Params["ArrivalTimeFrom"],
                String.IsNullOrEmpty(Request.Params["ArrivalTimeTo"]) ? currentDate.ToString("yyyy-MM-dd") : Request.Params["ArrivalTimeTo"],
                String.IsNullOrEmpty(Request.Params["DockCode"]) ? "" : Convert.ToString(Request.Params["DockCode"]).Replace(";", ","),
                
                //String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ","),
                String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode,

                String.IsNullOrEmpty(Request.Params["RcvPlantCode"]) ? "" : Request.Params["RcvPlantCode"],
                String.IsNullOrEmpty(Request.Params["RouteRate"]) ? "" : Request.Params["RouteRate"],
                String.IsNullOrEmpty(Request.Params["ManifestNo"]) ? "" : Request.Params["ManifestNo"],
               String.IsNullOrEmpty(Request.Params["ManifestReceiveFlag"]) ? "" : Request.Params["ManifestReceiveFlag"],
                String.IsNullOrEmpty(Request.Params["OrderNo"]) ? "" : Request.Params["OrderNo"]
                );
            Model.AddModel(_GoodsReceiptInquiryDataModel);
            #endregion
            
            // get goods receipt manifest status summary
            //_ManifestStatusSummaryModel.ORDERED_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "0").Count();
            //_ManifestStatusSummaryModel.DELIVERED_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "1").Count();
            //_ManifestStatusSummaryModel.SCANNED_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "2").Count();
            //_ManifestStatusSummaryModel.APPROVED_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "3").Count();
            //_ManifestStatusSummaryModel.POSTED_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "4").Count();
            //_ManifestStatusSummaryModel.ERROR_POSTING_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "5").Count();
            //_ManifestStatusSummaryModel.INVOICED_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "6").Count();
            //_ManifestStatusSummaryModel.PAID_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "7").Count();
            //_ManifestStatusSummaryModel.CANCELLED_SUMMARY = _GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "8").Count();
            //Model.AddModel(_ManifestStatusSummaryModel);

            return PartialView("GoodsReceiptInquiryDetailPartial", Model);
        }

        [HttpPost]
        public ActionResult PartialSlidingRightPaneContent(
            String ArrivalTimeFrom = "",
            String ArrivalTimeTo = "",
            String DockCode = "",
            String SupplierCode = "",
            String RcvPlantCode = "",
            String RouteRate = "",
            String ManifestNo = "",
            String ManifestReceiveFlag = "",
            String OrderNo = "")
        {
            _GoodsReceiptInquiryDataModel = GetAllGoodsReceiptInquiry(                
                ArrivalTimeFrom,
                ArrivalTimeTo,
                DockCode,
                SupplierCode,
                RcvPlantCode,
                RouteRate,
                ManifestNo,
                GetManifestReceiveFlagCodeList(ManifestReceiveFlag),
                OrderNo
                );

            List<String> summaryList = new List<String>();
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "0").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "1").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "2").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "3").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "4").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "5").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "6").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "7").Count()));
            summaryList.Add(Convert.ToString(_GoodsReceiptInquiryDataModel.Where(g => g.MANIFEST_RECEIVE_FLAG == "8").Count()));

            String summary = String.Join(";", summaryList.ToArray());

            return Json(
                new
                {
                    summary = summary
                });
        }
        #endregion

        #region Database Controllers
        private List<Dock> GetAllDock()
        {
            List<Dock> dockModel = new List<Dock>();
            IDBContext db = DbContext;
            try
            {
                dockModel = db.Fetch<Dock>("GetAllDock", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return dockModel;
        }

        private List<Supplier> GetAllSupplier()
        {
            List<Supplier> supplierModel = new List<Supplier>();
            IDBContext db = DbContext;
            try
            {
                supplierModel = db.Fetch<Supplier>("GetAllSupplierForecast", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return supplierModel;
        }

        private List<GoodsReceiptManifestStatus> GetAllManifestStatus()
        {
            List<GoodsReceiptManifestStatus> statusManifestModel = new List<GoodsReceiptManifestStatus>();
            IDBContext db = DbContext;
            try
            {
                statusManifestModel = db.Fetch<GoodsReceiptManifestStatus>("GetAllManifestStatus", new object[] { "" });
            }
            catch (Exception exc) { throw exc; } 
            db.Close(); 
            return statusManifestModel;
        }

        private List<GoodsReceiptInquiryData> GetAllGoodsReceiptInquiry(
            String arrivalTimeFrom = "",
            String arrivalTimeTo = "",
            String dockCode = "",
            String supplierCode = "",
            String rcvPlantCode = "",
            String routeRate = "",
            String manifestNo = "",
            String manifestReceiveFlag = "",
            String orderNo = ""
            )
        {
            List<GoodsReceiptInquiryData> goodsReceiptInquiryDataModel = new List<GoodsReceiptInquiryData>();
            string DockCodeLDAP = "";
            List<Dock> DockCodes = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DockCode", "GoodsReceiptInquiry", "GRIHDockLookup");
            foreach (Dock d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }
            
            IDBContext db = DbContext;
            try
            {

                goodsReceiptInquiryDataModel = db.Query<GoodsReceiptInquiryData>(
                    "GetAllGoodsReceiptInquiry",
                    new object[] { 
                        arrivalTimeFrom,
                        arrivalTimeTo,
                        dockCode,
                        supplierCode,
                        rcvPlantCode,
                        routeRate,
                        manifestNo,
                        manifestReceiveFlag,
                        orderNo,
                        DockCodeLDAP
                        }).ToList<GoodsReceiptInquiryData>();
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return goodsReceiptInquiryDataModel;
        }
        #region add riani (20220906)
        private List<GoodsReceiptInquiryData> SysMaster_GetAllGoodsReceiptInquiry( //get by using system master
            String arrivalTimeFrom = "",
            String arrivalTimeTo = "",
            String dockCode = "",
            String supplierCode = "",
            String rcvPlantCode = "",
            String routeRate = "",
            String manifestNo = "",
            String manifestReceiveFlag = "",
            String orderNo = ""
            )
        {
            List<GoodsReceiptInquiryData> goodsReceiptInquiryDataModel = new List<GoodsReceiptInquiryData>();
            string DockCodeLDAP = "";
            List<Dock> DockCodes = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DockCode", "GoodsReceiptInquiry", "GRIHDockLookup");
            foreach (Dock d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            IDBContext db = DbContext;
            try
            {

                goodsReceiptInquiryDataModel = db.Query<GoodsReceiptInquiryData>(
                    "SysMaster_GetAllGoodsReceiptInquiry",
                    new object[] { 
                        arrivalTimeFrom,
                        arrivalTimeTo,
                        dockCode,
                        supplierCode,
                        rcvPlantCode,
                        routeRate,
                        manifestNo,
                        manifestReceiveFlag,
                        orderNo,
                        DockCodeLDAP
                        }).ToList<GoodsReceiptInquiryData>();
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return goodsReceiptInquiryDataModel;
        }
        #endregion
        public ContentResult ApproveGoodsReceiptInquiry(String allParameterList)
        {
            char[] rowSeparator = { ';' };
            char[] fieldSeparator = { '|' };
            string[] rowList;
            string[] fieldList;
            string resultMessage = "";
            int postingAmount = 0;
            int errorAmount = 0;
            int i = 0;

            char[] splitter = { '~' };
            string parameterList;
            string[] parameterPerSplit = allParameterList.Split(splitter);


            List<IDictionary<string, string>> dataList = new List<IDictionary<string, string>>();
            IDictionary<string, string> dataMap;
            List<String> manifestNoList = new List<String>();
            List<GoodsReceiptInquiryData> noDataManifestList = new List<GoodsReceiptInquiryData>();
            List<String> errorManifestNoList = new List<String>();

            IDBContext db = DbContext;

            String message = "";
            String location = "";
            String module = "4";
            String function = "42001";
            String username = AuthorizedUser.Username;
            String processID = "";


            if ((parameterPerSplit != null) && (parameterPerSplit.Length > 0))
            {
                for (int j = 0; j < parameterPerSplit.Length; j++)
                {
                    i = 0; //reset pointer loop

                    parameterList = parameterPerSplit[j];



                    rowList = parameterList.Split(rowSeparator);

                    resultMessage = "";

                    #region inside

                    postingAmount = 0;
                    errorAmount = 0;

                    if ((rowList != null) && (rowList.Length > 0))
                    {
                        dataList = new List<IDictionary<string, string>>();
                        manifestNoList = new List<String>();

                        for (i = 0; i < rowList.Length; i++)
                        {
                            fieldList = rowList[i].Split(fieldSeparator);
                            dataMap = new Dictionary<string, string>();
                            dataMap.Add("DockCode", fieldList[0]);
                            dataMap.Add("SupplierCode", fieldList[1]);
                            dataMap.Add("RecPlantCode", fieldList[2]);
                            dataMap.Add("ManifestNo", fieldList[3]);
                            dataMap.Add("OrderNo", fieldList[4]);
                            dataList.Add(dataMap);

                            manifestNoList.Add(fieldList[3]);
                        }

                        postingAmount = dataList.Count;

                        try
                        {
                            #region Posting Old Structure
                            //List<GoodsReceiptICSTransfer> tempList;
                            //String processId = "";
                            //Boolean isValid = true;

                            //foreach (IDictionary<string, string> map in dataList)
                            //{
                            //    tempList = db.Fetch<GoodsReceiptICSTransfer>("ICSGeneratePostingFileGR", new object[] { AuthorizedUser.Username, map["ManifestNo"], map["SupplierCode"], map["RecPlantCode"], map["DockCode"] });
                            //    if ((tempList.Count == 0) || (tempList == null))
                            //    {   
                            //        isValid = false;
                            //    }
                            //    else
                            //    {
                            //        if (tempList.Count > 0)
                            //        {
                            //            processId = tempList[0].processID;                                
                            //        }
                            //    }
                            //}

                            //if (isValid)
                            //{
                            //    #region QA Deployment
                            //    //bool isLocked;
                            //    //isLocked = db.Query<bool>("InsertLogProcessLockByFunctionNo",
                            //    //            new object[] {
                            //    //                "Approve"
                            //    //                ,AuthorizedUser.Username
                            //    //}).SingleOrDefault<bool>();

                            //    //if (isLocked)
                            //    //{
                            //    //    resultMessage = "Already have running process for GR approve process by " + AuthorizedUser.Username + ". Please wait for completeness previous process.";
                            //    //}
                            //    //else
                            //    //{
                            //    //    //resultMessage = "Approval in Progress, Please wait a moment for the result! Check following process Id : " + processId;
                            //    //    resultMessage = "Approval in Progress, Please wait a moment for the result!";

                            //    //    // update initial status GR
                            //    //    foreach (IDictionary<string, string> map2 in dataList)
                            //    //    {
                            //    //        db.ExecuteScalar<string>(
                            //    //                    "ApproveGoodsReceiptInquiry",
                            //    //                    new object[] {
                            //    //                    map2["DockCode"],   // dockCode
                            //    //                    map2["SupplierCode"],   // supplierCode
                            //    //                    map2["RecPlantCode"],   // rcvPlantCode
                            //    //                    map2["ManifestNo"],   // manifestNo
                            //    //                    "3",
                            //    //                    map2["OrderNo"],   // orderNo 
                            //    //                    AuthorizedUser.Username
                            //    //            });
                            //    //    }

                            //    //    // call web service
                            //    //    ICSGeneratePostingFileGR(dataList, "Approve");
                            //    //}
                            //    #endregion

                            //    #region Production Deployment
                            //    //resultMessage = "Approval Success";

                            //    //foreach (IDictionary<string, string> map2 in dataList)
                            //    //{
                            //    //    db.ExecuteScalar<string>(
                            //    //                "ApproveGoodsReceiptInquiry",
                            //    //                new object[] {
                            //    //                map2["DockCode"],   // dockCode
                            //    //                map2["SupplierCode"],   // supplierCode
                            //    //                map2["RecPlantCode"],   // rcvPlantCode
                            //    //                map2["ManifestNo"],   // manifestNo
                            //    //                "3",
                            //    //                map2["OrderNo"],   // orderNo 
                            //    //                AuthorizedUser.Username
                            //    //        });
                            //    //}
                            //    #endregion
                            //}
                            //else
                            //{
                            //    resultMessage = "Can not Post selected Manifest [Manifest No] because either Manifest not Scan, Posted or all quantity are zero";
                            //}
                            #endregion

                            #region Posting New Structure
                            resultMessage = "Successfully send data to ICS";

                            #region Initial Phase : update initial status GR



                            /*foreach (IDictionary<string, string> map2 in dataList)
                            {
                                db.ExecuteScalar<string>(
                                            "ApproveGoodsReceiptInquiry",
                                            new object[] {
                                            map2["DockCode"],   // dockCode
                                            map2["SupplierCode"],   // supplierCode
                                            map2["RecPlantCode"],   // rcvPlantCode
                                            map2["ManifestNo"],   // manifestNo
                                            "3",
                                            map2["OrderNo"],   // orderNo 
                                            AuthorizedUser.Username
                                    });
                            }*/
                            db.ExecuteScalar<string>(
                                "ApproveGoodsReceiptInquiry",
                                new object[] {
                                "",   // dockCode
                                "",   // supplierCode
                                "",   // rcvPlantCode
                                String.Join(";", manifestNoList.ToArray()),   // manifestNo
                                "3",
                                "",   // orderNo 
                                AuthorizedUser.Username
                            });
                            


                            #endregion

                            #region Phase 1 : generate process id
                            message = "Start Process Goods Receipt Posting from IPPCS";
                            location = "Goods Receipt Inquiry";

                            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                            #endregion

                            #region Phase 2 : generate posting file gr
                            message = "Start Process Generate Data Posting";
                            location = "Generate Posting File";

                            db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00002INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                            //List<GoodsReceiptInquiryData> noDataManifestList = new List<GoodsReceiptInquiryData>();
                            //noDataManifestList = db.ExecuteScalar<List<GoodsReceiptInquiryData>>(
                            //     "ICSGeneratePostingFileGR",
                            //     new object[] { 
                            //        username, 
                            //        String.Join(";", manifestNoList.ToArray()), 
                            //        "", 
                            //        "", 
                            //        "",
                            //        processID
                            //    });
                            noDataManifestList = new List<GoodsReceiptInquiryData>();

                            noDataManifestList = db.Fetch<GoodsReceiptInquiryData>(
                                 "ICSGeneratePostingFileGR",
                                 new object[] { 
                            username, 
                            String.Join(";", manifestNoList.ToArray()), 
                            "", 
                            "", 
                            "",
                            processID
                        });

                            message = "Finish Process Generate Data Posting";

                            db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00003INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                            //Remark by : FID.Goldy
                            //Remark Date : 27 December 2013
                            //if (noDataManifestList != null)
                            //{
                            //    List<String> noDataManifestNoList = new List<String>();
                            //    foreach (GoodsReceiptInquiryData noDataManifest in noDataManifestList)
                            //    {
                            //        db.ExecuteScalar<string>(
                            //                    "ApproveGoodsReceiptInquiry",
                            //                    new object[] {
                            //                            noDataManifest.DOCK_CD,   // dockCode
                            //                            noDataManifest.SUPPLIER_CD,   // supplierCode
                            //                            noDataManifest.RCV_PLANT_CD,   // rcvPlantCode
                            //                            noDataManifest.MANIFEST_NO,   // manifestNo
                            //                            "5",
                            //                            noDataManifest.ORDER_NO,   // orderNo 
                            //                            AuthorizedUser.Username
                            //                    });

                            //        noDataManifestNoList.Add(noDataManifest.MANIFEST_NO);
                            //    }

                            //    if (noDataManifestNoList.Count != 0)
                            //    {
                            //        errorAmount = noDataManifestNoList.Distinct().Count();

                            //        message = "Fail in Generate Data Posting Manifest List : " + String.Join(";", noDataManifestNoList.ToArray()).Distinct();

                            //        db.ExecuteScalar<string>(
                            //            "GenerateProcessId",
                            //                new object[] { 
                            //                message,     // what
                            //                username,     // user
                            //                location,     // where
                            //                processID,     // pid OUTPUT
                            //                "MPCS00006ERR",     // id
                            //                "",     // type
                            //                module,     // module
                            //                function,     // function
                            //                "0"      // sts
                            //            });
                            //    }

                            //    //resultMessage = resultMessage + ". Success = " + (postingAmount - errorAmount) + ". Error = " + errorAmount;
                            //    resultMessage = resultMessage + ". Please check Process ID: " + processID ;
                            //}
                            //else
                            //{
                            //    //resultMessage = resultMessage + ". Success = " + postingAmount;
                            //    resultMessage = resultMessage + ". Please check Process ID: " + processID ;
                            //}

                            //End Of Remark by : FID.Goldy
                            //Remark Date : 27 December 2013
                            #endregion

                            #region Phase 3 : insert process id into ics queue
                            //message = "Start Process Insert ICS Queue";
                            //location = "Insert ICS Queue";

                            //db.ExecuteScalar<string>(
                            //    "GenerateProcessId",
                            //    new object[] { 
                            //    message,     // what
                            //    username,     // user
                            //    location,     // where
                            //    processID,     // pid OUTPUT
                            //    "MPCS00002INF",     // id
                            //    "",     // type
                            //    module,     // module
                            //    function,     // function
                            //    "0"      // sts
                            //});

                            //db.ExecuteScalar<string>(
                            //    "InsertICSQueue",
                            //    new object[] { 
                            //    module,     // ippcs module
                            //    function,     // ippcs function
                            //    processID,     // process id
                            //    "0",     // process status
                            //    username,     // created by
                            //    DateTime.Now,     // created dt
                            //});

                            //message = "Finish Process Insert ICS Queue";

                            //db.ExecuteScalar<string>(
                            //    "GenerateProcessId",
                            //    new object[] { 
                            //    message,     // what
                            //    username,     // user
                            //    location,     // where
                            //    processID,     // pid OUTPUT
                            //    "MPCS00003INF",     // id
                            //    "",     // type
                            //    module,     // module
                            //    function,     // function
                            //    "0"      // sts
                            //});
                            #endregion
                            #endregion
                        }
                        catch (Exception exc)
                        {
                            //resultMessage = "Approval Failed for Process ID : " + processID + ", please contact IPPCS administrator!";
                            resultMessage = "Sending process failed";

                            // if web service process fails
                            errorManifestNoList = new List<String>();
                            foreach (IDictionary<string, string> map in dataList)
                            {
                                db.ExecuteScalar<string>("ApproveGoodsReceiptInquiry", new object[] {
                                map["DockCode"],   // dockCode
                                map["SupplierCode"],   // supplierCode
                                map["RecPlantCode"],   // rcvPlantCode
                                map["ManifestNo"],   // manifestNo
                                "5",
                                map["OrderNo"],   // orderNo 
                                AuthorizedUser.Username
                        });

                                errorManifestNoList.Add(map["ManifestNo"]);
                            }

                            message = exc.Message + ". Manifest List : " + String.Join(";", errorManifestNoList.ToArray()); ;

                            db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00008ERR",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


                            break; //quit loop
                        }

                        
                    }

                    #endregion
                }
            }

            db.Close();
            return Content(resultMessage);
        }

        private void ICSGeneratePostingFileGR(List<IDictionary<string, string>> dataList, String process)
        {
            /*
             * Contoh panggil background tasknya
            */
            //IDictionary<string, object> parameters = new Dictionary<string, object>();
            //parameters["DataList"] = dataList;
            //parameters["Process"] = process;
            //parameters["User"] = SessionState.GetAuthorizedUser();
            //InternalBackgroundTaskQueue.GetInstance().Execute("GoodsReceiptPosting", parameters);
            
            BackgroundTaskService service = BackgroundTaskService.GetInstance();

            BackgroundTaskParameter taskParameter = new BackgroundTaskParameter();
            taskParameter.Add("DataList", dataList, typeof(List<IDictionary<string, string>>));
            taskParameter.Add("Process", process);
            taskParameter.Add("Username", AuthorizedUser.Username);

            BackgroundTask immediateTask = new ExternalBackgroundTask()
            {
                Id = "GRPTR_ID",
                Name = "GoodsReceiptPostingTaskRuntime",
                Description = "Background Task Posting Goods Receipt",
                FunctionName = "GRPTR_BT",
                Submitter = new Toyota.Common.Credential.User() { Username = AuthorizedUser.Username },
                Type = TaskType.Immediate,
                Range = new TaskRange()
                {
                    Time = new TimeSpan(1, 54, 0)
                },
                //Parameters = new string[] { JSON.ToString<List<IDictionary<string, string>>>(dataList), process, AuthorizedUser.Username },
                Parameters = taskParameter,
                Command = service.GetTaskPath("GoodsReceiptPostingTaskRuntime")
            };

            //immediateTask.Parameters.Add("DataList", dataList, typeof(List<IDictionary<string, string>>));
            //immediateTask.Parameters.Add("Process", process);
            //immediateTask.Parameters.Add("Username", AuthorizedUser.Username);

            service.Register(immediateTask);
        }

        public ContentResult CancelGoodsReceiptInquiry(String parameterList)
        {
            char[] rowSeparator = { ';' };
            char[] fieldSeparator = { '|' };
            string[] rowList = parameterList.Split(rowSeparator);
            string[] fieldList;
            string resultMessage = "";

            int postingAmount = 0;
            int errorAmount = 0;

            if ((rowList != null) && (rowList.Length > 0))
            {
                List<IDictionary<string, string>> dataList = new List<IDictionary<string, string>>();
                IDictionary<string, string> dataMap;
                List<String> manifestNoList = new List<String>();

                for (int i = 0; i < rowList.Length; i++)
                {
                    fieldList = rowList[i].Split(fieldSeparator);
                    dataMap = new Dictionary<string, string>();
                    dataMap.Add("DockCode", fieldList[0]);
                    dataMap.Add("SupplierCode", fieldList[1]);
                    dataMap.Add("RecPlantCode", fieldList[2]);
                    dataMap.Add("ManifestNo", fieldList[3]);
                    dataMap.Add("OrderNo", fieldList[4]);
                    dataList.Add(dataMap);

                    manifestNoList.Add(fieldList[3]);
                }

                postingAmount = dataList.Count;

                IDBContext db = DbContext;

                String message = "";
                String location = "";
                String module = "4";
                String function = "42002";
                String username = AuthorizedUser.Username;
                String processID = "";

                try
                {
                    #region Posting Old Structure
                    //List<GoodsReceiptICSTransfer> tempList;
                    //String processId = "";
                    //Boolean isValid = true;

                    //foreach (IDictionary<string, string> map in dataList)
                    //{
                    //    tempList = db.Fetch<GoodsReceiptICSTransfer>("ICSGeneratePostingFileGR", new object[] { AuthorizedUser.Username, map["ManifestNo"], map["SupplierCode"], map["RecPlantCode"], map["DockCode"] });
                    //    if ((tempList.Count == 0) || (tempList == null))
                    //    {
                    //        isValid = false;
                    //    }
                    //    else
                    //    {
                    //        if (tempList.Count > 0)
                    //        {
                    //            processId = tempList[0].processID;
                    //        }
                    //    }
                    //}

                    //if (isValid)
                    //{
                    //    #region QA Deployment
                    //    bool isLocked;
                    //    isLocked = db.Query<bool>("InsertLogProcessLockByFunctionNo",
                    //                new object[] {
                    //                    "Cancel"
                    //                    ,AuthorizedUser.Username
                    //    }).SingleOrDefault<bool>();

                    //    if (isLocked)
                    //    {
                    //        resultMessage = "Already have running process for GR cancel process by " + AuthorizedUser.Username + ". Please wait for completeness previous process.";
                    //    }
                    //    else
                    //    {
                    //        //resultMessage = "Cancellation in Progress, Please wait a moment for the result! Check following process Id : " + processId;
                    //        resultMessage = "Cancellation in Progress, Please wait a moment for the result!";

                    //        // call web service
                    //        ICSGeneratePostingFileGR(dataList, "Cancel");
                    //    }
                    //    #endregion

                    //    #region Production Deployment
                    //    //resultMessage = "Cancellation Success";

                    //    //foreach (IDictionary<string, string> map2 in dataList)
                    //    //{
                    //    //    db.ExecuteScalar<string>(
                    //    //                "CancelGoodsReceiptInquiry",
                    //    //                new object[] {
                    //    //                map2["DockCode"],   // dockCode
                    //    //                map2["SupplierCode"],   // supplierCode
                    //    //                map2["RecPlantCode"],   // rcvPlantCode
                    //    //                map2["ManifestNo"],   // manifestNo
                    //    //                "8",
                    //    //                map2["OrderNo"],   // orderNo 
                    //    //                AuthorizedUser.Username
                    //    //        });
                    //    //}
                    //    #endregion
                    //}
                    //else
                    //{
                    //    resultMessage = "Can not Post selected Manifest [Manifest No] because either Manifest not Scan, Posted or all quantity are zero";
                    //}
                    #endregion

                    #region Posting New Structure
                    resultMessage = "Cancellation is Finish";
                    
                    #region Phase 1 : generate process id
                    message = "Start Process Goods Receipt Posting from IPPCS";
                    location = "Goods Receipt Inquiry";

                    processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                    #endregion

                    #region Phase 2 : generate posting file gr
                    message = "Start Process Generate Data Posting";
                    location = "Generate Posting File";

                    db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00002INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                    List<GoodsReceiptInquiryData> noDataManifestList = new List<GoodsReceiptInquiryData>();
                    noDataManifestList = db.Fetch<GoodsReceiptInquiryData>(
                         "ICSGeneratePostingFileGR_Cancel",
                         new object[] { 
                            username, 
                            String.Join(";", manifestNoList.ToArray()), 
                            "", 
                            "", 
                            "",
                            processID
                        });

                    message = "Finish Process Generate Data Posting";

                    db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00003INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                    if (noDataManifestList != null)
                    {
                        List<String> noDataManifestNoList = new List<String>();
                        foreach (GoodsReceiptInquiryData noDataManifest in noDataManifestList)
                        {
                            db.ExecuteScalar<string>(
                                "CancelGoodsReceiptInquiry",
                                new object[] {
                                        noDataManifest.DOCK_CD,   // dockCode
                                        noDataManifest.SUPPLIER_CD,   // supplierCode
                                        noDataManifest.RCV_PLANT_CD,   // rcvPlantCode
                                        noDataManifest.MANIFEST_NO,   // manifestNo
                                        "5",
                                        noDataManifest.ORDER_NO,   // orderNo 
                                        AuthorizedUser.Username
                                });
                            

                                noDataManifestNoList.Add(noDataManifest.MANIFEST_NO);
                        }

                        if (noDataManifestNoList.Count != 0)
                        {
                            errorAmount = noDataManifestNoList.Distinct().Count();

                            message = "Fail in Generate Data Posting Manifest List : " + String.Join(";", noDataManifestNoList.ToArray()).Distinct();

                            db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                    new object[] { 
                                    message,     // what
                                    username,     // user
                                    location,     // where
                                    processID,     // pid OUTPUT
                                    "MPCS00006ERR",     // id
                                    "",     // type
                                    module,     // module
                                    function,     // function
                                    "0"      // sts
                                });
                        }

                        resultMessage = resultMessage + ". Success = " + (postingAmount - errorAmount) + ". Error = " + errorAmount;
                    }
                    else
                    {
                        resultMessage = resultMessage + ". Success = " + postingAmount;
                    }
                    #endregion

                    #region Phase 3 : insert process id into ics queue
                    //message = "Start Process Insert ICS Queue";
                    //location = "Insert ICS Queue";

                    //db.ExecuteScalar<string>(
                    //    "GenerateProcessId",
                    //    new object[] { 
                    //        message,     // what
                    //        username,     // user
                    //        location,     // where
                    //        processID,     // pid OUTPUT
                    //        "MPCS00002INF",     // id
                    //        "",     // type
                    //        module,     // module
                    //        function,     // function
                    //        "0"      // sts
                    //    });

                    //db.ExecuteScalar<string>(
                    //    "InsertICSQueue",
                    //    new object[] { 
                    //        module,     // ippcs module
                    //        function,     // ippcs function
                    //        processID,     // process id
                    //        "0",     // process status
                    //        username,     // created by
                    //        DateTime.Now,     // created dt
                    //    });

                    //message = "Finish Process Insert ICS Queue";

                    //db.ExecuteScalar<string>(
                    //    "GenerateProcessId",
                    //    new object[] { 
                    //        message,     // what
                    //        username,     // user
                    //        location,     // where
                    //        processID,     // pid OUTPUT
                    //        "MPCS00003INF",     // id
                    //        "",     // type
                    //        module,     // module
                    //        function,     // function
                    //        "0"      // sts
                    //    });
                    #endregion
                    #endregion
                }
                catch (Exception exc)
                {
                    resultMessage = "Cancellation Failed for Process ID : " + processID + ", please contact IPPCS administrator!";

                    // if web service process fails
                    List<String> errorManifestNoList = new List<String>();
                    foreach (IDictionary<string, string> map in dataList)
                    {
                        db.ExecuteScalar<string>("CancelGoodsReceiptInquiry", new object[] {
                                map["DockCode"],   // dockCode
                                map["SupplierCode"],   // supplierCode
                                map["RecPlantCode"],   // rcvPlantCode
                                map["ManifestNo"],   // manifestNo
                                "5",
                                map["OrderNo"],   // orderNo 
                                AuthorizedUser.Username
                        });

                        errorManifestNoList.Add(map["ManifestNo"]);
                    }

                    message = exc.Message + ". Manifest List : " + String.Join(";", errorManifestNoList.ToArray()); ;

                    db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00008ERR",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                }

                db.Close();
            }

            return Content(resultMessage.ToString());            
        }
        #endregion

        #region Common Controller
        public ContentResult GetManifestPostMaxData()
        {
            IDBContext db = DbContext;
            int maxData = -1;

            try
            {
                maxData = db.ExecuteScalar<int>(
                    "GetSystemValue" 
                    ,new object[] { 
                        "42001"    // function id
                        ,"MAX_GR_POST"  // system cd
                    });
            }
            catch (Exception exc) { }

            return Content(maxData.ToString());  
        }

        //get max perprocess
        public ContentResult GetSplitMaxData()
        {
            IDBContext db = DbContext;
            int maxData = -1;

            try
            {
                maxData = db.ExecuteScalar<int>(
                    "GetSystemValue"
                    , new object[] { 
                        "42001"    // function id
                        ,"MAX_GR_PERPROCESS"  // system cd
                    });
            }
            catch (Exception exc) { }

            return Content(maxData.ToString());
        }


        private String GetManifestReceiveFlagCodeList(String manifestReceiveFlagNameParameter)
        {
            List<String> manifestReceiveFlagCodeList = new List<String>();

            foreach (String manifestReceiveFlagName in manifestReceiveFlagNameParameter.Split(';'))
            {
                switch (manifestReceiveFlagName)
                {
                    case "Ordered":
                        manifestReceiveFlagCodeList.Add("0");
                        break;
                    case "Delivered":
                        manifestReceiveFlagCodeList.Add("1");
                        break;
                    case "Scanned":
                        manifestReceiveFlagCodeList.Add("2");
                        break;
                    case "Approved":
                        manifestReceiveFlagCodeList.Add("3");
                        break;
                    case "Posted":
                        manifestReceiveFlagCodeList.Add("4");
                        break;
                    case "Error Posting":
                        manifestReceiveFlagCodeList.Add("5");
                        break;
                    case "Invoiced":
                        manifestReceiveFlagCodeList.Add("6");
                        break;
                    case "Paid":
                        manifestReceiveFlagCodeList.Add("7");
                        break;
                    case "Cancelled":
                        manifestReceiveFlagCodeList.Add("8");
                        break;
                }
            }

            return String.Join(",", manifestReceiveFlagCodeList.ToArray());
        }

        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else 
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadGoodsReceiptInquiry(
            String arrivalTimeFrom = "",
            String arrivalTimeTo = "",
            String dockCode = "",
            String supplierCode = "",
            String rcvPlantCode = "",
            String routeRate = "",
            String manifestNo = "",
            String manifestReceiveFlag = "",
            String orderNo = "",
            Boolean downloadBy = false
            )
        {
            string fileName = String.Empty;

            // Update download user and status
            IEnumerable<GoodsReceiptInquiryData> qry;
            IEnumerable<GoodsReceiptInquiryPerManifestData> qryPerManifest;

            string DockCodeLDAP = "";
            List<Dock> DockCodes = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DockCode", "GoodsReceiptInquiry", "GRIHDockLookup");
            foreach (Dock d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;

            IDBContext db = DbContext;
           
            // get current user login detail
            if (supplierCode == "") {
                List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "GoodsReceiptInquiry", "GRIHSupplierLookup");

                if (suppliers.Count == 1)
                {
                    supplierCode = suppliers[0].SUPPLIER_CODE;
                }
            }

            
            if (downloadBy)
            {
                qryPerManifest = db.Fetch<GoodsReceiptInquiryPerManifestData>(
                    "ReportGetAllGoodsReceiptInquiryPerManifest",
                        new object[] { 
                            arrivalTimeFrom,
                            arrivalTimeTo,
                            dockCode.Replace(";", ","),
                            supplierCode.Replace(";", ","),
                            rcvPlantCode,
                            routeRate,
                            manifestNo,
                            GetManifestReceiveFlagCodeList(manifestReceiveFlag),
                            orderNo,
                            DockCodeLDAP
                            });

                fileName = "GoodsReceiptInquiryPerManifest" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + supplierCode.Trim().ToUpper() + rcvPlantCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
                hasil = exporter.Write(ConvertToDataTable(qryPerManifest), "GRPerManifest");
            }
            else
            {
                qry = db.Fetch<GoodsReceiptInquiryData>(
                    "ReportGetAllGoodsReceiptInquiry",
                        new object[] { 
                            arrivalTimeFrom,
                            arrivalTimeTo,
                            dockCode.Replace(";", ","),
                            supplierCode.Replace(";", ","),
                            rcvPlantCode,
                            routeRate,
                            manifestNo,
                            GetManifestReceiveFlagCodeList(manifestReceiveFlag),
                            orderNo,
                            DockCodeLDAP
                            });

                //fileName = "GoodsReceiptInquiry" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + supplierCode.Trim().ToUpper() + rcvPlantCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
                fileName = "GoodsReceiptInquiry" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
                hasil = exporter.Write(ConvertToDataTable(qry), "GR");
            }

            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            /*
             * @Lufty: numpang nambahin, kisanak
             */
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion
        #endregion

        #region Goods Receipt Inquiry Manifest Detail Controllers
        #region View Controllers
        public ActionResult PartialPopupGoodsReceiptInquiryManifestDetail()
        {
            #region Required model for [Goods receipt inquiry manifest detail gridview]
            _GoodsReceiptInquiryManifestDetailDataModel = GetAllGoodsReceiptInquiryManifestDetail(
                String.IsNullOrEmpty(Request.Params["DockCode"]) ? "" : Request.Params["DockCode"],
                String.IsNullOrEmpty(Request.Params["RcvPlantCode"]) ? "" : Request.Params["RcvPlantCode"],
                String.IsNullOrEmpty(Request.Params["ManifestNo"]) ? "" : Request.Params["ManifestNo"]
                );
            Model.AddModel(_GoodsReceiptInquiryManifestDetailDataModel);
            #endregion

            return PartialView("GoodsReceiptInquiryManifestDetailPartial", Model);
        }
        #endregion
        #region Error Posting Controllers 
        public ActionResult PartialPopupGoodsReceiptInquiryManifestDetailErrorPosting()
        {
            #region Required model for [Goods receipt inquiry manifest detail gridview]
            _GoodsReceiptInquiryManifestDetailDataModel = GetAllGoodsReceiptInquiryManifestDetailErrorPosting(
                String.IsNullOrEmpty(Request.Params["DockCode"]) ? "" : Request.Params["DockCode"],
                String.IsNullOrEmpty(Request.Params["RcvPlantCode"]) ? "" : Request.Params["RcvPlantCode"],
                String.IsNullOrEmpty(Request.Params["ManifestNo"]) ? "" : Request.Params["ManifestNo"]
                );
            Model.AddModel(_GoodsReceiptInquiryManifestDetailDataModel);
            #endregion

            return PartialView("GoodsReceiptInquiryManifestDetailErrorPostingPartial", Model);
        }
        #endregion
        #region Database Controllers
        public List<GoodsReceiptInquiryManifestDetailData> GetAllGoodsReceiptInquiryManifestDetail(
            String dockCode = "",
            String rcvPlantCode = "",
            String manifestNo = ""
            )
        {
            List<GoodsReceiptInquiryManifestDetailData> goodsReceiptInquiryManifestDetailDataModel = new List<GoodsReceiptInquiryManifestDetailData>();
            IDBContext db = DbContext;
            try
            {
                if (!String.IsNullOrEmpty(dockCode) || !String.IsNullOrEmpty(rcvPlantCode) || !String.IsNullOrEmpty(dockCode))
                    goodsReceiptInquiryManifestDetailDataModel = db.Fetch<GoodsReceiptInquiryManifestDetailData>(
                        "GetAllGoodsReceiptInquiryManifestDetail",
                        new object[] { 
                            dockCode,
                            rcvPlantCode,
                            manifestNo,
                            });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return goodsReceiptInquiryManifestDetailDataModel;
        }

        public List<GoodsReceiptInquiryManifestDetailData> GetAllGoodsReceiptInquiryManifestDetailErrorPosting(
            String dockCode = "",
            String rcvPlantCode = "",
            String manifestNo = ""
            )
        {
            List<GoodsReceiptInquiryManifestDetailData> goodsReceiptInquiryManifestDetailDataModel = new List<GoodsReceiptInquiryManifestDetailData>();
            IDBContext db = DbContext;
            try
            {
                if (!String.IsNullOrEmpty(dockCode) || !String.IsNullOrEmpty(rcvPlantCode) || !String.IsNullOrEmpty(dockCode))
                    goodsReceiptInquiryManifestDetailDataModel = db.Fetch<GoodsReceiptInquiryManifestDetailData>(
                        "GetAllGoodsReceiptInquiryManifestDetailErrorPosting",
                        new object[] {
                            dockCode,
                            rcvPlantCode,
                            manifestNo,
                            });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return goodsReceiptInquiryManifestDetailDataModel;
        }
        #endregion
        #endregion

        public ActionResult PartialGoodsReceiptErrorPIC()
        {
            List<GoodsReceiptErrorPIC> GoodsReceiptErrorPICs = new List<GoodsReceiptErrorPIC>();
            for (int j = 1; j <= 5; j++)
            {
                GoodsReceiptErrorPICs.Add(new GoodsReceiptErrorPIC
                {
                    ERROR_PIC_TITLE = "ErrorPICTitle" + j,
                    ERROR_PIC_NAME = "ErrorPICName" + j,
                    ERROR_PIC_DIVISION = "ErrorPICDiv" + j,
                    ERROR_PIC_PHONE = "ErrorPICPhone" + j,
                    ERROR_PIC_EXT = "ErrorPICExt" + j,
                    ERROR_PIC_EMAIL = "ErrorPICEmail" + j,
                });
            }

            return PartialView("PartialErrorPIC", GoodsReceiptErrorPICs);
        }



         public void downloadError(string param) {
             IDBContext db = DbContext;
             byte[] datas;

             IExcelWriter exporter = ExcelWriter.GetInstance();
             string fileName = String.Empty;
             List<string> TMMINrole = new List<string>();

             List<GoodsReceiptInquiryManifestDetailData> errorManifestDetail = new List<GoodsReceiptInquiryManifestDetailData>();
             //List<GoodReceiptAdditionalErrorData> errorManifestAdditional = new List<GoodReceiptAdditionalErrorData>();

             errorManifestDetail = db.Fetch<GoodsReceiptInquiryManifestDetailData>(
                "GetAllGoodsReceiptInquiryErrorManifestDetail",new object[] { param });

             //errorManifestAdditional = db.Fetch<GoodReceiptAdditionalErrorData>(
             //   "GetAllGoodReceiptAdditionalDetailData", new object[] { param });

             fileName = "ErrorGoodsReceiptInquiryPerManifest"+DateTime.Now.ToString("ddMMyyyy")+".xls";

             //TMMINrole = db.Fetch<string>("GetTMMINRole");
             //string username = AuthorizedUser.Username;

             //bool isTMMINUser = false;
             //foreach (var s in TMMINrole)
             //{
             //    foreach (var p in AuthorizedUser.Authorization)
             //    { 
             //       if(p.AuthorizationID == s)
             //           isTMMINUser = true;
             //    }
             //}

             datas = exporter.Write(ConvertToDataTable(errorManifestDetail), "ErrorGoodsReceipt");
             //exporter.Append(errorManifestDetail, "ErrorGoodsReceipt");

             //if(isTMMINUser)
             //   exporter.Append(errorManifestAdditional, "ErrorGoodsReceiptICS");
             
             //datas = exporter.Flush();

             db.Close();

             Response.Clear();
             Response.Cache.SetCacheability(HttpCacheability.Private);
             Response.Expires = -1;
             Response.Buffer = true;
             
             Response.ContentType = "application/octet-stream";
             Response.AddHeader("Content-Length", Convert.ToString(datas.Length));
            
             Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
             Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

             Response.BinaryWrite(datas);
             Response.End();

         }
    }
}

