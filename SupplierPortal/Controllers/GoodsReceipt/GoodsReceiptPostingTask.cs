﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.BackgroundTask;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.Credential;
using Portal.Models.GoodsReceiptInquiry;
using Toyota.Common.Web.Database;
using System.Data;
using System.IO;
using Toyota.Common.Web.Service;
using Toyota.Common.Web.Util.Converter;
using Toyota.Common.Util.Text;

namespace Portal.Controllers.GoodsReceipt
{
    public class GoodsReceiptPostingTask : ObservableBackgroundTask
    {
        protected override void DoWork(IDictionary<string, object> parameters)
        {
            List<IDictionary<string, string>> dataList = (List<IDictionary<string, string>>) parameters["DataList"];
            String process = (string) parameters["Process"];
            User AuthorizedUser = (User) parameters["User"];

            LogProvider logProvider = new LogProvider("4", "42001", AuthorizedUser);
            ILogSession sessionA = logProvider.CreateSession(true);

            string sendFlag = "";
            string processID = Convert.ToString(sessionA.GetProcessId());
            string sourceType = "";
            List<GoodsReceiptICSTransfer> tempList;
            List<GoodsReceiptICSTransfer> list = new List<GoodsReceiptICSTransfer>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);;

            sessionA.WriteDirectly("MPCS00002INF", new object[] { "Posting File GR to ICS" }, "SH Approve.Posting File GR to ICS");

            sessionA.WriteDirectly("MPCS00002INF", new object[] { "Get Manifest Data From DB Process" }, "SH Approve.Get Manifest Data From DB Process");
            
            foreach (IDictionary<string, string> map in dataList)
            {
                tempList = db.Fetch<GoodsReceiptICSTransfer>("ICSGeneratePostingFileGR", new object[] { AuthorizedUser.Username, map["ManifestNo"], map["SupplierCode"], map["RecPlantCode"], map["DockCode"] });
                if ((tempList.Count > 0) && (tempList != null))
                {
                    list.AddRange(tempList);
                    sendFlag = list[0].sendFlag;
                    //processID = list[0].processID;
                    sourceType = list[0].sourceType;
                }
                else
                {
                    sessionA.WriteDirectly("MPCS00006ERR", new string[] { "From Get Manifest Data From DB Process for " + map["ManifestNo"] }, "SH Approve.Get Manifest Data From DB Process");
                    
                    Progress = PROGRESS_EXCEPTION;
                }
            }

            sessionA.WriteDirectly("MPCS00003INF", new object[] { "Get Manifest Data From DB Process" }, "SH Approve.Get Manifest Data From DB Process");
            
            Progress += 20;

            switch (process)
            {
                case "Approve":
                    foreach (IDictionary<string, string> map in dataList)
                    {
                        db.ExecuteScalar<string>("ApproveGoodsReceiptInquiry", new object[] {
                                map["DockCode"],   // dockCode
                                map["SupplierCode"],   // supplierCode
                                map["RecPlantCode"],   // rcvPlantCode
                                map["ManifestNo"],   // manifestNo
                                "3",
                                map["OrderNo"],   // orderNo 
                                AuthorizedUser.Username
                        });
                    }
                    break;
                case "Cancel":
                    foreach (IDictionary<string, string> map in dataList)
                    {
                        db.ExecuteScalar<string>("CancelGoodsReceiptInquiry", new object[] {
                                map["DockCode"],   // dockCode
                                map["SupplierCode"],   // supplierCode
                                map["RecPlantCode"],   // rcvPlantCode
                                map["ManifestNo"],   // manifestNo
                                "8",
                                map["OrderNo"],   // orderNo 
                                AuthorizedUser.Username
                        });
                    }
                    break;
            }

            if (list.Count == 0 & sendFlag != "" & processID != "")
            {
                Progress = PROGRESS_EXCEPTION;
            }
            else
            {
                sessionA.WriteDirectly("MPCS00002INF", new object[] { "Send Data to ICS" }, "SH Approve.Send Data to ICS");
                
                GatewayService.WebServiceClient gateway = new GatewayService.WebServiceClient();
                ServiceResult result;
                bool serviceReady = false;
                try
                {
                    gateway.Open();
                    string testStringResult = gateway.Execute("Startup", "IntegrityCheck", null);
                    if (!string.IsNullOrEmpty(testStringResult))
                    {
                        result = ServiceResult.Create(testStringResult);
                        serviceReady = (result.Status == ServiceResult.STATUS_READY);
                    }
                }
                catch (Exception ex)
                {
                    gateway.Abort();
                    gateway = new GatewayService.WebServiceClient();
                    gateway.Open();

                    sessionA.WriteDirectly("MPCS00002ERR", new string[] { ex.Message, "", "", "" }, "SH Approve.Send Data to ICS");
                }

                if (!serviceReady)
                {
                    sessionA.WriteDirectly("MPCS00007ERR", new string[] { "Web Service"}, "SH Approve.Send Data to ICS");                    
                    Progress = PROGRESS_EXCEPTION;
                }

                string jsonResult;
                ServiceParameters sp = new ServiceParameters();
                string sessionID;
                sp.Add("state", "init");
                result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));
                if (result.Status != ServiceResult.STATUS_ERROR)
                {
                    sessionID = result.Value.ToString();

                    /* Start Process 2, Transfer */
                    sp.Clear();
                    sp.Add("state", "transfer");
                    sp.Add("sessionId", result.Value);
                    sp.Add("data", String.Empty);

                    const int MAX_DATA_TRANSFERRED = 10;
                    int dataLength = list.Count;
                    //int lastIndex = dataLength - 1;
                    int lastIndex = dataLength;
                    int counter = 0;
                    int fetchSize;
                    int indexDifference;
                    bool looping = true;
                    bool transferError = false;
                    while (looping)
                    {
                        indexDifference = (lastIndex - counter);
                        if (indexDifference < MAX_DATA_TRANSFERRED)
                        {
                            fetchSize = indexDifference;
                        }
                        else
                        {
                            fetchSize = MAX_DATA_TRANSFERRED;
                        }

                        tempList = list.GetRange(counter, fetchSize);
                        jsonResult = JSON.ToString<List<GoodsReceiptICSTransfer>>(tempList);

                        sp.Remove("data");
                        sp.Add("data", (object)jsonResult);
                        sp.Add("userId", AuthorizedUser.Username);
                        result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));
                        if (result.Status == ServiceResult.STATUS_SUCCESS)
                        {
                            counter += (fetchSize);
                        }
                        else
                        {
                            transferError = true;
                            looping = false;
                        }

                        if (counter == lastIndex)
                        {
                            looping = false;
                        }

                    }
                    /* End Process 2 */
                    sessionA.WriteDirectly("MPCS00003INF", new object[] { "Send Data to ICS" }, "SH Approve.Send Data to ICS");
                    
                    Progress += 20;

                    if (transferError)
                    {
                        /*
                        sessionA.WriteDirectly("MPCS00002ERR", new string[] { result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString(), "", "", "" });
                        sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_ERROR);
                        
                         */

                        sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Transfer Web Service" + result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString() }, "SH Approve.Send Data to ICS");
                        
                        Progress = PROGRESS_EXCEPTION;
                    }
                    else
                    {   
                        /* Start Process 3 process*/
                        sessionA.WriteDirectly("MPCS00002INF", new object[] { "Get Reply Data From ICS" }, "SH Approve.Get Reply Data From ICS");
                        

                        sp.Clear();
                        sp.Add("state", "process");
                        sp.Add("sessionId", sessionID);
                        sp.Add("sendFlag", sendFlag);
                        sp.Add("userId", AuthorizedUser.Username);
                        sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                        result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));

                        /* End Process 3 */
                        sessionA.WriteDirectly("MPCS00003INF", new object[] { "Get Reply Data From ICS" }, "SH Approve.Get Reply Data From ICS");
                        

                        if (result.Status != ServiceResult.STATUS_ERROR)
                        {
                            /* Start Process 4 inquiry */
                            sessionA.WriteDirectly("MPCS00002INF", new object[] { "Insert Reply Data to IPPCS" }, "SH Approve.Insert Reply Data to IPPCS");
                            

                            int totalPage = Convert.ToInt32(result.Value != null ? result.Value : 0);
                            sp.Clear();
                            sp.Add("state", "inquiry");
                            sp.Add("sessionId", sessionID);
                            sp.Add("processId", processID);
                            int seq_no = 1;

                            for (int i = 1; i <= totalPage; i++)
                            {
                                try
                                {
                                    sp.Add("currentPage", i);
                                    sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                                    result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.Contains("Timeout") || ex.Message.Contains("timeout"))
                                    {
                                        sp.Add("currentPage", i);
                                        sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                                        result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));
                                    }

                                    sessionA.WriteDirectly("MPCS00002ERR", new string[] { ex.Message, "", "", "" }, "SH Approve.Insert Reply Data to IPPCS");
                                }

                                if (result.Status != ServiceResult.STATUS_ERROR)
                                {
                                    if (result.Value == null)
                                    {
                                        sessionA.WriteDirectly("MPCS00009ERR", new string[] { "ICS" }, "SH Approve.Insert Reply Data to IPPCS");
                                        
                                        Progress = PROGRESS_EXCEPTION;
                                    }

                                    List<GoodsReceiptICSFinish> values = new List<GoodsReceiptICSFinish>();
                                    values = JSON.ToObject<List<GoodsReceiptICSFinish>>(result.Value.ToString());
                                    Nullable<DateTime> postingDate = null;
                                    Nullable<DateTime> processDate = null;

                                    // clear transaction log before process
                                    sessionA.WriteDirectly("MPCS00004INF", new object[] { "Deletion Log Transaction Data By Manifest" }, "SH Approve.Delete Log Transaction By Manifest");
                                    
                                    db.Execute("DeleteLogTransactionByManifest", new object[] { values[0].oriMatNo });

                                    sessionA.WriteDirectly("MPCS00002INF", new object[] { "Insert Posting File GR" }, "SH Approve.Insert Posting File GR");
                                    
                                    foreach (GoodsReceiptICSFinish e in values)
                                    {
                                        if (!String.IsNullOrEmpty(e.postingDate))
                                            postingDate = Convert.ToDateTime(e.postingDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));
                                        if (!String.IsNullOrEmpty(e.processDate))
                                            processDate = Convert.ToDateTime(e.processDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));

                                        db.ExecuteScalar<string>("ICSInsertPostingFileGR", new object[] { e.systemSource, e.userId, e.movementType, 
                                            postingDate, processDate, e.refNo, e.kanbanOrderNo, e.prodPurpose, 
                                            e.sourceType, e.oriMatNo, e.oriSuppCode, e.receivingPlantArea,
                                            e.plantCode, e.slocCode, e.matDocNo, e.matDocYear, e.errorStatus, e.errorCode, 
                                            e.errorDesc, e.errorMsg, processID, e.sessionId, AuthorizedUser.Username, process, seq_no});

                                        seq_no++;
                                    }                                    

                                    if (values[0].errorStatus == "0")
                                    {
                                        // clear transaction log if process is success (error status is 0).
                                        sessionA.WriteDirectly("MPCS00004INF", new object[] { "Deletion Log Transaction By Manifest" }, "SH Approve.Deletion Log Transaction By Manifest");
                                        sessionA.WriteDirectly("MPCS00003INF", new string[] { "Insert Reply Data to IPPCS With Status Success" }, "SH Approve.Insert Reply Data to IPPCS");

                                        db.Execute("DeleteLogTransactionByManifest", new object[] { values[0].oriMatNo });
                                    }
                                    if (values[0].errorStatus == "1")
                                    {
                                        sessionA.WriteDirectly("MPCS00003INF", new string[] { "Insert Reply Data to IPPCS With Status Error" }, "SH Approve.Insert Reply Data to IPPCS");
                                        
                                        Progress = PROGRESS_EXCEPTION;
                                    }
                                    sessionA.WriteDirectly("MPCS00003INF", new object[] { "Insert Posting File GR" }, "SH Approve.Insert Posting File GR");

                                    // clear result posting log after process
                                    sessionA.WriteDirectly("MPCS00004INF", new object[] { "Deletion Log Transaction Data By Process ID" }, "SH Approve.Deletion Log Transaction By Process ID");
                                    
                                    db.Execute("DeleteLogResultPostingByProcessId", new object[] { processID });

                                    // update daily order manifest detail with error result (if exists)
                                    sessionA.WriteDirectly("MPCS00004INF", new object[] { "Insert & Update Daily Order Manifest Detail For Detail Mapping" }, "SH Approve.Insert & Update Daily Order Manifest Detail For Detail Mapping");
                                    
                                    db.Execute("UpdateDailyOrderManifestDetail", new object[] { values[0].receivingPlantArea, "", values[0].refNo, AuthorizedUser.Username, DateTime.Now});
                                }
                                else
                                {
                                    sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Insert Reply Data to IPPCS" }, "SH Approve.Insert Reply Data to IPPCS");
                                    
                                    Progress = PROGRESS_EXCEPTION;
                                }

                                if (result.Status != ServiceResult.STATUS_SUCCESS)
                                {
                                    transferError = true;
                                }
                            }
                            /* End Process 4 */                            
                            Progress += 20;

                            /* Start Process 5 finish */
                            sp.Clear();
                            sp.Add("state", "finish");
                            sp.Add("sessionId", sessionID);
                            result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));
                            sp.Clear();
                            if (result.Status == ServiceResult.STATUS_ERROR)
                            {

                                sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Finish Web Service" }, "SH Approve.Finish State Web Service");
                                
                                Progress = PROGRESS_EXCEPTION;
                            }
                            /* End Process 5 */

                            Progress += 20;
                        }
                        else
                        {

                            sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Web Service" }, "SH Approve.Get Reply Data From ICS");
                            
                            Progress = PROGRESS_EXCEPTION;
                        }

                        //sessionA.WriteDirectly("MPCS00003INF", new object[] { "Insert Reply Data to IPPCS" }, "SH Approve.Send Data to ICS");
                    }
                }
                else
                {
                    sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Init Web Service" }, "SH Approve.Send Data to ICS");                    
                    Progress = PROGRESS_EXCEPTION;
                }
                db.Close();
                gateway.Close();
            }

            sessionA.WriteDirectly("MPCS00003INF", new object[] { "Posting File GR to ICS" }, "SH Approve.Posting File GR to ICS");

            if (Progress == -255)
                sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_ERROR);
            else
                sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);

            sessionA.Commit();

            Progress = 100;
        }

        public override string GetName()
        {
            return "GoodsReceiptPosting";
        }
    }
}