﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.MessageBoard;
using Toyota.Common.Web.Util.Converter;
using Toyota.Common.Web.Database;

namespace Portal.Controllers.Dummy
{
    public class DummyMessageBoardController : BaseController
    {
        public DummyMessageBoardController()
            : base("Dummy Message Board")
        {
            PageLayout.UseMessageBoard = true;
        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
        }        
    }
}
