﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.Forecast;
using Portal.Models.SupplierFeedbackSummary;
using Portal.Models.Dummy;
using Toyota.Common.Web.Database;
using Portal.Models.DCLInquiry;
using Toyota.Common.Web.Notifcation.JobMonitoring;
using Toyota.Common.Web.Email;
using Portal.Controllers.Dummy;
using System.IO;

namespace Portal.Controllers
{
    public class DummyController : BaseController
    {
        public DummyController(): base("Dummy Testing Page")
        {
            Blacklisted = false;            
        }

        protected override void StartUp()
        {
            DummyModel model = new DummyModel();
            Model.AddModel(model);

            //// sample Creating Notification /////
            //AttachmentProvider attach = new AttachmentProvider();
            //attach.Create("10010", "28");
            //attach.Write("System.ComponentModel.DataAnnotations.dll");
            //attach.Write(@"E:\Documents\Desktop\TB_R_NOTIFICATION_ATTACHMENT.xlsx");
            //string attachID = attach.Flush();

            //ArgumentParameter param = new ArgumentParameter();
            //param.Add("@File", "ABX.txt");
            //DirectJobProvider.Execute("10010", "28", param, attachID);
            //// ENd sample Creating Notification /////
            //AttachmentProvider attach = new AttachmentProvider();
            //ArgumentParameter param = new ArgumentParameter();
            //attach.Create("41001", "31");
            //attach.Write("System.ComponentModel.DataAnnotations.dll");
            //string attachID = attach.Flush();
            //param.Clear();
            //param.Add("@Dock", "1");
            //param.Add("@number_manifest", "1");
            //DirectJobProvider.Execute("41001", "31", "31", param, attachID);
            // Exception Raise Test
            
            //int x = 0, y;
            //for (int i = 0; i < 1; i++)
            //{
            //    y = i / x;
            //}
             
            
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            model.Inquiries = db.Fetch<DCLInquiryDb>("Dummy_ListDCL");
            db.Close(); 
        }

        protected override void Init()
        {   
            PageLayout.ShowSidePanes();
            PageLayout.UseSlidingPanes();
            int i, x = 0;

        }

         
        public ActionResult SampleGridCallback()
        {
            return PartialView("SampleGrid", Model);
        }
    }
}
