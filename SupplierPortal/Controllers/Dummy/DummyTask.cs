﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.BackgroundTask;
using System.Threading;

namespace Portal.Controllers.Dummy
{
    public class DummyTask: ObservableBackgroundTask
    {
        protected override void DoWork(IDictionary<string, object> parameters)
        {
            while (Progress < 100)
            {
                if (Progress == 50)
                {
                    Progress = PROGRESS_EXCEPTION;
                    break;
                }
                Thread.Sleep(100);
                Progress++;
            }

            /*int iterationNumber = Convert.ToInt32(parameters["IterationNumber"]);
            for (int i = 0; i < iterationNumber; i++)
            {
                if((i % 10) == 0) {
                    Progress++;
                    NotifyBackgroundTaskChanged(BackgroundTaskEvent.PROGRESS_CHANGED, this);
                }
            }*/
        }

        public override string GetName()
        {
            return "DummyTask";
        }
    }
}