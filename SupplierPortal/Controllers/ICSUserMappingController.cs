﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.ICSUserMapping;
using Toyota.Common.Web.Database;
namespace Portal.Controllers
{
    public class ICSUserMappingController : BaseController
    {
        public ICSUserMappingController()
            : base("ICS User Mapping")
        {
        }

        #region Property
        List<ICSUserMapping> _listmodel = null;

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_WORKFLOW);
            }
        }

        private List<ICSUserMapping> ListModel
        {
            get
            {
                if (_listmodel == null)
                      _listmodel = new List<ICSUserMapping>();
                return _listmodel;
            }
            set
            {
                _listmodel = value;
            }
        }
        #endregion

        #region ListGrid

        private List<ICSUserMapping> GetList(string icsuser, string ldapuser)
        {
            List<ICSUserMapping> VarList = new List<ICSUserMapping>();
            IDBContext db = DbContext;
            VarList = db.Fetch<ICSUserMapping>("GetICSUser", new object[] { icsuser, ldapuser});
            return VarList;
        }

        public ActionResult ReloadGridICSUserMapping()
        {
           
            ICSUserMappingModel mdls = Model.GetModel<ICSUserMappingModel>(); ;
            mdls.ICSUSerMappingList = GetList(Request.Params["IcsUser"],
                                Request.Params["LdapUser"]);
            Model.AddModel(mdls);
            return PartialView("GridICSUserMapping", Model);
        }

        #endregion

        protected override void StartUp()
        {

        }

        public ActionResult PartialPopupICSUserAdd()
        {
            return PartialView("PopupNewICSUser", Model);
        }

        protected override void Init()
        {
            ICSUserMappingModel mdl = new ICSUserMappingModel();
            mdl.ICSUSerMappingList = GetList("","");
            Model.AddModel(mdl);
        }

        #region SaveDAta
        public ContentResult SaveData(string picsuser, string pldapuser, string pstatus, string ptempics, string ptempldap)
        {
            IDBContext db = DbContext;
            String Result = "";
            string Query = string.Empty;
            string user = AuthorizedUser.Username;
            try
            {
                if (pstatus == "new")
                {
                    Query = db.ExecuteScalar<string>("InsertICSUser", new object[] { ptempics, ptempldap, user });
                    Result = "Data Has Been Saved";
                }

                if (pstatus == "edit")
                {
                    Query = db.ExecuteScalar<string>("UpdateICSUser", new object[] { ptempics, ptempldap, picsuser, pldapuser, user });
                    Result = "Data Has Been Updated";
                }
            }
              catch (Exception ex)
            {
                Result = "Error: " + ex.Message;
            }
            finally { db.Close(); }
            return Content(Result);
        }
        #endregion

        public void DeleteICSUser(string pICSUser, string pLDAPUser)
        {
            IDBContext db = DbContext;
            String Result = "";
            try
            {
                Result = db.ExecuteScalar<string>("DeleteICSUser", new object[] { pICSUser, pLDAPUser });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
        }


    }
}
