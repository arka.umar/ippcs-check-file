﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.Globals;
using Toyota.Common.Web.Database;
using Portal.Models.GoodsReceiptInquiry;


namespace Portal.Controllers
{
    public class DeliveryServiceAcceptanceController : BaseController
    {

        public DeliveryServiceAcceptanceController() : base("Delivery Service Acceptance") { }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
            ViewData["Status"] = GetAllManifestStatus();
        }

        public ActionResult HeaderSearch()
        {
            return PartialView("HeaderSearch");
        }
         
        public ActionResult DeliveryTypeLookupCallback()
        {
            TempData["Grid"] = "DeliveryType";
            return PartialView("GridLookup/PartialGrid", null);
        }

        public ActionResult LogisticPartnerLookupCallback()
        {
            TempData["Grid"] = "LogisticPartner";
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult StatusLookupCallback()
        {
            TempData["Grid"] = "StatusLookup";
            ViewData["Status"] = GetAllManifestStatus();
            return PartialView("GridLookup/PartialGrid", ViewData["Status"]);
        }

        public ActionResult Detail()
        {
            return PartialView("Detail");
        }
         
        public List<LogisticPartner> GetAllLogisticPartners()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();
            db.Close();
            return listLogisticPartner;
        }

        private List<GoodsReceiptManifestStatus> GetAllManifestStatus()
        {
            List<GoodsReceiptManifestStatus> statusManifestModel = new List<GoodsReceiptManifestStatus>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                statusManifestModel = db.Fetch<GoodsReceiptManifestStatus>("GetAllManifestStatusDSA", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();
            return statusManifestModel;
        }

    }
}
