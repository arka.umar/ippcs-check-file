﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteConvertionMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;


namespace Portal.Controllers
{
    public class RouteConvertionMasterController : BaseController
    {
         public RouteConvertionMaster modelExport;

         public RouteConvertionMasterController()
             : base("RouteConvertionMaster", "Route Convertion Master")
           
       {

       }

        
        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            //RouteConvertionMasterModel mdl = new RouteConvertionMasterModel();
            //mdl.RouteConvertionMaster = RouteConvertionMaster();
            //Model.AddModel(mdl);
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
        }

        public ActionResult Back()
        {
            return RedirectToAction("Index", "RoutePriceMaster");
        }

        public ActionResult IndexHeader()
        {

            return PartialView("IndexHeader", null);
        }

        public ActionResult RoutePriceTypeLookupCallback()
        {
            TempData["Grid"] = "RoutePrice";
            return PartialView("GridLookup/PartialGrid", null);
        }

        public ActionResult LogisticPartnerLookupCallback()
        {
            TempData["Grid"] = "LogisticPartner";
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public List<LogisticPartner> GetAllLogisticPartners()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();
            db.Close();
            return listLogisticPartner;
        }

        public ActionResult GridRouteConvertionMaster(string Route_cd, string Route_conv_cd)
        {
            Route_cd = ((Route_cd == null) ? "" : Route_cd);
            Route_conv_cd = ((Route_conv_cd == null) ? "" : Route_conv_cd);

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            RouteConvertionMasterModel rpm = new RouteConvertionMasterModel();
            rpm.RouteConvertionMaster = db.Query<RouteConvertionMaster>("GetRouteConversionMasterData", new object[] { Route_cd.ToUpper(), Route_conv_cd.ToUpper() }).ToList<RouteConvertionMaster>();
            Model.AddModel(rpm);

            db.Close();

            return PartialView("GridRouteConvertionMaster", Model);
        }

        public ContentResult AddingRouteConversion(string Route_Conv_Cd, string Route_Cd, string Route_Conv_Name, string LP_Cd, string Valid_From, string Valid_To)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isExistsRoute = 0;
            int isExistsRouteConv = 0;
            int isError = 0;
            string errorMessage = "";



            try
            {
                //---=== Check if exists on TB_M_DELIVERY_ROUTE GetValidationRoutePriceStat
                CheckResult QueryLog = db.SingleOrDefault<CheckResult>("GetRoutePriceMasterDataCheck", new object[] { 
                    "R", Route_Cd
                });
                isExistsRoute = QueryLog.result;
                //---=== Check if exists on TB_M_LOGISTIC_PARTNER
                QueryLog = db.SingleOrDefault<CheckResult>("GetRoutePriceMasterDataCheck", new object[] { 
                    "RC", Route_Conv_Cd
                });
                isExistsRouteConv = QueryLog.result;

                if ((isExistsRoute == 1) && (isExistsRouteConv == 1))
                {
                    db.Execute("SetMasterRouteConversion", new object[] { 
                            Route_Conv_Cd.ToUpper(), Route_Cd.ToUpper(), Route_Conv_Name, LP_Cd, Valid_From, Valid_To, AuthorizedUser.Username
                        });
                    errorMessage = "Master data saved.";
                }
                else
                {
                    isError = 1;
                    errorMessage = ((isExistsRoute == 0) ? "Route" : "");
                    errorMessage = ((isExistsRouteConv == 0) ? ((errorMessage != "") ? errorMessage + " and Route Conversion" : "Route Conversion") : errorMessage);
                    errorMessage = "Error : " + errorMessage + " not exists on master table.";
                }

            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = "Failed on saving process.";
            }

            return Content(((isError == 1) ? "Error|" : "Sucess|") + errorMessage);
        }

    }
}
