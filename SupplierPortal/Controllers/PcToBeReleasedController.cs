﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.Globals;
using Portal.Models.PcToBeReleased;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Portal.Models.PriceConfirmationApprovalByPC;
using Telerik.Reporting.XmlSerialization;
using System.Xml;
using System.Text;
using Telerik.Reporting.Processing;
using System.Globalization;
using Toyota.Common.Web.Workflow;

using Toyota.Common.Web.MessageBoard;

namespace Portal.Controllers
{
    public class PcToBeReleasedController : BaseController
    {
        private readonly string ModuleId = "2";
        private readonly string FunctionId = "25001";
        private readonly string ScreenId = "PcToBeReleased";

        string IDUSER = "niit.suwito";
        
        public PcToBeReleasedController()
            : base("Price Confirmation Approval")
        {
            PageLayout.UseMessageBoard = true;
        }
        protected override void StartUp() {
            User u = Model.GetModel<User>();
            if (u != null)
            {
                TempData["FullName"] = u.FullName;


            }
            else
                TempData["FullName"] = "";
            
        
        }
        protected List<PcToBeReleasedDetail> PcToBeReleasedList()
        {
            return (new List<PcToBeReleasedDetail>());
        }

        public ActionResult ReloadGridDetails(string PCNo)
        {
            PCNo = Request.Params["PCNo"];
            List<PriceConfirmationApprovalByPCDetail> LogList1 = new List<PriceConfirmationApprovalByPCDetail>();
            
            PriceConfirmationApprovalByPCModel mdl = new PriceConfirmationApprovalByPCModel();
            
            LogList1 = DB.Fetch<PriceConfirmationApprovalByPCDetail>("GetPriceConfirmationApprovalByPC", new object[] { PCNo }).ToList();
            
            mdl.PriceConfirmationApprovalByPCDetails = LogList1;
            Model.AddModel(mdl);
            return PartialView("PriceConfirmationApprovalByPCPartial", Model);
        }

        private IDBContext _db = null;
        private IDBContext DB
        {
            get
            {
                if (_db == null)
                {

                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (_db != null)
            {
                _db.Close();
                _db = null;
            }

            base.OnResultExecuted(filterContext);
        }

        public ActionResult ReloadGrid(
                string PCDate, string PCTo, 
                string EffectiveDate, string EffectiveDateTo,                 
                string StatusName, string CurrentPIC, string PCNo)
        {
            User u = Model.GetModel<User>();
            List<PcToBeReleasedDetail> LogList = new List<PcToBeReleasedDetail>();
            string grlLPHeader = Request.Params["grlLPHeader"];
            string grlLPHeaderPCNo = Request.Params["grlLPHeaderPCNo"];
            string Status = string.Empty;
            if (Request.Params["Status"] == "All")
            {
                Status = "";
            }
            else
            {
                Status = Request.Params["Status"];
            }

            
            PcToBeReleasedModel model = Model.GetModel<PcToBeReleasedModel>();
            
            LogList = DB.Fetch<PcToBeReleasedDetail>("GetPcToBeReleased", new object[] { 
                    grlLPHeaderPCNo, 
                    grlLPHeader, 
                    Status, 
                    PCDate, PCTo, 
                    EffectiveDate, EffectiveDateTo, 
                    CurrentPIC}).ToList();

            for (int i = 0; i < LogList.Count; i ++) 
            {
                PcToBeReleasedDetail v = LogList[i];

                v.SHApprovalStatusIcon = SetStatus(v.SHApprovalStatus);
                v.DPHApprovalStatusIcon = SetStatus(v.DPHApprovalStatus);
                v.DHApprovalStatusIcon = SetStatus(v.DHApprovalStatus);
                v.Notice = SetNoticeIconString(i+1);
            }

            model.PcToBeaReleasedDetails = LogList;
            
            int defaultReject = 0;

            if (AuthorizedUser.PositionName == "SH")
                defaultReject = 1;
            else
                defaultReject = 0;


            string vp = "";
            string p = "";

            if (u != null && u.PositionDetails != null && u.PositionDetails.Count > 0)
            {
                p = u.PositionDetails[0].Position;

                

                vp = DB.SingleOrDefault<string>("GetUserPositionAcronym", new object[] { p.ToUpper() });

                
            }

            ViewData["Position"] = vp;
            ViewData["PositionAbbr"] = p;
            ViewData["defaultReject"] = defaultReject;
            TempData["defaultReject"] = defaultReject;
            
            return PartialView("PcToBeReleasedPartial", Model);
        }
        protected override void Init()
        {
            PcToBeReleasedModel mdl = new PcToBeReleasedModel();
            List<PcToBeReleasedCpoDetail> cpo = new List<PcToBeReleasedCpoDetail>();
            PcToBeReleasedModel model = new PcToBeReleasedModel();
            model.PcToBeaReleasedDetails = PcToBeReleasedList();
            Model.AddModel(model);

            
            List<Supplier> listSuppliers = new List<Supplier>();
            var QueryLog = DB.Query<Supplier>("GetAllSupplier");
            listSuppliers = QueryLog.ToList<Supplier>();

            ViewData["ListSupplierData"] = listSuppliers;

            List<PCNo> listPCNOs = new List<PCNo>();
            var QueryLog1 = DB.Query<PCNo>("GetAllPCNo");
            listPCNOs = QueryLog1.ToList<PCNo>();

            ViewData["ListPCNoData"] = listPCNOs;

            User u = Model.GetModel<User>();
            int defaultReject = 0;
            if (u!=null && u.PositionName== "SH")
                defaultReject = 1;
            else
                defaultReject = 0;

            TempData["defaultReject"] = defaultReject;
            ViewData["defaultReject"] = defaultReject;
            ViewData["StatusPO"] = listStatusPOApproval();
        }


        protected List<ApprovalSectionHead> ListApprovalSectionHead(string PCNO)
        {
            
            List<ApprovalSectionHead> LogList = new List<ApprovalSectionHead>();
            var QueryLog = DB.Query<ApprovalSectionHead>("GetApprovalSectionHead", new object[] { PCNO });

            int index = 1;
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new ApprovalSectionHead()
                    {
                        No = index,
                        Name = q.Name,
                        Status = q.Status,
                        Date = q.Date,
                        Comment = q.Comment
                    });
                    index++;
                }
            }
            
            return LogList;
        }

        protected List<ApprovalDepartementHead> ListApprovalDepartementHead(string PCNO)
        {
            
            List<ApprovalDepartementHead> LogList = new List<ApprovalDepartementHead>();
            var QueryLog = DB.Query<ApprovalDepartementHead>("GetApprovalDepartementHead", new object[] { PCNO });

            int index = 1;
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new ApprovalDepartementHead()
                    {
                        No = index,
                        Status = q.Status,
                        Date = q.Date,
                        Comment = q.Comment
                    });
                    index++;
                }
            }
            
            return LogList;
        }

        protected List<ApprovalDivisionHead> ListApprovalDivisionHead(string PCNO)
        {
            
            List<ApprovalDivisionHead> LogList = new List<ApprovalDivisionHead>();
            var QueryLog = DB.Query<ApprovalDivisionHead>("GetApprovalDivisionHead", new object[] { PCNO });

            int index = 1;
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new ApprovalDivisionHead()
                    {
                        No = index,
                        Status = q.Status,
                        Date = q.Date,
                        Comment = q.Comment
                    });
                    index++;
                }
            }
            
            return LogList;
        }

        public List<Supplier> GetAllSuppliers()
        {
            
            List<Supplier> listSuppliers = new List<Supplier>();
            var QueryLog = DB.Query<Supplier>("GetAllSupplier");
            listSuppliers = QueryLog.ToList<Supplier>();
            
            return listSuppliers;
        }

        public List<PCNo> GetAllPCNO()
        {
            
            List<PCNo> listPCNOs = new List<PCNo>();
            var QueryLog = DB.Query<PCNo>("GetAllPCNo");
            listPCNOs = QueryLog.ToList<PCNo>();
            
            return listPCNOs;
        }

        public ActionResult PCNOPartial()
        {
            TempData["GridName"] = "grlLPHeaderPCNo";
            ViewData["ListPCNoData"] = GetAllPCNO();
            return PartialView("GridLookup/PartialGrid", ViewData["ListPCNoData"]);
        }

        public ActionResult SupplierPartial()
        {
            TempData["GridName"] = "grlLPHeader";
            ViewData["ListSupplierData"] = GetAllSuppliers();
            return PartialView("GridLookup/PartialGrid", ViewData["ListSupplierData"]);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("PcToBeReleasedHeader", Model);
        }

        public ActionResult ActionLinkToPriceConfirmation(string SupplierCode)
        {
            return RedirectToAction("Index", "PriceConfirmationApprovalByPC");
        }
        public ActionResult PcToBeReleasedPartial()
        {
            return PartialView("PcToBeReleasedPartial", Model);
        }

        
        public void DownloadPdf(string PcNo)
        {
            string conString = DBContextNames.DB_PCS;
            string sqlCommand = "SP_REP_PC_TO_BE_RELEASED_PRINT";
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PCNo", System.Data.DbType.String, PcNo);

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PCDownload\Rep_PC.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("pdf", rpt, null);
            String CurrDate = DateTime.Now.ToString("yyyyMMddhhmm");
            string fileName = "Price Confirmation_" + CurrDate + ".pdf";

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.Flush();
            Response.End();
        }
        public ActionResult PCToBeReleasedHeaderDetails(String PCNo)
        {
            ViewData["PCNo"] = PCNo;
            ViewData["AprovalStatus"] = AuthorizedUser.PositionName;
            PriceConfirmationApprovalByPC ListPrice = DB.SingleOrDefault<PriceConfirmationApprovalByPC>("GetHeaderPriceConfirmationApprovalByPC", new object[] { PCNo });
            PriceConfirmationApprovalByPC listRow = new PriceConfirmationApprovalByPC();
            if (ListPrice != null)
            {
                listRow.StatusApprove = StaApprove(ListPrice.SHApprovalStatus, ListPrice.DPHApprovalStatus, ListPrice.DHApprovalStatus);
                listRow.StatusReject = StaReject(ListPrice.SHApprovalStatus, ListPrice.DPHApprovalStatus, ListPrice.DHApprovalStatus);
                
                ViewData["ApprovalStatusLogin"] = null;

                listRow.PCDate = ListPrice.PCDate;
                
                listRow.EffectiveDate = ListPrice.EffectiveDate;
                listRow.SupplierName = ListPrice.SupplierName;
                
                listRow.Status = ListPrice.Status;
                listRow.PCNo = ListPrice.PCNo;
            }
            else {
                    ListPrice = new PriceConfirmationApprovalByPC();
            }
            ;
            ViewData["ApprovalSectionHeadList"] = ListApprovalSectionHead(PCNo);
            ViewData["ApprovalDepartementHeadList"] = ListApprovalDepartementHead(PCNo);
            ViewData["ApprovalDivisionHeadList"] = ListApprovalDivisionHead(PCNo);
            
            return PartialView("PriceConfirmationHeader", ListPrice);
        }

        public ActionResult PCToBeReleasedDetailPartial(String PCNo)
        {
            ViewData["PCNo"] = PCNo;
            PriceConfirmationApprovalByPCModel model1 = new PriceConfirmationApprovalByPCModel();
            model1.PriceConfirmationApprovalByPCDetails = PriceConfirmationApprovalByDetailList(PCNo);
            Model.AddModel(model1);
            return PartialView("PriceConfirmationApprovalByPCPartial", Model);
        }

        protected List<PriceConfirmationApprovalByPCDetail> PriceConfirmationApprovalByDetailList(string PCNO)
        {
            List<PriceConfirmationApprovalByPCDetail> LogList = new List<PriceConfirmationApprovalByPCDetail>();
            LogList = DB.Fetch<PriceConfirmationApprovalByPCDetail>("GetPriceConfirmationApprovalByPC", new object[] { PCNO }).ToList();
            if (LogList == null)
                LogList = new List<PriceConfirmationApprovalByPCDetail>();
            
            return LogList;
        }

        private string StaApprove(string SH, string DpH, string DH)
        {
            string StatusApprove = string.Empty;
            string AprovalStatus = AuthorizedUser.PositionName;
            if ((SH == null) && (DpH == null) && (DH == null))
            {
                if (AprovalStatus == "SH")
                {
                    StatusApprove = "Enabled";
                }
                else { StatusApprove = "Disabled"; }
            }
            else if ((SH == "APPROVE") && (DpH == null) && (DH == null))
            {
                if (AprovalStatus == "DPH")
                {
                    StatusApprove = "Enabled";
                }
                else { StatusApprove = "Disabled"; }
            }
            else if ((SH == "APPROVE") && (DpH == "APPROVE") && (DH == null))
            {
                if (AprovalStatus == "DH")
                {
                    StatusApprove = "Enabled";
                }
                else { StatusApprove = "Disabled"; }
            }
            else if ((SH == "APPROVE") && (DpH == "APPROVE") && (DH == "APPROVE"))
            {
                StatusApprove = "Disabled";
            }
            return StatusApprove;
        }

        private string StaReject(string SH, string DpH, string DH)
        {
            string StatusReject = string.Empty;
            string AprovalStatus = AuthorizedUser.PositionName;
            if ((SH == "REJECT") && (DpH == null) && (DH == null))
                StatusReject = "Disabled";
            else if ((SH == "REJECT") && (DpH == "REJECT") && (DH == null))
                StatusReject = "Disabled";
            else if ((SH == "REJECT") && (DpH == "REJECT") && (DH == "REJECT"))
                StatusReject = "Disabled";
            else if ((SH == null) && (DpH == "REJECT") && (DH == null))
                if (AprovalStatus == "SH")
                    StatusReject = "Enabled";
                else if ((SH == "APPROVE") && (DpH == null) && (DH == "REJECT"))
                    if (AprovalStatus == "DPH")
                        StatusReject = "Enabled";
            return StatusReject;
        }


        public ActionResult SHApprove(string PCNO, string Comments)
        {

            string SH = AuthorizedUser.PositionName;
            
            
            int ResultSHApprove = DB.Execute("SetSHApprove", new object[] { IDUSER, PCNO, Comments, SH });
            
            return Content(ResultSHApprove.ToString());
        }

        public ActionResult DPHApprove(string PCNO, string Comments)
        {

            string SH = AuthorizedUser.PositionName;
            
            
            int ResultDPHApprove = DB.Execute("SetDPHApprove", new object[] { IDUSER, PCNO, Comments, SH });
            
            return Content(ResultDPHApprove.ToString());
        }

        public ContentResult ActionPurchaseOrder(string DocNoList, string ActionBut, string commentAction, string RejectType)
        {
            IDBContext db = DB;
            string result = DocNoList;
            string userName = AuthorizedUser.Username;
            int positionLevel = Convert.ToInt16(AuthorizedUser.PositionLevel);
            string position = string.Empty;
            string action = string.Empty;
            string comment = string.Empty;
            string PoNo = string.Empty;
            string rejectType = RejectType;

            int resultDB = 0;
            string resultSuccess = "";
            string resultFailed = "";
            string resultWrongUser = "";
            string resultProcess = "";
            string processType = "";
            StringBuilder log = new StringBuilder("");
 

            List<string> resultOk = new List<string>();
            List<string> resultNG = new List<string>();
            List<string> resultNA = new List<string>();
            
            action = ActionBut.ToUpper();

            if (action == "APPROVE")
            {
                processType = "Approve";
            }
            else if (action == "REJECT")
            {
                processType = "Reject";
            }
            else
            {
                processType = "Redirect";
            }
            StringBuilder b = new StringBuilder("");

            log.AppendFormat("ActionPurchaseOrder DocNoList= {0} Action={1} Comment={2} RejectType={3}\r\n", DocNoList, ActionBut, commentAction, RejectType); 
            String[] Documents = DocNoList.Split(',');
            
            string ApproveOut = "";
            int silent = ((Documents != null) && (Documents.Length > 1)) ? 1: 0;
            int rType = 2;
            int.TryParse(rejectType, out rType);
            if (action == "REJECT") 
            {
                // check first 
                if (rType == 1)
                {
                    for (int i = 0; i < Documents.Length; i++)
                    {
                        string DocNo = Documents[i];
                        int h = db.ExecuteScalar<int>("PreActionCheck", new object[] { DocNo, 2, userName, 2 });
                        if (h == -2)
                            resultNG.Add(DocNo);
                        else if (h == -1)
                            resultNA.Add(DocNo);
                        else
                            resultOk.Add(DocNo);
                    }
                    if (resultOk.Count < Documents.Length)
                    {
                        //resultProcess = "Not All Rejection Can be processed" + Environment.NewLine;
                    }
                    if (resultOk.Count > 0)
                    {
                        resultSuccess = string.Join(",", resultOk.ToArray());
                        db.Execute("SendRejectionNotification", new object[] { resultSuccess, commentAction, userName, rType, 2 });
                    }
                }
                else
                {
                    List<string> CancelDoc = new List<string>();
                    for (int i = 0; i < Documents.Length; i++)
                    {
                        string doc = Documents[i];
                        int lastStatus = db.ExecuteScalar<int>("GetLastApprovedStatus", new object[] { doc, 2 });
                        if ((lastStatus == 0))
                            CancelDoc.Add(doc);
                    }
                    if (CancelDoc.Count > 0)
                    {
                        resultSuccess = string.Join(",", CancelDoc.ToArray());
                        db.Execute("SendRejectionNotification", new object[] { resultSuccess, commentAction, userName, 1, 2 });
                        if (CancelDoc.Count == Documents.Length)
                            rType = 1;
                    }
                }
                resultSuccess = "";
                resultOk.Clear();
                resultNG.Clear();
                resultNA.Clear();
            }

            foreach (string doc in Documents)
            {
                if (action == "APPROVE")
                {
                    resultDB = WorkflowFunction.ApproveWorklistPC(doc, "2", userName, ref ApproveOut, silent);
                    log.AppendFormat("Workflow.ApproveWorklistPC folio:{0}, module:2, ProceedBy:{1}, ApproveMessage:{2}, silent:{3}={4}\r\n", doc, userName, ApproveOut, silent, resultDB); 
                    if (resultDB < 0)
                    {
                        b.AppendLine("Error approve " + doc + ": " + ApproveOut);
                    }
                }
                else if (action == "REJECT")
                {
                    string recipient = "";
                    if (rejectType.Equals("1"))
                    {
                        recipient = WorkflowFunction.GetRegistrant(doc, "2");
                    }
                    resultDB = WorkflowFunction.RejectWorklistPC(doc, "2", userName, RejectType, silent);
                    log.AppendFormat("Workflow.RejectWorklistPC folio:{0}, module:2, ProceedBy:{1}, RejectType:{2}, silent:{3}={4}\r\n", doc, userName, RejectType, silent, resultDB); 
                    if (rejectType.Equals("2"))
                    {
                        List<string> s =  WorkflowFunction.GetNextApproverWorklist(doc, "2");
                        if (s != null && s.Count > 0)
                            recipient = s[0];
                        
                        if (string.IsNullOrEmpty( recipient ) && (s == null || s.Count < 1))
                            recipient = WorkflowFunction.GetRegistrant(doc, "2");
                    }

                    if (resultDB >= 0)
                    {
                        MessageBoardMessage boardMessage = new MessageBoardMessage();
                        boardMessage.GroupId = db.ExecuteScalar<int>("GetLastGroupID", new object[] { "PcToBeReleased_" + doc });
                        boardMessage.BoardName = "PcToBeReleased_" + doc;
                        boardMessage.Sender = AuthorizedUser.Username;
                        boardMessage.Recipient = recipient;
                        boardMessage.CarbonCopy = false;
                        boardMessage.Text = commentAction;
                        boardMessage.Date = DateTime.Now;
                        IMessageBoardService boardService = MessageBoardService.GetInstance();
                        boardService.Save(boardMessage);

                        
                    }
                }
                else
                {
                    resultDB = WorkflowFunction.RedirectWorklist(doc, "2", userName, positionLevel, silent);
                    log.AppendFormat("Workflow.RedirectWorklist folio:{0}, module:2, ProceedBy:{1}, PositionLevel:{2}, silent:{3}={4}\r\n", doc, userName, positionLevel, silent, resultDB); 
                }


                if (resultDB == -1)
                {
                    resultNA.Add(doc);
                    log.AppendLine("NA");
                }
                else if (resultDB == -2)
                {
                    resultNG.Add(doc);
                    log.AppendLine("NG");
                }
                else
                {
                    resultOk.Add(doc);
                    log.AppendLine("OK");
                }
            }

            resultSuccess = string.Join(",", resultOk.ToArray());
            resultFailed = string.Join(",", resultNG.ToArray());
            resultWrongUser = string.Join(",", resultNA.ToArray());

            if (resultSuccess != "")
            {
                if (resultOk.Count > 10)
                    resultSuccess = Environment.NewLine + WorkflowFunction.SplitLines(resultSuccess);
                resultProcess = "INFO : Success " + processType + " Data with PC Number = " + resultSuccess + Environment.NewLine;

                if (action != "REJECT")
                    db.Execute("SendBulkApprovalNotification", new object[] { 2, resultSuccess });
                else if (action == "REJECT" && rType == 2)
                    db.Execute("SendRejectionNotification", new object[] { resultSuccess, commentAction, userName, rType, 2 });
            }

            if (resultFailed != "")
            {
                if (resultNG.Count > 10)
                    resultFailed = Environment.NewLine + WorkflowFunction.SplitLines(resultFailed);

                resultProcess = resultProcess + "ERROR : Failed " + processType + " Data with PC Number = " + resultFailed + Environment.NewLine;
            }

            if (resultWrongUser != "")
            {
                if (resultNA.Count > 10)
                    resultWrongUser = Environment.NewLine + WorkflowFunction.SplitLines(resultWrongUser);
                resultProcess = resultProcess + "ERROR : User does not have authorization to " + processType + " with PC Number = " + resultWrongUser;
            }

            if (b.Length > 0)
                resultProcess = resultProcess + b.ToString();

            log.AppendLine(resultProcess);

            PutLog(log.ToString(), userName, "Approve", 0, "MPCS00001INF", "INF", ModuleId, FunctionId, 0); 
            return Content(resultProcess);
        }

        public Boolean AddRegisterPC(string FolioNo, string ModuleCd, int StatusCd, String UserSubmit)
        {
            bool test = WorkflowFunction.RegisterWorklistPC(FolioNo, ModuleCd, StatusCd, UserSubmit);
            return test;
        }


        public ActionResult DHApprove(string PCNO, string Comments)
        {

            string SH = AuthorizedUser.PositionName;
            
            
            int ResultDHApprove = DB.Execute("SetDHApprove", new object[] { IDUSER, PCNO, Comments, SH });
            
            return Content(ResultDHApprove.ToString());
        }

        public ActionResult SHReject(string PCNO, string Comment)
        {
            int ResultSHReject = DB.Execute("SetSHReject", new object[] { IDUSER, PCNO, Comment });
            
            return Content(ResultSHReject.ToString());
        }

        public ActionResult DPHReject(string PCNO, string Comment)
        {
            int ResultDPHReject = DB.Execute("SetDPHReject", new object[] { IDUSER, PCNO, Comment });
            
            return Content(ResultDPHReject.ToString());
        }

        public ActionResult DHReject(string PCNO, string Comment)
        {
            int ResultDHReject = DB.Execute("SetDHReject", new object[] { IDUSER, PCNO, Comment });
            
            return Content(ResultDHReject.ToString());
        }

        public ActionResult PCToBeReleasedPartial()
        {
            return PartialView("PriceConfirmationApprovalByPCPartial", Model);
        }

        public ActionResult PopupApprovePartial()
        {
            return PartialView("PopupApprove", Model);
        }

        public ActionResult PartialHeaderPrice()
        {
            return PartialView("PriceConfirmationHeader", Model);
        }

        public ActionResult PartialFooter()
        {
            return PartialView("PriceConfirmationFooter", Model);
        }

        public ActionResult ActionLinkToPcToBeReleased(string SupplierCode)
        {
            return RedirectToAction("Index", "PcToBeReleased");
        }

        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_close_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_open_icon.png";
            string imageBytes4 = "~/Content/Images/chat_3_icon.png";
            //string imageBytes2 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkJJREFUeNpkUk1rFEEQfdXT85XsGkPcHMSD6Aa8evCoSO4BT+JJ8A8oguLBHPwDIip4EPwDCmpAES8eDILKJsegHrwEURKy2Y/JZmdnusvq3llItKAoerqr3nv1hlqtFlwYpIiwNTOLL5eP8relmv11JuBRHRxlAH2H6r8B7T6X2gaGADOo1VqTxgRz+LB0glbux1wuoFQyjf0DHyRnIkBlP6F/3wKyV5ArVaKOeaxcOU3PXsc8tYA8APIeUOxKdoDSZVeGDaSmp1DMvwSrq2AD6qydb9a5va64WUcuDaNtwBqBs3DTPSIkSQNBIpkC4fYA4eY5PUOrN2Av1lHI5OEfQcjloTSiouwK05g6FzLT3dWmEIY3Ndvji2TiqnFfHpRVX9U8CX+0YxZ2WljXLmhiNFDK9uxedclV0v8DnBwyvhLbWc0oumR6DY9qnQV2TBGV1gms27yTYx19LV8Gfb2DxU/H7GoTlsd67ci7Xnl0AFaaVCRVFkdDWfjgqwL1HkH3C79NEptEh6PFPkW/S2+q8zqUKqhhR76WD9Uc3q1zNLiDSFBVOEY4GP4HcYNlqVoyyVCGvWWx+bP2q1HmAdJ2Dq7dg40aTjd5jcF4oKOrFTjOemWcLYvyx06Rpo7QSIREWD5RSf9kUBy5DZ6WBtEdOCLUhR5u2ih/XyrzVEb+mJDStK9gFGGo+Gxa0DVvR2hQxvmGCsx1xbwhy92y5P/2Q6EFPhQGl9Kc7uqR2jHJ3sdCm7cmwIuE0T9k+z/xV4ABAP9iEDZwmfEuAAAAAElFTkSuQmCCAA==";

            if (val % 4 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 4 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 4 == 2)
            {
                img = imageBytes3;
            }
            else if (val % 4 == 3)
            {
                img = imageBytes4;
            }

            return img;
        }


        private string SetStatus(string status)
        {
            string img = string.Empty;

            string[] aStatusImg = new string[] {"yellow_1", "Green_1", "red", "gray"};
            int id=0;
            if (int.TryParse(status, out id) && id > 0 && id < 5)
            {
                id -= 1;
                img = "~/Content/Images/" + aStatusImg[id] + ".png";
            }
 
            return img;
        }

        public ActionResult GridLookupSupplier()
        {
            TempData["GridName"] = "grlSupplier";
            ViewData["ListSupplierData"] = GetAllSupplierList();
            return PartialView("GridLookup/PartialGrid", ViewData["ListSupplierData"]);
        }

        public List<Supplier> GetAllSupplierList()
        {
            
            List<Supplier> listSupplier = new List<Supplier>();
            var QueryLog = DB.Query<Supplier>("GetAllSupplier");
            listSupplier = QueryLog.ToList<Supplier>();
            
            return listSupplier;
        }
        public ActionResult StatusPurchaseOrder()
        {
            ViewData["StatusPO"] = listStatusPOApproval();
            TempData["GridName"] = "GridStatusPO";
            
            return PartialView("GridLookup/PartialGrid", ViewData["StatusPO"]);
        }

        public void DownloadExport(String p_Pcnos, String p_SuppCd, String p_PcDateBegin, String p_PcDateEnd, String p_EfDateBegin, String p_EfDateEnd, String p_CurrPIC)
        {
            string conString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PcToBeReleased\HeaderPcToBeReleased.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            /*ssqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            qlDataSource.SelectCommand = @"select R.PO_NO, R.PO_DATE, M.SUPPLIER_NAME, R.TOTAL_AMOUNT, R.REQUIRED_DT
                                            from TB_R_PO_APPROVAL R inner join TB_M_SUPPLIER M
                                            on R.SUPPLIER_CD = M.SUPPLIER_CODE";*/

            /*string sqlCommand = "SP_REP_PC_TO_BE_RELEASED_PRINT";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PCNo", System.Data.DbType.String, "a");*/

            //added fid.deny 2015-07-23
            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource.SelectCommand = "SP_REP_PC_TO_BE_RELEASED_PRINT_HEADER";

            sqlDataSource.Parameters.Add("@p_Pcnos", System.Data.DbType.String, p_Pcnos);
            sqlDataSource.Parameters.Add("@p_SuppCd", System.Data.DbType.String, p_SuppCd);
            sqlDataSource.Parameters.Add("@p_PcDateBegin", System.Data.DbType.String, p_PcDateBegin);
            sqlDataSource.Parameters.Add("@p_PcDateEnd", System.Data.DbType.String, p_PcDateEnd);
            sqlDataSource.Parameters.Add("@p_EfDateBegin", System.Data.DbType.String, p_EfDateBegin);
            sqlDataSource.Parameters.Add("@p_EfDateEnd", System.Data.DbType.String, p_EfDateEnd);
            sqlDataSource.Parameters.Add("@p_CurrPIC", System.Data.DbType.String, p_CurrPIC);
            //end of added fid.deny


            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "PcToBeReleased" + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }

        public void DownloadExportDetails()
        {
            
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PcToBeReleased\PcToBeReleased.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            /*ssqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            qlDataSource.SelectCommand = @"select R.PO_NO, R.PO_DATE, M.SUPPLIER_NAME, R.TOTAL_AMOUNT, R.REQUIRED_DT
                                            from TB_R_PO_APPROVAL R inner join TB_M_SUPPLIER M
                                            on R.SUPPLIER_CD = M.SUPPLIER_CODE";*/
            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "PcToBeReleased" + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }

        public List<StatusPOApproval> listStatusPOApproval()
        {
            List<StatusPOApproval> getList = new List<StatusPOApproval>();
            getList.Add(new StatusPOApproval
            {
                StatusCode = "All"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Pending SH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Pending DpH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Pending DH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Approved by SH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Approved by DpH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Approved by DH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Rejected by SH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Rejected by DpH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Rejected by DH"
            });
            return getList;
        }

        public ActionResult UpdateCallBackPanel()
        {
            ViewData["PCNo"] = Request.Params["PCNo"];
            return PartialView("PopUpCallBackPartial");
        }

        public long PutLog(string msg, string uid, string loc = "", long pid = 0,
            string id = "INF", string typ = "INF", string module = "",
            string func = "", int sts = 0)
        {
            if (string.IsNullOrEmpty(module)) module = ModuleId;
            if (string.IsNullOrEmpty(func)) func = FunctionId;

            long r = DB.ExecuteScalar<long>("PutLog",
                    new object[] { msg, uid, loc, pid
                    , id, typ, module, func, sts}
                );
            return r;
        }


        public string GetFullName(string u)
        {
            return DB.SingleOrDefault<string>("GetEmployeeFullNameByUsername", new object[] { u });
        }
    }
}
