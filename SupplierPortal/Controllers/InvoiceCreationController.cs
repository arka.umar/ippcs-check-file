﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Portal.Models.Dock;
using Portal.Models.Globals;
using Portal.Models.InvoiceCreation;
using Portal.Models.Supplier;
using ServiceStack.Text;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using System.Data.OleDb;
using System.Diagnostics;
using System.Windows.Forms;
using Portal.Models;

namespace Portal.Controllers
{
    public partial class InvoiceCreationController : BaseController
    {
        private readonly string screenID = "InvoiceCreation";
        private readonly string osuppID = "OIHSupplierOption";
        private readonly string odockID = "OIHDockOption";
        private readonly string ModuleId = "5";
        private readonly string FunctionId = "51002";
        private readonly string CancelFunctionId = "51006";
        private readonly string FxSuppCd = "51002.SUPP_CD";
        private readonly string ErrNoUid = "User Session not found, logout then login again, contact support if problem persists";
        private readonly string ErrNoUpload = "Only Supplier can do this operation";

        private readonly string IN_FMT = "dd.MM.yyyy";
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        private string _supplierCode = null;
        private string SupplierCode
        {
            get
            {
                if (string.IsNullOrEmpty(_supplierCode))
                {
                    do
                    {
                        _supplierCode = SuppCodeOf(Model.GetModel<User>());
                        if (!string.IsNullOrEmpty(_supplierCode)) break;

                        _supplierCode = Request.Params["SupplierCode"];
                        if (!string.IsNullOrEmpty(_supplierCode)) break;

                        _supplierCode = Request.Params["supplierCode"];
                    } while (false);
                }
                return _supplierCode;
            }

            set
            {
                _supplierCode = value;
            }
        }

        private string LdapDomain
        {
            get
            {
                string x = ConfigurationManager.AppSettings["LdapDomain"];
                if (string.IsNullOrEmpty(x))
                    x = "Toyota";
                return x;
            }
        }
        private string _SelectedSupplier = "";
        private string SelectedSupplier
        {
            get
            {
                _SelectedSupplier = Session[FxSuppCd] as string;
                return _SelectedSupplier;
            }

            set
            {
                Session[FxSuppCd] = value;
            }
        }

        public InvoiceCreationController()
            : base("Invoice Creation")
        {
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        private IDBContext _db = null;
        private IDBContext DB
        {
            get
            {
                if (_db == null)
                {

                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }

        protected List<SupplierICS> Suppliers
        {
            get
            {
                List<SupplierICS> x = Model.GetModel<List<SupplierICS>>();
                if (x == null)
                {
                    x = Model.GetModel<User>().FilteringArea<SupplierICS>(
                        GetAllSupplier(), "SUPP_CD", screenID, osuppID);
                }
                User u = Model.GetModel<User>();
                string uid = (u != null) ? u.Username : "?";
                string suppcd = SuppCodeOf(uid);
                if (!string.IsNullOrEmpty(suppcd))
                    x = x.Where(a => a.SUPP_CD == suppcd).ToList();
                return x;
            }
        }

        protected List<string> CoilSuppliers
        {
            get
            {
                List<string> csu = Model.GetModel<List<string>>();
                if (csu == null)
                {
                    csu = DB.Fetch<string>("GetCoilSuppliersCode").ToList();
                }
                return csu;
            }
        }

        protected InvoiceCreationModel MyModel
        {
            get
            {
                InvoiceCreationModel m = Model.GetModel<InvoiceCreationModel>();
                if (m == null)
                {
                    m = new InvoiceCreationModel();
                    Model.AddModel(m);
                }
                return m;
            }

        }

        protected List<DockData> Docks
        {
            get
            {
                List<DockData> x = Model.GetModel<List<DockData>>();

                if (x == null)
                {
                    x = DB.Fetch<DockData>("GetAllDock")
                        .ToList();
                }
                return x;
            }
        }

        protected IEnumerable<ComboData> TransactionType
        {
            get
            {
                IEnumerable<ComboData> tt = Model.GetModel<IEnumerable<ComboData>>();
                if (tt == null)
                {
                    tt = DB.Fetch<ComboData>("GetTransactionType");
                }
                return tt;
            }
        }


        protected override void StartUp()
        {
            TempData["SupplierCode"] = SupplierCode;
            ViewData["InvoiceStatusGridLookup"] = GetAllInvoiceStatus();
            ViewData["InvoiceTax"] = BindInvoiceTax();
            ViewData["TaxGridLookup"] = GetTaxListStart();
            MyModel.LockReff = DB.SingleOrDefault<string>("GetInvoiceCreationUploadInProgress", new object[] { SupplierCode, FunctionId }); ;
            Model.AddModel(Suppliers);
            Model.AddModel(Docks);
            Model.AddModel(TransactionType);
        }

        protected override void Init()
        {
            MyModel.Preview = new List<InvoiceCreationPreview>();
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (_db != null)
            {
                _db.Close();
                _db = null;
            }

            base.OnResultExecuted(filterContext);
        }

        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }
        public JsonResult GetSelectedInvoices(string manifests,
            string supplierCode,
            string dockCode,
            string manifestNo,
            string orderNo,
            string dateFrom,
            string dateTo,
            string uploadedOnly)
        {
            string invoices = ViewData["invoices"] as string;

            int onlyUploaded = 0;

            if (string.IsNullOrEmpty(manifests))
                manifests = manifestNo;


            if ("true".Equals(uploadedOnly.ToLower()))
                onlyUploaded = 1;

            List<InvoiceCreationPreview> iPre
                = DB.FetchStatement<InvoiceCreationPreview>("exec pcs_GetAllInvoicePreview @0, @1, @2, @3, @4, @5, @6",
                new object[] { supplierCode, dockCode, manifests, orderNo, dateFrom, dateTo, onlyUploaded })
                .ToList();

            MyModel.Preview = iPre;

            return Json(iPre.Select(a => a.InvoiceNo).ToList());
        }

        private string SuppCodeOf(User u)
        {
            if (u == null || u.Authorization == null || u.Authorization.Count < 1) return null;

            string ag = (from authSource in u.Authorization
                         from item in authSource.AuthorizationDetails
                         where item.Screen.ToLower() == screenID.ToLower() &&
                             item.Object.ToLower() == osuppID.ToLower() &&
                             (item.AuthorizationGroup.ToLower() != "null")
                         select item.AuthorizationGroup).FirstOrDefault();
            return ag;
        }

        private string SuppCodeOf(string uid)
        {
            string[] param = uid.Split('.');

            return param[0];

            //Regex rx = new Regex("([0-9]+)(\\.)(.*)");
            //MatchCollection mx = rx.Matches(uid);
            //if (mx.Count > 0)
            //{
            //    sCode = mx[0].Groups[1].Value;
            //}
            //return sCode;
        }

        public void FillInvoiceCreationDetail(string manifests, bool isInit = false)
        {
            List<string> manifestNums = new List<string>();
            if (!string.IsNullOrEmpty(manifests))
                manifestNums = manifests.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            string supplierCode = "";
            string manifestNo = "";
            string dockCode = "";
            string orderNo = "";

            string dfrom, dto;
            int onlyUploaded = 0;
            dfrom = null;
            dto = null;

            if (Request != null)
            {
                supplierCode = Request.Params["SupplierCode"] ??
                    Request.Params["supplierCode"]; 
                if (string.IsNullOrEmpty(supplierCode))
                {
                    supplierCode = SupplierCode;
                }
                dockCode = Request.Params["DockCode"];
                orderNo = Request.Params["OrderNo"];
                dfrom = Request.Params["DateFrom"];
                dto = Request.Params["DateTo"];
                manifestNo = Request.Params["manifestNo"];
                string onlyUploadedString = Request.Params["UploadedOnly"];
                if ("true".Equals(onlyUploadedString))
                    onlyUploaded = 1;
            }

            List<InvoiceCreation> l = GetAllInvoiceCreation(
                    SupplierCode,
                    dockCode,
                    manifestNo,
                    orderNo,
                    dfrom, dto, onlyUploaded);
            MyModel.InvoiceCreationLists = l;

            List<string> invoices = l.Select(x => x.InvoiceNo).Distinct().ToList();

            Model.AddModel(invoices);
            ViewData["invoices"] = string.Join(";", invoices.ToArray());
        }

        public ActionResult InvoiceCreationPartial(string manifests)
        {
            List<string> manifestNums = new List<string>();
            if (!string.IsNullOrEmpty(manifests))
                manifestNums = manifests.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            string supplierCode = "";
            string manifestNo = "";
            string dockCode = "";
            string orderNo = "";

            string dfrom, dto;
            int onlyUploaded = 0;
            dfrom = null;
            dto = null;

            if (Request != null)
            {
                supplierCode = Request.Params["SupplierCode"] ??
                    Request.Params["supplierCode"];
                if (string.IsNullOrEmpty(supplierCode))
                {
                    supplierCode = SupplierCode;
                }
                dockCode = Request.Params["DockCode"];
                orderNo = Request.Params["OrderNo"];
                dfrom = Request.Params["DateFrom"];
                dto = Request.Params["DateTo"];
                manifestNo = Request.Params["manifestNo"];
                string onlyUploadedString = Request.Params["UploadedOnly"];
                if ("true".Equals(onlyUploadedString))
                    onlyUploaded = 1;
            }

            List<InvoiceCreation> l = GetAllInvoiceCreation(
                    SupplierCode,
                    dockCode,
                    manifestNo,
                    orderNo,
                    dfrom, dto, onlyUploaded);
            MyModel.InvoiceCreationLists = l;

            List<string> invoices = l.Select(x => x.InvoiceNo).Distinct().ToList();

            Model.AddModel(invoices);
            ViewData["invoices"] = string.Join(";", invoices.ToArray());

            return PartialView("InvoiceCreationPartial", Model);
        }

        public ActionResult GetUploadInProgress(string supplierCode)
        {
            string b = DB.SingleOrDefault<string>("GetInvoiceCreationUploadInProgress",
                new object[] { SupplierCode, FunctionId });
            if (string.IsNullOrEmpty(b))
            {
                b = DB.SingleOrDefault<string>("GetInvoiceCreationUploadInProgress",
                    new object[] { SupplierCode, CancelFunctionId });
                if (!string.IsNullOrEmpty(b))
                    b = "C" + b;
            }

            SelectedSupplier = supplierCode;

            return Content(b);
        }

        string manifests = "";
        string manifests2 = "";
        string invoices = "";
        string dockCode = "";
        string orderNo = "";
        string status = "";
        string dfrom = "";
        string dto = "";
        int onlyUploaded = 0;

        public ActionResult PopupGridInvoiceNo(string supplierCode, string manifests, string dateFrom, string dateTo, string showOnlyUploaded, string invoicenos)
        {
            dfrom = null;
            dto = null;
            if (Request != null)
            {
                //manifests = Request.Params["manifests"];
                //manifests2 = manifests;
                invoices = Request.Params["invoices"];

                dockCode = Request.Params["DockCode"];
                orderNo = Request.Params["OrderNo"];

                dfrom = Request.Params["DateFrom"];
                dto = Request.Params["DateTo"];

                status = Request.Params["Status"];
                string onlyUploadedString = Request.Params["UploadedOnly"];
                if ("true".Equals(onlyUploadedString))
                    onlyUploaded = 1;
            }

            List<InvoiceCreationPreview> iPre = DB.FetchStatement<InvoiceCreationPreview>("exec pcs_GetInvoiceCreation_PopupInvoiceNo @0, @1, @2, @3, @4, @5, @6, @7",
              new object[] {  SupplierCode,
                                dockCode,
                                manifests,
                                orderNo,
                                dfrom,
                                dto,
                                onlyUploaded,
                                invoices});
            InvoiceCreationModel ListModel = Model.GetModel<InvoiceCreationModel>();
            ListModel.Preview = iPre;
            return PartialView("PopupInvoicePreviewInvoiceGridPartial", Model);
        }

        public JsonResult PopupGridInvoiceDetail(string Manifest,
                                                string InvoiceNumber,
                                                string paramSupplierCode,
                                                string paramdockCode,
                                                string paramorderNo,
                                                string paramdfrom,
                                                string paramdto,
                                                bool paramonlyUploaded)
        {
            List<InvoiceCreationPreview> ListDetail = new List<InvoiceCreationPreview>();
           
            ListDetail = DB.FetchStatement<InvoiceCreationPreview>("exec pcs_GetInvoiceCreation_PopupInvoiceDetail @0, @1, @2, @3, @4, @5, @6, @7", new object[] {  
                    paramSupplierCode,
                    paramdockCode,
                    Manifest,
                    paramorderNo,
                    paramdfrom,
                    paramdto,
                    paramonlyUploaded,
                    InvoiceNumber});

            return Json(ListDetail);
        }

        public ActionResult PopupInvoicePreviewPartial()
        {
            string manifests = "";
            string invoices = "";
            string dockCode = "";
            string orderNo = "";
            string status = "";
            string dfrom = "";
            string dto = "";
            int onlyUploaded = 0;
            dfrom = null;
            dto = null;
            if (Request != null)
            {
                manifests = Request.Params["manifests"];
                invoices = Request.Params["invoices"];

                dockCode = Request.Params["DockCode"];
                orderNo = Request.Params["OrderNo"];

                dfrom = Request.Params["DateFrom"];
                dto = Request.Params["DateTo"];

                status = Request.Params["Status"];
                string onlyUploadedString = Request.Params["UploadedOnly"];
                if ("true".Equals(onlyUploadedString))
                    onlyUploaded = 1;
            }

            List<InvoiceCreationPreview> iPre
                = DB.FetchStatement<InvoiceCreationPreview>("exec pcs_GetAllInvoicePreview @0, @1, @2, @3, @4, @5, @6",
                new object[] { SupplierCode, dockCode, manifests, orderNo, dfrom, dto, onlyUploaded })
                .ToList();
            if (iPre != null)
            {
                MyModel.Preview = iPre;
            }
            else
            {
                MyModel.Preview = new List<InvoiceCreationPreview>();
            }

            int nullCount = iPre.Where(x => string.IsNullOrEmpty(x.InvoiceNo)).Count();
            if (nullCount > 0 && nullCount != iPre.Count)
            {
                return Content("Please select uploaded Invoice data or Non uploaded Invoice data only");
            }

            if (!string.IsNullOrEmpty(invoices))
            {
                MyModel.InvoicePreview = MyModel.Preview.Where(x => invoices.Equals(x.InvoiceNo)).FirstOrDefault();
            }

            return PartialView("PopupInvoicePreviewInvoiceGridPartial", Model);
        }

        public ActionResult PartialInvoiceCreationDetail(string ManifestNo)
        {
            ViewData["ID"] = ManifestNo;

            List<InvoiceCreationDetail> d = GetAllInvoiceCreationByManifest(ManifestNo);

            MyModel.InvoiceCreationDetails = d;

            return PartialView("PartialInvoiceCreationDetail", Model);
        }
        public ActionResult InvoicePreview(String param)
        {
            return RedirectToAction("Init", "InvoiceCreation");
        }

        public ActionResult PartialHeaderSupplierLookupGridSupplierInfo()
        {
            TempData["GridName"] = osuppID;
            ViewData["GridSupplier"] = Suppliers;
            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);
        }

        public ActionResult PartialInvoiceStatusGridLookup()
        {
            #region Required model in partial page : for OIPStatusOption GridLookup
            ViewData["InvoiceStatusGridLookup"] = GetAllInvoiceStatus();

            TempData["GridName"] = "OIPStatusOption";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["InvoiceStatusGridLookup"]);
        }

        public ActionResult PartialHeaderDockLookupGridInvoiceCreation()
        {
            #region Required model in partial page : for OIHDockOption GridLookup
            TempData["GridName"] = odockID;
            #endregion

            return PartialView("GridLookup/PartialGrid", Docks);
        }

        List<SupplierName> _orderInquirySupplierModel = null;
        private List<SupplierName> _OrderInquirySupplierModel
        {
            get
            {
                if (_orderInquirySupplierModel == null)
                    _orderInquirySupplierModel = new List<SupplierName>();

                return _orderInquirySupplierModel;
            }
            set
            {
                _orderInquirySupplierModel = value;
            }
        }

        List<SupplierICS> _supplierModel = null;
        private List<SupplierICS> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierICS>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        public ActionResult PartialHeaderSupplierLookupGridInvoiceCreation()
        {
            TempData["GridName"] = "OIHSupplierOption";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", screenID, osuppID);
            if (suppliers.Count > 0)
            {
                _SupplierModel = suppliers;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }


            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        private List<SupplierICS> GetAllSupplier()
        {
            User u = Model.GetModel<User>();
            string NOREG = u.NoReg; 

            List<SupplierICS> supplierModel = new List<SupplierICS>();
            try
            {
                supplierModel = DB.Fetch<SupplierICS>("GetAllSupplierICS");
            }
            catch (Exception exc)
            {
                exc.PutLog("", u.Username, "InvoiceCreation.GetAllSupplier"
                    , 0, "MPCS00002ERR", "ERR", "5", "51004", 1);
                throw;
            }

            return supplierModel;
        }

        private List<InvoiceStatus> GetAllInvoiceStatus()
        {
            List<InvoiceStatus> lstInvoiceStatus = new List<InvoiceStatus>();

            lstInvoiceStatus = DB.Fetch<InvoiceStatus>("GetAllInvoiceStatus", new object[] { });

            List<string> InvoiceCreationListedStatus = (new string[] { "-3", "-1", "1" }).ToList();
            lstInvoiceStatus = lstInvoiceStatus
                    .Where(x => InvoiceCreationListedStatus.Contains(x.STATUS_CD))
                    .ToList();

            return lstInvoiceStatus;
        }

        public ActionResult Refresh()
        {
            DB.Execute("StartJobRefreshInvoice");
            return null;
        }

        public ActionResult CancelOnUpload(string supplier)
        {
            string b = string.Empty;
            //Check proses upload is running
            b = DB.SingleOrDefault<string>("GetInvoiceCreationUploadInProgress", new object[] { supplier, FunctionId }); ;

            return Content(b);
        }

        public ActionResult CancelUpload(
                string supplier,
                string docks,
                string manifests,
                string orderNo,
                string dateFrom,
                string dateTo,
                string invoiceNo
            )
        {
            string r = "Ok";
            User u = Model.GetModel<User>();
            string uid = (u != null) ? u.Username : "?";

            if ((u == null) || string.IsNullOrEmpty(u.Username))
                return Content(ErrNoUid);
            else
            {
                if (!string.IsNullOrEmpty(SupplierCode) && string.Compare(SupplierCode, SuppCodeOf(uid)) != 0)
                {
                    return Content(ErrNoUpload);
                }
            }
            /// check if Cancel on progress or upload in progress 
            /// return when other cancelation in progress 
            /// or upload process in progress 
            long lockPid = DB.SingleOrDefault<long>("GetLockByReff", new object[] { FunctionId, supplier });
            if (lockPid != 0)
            {
                return Content("Locked by Upload process " + lockPid.ToString());
            }
            lockPid = DB.SingleOrDefault<long>("GetLockByReff", new object[] { CancelFunctionId, supplier });
            if (lockPid != 0)
            {
                return Content("Locked by Cancel process " + lockPid.ToString());
            }

            long pid = PutLog("CancelUpload ", uid, "InvoiceCreation.CancelUpload");
            string lockReff = supplier + "_" + pid.ToString();

            int getLock = DB.SingleOrDefault<int>("InsertLock", new object[] { CancelFunctionId, lockReff, uid });
            if (getLock != 1)
            {
                return Content("Unable to Acquire Lock, Cancel Upload failed");
            }

            int jobReturns = DB.SingleOrDefault<int>("CancelInvoiceCreationJob", new object[] {
                    supplier, docks, manifests, invoiceNo, orderNo, dateFrom, dateTo, uid, pid
            });

            if (jobReturns != 0)
            {
                int delLock = DB.SingleOrDefault<int>("DeleteLock", new object[] { CancelFunctionId, lockReff });
                return Content("Error Executing job");
            }
            return Content(r);
        }

        public string GetCoilSupplierCode()
        {
            return ";" + CoilSuppliers.Join(";") + ";"; 
        }
        private List<InvoiceCreation> GetAllInvoiceCreation(
            string suppliers,
            string docks,
            string manifestNo,
            string orderNo,
            string dateFrom,
            string dateTo,
            int onlyUploaded)
        {
            List<InvoiceCreation> l = new List<InvoiceCreation>();

            l = DB.FetchStatement<InvoiceCreation>("exec pcs_GetAllInvoiceCreation @0, @1, @2, @3, @4, @5, @6", new object[] {
                    suppliers,docks, manifestNo, orderNo, dateFrom, dateTo, onlyUploaded});

            
            return l;
        }

        private List<InvoiceCreationDetail> GetAllInvoiceCreationByManifest(string ManifestNo)
        {
            List<InvoiceCreationDetail> d = new List<InvoiceCreationDetail>();

            d = DB.Fetch<InvoiceCreationDetail>("GetAllInvoiceCreationByManifest", new object[] { ManifestNo });

            return d;
        }
        private string SetInvoiceTaxType(int val)
        {
            if (val % 2 == 0)
            {
                return "By Month";
            }
            else
            {
                return "By Invoice";
            }
        }

        private List<CsvColumn> CsvCol = CsvColumn.Parse(
            "ClientID|C|10|0;" +
            "Supplier Code|C|6|1|SUPP_CD;" +
            "PO Number|C|10|0|PO_NO;" +
            "PO Item No|C|5|0|PO_ITEM_NO;" +
            "New Kanban Material Number|C|23|0|MAT_NO_REF;" +
            "Material Number|C|23|1|MAT_NO;" +
            "Prod Purpose |C|5|0|PROD_PURPOSE_CD;" +
            "Source Type|C|1|0|SOURCE_TYPE_CD;" +
            "Packing Type|C|1|0|PACKING_TYPE;" +
            "Suffix Colour|C|2|1|PART_COLOR_SFX;" +
            "Material Description|C|40|0|MAT_TEXT;" +
            "Mat Doc No|C|10|0|MAT_DOC_NO;" +
            "Item No|C|4|0|MAT_DOC_ITEM;" +
            "Comp Price|C|4|0|COMP_PRICE_CD;" +
            "Supplier Manifest|C|16|1|INV_REF_NO;" +
            "Document Date|D|10|1|DOC_DT;" +
            "Quantity|N|16|1|QTY;" +
            "Order Number|C|16|0|ORDER_NO;" +
            "Dock Code|C|2|0|DOCK_CD;" +
            "Invoice No|C|12|1|SUPP_INV_NO;" +
            "Supplier Quantity|N|13|1|S_QTY;" +
            "Price|N|18|1|PRICE_AMT;" +
            "Currency|C|3|0|PAY_CURR;" +
            "Amount|N|18|1|AMT;");

        string DECFMT = "F0";
        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        public string CheckUploadProgress(string supplierCode)
        {
            string result = "";
            if (string.IsNullOrEmpty(supplierCode))
                supplierCode = SupplierCode;

            string b = DB.SingleOrDefault<string>("GetInvoiceCreationUploadInProgress",
                new object[] { supplierCode, FunctionId });

            if (string.IsNullOrEmpty(b))
                result = DB.SingleOrDefault<string>("CheckUploadError",
                    new object[] { supplierCode });
            else
                result = "Verification process is in progress, please wait";

            return result;
        }

        public ActionResult PreviewInvoiceDocument(
                string supplierCode,
                string manifests,
                string dateFrom,
                string dateTo,
                string showOnlyUploaded,
                string documentDate)
        {
            string r = "";
            if (string.IsNullOrEmpty(supplierCode))
                supplierCode = SupplierCode;
            int onlyUploaded = "true".Equals(showOnlyUploaded) ? 1 : 0;
            string b = DB.SingleOrDefault<string>("GetInvoiceCreationUploadInProgress",
                new object[] { supplierCode, FunctionId }); ;
            if (string.IsNullOrEmpty(b))
                r = DB.SingleOrDefault<string>("GetInvoiceCreationCanPreview",
                    new object[] { manifests, dateFrom, dateTo, supplierCode, onlyUploaded });
            else
                r = "Verification process is in progress, please wait";

            //validasi baru
            if (r != "OK")
                r = DB.SingleOrDefault<string>("GetDataDocumentDateValidation",
                    new object[] { documentDate });


            return Content(r);
        }

        public static decimal ToNumeric(string x)
        {
            NumberStyles ns = NumberStyles.Number;
            CultureInfo ci = new CultureInfo("en-US", false);
            if (x.LastIndexOf(",") > x.LastIndexOf("."))
                ci = new CultureInfo("id-ID", false);

            decimal dx = 0;
            decimal.TryParse(x, ns, ci, out dx);
            return dx;
        }

        public static long RoundDecimal(string x)
        {
            return (long)Math.Round(ToNumeric(x), 0);
        }

        public ActionResult SaveInvoiceDocument(
                string invoiceNo,
                string manifests,
                string invoiceDate,
                string invoiceTaxNo,
                string invoiceTaxDate,
                string stampFlag,
                string stampAmount,
                string turnOver,
                string invoiceTaxAmt,
                string totalManifest,
                string retroDocNo,
                string invTotalAmt,
                string dockCode,
                string orders,
                string dfrom,
                string dto,
                string showUploaded,
                string retroAmount,
                string retroRemaining, 
                string withholdingTaxAmt, 
                string transactionType, 
                bool isCoil, 
                string supplierCode,
                string TaxCode,
                string TaxRate
            )
        {
            string ret = "";
            int existCreation = 0;
            User u = Model.GetModel<User>();
            string uid = (u != null) ? u.Username : "?";

            if (string.IsNullOrEmpty(supplierCode))
                supplierCode = SupplierCode; 

            if ((u == null) || string.IsNullOrEmpty(u.Username))
                return Content(ErrNoUid);
            else
            {
                if (string.Compare(supplierCode, SuppCodeOf(uid)) != 0)
                {
                    return Content(ErrNoUpload);
                }
            }

            string x_char = GetInvoiceNoValidation();
            string[] x = x_char.Split(' ');
            foreach (string word in x)
            {
                if (invoiceNo.ToString().IndexOf(word) > 0)
                {
                    ret = "The following characters are not allowed on Supplier Invoice No: " + x_char.ToString();
                    return Content(ret);
                }
            }

            if (string.IsNullOrEmpty(invoiceNo))
                ret += "Invoice No is Mandatory. ";
            if (string.IsNullOrEmpty(invoiceDate))
                ret += "Invoice Date is Mandatory. ";
            //edit riani (20221007) samain dengan prod
            if (string.IsNullOrEmpty(TaxCode))
                ret += "Tax Code is Mandatory";


            if (string.IsNullOrEmpty(invoiceTaxNo) && !isCoil)
                ret += "Invoice Tax No is Mandatory";
            if (string.IsNullOrEmpty(invoiceTaxDate) && !isCoil)
                ret += "Invoice Tax Date is Mandatory";
            if (!string.IsNullOrEmpty(ret))
                return Content(ret);

            string existInvoiceCreation = null;
            existInvoiceCreation = DB.SingleOrDefault<string>("CheckExistInvoiceCreation", new object[] {
                    new { invoiceNo, supplierCode, invoiceDate }
            });
            existCreation = -1;

            string[] existing = existInvoiceCreation.Split(new char[] { '|' });
            if (existing.Length > 0 && !string.IsNullOrEmpty(existing[0]))
            {
                int.TryParse(existing[0], out existCreation);
                
            }
            if (existing.Length > 1)
                ret = existing[1];
            if (existCreation > -1)
            {
                return Content(ret); 
            }
            
            //if (existCreation == 1)
            //{
            //    ret = "Invoice with InvoiceNo: " + invoiceNo
            //        //  + " and InvoiceDate: " + invoiceDate 
            //        + " and Supplier: " + supplierCode
            //        + " is already exists";
            //    return Content(ret);
            //}
            //// ADD BY ALIRA.AGI 2015-03-26
            //if (existCreation == 2)
            //{
            //    ret = "Error when save invoice : Manifest has been invoiced";
            //    return Content(ret);
            //}
            string[] PreviewSaveParam =
                @"invoiceNo,manifests,invoiceDate,invoiceTaxNo,invoiceTaxDate,stampFlag,
                stampAmount,
                turnOver,invoiceTaxAmt,totalManifest,retroDocNo,invTotalAmt,dockCode,orders,
                dfrom,dto,showUploaded,retroAmount,retroRemaining,
                withholdingTaxAmt,transactionType,isCoil,TaxCode"
                .Split(new char[] {',', ' ', '\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);

            int stamped = "true".Equals(stampFlag) ? 1 : 0;
            int uploaded = "true".Equals(showUploaded) ? 1 : 0;
            decimal stampAmt = 0;
            decimal aTurnOver, aInvoiceTax, aRetroAmount, aInvTotal;

            //decimal retroTaxRate = DB.SingleOrDefault<decimal>("GetRetroTaxRate");

            stampAmt = ToNumeric(stampAmount);
            aTurnOver = ToNumeric(turnOver);
            aInvoiceTax = ToNumeric(invoiceTaxAmt);
            aRetroAmount = ToNumeric(retroAmount);
            aInvTotal = aTurnOver + aRetroAmount
                      + ((aTurnOver + aRetroAmount) * (ToNumeric(TaxRate) / 100))  // invoice tax -- recalc 
                      + ((stamped > 0) ? stampAmt : 0);


            string initLog = string.Format("SaveInvoiceDocument " +
                PreviewSaveParam.Select(a => a + "=" + (Request[a] ?? "")).Join(", ")
                );
            long pid = ExceptionalHelper.PutLog(initLog, uid, "InvoiceCreation.SaveInvoiceDocument.Init"
                , 0, "MPCS00002INF", "INF", "5", "51004", 0); 

            try
            {
                int inserted = 0;

                inserted = DB.SingleOrDefault<int>("UpdateSaveInvoicePreview", new object[] {
                    pid, 
                    supplierCode, 
                    invoiceNo,
                    invoiceDate,
                    invoiceTaxNo,
                    invoiceTaxDate,
                    stamped,
                    turnOver,
                    invoiceTaxAmt,
                    totalManifest,
                    retroDocNo,
                    aInvTotal,
                    manifests,
                    dockCode,
                    orders,
                    dfrom,
                    dto,
                    uploaded,
                    uid, 
                    withholdingTaxAmt, 
                    transactionType,
                    TaxCode,
                    TaxRate
                });
                string uip = "";
                if (inserted > 0)
                {
                    //modif by alira.agi 015-4-20 
                    PutLog("UpdateSaveInvoicePreview Inserted: " + inserted, uid, "SaveInvoiceDocument", pid);
                    // save record to mark Bulk Copy operation 
                    DB.Execute("InsertTempInvoiceUploadFile", new object[] {
                        pid,
                        uid,
                        supplierCode,
                        pid.ToString() + ".csv",
                        0,
                        null});

                    DB.Execute("ICSInvoiceCreationJobStart", new object[] {
                        pid });

                    uip = ", Verification in progress";
                }
                //modif by alira.agi 2015-04-20
                else
                {
                    PutLog("without liv InsertInvoiceFromTemp " + pid.ToString()
                        + " suppCode:" + supplierCode
                        + " invoiceNo : " + invoiceNo, uid
                        , "SaveInvoiceDocument", pid);
                    DB.Execute("InsertInvoiceFromTemp", new object[] {
                        pid, supplierCode, invoiceNo, uid
                    });
                }

                if (!string.IsNullOrEmpty(retroDocNo))
                {
                    int retroInserted = DB.Execute("InsertRetroRemaining", new object[] {
                        retroDocNo,
                        invoiceNo,
                        supplierCode,
                        invoiceDate,
                        retroAmount,
                        uid});
                    
                    DB.Execute("InsertInvGlAccountTemp", new object[] {
                        invoiceNo,
                        retroAmount,
                        "RETRO_GL_ACCOUNT",
                        supplierCode //modif by alira.agi 2015-04-14
                    });
                }


                if (stamped > 0 && stampAmt > 0)
                {
                    DB.Execute("InsertInvGlAccountTemp", new object[] {
                        invoiceNo,
                        stampAmt,
                        "STAMP_GL_ACCOUNT",
                        supplierCode
                    });
                }
                ret = "Data Saved successfully" + uip;
            }
            catch (Exception ex)
            {
                ret = ex.Message;
                ex.PutLog("", uid, "InvoiceCreation.SaveInvoiceDocument", pid, "MPCS00002ERR", "ERR", ModuleId, FunctionId, 2);                 
            }

            return Content(ret);
        }

        public void DownloadListExcel(string supplierCode, string dockCode, string manifestNo, string orderNo, string periodFrom, string periodTo, string status, bool uploadedOnly, string invoices)
        {

            string fileName = "GR_LIV_"
                                + SanitizeFilename(Model.GetModel<User>().Username)
                                + "_"
                                + DateTime.Now.ToString("yyyyMMdd_HHmmss")
                                + ".xls";
            IEnumerable<ReportGetAllInvoiceCreationExcel> listGeneral = DB.Fetch<ReportGetAllInvoiceCreationExcel>("ReportGetAllInvoiceCreationExcel", new object[] { supplierCode, dockCode, manifestNo, orderNo, periodFrom, periodTo, uploadedOnly, invoices });

            IExcelWriter exporter = ExcelWriter.GetInstance();
            exporter.Append(listGeneral, fileName);

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();

        }



        public StringBuilder GetCSV(
            string supplierCode,
            string dockCode,
            string manifestNo,
            string orderNo,
            string periodFrom,
            string periodTo,
            string status,
            bool uploadedOnly,
            string invoices,
            bool addEqualSign = true)
        {
            IEnumerable<ReportGetAllInvoiceCreation> qry =
                 DB.Fetch<ReportGetAllInvoiceCreation>("ReportGetAllInvoiceCreation", new object[] {
                supplierCode  ,
                dockCode  ,
                manifestNo ,
                orderNo ,
                periodFrom,
                periodTo,
                uploadedOnly,
                invoices
            });

            StringBuilder gr = new StringBuilder("");

            for (int i = 0; i < CsvCol.Count; i++)
            {
                gr.Append(Quote(CsvCol[i].Text)); gr.Append(CSV_SEP);
            }

            gr.Append(Environment.NewLine);

            foreach (ReportGetAllInvoiceCreation d in qry)
            {
                if (addEqualSign)
                    csvAddLine(gr, new string[] {d.ClientID.ToString(),
                        Equs(d.SupplierCode),
                        Equs(d.PoNumber),
                        Equs(d.PoItemNo),
                        Equs(d.NewKanbanMaterialNumber),
                        Equs(d.IcsMaterialNumber),
                        Equs(d.ProdPurpose),
                        Equs(d.SourceType),
                        Equs(d.PackingType),
                        Equs(d.SuffixColour),
                        Quote(d.MaterialDescription),
                        Equs(d.MatDoc),
                        d.ItemNo.ToString(),
                        Equs(d.CompPrice),
                        Equs(d.SupplierManifest),
                        (d.DocumentDate != null) ? d.DocumentDate.Value.ToString(IN_FMT) : "",
                        d.IcsQty.ToString(DECFMT),
                        Equs(d.OrderNumber),
                        Equs(d.DockCode),
                        Equs(d.SupplierInvoiceNo),
                        d.SuppQty.ToString(DECFMT),
                        d.Price.ToString(), 
                        Quot(d.Curr),
                        d.Amount.ToString()});
                else
                    csvAddLine(gr, new string[] {d.ClientID.ToString(),
                        d.SupplierCode,
                        d.PoNumber,
                        d.PoItemNo,
                        d.NewKanbanMaterialNumber,
                        d.IcsMaterialNumber,
                        d.ProdPurpose,
                        d.SourceType,
                        d.PackingType,
                        d.SuffixColour,
                        Quote(d.MaterialDescription),
                        d.MatDoc,
                        d.ItemNo.ToString(),
                        d.CompPrice,
                        d.SupplierManifest,
                        (d.DocumentDate != null) ? d.DocumentDate.Value.ToString(IN_FMT) : "",
                        d.IcsQty.ToString(DECFMT),
                        d.OrderNumber,
                        d.DockCode,
                        d.SupplierInvoiceNo,
                        d.SuppQty.ToString(DECFMT),
                        d.Price.ToString(), 
                        d.Curr,
                        d.Amount.ToString()});
            }

            return gr;
        }

        public void DownloadInvoice(
            string supplierCode,
            string dockCode,
            string manifestNo,
            string orderNo,
            string periodFrom,
            string periodTo,
            string status,
            bool uploadedOnly,
            string invoices)
        {
            string fileName = "LIV_"
                                + SanitizeFilename(Model.GetModel<User>().Username)
                                + "_"
                                + DateTime.Now.ToString("yyyyMMdd_HHmmss")
                                + ".csv";

            SendResponse(fileName, GetCSV(supplierCode, dockCode, manifestNo,
                    orderNo, periodFrom, periodTo, status, uploadedOnly, invoices));
        }

        public StringBuilder GetErr(string suppCode, bool addEqualSign = true)
        {
            User u = Model.GetModel<User>();
            string uid = (u != null) ? u.Username : "";

            IEnumerable<ReportGetAllInvoiceCreation> qry =
                DB.Fetch<ReportGetAllInvoiceCreation>("GetInvoiceCreationError", new object[] {
                suppCode, uid
            });

            StringBuilder gr = new StringBuilder("");

            for (int i = 0; i < CsvCol.Count; i++)
            {
                gr.Append(Quote(CsvCol[i].Text)); gr.Append(CSV_SEP);
            }
            gr.Append("Remarks");

            gr.Append(Environment.NewLine);

            foreach (ReportGetAllInvoiceCreation d in qry)
            {
                if (addEqualSign)
                    csvAddLine(gr, new string[] {d.ClientID.ToString(),
                        Equs(d.SupplierCode),
                        Equs(d.PoNumber),
                        Equs(d.PoItemNo),
                        Equs(d.NewKanbanMaterialNumber),
                        Equs(d.IcsMaterialNumber),
                        Equs(d.ProdPurpose),
                        Equs(d.SourceType),
                        Equs(d.PackingType),
                        Equs(d.SuffixColour),
                        Quote(d.MaterialDescription),
                        Equs(d.MatDoc),
                        d.ItemNo.ToString(),
                        Equs(d.CompPrice),
                        Equs(d.SupplierManifest),
                        (d.DocumentDate != null) ? d.DocumentDate.Value.ToString(IN_FMT) : "",
                        d.IcsQty.ToString(DECFMT),
                        Equs(d.OrderNumber),
                        Equs(d.DockCode),
                        Equs(d.SupplierInvoiceNo),
                        d.SuppQty.ToString(DECFMT),
                        d.Price.ToString(DECFMT),
                        Quot(d.Curr),
                        d.Amount.ToString(DECFMT),
                        Quot(d.Remarks)
                    });
                else
                    csvAddLine(gr, new string[] {d.ClientID.ToString(),
                        d.SupplierCode,
                        d.PoNumber,
                        d.PoItemNo,
                        d.NewKanbanMaterialNumber,
                        d.IcsMaterialNumber,
                        d.ProdPurpose,
                        d.SourceType,
                        d.PackingType,
                        d.SuffixColour,
                        Quote(d.MaterialDescription),
                        d.MatDoc,
                        d.ItemNo.ToString(),
                        d.CompPrice,
                        d.SupplierManifest,
                        (d.DocumentDate != null) ? d.DocumentDate.Value.ToString(IN_FMT) : "",
                        d.IcsQty.ToString(DECFMT),
                        d.OrderNumber,
                        d.DockCode,
                        d.SupplierInvoiceNo,
                        d.SuppQty.ToString(DECFMT),
                        d.Price.ToString(DECFMT),
                        d.Curr,
                        d.Amount.ToString(DECFMT),
                        Quot(d.Remarks)
                    });
            }

            return gr;
        }


        public void DownloadError(
            string supplierCode)
        {
            string fileName = "LIV_ERR_"
                                + SanitizeFilename(Model.GetModel<User>().Username)
                                + "_"
                                + DateTime.Now.ToString("yyyyMMdd_HHmmss")
                                + ".csv";
            SendResponse(fileName, GetErr(string.IsNullOrEmpty(supplierCode) ? SupplierCode : supplierCode));
        }

        public void DownloadList(
            string supplierCode,
            string dockCode,
            string manifestNo,
            string orderNo,
            string periodFrom,
            string periodTo,
            string status,
            bool uploadedOnly)
        {
            string fileName = "GR_LIV_"
                                + SanitizeFilename(Model.GetModel<User>().Username)
                                + "_"
                                + DateTime.Now.ToString("yyyyMMdd_HHmmss")
                                + ".csv";

            SendResponse(fileName, GetCSV(supplierCode, dockCode, manifestNo,
                    orderNo, periodFrom, periodTo, status, uploadedOnly, null));
        }

        private void SendResponse(string fileName, StringBuilder contents)
        {
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            byte[] b = UTF8Encoding.UTF8.GetBytes(contents.ToString());
            var encodings = Request.Headers["Accept-Encoding"];
            if (encodings.Contains("gzip"))
            {
                Response.AddHeader("Content-encoding", "gzip");
                b = Pack(b, 1);
            }
            else if (encodings.Contains("deflate"))
            {
                Response.AddHeader("Content-encoding", "deflate");
                b = Pack(b, 2);
            }

            Response.ContentType = "application/octet-stream";

            Response.AddHeader("Content-Length", Convert.ToString(b.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(b);
            Response.End();
        }

        private byte[] Pack(byte[] b, int method)
        {
            MemoryStream m = new MemoryStream();
            byte[] x = null;
            switch (method)
            {
                case 1:
                    using (GZipStream g = new GZipStream(m, CompressionMode.Compress))
                    {
                        g.Write(b, 0, b.Length);
                    }
                    x = m.ToArray();
                    break;
                case 2:
                    using (DeflateStream d = new DeflateStream(m, CompressionMode.Compress))
                    {
                        d.Write(b, 0, b.Length);
                    }
                    x = m.ToArray();
                    break;
                default:
                    x = new byte[b.Length];
                    b.CopyTo(x, 0);
                    break;
            }
            return x;
        }

        public string SanitizeFilename(string insane)
        {
            string DirtyFileNamePattern = "[\\+/\\\\\\#%&*{}/:<>?|\"-\\.]";
            string replacement = "_";

            Regex regEx = new Regex(DirtyFileNamePattern);
            return Regex.Replace(regEx.Replace(insane, replacement), @"\s+", "_");
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        /// <summary>
        /// put Equal Sign in front and quote char to prevent 'numerization' by Excel
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string Equs(string s)
        {
            return s != null ? "=\"" + s + "\"" : s;
        }

        private string Quot(string s)
        {
            if (!string.IsNullOrEmpty(s) && (s.IndexOfAny(new string[] { ",", ";", " ", "\t" }) >= 0))
            {
                s = "\"" + s.Trim() + "\"";
            }
            return s;
        }

        public ActionResult LivUploadCallback(IEnumerable<UploadedFile> ucCallbacks)
        {
            string supp = "";
            if (string.IsNullOrEmpty(SupplierCode))
                supp = EditorExtension.GetValue<string>("txtOrderNo");
            else
                supp = SupplierCode;

            UploadControlExtension.GetUploadedFiles("LivUpload", ValidationSettings, FileUploadComplete);

            return null;
        }
        public ActionResult LivUploadCallback_Excel(IEnumerable<UploadedFile> ucCallbacks)
        {
            //harcode supp code semntara
            //SupplierCode = "0001";
            string supp = "";
            if (string.IsNullOrEmpty(SupplierCode))
            { 
                supp = EditorExtension.GetValue<string>("txtOrderNo");
                if (string.IsNullOrEmpty(supp))
                {
                    supp = SupplierCode; 
                }
            }
            else
                supp = SupplierCode;

            UploadControlExtension.GetUploadedFiles("LivUpload_Excel", ValidationSettings_Excel, FileUploadComplete_Excel);

            return null;
        }
        public ActionResult DownloadCallback()
        {
            return null;
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".csv", ".zip" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };
        protected readonly ValidationSettings ValidationSettings_Excel = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls", ".zip" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };
        //private string _FilePath = "";
        private string UploadDir
        {
            get
            {
                string domain = ConfigurationManager.AppSettings["LdapDomain"];
                string app = ConfigurationManager.AppSettings["DeploymentContext"];

                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), domain, app);
            }
        }

        private string Unq(string q)
        {
            if (!string.IsNullOrEmpty(q) && q.StartsWith("\"") && q.EndsWith("\""))
                q = q.Substring(1, q.Length - 2);
            return q;
        }

        private string DescribeStatus(int status, string statusName)
        {
            string r = "";
            switch (status)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case -5:
                case -4:
                case -2:
                    r = "Has been " + (statusName ?? "?");
                    break;
                default:
                    r = "Data Not Found";
                    break;
            }
            return r;
        }

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (!e.UploadedFile.IsValid) return;
            User u = Model.GetModel<User>();
            string uid = (u != null) ? u.Username : "?";

            if ((u == null) || string.IsNullOrEmpty(u.Username))
            {
                e.ErrorText = ErrNoUid;
                return;
            }
            else
            {
                if (!string.IsNullOrEmpty(SupplierCode) && string.Compare(SupplierCode, SuppCodeOf(uid)) != 0)
                {
                    e.ErrorText = ErrNoUpload;
                    return;
                }
            }

            string supp = "";
            if (string.IsNullOrEmpty(SupplierCode))
                supp = EditorExtension.GetValue<string>("txtOrderNo");
            else
                supp = SupplierCode;
            if (string.IsNullOrEmpty(supp))
            {
                e.ErrorText = "Supplier is not selected";
                return;
            }
            else if (supp.IndexOf(";") > 0)
            {
                e.ErrorText = "Select only one supplier";
                return;
            }

            int seq = 0;

            long lockPid = DB.SingleOrDefault<long>("GetLockByReff", new object[] { FunctionId, supp });
            if (lockPid != 0)
            {
                string m = string.Format("Process Upload Lock by previous process id = {0}", lockPid);
                PutLog(m, "FileUploadComplete.GetLockByReff");

                e.ErrorText = m;
                e.IsValid = false;
                ViewData["lockPid"] = lockPid;
                return;
            }

            lockPid = DB.SingleOrDefault<long>("GetLockByReff", new object[] { CancelFunctionId, supp });
            if (lockPid != 0)
            {
                string m = "Locked by Cancel process " + lockPid.ToString();
                PutLog(m, "FileUploadComplete.GetLockByReff");
              
                e.ErrorText = m;
                e.IsValid = false;
                ViewData["lockPid"] = lockPid;
                return;
            }
            long pid = PutLog("Invoice Creation Upload File : " +
                e.UploadedFile.FileName,
                uid,
                "InvoiceCreation.FileUploadComplete");
            try
            {
                string tmpFilename = "";
                MemoryStream mx = null;

                bool isZip = e.UploadedFile.FileName.ToLower().EndsWith(".zip");
                if (SaveUploaded || isZip)
                {
                    string scode = SupplierCode;
                    if (string.IsNullOrEmpty(scode))
                        scode = "~";
                    string fPath = Path.Combine(UploadDir, FunctionId, scode, pid.ToString());
                    if (!Directory.Exists(fPath))
                    {
                        Directory.CreateDirectory(fPath);
                    }

                    string ufile = Path.Combine(fPath, e.UploadedFile.FileName);
                    FileStream f = new FileStream(ufile, FileMode.CreateNew, FileAccess.ReadWrite);

                    f.Write(e.UploadedFile.FileBytes, 0, e.UploadedFile.FileBytes.Length);
                    f.Close();

                    if (isZip)
                    {
                        ICompression iz = new IonicZip();
                        string msg = null;
                        iz.Decompress(ufile, fPath, ref msg);
                        List<string> l = Directory.GetFiles(fPath).ToList<string>();

                        for (int i = 0; i < l.Count; i++)
                        {
                            if (l[i].ToLower().EndsWith(".csv") || l[i].ToLower().EndsWith(".txt"))
                            {
                                tmpFilename = l[i];
                                break;
                            }
                        }

                        if (tmpFilename != null)
                        {
                            FileStream fx = new FileStream(Path.Combine(fPath, tmpFilename), FileMode.Open, FileAccess.Read);
                            mx = new MemoryStream();
                            fx.WriteTo(mx);
                            fx.Close();
                        }
                    }
                }

                //string body = null;

                // delete from tb_t where process_id != pid && supp_cd = @@supplierCode 
                PutLog("DeleteInvoiceUploadTemp", uid, "FileUploadComplete", pid);
                DB.Execute("DeleteInvoiceUploadTemp", new object[] { pid, supp });

                /* direct insert to TB_T_INV_UPLOAD USING BULK COPY */
                if (mx == null)
                {
                    BulkCopyStream(e.UploadedFile.FileContent, pid);
                }
                else
                {
                    mx.Seek(0, SeekOrigin.Begin);
                    BulkCopyStream(mx, pid);
                }


                // save record to mark Bulk Copy operation 
                //insert into TB_T_INV_UPLOAD_FILE
                DB.Execute("InsertTempInvoiceUploadFile", new object[] {
                    pid,
                    uid,
                    supp,
                    e.UploadedFile.FileName,
                    0,
                    null});

                int InvoiceSPOT = 0;
                InvoiceSPOT = DB.SingleOrDefault<int>("CheckInvoicePO", new object[] {
                    pid, supp
                });

                if (InvoiceSPOT > 0)
                {
                    PutLog("This Invoice is Service Part Invoice", "FileUploadComplete.InvoiceSPOT");
                    e.ErrorText = "This Invoice is Service Part Invoice, Please Upload in SPOT";
                    return;// Content(ret);
                }

                int manifestPartial = 0;
                manifestPartial = DB.SingleOrDefault<int>("CheckManifestPartial", new object[] {
                    pid, supp
                });

                if (manifestPartial > 0)
                {
                    PutLog("Upload Manifest Partialy is Not Allowed", "FileUploadComplete.ManifestPartial");
                    e.ErrorText = "Upload Manifest Partialy is Not Allowed";
                    return;// Content(ret);
                }

                // ADD BY ALIRA.AGI 2015-04-07
                int existCreation = 0;
                existCreation = DB.SingleOrDefault<int>("CheckUploadExistInvoice", new object[] {
                    pid, supp
                    });
                if (existCreation == 1)
                {
                    PutLog("Invoice is already exists", "FileUploadComplete.Invoiceexists");
                    e.ErrorText = "Invoice is already exists";
                    return;// Content(ret);
                }
                if (existCreation == 2)
                {
                    PutLog("Error when Processing Upload : Manifest has been invoiced", "FileUploadComplete.Manifestexists");
                    e.ErrorText = "Error when Processing Upload : Manifest has been invoiced";
                    return;// Content(ret);
                }
                //FID.Ridwan 2020-12-22 --> validation only 1 po no can be procceed
                if (existCreation == 3)
                {
                    PutLog("Error when Processing Upload : Upload file have more than one PO NO", "FileUploadComplete.POChecking");
                    e.ErrorText = "Error when Processing Upload : Upload file have more than one PO NO";
                    return;// Content(ret);
                }
                //insert into TB_R_ICS_QUEUE and TB_T_LOCK
                DB.Execute("ICSInvoiceCreationJobStart", new object[] { pid });

                e.ErrorText = "";
            }
            catch (Exception ex)
            {
                ex.PutLog("Processing Upload error", uid, "InvoiceCreation.FileUploadComplete", pid, "MPCS00002ERR", "ERR", ModuleId, FunctionId);
                        
                e.ErrorText = ex.Message;
            }

            string SSUPP = string.IsNullOrEmpty(SelectedSupplier) ? SupplierCode : SelectedSupplier;
            PutLog(string.Format("UpdateInvoiceUploadStatus supp='{0}'", SSUPP), uid, "FileUploadComplete", pid);

            //Remark by fid.Djoko for testing purpose only
            //DB.Execute("UpdateInvoiceUploadStatus", new object[] { pid, SSUPP, uid });

            e.CallbackData = pid.ToString();
        }
        protected void FileUploadComplete_Excel(object sender, FileUploadCompleteEventArgs e)
        {
            if (!e.UploadedFile.IsValid) return;
            User u = Model.GetModel<User>();
            string uid = (u != null) ? u.Username : "?";

            if ((u == null) || string.IsNullOrEmpty(u.Username))
            {
                e.ErrorText = ErrNoUid;
                return;
            }
            else
            {
                if (!string.IsNullOrEmpty(SupplierCode) && string.Compare(SupplierCode, SuppCodeOf(uid)) != 0)
                {
                    e.ErrorText = ErrNoUpload;
                    return;
                }
            }

            string supp = "";
            if (string.IsNullOrEmpty(SupplierCode))
            {
                supp = EditorExtension.GetValue<string>("txtOrderNo");
                if (string.IsNullOrEmpty(supp))
                {
                    supp = SupplierCode;
                }
            }
            else
                supp = SupplierCode;

            if (string.IsNullOrEmpty(supp))
            {
                e.ErrorText = "Supplier is not selected";
                return;
            }
            else if (supp.IndexOf(";") > 0)
            {
                e.ErrorText = "Select only one supplier";
                return;
            }

            int seq = 0;
    
            long lockPid = DB.SingleOrDefault<long>("GetLockByReff", new object[] { FunctionId, supp });
            if (lockPid != 0)
            {
                string m = string.Format("Process Upload Lock by previous process id = {0}", lockPid);
                PutLog(m, "FileUploadComplete.GetLockByReff");

                e.ErrorText = m;
                e.IsValid = false;
                ViewData["lockPid"] = lockPid;
                return;
            }

            lockPid = DB.SingleOrDefault<long>("GetLockByReff", new object[] { CancelFunctionId, supp });
            if (lockPid != 0)
            {
                string m = "Locked by Cancel process " + lockPid.ToString();
                PutLog(m, "FileUploadComplete.GetLockByReff");
               
                e.ErrorText = m;
                e.IsValid = false;
                ViewData["lockPid"] = lockPid;
                return;
            }

            long pid = PutLog("Invoice Creation Upload File : " +
                e.UploadedFile.FileName,
                uid,
                "InvoiceCreation.FileUploadComplete");
            try
            {
                string tmpFilename = "";
                MemoryStream mx = null;

                bool isZip = e.UploadedFile.FileName.ToLower().EndsWith(".zip");
                string scode = SupplierCode;
                if (string.IsNullOrEmpty(scode))
                    scode = "~";
                string fPath = Path.Combine(UploadDir, FunctionId, scode, pid.ToString());
                string ufile = Path.Combine(fPath, e.UploadedFile.FileName);
                if (SaveUploaded || isZip)
                {

                    if (!Directory.Exists(fPath))
                    {
                        Directory.CreateDirectory(fPath);
                    }


                    FileStream f = new FileStream(ufile, FileMode.CreateNew, FileAccess.ReadWrite);

                    f.Write(e.UploadedFile.FileBytes, 0, e.UploadedFile.FileBytes.Length);
                    f.Close();

                    if (isZip)
                    {
                        ICompression iz = new IonicZip();
                        string msg = null;
                        iz.Decompress(ufile, fPath, ref msg);
                        List<string> l = Directory.GetFiles(fPath).ToList<string>();

                        for (int i = 0; i < l.Count; i++)
                        {
                            if (l[i].ToLower().EndsWith(".xls"))
                            {
                                tmpFilename = l[i];
                                break;
                            }
                        }

                        if (tmpFilename != null)
                        {
                            FileStream fx = new FileStream(Path.Combine(fPath, tmpFilename), FileMode.Open, FileAccess.Read);
                            mx = new MemoryStream();
                            fx.WriteTo(mx);
                            fx.Close();
                        }
                    }
                }

                //string body = null;

                // delete from tb_t where process_id != pid && supp_cd = @@supplierCode 
                PutLog("DeleteInvoiceUploadTemp", uid, "FileUploadComplete", pid);
                DB.Execute("DeleteInvoiceUploadTemp", new object[] { pid, supp });

                /* direct insert to TB_T_INV_UPLOAD USING BULK COPY */
                if (mx == null)
                {

                    BulkCopyStream_Excel(e.UploadedFile.FileContent, pid, ufile);
                }
                else
                {
                    mx.Seek(0, SeekOrigin.Begin);
                    BulkCopyStream_Excel(mx, pid, ufile);
                }



                // save record to mark Bulk Copy operation 
                //insert into TB_T_INV_UPLOAD_FILE
                DB.Execute("InsertTempInvoiceUploadFile", new object[] {
                    pid,
                    uid,
                    supp,
                    e.UploadedFile.FileName,
                    0,
                    null});

                int InvoiceSPOT = 0;
                InvoiceSPOT = DB.SingleOrDefault<int>("CheckInvoicePO", new object[] {
                    pid, supp
                    });

                if (InvoiceSPOT > 0)
                {
                    PutLog("This Invoice is Service Part Invoice", "FileUploadComplete.InvoiceSPOT");
                    e.ErrorText = "This Invoice is Service Part Invoice, Please Upload in SPOT";
                    return;// Content(ret);
                }

                int manifestPartial = 0;
                manifestPartial = DB.SingleOrDefault<int>("CheckManifestPartial", new object[] {
                pid, supp
                });

                if (manifestPartial > 0)
                {
                    PutLog("Upload Manifest Partialy is Not Allowed", "FileUploadComplete.ManifestPartial");
                    e.ErrorText = "Upload Manifest Partialy is Not Allowed";
                    return;// Content(ret);
                }

                // ADD BY ALIRA.AGI 2015-04-07
                int existCreation = 0;
                existCreation = DB.SingleOrDefault<int>("CheckUploadExistInvoice", new object[] {
                    pid, supp
                    });
                if (existCreation == 1)
                {
                    PutLog("Invoice is already exists", "FileUploadComplete.Invoiceexists");
                    e.ErrorText = "Invoice is already exists";
                    return;// Content(ret);
                }
                if (existCreation == 2)
                {
                    PutLog("Error when Processing Upload : Manifest has been invoiced", "FileUploadComplete.Manifestexists");
                    e.ErrorText = "Error when Processing Upload : Manifest has been invoiced";
                    return;// Content(ret);
                }
                //FID.Ridwan 2020-12-22 --> validation only 1 po no can be procceed
                if (existCreation == 3)
                {
                    PutLog("Error when Processing Upload : Upload file have more than one PO NO", "FileUploadComplete.POChecking");
                    e.ErrorText = "Error when Processing Upload : Upload file have more than one PO NO";
                    return;// Content(ret);
                }
                //insert into TB_R_ICS_QUEUE and TB_T_LOCK
                DB.Execute("ICSInvoiceCreationJobStart", new object[] { pid });

                e.ErrorText = "";
            }
            catch (Exception ex)
            {
                ex.PutLog("Error when Processing Upload ", uid
                        , "InvoiceCreation.FileUploadComplete"
                        , pid
                        , "MPCS00002ERR", "ERR", ModuleId, FunctionId);
                e.ErrorText = ex.Message;
            }

            string SSUPP = string.IsNullOrEmpty(SelectedSupplier) ? SupplierCode : SelectedSupplier;
            PutLog(string.Format("UpdateInvoiceUploadStatus supp='{0}'", SSUPP), uid, "FileUploadComplete", pid);

            e.CallbackData = pid.ToString();
        }

        private string _csv_sep = ";";
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {

                    _csv_sep = ConfigurationManager.AppSettings["CSV_SEPARATOR"];
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        private bool SaveUploaded
        {
            get
            {
                string su = ConfigurationManager.AppSettings["SAVE_UPLOADED"];
                int saveUploaded = 1;
                if (!string.IsNullOrEmpty(su))
                {
                    int.TryParse(su, out saveUploaded);
                }
                return (saveUploaded != 0);
            }
        }

        private Regex rx = null;
        protected Regex RX
        {
            get
            {
                if (rx == null)
                {
                    rx = new Regex("([\"]([^\"]+)[\"]|([^" + CSV_SEP
                            + "]+)|)[\\" + CSV_SEP + "]?");
                }
                return rx;
            }
        }

        List<string> explode(string s)
        {
            List<string> b = new List<string>();

            MatchCollection c = RX.Matches(s);
            if (c.Count > 0)
            {
                for (int i = 0; i < c.Count; i++)
                {
                    if (c[i].Groups.Count > 1 && c[i].Groups[2].Value.Length > 0)
                        b.Add(c[i].Groups[2].Value);
                    else if (c[i].Groups.Count > 0 && c[i].Groups[1].Value.Length > 0)
                        b.Add(c[i].Groups[1].Value);
                    else
                        b.Add("");
                }
            }
            return b;
        }


        private int ParseErrorCount = 0;

        protected string GetInvoiceNoValidation()
        {
            string strChar = string.Empty;
            strChar = DB.SingleOrDefault<string>("GetInvoiceNoCharacter", new string[] { "UNALLOWABLE CHAR" });
            return strChar;
        }

        public DataTable ParseTemp(Stream st, long pid)
        {
            ParseErrorCount = 0;
            DataTable t = new DataTable("csv");
            st.Seek(0, SeekOrigin.Begin);
            StreamReader r = new StreamReader(st);
            t.Columns.Add("PROCESS_ID", typeof(long));
            t.Columns.Add("ROW_NO", typeof(int));
            string SSUPP = string.IsNullOrEmpty(SelectedSupplier) ? SupplierCode : SelectedSupplier;
            int suppErr = 0;
            int documentdtErr = 0;
            for (int i = 0; i < CsvCol.Count; i++)
            {
                switch (CsvCol[i].DataType)
                {
                    case "C":
                        t.Columns.Add(CsvCol[i].Column, typeof(string));
                        break;
                    case "D":
                        t.Columns.Add(CsvCol[i].Column, typeof(string));
                        break;
                    case "N":
                        t.Columns.Add(CsvCol[i].Column, typeof(decimal));
                        break;
                    default:
                        break;
                }
            }

            t.Columns.Add("REMARKS", typeof(string));
            t.Columns.Add("CREATED_BY", typeof(string));
            t.Columns.Add("CREATED_DT", typeof(DateTime));
            string uid = Model.GetModel<User>().Username;
            // Regex CleanInoRegex = new Regex("[~@#\\$%&\\*\\;=<>`!\\^\\-]");
            string l = null;
            int linenum = 0;
            int errCount = 0;
            string ino = "";
            string suppCd = "";
            string grDateString = "";
            int[] ftrim = new int[] { 2, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 17, 18, 22 };
            while ((l = r.ReadLine()) != null)
            {
                if (linenum > 0)
                {

                    List<string> lx = explode(l);
                    if ((lx != null) && lx.Count >= 25)
                    {
                        for (int itr = 0; itr < ftrim.Length; itr++)
                        {
                            int itrim = ftrim[itr];
                            lx[itrim] = !string.IsNullOrEmpty(lx[itrim]) ? lx[itrim].Trim() : lx[itrim];
                        }
                        string err = Validate(lx);
                        if (!string.IsNullOrEmpty(err))
                        {
                            errCount++;
                            PutLog("Row no : " + linenum.ToString()
                                  + " " + err, "", "ParseTemp", pid);
                            if (err.Length > 2000)
                                err = err.Substring(0, 2000);
                        }
                        ino = lx[19];
                        suppCd = (!string.IsNullOrEmpty(lx[1])) ? lx[1].PadLeft(4, '0'): "";
                        if (string.Compare(SSUPP, suppCd) != 0)
                        {
                            suppErr++;
                            errCount++;
                            string errSupp = "Row no: " + linenum.ToString() +
                             " Supplier is not " + SSUPP;
                            PutLog(errSupp, "", "ParseTemp", pid);
                            err = err + errSupp;
                        }

                        // validate tambahan ayu 20191008
                        /*string documenDate = YMD(lx[15]);
                        string grDateNextStr = "";
                        if (documenDate != null && !documenDate.Equals(""))
                        {
                            string errSupp = "Please Upload Data with Range Date 1-15 Or 16-31";
                            grDateNextStr = validateDocumentDate(documenDate);

                            if (grDateString != null && !grDateString.Equals(""))
                            {
                                if (!grDateString.Equals(grDateNextStr))
                                {
                                    documentdtErr++;

                                }
                            }
                            else
                            {
                                grDateString = grDateNextStr;
                            }


                        }*/

                        t.Rows.Add(new object[] { pid, linenum,
                            lx[ 0],
                            suppCd,
                            lx[ 2],
                            (! string.IsNullOrEmpty(lx[ 3])) ? lx[3].PadLeft(5, '0'): "",
                            lx[ 4],
                            lx[ 5],
                            lx[ 6],
                            lx[ 7],
                            lx[ 8],
                            lx[ 9],
                            lx[10],
                            lx[11],
                            lx[12],
                            lx[13],
                            lx[14],
                            YMD(lx[ 15]),
                            Dec(lx[ 16]),
                            lx[17],
                            lx[18],
                            ino,
                            Dec(lx[20]),
                            Dec(lx[21]),
                            lx[22],
                            Dec(lx[23]),
                            err,
                            uid,
                            DateTime.Now,
                        });
                    }

                }
                ++linenum;

            }
            if (suppErr > 0)
            {
                throw new Exception("Supplier code do not match " + SSUPP);
            }

			/* hiden ayu 20191121
            if (documentdtErr > 0)
            {
                // throw new Exception("Please Upload Data with Range Date 1-15 Or 16-31");         
                string message = "Invoice Will Be Paid Based on Last GR & Invoice Submission Date";
                throw new Exception(message);
                // MessageBox.Show(new Form() { WindowState = FormWindowState.Maximized, TopMost = true }, message, "Information");
            }*/

            ParseErrorCount = errCount;
            return t;

        }

        private string validateDocumentDate(string documentDate)
        {
            string process = "";

            process = DB.ExecuteScalar<string>("GetDataInvoiceValidationUpload", new object[] { documentDate, "" });

            return process;
        }

        private string Validate(List<string> l)
        {
            if (l == null || l.Count < CsvCol.Count) return null;
            int j = Math.Min(l.Count, CsvCol.Count);
            string err = "";

            for (int i = 0; i < j; i++)
            {
                if (!string.IsNullOrEmpty(l[i]))
                {
                    if (l[i].Length > CsvCol[i].Len)
                    {
                        err += "Truncated [" + CsvCol[i].Text + "] max " + CsvCol[i].Len + " chars ";
                        if (l[i].StartsWith("=\"") && l[i].EndsWith("\""))
                        {
                            throw new Exception("Data not edited");
                        }
                        l[i] = l[i].Substring(0, CsvCol[i].Len);
                    }
                }
                else
                {
                    if (CsvCol[i].Mandatory > 0)
                    {
                        err += "[" + CsvCol[i].Text + "] must be filled. ";
                    }
                }
            }
            return err;
        }

        private decimal Dec(string o, decimal v = 0m)
        {
            decimal x = 0;

            if (o == null || !decimal.TryParse(o.ToString(), out x))
            {
                x = v;
            }
            return x;
        }

        private string YMD(string s)
        {
            if (!string.IsNullOrEmpty(s) && s.Length > 9 && s.IndexOf(".") == 2 && s.LastIndexOf(".") == 5)
            {
                string t = s.Substring(6, 4) + "-" + s.Substring(3, 2) + "-" + s.Substring(0, 2);
                return t;
            }
            else
                return "";
        }

        public void BulkCopyStream(Stream st, long pid)
        {

            DataTable tx = ParseTemp(st, pid);
            string SSUPP = string.IsNullOrEmpty(SelectedSupplier) ? SupplierCode : SelectedSupplier;

            if (tx.Rows.Count < 1)
                throw new Exception("No Data Lines");

            string[] a = new string[] { "PROCESS_ID", "ROW_NO", "REMARKS",
                        "CREATED_BY", "CREATED_DT"};
            bool Ok = true;
            using (SqlBulkCopy bulk = new SqlBulkCopy(
                ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
            {
                bulk.DestinationTableName = "TB_T_INV_UPLOAD";
                bulk.ColumnMappings.Clear();
                for (int j = 0; j < 3; j++)
                {
                    bulk.ColumnMappings.Add(a[j], a[j]);
                }

                for (int i = 1; i < CsvCol.Count; i++)
                {
                    if (!string.IsNullOrEmpty(CsvCol[i].Column))
                    {
                        bulk.ColumnMappings.Add(CsvCol[i].Column, CsvCol[i].Column);
                    }
                }

                for (int k = 3; k < 5; k++)
                {
                    bulk.ColumnMappings.Add(a[k], a[k]);
                }

                string uid = Model.GetModel<User>().Username;
                try
                {
                    bulk.WriteToServer(tx);
                }
                catch (Exception ex)
                {
                    Ok = false;

                    ex.PutLog("", uid, "InvoiceCreation.BulkCopyStream",
                        pid, "MPCS00002ERR", "ERR", ModuleId, FunctionId, 1);
                }
            }
            if (!Ok)
                throw new Exception("Error writing data");

            if (ParseErrorCount > 0)
                throw new Exception("Invalid data");

        }
        public void BulkCopyStream_Excel(Stream st, long pid, string ufile)
        {
            OleDbConnection excelConn = null;
            OleDbCommand excelCommand = new OleDbCommand();
            OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
            string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + ufile + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
            excelConn = new OleDbConnection(excelConnStr);
            excelConn.Open();
            DataSet ds = new DataSet();
            DataSet dsExcel = new DataSet();
            DataTable dtPatterns = new DataTable();
            string sheet1 = excelConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
            string OleCommand = @"select " +
                                    pid + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as ClientID," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as SupplierCode," +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as PoNumber," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as PoItemNo," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as NewKanbanMaterialNumber," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as MaterialNumber," +
                                    @"IIf(IsNull(F7), Null, CStr(F7)) as ProdPurpose," +
                                    @"IIf(IsNull(F8), Null, CStr(F8)) as SourceType," +
                                    @"IIf(IsNull(F9), Null, CStr(F9)) as PackingType," +
                                    @"IIf(IsNull(F10), Null, CStr(F10)) as SuffixColour," +
                                    @"IIf(IsNull(F11), Null, CStr(F11)) as MaterialDescription," +
                                    @"IIf(IsNull(F12), Null, CStr(F12)) as MatDocNo," +
                                    @"IIf(IsNull(F13), Null, CStr(F13)) as ItemNo," +
                                    @"IIf(IsNull(F14), Null, CStr(F14)) as CompPrice," +
                                    @"IIf(IsNull(F15), Null, CStr(F15)) as SupplierManifest," +
                                    @"IIf(IsNull(F16), Null, CStr(F16)) as DocumentDate," +
                                    @"IIf(IsNull(F17), Null, CStr(F17)) as Quantity," +
                                    @"IIf(IsNull(F18), Null, CStr(F18)) as OrderNumber," +
                                    @"IIf(IsNull(F19), Null, CStr(F19)) as DockCode," +
                                    @"IIf(IsNull(F20), Null, CStr(F20)) as InvoiceNo," +
                                    @"IIf(IsNull(F21), Null, CStr(F21)) as SupplierQuantity," +
                                    @"IIf(IsNull(F22), Null, CStr(F22)) as Price," +
                                    @"IIf(IsNull(F23), Null, CStr(F23)) as Currency1," +
                                    @"IIf(IsNull(F24), Null, CStr(F24)) as Amount";

            OleCommand = OleCommand + " from [" + sheet1 + "]";

            excelCommand = new OleDbCommand(OleCommand, excelConn);

            excelDataAdapter.SelectCommand = excelCommand;

            bool Ok = true;
            string uid = Model.GetModel<User>().Username;
            try
            {
                dtPatterns.Columns.Add("Row_Num", typeof(int));
                dtPatterns.Columns.Add("CREATED_DT", typeof(DateTime));
                dtPatterns.Columns.Add("CREATED_BY", typeof(String));
                dtPatterns.Columns["Row_Num"].AutoIncrement = true;
                dtPatterns.Columns["Row_Num"].AutoIncrementSeed = 0;
                dtPatterns.Columns["Row_Num"].AutoIncrementStep = 1;

                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);
                DataRow rowDel = ds.Tables[0].Rows[0];
                ds.Tables[0].Rows.Remove(rowDel);

                /*string grDateString = "";

                int documentdtErr = 0;
                for (int i = 0; i < dtPatterns.Rows.Count; i++)
                {
                    dtPatterns.Rows[i]["CREATED_DT"] = DateTime.Now;
                    dtPatterns.Rows[i]["CREATED_BY"] = uid;
                    dtPatterns.Rows[i]["Price"] = dtPatterns.Rows[i]["Price"].ToString().Replace(",", "");
                    dtPatterns.Rows[i]["Amount"] = dtPatterns.Rows[i]["Amount"].ToString().Replace(",", "");


                    // Validasi Upload Excel  
                    if (dtPatterns.Rows[i]["DocumentDate"] != null)
                    {
                        string documentDate = dtPatterns.Rows[i]["DocumentDate"].ToString();
                        string grDateNextStr = "";
                        if (documentDate != null && !documentDate.Equals(""))
                        {
                            //string errSupp = "Please Upload Data with Range Date 1-15 Or 16-31";
                            grDateNextStr = validateDocumentDate(documentDate);

                            if (grDateString != null && !grDateString.Equals(""))
                            {
                                if (!grDateString.Equals(grDateNextStr))
                                {
                                    documentdtErr++;
                                    //throw new Exception(errSupp);
                                }
                            }
                            else
                            {
                                grDateString = grDateNextStr;
                            }
                        }
                    }
                    else
                    {
                        string errSupp = "Tanggal Document date tidak boleh kosong";
                        throw new Exception(errSupp);
                    }

                }

                if (documentdtErr > 0)
                {
                    // throw new Exception("Please Upload Data with Range Date 1-15 Or 16-31");         
                    string message = "Invoice Will Be Paid Based on Last GR & Invoice Submission Date";
                    throw new Exception(message);
                    // MessageBox.Show(new Form() { WindowState = FormWindowState.Maximized, TopMost = true }, message, "Information");
                }*/

            }
            catch (Exception ex)
            {
                Ok = false;

                ex.PutLog("", uid, "InvoiceCreation.BulkCopyStream", 
                    pid, "MPCS00002ERR", ModuleId, FunctionId);
                throw ex;
            }

            using (SqlBulkCopy bulk = new SqlBulkCopy(
                ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
            {
                bulk.DestinationTableName = "TB_T_INV_UPLOAD";
                bulk.ColumnMappings.Clear();
                bulk.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                bulk.ColumnMappings.Add("Row_Num", "ROW_NO");
                bulk.ColumnMappings.Add("SupplierCode", "SUPP_CD");
                bulk.ColumnMappings.Add("PoNumber", "PO_NO");
                bulk.ColumnMappings.Add("PoItemNo", "PO_ITEM_NO");
                bulk.ColumnMappings.Add("NewKanbanMaterialNumber", "MAT_NO_REF");
                bulk.ColumnMappings.Add("MaterialNumber", "MAT_NO");
                bulk.ColumnMappings.Add("ProdPurpose", "PROD_PURPOSE_CD");
                bulk.ColumnMappings.Add("SourceType", "SOURCE_TYPE_CD");
                bulk.ColumnMappings.Add("PackingType", "PACKING_TYPE");
                bulk.ColumnMappings.Add("SuffixColour", "PART_COLOR_SFX");
                bulk.ColumnMappings.Add("MaterialDescription", "MAT_TEXT");
                bulk.ColumnMappings.Add("MatDocNo", "MAT_DOC_NO");
                bulk.ColumnMappings.Add("ItemNo", "MAT_DOC_ITEM");
                bulk.ColumnMappings.Add("CompPrice", "COMP_PRICE_CD");
                bulk.ColumnMappings.Add("SupplierManifest", "INV_REF_NO");
                bulk.ColumnMappings.Add("DocumentDate", "DOC_DT");
                bulk.ColumnMappings.Add("Quantity", "QTY");
                bulk.ColumnMappings.Add("OrderNumber", "ORDER_NO");
                bulk.ColumnMappings.Add("DockCode", "DOCK_CD");
                bulk.ColumnMappings.Add("InvoiceNo", "SUPP_INV_NO");
                bulk.ColumnMappings.Add("SupplierQuantity", "S_QTY");
                bulk.ColumnMappings.Add("Price", "PRICE_AMT");
                bulk.ColumnMappings.Add("Currency1", "PAY_CURR");
                bulk.ColumnMappings.Add("Amount", "AMT");
                bulk.ColumnMappings.Add("CREATED_DT", "CREATED_DT");
                bulk.ColumnMappings.Add("CREATED_BY", "CREATED_BY");

                try
                {

                    bulk.WriteToServer(ds.Tables[0]);

                }
                catch (Exception ex)
                {
                    Ok = false;
                    ex.PutLog("", uid, "InvoiceCreation.BulkCopyStream", pid, "MPCS00002ERR", ModuleId, FunctionId);

                }
            }
            if (!Ok)
                throw new Exception("Error writing data");

            if (ParseErrorCount > 0)
                throw new Exception("Invalid data");



        }

        public long PutLog(string msg, string uid, string loc = "", long pid = 0,
            string id = "INF", string typ = "INF", string module = "",
            string func = "", int sts = 0)
        {
            if (string.IsNullOrEmpty(module)) module = ModuleId;
            if (string.IsNullOrEmpty(func)) func = FunctionId;

            long r = DB.ExecuteScalar<long>("PutLog",
                    new object[] { msg, uid, loc, pid
                    , id, typ, module, func, sts}
                );
            return r;
        }


        private string trim(string v)
        {
            if (!string.IsNullOrEmpty(v))
                return v.Trim();
            else
                return v;
        }

        #region DownloadXLS_Telerik
        public FileContentResult DownloadGR_IR_Summary(string DocDate)
        {
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string fileName = "";
            DateTime DocDateDt = (DocDate == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(DocDate, "dd.MM.yyyy", null);

            try
            {
                fileName = "Summary_GR_IR_" + DocDate + ".xls";
                documentBytes = DownloadGR_IR_SummaryReport(DocDateDt.ToString("yyyyMMdd"), DocDateDt);

                return File(documentBytes, "application/xls", fileName);
            }
            catch (Exception ex)
            {
                ex.PutLog("", AuthorizedUser.Username, "DownloadGR_IR_Summary", 0, "MPCS00002ERR", "ERR", ModuleId, FunctionId, 1);                
            }
            return null;
        }
        public FileContentResult DownloadGR_IR_Detail(string DocDate)
        {
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string fileName;
            DateTime DocDateDt = (DocDate == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(DocDate, "dd.MM.yyyy", null);

            try
            {
                fileName = "Detail_GR_IR_" + DocDate + ".xls";
                documentBytes = DownloadGR_IR_DetailReport(DocDateDt.ToString("yyyyMMdd"));

                return File(documentBytes, "application/xls", fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Telerik.Reporting.Report CreateReport(string reportPath,
            string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }

        private byte[] DownloadGR_IR_SummaryReport(string DocDate, DateTime DocFormatDate)
        {
            string Year1 = "";
            string Year2 = "";

            if (DocFormatDate.Month >= 1 && DocFormatDate.Month <= 3)
            {
                Year1 = (DocFormatDate.Year - 1).ToString();
                Year2 = (DocFormatDate.Year).ToString();
            }
            else
            {
                Year1 = (DocFormatDate.Year).ToString();
                Year2 = (DocFormatDate.Year + 1).ToString();
            }
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            Telerik.Reporting.ReportParameterCollection reportParameters = new Telerik.Reporting.ReportParameterCollection();

            parameters.Add("@DOC_DT", System.Data.DbType.String, DocDate);
            reportParameters.Add("Year1", Telerik.Reporting.ReportParameterType.String, Year1);
            reportParameters.Add("Year2", Telerik.Reporting.ReportParameterType.String, Year2);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\InvoiceAndPayment\RepInvoice_GR_IR_Summary.trdx", "SP_RepInvoice_GR_IR_Summary", parameters);
            rpt.ReportParameters.AddRange(reportParameters);

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("xls", rpt, null);

            return result.DocumentBytes;
        }
        private byte[] DownloadGR_IR_DetailReport(string DocDate)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();

            parameters.Add("@DOC_DT", System.Data.DbType.String, DocDate);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\InvoiceAndPayment\RepInvoice_GR_IR_Detail.trdx", "SP_RepInvoice_GR_IR_Detail", parameters);

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("xls", rpt, null);

            return result.DocumentBytes;
        }
        #endregion

        #region Download GR_IR_Detail
        public void DownloadGR_IR_DetailReport_Xls(string DocDate)
        {
            string fileName = "Detail_GR_IR_" + DocDate + ".xls";

            IDBContext db = DB;
            DateTime DocDateDt = (DocDate == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(DocDate, "dd.MM.yyyy", null);
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            IEnumerable<ReportGetGR_IR_Detail> qry = db.Fetch<ReportGetGR_IR_Detail>("ReportGetGR_IR_Detail", new object[] { DocDateDt });
            hasil = exporter.Write(qry, "Detail_GR_IR");
            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        public void WriteCSV<T>(IEnumerable<T> items, Stream stream)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(string.Join("; ", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join("; ", props.Select(p => p.GetValue(item, null))));
                }
            }
        }
        public FileContentResult DownloadListCSVNew(string DocDate)
        {
            Stream output = new MemoryStream();
            string fileName = "Detail_GR_IR_" + DocDate + ".csv";
            IDBContext db = DB;
            DateTime DocDateDt = (DocDate == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(DocDate, "dd.MM.yyyy", null);
            byte[] documentBytes;

            try
            {
                List<ReportGetGR_IR_Detail> lstReportGetGR_IR_Detail = db.Fetch<ReportGetGR_IR_Detail>("ReportGetGR_IR_Detail", new object[] { DocDateDt });
                db.Close();

                using (MemoryStream buffer = new MemoryStream())
                {
                    WriteCSV(lstReportGetGR_IR_Detail, buffer);
                    documentBytes = buffer.GetBuffer();
                }

                return File(documentBytes, "text/csv", fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public ActionResult GetInvoiceTax()
        {
            TempData["GridName"] = "InvoiceTaxNo";
            List<InvoiceCreationPreview> ICP = BindInvoiceTax();
            return PartialView("GridLookup/PartialGrid", ICP);
        }

        public List<InvoiceCreationPreview> BindInvoiceTax()
        {
            List<InvoiceCreationPreview> ICP = new List<InvoiceCreationPreview>();
            if (AuthorizedUser == null || string.IsNullOrEmpty(AuthorizedUser.Username) ) return null;
            String[] splittedUsername = AuthorizedUser.Username.Split('.');
            String vendorCode = (AuthorizedUser.Username.Length >= 4) ? AuthorizedUser.Username.Substring(0, 4) : null;
            if (splittedUsername.Length > 0)
                vendorCode = splittedUsername[0];
            
            try
            {
                ICP = DB.Fetch<InvoiceCreationPreview>("GetInvoiceTaxData", new object[] { vendorCode });
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                DB.Close();
            }

            return ICP.ToList();
        }

        public int GetInvoiceDateRange()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int range = 0;
            try
            {
                string function_id = "51002";
                range = db.ExecuteScalar<int>("GetInvoiceDateRange", new object[] { function_id });
            }
            catch (Exception e)
            {
                range = 0;
                throw (e);
            }
            return range;
        }

        public string GetDisabledRetroDate()
        {
            string retrodisabledDate = "";
            try
            {
                retrodisabledDate = DB.ExecuteScalar<string>("GetDisabledRetroDate");
            }
            catch (Exception e)
            {
                throw (e);
                retrodisabledDate = "E"; //Error when get Data from TB_M_SYSTEM
            }
            finally
            {
                DB.Close();
            }
            return retrodisabledDate;
        }

        public string PPh23TaxRate()
        {
            return DB.ExecuteScalar<string>("GetPPh23TaxRate");
        }

        public string CheckBeforePrintCertificate(string InvoiceNos, string InvoiceDates, string SupplierCodes)
        {
            string result = "";

            try
            {
                List<string> lstInvoiceDatesDt = new List<string>();
                string resultInvoiceDates = "";
                lstInvoiceDatesDt = InvoiceDates.Split(',').ToList<string>();

                foreach (string s in lstInvoiceDatesDt)
                {
                    DateTime InvoiceDatesDt = (s == "" || s == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(s, "dd.MM.yyyy", null);
                    resultInvoiceDates += InvoiceDatesDt.ToString("yyyyMMdd") + ",";
                }
                string role = "0";
                result = DB.ExecuteScalar<string>("CheckBeforePrintCertificate",
                        new object[] { InvoiceNos, resultInvoiceDates, SupplierCodes, role});
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally
            {
                DB.Close();
            }

            return result;
        }

        // ADDED BY fid.intan 2015-08-06
        // Get tolerance value for invoice creation from TB_M_SYSTEM
        public decimal GetToleranceVal()
        {
            decimal toleranceVal = 0;
            toleranceVal = DB.SingleOrDefault<decimal>("GetToleranceValue");
            return toleranceVal;
        }

        #region "Check Manifest"
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public ActionResult PopupCheckManifest()
        {
            InvoiceCreationModel model = Model.GetModel<InvoiceCreationModel>();
            //goodsReceiptInquiryDataModel = DB.Fetch<GoodsReceiptInquiryData>("GetDataManifestByManifestNo", new object[] { Request.Params["MANIFEST_NO"] });


            model.InvoiceCreationLists = getPartInfo(Request.Params["MANIFEST_NO"]);

            Model.AddModel(model);
            return PartialView("PopupCheckManifestGrid", Model);
        }

        public ActionResult PopupHeaderManifest()
        {
            return PartialView("PopupHeaderManifest");
        }

        protected List<InvoiceCreation> getPartInfo(string MANIFEST_NO)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<InvoiceCreation> listTooltipDatas = new List<InvoiceCreation>();

            listTooltipDatas = db.Query<InvoiceCreation>("GetDataManifestByManifestNo", new object[] { MANIFEST_NO }).ToList();

            db.Close();
            return listTooltipDatas;
        }

        List<InvoiceCreationErrorPosting> _goodsReceiptInquiryManifestDetailDataModel = null;
        private List<InvoiceCreationErrorPosting> _GoodsReceiptInquiryManifestDetailDataModel
        {
            get
            {
                if (_goodsReceiptInquiryManifestDetailDataModel == null)
                    _goodsReceiptInquiryManifestDetailDataModel = new List<InvoiceCreationErrorPosting>();

                return _goodsReceiptInquiryManifestDetailDataModel;
            }
            set
            {
                _goodsReceiptInquiryManifestDetailDataModel = value;
            }
        }
        public ActionResult PartialPopupGoodsReceiptInquiryManifestDetailErrorPosting()
        {
            #region Required model for [Goods receipt inquiry manifest detail gridview]
            _GoodsReceiptInquiryManifestDetailDataModel = GetAllGoodsReceiptInquiryManifestDetailErrorPosting(
                String.IsNullOrEmpty(Request.Params["DockCode"]) ? "" : Request.Params["DockCode"],
                String.IsNullOrEmpty(Request.Params["RcvPlantCode"]) ? "" : Request.Params["RcvPlantCode"],
                String.IsNullOrEmpty(Request.Params["ManifestNo"]) ? "" : Request.Params["ManifestNo"]
                );
            Model.AddModel(_GoodsReceiptInquiryManifestDetailDataModel);
            #endregion

            return PartialView("GoodsReceiptInquiryManifestDetailErrorPostingPartial", Model);
        }
        public List<InvoiceCreationErrorPosting> GetAllGoodsReceiptInquiryManifestDetailErrorPosting(
           String dockCode = "",
           String rcvPlantCode = "",
           String manifestNo = ""
           )
        {
            List<InvoiceCreationErrorPosting> goodsReceiptInquiryManifestDetailDataModel = new List<InvoiceCreationErrorPosting>();
            IDBContext db = DbContext;
            try
            {
                if (!String.IsNullOrEmpty(dockCode) || !String.IsNullOrEmpty(rcvPlantCode) || !String.IsNullOrEmpty(dockCode))
                    goodsReceiptInquiryManifestDetailDataModel = db.Fetch<InvoiceCreationErrorPosting>(
                        "GetAllGoodsReceiptInquiryManifestDetailErrorPosting",
                        new object[] {
                            dockCode,
                            rcvPlantCode,
                            manifestNo,
                            });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return goodsReceiptInquiryManifestDetailDataModel;
        }
        #endregion


        //Add By Arka.Taufik 2022-03-22
        #region GetTaxList
        public List<InvoiceCreationTax> GetTaxListStart()
        {
            List<InvoiceCreationTax> TaxLists = new List<InvoiceCreationTax>();
            try
            {
                TaxLists = DB.Fetch<InvoiceCreationTax>("GetTaxList");
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                DB.Close();
            }
            return TaxLists.ToList();
        }

        public List<InvoiceCreationTax> GetTaxList()
        {
            List<InvoiceCreationTax> TaxLists = new List<InvoiceCreationTax>();
            try
            {
                TaxLists = DB.Fetch<InvoiceCreationTax>("GetTaxList");
            }
            catch(Exception e)
            {
                throw (e);
            }
            finally
            {
                DB.Close();
            }
            return TaxLists;
        }

        public ActionResult PartialTaxGridLookup()
        {
            ViewData["TaxGridLookup"] = GetTaxList();

            TempData["GridName"] = "TaxCodeCombo";

            return PartialView("GridLookup/PartialGrid", ViewData["TaxGridLookup"]);
        }
        #endregion

    }

    
}
