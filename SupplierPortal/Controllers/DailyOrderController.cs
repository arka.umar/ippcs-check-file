﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Threading;
using System.Data;
using System.Xml;
using System.Transactions;
using System.Data.SqlClient;
using System.Diagnostics;

using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Excel;

using Portal.Models.DailyOrder;
using Portal.Models.Supplier;
using Portal.Models.Dock;
using Portal.Models.Globals;
using Portal.Models;
using System.Text;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.MessageBoard;


namespace Portal.Controllers
{
    public class DailyOrderController : BaseController
    {
        
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        /// <summary>
        /// If the current response is a FileResult (an MVC base class for files) then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (httpContext.Request.Cookies[CookieName] != null)
            {
                response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public DailyOrderController()
            : base("Order Inquiry")
        {
            PageLayout.UseMessageBoard = true;
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        #region Model properties
        List<SupplierName> _orderInquirySupplierModel = null;
        private List<SupplierName> _OrderInquirySupplierModel
        {
            get
            {
                if (_orderInquirySupplierModel == null)
                    _orderInquirySupplierModel = new List<SupplierName>();

                return _orderInquirySupplierModel;
            }
            set
            {
                _orderInquirySupplierModel = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }

        #endregion

        protected override void StartUp()
        {
            ViewData["EOT"] = GetEOType();
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;

            DailyOrderModel mdl = new DailyOrderModel();
            
            Model.AddModel(mdl);

            int getAuth = Model.GetModel<Toyota.Common.Web.Credential.User>().IsAuthorized("DailyOrder", "OIHCancelOrderButton");
            ViewData["CancelButtonAuth"] = getAuth;

            ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");
            ViewData["EOT"] = GetEOType();
        }

        #region Order Inquiry controller
        #region View controller
        public ActionResult PartialHeaderOrderInquiry()
        {
            Session["paramManifestNo"] = String.IsNullOrEmpty(Request.Params["ManifestNos"]) ? Session["paramManifestNo"] : Request.Params["ManifestNos"];
            #region Required model in partial page : for OrderInquiryHeaderPartial
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");

            _OrderInquirySupplierModel = suppliers;
            
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrder", "OIHDockOption");
            _OrderInquiryDockModel = DockCodes;

            Model.AddModel(_OrderInquirySupplierModel);
            Model.AddModel(_OrderInquiryDockModel);
            #endregion

            return PartialView("OrderInquiryHeaderPartial", Model);
        }
        public ActionResult PartialHeaderDockLookupGridOrderInquiry()
        {

            #region Required model in partial page : for OIHDockOption GridLookup
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrder", "OIHDockOption");
            TempData["GridName"] = "OIHDockOption";
            _OrderInquiryDockModel = DockCodes;
            
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }
        public ActionResult PartialHeaderSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");

            TempData["GridName"] = "OIHSupplierOption";
            _OrderInquirySupplierModel = suppliers;
            
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquirySupplierModel);
        }

        public ActionResult PartialDetailOrderInquiryDailyOrder()
        {
            #region Required model in partial page : for OrderInquiryDailyOrderDetailPartial
            


            DailyOrderModel model = Model.GetModel<DailyOrderModel>();
            model.RegularOrders = GetAllDailyOrder(Request.Params["SupplierCode"],
                                        Request.Params["DockCode"],
                                        Request.Params["ManifestNo"],
                                        Request.Params["DateFrom"],
                                        Request.Params["DateTo"],
                                        "1",
                                        Request.Params["Capability"]);
            #endregion

            return PartialView("OrderInquiryDailyOrderDetailPartial", Model);
        }
        public ActionResult PartialDetailOrderInquiryEmergencyOrder()
        {
            DailyOrderModel model = Model.GetModel<DailyOrderModel>();
            try
            {
                #region Required model in partial page : for OrderInquiryEmergencyOrderDetailPartial
                
                model.EmergencyOrders = GetAllNonDailyOrderPlanEO(Request.Params["SupplierCode"],
                                                    Request.Params["DockCode"],
                                                    Request.Params["ManifestNo"],
                                                    Request.Params["DateFrom"],
                                                    Request.Params["DateTo"],
                                                    "2",
                                                    Session["paramManifestNo"] == null ? "" : Session["paramManifestNo"].ToString(),
                                                    Request.Params["PartName"],
                                                    Request.Params["PartStatus"], 
                                                    Request.Params["EOType"]);
                #endregion
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("OrderInquiryEmergencyOrderDetailPartial", Model);
        }
        public ActionResult PartialDetailOrderInquiryComponentPartOrder()
        {
            #region Required model in partial page : for OrderInquiryComponenPartOrderDetailPartial
            DailyOrderModel model = Model.GetModel<DailyOrderModel>();
            model.ComponentPartOrders = GetAllNonDailyOrderPlan(Request.Params["SupplierCode"],
                                                Request.Params["DockCode"],
                                                Request.Params["ManifestNo"],
                                                Request.Params["DateFrom"],
                                                Request.Params["DateTo"],
                                                "3",
                                                Request.Params["Capability"]);
            #endregion

            return PartialView("OrderInquiryComponentPartOrderPartial", Model);
        }
        public ActionResult PartialDetailOrderInquiryJunbikiOrder()
        {
            #region Required model in partial page : for OrderInquiryComponenPartOrderDetailPartial
            DailyOrderModel model = Model.GetModel<DailyOrderModel>();
            model.ComponentPartOrders = GetAllNonDailyOrderPlan(Request.Params["SupplierCode"],
                                            Request.Params["DockCode"],
                                            Request.Params["ManifestNo"],
                                            Request.Params["DateFrom"],
                                            Request.Params["DateTo"],
                                            "4",
                                            Request.Params["Capability"]);
            #endregion

            return PartialView("OrderInquiryJunbikiOrderPartial", Model);
        }
        public ActionResult PartialDetailOrderInquiryProblemPartOrder()
        {
            #region Required model in partial page : for OrderInquiryProblemPartOrderPartial
            DailyOrderModel model = Model.GetModel<DailyOrderModel>();
            model.ProblemPartOrders = GetAllNonDailyOrderPlan(Request.Params["SupplierCode"],
                                            Request.Params["DockCode"],
                                            Request.Params["ManifestNo"],
                                            Request.Params["DateFrom"],
                                            Request.Params["DateTo"],
                                            "5",
                                            Request.Params["Capability"]);
            #endregion

            return PartialView("OrderInquiryProblemPartOrderPartial", Model);
        }
        public ActionResult PartialDetailOrderInquiryServicePartExport()
        {
            DailyOrderModel model = Model.GetModel<DailyOrderModel>();
            try
            {
                #region Required model in partial page : for OrderInquiryEmergencyOrderDetailPartial
                
                model.ServicePartExport = GetAllServicePartExport(Request.Params["SupplierCode"],
                                                    Request.Params["DockCode"],
                                                    Request.Params["ManifestNo"],
                                                    Request.Params["DateFrom"],
                                                    Request.Params["DateTo"],
                                                    "3",
                                                    Session["paramManifestNo"] == null ? "" : Session["paramManifestNo"].ToString(),
                                                    Request.Params["PartName"],
                                                    Request.Params["PartStatus"]);
                #endregion
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("OrderInquiryServicePartExportDetailPartial", Model);
        }

        public ActionResult UpdateCallBackPanel()
        {
            ViewData["OrderReleaseDate"] = Request.Params["OrderReleaseDate"];
            
            return PartialView("PopupDailyDataManifestDetail");
        }
        public ActionResult PopupHeaderDailyDataManifestDetail() //for refresh
        {
            ViewData["SupplierCode"] = Request.Params["SupplierCode"];
            ViewData["TotalManifest"] = Request.Params["TotalManifest"];
            ViewData["OrderPlantQty"] = Request.Params["OrderPlanQtyPost"];

            ViewData["DownloadStatus"] = "DownloadStatus";
            ViewData["DownloadOrderData"] = "DownloadOrderData";
            ViewData["Capability"] = "Capability";

            return PartialView("PartialHeaderDailyDataDetail", Model);
        }
        public ActionResult PopupDailyDataManifestDetail()
        {
            string orderReleaseDate = Request.Params["OrderReleaseDate"];
            string supplierCode = Request.Params["SupplierCode"];
            string rcvPlantCode = Request.Params["RcvPlantCode"];
            string remainingQty = Request.Params["RemainingQty"];
            string orderType = Request.Params["OrderType"];

            string supplierCD = string.Empty;
            string supplierPlant = string.Empty;
            if (supplierCode == null || supplierCode == "")
            {

            }
            else
            {
                char[] splitChar = { '-' };
                string[] splitString = supplierCode.Split(splitChar);
                supplierCD = splitString[0].Trim();
                supplierPlant = splitString[1].Trim();
            }

            ViewData["manifestDailyData"] = DailyDataManifestOrderFillGrid(orderReleaseDate, supplierCD, supplierPlant, rcvPlantCode, orderType);
            return PartialView("DailyDataDetail", ViewData["manifestDailyData"]);

        }
        public ActionResult PopupHeaderDailyDataPartSubDetail()
        {
            string manifest = Request.Params["ManifestNo"];

         
            int getAuth = Model.GetModel<Toyota.Common.Web.Credential.User>().IsAuthorized("DailyOrder", "OIHCancelOrderButton");
           

            ViewData["CancelButtonAuth"] = getAuth;
           
            return PartialView("PartialHeaderDailyDataSubDetail", ViewData["ManifestNo"]);
        }
        public ActionResult PopupDailyDataPartSubDetail()
        {
            string manifestNo = Request.Params["ManifestNo"];
            string problemFlag = Request.Params["ProblemFlag"];
            ViewData["partDailyData"] = DailyDataPartOrderFillGrid(manifestNo, problemFlag);
            return PartialView("DailyDataSubDetail", ViewData["partDailyData"]);
        }
        public JsonResult CancelOrder(string ManifestNo)
        {
            IDBContext db = DbContext;
            try
            {
                string[] manifestNos = ManifestNo.Split('|');
                foreach (string manifest in manifestNos)
                {
                    db.Execute("CancelEmergencyOrder", new object[] { manifest, AuthorizedUser.Username });
                }
            }
            catch (Exception exc) { throw exc; }

            db.Close();
            return Json("Success");
        }

        #endregion

        #region Common controller
        private String GetGeneralProductionMonth(String productionMonth)
        {
            Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
            monthDictionary.Add("01", "January");
            monthDictionary.Add("02", "February");
            monthDictionary.Add("03", "March");
            monthDictionary.Add("04", "April");
            monthDictionary.Add("05", "May");
            monthDictionary.Add("06", "June");
            monthDictionary.Add("07", "July");
            monthDictionary.Add("08", "August");
            monthDictionary.Add("09", "September");
            monthDictionary.Add("10", "October");
            monthDictionary.Add("11", "November");
            monthDictionary.Add("12", "December");

            String generalProductionMonth = "";

            if (!String.IsNullOrEmpty(productionMonth))
            {
                String year = productionMonth.Substring(0, 4);
                String month = productionMonth.Substring(productionMonth.Length - 2, 2);

                generalProductionMonth = monthDictionary[month] + " " + year;
            }

            return generalProductionMonth;
        }
        #endregion

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> orderInquirySupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }
        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            IDBContext db = DbContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return orderInquiryDockModel;
        }
        private List<DailyOrder> GetAllDailyOrder(String supplierCode,
            String dockCode,
            String manifestNo,
            String dateFrom,
            String dateTo,
            String OrderType,
            String Capability)
        {
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<DailyOrder> dailyOrderModel = new List<DailyOrder>();
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrder", "OIHDockOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            IDBContext db = DbContext;
            dailyOrderModel = db.Fetch<DailyOrder>("GetAllDailyOrder", new object[] {
                supplierCode == null ? "" : supplierCode,
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType,
                Capability,
                SupplierCodeLDAP,
                DockCodeLDAP
            });
            db.Close();

            return dailyOrderModel;
        }
        private List<NonDailyOrder> GetAllNonDailyOrderPlan(String supplierCode, String dockCode, String manifestNo, String dateFrom, String dateTo, string OrderType, string manifestNos)
        {
            IDBContext db = DbContext;
            List<NonDailyOrder> dailyOrderModel = db.Fetch<NonDailyOrder>("GetAllNonDailyOrderPlan", new object[] {
                supplierCode == null ? "" : supplierCode,
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType,
                manifestNos
            });
            db.Close();

            return dailyOrderModel;
        }
        private List<NonDailyOrder> GetAllNonDailyOrderPlanEO(String supplierCode, String dockCode, String manifestNo, String dateFrom, String dateTo,
            string OrderType, string manifestNos, string PartName, string PartStatus, string EOType)
        {
            IDBContext db = DbContext;
            List<NonDailyOrder> dailyOrderModel = db.Fetch<NonDailyOrder>("GetAllNonDailyOrderPlanEO", new object[] {
                supplierCode == null ? "" : supplierCode,
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType,
                manifestNos,
                PartName == "-" ? "" : PartName,
                PartStatus == null ? "" : PartStatus,
                EOType
            });
            db.Close();

            return dailyOrderModel;
        }
        private List<NonDailyOrder> GetAllServicePartExport(String supplierCode, String dockCode, String manifestNo, String dateFrom, String dateTo, string OrderType, string manifestNos, string PartName, string PartStatus)
        {
            IDBContext db = DbContext;
            List<NonDailyOrder> dailyOrderModel = db.Fetch<NonDailyOrder>("GetAllServicePartExport", new object[] {
                supplierCode == null ? "" : supplierCode,
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType,
                manifestNos,
                PartName == "-" ? "" : PartName,
                PartStatus == null ? "" : PartStatus
            });
            db.Close();

            return dailyOrderModel;
        }

        protected List<ManifestOrder> DailyDataManifestOrderFillGrid(string orderReleaseDate, string supplierCode, string supplierPlant, string rcvPlantCode, string orderType)
        {
            List<ManifestOrder> LogList = new List<ManifestOrder>();
            IDBContext db = DbContext;
            string DockCodeLDAP = "";
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrder", "OIHDockOption");
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            LogList = db.Query<ManifestOrder>("GetDailyOrderDailyDataManifestFillGrid", new object[] {
                    (orderReleaseDate == "" || orderReleaseDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) :  DateTime.ParseExact(orderReleaseDate,"dd.MM.yyyy hh:mm",null),
                    supplierCode,
                    supplierPlant,
                    orderType,
                    DockCodeLDAP,
                    rcvPlantCode
            }).ToList<ManifestOrder>();

            db.Close();
            return LogList;
        }
        protected List<PartDetailManifest> DailyDataPartOrderFillGrid(string manifestNo, string problemFlag)
        {
            List<PartDetailManifest> getPartDetailManifest = new List<PartDetailManifest>();
            IDBContext db = DbContext;
            getPartDetailManifest = db.Query<PartDetailManifest>("GetPartDetailManifest", new object[] { manifestNo, problemFlag }).ToList<PartDetailManifest>();

            db.Close();
            return getPartDetailManifest;
        }

        private void UpdateDownloadFlag(string OrderReleaseDate, string SupplierPlantCode, string RcvPlantCode)
        {
            IDBContext db = DbContext;
            string result = string.Empty;
            char[] splitStr = { '-' };
            string[] supps = SupplierPlantCode.Split(splitStr);
            string supplierCode = string.Empty;
            string plantCode = string.Empty;
            string replyCapability = string.Empty;
            DateTime OrdReleaseData = DateTime.ParseExact(OrderReleaseDate.Substring(0, 10), "dd.MM.yyyy", null);

            supplierCode = supps[0] == "null" || supps.Equals("") ? string.Empty : supps[0].Trim();
            plantCode = supps[1] == "null" || supps.Equals("") ? string.Empty : supps[1].Trim();
            db.Execute("UpdateDownloadFlagOrder", new Object[] { OrdReleaseData, supplierCode, plantCode, AuthorizedUser.Username, RcvPlantCode });
            db.Close();
        }
        private void UpdateUnDownloadFlag(string OrderReleaseDate, string SupplierPlantCode, string plantCode, string RcvPlantCode)
        {
            IDBContext db = DbContext;
            string result = string.Empty;
            char[] splitStr = { '-' };
            string[] supps = SupplierPlantCode.Split(splitStr);
            string supplierCode = string.Empty;
            
            string replyCapability = string.Empty;
            DateTime OrdReleaseData = DateTime.ParseExact(OrderReleaseDate.Substring(0, 10), "dd.MM.yyyy", null);

            supplierCode = supps[0] == "null" || supps.Equals("") ? string.Empty : supps[0].Trim();
            
            db.Execute("UpdateUnDownloadFlagOrder", new Object[] { OrdReleaseData, supplierCode, plantCode, AuthorizedUser.Username, RcvPlantCode });
            db.Close();
        }
        private IEnumerable<ComboData> GetEOType()
        {
            List<ComboData> coda = new List<ComboData>();

            string uname = (User != null) ? User.Identity.Name : "noone";
            IDBContext db = DbContext;
            try
            {
                coda = db.Fetch<ComboData>("GetEOType", new object[] { }).ToList();
                if (coda != null) coda.Insert(0, new ComboData() { Text = "ALL", Val = "" });                    
            }
            catch (Exception ex)
            {
                ex.PutLog("eot", uname, "GetEOT");
            }
            finally
            {
                db.Close();
            }
            return coda;
        }
        private void UpdateDownloadFlagNonRegular(string ManifestNo)
        {
            IDBContext db = DbContext;

            db.Execute("UpdateDownloadFlagOrderNonRegular", new Object[] { ManifestNo, AuthorizedUser.Username });
            db.Close();
        }
        public ContentResult UpdateCapability(string OrderReleaseDate, string SupplierPlantCode, string RcvPlantCode, string selected, string Reason)
        {
            IDBContext db = DbContext;
            string result = string.Empty;
            char[] splitStr = { '-' };
            string[] supps = SupplierPlantCode.Split(splitStr);
            string supplierCode = string.Empty;
            string plantCode = string.Empty;
            string replyCapability = string.Empty;
            DateTime OrdReleaseData = DateTime.ParseExact(OrderReleaseDate.Substring(0, 10), "dd.MM.yyyy", null);

            supplierCode = supps[0] == "null" || supps.Equals("") ? string.Empty : supps[0].Trim();
            plantCode = supps[1] == "null" || supps.Equals("") ? string.Empty : supps[1].Trim();
            replyCapability = selected.Equals("Yes") ? "1" : selected.Equals("No") ? "0" : string.Empty;
            db.Execute("UpdateCapabilityOrderWeb", new Object[] { OrdReleaseData, supplierCode, plantCode, replyCapability, AuthorizedUser.Username, RcvPlantCode });
            db.Close();

            if (selected.Equals("No"))
            {
                MessageBoardMessage boardMessage = new MessageBoardMessage();
                boardMessage.GroupId = 0;
                boardMessage.BoardName = "OrderInquiry_" + OrderReleaseDate + "_" + supplierCode + "_" + plantCode;
                boardMessage.Sender = AuthorizedUser.Username;
                boardMessage.Recipient = "supplierportal8";
                boardMessage.CarbonCopy = true;
                boardMessage.Text = Reason;
                boardMessage.Date = DateTime.Now;
                IMessageBoardService boardService = MessageBoardService.GetInstance();
                boardService.Save(boardMessage);
            }
            return Content(result);
        }
        public ContentResult UnlockCapability(string OrderReleaseDate, string SupplierPlantCode, string RcvPlantCode)
        {
            IDBContext db = DbContext;
            string result = string.Empty;
            char[] splitStr = { '-' };
            string[] supps = SupplierPlantCode.Split(splitStr);
            string supplierCode = string.Empty;
            string plantCode = string.Empty;
            string replyCapability = string.Empty;
            DateTime OrdReleaseData = DateTime.ParseExact(OrderReleaseDate.Substring(0, 10), "dd.MM.yyyy", null);

            supplierCode = supps[0] == "null" || supps.Equals("") ? string.Empty : supps[0].Trim();
            plantCode = supps[1] == "null" || supps.Equals("") ? string.Empty : supps[1].Trim();
            db.Execute("UnlockCapabilityOrderWeb", new Object[] { OrdReleaseData, supplierCode, plantCode, RcvPlantCode });
            db.Close();
            return Content(result);
        }

        public JsonResult UpdateManifest(bool cancelFlag, string manifest, string OrderReleaseDate, string Supplier, string RcvPlantCode)
        {
            FTPUpload vFtp = new FTPUpload();
            string FolderName = "";
            string SupplierCode = "";
            string SupplierPlant = "";
            string FileName;
            List<string> ManifestNos = new List<string>();
            ManifestNos = manifest.Split(',').ToList();
            string ErrMessage = "";
            DateTime OrderDate = DateTime.ParseExact(OrderReleaseDate, "dd.MM.yyyy hh:mm", null);
            OrderReleaseDate = OrderDate.ToString("yyyyMMddhhmm");

            if (Supplier != "" && Supplier != null)
            {
                string[] supplier = Supplier.Split('-');
                SupplierCode = supplier[0].Replace(" ", "");
                SupplierPlant = supplier[1].Replace(" ", "");
            }
            FolderName = OrderReleaseDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            IDBContext db = DbContext;
            ManifestResult result = new ManifestResult();

            foreach (string ManifestNo in ManifestNos)
            {
                FileName = ManifestNo + "-";
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/ManifestPrePrinted", FileName + "1-1.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/ManifestPlain", FileName + "0-1.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/Kanban", FileName + "2.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/Skid", FileName + "3.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName, OrderReleaseDate + "-" + SupplierCode + SupplierPlant + "-" + RcvPlantCode + ".zip", ref ErrMessage);

                try
                {
                    result = db.SingleOrDefault<ManifestResult>("UpdateDailyManifest", new object[] { cancelFlag, manifest, AuthorizedUser.Username });
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                    db.Close();
                }

            }

            db.Execute("DailyOrderUpdateTelerixFlag", new object[] { RcvPlantCode });

            return Json(result);
        }
        public void UpdateCancelPart(string OrderReleaseDate, string Supplier, string RcvPlantCode, string ManifestNo, string ItemNo)
        {
            FTPUpload vFtp = new FTPUpload();
            string FolderName = "";
            string SupplierCode = "";
            string SupplierPlant = "";
            string FileName = ManifestNo + "-";
            string ErrMessage = "";
            DateTime OrderDate = DateTime.ParseExact(OrderReleaseDate, "dd.MM.yyyy hh:mm", null);
            OrderReleaseDate = OrderDate.ToString("yyyyMMddhhmm");

            if (Supplier != "" && Supplier != null)
            {
                string[] supplier = Supplier.Split('-');
                SupplierCode = supplier[0].Replace(" ", "");
                SupplierPlant = supplier[1].Replace(" ", "");
            }
            FolderName = OrderReleaseDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            IDBContext db = DbContext;
            try
            {
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/ManifestPrePrinted", FileName + "1-1.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/ManifestPlain", FileName + "0-1.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/Kanban", FileName + "2.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName + "/Skid", FileName + "3.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(vFtp.Setting.FtpPath("DailyOrderPrinting") + FolderName, OrderReleaseDate + "-" + SupplierCode + SupplierPlant + "-" + RcvPlantCode + ".zip", ref ErrMessage);
                db.Execute("CancelPartOrder", new object[] { ManifestNo, ItemNo, AuthorizedUser.Username });
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                db.Close();
            }

            db.Execute("DailyOrderUpdateTelerixFlag", new object[] { RcvPlantCode });

        }
        public void UpdateCancelPartNonReguler(string ManifestNo, string ItemNo, string OrderType)
        {
            FTPUpload vFtp = new FTPUpload();
            string FolderName = "";
            string FileName = ManifestNo + "-";
            string ErrMessage = "";
            switch (OrderType)
            {
                case "1":
                    FolderName = vFtp.Setting.FtpPath("DailyOrderPrinting");
                    break;
                case "2":
                    FolderName = vFtp.Setting.FtpPath("EmergencyOrder");
                    break;
                case "3":
                    FolderName = vFtp.Setting.FtpPath("CPOOrder");
                    
                    break;
                case "4":
                    FolderName = vFtp.Setting.FtpPath("JunbikiOrder");
                    break;
                case "5":
                    FolderName = vFtp.Setting.FtpPath("ProblemPartOrder");
                    break;
            }

            IDBContext db = DbContext;
            try
            {
                vFtp.FTPRemoveFile(FolderName + "/ManifestPrePrinted", FileName + "1-1.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(FolderName + "/ManifestPlain", FileName + "0-1.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(FolderName + "/Kanban", FileName + "2.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(FolderName + "/Skid", FileName + "3.pdf", ref ErrMessage);
                vFtp.FTPRemoveFile(FolderName, ManifestNo + ".zip", ref ErrMessage);
                db.Execute("CancelPartOrderNonReguler", new object[] { ManifestNo, ItemNo, AuthorizedUser.Username });
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                db.Close();
            }
        }

        public ContentResult UpdatePartOrder(string ManifestNo, string ItemNos, string OrderQtys)
        {
            string[] ItemNo = ItemNos.Split(';');
            string[] OrderQty = OrderQtys.Split(';');
            string Result = "";
            IDBContext db = DbContext;
            try
            {
                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {
                    for (int i = 0; i < ItemNo.Length; i++)
                    {
                        db.ExecuteScalar<string>("UpdateDailyOrderPart", new object[] { ManifestNo, ItemNo[i], OrderQty[i], AuthorizedUser.Username });
                    }
                    db.Execute("UpdateDailyOrderManifest", new object[] { ManifestNo, AuthorizedUser.Username });
                    scope.Complete();
                    Result = "ManifestNo " + ManifestNo + ": Update Succesfully";
                }
            }
            catch (Exception ex)
            {
                Result = "ManifestNo " + ManifestNo + ": Error updating data (" + ex.Message + ")";
            }
            finally
            {
                db.Close();
            }
            return Content(Result);
        }
        public ContentResult GetDailyOrderCapabilityNull(string SupplierCode,
                string DockCode,
                string ManifestNo,
                string OrderReleaseDateFrom,
                string OrderReleaseDateTo)
        {
            IDBContext db = DbContext;
            string Result = "";
            DateTime OrdReleaseDateFrom = DateTime.ParseExact(OrderReleaseDateFrom.Substring(0, 10), "dd.MM.yyyy", null);
            DateTime OrdReleaseDateTo = DateTime.ParseExact(OrderReleaseDateTo.Substring(0, 10), "dd.MM.yyyy", null);

            try
            {
                Result = db.ExecuteScalar<string>("GetDailyOrderWithCapabilityNull", new object[] {
                                                SupplierCode,
                                                DockCode,
                                                ManifestNo,
                                                OrdReleaseDateFrom,
                                                OrdReleaseDateTo});
            }
            catch (Exception ex)
            {
                Result = "Error updating data (" + ex.Message + ")";
            }
            finally
            {
                db.Close();
            }
            return Content(Result);
        }
       
        #endregion
        #endregion

        #region DownloadPDF
        public FileContentResult PrintDailyOrder(string OrderDate, string SupplierCode, string RcvPlantCode)
        {
            
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string fileName;

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "dd.MM.yyyy hh:mm", null);
            try
            {
                FTPUpload vFtp = new FTPUpload();
                string msg = "";
                string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, vFtp.Setting.FtpPath("DailyOrderPrinting") + OrdDate.ToString("yyyyMMddhhmm") + "/" + Supplier[0] + "/" + Supplier[1] + "/" + RcvPlantCode);
                documentBytes = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, OrdDate.ToString("yyyyMMddhhmm") + "-" + Supplier[1] + Supplier[0] + "-" + RcvPlantCode + ".zip"),
                                                       ref msg);

                UpdateDownloadFlag(OrderDate, SupplierCode, RcvPlantCode);

                return File(documentBytes, "application/zip", OrdDate.ToString("yyyyMMddhhmm") + "-" + Supplier[1] + Supplier[0] + "-" + RcvPlantCode + ".zip");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public FileContentResult DownloadPDF(string ManifestNo, string OrderType)
        {
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string filePath = "";
            try
            {
                FTPUpload vFtp = new FTPUpload();
                string msg = "";
                switch (OrderType)
                {
                    case "1":
                        filePath = vFtp.Setting.FtpPath("DailyOrderPrinting");
                        break;
                    case "2":
                        filePath = vFtp.Setting.FtpPath("EmergencyOrder");
                        break;
                    case "3":
                        filePath = vFtp.Setting.FtpPath("CPOOrder");
                        
                        break;
                    case "4":
                        filePath = vFtp.Setting.FtpPath("JunbikiOrder");
                        break;
                    case "5":
                        filePath = vFtp.Setting.FtpPath("ProblemPartOrder");
                        break;
                }

                string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, filePath);
                documentBytes = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, ManifestNo + ".zip"),
                                                       ref msg);

                UpdateDownloadFlagNonRegular(ManifestNo);

                return File(documentBytes, "application/zip", ManifestNo + ".zip");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Telerik.Reporting.Report CreateReport(string reportPath,
            string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }

        private byte[] PrintManifestReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Stopwatch sw = new Stopwatch();
            TimeSpan ts;

            sw.Start();
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@PartNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\DailyOrder\Rep_SupplierManifest.trdx", "SP_Rep_DailyOrder_Manifest_Print", parameters);
            sw.Stop();
            ts = sw.Elapsed;

            FTPUpload vFtp = new FTPUpload();
            string msg = "";
       
            try
            {
                string[] ListFileFTP = vFtp.DirectoryList(vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant);

                foreach (string file in ListFileFTP)
                {
                    string[] filename = file.Split('/');
                    if (filename[1] == Filename)
                    {
                        string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant);
                        byte[] resDownload = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, Filename),
                                                               ref msg);
                        return resDownload;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            vFtp = new FTPUpload();
            bool resultUpload = vFtp.FtpUpload(vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);

            return result.DocumentBytes;
        }
        private byte[] PrintSkidReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Stopwatch sw = new Stopwatch();
            TimeSpan ts;

            sw.Start();
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\DailyOrder\Rep_ManifestSkid_Download.trdx", "SP_Rep_DailyOrder_Skid_Print", parameters);

            FTPUpload vFtp = new FTPUpload();
            string msg = "";
           

            
            try
            {
                string[] ListFileFTP = vFtp.DirectoryList(vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant);

                foreach (string file in ListFileFTP)
                {
                    string[] filename = file.Split('/');
                    if (filename[1] == Filename)
                    {
                        string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant);
                        byte[] resDownload = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, Filename),
                                                               ref msg);
                        return resDownload;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            vFtp = new FTPUpload();
            bool resultUpload = vFtp.FtpUpload(vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);

            sw.Stop();
            ts = sw.Elapsed;
            return result.DocumentBytes;
        }
        private byte[] PrintKanbanReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Stopwatch sw = new Stopwatch();
            TimeSpan ts;

            sw.Start();
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@PartNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\DailyOrder\Rep_ManifestKanban_Download.trdx", "SP_Rep_DailyOrder_Kanban_Print", parameters);

            FTPUpload vFtp = new FTPUpload();
            string msg = "";
            
            try
            {
                string[] ListFileFTP = vFtp.DirectoryList(vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant);

                foreach (string file in ListFileFTP)
                {
                    string[] filename = file.Split('/');
                    if (filename[1] == Filename)
                    {
                        string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant);
                        byte[] resDownload = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, Filename),
                                                               ref msg);
                        return resDownload;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            vFtp = new FTPUpload();
            bool resultUpload = vFtp.FtpUpload(vFtp.Setting.FtpPath("DailyOrder") + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);

            sw.Stop();
            ts = sw.Elapsed;
            return result.DocumentBytes;
        }
        #endregion


        #region DownloadCSV
        public FileContentResult PrintDailyOrderCSV(string OrderDate, string SupplierCode, string RcvPlantCode, string OrderType)
        {
            ICompression compress = ZipCompress.GetInstance();
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();

            Stream output = new MemoryStream();
            string fileName = "Order_" + OrderDate + ".csv";

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "dd.MM.yyyy hh:mm", null);

            if (OrderType == "1") OrderType = "R";
            else if (OrderType == "2") OrderType = "E";
            else if (OrderType == "3") OrderType = "C";
            else if (OrderType == "4") OrderType = "J";

            string connString = DBContextNames.DB_PCS;
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[connString].ConnectionString);

            try
            {
                #region query
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT m.MANIFEST_NO
                                            ,s.SUPPLIER_CODE
                                            ,s.SUPPLIER_PLANT_CD
                                            ,SHIPPING_DOCK=case when REPLACE(m.SHIPPING_DOCK,' ','') <> '' then m.SHIPPING_DOCK else '' end
                                            ,m.COMPANY_CD
                                            ,p.RCV_PLANT_CD
                                            ,p.DOCK_CD
                                            ,m.ORDER_NO
                                            ,CAR_FAMILY_CD=case when REPLACE(m.CAR_FAMILY_CD,' ','') <> '' then m.CAR_FAMILY_CD else '' end
                                            ,RE_EXPORT_CD=case when REPLACE(m.RE_EXPORT_CD,' ','') <> '' then m.RE_EXPORT_CD else '' end
                                            ,p.PART_NO
                                            ,p.KANBAN_PRINT_ADDRESS
                                            ,m.ORDER_TYPE
                                            ,ORDER_RELEASE_DT=CONVERT(VARCHAR(23), m.ORDER_RELEASE_DT, 121)
                                            ,p.KANBAN_NO
                                            ,p.QTY_PER_CONTAINER
                                            ,p.ORDER_QTY
                                            ,p.ORDER_PLAN_QTY_LOT
                                            ,p.BUILD_OUT_FLAG
                                            ,p.BUILD_OUT_QTY
                                            ,p.AICO_CEPT_FLAG
                                            ,p.SUPPLIER_NAME
                                            ,p.PART_NAME
                                            ,p.ORDER_MODE
                                            ,SHIPPING_DT=CONVERT(VARCHAR(23), m.SHIPPING_DT, 121)
                                            ,ARRIVAL_DT=CONVERT(VARCHAR(23), m.ARRIVAL_PLAN_DT, 121)
                                            ,m.P_LANE_CD
                                            ,m.P_LANE_SEQ
                                            ,m.P_LANE_DT
                                            ,m.SUB_ROUTE_CD
                                            ,m.SUB_ROUTE_SEQ
                                            ,m.SUB_ROUTE_DT
                                            ,m.FST_X_DOC_CD
                                            ,m.FST_X_PLANT_CD
                                            ,m.FST_X_SHIPPING_DOCK_CD
                                            ,FST_X_DOCK_ARRIVAL_DT=CONVERT(VARCHAR(23), m.FST_X_DOCK_ARRIVAL_DT, 121)
                                            ,FST_X_DOCK_DEPART_DT=CONVERT(VARCHAR(23), m.FST_X_DOCK_DEPART_DT, 121)
                                            ,m.FST_X_DOCK_ROUTE_GRP_CD
                                            ,m.FST_X_DOCK_SEQ_NO
                                            ,m.fst_x_route_dt
                                            ,m.SND_X_DOC_CD
                                            ,m.SND_X_PLANT_CD
                                            ,m.SND_X_SHIPPING_DOCK_CD
                                            ,SND_X_DOCK_ARRIVAL_DT=CONVERT(VARCHAR(23), m.SND_X_DOCK_ARRIVAL_DT, 121)
                                            ,SND_X_DOCK_DEPART_DT=CONVERT(VARCHAR(23), m.SND_X_DOCK_DEPART_DT, 121)
                                            ,m.SND_X_DOCK_ROUTE_GRP_CD
                                            ,m.SND_X_DOCK_SEQ_NO
                                            ,m.TRD_X_DOC_CD
                                            ,m.TRD_X_PLANT_CD
                                            ,m.TRD_X_SHIPPING_DOCK_CD
                                            ,TRD_X_DOCK_ARRIVAL_DT=CONVERT(VARCHAR(23), m.TRD_X_DOCK_ARRIVAL_DT, 121)
                                            ,TRD_X_DOCK_DEPART_DT=CONVERT(VARCHAR(23), m.TRD_X_DOCK_DEPART_DT, 121)
                                            ,m.TRD_X_DOCK_ROUTE_GRP_CD
                                            ,m.TRD_X_DOCK_SEQ_NO
                                            ,m.trd_x_route_dt
                                            ,m.MANIFEST_PRINT_POINT_CD
                                            ,MANIFEST_PRINT_DATE_PLANT=CONVERT(VARCHAR(23), m.MANIFEST_PRINT_DATE_PLANT, 121)
                                            ,MANIFEST_PRINT_DATE_LOCAL=CONVERT(VARCHAR(23), m.MANIFEST_PRINT_DATE_LOCAL, 121)
                                            ,p.KANBAN_PRINT_POINT_CD
                                            ,KANBAN_PRINT_DATE_PLANT=CONVERT(VARCHAR(23), p.KANBAN_PRINT_DATE_PLANT, 121)
                                            ,KANBAN_PRINT_DATE_LOCAL=CONVERT(VARCHAR(23), p.KANBAN_PRINT_DATE_LOCAL, 121)
                                            ,p.CONVEYANCE_ROUTE
                                            ,FERRY_PORT_ARRIVAL_DT=CONVERT(VARCHAR(23), p.FERRY_PORT_ARRIVAL_DT, 121)
                                            ,FERRY_PORT_DEPART_DT=CONVERT(VARCHAR(23), p.FERRY_PORT_DEPART_DT, 121)
                                            ,m.PRINT_FLAG
                                            ,PRINT_DATE=CONVERT(VARCHAR(23), m.PRINT_DATE, 121)
                                            ,m.RECEIVE_FLAG
                                            ,RECEIVE_DT=CONVERT(VARCHAR(23), m.RECEIVE_DT, 121)
                                            ,m.USAGE_FLAG
                                            ,USAGE_DT=CONVERT(VARCHAR(23), m.USAGE_DT, 121)
                                            ,m.DELETION_FLAG
                                            ,DELETION_DT=CONVERT(VARCHAR(23), m.DELETION_DT, 121)
                                            ,m.DROP_STATUS_FLAG
                                            ,DROP_STATUS_DT=CONVERT(VARCHAR(23), m.DROP_STATUS_DT, 121)
                                            ,np.RECEIVED_QTY
                                            ,SAPFlag=m.ICS_FLAG
                                            ,SAPDate=CONVERT(VARCHAR(23), m.ICS_DT, 121)
                                            ,m.TOTAL_PAGE
                                            ,p.EKBS_FLAG
                                            ,EKBS_DT=CONVERT(VARCHAR(23), p.EKBS_DT, 121)
                                            ,m.PAT_DP_FLAG
                                            ,m.PAT_PL_FLAG
                                            ,p.RECEIVE_PLANT_NAME
                                            ,p.PACKAGE_TYPE
                                            ,p.SUPPLIER_INFO1
                                            ,p.SUPPLIER_INFO2
                                            ,p.SUPPLIER_INFO3
                                            ,p.PRINT_CD
                                            ,p.IMPORTIR_INFO
                                            ,p.IMPORTIR_INFO2
                                            ,p.IMPORTIR_INFO3
                                            ,p.IMPORTIR_INFO4
                                            ,p.PART_BARCODE
                                        FROM TB_R_DAILY_ORDER_MANIFEST m
                                        INNER JOIN TB_R_DAILY_ORDER_PART p ON p.MANIFEST_NO = m.MANIFEST_NO
                                        INNER JOIN TB_M_SUPPLIER s ON s.SUPPLIER_CODE = m.SUPPLIER_CD AND s.SUPPLIER_PLANT_CD = m.SUPPLIER_PLANT
                                        LEFT JOIN TB_R_PROBLEM_PART np on p.MANIFEST_NO=np.MANIFEST_NO AND p.PART_NO=np.PART_NO
                                        WHERE m.ORDER_TYPE='1' AND
                                          CONVERT(VARCHAR(8),m.CREATED_DT,112) ='" + OrdDate.ToString("yyyyMMdd") + @"' AND
                                          m.SUPPLIER_CD = '" + Supplier[0] + @"' AND
                                          m.SUPPLIER_PLANT = '" + Supplier[1] + @"' AND
                                          m.RCV_PLANT_CD = '" + RcvPlantCode + @"'
                                        ORDER BY
                                          m.SUPPLIER_CD
                                        , m.SUPPLIER_PLANT
                                        , ARRIVAL_PLAN_DT";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn.Close();
                #endregion query

                byte[] documentBytes;

                CsvFile csvFile = new CsvFile();

                using (MemoryStream buffer = new MemoryStream())
                {
                    using (CsvWriter writer = new CsvWriter())
                    {
                        writer.WriteCsv(ds.Tables[0], buffer);
                        documentBytes = buffer.GetBuffer();
                    }
                }

                listCompress.Add(Supplier[0] + Supplier[1] + ".csv", documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                UpdateDownloadFlag(OrderDate, SupplierCode, RcvPlantCode);
                return File(documentBytes, "application/zip", Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DownloadTXT
        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }
        public FileContentResult PrintDailyOrderTXT(string OrderDate, string SupplierCode, string RcvPlantCode, string OrderType)
        {
            ICompression compress = ZipCompress.GetInstance();
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();

            Stream output = new MemoryStream();
            string fileName = "Order_" + OrderDate + ".txt";

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "dd.MM.yyyy hh:mm", null);

            if (OrderType == "1") OrderType = "R";
            else if (OrderType == "2") OrderType = "E";
            else if (OrderType == "3") OrderType = "C";
            else if (OrderType == "4") OrderType = "J";

            string connString = DBContextNames.DB_PCS;
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[connString].ConnectionString);

            try
            {

                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                
                cmd.CommandText = @"EXEC SP_DAILY_ORDER_REGULAR_REPORT_TXT '" + OrdDate.ToString("yyyyMMdd") + @"','" + Supplier[0] + @"','" + Supplier[1] + @"','" + RcvPlantCode + @"' ";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn.Close();

                string tab = "";
                StringBuilder strHeader = new StringBuilder();
                StringBuilder str = new StringBuilder();

                string filePath = Path.Combine(Server.MapPath("~/Views/DailyOrder/DownloadTemp/"), Supplier[0] + Supplier[1] + ".txt");
                string filePathZip = Path.Combine(Server.MapPath("~/Views/DailyOrder/DownloadTemp/"), Supplier[0] + Supplier[1] + RcvPlantCode + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");
                StreamWriter writer = new StreamWriter(filePath);

                int orderQty = 0;
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    tab = "";
                    for (int j = 0; j <= ds.Tables[0].Columns.Count - 1; j++)
                    {
                        //Response.Write(tab + ds.Tables[0].Rows[i][j].ToString());
                        if (j == 83) str.Append(tab + (ds.Tables[0].Rows[i][j] == null ? "" : ds.Tables[0].Rows[i][j].ToString()));
                        else str.Append(tab + (ds.Tables[0].Rows[i][j] == null ? "" : ds.Tables[0].Rows[i][j].ToString().TrimEnd()));

                        tab = "\t";
                        if (j == 16) orderQty = orderQty + (ds.Tables[0].Rows[i][j] == null || ds.Tables[0].Rows[i][j].ToString().Trim() == "" ? 0 : Convert.ToInt32(ds.Tables[0].Rows[i][j]));
                    }

                    str.AppendLine();
                    //Response.Write("\n");
                }
                strHeader.Append(Supplier[0] + "|" +
                                Supplier[1] + "|" +
                                OrdDate.ToString("yyyyMMddhhmm") + "|" +
                                ("00000000" + ds.Tables[0].Rows.Count.ToString()).Substring((("00000000" + ds.Tables[0].Rows.Count.ToString()).Length - 8), 8) + "|" +
                                ("00000000" + orderQty.ToString()).Substring((("00000000" + orderQty.ToString()).Length - 8), 8));
                strHeader.AppendLine();

                writer.Write(strHeader);
                writer.Write(str.ToString());
                writer.Dispose();

                
                byte[] documentBytes = StreamFile(filePath);
                listCompress.Add(Supplier[0] + Supplier[1] + ".txt", documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                UpdateDownloadFlag(OrderDate, SupplierCode, RcvPlantCode);
                return File(documentBytes, "application/zip", Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public FileContentResult PrintDailyOrderTXTNonRegular(string ManifestNo, string OrderDate, string SupplierCode, string OrderType)
        {
            ICompression compress = ZipCompress.GetInstance();
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();

            Stream output = new MemoryStream();
            string fileName = "Order_" + OrderDate + ".txt";

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "dd.MM.yyyy hh:mm", null);

            if (OrderType == "1") OrderType = "R";
            else if (OrderType == "2") OrderType = "E";
            else if (OrderType == "3") OrderType = "C";
            else if (OrderType == "4") OrderType = "J";

            string connString = DBContextNames.DB_PCS;
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[connString].ConnectionString);

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                
                cmd.CommandText = @"SP_DAILY_ORDER_NON_REGULAR_REPORT_TXT '" + ManifestNo + @"','" + OrdDate.ToString("yyyyMMdd") + @"', '" + Supplier[0] + @"' , '" + Supplier[1] + @"' ";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn.Close();

                string tab = "";
                StringBuilder strHeader = new StringBuilder();
                StringBuilder str = new StringBuilder();

                string filePath = Path.Combine(Server.MapPath("~/Views/DailyOrder/DownloadTemp/"), Supplier[0] + Supplier[1] + ".txt");
                string filePathZip = Path.Combine(Server.MapPath("~/Views/DailyOrder/DownloadTemp/"), Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");
                StreamWriter writer = new StreamWriter(filePath);

                int orderQty = 0;
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    tab = "";
                    for (int j = 0; j <= ds.Tables[0].Columns.Count - 1; j++)
                    {
                        //Response.Write(tab + ds.Tables[0].Rows[i][j].ToString());
                        if (j == 83) str.Append(tab + (ds.Tables[0].Rows[i][j] == null ? "" : ds.Tables[0].Rows[i][j].ToString()));
                        else str.Append(tab + (ds.Tables[0].Rows[i][j] == null ? "" : ds.Tables[0].Rows[i][j].ToString().TrimEnd()));

                        tab = "\t";
                        if (j == 16) orderQty = orderQty + (ds.Tables[0].Rows[i][j] == null || ds.Tables[0].Rows[i][j].ToString().Trim() == "" ? 0 : Convert.ToInt32(ds.Tables[0].Rows[i][j]));
                    }

                    str.AppendLine();
                    
                }
                strHeader.Append(Supplier[0] + "|" +
                                Supplier[1] + "|" +
                                OrdDate.ToString("yyyyMMddhhmm") + "|" +
                                ("00000000" + ds.Tables[0].Rows.Count.ToString()).Substring((("00000000" + ds.Tables[0].Rows.Count.ToString()).Length - 8), 8) + "|" +
                                ("00000000" + orderQty.ToString()).Substring((("00000000" + orderQty.ToString()).Length - 8), 8));
                strHeader.AppendLine();

                writer.Write(strHeader);
                writer.Write(str.ToString());
                writer.Dispose();

                
                byte[] documentBytes = StreamFile(filePath);
                listCompress.Add(Supplier[0] + Supplier[1] + ".txt", documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                UpdateDownloadFlagNonRegular(ManifestNo);
                return File(documentBytes, "application/zip", Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FileContentResult PrintDailyOrderTXTSPEX(string ManifestNo, string OrderDate, string SupplierCode, string OrderType)
        {
            ICompression compress = ZipCompress.GetInstance();
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();

            Stream output = new MemoryStream();
            string fileName = "Order_" + OrderDate + ".txt";

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "dd.MM.yyyy hh:mm", null);

            if (OrderType == "3") OrderType = "C";
            else OrderType = "XXX";

            IDBContext db = DbContext;

            string filePath = Path.Combine(Server.MapPath("~/Views/DailyOrder/DownloadTemp/"), Supplier[0] + Supplier[1] + ".txt");
            string filePathZip = Path.Combine(Server.MapPath("~/Views/DailyOrder/DownloadTemp/"), Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                try
                {
                    if (OrderType != "C")
                    {
                        throw new Exception("Wrong Data Selected, Please Refresh The Page Before Continue");
                    }

                    List<SPEXDailyOrderRAWData> rows = db.Fetch<SPEXDailyOrderRAWData>("GetTXTRawSPEX", new object[] { ManifestNo, OrdDate.ToString("yyyyMMdd"), Supplier[0], Supplier[1] });

                    string tab = "";
                    StringBuilder strHeader = new StringBuilder();
                    StringBuilder str = new StringBuilder();

                    int orderQty = 0;
                    string[] columns;
                    foreach (SPEXDailyOrderRAWData r in rows)
                    {
                        tab = "";
                        columns = r.DATA.Split('|');
                        for (int j = 0; j < columns.Length; j++)
                        {
                          
                            str.Append(tab + (columns[j] == null ? "" : columns[j].ToString().TrimEnd()));

                            tab = "\t";
                        }

                        str.AppendLine();
                        orderQty = orderQty + r.ORDER_QTY;
                    }

                    int count = rows == null ? 0 : rows.Count;
                    string RowsCount = ("00000000" + count.ToString()).Substring((("00000000" + count.ToString()).Length - 8), 8);
                    string orderqty_str = ("00000000" + orderQty.ToString()).Substring((("00000000" + orderQty.ToString()).Length - 8), 8);

                    strHeader.Append(Supplier[0] + "|" +
                                    Supplier[1] + "|" +
                                    OrdDate.ToString("yyyyMMddhhmm") + "|" +
                                    RowsCount + "|" +
                                    orderqty_str);
                    strHeader.AppendLine();

                    writer.Write(strHeader);
                    writer.Write(str.ToString());
                    writer.Dispose();

                    byte[] documentBytes = StreamFile(filePath);
                    listCompress.Add(Supplier[0] + Supplier[1] + ".txt", documentBytes);

                    compress.Compress(listCompress, output);
                    documentBytes = new byte[output.Length];
                    output.Position = 0;
                    output.Read(documentBytes, 0, documentBytes.Length);

                    UpdateDownloadFlagNonRegular(ManifestNo);
                    return File(documentBytes, "application/zip", Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");
                }
                catch (Exception ex)
                {
                    writer.Dispose();
                    throw ex;
                }
            }
        }
        #endregion

        #region Download
        public void DownloadList(string SupplierCode,
                            string DockCode,
                            string ManifestNo,
                            string OrderReleaseDateFrom,
                            string OrderReleaseDateTo,
                            string tabActived,
                            string partName,
                            string partStatus)
        {
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrder", "OIHDockOption");

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            string fileName = "DailyOrder.xls";

            IDBContext db = DbContext;
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            if (tabActived == "1")
            {
                IEnumerable<ReportGetAllDailyOrderModel> qry = db.Fetch<ReportGetAllDailyOrderModel>("ReportGetAllDailyOrder", new object[] {
                    SupplierCode == null ? "" : SupplierCode,
                    DockCode == null ? "" : DockCode,
                    ManifestNo == null ? "" : ManifestNo,
                    (OrderReleaseDateFrom == "" || OrderReleaseDateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateFrom,"dd.MM.yyyy",null),
                    (OrderReleaseDateTo == "" || OrderReleaseDateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateTo,"dd.MM.yyyy",null),
                    tabActived,
                    SupplierCodeLDAP,
                    DockCodeLDAP
                });
                hasil = exporter.Write(qry, "DailyOrderData");
            }
            else if (tabActived == "2") //emergency order
            {
                IEnumerable<ReportGetAllNonDailyOrderModelEO> qry = db.Fetch<ReportGetAllNonDailyOrderModelEO>("ReportGetAllNonDailyOrderEO", new object[] {
                    SupplierCode == null ? "" : SupplierCode,
                    DockCode == null ? "" : DockCode,
                    ManifestNo == null ? "" : ManifestNo,
                    (OrderReleaseDateFrom == "" || OrderReleaseDateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateFrom,"dd.MM.yyyy",null),
                    (OrderReleaseDateTo == "" || OrderReleaseDateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateTo,"dd.MM.yyyy",null),
                    tabActived,
                    SupplierCodeLDAP,
                    DockCodeLDAP,
                    partName,
                    partStatus
                });
                hasil = exporter.Write(qry, "DailyOrderData");
            }
 
            else
            {
                IEnumerable<ReportGetAllNonDailyOrderModel> qry = db.Fetch<ReportGetAllNonDailyOrderModel>("ReportGetAllNonDailyOrder", new object[] {
                    SupplierCode == null ? "" : SupplierCode,
                    DockCode == null ? "" : DockCode,
                    ManifestNo == null ? "" : ManifestNo,
                    (OrderReleaseDateFrom == "" || OrderReleaseDateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateFrom,"dd.MM.yyyy",null),
                    (OrderReleaseDateTo == "" || OrderReleaseDateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateTo,"dd.MM.yyyy",null),
                    tabActived,
                    SupplierCodeLDAP,
                    DockCodeLDAP
                });
                hasil = exporter.Write(qry, "DailyOrderData");
            }
            db.Close();

            Response.Clear();
            
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        public JsonResult UnCheckDownloadFlag(string OrderReleaseDate, string Supplier, string RcvPlantCode)
        {

            string result = "success";
            string SupplierCode = "";
            string SupplierPlant = "";


            DateTime OrderDate = DateTime.ParseExact(OrderReleaseDate, "dd.MM.yyyy hh:mm", null);
            

            if (Supplier != "" && Supplier != null)
            {
                string[] supplier = Supplier.Split('-');
                SupplierCode = supplier[0].Replace(" ", "");
                SupplierPlant = supplier[1].Replace(" ", "");
            }

            try
            {
                UpdateUnDownloadFlag(OrderReleaseDate, SupplierCode, SupplierPlant, RcvPlantCode);
            }
            catch (Exception ex)
            {
                throw (ex);
            }



            

            return Json(result);
        }
    }

    class ManifestResult
    {
        public string TOTAL_MANIFEST { set; get; }
        public string ORDER_PLAN_QTY { set; get; }
    }
    class UnDownloadFlagResult
    {
        public string Download_Flag { set; get; }

    }

}


