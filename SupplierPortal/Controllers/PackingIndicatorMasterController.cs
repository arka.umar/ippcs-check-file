﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Portal.Models.Globals;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Globalization;
using System.Configuration;
using DevExpress.Web.ASPxUploadControl;
using Portal.Models.PackingIndicatorMaster;
using Portal.Models.InvoiceAndPaymentInquiry;
using System.Net;
using DevExpress.Web.ASPxUploadControl;
using NPOI.HSSF.UserModel;
//using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using DevExpress.Web.Mvc;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;


namespace Portal.Controllers
{
    public class PackingIndicatorMasterController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        readonly string FUNCTION_ID = "71013";
        readonly string MODULE_ID = "7";

        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        public string UID { get {
            return (AuthorizedUser != null && !string.IsNullOrEmpty(AuthorizedUser.Username)) ? AuthorizedUser.Username : "";
        } }


        public PackingIndicatorMasterController()
            : base("P1.1.3 Packing Indicator Master")
        {

        }

        #region Model properties
        List<SupplierICS> _invoiceInquirySupplierModel = null;
        private List<SupplierICS> _InvoiceInquirySupplierModel
        {
            get
            {
                if (_invoiceInquirySupplierModel == null)
                    _invoiceInquirySupplierModel = new List<SupplierICS>();

                return _invoiceInquirySupplierModel;
            }
            set
            {
                _invoiceInquirySupplierModel = value;
            }
        }

        #endregion

        #region Strat Controller
        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            PackingIndicatorMasterModel prm = new PackingIndicatorMasterModel();
            List<PackingIndicatorMasterData> data = new List<PackingIndicatorMasterData>();
            PageLayout.UseSlidingBottomPane = true;
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");
            List<MaterialICS> mics = Dbutil.GetList<MaterialICS>("GetAllMaterialICS");
            ViewData["lookUPmaterial"] = mics;
            List<Dock> dockList = Dbutil.GetList<Dock>("getDropdownDock");
            ViewData["DockCodeCmb"] = dockList;
            
            List<Dock> Docks = new List<Dock>();
            Docks.Add(new Dock() { DockCode = "", DockName = "All" });
            Docks.AddRange(dockList);
            ViewData["Docks"] = Docks;

            List<ComboData> packingTypes = Dbutil.GetList<ComboData>("getDropdownPackingType");
            ViewData["PackingTypeCmb"] = packingTypes;

            List<ComboData> PackingTypes = new List<ComboData>();
            PackingTypes.Add(new ComboData() { Val = "", Text = "All" });
            PackingTypes.AddRange(packingTypes);
            ViewData["PackingTypes"] = PackingTypes;

            ViewData["MaterialNoCmb"] = mics;
            ViewData["ProdPurposeCmb"] = Dbutil.GetList<ComboData>("getDropdowngetAllProdPurpose");
            ViewData["SourceTypeCmb"] = Dbutil.GetList<ComboData>("GetSourceTypeICSCombo");
           
            prm.PackingIndicatorMasterDatas = data;
            Model.AddModel(prm);
        }

        
        #endregion
        #region LookUp
        List<MaterialICS> _packingIndicatorMaterialModel = null;
        private List<MaterialICS> _PackingIndicatorMaterialModel
        {
            get
            {
                if (_packingIndicatorMaterialModel == null)
                    _packingIndicatorMaterialModel = new List<MaterialICS>();
                return _packingIndicatorMaterialModel;
            }
            set
            {
                _packingIndicatorMaterialModel = value;
            }
        }
        public ActionResult LookUpMaterial()
        {
            List<MaterialICS> Materials = Model.GetModel<User>().FilteringArea<MaterialICS>(GetAllMaterial(), "MAT_NO", "PackingIndicatorMaster", "AddEditMaterialNo");
            TempData["GridName"] = "AddEditMaterialNo";

            _packingIndicatorMaterialModel = Materials;
            return PartialView("GridLookup/PartialGrid", _packingIndicatorMaterialModel);
        }
        public ActionResult LookupEditMaterial()
        {
            List<MaterialICS> Materials = Model.GetModel<User>().FilteringArea<MaterialICS>(GetAllMaterial(), "MAT_NO", "PackingIndicatorMaster", "EditNewMaterialNo");
            TempData["GridName"] = "EditNewMaterialNo";

            _packingIndicatorMaterialModel = Materials;
            return PartialView("GridLookup/PartialGrid", _packingIndicatorMaterialModel);
        }
        public List<MaterialICS> GetAllMaterial() 
        {
            return Dbutil.GetList<MaterialICS>("GetAllMaterialICS");
         }
        #endregion
        #region Database controller
        private List<SupplierICS> GetAllSupplierICS()
        {
            return Dbutil.GetList<SupplierICS>("GetAllSupplierICS");
        }

        private List<PackingIndicatorMasterData> GetAllPackingIndikator(string MaterialNo, string DockCd, string PackingType)
        {
            List<PackingIndicatorMasterData> packingIndicator = new List<PackingIndicatorMasterData>();
            IDBContext db = DbContext;

            packingIndicator = db.Fetch<PackingIndicatorMasterData>("GetAllPackingIndikator",
                new object[] { 
                MaterialNo == null ? "" : MaterialNo,
                DockCd == null ? "" : DockCd,
                PackingType == null ? "" : PackingType
                });
            db.Close();

            return packingIndicator;
        }
        #endregion

        #region Controller Views
        public ActionResult HeaderPackingIndicatorMaster()
        {
            var u = Model.GetModel<User>();
            if (u == null)
                return null;

            List<SupplierICS> suppliers = u.FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");
            _InvoiceInquirySupplierModel = suppliers;

            Model.AddModel(_InvoiceInquirySupplierModel);
            return PartialView("HeaderPackingIndicatorMaster", Model);
        }

        public ActionResult PackingIndicatorMasterInquiryPartial()
        {
            string MaterialNo = Request.Params["MaterialNo"];
            string DockCd = Request.Params["DockCd"];
            string PackingType = Request.Params["PackingType"];

            #region Required model in partial page : for InvoiceAndPaymentInquiryPartial
            PackingIndicatorMasterModel model = Model.GetModel<PackingIndicatorMasterModel>();
            model.PackingIndicatorMasterDatas = GetAllPackingIndikator(MaterialNo, DockCd, PackingType);

            return PartialView("PackingIndicatorMasterInquiryPartial", Model);
            #endregion
        }

       
        public ContentResult DeletePackingIndicatorMaster(string GridId)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                
                string[] ParamUpdate = GridId.Split(new char[] { ';' });

                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                int approve = 0;
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    if (ParamUpdate[i] != "")
                    {
                        string[] p = ParamUpdate[i].Split(new char[] { '_' });

                        var o = new { mat_no = p[0], source_type = p[1], prod_purpose_cd = p[2], part_color_sfx = p[3], dock_cd = p[4], valid_dt_fr = p[5] };
                        approve = db.SingleOrDefault<int>("DeleteCheckPackingIndicatorMaster", new object[] { o } );

                        if (approve == 0)
                        {
                            //Check data, maybe deleted by someone
                            throw new Exception("Error|Error in Delete because of concurrency check.");
                        }
                        else
                        {
                            db.ExecuteScalar<string>("DeletePackingIndicatorMaster", new object[] { o });                           
                        }
                    }
                }
                return Content("Success|Deletion process is completed successfully");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally { db.Close(); }

        }

        public ActionResult AddNewPart(string status, string MatNo, string SourceType, string ProdPurpose, string ColorSuffix, string DockCode, string PackingType, string SuppName, string ValidFrom, string ValidTo)
        {
            string ResultQuery = "Saving data is completed successfully";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                if (status == "new")
                    db.Execute("InsertPackingIndicatorMaster", new object[] { 
                        MatNo, 
                        SourceType, 
                        ProdPurpose, 
                        ColorSuffix, 
                        DockCode,
                        PackingType, 
                        ValidFrom, 
                        ValidTo,
                        UID,
                        DateTime.Now 
                    });
                else
                    db.Execute("UpdatePackingIndicatorMaster", new object[] { 
                        MatNo, 
                        PackingType, 
                        ValidFrom,
                        UID,
                        SourceType, 
                        ProdPurpose, 
                        ColorSuffix, 
                        DockCode,
                    });
            }
            catch (Exception ex)
            { ResultQuery = "Error|" + ex.Message; return Json(ResultQuery); }

            db.Close();
            return Content("Success|"+ ResultQuery);
        }
        #endregion

        #region Download Template
        public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/PackingIndicatorMaster/DownloadTemp/PackingIndicatorMasterData.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "PackingIndicatorMasterData.xls");
        }

        public FileContentResult DownloadErr(string pid)
        {
            string filename = "PackingIndicatorErr_" + pid + ".xls";
            string filePath = Path.Combine(_UploadDirectory, filename);
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", filename);
        }


        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }
        #endregion

        #region Download Excel

        #region Check Data Before Download
        public JsonResult CheckBeforeDownload(string MatNo, string DockCd, string PackingType)
        {
            string result = string.Empty;

            PackingIndicatorMasterData data = new PackingIndicatorMasterData();

            try
            {
                result = DbContext.ExecuteScalar<string>("DownloadCheckPackingIndicatorMaster", new object[] { MatNo, DockCd, PackingType });

                data.Result = result;
                data.MAT_NO = MatNo;
                data.DOCK_CD = DockCd;
                data.PACKING_TYPE_CD = PackingType;

            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally
            { DbContext.Close(); }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Configuration Download NPOI
        protected void SendDataAsAttachment(string fileName, byte[] data)
        {
            Response.Clear();
            
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        public static ICellStyle createCellStyleDataLeft(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.LEFT;

            return headerStyle;
        }

        public static ICellStyle createCellStyleDataRight(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.RIGHT;
            return headerStyle;
        }

        public static ICellStyle createCellStyleDataCenter(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.CENTER;

            return headerStyle;
        }

        public static ICellStyle createCellStyleDataDouble(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            IDataFormat dataFormatCustom = wb.CreateDataFormat();
            headerStyle.DataFormat = dataFormatCustom.GetFormat("#,##0.00");
            return headerStyle;
        }

        public static ICellStyle createCellStyleColumnHeader(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;
            headerStyle.FillPattern = FillPatternType.THIN_HORZ_BANDS;
            headerStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.CENTER;

            IFont headerLabelFont = wb.CreateFont();
            headerLabelFont.Color = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            headerLabelFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(headerLabelFont);
            return headerStyle;
        }

        public static ICellStyle createCellStyleColumnTitle(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();

            headerStyle.FillPattern = FillPatternType.NO_FILL;


            headerStyle.WrapText = false;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.LEFT;

            IFont headerLabelFont = wb.CreateFont();
            headerLabelFont.FontName = HSSFFont.FONT_ARIAL;
            headerLabelFont.FontHeightInPoints = 13;
            headerLabelFont.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            headerLabelFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(headerLabelFont);
            return headerStyle;
        }

        public static ICellStyle createCellStyleColumnTitleSecond(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
        
            headerStyle.FillPattern = FillPatternType.NO_FILL;
   
            headerStyle.WrapText = false;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.LEFT;

            IFont headerLabelFont = wb.CreateFont();
            headerLabelFont.FontName = HSSFFont.FONT_ARIAL;
            headerLabelFont.FontHeightInPoints = 10;
            headerLabelFont.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            headerLabelFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(headerLabelFont);
            return headerStyle;
        }

        public static ICellStyle createCellStyleDataDate(HSSFWorkbook wb, short dataFormat)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;
            headerStyle.DataFormat = dataFormat;

            return headerStyle;
        }

        private const int MaxNumberOfRowPerSheet = 65500;
        private const int MaximumSheetNameLength = 25;
        public static string EscapeSheetName(string sheetName)
        {
            string escapedSheetName = sheetName
                                        .Replace("/", "-")
                                        .Replace("\\", " ")
                                        .Replace("?", string.Empty)
                                        .Replace("*", string.Empty)
                                        .Replace("[", string.Empty)
                                        .Replace("]", string.Empty)
                                        .Replace(":", string.Empty);

            if (escapedSheetName.Length > MaximumSheetNameLength)
                escapedSheetName = escapedSheetName.Substring(0, MaximumSheetNameLength);

            return escapedSheetName;
        }

        public static void createCellText(
            IRow row,
            ICellStyle cellStyle,
            int colIdx, string txt)
        {
            ICell cell = row.CreateCell(colIdx);

            if (txt != null)
            {
                cell.SetCellValue(txt);
                cell.SetCellType(CellType.STRING);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;
            }
        }

        public static void createCellDecimal(
            IRow row,
            ICellStyle cellStyle,
            int colIdx, decimal? val)
        {
            ICell cell = row.CreateCell(colIdx);
            if (val != null)
            {
                cell.SetCellValue((double)val);
                cell.SetCellType(CellType.NUMERIC);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;
            }
        }

        public static void createCellDouble(HSSFWorkbook wb,
            IRow row,
            ICellStyle cellStyle,
            int colIdx, double? val)
        {
            ICell cell = row.CreateCell(colIdx);

            if (val != null)
            {
                cell.SetCellValue((double)val);
                cell.SetCellType(CellType.NUMERIC);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;
            }
        }

        public static void CreateSingleColHeader(HSSFWorkbook wb, ISheet sheet, int rows, int col, ICellStyle colStyleHeader, string cellValue)
        {
            IRow row = sheet.GetRow(rows) ?? sheet.CreateRow(rows);

            ICell cell = row.CreateCell(col);
            cell.CellStyle = colStyleHeader;
            cell.SetCellValue(cellValue);
        }
        #endregion

        #region Download Purchase Report With Custom Excel
        public void DownloadPackingIndicatorMaster(string MatNo, string DockCd, string PackingType)
        {
            byte[] result = null;
            string fileName = null;

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                List<PackingIndicatorMasterData> logList = new List<PackingIndicatorMasterData>();
                logList = db.Fetch<PackingIndicatorMasterData>("PackingIndicatorDownload", new object[] { MatNo, DockCd, PackingType });

                fileName = string.Format("Packing Report {0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
                result = GenerateDownloadFile(logList);
            }

            catch (Exception e)
            {

            }

            this.SendDataAsAttachment(fileName, result);
        }

        public byte[] GenerateDownloadFile(List<PackingIndicatorMasterData> PackingReport)
        {
            byte[] result = null;
            try
            {
                result = CreateFile(PackingReport);
            }
            finally
            { }

            return result;
        }

        private static readonly string SHEET_NAME_PACKING_REPORT = "PackingIndicator";
        public byte[] CreateFile(List<PackingIndicatorMasterData> PackingReport)
        {
            Dictionary<string, string> headers = null;
            ISheet sheet1 = null;
            HSSFWorkbook workbook = null;
            byte[] result;
            int startRow = 0;

            workbook = new HSSFWorkbook();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            short dateTimeFormat = dataFormat.GetFormat("dd/MM/yyyy HH:mm:ss");

            ICellStyle cellStyleDataLeft = createCellStyleDataLeft(workbook);
            ICellStyle cellStyleDataRight = createCellStyleDataRight(workbook);
            ICellStyle cellStyleDataCenter = createCellStyleDataCenter(workbook);
            ICellStyle cellStyleDataDouble = createCellStyleDataDouble(workbook);
            ICellStyle cellStyleHeader = createCellStyleColumnHeader(workbook);
            ICellStyle cellStyleTitle = createCellStyleColumnTitle(workbook);
            ICellStyle cellStyleTitleSecond = createCellStyleColumnTitleSecond(workbook);
            ICellStyle cellStyleDataTime = createCellStyleDataDate(workbook, dateTimeFormat);

            sheet1 = workbook.CreateSheet(EscapeSheetName(SHEET_NAME_PACKING_REPORT));
            sheet1.FitToPage = false;

            headers = new Dictionary<string, string>();

            WriteDetail(workbook, sheet1, startRow, cellStyleTitle, cellStyleTitleSecond, cellStyleHeader, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter, cellStyleDataDouble, PackingReport);

            using (MemoryStream buffer = new MemoryStream())
            {
                workbook.Write(buffer);
                result = buffer.GetBuffer();
            }

            workbook = null;
            return result;
        }

        public void WriteDetail(HSSFWorkbook wb, ISheet sheet1, int startRow, ICellStyle cellStyleTitle, ICellStyle cellStyleSecond, ICellStyle cellStyleHeader, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter, ICellStyle cellStyleDataDouble, List<PackingIndicatorMasterData> packingReport)
        {
            int rowIdx = startRow;
            int itemCount = 0;
            //Before Your Text, You must set column width if not column only have 8.43
            sheet1.SetColumnWidth(0, 17 * 256);
            sheet1.SetColumnWidth(1, 17 * 256);
            sheet1.SetColumnWidth(2, 17 * 256);
            sheet1.SetColumnWidth(3, 17 * 256);
            sheet1.SetColumnWidth(4, 17 * 256);
            sheet1.SetColumnWidth(5, 17 * 256);
            sheet1.SetColumnWidth(6, 17 * 256);

            CreateSingleColHeader(wb, sheet1, 0, 0, cellStyleHeader, "Material No");
            CreateSingleColHeader(wb, sheet1, 0, 1, cellStyleHeader, "Source Type");
            CreateSingleColHeader(wb, sheet1, 0, 2, cellStyleHeader, "Production Purpose");
            CreateSingleColHeader(wb, sheet1, 0, 3, cellStyleHeader, "Part Color Suffix");
            CreateSingleColHeader(wb, sheet1, 0, 4, cellStyleHeader, "Dock Code");
            CreateSingleColHeader(wb, sheet1, 0, 5, cellStyleHeader, "Packing Type");
            CreateSingleColHeader(wb, sheet1, 0, 6, cellStyleHeader, "Valid From");
            
            rowIdx = 1;
            foreach (PackingIndicatorMasterData data in packingReport)
            {
                WriteDetailSingleData(wb, cellStyleDataDouble, data, sheet1, ++itemCount, rowIdx++, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter);
            }

        }

        public void WriteDetailSingleData(HSSFWorkbook wb, ICellStyle cellStyleDouble, PackingIndicatorMasterData data, ISheet sheet1, int rowCount, int rowIndex, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter)
        {
            IRow row = sheet1.CreateRow(rowIndex);
            int col = 0;

            createCellText(row, cellStyleDataCenter, col++, data.MAT_NO);
            createCellText(row, cellStyleDataCenter, col++, data.SOURCE_TYPE);
            createCellText(row, cellStyleDataCenter, col++, data.PROD_PURPOSE_CD);
            createCellText(row, cellStyleDataCenter, col++, data.PART_COLOR_SFX);
            createCellText(row, cellStyleDataCenter, col++, data.DOCK_CD);
            createCellText(row, cellStyleDataCenter, col++, data.PACKING_TYPE_CD);
            createCellText(row, cellStyleDataCenter, col++, data.VALID_DT_FR);

        }
        #endregion

        #endregion

        #region uploadPacking
        [HttpPost]
        public void uploadPacking()
        {
            UploadControlExtension.GetUploadedFiles("PackingIndicatorMasterUpload", ValidationSettings, packingIndicator_FileUploadComplete);

        }

        protected void packingIndicator_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = ParamsHelper.Unique(Path.Combine(_UploadDirectory, e.UploadedFile.FileName));

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = Path.GetFileName(_FilePath);
            }
        }

        public ActionResult saveDataUpload(string filePath)
        {
            IDBContext db = DbContext;
            string message = "";
            string VALID_DT_TO = "31.12.9999";
            string location = "";
            string PROCESS_ID = "";
            string username = UID;
            int sts = 0;

            message = "Starting Upload Packing Indicator";
            location = "PackingIndicatorMaster.init";

            long pid = ExceptionalHelper.PutLog(message, UID, location, 0, "MPCS00001INF", "INF", MODULE_ID, FUNCTION_ID, 0);
            PROCESS_ID = pid.ToString(); 
                

            OleDbConnection excelConn = null;

            string tableFromTemp = "TB_T_PACKING_INDICATOR_TEMP";

            try
            {
                message = "Clear TB_T_PACKING_INDICATOR_TEMP";
                location = "PackingIndicatorMasterUpload.process";

                ExceptionalHelper.PutLog(message, UID, location, pid, "MPCS00001INF", "INF", MODULE_ID, FUNCTION_ID, 2);

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });
                filePath = Path.Combine(_UploadDirectory, filePath);
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string sheetName = "PackingIndicator";

                string OleCommand = @"SELECT " + PROCESS_ID + " " + " as PROCESS_ID, " + " '"
                                        + VALID_DT_TO + "' " + " as VALID_DT_TO, " + " " + " '"
                                    + UID + "' " + " as CREATED_BY, " + " " +
                                    @"IIf(IsNull(F1), Null, CStr([F1])) as Material_Number," +
                                    @"IIf(IsNull([F2]), Null, CStr([F2])) as Source_Type," +
                                    @"IIf(IsNull([F3]), Null, CStr([F3])) as Production_Purpose," +
                                    @"IIf(IsNull([F4]), Null, CStr([F4])) as Part_Color_Suffix," +
                                    @"IIf(IsNull([F5]), Null, CStr([F5])) as Dock_Code," +
                                    @"IIf(IsNull([F6]), Null, CStr([F6])) as Packing_Type," +
                                    @"IIf(IsNull([F7]), Null, Cstr([F7])) as Valid_From ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);
                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                
                excelConn.Close();

                dtPatterns = DeleteEmptyRows(dtPatterns);
                dtPatterns.Columns.Add("SEQ", typeof(Int32));
                

                ds.Tables.Add(dtPatterns);

                DataRow rowDel = ds.Tables[0].Rows[0];
                ds.Tables[0].Rows.Remove(rowDel);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["SEQ"] = i + 1;
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("Dock_Code", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("Valid_From", "VALID_DT_FR");
                    bulkCopy.ColumnMappings.Add("VALID_DT_TO", "VALID_DT_TO");
                    bulkCopy.ColumnMappings.Add("Packing_Type", "PACKING_TYPE_CD");
                    bulkCopy.ColumnMappings.Add("Part_Color_Suffix", "PART_COLOR_SFX");
                    bulkCopy.ColumnMappings.Add("Material_Number", "MAT_NO");
                    bulkCopy.ColumnMappings.Add("Production_Purpose", "PROD_PURPOSE_CD");
                    bulkCopy.ColumnMappings.Add("Source_Type", "SOURCE_TYPE");
                    bulkCopy.ColumnMappings.Add("CREATED_BY", "CREATED_BY");
                    bulkCopy.ColumnMappings.Add("SEQ", "SEQ");


                    // Write from the source to the destination.
                    // Check if the data set is not null and tables count > 0 etc

                    try
                    {
                        message = "Insert Data to TB_T_PACKING_INDICATOR_TEMP";
                        location = "PackingIndicatorMasterUpload.process";
                        ExceptionalHelper.PutLog(message,
                                UID,
                                location,
                                pid,
                                "MPCS00002ERR",
                                "INF",
                                MODULE_ID,
                                FUNCTION_ID,
                                2);
                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "PackingIndicatorMasterUpload.finish";

                        ExceptionalHelper.PutLog(
                                message,
                                UID,
                                location,
                                pid,
                                "MSPT00003INF",
                                "INF",
                                MODULE_ID,
                                FUNCTION_ID,
                                2
                            );

                        throw new Exception(ex.Message);
                    }
                }

                string validate = db.SingleOrDefault<string>("PackingIndicatorMasterValidate", new object[] { PROCESS_ID });

                if (string.Compare(validate,"success") != 0)
                {
                    TempData["IsValid"] = "false;"+ PROCESS_ID;
                    sts = 6;
                    
                    string errfilename = "PackingIndicatorErr_" + pid + ".xls";
                    string errfilePath = Path.Combine(_UploadDirectory, errfilename);
                    /// write error message to output file 
                    List<PackingIndicatorMasterData> li = db.Fetch<PackingIndicatorMasterData>("GetPackingIndicatorUploadErr", new object[] { PROCESS_ID });
                    Xlimex.Write<PackingIndicatorMasterData>(filePath, errfilePath, ";;;;;;;ERR_MSG", "SEQ", li, 0);
                    message = validate + "|" + PROCESS_ID;
                }
                else
                {
                    TempData["IsValid"] = "true;" + PROCESS_ID;
                    sts = 1;
                    message = validate;
                }

                location = "PackingIndicatorMasterUpload.finish";

                ExceptionalHelper.PutLog(
                        message,
                        UID,
                        location,
                        pid,
                        "MSPT00003INF",
                        "INF",
                        MODULE_ID,
                        FUNCTION_ID,
                        sts);

            }
            catch (Exception e)
            {
                TempData["IsValid"] = "false;" + PROCESS_ID + ";" + e.Message;

                message = "Error : " + e.Message;
                location = "MasterDockUpload.finish";

                ExceptionalHelper.PutLog(
                        message,
                        UID,
                        location,
                        pid,
                        "MSPT00003INF",
                        "INF",
                        MODULE_ID,
                        FUNCTION_ID,
                        2);
                throw new Exception(e.Message);
            }
         
            return Json(message);

        }

        public DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            try
            {
                List<DataRow> drList = new List<DataRow>();
                foreach (DataRow dr in formattedTable.Rows)
                {
                    int count = dr.ItemArray.Length;
                    int nullcounter = 0;
                    for (int i = 0; i < dr.ItemArray.Length; i++)
                    {
                        if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])) || dr.ItemArray[i].ToString().Length <= 0)
                        {
                            nullcounter++;
                        }
                    }

                    if (nullcounter == count - 2)
                    {
                        drList.Add(dr);
                    }
                }

                for (int i = 0; i < drList.Count; i++)
                {
                    formattedTable.Rows.Remove(drList[i]);
                }
                formattedTable.AcceptChanges();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return formattedTable;
        }
        #endregion

        #region configuration Upload
        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(_uploadDirectory))
                {
                    _uploadDirectory = Path.Combine(Sing.Me.AppData, "PackingIndicator");
                    if (!Directory.Exists(_uploadDirectory)) Directory.CreateDirectory(_uploadDirectory);
                }
                return _uploadDirectory;
            }
        }
        #endregion
    }
}