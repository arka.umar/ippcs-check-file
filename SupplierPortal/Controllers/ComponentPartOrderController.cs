﻿using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.Supplier;
using System;
using Portal.Models.ComponentPartOrder;

namespace Portal.Controllers
{
    public class ComponentPartOrderController : BaseController
    {
        public ComponentPartOrderController()
            : base("Component Part Order Creation")
        {
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            ComponentPartOrderModel model = new ComponentPartOrderModel();
            for (int i = 0; i < 21; i++)
            {
                model.CPOHeaders.Add(new ComponentPartOrderHeader()
                {
                     PRNo = "PR"+(i+1),
                     ItemPR = "Item PR"+(i+1),
                     PartNo = "Material No"+(i+1),
                     PartDesc = "Material Description"+(i+1),
                     Qty = i+1,
                     RemainQty = i+1,
                     UOM = "UOM"+(i+1),
                     DeliveredDate = DateTime.Now,
                     PlantCode = "Plant Code"+(i+1),
                     sLockCode = "sLock Code"+(i+1),
                     SupplierCode = "Supplier Code"+(i+1),
                     SupplierSearchTerm = "Search Term"+(i+1),
                     Price = 10000000+i,
                     ProdPurpose = "Purpose",
                     SourceType = "Source Type",
                     PackingType = "Packing Type",
                     SuffixColor = "Suffix Color",
                     PRCreationDate = DateTime.Now,
                     Requistioner = "Requisitioner",
                     POItemNo = "PO Item No"+(i+1),
                     POItemCreationDate = DateTime.Now,
                     PurchaseOrder = "PurchaseOrder"+(i+1),
                     PONo = "PO No"+(i+1),
                     DeletionFlag = true,

                     PRType= "PT Type"+(i+1),
                     PlanDeliveryDate = "",
                     RcvPlantCode ="",
                     DockCode ="",
                     Status ="",
                     Notice =""
                });
                for (int h = 0; h < ((1 + i)* 2); h++)
                {
                    model.CPODetails.Add(new ComponentPartOrderDetail()
                    {
                        PRNo = "PR" + (i + 1),
                        ItemNo = "XP-" + h * 2,
                        OrderQty = h * 3,
                        PackingType = (i+1) * 2 + "VK",
                        SpecialProcurementType = "XYZ ",
                        Suffix = "Lorem ipsum dolor sit amet"
                    });
                }
            }

          

            Model.AddModel(model);
        }

        public ActionResult PartialCPOGrid()
        {
            return PartialView("CPODataGrid", Model);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("HeaderComponentPartOrder", Model);
        }
         
        public ActionResult CPOGridDetail(string PRNo)
        {
            ViewData["ID"] = PRNo;
            ComponentPartOrderModel model = Model.GetModel<ComponentPartOrderModel>();
            var CPODetail = model.CPODetails.Where(p => p.PRNo == PRNo).ToList<ComponentPartOrderDetail>();
            Model.AddModel(model);
            return PartialView("CPODataGridDetail", Model);
        }
    }
}
