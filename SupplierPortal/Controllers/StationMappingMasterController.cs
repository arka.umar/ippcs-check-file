﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Master;
using Portal.ExcelUpload;
using DevExpress.Web.ASPxUploadControl;
using Toyota.Common.Web.Excel;
using System.IO;
using Toyota.Common.Web.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;

using Portal.ExcelUpload;
using Portal.Models;
using Portal.Models.Globals;

using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.MVC;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;


using Portal.Models.StationMappingMaster;

namespace Portal.Controllers
{
    public class StationMappingMasterController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        public StationMappingMasterController()
            : base("Station Mapping Master")
        {
            PageLayout.UseMessageBoard = true;
        }

        

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            StationMappingModel mdl = new StationMappingModel();
            PageLayout.UseSlidingBottomPane = true;
            mdl.StationMappingMaster = GetStationMapping("");
            Model.AddModel(mdl);
        }

        public List<StationMappingMaster> GetStationMapping(string DoorCode = "", string DockCode = "", string PhysicalDock = "", string PhysicalStation = "", string UsedFlag = "")
        {
            List<StationMappingMaster> stationMappingMasterData = new List<StationMappingMaster>();
            IDBContext db = DbContext;
            try
            {
                stationMappingMasterData = db.Fetch<StationMappingMaster>("GetStationMapping", new object[] { DoorCode, DockCode, PhysicalDock, PhysicalStation, UsedFlag });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return stationMappingMasterData;
            
        }

        StationMappingModel _stationMappingModel = null;
        private StationMappingModel _StationMappingModel
        {
            get
            {
                if (_stationMappingModel == null)
                    _stationMappingModel = new StationMappingModel();

                return _stationMappingModel;
            }
            set
            {
                _stationMappingModel = value;
            }
        }

        public ActionResult StationMappingMasterData()
        {

            _StationMappingModel.StationMappingMaster = GetStationMapping(
                Request.Params["cpdoorcd"],
                Request.Params["cpdockcd"],
                Request.Params["cpphysicaldock"],
                Request.Params["cpphysicalstation"],
                Request.Params["cpusedflag"]);
             
            Model.AddModel(_StationMappingModel);
         
            return PartialView("StationMappingDetail", Model);
        }

        public ContentResult DeleteStation(string GridId)
        {   
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string DoorCode = "";
            string DockCode = "";
            string PhysicalDock = "";
            string PhysicalStation = "";
            string UsageFlag = "";
            //string CHANGED_DT = "";


            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);

                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                int approve = 0;
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                  if (ParamUpdate[i] != "")
                    {
                        //SPLIT ID BY OBJECT WITH DELIMETER "_"
                        char[] SplitId = { '_' };
                        string[] ParamId = ParamUpdate[i].Split(SplitId);

                        DoorCode = ParamId[0];
                        DockCode = ParamId[1];
                        PhysicalDock = ParamId[2];
                        PhysicalStation = ParamId[3];
                        UsageFlag = ParamId[4];
                        //CHANGED_DT= ParamId[5];
                        ////systemval = ParamId[2];

                        approve = db.SingleOrDefault<int>("DeleteStationMappingCheck", new object[] { DoorCode, DockCode, PhysicalDock, PhysicalStation });

                        if (approve > 0)
                           throw new Exception("Cannot Delete Used Data");

                        else
                        {

                            db.ExecuteScalar<string>("DeleteStationMappingMaster", new object[] { DoorCode, DockCode, PhysicalDock, PhysicalStation });
                            //Result += "\n";
                        }
                    }
                }
                return Content("Succes To Deleted Data");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally { db.Close(); }
        }

        public ActionResult AddNewPart(string status, string DoorCode, string DockCode, string PhysicalDock, string PhysicalStation, string UsedFlag)
        {
            string ResultQuery = "Data Has Been Saved";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                if (status == "new")
                {
               
                    int delete = 0;
            
                    delete = db.SingleOrDefault<int>("InsertStationMappingCheck", new object[] 
                {
                   DoorCode
                  
                });
                  if (delete != 0)
                    throw new Exception("Data Already Exist");
                  else{
                    db.Execute("InsertStationMapping", new object[] 
                {
                   DoorCode,
                   DockCode,
                   PhysicalDock,
                   PhysicalStation,
                   ((UsedFlag=="Yes")?1:0),
                   AuthorizedUser.Username,
                   DateTime.Now
                });
                  }   //TempData["GridName"] = "PartGridView";
                }
                if (status == "update")
                {
                    db.Execute("UpdateStationMapping", new object[] 
                {
                   DoorCode,
                   DockCode,
                   PhysicalDock,
                   PhysicalStation,
                   ((UsedFlag=="Yes")?1:0),
                   AuthorizedUser.Username,
                   DateTime.Now
                });
                }

            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            db.Close();
            return Content(ResultQuery);
        }

        public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/StationMappingMaster/UploadTemp/StationMappingMasterData.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "StationMappingMasterData.xls");
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        #region UPLOAD CONFIRMATION - PROPERTIES
        private string _ErrorMessage = "";

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/StationMappingMaster/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private long _processId = 0;
        private long ProcessId
        {
            get
            {
                if (_processId == 0)
                {
                    IDBContext db = DbContext;

                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); }

                    return _processId;
                }

                return _processId;
            }
        }

        private int errorCount = 0;
        #endregion

        [HttpPost]
        public void UplDeliveryNonTLMSOrder_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("UplDeliveryNonTLMSOrder", ValidationSettings, UplLPOPConfirmation_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls", ".xlsx" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void UplLPOPConfirmation_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        [HttpPost]
        public ActionResult HeaderDeliveryNonTLMS_Upload(string filePath)
        {
            ProcessAllDeliveryNonTLMSUpload(filePath);

            String status = ((_ErrorMessage == "") ? "SUCCESS|Upload file success." : _ErrorMessage);

            return Json(
                new
                {
                    Status = status
                });
        }

        private StationMappingModel ProcessAllDeliveryNonTLMSUpload(string filePath)
        {
            string created_by = AuthorizedUser.Username;
            DateTime created_dt = DateTime.Now;
            _ErrorMessage = "";

            StationMappingModel _UploadedDeliveryNonTLMSModel = new StationMappingModel();

            //---=== Table destination properties
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "StationMappingMasterData";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "5";

            LogProvider logProvider = new LogProvider("102", "121", AuthorizedUser);
            ILogSession sessionNTLMS = logProvider.CreateSession();
            OleDbConnection excelConn = null;
            IDBContext db = DbContext;

            try
            {
                //sessionNTLMS.Write("MPCS00001INF", new object[] { "Process Upload has been started..!!!" });

                //---=== Clear temporary upload table.
                StationMappingModel model = new StationMappingModel();
                model.TempGridContent = model.TempGridContent.ToList<StationMappingMasterTemp>();
                Model.AddModel(model);

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                //fixing by: FID.Goldy
                // fixing date : 25 November 2013
                excelCommand = new OleDbCommand(@"select * from [" + _TableDestination.sheetName + "$] where DOOR_CD_TLMS <> '' ", excelConn);
                //end of fixing by FID.Goldy


                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                string errorMessage = "";
               
                int lastLine = 0;
                string DOOR_CD_TLMS = "";
                string DOCK_CD = "";
                string PHYSICAL_DOCK = "";
                string PHYSICAL_STATION="";
                string USED_FLAG = "";
                int nLog = 0;
                int n;


                for (int i = 0; i < dtPatterns.Rows.Count; i++)
                {

                    bool isNumeric2 = int.TryParse(Convert.ToString(ds.Tables[0].Rows[i]["USED_FLAG"]).Trim(), out n);
                    errorMessage = "";
                    bool isNumeric1 = int.TryParse(Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_STATION"]).Trim(), out n);
                    errorMessage = "";

                    #region Validate Data

                    //---=== Check DOOR_CD_TLMS
                    if (Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim() == "")
                    {
                        errorMessage = "Wrong Data at row " + (i + 1) + " : Door Code can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim().Length > 5)
                    {
                        errorMessage = "Wrong Data at row " + (i + 1) + " : Door Code length maximum 5 characters";
                    }

                    //---=== Check DOCK_CD
                    if (Convert.ToString(ds.Tables[0].Rows[i]["DOCK_CD"]).Trim() == "")
                    {
                        errorMessage = "Wrong Data at row " + (i + 1) + " : Dock Code can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["DOCK_CD"]).Trim().Length > 3)
                    {
                        errorMessage = "Wrong Data at row " + (i + 1) + " : Dock Code length maximum 3 characters";
                    }

                    //---=== Check PHYSICAL_DOCK
                    if (Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_DOCK"]).Trim() == "")
                    {
                        errorMessage = "Wrong Data at row " + (i + 1) + " : Physical Dock can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_DOCK"]).Trim().Length > 3)
                    {
                        errorMessage = "Wrong Data at row " + (i + 1) + " : Physical Dock length maximum 3 characters";
                    }

                    //---=== Check Physical Station
                    if (Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_STATION"]).Trim() == "")
                    {
                        errorMessage = " Wrong Data at row " + (i + 1) + " : Physical Station can't be empty";
                    }
                    //else if (int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim()) <= 0)
                    else if (!isNumeric1)
                    {

                        errorMessage = " Wrong Data at row " + (i + 1) + " : Physical Station must numeric and greater than 0";
                    }

                    //---=== Check Used Flag
                    if (Convert.ToString(ds.Tables[0].Rows[i]["USED_FLAG"]).Trim() == "")
                    {
                        errorMessage = " Wrong Data at row " + (i + 1) + " : Used Flag can't be empty";
                    }
                    //else if (int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim()) <= 0)
                    else if (!isNumeric2)
                    {

                        errorMessage = " Wrong Data at row " + (i + 1) + " : Used Flag must numeric and greater than 0";
                    }

                    

                    #endregion

                    if (errorMessage != "" )
                    {
                        errorCount = errorCount + 1;
                        lastLine = i;
                        if (_ErrorMessage == "")
                        {
                            _ErrorMessage = "ERROR|"+errorMessage;
                        }
                    }

                    #region Insert to Model Table

                    
                    #endregion

                }

                #endregion

                #region Insert Into Table
                if (errorCount == 0)
                {
                    for (int i = 0; i < dtPatterns.Rows.Count; i++)
                    {
                        if (getNonTLMSExist(
                                Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim()) == "0")
                        {
                            db.Execute("InsertStationMapping", new object[] { 
                                Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["DOCK_CD"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_DOCK"]).Trim(),
                                int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_STATION"]).Trim()),
                                int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["USED_FLAG"]).Trim()),
                                 created_by, created_dt
                            });
                        }
                        else
                        {
                            //---=== Rollback last insert
                            _ErrorMessage = "ERROR|Cannot duplicate entry data.";
                            //---=== End rollback last insert
                        }
                    }
                }
                #endregion
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                //sessionNTLMS.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });
                _ErrorMessage = "ERROR|" + exc.Message.ToString();
            }
            finally
            {
                //sessionNTLMS.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                //sessionNTLMS.Commit();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }
            db.Close();
            return _UploadedDeliveryNonTLMSModel;
        }

        private string getNonTLMSExist(string DOOR_CD_TLMS)
        {
            int result = 0;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (DOOR_CD_TLMS != "" )
            {
                DoorCodeCheckExists QueryLog = db.SingleOrDefault<DoorCodeCheckExists>("InsertStationMappingCheck", new object[] { 
                    DOOR_CD_TLMS
                });
                result = QueryLog.resultCount;
            }
            db.Close();
            return result.ToString();
        }


        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                if (extractString.Length >= 3)
                    result = extractString[2] + "/" + extractString[1] + "/" + extractString[0];
            }

            return result;
        }

        private string formatingShortDateTime(string dateString, string type)
        {
            string result = "";
            string[] extract = new string[2];
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extract = dateString.Split(' ');
                if (extract.Length > 0)
                {
                    extractString = extract[0].Split('.');
                    if (extractString.Length >= 3)
                        result = extractString[2] + "/" + extractString[1] + "/" + extractString[0] + " " + extract[1] + ((type == "S") ? "" : ":00");
                }
            }

            return result;
        }

        private string formattingFullToShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];
            //Sat May 04 2013 00:00:00 GMT+0700 (SE Asia Standard Time)
            extractString = dateString.Split(' ');
            result = extractString[3] + "-" + getMonthNumber(extractString[1]) + "-" + extractString[2];

            return result;
        }

        private string getMonthNumber(string month)
        {
            string result = "";

            switch (month)
            {
                case "January":
                case "Jan": result = "01"; break;
                case "February":
                case "Feb": result = "02"; break;
                case "March":
                case "Mar": result = "03"; break;
                case "April":
                case "Apr": result = "04"; break;
                case "May": result = "05"; break;
                case "June":
                case "Jun": result = "06"; break;
                case "July":
                case "Jul": result = "07"; break;
                case "August":
                case "Aug": result = "08"; break;
                case "September":
                case "Sep": result = "09"; break;
                case "October":
                case "Oct": result = "10"; break;
                case "November":
                case "Nov": result = "11"; break;
                case "December":
                case "Dec": result = "12"; break;
                default: result = "01"; break;
            }

            return result;
        }

        protected bool CheckDate(String date)
        {

            try
            {

                DateTime dt = DateTime.Parse(date);

                return true;
            }

            catch
            {

                return false;

            }

        }


        public ActionResult UploadStationMapping(string filePath)
        {
            ProcessAllStationMappingUpload(filePath);

            String status = ((_ErrorMessage == "") ? "SUCCESS|Upload file success." : _ErrorMessage);

            return Json(
                new
                {
                    Status = status
                });
        }

        private StationMappingModel ProcessAllStationMappingUpload(string filePath)
        {
            string created_by = AuthorizedUser.Username;
            DateTime created_dt = DateTime.Now;
            _ErrorMessage = "";

            StationMappingModel _UploadedStationMappingModel = new StationMappingModel();

            //---=== Table destination properties
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "StationMappingMasterData";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "5";

            LogProvider logProvider = new LogProvider("102", "121", AuthorizedUser);
            ILogSession sessionNTLMS = logProvider.CreateSession();
            OleDbConnection excelConn = null;
            IDBContext db = DbContext;

            try
            {
                //sessionNTLMS.Write("MPCS00001INF", new object[] { "Process Upload has been started..!!!" });

                //---=== Clear temporary upload table.
                StationMappingModel model = new StationMappingModel();
                model.TempGridContent = model.TempGridContent.ToList<StationMappingMasterTemp>();
                Model.AddModel(model);

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();
                excelCommand = new OleDbCommand(@"select * from [" + _TableDestination.sheetName + "$] where Route <> '' ", excelConn);
                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                string errorMessage = "";
                int lastLine = 0;
                string DoorCode = "";
                string DockCode = "";
                string PhysicalDock = "";
                string PhysicalStation = "";
                string UsedFlag = "";
                int nLog = 0;
                int n;


                for (int i = 0; i < dtPatterns.Rows.Count; i++)
                {

                    bool isNumeric = int.TryParse(Convert.ToString(ds.Tables[0].Rows[i]["USED_FLAG"]).Trim(), out n);
                    errorMessage = "";
                    isNumeric = int.TryParse(Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_STATION"]).Trim(), out n);
                    errorMessage = "";

                    #region Validate Data

                    //---=== Check Route
                    if (Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim() == "")
                    {
                        errorMessage = "Data " + (i + 1) + " : Door Code can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim().Length > 5)
                    {
                        errorMessage = "Data " + (i + 1) + " : Door Code length maximum 5 characters";
                    }

                    if (Convert.ToString(ds.Tables[0].Rows[i]["DOCK_CD"]).Trim() == "")
                    {
                        errorMessage = "Data " + (i + 1) + " : Dock Code can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["DOCK_CD"]).Trim().Length > 3)
                    {
                        errorMessage = "Data " + (i + 1) + " : Dock Code length maximum 3 characters";
                    }

                    if (Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_DOCK"]).Trim() == "")
                    {
                        errorMessage = "Data " + (i + 1) + " : Physical Dock can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_DOCK"]).Trim().Length > 3)
                    {
                        errorMessage = "Data " + (i + 1) + " : Physical Dock length maximum 3 characters";
                    }

                    //---=== Check Trip / Day
                    if (Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_STATION"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : Physical Station can't be empty";
                    }
                    //else if (int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim()) <= 0)
                    else if (!isNumeric)
                    {

                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : Physical Station must numeric and greater than 0";
                    }

                    //---=== Check Trip / Day
                    if (Convert.ToString(ds.Tables[0].Rows[i]["USED_FLAG"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : Used Flag can't be empty";
                    }
                    //else if (int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim()) <= 0)
                    else if (!isNumeric)
                    {

                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : Used Flag must numeric and greater than 0";
                    }

                    

                    #endregion

                    if (errorMessage != "")
                    {
                        errorCount = errorCount + 1;
                        lastLine = i;
                        if (_ErrorMessage == "")
                        {
                            _ErrorMessage = "ERROR|" + errorMessage;
                        }
                    }

                    
                }

                #endregion

                #region Insert Into Table
                if (errorCount == 0)
                {
                    for (int i = 0; i < dtPatterns.Rows.Count; i++)
                    {
                        if (getDoorCodeExist(
                                Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim()) == "0")
                        {
                            db.Execute("InsertStationMapping", new object[] { 
                                Convert.ToString(ds.Tables[0].Rows[i]["DOOR_CD_TLMS"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["DOCK_CD"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_DOCK"]).Trim(),
                                int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["PHYSICAL_STATION"]).Trim()),
                                int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["USED_FLAG"]).Trim()),
                                created_by, 
                                created_dt
                            });
                        }
                        else
                        {
                            //---=== Rollback last insert
                            _ErrorMessage = "ERROR|Cannot duplicate entry data.";
                            //---=== End rollback last insert
                        }
                    }
                }
                #endregion
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                //sessionNTLMS.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });
                _ErrorMessage = "ERROR|" + exc.ToString();
            }
            finally
            {
                //sessionNTLMS.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                //sessionNTLMS.Commit();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }
            db.Close();
            return _UploadedStationMappingModel;
        }

        private string getDoorCodeExist(string DoorCode)
        {
            int result = 0;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (DoorCode != "")
            {
                DoorCodeCheckExists QueryLog = db.SingleOrDefault<DoorCodeCheckExists>("InsertStationMappingCheck", new object[] { 
                    DoorCode
                });
                result = QueryLog.resultCount;
            }
            db.Close();
            return result.ToString();
        }

        public ActionResult DownloadCallback()
        {
            return null;
        }

}
}
