﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Portal.Models.SourceListMaintenance;
using Portal.Models.Globals;
using System.IO;
using Portal.Models;

namespace Portal.Controllers
{
    public class SourceListMaintenanceController : BaseController
    {
        //
        // GET: /SourceListMaintenance/
        public string module = "7";
        public string function = "71010";
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public SourceListMaintenanceController()
            : base("P1.1.2 Source List Maintenance Screen")
        {
            PageLayout.UseMessageBoard = true;
        }


        List<SupplierICS> _invoiceInquirySupplierModel = null;
        private List<SupplierICS> _InvoiceInquirySupplierModel
        {
            get
            {
                if (_invoiceInquirySupplierModel == null)
                    _invoiceInquirySupplierModel = new List<SupplierICS>();

                return _invoiceInquirySupplierModel;
            }
            set
            {
                _invoiceInquirySupplierModel = value;
            }
        }



        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            SourceListMaintenanceModel slmm = new SourceListMaintenanceModel();
            List<SourceListMaintenance> slm = new List<SourceListMaintenance>();
            PageLayout.UseSlidingBottomPane = true;


            ViewData["MatNoGridLookup"] = Dbutil.GetList<SourceListMaintenance>("GetAllMatNoICS");
            ViewData["SourceTypeCmb"] = Dbutil.GetList<SourceListMaintenance>("GetAllSourceTypeICS");
            ViewData["SourceTypeHead"] = Dbutil.GetList<SourceListMaintenance>("GetAllSourceTypeHeadICS");
            ViewData["ProdPurposeCmb"] = Dbutil.GetList<SourceListMaintenance>("GetAllProdPurposeICS");
            ViewData["ProdPurposeHead"] = Dbutil.GetList<SourceListMaintenance>("GetAllProdPurposeHeadICS");
            ViewData["BuyerCDCmb"] = Dbutil.GetList<SourceListMaintenance>("GetAllBuyerCD");
            List<SupplierICS> listSupp = Dbutil.GetList<SupplierICS>("GetAllSupplierICS");
            ViewData["SupplierCdGridLookup"] = listSupp;
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(listSupp, "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");
            ViewData["SupplierGridLookup"] = suppliers;
            ViewData["DateFrom"] = DateTime.Now.AddMonths(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");
            slmm.sourceList = slm;
            Model.AddModel(slmm);

        }
        
        private List<SourceListMaintenance> GetAllSourceListMaintenance(string MatNo, string SourceType, string ProdPurpose, string ColorSuffix, string MatDesc, string SuppCD, string SuppName, string BuyerCD, string Condition)
        {
            List<SourceListMaintenance> Source_List = new List<SourceListMaintenance>();
            IDBContext db = DbContext;

            Source_List = db.Fetch<SourceListMaintenance>("SourceListMaintenance",
            new object[] { 
                MatNo == null ? "" : MatNo, 
                SourceType == null ? "" : SourceType, 
                ProdPurpose == null ? "" : ProdPurpose, 
                ColorSuffix == null ? "" : ColorSuffix, 
                MatDesc == null ? "" : MatDesc, 
                SuppCD == null ? "" : SuppCD, 
                SuppName == null ? "" : SuppName, 
                BuyerCD == null ? "" : BuyerCD,
                Condition == null ? "" : Condition
            });
            db.Close();

            return Source_List;
        }
        public ActionResult HeaderSourceListMaintenance()
        {

            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(Dbutil.GetList<SupplierICS>("GetAllSupplierICS"), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");

            _InvoiceInquirySupplierModel = suppliers;

            Model.AddModel(_InvoiceInquirySupplierModel);


            return PartialView("HeaderSourceListMaintenance", Model);
        }

        public ActionResult SourceListMaintenancePartial()
        {
            string MatNo = Request.Params["MatNo"];
            string SourceType = Request.Params["SourceType"];
            string ProdPurpose = Request.Params["ProdPurpose"];
            string ColorSuffix = Request.Params["ColorSuffix"];
            string MatDesc = Request.Params["MatDesc"];
            string SuppCD = Request.Params["SuppCD"];
            string SuppName = Request.Params["SuppName"];
            string BuyerCD = Request.Params["BuyerCD"];
            string condition = Request.Params["Condition"];

            SourceListMaintenanceModel model = Model.GetModel<SourceListMaintenanceModel>();

            model.sourceList = GetAllSourceListMaintenance(MatNo, SourceType, ProdPurpose, ColorSuffix, MatDesc, SuppCD, SuppName, BuyerCD, condition);

            return PartialView("SourceListMaintenancePartial", Model);

        }

        public ActionResult PartialMatNoGridLookup()
        {
            // Required model in partial page : for OIPMaterialNo GridLookup
            ViewData["MatNoGridLookup"] = Dbutil.GetList<SourceListMaintenance>("GetAllMatNoICS");

            TempData["GridName"] = "OIPMaterialNo";

            return PartialView("GridLookup/PartialGrid", ViewData["MatNoGridLookup"]);
        }

        public ActionResult LookUpSupplierCode()
        {
            // Required model in partial page : for OIPSupplierOption GridLookup

            List<SupplierICS> AddEditsuppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(Dbutil.GetList<SupplierICS>("GetAllSupplierICS"), "suppLookUp", "BuyingPriceMaintenance", "AddEditSupplierCd");

            TempData["GridName"] = "AddEditSupplierCd";
            _InvoiceInquirySupplierModel = AddEditsuppliers;


            return PartialView("GridLookup/PartialGrid", _InvoiceInquirySupplierModel);
        }

        public ActionResult LookUpSupplierCodeHeader()
        {
            // Required model in partial page : for OIPSupplierOption GridLookup

            List<SupplierICS> AddEditsuppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(Dbutil.GetList<SupplierICS>("GetAllSupplierICS"), "suppLookUp", "BuyingPriceMaintenance", "AddEditSupplierCd");

            TempData["GridName"] = "OIPSuppCD";
            _InvoiceInquirySupplierModel = AddEditsuppliers;


            return PartialView("GridLookup/PartialGrid", _InvoiceInquirySupplierModel);
        }

        public ActionResult PartialMatNoGridLookupAdd()
        {
            // Required model in partial page : for OIPMaterialNo GridLookup
            ViewData["MatNoGridLookup"] = Dbutil.GetList<SourceListMaintenance>("GetAllMatNoICS");

            TempData["GridName"] = "AddNewMaterialNo";


            List<SourceListMaintenance> matDesc = Dbutil.GetList<SourceListMaintenance>("GetAllMatNoICS");
            return PartialView("GridLookup/PartialGrid", matDesc);
        }

        public ActionResult PartialSourceTypeCmb()
        {
            // Required model in partial page : for OIPSourceType ComboBox
            ViewData["SourceTypeCmb"] = Dbutil.GetList<SourceListMaintenance>("GetAllSourceTypeICS");

            TempData["GridName"] = "OIPSourceType";


            return PartialView("GridLookup/PartialGrid", ViewData["SourceTypeCmb"]);
        }

        public ActionResult PartialProdPurposeCmb()
        {
            // Required model in partial page : for OIPSourceType ComboBox
            ViewData["ProdPurposeCmb"] = Dbutil.GetList<SourceListMaintenance>("GetAllProdPurposeICS");

            TempData["GridName"] = "OIPProdPurpose";


            return PartialView("GridLookup/PartialGrid", ViewData["ProdPurposeCmb"]);
        }

        public ActionResult PartialBuyerCDCmb()
        {
            // Required model in partial page : for OIPSourceType ComboBox
            ViewData["BuyerCDCmb"] = Dbutil.GetList<SourceListMaintenance>("GetAllBuyerCD");

            TempData["GridName"] = "OIPBuyerCD";
            return PartialView("GridLookup/PartialGrid", ViewData["BuyerCDCmb"]);
        }


        public ContentResult DeleteSourceListMaintenance(string GridId)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                char[] rowsep = { ';' };
                char[] colsep = { '_' };
                string[] ParamUpdate = GridId.Split(rowsep);
                List<SourceListMaintenance> delist = new List<SourceListMaintenance>();

                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                int refers = 0;
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    if (ParamUpdate[i] != "")
                    {
                        //SPLIT ID BY OBJECT WITH DELIMETER "_"

                        string[] p = ParamUpdate[i].Split(colsep);
                        if (p.Length > 6)
                            delist.Add(new SourceListMaintenance()
                            {
                                MAT_NO = p[0],
                                SOURCE_TYPE = p[1],
                                PROD_PURPOSE_CD = p[2],
                                PART_COLOR_SFX = p[3],
                                SUPP_CD = p[4],
                                VALID_DT_FR = p[5],
                                DELETION_FLAG = p[6]
                            });
                    }
                }
                if (delist.Count < 1)
                {
                    throw new Exception("NO record to delete, or wrong data");
                }
                foreach (var d in delist)
                {
                    refers = db.SingleOrDefault<int>("DeleteCheckSourceListMaintenance", new object[] { 
                      d.MAT_NO, d.SOURCE_TYPE, d.PROD_PURPOSE_CD, d.PART_COLOR_SFX, d.SUPP_CD, d.VALID_DT_FR, d.DELETION_FLAG
                    });
                    if (refers < 0)
                    {
                        throw new Exception("Source List not found or deleted");
                    }
                    else if (refers != 0)
                    {
                        throw new Exception("Source list already used");
                    }
                }
                foreach (var d in delist)
                    db.ExecuteScalar<string>("DeleteSourceListMaintenance", new object[] { 
                      d.MAT_NO, d.SOURCE_TYPE, d.PROD_PURPOSE_CD, d.PART_COLOR_SFX, d.SUPP_CD, d.VALID_DT_FR, d.DELETION_FLAG
                    });

                return Content("Success|Deletion process is completed successfully");
            }
            catch (Exception ex)
            {
                return Content("Error|" + ex.Message);
            }
            finally { db.Close(); }
        }

        //FID.Ridwan - 20210315 -> Fix mapping as request istd.yanes
        public ActionResult Download(string MatNo, string SourceType, string ProdPurpose, string ColorSuffix, string MatDesc, string SuppCD, string SuppName, string BuyerCD, string condition="1")
        {
            List<SourceListMaintenance> li = GetAllSourceListMaintenance(MatNo, SourceType, ProdPurpose, ColorSuffix, MatDesc, SuppCD, SuppName, BuyerCD, condition);
            string ifile = Server.MapPath("~/Views/SourceListMaintenance/UploadTemp/SourceListMaintenanceDownload.xls");
            string ofile = ParamsHelper.Unique(Path.Combine(_UploadDirectory, Path.GetFileName(ifile)));
            Xlimex.Write<SourceListMaintenance>(ifile, ofile, "MAT_NO;MAT_DESC;SOURCE_TYPE;SOURCE_NAMEE;PROD_PURPOSE_CD;PROD_PURPOSE_NAME;PART_COLOR_SFX;VALID_DT_FR;VALID_DT_TO;SUPP_CD;SUPP_NAME", "SEQ", li);
            return File(StreamFile(ofile), "application/vnd.ms-excel", "SourceListDownload_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");
        }

        public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/SourceListMaintenance/UploadTemp/SourceListMaintenanceData.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "SourceListMaintenanceData.xls");
        }

        public FileContentResult DownloadErr(string filename)
        {
            string path = Path.Combine(_UploadDirectory, filename);
            if (System.IO.File.Exists(path))
                return File(StreamFile(path), "application/vnd.ms-excel", filename);
            else
                return File(new byte[] { 0 }, "text/plain", filename);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        public ActionResult AddNewPart(string MatNo, string SourceType, string ProdPurpose, string ColorSuffix, string MatDesc
            , string SuppCD, string SuppName, string ValidFrom, string ValidTo)
        {
            string ResultQuery = "Saving data is completed successfully";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("InsertSourceListMaintenance"
                    , new object[] { MatNo, SourceType, ProdPurpose, ColorSuffix, SuppCD, ValidFrom, ValidTo, AuthorizedUser.Username });
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
                ex.PutLog("", AuthorizedUser.Username, "AddSourceList", 0, "MPCS0002ERR", "ERR", module, function, 2);
            }
            db.Close();
            return Content(ResultQuery);
        }
        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }
        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Path.Combine(Sing.Me.AppData, "SourceList");
                if (!Directory.Exists(_uploadDirectory))
                    Directory.CreateDirectory(_uploadDirectory);
                return _uploadDirectory;
            }
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = ParamsHelper.Unique(Path.Combine(_UploadDirectory, e.UploadedFile.FileName));
                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = Path.GetFileName(_FilePath);
            }
        }
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("SourceListUpload", ValidationSettings, FileUploadComplete);
        }

        public ActionResult SourceListUpload(string filename)
        {
            string status = "OK";
            string path = Path.Combine(_UploadDirectory, filename);
            long pid=0;
            if (!System.IO.File.Exists(path))
            {
                return Json(new { Status = "empty" });
            }
            List<SourceListMaintenance> sl = null;
            try
            {
                sl = Xlimex.Read<SourceListMaintenance>(path, "MAT_NO;SOURCE_TYPE;PROD_PURPOSE_CD;PART_COLOR_SFX;SUPP_CD;VALID_DT_FR");
            }
            catch (Exception ex)
            {
                pid = ex.PutLog("", AuthorizedUser.Username, "SourceListUpload", 0, "MPCS0002ERR", "ERR", module, function, 2);
                return Json(new { Status = "error;" + pid});
            }
            if (sl.Count < 1)
                return Json(new { Status = "empty" });
            for (int SLI = 0; SLI < sl.Count; SLI++)
            {
                sl[SLI].SEQ = SLI + 1;                
            }
           
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int errs = 0;
            int rows = 0;
            for (int i = 0; i < sl.Count; i++)
            {
                SourceListMaintenance s = sl[i];

                if (string.IsNullOrEmpty(s.MAT_NO)
                    && string.IsNullOrEmpty(s.SOURCE_TYPE)
                    && string.IsNullOrEmpty(s.PROD_PURPOSE_CD)
                    && string.IsNullOrEmpty(s.PART_COLOR_SFX)
                    && string.IsNullOrEmpty(s.SUPP_CD)
                    && string.IsNullOrEmpty(s.VALID_DT_FR)
                    ) // just skip empty row
                {
                    s.ERR_MSG ="NULL";
                    continue;
                }
                s.ERR_MSG =
                      ((string.IsNullOrEmpty(s.MAT_NO) ? ",Material No" : "")
                    + (string.IsNullOrEmpty(s.SOURCE_TYPE) ? ",Source type" : "")
                    + (string.IsNullOrEmpty(s.PROD_PURPOSE_CD) ? ",Production Purpose" : "")
                    + (string.IsNullOrEmpty(s.PART_COLOR_SFX) ? ",Part Color Sfx" : "")
                    + (string.IsNullOrEmpty(s.SUPP_CD) ? ",Supplier Code" : "")
                    + (string.IsNullOrEmpty(s.VALID_DT_FR) ? ",Valid Date From" : ""));
                if (s.ERR_MSG.Length > 1)
                    s.ERR_MSG= s.ERR_MSG.Substring(1);
                
                if (!string.IsNullOrEmpty(s.ERR_MSG))
                {
                    s.ERR_MSG = s.ERR_MSG + " is empty";
                    continue;
                }

                s.ERR_MSG = 
                     ((s.MAT_NO.Length > 23)? " Material No" : "")
                    +((s.SOURCE_TYPE.Length > 1)? ",Source Type" : "")
                    +((s.PROD_PURPOSE_CD.Length > 5)? ",Production Purpose" : "")
                    +((s.PART_COLOR_SFX.Length > 2) ? ",Part Color Sfx" : "")
                    +((s.SUPP_CD.Length > 6) ? ",Supplier Code" : "")
                    +((s.VALID_DT_FR.Length > 10) ? ",Valid Date From" : "")
                    ;
                                
                if (s.ERR_MSG.Length > 1)
                    s.ERR_MSG = s.ERR_MSG.Substring(1);
                if (!string.IsNullOrEmpty(s.ERR_MSG))
                {
                    s.ERR_MSG = s.ERR_MSG + " data too long";
                    continue;
                }

                DateTime d = s.VALID_DT_FR.ToDateTime();
                string sformat = "dd.MM.yyyy";
                
                if (d.Year < 1900)                
                {
                    s.ERR_MSG = "Valid From date not in " + sformat + " format";
                    continue;
                }
                
                try
                {
                    string err=db.ExecuteScalar<string>("SourceListUploadCheck", new object[] { s });
                    if (!string.IsNullOrEmpty(err))
                    {
                        s.ERR_MSG = err;
                        continue;
                    }
                    else
                    {
                        ++rows;
                    }
                }
                catch (Exception ex)
                {
                    pid = ex.PutLog("", AuthorizedUser.Username, "SourceListUpload", pid, "MPCS0002ERR", "ERR", module, function, 2);
                    status = "error;" + pid;
                }
                s.CREATED_BY = AuthorizedUser.Username;
                s.CHANGED_BY = s.CREATED_BY;
                
            }
            List<string> Suppliers = sl.Select(a => a.SUPP_CD).Distinct().ToList();
            List<string> Materials = sl.Select(a => a.MAT_NO).Distinct().ToList();
            List<string> ProdPurposes = sl.Select(a => a.PROD_PURPOSE_CD).Distinct().ToList();
            List<string> SourceTypes = sl.Select(a => a.SOURCE_TYPE).Distinct().ToList();            
            List<string> neMaterials = new List<string>();
            int mJ = Materials.Count;
            int mI = 0, mN = 0, maxn = 300; // (8000 chars / 23 roughly 324, spare some chars)
            StringBuilder b = new StringBuilder();
            
            while (mI < mJ) 
            {
                b.Clear();
                mN = 0;
                while (mI < mJ && mN < maxn)
                {
                    if (mN>0) b.Append(";");
                    b.Append(Materials[mI]);
                    mI++;
                    mN++;
                }
                neMaterials.AddRange(db.FetchStatement<string>("exec pcs_GetNonExistICS @0", new object[] { b.ToString().Replace("'", "") }));
            }
           
            List<string> neSuppliers = db.Fetch<string>("GetNESupplierICS", new object[] { string.Join(";",Suppliers)});            
            List<string> neProdPurposes = db.FetchStatement<string>("exec pcs_GetNonExistICS @0, @1, @2", new object[] { string.Join(";",ProdPurposes), "PROD_PURPOSE_CD", "TB_M_PROD_PURPOSE"});
            List<string> neSourcetypes = db.FetchStatement<string>("exec pcs_GetNonExistICS @0, @1, @2", new object[] { string.Join(";",SourceTypes), "SOURCE_TYPE", "TB_M_SOURCE_TYPE"});

            /// duplication check 

            List<SourceListMaintenance> slo =
                sl.OrderBy(a => a.MAT_NO + a.PROD_PURPOSE_CD + a.SOURCE_TYPE + a.PART_COLOR_SFX + a.VALID_DT_FR).ToList();
            int prevSEQ = 0;
            string prevKEY = "";
            foreach (var o in slo)
            {
                string k = o.MAT_NO + o.PROD_PURPOSE_CD + o.SOURCE_TYPE + o.PART_COLOR_SFX + o.VALID_DT_FR;
                SourceListMaintenance ori = ((o.SEQ ?? 0) > 0) ? sl[(o.SEQ ?? 0) - 1] : null;
                if (ori == null) continue; 
                if (string.Compare(prevKEY, k) == 0)
                {
                    ori.ERR_MSG = "Duplicate with row : " + prevSEQ.ToString();
                }
                else
                {
                    prevKEY = k;
                    prevSEQ = o.SEQ ?? 0;
                }
               
                if (neSuppliers.Contains(o.SUPP_CD)) 
                    ori.ERR_MSG = ((string.IsNullOrEmpty(ori.ERR_MSG)) ? "": (ori.ERR_MSG + ". ")) + "Supplier Code not found";
                if (neSourcetypes.Contains(o.SOURCE_TYPE)) 
                    ori.ERR_MSG = ((string.IsNullOrEmpty(ori.ERR_MSG)) ? "" : (ori.ERR_MSG + ". ")) + "Source Type is not valid";
                if (neProdPurposes.Contains(o.PROD_PURPOSE_CD)) 
                    ori.ERR_MSG = ((string.IsNullOrEmpty(ori.ERR_MSG)) ? "" : (ori.ERR_MSG + ". ")) + "Production Purpose is not valid";
                if (neMaterials.Contains(o.MAT_NO))
                    ori.ERR_MSG = ((string.IsNullOrEmpty(ori.ERR_MSG)) ? "" : (ori.ERR_MSG + ". ")) + "Material No not found";
                
            }
            
            errs = sl.Where(a => !string.IsNullOrEmpty(a.ERR_MSG)).Count();
            if (errs<1) {
                foreach (var s in sl) {
                    db.ExecuteScalar<string>("SourceListPut", new object[] { s });
                }
                pid = ExceptionalHelper.PutLog("Source List Upload " + sl.Count + " records", AuthorizedUser.Username, "SourceListUpload", 0, "MPCS0001INF", "INF", module, function, 0);
                status = "true" + ";" + pid.ToString();
            }
            else {                
                status = "false";
            }
            db.Close();
            try
            {
                Xlimex.Write<SourceListMaintenance>(path, path, ";;;;;;ERR_MSG", "SEQ", sl, 0);
            }
            catch (Exception ex)
            {
                pid = ex.PutLog("", AuthorizedUser.Username, "SourceListUpload", 0, "MPCS0002ERR", "ERR", module, function, 2);
                status = "error;" + pid;
            }
            return Json(new{Status = status, filename=filename});
        }
    }
}
