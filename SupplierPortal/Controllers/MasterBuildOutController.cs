﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;
using Portal.Models.MasterBuildOut;
using Toyota.Common.Web.Excel;
using System.Web;
using System.Data;
using System.IO;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace Portal.Controllers
{
    public class MasterBuildOutController : BaseController
    {
        public MasterBuildOutController() : base("Master Build Out") { }

        private IDBContext dbcontext()
        {
            return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        }

        #region preparing screen

        public ActionResult PartialHeader()
        {
            return PartialView("PartialMasterBuildOutHeader");
        }

        public ActionResult PartialHeaderButton()
        {
            return PartialView("PartialMasterBuildOutButton");
        }

        public ActionResult PartialGrid()
        {
            return PartialView("PartialMasterBuildOutGrid");
        }

        public ActionResult getPartData()
        {
            TempData["GridName"] = Request.Params["p_gridName"];
            
            string supplier = Request.Params["w_supplier"];
            string dock = Request.Params["w_dock"];
            

            IDBContext db = dbcontext();
            List<partList> partlist = db.Query<partList>("getPartData", new object[] { supplier, dock }).ToList();
            db.Close();

            ViewData["partDatas"] = partlist;
            return PartialView("GridLookup/PartialGrid", ViewData["partDatas"]);
        }

        public ActionResult getSupplierData()
        {
            TempData["GridName"] = Request.Params["p_gridName"];

            IDBContext db = dbcontext();
            List<supplierList> supplierList = db.Query<supplierList>("getSupplierData").ToList();
            db.Close();

            ViewData["supplierDatas"] = supplierList;
            return PartialView("GridLookup/PartialGrid", ViewData["supplierDatas"]);
        }

        public ActionResult getDockData()
        {
            TempData["GridName"] = Request.Params["p_gridName"];
            
            IDBContext db = dbcontext();
            List<dockList> dockList = db.Query<dockList>("getDockData").ToList();
            db.Close();

            ViewData["dockDatas"] = dockList;
            return PartialView("GridLookup/PartialGrid", ViewData["dockDatas"]);
        }
        
        #endregion


        protected override void StartUp()
        {

        }

        protected override void Init() 
        {
            try
            {
                IDBContext db = dbcontext();

                var partDatas = db.Query<partList>("getPartData", new object[] {"",""});
                var supplierDatas = db.Query<supplierList>("getSupplierData");
                var dockDatas = db.Query<dockList>("getDockData");
                db.Close();

                ViewData["partDatas"] = partDatas;
                ViewData["supplierDatas"] = supplierDatas;
                ViewData["dockDatas"] = dockDatas;
            }
            catch (Exception ex)
            { 
            
            }
        }

     

        public ActionResult gridBOAction()
        {
            IDBContext db = dbcontext();

            string partNo = Request.Params["p_part_no"];
            string dock = Request.Params["p_dock"];
            string supplier = Request.Params["p_supplier"];
            string inactive = Request.Params["p_inactive"];
            string clear = Request.Params["p_clear"];
            string kanbanNo = Request.Params["p_kanban_no"];

            List<BOList> BOList = new List<BOList>();

            if (clear == "N")
            {
                BOList = db.Query<BOList>("getBuildOut", new object[] { partNo, dock, supplier, inactive, kanbanNo }).ToList();
            }

            db.Close();

            ViewData["gridData"] = BOList;

            return PartialView("PartialMasterBuildOutGrid", ViewData["gridData"]);
        }

        public ActionResult PartialPopUpBO()
        {
            IDBContext db = dbcontext();

            var partDatas = db.Query<partList>("getPartData", new object[] {"", ""});
            var supplierDatas = db.Query<supplierList>("getSupplierData");
            var dockDatas = db.Query<dockList>("getDockData");
            db.Close();

            ViewData["partDatas"] = partDatas;
            ViewData["supplierDatas"] = supplierDatas;
            ViewData["dockDatas"] = dockDatas;

            return PartialView("formBO");
        }

        public ActionResult save(string partNo, string partName, string dock, string supplier, string kanbanNo, string qtyPerBox, string inactive, string mode)
        {
            string result = "";

            if (inactive == "Active") { }
            int status = inactive == "Active" ? 0 : 1;

            string user = SessionState.GetAuthorizedUser().Username.ToString();

            try
            {
                IDBContext db = dbcontext();

                if (mode == "add")
                {
                    result = db.SingleOrDefault<string>("saveDataMasterBO", new object[] { partNo, supplier, dock, kanbanNo, partName, qtyPerBox, status, user });
                }
                else {
                    result = db.SingleOrDefault<string>("updateDataMasterBO", new object[] { partNo, supplier, dock, kanbanNo, partName, qtyPerBox, status, user });
                }
                
                db.Close();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            
            return Content(result);
        }

        public void download(string partNo, string dock, string supplier, string inactive, string kanbanNo)
        {
            IDBContext db = dbcontext();
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<BOList> boForExcel = new List<BOList>();

            boForExcel = db.Query<BOList>("getBuildOut", new object[] { partNo, dock, supplier, inactive, kanbanNo }).ToList();

            fileName = "MasterBuildOutList" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            data = exporter.Write(ConvertToDataTable(boForExcel), "MasterBuildOut");

            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public FileContentResult downloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "master_build_out_template.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/MasterBuildOut/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("BOUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/MasterBuildOut/TempUpload/");
                return _uploadDirectory;
            }
        }

        public ActionResult BOUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }



        private BOList AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            BOList _UploadModel = new BOList();

            string function = "81016";
            string module = "8";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_PART_BO";

            string sheetName = "MasterBuildOut";

            message = "Starting Upload Build Out";
            location = "BuildOutUpload.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_PART_BO";
                location = "BuildOutUpload.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
                
                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as partNo," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as supplierCode," +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as supplierPlant," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as dockCode," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as kanbanNo," + //kanban_no = unique no
                                    @"IIf(IsNull(F6), Null, Cstr(F6)) as qtyPerBox ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
               
                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    
                    message = "Error : " + e.Message;
                    location = "BuildOutUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });



                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //try
                    //{
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("partNo", "PART_NO");
                    bulkCopy.ColumnMappings.Add("supplierCode", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("supplierPlant", "SUPPLIER_PLANT");
                    bulkCopy.ColumnMappings.Add("dockCode", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("kanbanNo", "KANBAN_NO");
                    bulkCopy.ColumnMappings.Add("qtyPerBox", "QTY_PER_BOX");


                #endregion

                    try
                    {
                        message = "Insert Data to TB_T_PART_BO";
                        location = "BuildOutUpload.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "BuildOutUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                        throw new Exception(ex.Message);
                    }
                }


                // validate data in temporary uploaded table.
                string validate = db.SingleOrDefault<string>("ValidateBOUpload", new object[] { processID });

                if (validate == "error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found.")
                {
                    TempData["IsValid"] = "empty";
                    sts = "6";
                    message = validate;
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }


                location = "BuildOutUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            sts      // sts
                                        });

            }
            catch (Exception exc)
            {

                TempData["IsValid"] = "false;" + processID + ";" + exc.Message;


                message = "Error : " + exc.Message;
                location = "BuildOutUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

            }
            finally
            {
                
                excelConn.Close();

                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

            }
            return _UploadModel;
        }



        public ActionResult MoveBODataTemp(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            IDBContext db = dbcontext();

            try
            {
                
                db.Execute("moveBODataTemp", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return Content(message);
        }



        public void UploadInvalid(string process_id)
        {
            IDBContext db = dbcontext();
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<BOListError> boForExcel = new List<BOListError>();

            boForExcel = db.Query<BOListError>("getErrorList", new object[] { process_id }).ToList();

            fileName = "MasterBuildOutListError" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            data = exporter.Write(ConvertToDataTable(boForExcel), "MasterBuildOut");

            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }
    }
}
