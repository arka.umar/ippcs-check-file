﻿using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Portal.Models.BuyingPriceMaintenance;
using Portal.Models.Globals;
using Portal.Models.PurchaseReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;
using System.Globalization;
using Portal.Models;
using System.Text;
using System.Reflection;

namespace Portal.Controllers
{
    public class BuyingPriceMaintenanceController : BaseController
    {
        //
        // GET: /BuyingPriceMaintenance/
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        public readonly string moduleId = "7";
        public readonly string functionId = "71012";
        public readonly string eUNKNOWN = "MPCS00002ERR";

        public BuyingPriceMaintenanceController()
            : base("Buying Price Maintenance")
        {
            PageLayout.UseMessageBoard = true;
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        #region Model Properties
        List<SupplierICS> _invoiceInquirySupplierModel = null;
        List<MaterialICS> _buyingPriceMaterialModel = null;
        private List<SupplierICS> _InvoiceInquirySupplierModel
        {
            get
            {
                if (_invoiceInquirySupplierModel == null)
                    _invoiceInquirySupplierModel = new List<SupplierICS>();

                return _invoiceInquirySupplierModel;
            }
            set
            {
                _invoiceInquirySupplierModel = value;
            }
        }

        private List<MaterialICS> _BuyingPriceMaterialModel
        {
            get
            {
                if (_buyingPriceMaterialModel == null)
                    _buyingPriceMaterialModel = new List<MaterialICS>();
                return _buyingPriceMaterialModel;
            }
            set
            {
                _buyingPriceMaterialModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            BuyingPriceMaintenanceModel prm = new BuyingPriceMaintenanceModel();

            PageLayout.UseSlidingBottomPane = true;
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "BuyingPriceMaintenance", "OISupplierCd");
            List<MaterialICS> materials = Model.GetModel<User>().FilteringArea<MaterialICS>(GetAllMaterial(), "MAT_NO", "BuyingPriceMaintenance", "OIMatNo");
            ViewData["SupplierGridLookup"] = suppliers;
            ViewData["AddEditSupplierCd"] = suppliers;
            ViewData["lookUPmaterial"] = materials;
            ViewData["DropdownlistCurrCD"] = Dbutil.GetList<BuyingPriceMaintenance>("getCurrentCD");
            ViewData["DropdownnBusinessArea"] = Dbutil.GetList<BuyingPriceMaintenance>("GetBusinessArea");
            //ViewData["SourceTypeCombo"] = Dbutil.GetList<ComboData>("GetSourceTypeICSCombo");
            
            DropDownLIstProdPurpose();
            PriceStatus();
            packingType();
            SourceData();
            SourceTypeData();

            Model.AddModel(prm);
        }

        private List<SupplierICS> GetAllSupplierICS()
        {
            return Dbutil.GetList<SupplierICS>("GetAllSupplier_BuyingPrice");            
        }

        public ActionResult HeaderBuyingPricetInquiry()
        {
            #region Required in partial page inquiry
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OISupplierCd");

            _InvoiceInquirySupplierModel = suppliers;

            Model.AddModel(_InvoiceInquirySupplierModel);
            return PartialView("HeaderBuyingPriceMaintenance", Model);
            #endregion
        }

        public ActionResult BuyingPriceInquiryPartial()
        {
            #region Required model in partialview
            BuyingPriceMaintenanceModel model = Model.GetModel<BuyingPriceMaintenanceModel>();

            string SupplierCd = Request.Params["SupplierCode"];
            string MatNo = Request.Params["MaterialNo"];
            string releaseSts = Request.Params["ReleaseSts"];
            string PriceType = Request.Params["PrieType"];

            //FID.Ridwan - 20210714
            string BuyerCd = Request.Params["BuyerCd"];
            string MatDesc = Request.Params["MatDesc"];
            string PriceStatus = Request.Params["PriceStatus"];
            string PackingType = Request.Params["PackingType"];
            string SourceData = Request.Params["SourceData"];
            string WarpRefNo = Request.Params["WarpRefNo"];
            string DeletionFlag = Request.Params["DeletionFlag"];
            string SourceType = Request.Params["SourceType"];
            string PCNo = Request.Params["PCNo"];
            string ProdPurpose = Request.Params["ProdPurpose"];
            string CppFlag = Request.Params["CppFlag"];
            string ValidFrom = Request.Params["ValidFrom"];
            string ValidTo = Request.Params["ValidTo"];


            model.buyingPrices = getAllbuyingPriceResult(SupplierCd, MatNo, releaseSts, PriceType,
                BuyerCd, MatDesc, PriceStatus,PackingType,SourceData,WarpRefNo,DeletionFlag,SourceType,PCNo,ProdPurpose,CppFlag,ValidFrom,ValidTo
                );

            #endregion

            return PartialView("BuyingPriceInquiryPartial", Model);
        }

        private List<BuyingPriceMaintenance> getAllbuyingPriceResult(string SupplierCd, string MatNo, string releaseSts, string PriceType,
            string BuyerCd, string MatDesc, string PriceStatus, string PackingType, string SourceData, string WarpRefNo, string DeletionFlag,
                string SourceType, string PCNo, string ProdPurpose, string CppFlag, string ValidFrom, string ValidTo
            )
        {
            List<BuyingPriceMaintenance> result = new List<BuyingPriceMaintenance>();

            IDBContext db = DbContext;
            result = db.Fetch<BuyingPriceMaintenance>("BuyingPriceInquiry"
                  , new object[]
                        {
                            SupplierCd,MatNo,releaseSts,PriceType,
                            BuyerCd, MatDesc, PriceStatus,PackingType,SourceData,
                            WarpRefNo,DeletionFlag,SourceType,PCNo,ProdPurpose,CppFlag,
                            ValidFrom,ValidTo
                         });
            db.Close();

            return result;
        }

        public ActionResult BuyingPriceAddEdit()
        {
            return PartialView("BuyingPriceAddEditPopup", Model);
        }

        public ActionResult PartialHeaderSupplierCode()
        {
            #region Required model in partial page : for OIPSupplierOption GridLookup
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "BuyingPriceMaintenance", "OISupplierCd");
            TempData["GridName"] = "OISupplierCd";

            _InvoiceInquirySupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _InvoiceInquirySupplierModel);
        }

        public ActionResult LookUpSupplierCode()
        {
            #region Required model in partial page : for OIPSupplierOption GridLookup

            List<SupplierICS> AddEditsuppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "BuyingPriceMaintenance", "AddEditSupplierCd");

            TempData["GridName"] = "AddEditSupplierCd";
            _InvoiceInquirySupplierModel = AddEditsuppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _InvoiceInquirySupplierModel);
        }

        public ActionResult PartialHeaderMaterial()
        {
            List<MaterialICS> Materials = Model.GetModel<User>().FilteringArea<MaterialICS>(GetAllMaterial(), "MAT_NO", "BuyingPriceMaintenance", "OIMatNo");

            TempData["GridName"] = "OIMatNo";

            _BuyingPriceMaterialModel = Materials;
            return PartialView("GridLookup/PartialGrid", _BuyingPriceMaterialModel);

        }

        public ActionResult LookUpMaterial()
        {
            List<MaterialICS> Materials = Model.GetModel<User>().FilteringArea<MaterialICS>(GetAllMaterial(), "MAT_NO", "BuyingPriceMaintenance", "AddEditMaterialNo");
            List<MaterialICS> MaterialDes = Model.GetModel<User>().FilteringArea<MaterialICS>(GetAllMaterial(), "MAT_DESC", "BuyingPriceMaintenance", "AddEditMaterialDesc");

            TempData["GridName"] = "AddEditMaterialNo";

            _BuyingPriceMaterialModel = Materials;
            return PartialView("GridLookup/PartialGrid", _BuyingPriceMaterialModel);

        }

        public ActionResult SourceTypeData()
        {
            ViewData["SourceTypeCombo"] = Dbutil.GetList<ComboData>("GetSourceTypeICSCombo");

            TempData["GridName"] = "AddEditSourceTypeData";
            return PartialView("GridLookup/PartialGrid", ViewData["SourceTypeCombo"]);
        }

        public ActionResult DropDownLIstProdPurpose()
        {
            ViewData["DropdownlistProd"] = Dbutil.GetList<ComboData>("getDropdowngetAllProdPurpose");

            TempData["GridName"] = "AddEditProdPurpose";
            return PartialView("GridLookup/PartialGrid", ViewData["DropdownlistProd"]);
        }

        public ActionResult DropDownLIstPartColorSfx()
        {
            ViewData["Dropdownlist"] = Dbutil.GetList<ComboData>("getDropdowngetPartColorSfx"); 

            TempData["GridName"] = "AddEditSourceType";
            return PartialView("GridLookup/PartialGrid", ViewData["Dropdownlist"]);
        }

        private ActionResult PriceStatus()
        {
            ViewData["DropdownlistPriceSts"] = Dbutil.GetList<ComboData>("getDropdownPriceStatus"); 

            TempData["GridName"] = "AddEditPriceStatus";
            return PartialView("GridLookup/PartialGrid", ViewData["DropdownlistPriceSts"]);
        }

        public ActionResult packingType()
        {
            ViewData["DropdownlistPackingType"] = Dbutil.GetList<ComboData>("getDropdownPackingType"); 

            TempData["GridName"] = "AddEditPriceStatus";
            return PartialView("GridLookup/PartialGrid", ViewData["DropdownlistPackingType"]);
        }

        public ActionResult SourceData()
        {
            ViewData["DropdownlistSourceData"] = Dbutil.GetList<ComboData>("getDropdownSourceData");

            TempData["GridName"] = "AddEditSourceData";
            return PartialView("GridLookup/PartialGrid", ViewData["DropdownlistSourceData"]);
        }

        private List<MaterialICS> GetAllMaterial()
        {
            return Dbutil.GetList<MaterialICS>("GetAllMaterialICS"); 
        }

        public JsonResult AddEditData(BuyingPriceMaintenance data, string gScreenMode)
        {
            List<BuyingPriceMaintenance> buyingPriceM = new List<BuyingPriceMaintenance>();
            IDBContext db = DbContext;
            string result = "";

            string valid_dt_from = string.Empty;
            string valid_dt_to = string.Empty;
            valid_dt_from = data.VALID_FROM;
            valid_dt_to = data.VALID_DT_TO;
            if (!string.IsNullOrEmpty(valid_dt_from))
            {
                string[] valid_fromArr = data.VALID_FROM.Split('.');
                valid_dt_from = valid_fromArr[2] + '-' + valid_fromArr[1] + '-' + valid_fromArr[0];
                data.VALID_FROM = valid_dt_from;
            }
            else
            {
                data.VALID_FROM = "9999-12-31";
            }
            if (!string.IsNullOrEmpty(valid_dt_to))
            {
                string[] valid_toArr = data.VALID_DT_TO.Split('.');
                valid_dt_to = valid_toArr[2] + '-' + valid_toArr[1] + '-' + valid_toArr[0];
                data.VALID_DT_TO = valid_dt_to;
            }
            else
            {

                data.VALID_DT_TO = "9999-12-31";
            }

            try
            {
                if (gScreenMode == "ADD")
                {
                    result = saveDataBuyingPrice(data, db);
                }
                else if (gScreenMode == "EDIT")
                {
                    result = editDataBuyingPrice(data, db);
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                ex.PutLog("", AuthorizedUser.Username, "AddEditData", 0, eUNKNOWN, "ERR", moduleId, functionId, 2);
                db.Close();
                return Json("Error|" + message);
            }

            return Json("Success|" + result);

        }

        private string editDataBuyingPrice(BuyingPriceMaintenance data, IDBContext db)
        {
            string result = "";
            dynamic args = new object[]
            {
                data.MATERIAL_NO,
                data.SUPPLIER_CODE,
                data.SOURCE_TYPE,
                data.PROD_PURPOSE,
                data.VALID_FROM,
                data.PART_COLOR_SFX,
                data.PACKING_TYPE,
                data.PRICE_STS,
                data.PRICE,
                data.CURR_CD,
                data.VALID_DT_TO,
                data.BUSINESS_AREA,
                AuthorizedUser.Username
            };
            result = db.SingleOrDefault<string>("UpdateBuyingPrice", args);
            RemoveCachedData();
            return result;

        }
        private void RemoveCachedData()
        {
            string kdata = "key.BuyingPrice[" + AuthorizedUser.Username ?? "" + "]";
            string lkey = Memori.Get<string>(kdata);
            if (!string.IsNullOrEmpty(lkey))
            {
                Memori.Set(lkey, null);
            }
            Memori.Set(kdata, null);
        }
        private string saveDataBuyingPrice(BuyingPriceMaintenance data, IDBContext db)
        {
            string result = "";
            dynamic args = new object[]
            {
                data.BUSINESS_AREA,
                data.MATERIAL_NO,
                data.MATERIAL_DESC,
                data.PROD_PURPOSE,
                data.SOURCE_TYPE,
                data.SUPPLIER_CODE,
                data.PART_COLOR_SFX,
                data.PACKING_TYPE,
                data.PRICE_STS,
                data.PRICE,
                data.CURR_CD,
                data.VALID_FROM,
                data.VALID_DT_TO,
                AuthorizedUser.Username

            };
            result = db.SingleOrDefault<string>("InsertEditBuyingPrice", args);
            RemoveCachedData();
            return result;
        }

        public JsonResult getByKey(BuyingPriceMaintenance data)
        {
            string valid_dt_from = string.Empty;
            valid_dt_from = data.VALID_FROM;
            if (!string.IsNullOrEmpty(valid_dt_from))
            {
                string[] valid_fromArr = data.VALID_FROM.Split('.');
                valid_dt_from = valid_fromArr[2] + '-' + valid_fromArr[1] + '-' + valid_fromArr[0];
                data.VALID_FROM = valid_dt_from;
            }
            IDBContext db = DbContext;
            List<BuyingPriceMaintenance> result = new List<BuyingPriceMaintenance>();
            dynamic args = new object[]
            {
                data.MATERIAL_NO,
                data.SUPPLIER_CODE,
                data.SOURCE_TYPE,
                data.PROD_PURPOSE,
                data.PART_COLOR_SFX,
                data.PACKING_TYPE,
                data.VALID_FROM
            };

            result = db.Fetch<BuyingPriceMaintenance>("getByKeyEdit", args);
            db.Close();
            return Json(result);
        }

        public string CheckBeforeDownload(BuyingPriceMaintenance data)
        {
            string result = string.Empty;
            string valid_from = string.Empty;
            valid_from = data.VALID_FROM;

            try
            {
                if (!string.IsNullOrEmpty(valid_from))
                {
                    string[] dateArr = valid_from.Split('.');
                    valid_from = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
                }
                else
                {
                    valid_from = "9999-12-31";
                }

                result = DbContext.ExecuteScalar<string>("CheckBeforeDownloadBuyingPrice", new object[] { data.SUPPLIER_CODE, data.MATERIAL_NO, data.RELEASE_STS, data.PRICE_TYPE });

            }
            catch (Exception ex)
            {
                ex.PutLog("", AuthorizedUser.Username, "CheckBeforeDownload", 0, eUNKNOWN, "ERR", moduleId, functionId, 2);
                result = ex.Message;
            }
            finally
            {
                DbContext.Close();
            }
            return result;
        }

        #region Custom Excel Download

        public static ICellStyle createCellStyleDataLeft(HSSFWorkbook wb)
        {
            ICellStyle c = wb.CreateCellStyle();
            c.BorderBottom = BorderStyle.NONE;
            c.BorderLeft = BorderStyle.NONE;
            c.BorderRight = BorderStyle.NONE;
            c.BorderTop = BorderStyle.NONE;

            c.WrapText = true;
            c.VerticalAlignment = VerticalAlignment.CENTER;
            c.Alignment = HorizontalAlignment.LEFT;

            IFont f = wb.CreateFont();
            f.FontName = "Calibri";
            f.FontHeightInPoints = 10;
            f.Color = IndexedColors.BLACK.Index;
            f.Boldweight = (short)FontBoldWeight.NORMAL;
            c.SetFont(f);

            return c;
        }

        public static ICellStyle createCellStyleDataRight(HSSFWorkbook wb)
        {
            ICellStyle c = wb.CreateCellStyle();
            c.BorderBottom = BorderStyle.NONE;
            c.BorderLeft = BorderStyle.NONE;
            c.BorderRight = BorderStyle.NONE;
            c.BorderTop = BorderStyle.NONE;

            c.WrapText = true;
            c.VerticalAlignment = VerticalAlignment.CENTER;
            c.Alignment = HorizontalAlignment.RIGHT;
            IFont f = wb.CreateFont();
            f.FontName = "Calibri";
            f.FontHeightInPoints = 10;
            f.Color = IndexedColors.BLACK.Index;
            f.Boldweight = (short)FontBoldWeight.NORMAL;
            c.SetFont(f);

            return c;
        }

        public static void CreateSingleColHeader(HSSFWorkbook wb, ISheet sheet, int rows, int col, ICellStyle colStyleHeader, string cellValue)
        {
            IRow row = sheet.GetRow(rows) ?? sheet.CreateRow(rows);

            ICell cell = row.CreateCell(col);
            cell.CellStyle = colStyleHeader;
            cell.SetCellValue(cellValue);
        }

        public static ICellStyle createCellStyleDataCenter(HSSFWorkbook wb)
        {
            ICellStyle c = wb.CreateCellStyle();
            c.BorderBottom = BorderStyle.NONE;
            c.BorderLeft = BorderStyle.NONE;
            c.BorderRight = BorderStyle.NONE;
            c.BorderTop = BorderStyle.NONE;

            c.WrapText = true;
            c.VerticalAlignment = VerticalAlignment.CENTER;
            c.Alignment = HorizontalAlignment.CENTER;

            IFont f = wb.CreateFont();
            f.FontName = "Calibri";
            f.FontHeightInPoints = 10;
            f.Color = IndexedColors.BLACK.Index;
            f.Boldweight = (short)FontBoldWeight.NORMAL;
            c.SetFont(f);

            return c;
        }

        public static ICellStyle createCellStyleDataDouble(HSSFWorkbook wb)
        {
            ICellStyle c = wb.CreateCellStyle();
            c.BorderBottom = BorderStyle.NONE;
            c.BorderLeft = BorderStyle.NONE;
            c.BorderRight = BorderStyle.NONE;
            c.BorderTop = BorderStyle.NONE;

            IDataFormat dataFormatCustom = wb.CreateDataFormat();
            c.DataFormat = dataFormatCustom.GetFormat("#,##0.00");
            IFont f = wb.CreateFont();
            f.FontName = "Calibri";
            f.FontHeightInPoints = 10;
            f.Color = IndexedColors.BLACK.Index;
            f.Boldweight = (short)FontBoldWeight.NORMAL;
            c.SetFont(f);

            return c;
        }

        public static ICellStyle createCellStyleColumnHeader(HSSFWorkbook wb)
        {
            ICellStyle ch = wb.CreateCellStyle();
            ch.BorderBottom = BorderStyle.HAIR;
            ch.BorderLeft = BorderStyle.NONE;
            ch.BorderRight = BorderStyle.NONE;
            ch.BorderTop = BorderStyle.NONE;

            ch.FillBackgroundColor = IndexedColors.GREY_80_PERCENT.Index;
            ch.FillPattern = FillPatternType.NO_FILL;

            ch.WrapText = true;
            ch.VerticalAlignment = VerticalAlignment.CENTER;
            ch.Alignment = HorizontalAlignment.CENTER;

            IFont f11 = wb.CreateFont();
            f11.FontName = "Calibri";
            f11.FontHeightInPoints = 11;
            f11.Color = IndexedColors.BLACK.Index;
            f11.Boldweight = (short)FontBoldWeight.NORMAL;
            ch.SetFont(f11);
            return ch;
        }

        public static ICellStyle createCellStyleColumnTitle(HSSFWorkbook wb)
        {
            ICellStyle ct = wb.CreateCellStyle();

            ct.FillPattern = FillPatternType.NO_FILL;

            ct.WrapText = false;
            ct.VerticalAlignment = VerticalAlignment.CENTER;
            ct.Alignment = HorizontalAlignment.LEFT;
            IFont f11 = wb.CreateFont();
            f11.FontName = "Calibri";
            f11.FontHeightInPoints = 11;
            f11.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            ct.SetFont(f11);
            return ct;
        }

        public static ICellStyle createCellStyleColumnTitleSecond(HSSFWorkbook wb)
        {
            ICellStyle ct2 = wb.CreateCellStyle();

            ct2.FillPattern = FillPatternType.NO_FILL;

            ct2.WrapText = false;
            ct2.VerticalAlignment = VerticalAlignment.CENTER;
            ct2.Alignment = HorizontalAlignment.LEFT;

            IFont f11 = wb.CreateFont();
            f11.FontName = "Calibri";
            f11.FontHeightInPoints = 11;
            f11.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            f11.Boldweight = (short)FontBoldWeight.BOLD;
            ct2.SetFont(f11);
            return ct2;
        }

        public static ICellStyle createCellStyleDataDate(HSSFWorkbook wb, short dataFormat)
        {
            ICellStyle cs = wb.CreateCellStyle();
            cs.BorderBottom = BorderStyle.NONE;
            cs.BorderLeft = BorderStyle.NONE;
            cs.BorderRight = BorderStyle.NONE;
            cs.BorderTop = BorderStyle.NONE;
            cs.DataFormat = dataFormat;
            
            IFont f = wb.CreateFont();
            f.FontName = "Calibri";
            f.FontHeightInPoints = 10;
            f.Color = IndexedColors.BLACK.Index;
            f.Boldweight = (short)FontBoldWeight.NORMAL;
            cs.SetFont(f);


            return cs;
        }
        private const int MaximumSheetNameLength = 25;
        public static string EscapeSheetName(string sheetName)
        {
            string escapedSheetName = sheetName
                                        .Replace("/", "-")
                                        .Replace("\\", " ")
                                        .Replace("?", string.Empty)
                                        .Replace("*", string.Empty)
                                        .Replace("[", string.Empty)
                                        .Replace("]", string.Empty)
                                        .Replace(":", string.Empty);

            if (escapedSheetName.Length > MaximumSheetNameLength)
                escapedSheetName = escapedSheetName.Substring(0, MaximumSheetNameLength);

            return escapedSheetName;
        }

        protected void SendDataAsAttachment(string fileName, byte[] data)
        {
            Response.Clear();

            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";

            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        public void DownloadBuyingPrice(BuyingPriceMaintenance data)
        {
            byte[] result = null;
            string fileName = null;

            try
            {

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                List<BuyingPriceMaintenance> logList = new List<BuyingPriceMaintenance>();
                logList = db.Fetch<BuyingPriceMaintenance>("BuyingPriceInquiry"
                     , new object[]
                        {
                            data.SUPPLIER_CODE, data.MATERIAL_NO, data.RELEASE_STS, data.PRICE_TYPE,
                            data.WRAP_BUYER_CD, data.MATERIAL_DESC, data.PRICE_STS, data.PACKING_TYPE, data.SOURCE_DATA ,
                            data.WRAP_REF_NO, data.DELETION_FLAG, data.SOURCE_TYPE, data.PC_NO, data.PROD_PURPOSE, data.CPP_FLAG,
                            data.VALID_FROM, data.VALID_DT_TO
                         });
                string fn = "BuyingPrice";
                if (!string.IsNullOrEmpty(data.PRICE_TYPE))
                {
                    if (data.PRICE_TYPE.Length > 20)
                    {
                        fn = "Revised Buying Price";
                    }
                    else 
                    {
                        switch (data.PRICE_TYPE[0])
                        {
                            case 'B': fn = "Buying Price"; break;
                            case 'D': fn = "Draft Price"; break;
                            default: break;
                        }
                    }
                }

                fileName = string.Format(fn + " {0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
                result = GenerateDownloadFile(logList);
            }
            catch (Exception e)
            {
                e.PutLog("", AuthorizedUser.Username, "DownloadBuyingPrice", 0, eUNKNOWN, "ERR", moduleId, functionId, 2);
                Response.StatusCode = 500;
                Response.AddHeader(e.GetType().Name, e.Message);
            }
            if (result != null)
                this.SendDataAsAttachment(fileName, result);
        }

        public JsonResult WARP(int? process_id=0, string user="", int? run=1)
        {
            if (string.IsNullOrEmpty(user)) user = AuthorizedUser.Username;
            int i = DbContext.Execute("WARP", new object[] { process_id, user, run }); 
            return Json(i);
        }

        public byte[] GenerateDownloadFile(List<BuyingPriceMaintenance> buyigPriceMaintenance)
        {
            byte[] result = null;

            try
            {
                result = CreateFile(buyigPriceMaintenance);
            }
            catch (Exception ex)
            {
                ex.PutLog(null, AuthorizedUser.Username, "GenerateDownloadFile", 0, "MPCS00002ERR", "ERR", moduleId, functionId, 2);
            }
            finally
            {

            }

            return result;
        }

        private static readonly string SHEET_NAME_BUYING_PRICE = "Buying Price";
        public byte[] CreateFile(List<BuyingPriceMaintenance> buyngPrice)
        {
            Dictionary<string, string> headers = null;
            ISheet sheet1 = null;
            HSSFWorkbook workbook = null;
            byte[] result;
            int startRow = 0;

            workbook = new HSSFWorkbook();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            short dateTimeFormat = dataFormat.GetFormat("dd/MM/yyyy HH:mm:ss");

            ICellStyle cellStyleDataLeft = createCellStyleDataLeft(workbook);
            ICellStyle cellStyleDataRight = createCellStyleDataRight(workbook);
            ICellStyle cellStyleDataCenter = createCellStyleDataCenter(workbook);
            ICellStyle cellStyleDataDouble = createCellStyleDataDouble(workbook);
            ICellStyle cellStyleHeader = createCellStyleColumnHeader(workbook);
            ICellStyle cellStyleTitle = createCellStyleColumnTitle(workbook);
            ICellStyle cellStyleTitleSecond = createCellStyleColumnTitleSecond(workbook);
            ICellStyle cellStyleDataTime = createCellStyleDataDate(workbook, dateTimeFormat);

            sheet1 = workbook.CreateSheet(EscapeSheetName(SHEET_NAME_BUYING_PRICE));
            sheet1.FitToPage = false;

            headers = new Dictionary<string, string>();

            WriteDetail(workbook, sheet1, startRow, cellStyleTitle, cellStyleTitleSecond, cellStyleHeader, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter, cellStyleDataDouble, buyngPrice);

            using (MemoryStream buffer = new MemoryStream())
            {
                workbook.Write(buffer);
                result = buffer.GetBuffer();
            }

            workbook = null;
            return result;
        }

        public void WriteDetail(HSSFWorkbook wb, ISheet sheet1, int startRow, ICellStyle cellStyleTitle, ICellStyle cellStyleSecond, ICellStyle cellStyleHeader, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter, ICellStyle cellStyleDataDouble, List<BuyingPriceMaintenance> buyingPrice)
        {
            int rowIdx = startRow;
            int itemCount = 0;
            string[] colh = new string[] {
                  "MaterialNo"
                , "Material Desc"
                , "Packing Type"
                , "Part Color Sfx"
                , "Valid From"
                , "Price"
                , "Currency"
                , "Price Status"
                , "Release Status"
                , "PC No"
                , "Supp Code"
                , "Supplier Name"
                , "Source Type"
                , "Prod Purpose"
                , "UOM"
                , "MM DF"
                , "Valid To"
                , "Created By"
                , "Created Date"
                , "Changed By"
                , "Changed Date"
                , "Release Date"
                , "Price Type"
                , "Source Data"
                , "Draft Price DF"
                , "WARP Ref No"
                , "Cpp Flag"
                , "WARP Buyer Code"
                , "Business Area"
            };
            int coli = 0;
            foreach (string h in colh)
            {
                CreateSingleColHeader(wb, sheet1, 0, coli++, cellStyleHeader, h);
            }

            rowIdx = 1;
            foreach (BuyingPriceMaintenance st in buyingPrice)
            {
                WriteDetailSingleData(wb, cellStyleDataDouble, st, sheet1, ++itemCount, rowIdx++, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter);
            }

            for (int i = 0; i < colh.Length; i++)
            {
                try
                {
                    sheet1.AutoSizeColumn(i);
                }
                catch
                {
                    sheet1.SetColumnWidth(i, 20 * 256);
                }
            }
        }

        public static void createCellText(
            IRow row,
            ICellStyle cellStyle,
            int colIdx, string txt)
        {
            ICell cell = row.CreateCell(colIdx);

            if (txt != null)
            {
                cell.SetCellValue(txt);
                cell.SetCellType(CellType.STRING);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;
            }
        }
        public static void createCellDouble(HSSFWorkbook wb, IRow row, ICellStyle cellStyle, int colIdx, double? val)
        {
            ICell cell = row.CreateCell(colIdx);

            if (val != null)
            {
                cell.SetCellValue((double)val);
                cell.SetCellType(CellType.NUMERIC);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;

            }
        }
        public void WriteDetailSingleData(HSSFWorkbook wb, ICellStyle cellStyleDouble, BuyingPriceMaintenance data, ISheet sheet1, int rowCount, int rowIndex, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter)
        {
            IRow row = sheet1.CreateRow(rowIndex);
            int col = 0;

            createCellText(row, cellStyleDataCenter, col++, data.MATERIAL_NO ?? "-");
            createCellText(row, cellStyleDataLeft, col++, data.MATERIAL_DESC ?? "-");
            createCellText(row, cellStyleDataLeft, col++, data.PACKING_TYPE);
            createCellText(row, cellStyleDataLeft, col++, data.PART_COLOR_SFX);
            createCellText(row, cellStyleDataLeft, col++, data.VALID_FROM);
            createCellDouble(wb, row, cellStyleDataRight, col++, Convert.ToDouble(data.PRICE));             
            createCellText(row, cellStyleDataLeft, col++, data.CURR_CD);
            createCellText(row, cellStyleDataCenter, col++, data.PRICE_STS);
            createCellText(row, cellStyleDataCenter, col++, data.RELEASE_STS);
            createCellText(row, cellStyleDataCenter, col++, data.PC_NO ?? "-");
            createCellText(row, cellStyleDataCenter, col++, data.SUPPLIER_CODE);
            createCellText(row, cellStyleDataLeft, col++, data.SUPPLIER_NAME ?? "-");
            createCellText(row, cellStyleDataCenter, col++, data.SOURCE_TYPE);
            createCellText(row, cellStyleDataCenter, col++, data.PROD_PURPOSE);
            createCellText(row, cellStyleDataCenter, col++, data.UOM ?? "-");
            createCellText(row, cellStyleDataCenter, col++, data.MM_DF ?? "-");
            createCellText(row, cellStyleDataCenter, col++, data.VALID_DT_TO);
            createCellText(row, cellStyleDataCenter, col++, data.CREATED_BY);
            createCellText(row, cellStyleDataCenter, col++, (data.CREATED_DT.HasValue)? data.CREATED_DT.Value.ToString("yyyy-MM-dd HH:mm:ss"): "");
            createCellText(row, cellStyleDataCenter, col++, data.CHANGED_BY);
            createCellText(row, cellStyleDataCenter, col++, (data.CHANGED_DT.HasValue)? data.CHANGED_DT.Value.ToString("yyyy-MM-dd HH:mm:ss"): "");
            createCellText(row, cellStyleDataCenter, col++, data.RELEASE_DT ?? "-");
            createCellText(row, cellStyleDataCenter, col++, data.PRICE_TYPE);
            createCellText(row, cellStyleDataCenter, col++, data.SOURCE_DATA);
            createCellText(row, cellStyleDataCenter, col++, data.DRAFT_DF ?? "-");
            createCellText(row, cellStyleDataLeft, col++, data.WRAP_REF_NO ?? "-");
            createCellText(row, cellStyleDataCenter, col++, data.CPP_FLAG ?? "-");
            createCellText(row, cellStyleDataLeft, col++, data.WRAP_BUYER_CD ?? "-");
            createCellText(row, cellStyleDataLeft, col++, data.BUSINESS_AREA ?? "-");
        }
        #endregion

        #region DownloadTemplate

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        public FileContentResult downloadTemplate(string fileName = null)
        {
            string filePath = string.Empty;
            
            byte[] documentBytes = null;
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "BuyingPriceTemplate.xls";
                filePath = Path.Combine(Server.MapPath("~/Views/BuyingPriceMaintenance/TemplateBuyingPrice/" + fileName));
            }
            else 
            {
                filePath = Path.Combine(_UploadDirectory, Path.GetFileName(fileName));
            }
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }
        #endregion

        #region UPload
        public ActionResult BuyingPriceUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Path.Combine(Sing.Me.AppData, "BuyingPriceMaintenance");
                if (!Directory.Exists(_uploadDirectory)) Directory.CreateDirectory(_uploadDirectory);
                return _uploadDirectory;
            }
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = Path.Combine(_UploadDirectory, e.UploadedFile.FileName);
                _FilePath=ParamsHelper.Unique(_FilePath);
                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = Path.GetFileName(_FilePath); 
            }
        }
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("BuyingPriceUpload", ValidationSettings, FileUploadComplete);
        }

        private BuyingPriceMaintenance AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            BuyingPriceMaintenance _UploadModel = new BuyingPriceMaintenance();

            string function = "91022";
            string module = "91022";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;

            long pid = 0;
            string tableFromTemp = "TB_T_MAT_PRICE";
            int sts = 0;
            string sheetName = "BuyingPrice";
            message = "Upload Buying Price";
            location = "BuyingPriceUpload.init";

            pid = ExceptionalHelper.PutLog(message, username, location, 0, "MPCS00002INF", "INF", module, function, 0);

            OleDbConnection excelConn = null;

            try
            {
                message = "Clear TB_T_MAT_PRICE";
                location = "BuyingPriceUpload.process";

                ExceptionalHelper.PutLog(message, username, location, pid, "MPCS00001INF", "INF", module, function, 0);

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });
                filePath = Path.Combine(_UploadDirectory, filePath);
                // read uploaded excel file,
                // get temporary upload table column name,
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();
                string[] fields = new string[] { "MATERIAL_NO", "PROD_PURPOSE", "SOURCE_TYPE", "SUPPLIER_CODE", "PART_COLOR_SFX", "PACKING_TYPE", "PRICE_STS", "PRICE", "CURR_CD", "VALID_FROM", "VALID_DT_TO" };
                int[] fmandat = new int[] { 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0 };
                int[] lengs = new int[] { 23, 4, 1, 6, 2, 1, 1, 15, 4, 11, 11 };
                StringBuilder bf = new StringBuilder();
                StringBuilder bw = new StringBuilder();
                for (int i = 0; i < fields.Length; i++)
                {
                    bf.AppendFormat(", iif(isnull(F{0}), null, cstr(F{0})) as {1}", i + 1, fields[i]);
                    if (i > 0) bw.AppendLine(" AND "); 
                    bw.AppendFormat(" isnull(F{0})", i + 1);
                }
                string OleCommand = @"select " +
                                    pid.ToString() + " as PROCESS_ID " +
                                    bf.ToString()
                                    + "\r\n from [" + sheetName + "$] \r\n" 
                                    + " where not ( \r\n"
                                    + bw.ToString()
                                    + "\r\n)";
                
                // ExceptionalHelper.PutLog(OleCommand, username, "QXl", pid, "MPCS00001INF", "INF", module, function, 0);
                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
               
                try
                {
                    excelDataAdapter.Fill(dtPatterns);

                    dtPatterns.Columns.Add("ROW_NO", typeof(int));
                    dtPatterns.Columns.Add("CREATED_BY", typeof(string));
                    dtPatterns.Columns.Add("CREATED_DT", typeof(string));
                    dtPatterns.Columns.Add("ERR_MSG", typeof(string));

                    ds.Tables.Add(dtPatterns);
                    DataRow rowDel = ds.Tables[0].Rows[0];

                    ds.Tables[0].Rows.Remove(rowDel);
                    for (int i = 0; i < dtPatterns.Rows.Count; i++)
                    {
                        dtPatterns.Rows[i]["ROW_NO"] = i + 1;
                        dtPatterns.Rows[i]["CREATED_BY"] = username;
                        dtPatterns.Rows[i]["CREATED_DT"] = DateTime.Now.ToString("yyyy-MM-dd");
                        StringBuilder errm = new StringBuilder("");
                        for (int j = 0; j < fields.Length; j++) {
                            string v = dtPatterns.Rows[i][fields[j]].ToString();
                            if (string.IsNullOrEmpty(v)) 
                            {
                                if (fmandat[i] != 0) 
                                    errm.AppendFormat("{0} must be filled", fields[j]);
                            }
                            else if (v.Length > lengs[j])
                            {
                                errm.AppendFormat("{0} max length is {1}. ", fields[j], lengs[j]);
                                dtPatterns.Rows[i][fields[j]] = v.Substring(lengs[j]);
                            }
                        }
                        decimal dPrice = 0;
                        if (!decimal.TryParse(dtPatterns.Rows[i]["PRICE"].ToString(), out dPrice))
                        {
                            errm.Append("PRICE must be in Numeric Format. ");
                            dtPatterns.Rows[i]["PRICE"] = null;
                        }                        

                        string dateFR = dtPatterns.Rows[i]["VALID_FROM"].ToString().Replace(".", "/");
                        DateTime dateFR1;
                        bool isDateValid = true;
                        if (!DateTime.TryParseExact(dateFR, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateFR1))
                        {
                            if (!DateTime.TryParseExact(dateFR, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateFR1))                            
                            {
                                isDateValid = false;
                                errm.Append("VALID_FROM must be in 'dd.MM.yyyy' format.");
                            }
                        }
                        dtPatterns.Rows[i]["VALID_FROM"] = (isDateValid) ? dateFR1.ToString("yyyy-MM-dd") : "1900-01-01";

                        string dateTO = dtPatterns.Rows[i]["VALID_DT_TO"].ToString().Replace(".", "/");
                        DateTime dateTO1 = new DateTime(1900, 01, 01);
                        if (!DateTime.TryParseExact(dateTO, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTO1))
                            if (!DateTime.TryParseExact(dateTO, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTO1))
                            {
                                dateTO1 = new DateTime(9999, 12, 31);
                            }
                        dtPatterns.Rows[i]["VALID_DT_TO"] = dateTO1.ToString("yyyy-MM-dd");
                        dtPatterns.Rows[i]["ERR_MSG"] = errm.ToString();
                    }
                }
                catch (Exception e)
                {
                    message = "Error : " + e.Message;
                    location = "BuyingPriceUpload.finish";
                    e.PutLog("", username, location, pid, eUNKNOWN, "ERR", module, function, 2);

                    throw new Exception(e.Message);
                }
                finally
                {
                    excelConn.Close();
                }

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("ROW_NO", "ROW_NO");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("MATERIAL_NO", "MAT_NO");
                    bulkCopy.ColumnMappings.Add("PROD_PURPOSE", "PROD_PURPOSE_CD");
                    bulkCopy.ColumnMappings.Add("SOURCE_TYPE", "SOURCE_TYPE");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CODE", "SUPP_CD");
                    bulkCopy.ColumnMappings.Add("PART_COLOR_SFX", "PART_COLOR_SFX");
                    bulkCopy.ColumnMappings.Add("PACKING_TYPE", "PACKING_TYPE");
                    bulkCopy.ColumnMappings.Add("PRICE_STS", "PRICE_STATUS");
                    bulkCopy.ColumnMappings.Add("PRICE", "PRICE_AMT");
                    bulkCopy.ColumnMappings.Add("CURR_CD", "CURR_CD");
                    bulkCopy.ColumnMappings.Add("VALID_FROM", "VALID_DT_FR");
                    bulkCopy.ColumnMappings.Add("VALID_DT_TO", "VALID_DT_TO");
                    bulkCopy.ColumnMappings.Add("CREATED_BY", "CREATED_BY");
                    bulkCopy.ColumnMappings.Add("CREATED_DT", "CREATED_DT");
                    bulkCopy.ColumnMappings.Add("ERR_MSG", "ERR_MSG"); 
                    try
                    {
                        message = "Insert Data to TB_T_MAT_PRICE";
                        location = "BuyingPriceUpload.process";

                        ExceptionalHelper.PutLog(message, username, location, pid, "MPCS00001INF", "INF", module, function, 0);

                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "BuyingPriceUpload.finish";

                        ex.PutLog("", username, location, pid, eUNKNOWN, "ERR", module, function, 2);

                        TempData["IsValid"] = "false;" + pid.ToString();
                        message = "Write data failed. " + pid.ToString();
                        return _UploadModel;
                    }

                }

                string validate = db.FetchStatement<string>("exec sp_Buying_Price_upload_batch @0, @1", new object[] { pid, username }).SingleOrDefault();
                
                if (validate == "error")
                {
                    string fnm = Path.GetFileNameWithoutExtension(filePath);
                    if (fnm.Length > 5)
                        fnm = fnm.Substring(0, fnm.Length - 5);                        
                    fnm = fnm + Path.GetExtension(filePath); 
                    string oPath = ParamsHelper.Unique(Path.Combine(_UploadDirectory, fnm));                    
                    TempData["IsValid"] = "false;" + pid.ToString() + ';' + Path.GetFileName(oPath);
                    Xlimex.Write<BuyingPriceUpload>(filePath, oPath, ";;;;;;;;;;;ERR_MSG", "ROW_NO", 
                        db.Fetch<BuyingPriceUpload>("GetBuyingPriceUploadError", new object[] { pid })
                        , 0);
                    sts = 1;
                    message = "Upload finish with error.";
                }
                else if (validate == "empty")
                {
                    TempData["IsValid"] = "empty";
                    sts = 0;
                    message = validate;
                }
                else
                {
                    TempData["IsValid"] = "true;" + pid.ToString();
                    sts = 6;
                    message = "Upload finish.";
                }

                location = "BuyingPriceUpload.finish";

                ExceptionalHelper.PutLog(message, username, location, pid, "MPCS00001INF", "INF", module, function, sts);
                RemoveCachedData();
            }
            catch (Exception e)
            {
                TempData["IsValid"] = "false;" + processID + ";" + e.Message;
                message = "Error : " + e.Message;
                location = "BuyingPriceUpload.finish";

                e.PutLog("", username, location, pid, eUNKNOWN, "ERR", module, function, 2);

                TempData["IsValid"] = "false;" + pid.ToString();
                message = e.Message + " " + pid.ToString();
            }
            finally
            {
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);
            }
            return _UploadModel;
        }
        #endregion

        #region Delete Data
        public ContentResult DeleteMasterBuyingPrice(BuyingPriceMaintenance data)
        {
            string resultMessage = "";
            string allowDelete = "";
            string MatNo = "";
            string SupCd = "";
            string SourceType = "";
            string prodPurposeCD = "";
            string validFrom = "";
            string partColorSfx = "";
            string packingType = "";

            MatNo = data.MATERIAL_NO.Substring(0, data.MATERIAL_NO.Length - 1);
            SupCd = data.SUPPLIER_CODE.Substring(0, data.SUPPLIER_CODE.Length - 1);
            SourceType = data.SOURCE_TYPE.Substring(0, data.SOURCE_TYPE.Length - 1);
            prodPurposeCD = data.PROD_PURPOSE.Substring(0, data.PROD_PURPOSE.Length - 1);
            validFrom = data.VALID_FROM.Substring(0, data.VALID_FROM.Length - 1);
            partColorSfx = data.PART_COLOR_SFX.Substring(0, data.PART_COLOR_SFX.Length - 1);
            packingType = data.PACKING_TYPE.Substring(0, data.PACKING_TYPE.Length - 1);

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                allowDelete = db.ExecuteScalar<String>("checkAllowDeleteBuyingPrice", new object[] { MatNo, SupCd, SourceType, prodPurposeCD, validFrom, partColorSfx, packingType });

                if (allowDelete == "ALLOWED")
                {
                    db.Execute("deleteMaterBuyingPrice", new object[] { MatNo, SupCd, SourceType, prodPurposeCD, validFrom, partColorSfx, packingType });
                    resultMessage = "Delete Data Finish Successfully.";
                    RemoveCachedData();
                }
                else
                {
                    resultMessage = "The record is not available for deletion";
                }
            }
            catch (Exception e)
            {
                resultMessage = "Error Occurred";

                e.PutLog("", AuthorizedUser.Username, "DeleteMasterBuyingPrice", 0, eUNKNOWN, "ERR", moduleId, functionId, 2);
            }

            return Content(resultMessage.ToString());
        }
        #endregion

        #region Download CSV
        public void WriteCSV<T>(IEnumerable<T> items, Stream stream)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(string.Join("; ", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join("; ", props.Select(p => p.GetValue(item, null))));
                }
            }
        }
        public FileContentResult DownloadBuyingPriceCSV(BuyingPriceMaintenance data)
        {
            Stream output = new MemoryStream();

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<BuyingPriceMaintenanceCSV> logList = new List<BuyingPriceMaintenanceCSV>();

            string fn = "BuyingPrice";
            byte[] documentBytes;

            try
            {
                if (!string.IsNullOrEmpty(data.PRICE_TYPE))
                {
                    if (data.PRICE_TYPE.Length > 20)
                    {
                        fn = "Revised Buying Price";
                    }
                    else
                    {
                        switch (data.PRICE_TYPE[0])
                        {
                            case 'B': fn = "Buying Price"; break;
                            case 'D': fn = "Draft Price"; break;
                            default: break;
                        }
                    }
                }

                logList = db.Fetch<BuyingPriceMaintenanceCSV>("BuyingPriceInquiry"
                 , new object[]
                        {
                            data.SUPPLIER_CODE, data.MATERIAL_NO, data.RELEASE_STS, data.PRICE_TYPE,
                            data.WRAP_BUYER_CD, data.MATERIAL_DESC, data.PRICE_STS, data.PACKING_TYPE, data.SOURCE_DATA ,
                            data.WRAP_REF_NO, data.DELETION_FLAG, data.SOURCE_TYPE, data.PC_NO, data.PROD_PURPOSE, data.CPP_FLAG,
                            data.VALID_FROM, data.VALID_DT_TO
                         });

                using (MemoryStream buffer = new MemoryStream())
                {
                    WriteCSV(logList, buffer);
                    documentBytes = buffer.GetBuffer();
                }

                return File(documentBytes, "text/csv", string.Format(fn + " {0}.csv", DateTime.Now.ToString("yyyyMMddHHmmss")));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
