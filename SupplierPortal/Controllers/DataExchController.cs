﻿/**
 * File: DataExchController.cs
 * Description : Controller class for Data Exch
 * Created by/date : FID.Taufika/22.10.2012
 * Modify : niit.yudha/22/11/2012 | Replacing User class with UserDataExch
 * Modify : niit.mulki/26/11/2012 | Adding AutoCC Condition, 
 * **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.IO;
using System.Net.Mail;
using Portal.Models;
using Portal.Models.DataExch;
using Portal.Controllers.Upload;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Compression;
using DevExpress.Web.ASPxUploadControl;

using Toyota.Common.Web.Html;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Configuration;
using System.Web.UI.WebControls;
using System.Net;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.Notification;
using System.Security.AccessControl;
using Cryptography;

namespace Portal.Controllers
{
    public class DataExchController : BaseController
    {
        private IDBContextManager DBManager;
        private FTPUpload vFtp = new FTPUpload();
        private const string fileServerPath = "/Portal/DataExchange/";
        private string downloadedPath = "";
        private string attachedFilePath = "";
        private string messageID;

        /// <summary>
        ///     constructor method for DataExchController
        /// </summary>
        public DataExchController(string pageTitle)
            : base("Data Exchange")
        {
            Boolean delMsgRet = this.DeleteMsg_RetentionDate();
            this.searchMessage(0);    
        }

        /// <summary>
        ///     constructor method for DataExchController
        /// </summary>
        public DataExchController()
            : base("Data Exchange")
        {
            /** do search when access page **/
            Boolean delMsgRet = this.DeleteMsg_RetentionDate();
            this.searchMessage(0);
        }
         
        /// <summary>
        ///     initialization for DataExch Model
        /// </summary>
        private void init()
        {
            DataExchModel newDataExchModel = new DataExchModel() 
            { 
                MessageTitle = String.Empty, 
                MessageBody = String.Empty, 
                MessageDate = DateTime.Now, 
                Filename = String.Empty, 
                MessageFrom = String.Empty, 
                MessageTo = String.Empty, 
                ErrorTitle = String.Empty, 
                ErrorBody = String.Empty, 
                ErrorFilename = String.Empty, 
                ErrorRetDate = String.Empty, 
                ErrorSendTo = String.Empty, 
                ErrorSendMail = String.Empty, 
                lookupName = String.Empty, 
                LookUpUserList = this.GetUserData() 
            };

            Model.AddModel(newDataExchModel);
            Model.AddModel(new InboxOutboxMsg()
            {
                InboxOutboxMsgModel = new List<DataExchModel>()
            });

            ViewData["UserName"] = this.GetUserName();
                                    
        }

        /// <summary>
        ///     multiple combobox callback method
        /// </summary>
        /// <returns></returns>
        public ActionResult FilterRowPartial(jQueryDataTableParamModel param)
        {
            /** initial partialview component **/
            this.init();
            Model.AddModel(this.GetUserData());
            
            /** combobox component behaviour process **/
            var totalUserData = Model.GetModel<DataExchModel>().LookUpUserList;
            IEnumerable<UserDataExch> filteredUserData = totalUserData;

            /** searching **/
            if(!string.IsNullOrEmpty(param.sSearch)){
                filteredUserData = this.SearchLookUpUser(param.sSearch);
            }
            var displayedUserData = filteredUserData;
            /** end searching **/

            /** sorting **/
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            Func<UserDataExch, string> orderingFunction = (c => sortColumnIndex == 1 ? c.Username :
                                                        sortColumnIndex == 2 ? c.FirstName :
                                                        sortColumnIndex == 3 ? c.LastName :
                                                        sortColumnIndex == 4 ? c.Email :
                                                        sortColumnIndex == 5 ? c.Division :
                                                        sortColumnIndex == 6 ? c.Departement :
                                                        c.Username);

            var sortDirection = Request["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filteredUserData = filteredUserData.OrderBy(orderingFunction);
            else
                filteredUserData = filteredUserData.OrderByDescending(orderingFunction);
            /** end sorting **/

            /** paging **/
            displayedUserData = filteredUserData
                                    .Skip(param.iDisplayStart)
                                    //.Take(5);
                                    .Take(param.iDisplayLength);
            /** end paging **/      


            var result = from c in displayedUserData
                         select new[] { c.Username, 
                                        c.FirstName, 
                                        c.LastName, 
                                        c.Email, 
                                        c.Division, 
                                        c.Departement 
                                      };
            
            return Json(new
                        {
                            sEcho = param.sEcho,
                            iTotalRecords = totalUserData.Count(),
                            iTotalDisplayRecords = filteredUserData.Count(),
                            aaData = result
                        }, JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        ///     inboxoutbox tab control onchanged method
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult inboxOutboxTab_Changed()
        {
            object test = Request.Params["tabIndex"];
            String currview = "";
            if (test.Equals("0"))
            {
                this.searchMessage(0);
                currview = "GridMsgInboxPartial";
            }
            else
            {
                this.searchMessage(1);
                currview = "GridMsgOutboxPartial";
            }

            return PartialView(currview, Model);
        }

        /// <summary>
        ///     inbox partialview callback method
        /// </summary>
        /// <returns></returns>
        public ActionResult inboxCallback()
        {
            this.searchMessage(0);
            return PartialView("GridMsgInboxPartial", Model);
        }

        /// <summary>
        ///     outbox partialview callback method
        /// </summary>
        /// <returns></returns>
        public ActionResult outboxCallback()
        {
            this.searchMessage(1);
            return PartialView("GridMsgOutboxPartial", Model);
        }
        
        /// <summary>
        ///     search message based on type (0: inbox, 1: outbox) method
        /// </summary>
        /// <param name="type"></param>
        public void searchMessage(int type)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_SCP);
            InboxOutboxMsg listTemp = new InboxOutboxMsg();
            string receiverUser;
            string senderUser;
            
            if(type == 0)
            {
                receiverUser = String.Format("%{0}%", UserID);
                //receiverUser = "%fid.asvian%";
                senderUser = "%%";
            } else
            {
                receiverUser = "%%";
                senderUser = String.Format("%{0}%", AuthorizedUser.Username);
                //senderUser = "%fid.%";
            }

            var result = db.Query<DataExchModel>("ShowAllMessage", new Object[] { type, receiverUser, senderUser });
            
            foreach (var data in result)
            {
                DataExchModel listResultRow = new DataExchModel()
                {
                    No = data.No,
                    MessageID = data.MessageID,
                    MessageTitle = data.MessageTitle,
                    MessageBody = data.MessageBody,
                    MessageDate = data.MessageDate,
                    Filename = data.Filename,
                    MaxRetDate = data.MaxRetDate,
                    MessageFrom = data.MessageFrom,
                    MessageTo = data.MessageTo,
                    DownloadUsername = data.DownloadUsername
                };

                listTemp.InboxOutboxMsgModel.Add(listResultRow);
            }

            db.Close();
            Model.AddModel(listTemp);
        }
                        
        /// <summary>
        ///     delete selected message inbox/outbox method
        /// </summary>
        /// <returns></returns>
        public ActionResult Delete()
        {
            DBManager = ProviderResolver.GetInstance().Get<IDBContextManager>();
            string selectedMsg_ID = Request.Params["selectedMsg_Row"];
            if (selectedMsg_ID != null)
            {
                string[] listSelectedMsg_ID = selectedMsg_ID.Split(',');
                int delResult = 0;

                for (int i = 0; i < listSelectedMsg_ID.Count(); i++)
                {
                    delResult = DBManager.GetContext(DBContextNames.DB_SCP).Execute("DeleteMessage",
                                                                                    new Object[] { listSelectedMsg_ID[i] });
                    if (delResult == 0)
                    {
                        /** TO-DO WHEN ERROR **/
                    }
                }
            }
            this.searchMessage(0);
            return RedirectToAction("Index", "DataExch");
        }
        
        /// <summary>
        ///     delete message based on retention date method
        /// </summary>
        /// <returns></returns>
        public Boolean DeleteMsg_RetentionDate()
        {
            DBManager = ProviderResolver.GetInstance().Get<IDBContextManager>();
            DateTime todayDate = DateTime.Now;
            Boolean result = true;
            
            int delResult = DBManager.GetContext(DBContextNames.DB_SCP).Execute("DeleteMessageByRetentionDate",
                                                                                new Object[] { todayDate.ToString("yyyy-MM-dd") });
            if (delResult == 0)
            {
                /** TO-DO WHEN ERROR **/
                result = false;
            }

            return result;
        }
        
        /// <summary>
        ///     send or upload view callback method
        /// </summary>
        /// <returns></returns>
        public ActionResult sendOrUpload()
        {
            this.init();
            return View("sendOrUpload", Model);
        }
        
        /// <summary>
        ///     resend selected message handler method
        /// </summary>
        /// <param name="Title"></param>
        /// <param name="Body"></param>
        /// <param name="Filename"></param>
        /// <returns></returns>
        public ActionResult resendMessage_Action(String Title, String Body, String Filename)
        {
            string maxRet = Request.Params["MaxRetDate"];
            string sendTo = Request.Params["SendTo"];
            string expiredFlag = Request.Params["ExpiredFlag"];
            string notifyDownloadFlag = Request.Params["NotifyDownloadFlag"];

            FormCollection formTemp = new FormCollection();
            return (this.sendMessage(Title, Body, maxRet, formTemp, Filename, sendTo, 1,
                                     Convert.ToBoolean(expiredFlag), Convert.ToBoolean(notifyDownloadFlag)));
        }
        
        /// <summary>
        ///     send selected message handler method
        /// </summary>
        /// <param name="Title"></param>
        /// <param name="Body"></param>
        /// <param name="MaxRetDate"></param>
        /// <param name="fileUpload"></param>
        /// <param name="UsernameComboBox"></param>
        /// <returns></returns>
        public ActionResult sendMessage_Action(String Title, String Body, String MaxRetDate,
                                               FormCollection form, String UsernameComboBox)
        {
            bool notifyDownloadFlag = CheckBoxExtension.GetValue<Boolean>("notifyDownloadStatus");
            bool expiredFlag = CheckBoxExtension.GetValue<Boolean>("expiredStatus");
            return this.sendMessage(Title, Body, MaxRetDate, form, "", UsernameComboBox, 0, notifyDownloadFlag, expiredFlag);
        }

        /// <summary>
        ///     send message validation and ftp configuration
        /// </summary>
        /// <param name="Title"></param>
        /// <param name="Body"></param>
        /// <param name="MaxRetDate"></param>
        /// <param name="form"></param>
        /// <param name="sendToComboBox"></param>
        /// <param name="statusSend"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult sendMessage(String Title, String Body, String MaxRetDate, FormCollection form,
                                        String filename, String sendToComboBox, int statusSend,
                                        bool notifyDownloadFlag, bool expiredFlag)
        {
            DBManager = ProviderResolver.GetInstance().Get<IDBContextManager>();
            DataExchModel addData = new DataExchModel();
            DateTime addTime = DateTime.Now;
            String sender = AuthorizedUser.Username;
            String password = RandomizeGenerator.Generate(8);
            String zipFilename = String.Format("TMMIN_{0}.zip", addTime);
            byte[] Hash = new byte[0];

            Boolean valid = true;
            Boolean resUpload = false;
            Boolean resSending = false;

            /********* Fields Validation *********/
            if (String.IsNullOrEmpty(Title))
            {
                valid = false;
                addData.ErrorTitle = "Message Title is required.";
            } else { addData.ErrorTitle = ""; }
            if (String.IsNullOrEmpty(Body))
            {
                valid = false;
                addData.ErrorBody = "Message Body is required.";
            } else { addData.ErrorBody = ""; }
            if ((statusSend == 0) && (form == null || form.Count == 0))
            {
                valid = false;
                addData.ErrorFilename = "  Filename is required.";
            }
            else 
            {
                if ((statusSend == 1) && (String.IsNullOrEmpty(filename)))
                {
                    valid = false;
                    addData.ErrorFilename = "  Filename is required.";
                }
                else
                {
                    addData.ErrorFilename = "";
                }
            }

            if (!String.IsNullOrEmpty(MaxRetDate))
            {
                if ( DateTime.ParseExact(MaxRetDate, "d", null) <= DateTime.Now )
                {
                    valid = false;
                    addData.ErrorRetDate = "Retention Date must be greater than Today.";
                }
                else { addData.ErrorRetDate = ""; }
            }
            if (String.IsNullOrEmpty(sendToComboBox))
            {
                valid = false;
                addData.ErrorSendTo = "Send To is required.";
            } else { addData.ErrorSendTo = ""; }
            /********* End of Fields Validation *********/

            ///** for testing only**/
            //if (sendToComboBox.Contains("fid"))
            //{
                if (valid == true) 
                {
                    if (statusSend == 0)
                    {
                        /** upload attachment files **/
                        zipFilename = zipFilename.Replace("/", "").Replace(":", "").Replace(" ", "");
                        resUpload = this.UploadFileToServer(form, zipFilename, password, ref Hash);
                    }
                    else
                    {
                        zipFilename = filename;
                        resUpload = true;
                    }

                    if(resUpload == true)
                    {
                        /// Do Encrypt
                        string sadf245nv7bjr = "";
                        using (Symmetric sym = new Symmetric() { Key = Hash })
                        {
                            sadf245nv7bjr = Convert.ToBase64String(sym.Encrypt(System.Text.ASCIIEncoding.ASCII.GetBytes(password)));
                        }

                        String[] sendto = sendToComboBox.Split(',');
                        for (int i = 0; i < sendto.Count(); i++)
                        {                   

                            /** Insert New Inbox to Database **/
                            int insertInbox = DBManager.GetContext(DBContextNames.DB_SCP)
                                              .Execute("AddMessage", new Object[] {   Title, 
                                                                                      Body,
                                                                                      zipFilename,
                                                                                      MaxRetDate,
                                                                                      sendto[i].Trim(),
                                                                                      sadf245nv7bjr,
                                                                                      (byte) 0,
                                                                                      (byte) 0,
                                                                                      expiredFlag,
                                                                                      notifyDownloadFlag,
                                                                                      (byte) 0,
                                                                                      sender,
                                                                                      addTime
                                                                                  });
                            /* Get Message ID who was inserted*/
                            messageID = DBManager.GetContext(DBContextNames.DB_SCP).SingleOrDefault<string>("GetLastMessage", null);

                            int insertPassword = DBManager.GetContext(DBContextNames.DB_SCP)
                                                  .Execute("AddMessage", new Object[] {   "Password", 
                                                                                          String.Format("You have an attachment file. Password: {0}", password),
                                                                                          zipFilename,
                                                                                          MaxRetDate,
                                                                                          sendto[i].Trim(),
                                                                                          password,
                                                                                          (byte) 0,
                                                                                          (byte) 0,
                                                                                          expiredFlag,
                                                                                          notifyDownloadFlag,
                                                                                          (byte) 0,
                                                                                          sender,
                                                                                          addTime
                                                                                      });

                            /** Sending Msg using FTP **/
                            resSending = this.sendMailDataExch(Title, Body, zipFilename,
                                                               addTime.ToString(), sender,
                                                               sendto[i].Trim(), password);

                            if (insertInbox == 1 && insertPassword == 1)
                            {
                                /** TO-DO If Insert Success **/
                                
                            }
                        }
                    }
                }
                else resSending = false;
            //}else resSending = true;
                        
            if (valid == true && resSending == true)
            {
                int sendResult = DBManager.GetContext(DBContextNames.DB_SCP)
                                          .Execute("AddMessage", new Object[] {   Title, 
                                                                                  Body,
                                                                                  zipFilename,
                                                                                  MaxRetDate,
                                                                                  sendToComboBox,
                                                                                  password,
                                                                                  (byte) 1,
                                                                                  (byte) 0,
                                                                                  expiredFlag,
                                                                                  notifyDownloadFlag,
                                                                                  (byte) 0,
                                                                                  sender,
                                                                                  addTime
                                                                              });

                if (sendResult == 1)
                {
                    addData.MessageTitle = this.convertToString(Title);
                    addData.MessageBody = this.convertToString(Body);
                    addData.Filename = this.convertToStringFile(zipFilename);
                    addData.MessageTo = this.convertToString(sendToComboBox);
                    addData.MessageFrom = this.convertToString(sender);
                    addData.FilePassword = this.convertToString(password);

                    Model.AddModel(addData);

                    //Add new notification
                    NotificationProvider nProvider = NotificationProvider.GetInstance();
                    nProvider.Insert("New Message", String.Format("{0}{1}","You have 1 mail from ", UserID), sendToComboBox, 
                                     String.Empty, 1, 1, String.Empty,
                                     AuthorizedUser.Username, addTime, AuthorizedUser.Username, addTime);
                    
                }

                return PartialView("Confirm", Model);                
            }
            else
            {
                addData.MessageTitle = this.convertToString(Title);
                addData.MessageBody = this.convertToString(Body);
                addData.MessageFrom = this.convertToString(sender);
                addData.Filename = this.convertToStringFile(zipFilename);
                addData.MessageTo = this.convertToString(sendToComboBox);
                addData.FilePassword = this.convertToString(password);

                addData.savedMaxRetDate = this.convertToString(MaxRetDate);
                addData.ExpiredFlag = expiredFlag;
                addData.NotifyDownloadFlag = notifyDownloadFlag;
                
                if (resSending == false) addData.ErrorSendMail = "Sending Mail failed.";
                else addData.ErrorSendMail = "";

                addData.LookUpUserList = this.GetUserData();
                Model.AddModel(addData);
                ViewData["UserName"] = this.GetUserName();

                return View("sendOrUpload", Model);
            }
        }
        
        /// <summary>
        ///     send message
        /// </summary>
        /// <param name="MsgTitle"></param>
        /// <param name="MsgBody"></param>
        /// <param name="filename"></param>
        /// <param name="MsgDate"></param>
        /// <param name="MsgFrom"></param>
        /// <param name="MsgTo"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        protected Boolean sendMailDataExch(String MsgTitle, String MsgBody, String filename,
                                           String MsgDate, String MsgFrom, String MsgTo, String password)
        {
            bool result = false;
            List<string> userAutoCC = new List<string>();
            String SMTPSERVER = System.Configuration.ConfigurationManager.AppSettings["EmailHost"];
            String MailPath = String.Format(System.Configuration.ConfigurationManager.AppSettings["DataExchEmailTemplate"], Server.MapPath("~"));
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_TMMIN_ROLE);

            var resEmailFrom = db.SingleOrDefault<UserDataExch>("GetUserEmail", new Object[] { MsgFrom });
            var resEmailTo = db.SingleOrDefault<UserDataExch>("GetUserEmail", new Object[] { MsgTo });

            //config autoCC email
            //get current position -> get autoCC email 
            string emailContain = System.Configuration.ConfigurationManager.AppSettings["DataExchEmailDomain"];

            var resCurrPosition = db.SingleOrDefault<UserDataExch>("GetUserPositionInfo", new Object[] { AuthorizedUser.Username });
            if (!string.IsNullOrEmpty(resCurrPosition.PositionID))
            {
                if (resCurrPosition.PositionID.Equals("ST"))
                {
                    var resGet = db.Query<User>("GetUserPositionSH", new Object[]{ resCurrPosition.SectionID,
                                                                                   resCurrPosition.DivisionID,
                                                                                   resCurrPosition.DepartementID});

                    if (!string.IsNullOrEmpty(resCurrPosition.PositionID))
                    {
                        if (resCurrPosition.PositionID.Equals("ST"))
                        {
                            resGet = db.Query<User>("GetUserPositionSH", new Object[]{ resCurrPosition.SectionID,
                                                                                       resCurrPosition.DivisionID,
                                                                                       resCurrPosition.DepartementID});

                            foreach (var data in resGet)
                            {
                                if (!resEmailTo.Email.Contains(emailContain))
                                {
                                    //userAutoCC.Add("niit.mftampubolon@toyota.co.id");
                                    userAutoCC.Add(data.Email);
                                }
                            }
                        }
                        else if (resCurrPosition.PositionID.Equals("SH"))
                        {
                            resGet = db.Query<User>("GetUserPositionDPH", new Object[]{ resCurrPosition.DivisionID,
                                                                                        resCurrPosition.DepartementID});
                            foreach (var data in resGet)
                            {
                                if (!resEmailTo.Email.Contains(emailContain))
                                {
                                    userAutoCC.Add(data.Email);
                                }
                            }
                        }
                        else if (resCurrPosition.PositionID.Equals("DPH"))
                        {
                            resGet = db.Query<User>("GetUserPositionDH", new Object[] { resCurrPosition.DepartementID });
                            foreach (var data in resGet)
                            {
                                if (!resEmailTo.Email.Contains(emailContain))
                                {
                                    userAutoCC.Add(data.Email);
                                }
                            }
                        }
                    }

                    Boolean sending = this.ComposeAdminEmailNotification(MsgTitle, MsgBody, filename, MsgDate,
                                                                        this.convertToString(resEmailFrom.Email),
                                                                        this.convertToString(resEmailTo.Email),
                                                                        userAutoCC,
                                                                        MailPath,
                                                                        SMTPSERVER);
                    if (sending == true)
                    {
                        MailPath = String.Format(System.Configuration.ConfigurationManager.AppSettings["DataExchEmailPasswordTemplate"],
                                                 Server.MapPath("~"));
                        result = this.ComposeEmailPassword(filename, MsgDate,
                                                           this.convertToString(resEmailFrom.Email),
                                                           this.convertToString(resEmailTo.Email),
                                                           password, userAutoCC, MailPath, SMTPSERVER);
                    }                    
                }
            }

            db.Close();
            return result;
        }
    

        /// <summary>
        ///     compose main message
        /// </summary>
        /// <param name="MsgTitle"></param>
        /// <param name="MsgBody"></param>
        /// <param name="filename"></param>
        /// <param name="MsgDate"></param>
        /// <param name="MsgFrom"></param>
        /// <param name="MsgTo"></param>
        /// <param name="MailPath"></param>
        /// <param name="SMTPSERVER"></param>
        /// <returns></returns>
        public bool ComposeAdminEmailNotification(String MsgTitle, String MsgBody, String filename,
                                                  String MsgDate, String MsgFrom, String MsgTo, List<string> userAutoCC,
                                                  String MailPath, String SMTPSERVER)
        {
            bool result = false;
            try
            {
                
                string msgEmailLink = "";
                UrlHelper urlHelper = new UrlHelper(this.HttpContext.Request.RequestContext);
                msgEmailLink = urlHelper.Action("ViewDetail", "DataExch", new { msgID = messageID, type = "0" }, this.HttpContext.Request.Url.Scheme);
                String MailBody;
                using (TextReader reader = new StreamReader(MailPath))
                {
                    MailBody = reader.ReadToEnd();
                }

                MailBody = ReplaceString(MailBody, "[MSG_BODY]", MsgBody);
                MailBody = ReplaceString(MailBody, "[MSG_FILE_ATTACH]", filename);
                MailBody = ReplaceString(MailBody, "[MSG_DATE]", MsgDate);
                MailBody = ReplaceString(MailBody, "[MSG_LINK_DOWNLOAD]", msgEmailLink);               
                  
                using (MailMessage MailMessage = new MailMessage() { IsBodyHtml = true, Body = MailBody })
                {
                    if (MsgTo != null && MsgTo != "")
                    {
                        if (MsgTo.Contains("@"))
                        {
                            MailMessage.To.Add(MsgTo);
                            for (int i = 0; i < userAutoCC.Count(); i++)
                            {
                                MailMessage.CC.Add(userAutoCC[i]);
                            }
                        }
                    }

                    MailMessage.Subject = MsgTitle;
                    //mailMessage.Attachments.Add(new MailAttachment(Server.MapPath(filename)));

                    MailAddress sender = new MailAddress(MsgFrom);
                    MailMessage.Sender = sender;
                    MailMessage.From = sender;

                    if (MailMessage.To != null && MailMessage.To.Count() > 0)
                    {
                        using (SmtpClient client = new SmtpClient(SMTPSERVER))
                        {
                            client.Send(MailMessage);
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                BaseHandleException(ex, Toyota.Common.Web.Logging.EventType.Error);
            }
            return result;
        }

        /// <summary>
        ///     compose password message
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="MsgDate"></param>
        /// <param name="MsgFrom"></param>
        /// <param name="MsgTo"></param>
        /// <param name="password"></param>
        /// <param name="MailPath"></param>
        /// <param name="SMTPSERVER"></param>
        /// <returns></returns>
        public bool ComposeEmailPassword(String filename, String MsgDate, String MsgFrom, String MsgTo, String password,
                                         List<string> userAutoCC, String MailPath, String SMTPSERVER)
        {
            bool result = false;
            try
            {
                const String MsgBody = "You have an attachment file.";
                string MailBody;
                using (TextReader reader = new StreamReader(MailPath))
                {
                    MailBody = reader.ReadToEnd();
                }

                MailBody = ReplaceString(MailBody, "[MSG_BODY]", MsgBody);
                MailBody = ReplaceString(MailBody, "[MSG_FILE_ATTACH]", filename);
                MailBody = ReplaceString(MailBody, "[MSG_PASSWORD]", password);
                MailBody = ReplaceString(MailBody, "[MSG_DATE]", MsgDate);
                 
                using (MailMessage MailMessage = new MailMessage() { IsBodyHtml = true, Body = MailBody })
                {
                    if (MsgTo != null && MsgTo != "")
                    {
                        if (MsgTo.Contains("@"))
                        {
                            MailMessage.To.Add(MsgTo);
                            for (int i = 0; i < userAutoCC.Count(); i++)
                            {
                                MailMessage.CC.Add(userAutoCC[i]);
                            }
                        }
                    }

                    MailMessage.Subject = "Password";
                    MailAddress sender = new MailAddress(MsgFrom);
                    MailMessage.Sender = sender;
                    MailMessage.From = sender;

                    if (MailMessage.To != null && MailMessage.To.Count() > 0)
                    {
                        using (SmtpClient client = new SmtpClient(SMTPSERVER))
                        {
                            client.Send(MailMessage);
                        }
                        result = true;               
                    }
                }
            }
            catch (Exception ex)
            {
                BaseHandleException(ex, Toyota.Common.Web.Logging.EventType.Error);
            }
            return result;
        }

        private static string ReplaceString(string source, string Find, string ReplaceWith)
        {
            return source.Replace(Find, ReplaceWith);
        }

        /// <summary>
        ///     convert files uploaded to a ZIP file (with password) and upload ZIP file to FTP Server
        /// </summary>
        /// <param name="form"></param>
        /// <param name="msgID"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Boolean UploadFileToServer(FormCollection form, string zipFilename, string password, ref byte[] Hash)
        {
            Boolean resultConvert = false;
            Boolean resultUpload = false;
            ICompression zipCreator = ZipCompress.GetInstance();

            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("fileUpload", UploadModel.ValidationSettings);
            Session["Storage"] = new UploadControlFilesStorage();
            
            string[] filenames = new string[files.Length];
            string[] str = new string[files.Length];
            string msg = string.Empty;
            
            /** create Temporary Directory ZIP file consist of uploaded files **/
            string tmpPath = AuthorizedUser.Username + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString();
                       
            string temporaryPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + "\\tmp" + tmpPath + "zip\\";

            System.IO.Directory.CreateDirectory(temporaryPath);

            for (int i = 0; i < files.Length; i++)
            {
                files[i].SaveAs(temporaryPath + files[i].FileName);
            }

            filenames = System.IO.Directory.GetFiles(temporaryPath);
            resultConvert = zipCreator.Compress(filenames, temporaryPath+ zipFilename, password, ref msg);

            /** upload ZIP file to FTP server **/
            if (resultConvert == true)
            {
                if (!vFtp.FtpUpload(fileServerPath, zipFilename, temporaryPath, ref msg))
                {
                    resultUpload = false;
                    /** TO-DO **/
                }
                else
                {
                    IHash hash = new SHA512Hash();
                    Hash = hash.ComputeHash(temporaryPath + zipFilename);
                    System.IO.Directory.Delete(temporaryPath, true);
                    resultUpload = true;
                    hash.Dispose();
                    /** TO-DO **/
                }
            }

            return resultUpload;
        }

        /// <summary>
        ///     rename filename
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public Boolean RenameFileServer(String filename)
        {
            Boolean res = false;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_SCP);
            var msgIDRetrieved = db.SingleOrDefault<string>("GetMsgIDByFile", new Object[] { filename });
            string msg = string.Empty;
            string newFilename = filename;

            if (!string.IsNullOrEmpty(msgIDRetrieved))
            {
                /** Update filename in Server **/
                Boolean updateServer = vFtp.FtpRenameFile("", filename, newFilename.Replace("",""), ref msg);
                if( updateServer == true)
                {
                    /** Update filename in DB **/
                    int updateFilename = DBManager.GetContext(DBContextNames.DB_SCP)
                                                  .Execute("UpdateFileName", new Object[] { newFilename, msgIDRetrieved });

                    if (updateFilename == 1) res = true;
                    else res = false;
                }
            }

            db.Close();
            return res;
        }

        /// <summary>
        ///     download file method
        /// </summary>
        /// <param name="filename"></param>
        public ActionResult Download(string Filename, string FilePassword)
        {
            string downloadUsernameList = "";
            string msg = string.Empty;
            string msgId = Request.Params["MessageID"];
            string msgType = Request.Params["MessageType"];
            downloadedPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonDocuments) + "\\DownloadedFiles\\";
            System.IO.Directory.CreateDirectory(downloadedPath);

            string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, fileServerPath);
            Boolean resDownload = vFtp.FtpDownload(String.Format("{0}/{1}", serverPath, Filename), 
                                                   String.Format("{0}\\{1}",downloadedPath, Filename), 
                                                   ref msg);

            //get current downloaded username
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_SCP);
            var result = db.SingleOrDefault<DataExchModel>("GetDownloadedUsername", new Object[] { FilePassword });

            downloadUsernameList = String.Format("{0},{1}",this.convertToString(result.DownloadUsername), AuthorizedUser.Username);

            
            if(string.IsNullOrEmpty(result.DownloadUsername))
                downloadUsernameList = String.Format("{0}", UserID);
            else
                downloadUsernameList = String.Format("{0},{1}", this.convertToString(result.DownloadUsername), UserID);

            //update downloaded username
            int updateDownloadedUsername = DBManager.GetContext(DBContextNames.DB_SCP)
                                                    .Execute("UpdateDownloadFlag", new Object[] { downloadUsernameList, FilePassword });

            db.Close();
            // Prompts user to save file
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/plain";
            response.AddHeader("Content-Disposition", "attachment; filename=" + Filename + ";");
            response.TransmitFile(String.Format("{0}/{1}", downloadedPath, Filename));
            response.Flush();

            if (msgType.Equals("1"))
                return this.ViewDetailWithReSend(msgId, msgType);
            else
                return this.ViewDetail(msgId, msgType);
        }

        /// <summary>
        ///     cancel button callback
        /// </summary>
        /// <returns></returns>
        public ActionResult Cancel()
        {
            return RedirectToAction("Index", "DataExch");
        }
        
        /// <summary>
        ///     viewdetail partialview caller
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ViewResult ViewDetail(String msgID, String type)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_SCP);
            var result = db.SingleOrDefault<DataExchModel>("ViewMessage", new Object[] { msgID });
            DataExchModel listResultRow = new DataExchModel();

            if(result != null){
                listResultRow.No = result.No;
                listResultRow.MessageID = result.MessageID;
                listResultRow.MessageTitle = result.MessageTitle;
                listResultRow.MessageBody = result.MessageBody;
                listResultRow.MessageDate = result.MessageDate;
                listResultRow.Filename = result.Filename;
                listResultRow.MessageFrom = result.MessageFrom;
                listResultRow.MessageTo = result.MessageTo;
                listResultRow.FilePassword = result.FilePassword;
                listResultRow.MaxRetDate = result.MaxRetDate;
                listResultRow.MessageType = result.MessageType;
                listResultRow.ExpiredFlag = result.ExpiredFlag;
                listResultRow.NotifyDownloadFlag = result.NotifyDownloadFlag;
            };

            Model.AddModel(listResultRow);

            db.Close();
            return View("View", Model);
        }

        /// <summary>
        ///     viewDetailwithResend partialview caller
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ViewResult ViewDetailWithReSend(String msgID, String type)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_SCP);
            var result = db.SingleOrDefault<DataExchModel>("ViewMessage", new Object[] { msgID });
            DataExchModel listResultRow = new DataExchModel();

            if (result != null)
            {
                listResultRow.No = result.No;
                listResultRow.MessageID = result.MessageID;
                listResultRow.MessageTitle = result.MessageTitle;
                listResultRow.MessageBody = result.MessageBody;
                listResultRow.MessageDate = result.MessageDate;
                listResultRow.Filename = result.Filename;
                listResultRow.MessageFrom = result.MessageFrom;
                listResultRow.MessageTo = result.MessageTo;
                listResultRow.FilePassword = result.FilePassword;
                listResultRow.MaxRetDate = result.MaxRetDate;
                listResultRow.MessageType = result.MessageType;
                listResultRow.ExpiredFlag = result.ExpiredFlag;
                listResultRow.NotifyDownloadFlag = result.NotifyDownloadFlag;
            };

            Model.AddModel(listResultRow);
            db.Close();
            return View("ViewWithReSend", Model);
        }

        /// <summary>
        ///     viewdetail partialview caller
        /// </summary>
        /// <param name="Title"></param>
        /// <param name="Body"></param>
        /// <param name="Filename"></param>
        /// <param name="FilePassword"></param>
        /// <returns></returns>
        public ViewResult ViewDetail_NoQuery(String Title, String Body, String Filename, 
                                             String MsgID, String MsgType, String FilePassword)
        {
            DataExchModel listResultRow = new DataExchModel();
            listResultRow.MessageTitle = Title;
            listResultRow.MessageBody = Body;
            listResultRow.Filename = Filename;
            listResultRow.FilePassword = FilePassword;
            listResultRow.MessageID = int.Parse(MsgID);
            listResultRow.MessageType = int.Parse(MsgType);
                
            Model.AddModel(listResultRow);

            return View("ViewWithReSend", Model);
        }

        /// <summary>
        ///     searching user in grid filtering
        /// </summary>
        /// <param name="lookupName"></param>
        /// <returns></returns>
        public List<UserDataExch> SearchLookUpUser(string lookupName)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_TMMIN_ROLE);
            List<UserDataExch> LookUpUserList = new List<UserDataExch>();
            string searchName = String.Format("%{0}%", lookupName);
            var result = db.Query<UserDataExch>("GetAllUserCompleteData_Search", new Object[] { searchName, searchName });
            
            foreach (var data in result)
            {
                UserDataExch lookupUser = new UserDataExch()
                { 
                    Username = data.Username, 
                    FirstName = data.FirstName, 
                    LastName = data.LastName, 
                    Email = data.Email,
                    Division = data.Division,
                    Departement = data.Departement 
                };

                LookUpUserList.Add(lookupUser);
            }

            db.Close();
            return LookUpUserList;
        }
        
        /// <summary>
        ///     get username data for component
        /// </summary>
        /// <returns></returns>
        public IEnumerable GetUserName()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_TMMIN_ROLE);
            var result = db.Query<User>("GetAllUserCombobox");

            //var list = result.ToList();
            //User selectAll = new User(){
            //    Username = "select all",
            //    RegistrationNumber = "",
            //    Password = "",
            //    FirstName = "",
            //    LastName = "",
            //    Email = ""
            //};

            //list.Insert(0, selectAll);

            //return list;
            db.Close();
            return result;
        }

        /// <summary>
        ///     get userdata for component
        /// </summary>
        /// <returns></returns>
        public List<UserDataExch> GetUserData()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_TMMIN_ROLE);
            var result = db.Query<UserDataExch>("GetAllUserCompleteData");

            List<UserDataExch> LookUpUserList = new List<UserDataExch>();
            foreach(var data in result)
            {
                UserDataExch lookupUser = new UserDataExch()
                { 
                    Username = data.Username, 
                    FirstName = data.FirstName, 
                    LastName = data.LastName, 
                    Email = data.Email, 
                    Division = data.Division, 
                    Departement = data.Departement
                };

                LookUpUserList.Add(lookupUser);
            }

            db.Close();
            return LookUpUserList;
        }
        
        /// <summary>
        ///     startup method
        /// </summary>
        protected override void StartUp()
        {
            /** TO-DO **/   
        }

        /// <summary>
        ///     init method
        /// </summary>
        protected override void Init()
        {
            /** TO-DO **/       
        }

        /// <summary>
        ///     set FTP settings
        /// </summary>
        //public void setFTPSettings()
        //{
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    var result = db.Query<FTPModel>("GetFTPSettings", new Object[]{ "Ftp" });

        //    foreach (var data in result)
        //    {
        //        vFtp.Setting.IP = data.FtpIpAddress.ToString();
        //        vFtp.Setting.UserID = data.FtpUsername.ToString();
        //        vFtp.Setting.Password = data.FtpPassword.ToString();
        //    }

        //    //vFtp.Setting.IP = "10.16.20.73";
        //    //vFtp.Setting.UserID = "Adm1nSCP4";
        //    //vFtp.Setting.Password = "SCP4\\Administrator";
        //}
                
        /// <summary>
        ///     convert to string (prevent null value)
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        public String convertToString(String var)
        {
            String res = "";
            if(!String.IsNullOrEmpty(var)){
                res = var;
            }
            return res;
        }

        /// <summary>
        ///     convert to string for file (prevent null or blank value)
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        public String convertToStringFile(String var)
        {
            String res = " ";
            if (!String.IsNullOrEmpty(var))
            {
                res = var;
            }
            return res;
        }

        public ActionResult BodyMailPartial()
        {
            return PartialView("BodyMailPartial");
        }
        public ActionResult sendOrUploadPartial()
        {
            return PartialView("sendOrUploadPartial");
        }
    }
}
