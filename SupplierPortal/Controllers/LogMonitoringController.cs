﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.LogMonitoring;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Compression;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;



namespace Portal.Controllers
{
    public class LogMonitoringController : BaseController
    {
        public LogMonitoringModel modelExport;
        public LogMonitoringController()
            : base("LogMonitoring", "Log Monitoring")
        {

        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void StartUp()
        {

        }

       
        /// <summary>
        /// 
        public ActionResult PopupHeaderLogMonitoringDetail() //for refresh
        {
            return PartialView("PartialHeaderLogMonitoringDetail", Model);
        }

       /* public ActionResult PopupGridLogMonitoringDetail()
        {
            string id = "";
            id = Request.Params["ProcessID"];


            //ViewData["LogDetailData"] = LogMonitoringDetails(id);
            //return PartialView("GridIndexDetails", ViewData["LogDetailData"]);
        }*/



        /// </summary>
        /// <returns></returns>



        private List<functionMaster> GetAllFunction()
        {
            List<functionMaster> listFunctionModel = new List<functionMaster>();
            IDBContext db = DbContext;
            listFunctionModel = db.Fetch<functionMaster>("GetAllFunctionGrid", new object[] { });
            db.Close();

            return listFunctionModel;
        }

        private List<StatusMaster> GetAllStatus()
        {
            List<StatusMaster> listStatus = new List<StatusMaster>();
            IDBContext db = DbContext;
            listStatus = db.Fetch<StatusMaster>("GetStatus", new object[] { });
            db.Close();

            return listStatus;
        }



        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogMonitoringModel mdl = new LogMonitoringModel();
            //FunctionModel FunctionModel = new FunctionModel();
            //StatusModel StatusModel = new StatusModel();
           
            //mdl.LogMonitoring = LogMonitoring();
            
            ////mdl.LogMonitoring = db.Fetch<LogMonitoringMaster>("GetAllLog", new object[] { "", "", "", "", "" });
            
            //FunctionModel.FunctionLog = db.Fetch<functionMaster>("GetAllFunctionGrid", new object[] {  });

            //List<LogMonitoringModel> listLogMonitoringModel = new List<LogMonitoringModel>();

            ViewData["statustype"] = GetAllStatusType();

            mdl.LogMonitoring = new List<LogMonitoringMaster>();

            List<functionMaster> listFunctionModel = new List<functionMaster>();
            List<StatusMaster> listStatusModel = new List<StatusMaster>();
           
            listFunctionModel = db.Fetch<functionMaster>("GetAllFunctionGrid", new object[] { });
            listStatusModel = db.Fetch<StatusMaster>("GetStatus", new object[] { });
            db.Close();

            
            //Model.AddModel(mdl);
            Model.AddModel(mdl);
            Model.AddModel(listFunctionModel);
            Model.AddModel(listStatusModel);
          


        }

        private List<StatusType> GetAllStatusType()
        {
            List<StatusType> st = new List<StatusType>();

            st.Add(new StatusType() { StatusTypeName = "All" });
            st.Add(new StatusType() { StatusTypeName = "On Progress" });
            st.Add(new StatusType() { StatusTypeName = "Success" });
            st.Add(new StatusType() { StatusTypeName = "Error" });
            st.Add(new StatusType() { StatusTypeName = "On Hold" });

            return st;
        }


        public ActionResult ReloadGrid(string moduleID, string dateFrom, string dateTo, string function, string status, string userID)
        {
            if (!String.IsNullOrEmpty(dateFrom))
            {
                dateFrom = Convert.ToDateTime(dateFrom.Substring(4, 20)).ToString("yyyy-MM-dd");
            }
            if (!String.IsNullOrEmpty(dateTo))
            {
                dateTo = Convert.ToDateTime(dateTo.Substring(4, 20)).ToString("yyyy-MM-dd");
            }
            if (string.IsNullOrEmpty(moduleID))
            { 
                moduleID = Request.Params["ProcessID"];
            }

           
                if (status == "On Progress")
                {
                    status = "5";
                }
                else if (status == "Success")
                {
                    status = "0,2,6";
                }
                else if (status == "Error")
                {
                    status = "1,3,4";
                }
                else if (status == "On Hold")
                {
                    status = "7";
                }
                else
                {
                    status = "";
                }
            

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogMonitoringModel model = Model.GetModel<LogMonitoringModel>();
            model.LogMonitoring = db.Fetch<LogMonitoringMaster>("GetAllLog", new object[] { moduleID, dateFrom, dateTo, function, status, userID });
            db.Close();
            return PartialView("GridIndex", Model);
        }

        public ActionResult Back()
        {
            return RedirectToAction("Index", "LogMonitoring");
        }

        protected List<LogMonitoringMaster> LogMonitoring()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogMonitoringMaster> LogList = new List<LogMonitoringMaster>();
            IEnumerable<LogMonitoringMaster> QueryLog = db.Query<LogMonitoringMaster>("GetAllLog", new object[] { "", "", "", "", "", "" });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new LogMonitoringMaster()
                    {
                        ProcessID = q.ProcessID,
                        ProcessStatus = q.ProcessStatus,
                        FunctionID = q.FunctionID,
                        ProcessDate=q.ProcessDate,
                        EndDate=q.EndDate           
                    });
                }
            }
            db.Close();
            return LogList;
        }

        public void download(string processidd) {
            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;

                string filename = "Log_" + processidd + ".xls";
                string zipname = "Log_" + processidd + ".zip";


                documentBytes = downloadProcess(processidd);

                listCompress.Add(filename, documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/zip";
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipname));

                Response.BinaryWrite(documentBytes);
                Response.Flush();
                Response.End();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
            }
        
        
        }

        private List<LogMonitoringMaster> GetSpecifiedLog(string processId)
        {
            IDBContext db = DbContext;
            
            List<LogMonitoringMaster> logList = new List<LogMonitoringMaster>();
            logList = db.Query<LogMonitoringMaster>("GetLogDetailsByProcessID", new object[] { processId }).ToList();

            db.Close();

            return logList;


        }



        public byte[] downloadProcess(string processidd)
        {
            try
            {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                string title = "Process ID : " + processidd;
                title = title.ToUpper();


                filesTmp = HttpContext.Request.MapPath("~/Template/logMonitoring_template.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;

                int row = 4;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);


                #endregion

                List<LogMonitoringMaster> datas = new List<LogMonitoringMaster>();
                datas = GetSpecifiedLog(processidd);

                /*DateTime MyDateFrom;
                string stringDateFrom;
                MyDateFrom = new DateTime();
                MyDateFrom = DateTime.ParseExact(p_dateFrom, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);

                stringDateFrom = MyDateFrom.ToString("dd.MM.yyyy");

                DateTime MyDateTo;
                string stringDateTo;
                MyDateTo = new DateTime();
                MyDateTo = DateTime.ParseExact(p_dateTo, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);
                stringDateTo = MyDateTo.ToString("dd.MM.yyyy");*/


                ISheet sheet;

                sheet = workbook.GetSheetAt(0);
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue(title);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                Hrow.GetCell(0).CellStyle = styleContent4;


                /*Hrow = sheet.CreateRow(1);
                Hrow.CreateCell(0).SetCellValue("Prod. Date : " + stringDateFrom + " - " + stringDateTo);
                sheet.AddMergedRegion(new CellRangeAddress(1, 1, 0, 3));*/

                Hrow = sheet.CreateRow(3);

                Hrow.CreateCell(0).SetCellValue("Seq. ");
                sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("Message ID  ");
                sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("Message                                                                                               ");
                sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Location  ");
                sheet.AutoSizeColumn(3);
                Hrow.CreateCell(4).SetCellValue("Created By    ");
                sheet.AutoSizeColumn(4);
                Hrow.CreateCell(5).SetCellValue("Created Date                          ");
                sheet.AutoSizeColumn(5);

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                

                int no = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    //data.PRODUCTION_DATE.ToString("dd.MM.yyyy")
                    Hrow.CreateCell(0).SetCellValue(data.SequenceNumber);
                    Hrow.CreateCell(1).SetCellValue(data.MessageID);
                    Hrow.CreateCell(2).SetCellValue(data.Message);
                    Hrow.CreateCell(3).SetCellValue(data.Location);
                    Hrow.CreateCell(4).SetCellValue(data.createdby);
                    Hrow.CreateCell(5).SetCellValue(data.createddate.ToString("dd.MM.yyyy hh:mm"));
                    

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent3;
                    Hrow.GetCell(5).CellStyle = styleContent1;
                    //Hrow.GetCell(6).CellStyle = styleContent1;

                    row++;

                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();
                return a;
            }

        }




        public ActionResult LogMonitoringDetails(string ProcessID)

        {
            //int id;\
            //int.TryParse(ID, out id);
            string  id = "";
            id = Request.Params["ProcessID"];
            //ViewData["GroupID"] = ProcessID;
            //id = ProcessID;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogMonitoringModel model = Model.GetModel<LogMonitoringModel>();
            List<LogMonitoringMaster> logList = new List<LogMonitoringMaster>();
            logList = db.Query<LogMonitoringMaster>("GetLogDetailsByProcessID", new object[] { id }).ToList();

            if (logList.Any())
            {
                model.LogMonitoring.Clear();
                foreach (var q in logList)
                {
                    model.LogMonitoring.Add(new LogMonitoringMaster()
                    {
                     
                        ProcessID = q.ProcessID,
                        SequenceNumber = q.SequenceNumber,
                        FunctionID = q.FunctionID,
                        MessageID = q.MessageID,
                        MessageType = q.MessageType,
                        Message = q.Message,
                        MessageSplit = q.MessageSplit,
                        Location = q.Location,
                        createdby=q.createdby,
                        createddate=q.createddate 
                    });
                }
            }
            db.Close();
            Model.AddModel(model);
            return PartialView("GridIndexDetails", Model);
        }

        public ActionResult ExportGridReview()
        {
            foreach (string typeName in GridViewDemosHelper.ExportTypes.Keys)
            {
                if (Request.Params[typeName] != null)
                {
                    GridViewDemosHelper.ExportTypes["XLS"].Method(GridViewDemosHelper.ExportGridViewSettings, modelExport.LogMonitoring);

                }
            }
            return View();

        }

        public string Solution(string msgid)
        {
            string result = string.Empty;
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            Message msg = dbContext.SingleOrDefault<Message>("GetMessageByID", new object[] { msgid });
            result = (msg != null ? msg.PIC1 : "") + ";" + (msg != null ? msg.PIC2 : "") + (msg != null ? msg.Description : "") + (msg != null ? msg.Solution : "");
            return result;
        }

        public ActionResult IndexDetails(string ProcessID)
        {
            //LogMonitoringDetailsModel modelDetails = new LogMonitoringDetailsModel();
            //modelDetails.LogMonitoringDetails = LogMonitoringDetails(Convert.ToInt32(ProcessID));
            //Model.AddModel(modelDetails);

            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            LogMonitoringMaster logHeader = dbContext.SingleOrDefault<LogMonitoringMaster>("GetLogHeader", new object[] { Convert.ToInt32(ProcessID) });

            LogMonitoringMaster listRow = new LogMonitoringMaster();
            if (logHeader != null)
            {
                listRow.ProcessID = logHeader.ProcessID;
                listRow.FunctionID = logHeader.FunctionID;
                listRow.ProcessDate = logHeader.ProcessDate;
                listRow.ProcessStatus = logHeader.ProcessStatus;
            };
            dbContext.Close();
            Model.AddModel(listRow);
            return View("IndexDetails", Model);
        }

        public ActionResult IndexHeader()
        {

            return PartialView("IndexHeader", null);
        }

        public ActionResult PartialHeaderFunctionLookupGridLogMonitoring()
        {
            TempData["gridname"] = "OIHFunctionName";
            return PartialView("GridLookup/PartialGrid", GetAllFunction());
            
        }

        public ActionResult PartialHeaderStatusLookupGridLogMonitoring()
        {
            TempData["gridname"] = "status";

            List<StatusMaster> status = new List<StatusMaster>();
            status = GetAllStatus();
            return PartialView("GridLookup/PartialGrid", status);
 
        }

      
         
    }

    public delegate ActionResult ExportMethod(GridViewSettings settings, object dataObject);
    public class ExportType
    {
        public string Title { get; set; }
        public ExportMethod Method { get; set; }
    }

    public class GridViewDemosHelper
    {
        public const string ImageQueryKey = "DXImage";
        public const string PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09";

        public static int PageSize
        {
            get
            {
                if (HttpContext.Current.Session[PageSizeSessionKey] == null)
                    return 2;
                return (int)HttpContext.Current.Session[PageSizeSessionKey];
            }
            set { HttpContext.Current.Session[PageSizeSessionKey] = value; }
        }

        public static string GetEmployeeImageRouteUrl()
        {
            return DevExpressHelper.GetUrl(new { Controller = "GridView", Action = "EmployeeImage" });
        }

        static Dictionary<string, ExportType> exportTypes;
        public static Dictionary<string, ExportType> ExportTypes
        {
            get
            {
                if (exportTypes == null)
                    exportTypes = CreateExportTypes();
                return exportTypes;
            }
        }
        static Dictionary<string, ExportType> CreateExportTypes()
        {
            Dictionary<string, ExportType> types = new Dictionary<string, ExportType>();
            types.Add("PDF", new ExportType { Title = "Export to PDF", Method = GridViewExtension.ExportToPdf });
            types.Add("XLS", new ExportType { Title = "Export to XLS", Method = GridViewExtension.ExportToXls });

            return types;
        }

        static GridViewSettings exportGridViewSettings;
        public static GridViewSettings ExportGridViewSettings
        {
            get
            {
                if (exportGridViewSettings == null)
                    exportGridViewSettings = CreateExportGridViewSettings();
                return exportGridViewSettings;
            }
        }
        static GridViewSettings CreateExportGridViewSettings()
        {
            GridViewSettings setting = new GridViewSettings();

            setting.Name = "GridLogDetail";
            setting.Styles.Header.HorizontalAlign = HorizontalAlign.Center;
            setting.Width = Unit.Percentage(100);
            setting.KeyFieldName = "ProcessID";
            setting.SettingsPager.PageSize = 20;
            setting.SettingsPager.Position = PagerPosition.Bottom;
            setting.Settings.ShowVerticalScrollBar = true;
            setting.SettingsPager.FirstPageButton.Visible = true;
            setting.SettingsPager.LastPageButton.Visible = true;
            setting.CommandColumn.Visible = true;
            setting.CommandColumn.ShowSelectCheckbox = true;
            setting.CommandColumn.Width = Unit.Pixel(50);

            setting.Columns.Add("ProcessID", "ProcessID", MVCxGridViewColumnType.TextBox).Width = Unit.Pixel(100);
            setting.Columns["ProcessID"].CellStyle.HorizontalAlign = HorizontalAlign.Center;
            setting.Columns.Add("SequenceNumber", "SequenceNumber", MVCxGridViewColumnType.TextBox).Width = Unit.Pixel(100);
            setting.Columns["SequenceNumber"].CellStyle.HorizontalAlign = HorizontalAlign.Center;
            setting.Columns.Add("MessageID", "MessageID", MVCxGridViewColumnType.TextBox).Width = Unit.Pixel(350);
            setting.Columns["MessageID"].CellStyle.HorizontalAlign = HorizontalAlign.Center;
            setting.Columns.Add("MessageType", "MessageType", MVCxGridViewColumnType.TextBox).Width = Unit.Pixel(100);
            setting.Columns["MessageType"].CellStyle.HorizontalAlign = HorizontalAlign.Center;
            setting.Columns.Add("Message", "Message", MVCxGridViewColumnType.TextBox).Width = Unit.Pixel(100);
            setting.Columns["Message"].CellStyle.HorizontalAlign = HorizontalAlign.Center;
            setting.Columns.Add("Location", "Location", MVCxGridViewColumnType.TextBox).Width = Unit.Pixel(100);
            setting.Columns["Location"].CellStyle.HorizontalAlign = HorizontalAlign.Center;
            return setting;
        }

        

        
    }
    public enum GridViewExportType { None, Pdf, Xls }


}