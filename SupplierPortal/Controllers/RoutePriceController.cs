﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Transactions;
using System.Data;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RoutePrice;
using Portal.Models;
using Portal.Models.Globals;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using DevExpress.Web.Mvc;
using System.Data.SqlClient;

namespace Portal.Controllers
{
    public class RoutePriceController : BaseController
    {
        public RoutePriceController()
            : base("Route Price Master")
        {
        }
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            //IDBContext db = DbContext;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            RoutePriceModel mdl = new RoutePriceModel();
            ViewData["viewRoute"] = GetAllRoute();
            ViewData["viewLP"] = GetAllLP();
        }

        public List<LogisticPartner> GetAllLP()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLP = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLP = QueryLog.ToList<LogisticPartner>();
            return listLP;
        }

        public ActionResult CallBackLP()
        {
            TempData["GridName"] = "gridLP";
            ViewData["viewLP"] = GetAllLP();
            return PartialView("PG", ViewData["viewLP"]);
        }

        public ActionResult CallBackLPadd()
        {
            TempData["GridName"] = "gridLPAdd";
            ViewData["viewLP"] = GetAllLP();
            return PartialView("PG", ViewData["viewLP"]);
        }

        public List<Route> GetAllRoute()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<Route> listRoute = new List<Route>();
            var QueryLog = db.Query<Route>("GetAllRoute");
            listRoute = QueryLog.ToList<Route>();
            return listRoute;
        }

        public ActionResult CallBackRoute()
        {
            TempData["GridName"] = "gridRoute";
            ViewData["viewRoute"] = GetAllRoute();
            return PartialView("GridLookup/PartialGrid", ViewData["viewRoute"]);
        }

        public ActionResult CallBackRouteAdd()
        {
            TempData["GridName"] = "gridRouteAdd";
            ViewData["viewRoute"] = GetAllRoute();
            return PartialView("GridLookup/PartialGrid", ViewData["viewRoute"]);
        }


        #region GRID PRICE
        public ActionResult GridRoutePriceData()
        {
            string vLP_CD = String.IsNullOrEmpty(Request.Params["LPCd"]) ? "" : Request.Params["LPCd"];
            string vROUTE_CD = String.IsNullOrEmpty(Request.Params["RouteCd"]) ? "" : Request.Params["RouteCd"];
            string vDATE_FROM = String.IsNullOrEmpty(Request.Params["ValidFrom"]) ? "" : Request.Params["ValidFrom"];
            string vDATE_TO = String.IsNullOrEmpty(Request.Params["ValidTo"]) ? "" : Request.Params["ValidTo"];

            //---------------------------------------------------------
            string lpcdparameter = string.Empty;
            if (!string.IsNullOrEmpty(vLP_CD))
            {
                string[] lpcdlist = vLP_CD.Split(';');
                foreach (string l in lpcdlist)
                {
                    lpcdparameter += "'" + l + "',";
                }
                lpcdparameter = lpcdparameter.Remove(lpcdparameter.Length - 1, 1);
            }

            string Routecdparameter = string.Empty;
            if (!string.IsNullOrEmpty(vROUTE_CD))
            {
                string[] routecdlist = vROUTE_CD.Split(';');
                foreach (string r in routecdlist)
                {
                    Routecdparameter += "'" + r + "',";
                }
                Routecdparameter = Routecdparameter.Remove(Routecdparameter.Length - 1, 1);
            }

            //---------------------------------------------------------
            RoutePriceModel model = Model.GetModel<RoutePriceModel>();
            List<RoutePriceData> RoutePriceDatas = new List<RoutePriceData>();
            RoutePriceDatas = DbContext.Fetch<RoutePriceData>("GetAllRoutePrice", new object[] { lpcdparameter, Routecdparameter, vDATE_FROM, vDATE_TO });
            Model.AddModel(RoutePriceDatas);

            return PartialView("RoutePricePartial", Model);

        }

        #endregion

        #region INSERT PRICE
        public ActionResult InsertRoutePrice(string RouteCd, string LPCd, string Price, string Valid_From, string createBy, string Valid_To)
        {
            string ResultQuery = string.Empty;
            IDBContext db = DbContext;
            string[] Date = null;

            Price = Math.Round(Convert.ToDecimal(Price), 0).ToString();
            //Price = Price.Split('.')[0];
            
            Date = Valid_From.Split('.');
            Valid_From = Date[1] + "." + Date[0] + "." + Date[2];
            Valid_From = Convert.ToDateTime(Valid_From).ToString("yyyy-MM-dd");

            try
            {
                ResultQuery = db.ExecuteScalar<string>("InsertRoutePrice", new object[] 
                {
                   RouteCd,
                   LPCd,
                   Price,                  
                   Valid_From,
                   AuthorizedUser.Username,
                   "M"
                });
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion 
        
        
        #region DELETE PRICE

        public ContentResult Delete(string GridId)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string routecd = "";
            string lpcd = "";
            string validfrom = "";
            
            string ResultQuery = string.Empty;
           
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                //char[] splitchar = { ';' };
                //string[] ParamUpdate = GridId.Split(splitchar);

                //string[] Date = null;

                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                //for (int i = 0; i < ParamUpdate.Length; i++)
                //{
                    //SPLIT ID BY OBJECT WITH DELIMETER "_"
                    //if (ParamUpdate[i] != "")
                    //{
                        //char[] SplitId = { '_' };
                        //string[] ParamId = ParamUpdate[i].Split(SplitId);

                        //routecd = ParamId[0];
                        //lpcd = ParamId[1];
                        //Date = ParamId[2].Split('.');
                        //validfrom = Date[1] + "." + Date[0] + "." + Date[2];
                        //validfrom = Convert.ToDateTime(validfrom).ToString("yyyy-MM-dd");

                        ResultQuery = db.ExecuteScalar<string>("DeleteRoutePrice", new object[] { 
                         GridId,
                         AuthorizedUser.Username
                         });
                    //}                  
                //}
                  db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion 

        #region UPDATE PRICE

        public ActionResult UpdateRoutePrice(string RouteCd2, string LPCd2, string Price2, string Valid_From2, string Valid_To2)
        {
            string ResultQuery = string.Empty;
            IDBContext db = DbContext;
            string[] Date = null;
            string[] Date2 = null;

            Price2 = Math.Round(Convert.ToDecimal(Price2), 0).ToString();
            //Price = Price.Split('.')[0];

            Date = Valid_From2.Split('.');
            Valid_From2 = Date[1] + "." + Date[0] + "." + Date[2];
            Valid_From2 = Convert.ToDateTime(Valid_From2).ToString("yyyy-MM-dd");

            Date2 = Valid_To2.Split('.');
            Valid_To2 = Date2[1] + "." + Date2[0] + "." + Date2[2];
            Valid_To2 = Convert.ToDateTime(Valid_To2).ToString("yyyy-MM-dd");

            try
            {
                ResultQuery = db.ExecuteScalar<string>("UpdateRoutePrice", new object[] 
                {
                   RouteCd2,
                   LPCd2,
                   Price2,                  
                   Valid_From2,
                   Valid_To2,
                   AuthorizedUser.Username,
                   "M"
                });
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }

        //public ActionResult SubmitEdit(RoutePriceData item, string RouteCd, string LPCd, string ValidFrom)
        //{
        //    string LOGISTIC_PARTNER = String.IsNullOrEmpty(Request.Params["txtEditLPCode"]) ? "" : Request.Params["txtEditLPCode"];
        //    string PRICE = String.IsNullOrEmpty(Request.Params["sePrice_Raw"]) ? "" : Request.Params["sePrice_Raw"];
        //    string ROUTE = String.IsNullOrEmpty(Request.Params["txtEditLPRoute"]) ? "" : Request.Params["txtEditLPRoute"];
        //    string DATE = String.IsNullOrEmpty(Request.Params["deValidFrom"]) ? "" : Request.Params["deValidFrom"];

        //        string routeDateToParameter = string.Empty;
        //        if (!string.IsNullOrEmpty(DATE))
        //        {
        //            string d = DATE.Substring(0, 2);
        //            string m = DATE.Substring(3, 2);
        //            string y = DATE.Substring(6, 4);
        //            string tgl = y + "-" + m + "-" + d;
        //            routeDateToParameter += "'" + tgl + "'";
        //        }
                
        //    string ResultQuery = string.Empty;
        //    IDBContext db = DbContext;
        //    try
        //    {
        //        ResultQuery = db.ExecuteScalar<string>("UpdateRoutePrice", new object[] 
        //        {
        //            ROUTE, 
        //            LOGISTIC_PARTNER, 
        //            PRICE,
        //            routeDateToParameter,
        //            AuthorizedUser.Username
        //        });
        //        db.Close();
        //        if (ResultQuery == "1")
        //        {
        //            ViewData["EditResult"] = "[I] Data has been update";
        //        }
        //        else if (ResultQuery == "2")
        //        {
        //            ViewData["EditResult"] = "[X] Data can not edit, Input values valid from date less than current date";
        //        }
        //        else
        //        {
        //            ViewData["EditResult"] = "[X] Data can not edit, Input values valid from date no between valid from and valid to";
        //        }
                
        //    }
        //    catch (Exception ex)
        //    {
        //        ResultQuery = "Error: " + ex.Message;
        //        ViewData["EditResult"] = "[X] Error when update data";
        //    }

        //    string lpcdparameter = string.Empty;
        //    if (!string.IsNullOrEmpty(LOGISTIC_PARTNER))
        //    {
        //        string[] lpcdlist = LOGISTIC_PARTNER.Split(';');
        //        foreach (string l in lpcdlist)
        //        {
        //            lpcdparameter += "'" + l + "',";
        //        }
        //        lpcdparameter = lpcdparameter.Remove(lpcdparameter.Length - 1, 1);
        //    }

        //    string Routecdparameter = string.Empty;
        //    if (!string.IsNullOrEmpty(ROUTE))
        //    {
        //        string[] routecdlist = ROUTE.Split(';');
        //        foreach (string r in routecdlist)
        //        {
        //            Routecdparameter += "'" + r + "',";
        //        }
        //        Routecdparameter = Routecdparameter.Remove(Routecdparameter.Length - 1, 1);
        //    }


        //    RoutePriceModel model = Model.GetModel<RoutePriceModel>();
        //    List<RoutePriceData> RoutePriceDatas = new List<RoutePriceData>();
        //    RoutePriceDatas = DbContext.Fetch<RoutePriceData>("GetAllRoutePrice", new object[] { 
        //       lpcdparameter, "", " ", " "
        //      });
        //    Model.AddModel(RoutePriceDatas);

        //    return PartialView("RoutePricePartial", Model);
        //}
        
        
        #endregion 

    }
}
