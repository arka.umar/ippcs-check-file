﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.SupplierReconciliation;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;

namespace Portal.Controllers
{
    public class SupplierReconciliationController : BaseController
    {
        public SupplierReconciliationController()
            : base("Supplier Reconciliation Screen")
        {
        }

        protected override void StartUp()
        {

        }



        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        private List<SupplierReconciliation> GetAllSR()
        {
            List<SupplierReconciliation> listSR = new List<SupplierReconciliation>();
            IDBContext db = DbContext;
            listSR = db.Fetch<SupplierReconciliation>("GetSR", new object[] { });
            db.Close();

            return listSR;
        }

        private List<SupplierReconciliationPopup> GetAllPopupSR()
        {
            List<SupplierReconciliationPopup> listSR = new List<SupplierReconciliationPopup>();
            IDBContext db = DbContext;
            listSR = db.Fetch<SupplierReconciliationPopup>("GetAllPopupSR", new object[] { });
            db.Close();

            return listSR;
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            TempData["GridName"] = "grdVendor";
            List<SupplierReconciliationPopup> listSupplierRPopup = Model.GetModel<User>().FilteringArea<SupplierReconciliationPopup>(GetAllPopupSR(), "SupplierCd", "SupplierReconciliation", "grdVendor");

            if (listSupplierRPopup.Count > 0)
            {
                _SupplierReconciliationModelPopup = listSupplierRPopup;
            }
            else
            {
                _SupplierReconciliationModelPopup = GetAllPopupSR();
            }

            ViewData["SupplierReconciliationDataPopup"] = _SupplierReconciliationModelPopup;

            TempData["GridName"] = "SRSupplier";

            List<SupplierReconciliation> listSupplierR = Model.GetModel<User>().FilteringArea<SupplierReconciliation>(GetAllSR(), "SupplierCode;SupplierName", "SupplierReconciliation", "SRSupplier");

            if (listSupplierR.Count > 0)
            {
                _SupplierReconciliationModel = listSupplierR;
            }
            else
            {
                _SupplierReconciliationModel = GetAllSR();
            }

            ViewData["SupplierReconciliationData"] = _SupplierReconciliationModel;
            db.Close();



            SupplierReconciliationGridModel mdl = new SupplierReconciliationGridModel();
            mdl.SRGrid = GetListSR("", "", "");
            // mdl.SRGrid = GetLastReconcile();

            SupplierReconciliationGridSumModel model = new SupplierReconciliationGridSumModel();
            model.SRGridSum = GetListSRSum("", "", "");


            Model.AddModel(mdl);
            Model.AddModel(model);

        }

        public JsonResult saveAmount()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string DOCUMENT_NO = String.IsNullOrEmpty(Request.Params["DocumentNumber"]) ? "" : Request.Params["DocumentNumber"];
            string AMOUNT = String.IsNullOrEmpty(Request.Params["AmountSupplier"]) ? "" : Request.Params["AmountSupplier"];


            string user = AuthorizedUser.Username;
            var l = db.Fetch<note_supplier>("SaveAmountOK", new object[] {
                user,
                DOCUMENT_NO,
                AMOUNT
            });

            string mstatus = (from q in l select q).FirstOrDefault().MSTATUS;
            string mamount = (from q in l select q).FirstOrDefault().MAMOUNT;

            db.Close();
            return Json(new { status = mstatus, message = mamount }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveAmountTMMIN()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string DOCUMENT_NO = String.IsNullOrEmpty(Request.Params["DocumentNumber"]) ? "" : Request.Params["DocumentNumber"];
            string AMOUNT = String.IsNullOrEmpty(Request.Params["AmountTMMIN"]) ? "" : Request.Params["AmountTMMIN"];


            string user = AuthorizedUser.Username;
            var l = db.Fetch<note_supplier>("SaveAmountOKTMMIN", new object[] {
                user,
                DOCUMENT_NO,
                AMOUNT
            });

            string mstatus = (from q in l select q).FirstOrDefault().MSTATUS;
            string mamount = (from q in l select q).FirstOrDefault().MAMOUNT;

            db.Close();
            return Json(new { status = mstatus, message = mamount }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveFeedback()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string DOCUMENT_NO = String.IsNullOrEmpty(Request.Params["DocumentNumber"]) ? "" : Request.Params["DocumentNumber"];
            string FEEDBACK = String.IsNullOrEmpty(Request.Params["FeedbackSupplier"]) ? "" : Request.Params["FeedbackSupplier"];


            string user = AuthorizedUser.Username;
            var l = db.Fetch<note_supplier>("SaveFeedbackOK", new object[] {
                user,
                DOCUMENT_NO,
                FEEDBACK
            });

            string mstatus = (from q in l select q).FirstOrDefault().MSTATUS;
            string mfeedback = (from q in l select q).FirstOrDefault().MFEEDBACK;

            db.Close();
            return Json(new { status = mstatus, message = mfeedback }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveFeedbackTMMIN()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string DOCUMENT_NO = String.IsNullOrEmpty(Request.Params["DocumentNumber"]) ? "" : Request.Params["DocumentNumber"];
            string FEEDBACK = String.IsNullOrEmpty(Request.Params["FeedbackTMMIN"]) ? "" : Request.Params["FeedbackTMMIN"];


            string user = AuthorizedUser.Username;
            var l = db.Fetch<note_supplier>("SaveFeedbackTMMINOK", new object[] {
                user,
                DOCUMENT_NO,
                FEEDBACK
            });

            string mstatus = (from q in l select q).FirstOrDefault().MSTATUS;
            string mfeedback = (from q in l select q).FirstOrDefault().MFEEDBACK;

            db.Close();
            return Json(new { status = mstatus, message = mfeedback }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PlantCodePartialLookupForm()
        {
            try
            {
                TempData["GridName"] = "grlVendor";
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                var QueryPlantDock = db.Query<SupplierReconciliationMaster>("GetAllPopupSR");

                db.Close();
                ViewData["GridPlantDock"] = QueryPlantDock;
            }
            catch (Exception explantcode)
            {
                Console.WriteLine(explantcode.Message + explantcode.Source + explantcode.StackTrace + explantcode.TargetSite);
                return RedirectToAction("ShowError", explantcode.Message);
            }
            return PartialView("GridLookup/PartialGrid", ViewData["GridPlantDock"]);
        }

        List<SupplierReconciliationGridDetail> _srDetailModel = null;
        private List<SupplierReconciliationGridDetail> _SRDetailModel
        {
            get
            {
                if (_srDetailModel == null)
                    _srDetailModel = new List<SupplierReconciliationGridDetail>();

                return _srDetailModel;
            }
            set
            {
                _srDetailModel = value;
            }
        }

        public ActionResult GridDetail()
        {
            _SRDetailModel = GetDetailReconcile(
                String.IsNullOrEmpty(Request.Params["DocumentNumber"]) ? "" : Request.Params["DocumentNumber"]
                );
            Model.AddModel(_SRDetailModel);
            return PartialView("GridDetail", Model);
        }

        public List<SupplierReconciliationGridDetail> GetDetailReconcile(String DOCUMENT_NO = "")
        {
            List<SupplierReconciliationGridDetail> supplierReconciliationGridDetail = new List<SupplierReconciliationGridDetail>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            try
            {
                supplierReconciliationGridDetail = db.Fetch<SupplierReconciliationGridDetail>("GetDetailReconcile", new object[] { 
                DOCUMENT_NO
            });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }


            return supplierReconciliationGridDetail;
        }

        public ActionResult GeneralPartial()
        {
            string supplier = String.IsNullOrEmpty(Request.Params["SRSupplier"]) ? "" : Request.Params["SRSupplier"];
            string balance = String.IsNullOrEmpty(Request.Params["BalanceEndMonth"]) ? "" : Request.Params["BalanceEndMonth"];
            string progress = String.IsNullOrEmpty(Request.Params["ProgressStatus"]) ? "" : Request.Params["ProgressStatus"];

            SupplierReconciliationGridModel mdl = Model.GetModel<SupplierReconciliationGridModel>();
            mdl.SRGrid = GetListSR(supplier, balance, progress);
            Model.AddModel(mdl);
            return PartialView("GeneralPartial", Model);
        }

        public ActionResult RemoveSR(string data)
        {
            List<SupplierReconciliationGrid> areas = Toyota.Common.Util.Text.JSON.ToObject<List<SupplierReconciliationGrid>>(data);
            int ret = areas.Count();
            foreach (SupplierReconciliationGrid pack in areas)
            {
                ret = DeleteSR(pack.DOCUMENT_NO);
            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfirmSR(string data)
        {
            List<SupplierReconciliationGrid> areas = Toyota.Common.Util.Text.JSON.ToObject<List<SupplierReconciliationGrid>>(data);
            int ret = areas.Count();
            foreach (SupplierReconciliationGrid pack in areas)
            {
                ret = ConfirmSupplier(pack.DOCUMENT_NO);
            }
            if (ret > 0)
            {
                ret = -1;
            }
            else
            {
                foreach (SupplierReconciliationGrid pack in areas)
                {
                    ret = UpdateConfirmation(pack.DOCUMENT_NO);
                }
            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApprovedSR(string data)
        {
            List<SupplierReconciliationGrid> areas = Toyota.Common.Util.Text.JSON.ToObject<List<SupplierReconciliationGrid>>(data);
            int ret = areas.Count();
            foreach (SupplierReconciliationGrid pack in areas)
            {
                ret = ApprovedSupplier(pack.DOCUMENT_NO);
            }
            if (ret > 0)
            {
                ret = -1;
            }
            else
            {
                foreach (SupplierReconciliationGrid pack in areas)
                {
                    ret = UpdateApproved(pack.DOCUMENT_NO);
                }
            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public int DeleteSR(string documentNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int result = db.Execute("DeleteSR", new object[] { documentNo });
            db.Close();
            return result;
        }

        public int ConfirmSupplier(string documentNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int result = db.Execute("ConfirmSR", new object[] { documentNo });
            db.Close();
            return result;
        }

        public int ApprovedSupplier(string documentNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int result = db.Execute("ApprovedSR", new object[] { documentNo });
            db.Close();
            return result;
        }

        public int UpdateConfirmation(string documentNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int result = db.Execute("UpdateConfirm", new object[] { documentNo, AuthorizedUser.Username });
            db.Close();
            return result;
        }

        public int UpdateApproved(string documentNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int result = db.Execute("UpdateApproved", new object[] { documentNo, AuthorizedUser.Username });
            db.Close();
            return result;
        }

        public ActionResult InsertNewData(string SUPPLIER_CD, string DOCUMENT_NO, string ITEM_CURR, int SUPP_AMOUNT, int SUPP_TAX_AMOUNT)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string ResultQuery = "Data Has Been Saved";
            try
            {
                //SupplierReconciliationGrid Check = new SupplierReconciliationGrid();
                //Check = db.SingleOrDefault<SupplierReconciliationGrid>("CheckPostingDate", new object[] { });

                //if (Check.Equals(0))
                //{
                //    ResultQuery = "Cannot add Reconcile. Data Reconcile of previous month is not exist.";
                //}
                //else
                //{
                db.Execute("InsertSR", new object[] { DOCUMENT_NO, SUPPLIER_CD, ITEM_CURR, SUPP_AMOUNT, SUPP_TAX_AMOUNT, AuthorizedUser.Username });
                ResultQuery = "Insert Data is success";
                //}



            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;

            }
            db.Close();
            return Content(ResultQuery);
        }

        List<SupplierReconciliation> _supplierReconciliationModel = null;
        private List<SupplierReconciliation> _SupplierReconciliationModel
        {
            get
            {
                if (_supplierReconciliationModel == null)
                    _supplierReconciliationModel = new List<SupplierReconciliation>();

                return _supplierReconciliationModel;
            }
            set
            {
                _supplierReconciliationModel = value;
            }
        }

        List<SupplierReconciliationPopup> _supplierReconciliationModelPopup = null;
        private List<SupplierReconciliationPopup> _SupplierReconciliationModelPopup
        {
            get
            {
                if (_supplierReconciliationModelPopup == null)
                    _supplierReconciliationModelPopup = new List<SupplierReconciliationPopup>();

                return _supplierReconciliationModelPopup;
            }
            set
            {
                _supplierReconciliationModelPopup = value;
            }
        }

        List<SupplierReconciliationGridDetail> _goodsReceiptInquiryManifestDetailDataModel = null;
        private List<SupplierReconciliationGridDetail> _GoodsReceiptInquiryManifestDetailDataModel
        {
            get
            {
                if (_goodsReceiptInquiryManifestDetailDataModel == null)
                    _goodsReceiptInquiryManifestDetailDataModel = new List<SupplierReconciliationGridDetail>();

                return _goodsReceiptInquiryManifestDetailDataModel;
            }
            set
            {
                _goodsReceiptInquiryManifestDetailDataModel = value;
            }
        }

        public ContentResult Reconcile(string SCAN_DT)
        {
            int isError = 0;
            string errorMessage = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                if (SCAN_DT == GetLastReconcile())
                {
                    errorMessage = "Reconcile already performace for this month";
                }
                else
                {
                    db.Execute("SR_Reconcile", new object[] {
                        SCAN_DT, AuthorizedUser.Username
                    });

                    // errorMessage = "Reconcile process has been finished.";
                }
            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = exc.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }


        public void EditDataPopup(string SupplierCode)
        {
            getSupplierCode = SupplierCode;
            //getDocumentNumber = DocumentNumber;
            //getPostingDate = PostingDate;
            //getDocumentType = DocumentType;
            //getCurr = Curr;
            //getSupplierAmount = SupplierAmount;
            //getTMMINAmount = TMMINAmount;
            //getDifferent = Different;
            //getStatus = Status;
            //getFeedbackSupplier = FeedbackSupplier;
            //getFeedbackTMMIN = FeedbackTMMIN;

        }

        public ActionResult PopupNewGeneralInfo2()
        {
            ViewData["SetSupplierCodeLabel"] = getSupplierCode;
            //ViewData["SetDocumentNumberLabel"] = getDocumentNumber;
            //ViewData["SetPostingDateLabel"] = getPostingDate;
            //ViewData["SetDocumentTypeLabel"] = getDocumentType;
            //ViewData["SetCurrLabel"] = getCurr;
            //ViewData["SetSupplierAmountLabel"] = getSupplierAmount;
            //ViewData["SetTMMINAmountLabel"] = getTMMINAmount;
            //ViewData["SetDifferentLabel"] = getDifferent;
            //ViewData["SetStatusLabel"] = getStatus;
            //ViewData["SetFeedbackSupplierLabel"] = getFeedbackSupplier;
            //ViewData["SetFeedbackTMMINLabel"] = getFeedbackTMMIN;

            //string docNo = String.IsNullOrEmpty(Request.Params["DocumentNumber"]) ? "" : Request.Params["DocumentNumber"];
            //SupplierReconciliationGridModel mdl = Model.GetModel<SupplierReconciliationGridModel>();
            //mdl.SRGrid = GetListEdit(docNo);
            //Model.AddModel(mdl);
            return PartialView("PopupNewGeneralInfo");
        }

        public string getSupplierCode
        {
            get
            {
                if (Session["getSupplierCode"] != null)
                {
                    return Session["getSupplierCode"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getSupplierCode"] = value;
            }
        }

        public string getDocumentNumber
        {
            get
            {
                if (Session["getDocumentNumber"] != null)
                {
                    return Session["getDocumentNumber"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getDocumentNumber"] = value;
            }
        }

        public string getPostingDate
        {
            get
            {
                if (Session["getPostingDate"] != null)
                {
                    return Session["getPostingDate"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getPostingDate"] = value;
            }
        }

        public string getDocumentType
        {
            get
            {
                if (Session["getDocumentType"] != null)
                {
                    return Session["getDocumentType"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getDocumentType"] = value;
            }
        }

        public string getCurr
        {
            get
            {
                if (Session["getCurr"] != null)
                {
                    return Session["getCurr"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getCurr"] = value;
            }
        }

        public string getSupplierAmount
        {
            get
            {
                if (Session["getSupplierAmount"] != null)
                {
                    return Session["getSupplierAmount"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getSupplierAmount"] = value;
            }
        }

        public string getTMMINAmount
        {
            get
            {
                if (Session["getTMMINAmount"] != null)
                {
                    return Session["getTMMINAmount"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getTMMINAmount"] = value;
            }
        }

        public string getDifferent
        {
            get
            {
                if (Session["getDifferent"] != null)
                {
                    return Session["getDifferent"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getDifferent"] = value;
            }
        }

        public string getStatus
        {
            get
            {
                if (Session["getStatus"] != null)
                {
                    return Session["getStatus"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getStatus"] = value;
            }
        }

        public string getFeedbackSupplier
        {
            get
            {
                if (Session["getFeedbackSupplier"] != null)
                {
                    return Session["getFeedbackSupplier"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getFeedbackSupplier"] = value;
            }
        }

        public string getFeedbackTMMIN
        {
            get
            {
                if (Session["getFeedbackTMMIN"] != null)
                {
                    return Session["getFeedbackTMMIN"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getFeedbackTMMIN"] = value;
            }
        }

        public List<SupplierReconciliationGrid> GetListEdit(string docNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierReconciliationGrid> listEdit = db.Fetch<SupplierReconciliationGrid>("GetListEdit", new object[] {
                docNo
            });
            return listEdit;
        }


        public ContentResult GetTotalAmountDetail(string DOCUMENTNO)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string[] amount = new string[3];
            amount[0] = "0";
            amount[1] = "0";
            amount[2] = "0";

            try
            {
                var result = db.Fetch<TotalAmountDetail>("GetTotalAmountDetail", new object[] { DOCUMENTNO });

                //if (result.Count() == 1)
                //{
                foreach (TotalAmountDetail f in result)
                {
                    if (f.curr == "IDR")
                    {
                        amount[0] = f.idrSuppDetail.ToString();
                    }
                    else if (f.curr == "JPY")
                    {
                        amount[1] = f.idrSuppDetail.ToString();
                        //resultIdr = "0,0,0," + f.idrSupplier + ',' + f.idrTMMIN + ',' + f.idrDiff + ",0,0,0";
                    }
                    else if (f.curr == "USD")
                    {
                        amount[2] = f.idrSuppDetail.ToString();
                        //resultIdr = "0,0,0,0,0,0," + f.idrSupplier + ',' + f.idrTMMIN + ',' + f.idrDiff;
                    }

                }

            }
            catch (Exception ex)
            {

            }
            db.Close();
            db.Dispose();

            return Content(amount[0] + ',' + amount[1] + ',' + amount[2]);
        }

        public ContentResult GetTotalAmount(string SUPPLIER, string BALANCE, string PROGRESS)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string[] amount = new string[3];
            amount[0] = "0,0,0";
            amount[1] = "0,0,0";
            amount[2] = "0,0,0";
            //string idrTMMIN = "";
            try
            {
                var result = db.Fetch<TotalAmount>("GetTotalAmount", new object[] { SUPPLIER, BALANCE, PROGRESS });

                //if (result.Count() == 1)
                //{
                foreach (TotalAmount f in result)
                {
                    if (f.itemCurr == "IDR")
                    {
                        amount[0] = f.idrTMMIN.ToString() + ',' + f.idrSupplier + ',' + f.idrDiff;
                    }
                    else if (f.itemCurr == "JPY")
                    {
                        amount[1] = f.idrTMMIN.ToString() + ',' + f.idrSupplier + ',' + f.idrDiff;
                        //resultIdr = "0,0,0," + f.idrSupplier + ',' + f.idrTMMIN + ',' + f.idrDiff + ",0,0,0";
                    }
                    else if (f.itemCurr == "USD")
                    {
                        amount[2] = f.idrTMMIN.ToString() + ',' + f.idrSupplier + ',' + f.idrDiff;
                        //resultIdr = "0,0,0,0,0,0," + f.idrSupplier + ',' + f.idrTMMIN + ',' + f.idrDiff;
                    }

                }

            }
            catch (Exception ex)
            {

            }
            db.Close();
            db.Dispose();

            return Content(amount[0] + ',' + amount[1] + ',' + amount[2]);
        }

        public ContentResult GetTotalAmountSum(string SUPPLIER, string BALANCE)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string[] amount = new string[3];
            amount[0] = "0,0,0,0,0,0,0";
            amount[1] = "0,0,0,0,0,0,0";
            amount[2] = "0,0,0,0,0,0,0";
            //string idrTMMIN = "";
            try
            {
                var result = db.Fetch<TotalAmountSum>("GetTotalAmountSum", new object[] { SUPPLIER, BALANCE });

                //if (result.Count() == 1)
                //{
                foreach (TotalAmountSum f in result)
                {
                    if (f.itemCurr == "IDR")
                    {
                        amount[0] = f.amountTMMIN.ToString() + ',' + f.taxTMMIN + ',' + f.totalTMMIN + ',' + f.amountSupplier + ',' + f.taxSupplier + ',' + f.totalSupplier + ',' + f.idrDiff;
                    }
                    else if (f.itemCurr == "JPY")
                    {
                        amount[1] = f.amountTMMIN.ToString() + ',' + f.taxTMMIN + ',' + f.totalTMMIN + ',' + f.amountSupplier + ',' + f.taxSupplier + ',' + f.totalSupplier + ',' + f.idrDiff;
                        //resultIdr = "0,0,0," + f.idrSupplier + ',' + f.idrTMMIN + ',' + f.idrDiff + ",0,0,0";
                    }
                    else if (f.itemCurr == "USD")
                    {
                        amount[2] = f.amountTMMIN.ToString() + ',' + f.taxTMMIN + ',' + f.totalTMMIN + ',' + f.amountSupplier + ',' + f.taxSupplier + ',' + f.totalSupplier + ',' + f.idrDiff;
                        //resultIdr = "0,0,0,0,0,0," + f.idrSupplier + ',' + f.idrTMMIN + ',' + f.idrDiff;
                    }

                }

            }
            catch (Exception ex)
            {

            }
            db.Close();
            db.Dispose();

            return Content(amount[0] + ',' + amount[1] + ',' + amount[2]);
        }


        public string GetLastReconcile()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string lastMonth = db.SingleOrDefault<string>("GetLastReconcile", new object[] { });
            return lastMonth;
        }

        public List<SupplierReconciliationGrid> GetListSR(string supplier, string balance, string progress)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierReconciliationGrid> listSR = db.Fetch<SupplierReconciliationGrid>("GetAllSR", new object[] {
                supplier,
                balance,
                progress
            });
            return listSR;
        }

        public List<SupplierReconciliationGridSum> GetListSRSum(string supplier, string balance, string progress)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierReconciliationGridSum> listSR = db.Fetch<SupplierReconciliationGridSum>("GetAllSRSum", new object[] {
                supplier,
                balance,
                progress
            });
            return listSR;
        }

        public ActionResult PartialListDetailData()
        {
            return PartialView("GridDetail", Model);
        }

        public ActionResult PartialUpdate()
        {
            string supplier = String.IsNullOrEmpty(Request.Params["SRSupplier"]) ? "" : Request.Params["SRSupplier"];
            string balance = String.IsNullOrEmpty(Request.Params["BalanceEndMonth"]) ? "" : Request.Params["BalanceEndMonth"];
            string progress = String.IsNullOrEmpty(Request.Params["ProgressStatus"]) ? "" : Request.Params["ProgressStatus"];

            SupplierReconciliationGridSumModel mdl = Model.GetModel<SupplierReconciliationGridSumModel>();
            mdl.SRGridSum = GetListSRSum(supplier, balance, progress);
            Model.AddModel(mdl);
            return PartialView("UpdatePartial", Model);
        }

        public ActionResult TotalAmountBeforeTax()
        {
            return PartialView("FooterGeneralPartial", Model);
        }

        public ActionResult TotalAmountBeforeTaxUpdate()
        {
            return PartialView("FooterUpdatePartial", Model);
        }

        public ActionResult PartialHeaderSupplierLookupGridSR()
        {
            TempData["GridName"] = "SRSupplier";

            List<SupplierReconciliation> listSupplierR = Model.GetModel<User>().FilteringArea<SupplierReconciliation>(GetAllSR(), "SupplierCode;SupplierName", "SupplierReconciliation", "SRSupplier");

            if (listSupplierR.Count > 0)
            {
                _SupplierReconciliationModel = listSupplierR;
            }
            else
            {
                _SupplierReconciliationModel = GetAllSR();
            }

            ViewData["SupplierReconciliationData"] = _SupplierReconciliationModel;
            return PartialView("GridLookup/PartialGrid", ViewData["SupplierReconciliationData"]);

        }

        public ActionResult PartialSupplierPopupSR()
        {
            TempData["GridName"] = "grdVendor";
            List<SupplierReconciliationPopup> listSupplierRPopup = Model.GetModel<User>().FilteringArea<SupplierReconciliationPopup>(GetAllPopupSR(), "SupplierCd", "SupplierReconciliation", "grdVendor");

            if (listSupplierRPopup.Count > 0)
            {
                _SupplierReconciliationModelPopup = listSupplierRPopup;
            }
            else
            {
                _SupplierReconciliationModelPopup = GetAllPopupSR();
            }

            ViewData["SupplierReconciliationDataPopup"] = _SupplierReconciliationModelPopup;
            return PartialView("GridLookup/PartialGrid", ViewData["SupplierReconciliationDataPopup"]);
            //return PartialView("GridLookup/PartialGrid", GetAllPopupSR());

        }

        public ContentResult GetAuthorization()
        {
            //SupplierCode = Request.Params["SupplierCode"];


            int auth = Model.GetModel<Toyota.Common.Web.Credential.User>().IsAuthorized("SupplierReconciliation", "btnAddSR");


            return Content(auth.ToString());
        }

        public void DownloadSRDetail(
            String DocumentNumber = ""

            )
        {
            string fileName = String.Empty;

            // Update download user and status
            IEnumerable<SupplierReconciliationGridReportDetail> qry;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;

            IDBContext db = DbContext;

            // get current user login detail
            /*if (supplierCode == "")
            {
                List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "GoodsReceiptInquiry", "GRIHSupplierLookup");

                if (suppliers.Count == 1)
                {
                    supplierCode = suppliers[0].SUPPLIER_CODE;
                }
            }*/


            /* if (downloadBy)
            {
                 qryPerManifest = db.Fetch<GoodsReceiptInquiryPerManifestData>(
                     "ReportGetAllGoodsReceiptInquiryPerManifest",
                         new object[] { 
                             arrivalTimeFrom,
                             arrivalTimeTo,
                             dockCode.Replace(";", ","),
                             supplierCode.Replace(";", ","),
                             rcvPlantCode,
                             routeRate,
                             manifestNo,
                             GetManifestReceiveFlagCodeList(manifestReceiveFlag),
                             orderNo,
                             DockCodeLDAP
                             });

                 fileName = "GoodsReceiptInquiryPerManifest" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + supplierCode.Trim().ToUpper() + rcvPlantCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
                 hasil = exporter.Write(ConvertToDataTable(qryPerManifest), "GRPerManifest");
             }
             else
             {*/
            qry = db.Fetch<SupplierReconciliationGridReportDetail>(
                    "ReportGetAllSRDetail",
                        new object[] { 
                                DocumentNumber
                            });

            //fileName = "GoodsReceiptInquiry" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + supplierCode.Trim().ToUpper() + rcvPlantCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
            fileName = "SupplierReconciliationDetail" + "_" + DocumentNumber + ".xls";
            hasil = exporter.Write(ConvertToDataTable(qry), "GR");
            // }

            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            /*
             * @Lufty: numpang nambahin, kisanak
             */
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        public void DownloadSR(
            int TabSelected,
            String Supplier = "",
            String Balance = "",
            String Progress = ""

            )
        {
            string fileName = String.Empty;

            IEnumerable<SupplierReconciliationGridReport> qry;
            IEnumerable<SupplierReconciliationGridReportSum> q;


            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;

            IDBContext db = DbContext;

            switch (TabSelected)
            {
                case 0:
                    qry = db.Fetch<SupplierReconciliationGridReport>(
                    "ReportGetAllSR",
                        new object[] { 
                                Supplier,
                                Balance,
                                Progress
                            });

                    //fileName = "GoodsReceiptInquiry" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + supplierCode.Trim().ToUpper() + rcvPlantCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
                    fileName = "SupplierReconciliation" + "_" + Supplier + "_" + Balance + ".xls";
                    hasil = exporter.Write(ConvertToDataTable(qry), "GR");

                    db.Close();
                    Response.Clear();
                    //Response.ContentType = result.MimeType;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.Expires = -1;
                    Response.Buffer = true;

                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
                    /* ------------------------------*/
                    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
                    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                    Response.BinaryWrite(hasil);
                    Response.End();
                    break;
                case 1:
                    q = db.Fetch<SupplierReconciliationGridReportSum>(
                    "ReportGetAllSRSum",
                        new object[] { 
                                Supplier,
                                Balance,
                                Progress
                            });

                    //fileName = "GoodsReceiptInquiry" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + supplierCode.Trim().ToUpper() + rcvPlantCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
                    fileName = "SupplierReconciliation_Summary" + "_" + Supplier + "_" + Balance + ".xls";
                    hasil = exporter.Write(ConvertToDataTable(q), "GR");

                    db.Close();
                    Response.Clear();
                    //Response.ContentType = result.MimeType;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.Expires = -1;
                    Response.Buffer = true;

                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
                    /* ------------------------------*/
                    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
                    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                    Response.BinaryWrite(hasil);
                    Response.End();
                    break;

            }

            // }


        }

        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        //[HttpPost]
        //public void OnUploadCallbackRouteValues()
        //{
        //    UploadControlExtension.GetUploadedFiles("btnBrowseSR", ValidationSettings, FileUploadComplete);
        //}

        public ActionResult OnUploadCallbackRouteValues(IEnumerable<UploadedFile> ucCallbacks)
        {

            UploadControlExtension.GetUploadedFiles("btnBrowseSR", ValidationSettings, FileUploadComplete);

            return null;
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls", ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            //if (e.UploadedFile.IsValid)
            //{
            //    _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

            //    e.UploadedFile.SaveAs(_FilePath, true);
            //    e.CallbackData = _FilePath;
            //}
        }

        public void UploadValid(String functionId, String processId, int SelectedTab, String temporaryTableValidate)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //string ResultQuery = "Data Has Been Saved";
            //try
            //{
            //    SupplierReconciliationGrid Check = new SupplierReconciliationGrid();
            //    Check = db.SingleOrDefault<SupplierReconciliationGrid>("CheckPostingDate", new object[] { });

            //    if (Check.Equals(0))
            //    {
            //        ResultQuery = "Cannot add Reconcile. Data Reconcile of previous month is not exist.";
            //    }
            //    else
            //    {
            db.ExecuteScalar<string>("InsertSupplierReconciliationUpload",
                    new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            SelectedTab,
                            AuthorizedUser.Username
                        });
            //    }



            //}
            //catch (Exception ex)
            //{
            //    ResultQuery = "Error: " + ex.Message;

            //}
            //db.Close();
            //return Content(ResultQuery);

        }

        [HttpPost]
        public void ClearUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                // clear data in temporary uploaded table.
                db.Execute("ClearFeedbackPartUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception exc)
            {
                throw new Exception();
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/SupplierReconciliation/TempUpload/");
                return _uploadDirectory;
            }
        }

        private long _processId = 0;
        private long ProcessId
        {

            get
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                if (_processId == 0)
                {
                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); }

                    return _processId;

                    //_processId = Math.Abs(Environment.TickCount);
                }

                return _processId;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        public FileContentResult DownloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;


            fileName = "SRTemp.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/SupplierReconciliation/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        [HttpPost]
        public ActionResult SupplierReconciliationUpload(string filePath, int SelectedTab)
        {
            AllProcessUpload(filePath, SelectedTab);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        private SupplierReconciliationGridModel AllProcessUpload(string filePath, int SelectedTab)
        {
            SupplierReconciliationGridModel _UploadModel = new SupplierReconciliationGridModel();

            #region DECLARE XLS PROPERTIES
            // table destination properties
            _TableDestination.functionID = "71003";
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.tableFromTemp = "TB_T_REC_H_UPLOAD";
            _TableDestination.tableToAccess = "TB_T_REC_H_VALIDATE";

            switch (SelectedTab)
            {
                case 0:
                    _TableDestination.sheetName = "SupplierReconciliation";
                    _TableDestination.columnEnd = "16";
                    break;

            }
            #endregion

            LogProvider logProvider = new LogProvider("", "7", AuthorizedUser);
            ILogSession sessionSP = logProvider.CreateSession();

            OleDbConnection excelConn = null;

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                sessionSP.Write("MPCS00004INF", new object[] { "Process Upload has been started..!!!" });

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableFromTemp });
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties = Excel 8.0";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                excelCommand = new OleDbCommand(@"select " +
                                    _TableDestination.functionID + " as FUNCTION_ID, " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @" *
                                from [" + _TableDestination.sheetName + "$]", excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    //bulkCopy.ColumnMappings.Add("ProductionMonth", "PROD_MONTH");
                    //bulkCopy.ColumnMappings.Add("SupplierCode", "SUPPLIER_CD");

                    switch (SelectedTab)
                    {
                        ///maping excel to sql 
                        ///when quality is upload
                        case 0:
                            bulkCopy.ColumnMappings.Add("SupplierCode", "SUPPLIER_CD");
                            bulkCopy.ColumnMappings.Add("DocumentNumber", "DOCUMENT_NO");
                            bulkCopy.ColumnMappings.Add("Curr", "ITEM_CURR");
                            bulkCopy.ColumnMappings.Add("SupplierAmount", "SUPP_AMOUNT");
                            bulkCopy.ColumnMappings.Add("SupplierTaxAmount", "SUPP_TAX_AMOUNT");
                            break;
                        ///when delivery is upload

                    }
                #endregion

                    try
                    {
                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        sessionSP.Write("MPCS00002ERR", new string[] { ex.Source, ex.Message, "", "" });
                        Console.WriteLine(ex.Message);
                    }
                }

                String columnNameList = GetColumnName();

                // validate data in temporary uploaded table.
                db.Execute("ValidateSupplierReconciliationUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });

                // check for invalid data.
                try
                {
                    _ErrorValidationList = db.Fetch<ErrorValidation>("GetErrorMessageUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });
                }
                catch (Exception exc)
                {
                    sessionSP.Write("MPCS00002ERR", new string[] { exc.Source, exc.Message, "", "" });
                    throw exc;
                }

                if (_ErrorValidationList.Count == 0)
                {
                    // insert data from temporary uploaded table into temporary validated table.
                    db.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] { _TableDestination.tableToAccess, columnNameList, columnNameList, _TableDestination.tableFromTemp });

                    // read uploaded data from temporary validated table to transcation table.
                    //_UploadedLPOPConsolidationModel = GetAllLPOPConsolidationUpload(productionMonth, version, _TableDestination.processID, _TableDestination.functionID);

                    // set process validation status.
                    TempData["IsValid"] = "true;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;

                    //add by fid.goldy
                    //+ ";"+ Convert.ToInt32(ds.Tables[0].Rows[0].Field<double>("productionmonth")) + ";"+ ds.Tables[0].Rows[0].Field<string>("SupplierCode");
                    //end of add by fid.goldy

                    sessionSP.Write("MPCS00004INF", new object[] { @"Insert upload success info into table log detail.
                                                                        Insert data from temporary uploaded table into temporary validated table.
                                                                        Read uploaded data from temporary validated table to transcation table.
                                                                        set process validation status...!!!" });
                }
                else
                {

                    // load previous data for supplier feedback part model
                    //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail = GetAllLPOPConsolidationUpload(productionMonth, version);

                    // set process validation status.
                    TempData["IsValid"] = "false;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;

                    sessionSP.Write("MPCS00004INF", new object[] { @"Insert upload fail info into table log detail
                                                                        set process validation status...!!!" });
                }
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                sessionSP.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });

                // load previous data for supplier feedback part model
                //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail= GetAllFeedbackPart(productionMonth, supplierCode, supplierPlanCode, version);
            }
            finally
            {
                sessionSP.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                sessionSP.Commit();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }
            return _UploadModel;
        }

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            //db.Close();

            return String.Join(",", columnNameArray.ToArray());
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }
    }
}
