﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.WorklistDistribution;
using Toyota.Common.Web.Database;
namespace Portal.Controllers
{
    public class WorklistDistributionController : BaseController
    {
        public WorklistDistributionController()
            : base("Worklist Distribution")
        {
        }

        #region Property
        List<WorklistDistribution> _listmodel = null;

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_WORKFLOW);
            }
        }

        private List<WorklistDistribution> ListModel
        {
            get
            {
                if (_listmodel == null)
                    _listmodel = new List<WorklistDistribution>();
                return _listmodel;
            }
            set
            {
                _listmodel = value;
            }
        }
        #endregion

        #region ListGrid

        private List<WorklistDistribution> GetList(string destination, string modulename)
        {
            List<WorklistDistribution> VarList = new List<WorklistDistribution>();
            IDBContext db = DbContext;
            VarList = db.Fetch<WorklistDistribution>("GetWorklistDistribution", new object[] { destination, modulename });
            return VarList;
        }

        public ActionResult ReloadGridWorklist()
        {

            WorklistDistributionModel mdls = Model.GetModel<WorklistDistributionModel>(); ;
            mdls.WorklistDistributionList = GetList(Request.Params["destination"],
                                Request.Params["modulename"]);
            Model.AddModel(mdls);
            return PartialView("GridWorklist", Model);
        }

        #endregion

        protected override void StartUp()
        {

        }

        public ActionResult PopupEditWorklistPartial()
        {
            return PartialView("PopupEditWorklist", Model);
        }

        protected override void Init()
        {
            WorklistDistributionModel mdl = new WorklistDistributionModel();
            mdl.WorklistDistributionList = GetList("", "");
            Model.AddModel(mdl);
        }

        public ContentResult UpdateWorklist(string pdestination, string pmodule, string pfolio, int pmodulecd, string ptempdestination)
        {
            string QueryResult = string.Empty;
            string user = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                var Query = db.ExecuteScalar<string>("UpdateWorklist", new object[] 
                {
                    pdestination, pmodulecd, pfolio, user, ptempdestination
                });
                QueryResult = "Data has been Updated";
            }
            catch (Exception ex)
            {
                QueryResult = "Error: " + ex.Message;
            }
            return Content(QueryResult);
        }


    }
}
