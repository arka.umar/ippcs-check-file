﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Threading;
using System.Data;
using System.Xml;
using System.Transactions;

using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;

using Portal.Models.AnnualPlan;
using Portal.Models.Forecast;
using Portal.Models.Supplier;
using Portal.Models.SupplierFeedbackPart;
using Portal.Models.SupplierFeedbackSummary;

using Portal.ExcelUpload;
using Toyota.Common.Web.Credential;
using System.Diagnostics;
using Toyota.Common.Web.Excel;

/*
 * Description      : Controller for Forecast
 * Created by/date  : niit.yudha/11-des-2012
 * Changed by/date  : fid.salman/21-jan-2013
 */

namespace Portal.Controllers
{

    public class ForecastController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        //private IDBContext DbContextLdap
        //{
        //    get
        //    {
        //        return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_LDAP_OPEN);
        //    }
        //}

        public ForecastController()
            : base("Monthly Forecast")
        {
            PageLayout.UseMessageBoard = true;
        }

        #region Model properties
        List<SupplierName> _forecastSupplierModel = null;
        private List<SupplierName> _ForecastSupplierModel
        {
            get
            {
                if (_forecastSupplierModel == null)
                    _forecastSupplierModel = new List<SupplierName>();

                return _forecastSupplierModel;
            }
            set
            {
                _forecastSupplierModel = value;
            }
        }

        List<SupplierPlan> _forecastSupplierPlanModel = null;
        private List<SupplierPlan> _ForecastSupplierPlanModel
        {
            get
            {
                if (_forecastSupplierPlanModel == null)
                    _forecastSupplierPlanModel = new List<SupplierPlan>();

                return _forecastSupplierPlanModel;
            }
            set
            {
                _forecastSupplierPlanModel = value;
            }
        }

        ForecastModelList _forecastModel = null;
        private ForecastModelList _ForecastModel
        {
            get
            {
                if (_forecastModel == null)
                    _forecastModel = new ForecastModelList();

                return _forecastModel;
            }
            set
            {
                _forecastModel = value;
            }
        }

        List<ForecastDetailDataModel> _forecastDetailDataModel = null;
        private List<ForecastDetailDataModel> _ForecastDetailDataModel
        {
            get
            {
                if (_forecastDetailDataModel == null)
                    _forecastDetailDataModel = new List<ForecastDetailDataModel>();

                return _forecastDetailDataModel;
            }
            set
            {
                _forecastDetailDataModel = value;
            }
        }

        SupplierFeedbackPartModel _supplierFeedbackPartModel = null;
        private SupplierFeedbackPartModel _SupplierFeedbackPartModel
        {
            get
            {
                if (_supplierFeedbackPartModel == null)
                    _supplierFeedbackPartModel = new SupplierFeedbackPartModel();

                return _supplierFeedbackPartModel;
            }
            set
            {
                _supplierFeedbackPartModel = value;
            }
        }

        SupplierFeedbackPartStatus _supplierFeedbackPartStatusModel = null;
        private SupplierFeedbackPartStatus _SupplierFeedbackPartStatusModel
        {
            get
            {
                if (_supplierFeedbackPartStatusModel == null)
                    _supplierFeedbackPartStatusModel = new SupplierFeedbackPartStatus();

                return _supplierFeedbackPartStatusModel;
            }
            set
            {
                _supplierFeedbackPartStatusModel = value;
            }
        }

        SupplierFeedbackPartParameter _supplierFeedbackPartParameterModel = null;
        private SupplierFeedbackPartParameter _SupplierFeedbackPartParameterModel
        {
            get
            {
                if (_supplierFeedbackPartParameterModel == null)
                    _supplierFeedbackPartParameterModel = new SupplierFeedbackPartParameter();

                return _supplierFeedbackPartParameterModel;
            }
            set
            {
                _supplierFeedbackPartParameterModel = value;
            }
        }

        SupplierFeedbackSummaryModel _supplierFeedbackSummaryModel = null;
        private SupplierFeedbackSummaryModel _SupplierFeedbackSummaryModel
        {
            get
            {
                if (_supplierFeedbackSummaryModel == null)
                    _supplierFeedbackSummaryModel = new SupplierFeedbackSummaryModel();

                return _supplierFeedbackSummaryModel;
            }
            set
            {
                _supplierFeedbackSummaryModel = value;
            }
        }

        SupplierFeedbackSummaryParameter _supplierFeedbackSummaryParameterModel = null;
        private SupplierFeedbackSummaryParameter _SupplierFeedbackSummaryParameterModel
        {
            get
            {
                if (_supplierFeedbackSummaryParameterModel == null)
                    _supplierFeedbackSummaryParameterModel = new SupplierFeedbackSummaryParameter();

                return _supplierFeedbackSummaryParameterModel;
            }
            set
            {
                _supplierFeedbackSummaryParameterModel = value;
            }
        }

        AnnualPlanModel _annualPlanModel = null;
        private AnnualPlanModel _AnnualPlanModel
        {
            get
            {
                if (_annualPlanModel == null)
                    _annualPlanModel = new AnnualPlanModel();

                return _annualPlanModel;
            }
            set
            {
                _annualPlanModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
            /*
             * NOTE BY   : fid.salman 
             * NOTE DATE : 01-Feb-2013
             * NOTE      : 
             *      - StartUp() method used to store model for first time, and it only executed once.
             *      - Either in StartUp() or Init(), model always flushed after the controll class cycle.
             */
            Lookup.Add(new ForecastDataCache());
        }

        protected override void Init()
        {
            /*
             * NOTE BY   : fid.salman 
             * NOTE DATE : 01-Feb-2013
             * NOTE      : 
             *      - Init() method used to store model every time controll class called.
             *      - Either in StartUp() or Init(), model always flushed after the controll class cycle.
             */

        }

        #region Forecast controller
        #region View controller
        public ActionResult PartialHeaderForecast()
        {
            #region Required model in partial page : for ForecastHeaderPartial

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");

            if (suppliers.Count > 0)
            {
                _ForecastSupplierModel = suppliers;
                _ForecastSupplierPlanModel = GetAllSupplierPlan(suppliers[0].SUPPLIER_CODE);
            }
            else
            {
                _ForecastSupplierModel = GetAllSupplier();
                _ForecastSupplierPlanModel = GetAllSupplierPlan(String.Empty);
            }

            Model.AddModel(_ForecastSupplierModel);
            Model.AddModel(_ForecastSupplierPlanModel);
            #endregion

            return PartialView("ForecastHeaderPartial", Model);
        }

        public ActionResult PartialHeaderSupplierPlanLookupGridForecast()
        {

            #region Required model in partial page : for FHSupplierPlanCodeOption GridLookup
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");
            String supplierCode = String.Empty;
            if (suppliers.Count > 0)
            {
                //supplierCode = suppliers.Select(i => i.SUPPLIER_CODE).SingleOrDefault<String>();
                supplierCode = suppliers[0].SUPPLIER_CODE;
            }

            TempData["GridName"] = "FHSupplierPlanCodeOption";
            _ForecastSupplierPlanModel = GetAllSupplierPlan(supplierCode == "0000" ? Request.Params["SupplierCode"] : supplierCode);
            #endregion

            return PartialView("GridLookup/PartialGrid", _ForecastSupplierPlanModel);
        }

        public ActionResult PartialHeaderSupplierLookupGridForecast()
        {
            #region Required model in partial page : for FHSupplierIDOption GridLookup
            TempData["GridName"] = "FHSupplierIDOption";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");
            if (suppliers.Count > 0)
                _ForecastSupplierModel = suppliers;
            else
                _ForecastSupplierModel = GetAllSupplier();

            #endregion

            return PartialView("GridLookup/PartialGrid", _ForecastSupplierModel);
        }

        public ActionResult PartialDetailForecast()
        {
            // get current month production month feedback summary
            DateTime currentMonth = DateTime.Now;
            String currentProductionMonth = Convert.ToString(currentMonth.Year) + String.Format("{0:D2}", (currentMonth.Month + 1));

            // get current user login detail
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");

            String supplierCode = String.Empty;
            String supplierPlant = String.Empty;
            if (suppliers.Count == 1)
            {
                /*
                 * Man ini yg soal supplier plant code juga ya
                 */
                //supplierCode = suppliers.Select(i => i.SUPPLIER_CODE).SingleOrDefault<String>();
                //supplierPlant = suppliers.Select(i => i.SUPPLIER_PLANT_CD).SingleOrDefault<String>();

                supplierCode = suppliers[0].SUPPLIER_CODE;
                //supplierPlant = suppliers[0].SUPPLIER_PLANT_CD;
            }

            #region Required model in partial page : for ForecastDetailPartial
            _ForecastModel = GetAllForecast(String.IsNullOrEmpty(Request.Params["ProductionMonth"]) ? currentProductionMonth : Request.Params["ProductionMonth"], String.IsNullOrEmpty(supplierCode) ? Request.Params["SupplierCode"] : supplierCode, String.IsNullOrEmpty(supplierPlant) ? Request.Params["SupplierPlanCode"] : supplierPlant);
            Model.AddModel(_ForecastModel);
            #endregion

            return PartialView("ForecastDetailPartial", Model);
        }
        #endregion

        #region Common controller
        private String GetGeneralProductionMonth(String productionMonth)
        {
            Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
            monthDictionary.Add("01", "January");
            monthDictionary.Add("02", "February");
            monthDictionary.Add("03", "March");
            monthDictionary.Add("04", "April");
            monthDictionary.Add("05", "May");
            monthDictionary.Add("06", "June");
            monthDictionary.Add("07", "July");
            monthDictionary.Add("08", "August");
            monthDictionary.Add("09", "September");
            monthDictionary.Add("10", "October");
            monthDictionary.Add("11", "November");
            monthDictionary.Add("12", "December");

            String generalProductionMonth = "";

            if (!String.IsNullOrEmpty(productionMonth))
            {
                String year = productionMonth.Substring(0, 4);
                String month = productionMonth.Substring(productionMonth.Length - 2, 2);

                generalProductionMonth = monthDictionary[month] + " " + year;
            }

            return generalProductionMonth;
        }
        #endregion

        #region Database controller
        private ForecastModelList GetAllForecast(String productionMonth, String supplierCode, String supplierPlanCode)
        {
            ForecastModelList forecastModelList = new ForecastModelList();
            IDBContext db = DbContext;
            try
            {
                forecastModelList.Collection = db.Fetch<ForecastModel>("GetAllForeCast", new object[] { productionMonth, supplierCode, supplierPlanCode });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return forecastModelList;
        }

        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> forecastSupplierModelList = new List<SupplierName>();
            IDBContext db = DbContext;
            try
            {
                forecastSupplierModelList = db.Fetch<SupplierName>("GetAllSupplierForecast", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return forecastSupplierModelList;
        }

        private List<SupplierPlan> GetAllSupplierPlan(String supplierCode)
        {
            List<SupplierPlan> forecastSupplierPlantModelList = new List<SupplierPlan>();
            IDBContext db = DbContext;
            try
            {
                forecastSupplierPlantModelList = db.Fetch<SupplierPlan>("GetAllSupplierPlantForecast", new object[] { supplierCode });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return forecastSupplierPlantModelList;
        }

        public ContentResult UnlockForecast(String productionMonth, String supplierCode, String supplierPlanCode, String version)
        {
            String resultMessage = "";

            if ((productionMonth != "" && productionMonth != null) || (supplierCode != "" && supplierCode != null) || (supplierPlanCode != "" && supplierPlanCode != null) || (version != "" && version != null))
            {
                IDBContext db = DbContext;
                try
                {
                    resultMessage += db.ExecuteScalar<string>("UnlockForecast", new object[] { productionMonth, supplierCode, supplierPlanCode, version, AuthorizedUser.Username });
                }
                catch (Exception exc) { throw exc; }
                finally { db.Close(); }

                resultMessage = "Unlock forecast success.";
            }
            else
            {
                resultMessage = "Unlock forecast failed, neither production month nor supplier code is filled.";
            }

            return Content(resultMessage.ToString());
        }

        public void UpdateForecast(String productionMonth, String supplierCode, String supplierPlanCode, String version)
        {
            String resultMessage = "";

            if ((productionMonth != "" && productionMonth != null) || (supplierCode != "" && supplierCode != null) || (supplierPlanCode != null) || (version != "" && version != null))
            {
                IDBContext db = DbContext;
                try
                {
                    resultMessage += db.ExecuteScalar<string>("UpdateForecast", new object[] { productionMonth, supplierCode, supplierPlanCode, version, AuthorizedUser.Username });
                }
                catch (Exception exc) { throw exc; }
                finally { db.Close(); }

                resultMessage = "Update forecast success.";
            }
            else
            {
                resultMessage = "Update forecast failed, neither production month nor supplier code is filled.";
            }

            //return Content(resultMessage.ToString());
        }
        public string GetLdapCompanyID()
        {
            //IDBContext dbLdap = DbContextLdap;
            String resultMessage = "";
            try
            {
                resultMessage = DbContext.ExecuteScalar<string>("GetLdapCompanyID", new object[] { AuthorizedUser.Username });
            }
            catch (Exception exc) { throw exc; }
            finally { DbContext.Close(); }

            return (resultMessage.ToString());
        }
        #endregion
        #endregion

        #region Forecast Feedback Part controller
        #region View controller
        public ActionResult PartialPopupForecastFeedbackPart()
        {
            #region Required model in partial page
            #region Model for ForecastFeedbackPartHeaderLeftPanel
            _SupplierFeedbackPartParameterModel.PRODUCTION_MONTH = Request.Params["ProductionMonth"];
            _SupplierFeedbackPartParameterModel.SUPPLIER_CODE = Request.Params["SupplierCode"];
            _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD = Request.Params["SupplierPlanCode"];
            _SupplierFeedbackPartParameterModel.VERSION = Request.Params["Version"];
            _SupplierFeedbackPartParameterModel.PARENT = "1";
            _SupplierFeedbackPartParameterModel.LOCK = Request.Params["Lock"];

            if (_SupplierFeedbackPartParameterModel.LOCK == "false")
            {
                _SupplierFeedbackPartParameterModel.LOCK = "0";
            }
            else
            {
                _SupplierFeedbackPartParameterModel.LOCK = "1";
            }

            Model.AddModel(_SupplierFeedbackPartParameterModel);
            #endregion

            #region Model for SupplierFeedbackPartRightHeaderPartial
            _SupplierFeedbackPartStatusModel = GetSupplierFeedbackStatus(_SupplierFeedbackPartParameterModel.PRODUCTION_MONTH, _SupplierFeedbackPartParameterModel.SUPPLIER_CODE, _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD, _SupplierFeedbackPartParameterModel.VERSION, _SupplierFeedbackPartParameterModel.LOCK);
            Model.AddModel(_SupplierFeedbackPartStatusModel);
            #endregion

            #region Model for SFPGFSupplierFeedbackPartView
            _SupplierFeedbackPartModel.SupplierFeedbackParts = GetAllFeedbackPart(_SupplierFeedbackPartParameterModel.PRODUCTION_MONTH, _SupplierFeedbackPartParameterModel.SUPPLIER_CODE, _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD, _SupplierFeedbackPartParameterModel.VERSION, _SupplierFeedbackPartParameterModel.LOCK);
            Model.AddModel(_SupplierFeedbackPartModel);
            #endregion

            ForecastDataCache cache = Lookup.Get<ForecastDataCache>();
            Model.AddModel(cache);
            #endregion

            return PartialView("ForecastFeedbackPartPartial", Model);
        }

        public void _ajax_CacheUpcomingMessageBoardName(string boardName)
        {
            ForecastDataCache cache = Lookup.Get<ForecastDataCache>();
            cache.UpcomingUploadMessageBoardName = boardName;
        }

        public ActionResult PartialEmptyPopupForecastFeedbackPart()
        {
            ForecastDataCache cache = Lookup.Get<ForecastDataCache>();
            Model.AddModel(cache);
            Model.AddModel(_SupplierFeedbackPartParameterModel);
            Model.AddModel(_SupplierFeedbackPartModel);

            return PartialView("ForecastFeedbackPartPartial", Model);
        }
        #endregion

        #region Database controller
        private List<SupplierFeedbackPartDetail> GetAllFeedbackPart(String productionMonth, String supplierCode, String supplierPlanCode, String version, String feedbacklock, String partNo = "")
        {
            List<SupplierFeedbackPartDetail> feedbackPartModel = new List<SupplierFeedbackPartDetail>();

            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();

            string CompanyID = "", noreg = "";
            noreg = user.NoReg;
            if (noreg == null)
            {
                noreg = "0";
            }
            CompanyID = GetLdapCompanyID();
            IDBContext db = DbContext;
            try
            {
                if (noreg == "0" && Convert.ToInt32(CompanyID) > 1)
                {
                    feedbackPartModel = db.Fetch<SupplierFeedbackPartDetail>("GetAllFeedbackPartSupp", new object[] { productionMonth, supplierCode, supplierPlanCode, version, feedbacklock, partNo });
                }
                else
                {
                    feedbackPartModel = db.Fetch<SupplierFeedbackPartDetail>("GetAllFeedbackPart", new object[] { productionMonth, supplierCode, supplierPlanCode, version, feedbacklock, partNo });
                }
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return feedbackPartModel;
        }

        private SupplierFeedbackPartStatus GetSupplierFeedbackStatus(String productionMonth, String supplierCode, String supplierPlantCode, String version, String FeedbackLock)
        {
            SupplierFeedbackPartStatus supplierFeedbackPartStatus = new SupplierFeedbackPartStatus();

            IDBContext db = DbContext;
            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();


            string CompanyID = "", noreg = "";
            noreg = user.NoReg;
            if (noreg == null)
            {
                noreg = "0";
            }
            CompanyID = GetLdapCompanyID();

            try
            {

                if (noreg == "0" && Convert.ToInt32(CompanyID) > 1)
                {
                    var QueryLog = db.Query<SupplierFeedbackPartStatus>("GetFeedbackPartStatusSupp", new object[] { productionMonth, supplierCode, supplierPlantCode, version, FeedbackLock });
                    if (QueryLog.Any())
                    {
                        foreach (var q in QueryLog)
                        {
                            supplierFeedbackPartStatus.IS_N_REPLIED = q.IS_N_REPLIED;
                            supplierFeedbackPartStatus.IS_N_1_REPLIED = q.IS_N_1_REPLIED;
                            supplierFeedbackPartStatus.IS_N_2_REPLIED = q.IS_N_2_REPLIED;
                            supplierFeedbackPartStatus.IS_N_3_REPLIED = q.IS_N_3_REPLIED;
                            supplierFeedbackPartStatus.N_STATUS = q.N_STATUS;
                            supplierFeedbackPartStatus.N_1_STATUS = q.N_1_STATUS;
                            supplierFeedbackPartStatus.N_2_STATUS = q.N_2_STATUS;
                            supplierFeedbackPartStatus.N_3_STATUS = q.N_3_STATUS;
                            supplierFeedbackPartStatus.IS_CONFIRMED = q.IS_CONFIRMED;
                            supplierFeedbackPartStatus.LOCK = q.LOCK;
                        }
                    }
                }
                else
                {
                    var QueryLog = db.Query<SupplierFeedbackPartStatus>("GetFeedbackPartStatus", new object[] { productionMonth, supplierCode, supplierPlantCode, version, FeedbackLock });
                    if (QueryLog.Any())
                    {
                        foreach (var q in QueryLog)
                        {
                            supplierFeedbackPartStatus.IS_N_REPLIED = q.IS_N_REPLIED;
                            supplierFeedbackPartStatus.IS_N_1_REPLIED = q.IS_N_1_REPLIED;
                            supplierFeedbackPartStatus.IS_N_2_REPLIED = q.IS_N_2_REPLIED;
                            supplierFeedbackPartStatus.IS_N_3_REPLIED = q.IS_N_3_REPLIED;
                            supplierFeedbackPartStatus.N_STATUS = q.N_STATUS;
                            supplierFeedbackPartStatus.N_1_STATUS = q.N_1_STATUS;
                            supplierFeedbackPartStatus.N_2_STATUS = q.N_2_STATUS;
                            supplierFeedbackPartStatus.N_3_STATUS = q.N_3_STATUS;
                            supplierFeedbackPartStatus.IS_CONFIRMED = q.IS_CONFIRMED;
                            supplierFeedbackPartStatus.LOCK = q.LOCK;
                        }
                    }
                }

            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return supplierFeedbackPartStatus;
        }
        #endregion
        #endregion

        #region Forecast Feedback Summary controller
        #region View controller
        public ActionResult PartialPopupForecastFeedbackSummary()
        {
            #region Required model in partial page
            #region Model for SupplierFeedbackSummaryHeaderPartial
            _SupplierFeedbackSummaryParameterModel.PRODUCTION_MONTH = Request.Params["ProductionMonth"];
            _SupplierFeedbackSummaryParameterModel.SUPPLIER_CODE = Request.Params["SupplierCode"];
            _SupplierFeedbackSummaryParameterModel.SUPPLIER_PLANT_CD = Request.Params["SupplierPlanCode"];
            _SupplierFeedbackSummaryParameterModel.VERSION = Request.Params["Version"];
            Model.AddModel(_SupplierFeedbackSummaryParameterModel);

            _ForecastSupplierModel = GetAllSupplier();
            _ForecastSupplierPlanModel = GetAllSupplierPlan(String.Empty);

            Model.AddModel(_ForecastSupplierModel);
            Model.AddModel(_ForecastSupplierPlanModel);
            #endregion

            #region Model for SupplierFeedbackSummaryDetailPartial
            _SupplierFeedbackSummaryModel = GetAllFeedbackSummary(_SupplierFeedbackSummaryParameterModel.PRODUCTION_MONTH, _SupplierFeedbackSummaryParameterModel.VERSION, _SupplierFeedbackSummaryParameterModel.SUPPLIER_CODE, _SupplierFeedbackSummaryParameterModel.SUPPLIER_PLANT_CD);
            Model.AddModel(_SupplierFeedbackSummaryModel);
            #endregion
            #endregion

            return PartialView("ForecastFeedbackSummaryPartial", Model);
        }

        public ActionResult PartialEmptyPopupForecastFeedbackSummary()
        {
            Model.AddModel(_SupplierFeedbackSummaryParameterModel);
            Model.AddModel(_SupplierFeedbackSummaryModel);

            return PartialView("ForecastFeedbackSummaryPartial", Model);
        }
        #endregion

        #region Database controller
        private SupplierFeedbackSummaryModel GetAllFeedbackSummary(String productionMonth, String timing, String supplierCode, String supplierPlanCode)
        {
            SupplierFeedbackSummaryModel supplierFeedbackSummaryModel = new SupplierFeedbackSummaryModel();

            IDBContext db = DbContext;
            try
            {
                supplierFeedbackSummaryModel.SupplierFeedbackSummary = db.Fetch<SupplierFeedbackSummary>("GetAllFeedbackSummary", new object[] { productionMonth, timing, supplierCode, supplierPlanCode });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return supplierFeedbackSummaryModel;
        }
        #endregion
        #endregion

        #region Forecast Detail Data controller
        #region View controller
        public ActionResult PartialPopupForecastDetailData()
        {
            #region Required model in partial page
            #region Model for ForecastDetailDataDetailPartial
            _ForecastDetailDataModel = GetAllForecastDetailData(Request.Params["ProductionMonth"], Request.Params["SupplierCode"], Request.Params["SupplierPlanCode"]);
            Model.AddModel(_ForecastDetailDataModel);
            #endregion
            #endregion

            return PartialView("ForecastDetailDataPartial", Model);
        }

        public ActionResult PartialEmptyPopupForecastDetailData()
        {
            Model.AddModel(_ForecastDetailDataModel);

            return PartialView("ForecastDetailDataPartial", Model);
        }

        public ActionResult PartialDetailForecastDetailData()
        {
            #region Required model in partial page
            #region Model for ForecastDetailDataDetailPartial
            _ForecastDetailDataModel = GetAllForecastDetailData(Request.Params["ProductionMonth"], Request.Params["SupplierCode"], Request.Params["SupplierPlanCode"]);
            Model.AddModel(_ForecastDetailDataModel);
            #endregion
            #endregion

            return PartialView("ForecastDetailDataDetailPartial", Model);
        }
        #endregion

        #region Database controller
        private List<ForecastDetailDataModel> GetAllForecastDetailData(String productionMonth, String supplierCode, String supplierPlanCode)
        {
            List<ForecastDetailDataModel> forecastDetailDataModelList = new List<ForecastDetailDataModel>();

            // Update download user and status
            IDBContext db = DbContext;
            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();
            IEnumerable<ForecastDetailDataModel> qry;

            string CompanyID = "", noreg = "";
            noreg = user.NoReg;
            if (noreg == null)
            {
                noreg = "0";
            }
            CompanyID = GetLdapCompanyID();
            try
            {
                if (noreg == "0" && Convert.ToInt32(CompanyID) > 1)
                {
                    forecastDetailDataModelList = db.Fetch<ForecastDetailDataModel>("GetAllForeCastDetailDataSupplier", new object[] { productionMonth, supplierCode, supplierPlanCode });
                }
                else
                {
                    forecastDetailDataModelList = db.Fetch<ForecastDetailDataModel>("GetAllForeCastDetailData", new object[] { productionMonth, supplierCode, supplierPlanCode });
                }
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return forecastDetailDataModelList;
        }

        private string SetNoticeIconLock(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/Lock-icon.png";
            string imageBytes2 = "~/Content/Images/Unlock-icon.png";

            if (val % 2 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 2 == 1)
            {
                img = imageBytes2;
            }
            return img;
        }
        #endregion
        #endregion

        #region Download controller
        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /// <summary>
        /// Download forecast detail data from table tb_r_lpop.
        /// </summary>
        /// <param name="partNo"></param>
        /// <param name="productionMonth"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        public void DownloadForecastDetailData(String productionMonth, String supplierCode, String supplierPlanCode, String version, String downloadBy)
        {
            string fileName = String.Empty;

            // Update download user and status
            IDBContext db = DbContext;
            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();
            IEnumerable<ForecastDetailDataModel> qry;

            string CompanyID = "", noreg = "";
            noreg = user.NoReg;
            if (noreg == null)
            {
                noreg = "0";
            }
            CompanyID = GetLdapCompanyID();

            if (downloadBy == "0")
            {
                if (noreg == "0" && Convert.ToInt32(CompanyID) > 1)
                {
                    fileName = "ForecastDetailData" + (version == "T" ? "Tentative" : "Firm") + productionMonth + supplierCode + ".xls";
                    qry = db.Fetch<ForecastDetailDataModel>("ReportGetAllForecastDataSupp", new object[] { productionMonth, supplierCode, version });
                }
                else
                {
                    fileName = "ForecastDetailData" + (version == "T" ? "Tentative" : "Firm") + productionMonth + supplierCode + ".xls";
                    qry = db.Fetch<ForecastDetailDataModel>("ReportGetAllForecastData", new object[] { productionMonth, supplierCode, version });
                }
            }
            else
            {
                if (noreg == "0" && Convert.ToInt32(CompanyID) > 1)
                {
                    fileName = "ForecastDetailData" + (version == "T" ? "Tentative" : "Firm") + productionMonth + supplierCode + supplierPlanCode + ".xls";
                    qry = db.Fetch<ForecastDetailDataModel>("ReportGetAllForecastDataByPlantCodeSupp", new object[] { productionMonth, supplierCode, supplierPlanCode, version });
                }
                else
                {
                    fileName = "ForecastDetailData" + (version == "T" ? "Tentative" : "Firm") + productionMonth + supplierCode + supplierPlanCode + ".xls";
                    qry = db.Fetch<ForecastDetailDataModel>("ReportGetAllForecastDataByPlantCode", new object[] { productionMonth, supplierCode, supplierPlanCode, version });
                }

            }

            IExcelWriter exporter = ExcelWriter.GetInstance();

            db.Close();
            UpdateForecast(productionMonth, supplierCode, supplierPlanCode, version);
            byte[] hasil = exporter.Write(ConvertToDataTable(qry), "ForecastDetailData");
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            /*
             * @Lufty: numpang nambahin, kisanak
             */
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion
    }

}