using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Management;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Util.Converter;

using Portal.Models.DailyOrder;
using Portal.Models.DailyOrderPrinting;
using Portal.Models.Supplier;
using Portal.Models.Dock;
using Portal.Models.Globals;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.FTP;
using Telerik.Reporting.Processing;
using Toyota.Common.Web.Credential;
using System.IO;
using Toyota.Common.Web.Excel;
using Toyota.Common.Util.Text;

using System.Drawing;
using System.Drawing.Imaging;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using ThoughtWorks.QRCode.Codec.Util;


namespace Portal.Controllers
{
    public class DailyOrderPrintingSPEXController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public DailyOrderPrintingSPEXController()
            : base("SPEX Order Printing")
        {
        }

        #region MODEL PROPERTIES
        List<Supplier> _orderInquirySupplierModel = null;
        private List<Supplier> _OrderInquirySupplierModel
        {
            get
            {
                if (_orderInquirySupplierModel == null)
                    _orderInquirySupplierModel = new List<Supplier>();

                return _orderInquirySupplierModel;
            }
            set
            {
                _orderInquirySupplierModel = value;
            }
        }

        List<SupplierNameSPEX> _orderInquirySupplierSPEXModel = null;
        private List<SupplierNameSPEX> _OrderInquirySupplierSPEXModel
        {
            get
            {
                if (_orderInquirySupplierSPEXModel == null)
                    _orderInquirySupplierSPEXModel = new List<SupplierNameSPEX>();

                return _orderInquirySupplierSPEXModel;
            }
            set
            {
                _orderInquirySupplierSPEXModel = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            DailyOrderPrintingModelSPEX mdl = new DailyOrderPrintingModelSPEX();
            DailyOrderPrintingDetailModel mdl2 = new DailyOrderPrintingDetailModel();

            Model.AddModel(mdl);
            Model.AddModel(mdl2);
            ViewData["Date"] = DateTime.Now.ToString("dd.MM.yyyy");
            ViewData["onlyGetPrinterList"] = "true";
            ViewData["DateHeaderSchedule"] = DateTime.Now.ToString("dd.MM.yyyy");
        }

        #region VIEW CONTROLLER
        public ActionResult PartialHeaderOrderPrinting()
        {
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrintingSPEX", "OPHSupplierOption");
            _OrderInquirySupplierModel = suppliers;

            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "DailyOrderPrintingSPEX", "OPHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "DailyOrderPrintingSPEX", "OPHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrintingSPEX", "OPHDockOption");
            _OrderInquiryDockModel = DockCodes;

            Model.AddModel(_OrderInquirySupplierModel);
            Model.AddModel(_orderInquirySupplierSPEXModel);
            Model.AddModel(_OrderInquiryDockModel);

            return PartialView("OrderPrintingHeaderPartial", Model);
        }

        public ActionResult PartialHeaderPrintSetup()
        {
            return PartialView("OrderPrintSetupPartial", Model);
        }

        public ActionResult PartialDetailOrderPrintingServicePartExport()
        {
            DailyOrderPrintingModelSPEX model = Model.GetModel<DailyOrderPrintingModelSPEX>();
            model.ServicePartExport = GetAllDailyOrderPrinting(Request.Params["Supplier"],
                                                Request.Params["DockCode"],
                                                Request.Params["OrderNo"],
                                                Request.Params["PrintDateFrom"],
                                                Request.Params["PrintDateTo"],
                                                Request.Params["TimeFrom"],
                                                Request.Params["TimeTo"],
                                                "3",
                                                Request.Params["PrintFlag"],
                                                Request.Params["SubSupplier"]);

            return PartialView("OrderPrintingServicePartExportDetailPartial", Model);
        }

        public ActionResult PartialHeaderSupplierLookupGridOrderPrinting()
        {
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrintingSPEX", "OPHSupplierOption");
            TempData["GridName"] = "OPHSupplierOption";
            _OrderInquirySupplierModel = GetAllSupplier();

            return PartialView("GridLookup/PartialGrid", _OrderInquirySupplierModel);
        }

        public ActionResult PartialHeaderSubSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "DailyOrderPrintingSPEX", "OPHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "DailyOrderPrintingSPEX", "OPHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();

            TempData["GridName"] = "OPHSubSupplierOption";
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;
            #endregion

            return PartialView("GridLookup/PartialGrid", _orderInquirySupplierSPEXModel);
        }

        public ActionResult PartialHeaderDockLookupGridOrderPrinting()
        {
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrintingSPEX", "OPHDockOption");
            TempData["GridName"] = "OPHDockOption";
            _OrderInquiryDockModel = GetAllDock(); ;

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }

        public ActionResult PopupHeaderDataPartDetail()
        {
            string manifest = Request.Params["ManifestNo"];
            return PartialView("OrderPrintingHeaderPartPartial", ViewData["ManifestNo"]);
        }

        public ActionResult PopupDataPartDetail()
        {
            string manifestNo = Request.Params["ManifestNo"];
            string problemFlag = Request.Params["ProblemFlag"];
            ViewData["partDailyData"] = DataPartOrderFillGrid(manifestNo, problemFlag);
            return PartialView("OrderPrintingDetailPartPartial", ViewData["partDailyData"]);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PrintManifest()
        {
            Session["onlyGetPrinterList"] = "false";
            return View("PrintManifest");
        }
        #endregion

        #region GRID DATA ACCESS
        private List<Supplier> GetAllSupplier()
        {
            List<Supplier> orderInquirySupplierModel = new List<Supplier>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<Supplier>("GetAllSupplierGrid", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<SupplierNameSPEX> GetAllSubSupplier()
        {
            List<SupplierNameSPEX> orderInquirySupplierModel = new List<SupplierNameSPEX>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierNameSPEX>("GetAllSubSupplierSPEX", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            IDBContext db = DbContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();
            return orderInquiryDockModel;
        }

        private List<DailyOrderPrintingSPEX> GetAllDailyOrderPrinting(String supplier,
                                String dockCode,
                                String orderNo,
                                String printDateFrom,
                                String printDateTo,
                                String timeFrom,
                                String timeTo,
                                String OrderType,
                                String printFlag,
                                String subsupp)
        {
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<DailyOrderPrintingSPEX> dailyOrderModel = new List<DailyOrderPrintingSPEX>();
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrinting", "OPHSupplierOption");
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            //string supplierCode = "";
            //string supplierPlantCode = "";
            //if (supplier != "" && supplier != null)
            //{
            //    string[] Supplier = supplier.Split('-');
            //    supplierCode = Supplier[0].Replace(" ", "");
            //    supplierPlantCode = Supplier[1].Replace(" ", "");
            //}

            printFlag = (printFlag == null) ? "0" : printFlag;

            DateTime timefrom = (timeFrom == "" || timeFrom == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(printDateFrom + " " + timeFrom + ":00", "dd.MM.yyyy HH:mm", null);
            DateTime timeto = (timeTo == "" || timeTo == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(printDateTo + " " + timeTo + ":59", "dd.MM.yyyy HH:mm", null);

            IDBContext db = DbContext;
            dailyOrderModel = db.Fetch<DailyOrderPrintingSPEX>("GetAllDailyOrderPrintingSPEX", new object[] {
                //supplierCode == null ? "" : supplierCode.Trim(),
                //supplierPlantCode == null ? "" : supplierPlantCode.Trim(),
                supplier,
                dockCode == null ? "" : dockCode,
                orderNo == null ? "" : orderNo,
                timefrom,
                timeto,
                OrderType,
                SupplierCodeLDAP,
                printFlag,
                DockCodeLDAP,
                subsupp
            });
            db.Close();

            return dailyOrderModel;
        }

        protected List<PartDetailManifest> DataPartOrderFillGrid(string manifestNo, string problemFlag)
        {
            List<PartDetailManifest> getPartDetailManifest = new List<PartDetailManifest>();
            IDBContext db = DbContext;
            getPartDetailManifest = db.Query<PartDetailManifest>("GetPartDetailManifest", new object[] { manifestNo, problemFlag }).ToList<PartDetailManifest>();

            db.Close();
            return getPartDetailManifest;
        }

        private string GetData(string Manifest)
        {
            string statusDock = "";
            IDBContext db = DbContext;
            Manifest = Manifest.Replace("\'", "");
            try
            {
                string DOCK_LIST = db.SingleOrDefault<string>("GetDockPacking");
                List<string> Dock = new List<string>();
                Dock = DOCK_LIST.Split(',').ToList();
                string DOCK_CD = db.SingleOrDefault<string>("GetManifestDock", new object[] { Manifest });

                for (int i = 0; i < Dock.Count; i++)
                {
                    if (Dock[i].Trim() == DOCK_CD)
                    {
                        statusDock = "1";
                        break;
                    }
                    else
                    {
                        statusDock = "2";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return statusDock;
        }
        #endregion

        #region PRINTER SETUP
        class PrinterInfo
        {
            public string MANIFEST_PRINTER_NAME { set; get; }
            public string OTHER_PRINTER_NAME { set; get; }
            public Int32 PAPER_TYPE { set; get; }
        }

        public JsonResult GetPrinterSetup()
        {
            PrinterInfo printerInfo = new PrinterInfo();

            IDBContext db = DbContext;
            try
            {
                printerInfo = db.SingleOrDefault<PrinterInfo>("GetDailyOrderPrinterSetup", new object[] { AuthorizedUser.Username });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return new JsonResult
            {
                Data = new
                {
                    ManifestPrinterName = printerInfo.MANIFEST_PRINTER_NAME,
                    OtherPrinterName = printerInfo.OTHER_PRINTER_NAME,
                    PaperType = printerInfo.PAPER_TYPE
                }
            };
        }

        public ContentResult SavePrinterSetup(string PrinterNameManifest, string PrinterNameOtherDocuments, string PaperType)
        {
            IDBContext db = DbContext;
            db.Execute("InsertDailyOrderPrinterSetup", new object[] { AuthorizedUser.Username, PrinterNameManifest, PrinterNameOtherDocuments, PaperType });

            db.Close();
            return Content("Sukses");
        }
        #endregion

        #region PRINTING PROCESS
        class PrintParameter
        {
            public string urlFile { set; get; }
            public string targetPrinter { set; get; }
        }

        private Telerik.Reporting.Report CreateReport(
                string reportPath, string sqlCommand,
                Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
                Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure)
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }

        public ContentResult PrintDailyOrderLoopNew(
                string ManifestNo, string ManifestPrinter, string OtherDocumentPrinter, 
                string PaperType, string OrderDate, string SupplierCode, 
                string SupplierPlant, string RcvPlantCode, string OrderType, 
                string TotalQty, bool Manifest, bool Kanban, bool Skid, string PrintFlag)
        {
            string jsonresult = "";
            List<string> ManifestNos = ManifestNo.Split(';').ToList();
            List<string> OrderDates = OrderDate.Split(';').ToList();
            List<string> SupplierCodes = SupplierCode.Split(';').ToList();
            List<string> SupplierPlants = SupplierPlant.Split(';').ToList();
            List<string> RcvPlantCodes = RcvPlantCode.Split(';').ToList();
            List<string> TotalQtys = TotalQty.Split(';').ToList();

            try
            {
                List<PrintParameter> printParameters = new List<PrintParameter>();
                string UrlPrint = "";
                for (int i = 0; i < ManifestNos.Count; i++)
                {
                    string FileName = ManifestNos[i] + "-";

                    if (TotalQtys[i] != "0")
                    {
                        if (Session["PrevSupplierCode"] == null)
                        {
                            Session["PrevSupplierCode"] = SupplierCodes[i];
                            Session["PrevSupplierPlant"] = SupplierPlants[i];
                            Session["PrevRcvPlantCode"] = RcvPlantCodes[i];
                        }
                        else
                        {
                            if (SupplierCodes[i] != Session["PrevSupplierCode"].ToString())
                            {
                                UrlPrint = PrintSplitterReport(FileName, OrderDates[i], Session["PrevSupplierCode"].ToString(), Session["PrevSupplierPlant"].ToString(), Session["PrevRcvPlantCode"].ToString(), OrderType);
                                printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });

                                Session["PrevSupplierCode"] = SupplierCodes[i];
                                Session["PrevSupplierPlant"] = SupplierPlants[i];
                                Session["PrevRcvPlantCode"] = RcvPlantCodes[i];
                            }
                        }
                    }

                    //Manifest
                    if (Manifest)
                    {
                        if (PrintFlag == "0") UrlPrint = PrintManifestReport(ManifestNos[i], PaperType, FileName + PaperType + "-", OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);
                        else UrlPrint = PrintManifestReport(ManifestNos[i], PaperType, FileName + PaperType + "-Duplicate-", OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);

                        printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = ManifestPrinter });
                    }

                    if (TotalQtys[i] != "0")
                    {
                        if (Skid)
                        {
                            UrlPrint = PrintSkidReport(ManifestNos[i], FileName, OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);
                            printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                        }

                        if (Kanban)
                        {
                            UrlPrint = PrintSelectionKanbanReport(ManifestNos[i], FileName, OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);
                            printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                        }
                    }

                    UpdatePrintFlag(ManifestNos[i].Replace("'", ""));
                }

                jsonresult = JSON.ToString<List<PrintParameter>>(printParameters);
                return Content(jsonresult);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string PrintSplitterReport(string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string FolderName = "";
            if (OrderType == "2") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            string reportPath = @"\Report\DailyOrder\Rep_Splitter.trdx";
            string sqlCommand = "SP_Rep_DailyOrder_Splitter_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameter.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameter.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameter);

            return SavePDFReportToFTP(rpt, FileName + "5", FolderName, OrderType, "");
        }

        private string filePath = "";
        private string localFilePath = "/Content/DailyOrderPrinting/tmp/";
        private string SavePDFReportToFTP(
                    Telerik.Reporting.Report rpt, string FileName, string FolderName, 
                    string OrderType, string UploadFilePath = "")
        {
            string msg = "";
            string LocalFileName = FileName + ".pdf";
            string ContextFTP = "";
            FileName = FileName + ".pdf";

            FTPUpload vFtp = new FTPUpload();
            filePath = vFtp.Setting.FtpPath("ServicePartExport");
            ContextFTP = "servicepartexport";
            
            if (!vFtp.IsDirectoryExist(filePath, UploadFilePath))
            {
                vFtp.CreateDirectory(filePath + UploadFilePath);
            }
            filePath = filePath + UploadFilePath;

            if (vFtp.directoryExists(filePath + "/" + FolderName))
            {
                if (!vFtp.DirectoryList(filePath + FolderName).Contains((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName))
                {
                    ReportProcessor reportProcess = new ReportProcessor();
                    RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                    bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                    rpt.Dispose();
                }
                else
                {
                    byte[] file = vFtp.FtpDownloadByte((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName, ref msg);
                    if (file.Length <= 1)
                    {
                        ReportProcessor reportProcess = new ReportProcessor();
                        RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                        bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                        rpt.Dispose();
                    }

                    if (file.Length > 1)
                    {
                        if (file[0] == 0)
                        {
                            ReportProcessor reportProcess = new ReportProcessor();
                            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                            bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                            rpt.Dispose();
                        }
                    }
                }
            }
            else
            {
                ReportProcessor reportProcess = new ReportProcessor();
                RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                rpt.Dispose();
            }

            string serverPath = "";
            serverPath = ApplicationUrl + "/FileDownload?context=" + ContextFTP + "&path=" + FolderName + "/" + FileName;

            return serverPath;
        }

        private string PrintManifestReport(string ManifestNos, string PaperType, string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string FolderName = "";
            if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            string reportPath = @"\Report\DailyOrder\Rep_SupplierManifest.trdx";
            if (PaperType == "1")
                reportPath = @"\Report\DailyOrder\Rep_SupplierManifest_WithoutBorder.trdx";

            string sqlCommand = "SP_Rep_DailyOrder_Manifest_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
            if (OrderType == "5") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameters);

            if (PaperType == "1")
            {
                return SavePDFReportToFTP(rpt, FileName + "1", FolderName + "/ManifestPrePrinted", OrderType, "");
            }
            else
            {
                return SavePDFReportToFTP(rpt, FileName + "1", FolderName + "/ManifestPlain", OrderType, "");
            }
        }

        private string PrintSkidReport(string ManifestNos, string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string FolderName = "";
            if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            string reportPath = @"\Report\DailyOrder\Rep_ManifestSkid_Download.trdx";
            string sqlCommand = "SP_Rep_DailyOrder_Skid_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
            if (OrderType == "5") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameters);

            return SavePDFReportToFTP(rpt, FileName + "3", FolderName + "/Skid", OrderType, "");
        }

        private string PrintSelectionKanbanReport(string ManifestNo, string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType, string ItemNos = null, string OrderPlanQtyLot = null)
        {
            string FolderName = "";
            if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";

            string dock = GetData(ManifestNo);

            if (dock == "1")
            {
                string reportPath = @"\Report\DailyOrder\Rep_ManifestKanban_Sunter.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print_Sunter";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                parameter.Add("@ItemNo", System.Data.DbType.String, ItemNos);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);
                if (OrderPlanQtyLot != null) parameter.Add("@ORDER_PLAN_QTY_LOT", System.Data.DbType.String, OrderPlanQtyLot);
                if (OrderType == "5") parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                return SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
            else
            {
                string reportPath = @"\Report\DailyOrder\Rep_ManifestKanban_Download.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                parameter.Add("@ItemNo", System.Data.DbType.String, ItemNos);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);
                if (OrderPlanQtyLot != null) parameter.Add("@ORDER_PLAN_QTY_LOT", System.Data.DbType.String, OrderPlanQtyLot);
                if (OrderType == "5") parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                return SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
        }

        private void UpdatePrintFlag(string ManifestNos)
        {
            IDBContext db = DbContext;
            db.Execute("UpdateDailyOrderPrintFlag", new object[] { ManifestNos, AuthorizedUser.Username });
            db.Close();
        }

        public string PrintKanban(
                    string ManifestNo, string ItemNos, string OtherDocumentPrinter, 
                    string OrderDate, string SupplierCode, string SupplierPlant, 
                    string RcvPlantCode, string OrderType, string OrderPlanQtyLot)
        {
            List<PrintParameter> printParameters = new List<PrintParameter>();
            try
            {
                string UrlPrint = "";

                if (OrderPlanQtyLot != null) UrlPrint = PrintSelectionKanbanReport(ManifestNo, ManifestNo + "-" + ItemNos + "-" + OrderPlanQtyLot + "-", OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType, ItemNos, OrderPlanQtyLot);
                else UrlPrint = PrintSelectionKanbanReport(ManifestNo, ManifestNo + "-" + ItemNos + "-", OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType, ItemNos);

                printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return JSON.ToString<List<PrintParameter>>(printParameters);
        }
        #endregion

        #region DOWNLOAD PROCESS
        public void DownloadList(string Supplier,
                            string DockCode,
                            string OrderNo,
                            string PrintDateFrom,
                            string PrintDateTo,
                            string TimeFrom,
                            string TimeTo,
                            string tabActived,
                            string printFlag,
                            string subSupplier)
        {
            string SupplierCodeLDAP = "";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrinting", "OPHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            string DockCodeLDAP = "";
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }
            
            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }

            //string supplierCode = "";
            //string supplierPlantCode = "";
            //if (Supplier != "" && Supplier != null)
            //{
            //    string[] supplier = Supplier.Split('-');
            //    supplierCode = supplier[0].Replace(" ", "");
            //    supplierPlantCode = supplier[1].Replace(" ", "");
            //}

            string fileName = "DailyOrderPrintingSPEX.xls";

            IDBContext db = DbContext;
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;
            
            IEnumerable<ReportSPEXOrder> qry = db.Fetch<ReportSPEXOrder>("GetAllDailyOrderPrintingSPEX", new object[] {
                //supplierCode == null ? "" : supplierCode.Trim(),
                //supplierPlantCode == null ? "" : supplierPlantCode.Trim(),
                Supplier,
                DockCode == null ? "" : DockCode,
                OrderNo == null ? "" : OrderNo,
                (TimeFrom == "" || TimeFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PrintDateFrom + " " + TimeFrom + ":00","dd.MM.yyyy HH:mm",null),
                (TimeTo == "" || TimeTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PrintDateTo + " " + TimeTo + ":59","dd.MM.yyyy HH:mm",null),
                tabActived,
                SupplierCodeLDAP,
                printFlag,
                DockCodeLDAP,
                subSupplier
            });
            hasil = exporter.Write(qry, "OrderPrintingSPEXData");
            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion
    }
}
