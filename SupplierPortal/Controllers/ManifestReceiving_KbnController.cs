﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.ManifestReceiving_Kbn;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Transactions;
using Toyota.Common.Web.FTP;
using System.Diagnostics;

namespace Portal.Controllers
{
    [SkipUnlockController]
    public class ManifestReceiving_KbnController : BaseController
    {
        private ManifestReceiving_KbnModel mar = new ManifestReceiving_KbnModel();

        public ManifestReceiving_KbnController()
            : base("ManifestReceiving_Kbn", "Manifest Receiving Screen")
        {
            PageLayout.UseMessageBoard = true;
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            ManifestReceiving_KbnModel mdl = new ManifestReceiving_KbnModel();
            Model.AddModel(mdl);
            ViewData["MsgError"] = "";
        }

        public ActionResult ReloadGrid()
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ManifestReceiving_KbnModel model = Model.GetModel<ManifestReceiving_KbnModel>();
            try
            {
                model.ManifestReceiving_KbnDetails = db.Fetch<ManifestReceiving_KbnDetail>("GetManifestReceivingDetail_Kbn", new object[] { Request.Params["ManifestNo"] });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            db.Close();
            Model.AddModel(model);
            return PartialView("ManifestReceiving_KbnPartial", Model);
        }

        private string SetReceiveStatus(int value)
        {
            if (value % 2 == 0)
            { return "OK"; }
            else
            { return "NG"; }
        }

        public ActionResult GetManifestReceivingData(string ManifestNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            ManifestReceiving_Kbn QueryLog = db.SingleOrDefault<ManifestReceiving_Kbn>("GetManifestReceiving_Kbn", new object[] { ManifestNo });
            db.Close();
            return PartialView("ManifestHeaderDetail", QueryLog);
        }
        public ActionResult UpdatePopUp(string MsgError)
        {
            ViewData["MsgError"] = MsgError;

            return PartialView("PopUp");
        }
        public ContentResult UpdateManifestReceivingData(string ManifestNo, string ItemNos, string PartNos, string ShortageQtys, string MissPartQtys, string DamageQtys, string OrderQtys)
        {
            Stopwatch sw = new Stopwatch();

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            char[] splitchar = { ';' };
            string[] ItemNo = ItemNos.Split(splitchar);
            string[] PartNo = PartNos.Split(splitchar);
            string[] ShortageQty = ShortageQtys.Split(splitchar);
            string[] MissPartQty = MissPartQtys.Split(splitchar);
            string[] DamageQty = DamageQtys.Split(splitchar);
            string[] OrderQty = OrderQtys.Split(splitchar);
            string Result = "";

            try
            {
                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {
                    sw.Start();
                    bool isProblem = false;
                    int NGQty = 0;
                    for (int i = 0; i < ItemNo.Length; i++)
                    {
                        if (
                                (ShortageQty[i] != "0" && ShortageQty[i] != null && ShortageQty[i] != "") ||
                                (MissPartQty[i] != "0" && MissPartQty[i] != null && MissPartQty[i] != "") ||
                                (DamageQty[i] != "0" && DamageQty[i] != null && DamageQty[i] != "")
                            )
                        {
                            isProblem = true;
                        }
                        db.Execute("UpdateDOPart_Kbn", new object[] { ManifestNo, PartNo[i], AuthorizedUser.Username, ShortageQty[i], MissPartQty[i], DamageQty[i], ItemNo[i] });                            
                    }
                    
                    int hasNoProblem = db.Execute("UpdateMRFManifest_Kbn", new object[] { ManifestNo, AuthorizedUser.Username, Convert.ToInt16(isProblem) });                    
                    

                    //Update ERROR_FLAG TB_R_DO_Manifest
                    int isSolved = db.Execute("GetIsSolvedManifestReceiving_Kbn", new object[] { ManifestNo, AuthorizedUser.Username });
                    
                    //if(isSolved==0)
                    //    db.Execute("UpdateErrorFlagManifest", new object[] { ManifestNo, AuthorizedUser.Username });

                    db.Close();
                    scope.Complete();
                    Result = "ManifestNo " + ManifestNo + ": Received Succesfully";

                    sw.Stop();
                }
            }
            catch (Exception ex)
            {
                Result = "ManifestNo " + ManifestNo + ": Error updating data (" + ex.Message + ")";
            }
            db.Close();
            

            TimeSpan td = sw.Elapsed;

            string hasil = string.Format("{0}, {1}, {2}, {3}", td.Hours, td.Minutes, td.Seconds, td.Milliseconds);
            return Content(Result);
        }
        public string CheckManifestData(string txtManifestNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string ResultQuery = db.ExecuteScalar<string>("CheckManifestData_Kbn", new object[] { txtManifestNo });

            db.Close();
            return (ResultQuery == null ? " " : ResultQuery);
        }

        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_close_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_open_icon.png";
            string imageBytes4 = "~/Content/Images/notice_add_icon.png";
            //string imageBytes2 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkJJREFUeNpkUk1rFEEQfdXT85XsGkPcHMSD6Aa8evCoSO4BT+JJ8A8oguLBHPwDIip4EPwDCmpAES8eDILKJsegHrwEURKy2Y/JZmdnusvq3llItKAoerqr3nv1hlqtFlwYpIiwNTOLL5eP8relmv11JuBRHRxlAH2H6r8B7T6X2gaGADOo1VqTxgRz+LB0glbux1wuoFQyjf0DHyRnIkBlP6F/3wKyV5ArVaKOeaxcOU3PXsc8tYA8APIeUOxKdoDSZVeGDaSmp1DMvwSrq2AD6qydb9a5va64WUcuDaNtwBqBs3DTPSIkSQNBIpkC4fYA4eY5PUOrN2Av1lHI5OEfQcjloTSiouwK05g6FzLT3dWmEIY3Ndvji2TiqnFfHpRVX9U8CX+0YxZ2WljXLmhiNFDK9uxedclV0v8DnBwyvhLbWc0oumR6DY9qnQV2TBGV1gms27yTYx19LV8Gfb2DxU/H7GoTlsd67ci7Xnl0AFaaVCRVFkdDWfjgqwL1HkH3C79NEptEh6PFPkW/S2+q8zqUKqhhR76WD9Uc3q1zNLiDSFBVOEY4GP4HcYNlqVoyyVCGvWWx+bP2q1HmAdJ2Dq7dg40aTjd5jcF4oKOrFTjOemWcLYvyx06Rpo7QSIREWD5RSf9kUBy5DZ6WBtEdOCLUhR5u2ih/XyrzVEb+mJDStK9gFGGo+Gxa0DVvR2hQxvmGCsx1xbwhy92y5P/2Q6EFPhQGl9Kc7uqR2jHJ3sdCm7cmwIuE0T9k+z/xV4ABAP9iEDZwmfEuAAAAAElFTkSuQmCCAA==";

            if (val % 4 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 4 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 4 == 2)
            {
                img = imageBytes3;
            }
            else if (val % 4 == 3)
            {
                img = imageBytes4;
            }
            return img;
        }

        public ActionResult PartialHeaderManifest()
        {
            return PartialView("ManifestHeader", Model);
        }

        public ActionResult PartialManifestHeaderDetail()
        {
            ManifestReceiving_Kbn ManifestScreenDetail = new ManifestReceiving_Kbn();

            return PartialView("ManifestHeaderDetail", ManifestScreenDetail);
        }

        public void DownloadTest()
        {
            FTPUpload vFtp = new FTPUpload();

            string msg = "";

            byte[] test = vFtp.FtpDownloadByte(@"ftp://10.16.20.73/Portal/DataExchange/10052504SupplierFeedbackPartTentative20130500031.xls", ref msg);

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;


            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                            "test.xls"));

            Response.BinaryWrite(test);
            Response.End();
            
        }
    }
}
