﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Xml;
using System.Data;
using System.Transactions;
using System.IO;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;
using Portal.Models.PO;
using Portal.Models.LPOPConfirmation;
using Portal.Models.ConsolidatedLPOP;
using Portal.Models.MasterDataCompletenessCheck;
using Portal.Models.Supplier;
using Portal.Models.Forecast;
using Portal.Models.SupplierFeedbackSummary;
using Portal.Models.Globals;
using Portal.ExcelUpload;
using Portal.Models;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Util.Configuration;
using System.Diagnostics;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Log;
using System.Data.OleDb;
using System.Data.SqlClient;
using Toyota.Common.Web.Service;
using Toyota.Common.Web.Util.Converter;
using Toyota.Common.Web.Workflow;
using Toyota.Common.Util.Text;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Task;
using System.Configuration;
using Microsoft.Win32.TaskScheduler;

namespace Portal.Controllers
{
    public class LPOPConfirmationController : BaseController
    {
        public LPOPConfirmationController()
            : base("LPOP Confirmation")
        {
        }

        protected override void StartUp()
        {
        }

        protected override void Init()
        {           
            string ThisMonth = DateTime.ParseExact(DateTime.Now.ToString("MMMM yyyy"), "MMMM yyyy", new System.Globalization.CultureInfo("en-US")).ToString("MMMM yyyy");
            string NextMonth = DateTime.ParseExact(DateTime.Now.ToString("MMMM yyyy"), "MMMM yyyy", new System.Globalization.CultureInfo("en-US")).AddMonths(1).ToString("MMMM yyyy");
            ViewData["pm_consolidation"] = ThisMonth;
            ViewData["thisMonth"] = ThisMonth;
            ViewData["nextMonth"] = NextMonth;


            LPOPConfirmationModel modelLPOP = new LPOPConfirmationModel();
            modelLPOP.LPOPConsolidationConfirmations = LisiLPOPConfirmation();
            Model.AddModel(modelLPOP);

            ForecastFeedbackSummaryList model1 = new ForecastFeedbackSummaryList();
            Model.AddModel(model1);
            PageLayout.UseSlidingBottomPane = true;

            ConsolidatedLPOPModel mdl1 = new ConsolidatedLPOPModel();
            MasterDataCompletenessCheckModel model = new MasterDataCompletenessCheckModel();
            Model.AddModel(mdl1);
            Model.AddModel(model);

            List<Supplier> listSupplier = new List<Supplier>();

            for (int i = 1; i <= 100; i++)
            {
                listSupplier.Add(new Supplier
                {
                    SupplierCode = "5000" + i.ToString("0##"),
                    SupplierName = "Supplier " + i
                });
            }

            ViewData["SupplierData"] = listSupplier;

        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        private string year = "";
        private string month = "";
        private int monthVal = 0;
        private string MonthNum = "";

        public string getMonth(string month)
        {
            string result = string.Empty;
            char[] splitChar = { '-' };
            string[] splitMonth = month.Split(splitChar);
            result = GetProductionMonth(splitMonth[0].Trim() + " " + splitMonth[1].Trim());
            return result;
        }

        public string GetProductionMonth(string productionMonth)
        {
            string paramStr = (productionMonth);
            int paramStrLength = (paramStr).Length;

            string year = productionMonth.Substring(paramStrLength - 4, 4);
            string month = (paramStr).Substring(0, paramStrLength - 5);

            int monthVal = DateTime.ParseExact(month, "MMMM", new System.Globalization.CultureInfo("en-US")).Month;
            string MonthNum = monthVal.ToString().PadLeft(2, '0');

            return year + MonthNum;
        }

        public List<LPOPTabConfirmation> LisiLPOPConfirmation()
        {
            IDBContext db = DbContext;
            List<LPOPTabConfirmation> ConfirmationList = new List<LPOPTabConfirmation>();
            try
            {
                ConfirmationList = db.Fetch<LPOPTabConfirmation>("GetLPOPConfirmation", new object[] { "", "" });
            }
            catch (Exception exc) { }
            finally
            {
                db.Close();
            }
            return ConfirmationList;
        }

        List<LPOPConfirmationDetail> ListPopupDetailLPOPConsolidation_ = null;
        private List<LPOPConfirmationDetail> ListPopupDetailLPOPConsolidation
        {
            get
            {
                if (ListPopupDetailLPOPConsolidation_ == null)
                    ListPopupDetailLPOPConsolidation_ = new List<LPOPConfirmationDetail>();

                return ListPopupDetailLPOPConsolidation_;
            }
            set
            {
                ListPopupDetailLPOPConsolidation_ = value;
            }
        }

        private List<LPOPConfirmationDetail> GetAllLPOPConsolidateDetail(String productionMonth, String sourceData, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConfirmationDetail> LPOPConsolidateDetailModelList = new List<LPOPConfirmationDetail>();
            IDBContext db = DbContext;
            try
            {
                LPOPConsolidateDetailModelList = db.Fetch<LPOPConfirmationDetail>("GetLPOPConsolidationDetail", new object[] { StrProductionMonth, sourceData, strTiming });
            }
            catch (Exception exc) { }
            finally
            {
                db.Close();
            }
            return LPOPConsolidateDetailModelList;
        }

        public ActionResult HeaderTabLPOPConfirmation()
        {
            return PartialView("HeaderTabLPOPConfirmation", Model);
        }

        public ActionResult ReloadGridLPOPTabConfirmation()
        {
            string Timing = Request.Params["TimingConsolidation"];
            string ProductionMonth = Request.Params["PMConfirmation"];
            if (ProductionMonth.Length > 0)
            {
                ProductionMonth = GetProductionMonth(ProductionMonth);
            }

            IDBContext db = DbContext;
            LPOPConfirmationModel model = Model.GetModel<LPOPConfirmationModel>();
            model.LPOPConsolidationConfirmations = db.Query<LPOPTabConfirmation>("GetLPOPConfirmationSPRINTS", new object[] { ProductionMonth, Timing }).ToList<LPOPTabConfirmation>();
            Model.AddModel(model);
            db.Close();
            return PartialView("PartialGridTabLPOPConfirmation", Model);
        }

        #region DETAIL POPUP CONFIRMATION DETAIL

        List<LPOPConfirmationDetail> ListPopupDetailLPOPConfirmation_ = null;
        private List<LPOPConfirmationDetail> ListPopupDetailLPOPConfirmation
        {
            get
            {
                if (ListPopupDetailLPOPConfirmation_ == null)
                    ListPopupDetailLPOPConfirmation_ = new List<LPOPConfirmationDetail>();

                return ListPopupDetailLPOPConfirmation_;
            }
            set
            {
                ListPopupDetailLPOPConfirmation_ = value;
            }
        }
        private List<LPOPConfirmationDetail> GetPopupLPOPConfirmationDetail(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConfirmationDetail> LPOPConfirmationDetailList = new List<LPOPConfirmationDetail>();
            IDBContext db = DbContext;
            LPOPConfirmationDetailList = db.Fetch<LPOPConfirmationDetail>("GetLPOPConfirmationUnionSPRINTS", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConfirmationDetailList;
        }

        public ActionResult PopupLPOPConfirmationDetail()
        {
            ListPopupDetailLPOPConfirmation = GetPopupLPOPConfirmationDetail(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConfirmation);

            return PartialView("PopupLPOPConfirmationDetail", Model);
        }

        #endregion

        #region DETAIL POPUP ABNORMALITY

        List<LPOPConfirmationAbnormalityDetail> ListPopupDetailLPOPConfirmationAbnormality_ = null;
        private List<LPOPConfirmationAbnormalityDetail> ListPopupDetailLPOPConfirmationAbnormality
        {
            get
            {
                if (ListPopupDetailLPOPConfirmationAbnormality_ == null)
                    ListPopupDetailLPOPConfirmationAbnormality_ = new List<LPOPConfirmationAbnormalityDetail>();

                return ListPopupDetailLPOPConfirmationAbnormality_;
            }
            set
            {
                ListPopupDetailLPOPConfirmationAbnormality_ = value;
            }
        }
        private List<LPOPConfirmationAbnormalityDetail> GetPopupLPOPConfirmationDetail_Abnormality(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConfirmationAbnormalityDetail> LPOPConfirmationDetailAbnormalityList = new List<LPOPConfirmationAbnormalityDetail>();
            IDBContext db = DbContext;
            LPOPConfirmationDetailAbnormalityList = db.Fetch<LPOPConfirmationAbnormalityDetail>("GetLPOPConfirmation_AbnormalitySPRINTS", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConfirmationDetailAbnormalityList;
        }
        public ActionResult PopupLPOPConfirmationAbnormality()
        {
            ListPopupDetailLPOPConfirmationAbnormality = GetPopupLPOPConfirmationDetail_Abnormality(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConfirmationAbnormality);

            return PartialView("PopupLPOPConfirmationAbnormality", Model);
        }

        #endregion

        #region DELETE ON TAB CONFIRMATION HEADER
        public ContentResult DeleteLPOPConfirmation(string GridId)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string ProductionMonth = "";
            string Timing = "";

            IDBContext db = DbContext;
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                char[] SplitChar = { ';' };
                string[] ParamUpdate = GridId.Split(SplitChar);

                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    //SPLIT ID BY OBJECT WITH DELIMETER "_"
                    char[] SplitId = { '_' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);
                    ProductionMonth = ParamId[0];
                    Timing = ParamId[1].ToString().Substring(0, 1);

                    //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
                    year = ProductionMonth.Substring(ProductionMonth.Length - 4, 4);
                    month = (ProductionMonth).Substring(0, ProductionMonth.Length - 5);
                    monthVal = DateTime.ParseExact(month, "MMMM", new System.Globalization.CultureInfo("en-US")).Month;
                    MonthNum = monthVal.ToString().PadLeft(2, '0');

                    //UPDATE DATA
                    //Result += ParamUpdate[i] + ": ";
                    db.ExecuteScalar<string>("DeleteLPOPConfirmation", new object[] { year + MonthNum, Timing });
                    //Result += "\n";
                }
                return Content("Succes To Deleted Data");
            }
            catch (Exception ex)
            {
                string msg = "An Error happened while delete operation.";
                ex.PutLog(msg, AuthorizedUser.Username, "DeleteLPOPConfirmation", 0, "MSTD0001INF", "INF", "0", "11005", 1);
                return Content(ex.Message);
            }
            finally { db.Close(); }
        }
        #endregion

        #region CREATE PO ON TAB CONFIRMATION HEADER TO ICS
        public ContentResult CreatePO(string GridId)
        {
            string ProductionMonth = "";
            string Timing = "";
            string resultMessage = string.Empty;
            string module = "1";
            string function = "11004";
            String processID = "0";
            const string msg1 = "PO Creation {0} with Process ID {1}"; 
            IDictionary<string, object> parameters = new Dictionary<string, object>();
            parameters["GridID"] = GridId;
            parameters["User"] = SessionState.GetAuthorizedUser();

            IDBContext db = DbContext;

            char[] SplitChar = { ';' };
            string[] ParamUpdate = GridId.Split(SplitChar);
            int tempList = 0;

            for (int i = 0; i < ParamUpdate.Length; i++)
            {
                char[] SplitId = { '_' };
                string[] ParamId = ParamUpdate[i].Split(SplitId);

                ProductionMonth = ParamId[0];
                Timing = ParamId[1].ToString().Substring(0, 1);

                tempList = tempList + db.ExecuteScalar<int>("CheckCreatePOHasBeenCreatedSPRINTS", new object[] { GetProductionMonth(ProductionMonth) });

                //if (Timing == "F")
                //{
                //db.Execute("InsertLPOPConfirmation_Sum", new string[] { GetProductionMonth(ProductionMonth), Timing, AuthorizedUser.Username });
                //db.ExecuteScalar<string>("InsertLPOPConfirmation_CreatePO", new string[] { AuthorizedUser.Username, GetProductionMonth(ProductionMonth), Timing, AuthorizedUser.Username });
                //db.Execute("InsertSupplierFeedbackFromLPOP", new object[] { "FCST", Timing, GetProductionMonth(ProductionMonth) });
                //}
            }

            if (ParamUpdate.Length == tempList)
            {
                resultMessage = "PO has been created";
            }
            try
            {
                //BackgroundTaskService service = BackgroundTaskService.GetInstance();

                //BackgroundTaskParameter taskParameter = new BackgroundTaskParameter();
                //taskParameter.Add("GridId", GridId);
                //taskParameter.Add("Username", AuthorizedUser.Username);

                //BackgroundTask immediateTask = new ExternalBackgroundTask()
                //{
                //    Id = "CPOPTR_ID",
                //    Name = "CreatePOPostingTaskRuntime",
                //    Description = "Background Task Posting Create PO",
                //    FunctionName = "CPOPTR_BT",
                //    Submitter = new Toyota.Common.Credential.User() { Username = AuthorizedUser.Username },
                //    Type = TaskType.Immediate,
                //    Range = new TaskRange()
                //    {
                //        Time = new TimeSpan(1, 54, 0)
                //    },
                //    //Parameters = new string[] { JSON.ToString<List<IDictionary<string, string>>>(dataList), process, AuthorizedUser.Username },
                //    Parameters = taskParameter,
                //    Command = service.GetTaskPath("CreatePOPostingTaskRuntime")
                //};

                //service.Register(immediateTask);
                string message = "Start Process Create PO from IPPCS";
                string location = "LPOP Create PO";

                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            AuthorizedUser.Username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                string ret= db.ExecuteScalar<string>("PutPCSLocalOrderSPRINTS", new object[] { GetProductionMonth(ProductionMonth), Timing, processID, AuthorizedUser.Username });

                resultMessage = string.Format(msg1, "Started", processID);
            }
            catch (Exception ex)
            {
                ex.PutLog(ex.GetType().Name, AuthorizedUser.Username, "LPOPConfirmation.CreatePO", Convert.ToInt64(processID), "MSTD0001INF", "INF", null, null, 1);
                resultMessage = string.Format(msg1, "ERROR", processID);
            }
            return Content(resultMessage);
        }
        # endregion

        #region DOWNLOAD TEMPLATE (SCREEN HIDE BY USER)
        public void DownloadTemplate()
        {
            String connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            //settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\Forecast\ForecastDetailData.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;

            sqlDataSource.SelectCommand = @"select * from TB_R_LPOP where 1=0";

            ReportProcessor reportProcessor = new ReportProcessor();

            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "LPOPConsolidationTemplate" + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }
        #endregion

        #region GridConfirmationDownloadPOXLS
        public ContentResult GridConfirmationDownloadPOXLS(string ProductionMonth)
        {

            string YearNow = DateTime.Now.ToString("yyyy");
            year = YearNow.Substring(0, 2) + ProductionMonth.Substring(ProductionMonth.Length - 2, 2);
            month = (ProductionMonth).Substring(0, ProductionMonth.Length - 3);
            monthVal = DateTime.ParseExact(month, "MMM", new System.Globalization.CultureInfo("en-US")).Month;
            MonthNum = monthVal.ToString().PadLeft(2, '0');

            string valProdMonth = year + MonthNum;
            string valProdmonthTitle = year + "-" + MonthNum;

            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PO\DownloadListPO.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlDataSource.SelectCommand = "SELECT row_number() OVER (ORDER BY SUPPLIER_NAME) AS SeqNbr, * FROM ( " +
                                          "     SELECT DISTINCT POH.SUPPLIER_CD AS SUPPLIER_CD, CASE WHEN CHARINDEX('(', S.SUPPLIER_NAME) > 0 THEN SUBSTRING(S.SUPPLIER_NAME, 0, CHARINDEX('(', S.SUPPLIER_NAME)) ELSE S.SUPPLIER_NAME END AS SUPPLIER_NAME, POH.PO_NO AS PO_NO, " +
                                          "                     CAST(YEAR(POH.CREATED_DT) AS VARCHAR) + '.' + CASE WHEN MONTH(POH.CREATED_DT) < 10 THEN  '0'+CAST(MONTH(POH.CREATED_DT) AS VARCHAR) ELSE CAST(MONTH(POH.CREATED_DT) AS VARCHAR) END + " +
                                          "                     '.' + CASE WHEN DAY(POH.CREATED_DT) < 10 THEN  '0'+CAST(DAY(POH.CREATED_DT) AS VARCHAR) ELSE CAST(DAY(POH.CREATED_DT) AS VARCHAR) END + ' ' + " +
                                          "                     SUBSTRING(CAST(CONVERT(TIME, POH.CREATED_DT) AS VARCHAR), 0, 6) AS PO_DATE, " +
                                          "                     UPPER(DATENAME(MONTH, '" + valProdmonthTitle + "-01')) + ' " + year + "' AS PRODUCTION_MONTH " +
                                          "     FROM TB_R_PO_H POH " +
                                          "         LEFT JOIN TB_M_SUPPLIER S ON S.SUPPLIER_CODE = POH.SUPPLIER_CD " +
                                          "     WHERE POH.PRODUCTION_MONTH = '" + valProdMonth + "' AND POH.PO_NO IS NOT NULL " +
                                          ") tbl";

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "PO_List_" + ProductionMonth + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
            rpt.Dispose();
            return Content("Succes");
        }
        #endregion

        #region DOWNLOAD XLS ON GRID CONFIRMATION
        public void GridConfirmationDownloadXLS(string ProductionMonth, string Timing)
        {
            //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
            string YearNow = DateTime.Now.ToString("yyyy");
            year = YearNow.Substring(0, 2) + ProductionMonth.Substring(ProductionMonth.Length - 2, 2);
            month = (ProductionMonth).Substring(0, ProductionMonth.Length - 3);
            monthVal = DateTime.ParseExact(month, "MMM", new System.Globalization.CultureInfo("en-US")).Month;
            MonthNum = monthVal.ToString().PadLeft(2, '0');

            string valProdMonth = year + MonthNum;
            string valTiming = Timing.Substring(0, 1);

            string fileName = "LPOPConfirmation" + Timing + valProdMonth + ".xls";

            IDBContext db = DbContext;
            IEnumerable<LPOPConfirmationReportUnion> qry = db.Query<LPOPConfirmationReportUnion>("RepLPOPConfirmationUnionSPRINTS", new object[] { valProdMonth, valTiming });

            IExcelWriter exporter = ExcelWriter.GetInstance();

            List<string> footerText = new List<string>();
            footerText.Add("NOTE :");
            footerText.Add("1. Fill Action column with value (Blank/0 : Ignored; 1 : Insert, 2 : Edit, 3 : Delete");

            byte[] hasil = exporter.Write(qry, "LPOPConfirmationData", footerText.ToArray());
            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        #region APPROVE DATA ON TAB CONFIRMATION HEADER
        public ContentResult ApproveLPOPConfirmation(string GridId)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string ProductionMonth = "";
            string Timing = "";
            string ApproveBy = "";
            string Result = "";

            //SPLIT ID WITH DELIMETER ";"
            char[] SplitChar = { ';' };
            string[] ParamUpdate = GridId.Split(SplitChar);

            //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
            IDBContext db = DbContext;
            for (int i = 0; i < ParamUpdate.Length; i++)
            {
                try
                {
                    //SPLIT ID BY OBJECT WITH DELIMETER "_"
                    char[] SplitId = { '_' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);
                    ProductionMonth = ParamId[0];
                    Timing = ParamId[1].ToString().Substring(0, 1);
                    ApproveBy = AuthorizedUser.Username; //ParamId[2];

                    TimeSpan ts = new TimeSpan(0, 30, 0);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                    {
                        //---=== Insert to LPOP confirmation summary for tentative timing
                        //if (Timing == "T")
                        //{
                        db.Execute("InsertLPOPConfirmation_Sum", new string[] { GetProductionMonth(ProductionMonth), Timing, AuthorizedUser.Username });
                        db.ExecuteScalar<string>("InsertLPOPConfirmation_CreatePO", new string[] { AuthorizedUser.Username, GetProductionMonth(ProductionMonth), Timing, ApproveBy });
                        db.Execute("InsertSupplierFeedbackFromLPOP", new object[] { "FCST", Timing, GetProductionMonth(ProductionMonth) });
                        //}
                        //if (Timing == "F")
                        //{
                        //    db.Execute("InsertLPOPConfirmation_Sum", new string[] { GetProductionMonth(ProductionMonth), Timing, AuthorizedUser.Username });
                        //    db.ExecuteScalar<string>("InsertLPOPConfirmation_CreatePO", new string[] { AuthorizedUser.Username, GetProductionMonth(ProductionMonth), Timing, AuthorizedUser.Username });
                        //    db.Execute("InsertSupplierFeedbackFromLPOP", new object[] { "FCST", Timing, GetProductionMonth(ProductionMonth) });
                        //}
                        //DOWNLOAD DATA
                        db.ExecuteScalar<string>("UpdateLPOPConfirmation_Approve", new string[] { AuthorizedUser.Username, GetProductionMonth(ProductionMonth), Timing });
                        scope.Complete();

                        //Approval Notification Email to Supplier  (Mini)
                        string prodMonth = GetProductionMonth(ProductionMonth);
                        //ProcessStartInfo startInfo = new ProcessStartInfo(ConfigurationManager.AppSettings["SUPP_INFO"]);
                        //startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                        //startInfo.Arguments = prodMonth;
                        //Process.Start(startInfo);

                        db.Execute("sendEmailLPOPApprove", new string[] { prodMonth });


                        Result += "Production Month " + ProductionMonth + " and Timing " + Timing + ": success\n";

                    }
                }
                catch (Exception ex)
                {
                    string msg = "Error while Approve Production Month " + ProductionMonth + " and Timing " + Timing; 

                    Result += msg;
                    ex.PutLog(msg, AuthorizedUser.Username, "ApproveLPOPConfirmation", 0, "MSTD0001INF", "INF", "0", "11005", 1);                    
                }
            }
            db.Close();
            return Content(Result);
        }
        #endregion

        #region UPDATE TIME

        public ContentResult UpdateDataGrid(string feedbackDueDate, string lastRemindingDate, string prodMonth, string timing)
        {
            string resultMessage = string.Empty;
            string module = "1";
            string function = "11005";
            string processID = "0";

            prodMonth = GetProductionMonth(prodMonth);
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            //string taskID = "";

            //string format = "dd.MM.yyyy HH:mm";
            //DateTime dtstartDate = DateTime.ParseExact(feedbackDueDate + " " + ConfigurationManager.AppSettings["TIME_EXECUTE"].ToString(), format, System.Globalization.CultureInfo.InvariantCulture);
            //DateTime dtendDate = DateTime.ParseExact(lastRemindingDate + " " + ConfigurationManager.AppSettings["TIME_EXECUTE"].ToString(), format, System.Globalization.CultureInfo.InvariantCulture);

            try
            {
                string message = "Start Process Register Job LPOP Confirmation";
                string location = "LPOP Confirmation";

                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            AuthorizedUser.Username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                db.Execute("UpdateRemindingTime", new object[] { prodMonth, ((timing == "Firm") ? "F" : "T"), formatingShortDate(feedbackDueDate), formatingShortDate(lastRemindingDate), processID });

                ////daftarkan dijobmonitoring
                //BackgroundTaskService service = BackgroundTaskService.GetInstance();
                //BackgroundTaskParameter taskParameter = new BackgroundTaskParameter();
                //taskParameter.Add("productionMonth", prodMonth);

                ////---=== Check & delete ID if Exists
                //string tParamTiming = "%timing&quot;:&quot;" + ((timing == "Firm")?"F":"T") + "&quot;%";
                //db.Execute("CheckLPOPConfirmationTask", new object[] { tParamTiming });

                //if (timing == "Firm")
                //{
                //    taskParameter.Add("timing", "F");
                //}
                //else
                //{
                //    taskParameter.Add("timing", "T");
                //}


                //BackgroundTask immediateTask = new ExternalBackgroundTask()
                //    {
                //        Id = "SFTTR_ID",
                //        Name = "SupplierFeedbackTentativeTaskRuntime",
                //        Description = "Background Task Supplier Feedback",
                //        FunctionName = "UpdateDataGrid",
                //        Submitter = new Toyota.Common.Credential.User() { Username = AuthorizedUser.Username },
                //        Type = TaskType.Periodic,

                //        Range = new DailyTaskRange()
                //        {
                //            Interval = 1,
                //            //Time = new TimeSpan(9, 15, 00)
                //            Time = new TimeSpan(Convert.ToInt16(ConfigurationManager.AppSettings["HOUR_JOB"]), Convert.ToInt16(ConfigurationManager.AppSettings["MINUTE_JOB"]), 00)
                //        },
                //        StartDate = dtstartDate,
                //        EndDate = dtendDate,
                //        Parameters = taskParameter,
                //        Command = "C:\\Background_Task\\Tasks\\SuppFeedbackTentativeTaskRuntime.exe"
                //    };
                //    service.Register(immediateTask);

                ////--------------

                return Content("Success");
            }
            catch (Exception ex)
            {
                //return Content("Error, failed update feedback due date !");
                string msg = "Error, failed update";

                ex.PutLog(msg, AuthorizedUser.Username, "UpdateDataGrid", 0, "MSTD0001INF", "INF", "0", "11005", 1); 
                return Content(msg);
            }
            finally
            {
                db.Close();
            }
        }

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                result = extractString[2] + "/" + extractString[1] + "/" + extractString[0];
            }

            return result;
        }

        #endregion

        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}
