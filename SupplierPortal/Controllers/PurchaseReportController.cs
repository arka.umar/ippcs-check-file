﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Portal.Models.PurchaseReport;
using Portal.Models.Globals;
using Portal.Models.InvoiceAndPaymentInquiry;
using System.Net;
using DevExpress.Web.ASPxUploadControl;
using NPOI.HSSF.UserModel;
//using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;


namespace Portal.Controllers
{
    public class PurchaseReportController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        public PurchaseReportController()
            : base("PurchaseReport")
        {
            PageLayout.UseMessageBoard = true;
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        #region Model properties
        List<SupplierICS> _invoiceInquirySupplierModel = null;
        private List<SupplierICS> _InvoiceInquirySupplierModel
        {
            get
            {
                if (_invoiceInquirySupplierModel == null)
                    _invoiceInquirySupplierModel = new List<SupplierICS>();

                return _invoiceInquirySupplierModel;
            }
            set
            {
                _invoiceInquirySupplierModel = value;
            }
        }

        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
       {
           PurchaseReportModel prm = new PurchaseReportModel();
           List<PurchaseReport> prs = new List<PurchaseReport>();
           List<PurchaseReportGoodReceipt> pgrs = new List<PurchaseReportGoodReceipt>();
           List<PurchaseReportInvoiceActual> prac = new List<PurchaseReportInvoiceActual>();
           List<PurchaseReportInvoiceOutstanding> prou = new List<PurchaseReportInvoiceOutstanding>();
           PageLayout.UseSlidingBottomPane = true;
           List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");
           ViewData["SupplierGridLookup"] = suppliers;
           ViewData["DateFrom"] = DateTime.Now.AddMonths(-1).ToString("dd.MM.yyyy");
           ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");
           prm.purchaseReports = prs;
           prm.purchaseReportGoodReceipts = pgrs;
           prm.purchaseReportInvoiceActuals = prac;
           prm.purchaseReportInvoiceOutstandings = prou;
           Model.AddModel(prm);
       }

        #region Database controller
        private List<SupplierICS> GetAllSupplierICS()
        {
            List<SupplierICS> lstSupplierICS = new List<SupplierICS>();
            IDBContext db = DbContext;
            lstSupplierICS = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });
            db.Close();

            return lstSupplierICS;
        }

        private List<PurchaseReport> GetAllPurchaseReports(string vendorCd, string vendorName, string periodFromDt, string periodToDt)
        {
            List<PurchaseReport> purchase_reports = new List<PurchaseReport>();
            IDBContext db = DbContext;

            purchase_reports = db.Fetch<PurchaseReport>("PurchaseReportInquiry", new object[] { vendorCd, vendorName, periodFromDt, periodToDt });
            db.Close();

            return purchase_reports;
        }
        #endregion

        public ActionResult PurchaseReportsInquiryPartial()
        {
            #region Required model in partial page : for InvoiceAndPaymentInquiryPartial
            PurchaseReportModel model = Model.GetModel<PurchaseReportModel>();
            string vendorCd = Request.Params["VendorCd"];
            string vendorName = Request.Params["VendorName"];
            string periodFrom = Request.Params["PeriodFrom"];
            string periodTo = Request.Params["PeriodTo"];
            string periodFromDt = string.Empty;
            string periodToDt = string.Empty;

            if (!string.IsNullOrEmpty(periodFrom))
            {
                string[] PeriodFromArr = periodFrom.Split('.');
                periodFromDt = PeriodFromArr[2] + "-" + PeriodFromArr[1] + "-" + PeriodFromArr[0];
            }
            else
            {
                periodFromDt = "9999-12-31";
            }

            if (!string.IsNullOrEmpty(periodTo))
            {
                string[] PeriodToArr = periodTo.Split('.');
                periodToDt = PeriodToArr[2] + "-" + PeriodToArr[1] + "-" + PeriodToArr[0];
            }
            else
            {
                periodToDt = "9999-12-31";
            }

            model.purchaseReports = GetAllPurchaseReports(vendorCd, vendorName, periodFromDt, periodToDt);
            #endregion

            return PartialView("PurchaseReportsInquiryPartial", Model);
        }

        public ActionResult HeaderPurchaseReportInquiry()
       {
           #region Required model in partial page : for OrderInquiryHeaderPartial
           List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");

           _InvoiceInquirySupplierModel = suppliers;

           Model.AddModel(_InvoiceInquirySupplierModel);
           #endregion

           return PartialView("HeaderPurchaseReport", Model);
       }

        public ActionResult GoodReceiptActualDetailGrid()
        {
            string vendorCd = Request.Params["VendorCode"];
            string vendorName = Request.Params["VendorName"];
            string periodFrom = Request.Params["PeriodFrom"];
            string periodTo = Request.Params["PeriodTo"];
            string periodFromDt = string.Empty;
            string periodToDt = string.Empty;

            if (!string.IsNullOrEmpty(periodFrom))
            {
                string[] PeriodFromArr = periodFrom.Split('.');
                periodFromDt = PeriodFromArr[2] + "-" + PeriodFromArr[1] + "-" + PeriodFromArr[0];
            }
            else
            {
                periodFromDt = "9999-12-31";
            }

            if (!string.IsNullOrEmpty(periodTo))
            {
                string[] PeriodToArr = periodTo.Split('.');
                periodToDt = PeriodToArr[2] + "-" + PeriodToArr[1] + "-" + PeriodToArr[0];
            }
            else
            {
                periodToDt = "9999-12-31";
            }

            ViewData["GoodReceiptActualDetail"] = GetGoodReceiptActualDetail(vendorCd, vendorName, periodFromDt, periodToDt);;

            return PartialView("GoodReceiptActualDetailGrid", ViewData["GoodReceiptActualDetail"]);
        }

        protected List<PurchaseReportGoodReceipt> GetGoodReceiptActualDetail(string vendorCd, string vendorName, string periodFromDt, string periodToDt)
        {
            List<PurchaseReportGoodReceipt> GRAList = new List<PurchaseReportGoodReceipt>();
            IDBContext db = DbContext;

            GRAList = db.Query<PurchaseReportGoodReceipt>("GetGoodReceiptActualDetail", new object[] { vendorCd, vendorName, periodFromDt, periodToDt })
                .ToList<PurchaseReportGoodReceipt>();

            db.Close();

            return GRAList;
        }

        public ActionResult InvoiceActualDetailGrid()
        {
            string vendorCd = Request.Params["VendorCode"];
            string vendorName = Request.Params["VendorName"];
            string periodFrom = Request.Params["PeriodFrom"];
            string periodTo = Request.Params["PeriodTo"];
            string periodFromDt = string.Empty;
            string periodToDt = string.Empty;

            if (!string.IsNullOrEmpty(periodFrom))
            {
                string[] PeriodFromArr = periodFrom.Split('.');
                periodFromDt = PeriodFromArr[2] + "-" + PeriodFromArr[1] + "-" + PeriodFromArr[0];
            }
            else
            {
                periodFromDt = "9999-12-31";
            }

            if (!string.IsNullOrEmpty(periodTo))
            {
                string[] PeriodToArr = periodTo.Split('.');
                periodToDt = PeriodToArr[2] + "-" + PeriodToArr[1] + "-" + PeriodToArr[0];
            }
            else
            {
                periodToDt = "9999-12-31";
            }

            ViewData["InvoiceActualDetail"] = GetInvoiceActualDetail(vendorCd, vendorName, periodFromDt, periodToDt); ;

            return PartialView("InvoiceActualDetailGrid", ViewData["InvoiceActualDetail"]);
        }
        
        protected List<PurchaseReportInvoiceActual> GetInvoiceActualDetail(string vendorCd, string vendorName, string periodFromDt, string periodToDt)
        {
            List<PurchaseReportInvoiceActual> GRAList = new List<PurchaseReportInvoiceActual>();
            IDBContext db = DbContext;

            GRAList = db.Query<PurchaseReportInvoiceActual>("GetInvoiceActualDetail", new object[] { vendorCd, vendorName, periodFromDt, periodToDt })
                .ToList<PurchaseReportInvoiceActual>();

            db.Close();

            return GRAList;
        }

        public ActionResult InvoiceOutstandingDetailGrid()
        {
            string vendorCd = Request.Params["VendorCode"];
            string vendorName = Request.Params["VendorName"];
            string periodFrom = Request.Params["PeriodFrom"];
            string periodTo = Request.Params["PeriodTo"];
            string periodFromDt = string.Empty;
            string periodToDt = string.Empty;

            if (!string.IsNullOrEmpty(periodFrom))
            {
                string[] PeriodFromArr = periodFrom.Split('.');
                periodFromDt = PeriodFromArr[2] + "-" + PeriodFromArr[1] + "-" + PeriodFromArr[0];
            }
            else
            {
                periodFromDt = "9999-12-31";
            }

            if (!string.IsNullOrEmpty(periodTo))
            {
                string[] PeriodToArr = periodTo.Split('.');
                periodToDt = PeriodToArr[2] + "-" + PeriodToArr[1] + "-" + PeriodToArr[0];
            }
            else
            {
                periodToDt = "9999-12-31";
            }

            ViewData["InvoiceOutstandingDetail"] = GetInvoiceOutstandingDetail(vendorCd, vendorName, periodFromDt, periodToDt); ;

            return PartialView("InvoiceOutstandingDetailGrid", ViewData["InvoiceOutstandingDetail"]);
        }

        protected List<PurchaseReportInvoiceOutstanding> GetInvoiceOutstandingDetail(string vendorCd, string vendorName, string periodFromDt, string periodToDt)
        {
            List<PurchaseReportInvoiceOutstanding> GRAList = new List<PurchaseReportInvoiceOutstanding>();
            IDBContext db = DbContext;

            GRAList = db.Query<PurchaseReportInvoiceOutstanding>("GetInvoiceOutstandingDetail", new object[] { vendorCd, vendorName, periodFromDt, periodToDt })
                .ToList<PurchaseReportInvoiceOutstanding>();

            db.Close();

            return GRAList;
        }

        public ActionResult PartialHeaderVendorLookupGrid()
        {
            #region Required model in partial page : for OIPSupplierOption GridLookup
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "PurchaseReport", "OIPVendorCode");

            TempData["GridName"] = "OIPVendorCode";
            _InvoiceInquirySupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _InvoiceInquirySupplierModel);
        }

        public ActionResult PartialHeaderVendorLookupGrid2()
        {
            #region Required model in partial page : for OIPSupplierOption GridLookup
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "PurchaseReport", "OIPVendorName");

            TempData["GridName"] = "OIPVendorName";
            _InvoiceInquirySupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _InvoiceInquirySupplierModel);
        }

        public JsonResult CheckBeforeDownload(string vendorCd, string vendorName, string periodFrom, string periodTo, string desc)
        {
            string result = string.Empty;
            string periodFromDt = string.Empty;
            string periodToDt = string.Empty;
            SendResultModel model = new SendResultModel();
            try
            {
                if (!string.IsNullOrEmpty(periodFrom))
                {
                    string[] PeriodFromArr = periodFrom.Split('.');
                    periodFromDt = PeriodFromArr[2] + "-" + PeriodFromArr[1] + "-" + PeriodFromArr[0];
                }
                else
                {
                    periodFromDt = "9999-12-31";
                }

                if (!string.IsNullOrEmpty(periodTo))
                {
                    string[] PeriodToArr = periodTo.Split('.');
                    periodToDt = PeriodToArr[2] + "-" + PeriodToArr[1] + "-" + PeriodToArr[0];
                }
                else
                {
                    periodToDt = "9999-12-31";
                }

                if (desc == "PurchaseReport")
                {
                    result = DbContext.ExecuteScalar<string>("CheckBeforeDownloadPurchaseReport", new object[] { vendorCd, vendorName, periodFromDt, periodToDt });
                }
                else if (desc == "GoodReceipt")
                {
                    result = DbContext.ExecuteScalar<string>("CheckBeforeDownloadPurchaseGRReport", new object[] { vendorCd, vendorName, periodFromDt, periodToDt });
                }
                else if (desc == "InvoicingActual")
                {
                    result = DbContext.ExecuteScalar<string>("CheckBeforeDownloadPurchaseInvoicingActualReport", new object[] { vendorCd, vendorName, periodFromDt, periodToDt });
                }
                else if (desc == "InvoicingOutstanding")
                {
                    result = DbContext.ExecuteScalar<string>("CheckBeforeDownloadPurchaseInvoicingOutstandingReport", new object[] { vendorCd, vendorName, periodFromDt, periodToDt });
                }

                model.Result = result;
                model.SupplierCode = vendorCd;
                model.SupplierName = vendorName;
                model.PeriodFrom = periodFromDt;
                model.PeriodTo = periodToDt;
                model.Desc = desc;
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally
            {
                DbContext.Close();
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region Download without Custom Excel
        //DONWLOAD WITHOUT CUSTM EXCEL
        //[HttpGet]
        //public void DownloadPurchaseReport(string vendorCd, string vendorName, string periodFrom, string periodTo)
        //{
        //    string fileName = "";
        //    byte[] resultExport = null;
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //
        //    IExcelWriter exporter = ExcelWriter.GetInstance();
        //    
        //    fileName = "PurchaseReport.xls";
        //    IEnumerable<PurchaseReportDownload> listPurchaseReport = db.Query<PurchaseReportDownload>("PurchaseReportInquiryDownload", new object[] { vendorCd, vendorName, periodFrom, periodTo });
        //    exporter.Append(listPurchaseReport, "PurchaseReport");
        //
        //    resultExport = exporter.Flush();
        //
        //    Response.Clear();
        //    Response.Cache.SetCacheability(HttpCacheability.Private);
        //    Response.Expires = -1;
        //    Response.Buffer = true;
        //
        //    Response.ContentType = "application/octet-stream";
        //    Response.AddHeader("Content-Length", Convert.ToString(resultExport.Length));
        //    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
        //    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
        //
        //    Response.BinaryWrite(resultExport);
        //    Response.End();
        //}
        #endregion

        //CREATE BY AGUNGPANDUAN.COM
        #region Configuration Download NPOI
        protected void SendDataAsAttachment(string fileName, byte[] data)
        {
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        public static ICellStyle createCellStyleDataLeft(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.LEFT;

            return headerStyle;
        }

        public static ICellStyle createCellStyleDataRight(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.RIGHT;
            return headerStyle;
        }

        public static ICellStyle createCellStyleDataCenter(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.CENTER;

            return headerStyle;
        }

        public static ICellStyle createCellStyleDataDouble(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            IDataFormat dataFormatCustom = wb.CreateDataFormat();
            headerStyle.DataFormat = dataFormatCustom.GetFormat("#,##0");
            return headerStyle;
        }

        public static ICellStyle createCellStyleColumnHeader(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            headerStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;
            headerStyle.FillPattern = FillPatternType.THIN_HORZ_BANDS;
            headerStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;

            headerStyle.WrapText = true;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.CENTER;

            IFont headerLabelFont = wb.CreateFont();
            headerLabelFont.Color = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            headerLabelFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(headerLabelFont);
            return headerStyle;
        }

        public static ICellStyle createCellStyleColumnTitle(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            //headerStyle.BorderBottom = BorderStyle.THIN;
            //headerStyle.BorderLeft = BorderStyle.THIN;
            //headerStyle.BorderRight = BorderStyle.THIN;
            //headerStyle.BorderTop = BorderStyle.THIN;
            

            //headerStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;
            headerStyle.FillPattern = FillPatternType.NO_FILL;
            //headerStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;

            headerStyle.WrapText = false;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.LEFT;

            IFont headerLabelFont = wb.CreateFont();
            headerLabelFont.FontName = HSSFFont.FONT_ARIAL;
            headerLabelFont.FontHeightInPoints = 13;
            headerLabelFont.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            headerLabelFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(headerLabelFont);
            return headerStyle;
        }

        public static ICellStyle createCellStyleColumnTitleSecond(HSSFWorkbook wb)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            //headerStyle.BorderBottom = BorderStyle.THIN;
            //headerStyle.BorderLeft = BorderStyle.THIN;
            //headerStyle.BorderRight = BorderStyle.THIN;
            //headerStyle.BorderTop = BorderStyle.THIN;


            //headerStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;
            headerStyle.FillPattern = FillPatternType.NO_FILL;
            //headerStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_BLUE.index;

            headerStyle.WrapText = false;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.LEFT;

            IFont headerLabelFont = wb.CreateFont();
            headerLabelFont.FontName = HSSFFont.FONT_ARIAL;
            headerLabelFont.FontHeightInPoints = 10;
            headerLabelFont.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            headerLabelFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(headerLabelFont);
            return headerStyle;
        }

        public static ICellStyle createCellStyleDataDate(HSSFWorkbook wb, short dataFormat)
        {
            ICellStyle headerStyle = wb.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;
            headerStyle.DataFormat = dataFormat;

            return headerStyle;
        }

        private const int MaxNumberOfRowPerSheet = 65500;
        private const int MaximumSheetNameLength = 25;
        public static string EscapeSheetName(string sheetName)
        {
            string escapedSheetName = sheetName
                                        .Replace("/", "-")
                                        .Replace("\\", " ")
                                        .Replace("?", string.Empty)
                                        .Replace("*", string.Empty)
                                        .Replace("[", string.Empty)
                                        .Replace("]", string.Empty)
                                        .Replace(":", string.Empty);

            if (escapedSheetName.Length > MaximumSheetNameLength)
                escapedSheetName = escapedSheetName.Substring(0, MaximumSheetNameLength);

            return escapedSheetName;
        }

        public static void createCellText(
            IRow row,
            ICellStyle cellStyle,
            int colIdx, string txt)
        {
            ICell cell = row.CreateCell(colIdx);

            if (txt != null)
            {
                cell.SetCellValue(txt);
                cell.SetCellType(CellType.STRING);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;
            }
        }

        public static void createCellDecimal(
            IRow row,
            ICellStyle cellStyle,
            int colIdx, decimal? val)
        {
            ICell cell = row.CreateCell(colIdx);
            if (val != null)
            {
                cell.SetCellValue((double)val);
                cell.SetCellType(CellType.NUMERIC);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;
            }
        }

        public static void createCellDouble(HSSFWorkbook wb,
            IRow row,
            ICellStyle cellStyle,
            int colIdx, double? val)
        {
            ICell cell = row.CreateCell(colIdx);

            if (val != null)
            {
                cell.SetCellValue((double)val);
                cell.SetCellType(CellType.NUMERIC);
            }
            else
            {
                cell.SetCellType(CellType.BLANK);
            }

            if (cellStyle != null)
            {
                cell.CellStyle = cellStyle;
                //IDataFormat dataFormatCustom = wb.CreateDataFormat();
                //cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("#,##0.00");
                //
                //cell.CellStyle.BorderBottom = BorderStyle.THIN;
                //cell.CellStyle.BorderLeft = BorderStyle.THIN;
                //cell.CellStyle.BorderRight = BorderStyle.THIN;
                //cell.CellStyle.BorderTop = BorderStyle.THIN;
            }
        }

        public static void CreateSingleColHeader(HSSFWorkbook wb, ISheet sheet, int rows, int col, ICellStyle colStyleHeader, string cellValue)
        {
            IRow row = sheet.GetRow(rows) ?? sheet.CreateRow(rows);

            ICell cell = row.CreateCell(col);
            cell.CellStyle = colStyleHeader;
            cell.SetCellValue(cellValue);
        }

        #endregion

        //CREATE BY AGUNGPANDUAN.COM
        #region Download Purchase Report With Custom Excel
        public void DownloadPurchaseReport(string vendorCd, string vendorName, string periodFrom, string periodTo)
        {
            byte[] result = null;
            string fileName = null;

            try
            {

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                List<PurchaseReportDownload> logList = new List<PurchaseReportDownload>();
                logList = db.Fetch<PurchaseReportDownload>("PurchaseReportInquiryDownload", new object[] { vendorCd, vendorName, periodFrom, periodTo });

                fileName = "Purchase Report IPPCS.xls";
                result = GenerateDownloadFile(logList);
            }
            catch (Exception e)
            {

            }

            this.SendDataAsAttachment(fileName, result);
        }

        public byte[] GenerateDownloadFile(List<PurchaseReportDownload> purchaseReport)
        {
            byte[] result = null;

            try
            {
                result = CreateFile(purchaseReport);
            }
            finally
            {

            }

            return result;
        }

        private static readonly string SHEET_NAME_PURCHASE_REPORT = "Purchase Report";
        public byte[] CreateFile(List<PurchaseReportDownload> purchaseReport)
        {
            Dictionary<string, string> headers = null;
            ISheet sheet1 = null;
            HSSFWorkbook workbook = null;
            byte[] result;
            int startRow = 0;

            workbook = new HSSFWorkbook();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            short dateTimeFormat = dataFormat.GetFormat("dd/MM/yyyy HH:mm:ss");

            ICellStyle cellStyleDataLeft = createCellStyleDataLeft(workbook);
            ICellStyle cellStyleDataRight = createCellStyleDataRight(workbook);
            ICellStyle cellStyleDataCenter = createCellStyleDataCenter(workbook);
            ICellStyle cellStyleDataDouble = createCellStyleDataDouble(workbook);
            ICellStyle cellStyleHeader = createCellStyleColumnHeader(workbook);
            ICellStyle cellStyleTitle = createCellStyleColumnTitle(workbook);
            ICellStyle cellStyleTitleSecond = createCellStyleColumnTitleSecond(workbook);
            ICellStyle cellStyleDataTime = createCellStyleDataDate(workbook, dateTimeFormat);

            sheet1 = workbook.CreateSheet(EscapeSheetName(SHEET_NAME_PURCHASE_REPORT));
            sheet1.FitToPage = false;

            headers = new Dictionary<string, string>();

            WriteDetail(workbook, sheet1, startRow, cellStyleTitle, cellStyleTitleSecond, cellStyleHeader, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter, cellStyleDataDouble, purchaseReport);
            
            using (MemoryStream buffer = new MemoryStream())
            {
                workbook.Write(buffer);
                result = buffer.GetBuffer();
            }

            workbook = null;
            return result;
        }

        public void WriteDetail(HSSFWorkbook wb, ISheet sheet1, int startRow, ICellStyle cellStyleTitle, ICellStyle cellStyleSecond, ICellStyle cellStyleHeader, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter, ICellStyle cellStyleDataDouble, List<PurchaseReportDownload> purchaseReport)
        {
            int rowIdx = startRow;
            int itemCount = 0;

            sheet1.SetColumnWidth(0, 5 * 256);
            sheet1.SetColumnWidth(1, 42 * 256);
            sheet1.SetColumnWidth(2, 10 * 256);
            sheet1.SetColumnWidth(3, 12 * 256);
            sheet1.SetColumnWidth(4, 17 * 256);
            sheet1.SetColumnWidth(5, 17 * 256);
            sheet1.SetColumnWidth(6, 17 * 256);
            sheet1.SetColumnWidth(7, 17 * 256);
            sheet1.SetColumnWidth(8, 17 * 256);
            sheet1.SetColumnWidth(9, 7 * 256);

            CreateSingleColHeader(wb, sheet1, 0, 0, cellStyleHeader, "No");
            CreateSingleColHeader(wb, sheet1, 1, 0, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 2, 0, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 0, 1, cellStyleHeader, "Vendor Name");
            CreateSingleColHeader(wb, sheet1, 1, 1, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 2, 1, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 0, 2, cellStyleHeader, "Vendor Code");
            CreateSingleColHeader(wb, sheet1, 1, 2, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 2, 2, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 0, 3, cellStyleHeader, "Category");
            CreateSingleColHeader(wb, sheet1, 1, 3, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 2, 3, cellStyleHeader, "");

            CreateSingleColHeader(wb, sheet1, 0, 4, cellStyleHeader, "Component");
            CreateSingleColHeader(wb, sheet1, 0, 5, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 0, 6, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 0, 7, cellStyleHeader, "");

            CreateSingleColHeader(wb, sheet1, 1, 4, cellStyleHeader, "Good Receipt (A) (Tableau)");
            CreateSingleColHeader(wb, sheet1, 2, 4, cellStyleHeader, "Actual");

            CreateSingleColHeader(wb, sheet1, 1, 5, cellStyleHeader, "Invoicing (B)");
            CreateSingleColHeader(wb, sheet1, 1, 6, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 1, 7, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 2, 5, cellStyleHeader, "Actual");
            CreateSingleColHeader(wb, sheet1, 2, 6, cellStyleHeader, "Outstanding");
            CreateSingleColHeader(wb, sheet1, 2, 7, cellStyleHeader, "Total");

            CreateSingleColHeader(wb, sheet1, 1, 8, cellStyleHeader, "Different (C=A-B)");
            CreateSingleColHeader(wb, sheet1, 2, 8, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 3, 8, cellStyleHeader, "");

            CreateSingleColHeader(wb, sheet1, 1, 9, cellStyleHeader, "% [C/A]");
            CreateSingleColHeader(wb, sheet1, 2, 9, cellStyleHeader, "");
            CreateSingleColHeader(wb, sheet1, 3, 9, cellStyleHeader, "");

            var merger1 = new NPOI.SS.Util.CellRangeAddress(0, 2, 0, 0);
            var merger2 = new NPOI.SS.Util.CellRangeAddress(0, 2, 1, 1);
            var merger3 = new NPOI.SS.Util.CellRangeAddress(0, 2, 2, 2);
            var merger4 = new NPOI.SS.Util.CellRangeAddress(0, 2, 3, 3);

            var merger5 = new NPOI.SS.Util.CellRangeAddress(0, 0, 4, 9);
            var merger6 = new NPOI.SS.Util.CellRangeAddress(1, 1, 5, 7);
            var merger7 = new NPOI.SS.Util.CellRangeAddress(1, 2, 8, 8);
            var merger8 = new NPOI.SS.Util.CellRangeAddress(1, 2, 9, 9);
            sheet1.AddMergedRegion(merger1);
            sheet1.AddMergedRegion(merger2);
            sheet1.AddMergedRegion(merger3);
            sheet1.AddMergedRegion(merger4);
            sheet1.AddMergedRegion(merger5);
            sheet1.AddMergedRegion(merger6);
            sheet1.AddMergedRegion(merger7);
            sheet1.AddMergedRegion(merger8);

            rowIdx = 3;
            foreach (PurchaseReportDownload st in purchaseReport)
            {
                WriteDetailSingleData(wb, cellStyleDataDouble, st, sheet1, ++itemCount, rowIdx++, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter);
            }
        }

        public void WriteDetailSingleData(HSSFWorkbook wb, ICellStyle cellStyleDouble, PurchaseReportDownload data, ISheet sheet1, int rowCount, int rowIndex, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter)
        {
            IRow row = sheet1.CreateRow(rowIndex);
            int col = 0;

            createCellText(row, cellStyleDataCenter, col++, data.NO.ToString());
            createCellText(row, cellStyleDataRight, col++, data.SUPPLIER_NAME);
            createCellText(row, cellStyleDataRight, col++, data.SUPPLIER_CODE);
            createCellText(row, cellStyleDataRight, col++, data.CATEGORY);
            createCellDouble(wb, row, cellStyleDouble, col++, data.GOOD_RECEIPT_ACTUAL);
            createCellDouble(wb, row, cellStyleDouble, col++, data.INVOICING_ACTUAL);
            createCellDouble(wb, row, cellStyleDouble, col++, data.INVOICING_OUTSTANDING);
            createCellDouble(wb, row, cellStyleDouble, col++, data.INVOICING_TOTAL);
            createCellDouble(wb, row, cellStyleDouble, col++, data.DIFFERENT);
            createCellDouble(wb, row, cellStyleDouble, col++, data.PERCENT);
            //createCellText(row, cellStyleDataCenter, col++, data.PERCENT);
        }
        #endregion

        //CREATE BY AGUNGPANDUAN.COM
        #region Download Purchase GR With Custom Excel
        private static string PeriodFrom = "";
        private static string PeriodTo = "";
        private static string VendorCode = "";
        private static string VendorName = "";
        public void DownloadPurchaseGRReport(string vendorCd, string vendorName, string periodFrom, string periodTo)
        {
            byte[] result = null;
            string fileName = null;

            try
            {
                PeriodFrom = periodFrom;
                PeriodTo = periodTo;
                VendorCode = vendorCd;
                VendorName = vendorName;
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                List<PurchaseReportGoodReceipt> logList = new List<PurchaseReportGoodReceipt>();
                logList = db.Fetch<PurchaseReportGoodReceipt>("GetGoodReceiptActualDetail", new object[] { vendorCd, vendorName, periodFrom, periodTo });

                fileName = string.Format("Purchase Good Receipt Detail Report {0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
                result = GenerateDownloadGRFile(logList);
            }
            catch (Exception e)
            {

            }

            this.SendDataAsAttachment(fileName, result);
        }

        public byte[] GenerateDownloadGRFile(List<PurchaseReportGoodReceipt> purchaseReport)
        {
            byte[] result = null;

            try
            {
                result = CreateGRFile(purchaseReport);
            }
            finally
            {

            }

            return result;
        }

        private static readonly string SHEET_NAME_PURCHASE_GR_REPORT = "Purchase GR Report";
        public byte[] CreateGRFile(List<PurchaseReportGoodReceipt> purchaseReport)
        {
            Dictionary<string, string> headers = null;
            ISheet sheet1 = null;
            HSSFWorkbook workbook = null;
            byte[] result;
            int startRow = 0;

            workbook = new HSSFWorkbook();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            short dateTimeFormat = dataFormat.GetFormat("dd/MM/yyyy HH:mm:ss");

            ICellStyle cellStyleDataLeft = createCellStyleDataLeft(workbook);
            ICellStyle cellStyleDataRight = createCellStyleDataRight(workbook);
            ICellStyle cellStyleDataCenter = createCellStyleDataCenter(workbook);
            ICellStyle cellStyleDataDouble = createCellStyleDataDouble(workbook);
            ICellStyle cellStyleHeader = createCellStyleColumnHeader(workbook);
            ICellStyle cellStyleTitle = createCellStyleColumnTitle(workbook);
            ICellStyle cellStyleTitleSecond = createCellStyleColumnTitleSecond(workbook);
            ICellStyle cellStyleDataTime = createCellStyleDataDate(workbook, dateTimeFormat);
            
            sheet1 = workbook.CreateSheet(EscapeSheetName(SHEET_NAME_PURCHASE_GR_REPORT));
            sheet1.FitToPage = false;

            headers = new Dictionary<string, string>();

            WriteGRDetail(workbook, sheet1, startRow, cellStyleTitle, cellStyleTitleSecond, cellStyleHeader, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter, cellStyleDataDouble, purchaseReport);

            using (MemoryStream buffer = new MemoryStream())
            {
                workbook.Write(buffer);
                result = buffer.GetBuffer();
            }

            workbook = null;
            return result;
        }

        public void WriteGRDetail(HSSFWorkbook wb, ISheet sheet1, int startRow, ICellStyle cellStyleTitle, ICellStyle cellStyleSecond, ICellStyle cellStyleHeader, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter, ICellStyle cellStyleDataDouble, List<PurchaseReportGoodReceipt> purchaseReport)
        {
            int rowIdx = startRow;
            int itemCount = 0;

            CreateSingleColHeader(wb, sheet1, 0, 0, cellStyleTitle, "PT. Toyota Motor Manufacturing Indonesia");
            CreateSingleColHeader(wb, sheet1, 1, 0, cellStyleTitle, "Purchase Report Detail - GR Amount");
            //CreateSingleColHeader(wb, sheet1, 1, 1, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 2, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 3, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 4, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 5, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 6, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 7, cellStyleHeader, "");

            CreateSingleColHeader(wb, sheet1, 3, 0, cellStyleSecond, "Document Date:");

            CreateSingleColHeader(wb, sheet1, 3, 0, cellStyleSecond, "From");
            CreateSingleColHeader(wb, sheet1, 3, 1, cellStyleSecond, ": " + DateTime.Parse(PeriodFrom).ToString("dd-MMM-yyyy"));
            CreateSingleColHeader(wb, sheet1, 4, 0, cellStyleSecond, "To");
            CreateSingleColHeader(wb, sheet1, 4, 1, cellStyleSecond, ": " + DateTime.Parse(PeriodTo).ToString("dd-MMM-yyyy"));
            CreateSingleColHeader(wb, sheet1, 3, 2, cellStyleSecond, "Vendor Code");
            CreateSingleColHeader(wb, sheet1, 3, 3, cellStyleSecond, ": " + VendorCode);
            CreateSingleColHeader(wb, sheet1, 4, 2, cellStyleSecond, "Vendor Name");
            CreateSingleColHeader(wb, sheet1, 4, 3, cellStyleSecond, ": " + VendorName);

            //Before Your Text, You must set column width if not column only have 8.43
            sheet1.SetColumnWidth(0, 13 * 256);
            sheet1.SetColumnWidth(1, 50 * 256);
            sheet1.SetColumnWidth(2, 13 * 256);
            sheet1.SetColumnWidth(3, 15 * 256);
            sheet1.SetColumnWidth(4, 12 * 256);
            sheet1.SetColumnWidth(5, 14 * 256);
            sheet1.SetColumnWidth(6, 14 * 256);
            sheet1.SetColumnWidth(7, 14 * 256);
            sheet1.SetColumnWidth(8, 17 * 256);
            sheet1.SetColumnWidth(9, 17 * 256);

            CreateSingleColHeader(wb, sheet1, 6, 0, cellStyleHeader, "Vendor Code");
            CreateSingleColHeader(wb, sheet1, 6, 1, cellStyleHeader, "Vendor Name");
            CreateSingleColHeader(wb, sheet1, 6, 2, cellStyleHeader, "Category");
            CreateSingleColHeader(wb, sheet1, 6, 3, cellStyleHeader, "Condition Type");
            CreateSingleColHeader(wb, sheet1, 6, 4, cellStyleHeader, "GL Account");
            CreateSingleColHeader(wb, sheet1, 6, 5, cellStyleHeader, "PO No");
            CreateSingleColHeader(wb, sheet1, 6, 6, cellStyleHeader, "Posting date");
            CreateSingleColHeader(wb, sheet1, 6, 7, cellStyleHeader, "Local Curr");
            CreateSingleColHeader(wb, sheet1, 6, 8, cellStyleHeader, "Amount Doc");
            CreateSingleColHeader(wb, sheet1, 6, 9, cellStyleHeader, "Amount local");

            var mergerTitle1 = new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 9);
            var mergerTitle2 = new NPOI.SS.Util.CellRangeAddress(1, 1, 0, 9);
            //var mergerTitleSecond1 = new NPOI.SS.Util.CellRangeAddress(3, 3, 2, 3);
            //var mergerTitleSecond2 = new NPOI.SS.Util.CellRangeAddress(4, 4, 2, 3);

            sheet1.AddMergedRegion(mergerTitle1);
            sheet1.AddMergedRegion(mergerTitle2);
            //sheet1.AddMergedRegion(mergerTitleSecond1);
            //sheet1.AddMergedRegion(mergerTitleSecond2);

            rowIdx = 7;

            //Get Total Amount
            double TotalAmountDoc = 0;
            double TotalAmountLoc = 0;
            int rowTotal = 0;
            foreach (PurchaseReportGoodReceipt st in purchaseReport)
            {
                WriteGRDetailSingleData(wb, cellStyleDataDouble, st, sheet1, ++itemCount, rowIdx++, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter);
                TotalAmountDoc = TotalAmountDoc + st.AMOUNT_DOC;
                TotalAmountLoc = TotalAmountLoc + st.AMOUNT_LOCAL;
                rowTotal++;
            }

            //Create Row Total
            IRow row = sheet1.CreateRow(rowTotal + 7);
            createCellText(row, cellStyleDataRight, 0, "Total");
            createCellText(row, cellStyleDataRight, 1, "");
            createCellText(row, cellStyleDataRight, 2, "");
            createCellText(row, cellStyleDataRight, 3, "");
            createCellText(row, cellStyleDataRight, 4, "");
            createCellText(row, cellStyleDataRight, 5, "");
            createCellText(row, cellStyleDataRight, 6, "");
            createCellText(row, cellStyleDataRight, 7, "");
            var mergerTotal = new NPOI.SS.Util.CellRangeAddress(rowTotal + 7, rowTotal + 7, 0, 7);
            sheet1.AddMergedRegion(mergerTotal);

            createCellDouble(wb, row, cellStyleDataDouble, 8, TotalAmountDoc);
            createCellDouble(wb, row, cellStyleDataDouble, 9, TotalAmountDoc);
        }

        public void WriteGRDetailSingleData(HSSFWorkbook wb, ICellStyle cellStyleDouble, PurchaseReportGoodReceipt data, ISheet sheet1, int rowCount, int rowIndex, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter)
        {
            IRow row = sheet1.CreateRow(rowIndex);
            int col = 0;

            createCellText(row, cellStyleDataCenter, col++, data.SUPPLIER_CODE);
            createCellText(row, cellStyleDataCenter, col++, data.SUPPLIER_NAME);
            createCellText(row, cellStyleDataCenter, col++, data.CATEGORY);
            createCellText(row, cellStyleDataCenter, col++, data.CONDITION_TYPE);
            createCellText(row, cellStyleDataCenter, col++, data.GL_ACCOUNT);
            createCellText(row, cellStyleDataCenter, col++, data.PO_NO);
            createCellText(row, cellStyleDataCenter, col++, data.POSTING_DATE.ToString("dd-MMM-yyyy"));
            createCellText(row, cellStyleDataCenter, col++, data.LOCAL_CURR);
            createCellDouble(wb, row, cellStyleDouble, col++, data.AMOUNT_DOC);
            createCellDouble(wb, row, cellStyleDouble, col++, data.AMOUNT_LOCAL);
        }
        #endregion

        //CREATE BY AGUNGPANDUAN.COM
        #region Download Purchase Invoicing Actual With Custom Excel
        public void DownloadPurchaseActualReport(string vendorCd, string vendorName, string periodFrom, string periodTo)
        {
            byte[] result = null;
            string fileName = null;

            try
            {
                PeriodFrom = periodFrom;
                PeriodTo = periodTo;
                VendorCode = vendorCd;
                VendorName = vendorName;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                List<PurchaseReportInvoiceActual> logList = new List<PurchaseReportInvoiceActual>();
                logList = db.Fetch<PurchaseReportInvoiceActual>("GetInvoiceActualDetail", new object[] { vendorCd, vendorName, periodFrom, periodTo });

                fileName = string.Format("Purchase Invoicing Actual Report {0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
                result = GenerateDownloadActualFile(logList);
            }
            catch (Exception e)
            {

            }

            this.SendDataAsAttachment(fileName, result);
        }

        public byte[] GenerateDownloadActualFile(List<PurchaseReportInvoiceActual> purchaseReport)
        {
            byte[] result = null;

            try
            {
                result = CreateActualFile(purchaseReport);
            }
            finally
            {

            }

            return result;
        }

        private static readonly string SHEET_NAME_PURCHASE_ACTUAL_REPORT = "Purchase Actual Report";
        public byte[] CreateActualFile(List<PurchaseReportInvoiceActual> purchaseReport)
        {
            Dictionary<string, string> headers = null;
            ISheet sheet1 = null;
            HSSFWorkbook workbook = null;
            byte[] result;
            int startRow = 0;

            workbook = new HSSFWorkbook();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            short dateTimeFormat = dataFormat.GetFormat("dd/MM/yyyy HH:mm:ss");

            ICellStyle cellStyleDataLeft = createCellStyleDataLeft(workbook);
            ICellStyle cellStyleDataRight = createCellStyleDataRight(workbook);
            ICellStyle cellStyleDataCenter = createCellStyleDataCenter(workbook);
            ICellStyle cellStyleDataDouble = createCellStyleDataDouble(workbook);
            ICellStyle cellStyleHeader = createCellStyleColumnHeader(workbook);
            ICellStyle cellStyleTitle = createCellStyleColumnTitle(workbook);
            ICellStyle cellStyleTitleSecond = createCellStyleColumnTitleSecond(workbook);
            ICellStyle cellStyleDataTime = createCellStyleDataDate(workbook, dateTimeFormat);

            sheet1 = workbook.CreateSheet(EscapeSheetName(SHEET_NAME_PURCHASE_ACTUAL_REPORT));
            sheet1.FitToPage = false;

            headers = new Dictionary<string, string>();

            WriteActualDetail(workbook, sheet1, startRow, cellStyleTitle, cellStyleTitleSecond, cellStyleHeader, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter, cellStyleDataDouble, purchaseReport);

            using (MemoryStream buffer = new MemoryStream())
            {
                workbook.Write(buffer);
                result = buffer.GetBuffer();
            }

            workbook = null;
            return result;
        }

        public void WriteActualDetail(HSSFWorkbook wb, ISheet sheet1, int startRow, ICellStyle cellStyleTitle, ICellStyle cellStyleSecond, ICellStyle cellStyleHeader, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter, ICellStyle cellStyleDataDouble, List<PurchaseReportInvoiceActual> purchaseReport)
        {
            int rowIdx = startRow;
            int itemCount = 0;

            CreateSingleColHeader(wb, sheet1, 0, 0, cellStyleTitle, "PT. Toyota Motor Manufacturing Indonesia");
            CreateSingleColHeader(wb, sheet1, 1, 0, cellStyleTitle, "Purchase Report Detail - Actual Amount");
            //CreateSingleColHeader(wb, sheet1, 1, 1, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 2, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 3, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 4, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 5, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 6, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 7, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 8, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 9, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 10, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 11, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 12, cellStyleHeader, "");

            CreateSingleColHeader(wb, sheet1, 3, 0, cellStyleSecond, "Document Date:");

            CreateSingleColHeader(wb, sheet1, 3, 0, cellStyleSecond, "From");
            CreateSingleColHeader(wb, sheet1, 3, 1, cellStyleSecond, ": " + DateTime.Parse(PeriodFrom).ToString("dd-MMM-yyyy"));
            CreateSingleColHeader(wb, sheet1, 4, 0, cellStyleSecond, "To");
            CreateSingleColHeader(wb, sheet1, 4, 1, cellStyleSecond, ": " + DateTime.Parse(PeriodTo).ToString("dd-MMM-yyyy"));
            CreateSingleColHeader(wb, sheet1, 3, 2, cellStyleSecond, "Vendor Code");
            CreateSingleColHeader(wb, sheet1, 3, 3, cellStyleSecond, ": " + VendorCode);
            CreateSingleColHeader(wb, sheet1, 4, 2, cellStyleSecond, "Vendor Name");
            CreateSingleColHeader(wb, sheet1, 4, 3, cellStyleSecond, ": " + VendorName);

            //Before Your Text, You must set column width if not column only have 8.43
            sheet1.SetColumnWidth(0, 13 * 256);
            sheet1.SetColumnWidth(1, 50 * 256);
            sheet1.SetColumnWidth(2, 13 * 256);
            sheet1.SetColumnWidth(3, 15 * 256);
            sheet1.SetColumnWidth(4, 12 * 256);
            sheet1.SetColumnWidth(5, 14 * 256);
            sheet1.SetColumnWidth(6, 14 * 256);
            sheet1.SetColumnWidth(7, 17 * 256);
            sheet1.SetColumnWidth(8, 17 * 256);
            sheet1.SetColumnWidth(9, 17 * 256);

            sheet1.SetColumnWidth(10, 11 * 256);
            sheet1.SetColumnWidth(11, 13 * 256);
            sheet1.SetColumnWidth(12, 13 * 256);

            CreateSingleColHeader(wb, sheet1, 6, 0, cellStyleHeader, "Vendor Code");
            CreateSingleColHeader(wb, sheet1, 6, 1, cellStyleHeader, "Vendor Name");
            CreateSingleColHeader(wb, sheet1, 6, 2, cellStyleHeader, "Category");
            CreateSingleColHeader(wb, sheet1, 6, 3, cellStyleHeader, "Condition Type");
            CreateSingleColHeader(wb, sheet1, 6, 4, cellStyleHeader, "Invoice No");
            CreateSingleColHeader(wb, sheet1, 6, 5, cellStyleHeader, "Invoice Date");
            CreateSingleColHeader(wb, sheet1, 6, 6, cellStyleHeader, "Currency");
            CreateSingleColHeader(wb, sheet1, 6, 7, cellStyleHeader, "Invoice Net Amount");
            CreateSingleColHeader(wb, sheet1, 6, 8, cellStyleHeader, "PPV Amount");
            CreateSingleColHeader(wb, sheet1, 6, 9, cellStyleHeader, "Total Purchase");
            CreateSingleColHeader(wb, sheet1, 6, 10, cellStyleHeader, "Status");
            CreateSingleColHeader(wb, sheet1, 6, 11, cellStyleHeader, "SAP Doc No");
            CreateSingleColHeader(wb, sheet1, 6, 12, cellStyleHeader, "Payment Doc No");

            var mergerTitle1 = new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 12);
            var mergerTitle2 = new NPOI.SS.Util.CellRangeAddress(1, 1, 0, 12);
            //var mergerTitleSecond1 = new NPOI.SS.Util.CellRangeAddress(3, 3, 2, 3);
            //var mergerTitleSecond2 = new NPOI.SS.Util.CellRangeAddress(4, 4, 2, 3);

            sheet1.AddMergedRegion(mergerTitle1);
            sheet1.AddMergedRegion(mergerTitle2);
            //sheet1.AddMergedRegion(mergerTitleSecond1);
            //sheet1.AddMergedRegion(mergerTitleSecond2);

            rowIdx = 7;

            //Get Total Amount
            double TotalPurchase = 0;
            int rowTotal = 0;
            foreach (PurchaseReportInvoiceActual st in purchaseReport)
            {
                WriteActualDetailSingleData(wb, cellStyleDataDouble, st, sheet1, ++itemCount, rowIdx++, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter);
                TotalPurchase = TotalPurchase + st.TOTAL_PURCHASE;
                rowTotal++;
            }

            //Create Row Total
            IRow row = sheet1.CreateRow(rowTotal + 7);
            createCellText(row, cellStyleDataRight, 0, "Total");
            createCellText(row, cellStyleDataRight, 1, "");
            createCellText(row, cellStyleDataRight, 2, "");
            createCellText(row, cellStyleDataRight, 3, "");
            createCellText(row, cellStyleDataRight, 4, "");
            createCellText(row, cellStyleDataRight, 5, "");
            createCellText(row, cellStyleDataRight, 6, "");
            createCellText(row, cellStyleDataRight, 7, "");
            createCellText(row, cellStyleDataRight, 8, "");
            var mergerTotal = new NPOI.SS.Util.CellRangeAddress(rowTotal + 7, rowTotal + 7, 0, 8);
            sheet1.AddMergedRegion(mergerTotal);

            createCellDouble(wb, row, cellStyleDataDouble, 9, TotalPurchase);
        }

        public void WriteActualDetailSingleData(HSSFWorkbook wb, ICellStyle cellStyleDouble, PurchaseReportInvoiceActual data, ISheet sheet1, int rowCount, int rowIndex, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter)
        {
            IRow row = sheet1.CreateRow(rowIndex);
            int col = 0;

            createCellText(row, cellStyleDataCenter, col++, data.SUPPLIER_CODE);
            createCellText(row, cellStyleDataCenter, col++, data.SUPPLIER_NAME);
            createCellText(row, cellStyleDataCenter, col++, data.CATEGORY);
            createCellText(row, cellStyleDataCenter, col++, data.CONDITION_TYPE);
            createCellText(row, cellStyleDataCenter, col++, data.INVOICE_NO);
            createCellText(row, cellStyleDataCenter, col++, data.INVOICE_DATE.ToString("dd-MMM-yyyy"));
            createCellText(row, cellStyleDataCenter, col++, data.CURRENCY);
            createCellDouble(wb, row, cellStyleDouble, col++, data.INVOICE_NET_AMOUNT);
            createCellDouble(wb, row, cellStyleDouble, col++, data.PPV_AMOUNT);
            createCellDouble(wb, row, cellStyleDouble, col++, data.TOTAL_PURCHASE);
            createCellText(row, cellStyleDataCenter, col++, data.STATUS);
            createCellText(row, cellStyleDataCenter, col++, data.SAP_DOC_NO);
            createCellText(row, cellStyleDataCenter, col++, data.PAYMENT_DOC_NO);
        }
        #endregion

        #region Download Purchase Invoicing Outstanding With Custom Excel
        public void DownloadPurchaseOutstandingReport(string vendorCd, string vendorName, string periodFrom, string periodTo)
        {
            byte[] result = null;
            string fileName = null;

            try
            {
                PeriodFrom = periodFrom;
                PeriodTo = periodTo;
                VendorCode = vendorCd;
                VendorName = vendorName;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                List<PurchaseReportInvoiceOutstanding> logList = new List<PurchaseReportInvoiceOutstanding>();
                logList = db.Fetch<PurchaseReportInvoiceOutstanding>("GetInvoiceOutstandingDetail", new object[] { vendorCd, vendorName, periodFrom, periodTo });

                fileName = string.Format("Purchase Outstanding Report {0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
                result = GenerateDownloadOutstandingFile(logList);
            }
            catch (Exception e)
            {

            }

            this.SendDataAsAttachment(fileName, result);
        }

        public byte[] GenerateDownloadOutstandingFile(List<PurchaseReportInvoiceOutstanding> purchaseReport)
        {
            byte[] result = null;

            try
            {
                result = CreateOutstandingFile(purchaseReport);
            }
            finally
            {

            }

            return result;
        }

        private static readonly string SHEET_NAME_PURCHASE_OUTSTANDING_REPORT = "Purchase Outstanding Report";
        public byte[] CreateOutstandingFile(List<PurchaseReportInvoiceOutstanding> purchaseReport)
        {
            Dictionary<string, string> headers = null;
            ISheet sheet1 = null;
            HSSFWorkbook workbook = null;
            byte[] result;
            int startRow = 0;

            workbook = new HSSFWorkbook();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            short dateTimeFormat = dataFormat.GetFormat("dd/MM/yyyy HH:mm:ss");

            ICellStyle cellStyleDataLeft = createCellStyleDataLeft(workbook);
            ICellStyle cellStyleDataRight = createCellStyleDataRight(workbook);
            ICellStyle cellStyleDataCenter = createCellStyleDataCenter(workbook);
            ICellStyle cellStyleDataDouble = createCellStyleDataDouble(workbook);
            ICellStyle cellStyleHeader = createCellStyleColumnHeader(workbook);
            ICellStyle cellStyleTitle = createCellStyleColumnTitle(workbook);
            ICellStyle cellStyleTitleSecond = createCellStyleColumnTitleSecond(workbook);
            ICellStyle cellStyleDataTime = createCellStyleDataDate(workbook, dateTimeFormat);

            sheet1 = workbook.CreateSheet(EscapeSheetName(SHEET_NAME_PURCHASE_OUTSTANDING_REPORT));
            sheet1.FitToPage = false;

            headers = new Dictionary<string, string>();

            WriteOutstandingDetail(workbook, sheet1, startRow, cellStyleTitle, cellStyleTitleSecond, cellStyleHeader, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter, cellStyleDataDouble, purchaseReport);

            using (MemoryStream buffer = new MemoryStream())
            {
                workbook.Write(buffer);
                result = buffer.GetBuffer();
            }

            workbook = null;
            return result;
        }

        public void WriteOutstandingDetail(HSSFWorkbook wb, ISheet sheet1, int startRow, ICellStyle cellStyleTitle, ICellStyle cellStyleSecond, ICellStyle cellStyleHeader, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter, ICellStyle cellStyleDataDouble, List<PurchaseReportInvoiceOutstanding> purchaseReport)
        {
            int rowIdx = startRow;
            int itemCount = 0;

            CreateSingleColHeader(wb, sheet1, 0, 0, cellStyleTitle, "PT. Toyota Motor Manufacturing Indonesia");
            CreateSingleColHeader(wb, sheet1, 1, 0, cellStyleTitle, "Purchase Report Detail - Outstanding Amount");
            //CreateSingleColHeader(wb, sheet1, 1, 1, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 2, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 3, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 4, cellStyleHeader, "");
            //CreateSingleColHeader(wb, sheet1, 1, 5, cellStyleHeader, "");

            CreateSingleColHeader(wb, sheet1, 3, 0, cellStyleSecond, "Document Date:");

            CreateSingleColHeader(wb, sheet1, 3, 0, cellStyleSecond, "From");
            CreateSingleColHeader(wb, sheet1, 3, 1, cellStyleSecond, ": " + DateTime.Parse(PeriodFrom).ToString("dd-MMM-yyyy"));
            CreateSingleColHeader(wb, sheet1, 4, 0, cellStyleSecond, "To");
            CreateSingleColHeader(wb, sheet1, 4, 1, cellStyleSecond, ": " + DateTime.Parse(PeriodTo).ToString("dd-MMM-yyyy"));
            CreateSingleColHeader(wb, sheet1, 3, 2, cellStyleSecond, "Vendor Code");
            CreateSingleColHeader(wb, sheet1, 3, 3, cellStyleSecond, ": " + VendorCode);
            CreateSingleColHeader(wb, sheet1, 4, 2, cellStyleSecond, "Vendor Name");
            CreateSingleColHeader(wb, sheet1, 4, 3, cellStyleSecond, ": " + VendorName);

            //Before Your Text, You must set column width if not column only have 8.43
            sheet1.SetColumnWidth(0, 13 * 256);
            sheet1.SetColumnWidth(1, 50 * 256);
            sheet1.SetColumnWidth(2, 13 * 256);
            sheet1.SetColumnWidth(3, 15 * 256);
            sheet1.SetColumnWidth(4, 12 * 256);
            sheet1.SetColumnWidth(5, 17 * 256);

            CreateSingleColHeader(wb, sheet1, 6, 0, cellStyleHeader, "Vendor Code");
            CreateSingleColHeader(wb, sheet1, 6, 1, cellStyleHeader, "Vendor Name");
            CreateSingleColHeader(wb, sheet1, 6, 2, cellStyleHeader, "Category");
            CreateSingleColHeader(wb, sheet1, 6, 3, cellStyleHeader, "Condition Type");
            CreateSingleColHeader(wb, sheet1, 6, 4, cellStyleHeader, "Currency");
            CreateSingleColHeader(wb, sheet1, 6, 5, cellStyleHeader, "Total Purchase");

            var mergerTitle1 = new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 9);
            var mergerTitle2 = new NPOI.SS.Util.CellRangeAddress(1, 1, 0, 9);
            //var mergerTitleSecond1 = new NPOI.SS.Util.CellRangeAddress(3, 3, 2, 3);
            //var mergerTitleSecond2 = new NPOI.SS.Util.CellRangeAddress(4, 4, 2, 3);

            sheet1.AddMergedRegion(mergerTitle1);
            sheet1.AddMergedRegion(mergerTitle2);
            //sheet1.AddMergedRegion(mergerTitleSecond1);
            //sheet1.AddMergedRegion(mergerTitleSecond2);

            rowIdx = 7;

            //Get Total Amount
            double TotalPurchase = 0;
            int rowTotal = 0;
            foreach (PurchaseReportInvoiceOutstanding st in purchaseReport)
            {
                WriteOutstandingDetailSingleData(wb, cellStyleDataDouble, st, sheet1, ++itemCount, rowIdx++, cellStyleDataLeft, cellStyleDataRight, cellStyleDataCenter);
                TotalPurchase = TotalPurchase + st.TOTAL_PURCHASE;
                rowTotal++;
            }

            //Create Row Total
            IRow row = sheet1.CreateRow(rowTotal + 7);
            createCellText(row, cellStyleDataRight, 0, "Total");
            createCellText(row, cellStyleDataRight, 1, "");
            createCellText(row, cellStyleDataRight, 2, "");
            createCellText(row, cellStyleDataRight, 3, "");
            createCellText(row, cellStyleDataRight, 4, "");
            var mergerTotal = new NPOI.SS.Util.CellRangeAddress(rowTotal + 7, rowTotal + 7, 0, 4);
            sheet1.AddMergedRegion(mergerTotal);

            createCellDouble(wb, row, cellStyleDataDouble, 5, TotalPurchase);
        }

        public void WriteOutstandingDetailSingleData(HSSFWorkbook wb, ICellStyle cellStyleDouble, PurchaseReportInvoiceOutstanding data, ISheet sheet1, int rowCount, int rowIndex, ICellStyle cellStyleDataLeft, ICellStyle cellStyleDataRight, ICellStyle cellStyleDataCenter)
        {
            IRow row = sheet1.CreateRow(rowIndex);
            int col = 0;

            createCellText(row, cellStyleDataCenter, col++, data.SUPPLIER_CODE);
            createCellText(row, cellStyleDataCenter, col++, data.SUPPLIER_NAME);
            createCellText(row, cellStyleDataCenter, col++, data.CATEGORY);
            createCellText(row, cellStyleDataCenter, col++, data.CONDITION_TYPE);
            createCellText(row, cellStyleDataCenter, col++, data.CURRENCY);
            createCellDouble(wb, row, cellStyleDouble, col++, data.TOTAL_PURCHASE);
        }
        #endregion
    }
}
