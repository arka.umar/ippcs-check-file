﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;

using Portal.ExcelUpload;
using Portal.Models;
using Portal.Models.DeliveryNonTLMS;
using Portal.Models.Globals;

using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.MVC;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;

namespace Portal.Controllers
{
    public class DeliveryNonTLMSController : BaseController
    {
        public DeliveryNonTLMSController()
            : base("Delivery Non TLMS")
        { 
        
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void StartUp()
        {
            if (TempData["SesDataPopup"] != null)
                TempData["SesDataPopup"] = "";
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            ViewData["LogisticPartnerData"] = QueryLog;

            db.Close();
        }

        public ActionResult DeliveryNonTLMS()
        {
            return PartialView("HeaderDeliveryNonTLMS");
        }

        #region Grid Action
        
        public ActionResult PartialGridTLMS(string DateFrom, string DateTo, string Publish, string Route, string Rate)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryNonTLMSModel model = new DeliveryNonTLMSModel();
            model.GridContent = db.Query<DeliveryNonTLMSGridContent>("getGridDeliveryNonTLMS", new object[] {
                DateFrom, DateTo, Route, Rate, Publish
            }).ToList<DeliveryNonTLMSGridContent>();
            Model.AddModel(model);

            db.Close();
            return PartialView("GridDeliveryNonTLMS", Model);
        }

        public PartialViewResult UpdatePartial(DeliveryNonTLMSData editable)
        {
            ViewData["DeliveryNonTLMS"] = editable;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryNonTLMSModel model = Model.GetModel<DeliveryNonTLMSModel>();
            model.Apps = db.Fetch<DeliveryNonTLMSData>("getGridDeliveryNonTLMS");
            db.Close();
            return PartialView("GridDeliveryNonTLMS", Model);

        }

        public ActionResult LPPartialAdditional()
        {
            TempData["GridName"] = "grlLogisticPartnerHeader";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            ViewData["LogisticPartnerData"] = QueryLog;

            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult PartialDeliveryTLMS()
        {
            return PartialView("PartialDeliveryTLMSVw");
        }

        #endregion

        #region Popup Replication Data

        public ActionResult PartialGridTLMSExeptionDate()
        {
            char[] splitChar = { ';' };
            char[] SplitLastChar = { ',' };
            string[] ParamUpdate;
            string[] ParamUpdateId;

            DeliveryNonTLMSModel model = new DeliveryNonTLMSModel();
            //model.ExeptionGridContent = model.ExeptionGridContent.ToList<DeliveryNonTLMSExeptionDate>();
            Model.AddModel(model);
            if (TempData["SesDataPopup"] == null)
                TempData["SesDataPopup"] = "";

            if (TempData["SesDataPopup"] == "")
            {
                //for (int i = 1; i <= 30; i++)
                //{
                //    Model.GetModel<DeliveryNonTLMSModel>().ExeptionGridContent.Add(new DeliveryNonTLMSExeptionDate()
                //    {
                //        EXEPTION_DATE = DateTime.Parse("2013-07-" + ((i < 10) ? "0" + i.ToString() : i.ToString()) + " 00:00:00")
                //    });

                //    if (TempData["SesDataPopup"] != null)
                //        TempData["SesDataPopup"] = TempData["SesDataPopup"] + ((TempData["SesDataPopup"].ToString() != "") ? ";" : "") + "2013-07-" + ((i < 10) ? "0" + i.ToString() : i.ToString()) + " 00:00:00";
                //    else
                //        TempData["SesDataPopup"] = "2013-07-" + ((i < 10) ? "0" + i.ToString() : i.ToString()) + " 00:00:00";
                //}
            }
            else
            {
                ParamUpdate = TempData["SesDataPopup"].ToString().Split(splitChar);
                TempData["SesDataPopup"] = null;
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                    Model.GetModel<DeliveryNonTLMSModel>().ExeptionGridContent.Add(new DeliveryNonTLMSExeptionDate()
                    {
                        EXEPTION_DATE = DateTime.Parse(ParamUpdateId[0].ToString())
                    });

                    if (TempData["SesDataPopup"] != null)
                        TempData["SesDataPopup"] = TempData["SesDataPopup"] + ((TempData["SesDataPopup"].ToString() != "") ? ";" : "") + ParamUpdateId[0].ToString();
                    else
                        TempData["SesDataPopup"] = ParamUpdateId[0].ToString();
                }
            }

            return PartialView("gridDeliveryNonTLMSReplicateExeption", Model);
        }

        public PartialViewResult PartialGridTLMSExeptionDateExeption(DeliveryNonTLMSExeptionDate editable)
        {
            ViewData["DeliveryNonTLMSReplicateExeption"] = editable;

            char[] splitChar = { ';' };
            char[] SplitLastChar = { ',' };
            string[] ParamUpdate;
            string[] ParamUpdateId;

            DeliveryNonTLMSModel model = new DeliveryNonTLMSModel();
            //model.ExeptionGridContent = model.ExeptionGridContent.ToList<DeliveryNonTLMSExeptionDate>();
            Model.AddModel(model);
            if (TempData["SesDataPopup"] == null)
                TempData["SesDataPopup"] = "";

            if (TempData["SesDataPopup"] == "")
            {
                //for (int i = 1; i <= 30; i++)
                //{
                //    Model.GetModel<DeliveryNonTLMSModel>().ExeptionGridContent.Add(new DeliveryNonTLMSExeptionDate()
                //    {
                //        EXEPTION_DATE = DateTime.Parse("2013-07-" + ((i < 10) ? "0" + i.ToString() : i.ToString()) + " 00:00:00")
                //    });

                //    if (TempData["SesDataPopup"] != null)
                //        TempData["SesDataPopup"] = TempData["SesDataPopup"] + ((TempData["SesDataPopup"].ToString() != "") ? ";" : "") + "2013-07-" + ((i < 10) ? "0" + i.ToString() : i.ToString()) + " 00:00:00";
                //    else
                //        TempData["SesDataPopup"] = "2013-07-" + ((i < 10) ? "0" + i.ToString() : i.ToString()) + " 00:00:00";
                //}
            }
            else
            {
                ParamUpdate = TempData["SesDataPopup"].ToString().Split(splitChar);
                TempData["SesDataPopup"] = null;
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                    Model.GetModel<DeliveryNonTLMSModel>().ExeptionGridContent.Add(new DeliveryNonTLMSExeptionDate()
                    {
                        EXEPTION_DATE = DateTime.Parse(ParamUpdateId[0].ToString())
                    });

                    if (TempData["SesDataPopup"] != null)
                        TempData["SesDataPopup"] = TempData["SesDataPopup"] + ((TempData["SesDataPopup"].ToString() != "") ? ";" : "") + ParamUpdateId[0].ToString();
                    else
                        TempData["SesDataPopup"] = ParamUpdateId[0].ToString();
                }
            }
            return PartialView("gridDeliveryNonTLMSReplicateExeption", Model);
        }

        public ContentResult DeletePopupExeptionScreen(string GridId, string GridSelection)
        {
            char[] splitChar = { ';' };
            char[] SplitLastChar = { ',' };
            string[] ParamUpdatePrevious;
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string[] ParamUpdateIdPrevious;
            string result = "";
            int isSame = 0;

            ParamUpdate = GridId.Split(splitChar);
            ParamUpdatePrevious = TempData["SesDataPopup"].ToString().Split(splitChar);
            if (GridSelection == "All")
            {
                ParamUpdate = TempData["SesDataPopup"].ToString().Split(splitChar);
            }

            try
            {
                TempData["SesDataPopup"] = null;
                DeliveryNonTLMSModel model = new DeliveryNonTLMSModel();
                model.ExeptionGridContent = model.ExeptionGridContent.ToList<DeliveryNonTLMSExeptionDate>();
                Model.AddModel(model);
                for (int i = 0; i < ParamUpdatePrevious.Length; i++)
                {
                    isSame = 0;
                    ParamUpdateIdPrevious = ParamUpdatePrevious[i].ToString().Split(SplitLastChar);
                    for (int j = 0; j < ParamUpdate.Length; j++)
                    {
                        ParamUpdateId = ParamUpdate[j].ToString().Split(SplitLastChar);
                        if (ParamUpdateIdPrevious[0].ToString() == ((CheckDate(ParamUpdateId[1].ToString()) ? ParamUpdateId[1].ToString() : formattingFullToShortDate(ParamUpdateId[1].ToString()).Replace("-","/") + " 00:00:00")))
                        {
                            isSame = isSame + 1;
                        }
                    }

                    if (isSame == 0)
                    {
                        Model.GetModel<DeliveryNonTLMSModel>().ExeptionGridContent.Add(new DeliveryNonTLMSExeptionDate()
                        {
                            EXEPTION_DATE = DateTime.Parse(ParamUpdateIdPrevious[0].ToString())
                        });

                        if (TempData["SesDataPopup"] != null)
                            TempData["SesDataPopup"] = TempData["SesDataPopup"] + ((TempData["SesDataPopup"].ToString() != "") ? ";" : "") + ParamUpdateIdPrevious[0].ToString();
                        else
                            TempData["SesDataPopup"] = ParamUpdateIdPrevious[0].ToString();
                    }

                }

            }
            catch (Exception ex)
            {
                result = "Error, failed to delete list.";
            }

            return Content(result);
        }

        public ContentResult SubmitReplication(string GridId, string GridSelection, string GridIdExeption, string ValidFrom, string ValidTo, string WorkDayOnly, string IsPublish)
        {
            char[] splitChar = { ';' };
            char[] SplitLastChar = { ',' };
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string result = "";
            string exeptionString = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (TempData["SesDataPopup"] != null) 
            {
                ParamUpdate = TempData["SesDataPopup"].ToString().Split(splitChar);
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                    exeptionString = exeptionString + ((exeptionString != "") ? ";" : "") + DateTime.Parse(ParamUpdateId[0].ToString()).ToString("ddd MMM dd yyyy HH:mm:ss") + " GMT+0700 (SE Asia Standard Time)";
                }
            }

            //if (GridSelection == "All")
            //{
            //    GridId = getListAllSelection(IsPublish);
            //}
             
            //---=== Copying Process ===---//
            try
            {
                List<DeliveryNonTLMSNormalisasi> QueryLogPublish = db.Fetch<DeliveryNonTLMSNormalisasi>("DoReplicateNonTLMS", new object[] { 
                    GridId, exeptionString, ((WorkDayOnly=="true")?"Y":"N"), formatingShortDate(ValidFrom), formatingShortDate(ValidTo), AuthorizedUser.Username
                });
                for (int i = 0; i < QueryLogPublish.Count; i++)
                {
                    result = QueryLogPublish[i].Result.ToString();
                }
            }
            catch (Exception ex)
            {
                result = "Error, failed to replicate data.";
            }
            //---=== End Copying Process ===---//

            db.Close();
            return Content(result);
        }

        public ContentResult AddReplicationExeption(string ValidDate)
        {
            char[] splitChar = { ';' };
            char[] SplitLastChar = { ',' };
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string result = "Process Succes";

            if (TempData["SesDataPopup"] == null)
            {
                TempData["SesDataPopup"] = "";
            }

            try
            {
                DeliveryNonTLMSModel model = new DeliveryNonTLMSModel();
                //model.ExeptionGridContent = model.ExeptionGridContent.ToList<DeliveryNonTLMSExeptionDate>();
                Model.AddModel(model);

                if (TempData["SesDataPopup"].ToString() != "")
                {
                    ParamUpdate = TempData["SesDataPopup"].ToString().Split(splitChar);
                    TempData["SesDataPopup"] = null;
                    for (int i = 0; i < ParamUpdate.Length; i++)
                    {
                        ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                        Model.GetModel<DeliveryNonTLMSModel>().ExeptionGridContent.Add(new DeliveryNonTLMSExeptionDate()
                        {
                            EXEPTION_DATE = DateTime.Parse(ParamUpdateId[0].ToString())
                        });

                        if (TempData["SesDataPopup"] != null)
                            TempData["SesDataPopup"] = TempData["SesDataPopup"] + ((TempData["SesDataPopup"].ToString() != "") ? ";" : "") + ParamUpdateId[0].ToString();
                        else
                            TempData["SesDataPopup"] = ParamUpdateId[0].ToString();
                    }
                }

                Model.GetModel<DeliveryNonTLMSModel>().ExeptionGridContent.Add(new DeliveryNonTLMSExeptionDate()
                {
                    EXEPTION_DATE = DateTime.Parse(formatingShortDate(ValidDate) + " 00:00:00")
                });

                if (TempData["SesDataPopup"].ToString() == "")
                {
                    TempData["SesDataPopup"] = formatingShortDate(ValidDate) + " 00:00:00";
                }
                else
                {
                    TempData["SesDataPopup"] = TempData["SesDataPopup"].ToString() + ";" + formatingShortDate(ValidDate) + " 00:00:00";
                }

            }
            catch (Exception ex)
            {
                result = "Error, to entry new exeption date.";
            }

            return Content(result);
        }

        #endregion

        #region Screen Action

        public ContentResult SavingOrderScreen(string Route, string TripDay, string TripNo, string LogPointCode, string LogPointPlant,
                    string LPCode, string DockCd, string Station, string ArrivalLP, string DepartureLP, string PickupDate, string OrderNo)
        {
            string Result = "";
            string errorMessage = "";
            string created_by = AuthorizedUser.Username;
            DateTime created_dt = DateTime.Now;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            try 
            {
                #region Validate Data

                //---=== Check Route
                if (Route.Trim() == "")
                {
                    errorMessage = " Route can't be empty";
                }
                else if (Route.Trim().Length > 4)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Route length maximum 4 characters");
                }

                //---=== Check Trip / Day
                if (TripDay.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Trip / Day can't be empty");
                }
                else if (int.Parse(TripDay.Trim()) <= 0)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Trip / Day must numeric and greater than 0");
                }

                //---=== Check Trip No
                if (TripNo.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : " Trip No can't be empty");
                }
                else if (int.Parse(TripNo.Trim()) <= 0)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Trip No must numeric and greater than 0");
                }

                //---=== Check Logistic Partner
                if (LPCode.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Logistic Partner can't be empty");
                }
                else if (getLogisticPartnerExist(LPCode.Trim()) == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Logistic Partner is not member of master logistic partner");
                }

                //---=== Check Pick Up Date
                if (PickupDate.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Pick Up Date can't be empty");
                }
                else if (!CheckDate(formatingShortDate(PickupDate.Trim())))
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Pick Up Date invalid format");
                }

                //---=== Check Log. Point Code
                if (LogPointCode.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Log Point Code can't be empty");
                }
                else if (LogPointCode.Trim().Length > 4)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Log Point Code length maximum 4 characters");
                }

                //---=== Check Log. Plant Code
                if (LogPointPlant.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Log Plant Code can't be empty");
                }
                else if (LogPointPlant.Trim().Length > 1)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Log Plant Code length maximum 1 characters");
                }

                //---=== Check Dock
                if (DockCd.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Dock can't be empty");
                } 
                else if (DockCd.Trim().Length > 2)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Dock length maximum 2 characters");
                }

                //---=== Check Station
                if (Station.Trim().Length > 3)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Dock length maximum 3 characters");
                }

                //---=== Check Arrival Logistic Point
                if (ArrivalLP.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Arrival Logistic Point can't be empty");
                }
                else if (!CheckDate(formatingShortDateTime(ArrivalLP.Trim(), "S")))
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Arrival Logistic Point Date invalid format");
                }

                //---=== Check Departure Logistic Point
                if (DepartureLP.Trim() == "")
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Departure Logistic Point can't be empty");
                }
                else if (!CheckDate(formatingShortDateTime(DepartureLP.Trim(), "S")))
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Departure Logistic Point Date invalid format");
                }

                //---=== Check Order No
                if (OrderNo.Trim().Length > 10)
                {
                    errorMessage = ((errorMessage != "") ? errorMessage : "Order No length maximum 10 characters");
                }

                #endregion

                if (errorMessage == "")
                {
                    db.Execute("InsertDeliveryNonTLMSData", new object[] { 
                            Route.Trim(),
                            int.Parse(TripDay.Trim()),
                            int.Parse(TripNo.Trim()),
                            LPCode.Trim(),
                            formatingShortDate(PickupDate.Trim()),
                            LogPointCode.Trim(),
                            LogPointPlant.Trim(),
                            DockCd.Trim(),
                            Station.Trim(),
                            formatingShortDateTime(ArrivalLP.Trim(), "S"),
                            formatingShortDateTime(DepartureLP.Trim(), "S"),
                            OrderNo.Trim(), created_by, created_dt
                        });
                    Result = "Success|save";
                }
                else
                {
                    Result = "Fail|" + errorMessage;
                }

            }
            catch(Exception ex) 
            {
                Result = "Fail|Failed to save, logical error.";
            }

            db.Close();
            return Content(Result);
        }

        public ContentResult DeleteOrderScreen(string GridId, string GridSelection, string IsPublish)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            char[] splitChar = { ';' };
            char[] SplitLastChar = { ',' };
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string result = "Process Succes";
            //int isPublish = 0;

            ParamUpdate = GridId.Split(splitChar);
            //if (GridSelection == "All")
            //{
            //    ParamUpdate = getListAllSelection(IsPublish).Split(splitChar);
            //}

            try
            {
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                    db.Execute("DeleteDeliveryNonTLMSData", new object[] { 
                            ParamUpdateId[1].ToString(), ParamUpdateId[3].ToString(), formattingFullToShortDate(ParamUpdateId[5].ToString())
                        });
                }
            }
            catch (Exception ex)
            {
                result = "Error, failed to delete list.";
            }

            db.Close();
            return Content(result);
        }

        public ContentResult PublishOrderScreen(string GridId, string GridSelection)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            char[] splitChar = { ';' };
            char[] SplitLastChar = { ',' };
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string result = "";
            string IsPublish = "0";

            ParamUpdate = GridId.Split(splitChar);
            //if (GridSelection == "All"){
            //    string test = getListAllSelection(IsPublish);
            //    GridId = getListAllSelection(IsPublish);
            //    ParamUpdate = getListAllSelection(IsPublish).Split(splitChar);
            //}

            try
            {
                List<DeliveryNonTLMSNormalisasi> QueryLogPublish = db.Fetch<DeliveryNonTLMSNormalisasi>("DoNormalisasiNonTLMS", new object[] { GridId, AuthorizedUser.Username });
                for (int i = 0; i < QueryLogPublish.Count; i++)
                {
                    result = QueryLogPublish[i].Result.ToString();
                }

                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                    db.Execute("PublishDeliveryNonTLMSData", new object[] { 
                            ParamUpdateId[1].ToString(), ParamUpdateId[2].ToString(), ParamUpdateId[3].ToString(), ParamUpdateId[4].ToString(),
                            formattingFullToShortDate(ParamUpdateId[5].ToString()), ParamUpdateId[6].ToString(), ParamUpdateId[7].ToString(), ParamUpdateId[8].ToString(),
                            ParamUpdateId[12].ToString()
                        });
                }
            }
            catch (Exception ex)
            {
                result = "Failed to publish list.";
            }

            db.Close();
            return Content(result);
        }

        public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/DeliveryNonTLMS/UploadTemp/NonTLMSData_Template.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "NonTLMSData_Template.xls");
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        #endregion

        #region Upload Area

        #region UPLOAD CONFIRMATION - PROPERTIES
        private string _ErrorMessage = "";

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/DeliveryNonTLMS/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private long _processId = 0;
        private long ProcessId
        {
            get
            {
                if (_processId == 0)
                {
                    IDBContext db = DbContext;

                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); }

                    return _processId;
                }

                return _processId;
            }
        }

        private int errorCount = 0;
        #endregion

        [HttpPost]
        public void UplDeliveryNonTLMSOrder_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("UplDeliveryNonTLMSOrder", ValidationSettings, UplLPOPConfirmation_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls", ".xlsx" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        [HttpPost]
        public ActionResult HeaderDeliveryNonTLMS_Upload(string filePath)
        {
            ProcessAllDeliveryNonTLMSUpload(filePath);

            String status = ((_ErrorMessage == "") ? "SUCCESS|Upload file success." : _ErrorMessage);

            return Json(
                new
                {
                    Status = status
                });
        }

        private DeliveryNonTLMSModel ProcessAllDeliveryNonTLMSUpload(string filePath)
        {
            string created_by = AuthorizedUser.Username;
            DateTime created_dt = DateTime.Now;
            _ErrorMessage = "";

            DeliveryNonTLMSModel _UploadedDeliveryNonTLMSModel = new DeliveryNonTLMSModel();

            //---=== Table destination properties
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "DeliveryNonTLMSData";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "13";

            LogProvider logProvider = new LogProvider("102", "121", AuthorizedUser);
            ILogSession sessionNTLMS = logProvider.CreateSession();
            OleDbConnection excelConn = null;
            IDBContext db = DbContext;
            
            try
            {
                //sessionNTLMS.Write("MPCS00001INF", new object[] { "Process Upload has been started..!!!" });

                //---=== Clear temporary upload table.
                DeliveryNonTLMSModel model = new DeliveryNonTLMSModel();
                model.TempGridContent = model.TempGridContent.ToList<DeliveryNonTLMSTempGridContent>();
                Model.AddModel(model);

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                excelCommand = new OleDbCommand(@"select * from [" + _TableDestination.sheetName + "$]", excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                string errorMessage = "";
                int lastLine = 0;
                string pickupDate = "";
                string logArrival = "";
                string logDeparture = "";
                string[] logPartner;
                int nLog = 0;
                int n;
                
                for (int i = 0; i < dtPatterns.Rows.Count; i++)
                {

                    bool isNumeric = int.TryParse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim(), out n);
                    errorMessage = "";

                    #region Validate Data

                    //---=== Check Route
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Route"]).Trim() == "")
                    {
                        errorMessage = "Data " + (i + 1) + " : route can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["Route"]).Trim().Length > 4)
                    {
                        errorMessage = "Data " + (i + 1) + " : route length maximum 4 characters";
                    }

                    //---=== Check Trip / Day
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : trip / Day can't be empty";
                    }
                    //else if (int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim()) <= 0)
                    else if (!isNumeric)
                    {

                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : trip / Day must numeric and greater than 0";
                    }

                    //---=== Check Trip No
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Trip No"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : trip No can't be empty";
                    }
                    else if (int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip No"]).Trim()) <= 0)
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : trip No must numeric and greater than 0";
                    }

                    //---=== Check Logistic Partner
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : logistic Partner can't be empty";
                    }
                    else if (getLogisticPartnerExist(Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim()) == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : logistic Partner is not member of master logistic partner";
                    }

                    //---=== Check Pick Up Date
                    pickupDate = formatingShortDate(Convert.ToString(ds.Tables[0].Rows[i]["Pick Up Date"]).Trim());
                    if (pickupDate == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : pick Up Date can't be empty";
                        pickupDate = "";
                    }
                    else if (!CheckDate(pickupDate))
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : pick Up Date invalid format";
                        pickupDate = "";
                    }

                    //---=== Check Log. Point Code
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Log Point Code"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : log Point Code can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["Log Point Code"]).Trim().Length > 4)
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : log Point Code length maximum 4 characters";
                    }

                    //---=== Check Log. Plant Code
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Log Plant Code"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : log Plant Code can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["Log Plant Code"]).Trim().Length > 1)
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : log Plant Code length maximum 1 characters";
                    }

                    //---=== Check Dock
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Dock"]).Trim() == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : dock Code can't be empty";
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["Dock"]).Trim().Length > 3)
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : dock length maximum 2 characters";
                    }

                    //---=== Check Station
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Station"]).Trim().Length > 3)
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : station length maximum 3 characters";
                    }

                    //---=== Check Arrival Logistic Point
                    logArrival = formatingShortDateTime(Convert.ToString(ds.Tables[0].Rows[i]["Arrival Logistic Point"]).Trim(), "U");
                    if (logArrival == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : arrival Logistic Point can't be empty";
                        logArrival = "";
                    }
                    else if (!CheckDate(logArrival))
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : arrival Logistic Point Date invalid format";
                        logArrival = "";
                    }

                    //---=== Check Log. Departure
                    logDeparture = formatingShortDateTime(Convert.ToString(ds.Tables[0].Rows[i]["Departure Logistic Point"]).Trim(), "U");
                    if (logDeparture == "")
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : departure Logistic Point can't be empty";
                        logDeparture = "";
                    }
                    else if (!CheckDate(logDeparture))
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : departure Logistic Point Date invalid format";
                        logDeparture = "";
                    }

                    //---=== Check Order No
                    if (Convert.ToString(ds.Tables[0].Rows[i]["Order No"]).Trim().Length > 10)
                    {
                        errorMessage = " uploaded excel contain Wrong inputData at row " + (i + 1) + " : order No length maximum 10 characters";
                    }

                    //---=== Check logistic partner must not be same in same route - rate
                    //if (logPartner.Length == 0)
                    //{
                    //    logPartner[nLog] = Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim();
                    //}
                    //else
                    //{
                    //    for (int vLogLoop = 0; vLogLoop < logPartner.Length; vLogLoop++)
                    //    { 
                            
                    //    }
                    //}
                    //if (logPartner == "")
                    //{
                    //    if ((Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim() != "") || (getLogisticPartnerExist(Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim()) != ""))
                    //    {
                    //        logPartner = Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim();
                    //    }
                    //}
                    //else
                    //{
                    //    if ((Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim() != "") || (getLogisticPartnerExist(Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim()) != ""))
                    //    {
                    //        if (logPartner != Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim())
                    //        {
                    //            errorMessage = "Cannot upload multiple Logistic Partner";
                    //        }
                    //    }
                    //}

                    #endregion

                    if (errorMessage != "")
                    {
                        errorCount = errorCount + 1;
                        lastLine = i;
                        if (_ErrorMessage == "")
                        {
                            _ErrorMessage = "ERROR|"+errorMessage;
                        }
                    }

                    #region Insert to Model Table

                    //Model.GetModel<DeliveryNonTLMSModel>().TempGridContent.Add(new DeliveryNonTLMSTempGridContent()
                    //{
                    //    Route = Convert.ToString(ds.Tables[0].Rows[i]["Route"]).Trim(),
                    //    TripDay = int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim()),
                    //    TripNo = int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip No"]).Trim()),
                    //    LPCode = Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim(),
                    //    PickUpDate = ((pickupDate == "") ? (DateTime?)null : DateTime.Parse(formatingShortDate(Convert.ToString(ds.Tables[0].Rows[i]["Pick Up Date"]).Trim()))),
                    //    LogPointCode = Convert.ToString(ds.Tables[0].Rows[i]["Log. Point Code"]).Trim(),
                    //    LogPlantCode = Convert.ToString(ds.Tables[0].Rows[i]["Log. Plant Code"]).Trim(),
                    //    LogPointName = "",
                    //    DockCode = Convert.ToString(ds.Tables[0].Rows[i]["Dock"]).Trim(),
                    //    ArrivalLogisticPoint = ((logArrival == "") ? (DateTime?)null : DateTime.Parse(formatingShortDateTime(Convert.ToString(ds.Tables[0].Rows[i]["Arrival Logistic Point"]).Trim()))),
                    //    DepartureLogPoint = ((logDeparture == "") ? (DateTime?)null : DateTime.Parse(formatingShortDateTime(Convert.ToString(ds.Tables[0].Rows[i]["Departure Logistic Point"]).Trim()))),
                    //    OrderNo = Convert.ToString(ds.Tables[0].Rows[i]["Order No"]).Trim(),
                    //    ErrorMessage = errorMessage
                    //});
                    //Model.AddModel(Model.GetModel<DeliveryNonTLMSModel>());
                    #endregion

                }

                #endregion

                #region Insert Into Table 
                if (errorCount == 0)
                {
                    for (int i = 0; i < dtPatterns.Rows.Count; i++)
                    {
                        if (getNonTLMSExist(
                                Convert.ToString(ds.Tables[0].Rows[i]["Route"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["Trip No"]).Trim(),
                                formatingShortDate(Convert.ToString(ds.Tables[0].Rows[i]["Pick Up Date"]).Trim()),
                                Convert.ToString(ds.Tables[0].Rows[i]["Log Point Code"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["Log Plant Code"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["Dock"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["Order No"]).Trim()) == "0")
                        {
                            db.Execute("InsertDeliveryNonTLMSData", new object[] { 
                                Convert.ToString(ds.Tables[0].Rows[i]["Route"]).Trim(),
                                int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip / Day"]).Trim()),
                                int.Parse(Convert.ToString(ds.Tables[0].Rows[i]["Trip No"]).Trim()),
                                Convert.ToString(ds.Tables[0].Rows[i]["Logistic Partner"]).Trim(),
                                formatingShortDate(Convert.ToString(ds.Tables[0].Rows[i]["Pick Up Date"]).Trim()),
                                Convert.ToString(ds.Tables[0].Rows[i]["Log Point Code"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["Log Plant Code"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["Dock"]).Trim(),
                                Convert.ToString(ds.Tables[0].Rows[i]["Station"]).Trim(),
                                formatingShortDateTime(Convert.ToString(ds.Tables[0].Rows[i]["Arrival Logistic Point"]).Trim(), "U"),
                                formatingShortDateTime(Convert.ToString(ds.Tables[0].Rows[i]["Departure Logistic Point"]).Trim(), "U"),
                                Convert.ToString(ds.Tables[0].Rows[i]["Order No"]).Trim(), created_by, created_dt
                            });
                        }
                        else
                        { 
                            //---=== Rollback last insert
                            _ErrorMessage = "ERROR|Cannot duplicate entry data.";
                            //---=== End rollback last insert
                        }
                    }
                }
                #endregion
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                //sessionNTLMS.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });
                _ErrorMessage = "ERROR|" + exc.ToString();
            }
            finally
            {
                //sessionNTLMS.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                //sessionNTLMS.Commit();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }
            db.Close();
            return _UploadedDeliveryNonTLMSModel;
        }

        protected void UplLPOPConfirmation_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();

            IDBContext db = DbContext;
            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            db.Close();

            return String.Join(",", columnNameArray.ToArray());
        }
        #endregion

        #region Additional Function

        protected bool CheckDate(String date)
        {

            try
            {

                DateTime dt = DateTime.Parse(date);

                return true;
            }

            catch
            {

                return false;

            }

        }

        private string getListAllSelection(string IsPublish)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            
            var QueryAdditonalDelivery = db.Query<DeliveryNonTLMSData>("getGridDeliveryNonTLMS", new object[] {
                "--", "--", "", "", IsPublish
            });

            int i = 0;

            foreach (var item in QueryAdditonalDelivery)
            {
                result = result + ((result == "") ? "" : ";") +
                         i.ToString() + "," +
                         item.Route + "," +
                         item.TRIP_PER_DAY + "," +
                         item.TRIP_NO + "," +
                         item.LP_CD + "," +
                         DateTime.Parse(item.PICKUP_DT.ToString()).ToString("ddd MMM dd yyyy HH:mm:ss") + " GMT+0700 (SE Asia Standard Time)," +
                         item.LOGISTIC_POINT_CD + "," +
                         item.LOGISTIC_PLANT_CD + "," +
                         item.DOCK_CD + "," +
                         item.STATION + "," +
                         DateTime.Parse(item.ARRIVAL_LOG_POINT_TIME.ToString()).ToString("ddd MMM dd yyyy HH:mm:ss") + " GMT+0700 (SE Asia Standard Time)," +
                         DateTime.Parse(item.DEPARTURE_LOG_POINT_TIME.ToString()).ToString("ddd MMM dd yyyy HH:mm:ss") + " GMT+0700 (SE Asia Standard Time)," +
                         item.ORDER_NO;
                i = i + 1;
            }

            db.Close();
            return result;
        }

        private string getNonTLMSExist(string Route, string TripNo, string PickupDate, string LogPointCode,
                                        string LogPlantCode, string DockCode, string OrderNo)
        {
            int result = 0;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (Route != "" && TripNo != "" && PickupDate != "" && LogPointCode != "" && LogPlantCode != "" && 
                DockCode != "" && OrderNo != "")
            {
                DeliveryNonTLMSCheckExists QueryLog = db.SingleOrDefault<DeliveryNonTLMSCheckExists>("GetDeliveryNonTLMSDataCheck", new object[] { 
                    Route, TripNo, PickupDate, LogPointCode, LogPlantCode, DockCode, OrderNo
                });
                result = QueryLog.resultCount;
            }
            db.Close();
            return result.ToString();
        }

        private string getLogisticPartnerExist(string LogisticPartnerCode)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (LogisticPartnerCode != "")
            {
                LogisticPartner QueryLog = db.SingleOrDefault<LogisticPartner>("GetLogisticPartnerNamebyCode", new object[] { LogisticPartnerCode });
                result = QueryLog.LPCode;
            }
            db.Close();
            return result;
        }

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                if (extractString.Length >= 3)
                    result = extractString[2] + "/" + extractString[1] + "/" + extractString[0];
            }

            return result;
        }

        private string formatingShortDateTime(string dateString, string type)
        {
            string result = "";
            string[] extract = new string[2];
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extract = dateString.Split(' ');
                if (extract.Length > 0)
                {
                    extractString = extract[0].Split('.');
                    if (extractString.Length >= 3)
                        result = extractString[2] + "/" + extractString[1] + "/" + extractString[0] + " " + extract[1] + ((type == "S") ? "" : ":00");
                }
            }

            return result;
        }

        private string formattingFullToShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];
            //Sat May 04 2013 00:00:00 GMT+0700 (SE Asia Standard Time)
            extractString = dateString.Split(' ');
            result = extractString[3] + "-" + getMonthNumber(extractString[1]) + "-" + extractString[2];

            return result;
        }

        private string getMonthNumber(string month)
        {
            string result = "";

            switch (month)
            {
                case "January":
                case "Jan": result = "01"; break;
                case "February":
                case "Feb": result = "02"; break;
                case "March":
                case "Mar": result = "03"; break;
                case "April":
                case "Apr": result = "04"; break;
                case "May": result = "05"; break;
                case "June":
                case "Jun": result = "06"; break;
                case "July":
                case "Jul": result = "07"; break;
                case "August":
                case "Aug": result = "08"; break;
                case "September":
                case "Sep": result = "09"; break;
                case "October":
                case "Oct": result = "10"; break;
                case "November":
                case "Nov": result = "11"; break;
                case "December":
                case "Dec": result = "12"; break;
                default: result = "01"; break;
            }

            return result;
        }
        
        #endregion

        #region UPLOAD CONFIRMATION - RESULT
        //[HttpGet]
        //public void DownloadInvalid(String processId, String functionId, String temporaryTableUpload)
        //{

        //    string fileName = "LPOPConfirmationUploadInvalid" + ".xls";

        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    IEnumerable<ReportLPOPConfirmationUploadError> qry = db.Query<ReportLPOPConfirmationUploadError>("ReportGetLPOPUploadError", new object[] { functionId, processId });

        //    IExcelWriter exporter = ExcelWriter.GetInstance();
        //    byte[] hasil = exporter.Write(qry, "LPOPConfirmationData");

        //    db.Close();
        //    Response.Clear();
        //    Response.Cache.SetCacheability(HttpCacheability.Private);
        //    Response.Expires = -1;
        //    Response.Buffer = true;

        //    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
        //    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

        //    Response.BinaryWrite(hasil);
        //    Response.End();
        //}
        #endregion

    }
}