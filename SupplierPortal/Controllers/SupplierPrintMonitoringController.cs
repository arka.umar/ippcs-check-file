﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;

using Portal.Models.SupplierPrintMonitoring;
using Portal.Models.Dock;

namespace Portal.Controllers
{
    public class SupplierPrintMonitoringController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public SupplierPrintMonitoringController()
             : base("Supplier Print Monitoring")
         {
             PageLayout.UseMessageBoard = true;
         }

        #region Model properties
        List<DockData> _supplierprintDockModel = null;
        private List<DockData> _SupplierPrintDockModel
        {
            get
            {
                if (_supplierprintDockModel == null)
                    _supplierprintDockModel = new List<DockData>();

                return _supplierprintDockModel;
            }
            set
            {
                _supplierprintDockModel = value;
            }
        }

        #endregion
        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;
            SupplierPrintMonitoringModel mdl = new SupplierPrintMonitoringModel();
            Model.AddModel(mdl);

            ViewData["OrderReleaseDate"] = DateTime.Now.ToString("dd.MM.yyyy");
        }

        #region Order Monitoring controller
        #region View controller
        public ActionResult SupplierPrintMonitoringHeaderPartial()
        {
            #region Required model in partial page : for OrderInquiryHeaderPartial
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            _SupplierPrintDockModel = DockCodes;

            Model.AddModel(_SupplierPrintDockModel);
            #endregion

            return PartialView("SupplierPrintMonitoringHeaderPartial", Model);
        }
        public ActionResult SupplierPrintMonitoringDetailPartial()
        {
            #region Required model in partial page : for SupplierPrintMonitoringDetailPartial
            SupplierPrintMonitoringModel model = Model.GetModel<SupplierPrintMonitoringModel>();
            model.SupplierPrintMonitorings = GetAllSupplierPrintMonitoring(String.IsNullOrEmpty(Request.Params["OrderReleaseDate"]) == true ? DateTime.Now.ToString("dd.MM.yyyy") : Request.Params["OrderReleaseDate"], Request.Params["DockCode"]);
            #endregion

            return PartialView("SupplierPrintMonitoringDetailPartial", Model);
        }
        public ActionResult PartialHeaderDockLookupGridSupplierPrintMonitoring()
        {
            #region Required model in partial page : for OIHDockOption GridLookup
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            TempData["GridName"] = "OSPMDockOption";
            _SupplierPrintDockModel = DockCodes;
            //_OrderInquiryDockModel = GetAllDock(); ;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierPrintDockModel);
        }
        
        #endregion

        #region Database controller
        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            IDBContext db = DbContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();
            return orderInquiryDockModel;
        }
        private List<SupplierPrintMonitoring> GetAllSupplierPrintMonitoring(String OrderReleaseDate, String DockCode)
        {
            string DockCodeLDAP = "";
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }
            
            List<SupplierPrintMonitoring> supplierprintMonitoringModel = new List<SupplierPrintMonitoring>();
            IDBContext db = DbContext;

            supplierprintMonitoringModel = db.Fetch<SupplierPrintMonitoring>("GetAllSupplierPrintMonitoring", new object[] { 
                (OrderReleaseDate == "" || OrderReleaseDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDate,"dd.MM.yyyy",null),
                DockCode,
                DockCodeLDAP
            }).ToList();
            db.Close();

            return supplierprintMonitoringModel;
        }
        #endregion
        #endregion
    }
}
