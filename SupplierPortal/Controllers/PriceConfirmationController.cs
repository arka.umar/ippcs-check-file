﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models;
using Portal.Models.Globals;
using Portal.Models.PriceConfirmation;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;

namespace Portal.Controllers
{
    public class PriceConfirmationController : BaseController
    {
        private readonly string ModuleId = "2";
        private readonly string FunctionId = "25124";
        private readonly string ScreenId = "PriceConfirmation";
        private IDBContext _db = null;
        private IDBContext DB
        {
            get
            {
                if (_db == null)
                {
                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (_db != null)
            {
                _db.Close();
                _db = null;
            }
            base.OnResultExecuted(filterContext);
        }

        public PriceConfirmationController()
            : base("Price Confirmation")
        {
        }

        protected override void StartUp()
        {
            Model.AddModel(new PriceConfirmationModel() { Detail = new PriceConfirmation() { }, Rows = new List<PriceConfirmation>() });
            var cl = CategoryList();
            ViewData["CategoryList"] = cl.Select(a => new[] { a.Val, a.Text }).AsEnumerable().ToArray().AsJson();
            ViewData["CoverLetter"] = ParamsHelper.EncodeJs(CoverLetter());
            cl.Insert(0, new ComboData() { Val = "", Text = "" }); 
            ViewData["CategoryCombo"] = cl;
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<SupplierICS> _supplierModel = null;
        private List<SupplierICS> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierICS>();
                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }
        [Obsolete]
        protected override void Init()
        {
            // Model.AddModel(new PriceConfirmationModel() { Detail = new PriceConfirmation() { }, Rows = new List<PriceConfirmation>() });
        }

        public string Messages(string msgid)
        {
            return Sing.Me.Msg.Delimited(msgid);                
        }

        [HttpGet]
        public void DownloadSheet(string SupplierCode, string DockCode, string PartNo)
        {
            string fileName = ScreenId + DateTime.Now + ".xls";
            string SupplierCodeLDAP = "";
            var u = Model.GetModel<User>();
            if (u == null)
                return;
            List<SupplierICS> suppliers = u.FilteringArea<SupplierICS>(GetAllSupplierICSBuyerMapped(),
                "SUPP_CD", ScreenId, "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPP_CD).Distinct().ToList<string>();
            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            IEnumerable<PriceConfirmation> listGeneral = DB.Query<PriceConfirmation>("GetPriceConfirmation",
                new object[] { SupplierCode });
            IExcelWriter exporter = ExcelWriter.GetInstance();
            exporter.Append(listGeneral, ScreenId);
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();
        }

        public List<ComboData> CategoryList()
        {
            List<ComboData> r ;
            r = DB.Fetch<ComboData>("GetSysValRemarkCombo", new object[] { new { FunctionId = "010204", SystemCd = "Category" } });
             
            Sing.Me.Set("CategoryList", r.AsJson());
            
            return r;
        }

        public string CoverLetter()
        {
            string r = "category:{Category} effective date:{EffectiveDate}";
            r = Sing.Me.Get<string>("PCCoverLetter");
            if (string.IsNullOrEmpty(r))
            {
                r = DB.SingleOrDefault<string>("GetSystemValue", new object[] { "010204", "COVER_LETTER" });
                Sing.Me.Set("PCCoverLetter", r); 
            }
            return r; 
        }

        public ActionResult PartialHeader()
        {
            Model.AddModel(new PriceConfirmationModel() { Detail = new PriceConfirmation() { }, Rows = new List<PriceConfirmation>() });
            LookupPart<SupplierICS>(ScreenId, "OIHSupplierOption", "GridSupplier", "SUPP_CD", GetAllSupplierICSBuyerMapped, null);
            return PartialView("PartialHeader");
        }

        public ActionResult PartialDetail()
        {
            string SupplierCodeLDAP = "";
            var u = Model.GetModel<User>();
            if (u == null) return null;
            List<SupplierICS> suppliers = u.FilteringArea<SupplierICS>(
                GetAllSupplierICSBuyerMapped(), "SUPP_CD", ScreenId, "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPP_CD).Distinct().ToList<string>();
            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            PriceConfirmationModel m = new PriceConfirmationModel() { Detail = new PriceConfirmation() { }, Rows = new List<PriceConfirmation>() };
            PriceConfirmationParam prm = new PriceConfirmationParam();
            prm  = Request.Params.AsObject<PriceConfirmationParam>();
            try
            {
                m.Rows = Get(prm);
            }
            catch (Exception ex)
            {
                m.Message = ex.Message;
                ex.PutLog("PriceConfirmation", UserID, "Get");
            }
            Model.AddModel(m);

            return PartialView("PartialDetail", Model);
        }

        protected List<PriceConfirmation> Get(PriceConfirmationParam p)
        {
            List<PriceConfirmation> listn = new List<PriceConfirmation>();
            User u = Model.GetModel<User>();

            p.Validate(1); 

            p.PCDateFrom = p.PCDateFrom.ToSQLDate();
            p.PCDateTo = p.PCDateTo.ToSQLDate();
            p.EffectiveDate = ("01." + p.EffectiveDate).ToSQLDate();
            p.Noreg = u.NoReg;

            listn = DB.Query<PriceConfirmation>("GetPriceConfirmation", new object[] { p }).ToList();
            return listn;
        }

        public ActionResult PartialHeaderSupplierLookup()
        {
            return LookupPart<SupplierICS>(ScreenId,
                    "OIHSupplierOption",
                    "GridSupplier",
                    "SUPP_CD",
                    GetAllSupplierICSBuyerMapped);
        }

        public ActionResult PartialSupplierLookupGrid()
        {
            return LookupPart<SupplierICS>(
                ScreenId,
                "InSupplierLookup",
                "GridSupplier",
                "SUPP_CD",
                GetAllSupplierICSBuyerMapped);
        }
        
        private List<SupplierICS> GetAllSupplierICSBuyerMapped()
        {
            User u = Model.GetModel<User>();
            string NOREG = u.NoReg;
            string SUPP_CD = null;
            if (u != null && u.Username != null && u.Username.IndexOf('.') > 0)
            {
                string[] ur = u.Username.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (ur.Length > 0)
                    SUPP_CD = ur[0];
            }
            List<SupplierICS> supplierModel = new List<SupplierICS>();
            try
            {
                supplierModel = DB.Fetch<SupplierICS>("GetAllSupplierICSBuyerMapped", new object[] { new { NOREG, SUPP_CD } });
            }
            catch (Exception exc)
            {
                exc.PutLog("", u.Username, ScreenId +".GetAllSupplier");
                throw;
            }

            return supplierModel;
        }

        public ActionResult PopupNew()
        {
            List<ComboData> l = CategoryList();
            l.Insert(0, new ComboData() { Val="" , Text= ""});
            ViewData["CategoryCombo"] =  l;
            LookupPart<SupplierICS>(ScreenId,
                   "OIHSupplierOption",
                   "GridSupplier",
                   "SUPP_CD",
                   GetAllSupplierICSBuyerMapped, null);
            return PartialView("PopupNew", null);
        }
        public string NextPCNo(string PCDate, int run = 0)
        {
            DateTime pcDate = PCDate.ToDateTime();
            string s = pcDate.ToString("yyyy-MM-dd");
            if (DateTime.Compare(pcDate, new DateTime(1900, 1, 1)) <= 0)
                return Sing.Me.Msg.Text("MPCS00016ERR", "PC Date");
            PCDate = s;
            return DB.Query<string>("GetNextPCNo", new object[] { new {PCDate, Run = run}}).FirstOrDefault(); 
        }

        public string Save(
            string PCNo,
            string PCDate,
            string Category,
            string SupplierCode,
            string EffectiveDate,
            string CoverLetterBody, 
            string PrintFlag)
        {
            User u= Model.GetModel<User>();
            if (u == null)
            {
                return null;
            }
            if (string.IsNullOrEmpty(PCNo))
                PCNo = NextPCNo(PCDate, 1);

            PriceConfirmation p = new PriceConfirmation()
            {
                PCNo = PCNo,
                PCDate = PCDate.ToDateTime(),
                Category = Category,
                SupplierCode = SupplierCode,
                EffectiveDate = EffectiveDate.ToDateTime(),
                CoverLetterBody = CoverLetterBody,
                PrintFlag = PrintFlag, 
                ReleaseStatus= "N",
                CreatedBy = u.Username, 
                ChangedBy = u.Username 
            };
            string User = AuthorizedUser.Username;
            string ResultQuery = "SUCCESS";
            try
            {
                ResultQuery = DB.ExecuteScalar<string>("SavePriceConfirmation", new object[] { p });
                TempData["GridName"] = "PartialDetail";
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
                ex.PutLog("", Model.GetModel<User>().Username, "Save");
            }
            return ResultQuery; 
        }

      
        public string Delete(string PCNo)
        {
            string ResultQuery = "Process succesfully";
            try
            {
                DB.Execute("DeletePriceConfirmation", new object[]
                {
                   new { PCNo }
                });
                TempData["GridName"] = "GridDetail";
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
                ex.PutLog("", Model.GetModel<User>().Username, "Delete");
            }
            return ResultQuery;
        }

        public string Submit(string PCNos)
        {
            User u = Model.GetModel<User>();
            string Usrnm = (u != null) ? u.Username : "";
            if (string.IsNullOrEmpty(PCNos))
                return "PC No List is empty";
            
            string validation = DB.SingleOrDefaultStatement<string>("exec pcs_ValidatePCSubmit @0", new object[] { PCNos}); 
            if (!string.IsNullOrEmpty(validation))
                return validation;
            
            string submission = DB.SingleOrDefaultStatement<string>("exec pcs_SubmitPC @0, @1", new object[] { PCNos, Usrnm });
            if (!string.IsNullOrEmpty(submission))
            {
                return submission;
            }
            return "SUCCESS";
        }


        public ActionResult Detail(string PCNo)
        {
            TempData["PCNo"] = PCNo; 
            return RedirectToAction("Index", "PCDetail");
        }
    }
}
