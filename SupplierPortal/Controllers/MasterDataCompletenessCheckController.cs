﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.MasterDataCompletenessCheck;
using Toyota.Common.Web.Database;
using Portal.Models.Dock;

namespace Portal.Controllers
{
    public class MasterDataCompletenessCheckController : BaseController
    {
        private string[] compMonth;
        private string prodMonth;
        private MasterDataCompletenessCheckModel model;
        private  string currentProductionMonth;        
        private IDBContext db;

        private IDBContext DBContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<MDCompletenessCheckData> _getListMode = null;
        private List<MDCompletenessCheckData> getListModel
        {
            get
            {
                if (_getListMode == null)
                    _getListMode = new List<MDCompletenessCheckData>();

                return _getListMode;
            }
            set
            {
                _getListMode = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;        
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }

        public MasterDataCompletenessCheckController()
            : base("Master Data Completeness Check")
        { }

        protected override void StartUp()
        {
        }

        public ActionResult HandleForm(
                string MMActivityOption,
                string MMProdMonthDate,
                string MMMatDescText,
                string MMDockOption,
                string MMCheckTimingDate,
                string MMLastRunDate,
                string MMMatNoText
            )
        {
            //TempData["MMProdMonthDisplay"] = MMProdMonthDate;
            //string[] mmProdMonth = MMProdMonthDate.Split('.').ToArray();
            //string mmProdMonthDate =  mmProdMonth[1] + mmProdMonth[0];

            TempData["MMActivity"] = MMActivityOption;
            TempData["MMProdMonth"] = MMProdMonthDate;
            TempData["MMMatDesc"] = MMMatDescText;
            TempData["MMDock"] = MMDockOption;
            TempData["MMCheckTiming"] = MMCheckTimingDate;
            TempData["MMLastRun"] = MMLastRunDate;
            TempData["MMMatNo"] = MMMatNoText;
            
            model = new MasterDataCompletenessCheckModel();
            model.MDCompletenessCheckDatas = GetDataCompletenessCheck(  
                MMActivityOption,
                MMMatNoText,                
                MMProdMonthDate,
                MMMatDescText,               
                MMCheckTimingDate,
                MMLastRunDate,
                MMDockOption
                );
            Model.AddModel(model);
            TempData["MDCompData"] = model;

            return Redirect("/MasterDataCompletenessCheck/");
            
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            db = DBContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return orderInquiryDockModel;
        }

        private List<MDCompletenessCheckData> GetDataCompletenessCheck(
            string mmActivityOption,
            string mmMatNoText,
            string mmProdMonthDate,
            string mmMatDescText,
            string mmCheckTimingDate,
            string mmLastRunDate,
            string mmDockOption)
        {
            if (!string.IsNullOrEmpty(mmProdMonthDate))
            {
                compMonth = mmProdMonthDate.Split('.').ToArray();
                prodMonth = compMonth[1] + compMonth[0];
            }

            db = DBContext;
            if (!string.IsNullOrEmpty(mmActivityOption))
            {
                if (mmActivityOption.Equals("LPOP"))
                {
                    getListModel = db.Fetch<MDCompletenessCheckData>("GetDefLPOPMatDatCompCheck", new object[]{
                        prodMonth == null? currentProductionMonth : prodMonth,
                        mmMatNoText == null? "" : mmMatNoText,
                        mmMatDescText == null? "" : mmMatDescText,
                        mmDockOption == null? "" : mmDockOption
                    });
                }
                if (mmActivityOption.Equals("GR"))
                {
                    getListModel = db.Fetch<MDCompletenessCheckData>("GetDefGRMatDatCompCheck", new object[]{
                        prodMonth == null? prodMonth : currentProductionMonth,
                        mmMatNoText == null? "" : mmMatNoText,
                        mmMatDescText == null? "" : mmMatDescText,
                        mmDockOption == null? "" : mmDockOption
                    });
                }

            }
            else
            {
                getListModel = db.Fetch<MDCompletenessCheckData>("GetDefLPOPMatDatCompCheck", new object[]{
                        currentProductionMonth, "", "", ""
                    });
            }
            db.Close();
            return getListModel;
        }

        protected override void Init()
        {
            DateTime currentMonth = DateTime.Now;
            currentProductionMonth = Convert.ToString(currentMonth.Year) + String.Format("{0:D2}", (currentMonth.Month));
        }

        public ActionResult PartialHeaderCOmpletenessCheck()
        {
            _OrderInquiryDockModel = GetAllDock();
            Model.AddModel(_OrderInquiryDockModel);
            return PartialView("MasterDataCompletenessCheckHeader", Model);
        }

        public ActionResult PartialMasterDataCompletenessDataGridView()
        {
            if (TempData["MDCompData"] != null)
            {
                string mmActivityOption = TempData["MMActivity"] as string;
                string mmMatNoText = TempData["MMMatNo"] as string;
                string mmLastRunDate = TempData["MMLastRun"] as string;
                string mmProdMonthDate = TempData["MMProdMonth"] as string;
                string mmMatDescText = TempData["MMMatDesc"] as string;
                string mmCheckTimingDate = TempData["MMCheckTiming"] as string;
                string mmDockOption = TempData["MMDock"] as string;

                model = new MasterDataCompletenessCheckModel();
                model.MDCompletenessCheckDatas = GetDataCompletenessCheck(
                    mmActivityOption,
                    mmMatNoText,
                    mmProdMonthDate,
                    mmMatDescText,                    
                    mmCheckTimingDate,
                    mmLastRunDate,
                    mmDockOption);
                Model.AddModel(model);               

                return PartialView("MasterDataCompletenessCheckData", Model);                   
            }
            else
            {
                model = new MasterDataCompletenessCheckModel();
                model.MDCompletenessCheckDatas = GetDataCompletenessCheck(
                    Request.Params["_MMActivityOption"],
                    Request.Params["_MMMatNoText"],
                    Request.Params["_MMProdMonthDate"],
                    Request.Params["_MMMatDescText"],
                    Request.Params["_MMCheckTimingDate"],
                    Request.Params["_MMLastRunDate"],  
                    Request.Params["_MMDockOption"]);
                Model.AddModel(model);

                return PartialView("MasterDataCompletenessCheckData", Model);
            }            
        }        

        public ActionResult MasterDataCompletenessGrid()
        {
            return PartialView("MasterDataCompletenessCheckGridVw", Model);
        }

        public ActionResult PartialPopUpCompletenessCheck()
        {
            return PartialView("popupCompletenessCheck", Model);
        }

        public ActionResult PartialPopUpMatNoDetails()
        {
            return PartialView("popupMaterialNoDetails", Model);
        }

        public ActionResult PartialMMDockOption()
        {
            TempData["GridName"] = "MMDockOption";
            _OrderInquiryDockModel = GetAllDock();           

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }
    }
}
