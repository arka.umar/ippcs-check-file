﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.ChatDownload;

namespace Portal.Controllers
{
    public class ChatDownloadController : BaseController
    {
        public ChatDownloadController()
            : base("Chat Download Screen")
        { }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            ChatDownloadModel model = new ChatDownloadModel();
            for (int i = 0; i < 21; i++)
            {
                model.ChatDatas.Add(new ChatData()
                    {
                        ChatId = i + 1,
                        ProcessType = "ProcessType",
                        UserId = "PCD_User" + (i + 1),
                        CommentTo = "Supplier" + (i + 1),
                        CommentCc = "Supplier" + (i + 3),
                        Participant = "PCD_User" + (i + 2),
                        CommentContent = "View",
                        StartDt = DateTime.Now,
                        EndDt = DateTime.Now,

                    });
            }
            Model.AddModel(model);
        }

        public ActionResult SubmitterPartial()
        {
            return PartialView("SubmitterPartial", Model);
        }

        public ActionResult ToPartial()
        {
            return PartialView("ToPartial", Model);
        }

        public ActionResult CcPartial()
        {
            return PartialView("CcPartial", Model);
        }

    }
}
