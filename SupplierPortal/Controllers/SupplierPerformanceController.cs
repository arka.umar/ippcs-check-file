﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Threading;
using Portal.Models.Globals;
using Portal.Models.SupplierPerformance;
using Toyota.Common.Web;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Util.Configuration;
using DevExpress.Web.Mvc;
using System.Globalization;
using Toyota.Common.Web.Excel;
using System.IO;
using Portal.ExcelUpload;
using System.Collections;
using Toyota.Common.Web.Log;
using System.Data.OleDb;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using Telerik.Reporting.Charting;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;



namespace Portal.Controllers
{
    public class SupplierPerformanceController : BaseController
    {
        public SupplierPerformanceController()
            : base("Supplier Performance")
        {

        }


        protected override void StartUp()
        {
            //set production month with this month value
            DateTime CurrentMonth = DateTime.Now;
            TempData["ThisMonth"] = CurrentMonth.ToString("yyyy MMMM");

            //set gridlookup Supplier
            TempData["SupplierCode"] = SupplierCode;
            Model.AddModel(Suppliers);

            //var QuerySupplier = db.Query<SupplierICS>("GetAllSupplierICS");
            //ViewData["SupplierCode"] = QuerySupplier;


            //set gridlookup receiving area or dock
            var QueryDock = db.Query<Plant>("GetAllPlant");
            ViewData["DockCode"] = QueryDock;


        }


        protected override void Init()
        {
            ///set supplier combobox in gridview
            GridLookupSupplier();
            YearMonth();
            //add riani (20221019)
            Year();
            List<SysMaster> Sysmaster = MasterKPI();
            //--//
            //add by alira.agi 2015-04-09 add to model plan and dock master
            List<RcvArea> RCVAREAs = db.Fetch<RcvArea>("getReceivingArea");

            List<TMMINModel> models = db.Fetch<TMMINModel>("getTMMINModel");
            Model.AddModel(Sysmaster);
            Model.AddModel(models);
            Model.AddModel(RCVAREAs);
        }

        private void YearMonth()
        {
            DateTime d = DateTime.Now;
            List<SPMonthYear> ListAdd = new List<SPMonthYear>();

            int i = 0;
            while (i <= 7)
            {
                ListAdd.Add(new SPMonthYear()
                {
                    MonthYear = new DateTime(d.Year, d.Month, 1).AddMonths(i - 2).AddDays(-1).ToString("yyyyMM"),
                });

                i++;
            }

            Model.AddModel(ListAdd);
        }
        //add riani (20221019)
        private void Year()
        {
            DateTime d = DateTime.Now;
            List<SPYear> ListAdd = new List<SPYear>();
            List<SPYear> ListAddDistinct = new List<SPYear>();
            int i = 0;
            while (i <= 7)
            {
                string yearValue = new DateTime(d.Year, d.Month, 1).AddMonths(i - 2).AddDays(-1).ToString("yyyy");
                if (i != 0)
                {
                    if (ListAdd.Where(t => t.Year == yearValue).Count() < 1)
                    {
                        ListAdd.Add(new SPYear()
                        {
                            Year = new DateTime(d.Year, d.Month, 1).AddMonths(i - 2).AddDays(-1).ToString("yyyy"),
                        });
                    }
                }
                else
                {
                    ListAdd.Add(new SPYear()
                    {
                        Year = new DateTime(d.Year, d.Month, 1).AddMonths(i - 2).AddDays(-1).ToString("yyyy"),
                    });
                }
                i++;
            }
            ListAddDistinct = ListAdd;
            Model.AddModel(ListAdd);
        }
        //-------------------//
        #region DATABASE CONNECTION
        private IDBContext _db = null;

        private IDBContext db
        {
            get
            {
                if (_db == null)
                {

                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (_db != null)
            {
                _db.Close();
                _db = null;
            }

            base.OnResultExecuted(filterContext);
        }
        #endregion


        #region GRID LOOKUP PARTIAL

        #region GRID LOOKUP SUPPLIER
        protected List<SupplierICS> Suppliers
        {
            get
            {
                List<SupplierICS> x = Model.GetModel<List<SupplierICS>>();
                if (x == null)
                {
                    x = Model.GetModel<User>().FilteringArea<SupplierICS>(
                        GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");
                }
                return x;
            }
        }

        private List<SupplierICS> GetAllSupplier()
        {
            List<SupplierICS> ListSupplier = new List<SupplierICS>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ListSupplier = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });

            return ListSupplier;
        }

        public ActionResult GridLookupSupplier()
        {
            List<SupplierICS> ListSupplier = Model.GetModel<User>().FilteringArea<SupplierICS>(
                    GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");
            TempData["GridName"] = "grlSupplier";
            Model.AddModel(ListSupplier);

            return PartialView("GridLookup/PartialGrid", ListSupplier);
        }
        #endregion

        #region GRID LOOKUP DOCK
        public ActionResult DockCodePartial()
        {
            TempData["GridName"] = "grlDock";
            var QueryDock = db.Query<Plant>("GetAllPlant");
            ViewData["DockCode"] = QueryDock;
            return PartialView("GridLookup/PartialGrid", ViewData["DockCode"]);
        }
        #endregion

        #region DDL KPI 
        //add riani (20221020)
        protected List<SysMaster> MasterKPI()
        {

            List<SysMaster> x = Model.GetModel<List<SysMaster>>();
            if (x == null)
            {
                x = db.Query<SysMaster>("GetSPMasterKPI").ToList();
            }
            return x;
        }
        #endregion
        #endregion


        #region RELOAD GRID DETAIL

        string SupplierCode = String.Empty;
        string ProdMonth = String.Empty;
        string ProdMonthTo = String.Empty;
        string DockCode = String.Empty;

        #region GRID QUALITY
        List<SPQuality> ListSPQuality_ = null;
        private List<SPQuality> ListSPQuality
        {
            get
            {
                if (ListSPQuality_ == null)
                    ListSPQuality_ = new List<SPQuality>();

                return ListSPQuality_;
            }
            set
            {
                ListSPQuality_ = value;
            }
        }

        protected List<SPQuality> GetAllQualityDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPQuality> AllDetailDeta = new List<SPQuality>();

            AllDetailDeta = db.Fetch<SPQuality>("GetAllSPQuality", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59", DockCode });
            return AllDetailDeta;
        }

        public ContentResult GetAuthorization()
        {
            //SupplierCode = Request.Params["SupplierCode"];


            int auth = Model.GetModel<Toyota.Common.Web.Credential.User>().IsAuthorized("SupplierPerformance", "BtnAdd");

            int suppliercd = 0;
            bool isSupplierCd = Int32.TryParse(AuthorizedUser.Username.Substring(0, 4), out suppliercd);

            if (isSupplierCd)
            {
                //If user is Supplier
                return Content("1");
                //If User is Admin or TMMIN User, return auth
            }

            return Content(auth.ToString());
        }

        public ActionResult ReloadGridQualityDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];
            DockCode = Request.Params["DockCode"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPQuality = GetAllQualityDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPQuality);

            return PartialView("SPDetailQuality", Model);
        }
        #endregion

        #region GRID Quality Non Manufacturing
        //add riani (20221007)
        List<SPQuality> ListSPQualityNonManufacturing_ = null;
        private List<SPQuality> ListSPQualityNonManufacturing
        {
            get
            {
                if (ListSPQualityNonManufacturing_ == null)
                    ListSPQualityNonManufacturing_ = new List<SPQuality>();

                return ListSPQualityNonManufacturing_;
            }
            set
            {
                ListSPQualityNonManufacturing_ = value;
            }
        }

        protected List<SPQuality> GetAllQualityNonManufacturingDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPQuality> AllDetailDeta = new List<SPQuality>();

            AllDetailDeta = db.Fetch<SPQuality>("GetAllSPNonManufacturingQuality", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59", DockCode });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridQualityNonManufacturingDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];
            DockCode = Request.Params["DockCode"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPQualityNonManufacturing = GetAllQualityNonManufacturingDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPQualityNonManufacturing);

            return PartialView("SPDetailQualityNonManufacturing", Model);
        }
        //-----------------------------------------------//
        #endregion

        #region GRID DELIVERY
        List<SPDelivery> ListSPDelivery_ = null;
        private List<SPDelivery> ListSPDelivery
        {
            get
            {
                if (ListSPDelivery_ == null)
                    ListSPDelivery_ = new List<SPDelivery>();

                return ListSPDelivery_;
            }
            set
            {
                ListSPDelivery_ = value;
            }
        }

        protected List<SPDelivery> GetAllDeliveryDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPDelivery> AllDetailDeta = new List<SPDelivery>();

            AllDetailDeta = db.Fetch<SPDelivery>("GetAllSPDelivery", new Object[] { SupplierCode, ProdMonth, ProdMonthTo, DockCode });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridDeliveryDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];
            DockCode = Request.Params["DockCode"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPDelivery = GetAllDeliveryDetail(
             String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy


            Model.AddModel(ListSPDelivery);

            return PartialView("SPDetailDelivery", Model);
        }

        #endregion

        #region GRID SAFETY HEADER
        List<SPSafety> ListSPSafety_ = null;
        private List<SPSafety> ListSPSafety
        {
            get
            {
                if (ListSPSafety_ == null)
                    ListSPSafety_ = new List<SPSafety>();

                return ListSPSafety_;
            }
            set
            {
                ListSPSafety_ = value;
            }
        }

        protected List<SPSafety> GetAllSafetyDetail(string SupplierCode, string ProdMonth, string ProdMonthTo)
        {
            List<SPSafety> AllDetailDeta = new List<SPSafety>();

            AllDetailDeta = db.Fetch<SPSafety>("GetAllSPSafety", new Object[] { SupplierCode, ProdMonth, ProdMonthTo });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridSPSafety()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            // modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }

            ListSPSafety = GetAllSafetyDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo);
            Model.AddModel(ListSPSafety);
            //end of modified by fid.goldy

            return PartialView("SPDetailSafety", Model);
        }

        #endregion

        #region GRID SAFETY POPUP
        List<SPSafetyPopup> ListSPSafetyPopup_ = null;
        private List<SPSafetyPopup> ListSPSafetyPopup
        {
            get
            {
                if (ListSPSafetyPopup_ == null)
                    ListSPSafetyPopup_ = new List<SPSafetyPopup>();

                return ListSPSafetyPopup_;
            }
            set
            {
                ListSPSafetyPopup_ = value;
            }
        }

        protected List<SPSafetyPopup> GetAllSafetyPopup(string SupplierCode, string ProdMonth, string ProdMonthTo)
        {
            List<SPSafetyPopup> AllDetailDeta = new List<SPSafetyPopup>();

            AllDetailDeta = db.Fetch<SPSafetyPopup>("GetAllSPSafetyPopup", new Object[] { SupplierCode, ProdMonth, ProdMonthTo });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridSPSafetyPopup()
        {
            string ProductionMonth = Request.Params["ProdMonth"];
            int intYear = Convert.ToInt16(ProductionMonth.Substring(0, 4));
            int intMonth = Convert.ToInt16(ProductionMonth.Substring(ProductionMonth.Length - 2, 2));

            SupplierCode = Request.Params["SupplierCode"];

            DateTime dtProdMonth = new DateTime(intYear, intMonth, 1);
            DateTime dtProdMonthTo = dtProdMonth.AddMonths(1).AddDays(-1);

            ProdMonth = dtProdMonth.ToString("yyyy-MM-dd");
            ProdMonthTo = dtProdMonthTo.ToString("yyyy-MM-dd") + " 23:59:59";

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPSafetyPopup = GetAllSafetyPopup(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
                , ProdMonth, ProdMonthTo);
            Model.AddModel(ListSPSafetyPopup);
            //end of modified by fid.goldy

            return PartialView("PopupSPSafetyGrid", Model);
        }

        #endregion

        #region GRID TWS
        List<SPTWS> ListSPTWS_ = null;
        private List<SPTWS> ListSPTWS
        {
            get
            {
                if (ListSPTWS_ == null)
                    ListSPTWS_ = new List<SPTWS>();

                return ListSPTWS_;
            }
            set
            {
                ListSPTWS_ = value;
            }
        }

        protected List<SPTWS> GetAllTWSDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPTWS> AllDetailDeta = new List<SPTWS>();

            AllDetailDeta = db.Fetch<SPTWS>("GetAllSPTWS", new Object[] { SupplierCode, ProdMonth, ProdMonthTo, DockCode });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridTWSDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];
            DockCode = Request.Params["DockCode"];


            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPTWS = GetAllTWSDetail(
             String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
                , ProdMonth, ProdMonthTo, DockCode);
            Model.AddModel(ListSPTWS);
            //end of modified by fid.goldy

            return PartialView("SPDetailTWS", Model);
        }

        #endregion

        #region GRID SPD
        List<SPSPD> ListSPSPD_ = null;
        private List<SPSPD> ListSPSPD
        {
            get
            {
                if (ListSPSPD_ == null)
                    ListSPSPD_ = new List<SPSPD>();

                return ListSPSPD_;
            }
            set
            {
                ListSPSPD_ = value;
            }
        }

        protected List<SPSPD> GetAllSPDDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPSPD> AllDetailDeta = new List<SPSPD>();

            AllDetailDeta = db.Fetch<SPSPD>("GetAllSPSPD", new Object[] { SupplierCode, ProdMonth, ProdMonthTo, DockCode });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridSPDDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];
            DockCode = Request.Params["DockCode"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPSPD = GetAllSPDDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
                , ProdMonth, ProdMonthTo, DockCode);
            Model.AddModel(ListSPSPD);
            //end of modified by fid.goldy

            return PartialView("SPDetailSPD", Model);
        }

        #endregion

        #region GRID PART RECEIVE
        List<SPReceive> ListSPReceive_ = null;
        private List<SPReceive> ListSPReceive
        {
            get
            {
                if (ListSPReceive_ == null)
                    ListSPReceive_ = new List<SPReceive>();

                return ListSPReceive_;
            }
            set
            {
                ListSPReceive_ = value;
            }
        }

        protected List<SPReceive> GetAllReceiveDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPReceive> AllDetailData = new List<SPReceive>();

            AllDetailData = db.Fetch<SPReceive>("GetAllSPReceive", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59", DockCode });
            return AllDetailData;
        }

        protected List<SPReceiveReportTMMINPivot> GetAllReceiveDetailReportTMMINPivot(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPReceiveReportTMMINPivot> AllDetailData = new List<SPReceiveReportTMMINPivot>();

            AllDetailData = db.Fetch<SPReceiveReportTMMINPivot>("GetAllSPReceiveReportTMMINPivot", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59", DockCode });
            return AllDetailData;
        }

        protected List<SPReceiveReportTMMIN> GetAllReceiveDetailReportTMMIN(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPReceiveReportTMMIN> AllDetailData = new List<SPReceiveReportTMMIN>();

            AllDetailData = db.Fetch<SPReceiveReportTMMIN>("GetAllSPReceiveReportTMMIN", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59", DockCode });
            return AllDetailData;
        }

        protected List<string> GetPartReceivedColumnName()
        {
            List<string> rcv_area = db.Fetch<string>("GetPartReceivedColumnName");
            return rcv_area;
        }

        public ActionResult ReloadGridReceiveDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];
            DockCode = Request.Params["DockCode"];

            // modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPReceive = GetAllReceiveDetail(
              String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
                , ProdMonth, ProdMonthTo, DockCode);
            Model.AddModel(ListSPReceive);
            //end of modified by fid.goldy
            return PartialView("SPDetailReceive", Model);
        }
        #endregion

        #region GRID WORK ABILITY

        List<SPWorkAbility> ListSPWorkAbility_ = null;
        private List<SPWorkAbility> ListSPWorkAbility
        {
            get
            {
                if (ListSPWorkAbility_ == null)
                    ListSPWorkAbility_ = new List<SPWorkAbility>();

                return ListSPWorkAbility_;
            }
            set
            {
                ListSPWorkAbility_ = value;
            }
        }

        protected List<SPWorkAbility> GetAllWorkAbilityDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPWorkAbility> AllDetailDeta = new List<SPWorkAbility>();

            AllDetailDeta = db.Fetch<SPWorkAbility>("GetAllSPWorkAbility", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59", DockCode });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridWorkAbilityDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];
            DockCode = Request.Params["DockCode"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPWorkAbility = GetAllWorkAbilityDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPWorkAbility);

            return PartialView("SPDetailWorkAbility", Model);
        }
        #endregion



        //new shortage
        #region GRID SHORTAGE

        List<SPShortage> ListSPShortage_ = null;
        private List<SPShortage> ListSPShortage
        {
            get
            {
                if (ListSPShortage_ == null)
                    ListSPShortage_ = new List<SPShortage>();

                return ListSPShortage_;
            }
            set
            {
                ListSPShortage_ = value;
            }
        }

        protected List<SPShortage> GetAllShortageDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPShortage> AllDetailDeta = new List<SPShortage>();

            AllDetailDeta = db.Fetch<SPShortage>("GetAllSPShortage", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59"});
            return AllDetailDeta;
        }

        public ActionResult ReloadGridShortageDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPShortage = GetAllShortageDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPShortage);

            return PartialView("SPDetailShortage", Model);
        }
        #endregion

        //end of new shortage

        #region GRID SHORTAGE NON MANUFACTURING
        //add riani (20221010)
        List<SPShortage> ListSPShortageNonManufacturing_ = null;
        private List<SPShortage> ListSPShortageNonManufacturing
        {
            get
            {
                if (ListSPShortageNonManufacturing_ == null)
                    ListSPShortageNonManufacturing_ = new List<SPShortage>();

                return ListSPShortageNonManufacturing_;
            }
            set
            {
                ListSPShortageNonManufacturing_ = value;
            }
        }

        //add riani (20221009)
        protected List<SPShortage> GetAllShortageNonManufacturingDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPShortage> AllDetailDeta = new List<SPShortage>();

            AllDetailDeta = db.Fetch<SPShortage>("GetAllSPShortageNonManufacturing", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }
        //--------------------//
        public ActionResult ReloadGridShortageNonManufacturingDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPShortage = GetAllShortageNonManufacturingDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPShortage);

            return PartialView("SPDetailShortageNonManufacturing", Model);
        }
        #endregion

        //end of new shortage






        //new cripple
        #region GRID cripple

        List<SPCripple> ListSPCripple_ = null;
        private List<SPCripple> ListSPCripple
        {
            get
            {
                if (ListSPCripple_ == null)
                    ListSPCripple_ = new List<SPCripple>();

                return ListSPCripple_;
            }
            set
            {
                ListSPCripple_ = value;
            }
        }

        protected List<SPCripple> GetAllCrippleDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPCripple> AllDetailDeta = new List<SPCripple>();

            AllDetailDeta = db.Fetch<SPCripple>("GetAllSPCripple", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridCrippleDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPCripple = GetAllCrippleDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPCripple);

            return PartialView("SPDetailCripple", Model);
        }
        #endregion

        //end of new cripple




        //new comment
        #region GRID comment

        List<SPComment> ListSPComment_ = null;
        private List<SPComment> ListSPComment
        {
            get
            {
                if (ListSPComment_ == null)
                    ListSPComment_ = new List<SPComment>();

                return ListSPComment_;
            }
            set
            {
                ListSPComment_ = value;
            }
        }

        protected List<SPComment> GetAllCommentDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPComment> AllDetailDeta = new List<SPComment>();

            AllDetailDeta = db.Fetch<SPComment>("GetAllSPComment", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridCommentDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPComment = GetAllCommentDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPComment);

            return PartialView("SPDetailComment", Model);
        }
        #endregion

        //end of new cripple

        //new misspart
        #region GRID misspart

        List<SPMisspart> ListSPMisspart_ = null;
        private List<SPMisspart> ListSPMisspart
        {
            get
            {
                if (ListSPMisspart_ == null)
                    ListSPMisspart_ = new List<SPMisspart>();

                return ListSPMisspart_;
            }
            set
            {
                ListSPMisspart_ = value;
            }
        }

        protected List<SPMisspart> GetAllMisspartDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPMisspart> AllDetailDeta = new List<SPMisspart>();

            AllDetailDeta = db.Fetch<SPMisspart>("GetAllSPMisspart", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridMisspartDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPMisspart = GetAllMisspartDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPMisspart);

            return PartialView("SPDetailMisspart", Model);
        }
        #endregion

        //end of new misspart

        //new misspart
        #region GRID misspart Non Manufacturing
        //add riani (20221010)
        List<SPMisspart> ListSPMisspartNonManufacturing_ = null;
        private List<SPMisspart> ListSPMisspartNonManufacturing
        {
            get
            {
                if (ListSPMisspartNonManufacturing_ == null)
                    ListSPMisspartNonManufacturing_ = new List<SPMisspart>();

                return ListSPMisspartNonManufacturing_;
            }
            set
            {
                ListSPMisspartNonManufacturing_ = value;
            }
        }

        //add riani (20221009)
        protected List<SPMisspart> GetAllMisspartNonManufacturingDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPMisspart> AllDetailDeta = new List<SPMisspart>();

            AllDetailDeta = db.Fetch<SPMisspart>("GetAllSPMisspartNonManufacturing", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }
        //-------------------//        

        public ActionResult ReloadGridMisspartNonManufacturingDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPMisspart = GetAllMisspartNonManufacturingDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPMisspart);

            return PartialView("SPDetailMisspartNonManufacturing", Model);
        }
        #endregion

        //end of new misspart





        //new linestop
        #region GRID linestop

        List<SPLineStop> ListSPLineStop_ = null;
        private List<SPLineStop> ListSPLineStop
        {
            get
            {
                if (ListSPLineStop_ == null)
                    ListSPLineStop_ = new List<SPLineStop>();

                return ListSPLineStop_;
            }
            set
            {
                ListSPLineStop_ = value;
            }
        }

        protected List<SPLineStop> GetAllLineStopDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPLineStop> AllDetailDeta = new List<SPLineStop>();

            AllDetailDeta = db.Fetch<SPLineStop>("GetAllSPLineStop", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridLineStopDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPLineStop= GetAllLineStopDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPLineStop);

            return PartialView("SPDetailLineStop", Model);
        }
        #endregion

        //end of new linestop







        //new Ontime
        #region GRID Ontime

        List<SPOntime> ListSPOntime_ = null;
        private List<SPOntime> ListSPOntime
        {
            get
            {
                if (ListSPOntime_ == null)
                    ListSPOntime_ = new List<SPOntime>();

                return ListSPOntime_;
            }
            set
            {
                ListSPOntime_ = value;
            }
        }

        protected List<SPOntime> GetAllOntimeDetail(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPOntime> AllDetailDeta = new List<SPOntime>();

            AllDetailDeta = db.Fetch<SPOntime>("GetAllSPOntime", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }

        public ActionResult ReloadGridOntimeDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPOntime = GetAllOntimeDetail(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPOntime);

            return PartialView("SPDetailOntime", Model);
        }
        #endregion

        //end of new linestop

        //new environment
        #region GRID environment
        //add riani (20221010)
        List<SPKPI> ListSPKPI_ = null;
        private List<SPKPI> ListSPKPI
        {
            get
            {
                if (ListSPKPI_ == null)
                    ListSPKPI_ = new List<SPKPI>();

                return ListSPKPI_;
            }
            set
            {
                ListSPKPI_ = value;
            }
        }
        //add riani (20221018)
        protected List<SPKPI> GetAllKPI(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPKPI> AllDetailDeta = new List<SPKPI>();

            AllDetailDeta = db.Fetch<SPKPI>("GetAllSPKPI", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }
        //--------------------//        
        public ActionResult ReloadGridKPIDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");
            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPKPI = GetAllKPI(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPKPI);

            return PartialView("SPDetailKPI", Model);
        }
        #endregion

        //end of new environment

        #region GRID KPI
        //add riani (20221020)
        List<SPEnvironment> ListSPEnvironment_ = null;
        private List<SPEnvironment> ListSPEnvironment
        {
            get
            {
                if (ListSPEnvironment_ == null)
                    ListSPEnvironment_ = new List<SPEnvironment>();

                return ListSPEnvironment_;
            }
            set
            {
                ListSPEnvironment_ = value;
            }
        }
        //add riani (20221010)
        List<SPEnvironmentMaster> ListSPEnvironmentMaster_ = null;
        private List<SPEnvironmentMaster> ListSPEnvironmentMaster
        {
            get
            {
                if (ListSPEnvironmentMaster_ == null)
                    ListSPEnvironmentMaster_ = new List<SPEnvironmentMaster>();

                return ListSPEnvironmentMaster_;
            }
            set
            {
                ListSPEnvironmentMaster_ = value;
            }
        }
        //add riani (20221018)
        protected List<SPEnvironment> GetAllEnvironment(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPEnvironment> AllDetailDeta = new List<SPEnvironment>();

            AllDetailDeta = db.Fetch<SPEnvironment>("GetAllSPEnvironment", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }
        //--------------------//
        //add riani (20221018)
        protected List<SPEnvironmentMaster> GetAllEnvironmentMaster(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            List<SPEnvironmentMaster> AllDetailDeta = new List<SPEnvironmentMaster>();

            AllDetailDeta = db.Fetch<SPEnvironmentMaster>("GetAllSPEnvironmentMaster", new Object[] { SupplierCode, ProdMonth, ProdMonthTo + " 23:59:59" });
            return AllDetailDeta;
        }
        //--------------------//
        public ActionResult ReloadGridEnvironmentDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");
            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPEnvironment = GetAllEnvironment(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPEnvironment);

            return PartialView("SPDetailEnvironment", Model);
        }
        public ActionResult ReloadGridEnvironmentMaster()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];
            ProdMonthTo = Request.Params["ProdMonthTo"];

            //modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");
            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }


            ListSPEnvironmentMaster = GetAllEnvironmentMaster(
            String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
            , ProdMonth, ProdMonthTo, DockCode);
            //end of modified by fid.goldy
            Model.AddModel(ListSPEnvironmentMaster);

            return PartialView("SPMasterEnvironment", Model);
        }
        #endregion


        #endregion


        #region INSERT DATA

        #region INSERT ON MAIN GRID
        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
        //public ContentResult SaveData (int TabIndex, string ProdMonth, string SupplierCode, 
        //    string PartNo, string QtyDefect, string Problem, string DockCode,
        //    string SupplierMonth, string Delay, string Shortage, string Incorrect,
        //    string Ori, string Adj, string Receive, string Quality, string Workability,
        //    string Claim, string Cost, string TriY,
        //    string Fatal, string Disabilty, string LostOrgan, string Absent, string AbsentPeriode, string SmallInjured, string TotalWorkers, string Judgment,
        //    string QtyReceive,
        //    string DProblem,
        //    string QProblem,
        //    string WProblem            
        //    )
        public ContentResult SaveData(int TabIndex, string ProdMonth, string SupplierCode, string PartNo,
            string QtyDefect, string Problem, string DockCode,
            string SupplierMonth, string Delay, string Shortage, string Incorrect,
            string Ori, string Adj, string Receive, string Quality, string Workability,
            string Claim, string Cost, string TriY,
            string Fatal, string Disabilty, string LostOrgan, string Absent, string AbsentPeriode, string SmallInjured, string TotalWorkers, string Judgment,
            string QtyReceive,
            string DProblem,
            string QProblem,
            string WProblem,
            string SPQtyReceive,
            string NGQuality,
            string DescQuality,
            string Time1,
            string NGWorkAbility,
            string DescWorkAbility,
            string Time2,
            //add by alira.agi 2015-05-26
            string NGDelivery,
            string DescDelivery,
            string Time3,
            //for shortageqty
            string partName,
            string ShortageQty,
            string ModelCode,
            string MisspartQty,
            string MinLineStop,
            string Cripple,
            string DelayFreq,
            string SupplyPerMonth
            )
        {
            string QueryResult = string.Empty;
            string strYear = ProdMonth.Substring(0, 4);
            string strMonth = ProdMonth.Substring(4, ProdMonth.Length - 4);
            DateTime ProductionMonth = DateTime.Parse(strYear + "-" + strMonth + "-01");
            try
            {
                QueryResult = db.ExecuteScalar<string>("InsertSupplierPerformance", new object[] 
                {
                    TabIndex, ProductionMonth, SupplierCode, PartNo, QtyDefect, Problem.Trim(), DockCode.Trim(), AuthorizedUser.Username,
                    SupplierMonth, Delay, Shortage, Incorrect,
                    Ori, Adj, Receive, Quality, Workability,
                    Claim, Cost, TriY,
                    Fatal, Disabilty, LostOrgan, Absent, AbsentPeriode, SmallInjured, TotalWorkers, Judgment.Trim(),
                    QtyReceive,DProblem,QProblem,WProblem,
                    SPQtyReceive,NGQuality,DescQuality,Time1,NGWorkAbility,DescWorkAbility,Time2,
                    NGDelivery , DescDelivery , Time3, partName, ShortageQty, ModelCode, MisspartQty, MinLineStop, Cripple, DelayFreq, SupplyPerMonth
                });

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    QueryResult = "Error: " + "data already exist !!!";
                else
                    QueryResult = "Error: " + ex.Message;
            }

            return Content(QueryResult);
        }
        #endregion
        #region Save data non manufacturing
        //add riani (20221010)
        public ContentResult SaveDataNonManufacturing(int TabIndex, string ProdMonth, string SupplierCode, string PartNo,
            string QtyDefect, string Problem, string DockCode,
            string SupplierMonth, string Delay, string Shortage, string Incorrect,
            string Ori, string Adj, string Receive, string Quality, string Workability,
            string Claim, string Cost, string TriY,
            string Fatal, string Disabilty, string LostOrgan, string Absent, string AbsentPeriode, string SmallInjured, string TotalWorkers, string Judgment,
            string QtyReceive,
            string DProblem,
            string QProblem,
            string WProblem,
            string SPQtyReceive,
            string NGQuality,
            string DescQuality,
            string Time1,
            string NGWorkAbility,
            string DescWorkAbility,
            string Time2,
            //add by alira.agi 2015-05-26
            string NGDelivery,
            string DescDelivery,
            string Time3,
            //for shortageqty
            string partName,
            string ShortageQty,
            string ModelCode,
            string MisspartQty,
            string MinLineStop,
            string Cripple,
            string DelayFreq,
            string SupplyPerMonth,
            string CO2Reduction,
            string CO2Emission,
            string ProdYear,
            string Q1Safety,
            string Q2Safety,
            string Q3Safety,
            string Q4Safety,
            string TargetSafety,
            string AchievementSafety,
            string Q1Quality,
            string Q2Quality,
            string Q3Quality,
            string Q4Quality,
            string TargetQuality,
            string AchievementQuality,
            string Q1Delivery,
            string Q2Delivery,
            string Q3Delivery,
            string Q4Delivery,
            string TargetDelivery,
            string AchievementDelivery,
            string Q1Environment,
            string Q2Environment,
            string Q3Environment,
            string Q4Environment,
            string TargetEnvironment,
            string AchievementEnvironment
            )
        {
            string QueryResult = string.Empty;
            string strYear = "";
            string strMonth = "";
            if (ProdMonth != "" && ProdMonth != null)
            {
                strYear = ProdMonth.Substring(0, 4);
                strMonth = ProdMonth.Substring(4, ProdMonth.Length - 4);
            }
            DateTime ProductionMonth = DateTime.Now;
            if (strYear != "" && strMonth != "")
            {
                ProductionMonth = DateTime.Parse(strYear + "-" + strMonth + "-01");
            }
            try
            {
                QueryResult = db.ExecuteScalar<string>("InsertSupplierPerformanceNonManufacturing", new object[]
                {
                    TabIndex, ProductionMonth, SupplierCode, PartNo, QtyDefect, Problem.Trim(), DockCode.Trim(), AuthorizedUser.Username,
                    SupplierMonth, Delay, Shortage, Incorrect,
                    Ori, Adj, Receive, Quality, Workability,
                    Claim, Cost, TriY,
                    Fatal, Disabilty, LostOrgan, Absent, AbsentPeriode, SmallInjured, TotalWorkers, Judgment.Trim(),
                    QtyReceive,DProblem,QProblem,WProblem,
                    SPQtyReceive,NGQuality,DescQuality,Time1,NGWorkAbility,DescWorkAbility,Time2,
                    NGDelivery , DescDelivery , Time3, partName, ShortageQty, ModelCode, MisspartQty, MinLineStop, Cripple, DelayFreq, SupplyPerMonth,
                    CO2Reduction,CO2Emission,ProdYear,
                    Q1Safety,
                    Q2Safety,
                    Q3Safety,
                    Q4Safety,
                    TargetSafety,
                    AchievementSafety,
                    Q1Quality,
                    Q2Quality,
                    Q3Quality,
                    Q4Quality,
                    TargetQuality,
                    AchievementQuality,
                    Q1Delivery,
                    Q2Delivery,
                    Q3Delivery,
                    Q4Delivery,
                    TargetDelivery,
                    AchievementDelivery,
                    Q1Environment,
                    Q2Environment,
                    Q3Environment,
                    Q4Environment,
                    TargetEnvironment,
                    AchievementEnvironment
                });

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    QueryResult = "Error: " + "data already exist !!!";
                else
                    QueryResult = "Error: " + ex.Message;
            }

            return Content(QueryResult);
        }
        #endregion
        #region INSERT ON POPUP SAFETY
        public ContentResult EditMultiRowOnPopup(string ValuePopup)
        {
            string qResult = string.Empty;
            return Content(qResult);
        }

        public ContentResult EditSingleRowOnPopup(string ValuePopup)
        {
            string qResult = string.Empty;

            ///get key value by split from keyvalue on popup
            string[] strValuePopup = ValuePopup.Split(';');
            ProdMonth = strValuePopup[0] + "01";
            SupplierCode = strValuePopup[1];
            string Stop6 = strValuePopup[2];
            string[] strValueA = strValuePopup[3].Split('|');
            string[] strValueB = strValuePopup[4].Split('|');
            string[] strValueC = strValuePopup[5].Split('|');

            ///get value production month "from" and "to"
            DateTime pmFrom = DateTime.ParseExact(ProdMonth, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
            DateTime pmTo = new DateTime(pmFrom.Year, pmFrom.Month, 1).AddMonths(1).AddDays(-1);

            ///save data potensial hazard
            int A = 0;
            while (A <= 2)
            {
                string[] Value = strValueA[A].Split('_');
                if (Value.Length == 2)
                {
                    qResult = db.ExecuteScalar<string>("UpdateSupplierPerformancePopup",
                        new object[] { pmFrom, pmTo, SupplierCode, Stop6, 
                    Value[0], (A+1), Value[1], AuthorizedUser.Username});
                }

                A++;
            }

            ///save data counter permanent
            int B = 0;
            while (B <= 2)
            {
                string[] Value = strValueB[B].Split('_');
                if (Value.Length == 2)
                {
                    qResult = db.ExecuteScalar<string>("UpdateSupplierPerformancePopup",
                        new object[] { pmFrom, pmTo, SupplierCode, Stop6,
                            Value[0], (B+1) , Value[1], AuthorizedUser.Username});
                }

                B++;
            }

            ///save data counter temp
            int C = 0;
            while (C <= 2)
            {
                string[] Value = strValueC[C].Split('_');
                if (Value.Length == 2)
                {
                    qResult = db.ExecuteScalar<string>("UpdateSupplierPerformancePopup",
                        new object[] { pmFrom, pmTo, SupplierCode, Stop6,
                            Value[0], (C+1), Value[1], AuthorizedUser.Username});
                }

                C++;
            }

            return Content(qResult);
        }
        #endregion

        #endregion


        #region UPDATE DATA

        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
        //public ContentResult UpdateData(int TabIndex, string ProdMonth, string SupplierCode,
        //    string PartNo, string QtyDefect, string Problem, string DockCode,
        //    string SupplierMonth, string Delay, string Shortage, string Incorrect,
        //    string Ori, string Adj, string Receive, string Quality, string Workability,
        //    string Claim, string Cost, string TriY,
        //    string Fatal, string Disabilty, string LostOrgan, string Absent, string AbsentPeriode, string SmallInjured, string TotalWorkers, string Judgment,
        //    string QtyReceive,
        //    string DProblem,
        //    string QProblem,
        //    string WProblem            
        //    )
        public ContentResult UpdateData(int TabIndex, string ProdMonth, string SupplierCode, string PartNo,
            string QtyDefect, string Problem, string DockCode,
            string SupplierMonth, string Delay, string Shortage, string Incorrect,
            string Ori, string Adj, string Receive, string Quality, string Workability,
            string Claim, string Cost, string TriY,
            string Fatal, string Disabilty, string LostOrgan, string Absent, string AbsentPeriode, string SmallInjured, string TotalWorkers, string Judgment,
            string QtyReceive,
            string DProblem,
            string QProblem,
            string WProblem,
            string SPQtyReceive,
            string NGQuality,
            string DescQuality,
            string Time1,
            string NGWorkAbility,
            string DescWorkAbility,
            string Time2,
            //add by alira.agi 2015-05-26
            string NGDelivery,
            string DescDelivery,
            string Time3,
            string ShortageQty,
            string Misspart,
            string MinLineStop,
            string Cripple,
            string delayFreq,
            string supplyPerMonth,
            string partName,
            string modelCd
            )
        {
            string QueryResult = string.Empty;
            string[] ArrProdMonth = ProdMonth.Split(';');
            string[] ArrSupplierCode = SupplierCode.Split(';');
            //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
            string[] ArrPartNo = PartNo.Split(';');
            string[] ArrQtyDefect = QtyDefect.Split(';');
            string[] ArrProblem = Problem.Split(';');
            string[] ArrDockCode = DockCode.Split(';');
            string[] ArrSupplierMonth = SupplierMonth.Split(';');
            string[] ArrDelay = Delay.Split(';');
            string[] ArrShortage = Shortage.Split(';');
            string[] ArrIncorrect = Incorrect.Split(';');
            string[] ArrOri = Ori.Split(';');
            string[] ArrAdj = Adj.Split(';');
            string[] ArrReceive = Receive.Split(';');
            string[] ArrQuality = Quality.Split(';');
            string[] ArrWorkability = Workability.Split(';');
            string[] ArrClaim = Claim.Split(';');
            string[] ArrCost = Cost.Split(';');
            string[] ArrTriY = TriY.Split(';');
            string[] ArrFatal = Fatal.Split(';');
            string[] ArrDisabilty = Disabilty.Split(';');
            string[] ArrLostOrgan = LostOrgan.Split(';');
            string[] ArrAbsent = Absent.Split(';');
            string[] ArrAbsentPeriode = AbsentPeriode.Split(';');
            string[] ArrSmallInjured = SmallInjured.Split(';');
            string[] ArrTotalWorkers = TotalWorkers.Split(';');
            string[] ArrJudgment = Judgment.Split(';');
            string[] ArrQtyReceive = QtyReceive.Split(';');

            string[] ArrDProblem = DProblem.Split(';');
            string[] ArrQProblem = QProblem.Split(';');
            string[] ArrWProblem = WProblem.Split(';');

            string[] ArrSPQtyReceive = SPQtyReceive.Split(';');
            string[] ArrNGQuality = NGQuality.Split(';');
            string[] ArrDescQuality = DescQuality.Split(';');
            string[] ArrTime1 = Time1.Split(';');
            string[] ArrNGWorkAbility = NGWorkAbility.Split(';');
            string[] ArrDescWorkAbility = DescWorkAbility.Split(';');
            string[] ArrTime2 = Time2.Split(';');
            //add by alira.agi 2015-05-26
            string[] ArrNGDelivery = NGDelivery.Split(';');
            string[] ArrDescNGDelivery = DescDelivery.Split(';');
            string[] ArrTime3 = Time3.Split(';');
            
            int intYear;
            int intMonth;
            DateTime ProductionMonth;
            DateTime ProductionMonthTo;

            try
            {
                for (int i = 0; i < ArrProdMonth.Length; i++)
                {
                    ///get date from and date to
                    intYear = Convert.ToInt16(ArrProdMonth[i].Substring(0, 4));
                    intMonth = Convert.ToInt16(ArrProdMonth[i].Substring(4, ArrProdMonth[i].Length - 4));
                    ProductionMonth = new DateTime(intYear, intMonth, 1);
                    ProductionMonthTo = new DateTime(ProductionMonth.AddMonths(1).Year, ProductionMonth.AddMonths(1).Month, 1);
                    //add by alira.agi 2015-05-28
                    ProductionMonthTo = ProductionMonthTo.AddDays(-1);

                    QueryResult = db.ExecuteScalar<string>("UpdateSupplierPerformance", new object[] 
                    {
                        ///quality as main parameter value
                        TabIndex, ProductionMonth, ProductionMonthTo, ArrSupplierCode[i], 
                        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
                        ArrPartNo.Length > 1 ? ArrPartNo[i].Trim() : PartNo.Trim(),
                        ArrQtyDefect.Length > 1 ? ArrQtyDefect[i]: QtyDefect,
                        ArrProblem.Length > 1 ? ArrProblem[i].Trim() : Problem.Trim(),
                        ArrDockCode.Length > 1 ? ArrDockCode[i] : DockCode,
                        AuthorizedUser.Username,
                        ///delivery
                        ArrSupplierMonth.Length > 1 ? ArrSupplierMonth[i] : SupplierMonth,
                        ArrDelay.Length > 1 ? ArrDelay[i] : Delay,
                        ArrShortage.Length > 1 ? ArrShortage[i] : Shortage,
                        ArrIncorrect.Length > 1 ? ArrIncorrect[i] : Incorrect,
                        ///spd
                        ArrOri.Length > 1 ? ArrOri[i] : Ori,
                        ArrAdj.Length > 1 ? ArrAdj[i] : Adj,
                        ArrReceive.Length > 1 ? ArrReceive[i] : Receive,
                        ArrQuality.Length > 1 ? ArrQuality[i] : Quality,
                        ArrWorkability.Length > 1 ? ArrWorkability[i] : Workability,
                        ///pws
                        ArrClaim.Length > 1 ? ArrClaim[i] : Claim,
                        ArrCost.Length > 1 ? ArrCost[i] : Cost,
                        ArrTriY.Length > 1 ? ArrTriY[i] : TriY,
                        ///safety
                        ArrFatal.Length > 1 ? ArrFatal[i] : Fatal,
                        ArrDisabilty.Length > 1 ? ArrDisabilty[i] : Disabilty,
                        ArrLostOrgan.Length > 1 ? ArrLostOrgan[i] : LostOrgan,
                        ArrAbsent.Length > 1 ?  ArrAbsent[i] : Absent,
                        ArrAbsentPeriode.Length > 1 ? ArrSmallInjured[i] : AbsentPeriode,
                        ArrSmallInjured.Length > 1 ? ArrSmallInjured[i] : SmallInjured,
                        ArrTotalWorkers.Length > 1 ? ArrTotalWorkers[i] : TotalWorkers,
                        ArrJudgment.Length > 1 ? ArrJudgment[i].Trim() : Judgment.Trim(),
                        ArrQtyReceive.Length > 1 ? ArrQtyReceive[i].Trim() : QtyReceive.Trim(),

                        ArrDProblem.Length > 1 ? ArrDProblem[i].Trim() : DProblem.Trim(),
                            ArrQProblem.Length > 1 ? ArrQProblem[i].Trim() : QProblem.Trim(),
                                ArrWProblem.Length > 1 ? ArrWProblem[i].Trim() : WProblem.Trim(),
                        //service part quality
                        ArrSPQtyReceive.Length > 1 ? ArrSPQtyReceive[i] : SPQtyReceive,
                        ArrNGQuality.Length > 1 ? ArrNGQuality[i] : NGQuality,
                        ArrDescQuality.Length > 1 ? ArrDescQuality[i] : DescQuality,
                        ArrTime1.Length > 1 ? ArrTime1[i] : Time1,
                        ArrNGWorkAbility.Length > 1 ? ArrNGWorkAbility[i] : NGWorkAbility,
                        ArrDescWorkAbility.Length > 1 ? ArrDescWorkAbility[i] : DescWorkAbility,
                        ArrTime2.Length > 1 ? ArrTime2[i] : Time2,
                        //add by alira.agi 2015-05-26
                        ArrNGDelivery.Length > 1 ? ArrNGDelivery[i] : NGDelivery,
                        ArrDescNGDelivery.Length > 1 ? ArrDescNGDelivery[i] : DescDelivery,
                        ArrTime3.Length > 1 ? ArrTime3[i] : Time3,
                        ShortageQty,
                        Misspart,
                        MinLineStop,
                        Cripple,
                        delayFreq,
                        supplyPerMonth,
                        partName,
                        modelCd
                    });
                }
            }
            catch (Exception ex)
            {
                QueryResult = "Error: " + ex.Message;
            }

            return Content(QueryResult);
        }

        #endregion


        #region DELETE DATA
        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri	
        //Uncomented by FID.Reggy 17/09/2015, recomended by Niit.Michael
        public ContentResult DeleteData(int TabIndex, string ProdMonth, string SupplierCode, string PartNo, string ReceivingArea, string Problem)
        //public ContentResult DeleteData(int TabIndex, string ProdMonth, string SupplierCode, string ReceivingArea)
        {
            string QueryResult = string.Empty;
            string[] ArrProdMonth = ProdMonth.Split(';');
            string[] ArrSupplierCode = SupplierCode.Split(';');
            //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri	
            string[] ArrPartNo = PartNo.Split(';');
            string[] ArrReceivingArea = ReceivingArea.Split(';');

            string[] ArrProblem = Problem.Split(';');

            int intYear;
            int intMonth;
            DateTime ProductionMonth;
            DateTime ProductionMonthTo;

            try
            {
                for (int i = 0; i < ArrProdMonth.Length; i++)
                {
                    ///get date from and date to
                    intYear = Convert.ToInt16(ArrProdMonth[i].Substring(0, 4));
                    intMonth = Convert.ToInt16(ArrProdMonth[i].Substring(4, ArrProdMonth[i].Length - 4));
                    ProductionMonth = new DateTime(intYear, intMonth, 1);
                    ProductionMonthTo = new DateTime(ProductionMonth.AddMonths(1).Year, ProductionMonth.AddMonths(1).Month, 1);
                    //add by alira.agi 2015-05-1
                    ProductionMonthTo = ProductionMonthTo.AddDays(-1);
                    ///delete data
                    QueryResult = db.ExecuteScalar<string>("DeleteSupplierPerformance", new object[] 
                    {
                        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
                        TabIndex, ProductionMonth, ProductionMonthTo, ArrSupplierCode[i], ArrReceivingArea[i], ArrPartNo[i], ArrProblem[i]
                    });
                }
            }
            catch (Exception ex)
            {
                QueryResult = "Error: " + ex.Message;
            }


            return Content(QueryResult);
        }

        #endregion


        #region UPDATE DATA NON MANUFACTURING
        //add riani (20221010)

        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
        //public ContentResult UpdateData(int TabIndex, string ProdMonth, string SupplierCode,
        //    string PartNo, string QtyDefect, string Problem, string DockCode,
        //    string SupplierMonth, string Delay, string Shortage, string Incorrect,
        //    string Ori, string Adj, string Receive, string Quality, string Workability,
        //    string Claim, string Cost, string TriY,
        //    string Fatal, string Disabilty, string LostOrgan, string Absent, string AbsentPeriode, string SmallInjured, string TotalWorkers, string Judgment,
        //    string QtyReceive,
        //    string DProblem,
        //    string QProblem,
        //    string WProblem            
        //    )
        public ContentResult UpdateDataNonManufacturing(int TabIndex, string ProdMonth, string SupplierCode, string PartNo,
            string QtyDefect, string Problem, string DockCode,
            string SupplierMonth, string Delay, string Shortage, string Incorrect,
            string Ori, string Adj, string Receive, string Quality, string Workability,
            string Claim, string Cost, string TriY,
            string Fatal, string Disabilty, string LostOrgan, string Absent, string AbsentPeriode, string SmallInjured, string TotalWorkers, string Judgment,
            string QtyReceive,
            string DProblem,
            string QProblem,
            string WProblem,
            string SPQtyReceive,
            string NGQuality,
            string DescQuality,
            string Time1,
            string NGWorkAbility,
            string DescWorkAbility,
            string Time2,
            //add by alira.agi 2015-05-26
            string NGDelivery,
            string DescDelivery,
            string Time3,
            string ShortageQty,
            string Misspart,
            string MinLineStop,
            string Cripple,
            string delayFreq,
            string supplyPerMonth,
            string partName,
            string modelCd,
            string reduction,
            string emission,
            string prodYearValue,
            string Q1Safety,
            string Q2Safety,
            string Q3Safety,
            string Q4Safety,
            string TargetSafety,
            string AchievementSafety,
            string Q1Quality,
            string Q2Quality,
            string Q3Quality,
            string Q4Quality,
            string TargetQuality,
            string AchievementQuality,
            string Q1Delivery,
            string Q2Delivery,
            string Q3Delivery,
            string Q4Delivery,
            string TargetDelivery,
            string AchievementDelivery,
            string Q1Environment,
            string Q2Environment,
            string Q3Environment,
            string Q4Environment,
            string TargetEnvironment,
            string AchievementEnvironment
            )
        {
            string QueryResult = string.Empty;
            string[] ArrProdMonth = ProdMonth.Split(';');
            string[] ArrSupplierCode = SupplierCode.Split(';');
            //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
            string[] ArrPartNo = PartNo.Split(';');
            string[] ArrQtyDefect = QtyDefect.Split(';');
            string[] ArrProblem = Problem.Split(';');
            string[] ArrDockCode = DockCode.Split(';');
            string[] ArrSupplierMonth = SupplierMonth.Split(';');
            string[] ArrDelay = Delay.Split(';');
            string[] ArrShortage = Shortage.Split(';');
            string[] ArrIncorrect = Incorrect.Split(';');
            string[] ArrOri = Ori.Split(';');
            string[] ArrAdj = Adj.Split(';');
            string[] ArrReceive = Receive.Split(';');
            string[] ArrQuality = Quality.Split(';');
            string[] ArrWorkability = Workability.Split(';');
            string[] ArrClaim = Claim.Split(';');
            string[] ArrCost = Cost.Split(';');
            string[] ArrTriY = TriY.Split(';');
            string[] ArrFatal = Fatal.Split(';');
            string[] ArrDisabilty = Disabilty.Split(';');
            string[] ArrLostOrgan = LostOrgan.Split(';');
            string[] ArrAbsent = Absent.Split(';');
            string[] ArrAbsentPeriode = AbsentPeriode.Split(';');
            string[] ArrSmallInjured = SmallInjured.Split(';');
            string[] ArrTotalWorkers = TotalWorkers.Split(';');
            string[] ArrJudgment = Judgment.Split(';');
            string[] ArrQtyReceive = QtyReceive.Split(';');

            string[] ArrDProblem = DProblem.Split(';');
            string[] ArrQProblem = QProblem.Split(';');
            string[] ArrWProblem = WProblem.Split(';');

            string[] ArrSPQtyReceive = SPQtyReceive.Split(';');
            string[] ArrNGQuality = NGQuality.Split(';');
            string[] ArrDescQuality = DescQuality.Split(';');
            string[] ArrTime1 = Time1.Split(';');
            string[] ArrNGWorkAbility = NGWorkAbility.Split(';');
            string[] ArrDescWorkAbility = DescWorkAbility.Split(';');
            string[] ArrTime2 = Time2.Split(';');
            //add by alira.agi 2015-05-26
            string[] ArrNGDelivery = NGDelivery.Split(';');
            string[] ArrDescNGDelivery = DescDelivery.Split(';');
            string[] ArrTime3 = Time3.Split(';');
            //add riani (20221019)
            string[] ArrReduction = reduction.Split(';');
            string[] ArrEmission = emission.Split(';');
            string[] ArrprodYearValue = prodYearValue.Split(';');
            //----------------//
            //KPI//
            string[] ArrQ1Safety = Q1Safety.Split(';');
            string[] ArrQ2Safety = Q2Safety.Split(';');
            string[] ArrQ3Safety = Q3Safety.Split(';');
            string[] ArrQ4Safety = Q4Safety.Split(';');
            string[] ArrTargetSafety = TargetSafety.Split(';');
            string[] ArrAchievementSafety = AchievementSafety.Split(';');
            string[] ArrQ1Quality = Q1Quality.Split(';');
            string[] ArrQ2Quality = Q2Quality.Split(';');
            string[] ArrQ3Quality = Q3Quality.Split(';');
            string[] ArrQ4Quality = Q4Quality.Split(';');
            string[] ArrTargetQuality = TargetQuality.Split(';');
            string[] ArrAchievementQuality = AchievementQuality.Split(';');
            string[] ArrQ1Delivery = Q1Delivery.Split(';');
            string[] ArrQ2Delivery = Q2Delivery.Split(';');
            string[] ArrQ3Delivery = Q3Delivery.Split(';');
            string[] ArrQ4Delivery = Q4Delivery.Split(';');
            string[] ArrTargetDelivery = TargetDelivery.Split(';');
            string[] ArrAchievementDelivery = AchievementDelivery.Split(';');
            string[] ArrQ1Environment = Q1Environment.Split(';');
            string[] ArrQ2Environment = Q2Environment.Split(';');
            string[] ArrQ3Environment = Q3Environment.Split(';');
            string[] ArrQ4Environment = Q4Environment.Split(';');
            string[] ArrTargetEnvironment = TargetEnvironment.Split(';');
            string[] ArrAchievementEnvironment = AchievementEnvironment.Split(';');

            //----//
            int intYear;
            int intMonth;
            DateTime ProductionMonth;
            DateTime ProductionMonthTo;

            try
            {
                for (int i = 0; i < ArrProdMonth.Length; i++)
                {
                    ///get date from and date to
                    intYear = Convert.ToInt16(ArrProdMonth[i].Substring(0, 4));
                    intMonth = Convert.ToInt16(ArrProdMonth[i].Substring(4, ArrProdMonth[i].Length - 4));
                    ProductionMonth = new DateTime(intYear, intMonth, 1);
                    ProductionMonthTo = new DateTime(ProductionMonth.AddMonths(1).Year, ProductionMonth.AddMonths(1).Month, 1);
                    //add by alira.agi 2015-05-28
                    ProductionMonthTo = ProductionMonthTo.AddDays(-1);

                    QueryResult = db.ExecuteScalar<string>("UpdateSupplierPerformanceNonManufacturing", new object[]
                    {
                        ///quality as main parameter value
                        TabIndex, ProductionMonth, ProductionMonthTo, ArrSupplierCode[i], 
                        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
                        ArrPartNo.Length > 1 ? ArrPartNo[i].Trim() : PartNo.Trim(),
                        ArrQtyDefect.Length > 1 ? ArrQtyDefect[i]: QtyDefect,
                        ArrProblem.Length > 1 ? ArrProblem[i].Trim() : Problem.Trim(),
                        ArrDockCode.Length > 1 ? ArrDockCode[i] : DockCode,
                        AuthorizedUser.Username,
                        ///delivery
                        ArrSupplierMonth.Length > 1 ? ArrSupplierMonth[i] : SupplierMonth,
                        ArrDelay.Length > 1 ? ArrDelay[i] : Delay,
                        ArrShortage.Length > 1 ? ArrShortage[i] : Shortage,
                        ArrIncorrect.Length > 1 ? ArrIncorrect[i] : Incorrect,
                        ///spd
                        ArrOri.Length > 1 ? ArrOri[i] : Ori,
                        ArrAdj.Length > 1 ? ArrAdj[i] : Adj,
                        ArrReceive.Length > 1 ? ArrReceive[i] : Receive,
                        ArrQuality.Length > 1 ? ArrQuality[i] : Quality,
                        ArrWorkability.Length > 1 ? ArrWorkability[i] : Workability,
                        ///pws
                        ArrClaim.Length > 1 ? ArrClaim[i] : Claim,
                        ArrCost.Length > 1 ? ArrCost[i] : Cost,
                        ArrTriY.Length > 1 ? ArrTriY[i] : TriY,
                        ///safety
                        ArrFatal.Length > 1 ? ArrFatal[i] : Fatal,
                        ArrDisabilty.Length > 1 ? ArrDisabilty[i] : Disabilty,
                        ArrLostOrgan.Length > 1 ? ArrLostOrgan[i] : LostOrgan,
                        ArrAbsent.Length > 1 ?  ArrAbsent[i] : Absent,
                        ArrAbsentPeriode.Length > 1 ? ArrSmallInjured[i] : AbsentPeriode,
                        ArrSmallInjured.Length > 1 ? ArrSmallInjured[i] : SmallInjured,
                        ArrTotalWorkers.Length > 1 ? ArrTotalWorkers[i] : TotalWorkers,
                        ArrJudgment.Length > 1 ? ArrJudgment[i].Trim() : Judgment.Trim(),
                        ArrQtyReceive.Length > 1 ? ArrQtyReceive[i].Trim() : QtyReceive.Trim(),

                        ArrDProblem.Length > 1 ? ArrDProblem[i].Trim() : DProblem.Trim(),
                            ArrQProblem.Length > 1 ? ArrQProblem[i].Trim() : QProblem.Trim(),
                                ArrWProblem.Length > 1 ? ArrWProblem[i].Trim() : WProblem.Trim(),
                        //service part quality
                        ArrSPQtyReceive.Length > 1 ? ArrSPQtyReceive[i] : SPQtyReceive,
                        ArrNGQuality.Length > 1 ? ArrNGQuality[i] : NGQuality,
                        ArrDescQuality.Length > 1 ? ArrDescQuality[i] : DescQuality,
                        ArrTime1.Length > 1 ? ArrTime1[i] : Time1,
                        ArrNGWorkAbility.Length > 1 ? ArrNGWorkAbility[i] : NGWorkAbility,
                        ArrDescWorkAbility.Length > 1 ? ArrDescWorkAbility[i] : DescWorkAbility,
                        ArrTime2.Length > 1 ? ArrTime2[i] : Time2,
                        //add by alira.agi 2015-05-26
                        ArrNGDelivery.Length > 1 ? ArrNGDelivery[i] : NGDelivery,
                        ArrDescNGDelivery.Length > 1 ? ArrDescNGDelivery[i] : DescDelivery,
                        ArrTime3.Length > 1 ? ArrTime3[i] : Time3,
                        ShortageQty,
                        Misspart,
                        MinLineStop,
                        Cripple,
                        delayFreq,
                        supplyPerMonth,
                        partName,
                        modelCd,
                        ArrReduction.Length>1?ArrReduction[i]:reduction,
                        ArrEmission.Length>1?ArrEmission[i]:emission,
                        ArrprodYearValue.Length>1?ArrprodYearValue[i]:prodYearValue,
                        ArrQ1Safety.Length>1?ArrQ1Safety[i]:Q1Safety,
                        ArrQ2Safety.Length>1?ArrQ2Safety[i]:Q2Safety,
                        ArrQ3Safety.Length>1?ArrQ3Safety[i]:Q3Safety,
                        ArrQ4Safety.Length>1?ArrQ4Safety[i]:Q4Safety,
                        ArrTargetSafety.Length>1?ArrTargetSafety[i]:TargetSafety,
                        ArrAchievementSafety.Length>1?ArrAchievementSafety[i]:AchievementSafety,
                        ArrQ1Quality.Length>1?ArrQ1Quality[i]:Q1Quality,
                        ArrQ2Quality.Length>1?ArrQ2Quality[i]:Q2Quality,
                        ArrQ3Quality.Length>1?ArrQ3Quality[i]:Q3Quality,
                        ArrQ4Quality.Length>1?ArrQ4Quality[i]:Q4Quality,
                        ArrTargetQuality.Length>1?ArrTargetQuality[i]:TargetQuality,
                        ArrAchievementQuality.Length>1?ArrAchievementQuality[i]:AchievementQuality,
                        ArrQ1Delivery.Length>1?ArrQ1Delivery[i]:Q1Delivery,
                        ArrQ2Delivery.Length>1?ArrQ2Delivery[i]:Q2Delivery,
                        ArrQ3Delivery.Length>1?ArrQ3Delivery[i]:Q3Delivery,
                        ArrQ4Delivery.Length>1?ArrQ4Delivery[i]:Q4Delivery,
                        ArrTargetDelivery.Length>1?ArrTargetDelivery[i]:TargetDelivery,
                        ArrAchievementDelivery.Length>1?ArrAchievementDelivery[i]:AchievementDelivery,
                        ArrQ1Environment.Length>1?ArrQ1Environment[i]:Q1Environment,
                        ArrQ2Environment.Length>1?ArrQ2Environment[i]:Q2Environment,
                        ArrQ3Environment.Length>1?ArrQ3Environment[i]:Q3Environment,
                        ArrQ4Environment.Length>1?ArrQ4Environment[i]:Q4Environment,
                        ArrTargetEnvironment.Length>1?ArrTargetEnvironment[i]:TargetEnvironment,
                        ArrAchievementEnvironment.Length>1?ArrAchievementEnvironment[i]:AchievementEnvironment,

                    });
                }
            }
            catch (Exception ex)
            {
                QueryResult = "Error: " + ex.Message;
            }

            return Content(QueryResult);
        }

        #endregion


        #region DELETE DATA NON MANUFACTURING
        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri	
        //Uncomented by FID.Reggy 17/09/2015, recomended by Niit.Michael
        public ContentResult DeleteDataNonManufacturing(int TabIndex, string ProdMonth, string SupplierCode, string PartNo, string ReceivingArea, string Problem, string ProdYear)
        //public ContentResult DeleteData(int TabIndex, string ProdMonth, string SupplierCode, string ReceivingArea)
        {
            string QueryResult = string.Empty;
            string[] ArrProdMonth = ProdMonth.Split(';');
            string[] ArrSupplierCode = SupplierCode.Split(';');
            //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri	
            string[] ArrPartNo = PartNo.Split(';');
            string[] ArrReceivingArea = ReceivingArea.Split(';');
            string[] ArrYear = ProdYear.Split(';');
            string[] ArrProblem = Problem.Split(';');

            int intYear;
            int intMonth;
            DateTime ProductionMonth;
            DateTime ProductionMonthTo;
            string Year = "";
            try
            {
                for (int i = 0; i < ArrProdMonth.Length; i++)
                {
                    ///get date from and date to
                    //edit riani (20221020) --> add conditional
                    if (ArrProdMonth[i] != "" && ArrProdMonth[i] != null)
                    {
                        intYear = Convert.ToInt16(ArrProdMonth[i].Substring(0, 4));
                        intMonth = Convert.ToInt16(ArrProdMonth[i].Substring(4, ArrProdMonth[i].Length - 4));
                    }

                    else
                    {
                        intYear = 2002;
                        intMonth = 1;
                    }
                    ProductionMonth = new DateTime(intYear, intMonth, 1);
                    ProductionMonthTo = new DateTime(ProductionMonth.AddMonths(1).Year, ProductionMonth.AddMonths(1).Month, 1);
                    //add by alira.agi 2015-05-1
                    ProductionMonthTo = ProductionMonthTo.AddDays(-1);
                    ///delete data
                    ///add riani (20221020)
                    Year = ArrYear[i];
                    QueryResult = db.ExecuteScalar<string>("DeleteSupplierPerformanceNonManufacturing", new object[]
                    {
                        //commented by fid.yusuf, 28 oktober 2014, recomended by istd.mangetya argyaputri
                        TabIndex, ProductionMonth, ProductionMonthTo, ArrSupplierCode[i], ArrReceivingArea[i], ArrPartNo[i], ArrProblem[i],Year //add riani (20221020) --> year
                    });
                }
            }
            catch (Exception ex)
            {
                QueryResult = "Error: " + ex.Message;
            }


            return Content(QueryResult);
        }

        #endregion

        #region DOWNLOAD
        public void DownloadData(int TabSelected, string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            /// variable excel file name
            string FileNames = string.Empty;
            string strProdMonth = Convert.ToDateTime(ProdMonth).ToString("yyyyMM");

            IEnumerable<SPQuality> ListQuality = null;
            IEnumerable<SPDelivery> ListDelivery = null;
            IEnumerable<SPSafety> ListSafety = null;
            IEnumerable<SPSPD> ListServicePart = null;
            IEnumerable<SPTWS> ListWarranty = null;
            IEnumerable<SPReceive> ListReceive = null;
            IEnumerable<SPWorkAbility> ListWorkAbility = null;
            IEnumerable<SPShortage> ListShortage = null;
            IEnumerable<SPMisspart> ListMisspart = null;
            IEnumerable<SPLineStop> ListLineStop = null;
            IEnumerable<SPCripple> ListCripple = null;
            IEnumerable<SPOntime> ListOntime = null;
            IEnumerable<SPComment> ListComment = null;

            //add riani (20221009)
            IEnumerable<SPQuality> ListQualityNonManufacturing = null;
            IEnumerable<SPShortage> ListShortageNonManufacturing = null;
            IEnumerable<SPMisspart> ListMisspartNonManufacturing = null;
            IEnumerable<SPEnvironment> ListEnvironment = null;
            IEnumerable<SPEnvironmentMaster> ListEnvironmentMaster = null;
            //-------------------//

            IEnumerable<SPReceiveReportTMMINPivot> ListReportTMMIN = null;

            ///download data
            IExcelWriter Exporter = ExcelWriter.GetInstance();
            byte[] ResultExport = null;

            switch (TabSelected)
            {
                case 0:
                    FileNames = "SupplierPerformance_Quality_" + strProdMonth + ".xls";
                    ListQuality = GetAllQualityDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListQuality, "DataQuality");
                    break;
                case 1:
                    FileNames = "SupplierPerformance_Delivery_" + strProdMonth + ".xls";
                    ListDelivery = GetAllDeliveryDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListDelivery, "DataDelivery");
                    break;
                case 2:
                    FileNames = "SupplierPerformance_Safety_" + strProdMonth + ".xls";
                    ListSafety = GetAllSafetyDetail(SupplierCode, ProdMonth, ProdMonthTo);
                    ResultExport = Exporter.Write(ListSafety, "DataSafety");
                    break;
                case 3:
                    FileNames = "SupplierPerformance_ServicePart_" + strProdMonth + ".xls";
                    ListServicePart = GetAllSPDDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListServicePart, "DataPart");
                    break;
                case 4:
                    FileNames = "SupplierPerformance_WarrantyClaim_" + strProdMonth + ".xls";
                    ListWarranty = GetAllTWSDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListWarranty, "DataWarranty");
                    break;
                case 5:
                    FileNames = "SupplierPerformance_PartReceive_" + strProdMonth + ".xls";

                    //ResultExport = CreatePartReceivedExcelPivot(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = CreatePartReceivedExcel(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    break;
                case 6:
                    FileNames = "SupplierPerformance_WorkAbility_" + strProdMonth + ".xls";
                    ListWorkAbility = GetAllWorkAbilityDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListWorkAbility, "DataWorkAbility");
                    break;
                case 7:
                    FileNames = "SupplierPerformance_Shortage_" + strProdMonth + ".xls";
                    ListShortage = GetAllShortageDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListShortage, "DataShortage");
                    break;
                case 8:
                    FileNames = "SupplierPerformance_Misspart_" + strProdMonth + ".xls";
                    ListMisspart = GetAllMisspartDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListMisspart, "DataMisspart");
                    break;
                case 9:
                    FileNames = "SupplierPerformance_Ontime_" + strProdMonth + ".xls";
                    ListOntime= GetAllOntimeDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListOntime, "DataOntime");
                    break;
                case 10:
                    FileNames = "SupplierPerformance_LineStop_" + strProdMonth + ".xls";
                    ListLineStop = GetAllLineStopDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListLineStop, "DataLineStop");
                    break;
                case 11:
                    FileNames = "SupplierPerformance_Cripple_" + strProdMonth + ".xls";
                    ListCripple = GetAllCrippleDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListCripple, "DataCripple");
                    break;
                case 12:
                    FileNames = "SupplierPerformance_Comment_" + strProdMonth + ".xls";
                    ListComment = GetAllCommentDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListComment, "DataCripple");
                    break;
                //add riani (20221009)
                case 13:
                    FileNames = "SupplierPerformance_Quality_NonManufacturing_" + strProdMonth + ".xls";
                    ListQualityNonManufacturing = GetAllQualityNonManufacturingDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListQualityNonManufacturing, "DataQualityNonManufacturing");
                    break;
                case 14:
                    FileNames = "SupplierPerformance_Shortage_NonManufacturing_" + strProdMonth + ".xls";
                    ListShortageNonManufacturing = GetAllShortageNonManufacturingDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListShortageNonManufacturing, "DataShortageNonManufacturing");
                    break;
                case 15:
                    FileNames = "SupplierPerformance_Misspart_NonManufacturing_" + strProdMonth + ".xls";
                    ListMisspartNonManufacturing = GetAllMisspartNonManufacturingDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListMisspartNonManufacturing, "DataMisspartNonManufacturing");
                    break;
                case 16:
                    FileNames = "SupplierPerformance_Environment_" + strProdMonth + ".xls";
                    ListEnvironment = GetAllEnvironment(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListEnvironment, "DataEnvironment");
                    break;
                case 17:
                    FileNames = "SupplierPerformance_EnvironmentMaster_" + strProdMonth + ".xls";
                    ListEnvironmentMaster = GetAllEnvironmentMaster(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                    ResultExport = Exporter.Write(ListEnvironmentMaster, "DataEnvironmentMaster");
                    break;
                    //-------------------//
            }

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(ResultExport.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(ResultExport);
            Response.End();
        }

        public byte[] CreatePartReceivedExcelPivot(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            IExcelWriter Exporter = ExcelWriter.GetInstance();
            byte[] ResultExport = null;

            IEnumerable<SPReceive> ListReceive = null;
            IEnumerable<SPReceiveReportTMMINPivot> ListReportTMMIN = null;

            ListReceive = GetAllReceiveDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
            ListReportTMMIN = GetAllReceiveDetailReportTMMINPivot(SupplierCode, ProdMonth, ProdMonthTo, DockCode);

            List<string> TMMINrole = db.Fetch<string>("GetTMMINRole");
            string username = AuthorizedUser.Username;

            bool isTMMINUser = false;
            foreach (var s in TMMINrole)
            {
                foreach (var p in AuthorizedUser.Authorization)
                {
                    if (p.AuthorizationID == s)
                        isTMMINUser = true;
                }
            }

            Exporter.Append(ListReceive, "DataPartReceived");

            if(isTMMINUser)
                Exporter.Append(ListReportTMMIN, "DataPartReceivedDetail");

            return Exporter.Flush();
        }

        public byte[] CreatePartReceivedExcel(string SupplierCode, string ProdMonth, string ProdMonthTo, string DockCode)
        {
            try
            {
                Stream output = new MemoryStream();
                HSSFWorkbook workbook = new HSSFWorkbook();

                List<string> TMMINrole = db.Fetch<string>("GetTMMINRole");
                string username = AuthorizedUser.Username;

                bool isTMMINUser = false;
                foreach (var s in TMMINrole)
                {
                    foreach (var p in AuthorizedUser.Authorization)
                    {
                        if (p.AuthorizationID == s)
                            isTMMINUser = true;
                    }
                }

                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                /*Header Column (Not-Merged)*/
                ICellStyle HeaderStyle = workbook.CreateCellStyle();
                HeaderStyle.VerticalAlignment = VerticalAlignment.TOP;
                HeaderStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                HeaderStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                HeaderStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                HeaderStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                HeaderStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                HeaderStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
                HeaderStyle.Alignment = HorizontalAlignment.CENTER;
                HeaderStyle.SetFont(Font1);

                ICellStyle CenterCell = workbook.CreateCellStyle();
                CenterCell.VerticalAlignment = VerticalAlignment.TOP;
                CenterCell.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                CenterCell.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                CenterCell.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                CenterCell.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                CenterCell.SetFont(Font4);
                CenterCell.Alignment = HorizontalAlignment.CENTER;

                /*Data with Align Right*/
                ICellStyle RightCell = workbook.CreateCellStyle();
                RightCell.VerticalAlignment = VerticalAlignment.TOP;
                RightCell.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                RightCell.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                RightCell.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                RightCell.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                RightCell.SetFont(Font4);
                RightCell.Alignment = HorizontalAlignment.RIGHT;

                /*Data with Align Left*/
                ICellStyle LeftCell = workbook.CreateCellStyle();
                LeftCell.VerticalAlignment = VerticalAlignment.TOP;
                LeftCell.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                LeftCell.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                LeftCell.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                LeftCell.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                LeftCell.SetFont(Font4);
                LeftCell.Alignment = HorizontalAlignment.LEFT;

                IRow Hrow;
                ISheet sheet;
                sheet = workbook.CreateSheet("DataPartReceived");

                List<SPReceive> ListReceive = GetAllReceiveDetail(SupplierCode, ProdMonth, ProdMonthTo, DockCode);
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue("Prod. Month");
                Hrow.CreateCell(1).SetCellValue("Supplier Cd.");
                Hrow.CreateCell(2).SetCellValue("Supplier Name");
                Hrow.CreateCell(3).SetCellValue("QTY Receive");
                Hrow.CreateCell(4).SetCellValue("Receiving Area");
                Hrow.GetCell(0).CellStyle = HeaderStyle;
                Hrow.GetCell(1).CellStyle = HeaderStyle;
                Hrow.GetCell(2).CellStyle = HeaderStyle;
                Hrow.GetCell(3).CellStyle = HeaderStyle;
                Hrow.GetCell(4).CellStyle = HeaderStyle;

                int m = 1;

                foreach (var rcv in ListReceive)
                {
                    Hrow = sheet.CreateRow(m);
                    Hrow.CreateCell(0).SetCellValue(rcv.PROD_MONTH);
                    Hrow.CreateCell(1).SetCellValue(rcv.SUPPLIER_CD);
                    Hrow.CreateCell(2).SetCellValue(rcv.SUPPLIER_NAME);
                    Hrow.CreateCell(3).SetCellValue(rcv.QTY_RECEIVE);
                    Hrow.CreateCell(4).SetCellValue(rcv.RECEIVING_AREA);

                    Hrow.GetCell(0).CellStyle = CenterCell;
                    Hrow.GetCell(1).CellStyle = CenterCell;
                    Hrow.GetCell(2).CellStyle = LeftCell;
                    Hrow.GetCell(3).CellStyle = RightCell;
                    Hrow.GetCell(4).CellStyle = CenterCell;

                    m++;
                }

                sheet.SetColumnWidth(0, ConvertExcelWidth(10.43));
                sheet.SetColumnWidth(1, ConvertExcelWidth(10.43));
                sheet.SetColumnWidth(2, ConvertExcelWidth(40.43));
                sheet.SetColumnWidth(3, ConvertExcelWidth(10.43));
                sheet.SetColumnWidth(4, ConvertExcelWidth(15.43));

                if (isTMMINUser)
                {
                    #region Report TMMIN
                    IRow Hrow2;
                    ISheet sheet2;
                    sheet2 = workbook.CreateSheet("DataPartReceivedDetail");
                    List<SPReceiveReportTMMIN> rcvreport = GetAllReceiveDetailReportTMMIN(SupplierCode, ProdMonth, ProdMonthTo, DockCode);

                    List<string> Receiving_Area = GetPartReceivedColumnName();

                    Hrow2 = sheet2.CreateRow(0);
                    Hrow2.CreateCell(0).SetCellValue("No");
                    Hrow2.CreateCell(1).SetCellValue("Prod. Month");
                    Hrow2.CreateCell(2).SetCellValue("Supplier Cd.");
                    Hrow2.CreateCell(3).SetCellValue("Supplier Name");
                    Hrow2.GetCell(0).CellStyle = HeaderStyle;
                    Hrow2.GetCell(1).CellStyle = HeaderStyle;
                    Hrow2.GetCell(2).CellStyle = HeaderStyle;
                    Hrow2.GetCell(3).CellStyle = HeaderStyle;

                    int k = 4;
                    int j = 1;
                    foreach (var s in Receiving_Area)
                    {
                        Hrow2.CreateCell(k).SetCellValue(s);
                        Hrow2.GetCell(k).CellStyle = HeaderStyle;
                        k++;
                    }
                    Hrow2.CreateCell(k).SetCellValue("Total QTY");
                    Hrow2.GetCell(k).CellStyle = HeaderStyle;

                    var rcvHeader = rcvreport.GroupBy(x => new { x.PROD_MONTH, x.SUPP_CD, x.SUPP_NAME }).Select(y => y.First());
                    int TotalQTY = 0;

                    foreach (var head in rcvHeader)
                    {
                        TotalQTY = 0;
                        Hrow2 = sheet2.CreateRow(j);
                        Hrow2.CreateCell(0).SetCellValue(j.ToString());
                        Hrow2.CreateCell(1).SetCellValue(head.PROD_MONTH);
                        Hrow2.CreateCell(2).SetCellValue(head.SUPP_CD);
                        Hrow2.CreateCell(3).SetCellValue(head.SUPP_NAME);
                        Hrow2.GetCell(0).CellStyle = CenterCell;
                        Hrow2.GetCell(1).CellStyle = CenterCell;
                        Hrow2.GetCell(2).CellStyle = CenterCell;
                        Hrow2.GetCell(3).CellStyle = LeftCell;

                        for (int l = 4; l <= Receiving_Area.Count() + 4; l++)
                        {
                            Hrow2.CreateCell(l).SetCellValue("0");
                        }

                        var details = rcvreport.Where(item => (item.PROD_MONTH == head.PROD_MONTH) &&
                                                                (item.SUPP_CD == head.SUPP_CD) &&
                                                                (item.SUPP_NAME == head.SUPP_NAME));
                        var detail = details.ToList();

                        foreach (var d in detail)
                        {
                            for (int l = 4; l <= Receiving_Area.Count() + 4; l++)
                            {
                                string Column = sheet2.GetRow(0).GetCell(l).StringCellValue;
                                if (Column == d.RECEIVING_AREA)
                                {
                                    Hrow2.GetCell(l).SetCellValue(d.QTY);
                                    TotalQTY = TotalQTY + Convert.ToInt32(d.QTY);
                                }

                                Hrow2.GetCell(l).CellStyle = RightCell;
                            }
                        }

                        Hrow2.CreateCell(Receiving_Area.Count() + 4).SetCellValue(TotalQTY.ToString());
                        Hrow2.GetCell(Receiving_Area.Count() + 4).CellStyle = RightCell;

                        sheet2.SetColumnWidth(0, ConvertExcelWidth(10.43));
                        sheet2.SetColumnWidth(1, ConvertExcelWidth(10.43));
                        sheet2.SetColumnWidth(2, ConvertExcelWidth(10.43));
                        sheet2.SetColumnWidth(3, ConvertExcelWidth(40.43));

                        for (int l = 4; l <= Receiving_Area.Count() + 4; l++)
                        {
                            sheet2.SetColumnWidth(l, ConvertExcelWidth(10.43));
                        }
                        sheet2.SetColumnWidth(Receiving_Area.Count() + 4, ConvertExcelWidth(10.43));

                        j++;
                    }
                    #endregion
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                return ms.ToArray();
            }
            catch (Exception e)
            {
                return new byte[] { };
            }
        }

        public int ConvertExcelWidth(double ExcelWidth)
        {
            const double Widther = 0.00314;
            int NpoiWidth = Convert.ToInt32(Math.Ceiling(ExcelWidth / Widther));

            return NpoiWidth;
        }

        public int ExcelWidthByContentLength(int CharLength)
        {
            const double Lengther = 1.2;
            int ExcelWidth = Convert.ToInt32(Math.Ceiling(CharLength * Lengther));

            return ExcelWidth;
        }

        public FileContentResult DownloadTemplate(int TabSelected)
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            switch (TabSelected)
            {
                case 0:
                    fileName = "SPQualityTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 1:
                    fileName = "SPDeliveryTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 2:
                    fileName = "SPSafetyTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 3:
                    fileName = "SPServicePartTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 4:
                    fileName = "SPWarrantyTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 5:
                    fileName = "SPReceiveTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 6:
                    fileName = "SPWorkAbilityTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 7:
                    fileName = "SPShortageTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 8:
                    fileName = "SPMisspartTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 9:
                    fileName = "SPOntimeTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 10:
                    fileName = "SPLineStopTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 11:
                    fileName = "SPCrippleTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 12:
                    fileName = "SPCommentTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                //add riani (20221009)
                case 13:
                    fileName = "SPQualityNonManufacturingTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 14:
                    fileName = "SPShortageNonManufacturingTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 15:
                    fileName = "SPMisspartNonManufacturingTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 16:
                    fileName = "SPEnvironmentTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 17:
                    fileName = "SPEnvironmentMasterTemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                case 18:
                    fileName = "SPKPITemp.xls";
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierPerformance/TempXls/" + fileName));
                    documentBytes = StreamFile(filePath);
                    break;
                    //------------------//
            }
            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }
        #endregion


        #region UPLOAD

        #region PROPERTIES UPLOAD
        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/SupplierPerformance/TempUpload/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private long _processId = 0;
        private long ProcessId
        {
            get
            {
                if (_processId == 0)
                {
                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); }

                    return _processId;

                    //_processId = Math.Abs(Environment.TickCount);
                }

                return _processId;
            }
        }

        private Int32 _seqNo = 0;
        private Int32 SeqNo
        {
            get
            {
                return ++_seqNo;
            }
        }
        #endregion

        #region ACTION
        [HttpPost]
        public ActionResult SupplierPerformanceUpload(string filePath, int SelectedTab)
        {
            AllProcessUpload(filePath, SelectedTab);
            String status = Convert.ToString(TempData["IsValid"]);
            return Json(new { Status = status });
        }
        #endregion

        #region REQUEST
        private ArrayList GetUploadedFile()
        {
            ArrayList fileRowList = new ArrayList();

            try
            {
                // read uploaded excel file
                fileRowList = ExcelReader.ReaderExcel(
                    _TableDestination.filePath,
                    _TableDestination.sheetName,
                    Convert.ToInt32(_TableDestination.rowStart),
                    Convert.ToInt32(_TableDestination.columnStart),
                    Convert.ToInt32(_TableDestination.columnEnd));
            }
            catch (Exception exc)
            {
                throw new Exception("GetUploadedFile:" + exc.Message);
            }

            return fileRowList;
        }

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();

            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            //db.Close();

            return String.Join(",", columnNameArray.ToArray());
        }

        private SPModel AllProcessUpload(string filePath, int SelectedTab) //abc
        {
            SPModel _UploadModel = new SPModel();

            #region DECLARE XLS PROPERTIES
            _TableDestination.functionID = "71001";
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.tableFromTemp = "TB_T_SUPPLIER_PERFORMANCE_UPLOAD";
            _TableDestination.tableToAccess = "TB_T_SUPPLIER_PERFORMANCE_VALIDATE";

            switch (SelectedTab)
            {
                case 0:
                    _TableDestination.sheetName = "DataQuality";
                    _TableDestination.columnEnd = "8";
                    break;
                case 1:
                    _TableDestination.sheetName = "DataDelivery";
                    _TableDestination.columnEnd = "8";
                    break;
                case 2:
                    _TableDestination.sheetName = "DataSafety";
                    _TableDestination.columnEnd = "9";
                    break;
                case 3:
                    _TableDestination.sheetName = "DataServicePart";
                    _TableDestination.columnEnd = "8";
                    break;
                case 4:
                    _TableDestination.sheetName = "DataWarranty";
                    _TableDestination.columnEnd = "3";
                    break;
                case 5:
                    _TableDestination.sheetName = "DataReceived";
                    _TableDestination.columnEnd = "5";
                    break;
                case 6:
                    _TableDestination.sheetName = "DataWorkAbility";
                    _TableDestination.columnEnd = "6";
                    break;
                case 7:
                    _TableDestination.sheetName = "DataShortage";
                    _TableDestination.columnEnd = "6";
                    break;
                case 8:
                    _TableDestination.sheetName = "DataMisspart";
                    _TableDestination.columnEnd = "6";
                    break;
                case 9:
                    _TableDestination.sheetName = "DataOntime";
                    _TableDestination.columnEnd = "6";
                    break;
                case 10:
                    _TableDestination.sheetName = "DataLineStop";
                    _TableDestination.columnEnd = "5";
                    break;
                case 11:
                    _TableDestination.sheetName = "DataCripple";
                    _TableDestination.columnEnd = "5";
                    break;
                case 12:
                    _TableDestination.sheetName = "DataComment";
                    _TableDestination.columnEnd = "3";
                    break;
                //add riani (20221007)
                case 13:
                    _TableDestination.sheetName = "DataQualityNonManufacturing";
                    _TableDestination.columnEnd = "8";
                    break;
                case 14:
                    _TableDestination.sheetName = "DataShortageNonManufacturing";
                    _TableDestination.columnEnd = "6";
                    break;
                case 15:
                    _TableDestination.sheetName = "DataMisspartNonManufacturing";
                    _TableDestination.columnEnd = "6";
                    break;
                case 16:
                    _TableDestination.sheetName = "DataEnvironment";
                    _TableDestination.columnEnd = "4";
                    break;
                case 17:
                    _TableDestination.sheetName = "DataEnvironmentMaster";
                    _TableDestination.columnEnd = "4";
                    break;
                case 18:
                    _TableDestination.sheetName = "DataKPI";
                    _TableDestination.columnEnd = "26";
                    _TableDestination.rowStart = "3";
                    break;
                    //--------------------//
            }
            #endregion

            LogProvider logProvider = new LogProvider("71001", "7", AuthorizedUser);
            ILogSession sessionSP = logProvider.CreateSession();
            OleDbConnection excelConn = null;

            try
            {
                sessionSP.Write("MPCS00004INF", new object[] { "Process Upload has been started..!!!" });

                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableFromTemp });
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();

                //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
                
                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                #region Select Data From Excel
                string OleCommand = @"select " +
                                    _TableDestination.functionID + " as FUNCTION_ID, " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as ProductionMonth," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as SupplierCode ";

                switch (SelectedTab)
                {
                    case 0:
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as PartName," +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as QTY_REJECT," +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as Problem," +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as ReceivingArea, " +
                                     @"IIf(IsNull(f8), Null, CStr(f8)) as ModelTMMIN ";
                        break;
                    case 1:
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as SupplyPerMonth," +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as Delay," +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as ReceivingArea," +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as Shortage," +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as Incorrect, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as Problem ";
                        break;
                    case 2:
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as Fatal," +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as Disability," +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as LostOrgant," +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as Absent," +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as AbsentPeriode," +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as SmallInjured," +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as TotalWorkers ";
                        break;
                    case 3:
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as Ori," +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as Adj," +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as QtyReceive," +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as NGQuality," +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as NGWorkability ";
                                     //@"IIf(IsNull(F8), Null, CStr(F8)) as DeliveryProblem," +
                                     //@"IIf(IsNull(F9), Null, CStr(F9)) as QualityProblem," +
                                     //@"IIf(IsNull(F10), Null, CStr(F10)) as WorkabilityProblem ";
                        break;
                    case 4:
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as Claim ";
                                     //@"IIf(IsNull(F4), Null, CStr(F4)) as CostShare," +
                                     //@"IIf(IsNull(F5), Null, CStr(F5)) as 3YVol ";
                        break;
                    case 5:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as QtyReceive," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as ReceivingArea ";
                        break;
                    case 6:
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as QtyReceive," +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as NGQuality," +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as DescQuality," +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as Time1," +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as NGWorkAbility," +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as DescWorkAbility," +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as Time2," +
                                     @"IIf(IsNull(F11), Null, CStr(F11)) as NG_DELIVERY," +
                                     @"IIf(IsNull(F12), Null, CStr(F12)) as DESC_DELIVERY," +
                                     @"IIf(IsNull(F13), Null, CStr(F13)) as Time3 ";
                        break;
                    case 7:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as PartName," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as Shortage," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as ReceivingArea ";
                        break;
                    case 8:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as PartName," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as Misspart," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as ReceivingArea ";
                        break;
                    case 9:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as ReceivingArea," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as FreqDelay," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as SupplyPerMonth," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as Problem ";
                        break;
                    case 10:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as ReceivingArea," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as MinuteLineStop," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as Problem ";
                        break;
                    case 11:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as ReceivingArea," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as Cripple," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as Problem ";
                        break;
                    case 12:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as Problem ";
                        break;
                    //add riani (20221007)
                    case 13:
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as PartName," +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as QTY_REJECT," +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as Problem," +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as ReceivingArea, " +
                                     @"IIf(IsNull(f8), Null, CStr(f8)) as ModelTMMIN ";
                        break;
                    case 14:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as PartName," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as Shortage," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as ReceivingArea ";
                        break;
                    case 15:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as PartNo," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as PartName," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as Misspart," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as ReceivingArea ";
                        break;
                    case 16:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as CO2_REDUCTION," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as CO2_EMISSION ";
                        break;
                    case 17:
                        OleCommand = @"select " +
                                    _TableDestination.functionID + " as FUNCTION_ID, " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1+'01')) as ProductionMonth," +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as ProductionYear," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as SupplierCode, " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as CO2_REDUCTION, " +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as CO2_EMISSION ";
                        break;
                    case 18:
                        OleCommand = OleCommand + ", " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as Q1_SAFETY," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as Q2_SAFETY,"+
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as Q3_SAFETY," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as Q4_SAFETY," +
                                    @"IIf(IsNull(F7), Null, CStr(F7)) as TARGET_SAFETY," +
                                    @"IIf(IsNull(F8), Null, CStr(F8)) as ACHIEVEMENT_SAFETY," +
                                    @"IIf(IsNull(F9), Null, CStr(F9)) as Q1_QUALITY," +
                                    @"IIf(IsNull(F10), Null, CStr(F10)) as Q2_QUALITY," +
                                    @"IIf(IsNull(F11), Null, CStr(F11)) as Q3_QUALITY," +
                                    @"IIf(IsNull(F12), Null, CStr(F12)) as Q4_QUALITY," +
                                    @"IIf(IsNull(F13), Null, CStr(F13)) as TARGET_QUALITY," +
                                    @"IIf(IsNull(F14), Null, CStr(F14)) as ACHIEVEMENT_QUALITY," +
                                    @"IIf(IsNull(F15), Null, CStr(F15)) as Q1_DELIVERY," +
                                    @"IIf(IsNull(F10), Null, CStr(F16)) as Q2_DELIVERY," +
                                    @"IIf(IsNull(F11), Null, CStr(F17)) as Q3_DELIVERY," +
                                    @"IIf(IsNull(F12), Null, CStr(F18)) as Q4_DELIVERY," +
                                    @"IIf(IsNull(F13), Null, CStr(F19)) as TARGET_DELIVERY," +
                                    @"IIf(IsNull(F14), Null, CStr(F20)) as ACHIEVEMENT_DELIVERY," +
                                    @"IIf(IsNull(F15), Null, CStr(F21)) as Q1_ENVIRONMENT," +
                                    @"IIf(IsNull(F10), Null, CStr(F22)) as Q2_ENVIRONMENT," +
                                    @"IIf(IsNull(F11), Null, CStr(F23)) as Q3_ENVIRONMENT," +
                                    @"IIf(IsNull(F12), Null, CStr(F24)) as Q4_ENVIRONMENT," +
                                    @"IIf(IsNull(F13), Null, CStr(F25)) as TARGET_ENVIRONMENT," +
                                    @"IIf(IsNull(F14), Null, CStr(F26)) as ACHIEVEMENT_ENVIRONMENT "
                                    ;
                        break;
                        //---------------//
                }

                OleCommand = OleCommand + "from [" + _TableDestination.sheetName + "$]";
                excelCommand = new OleDbCommand(OleCommand, excelConn);
                excelDataAdapter.SelectCommand = excelCommand;
                #endregion

                excelDataAdapter.Fill(dtPatterns);
                dtPatterns = DeleteEmptyRows(dtPatterns);
                ds.Tables.Add(dtPatterns);
                DataRow rowDel = ds.Tables[0].Rows[0];
                ds.Tables[0].Rows.Remove(rowDel);
                
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    #region BULK COPY Column Mapping
                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("ProductionMonth", "PROD_MONTH");
                    bulkCopy.ColumnMappings.Add("SupplierCode", "SUPPLIER_CD");

                    switch (SelectedTab)
                    {
                        case 0:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("PartName", "PART_NAME");
                            bulkCopy.ColumnMappings.Add("QTY_REJECT", "QTY_REJECT");
                            bulkCopy.ColumnMappings.Add("Problem", "PROBLEM_DESC");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            bulkCopy.ColumnMappings.Add("ModelTMMIN", "MODEL_CD");
                            break;
                        case 1:
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            bulkCopy.ColumnMappings.Add("Problem", "PROBLEM_DESC");
                            bulkCopy.ColumnMappings.Add("SupplyPerMonth", "QTY_PART_SUPPLY_MONTH");
                            bulkCopy.ColumnMappings.Add("Delay", "QTY_DELAY");
                            bulkCopy.ColumnMappings.Add("Shortage", "QTY_SHORTAGE");
                            bulkCopy.ColumnMappings.Add("Incorrect", "QTY_INCORRECT");
                            break;
                        case 2:
                            bulkCopy.ColumnMappings.Add("Fatal", "QTY_FATAL");
                            bulkCopy.ColumnMappings.Add("Disability", "QTY_DISABILITY");
                            bulkCopy.ColumnMappings.Add("LostOrgant", "QTY_LOST_ORGAN");
                            bulkCopy.ColumnMappings.Add("Absent", "QTY_ABSENT");
                            bulkCopy.ColumnMappings.Add("AbsentPeriode", "QTY_ABSENT_PERIOD");
                            bulkCopy.ColumnMappings.Add("SmallInjured", "QTY_SMALL_INJURED");
                            bulkCopy.ColumnMappings.Add("TotalWorkers", "QTY_TOTAL_WORKERS_MH");
                            break;
                        case 3:
                            bulkCopy.ColumnMappings.Add("Ori", "QTY_ORI");
                            bulkCopy.ColumnMappings.Add("Adj", "QTY_ADJ");
                            bulkCopy.ColumnMappings.Add("QtyReceive", "QTY_RECEIVE");
                            bulkCopy.ColumnMappings.Add("NGQuality", "QTY_NG_QUALITY");
                            bulkCopy.ColumnMappings.Add("NGWorkability", "QTY_NG_WORKABILITY");
                            //bulkCopy.ColumnMappings.Add("DeliveryProblem", "DeliveryProblem");
                            //bulkCopy.ColumnMappings.Add("QualityProblem", "QualityProblem");
                            //bulkCopy.ColumnMappings.Add("WorkabilityProblem", "WorkabilityProblem");
                            break;
                        case 4:
                            bulkCopy.ColumnMappings.Add("Claim", "QTY_CLAIM");
                            //bulkCopy.ColumnMappings.Add("CostShare", "COST_SHARE");
                            //bulkCopy.ColumnMappings.Add("3YVol", "QTY_3Y");
                            break;
                        case 5:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("QtyReceive", "QTY_RECEIVE");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            break;
                        case 6:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("QtyReceive", "SP_QTY_RECEIVE");
                            bulkCopy.ColumnMappings.Add("NGQuality", "NG_QUALITY");
                            bulkCopy.ColumnMappings.Add("DescQuality", "DESC_QUALITY");
                            bulkCopy.ColumnMappings.Add("Time1", "TIME1");
                            bulkCopy.ColumnMappings.Add("NGWorkAbility", "NG_WORKABILITY");
                            bulkCopy.ColumnMappings.Add("DescWorkAbility", "DESC_WORKABILITY");
                            bulkCopy.ColumnMappings.Add("Time2", "TIME2");
                            bulkCopy.ColumnMappings.Add("NG_DELIVERY", "NG_DELIVERY");
                            bulkCopy.ColumnMappings.Add("DESC_DELIVERY", "DESC_DELIVERY");
                            bulkCopy.ColumnMappings.Add("Time3", "TIME3");
                            break;
                        case 7:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("PartName", "PART_NAME");
                            bulkCopy.ColumnMappings.Add("Shortage", "SHORTAGE");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            break;
                        case 8:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("PartName", "PART_NAME");
                            bulkCopy.ColumnMappings.Add("Misspart", "MISSPART");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            break;
                        case 9:
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            bulkCopy.ColumnMappings.Add("Problem", "PROBLEM_DESC");
                            bulkCopy.ColumnMappings.Add("FreqDelay", "DELAY_FREQ");
                            bulkCopy.ColumnMappings.Add("SupplyPerMonth", "SUPPLY_PER_MONTH");
                            break;
                        case 10:
                            bulkCopy.ColumnMappings.Add("MinuteLineStop", "MIN_LINE_STOP");
                            bulkCopy.ColumnMappings.Add("problem", "PROBLEM_DESC");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            break;
                        case 11:
                            bulkCopy.ColumnMappings.Add("Cripple", "UNIT_CRIPPLE");
                            bulkCopy.ColumnMappings.Add("problem", "PROBLEM_DESC");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA");
                            break;
                        case 12:
                            bulkCopy.ColumnMappings.Add("problem", "PROBLEM_DESC");
                            break;
                        //add riani (20221007)
                        case 13:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("PartName", "PART_NAME");
                            bulkCopy.ColumnMappings.Add("QTY_REJECT", "QTY_REJECT_N_M");
                            bulkCopy.ColumnMappings.Add("Problem", "PROBLEM_DESC_N_M");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA_N_M");
                            bulkCopy.ColumnMappings.Add("ModelTMMIN", "MODEL_CD_N_M");
                            break;
                        case 14:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("PartName", "PART_NAME");
                            bulkCopy.ColumnMappings.Add("Shortage", "SHORTAGE_N_M");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA_N_M");
                            break;
                        case 15:
                            bulkCopy.ColumnMappings.Add("PartNo", "PART_NO");
                            bulkCopy.ColumnMappings.Add("PartName", "PART_NAME");
                            bulkCopy.ColumnMappings.Add("Misspart", "MISSPART_N_M");
                            bulkCopy.ColumnMappings.Add("ReceivingArea", "RECEIVING_AREA_N_M");
                            break;
                        case 16:
                            bulkCopy.ColumnMappings.Add("CO2_REDUCTION", "CO2_REDUCTION");
                            bulkCopy.ColumnMappings.Add("CO2_EMISSION", "CO2_EMISSION");
                            break;
                        case 17:
                            bulkCopy.ColumnMappings.Add("ProductionYear", "PROD_YEAR");
                            bulkCopy.ColumnMappings.Add("CO2_REDUCTION", "CO2_REDUCTION");
                            bulkCopy.ColumnMappings.Add("CO2_EMISSION", "CO2_EMISSION");
                            break;
                        case 18:
                            bulkCopy.ColumnMappings.Add("Q1_SAFETY", "Q1_SAFETY");
                            bulkCopy.ColumnMappings.Add("Q2_SAFETY", "Q2_SAFETY");
                            bulkCopy.ColumnMappings.Add("Q3_SAFETY", "Q3_SAFETY");
                            bulkCopy.ColumnMappings.Add("Q4_SAFETY", "Q4_SAFETY");
                            bulkCopy.ColumnMappings.Add("TARGET_SAFETY", "TARGET_SAFETY");
                            bulkCopy.ColumnMappings.Add("ACHIEVEMENT_SAFETY", "ACHIEVEMENT_SAFETY");
                            bulkCopy.ColumnMappings.Add("Q1_QUALITY", "Q1_QUALITY");
                            bulkCopy.ColumnMappings.Add("Q2_QUALITY", "Q2_QUALITY");
                            bulkCopy.ColumnMappings.Add("Q3_QUALITY", "Q3_QUALITY");
                            bulkCopy.ColumnMappings.Add("Q4_QUALITY", "Q4_QUALITY");
                            bulkCopy.ColumnMappings.Add("TARGET_QUALITY", "TARGET_QUALITY");
                            bulkCopy.ColumnMappings.Add("ACHIEVEMENT_QUALITY", "ACHIEVEMENT_QUALITY");
                            bulkCopy.ColumnMappings.Add("Q1_DELIVERY", "Q1_DELIVERY");
                            bulkCopy.ColumnMappings.Add("Q2_DELIVERY", "Q2_DELIVERY");
                            bulkCopy.ColumnMappings.Add("Q3_DELIVERY", "Q3_DELIVERY");
                            bulkCopy.ColumnMappings.Add("Q4_DELIVERY", "Q4_DELIVERY");
                            bulkCopy.ColumnMappings.Add("TARGET_DELIVERY", "TARGET_DELIVERY");
                            bulkCopy.ColumnMappings.Add("ACHIEVEMENT_DELIVERY", "ACHIEVEMENT_DELIVERY");
                            bulkCopy.ColumnMappings.Add("Q1_ENVIRONMENT", "Q1_ENVIRONMENT");
                            bulkCopy.ColumnMappings.Add("Q2_ENVIRONMENT", "Q2_ENVIRONMENT");
                            bulkCopy.ColumnMappings.Add("Q3_ENVIRONMENT", "Q3_ENVIRONMENT");
                            bulkCopy.ColumnMappings.Add("Q4_ENVIRONMENT", "Q4_ENVIRONMENT");
                            bulkCopy.ColumnMappings.Add("TARGET_ENVIRONMENT", "TARGET_ENVIRONMENT");
                            bulkCopy.ColumnMappings.Add("ACHIEVEMENT_ENVIRONMENT", "ACHIEVEMENT_ENVIRONMENT");
                            break;
                            //------//
                    }
                    #endregion

                    bulkCopy.WriteToServer(ds.Tables[0]);
                }

                String columnNameList = GetColumnName();
                db.Execute("ValidateSupplierPerformanceUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });
                _ErrorValidationList = db.Fetch<ErrorValidation>("GetErrorMessageUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });
                
                if (_ErrorValidationList.Count == 0)
                {
                    db.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] { _TableDestination.tableToAccess, columnNameList, columnNameList, _TableDestination.tableFromTemp });
                    TempData["IsValid"] = "true;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    sessionSP.Write("MPCS00004INF", new object[] { @"Insert upload success info into table log detail.
                                                                        Insert data from temporary uploaded table into temporary validated table.
                                                                        Read uploaded data from temporary validated table to transcation table.
                                                                        set process validation status...!!!" });
                }
                else
                {
                    TempData["IsValid"] = "false;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    sessionSP.Write("MPCS00004INF", new object[] { @"Insert upload fail info into table log detail
                                                                        set process validation status...!!!" });
                }
            }
            catch (Exception exc)
            {
                TempData["IsValid"] = "false;" + exc.Message;
                sessionSP.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });
            }
            finally
            {
                sessionSP.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                sessionSP.Commit();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }
            return _UploadModel;
        }

        public DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            List<DataRow> drList = new List<DataRow>();
            foreach (DataRow dr in formattedTable.Rows)
            {
                int count = dr.ItemArray.Length;
                int nullcounter = 0;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])) || dr.ItemArray[i].ToString().Length <= 0)
                    {
                        nullcounter++;
                    }
                }

                if (nullcounter == count - 2)
                {
                    drList.Add(dr);
                }
            }

            for (int i = 0; i < drList.Count; i++)
            {
                formattedTable.Rows.Remove(drList[i]);
            }
            formattedTable.AcceptChanges();

            return formattedTable;
        }

        [HttpPost]
        public void ClearUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            try
            {
                db.Execute("ClearFeedbackPartUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception exc)
            {
                throw new Exception();
            }
        }

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("SPUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }
        #endregion

        #region RESULT
        public ContentResult UploadValid(String functionId, String processId, int SelectedTab, String temporaryTableValidate)
        {
            //Add try catch by FID.Reggy
            try
            {
            db.ExecuteScalar<string>("InsertSupplierPerformanceUpload",
                            new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            SelectedTab,
                            AuthorizedUser.Username
                        });


            // Add by fid.goldy 

            // new logic for completeness check at Supplier Performance status
            db.ExecuteScalar<string>("SupplierPerformanceCompletenessCheck",
                           new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            SelectedTab,
                            AuthorizedUser.Username
                            //productionMonth,
                            //supplierCode
                        });

            // new logic for Safety check at Supplier Performance status
            db.ExecuteScalar<string>("SupplierPerformanceSafetyCheck",
                           new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            SelectedTab,
                            AuthorizedUser.Username
                            
                        });

            // end of add by fid.goldy
            }
            catch (Exception e)
            {
                return Content(e.Message);
            }
            return Content("SUCCESS");
        }

        [HttpGet]
        public void UploadInvalid(String functionId, String processId, int SelectedTab, String temporaryTableUpload)
        {
            string FileNames = "SupplierPerformance_Invalid.xls";
            string SheetNames = string.Empty;
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] ResultQuery = null;

            switch (SelectedTab)
            {
                case 0:
                    SheetNames = "DataQuality";
                    IEnumerable<SPQualityError> qryQuality = db.Query<SPQualityError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryQuality), SheetNames);
                    break;
                case 1:
                    SheetNames = "DataDelivery";
                    IEnumerable<SPDeliveryError> qryDelivery = db.Query<SPDeliveryError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryDelivery), SheetNames);
                    break;
                case 2:
                    SheetNames = "DataSafety";
                    IEnumerable<SPSafetyError> qrySafety = db.Query<SPSafetyError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qrySafety), SheetNames);
                    break;
                case 3:
                    SheetNames = "DataServicePart";
                    IEnumerable<SPSPDError> qryServicePart = db.Query<SPSPDError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryServicePart), SheetNames);
                    break;
                case 4:
                    SheetNames = "DataWarranty";
                    IEnumerable<SPTWSError> qryWarranty = db.Query<SPTWSError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryWarranty), SheetNames);
                    break;
                case 5:
                    SheetNames = "DataReceived";
                    IEnumerable<SPReceiveError> qryReceive = db.Query<SPReceiveError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryReceive), SheetNames);
                    break;
                case 6:
                    SheetNames = "DataWorkAbility";
                    IEnumerable<SPWorkAbilityError> qryWorkAbility = db.Query<SPWorkAbilityError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryWorkAbility), SheetNames);
                    break;
                case 7:
                    SheetNames = "DataShortage";
                    IEnumerable<SPShortageError> qryShortage = db.Query<SPShortageError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryShortage), SheetNames);
                    break;
                case 8:
                    SheetNames = "DataMisspart";
                    IEnumerable<SPMisspartError> qryMisspart = db.Query<SPMisspartError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryMisspart), SheetNames);
                    break;
                case 9:
                    SheetNames = "DataOntime";
                    IEnumerable<SPOntimeError> qryOntime = db.Query<SPOntimeError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryOntime), SheetNames);
                    break;

                case 10:
                    SheetNames = "DataLineStop";
                    IEnumerable<SPLineStopError> qryLineStop = db.Query<SPLineStopError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryLineStop), SheetNames);
                    break;
                case 11:
                    SheetNames = "DataCripple";
                    IEnumerable<SPCrippleError> qryCripple = db.Query<SPCrippleError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryCripple), SheetNames);
                    break;
                //add riani (20221018)
                case 13:
                    SheetNames = "DataQualityNonManufacturing";
                    IEnumerable<SPQualityError> qryQualityNonManufacturing = db.Query<SPQualityError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryQualityNonManufacturing), SheetNames);
                    break;
                case 14:
                    SheetNames = "DataShortageNonManufacturing";
                    IEnumerable<SPShortageError> qryShortageNonManufacturing = db.Query<SPShortageError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryShortageNonManufacturing), SheetNames);
                    break;
                case 15:
                    SheetNames = "DataMisspartNonManufacturing";
                    IEnumerable<SPMisspartError> qryMisspartNonManufacturing = db.Query<SPMisspartError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryMisspartNonManufacturing), SheetNames);
                    break;
                case 16:
                    SheetNames = "DataEnvironment";
                    IEnumerable<SPEnvironmentError> qryEnvironment = db.Query<SPEnvironmentError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryEnvironment), SheetNames);
                    break;
                case 17:
                    SheetNames = "DataEnvironmentMaster";
                    IEnumerable<SPEnvironmentMasterError> qryEnvironmentMaster = db.Query<SPEnvironmentMasterError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryEnvironmentMaster), SheetNames);
                    break;
                case 18:
                    SheetNames = "DataKPI";
                    IEnumerable<SPKPIError> qryKPI = db.Query<SPKPIError>("ReportSupplierPerformanceUploadError", new object[] { functionId, processId, SelectedTab });
                    ResultQuery = exporter.Write(ConvertToDataTable(qryKPI), SheetNames);
                    break;
                    //--------------------//
            }

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(ResultQuery.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(ResultQuery);
            Response.End();
        }
        #endregion

        #endregion


        #region CONVERT DATA TO DATA READER

        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        #endregion


        #region REPORT

        #region CREATE REPORT ON BYTE TYPE
        private byte[] ReportOnByte(int typeFile, string year, string month, string suppCode)
        {
            string connString = DBContextNames.DB_PCS;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            #region REPORT 0 - Main Report
            Telerik.Reporting.Report rep0;
            //Telerik.Reporting.SqlDataSource sqlSource0;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\RepSupplierPerformance0.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rep0 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            #endregion

            #region REPORT 1 - Main Report Left
            Telerik.Reporting.Report rep1;
            Telerik.Reporting.SqlDataSource sqlRep1;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\RepSupplierPerformance1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rep1 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport mRep1 = (Telerik.Reporting.SubReport)rep0.Items.Find("subRep1", true)[0];
            mRep1.ReportSource = rep1;
            sqlRep1 = (Telerik.Reporting.SqlDataSource)rep1.DataSource;
            sqlRep1.ConnectionString = connString;
            sqlRep1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlRep1.SelectCommand = @"SELECT DISTINCT S.SUPPLIER_NAME, SC.CUSTOMER_NAME, " +
                                        "CONVERT(DATE,'" + year + month + "01" + "') AS MONTHYEAR " +
                                    "FROM TB_M_SUPPLIER S " +
                                        "LEFT JOIN TB_M_SUPPLIER_CUSTOMER SC " +
                                        "ON SC.SUPPLIER_CD = S.SUPPLIER_CODE " +
                                    "WHERE S.SUPPLIER_CODE = '" + suppCode + "'";
            #endregion

            #region REPORT 2 - Main Report Right
            Telerik.Reporting.Report rep2;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\RepSupplierPerformance2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rep2 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            Telerik.Reporting.SubReport mRep2 = (Telerik.Reporting.SubReport)rep0.Items.Find("subRep2", true)[0];
            mRep2.ReportSource = rep2;
            #endregion

            #region SUB REPORT ON LEFT PAGE

            #region SUB REPORT 1 - Safety Report Submission
            Telerik.Reporting.Report rpt1;
            Telerik.Reporting.SqlDataSource sqlSource1;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepSafety.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt1 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep1 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport1", true)[0];
            subRep1.ReportSource = rpt1;
            sqlSource1 = (Telerik.Reporting.SqlDataSource)rpt1.DataSource;
            sqlSource1.ConnectionString = connString;
            sqlSource1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource1.SelectCommand = @"SELECT SUPPLIER_CD
	                                        ,DATEPART(YEAR, PROD_MONTH) AS YYYY
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 1) AS JAN
			                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 4) AS APR		
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT C.JUDGEMENT FROM TB_R_SUPPLIER_SAFETY_H C
		                                        WHERE C.SUPPLIER_CD = F.SUPPLIER_CD
			                                        AND DATEPART(YEAR, C.PROD_MONTH) = DATEPART(YEAR, F.PROD_MONTH)
			                                        AND DATEPART(MONTH, C.PROD_MONTH) = 12) AS [DEC]	
                                        FROM TB_R_SUPPLIER_SAFETY_H F
                                        WHERE SUPPLIER_CD = '0001' AND DATEPART(YEAR, PROD_MONTH) = 2013
                                        GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, PROD_MONTH)
	                                        --,DATEPART(MONTH, PROD_MONTH)";
            //sqlSource1.Parameters.Add("@ProductionYear", System.Data.DbType.String, ProductionYear);
            //sqlSource1.Parameters.Add("@ProductionMonth", System.Data.DbType.String, (((int.Parse(ProductionMonth) - 1).ToString().Length == 1) ? "0" + (int.Parse(ProductionMonth) - 1).ToString() : (int.Parse(ProductionMonth) - 1).ToString()));
            //sqlSource1.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            //sqlSource1.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            #endregion

            #region SUB REPORT 2 - Quality Performance (ppm)
            Telerik.Reporting.Report rpt2;
            Telerik.Reporting.SqlDataSource sqlSource2;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt2 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep2 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport2", true)[0];
            subRep2.ReportSource = rpt2;
            sqlSource2 = (Telerik.Reporting.SqlDataSource)rpt2.DataSource;
            sqlSource2.ConnectionString = connString;
            sqlSource2.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource2.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1)) * 1000000 AS JAN

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2))  * 1000000 AS FEB

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3))  * 1000000 AS MAR			

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4))  * 1000000 AS APR

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5))  * 1000000 AS MAY

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6))   * 1000000 AS JUN

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7))   * 1000000 AS JUL

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8))   * 1000000 AS AGS

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9))   * 1000000 AS SEP

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10))   * 1000000 AS OCT

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11))   * 1000000 AS NOV

                                            ,CONVERT (DECIMAL,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12)) /
                                                CONVERT(DECIMAL, (SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12))   * 1000000 AS [DEC]
            
                                            ,(SELECT TOP 1 TARGET_VALUE FROM dbo.TB_M_TMMIN_TARGET
                                                WHERE TARGET_ID = '71001' AND TARGET_NAME = 'QUALITY' AND VALID_FROM = '" + year + "-01-01')  AS TARGET_TMMIN " +

                                        @"FROM TB_R_SUPPLIER_QUALITY_RECEIVE H
                                        FULL OUTER JOIN TB_R_SUPPLIER_QUALITY HH
	                                        ON HH.SUPPLIER_CD = H.SUPPLIER_CD 
	                                        AND HH.PROD_MONTH = H.PROD_MONTH " +
                                        " WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 3 - Quality Part Receive
            Telerik.Reporting.Report rpt3;
            Telerik.Reporting.SqlDataSource sqlSource3;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt3 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep3 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport3", true)[0];
            subRep3.ReportSource = rpt3;
            sqlSource3 = (Telerik.Reporting.SqlDataSource)rpt3.DataSource;
            sqlSource3.ConnectionString = connString;
            sqlSource3.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource3.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_QUALITY_RECEIVE H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 4 - Quality Part Rejected
            Telerik.Reporting.Report rpt4;
            Telerik.Reporting.SqlDataSource sqlSource4;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality3.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt4 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep4 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport4", true)[0];
            subRep4.ReportSource = rpt4;
            sqlSource4 = (Telerik.Reporting.SqlDataSource)rpt4.DataSource;
            sqlSource4.ConnectionString = connString;
            sqlSource4.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource4.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                    ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                    ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                    WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                    FROM TB_R_SUPPLIER_QUALITY H " +
                                    " WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                    @"GROUP BY SUPPLIER_CD
	                                    ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 5 - Warranty (ppm)
            Telerik.Reporting.Report rpt5;
            Telerik.Reporting.SqlDataSource sqlSource5;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepWarranty1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt5 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep5 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport5", true)[0];
            subRep5.ReportSource = rpt5;
            sqlSource5 = (Telerik.Reporting.SqlDataSource)rpt5.DataSource;
            sqlSource5.ConnectionString = connString;
            sqlSource5.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource5.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_CLAIM) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_TWC H
                                        WHERE H.SUPPLIER_CD = '0001' AND DATEPART(YEAR, H.PROD_MONTH) = 2013
                                        GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 6 - Warranty Responsible
            Telerik.Reporting.Report rpt6;
            Telerik.Reporting.SqlDataSource sqlSource6;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepWarranty2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt6 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep6 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport6", true)[0];
            subRep6.ReportSource = rpt6;
            sqlSource6 = (Telerik.Reporting.SqlDataSource)rpt6.DataSource;
            sqlSource6.ConnectionString = connString;
            sqlSource6.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource6.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.COST_SHARE) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_TWC H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 7 - Warranty Rcv 3 Year
            Telerik.Reporting.Report rpt7;
            Telerik.Reporting.SqlDataSource sqlSource7;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepWarranty3.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt7 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep7 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport7", true)[0];
            subRep7.ReportSource = rpt7;
            sqlSource7 = (Telerik.Reporting.SqlDataSource)rpt7.DataSource;
            sqlSource7.ConnectionString = connString;
            sqlSource7.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource7.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_3Y) FROM TB_R_SUPPLIER_TWC D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_TWC H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 8 - Delivery %
            Telerik.Reporting.Report rpt8;
            Telerik.Reporting.SqlDataSource sqlSource8;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepDelivery1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt8 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep8 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport8", true)[0];
            subRep8.ReportSource = rpt8;
            sqlSource8 = (Telerik.Reporting.SqlDataSource)rpt8.DataSource;
            sqlSource8.ConnectionString = connString;
            sqlSource8.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource8.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000  FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000 FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
                                            ,(SELECT SUM(CONVERT(DECIMAL(5,2),D.QTY_DELAY))/SUM(D.QTY_PART_SUPPLY_MONTH) * 1000000 FROM TB_R_SUPPLIER_DELIVERY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
	                                        , (SELECT TOP 1 TARGET_VALUE FROM dbo.TB_M_TMMIN_TARGET
		                                        WHERE TARGET_ID = '71001' AND TARGET_NAME = 'DELIVERY' AND VALID_FROM = '" + year + "-01-01') AS TARGET_TMMIN " +


                                        "FROM TB_R_SUPPLIER_DELIVERY H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 9 - Delivery Supply
            Telerik.Reporting.Report rpt9;
            Telerik.Reporting.SqlDataSource sqlSource9;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepDelivery2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt9 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep9 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport9", true)[0];
            subRep9.ReportSource = rpt9;
            sqlSource9 = (Telerik.Reporting.SqlDataSource)rpt9.DataSource;
            sqlSource9.ConnectionString = connString;
            sqlSource9.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource9.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_PART_SUPPLY_MONTH) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_DELIVERY H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 10 - Delivery Delay
            Telerik.Reporting.Report rpt10;
            Telerik.Reporting.SqlDataSource sqlSource10;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepDelivery3.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt10 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep10 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport10", true)[0];
            subRep10.ReportSource = rpt10;
            sqlSource10 = (Telerik.Reporting.SqlDataSource)rpt10.DataSource;
            sqlSource10.ConnectionString = connString;
            sqlSource10.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource10.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_DELAY) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_DELIVERY H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 11 - Shortage
            Telerik.Reporting.Report rpt11;
            Telerik.Reporting.SqlDataSource sqlSource11;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepDelivery4.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt11 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep11 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport11", true)[0];
            subRep11.ReportSource = rpt11;
            sqlSource11 = (Telerik.Reporting.SqlDataSource)rpt11.DataSource;
            sqlSource11.ConnectionString = connString;
            sqlSource11.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource11.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_SHORTAGE) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_DELIVERY H " +
                                        " WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 12 - Incorrect
            Telerik.Reporting.Report rpt12;
            Telerik.Reporting.SqlDataSource sqlSource12;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepDelivery5.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt12 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep12 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport12", true)[0];
            subRep12.ReportSource = rpt12;
            sqlSource12 = (Telerik.Reporting.SqlDataSource)rpt12.DataSource;
            sqlSource12.ConnectionString = connString;
            sqlSource12.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource12.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_INCORRECT) FROM TB_R_SUPPLIER_DELIVERY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_DELIVERY H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 13 - Line Stop
            Telerik.Reporting.Report rpt13;
            Telerik.Reporting.SqlDataSource sqlSource13;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepDelivery6.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt13 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep13 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport13", true)[0];
            subRep13.ReportSource = rpt13;
            //            sqlSource13 = (Telerik.Reporting.SqlDataSource)rpt13.DataSource;
            //            sqlSource13.ConnectionString = connString;
            //            sqlSource13.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            //            sqlSource13.SelectCommand = @"SELECT H.SUPPLIER_CD
            //	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
            //	                                        ,(SELECT D.QTY_INCORRECT FROM TB_R_SUPPLIER_DELIVERY D
            //		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
            //			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
            //			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
            //                                        FROM TB_R_SUPPLIER_DELIVERY H
            //                                        WHERE H.SUPPLIER_CD = '0001' AND DATEPART(YEAR, H.PROD_MONTH) = 2013
            //                                        GROUP BY SUPPLIER_CD
            //	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 15 - Grafik Quality Problem
            Telerik.Reporting.Report rpt15;
            Telerik.Reporting.SqlDataSource sqlSource15;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepGrafik1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt15 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep15 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport15", true)[0];
            subRep15.ReportSource = rpt15;
            sqlSource15 = (Telerik.Reporting.SqlDataSource)rpt15.DataSource;

            sqlSource15.ConnectionString = connString;
            sqlSource15.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource15.SelectCommand = @"SELECT 'JAN' [MONTH] INTO #temp1
                                        UNION SELECT 'FEB'
                                        UNION SELECT 'MAR'
                                        UNION SELECT 'APR'
                                        UNION SELECT 'MAY'
                                        UNION SELECT 'JUN'
                                        UNION SELECT 'JUL'
                                        UNION SELECT 'AUG'
                                        UNION SELECT 'SEP'
                                        UNION SELECT 'OCT'
                                        UNION SELECT 'NOV'
                                        UNION SELECT 'DEC'
                                        --SELECT * FROM #temp1

                                        SELECT DISTINCT
                                            H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS PROD_MONTH
                                            ,SUBSTRING(CONVERT(varchar(11), H.PROD_MONTH, 113),4,3) AS [MONTH]
                                            ,H.PART_NO
                                            ,M.PART_NAME

                                            ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND D.PART_NO = H.PART_NO		
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_DEFECT

                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND D.PART_NO = H.PART_NO		
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_RECEIVE
                                        INTO #temp2
                                        FROM TB_R_SUPPLIER_QUALITY_RECEIVE H
                                        FULL OUTER JOIN TB_R_SUPPLIER_QUALITY HH
                                            ON HH.SUPPLIER_CD = H.SUPPLIER_CD 
                                            AND HH.PROD_MONTH = H.PROD_MONTH
                                        LEFT JOIN TB_M_PART_INFO M
                                            ON M.PART_NO = H.PART_NO " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY 
                                            H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH)
                                            ,SUBSTRING(CONVERT(varchar(11), H.PROD_MONTH, 113),4,3)
                                            ,H.PART_NO
                                            ,M.PART_NAME


                                        SELECT T1.[MONTH]
                                            ,ISNULL(T2.QTY_DEFECT, 0) AS QTY_REJECT
                                            ,ISNULL(T2.QTY_RECEIVE, 0) AS QTY_RECEIVE
                                        FROM #temp1 T1 
                                            LEFT OUTER JOIN #temp2 T2 ON T2.[MONTH] = T1.[MONTH]

                                        DROP TABLE #temp1
                                        DROP TABLE #temp2";
            #endregion

            #region SUB REPORT 14 - Tabulasi Quality Problem
            Telerik.Reporting.Report rpt14;
            Telerik.Reporting.SqlDataSource sqlSource14;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepTabulasi1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt14 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep14 = (Telerik.Reporting.SubReport)rpt15.Items.Find("subReport14", true)[0];
            subRep14.ReportSource = rpt14;
            sqlSource14 = (Telerik.Reporting.SqlDataSource)rpt14.DataSource;
            sqlSource14.ConnectionString = connString;
            sqlSource14.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource14.SelectCommand = @"SELECT DISTINCT
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS PROD_MONTH
	                                        ,H.PART_NO
	                                        ,M.PART_NAME
	
	                                        ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_DEFECT
			
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_RECEIVE
	
	                                        ,(SELECT TOP 1 D.PROBLEM_DESC FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
		                                        ORDER BY PROBLEM_DESC DESC) AS PROBLEM_DESC	
	
                                        FROM TB_R_SUPPLIER_QUALITY_RECEIVE H
                                        FULL OUTER JOIN TB_R_SUPPLIER_QUALITY HH
	                                        ON HH.SUPPLIER_CD = H.SUPPLIER_CD 
	                                        AND HH.PROD_MONTH = H.PROD_MONTH
                                        LEFT JOIN TB_M_PART_INFO M
	                                        ON M.PART_NO = H.PART_NO " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY 
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)
	                                        ,H.PART_NO
	                                        ,M.PART_NAME";
            #endregion

            #region SUB REPORT 17 - Grafik Delivery Problem
            Telerik.Reporting.Report rpt17;
            Telerik.Reporting.SqlDataSource sqlSource17;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepGrafik2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt17 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep17 = (Telerik.Reporting.SubReport)rep1.Items.Find("subReport17", true)[0];
            subRep17.ReportSource = rpt17;
            sqlSource17 = (Telerik.Reporting.SqlDataSource)rpt17.DataSource;

            sqlSource17.ConnectionString = connString;
            sqlSource17 = (Telerik.Reporting.SqlDataSource)rpt17.DataSource;
            sqlSource17.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource17.SelectCommand = @"SELECT '1' [MONTH] INTO #temp1
                                            UNION SELECT '2'
                                            UNION SELECT '3'
                                            UNION SELECT '4'
                                            UNION SELECT '5'
                                            UNION SELECT '6'
                                            UNION SELECT '7'
                                            UNION SELECT '8'
                                            UNION SELECT '9'
                                            UNION SELECT '10'
                                            UNION SELECT '11'
                                            UNION SELECT '12'
                                            --SELECT * FROM #temp1

                                            SELECT DATEPART(MONTH,R.PROD_MONTH) AS [MONTH]
	                                            ,ISNULL(SUM(CONVERT(INT,R.QTY_DELAY)), 0) AS QUANTITY
                                            INTO #temp2
                                            FROM TB_R_SUPPLIER_DELIVERY R
	                                            INNER JOIN dbo.TB_M_PART_INFO M ON M.PART_NO = R.PART_NO " +
                                            "WHERE R.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, R.PROD_MONTH) = " + year + " " +
                                            @"GROUP BY DATEPART(MONTH,R.PROD_MONTH)


                                            SELECT T1.[MONTH]
	                                            ,ISNULL(T2.QUANTITY, 0) AS QUALITY
	                                            ,'0' AS PRICE
                                            FROM #temp1 T1 
	                                            LEFT OUTER JOIN #temp2 T2 ON T2.[MONTH] = T1.[MONTH]
                                            ORDER BY CONVERT(INT,T1.[MONTH])

                                            DROP TABLE #temp1
                                            DROP TABLE #temp2";
            #endregion

            #region SUB REPORT 16 - Tabulasi Delivery Problem
            Telerik.Reporting.Report rpt16;
            Telerik.Reporting.SqlDataSource sqlSource16;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepTabulasi2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt16 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep16 = (Telerik.Reporting.SubReport)rpt17.Items.Find("subReport16", true)[0];
            subRep16.ReportSource = rpt16;
            sqlSource16 = (Telerik.Reporting.SqlDataSource)rpt16.DataSource;
            sqlSource16.ConnectionString = connString;
            sqlSource16.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource16.SelectCommand = @"SELECT DISTINCT R.PART_NO
	                                        ,M.PART_NAME
	                                        ,R.QTY_DEFECT
	                                        ,'' AS TOTAL_RP
	                                        ,PROBLEM_DESC
                                        FROM TB_R_SUPPLIER_QUALITY R
	                                        INNER JOIN dbo.TB_M_PART_INFO M ON M.PART_NO = R.PART_NO
                                        WHERE R.SUPPLIER_CD = '0001' AND DATEPART(YEAR, R.PROD_MONTH) = 2013";
            #endregion

            #endregion

            #region SUB REPORT ON RIGHT PAGE

            #region SUB REPORT 18 - Acc by Qty Original
            Telerik.Reporting.Report rpt18;
            Telerik.Reporting.SqlDataSource sqlSource18;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepAccBy1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt18 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep18 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport18", true)[0];
            subRep18.ReportSource = rpt18;
            sqlSource18 = (Telerik.Reporting.SqlDataSource)rpt18.DataSource;
            sqlSource18.ConnectionString = connString;
            sqlSource18.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource18.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
                                            ,(SELECT SUM(D.QTY_ORI) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 19 - Acc by Qty Adjusted
            Telerik.Reporting.Report rpt19;
            Telerik.Reporting.SqlDataSource sqlSource19;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepAccBy2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt19 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep19 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport19", true)[0];
            subRep19.ReportSource = rpt19;
            sqlSource19 = (Telerik.Reporting.SqlDataSource)rpt19.DataSource;
            sqlSource19.ConnectionString = connString;
            sqlSource19.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource19.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
                                            ,(SELECT SUM(D.QTY_ADJ) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 20 - Quality Performance (ppm)
            Telerik.Reporting.Report rpt20;
            Telerik.Reporting.SqlDataSource sqlSource20;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt20 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep20 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport20", true)[0];
            subRep20.ReportSource = rpt20;
            sqlSource20 = (Telerik.Reporting.SqlDataSource)rpt20.DataSource;
            sqlSource20.ConnectionString = connString;
            sqlSource20.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource20.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR			

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6)  AS JUN

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7)  AS JUL

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8)  AS AGS

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9)  AS SEP

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10)  AS OCT

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11)  AS NOV

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_QUALITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
                                                FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12)  AS [DEC]

                                            , (SELECT TOP 1 TARGET_VALUE FROM dbo.TB_M_TMMIN_TARGET
		                                        WHERE TARGET_ID = '71001' AND TARGET_NAME = 'DELIVERY' AND VALID_FROM = '" + year + "-01-01') AS TARGET_TMMIN " +

                                        "FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 21 - Quality Part Receive
            Telerik.Reporting.Report rpt21;
            Telerik.Reporting.SqlDataSource sqlSource21;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt21 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep21 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport21", true)[0];
            subRep21.ReportSource = rpt21;
            sqlSource21 = (Telerik.Reporting.SqlDataSource)rpt21.DataSource;
            sqlSource21.ConnectionString = connString;
            sqlSource21.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource21.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
                                            ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 22 - Quality Part Rejected
            Telerik.Reporting.Report rpt22;
            Telerik.Reporting.SqlDataSource sqlSource22;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality3.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt22 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep22 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport22", true)[0];
            subRep22.ReportSource = rpt22;
            sqlSource22 = (Telerik.Reporting.SqlDataSource)rpt22.DataSource;
            sqlSource22.ConnectionString = connString;
            sqlSource22.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource22.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
                                            ,(SELECT SUM(D.QTY_NG_QUALITY) FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 23 - Workability (ppm)
            Telerik.Reporting.Report rpt23;
            Telerik.Reporting.SqlDataSource sqlSource23;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepWorkability1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt23 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep23 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport23", true)[0];
            subRep23.ReportSource = rpt23;
            sqlSource23 = (Telerik.Reporting.SqlDataSource)rpt23.DataSource;
            sqlSource23.ConnectionString = connString;
            sqlSource23.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource23.SelectCommand = @"SELECT H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR			

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 6)  AS JUN

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 7)  AS JUL

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 8)  AS AGS

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 9)  AS SEP

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 10)  AS OCT

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 11)  AS NOV

                                            ,(SELECT CONVERT(DECIMAL, SUM(D.QTY_NG_WORKABILITY))/CONVERT(DECIMAL, SUM(D.QTY_RECEIVE)) 
		                                        FROM TB_R_SUPPLIER_SPD D
                                                WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
                                                    AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
                                                    AND DATEPART(MONTH, D.PROD_MONTH) = 12)  AS [DEC]

                                        FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY H.SUPPLIER_CD
                                            ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 24 - Workability Part Received
            Telerik.Reporting.Report rpt24;
            Telerik.Reporting.SqlDataSource sqlSource24;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality2.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt24 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep24 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport24", true)[0];
            subRep24.ReportSource = rpt24;
            sqlSource24 = (Telerik.Reporting.SqlDataSource)rpt24.DataSource;
            sqlSource24.ConnectionString = connString;
            sqlSource24.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource24.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 25 - Workability Part Rejected
            Telerik.Reporting.Report rpt25;
            Telerik.Reporting.SqlDataSource sqlSource25;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepQuality3.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt25 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep25 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport25", true)[0];
            subRep25.ReportSource = rpt25;
            sqlSource25 = (Telerik.Reporting.SqlDataSource)rpt25.DataSource;
            sqlSource25.ConnectionString = connString;
            sqlSource25.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource25.SelectCommand = @"SELECT H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS YYYY
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 1) AS JAN
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 2) AS FEB
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 3) AS MAR
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 4) AS APR
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 5) AS MAY
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 6) AS JUN
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 7) AS JUL
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 8) AS AGS
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 9) AS SEP
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 10) AS OCT
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 11) AS NOV
	                                        ,(SELECT SUM(D.QTY_NG_WORKABILITY) FROM TB_R_SUPPLIER_SPD D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
			                                        AND DATEPART(MONTH, D.PROD_MONTH) = 12) AS [DEC]
                                        FROM TB_R_SUPPLIER_SPD H " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)";
            #endregion

            #region SUB REPORT 27 - Dummy From Grafik Delivery Problem
            Telerik.Reporting.Report rpt27;
            Telerik.Reporting.SqlDataSource sqlSource27;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepGrafik3.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt27 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep27 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport27", true)[0];
            subRep27.ReportSource = rpt27;
            sqlSource27 = (Telerik.Reporting.SqlDataSource)rpt27.DataSource;

            sqlSource27.ConnectionString = connString;
            sqlSource27.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource27.SelectCommand = @"SELECT 'JAN' [MONTH] INTO #temp1
                                        UNION SELECT 'FEB'
                                        UNION SELECT 'MAR'
                                        UNION SELECT 'APR'
                                        UNION SELECT 'MAY'
                                        UNION SELECT 'JUN'
                                        UNION SELECT 'JUL'
                                        UNION SELECT 'AUG'
                                        UNION SELECT 'SEP'
                                        UNION SELECT 'OCT'
                                        UNION SELECT 'NOV'
                                        UNION SELECT 'DEC'
                                        --SELECT * FROM #temp1

                                        SELECT SUBSTRING(CONVERT(varchar(11), R.PROD_MONTH, 113),4,3) AS [MONTH]
	                                        ,ISNULL(SUM(R.QTY_DEFECT), 0) AS QUANTITY
                                        INTO #temp2
                                        FROM TB_R_SUPPLIER_QUALITY R
	                                        INNER JOIN dbo.TB_M_PART_INFO M ON M.PART_NO = R.PART_NO
                                        WHERE R.SUPPLIER_CD = '0001' AND DATEPART(YEAR, R.PROD_MONTH) = 2013
                                        GROUP BY SUBSTRING(CONVERT(varchar(11), R.PROD_MONTH, 113),4,3)


                                        SELECT T1.[MONTH]
	                                        ,ISNULL(T2.QUANTITY, 0) AS QUALITY
	                                        ,'5' AS PRICE
                                        FROM #temp1 T1 
	                                        LEFT OUTER JOIN #temp2 T2 ON T2.[MONTH] = T1.[MONTH]

                                        DROP TABLE #temp1
                                        DROP TABLE #temp2";
            //sqlSource15.Parameters.Add("@ProductionYear", System.Data.DbType.String, ProductionYear);
            //sqlSource15.Parameters.Add("@ProductionMonth", System.Data.DbType.String, ProductionMonth);
            //sqlSource15.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            //sqlSource15.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            #endregion

            #region SUB REPORT 26 - Dummy From Tabulasi Delivery Problem
            Telerik.Reporting.Report rpt26;
            Telerik.Reporting.SqlDataSource sqlSource26;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepTabulasi1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt26 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep26 = (Telerik.Reporting.SubReport)rpt27.Items.Find("subReport14", true)[0];
            subRep26.ReportSource = rpt26;
            sqlSource26 = (Telerik.Reporting.SqlDataSource)rpt26.DataSource;
            sqlSource26.ConnectionString = connString;
            sqlSource26.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource26.SelectCommand = @"SELECT DISTINCT
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS PROD_MONTH
	                                        ,H.PART_NO
	                                        ,M.PART_NAME
	
	                                        ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_DEFECT
			
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_RECEIVE
	
	                                        ,(SELECT TOP 1 D.PROBLEM_DESC FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
		                                        ORDER BY PROBLEM_DESC DESC) AS PROBLEM_DESC	
	
                                        FROM TB_R_SUPPLIER_QUALITY_RECEIVE H
                                        FULL OUTER JOIN TB_R_SUPPLIER_QUALITY HH
	                                        ON HH.SUPPLIER_CD = H.SUPPLIER_CD 
	                                        AND HH.PROD_MONTH = H.PROD_MONTH
                                        LEFT JOIN TB_M_PART_INFO M
	                                        ON M.PART_NO = H.PART_NO " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY 
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)
	                                        ,H.PART_NO
	                                        ,M.PART_NAME";
            #endregion

            #region SUB REPORT 29 - Dummy From Grafik Quality Problem
            Telerik.Reporting.Report rpt29;
            Telerik.Reporting.SqlDataSource sqlSource29;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepGrafik1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt29 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep29 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport29", true)[0];
            subRep29.ReportSource = rpt29;
            sqlSource29 = (Telerik.Reporting.SqlDataSource)rpt29.DataSource;

            sqlSource29.ConnectionString = connString;
            sqlSource29.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource29.SelectCommand = @"SELECT 'JAN' [MONTH] INTO #temp1
                                        UNION SELECT 'FEB'
                                        UNION SELECT 'MAR'
                                        UNION SELECT 'APR'
                                        UNION SELECT 'MAY'
                                        UNION SELECT 'JUN'
                                        UNION SELECT 'JUL'
                                        UNION SELECT 'AUG'
                                        UNION SELECT 'SEP'
                                        UNION SELECT 'OCT'
                                        UNION SELECT 'NOV'
                                        UNION SELECT 'DEC'
                                        --SELECT * FROM #temp1

                                        SELECT SUBSTRING(CONVERT(varchar(11), R.PROD_MONTH, 113),4,3) AS [MONTH]
	                                        ,ISNULL(SUM(R.QTY_DEFECT), 0) AS QUANTITY
                                        INTO #temp2
                                        FROM TB_R_SUPPLIER_QUALITY R
	                                        INNER JOIN dbo.TB_M_PART_INFO M ON M.PART_NO = R.PART_NO
                                        WHERE R.SUPPLIER_CD = '0001' AND DATEPART(YEAR, R.PROD_MONTH) = 2013
                                        GROUP BY SUBSTRING(CONVERT(varchar(11), R.PROD_MONTH, 113),4,3)


                                        SELECT T1.[MONTH]
	                                        ,ISNULL(T2.QUANTITY, 0) AS QUALITY
	                                        ,'5' AS PRICE
                                        FROM #temp1 T1 
	                                        LEFT OUTER JOIN #temp2 T2 ON T2.[MONTH] = T1.[MONTH]

                                        DROP TABLE #temp1
                                        DROP TABLE #temp2";
            //sqlSource15.Parameters.Add("@ProductionYear", System.Data.DbType.String, ProductionYear);
            //sqlSource15.Parameters.Add("@ProductionMonth", System.Data.DbType.String, ProductionMonth);
            //sqlSource15.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            //sqlSource15.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            #endregion

            #region SUB REPORT 28 - Dummy From Tabulasi Quality Problem
            Telerik.Reporting.Report rpt28;
            Telerik.Reporting.SqlDataSource sqlSource28;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepTabulasi1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt28 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep28 = (Telerik.Reporting.SubReport)rpt29.Items.Find("subReport14", true)[0];
            subRep28.ReportSource = rpt28;
            sqlSource28 = (Telerik.Reporting.SqlDataSource)rpt28.DataSource;
            sqlSource28.ConnectionString = connString;
            sqlSource28.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource28.SelectCommand = @"SELECT DISTINCT
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS PROD_MONTH
	                                        ,H.PART_NO
	                                        ,M.PART_NAME
	
	                                        ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_DEFECT
			
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_RECEIVE
	
	                                        ,(SELECT TOP 1 D.PROBLEM_DESC FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
		                                        ORDER BY PROBLEM_DESC DESC) AS PROBLEM_DESC	
	
                                        FROM TB_R_SUPPLIER_QUALITY_RECEIVE H
                                        FULL OUTER JOIN TB_R_SUPPLIER_QUALITY HH
	                                        ON HH.SUPPLIER_CD = H.SUPPLIER_CD 
	                                        AND HH.PROD_MONTH = H.PROD_MONTH
                                        LEFT JOIN TB_M_PART_INFO M
	                                        ON M.PART_NO = H.PART_NO " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY 
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)
	                                        ,H.PART_NO
	                                        ,M.PART_NAME";
            #endregion

            #region SUB REPORT 31 - Dummy From Grafik Workability Problem
            Telerik.Reporting.Report rpt31;
            Telerik.Reporting.SqlDataSource sqlSource31;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepGrafik4.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt31 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep31 = (Telerik.Reporting.SubReport)rep2.Items.Find("subReport31", true)[0];
            subRep31.ReportSource = rpt31;
            sqlSource31 = (Telerik.Reporting.SqlDataSource)rpt31.DataSource;

            sqlSource31.ConnectionString = connString;
            sqlSource31.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource31.SelectCommand = @"SELECT 'JAN' [MONTH] INTO #temp1
                                        UNION SELECT 'FEB'
                                        UNION SELECT 'MAR'
                                        UNION SELECT 'APR'
                                        UNION SELECT 'MAY'
                                        UNION SELECT 'JUN'
                                        UNION SELECT 'JUL'
                                        UNION SELECT 'AUG'
                                        UNION SELECT 'SEP'
                                        UNION SELECT 'OCT'
                                        UNION SELECT 'NOV'
                                        UNION SELECT 'DEC'
                                        --SELECT * FROM #temp1

                                        SELECT SUBSTRING(CONVERT(varchar(11), R.PROD_MONTH, 113),4,3) AS [MONTH]
	                                        ,ISNULL(SUM(R.QTY_DEFECT), 0) AS QUANTITY
                                        INTO #temp2
                                        FROM TB_R_SUPPLIER_QUALITY R
	                                        INNER JOIN dbo.TB_M_PART_INFO M ON M.PART_NO = R.PART_NO
                                        WHERE R.SUPPLIER_CD = '0001' AND DATEPART(YEAR, R.PROD_MONTH) = 2013
                                        GROUP BY SUBSTRING(CONVERT(varchar(11), R.PROD_MONTH, 113),4,3)


                                        SELECT T1.[MONTH]
	                                        ,ISNULL(T2.QUANTITY, 0) AS QUALITY
	                                        ,'5' AS PRICE
                                        FROM #temp1 T1 
	                                        LEFT OUTER JOIN #temp2 T2 ON T2.[MONTH] = T1.[MONTH]

                                        DROP TABLE #temp1
                                        DROP TABLE #temp2";
            //sqlSource15.Parameters.Add("@ProductionYear", System.Data.DbType.String, ProductionYear);
            //sqlSource15.Parameters.Add("@ProductionMonth", System.Data.DbType.String, ProductionMonth);
            //sqlSource15.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            //sqlSource15.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            #endregion

            #region SUB REPORT 30 - Dummy From Tabulasi Workability Problem
            Telerik.Reporting.Report rpt30;
            Telerik.Reporting.SqlDataSource sqlSource30;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\sRepTabulasi1.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt30 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep30 = (Telerik.Reporting.SubReport)rpt31.Items.Find("subReport14", true)[0];
            subRep30.ReportSource = rpt30;
            sqlSource30 = (Telerik.Reporting.SqlDataSource)rpt30.DataSource;
            sqlSource30.ConnectionString = connString;
            sqlSource30.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlSource30.SelectCommand = @"SELECT DISTINCT
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH) AS PROD_MONTH
	                                        ,H.PART_NO
	                                        ,M.PART_NAME
	
	                                        ,(SELECT SUM(D.QTY_DEFECT) FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_DEFECT
			
	                                        ,(SELECT SUM(D.QTY_RECEIVE) FROM TB_R_SUPPLIER_QUALITY_RECEIVE D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)) AS QTY_RECEIVE
	
	                                        ,(SELECT TOP 1 D.PROBLEM_DESC FROM TB_R_SUPPLIER_QUALITY D
		                                        WHERE D.SUPPLIER_CD = H.SUPPLIER_CD
			                                        AND D.PART_NO = H.PART_NO		
			                                        AND DATEPART(YEAR, D.PROD_MONTH) = DATEPART(YEAR, H.PROD_MONTH)
		                                        ORDER BY PROBLEM_DESC DESC) AS PROBLEM_DESC	
	
                                        FROM TB_R_SUPPLIER_QUALITY_RECEIVE H
                                        FULL OUTER JOIN TB_R_SUPPLIER_QUALITY HH
	                                        ON HH.SUPPLIER_CD = H.SUPPLIER_CD 
	                                        AND HH.PROD_MONTH = H.PROD_MONTH
                                        LEFT JOIN TB_M_PART_INFO M
	                                        ON M.PART_NO = H.PART_NO " +
                                        "WHERE H.SUPPLIER_CD = '" + suppCode + "' AND DATEPART(YEAR, H.PROD_MONTH) = " + year + " " +
                                        @"GROUP BY 
	                                        H.SUPPLIER_CD
	                                        ,DATEPART(YEAR, H.PROD_MONTH)
	                                        ,H.PART_NO
	                                        ,M.PART_NAME";
            #endregion

            #endregion

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport(typeFile == 1 ? "xls" : "pdf", rep0, null);
            return result.DocumentBytes;
        }
        #endregion

        #region PREVIEW REPORT
        public FileContentResult Reporting(string id)
        {
            string[] valReport = id.Split(';');

            int typeFile = Convert.ToInt16(valReport[0].ToString());
            string suppCode = valReport[1];
            string datePerformance = valReport[2];
            string year = Convert.ToDateTime(datePerformance).ToString("yyyy");
            string month = Convert.ToDateTime(datePerformance).ToString("MM");


            byte[] reportingByte = null;
            reportingByte = ReportOnByte(typeFile, year, month, suppCode);
            //string filePath = Path.Combine(Server.MapPath("~/Report/SupplierPerformance/TestReport.pdf"));
            //reportingByte = StreamFile(filePath);
            return File(reportingByte, "application/pdf");
        }
        #endregion

        #region PRINT TO XLS
        public void PrintReporting(string id)
        {
            string[] valReport = id.Split(';');

            int typeFile = Convert.ToInt16(valReport[0].ToString());
            string suppCode = valReport[1];
            string datePerformance = valReport[2];
            string year = Convert.ToDateTime(datePerformance).ToString("yyyy");
            string month = Convert.ToDateTime(datePerformance).ToString("MM");

            string FileNames = "SupplierPerformance" + suppCode + "_" + year + ".xls";

            byte[] reportingByte = null;
            reportingByte = ReportOnByte(typeFile, year, month, suppCode);

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(reportingByte.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(reportingByte);
            Response.End();
        }
        #endregion

        #endregion
    }
}
