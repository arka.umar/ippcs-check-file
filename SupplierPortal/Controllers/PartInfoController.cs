﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.PartInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;
using Portal.Models.Dock;

using Portal.Models.Part;
using Portal.Models.Supplier;

namespace Portal.Controllers
{
    public class PartInfoController : BaseController
    {
        public PartInfoController()
            : base("Part Info : ")
        {
        }

        protected override void StartUp()
        {

        }     

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<SupplierName> _supplierModel = null;
        private List<SupplierName> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierName>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<PartData> _partModel = null;
        private List<PartData> _PartModel
        {
            get
            {
                if (_partModel == null)
                    _partModel = new List<PartData>();

                return _partModel;
            }
            set
            {
                _partModel = value;
            }
        }
        
        protected override void Init()
        {
            IDBContext db = DbContext;

            PartInfoModel modelArsInfo = new PartInfoModel();
            modelArsInfo.PartInfoDatas = getPartInfo("","","","","");
            Model.AddModel(modelArsInfo);
        }

        [HttpGet]
        public void DownloadSheet(string SupplierCode,string DockCode, string PartNo)
        {
            string fileName = "PartInfo" + DateTime.Now + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "PartInfo", "OIHSupplierOptionPartInfo");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "PartInfo", "OIHDockCodePartInfo");
            
            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            IEnumerable<PartInfoData> listGeneral = db.Query<PartInfoData>("GetPartInfo", new object[] { SupplierCode, PartNo, DockCode, SupplierCodeLDAP, DockCodeLDAP });
            IExcelWriter exporter = ExcelWriter.GetInstance();
            exporter.Append(listGeneral, "PartInfo");
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();
        }

        public ActionResult PartialHeader()
        {
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "PartInfo", "OIHSupplierOptionPartInfo");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "PartInfo", "OIHDockCodePartInfo");

            ViewData["GridPart"] = GetAllPart();
            ViewData["GridDock"] = docks;
            return PartialView("PartialHeader");
        }

        public ActionResult PartialDetail()
        {
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "PartInfo", "OIHSupplierOptionPartInfo");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "PartInfo", "OIHDockCodePartInfo");
            
            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            PartInfoModel model = Model.GetModel<PartInfoModel>();
            model.PartInfoDatas = getPartInfo(Request.Params["SupplierPlantCode"],
                                              Request.Params["DockCode"],
                                              Request.Params["PartNo"],
                                              SupplierCodeLDAP,
                                              DockCodeLDAP);

            Model.AddModel(model);

            return PartialView("PartialDetail", Model);
        }

        private List<PartData> GetAllPart()
        {
            IDBContext db = DbContext;

            List<PartData> partModel = new List<PartData>();
            partModel = db.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
            db.Close();

            return partModel;

        }

        protected List<PartInfoData> getPartInfo(string suppCdParam, string dockCdParam, string partNoParam, 
                                                 string SupplierCodeLdap, string DockCdLdap)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<PartInfoData> listTooltipDatas = new List<PartInfoData>();
            listTooltipDatas = db.Query<PartInfoData>("GetPartInfo", new object[] { suppCdParam,partNoParam,dockCdParam,SupplierCodeLdap,DockCdLdap}).ToList();
            db.Close();
            return listTooltipDatas;
        }

        public ActionResult PartialHeaderSupplierLookupGridPartInfo()
        {
            TempData["GridName"] = "OIHSupplierOptionPartInfo";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "PartInfo", "OIHSupplierOptionPartInfo");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);
        }
        
        public ActionResult PartialSupplierLookupGridPartInfo()
        {
            TempData["GridName"] = "PartInfoHSupplierGridLookup";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "PartInfo", "PartInfoHSupplierGridLookup");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);
        }
        
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> SISupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            SISupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return SISupplierModel;
        }

        public ActionResult PartialPopupPartInfo()
        {
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "PartInfo", "OIHDockCodePartInfo");
          
            ViewData["GridPart"] = GetAllPart();
            ViewData["GridDock"] = docks;
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "PartInfo", "OIHSupplierOptionPartInfo");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }
            return PartialView("PopupNew", null);
        }

        public ActionResult PartialHeaderPartLookupGridPartInfo()
        {
            TempData["GridName"] = "OIHPartOptionPartInfo";
            ViewData["GridPart"] = GetAllPart();
            
            return PartialView("GridLookup/PartialGrid", ViewData["GridPart"]);
        }

        public ActionResult PartialHeaderDockGridHeaderSupplierInfo()
        {
            TempData["GridName"] = "OIHDockCodePartInfo";
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "PartInfo", "OIHDockCodePartInfo");
            ViewData["GridDock"] = docks;
            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);
        }

        public ActionResult PartialDockGridHeaderSupplierInfo()
        {
            TempData["GridName"] = "PartInfoHDockGridLookup";
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "PartInfo", "PartInfoHDockGridLookup");
            ViewData["GridDock"] = docks;
            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);
        }
        
        private List<DockData> GetAllDock()
        {
            List<DockData> PartIndoDockModel = new List<DockData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return PartIndoDockModel;
        }
       
        public string AddNewPart(string Supplier, string DockCd, string PartNo, string KanbanQty,
                                       string StartDate, string EndDate, string Partname, string KanbanNo, 
                                       string PackType, string Mode)
        {
            string User = AuthorizedUser.Username;
            IDBContext db = DbContext;

            string ResultQuery = "SUCCESS";
            string[] Suppliers = Supplier.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");
            string Flag="";
            DateTime? vTo ;
            DateTime? vFrom;

            if (StartDate == "")
            {
                vTo = null;
            }
            else
            {
                vTo = DateTime.ParseExact(StartDate, "dd/MM/yyyy", null);
                Flag="Y";
            }
            
            if (EndDate == "")
            {
                vFrom = null;
            }
            else
            {
                vFrom = DateTime.ParseExact((EndDate), "dd/MM/yyyy", null);
                Flag = "Y";
            }
            
            try
            {
                ResultQuery = db.ExecuteScalar<string>("UpdatePartInfo", new object[] 
                                            {
                                               SupplierCode,
                                               SupplierPlant,
                                               DockCd,
                                               PartNo,
                                               KanbanQty,
                                               vTo,
                                               vFrom,
                                               User,
                                               Flag,
                                               Partname,
                                               KanbanNo,
                                               PackType,
                                               Mode
                                            });

                TempData["GridName"] = "PartialDetailPartInfo";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return ResultQuery;
        }

        public ActionResult DeletePartInfo(string SupplierCode, string SupplierPlant,string PartNo, string DockCd)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeletePartInfo", new object[] 
                {
                   SupplierCode,
                   SupplierPlant,
                   PartNo,
                   DockCd,
                });
                TempData["GridName"] = "PartialDetailPartInfo";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
    }
}