﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models.Globals;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Portal.Models.CounterReceiving;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using Toyota.Common.Web.MessageBoard;

namespace Portal.Controllers
{
    public class CounterReceivingController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        public CounterReceivingController()
            : base("Invoice Receiving")
        {
            PageLayout.UseMessageBoard = true;
        }
         
        protected override void StartUp()
        {

        }
        protected override void Init()
        {
            //PageLayout.UseSlidingBottomPane = true;
            //CounterReceivingModel mdl = new CounterReceivingModel();
            //List<CounterReceivingDetail> lst = new List<CounterReceivingDetail>();
            
            //for (int i = 1; i <= 100; i++)
            //{
            //    mdl.CounterReceivings.Add(new CounterReceiving {
            //        CertificateID = "C001",
            //        SupplierCode = "951NTC1211" + i.ToString("##"),
            //        SupplierName = "NUSA CORP.",
            //        InvoiceNo = "00" + i + "/TMMIN/12",
            //        Currency = "IDR",
            //        InvoiceAmount = "476,507,630",
            //        InvoiceTaxNo = "875NTC121" + i.ToString("##"),
            //        InvoiceTaxAmount = "500,000",
            //        InvoiceTaxType = "By Invoice",
            //        ReceiveDate = String.Format("{0:dd/MM/yyyy}", DateTime.Now),
            //        Status = i % 2 != 0 ? "0" : "1"
            //    });
            //}

            //mdl.CounterReceivingDetails = lst;

            //Rejectmodel rejectmodel = new Rejectmodel();
            //rejectmodel.Rejects.Add(new Reject
            //{


            //});

            //Model.AddModel(mdl);
            //Model.AddModel(rejectmodel);
            LstCurr();
            LstSupp();
            CounterReceivingModel mdl = new CounterReceivingModel();
            mdl.CounterReceivings = GetListGrid();
            Model.AddModel(mdl);
            
        }

        #region Counter Receiving controller
        #region View controller
        public ActionResult PartialCounterReceivingDetail()
        {
            #region Required model in partial page : for OrderInquiryHeaderPartial
            //List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");

            //_InvoiceInquirySupplierModel = suppliers;

            //Model.AddModel(_InvoiceInquirySupplierModel);
            #endregion

            return PartialView("HeaderDetailCounterReceiving", Model);
        }
        #endregion
        #endregion

        public ActionResult CounterReceivingPartial()
        {
            string CertificateId = Request.Params["CertificateID"];
            //SaveAndUpdate(CertificateId);
            CounterReceivingModel mdl = new CounterReceivingModel();
            mdl.CounterReceivings = GetListGrid();
            Model.AddModel(mdl);
            return PartialView("CounterReceivingPartial", Model);
        }
        public ActionResult Preview()
        {
            return PartialView("RejectPopup", Model);
        }

        #region GetDataHeader
        public ContentResult GetDataHeader(string CertificateId)
        {
            String resultMessage = "";
            IDBContext db = DbContext;

            if (CertificateId != "")
            {
                 resultMessage = db.SingleOrDefault<string>("GetHeaderDataInvoiceReceiving", new object[] { CertificateId });
                 if (resultMessage == null)
                 {
                     resultMessage = db.SingleOrDefault<string>("CheckInvoiceUploadHistory", new object[] { CertificateId });
                 }
            }
            db.Close();
            return Content(resultMessage.ToString());
        }
        #endregion

        #region GetDataGrid
        private List<CounterReceiving> GetListGrid()
        {
           
            List<CounterReceiving> CounterListDetail = new List<CounterReceiving>();
            IDBContext db = DbContext;
            try
            {
                var query = db.Query<CounterReceiving>("GetListCounterDetail", new object[] { });
                if (query.Any())
                    {
                        foreach (var q in query)
                            {
                                CounterListDetail.Add(new CounterReceiving
                                {
                                  CertificateID = q.CertificateID,
                                  SupplierName = q.SupplierName,
                                  SupplierCode = q.SupplierCode,
                                  Currency = q.Currency,
                                  InvoiceAmount = q.InvoiceAmount,
                                  InvoiceNo = q.InvoiceNo,
                                  InvoiceTaxAmount  = q.InvoiceTaxAmount,
                                  InvoiceTaxNo = q.InvoiceTaxNo,
                                  Status = q.Status,
                                  //ReceiveDate = q.ReceiveDate == null ? "" : DateTime.Parse(q.ReceiveDate).ToString("dd.MM.yyyy"),
                                  SubmitBy = q.SubmitBy,
                                  SubmitDate = q.SubmitDate
                                });
                            }
                    }
                return CounterListDetail;
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return CounterListDetail;
        }
        #endregion

        #region Download Data
        public void Downloadxls(string CerID, string Suppcode, string SuppName, string Curr, string Status,
                                string InvNo, string InvAmt, string InvTaxNo, string InvTaxAmt, string HFDate, string SubmitBy)
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;


            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\InvoiceAndPayment\CounterReceiving.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource.SelectCommand = "SP_Rep_Counter_Receiving";

            sqlDataSource.Parameters.Add("@CerfId", System.Data.DbType.String, CerID);
            sqlDataSource.Parameters.Add("@SupCD", System.Data.DbType.String, Suppcode);
            sqlDataSource.Parameters.Add("@SupName", System.Data.DbType.String, SuppName);
            sqlDataSource.Parameters.Add("@InvNo", System.Data.DbType.String, InvNo);
            sqlDataSource.Parameters.Add("@InvAmount", System.Data.DbType.Double, double.Parse(InvAmt));
            sqlDataSource.Parameters.Add("@InvTaxNo", System.Data.DbType.String, InvTaxNo);
            sqlDataSource.Parameters.Add("@InvTaxAmount", System.Data.DbType.Double, double.Parse(InvTaxAmt));
            //sqlDataSource.Parameters.Add("@SubmitDate", System.Data.DbType.DateTime, HFDate);
            sqlDataSource.Parameters.Add("@SubmitBy", System.Data.DbType.String, SubmitBy);
            sqlDataSource.Parameters.Add("@Curr", System.Data.DbType.String, Curr.Trim());
            sqlDataSource.Parameters.Add("@Status", System.Data.DbType.String, Status.Trim());

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string tgl = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
            string fileName = "InvoiceReceiving_" + tgl + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
            rpt.Dispose();
        }
        #endregion


        public ContentResult checkStatusInvoice(string supplier, string invoiceNo, string invoiceTaxNo) 
        {
            IDBContext db = DbContext;

            string check = String.Empty;

            try
            {
                check = db.ExecuteScalar<string>("checkStatusInvoice", new object[]{supplier, invoiceNo});

                // check data TB_M_DIFFERENCE_INV_AMOUNT AND UPDATE TB_R_INV_UPLOAD SET AUTO_POSTING = 'N'
                if (check == "accepted")
                {
                    check = db.ExecuteScalar<string>("CheckDifferenceInvAmount", new object[] { supplier, invoiceNo, invoiceTaxNo });
                }
                
            }
            catch (Exception E)
            {
                check = "error";
            }
            finally {
                db.Close();         
            }

            return Content(check);
        
        
        
        }


         public void SubmitData(string CertificateID, string Reason, int Sta, string InvoiceNo, string Supplier, string CreatedBy
             , string InvoiceTaxamount, string InvoiceAmount, string InvoiceTaxNo, string Curr, string docdate)
         {
             String Result = "";
             //long InvTaxAmount = long.Parse(InvoiceTaxamount.Replace(",",""));
             //long InvAmount = long.Parse(InvoiceAmount.Replace(",", ""));
             string user = AuthorizedUser.Username;
             IDBContext db = DbContext;
             try
             {
                 string[] sup = Supplier.Split(':');
                 string baselinedt = "";
                 if (Sta == -2 && Reason != "")
                 {
                     MessageBoardMessage boardMessage = new MessageBoardMessage();
                     boardMessage.GroupId = db.ExecuteScalar<int>("GetLastGroupID", new object[] { "CounterReceivingMB_" + sup[0] + InvoiceNo });
                     boardMessage.BoardName = "CounterReceivingMB_" + sup[0] + InvoiceNo;
                     boardMessage.Sender = user;
                     boardMessage.Recipient = sup[0];
                     boardMessage.CarbonCopy = false;
                     boardMessage.Text = Reason;
                     boardMessage.Date = DateTime.Now;
                     IMessageBoardService boardService = MessageBoardService.GetInstance();
                     boardService.Save(boardMessage);
                     //string NotifID = "CounterReceivingMB_" + sup[0] + InvoiceNo;

                     //string tgl = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");
                     //var Insert = db.ExecuteScalar<string>("MessageBoard_Insert", new object[] { 333, NotifID, user, sup[0], Reason, tgl, 0, 0, user, tgl });

                     //---send email
                     var tesResult = db.ExecuteScalar<string>("SERejectInvoiveReceiving", new object[] { "52001", CreatedBy, sup[1], InvoiceNo, Reason});

                     //---end send email
                 }
                 else
                 {
                     //set baseline date
                     baselinedt = db.SingleOrDefault<string>("UpdateBaselineInvoiceReceiving", new object[] { docdate });
                 }
                

                 Result = db.ExecuteScalar<string>("UpdateCounterReceiving", new object[] { CertificateID, sup[0], InvoiceNo,
                                                                                Curr, InvoiceAmount, InvoiceTaxNo, InvoiceTaxamount, user, Sta, Reason, baselinedt});
                
             }
             catch (Exception exc) { throw exc; }
             finally { db.Close(); }
         
         }
        

            

         private string SetNoticeIconString(string val)
         {
             string img = string.Empty;
             string imageBytes1 = "~/Content/Images/notice_open_icon.png";
             string imageBytes2 = "~/Content/Images/notice_new_icon.png";
             string imageBytes3 = "~/Content/Images/notice_close_icon.png";
             string imageBytes4 = "~/Content/Images/notice_add_icon.png";

             if (Convert.ToInt16(val) % 4 == 0)
             {
                 img = imageBytes4;
             }
             else if (Convert.ToInt16(val) % 4 == 1)
             {
                 img = imageBytes2;
             }
             else if (Convert.ToInt16(val) % 4 == 2)
             {
                 img = imageBytes1;
             }
             else if (Convert.ToInt16(val) % 4 == 3)
             {
                 img = imageBytes3;
             }
             return img;
         }

         public IEnumerable<Currency> LstCurr()
         {
             IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
             IEnumerable<Currency> QueryData = db.Query<Currency>("GetAllCurrency", new Object[] { "", "" });
             db.Close();
             Model.AddModel(QueryData);
             return QueryData;
         }

         public IEnumerable<Supplier> LstSupp()
         {
             IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
             IEnumerable<Supplier> QueryDataSup = db.Query<Supplier>("GetAllSuppFilter", new Object[] {  });
             db.Close();
             Model.AddModel(QueryDataSup);
             return QueryDataSup;
         }
    }

    
}
