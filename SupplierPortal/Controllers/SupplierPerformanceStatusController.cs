﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.Globals;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Portal.Models.SupplierPerformanceStatus;
//add by fid.goldy
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using Telerik.Reporting.Charting;
//end add of fid.goldy


namespace Portal.Controllers
{
    public class SupplierPerformanceStatusController : BaseController
    {
        //
        // GET: /SupplierPerformanceStatus/
        public SupplierPerformanceStatusController()
            : base("Supplier Performance Status")
        {
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void StartUp()
        {
            //set gridlookup Supplier
            TempData["SupplierCode"] = SupplierCode;
            TempData["ReleasedFlag"] = "0";
            Model.AddModel(Suppliers);
        }

        protected override void Init()
        {

        }

        //Add by FID.Goldy
        #region CREATE REPORT ON BYTE TYPE
        private byte[] ReportOnByte(int typeFile, string year, string month, string suppCode)
        {
            string connString = DBContextNames.DB_PCS;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            #region Get TMMIN TARGET
            SPTMMINTarget TMMINTarget = new SPTMMINTarget();
            IDBContext db = DbContext;
            try
            {
                TMMINTarget = db.SingleOrDefault<SPTMMINTarget>("SupplierPerformanceGetTMMINTarget", new object[] { suppCode, year, month });
            }
            catch (Exception exc) { throw exc; }
            db.Close();
            #endregion

            #region REPORT - MAIN
            Telerik.Reporting.Report repMaster;
            Telerik.Reporting.SqlDataSource sqlSourceMaster;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\repMasterSupplierPerformance.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                repMaster = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlSourceMaster = (Telerik.Reporting.SqlDataSource)repMaster.DataSource;
            sqlSourceMaster.ConnectionString = connString;
            sqlSourceMaster.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlSourceMaster.SelectCommand = "SupplierPerformance_ReportHeaderData";
            sqlSourceMaster.Parameters.Add("@suppCode", System.Data.DbType.String, suppCode);
            sqlSourceMaster.Parameters.Add("@year", System.Data.DbType.String, year);
            sqlSourceMaster.Parameters.Add("@month", System.Data.DbType.String, month);
            #endregion

            #region SUB REPORT - SAFETY
            Telerik.Reporting.Report rptSafetyMaster = GenerateSubReport(
                                                        repMaster,
                                                        @"\Report\SupplierPerformance\repSafetyMasterSupplierPerformance.trdx",
                                                        "subRepSafety",
                                                        "SupplierPerformance_ReportSafetyJudgement",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptSafetySubmission = GenerateSubReport(
                                                            rptSafetyMaster,
                                                            @"\Report\SupplierPerformance\repSafetySubmissionSupplierPerformance.trdx",
                                                            "subRepSafetySubmission",
                                                            "SupplierPerformance_ReportSafetySubmission",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptSafetyFatal = GenerateSubReport(
                                                        rptSafetyMaster,
                                                        @"\Report\SupplierPerformance\repSafetyFatalSupplierPerformance.trdx",
                                                        "subRepSafetyFatal",
                                                        "SupplierPerformance_ReportSafetyFatalAccident",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptSafetyAbsent = GenerateSubReport(
                                                        rptSafetyMaster,
                                                        @"\Report\SupplierPerformance\repSafetyAbsentSupplierPerformance.trdx",
                                                        "subRepSafetyAbsent",
                                                        "SupplierPerformance_ReportSafetyAbsent",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptSafetySmallInjured = GenerateSubReport(
                                                            rptSafetyMaster,
                                                            @"\Report\SupplierPerformance\repSafetySmallInjuredtSupplierPerformance.trdx",
                                                            "subRepSafetySmallInjured",
                                                            "SupplierPerformance_ReportSafetySmallInjured",
                                                            suppCode, year, month);
            #endregion

            #region SUB REPORT - QUALITY
            Telerik.Reporting.Report rptQualityMaster = GenerateSubReport(
                                                        repMaster,
                                                        @"\Report\SupplierPerformance\repQualityMasterSupplierPerformance.trdx",
                                                        "subRepQuality",
                                                        "SupplierPerformance_ReportQualityJudgement",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptQualityPieByArea = GenerateReportPieChart(
                                                            rptQualityMaster,
                                                            @"\Report\SupplierPerformance\repQualityDefectAreaChartSupplierPerformance.trdx",
                                                            "subRepDefectAreaChart",
                                                            "SupplierPerformanceQualityPieByArea",
                                                            suppCode, year, month,
                                                            "QualityPieByArea", "QualityPieByArea", "QualityPieByArea",
                                                            "", "", "");

            Telerik.Reporting.Report rptQualityPieByModel = GenerateReportPieChart(
                                                            rptQualityMaster,
                                                            @"\Report\SupplierPerformance\repQualityDefectModelChartSupplierPerformance.trdx",
                                                            "subRepDefectModelChart",
                                                            "SupplierPerformanceQualityPieByModel",
                                                            suppCode, year, month,
                                                            "QualityPieByModel", "QualityPieByModel", "QualityPieByModel",
                                                            "", "", "");

            Telerik.Reporting.Report rptQualityGraph = GenerateReportGraph(
                                                        rptQualityMaster,
                                                        @"\Report\SupplierPerformance\repQualityGraphSupplierPerformance.trdx",
                                                        "subRepQualityGraph",
                                                        "SupplierPerformanceQualityGraphData",
                                                        suppCode, year, month,
                                                        "QualityGraph", "QualityChart", "QualityLine",
                                                        "PPM", "Month", TMMINTarget.QUALITY, 8, 7);

            Telerik.Reporting.Report rptQualityPartReceived = GenerateSubReport(
                                                                rptQualityMaster,
                                                                @"\Report\SupplierPerformance\repQualityPartReceivedSupplierPerformance.trdx",
                                                                "subRepQualityPartReceived",
                                                                "SupplierPerformance_ReportPartReceived",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptQualityPartNG = GenerateSubReport(
                                                        rptQualityMaster,
                                                        @"\Report\SupplierPerformance\repQualityPartNGSupplierPerformance.trdx",
                                                        "subRepQualityPartNG",
                                                        "SupplierPerformance_ReportQualityPartNG",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptQualityPPM = GenerateSubReport(
                                                    rptQualityMaster,
                                                    @"\Report\SupplierPerformance\repQualityPPMSupplierPerformance.trdx",
                                                    "subRepQualityPPM",
                                                    "SupplierPerformance_ReportQualityPPM",
                                                    suppCode, year, month);

            Telerik.Reporting.Report rptQualityDetailProblem = GenerateSubReport(
                                                                rptQualityMaster,
                                                                @"\Report\SupplierPerformance\repQualityDetailProblemSupplierPerformance.trdx",
                                                                "subRepDetailProblem",
                                                                "SupplierPerformance_ReportQualityDetailProblem",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPORT - WARRANTY
            Telerik.Reporting.Report rptwarrantyMaster = GenerateSubReport(
                                                            repMaster,
                                                            @"\Report\SupplierPerformance\repWarrantySupplierPerformance.trdx",
                                                            "subRepWarranty",
                                                            "SupplierPerformance_ReportWarrantyTWC",
                                                            suppCode, year, month);
            #endregion

            #region SUB REPORT - DELIVERY AND SERVICE PART
            #region SUB REPORT - DELIVERY AND SERVICE MASTER
            Telerik.Reporting.Report repDeliveryAndServicePart = GenerateSubReport(
                                                                repMaster,
                                                                @"\Report\SupplierPerformance\repDeliveryAndServicesPartMasterSupplierPerformance.trdx",
                                                                "subRepDeliveryAndSafety",
                                                                "SupplierPerformance_ReportComment",
                                                                suppCode, year, month);

            #endregion

            #region SUB REPORT - SHORTAGE
            Telerik.Reporting.Report rptDeliveryShortage = GenerateSubReport(
                                                            repDeliveryAndServicePart,
                                                            @"\Report\SupplierPerformance\repDeliveryShortageSupplierPerformance.trdx",
                                                            "subRepShortage",
                                                            "SupplierPerformance_ReportShortageJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageGraph = GenerateReportGraph(
                                                                rptDeliveryShortage,
                                                                @"\Report\SupplierPerformance\repDeliveryShortageGraphSupplierPerformance.trdx",
                                                                "subRepShortageGraph",
                                                                "SupplierPeformanceDeliveryShortageGraphData",
                                                                suppCode, year, month,
                                                                "ShortageGraph", "ShortageChart", "ShortageLine",
                                                                "PPM", "Month", TMMINTarget.SHORTAGE);

            Telerik.Reporting.Report rptDeliveryShortagePartReceived = GenerateSubReport(
                                                                        rptDeliveryShortage,
                                                                        @"\Report\SupplierPerformance\repDeliveryPartReceivedSupplierPerformance.trdx",
                                                                        "subRepDeliveryPartReceive",
                                                                        "SupplierPerformance_ReportPartReceived",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortagePartPCS = GenerateSubReport(
                                                                    rptDeliveryShortage,
                                                                    @"\Report\SupplierPerformance\repDeliveryShortagePCSSupplierPerformance.trdx",
                                                                    "subRepShortagePcs",
                                                                    "SupplierPerformance_ReportShortagePcs",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortagePPM = GenerateSubReport(
                                                                rptDeliveryShortage,
                                                                @"\Report\SupplierPerformance\repDeliveryShortagePPMSupplierPerformance.trdx",
                                                                "subRepShortagePPM",
                                                                "SupplierPerformance_ReportShortagePPM",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageDetail = GenerateSubReport(
                                                                    rptDeliveryShortage,
                                                                    @"\Report\SupplierPerformance\repDeliveryShortageDetailSupplierPerformance.trdx",
                                                                    "subRepDetailShortage",
                                                                    "SupplierPerformance_ReportShortageDetailShortage",
                                                                    suppCode, year, month);
            #endregion

            #region SUB REPORT - MISSPART
            Telerik.Reporting.Report rptDeliveryMisspart = GenerateSubReport(
                                                            repDeliveryAndServicePart,
                                                            @"\Report\SupplierPerformance\repDeliveryMisspartSupplierPerformance.trdx",
                                                            "subRepMisspart",
                                                            "SupplierPerformance_ReportMisspartJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartGraph = GenerateReportGraph(
                                                                rptDeliveryMisspart,
                                                                @"\Report\SupplierPerformance\repDeliveryMisspartGraphSupplierPerformance.trdx",
                                                                "subRepMisspartGraph",
                                                                "SupplierPeformanceDeliveryMisspartGraphData",
                                                                suppCode, year, month,
                                                                "MisspartGraph", "MisspartChart", "MisspartLine",
                                                                "PPM", "Month", TMMINTarget.MISSPART);

            Telerik.Reporting.Report rptDeliveryMisspartPartReceived = GenerateSubReport(
                                                                        rptDeliveryMisspart,
                                                                        @"\Report\SupplierPerformance\repDeliveryPartReceivedSupplierPerformance.trdx",
                                                                        "subRepDeliveryPartReceive",
                                                                        "SupplierPerformance_ReportPartReceived",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartPartPCS = GenerateSubReport(
                                                                    rptDeliveryMisspart,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartPCSSupplierPerformance.trdx",
                                                                    "subRepMisspartPcs",
                                                                    "SupplierPerformance_ReportMisspartPcs",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartPartPPM = GenerateSubReport(
                                                                    rptDeliveryMisspart,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartPPMSupplierPerformance.trdx",
                                                                    "subRepMisspartPPM",
                                                                    "SupplierPerformance_ReportMisspartPPM",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartDetail = GenerateSubReport(
                                                                    rptDeliveryMisspart,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartDetailSupplierPerformance.trdx",
                                                                    "subRepDetailMisspart",
                                                                    "SupplierPerformance_ReportMisspartDetailMisspart",
                                                                    suppCode, year, month);
            #endregion

            #region SUB REPORT - ONTIME
            Telerik.Reporting.Report rptDeliveryOntime = GenerateSubReport(
                                                            repDeliveryAndServicePart,
                                                            @"\Report\SupplierPerformance\repDeliveryOntimeSupplierPerformance.trdx",
                                                            "subRepOntime",
                                                            "SupplierPerformance_ReportOntimeJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimeGraph = GenerateReportGraph(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeGraphSupplierPerformance.trdx",
                                                                "subRepOntimeGraph",
                                                                "SupplierPeformanceDeliveryOntimeGraphData",
                                                                suppCode, year, month,
                                                                "OntimeGraph", "OntimeChart", "OntimeLine",
                                                                "ON TIME", "Month", TMMINTarget.ONTIME);

            Telerik.Reporting.Report rptDeliveryOntimeSupply = GenerateSubReport(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeSupplySupplierPerformance.trdx",
                                                                "subRepOntimeSupply",
                                                                "SupplierPerformance_ReportOntimeSupply",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimeDelay = GenerateSubReport(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeDelaySupplierPerformance.trdx",
                                                                "subRepOntimeDelay",
                                                                "SupplierPerformance_ReportOntimeDelay",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimePercentage = GenerateSubReport(
                                                                    rptDeliveryOntime,
                                                                    @"\Report\SupplierPerformance\repDeliveryOntimePercentageSupplierPerformance.trdx",
                                                                    "subRepOntimePercentage",
                                                                    "SupplierPerformance_ReportOntimePercentage",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimeDetail = GenerateSubReport(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeDetailSupplierPerformance.trdx",
                                                                "subRepDetailOntime",
                                                                "SupplierPerformance_ReportOntimeDetailOntime",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPORT - LINESTOP
            Telerik.Reporting.Report rptDeliveryLinestop = GenerateSubReport(
                                                            repDeliveryAndServicePart,
                                                            @"\Report\SupplierPerformance\repDeliveryLinestopSupplierPerformance.trdx",
                                                            "subRepLinestop",
                                                            "SupplierPerformance_ReportLineStopJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryLinestopGraph = GenerateReportGraph(
                                                                rptDeliveryLinestop,
                                                                @"\Report\SupplierPerformance\repDeliveryLinestopGraphSupplierPerformance.trdx",
                                                                "subRepLinestopGraph",
                                                                "SupplierPeformanceDeliveryLinestopGraphData",
                                                                suppCode, year, month,
                                                                "LinestopGraph", "LinestopChart", "LinestopLine",
                                                                "MINUTES", "Month", TMMINTarget.LINESTOP);

            Telerik.Reporting.Report rptDeliveryLinestopMinute = GenerateSubReport(
                                                                rptDeliveryLinestop,
                                                                @"\Report\SupplierPerformance\repDeliveryLinestopMinuteSupplierPerformance.trdx",
                                                                "subRepLinestopMinute",
                                                                "SupplierPerformance_ReportLinestopMinute",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryLinestopDetail = GenerateSubReport(
                                                                rptDeliveryLinestop,
                                                                @"\Report\SupplierPerformance\repDeliveryLinestopDetailSupplierPerformance.trdx",
                                                                "subRepDetailOntime",
                                                                "SupplierPerformance_ReportLinestopDetailLinestop",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPORT - CRIPPLE
            Telerik.Reporting.Report rptDeliveryCripple = GenerateSubReport(
                                                            repDeliveryAndServicePart,
                                                            @"\Report\SupplierPerformance\repDeliveryCrippleSupplierPerformance.trdx",
                                                            "subRepCripple",
                                                            "SupplierPerformance_ReportCrippleJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryCrippleGraph = GenerateReportGraph(
                                                                rptDeliveryCripple,
                                                                @"\Report\SupplierPerformance\repDeliveryCrippleGraphSupplierPerformance.trdx",
                                                                "subRepCrippleGraph",
                                                                "SupplierPeformanceDeliveryCrippleGraphData",
                                                                suppCode, year, month,
                                                                "CrippleGraph", "CrippleChart", "CrippleLine",
                                                                "UNIT", "Month", TMMINTarget.CRIPPLE);

            Telerik.Reporting.Report rptDeliveryCrippleUnit = GenerateSubReport(
                                                                rptDeliveryCripple,
                                                                @"\Report\SupplierPerformance\repDeliveryCrippleUnitSupplierPerformance.trdx",
                                                                "subRepCrippleUnit",
                                                                "SupplierPerformance_ReportCrippleUnit",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryCrippleDetail = GenerateSubReport(
                                                                rptDeliveryCripple,
                                                                @"\Report\SupplierPerformance\repDeliveryCrippleDetailSupplierPerformance.trdx",
                                                                "subRepDetailCripple",
                                                                "SupplierPerformance_ReportCrippleDetailCripple",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPORT - DELIVERY DUE DATE
            Telerik.Reporting.Report rptServicePartDDD = GenerateSubReport(
                                                            repDeliveryAndServicePart,
                                                            @"\Report\SupplierPerformance\repServicePartDeliveryDueDateSupplierPerformance.trdx",
                                                            "subRepDDD",
                                                            "SupplierPerformance_ReportServicePartDeliveryDueDateJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartDDDGraph = GenerateReportGraph(
                                                                rptServicePartDDD,
                                                                @"\Report\SupplierPerformance\repServicePartDeliveryDueDateGraphSupplierPerformance.trdx",
                                                                "subRepDDDGraph",
                                                                "SupplierPeformanceServicePartDDDGraphData",
                                                                suppCode, year, month,
                                                                "DDDGraph", "DDDChart", "DDDLine",
                                                                "ON TIME", "Month", TMMINTarget.DDD);

            Telerik.Reporting.Report rptServicePartDDDOri = GenerateSubReport(
                                                            rptServicePartDDD,
                                                            @"\Report\SupplierPerformance\repServicePartDeliveryDueDateOriSupplierPerformance.trdx",
                                                            "subRepDDDOri",
                                                            "SupplierPerformance_ReportServicePartDeliveryDueDateOri",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartDDDAdj = GenerateSubReport(
                                                            rptServicePartDDD,
                                                            @"\Report\SupplierPerformance\repServicePartDeliveryDueDateAdjSupplierPerformance.trdx",
                                                            "subRepDDDAdj",
                                                            "SupplierPerformance_ReportServicePartDeliveryDueDateAdj",
                                                            suppCode, year, month);
            #endregion

            #region SUB REPORT - SP QUALITY
            Telerik.Reporting.Report rptServicePartQuality = GenerateSubReport(
                                                            repDeliveryAndServicePart,
                                                            @"\Report\SupplierPerformance\repServicePartQualitySupplierPerformance.trdx",
                                                            "subRepSPQuality",
                                                            "SupplierPerformance_ReportServicePartQualityJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartQualityGraph = GenerateReportGraph(
                                                                    rptServicePartQuality,
                                                                    @"\Report\SupplierPerformance\repServicePartQualityGraphSupplierPerformance.trdx",
                                                                    "subRepSPQualityGraph",
                                                                    "SupplierPeformanceServicePartQualityGraphData",
                                                                    suppCode, year, month,
                                                                    "SPQualityGraph", "SPQualityChart", "SPQualityLine",
                                                                    "PPM", "Month", TMMINTarget.SPQUALITY);

            Telerik.Reporting.Report rptServicePartQualityPartReceived = GenerateSubReport(
                                                                        rptServicePartQuality,
                                                                        @"\Report\SupplierPerformance\repServicePartPartReceivedSupplierPerformance.trdx",
                                                                        "subRepSPQualityPartReceived",
                                                                        "SupplierPerformance_ReportServicePartQualityPartReceive",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptServicePartQualityPartNG = GenerateSubReport(
                                                                    rptServicePartQuality,
                                                                    @"\Report\SupplierPerformance\repServicePartQualityNGSupplierPerformance.trdx",
                                                                    "subRepSPQualityNG",
                                                                    "SupplierPerformance_ReportServicePartQualityPartNG",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptServicePartQualityPPM = GenerateSubReport(
                                                                rptServicePartQuality,
                                                                @"\Report\SupplierPerformance\repServicePartQualityPPMSupplierPerformance.trdx",
                                                                "subRepSPQualityPPM",
                                                                "SupplierPerformance_ReportServicePartQualityPPM",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPOST - WORKABILITY
            Telerik.Reporting.Report rptServicePartWorkability = GenerateSubReport(
                                                                repDeliveryAndServicePart,
                                                                @"\Report\SupplierPerformance\repServicePartWorkabilitySupplierPerformance.trdx",
                                                                "subRepSPWorkability",
                                                                "SupplierPerformance_ReportServicePartQualityJudgement",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptServicePartWorkabilityGraph = GenerateReportGraph(
                                                                        rptServicePartWorkability,
                                                                        @"\Report\SupplierPerformance\repServicePartWorkabilityGraphSupplierPerformance.trdx",
                                                                        "subRepSPWorkabilityGraph",
                                                                        "SupplierPeformanceServicePartWorkabilityGraphData",
                                                                        suppCode, year, month,
                                                                        "SPWorkabilityGraph", "SPWorkabilityChart", "SPWorkabilityLine",
                                                                        "PPM", "Month", TMMINTarget.WORKABILITY);

            Telerik.Reporting.Report rptServicePartWorkabilityPartReceived = GenerateSubReport(
                                                                            rptServicePartWorkability,
                                                                            @"\Report\SupplierPerformance\repServicePartPartReceivedSupplierPerformance.trdx",
                                                                            "subRepSPWorkabilityPartReceived",
                                                                            "SupplierPerformance_ReportServicePartQualityPartReceive",
                                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartWorkabilityPartNG = GenerateSubReport(
                                                                        rptServicePartWorkability,
                                                                        @"\Report\SupplierPerformance\repServicePartWorkabilityNGSupplierPerformance.trdx",
                                                                        "subRepSPWorkabilityNG",
                                                                        "SupplierPerformance_ReportServicePartWorkabilityPartNG",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptServicePartWorkabilityPPM = GenerateSubReport(
                                                                    rptServicePartWorkability,
                                                                    @"\Report\SupplierPerformance\repServicePartWorkabilityPPMSupplierPerformance.trdx",
                                                                    "subRepSPWorkabilityPPM",
                                                                    "SupplierPerformance_ReportServicePartWorkabilityPPM",
                                                                    suppCode, year, month);
            #endregion
            #endregion

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport(typeFile == 1 ? "xls" : "pdf", repMaster, null);
            return result.DocumentBytes;
        }
        //add riani (20221021
        #region New Report
        private byte[] ReportOnByteNew(int typeFile, string year, string month, string suppCode)
        {
            string connString = DBContextNames.DB_PCS;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            #region Get TMMIN TARGET
            SPTMMINTarget TMMINTarget = new SPTMMINTarget();
            IDBContext db = DbContext;
            try
            {
                TMMINTarget = db.SingleOrDefault<SPTMMINTarget>("SupplierPerformanceGetTMMINTarget", new object[] { suppCode, year, month });
            }
            catch (Exception exc) { throw exc; }
            db.Close();
            #endregion

            #region REPORT - MAIN
            Telerik.Reporting.Report repMaster;
            Telerik.Reporting.SqlDataSource sqlSourceMaster;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierPerformance\repMasterSupplierPerformanceNew.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                repMaster = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlSourceMaster = (Telerik.Reporting.SqlDataSource)repMaster.DataSource;
            sqlSourceMaster.ConnectionString = connString;
            sqlSourceMaster.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlSourceMaster.SelectCommand = "SupplierPerformance_ReportHeaderData";
            sqlSourceMaster.Parameters.Add("@suppCode", System.Data.DbType.String, suppCode);
            sqlSourceMaster.Parameters.Add("@year", System.Data.DbType.String, year);
            sqlSourceMaster.Parameters.Add("@month", System.Data.DbType.String, month);
            #endregion

            #region SUB REPORT - SAFETY
            Telerik.Reporting.Report rptSafetyMaster = GenerateSubReport(
                                                        repMaster,
                                                        @"\Report\SupplierPerformance\repSafetyMasterSupplierPerformance.trdx",
                                                        "subRepSafety",
                                                        "SupplierPerformance_ReportSafetyJudgement",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptSafetySubmission = GenerateSubReport(
                                                            rptSafetyMaster,
                                                            @"\Report\SupplierPerformance\repSafetySubmissionSupplierPerformance.trdx",
                                                            "subRepSafetySubmission",
                                                            "SupplierPerformance_ReportSafetySubmission",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptSafetyFatal = GenerateSubReport(
                                                        rptSafetyMaster,
                                                        @"\Report\SupplierPerformance\repSafetyFatalSupplierPerformance.trdx",
                                                        "subRepSafetyFatal",
                                                        "SupplierPerformance_ReportSafetyFatalAccident",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptSafetyAbsent = GenerateSubReport(
                                                        rptSafetyMaster,
                                                        @"\Report\SupplierPerformance\repSafetyAbsentSupplierPerformance.trdx",
                                                        "subRepSafetyAbsent",
                                                        "SupplierPerformance_ReportSafetyAbsent",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptSafetySmallInjured = GenerateSubReport(
                                                            rptSafetyMaster,
                                                            @"\Report\SupplierPerformance\repSafetySmallInjuredtSupplierPerformance.trdx",
                                                            "subRepSafetySmallInjured",
                                                            "SupplierPerformance_ReportSafetySmallInjured",
                                                            suppCode, year, month);
            #endregion

            #region SUB REPORT - ENVIRONMENT
            //add riani (20221019)
            Telerik.Reporting.Report rptEnvironmentMaster = GenerateSubReport(
                                                        repMaster,
                                                        @"\Report\SupplierPerformance\repEnvironmentMasterSupplierPerformance.trdx",
                                                        "subRepEnvironment",
                                                        "SupplierPerformance_ReportEnvironmentJudgement",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptEnvironmentReduction = GenerateSubReport(
                                                            rptEnvironmentMaster,
                                                            @"\Report\SupplierPerformance\repEnvironmentReductionSupplierPerformance.trdx",
                                                            "subRepEnvironmentReduction",
                                                            "SupplierPerformance_ReportEnvironmentReduction",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptEnvironmentEmission = GenerateSubReport(
                                                            rptEnvironmentMaster,
                                                            @"\Report\SupplierPerformance\repEnvironmentEmissionSupplierPerformance.trdx",
                                                            "subRepEnvironmentEmission",
                                                            "SupplierPerformance_ReportEnvironmentEmission",
                                                            suppCode, year, month);
            #endregion

            #region SUB REPORT - MANUFACTURING NON MANUFACTURING
            Telerik.Reporting.Report rptMasterManufacturingNonManufacturing = GenerateSubReport(
                                                        repMaster,
                                                        @"\Report\SupplierPerformance\repManufacturingNonManufacturing.trdx",
                                                        "subRepManufacturingNonManufacturing",
                                                        "SupplierPerformance_ReportComment",
                                                        suppCode, year, month);
            #region SUB REPORT - QUALITY
            Telerik.Reporting.Report rptQualityMaster = GenerateSubReport(
                                                        rptMasterManufacturingNonManufacturing,
                                                        @"\Report\SupplierPerformance\repQualityMasterSupplierPerformance.trdx",
                                                        "subRepQuality",
                                                        "SupplierPerformance_ReportQualityJudgement",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptQualityGraph = GenerateReportGraph(
                                                        rptQualityMaster,
                                                        @"\Report\SupplierPerformance\repQualityGraphSupplierPerformance.trdx",
                                                        "subRepQualityGraph",
                                                        "SupplierPerformanceQualityGraphData",
                                                        suppCode, year, month,
                                                        "QualityGraph", "QualityChart", "QualityLine",
                                                        "PPM", "Month", TMMINTarget.QUALITY, 8, 7);

            Telerik.Reporting.Report rptQualityPartReceived = GenerateSubReport(
                                                                rptQualityMaster,
                                                                @"\Report\SupplierPerformance\repQualityPartReceivedSupplierPerformance.trdx",
                                                                "subRepQualityPartReceived",
                                                                "SupplierPerformance_ReportPartReceived",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptQualityPartNG = GenerateSubReport(
                                                        rptQualityMaster,
                                                        @"\Report\SupplierPerformance\repQualityPartNGSupplierPerformance.trdx",
                                                        "subRepQualityPartNG",
                                                        "SupplierPerformance_ReportQualityPartNG",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptQualityPPM = GenerateSubReport(
                                                    rptQualityMaster,
                                                    @"\Report\SupplierPerformance\repQualityPPMSupplierPerformance.trdx",
                                                    "subRepQualityPPM",
                                                    "SupplierPerformance_ReportQualityPPM",
                                                    suppCode, year, month);

            Telerik.Reporting.Report rptQualityDetailProblem = GenerateSubReport(
                                                                rptQualityMaster,
                                                                @"\Report\SupplierPerformance\repQualityDetailProblemSupplierPerformance.trdx",
                                                                "subRepDetailProblem",
                                                                "SupplierPerformance_ReportQualityDetailProblem",
                                                                suppCode, year, month);
            #endregion
            #region SUB REPORT - QUALITY NON MANUFACTURING
            //add riani (20221019)
            Telerik.Reporting.Report rptQualityNonManufacturingMaster = GenerateSubReport(
                                                        rptMasterManufacturingNonManufacturing,
                                                        @"\Report\SupplierPerformance\repQualityNonManufacturingMasterSupplierPerformance.trdx",
                                                        "subRepQualityNonManufacturing",
                                                        "SupplierPerformance_ReportQualityJudgement_NonManufacturing",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptQualityNonManufacturingPartReceived = GenerateSubReport(
                                                                rptQualityNonManufacturingMaster,
                                                                @"\Report\SupplierPerformance\repQualityNonManufacturingPartReceivedSupplierPerformance.trdx",
                                                                "subRepQualityNonManufacturingPartReceived",
                                                                "SupplierPerformance_ReportPartReceived",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptQualityNonManufacturingGraph = GenerateReportGraph(
                                                        rptQualityNonManufacturingMaster,
                                                        @"\Report\SupplierPerformance\repQualityNonManufacturingGraphSupplierPerformance.trdx",
                                                        "subRepQualityNonManufacturingGraph",
                                                        "SupplierPerformanceQualityGraphData_NonManufacturing",
                                                        suppCode, year, month,
                                                        "QualityGraph", "QualityChart", "QualityLine",
                                                        "PPM", "Month", TMMINTarget.QUALITY_NM, 8, 7);

            Telerik.Reporting.Report rptQualityNonManufacturingPartNG = GenerateSubReport(
                                                        rptQualityNonManufacturingMaster,
                                                        @"\Report\SupplierPerformance\repQualityNonManufacturingPartNGSupplierPerformance.trdx",
                                                        "subRepQualityNonManufacturingPartNG",
                                                        "SupplierPerformance_ReportQualityPartNG_NonManufacturing",
                                                        suppCode, year, month);

            Telerik.Reporting.Report rptQualityNonManufacturingPPM = GenerateSubReport(
                                                    rptQualityNonManufacturingMaster,
                                                    @"\Report\SupplierPerformance\repQualityNonManufacturingPPMSupplierPerformance.trdx",
                                                    "subRepQualityNonManufacturingPPM",
                                                    "SupplierPerformance_ReportQualityPPM_NonManufacturing",
                                                    suppCode, year, month);

            Telerik.Reporting.Report rptQualityDetailProblemNonManufacturing = GenerateSubReport(
                                                                rptQualityNonManufacturingMaster,
                                                                @"\Report\SupplierPerformance\repQualityNonManufacturingDetailProblemSupplierPerformance.trdx",
                                                                "subRepDetailProblemNonManufacturing",
                                                                "SupplierPerformance_ReportQualityDetailProblem_NonManufacturing",
                                                                suppCode, year, month);
            #endregion
            #region SUB REPORT - SHORTAGE
            Telerik.Reporting.Report rptDeliveryShortage = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repDeliveryShortageSupplierPerformance.trdx",
                                                            "subRepShortage",
                                                            "SupplierPerformance_ReportShortageJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageGraph = GenerateReportGraph(
                                                                rptDeliveryShortage,
                                                                @"\Report\SupplierPerformance\repDeliveryShortageGraphSupplierPerformance.trdx",
                                                                "subRepShortageGraph",
                                                                "SupplierPeformanceDeliveryShortageGraphData",
                                                                suppCode, year, month,
                                                                "ShortageGraph", "ShortageChart", "ShortageLine",
                                                                "PPM", "Month", TMMINTarget.SHORTAGE);

            Telerik.Reporting.Report rptDeliveryShortagePartReceived = GenerateSubReport(
                                                                        rptDeliveryShortage,
                                                                        @"\Report\SupplierPerformance\repDeliveryPartReceivedSupplierPerformance.trdx",
                                                                        "subRepDeliveryPartReceive",
                                                                        "SupplierPerformance_ReportPartReceived",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortagePartPCS = GenerateSubReport(
                                                                    rptDeliveryShortage,
                                                                    @"\Report\SupplierPerformance\repDeliveryShortagePCSSupplierPerformance.trdx",
                                                                    "subRepShortagePcs",
                                                                    "SupplierPerformance_ReportShortagePcs",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortagePPM = GenerateSubReport(
                                                                rptDeliveryShortage,
                                                                @"\Report\SupplierPerformance\repDeliveryShortagePPMSupplierPerformance.trdx",
                                                                "subRepShortagePPM",
                                                                "SupplierPerformance_ReportShortagePPM",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageDetail = GenerateSubReport(
                                                                    rptDeliveryShortage,
                                                                    @"\Report\SupplierPerformance\repDeliveryShortageDetailSupplierPerformance.trdx",
                                                                    "subRepDetailShortage",
                                                                    "SupplierPerformance_ReportShortageDetailShortage",
                                                                    suppCode, year, month);
            #endregion

            #region SUB REPORT - MISSPART
            Telerik.Reporting.Report rptDeliveryMisspart = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repDeliveryMisspartSupplierPerformance.trdx",
                                                            "subRepMisspart",
                                                            "SupplierPerformance_ReportMisspartJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartGraph = GenerateReportGraph(
                                                                rptDeliveryMisspart,
                                                                @"\Report\SupplierPerformance\repDeliveryMisspartGraphSupplierPerformance.trdx",
                                                                "subRepMisspartGraph",
                                                                "SupplierPeformanceDeliveryMisspartGraphData",
                                                                suppCode, year, month,
                                                                "MisspartGraph", "MisspartChart", "MisspartLine",
                                                                "PPM", "Month", TMMINTarget.MISSPART);

            Telerik.Reporting.Report rptDeliveryMisspartPartReceived = GenerateSubReport(
                                                                        rptDeliveryMisspart,
                                                                        @"\Report\SupplierPerformance\repDeliveryPartReceivedSupplierPerformance.trdx",
                                                                        "subRepDeliveryPartReceive",
                                                                        "SupplierPerformance_ReportPartReceived",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartPartPCS = GenerateSubReport(
                                                                    rptDeliveryMisspart,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartPCSSupplierPerformance.trdx",
                                                                    "subRepMisspartPcs",
                                                                    "SupplierPerformance_ReportMisspartPcs",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartPartPPM = GenerateSubReport(
                                                                    rptDeliveryMisspart,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartPPMSupplierPerformance.trdx",
                                                                    "subRepMisspartPPM",
                                                                    "SupplierPerformance_ReportMisspartPPM",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartDetail = GenerateSubReport(
                                                                    rptDeliveryMisspart,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartDetailSupplierPerformance.trdx",
                                                                    "subRepDetailMisspart",
                                                                    "SupplierPerformance_ReportMisspartDetailMisspart",
                                                                    suppCode, year, month);
            #endregion

            #region SUB REPORT - SHORTAGE NON MANUFACTURING 
            //add riani (20221016)
            Telerik.Reporting.Report rptDeliveryShortageNonManufacturing = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repDeliveryShortageNonManufacturingSupplierPerformance.trdx",
                                                            "subRepShortageNonManufacturing",
                                                            "SupplierPerformance_ReportShortageJudgement_NonManufacturing",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageNonManufacturingGraph = GenerateReportGraph(
                                                                rptDeliveryShortageNonManufacturing,
                                                                @"\Report\SupplierPerformance\repDeliveryShortageNonManufacturingGraphSupplierPerformance.trdx",
                                                                "subRepShortageNonManufacturingGraph",
                                                                "SupplierPeformanceDeliveryShortageGraphDataNonManufacturing",
                                                                suppCode, year, month,
                                                                "ShortageGraph", "ShortageChart", "ShortageLine",
                                                                "PPM", "Month", TMMINTarget.SHORTAGE_NM);

            Telerik.Reporting.Report rptDeliveryShortagePartReceivedNonManufacturing = GenerateSubReport(
                                                                        rptDeliveryShortageNonManufacturing,
                                                                        @"\Report\SupplierPerformance\repDeliveryPartReceivedSupplierPerformanceNonManufacturing.trdx",
                                                                        "subRepDeliveryPartReceiveNonManufacturing",
                                                                        "SupplierPerformance_ReportPartReceived",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageNonManufacturingPartPCS = GenerateSubReport(
                                                                    rptDeliveryShortageNonManufacturing,
                                                                    @"\Report\SupplierPerformance\repDeliveryShortageNonManufacturingPCSSupplierPerformance.trdx",
                                                                    "subRepShortageNonManufacturingPcs",
                                                                    "SupplierPerformance_ReportShortagePcs_NonManufacturing",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageNonManufacturingPPM = GenerateSubReport(
                                                                rptDeliveryShortageNonManufacturing,
                                                                @"\Report\SupplierPerformance\repDeliveryShortageNonManufacturingPPMSupplierPerformance.trdx",
                                                                "subRepShortageNonManufacturingPPM",
                                                                "SupplierPerformance_ReportShortagePPM_NonManufacturing",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryShortageNonManufacturingDetail = GenerateSubReport(
                                                                    rptDeliveryShortageNonManufacturing,
                                                                    @"\Report\SupplierPerformance\repDeliveryShortageNonManufacturingDetailSupplierPerformance.trdx",
                                                                    "subRepDetailShortageNonManufacturing",
                                                                    "SupplierPerformance_ReportShortageDetailShortage_NonManufacturing",
                                                                    suppCode, year, month);
            #endregion

            #region SUB REPORT - MISSPART NON MANUFACTURING
            //add riani (20221016)
            Telerik.Reporting.Report rptDeliveryMisspartNonManufacturing = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repDeliveryMisspartNonManufacturingSupplierPerformance.trdx",
                                                            "subRepMisspartNonManufacturing",
                                                            "SupplierPerformance_ReportMisspartJudgement_NonManufacturing",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartNonManufacturingGraph = GenerateReportGraph(
                                                                rptDeliveryMisspartNonManufacturing,
                                                                @"\Report\SupplierPerformance\repDeliveryMisspartNonManufacturingGraphSupplierPerformance.trdx",
                                                                "subRepMisspartNonManufacturingGraph",
                                                                "SupplierPeformanceDeliveryMisspartGraphDataNonManufacturing",
                                                                suppCode, year, month,
                                                                "MisspartGraph", "MisspartChart", "MisspartLine",
                                                                "PPM", "Month", TMMINTarget.MISSPART_NM);

            Telerik.Reporting.Report rptDeliveryMisspartNonManufacturingPartReceived = GenerateSubReport(
                                                                        rptDeliveryMisspartNonManufacturing,
                                                                        @"\Report\SupplierPerformance\repDeliveryPartReceivedSupplierPerformanceNonManufacturing.trdx",
                                                                        "subRepDeliveryPartReceiveNonManufacturing",
                                                                        "SupplierPerformance_ReportPartReceived",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartNonManufacturingPartPCS = GenerateSubReport(
                                                                    rptDeliveryMisspartNonManufacturing,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartNonManufacturingPCSSupplierPerformance.trdx",
                                                                    "subRepMisspartNonManufacturingPcs",
                                                                    "SupplierPerformance_ReportMisspartPcs_NonManufacturing",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartNonManufacturingPartPPM = GenerateSubReport(
                                                                    rptDeliveryMisspartNonManufacturing,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartNonManufacturingPPMSupplierPerformance.trdx",
                                                                    "subRepMisspartNonManufacturingPPM",
                                                                    "SupplierPerformance_ReportMisspartPPM_NonManufacturing",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryMisspartNonManufacturingDetail = GenerateSubReport(
                                                                    rptDeliveryMisspartNonManufacturing,
                                                                    @"\Report\SupplierPerformance\repDeliveryMisspartNonManufacturingDetailSupplierPerformance.trdx",
                                                                    "subRepDetailMisspartNonManufacturing",
                                                                    "SupplierPerformance_ReportMisspartDetailMisspart_NonManufacturing",
                                                                    suppCode, year, month);
            #endregion

            #region SUB REPORT - WARRANTY
            Telerik.Reporting.Report rptwarrantyMaster = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repWarrantySupplierPerformance.trdx",
                                                            "subRepWarranty",
                                                            "SupplierPerformance_ReportWarrantyTWC",
                                                            suppCode, year, month);
            #endregion

            #region SUB REPORT - ONTIME
            Telerik.Reporting.Report rptDeliveryOntime = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repDeliveryOntimeSupplierPerformance.trdx",
                                                            "subRepOntime",
                                                            "SupplierPerformance_ReportOntimeJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimeGraph = GenerateReportGraph(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeGraphSupplierPerformance.trdx",
                                                                "subRepOntimeGraph",
                                                                "SupplierPeformanceDeliveryOntimeGraphData",
                                                                suppCode, year, month,
                                                                "OntimeGraph", "OntimeChart", "OntimeLine",
                                                                "ON TIME", "Month", TMMINTarget.ONTIME);

            Telerik.Reporting.Report rptDeliveryOntimeSupply = GenerateSubReport(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeSupplySupplierPerformance.trdx",
                                                                "subRepOntimeSupply",
                                                                "SupplierPerformance_ReportOntimeSupply",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimeDelay = GenerateSubReport(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeDelaySupplierPerformance.trdx",
                                                                "subRepOntimeDelay",
                                                                "SupplierPerformance_ReportOntimeDelay",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimePercentage = GenerateSubReport(
                                                                    rptDeliveryOntime,
                                                                    @"\Report\SupplierPerformance\repDeliveryOntimePercentageSupplierPerformance.trdx",
                                                                    "subRepOntimePercentage",
                                                                    "SupplierPerformance_ReportOntimePercentage",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryOntimeDetail = GenerateSubReport(
                                                                rptDeliveryOntime,
                                                                @"\Report\SupplierPerformance\repDeliveryOntimeDetailSupplierPerformance.trdx",
                                                                "subRepDetailOntime",
                                                                "SupplierPerformance_ReportOntimeDetailOntime",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPORT - LINESTOP
            Telerik.Reporting.Report rptDeliveryLinestop = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repDeliveryLinestopSupplierPerformance.trdx",
                                                            "subRepLinestop",
                                                            "SupplierPerformance_ReportLineStopJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryLinestopGraph = GenerateReportGraph(
                                                                rptDeliveryLinestop,
                                                                @"\Report\SupplierPerformance\repDeliveryLinestopGraphSupplierPerformance.trdx",
                                                                "subRepLinestopGraph",
                                                                "SupplierPeformanceDeliveryLinestopGraphData",
                                                                suppCode, year, month,
                                                                "LinestopGraph", "LinestopChart", "LinestopLine",
                                                                "MINUTES", "Month", TMMINTarget.LINESTOP);

            Telerik.Reporting.Report rptDeliveryLinestopMinute = GenerateSubReport(
                                                                rptDeliveryLinestop,
                                                                @"\Report\SupplierPerformance\repDeliveryLinestopMinuteSupplierPerformance.trdx",
                                                                "subRepLinestopMinute",
                                                                "SupplierPerformance_ReportLinestopMinute",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryLinestopDetail = GenerateSubReport(
                                                                rptDeliveryLinestop,
                                                                @"\Report\SupplierPerformance\repDeliveryLinestopDetailSupplierPerformance.trdx",
                                                                "subRepDetailOntime",
                                                                "SupplierPerformance_ReportLinestopDetailLinestop",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPORT - CRIPPLE
            Telerik.Reporting.Report rptDeliveryCripple = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repDeliveryCrippleSupplierPerformance.trdx",
                                                            "subRepCripple",
                                                            "SupplierPerformance_ReportCrippleJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryCrippleGraph = GenerateReportGraph(
                                                                rptDeliveryCripple,
                                                                @"\Report\SupplierPerformance\repDeliveryCrippleGraphSupplierPerformance.trdx",
                                                                "subRepCrippleGraph",
                                                                "SupplierPeformanceDeliveryCrippleGraphData",
                                                                suppCode, year, month,
                                                                "CrippleGraph", "CrippleChart", "CrippleLine",
                                                                "UNIT", "Month", TMMINTarget.CRIPPLE);

            Telerik.Reporting.Report rptDeliveryCrippleUnit = GenerateSubReport(
                                                                rptDeliveryCripple,
                                                                @"\Report\SupplierPerformance\repDeliveryCrippleUnitSupplierPerformance.trdx",
                                                                "subRepCrippleUnit",
                                                                "SupplierPerformance_ReportCrippleUnit",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptDeliveryCrippleDetail = GenerateSubReport(
                                                                rptDeliveryCripple,
                                                                @"\Report\SupplierPerformance\repDeliveryCrippleDetailSupplierPerformance.trdx",
                                                                "subRepDetailCripple",
                                                                "SupplierPerformance_ReportCrippleDetailCripple",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPORT - KPI
            //add riani (20221023)
            Telerik.Reporting.Report rptKPI = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repKPIMasterSupplierPerformance.trdx",
                                                            "subRepKPI",
                                                            "SupplierPerformance_ReportKPI",
                                                            suppCode, year, month);

            #endregion

            #region SUB REPORT - DELIVERY DUE DATE
            Telerik.Reporting.Report rptServicePartDDD = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repServicePartDeliveryDueDateSupplierPerformance.trdx",
                                                            "subRepDDD",
                                                            "SupplierPerformance_ReportServicePartDeliveryDueDateJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartDDDGraph = GenerateReportGraph(
                                                                rptServicePartDDD,
                                                                @"\Report\SupplierPerformance\repServicePartDeliveryDueDateGraphSupplierPerformance.trdx",
                                                                "subRepDDDGraph",
                                                                "SupplierPeformanceServicePartDDDGraphData",
                                                                suppCode, year, month,
                                                                "DDDGraph", "DDDChart", "DDDLine",
                                                                "ON TIME", "Month", TMMINTarget.DDD);

            Telerik.Reporting.Report rptServicePartDDDOri = GenerateSubReport(
                                                            rptServicePartDDD,
                                                            @"\Report\SupplierPerformance\repServicePartDeliveryDueDateOriSupplierPerformance.trdx",
                                                            "subRepDDDOri",
                                                            "SupplierPerformance_ReportServicePartDeliveryDueDateOri",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartDDDAdj = GenerateSubReport(
                                                            rptServicePartDDD,
                                                            @"\Report\SupplierPerformance\repServicePartDeliveryDueDateAdjSupplierPerformance.trdx",
                                                            "subRepDDDAdj",
                                                            "SupplierPerformance_ReportServicePartDeliveryDueDateAdj",
                                                            suppCode, year, month);
            #endregion

            #region SUB REPORT - SP QUALITY
            Telerik.Reporting.Report rptServicePartQuality = GenerateSubReport(
                                                            rptMasterManufacturingNonManufacturing,
                                                            @"\Report\SupplierPerformance\repServicePartQualitySupplierPerformance.trdx",
                                                            "subRepSPQuality",
                                                            "SupplierPerformance_ReportServicePartQualityJudgement",
                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartQualityGraph = GenerateReportGraph(
                                                                    rptServicePartQuality,
                                                                    @"\Report\SupplierPerformance\repServicePartQualityGraphSupplierPerformance.trdx",
                                                                    "subRepSPQualityGraph",
                                                                    "SupplierPeformanceServicePartQualityGraphData",
                                                                    suppCode, year, month,
                                                                    "SPQualityGraph", "SPQualityChart", "SPQualityLine",
                                                                    "PPM", "Month", TMMINTarget.SPQUALITY);

            Telerik.Reporting.Report rptServicePartQualityPartReceived = GenerateSubReport(
                                                                        rptServicePartQuality,
                                                                        @"\Report\SupplierPerformance\repServicePartPartReceivedSupplierPerformance.trdx",
                                                                        "subRepSPQualityPartReceived",
                                                                        "SupplierPerformance_ReportServicePartQualityPartReceive",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptServicePartQualityPartNG = GenerateSubReport(
                                                                    rptServicePartQuality,
                                                                    @"\Report\SupplierPerformance\repServicePartQualityNGSupplierPerformance.trdx",
                                                                    "subRepSPQualityNG",
                                                                    "SupplierPerformance_ReportServicePartQualityPartNG",
                                                                    suppCode, year, month);

            Telerik.Reporting.Report rptServicePartQualityPPM = GenerateSubReport(
                                                                rptServicePartQuality,
                                                                @"\Report\SupplierPerformance\repServicePartQualityPPMSupplierPerformance.trdx",
                                                                "subRepSPQualityPPM",
                                                                "SupplierPerformance_ReportServicePartQualityPPM",
                                                                suppCode, year, month);
            #endregion

            #region SUB REPOST - WORKABILITY
            Telerik.Reporting.Report rptServicePartWorkability = GenerateSubReport(
                                                                rptMasterManufacturingNonManufacturing,
                                                                @"\Report\SupplierPerformance\repServicePartWorkabilitySupplierPerformance.trdx",
                                                                "subRepSPWorkability",
                                                                "SupplierPerformance_ReportServicePartWorkabilityJudgement",//edit riani (20230202)-->//"SupplierPerformance_ReportServicePartQualityJudgement",
                                                                suppCode, year, month);

            Telerik.Reporting.Report rptServicePartWorkabilityGraph = GenerateReportGraph(
                                                                        rptServicePartWorkability,
                                                                        @"\Report\SupplierPerformance\repServicePartWorkabilityGraphSupplierPerformance.trdx",
                                                                        "subRepSPWorkabilityGraph",
                                                                        "SupplierPeformanceServicePartWorkabilityGraphData",
                                                                        suppCode, year, month,
                                                                        "SPWorkabilityGraph", "SPWorkabilityChart", "SPWorkabilityLine",
                                                                        "PPM", "Month", TMMINTarget.WORKABILITY);

            Telerik.Reporting.Report rptServicePartWorkabilityPartReceived = GenerateSubReport(
                                                                            rptServicePartWorkability,
                                                                            @"\Report\SupplierPerformance\repServicePartPartReceivedSupplierPerformance.trdx",
                                                                            "subRepSPWorkabilityPartReceived",
                                                                            "SupplierPerformance_ReportServicePartQualityPartReceive",
                                                                            suppCode, year, month);

            Telerik.Reporting.Report rptServicePartWorkabilityPartNG = GenerateSubReport(
                                                                        rptServicePartWorkability,
                                                                        @"\Report\SupplierPerformance\repServicePartWorkabilityNGSupplierPerformance.trdx",
                                                                        "subRepSPWorkabilityNG",
                                                                        "SupplierPerformance_ReportServicePartWorkabilityPartNG",
                                                                        suppCode, year, month);

            Telerik.Reporting.Report rptServicePartWorkabilityPPM = GenerateSubReport(
                                                                    rptServicePartWorkability,
                                                                    @"\Report\SupplierPerformance\repServicePartWorkabilityPPMSupplierPerformance.trdx",
                                                                    "subRepSPWorkabilityPPM",
                                                                    "SupplierPerformance_ReportServicePartWorkabilityPPM",
                                                                    suppCode, year, month);
            #endregion

            #endregion


            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport(typeFile == 1 ? "xls" : "pdf", repMaster, null);
            return result.DocumentBytes;
        }
        //---------------------//
        #endregion
        //-----------------//
        private Telerik.Reporting.Report GenerateSubReport(Telerik.Reporting.Report rpt, string rptPath, string subReportID,
                                                           string Queries, string suppCode, string year, string month)
        {
            string connString = DBContextNames.DB_PCS;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            Telerik.Reporting.Report rptSub;
            Telerik.Reporting.SqlDataSource sqlSourceSubRep;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + rptPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rptSub = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep = (Telerik.Reporting.SubReport)rpt.Items.Find(subReportID, true)[0];
            subRep.ReportSource = rptSub;
            sqlSourceSubRep = (Telerik.Reporting.SqlDataSource)rptSub.DataSource;
            sqlSourceSubRep.ConnectionString = connString;
            sqlSourceSubRep.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlSourceSubRep.SelectCommand = Queries;
            sqlSourceSubRep.Parameters.Add("@suppCode", System.Data.DbType.String, suppCode);
            sqlSourceSubRep.Parameters.Add("@year", System.Data.DbType.String, year);
            sqlSourceSubRep.Parameters.Add("@month", System.Data.DbType.String, month);

            return rptSub;
        }

        private Telerik.Reporting.Report GenerateReportGraph(Telerik.Reporting.Report rpt, string rptPath, string subReportID,
                                                             string Queries, string suppCode, string year, string month,
                                                             string GraphName, string ChartSeriesName, string LineName,
                                                             string YLabel, string XLabel, string LineLabel, float LineLabelSize = 6,
                                                             float AxisLabelSize = 5)
        {
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            Telerik.Reporting.Report rptSub;

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + rptPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rptSub = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep = (Telerik.Reporting.SubReport)rpt.Items.Find(subReportID, true)[0];
            subRep.ReportSource = rptSub;

            var Graph = rptSub.Items.Find(typeof(Telerik.Reporting.Chart), true)[0] as Telerik.Reporting.Chart;
            Graph.BitmapResolution = 96F;
            Graph.ImageFormat = System.Drawing.Imaging.ImageFormat.Emf;
            //grafikQualityProblem.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.3), Telerik.Reporting.Drawing.Unit.Cm(1.8));
            Graph.Name = GraphName;
            Graph.PlotArea.EmptySeriesMessage.Appearance.Visible = true;
            Graph.PlotArea.EmptySeriesMessage.Visible = true;
            //grafikQualityProblem.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10), Telerik.Reporting.Drawing.Unit.Cm(7.5));
            Graph.ChartTitle.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
            Graph.PlotArea.EmptySeriesMessage.Appearance.Visible = false;
            Graph.PlotArea.EmptySeriesMessage.Visible = false;
            Graph.PlotArea.XAxis.AxisLabel.Visible = false;
            Graph.PlotArea.YAxis.AxisLabel.Visible = true;

            ////Create a ChartSeries and assign its name and chart type
            ChartSeries chartSeries = new ChartSeries();
            chartSeries.Appearance.LabelAppearance.Visible = false;
            chartSeries.Name = ChartSeriesName;
            chartSeries.Type = ChartSeriesType.Line;
            chartSeries.Appearance.LineSeriesAppearance.Color = System.Drawing.Color.FromArgb(1, 153, 49);//System.Drawing.Color.Green;
            chartSeries.Appearance.LineSeriesAppearance.PenStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            chartSeries.Appearance.LineSeriesAppearance.Width = 5;

            // visually enhance the data points
            chartSeries.Appearance.PointMark.Figure = "Rectangle";
            chartSeries.Appearance.PointMark.Dimensions.Width = Telerik.Reporting.Charting.Styles.Unit.Pixel(50);
            chartSeries.Appearance.PointMark.Dimensions.Height = Telerik.Reporting.Charting.Styles.Unit.Pixel(50);
            chartSeries.Appearance.PointMark.FillStyle.MainColor = System.Drawing.Color.FromArgb(1, 153, 49);//System.Drawing.Color.Green;
            chartSeries.Appearance.PointMark.Visible = true;

            ChartSeries line = new ChartSeries();
            line.Appearance.LabelAppearance.Visible = true;
            line.Name = LineName;
            line.Type = ChartSeriesType.Line;
            line.Appearance.LineSeriesAppearance.Color = System.Drawing.Color.Red;
            line.Appearance.LineSeriesAppearance.Width = 3;
            line.Appearance.LabelAppearance.LabelLocation = Telerik.Reporting.Charting.Styles.StyleSeriesItemLabel.ItemLabelLocation.Outside;
            line.DefaultLabelValue = "";

            // visually enhance the data points
            line.Appearance.PointMark.Dimensions.Width = 0;
            line.Appearance.PointMark.Dimensions.Height = 0;
            line.Appearance.PointMark.FillStyle.MainColor = System.Drawing.Color.Red;
            line.Appearance.PointMark.Visible = false;
            line.Appearance.LineSeriesAppearance.PenStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            line.Appearance.ShowLabels = true;

            // set the plot area gradient background fill
            Graph.PlotArea.Appearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.Solid;
            Graph.PlotArea.Appearance.FillStyle.MainColor = System.Drawing.Color.White;
            Graph.PlotArea.Appearance.FillStyle.SecondColor = System.Drawing.Color.White;

            // Set text and line for X axis
            Graph.PlotArea.XAxis.AxisLabel.TextBlock.Text = XLabel;
            Graph.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
            Graph.PlotArea.XAxis.Appearance.Width = 1;
            Graph.PlotArea.XAxis.Appearance.Color = System.Drawing.Color.Transparent;

            // Set text and line for Y axis
            Graph.PlotArea.YAxis.AxisLabel.TextBlock.Text = YLabel;
            Graph.PlotArea.YAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
            Graph.PlotArea.YAxis.Appearance.Width = 1;
            Graph.PlotArea.YAxis.Appearance.Color = System.Drawing.Color.Transparent;

            System.Drawing.Font fontAxis = new System.Drawing.Font("Verdana", AxisLabelSize, System.Drawing.FontStyle.Bold);
            Graph.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Font = fontAxis;
            Graph.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Font = fontAxis;

            Graph.PlotArea.XAxis.AddItem("Jan");
            Graph.PlotArea.XAxis.AddItem("Feb");
            Graph.PlotArea.XAxis.AddItem("Mar");
            Graph.PlotArea.XAxis.AddItem("Apr");
            Graph.PlotArea.XAxis.AddItem("May");
            Graph.PlotArea.XAxis.AddItem("Jun");
            Graph.PlotArea.XAxis.AddItem("Jul");
            Graph.PlotArea.XAxis.AddItem("Aug");
            Graph.PlotArea.XAxis.AddItem("Sep");
            Graph.PlotArea.XAxis.AddItem("Oct");
            Graph.PlotArea.XAxis.AddItem("Nov");
            Graph.PlotArea.XAxis.AddItem("Dec");

            List<SPSubCommonGraph> GraphData = new List<SPSubCommonGraph>();

            IDBContext db = DbContext;
            try
            {
                GraphData = db.Fetch<SPSubCommonGraph>(Queries, new object[] { suppCode, year, month });
                if (GraphName == "OntimeGraph")
                {
                    List<SPSubCommonGraphYAxis> YAxisData = db.Fetch<SPSubCommonGraphYAxis>("SupplierPeformanceDeliveryOntimeGraphYAxis");
                    List<ChartAxisItem> yaxs = new List<ChartAxisItem>();
                    foreach (var y in YAxisData)
                    {
                        ChartAxisItem yax = new ChartAxisItem();
                        yax.Value = Convert.ToDecimal(y.YAxis);
                        yaxs.Add(yax);
                    }

                    Graph.PlotArea.YAxis.AddItem(yaxs);
                }
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            foreach (SPSubCommonGraph g in GraphData)
            {

                if (Convert.ToInt32(month) >= 1)
                {
                    chartSeries.AddItem(g.JAN);
                }

                if (Convert.ToInt32(month) >= 2)
                {
                    chartSeries.AddItem(g.FEB);
                }

                if (Convert.ToInt32(month) >= 3)
                {
                    chartSeries.AddItem(g.MAR);
                }

                if (Convert.ToInt32(month) >= 4)
                {
                    chartSeries.AddItem(g.APR);
                }

                if (Convert.ToInt32(month) >= 5)
                {
                    chartSeries.AddItem(g.MAY);
                }

                if (Convert.ToInt32(month) >= 6)
                {
                    chartSeries.AddItem(g.JUN);
                }

                if (Convert.ToInt32(month) >= 7)
                {
                    chartSeries.AddItem(g.JUL);
                }

                if (Convert.ToInt32(month) >= 8)
                {
                    chartSeries.AddItem(g.AGS);
                }

                if (Convert.ToInt32(month) >= 9)
                {
                    chartSeries.AddItem(g.SEP);
                }

                if (Convert.ToInt32(month) >= 10)
                {
                    chartSeries.AddItem(g.OCT);
                }

                if (Convert.ToInt32(month) >= 11)
                {
                    chartSeries.AddItem(g.NOV);
                }

                if (Convert.ToInt32(month) >= 12)
                {
                    chartSeries.AddItem(g.DEC);
                }


                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);
                line.AddItem(g.TARGET_TMMIN);

                line.Items[0].Label.TextBlock.Text = "";
                line.Items[1].Label.TextBlock.Text = "";
                line.Items[2].Label.TextBlock.Text = "";
                line.Items[3].Label.TextBlock.Text = "";
                line.Items[4].Label.TextBlock.Text = "";
                line.Items[5].Label.TextBlock.Text = "";
                line.Items[6].Label.TextBlock.Text = "";
                line.Items[7].Label.TextBlock.Text = "";
                line.Items[8].Label.TextBlock.Text = "";
                line.Items[9].Label.TextBlock.Text = "";
                line.Items[10].Label.TextBlock.Text = "";
                line.Items[11].Label.TextBlock.Text = LineLabel;

                System.Drawing.Font fontLineTarget = new System.Drawing.Font("Verdana", LineLabelSize, System.Drawing.FontStyle.Bold);
                line.Items[11].Label.TextBlock.Appearance.TextProperties.Font = fontLineTarget;
                line.Items[11].Label.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
            }

            ////// add the series to the Chart Series collection
            Graph.Series.Add(chartSeries);
            Graph.Series.Add(line);

            return rptSub;
        }

        private Telerik.Reporting.Report GenerateReportPieChart(Telerik.Reporting.Report rpt, string rptPath, string subReportID,
                                                                 string Queries, string suppCode, string year, string month,
                                                                 string GraphName, string ChartSeriesName, string LineName,
                                                                 string YLabel, string XLabel, string LineLabel)
        {
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            Telerik.Reporting.Report rptSub;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + rptPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rptSub = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Telerik.Reporting.SubReport subRep = (Telerik.Reporting.SubReport)rpt.Items.Find(subReportID, true)[0];
            subRep.ReportSource = rptSub;

            var Graph = rptSub.Items.Find(typeof(Telerik.Reporting.Chart), true)[0] as Telerik.Reporting.Chart;
            Graph.BitmapResolution = 96F;
            Graph.ImageFormat = System.Drawing.Imaging.ImageFormat.Emf;
            Graph.Name = GraphName;
            Graph.PlotArea.EmptySeriesMessage.Appearance.Visible = false;
            Graph.PlotArea.EmptySeriesMessage.Visible = false;
            Graph.ChartTitle.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
            Graph.Appearance.Border.Visible = false;

            System.Drawing.Font fontChartTitle = new System.Drawing.Font("Verdana", 6, System.Drawing.FontStyle.Bold);
            Graph.ChartTitle.TextBlock.Appearance.TextProperties.Font = fontChartTitle;
            Graph.PlotArea.EmptySeriesMessage.Appearance.Visible = true;
            Graph.PlotArea.EmptySeriesMessage.Visible = true;
            Graph.PlotArea.XAxis.AxisLabel.Visible = false;
            Graph.PlotArea.YAxis.AxisLabel.Visible = false;
            Graph.PlotArea.Appearance.Dimensions.Margins.Bottom = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);
            Graph.PlotArea.Appearance.Dimensions.Margins.Top = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);
            Graph.PlotArea.Appearance.Dimensions.Margins.Left = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);
            Graph.PlotArea.Appearance.Dimensions.Margins.Right = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);
            Graph.PlotArea.Appearance.Dimensions.Paddings.Bottom = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);
            Graph.PlotArea.Appearance.Dimensions.Paddings.Top = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);
            Graph.PlotArea.Appearance.Dimensions.Paddings.Left = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);
            Graph.PlotArea.Appearance.Dimensions.Paddings.Right = Telerik.Reporting.Charting.Styles.Unit.Pixel(0);

            ////Create a ChartSeries and assign its name and chart type
            ChartSeries chartSeries = new ChartSeries();
            chartSeries.Appearance.LabelAppearance.Visible = true;
            chartSeries.Name = ChartSeriesName;
            chartSeries.Type = ChartSeriesType.Pie;
            chartSeries.Appearance.DiameterScale = 0.95;
            chartSeries.Appearance.Border.Visible = false;
            chartSeries.Appearance.LabelAppearance.LabelLocation = Telerik.Reporting.Charting.Styles.StyleSeriesItemLabel.ItemLabelLocation.Inside;

            // set the plot area gradient background fill
            Graph.PlotArea.Appearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.Solid;
            Graph.PlotArea.Appearance.FillStyle.MainColor = System.Drawing.Color.White;
            Graph.PlotArea.Appearance.FillStyle.SecondColor = System.Drawing.Color.White;

            List<SPSubCommonPieChart> GraphData = new List<SPSubCommonPieChart>();
            List<SPSubCommonPieColor> Colors = new List<SPSubCommonPieColor>();
            IDBContext db = DbContext;
            try
            {
                GraphData = db.Fetch<SPSubCommonPieChart>(Queries, new object[] { suppCode, year, month });
                Colors = db.Fetch<SPSubCommonPieColor>("SupplierPerformanceGetPieChartColors");
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            int chartindex = 0;
            int colordefined = Colors.Count;
            int colorindex = 0;
            System.Drawing.Font fontChartPieLabel = new System.Drawing.Font("Verdana", 5, System.Drawing.FontStyle.Bold);
            foreach (SPSubCommonPieChart g in GraphData)
            {
                if (colorindex > colordefined)
                {
                    colorindex = 0;
                }

                chartSeries.AddItem(g.PERCENTAGE);
                chartSeries.Items[chartindex].Label.TextBlock.Text = g.DATA;
                chartSeries.Items[chartindex].Label.TextBlock.Appearance.TextProperties.Font = fontChartPieLabel;
                chartSeries.Items[chartindex].Appearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.Solid;

                if (Colors[chartindex].COLOR_TYPE == "NAME")
                {
                    chartSeries.Items[chartindex].Appearance.FillStyle.MainColor = System.Drawing.Color.FromName(Colors[colorindex].COLOR);
                    chartSeries.Items[chartindex].Appearance.FillStyle.SecondColor = System.Drawing.Color.FromName(Colors[colorindex].COLOR);
                }
                else if (Colors[chartindex].COLOR_TYPE == "HEXA")
                {
                    chartSeries.Items[chartindex].Appearance.FillStyle.MainColor = System.Drawing.ColorTranslator.FromHtml(Colors[colorindex].COLOR);
                    chartSeries.Items[chartindex].Appearance.FillStyle.SecondColor = System.Drawing.ColorTranslator.FromHtml(Colors[colorindex].COLOR);
                }

                chartindex++;
                colorindex++;
            }

            ////// add the series to the Chart Series collection
            Graph.Series.Add(chartSeries);

            return rptSub;
        }
        #endregion

        #region PREVIEW REPORT
        public FileContentResult Reporting(string id)
        {
            string[] valReport = id.Split(';');

            int typeFile = Convert.ToInt16(valReport[0].ToString());
            string suppCode = valReport[1];
            string datePerformance = valReport[2];
            string year = Convert.ToDateTime(datePerformance).ToString("yyyy");
            string month = Convert.ToDateTime(datePerformance).ToString("MM");


            byte[] reportingByte = null;

            //edit riani (20221021)
            //reportingByte = ReportOnByte(typeFile, year, month, suppCode);
            reportingByte = ReportOnByteNew(typeFile, year, month, suppCode);
            //----//

            //string filePath = Path.Combine(Server.MapPath("~/Report/SupplierPerformance/TestReport.pdf"));
            //reportingByte = StreamFile(filePath);
            return File(reportingByte, "application/pdf");
        }
        #endregion

        #region DATABASE CONNECTION
        private IDBContext _db = null;

        private IDBContext db
        {
            get
            {
                if (_db == null)
                {

                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (_db != null)
            {
                _db.Close();
                _db = null;
            }

            base.OnResultExecuted(filterContext);
        }
        #endregion


        #region GRID LOOKUP SETTING
        protected List<SupplierICS> Suppliers
        {
            get
            {
                List<SupplierICS> x = Model.GetModel<List<SupplierICS>>();
                if (x == null)
                {
                    x = Model.GetModel<User>().FilteringArea<SupplierICS>(
                        GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");
                }
                return x;
            }
        }

        private List<SupplierICS> GetAllSupplier()
        {
            List<SupplierICS> ListSupplier = new List<SupplierICS>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ListSupplier = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });

            return ListSupplier;
        }

        public ActionResult GridLookupSupplier()
        {
            List<SupplierICS> ListSupplier = Model.GetModel<User>().FilteringArea<SupplierICS>(
                    GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");
            TempData["GridName"] = "grlSupplier";
            Model.AddModel(ListSupplier);

            return PartialView("GridLookup/PartialGrid", ListSupplier);
        }
        #endregion


        #region RELOAD GRID DETAIL
        string SupplierCode = String.Empty;
        string ProdMonth = String.Empty;
        List<SPSRelease> ListSPSReleased_ = null;

        private List<SPSRelease> ListSPSReleased
        {
            get
            {
                if (ListSPSReleased_ == null)
                    ListSPSReleased_ = new List<SPSRelease>();

                return ListSPSReleased_;
            }
            set
            {
                ListSPSReleased_ = value;
            }
        }

        protected List<SPSRelease> GetAllReleasedDetail(string SupplierCode, string ProdMonth)
        {
            //Modified by fid.goldy
            List<SPSRelease> AllDetailData = new List<SPSRelease>();
            //AllDetailData = db.Fetch<SPSRelease>("GetAllSupplierReleased", new Object[] { SupplierCode, ProdMonth });
            AllDetailData = db.Fetch<SPSRelease>("GetAllSupplierPerformanceStatus", new Object[] { SupplierCode, ProdMonth });
            //end of modified by fid.goldy
            return AllDetailData;
        }

        public ContentResult GetAuthorization()
        {

            string position = "0"; ;


            if (AuthorizedUser.PositionDetails.Count > 0)
            {
                position = (AuthorizedUser.PositionDetails[0].Position == "Department Head") ? "1" : "0";
            }

            int auth = Model.GetModel<Toyota.Common.Web.Credential.User>().IsAuthorized("SupplierPerformanceStatus", "BtnReport");


            return Content(position + ";" + auth.ToString());
        }

        public ActionResult ReloadGridReleasedDetail()
        {
            SupplierCode = Request.Params["SupplierCode"];
            ProdMonth = Request.Params["ProdMonth"];

            //Modified by fid.goldy
            // get current user login detail
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierPerformance", "grlSupplier");

            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {

                supplierCode = suppliers[0].SUPP_CD;
            }
            string strProdMonth = Convert.ToDateTime(ProdMonth).ToString("yyyyMM");

            ListSPSReleased = GetAllReleasedDetail(
               String.IsNullOrEmpty(supplierCode) ? String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Convert.ToString(Request.Params["SupplierCode"]).Replace(";", ",") : supplierCode
             , strProdMonth);
            //end of modified by fid.goldy

            if (ListSPSReleased.Count != 0)
            {
                string releasedFlag = ListSPSReleased[0].ReleaseFlag.ToString();
                TempData["ReleasedFlag"] = releasedFlag;
            }
            else
            {
                TempData["ReleasedFlag"] = "2";
            }


            Model.AddModel(ListSPSReleased);

            return PartialView("SPSPartialGrid", Model);


            /////////////////////////////////////////////////////////

            //SupplierCode = Request.Params["SupplierCode"];
            //ProdMonth = Request.Params["ProdMonth"];
            //ProdMonthTo = Request.Params["ProdMonthTo"];

            //ListSPSafety = GetAllSafetyDetail(SupplierCode, ProdMonth, ProdMonthTo);
            //Model.AddModel(ListSPSafety);

            //return PartialView("SPDetailSafety", Model);
        }
        #endregion


        #region RELEASED DATA
        //Modified by fid.goldy

        public ContentResult ReleasedData(string ProdMonth, string SuppCd)
        {
            string[] ArrProductionMonth = ProdMonth.Substring(0, ProdMonth.Length - 1).Split(';');
            string[] ArrSupplierCode = SuppCd.Substring(0, SuppCd.Length - 1).Split(';');

            string QueryResult = string.Empty;
            string strYear = string.Empty;
            string strMonth = string.Empty;
            DateTime ProductionMonth = DateTime.Now;

            db.BeginTransaction();

            try
            {

                for (int i = 0; i < ArrProductionMonth.Length; i++)
                {
                    //strYear = ArrProductionMonth[i].Substring(0, 4);
                    //strMonth = ArrProductionMonth[i].Substring(4, ArrProductionMonth[i].Length - 4);
                    //ProductionMonth = DateTime.Parse(strYear + "-" + strMonth + "-01");
                    QueryResult = db.ExecuteScalar<string>("UpdateSupplierReleased", new object[] { ArrProductionMonth[i], ArrSupplierCode[i], AuthorizedUser.Username });
                    //ProdMonth = Request.Params["ProdMonth"];
                    //throw new Exception("error");
                    //string strProdMonth = Convert.ToDateTime(ProdMonth).ToString("yyyyMM");
                    //QueryResult = db.ExecuteScalar<string>("UpdateSupplierReleased", new object[] { strProdMonth, SuppCd, AuthorizedUser.Username });
                }

                db.CommitTransaction();

            }
            catch (Exception ex)
            {
                db.Dispose();
                QueryResult = "Error: " + ex.Message;
            }
            finally
            {
                db.Close();
            }

            return Content(QueryResult);
        }
        //End of Modified by fid.goldy
        #endregion
    }
}
