﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.PartInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;
using Portal.Models.Dock;
//using Portal.Models.Area;
using Portal.Models.SuppConv;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.Plant;

namespace Portal.Controllers
{
    public class SupplierConversionController : BaseController
    {

        public SupplierConversionController()
            : base("SupplierConversion : ")
        {
        }

        protected override void StartUp()
        {
        }

        private IDBContext DbContext()
        {
            return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        }

        List<SupplierName> _supplierModel = null;
        private List<SupplierName> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierName>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<PartData> _partModel = null;
        private List<PartData> _PartModel
        {
            get
            {
                if (_partModel == null)
                    _partModel = new List<PartData>();

                return _partModel;
            }
            set
            {
                _partModel = value;
            }
        }


        protected override void Init()
        {

            IDBContext db = DbContext();

            SuppConvModel modelAreaInfo = new SuppConvModel();
            modelAreaInfo.SuppConvDatas = getPartInfo("","","","");
            Model.AddModel(modelAreaInfo);


        }

        [HttpGet]
        public ActionResult PartialUpload()
        {

            //ViewData["GridArea"] = GetAllArea();


            return PartialView("PartialUpload");

        }
        public ActionResult PartialHeader()
        {
       
            //ViewData["GridArea"] = GetAllArea();
            

            return PartialView("PartialHeader");

        }
        public ActionResult PartialDetail()
        {
            //string SupplierCodeLDAP = "";
            //string DockCodeLDAP = "";
            //List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "UtilPlane", "OIHSupplierOptionUtilPlane");
            //List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            //List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "UtilPlane", "OIHDockCodeUtilPlane");

            //if (suppCode.Count == 1)
            //{
            //    SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            //}
            //else
            //{
            //    SupplierCodeLDAP = "";
            //}
            //foreach (DockData d in DockCodes)
            //{
            //    DockCodeLDAP += d.DockCode + ",";
            //}

            SuppConvModel model = Model.GetModel<SuppConvModel>();
            model.SuppConvDatas = getPartInfo(Request.Params["SupplierCodeTmmin"], Request.Params["SupplierPlantTmmin"], Request.Params["SupplierCodeADM"], Request.Params["SupplierPlantADM"]);

            Model.AddModel(model);

            return PartialView("PartialDetail", Model);
        }

        protected List<SuppConvData> getPartInfo(string SupplierCodeTmmin, string SupplierPlantTmmin, string SupplierCodeADM, string SupplierPlantADM)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SuppConvData> listTooltipDatas = new List<SuppConvData>();

            listTooltipDatas = db.Query<SuppConvData>("GetSupplierConversionSearch", new object[] {
                SupplierCodeTmmin,SupplierPlantTmmin,SupplierCodeADM,SupplierPlantADM

            }).ToList();

             db.Close();
            return listTooltipDatas;
        }
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> SISupplierModel = new List<SupplierName>();
            IDBContext db = DbContext();
            SISupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return SISupplierModel;
        }
        public ActionResult PartialAddLookupGridSupplierConversion()
        {
            TempData["GridName"] = "SupplierConversionGridLookup";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "supplierConversion", "OIHSupplierConversionOption");
            if (suppliers.Count > 0)            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);

        }
        public ActionResult PartialHeaderPlantGridHeaderSupplierConversion()
        {
            TempData["GridName"] = "SupplierConversionPlantGridLookup";
            List<PlantData> docks = GetAllPlant();

            ViewData["GridPlanCd"] = docks;

            return PartialView("GridLookup/PartialGrid", ViewData["GridPlanCd"]);
        }

        private List<PlantData> GetAllPlant()
        {
            List<PlantData> PartIndoDockModel = new List<PlantData>();
            IDBContext db = DbContext();
            PartIndoDockModel = db.Fetch<PlantData>("GetAllPlant", new object[] { });
            db.Close();

            return PartIndoDockModel;

        }
        public ActionResult PartialPopupSupplierConversion()
        {

            ViewData["GridPlanCd"] = GetAllPlant();
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "supplierConversion", "OIHSupplierConversionOption");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }
            return PartialView("PopupNew", null);
        }

        //public ActionResult PartialHeaderAreaGridHeaderArea()
        //{
        //    TempData["GridName"] = "OIHAreaCodeArea";
        //    List<AreaData> Areas = Model.GetModel<User>().FilteringArea<AreaData>(GetAllArea(), "AREA_CD", "Area", "OIHAreaCodeArea");
            
        //    ViewData["GridArea"] = Areas;

        //    return PartialView("GridLookup/PartialGrid", ViewData["GridArea"]);
        //}

        //public ActionResult PartialGridLookUpDockUtilPlane()
        //{
        //    TempData["GridName"] = "UtilPlaneHDockGridLookup";
        //    List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DOCK_CODE", "UtilPlane", "UtilPlaneHDockGridLookup");

        //    ViewData["GridDock"] = docks;

        //    return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);

        //}

       
        //private List<AreaData> GetAllArea()
        //{
        //    List<AreaData> PartIndoAreaModel = new List<AreaData>();
        //    IDBContext db = DbContext;
        //    PartIndoAreaModel = db.Fetch<AreaData>("GetAllArea", new object[] { });
        //    db.Close();

        //    return PartIndoAreaModel;
        //}
       
    
        public ActionResult AddNewSupplierConversion(string SupplierCodeTmmin, string SupplierCodeADM, string PlantCodeADM, string User)
        {
        
            string ResultQuery = "";
            string[] Suppliers = SupplierCodeTmmin.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");

            User = AuthorizedUser.Username;
            IDBContext db = DbContext();
            try
            {
                string CheckData;

                
                CheckData = db.ExecuteScalar<string>("InsertSupplierConversion", new object[] 
                {
                   SupplierCode,
                   SupplierPlant,
                   SupplierCodeADM,
                   PlantCodeADM,
                   User
                });

                if (CheckData == "Alreadyexists")
                  {
                    ResultQuery = " Data SuppConversion Already Exists, Please Input Other Data .. ";
                  }
               else
                 {
                    //TempData["GridName"] = "PartialDetailArea";
                    ResultQuery = " Process succesfully..";
                 }
                
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }

        public ActionResult AddEditSupplierConversion(string SupplierCodeTmmin, string SupplierCodeADM, string PlantCodeADM, string SupplierCodeADMold, string PlantCodeADMold, string User)
        {
            string[] Suppliers = SupplierCodeTmmin.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");
            string ResultQuery = "";
            User = AuthorizedUser.Username;
            IDBContext db = DbContext();
            try
            {
                string CheckData;


                CheckData = db.ExecuteScalar<string>("UpdateSupplierConversion", new object[]
                {
                   SupplierCode,
                   SupplierPlant,
                   SupplierCodeADM,
                   PlantCodeADM,
                   User,
                   SupplierCodeADMold,
                   PlantCodeADMold

                });

                if (CheckData == "Alreadyexists")
                {
                    ResultQuery = " Data SupplierCode and PlantCode ADM Conversion Already Exists, Please Edit Other Data ... ";
                }
                else
                {
                    //TempData["GridName"] = "PartialDetailArea";
                    ResultQuery = " Process succesfully..";
                }

                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
 

        public ActionResult DeleteSupplierConversion(string pSuppCodePlant, string pSuppCodeAdm, string pSuppPlantAdm)
        {
            string ResultQuery = "Process succesfully";
            string[] Suppliers = pSuppCodePlant.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");
            IDBContext db = DbContext();
            try
            {
                db.Execute("DeleteSuppConversion", new object[] 
                {
                   SupplierCode,
                   SupplierPlant,
                   pSuppCodeAdm,
                   pSuppPlantAdm

                });
                //TempData["GridName"] = "PartialDetailArea";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public void DownloadSheet(string SupplierCodeTmminParam,string SupplierPlantTmminParam,string SupplierCodeADMParam, string SupplierPlantADMParam)
        {

            string fileName = "SupplierConversion" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //string SupplierCodeLDAP = "";
            //string DockCodeLDAP = "";

            //List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierInfo", "OIHSupplierOption");
            //List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            //List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "EmergencyOrderCreation", "EOCHDockGridLookup");

            //if (suppCode.Count == 1)
            //{
            //    SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            //}
            //else
            //{
            //    SupplierCodeLDAP = "";
            //}
            //foreach (DockData d in DockCodes)
            //{
            //    DockCodeLDAP += d.DockCode + ",";
            //}
            IEnumerable<SuppConvDataDownload> listGeneral = db.Query<SuppConvDataDownload>("GetSupplierConversionSearch", new object[] { SupplierCodeTmminParam , SupplierPlantTmminParam, SupplierCodeADMParam, SupplierPlantADMParam });

            IExcelWriter exporter = ExcelWriter.GetInstance();
            exporter.Append(listGeneral, "SuppConversion");
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();
        }
        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }
        public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/SupplierConversion/UploadTemp/SupplierConversion_Template.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "SupplierConversion_Template.xls");

            
        }

        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("DockUpload", ValidationSettings, FileUploadComplete);
        }
        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls", ".xlsx" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            NotAllowedFileExtensionErrorText = "This file extension isn't allowed",
            ShowErrors = false
        };
        public ActionResult DockUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }
        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/SupplierConversion/DirecUpload/");
                return _uploadDirectory;
            }
        }

        private SuppConvDataUpload AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            SuppConvDataUpload _UploadModel = new SuppConvDataUpload();

            string function = "81016";
            string module = "8";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_SUPPLIER_CONVERSION";

            string sheetName = "SupplierInfo";
            message = "Starting Upload SUPPLIER_CONVERSION";
            location = "SUPPLIER_CONVERSIONUpload.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] {
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_SUPPLIER_CONVERSION";
                location = "SUPPLIER_CONVERSIONUpload.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] {
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });

                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as SUPPLIER_CODE_TMMIN," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as SUPPLIER_PLANT_TMMIN," +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as SUPPLIER_CODE_ADM," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as SUPPLIER_PLANT_ADM" ;
                OleCommand = OleCommand + " from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;

                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    message = "Error : " + e.Message;
                    location = "SupplierConversionUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] {
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {

                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CODE_TMMIN", "SUPPLIER_CODE_TMMIN");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_PLANT_TMMIN", "SUPPLIER_PLANT_TMMIN");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CODE_ADM", "SUPPLIER_CODE_ADM");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_PLANT_ADM", "SUPPLIER_PLANT_ADM");
                    

                    try
                    {
                        message = "Insert Data to TB_T_SUPPLIER_CONVERSION";
                        location = "SupplierConversionUpload.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] {
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "SupplierConversionUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] {
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                        throw new Exception(ex.Message);
                    }
                    //bulkCopy.WriteToServer(ds.Tables[0]);
                }

                string validate = db.SingleOrDefault<string>("ValidateSupplierConversionUpload", new object[] { processID });

                if (validate == "error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found.")
                {
                    TempData["IsValid"] = "empty";
                    sts = "6";
                    message = validate;
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }

                location = "SupplierConversion.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] {
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            sts      // sts
                                        });

            }
            catch (Exception e)
            {

                TempData["IsValid"] = "false;" + processID + ";" + e.Message;


                message = "Error : " + e.Message;
                location = "SupplierConversion.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] {
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });
                throw new Exception(e.Message);
            }
            finally
            {

                excelConn.Close();

                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

            }
            return _UploadModel;
        }

        public ActionResult MoveDockDataTemp(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            IDBContext db = DbContext();

            try
            {

                db.Execute("moveSupplierConversionDataTemp", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return Content(message);
        }

        public void UploadInvalid(string process_id)
        {
            IDBContext db = DbContext();
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<SuppConvDataUploadError> boForExcel = new List<SuppConvDataUploadError>();

            //boForExcel = db.Query<DockList>("getErrorList", new object[] { process_id }).ToList();

            fileName = "SupplierConversionListError" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            data = exporter.Write(ConvertToDataTable(boForExcel), "SupplierInfo");

            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }
          private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}