﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;
using Portal.Models.RouteCapacity;
using DevExpress.Web.Mvc;
using Portal.Models;
using Toyota.Common.Reporting;
using Toyota.Common.Reporting.Services;


using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using System.ComponentModel;
using NPOI.HSSF.UserModel;
using System.IO;
using NPOI.SS.UserModel;
using Toyota.Common.Web.Messaging;
using System.Web.UI;
using System.Data.OleDb;
using System.Data;
using Toyota.Common.Web.Excel;




 

namespace Portal.Controllers
{
    public class DlvrRouteMasterController : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public DlvrRouteMasterController()
            : base("Route Capacity Master")
        {
            PageLayout.UseMessageBoard = true;
        }

        #region EventPage
        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            List<RouteCapacityData> mdl = new List<RouteCapacityData>();
            ViewData["ListRouteCapacity"] = GetRouteCapacityMaster("");
        }

        public ActionResult RouteCapacityCallback(string value, string key)
        {
            ViewData["values"] = value;
            ViewData["keys"] = key;
            List<RouteCapacityData> List=GetRouteCapacityMaster(GetCondition(value, key)).ToList();
            if (List.Count==0) {List.Add(new RouteCapacityData());}
            return PartialView("RouteCapacityGrid", List);
        }

        public ActionResult RouteCapacityAdd([ModelBinder(typeof(DevExpressEditorsBinder))]RouteCapacityData obj)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ExecuteInsertUpdateRouteCapacity(
                obj.RouteId, obj.RouteCode, obj.TruckType, obj.VolumeTarget,
                obj.EfficiencyTarget,  AuthorizedUser.Username, 'Y');
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";

            }
            return PartialView("RouteCapacityGrid", GetRouteCapacityMaster(""));
        }

        public ActionResult RouteCapacityUpdate([ModelBinder(typeof(DevExpressEditorsBinder))]RouteCapacityData obj)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ExecuteInsertUpdateRouteCapacity(
                obj.RouteId, obj.RouteCode, obj.TruckType, obj.VolumeTarget,
                obj.EfficiencyTarget, AuthorizedUser.Username, 'N');
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";

            }
            return PartialView("RouteCapacityGrid", GetRouteCapacityMaster(""));
        }

        public ActionResult RemoveMaster(string data)
        {
            List<RouteCapacityData> areas = Toyota.Common.Util.Text.JSON.ToObject<List<RouteCapacityData>>(data);
            int ret = areas.Count();
            foreach (RouteCapacityData rout in areas)
            {
                ret = ExecuteDeleteRouteCapacity(rout.RouteId);
            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        #endregion EventPage


        #region NewUpload
        private String _uploadDirectory = "";
        private String _filePath = "";
        string MSG = "";

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        private long _processId = 0;
        private long ProcessId
        {
            get
            {
                if (_processId == 0)
                {
                    IDBContext db = DbContext; 
                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); } 
                    return _processId; 
                } 
                return _processId;
            }
        }
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/DlvrRouteMaster/UploadTemp/");
                return _uploadDirectory;
            }
        } 
        public class UploadControlHelper
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 60751520
            };
        }

        private void ReadingExcelInServer()
        {
            string conStr = "";
            conStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source='" + _FilePath + "'" + "; Extended Properties ='Excel 8.0;HDR=Yes'";
            conStr = string.Format(conStr, _FilePath);
            OleDbConnection con = new OleDbConnection(conStr);
            try
            {
                List<RouteCapacityData> listobj = new List<RouteCapacityData>();
                OleDbDataReader dr = null;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = "Select * from [Sheet1$]";
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    RouteCapacityData obj = new RouteCapacityData();
                    if (!dr.IsDBNull(dr.GetOrdinal("RouteCode"))) obj.RouteCode = Convert.ToString(dr.GetValue(dr.GetOrdinal("RouteCode")));
                    if (!dr.IsDBNull(dr.GetOrdinal("TruckType"))) obj.TruckType = Convert.ToString(dr.GetValue(dr.GetOrdinal("TruckType")));
                    if (!dr.IsDBNull(dr.GetOrdinal("VolumeTarget"))) obj.VolumeTarget = Convert.ToDouble(dr.GetValue(dr.GetOrdinal("VolumeTarget")));
                    if (!dr.IsDBNull(dr.GetOrdinal("EfficiencyTarget"))) obj.EfficiencyTarget = Convert.ToDouble(dr.GetValue(dr.GetOrdinal("EfficiencyTarget")));

                    if (!dr.IsDBNull(dr.GetOrdinal("TimeControlFrom"))) obj.TimeControlFrom = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("TimeControlFrom")));
                    if (!dr.IsDBNull(dr.GetOrdinal("TimeControlTo"))) obj.TimeControlTo = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("TimeControlTo")));
                    listobj.Add(obj);
                }
                dr.Close();
                //proses upload to database
                ProsesUploadToDatabase(listobj);
            }
            catch (Exception ex)
            {
                MSG += "\nField " + ex.Message + " not match in database!";
            }
            finally
            {
                con.Dispose();
            }
        }
        public void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            DataSet ds = new DataSet();
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName; 
                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
                ReadingExcelInServer();
            }

            if (System.IO.File.Exists(_filePath))
            {
                System.IO.File.Delete(_filePath);
            }
            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = MSG;
        }
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("ucCallbacks", UploadControlHelper.ValidationSettings, ucCallbacks_FileUploadComplete);
            return null;
        }
        private void ProsesUploadToDatabase(List<RouteCapacityData> ObjList)
        {
            foreach (RouteCapacityData obj in ObjList)
            {
                UploadRouteCapacity(
                obj.RouteId, obj.RouteCode, obj.TruckType, obj.VolumeTarget,
                obj.EfficiencyTarget, AuthorizedUser.Username,obj.TimeControlFrom,obj.TimeControlTo);
            }
            MSG = "Upload data success";
        }
        #endregion NewUpload

        #region UploadfileOLD


        //public ActionResult CallbackUpload()
        //{
        //    UploadControlExtension.GetUploadedFiles("ucCallbacks", UploadControlHelper.ValidationSettings, ucCallbacks_FileUploadComplete);
        //    return null;
        //}

        //public class UploadControlHelper
        //{
        //    public static readonly ValidationSettings ValidationSettings = new ValidationSettings
        //    {
        //        AllowedFileExtensions = new string[] { ".xls" },
        //        MaxFileSize = 60751520
        //    };
        //}

        //public void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        //{ 
        //    UploadRouteCapacity(new MemoryStream(e.UploadedFile.FileBytes));
        //    IUrlResolutionService urlResolver = sender as IUrlResolutionService;
        //    if (urlResolver != null)
        //        e.CallbackData = MSG;
        //}

        //public void UploadRouteCapacity(Stream filestream)
        //{
        //    HSSFWorkbook hssfworkbook;
        //    List<RouteCapacityData> list = new List<RouteCapacityData>();
        //    hssfworkbook = new HSSFWorkbook(filestream);
        //    ISheet sheet = hssfworkbook.GetSheet("Sheet1");
        //    int lastrow = sheet.LastRowNum;
        //    for (int i = 1; i <= lastrow; i++)// row dimulai dari 0
        //    {
        //        try
        //        {
        //            IRow row = sheet.GetRow(i);
        //            RouteCapacityData obj = new RouteCapacityData();
        //            obj.RouteId = Convert.ToInt32(GetValueCell(row.GetCell(0)));
        //            obj.RouteCode = GetValueCell(row.GetCell(1));
        //            obj.TruckType = GetValueCell(row.GetCell(2));
        //            obj.VolumeTarget = Convert.ToInt32(GetValueCell(row.GetCell(3)));
        //            obj.EfficiencyTarget = Convert.ToInt32(GetValueCell(row.GetCell(4)));
        //            list.Add(obj);
                    
        //        }
        //        catch (Exception e)
        //        {
        //            MSG += "\nRow (" + (i+1) + ") error : " + e.Message;
        //        }
        //    }
        //    if (MSG=="")
        //        ProsesUploadToDatabase(list);
        //}

        //private string GetFileExtension(string sFileName)
        //{
        //    sFileName = sFileName.Trim();
        //    if (String.IsNullOrEmpty(sFileName))
        //    {
        //        return String.Empty;
        //    }
        //    string sExtension = String.Empty;
        //    char[] cArr = sFileName.ToCharArray();
        //    int iIndex = 0;
        //    for (int i = cArr.Length - 1; i > -1; i--)
        //    {
        //        if (cArr[i].Equals('.'))
        //        {
        //            iIndex = i;
        //            break;
        //        }
        //    }
        //    if (iIndex > 0)
        //    {
        //        for (int i = iIndex + 1; i < cArr.Length; i++)
        //        {
        //            sExtension += cArr[i];
        //        }
        //    }
        //    return sExtension.ToLower();

        //}

        //private string GetValueCell(ICell cell)
        //{
        //    if (cell.CellType.ToString() == "NUMERIC")
        //    {
        //        return Convert.ToString(cell.NumericCellValue);
        //    }
        //    else if (cell.CellType.ToString() == "STRING")
        //    {
        //        return cell.StringCellValue;
        //    }
        //    else
        //    {
        //        return Convert.ToString(cell.DateCellValue);
        //    }
        //}

        //private void ProsesUploadToDatabase(List<RouteCapacityData> ObjList)
        //{ 
        //    foreach (RouteCapacityData obj in ObjList)
        //    { 
        //        UploadRouteCapacity(
        //        obj.RouteId, obj.RouteCode, obj.TruckType, obj.VolumeTarget,
        //        obj.EfficiencyTarget, AuthorizedUser.Username);
        //    }
        //    MSG = "Upload data success";
        //}

        #endregion UploadfileOLD


        #region NewDownload
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadFile(string  value, string key)
        {
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;
            string fileName = String.Empty;
            List<RouteCapacityData> List = GetRouteCapacityMaster(GetCondition(value, key)).ToList();
            fileName = "RouteCapacity.xls";
            hasil = exporter.Write(ConvertToDataTable(List), "Sheet1");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion NewDownload

        #region DownloadReport
        //public void DownloadFile(string value, string key)
        //{

        //    var workbook = new HSSFWorkbook();
        //    var sheet = workbook.CreateSheet("Sheet1");

        //    // Siap siap buat header
        //    var rowIndex = 0;
        //    var row = sheet.CreateRow(rowIndex);

        //    var cell0 = sheet.CreateRow(0).CreateCell(0);
        //    var cell1 = sheet.CreateRow(0).CreateCell(1);
        //    var cell2 = sheet.CreateRow(0).CreateCell(2);
        //    var cell3 = sheet.CreateRow(0).CreateCell(3);
        //    var cell4 = sheet.CreateRow(0).CreateCell(4); 

        //    // Set warna cell header background
        //    var warna = workbook.CreateCellStyle();
        //    warna.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
        //    warna.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
        //    warna.FillPattern = FillPatternType.SOLID_FOREGROUND;

        //    // Set font
        //    var font = workbook.CreateFont();
        //    font.FontHeightInPoints = 10;                                                   // Font size
        //    font.FontName = "Times New Roman";                                              // Font type
        //    font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;                 // Font weight

        //    var cell = row.CreateCell(0);
        //    cell.CellStyle = workbook.CreateCellStyle();
        //    cell.CellStyle.SetFont(font);

        //    // Set lebar cell header
        //    //sheet.SetColumnWidth(0,50);
        //    //sheet.SetColumnWidth(1, 70);
        //    //sheet.SetColumnWidth(2, 100);
        //    //sheet.SetColumnWidth(3, 150);
        //    //sheet.SetColumnWidth(4, 150);       
        //    sheet.AutoSizeColumn(0);
        //    sheet.AutoSizeColumn(1);
        //    sheet.AutoSizeColumn(2);
        //    sheet.AutoSizeColumn(3);
        //    sheet.AutoSizeColumn(4);
        //    // Freeze row header
        //    sheet.CreateFreezePane(0, 1, 0, 1);

        //    // Buat header
        //    cell0.CellStyle = warna;
        //    cell0.SetCellValue("Route Id");
        //    cell0.CellStyle.SetFont(font); 
        //    cell1.CellStyle = warna;
        //    cell1.SetCellValue("Route Code");
            


        //    cell2.CellStyle = warna;
        //    cell2.SetCellValue("Truck Type");

        //    cell3.CellStyle = warna;
        //    cell3.SetCellValue("Volume Target");

        //    cell4.CellStyle = warna;
        //    cell4.SetCellValue("Efficiency Target");
            
        //    rowIndex++;

        //    List<RouteCapacityData> List = GetRouteCapacityMaster(GetCondition(value, key)).ToList();
        //    // Buat row content
        //    foreach (RouteCapacityData obj in List)
        //    {
        //        row = sheet.CreateRow(rowIndex);
        //        row.CreateCell(0, CellType.STRING).SetCellValue(obj.RouteId);
        //        row.CreateCell(1, CellType.STRING).SetCellValue(obj.RouteCode);
        //        row.CreateCell(2, CellType.STRING).SetCellValue(obj.TruckType);
        //        row.CreateCell(3, CellType.STRING).SetCellValue(obj.VolumeTarget);
        //        row.CreateCell(4, CellType.STRING).SetCellValue(obj.EfficiencyTarget);  
        //        rowIndex++;
        //    } 

        //    //// Auto-size setiap kolom
        //    //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
        //    //    sheet.AutoSizeColumn(i);

        //    // Simpan Workbook di MemoryStream lalu panggil di Client
        //    using (var exportData = new MemoryStream())
        //    {
        //        workbook.Write(exportData);

        //        string saveAsFileName = string.Format("RouteCapacity-{0:d}.xls", DateTime.Now).Replace("/", "-");

        //        Response.ClearContent();
        //        Response.Buffer = true;
        //        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
        //        Response.ContentType = "application/vnd.ms-excel";
        //        Response.BinaryWrite(exportData.GetBuffer());
        //        Response.Flush();
        //        Response.End();
        //    }


            //string filename = "RouteCapacityData";
            //try
            //{
            //    ReportResult result = GenerateReport(GetCondition(value, key), Server.MapPath("~/ReportingServices/" + filename + ".rdlc"), filename, MicrosoftReportType.Excel.ToString());
            //    Response.Clear();
            //    Response.Cache.SetCacheability(HttpCacheability.Private);
            //    Response.Expires = -1;
            //    Response.Buffer = true;
            //    Response.ContentType = "application/octet-stream";
            //    Response.AddHeader("Content-Length", Convert.ToString(result.DocumentBytes.Length));
            //    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", result.Name));
            //    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
            //    Response.BinaryWrite(result.DocumentBytes);
            //    Response.End();
            //}
            //catch (Exception e)
            //{
            //    string erore = e.Message;
            //}
        //}

        private string GetCondition(string value, string key)
        {
            string where="";
            if ((key == "") || (key == null))
            {
                return where;
            }
            else
            {
                string[] Arrkey = key.Split(';');
                where = " WHERE ";
                string rplkey = "";
                foreach (string keyvalue in Arrkey)
                {
                    //didapat dari view di grid
                    if (keyvalue == "Route Code")
                    {
                        rplkey = "ROUTE_CD";
                    }
                    else if (keyvalue == "Truck Type")
                    {
                        rplkey = "TRUCK_TYPE";
                    }
                    //else if (keyvalue == "Volume")
                    //{
                    //    rplkey = "VOLUME_TARGET";
                    //}
                    //else if (keyvalue == "Efficiency")
                    //{
                    //    rplkey = "EFFICIENCY_TARGET";
                    //}
                    where = where + rplkey + " like '%" + value + "%' OR ";
                }
                where = where.Substring(0, (where.Length - 3));
                return where;
            }
            
        }

        #endregion DownloadReport

        #region CRUD
        public RouteCapacityData GetSingleRouteMaster(int routeid)
        {
            RouteCapacityData data;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            data = db.SingleOrDefault<RouteCapacityData>("RouteCapacitySingleMasterGet", new object[] { routeid });
            db.Close();
            return data;

        }

        public IEnumerable<RouteCapacityData> GetRouteCapacityMaster(string condition)
        {
            IEnumerable<RouteCapacityData> listdata;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            listdata = db.Query<RouteCapacityData>("RouteCapacityMasterGet", new object[] { condition });
            db.Close();
            return listdata;
        }

        public int ExecuteInsertUpdateRouteCapacity
            (
             int routeId,
             string routeCode, 
             string truckType, 
             double volumeTarget, 
             double efficiencyTarget, 
             string optr,
             char inserting
            )
        {
            int result;
            if (inserting == 'Y')
            {
                result = db.Execute("RouteCapacityMasterInsert",new object[] 
                { 
                    routeId,
                    routeCode,
                    truckType,
                    volumeTarget,
                    efficiencyTarget,
                    optr
                });
                    
                    
            }
            else
            {
                result = db.Execute("RouteCapacityMasterUpdate", new object[] 
                { 
                    routeId,
                    routeCode,
                    truckType,
                    volumeTarget,
                    efficiencyTarget,
                    optr
                });
            }
            db.Close();
            return result;
        }

        public int UploadRouteCapacity
            (
             int routeId,
             string routeCode,
             string truckType,
             double volumeTarget,
             double efficiencyTarget,
             string optr,
            DateTime from,
            DateTime to
            )
        {
            int result;
            result = db.Execute("RouteCapacityMasterUpload", new object[] 
                { 
                    routeId,
                    routeCode,
                    truckType,
                    volumeTarget,
                    efficiencyTarget,
                    optr,
                    from,
                    to
                });
            db.Close();
            return result;
        }

        public int ExecuteDeleteRouteCapacity(int routeId)
        {
            int result = db.Execute("RouteCapacityMasterDelete", new object[] { routeId });
            db.Close();
            return result;
        }

        #endregion CRUD
    }
}

        //public ReportResult GenerateReport(string data, string reportPath,string namafile,string rpttype)
        //{
        //    List<DataTableSource> dataSource = new List<DataTableSource>();
        //    dataSource.Add(new DataTableSource()
        //    {
        //        DataTable = ReportHelper.ConvertToDataTable<RouteCapacityData>(GetRouteCapacityMaster(data)),
        //        Name = namafile
        //    });

        //    ReportResult result;
        //    Toyota.Common.Reporting.Services.MicrosoftReport reportService = new MicrosoftReport(namafile, reportPath, dataSource, rpttype);
        //    result = reportService.Generate();
        //    return result;
        //}
        //public ActionResult CallOpenPopUp(int ids)
        //{
        //    RouteCapacityData route = GetSingleRouteMaster(ids);
        //    return Json(route, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult InsertUpdateRouteCapacityMaster(int routeId,string routeCode, string truckType,string volTarget, string effTarget, char inserting)
        //{
        //    double vol, eff;
        //    vol = System.Convert.ToDouble(volTarget);
        //    eff = System.Convert.ToDouble(effTarget);
        //    int result=0;
        //    result = ExecuteInsertUpdateRouteCapacity(routeId,routeCode,truckType,vol,eff,AuthorizedUser.Username,inserting);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult DeleteDataMaster(int routeid)
        //{
        //    int result = 0;
        //    result = ExecuteDeleteRouteCapacity(routeid);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
