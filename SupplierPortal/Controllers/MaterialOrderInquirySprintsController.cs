﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Portal.Models.Supplier;
using Portal.Models.MaterialSprints;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.ASPxUploadControl;
using System.Xml;
using System.Diagnostics;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using DevExpress.Web.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using DevExpress.Utils.Zip;
using System.Configuration;
using Portal.Models.LogMonitoring;
using System.Globalization;
//using System.Configuration;

namespace Portal.Controllers
{
    public class MaterialOrderInquirySprintsController : BaseController
    {
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
            {
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            }
            else
            {
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public MaterialOrderInquirySprintsController()
            : base("Material Order Inquiry Sprints")
        {
            PageLayout.UseMessageBoard = true;
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        #region Model properties

        List<SupplierName> _oSupplier = null;
        private List<SupplierName> OSupplier
        {
            get
            {
                if (_oSupplier == null)
                    _oSupplier = new List<SupplierName>();

                return _oSupplier;
            }
            set
            {
                _oSupplier = value;
            }
        }

        List<MaterialSprints> _oMaterial = null;
        private List<MaterialSprints> OMaterial
        {
            get
            {
                if (_oMaterial == null)
                    _oMaterial = new List<MaterialSprints>();

                return _oMaterial;
            }
            set
            {
                _oMaterial = value;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/MaterialOrderInquirySprints/Temporary/");
                return _uploadDirectory;
            }
        }

        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;

            List<MaterialSprints> oMaterialSprints = new List<MaterialSprints>();
            Model.AddModel(oMaterialSprints);

            string dateFrom = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            string dateTo = DateTime.Now.ToString("dd.MM.yyyy");

            ViewData["DateFrom"] = dateFrom;
            ViewData["DateTo"] = dateTo;
            //ViewData["MaterialSprintsModel"] = GetAllMaterialOrderInquiry(oMaterialSprints, dateFrom, dateTo);
        }


        public ActionResult PartialHeaderMaterialOrderInquiry()
        {
            Session["paramManifestNo"] = String.IsNullOrEmpty(Request.Params["ManifestNos"]) ? Session["paramManifestNo"] : Request.Params["ManifestNos"];

            #region Required model in partial page : for HeaderMaterialOrderInquiry
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "MaterialOrderInquirySprints", "OIHSupplierOption");
            OSupplier = suppliers;

            //List<MaterialSprints> materials = Model.GetModel<User>().FilteringArea<MaterialSprints>(GetAllMaterial(), "MATERIAL_NO", "MaterialOrderInquirySprints", "OIHMaterialOption");
            //OMaterial = materials;

            Model.AddModel(OSupplier);
            //Model.AddModel(OMaterial);

            #endregion

            return PartialView("PartialHeaderMaterialOrderInquiry", Model);
        }

        public ActionResult PartialDetailMaterialOrderInquiry()
        {

            MaterialSprints oMaterialSprints = new MaterialSprints
            {
                SUPPLIER_CD = Request.Params["SupplierCode"],
                //MATERIAL_NO = Request.Params["MaterialNo"],
                ORDER_NO = Request.Params["OrderNo"]
            };
            //User UserModel = Model.GetModel<User>();
            Model.AddModel(GetAllMaterialOrderInquiry(oMaterialSprints, Request.Params["DateFrom"], Request.Params["DateTo"]));
            //if (UserModel.Authorization[0].AuthorizationName == "System Administrator")
            //    Model.AddModel(GetAllMaterialOrderInquiry(oMaterialSprints, Request.Params["DateFrom"], Request.Params["DateTo"]));
            //else
            //{
            //    if(!string.IsNullOrWhiteSpace(oMaterialSprints.SUPPLIER_CD))
            //        Model.AddModel(GetAllMaterialOrderInquiry(oMaterialSprints, Request.Params["DateFrom"], Request.Params["DateTo"]));
            //}

            return PartialView("PartialDetailMaterialOrderInquiry", Model);
        }

        public ActionResult PartialViewOrderHeader()
        {
            return PartialView("PartialViewOrderHeader");
        }

        public ActionResult PartialViewOrderDetail()
        {
            var ArrivalPlanDate = Request.Params["ArrivalPlanDate"];
            MaterialSprints oMaterialSprints = new MaterialSprints
            {
                ORDER_NO = Request.Params["OrderNo"],
                PLAN_RECEIVING_DATE = ArrivalPlanDate != null ? Convert.ToDateTime(ArrivalPlanDate) : (DateTime?)null
            };

            Model.AddModel(GetAllMaterialOrderDetail(oMaterialSprints));

            return PartialView("PartialViewOrderDetail", Model);
        }

        public ActionResult PartialPopupViewSeqDetail()
        {
            MaterialSprints oMaterialSprints = new MaterialSprints
            {
                ORDER_NO_KBN_SEQ = Request.Params["OrderNoKBNSeq"]
            };

            Model.AddModel(GetAllMaterialSeqDetail(oMaterialSprints));

            return PartialView("PartialPopupViewSeqDetail", Model);
        }

        public ActionResult LookupGridSupplier()
        {
            #region Required model in partial page : for HeaderMaterialOrderInquiry

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "MaterialOrderInquirySprints", "OIHSupplierOption");
            OSupplier = suppliers;

            TempData["GridName"] = "OIHSupplierOption";

            #endregion

            return PartialView("GridLookup/PartialGrid", OSupplier);
        }

        //public ActionResult LookupGridMaterial()
        //{
        //    #region Required model in partial page : for HeaderMaterialOrderInquiry

        //    List<MaterialSprints> materials = Model.GetModel<User>().FilteringArea<MaterialSprints>(GetAllMaterial(), "MATERIAL_NO", "MaterialOrderInquirySprints", "OIHMaterialOption");
        //    OMaterial = materials;

        //    TempData["GridName"] = "OIHMaterialOption";

        //    #endregion

        //    return PartialView("GridLookup/PartialGrid", OMaterial);
        //}

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> oSuppliers = new List<SupplierName>();
            IDBContext db = DbContext;
            oSuppliers = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return oSuppliers;
        }

        private List<MaterialSprints> GetAllMaterial()
        {
            List<MaterialSprints> oMaterials = new List<MaterialSprints>();
            IDBContext db = DbContext;
            oMaterials = db.Fetch<MaterialSprints>("GetAllMaterialSprints", new object[] { });
            db.Close();

            return oMaterials;
        }

        private List<MaterialSprints> GetAllMaterialOrderInquiry(MaterialSprints oMaterialSprints, string dateFrom, string dateTo)
        {
            List<MaterialSprints> oMaterials = new List<MaterialSprints>();

            IDBContext db = DbContext;
            oMaterials = db.Fetch<MaterialSprints>("GetAllMaterialOrderInquirySprints", new object[] { 
                oMaterialSprints.SUPPLIER_CD,
                oMaterialSprints.MATERIAL_NO,
                oMaterialSprints.ORDER_NO,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null)
            });
            db.Close();

            return oMaterials;
        }

        private List<MaterialSprints> GetAllMaterialOrderDetail(MaterialSprints oMaterialSprints)
        {
            List<MaterialSprints> oMaterials = new List<MaterialSprints>();
            oMaterialSprints.PLAN_RECEIVING_TIME = oMaterialSprints.PLAN_RECEIVING_DATE != null ? oMaterialSprints.PLAN_RECEIVING_DATE.Value.TimeOfDay : (TimeSpan?)null;
            //String strDateFormat = "yyyy-MM-dd";;
            //String strTimeFormat = "hh:mm:ss.FFFFFFF";
            //TimeSpan ArrivalPlanTime = TimeSpan.ParseExact(oMaterialSprints.PLAN_RECEIVING_DATE.ToString(), strTimeFormat, CultureInfo.InvariantCulture);
            IDBContext db = DbContext;
            oMaterials = db.Fetch<MaterialSprints>("GetAllMaterialOrderDetailSprints", new object[] { 
                oMaterialSprints.ORDER_NO,
                oMaterialSprints.PLAN_RECEIVING_DATE != null ? oMaterialSprints.PLAN_RECEIVING_DATE.Value.Date : (DateTime?)null,
                oMaterialSprints.PLAN_RECEIVING_TIME 
            });
            db.Close();

            return oMaterials;
        }

        private List<MaterialSprints> GetAllMaterialSeqDetail(MaterialSprints oMaterialSprints)
        {
            List<MaterialSprints> oMaterials = new List<MaterialSprints>();

            IDBContext db = DbContext;
            oMaterials = db.Fetch<MaterialSprints>("GetAllMaterialSeqDetailSprints", new object[] { 
                oMaterialSprints.ORDER_NO_KBN_SEQ
            });
            db.Close();

            return oMaterials;
        }

        private string AddLog(string message, string messageId, string username, string location, string module, string function, string status, string processId)
        {
            IDBContext db = DbContext;
            string processIdResult = db.ExecuteScalar<string>("GenerateProcessId",
                    new object[] 
                    { 
                        message,     // what
                        username,     // user
                        location,     // where
                        processId,     // pid OUTPUT
                        messageId,     // id
                        "",     // type
                        module,     // module
                        function,     // function
                        status     // sts
                    });

            db.Close();
            return processIdResult;
        }

        private bool InsertTemp(MaterialSprints oMaterialSprints, string username, string processId, string moduleId, string functionId, string location, string currentOrderNo)
        {
            IDBContext db = DbContext;

            int returnVal = db.ExecuteScalar<int>("UploadBatchInsertTempSprints", new object[] { 
                username,
                processId,
                moduleId,
                functionId,
                location,
                currentOrderNo,
                oMaterialSprints.COMP_CD,
                oMaterialSprints.PLANT_CD,
                oMaterialSprints.ORDER_NO,
                oMaterialSprints.SEQ_NO,
                oMaterialSprints.MATERIAL_NO,
                oMaterialSprints.BATCH,
                oMaterialSprints.QTY,
                oMaterialSprints.WEIGHT,
                oMaterialSprints.MILLS_CERT_NO
            });

            db.Close();

            return Convert.ToBoolean(returnVal);
        }

        private bool InsertAllOrderNoTemp(MaterialSprints oMaterialSprints, string username, string processId, string moduleId, string functionId, string location)
        {
            IDBContext db = DbContext;

            int returnVal = db.ExecuteScalar<int>("UploadBatchInsertTempAllOrderNoSprints", new object[] { 
                username,
                processId,
                moduleId,
                functionId,
                location,
                oMaterialSprints.COMP_CD,
                oMaterialSprints.PLANT_CD,
                oMaterialSprints.ORDER_NO,
                oMaterialSprints.SEQ_NO,
                oMaterialSprints.MATERIAL_NO,
                oMaterialSprints.BATCH,
                oMaterialSprints.QTY,
                oMaterialSprints.WEIGHT,
                oMaterialSprints.MILLS_CERT_NO
            });

            db.Close();

            return Convert.ToBoolean(returnVal);
        }

        private bool Insert(string username, string processId, string moduleId, string functionId, string location, string currentOrderNo)
        {
            IDBContext db = DbContext;

            int returnVal = db.ExecuteScalar<int>("UploadBatchInsertSprints", new object[] { 
                username,
                processId,
                moduleId,
                functionId,
                location,
                currentOrderNo
            });

            db.Close();

            return Convert.ToBoolean(returnVal);
        }

        private bool InsertAllOrderNo(string username, string processId, string moduleId, string functionId, string location)
        {
            IDBContext db = DbContext;

            int returnVal = db.ExecuteScalar<int>("UploadBatchInsertAllOrderNoSprints", new object[] { 
                username,
                processId,
                moduleId,
                functionId,
                location
            });

            db.Close();

            return Convert.ToBoolean(returnVal);
        }

        private string GenerateTemp(string username, String ListOfOrderNoKbnSeq, IDBContext db)
        {
            string returnVal = db.ExecuteScalar<string>("GenerateManifestTempSprints", new object[] { 
                username,
                ListOfOrderNoKbnSeq
            });

            return returnVal;
        }

        private string Generate(string username, IDBContext db)
        {
            string returnVal = db.ExecuteScalar<string>("GenerateManifestSprints", new object[] { 
                username
            });

            return returnVal;
        }

        private void DeleteGenerateTemp(string username)
        {
            IDBContext db = DbContext;
            int returnVal = db.ExecuteScalar<int>("DeleteGenerateManifestTempSprints", new object[] { 
                username
            });
        }

        #endregion

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("FileUpload", ValidationSettings, FileUploadComplete);
        }

        [HttpPost]
        public void OnUploadMOIHCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("MOIFileUpload", ValidationSettings, OnMOIUploadComplete);
        }

        [HttpPost]
        public ActionResult Upload(string filePath, string orderNo)
        {
            string function = "91001";
            string module = "9";
            string username = AuthorizedUser.Username;
            string status = "0";
            string message = "Upload Batch is started";
            string messageId = "MPCS00002INF";
            string processId = string.Empty;
            string location = "MaterialOrderInquirySPRINTS";

            processId = AddLog(message, messageId, username, location, module, function, status, processId);

            FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            string filetype = Path.GetExtension(file.Name);
            try
            {
                HSSFWorkbook oHSSFWorkbook = new HSSFWorkbook(file);
                ISheet oISheet = oHSSFWorkbook.GetSheet("UploadBatch");
                if (oISheet != null)
                {
                    if (oISheet.LastRowNum == 1)
                    {
                        message = string.Format("Excel has no data");
                        messageId = "MPCS00013ERR";
                        status = "6";
                        AddLog(message, messageId, username, location, module, function, status, processId);
                    }

                    IRow header = oISheet.GetRow(1);
                    if (header.LastCellNum != 9)
                    {
                        message = string.Format("Excel format is not valid. Please make sure following the correct format.");
                        messageId = "MSPT00001ERR";
                        status = "6";
                        AddLog(message, messageId, username, location, module, function, status, processId);
                    }

                    if (status != "6")
                    {

                        MaterialSprints oMaterialSprints = new MaterialSprints();

                        for (int i = 2; i <= oISheet.LastRowNum; i++)
                        {
                            IRow oIRow = oISheet.GetRow(i);

                            if (oIRow != null)
                            {
                                if ((oIRow.GetCell(0) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(0).ToString())) &&
                                    (oIRow.GetCell(1) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(1).ToString())) &&
                                    (oIRow.GetCell(2) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(2).ToString())) &&
                                    (oIRow.GetCell(3) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(3).ToString())) &&
                                    (oIRow.GetCell(4) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(4).ToString())) &&
                                    (oIRow.GetCell(5) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(5).ToString())) &&
                                    (oIRow.GetCell(6) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(6).ToString())) &&
                                    (oIRow.GetCell(7) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(7).ToString())) &&
                                    (oIRow.GetCell(8) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(8).ToString())))
                                {
                                    continue;
                                }

                                oMaterialSprints = new MaterialSprints();

                                oMaterialSprints.COMP_CD = oIRow.GetCell(0) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(0).ToString()) ? null : oIRow.GetCell(0).ToString().Trim();
                                oMaterialSprints.PLANT_CD = oIRow.GetCell(1) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(1).ToString()) ? null : oIRow.GetCell(1).ToString().Trim();
                                oMaterialSprints.ORDER_NO = oIRow.GetCell(2) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(2).ToString()) ? null : oIRow.GetCell(2).ToString().Trim();
                                oMaterialSprints.SEQ_NO = oIRow.GetCell(3) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(3).ToString()) ? null : oIRow.GetCell(3).ToString().Trim();
                                //oMaterialSprints.SEQ_NO = oIRow.GetCell(3) == null || oIRow.GetCell(3).CellType != CellType.NUMERIC ? (int?)null : (int)oIRow.GetCell(3).NumericCellValue;
                                oMaterialSprints.MATERIAL_NO = oIRow.GetCell(4) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(4).ToString()) ? null : oIRow.GetCell(4).ToString().Trim();
                                oMaterialSprints.BATCH = oIRow.GetCell(5) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(5).ToString()) ? null : oIRow.GetCell(5).ToString().Trim();
                                oMaterialSprints.QTY = oIRow.GetCell(6) == null || oIRow.GetCell(6).CellType != CellType.NUMERIC ? (int?)null : (int)oIRow.GetCell(6).NumericCellValue;
                                oMaterialSprints.WEIGHT = oIRow.GetCell(7) == null || oIRow.GetCell(7).CellType != CellType.NUMERIC ? (decimal?)null : (decimal)oIRow.GetCell(7).NumericCellValue;
                                oMaterialSprints.MILLS_CERT_NO = oIRow.GetCell(8) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(8).ToString()) ? null : oIRow.GetCell(8).ToString().Trim();

                                InsertTemp(oMaterialSprints, username, processId, module, function, location, orderNo);

                            }

                        }

                        if (Insert(username, processId, module, function, location, orderNo))
                        {
                            message = "Upload Batch is finished";
                            messageId = "MPCS00003INF";
                            status = "0";
                        }
                        else
                        {
                            message = "Upload Batch is finished with error";
                            messageId = "MSPT00006INF";
                            status = "6";
                        }

                        AddLog(message, messageId, username, location, module, function, status, processId);
                    }
                }
                else
                {
                    message = "Sheet name must be \"UploadBatch\"";
                    messageId = "MSPT00001ERR";
                    status = "6";
                    AddLog(message, messageId, username, location, module, function, status, processId);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            finally
            {
                file.Close();
                DeleteFile(filePath);
            }

            return Json(
            new
            {
                ProcessID = processId,
                Message = message,
                Status = status
            });
        }

        [HttpPost]
        public ActionResult UploadAllOrderNo(string filePath)
        {
            string function = "91001";
            string module = "9";
            string username = AuthorizedUser.Username;
            string status = "0";
            string message = "Upload Batch is started";
            string messageId = "MPCS00002INF";
            string processId = string.Empty;
            string location = "MaterialOrderInquirySPRINTS";

            processId = AddLog(message, messageId, username, location, module, function, status, processId);

            FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            string filetype = Path.GetExtension(file.Name);
            try
            {
                HSSFWorkbook oHSSFWorkbook = new HSSFWorkbook(file);
                ISheet oISheet = oHSSFWorkbook.GetSheet("UploadBatch");
                if (oISheet != null)
                {
                    if (oISheet.LastRowNum == 1)
                    {
                        message = string.Format("Excel has no data");
                        messageId = "MPCS00013ERR";
                        status = "6";
                        AddLog(message, messageId, username, location, module, function, status, processId);
                    }

                    IRow header = oISheet.GetRow(1);
                    if (header.LastCellNum != 9)
                    {
                        message = string.Format("Excel format is not valid. Please make sure following the correct format.");
                        messageId = "MSPT00001ERR";
                        status = "6";
                        AddLog(message, messageId, username, location, module, function, status, processId);
                    }

                    if (status != "6")
                    {

                        MaterialSprints oMaterialSprints = new MaterialSprints();

                        for (int i = 2; i <= oISheet.LastRowNum; i++)
                        {
                            IRow oIRow = oISheet.GetRow(i);

                            if (oIRow != null)
                            {
                                if ((oIRow.GetCell(0) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(0).ToString())) &&
                                    (oIRow.GetCell(1) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(1).ToString())) &&
                                    (oIRow.GetCell(2) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(2).ToString())) &&
                                    (oIRow.GetCell(3) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(3).ToString())) &&
                                    (oIRow.GetCell(4) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(4).ToString())) &&
                                    (oIRow.GetCell(5) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(5).ToString())) &&
                                    (oIRow.GetCell(6) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(6).ToString())) &&
                                    (oIRow.GetCell(7) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(7).ToString())) &&
                                    (oIRow.GetCell(8) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(8).ToString())))
                                {
                                    continue;
                                }

                                oMaterialSprints = new MaterialSprints();

                                oMaterialSprints.COMP_CD = oIRow.GetCell(0) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(0).ToString()) ? null : oIRow.GetCell(0).ToString().Trim();
                                oMaterialSprints.PLANT_CD = oIRow.GetCell(1) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(1).ToString()) ? null : oIRow.GetCell(1).ToString().Trim();
                                oMaterialSprints.ORDER_NO = oIRow.GetCell(2) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(2).ToString()) ? null : oIRow.GetCell(2).ToString().Trim();
                                oMaterialSprints.SEQ_NO = oIRow.GetCell(3) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(3).ToString()) ? null : oIRow.GetCell(3).ToString().Trim();
                                //oMaterialSprints.SEQ_NO = oIRow.GetCell(3) == null || oIRow.GetCell(3).CellType != CellType.NUMERIC ? (int?)null : (int)oIRow.GetCell(3).NumericCellValue;
                                oMaterialSprints.MATERIAL_NO = oIRow.GetCell(4) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(4).ToString()) ? null : oIRow.GetCell(4).ToString().Trim();
                                oMaterialSprints.BATCH = oIRow.GetCell(5) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(5).ToString()) ? null : oIRow.GetCell(5).ToString().Trim();
                                oMaterialSprints.QTY = oIRow.GetCell(6) == null || oIRow.GetCell(6).CellType != CellType.NUMERIC ? (int?)null : (int)oIRow.GetCell(6).NumericCellValue;
                                oMaterialSprints.WEIGHT = oIRow.GetCell(7) == null || oIRow.GetCell(7).CellType != CellType.NUMERIC ? (decimal?)null : (decimal)oIRow.GetCell(7).NumericCellValue;
                                oMaterialSprints.MILLS_CERT_NO = oIRow.GetCell(8) == null || string.IsNullOrWhiteSpace(oIRow.GetCell(8).ToString()) ? null : oIRow.GetCell(8).ToString().Trim();

                                InsertAllOrderNoTemp(oMaterialSprints, username, processId, module, function, location);

                            }

                        }

                        if (InsertAllOrderNo(username, processId, module, function, location))
                        {
                            message = "Upload Batch is finished";
                            messageId = "MPCS00003INF";
                            status = "0";
                        }
                        else
                        {
                            message = "Upload Batch is finished with error";
                            messageId = "MSPT00006INF";
                            status = "6";
                        }

                        AddLog(message, messageId, username, location, module, function, status, processId);
                    }
                }
                else
                {
                    message = "Sheet name must be \"UploadBatch\"";
                    messageId = "MSPT00001ERR";
                    status = "6";
                    AddLog(message, messageId, username, location, module, function, status, processId);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            finally
            {
                file.Close();
                DeleteFile(filePath);
            }

            return Json(
            new
            {
                ProcessID = processId,
                Message = message,
                Status = status
            });
        }

        [HttpPost]
        public ActionResult Generate(List<MaterialSprints> oMaterialSprints)
        {
            string message = string.Empty;
            string username = AuthorizedUser.Username;

            IDBContext db = DbContext;

            try
            {
                db.BeginTransaction();

                //foreach (MaterialSprints oMaterial in oMaterialSprints)
                //{
                string ListOfSelectedKanbanSeq = string.Join(",", oMaterialSprints.Select(x => x.ORDER_NO_KBN_SEQ).ToList());
                //}
                message = GenerateTemp(username, ListOfSelectedKanbanSeq, db);

                if (string.IsNullOrEmpty(message))
                {
                    db.CommitTransaction();

                    db = DbContext;
                    db.BeginTransaction();

                    message = Generate(username, db);
                    if (string.IsNullOrEmpty(message))
                        db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            finally
            {
                DeleteGenerateTemp(username);
            }

            return Json(
            new
            {
                Message = message
            });
        }

        public FileContentResult DownloadManifestUploadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "UploadBatchTemplate.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/MaterialOrderInquirySprints/Template/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        public void DownloadErrorLog(string ProcessId)
        {
            string fileName = "UploadBatchErrorlog" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportUploadBatch> qry = db.Query<ReportUploadBatch>("ReportGetUploadBatchErrorLog", new object[] { ProcessId });
            db.Close();
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(qry, "UploadBatch");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
            //string fileName = "UploadBatchErrorlog.xls";

            //IDBContext db = DbContext;
            //IExcelWriter exporter = new NPOIWriter();
            //byte[] hasil;

            //IEnumerable<LogMonitoringMaster> qry = db.Fetch<LogMonitoringMaster>("ReportGetUploadBatchErrorLog", new object[] { 
            //        ProcessId == null ? "" : ProcessId
            //    });
            //hasil = exporter.Write(qry, "UploadBatch");

            //db.Close();

            //Response.Clear();
            ////Response.ContentType = result.MimeType;
            //Response.Cache.SetCacheability(HttpCacheability.Private);
            //Response.Expires = -1;
            //Response.Buffer = true;

            //Response.ContentType = "application/octet-stream";
            //Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            //Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            //Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            //Response.BinaryWrite(hasil);
            //Response.End();
        }

        public void DownloadList(string SupplierCode,
                                 string OrderNo,
                                 string DateFrom,
                                 string DateTo)
        {
            //string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "MaterialOrderInquirySprints", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            //if (suppCode.Count == 1)
            //{
            //    SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            //}
            //else
            //{
            //    SupplierCodeLDAP = "";
            //}

           

            string fileName = "MaterialOrderSprintDownload.xls";

            IDBContext db = DbContext;
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            IEnumerable<ReportMaterialSprintsOrder> qry = db.Fetch<ReportMaterialSprintsOrder>("GetAllMaterialOrderPrintingSprints", new object[] {
                SupplierCode,
                OrderNo == null ? "" : OrderNo,
                (DateFrom == "" || DateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(DateFrom,"dd.MM.yyyy",null),
                (DateTo == "" || DateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(DateTo ,"dd.MM.yyyy",null)
            });
            hasil = exporter.Write(qry, "OrderPrintingSPEXData");
            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        #region DownloadPDF
        public FileContentResult DownloadPDF(string ManifestNo, string OrderNo, string TtlKbn)
        {
            FTPUpload vFtp = new FTPUpload();
            string zipName = string.Format("{0}.zip", ManifestNo);
            string folderReport = "/Report/MaterialOrderSprints/";
            //string folderTemp = "/Views/MaterialOrderInquirySprints/Temporary/";
            string folderTemp = vFtp.Setting.FtpPath("MaterialOrderInquiry");

            if (!vFtp.IsDirectoryExist(folderTemp, ""))
            {
                vFtp.CreateDirectory(folderTemp);
            }

            string temp = "/Views/MaterialOrderInquirySprints/Temporary/";
            string filePath = Path.Combine(Server.MapPath(string.Format("~{0}{1}", temp, zipName)));
            string repManifest = string.Format("{0}-1.pdf", ManifestNo);
            string repKanban = string.Format("{0}-2.pdf", ManifestNo);
            string repManifestPath = Path.Combine(Server.MapPath(string.Format("{0}{1}", folderTemp, repManifest)));
            string repKanbanPath = Path.Combine(Server.MapPath(string.Format("{0}{1}", folderTemp, repKanban)));

            string msg = "";


            byte[] doc = null;

            //FileInfo zipFile = new FileInfo(filePath);
            //if (zipFile.Exists)
            //    doc = StreamFile(filePath);
            if (doc == null)
            //else
            {
                MaterialSprints oMaterialSprints = new MaterialSprints { ORDER_NO = OrderNo };
                oMaterialSprints = GetAllMaterialOrderDetail(oMaterialSprints).First();

                string reportName = "Rep_Manifest_Sprints.trdx";
                string fileReport = Path.Combine(Server.MapPath(string.Format("~{0}{1}", folderReport, reportName)));
                string sp = "Sp_Rep_Manifest_Sprints";

                Telerik.Reporting.SqlDataSourceParameterCollection sqlParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
                sqlParameters.Add("@MANIFEST_NO", DbType.String, ManifestNo);

                Telerik.Reporting.ReportParameterCollection parameters = new Telerik.Reporting.ReportParameterCollection();
                parameters.Add("Manifest_No", Telerik.Reporting.ReportParameterType.String, ManifestNo);
                parameters.Add("Plant_Cd", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.SUPPLIER_PLANT_CD) ? string.Empty : oMaterialSprints.SUPPLIER_PLANT_CD);
                parameters.Add("Sloc_Cd", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.SLOC_CD) ? string.Empty : oMaterialSprints.SLOC_CD);
                parameters.Add("Dock_Cd", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.DOCK_CD) ? string.Empty : oMaterialSprints.DOCK_CD);
                parameters.Add("Plant_Order_Date", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.PLAN_ORDER_DATE.ToString()) ? string.Empty : oMaterialSprints.PLAN_ORDER_DATE.Value.ToString("MM/dd/yyyy"));
                parameters.Add("Plant_Order_Time", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.PLAN_ORDER_TIME.ToString()) ? string.Empty : (new DateTime() + oMaterialSprints.PLAN_ORDER_TIME).Value.ToString("hh:mm tt"));
                parameters.Add("Plant_Arrival_Date", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.PLAN_RECEIVING_DATE.ToString()) ? string.Empty : oMaterialSprints.PLAN_RECEIVING_DATE.Value.ToString("MM/dd/yyyy"));
                parameters.Add("Plant_Arrival_Time", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.PLAN_RECEIVING_TIME.ToString()) ? string.Empty : (new DateTime() + oMaterialSprints.PLAN_RECEIVING_TIME).Value.ToString("hh:mm tt"));
                parameters.Add("Supplier_Name", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.SUPPLIER_NAME) ? string.Empty : oMaterialSprints.SUPPLIER_NAME);
                parameters.Add("Supplier_Cd", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.SUPPLIER_CD) ? string.Empty : oMaterialSprints.SUPPLIER_CD);
                parameters.Add("Order_No", Telerik.Reporting.ReportParameterType.String, string.IsNullOrEmpty(oMaterialSprints.ORDER_NO) ? string.Empty : oMaterialSprints.ORDER_NO);
                parameters.Add("Total_Kanban", Telerik.Reporting.ReportParameterType.String, TtlKbn);
                Telerik.Reporting.Report rpt = CreateReport(fileReport, sp, sqlParameters, parameters);
                ReportProcessor reportProcess = new ReportProcessor();
                RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);

                reportName = "Rep_ManifestKanban_Sprints.trdx";
                fileReport = Path.Combine(Server.MapPath(string.Format("~{0}{1}", folderReport, reportName)));
                sp = "Sp_Rep_ManifestKanban_Sprints";

                sqlParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
                sqlParameters.Add("@MANIFEST_NO", DbType.String, ManifestNo);

                parameters = new Telerik.Reporting.ReportParameterCollection();
                parameters.Add("Manifest_No", Telerik.Reporting.ReportParameterType.String, ManifestNo);
                Telerik.Reporting.Report rpt1 = CreateReport(fileReport, sp, sqlParameters, parameters);
                reportProcess = new ReportProcessor();
                RenderingResult result1 = reportProcess.RenderReport("pdf", rpt1, null);

                rpt.Dispose();
                rpt1.Dispose();

                Stream file = new FileStream(filePath, FileMode.Create);

                using (ZipArchive zip = new ZipArchive(file))
                {
                    zip.Add(repManifest, DateTime.Now, result.DocumentBytes);
                    zip.Add(repKanban, DateTime.Now, result1.DocumentBytes);
                }

                file.Close();
                doc = StreamFile(filePath);
                vFtp.FtpUpload(folderTemp, zipName, doc, ref msg);
                DeleteFile(filePath);
            }

            return File(doc, "application/zip", zipName);

        }

        #endregion

        #region Common function

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                if (!Directory.Exists(_UploadDirectory))
                    Directory.CreateDirectory(_UploadDirectory);

                _FilePath = string.Format("{0}{1}_{2}.xls", _UploadDirectory, e.UploadedFile.FileName.Replace(".xls", ""), DateTime.Now.ToString("ddmmmyyyyhhmmss"));

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        protected void OnMOIUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                if (!Directory.Exists(_UploadDirectory))
                    Directory.CreateDirectory(_UploadDirectory);

                _FilePath = string.Format("{0}{1}_{2}.xls", _UploadDirectory, e.UploadedFile.FileName.Replace(".xls", ""), DateTime.Now.ToString("ddmmmyyyyhhmmss"));

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] ImageData = new byte[fs.Length];
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return ImageData; //return the byte data
        }

        private Telerik.Reporting.Report CreateReport(string reportPath,
            string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection sqlParameters = null,
            Telerik.Reporting.ReportParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = ConfigurationManager.ConnectionStrings["PCS"].ConnectionString;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource = new Telerik.Reporting.SqlDataSource();
            Telerik.Reporting.Table table;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            if (sqlDataSource == null)
            {
                table = (Telerik.Reporting.Table)rpt.Items[1].Items.Find("table1", true)[0];
                sqlDataSource = (Telerik.Reporting.SqlDataSource)table.DataSource;
            }

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;

            if (sqlParameters != null)
                sqlDataSource.Parameters.AddRange(sqlParameters);

            if (parameters != null)
                rpt.ReportParameters.AddRange(parameters);

            return rpt;
        }

        private void DeleteFile(string path)
        {
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
        }

        private void WriteFile(string path, byte[] documentBytes)
        {
            using (FileStream fs = new FileStream(path, FileMode.CreateNew))
            {
                fs.Write(documentBytes, 0, documentBytes.Length);
            }
        }

        #endregion
    }
}
