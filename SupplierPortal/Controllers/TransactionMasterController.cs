﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.TransactionMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class TransactionMasterController : BaseController
    {
        //
        // GET: /TransactionMaster/

        public TransactionMaster modelExport;
       IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

       public TransactionMasterController()
           : base("TransactionMaster")
           
       {

       }

        
        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            TransactionMasterModel mdl = new TransactionMasterModel();
            ViewData["TransactionGridLookup"] = GetAllTransaction();

            //mdl.TransactionMaster = GetTransactionMaster(string.Empty);
            Model.AddModel(mdl);
           
        }

        public ActionResult PartialHeader()
        {
            return PartialView("MasterIndexHeader");
        }

        public ActionResult PartialTransactionDatas()
        {
            string TransactionCD = String.IsNullOrEmpty(Request.Params["TransactionValues"]) ? "" : Request.Params["TransactionValues"];

            TransactionMasterModel mdl = Model.GetModel<TransactionMasterModel>();
            mdl.TransactionMaster = GetTransactionMaster(TransactionCD);
            Model.AddModel(mdl);

            return PartialView("MasterGridIndex", Model);
        }

        public ActionResult PartialHeaderTransactionGridLookupPart()
        {

            TempData["GridName"] = "OIHTransactionOption";
            ViewData["TransactionGridLookup"] = GetAllTransaction();

            return PartialView("GridLookup/PartialGrid", ViewData["TransactionGridLookup"]);
        }


        

        protected List<TransactionMaster> GetTransactionMaster(string transactionCode)
        {
            string transactionCodeParameter = string.Empty;
            if (!string.IsNullOrEmpty(transactionCode))
            {
                string[] transactionCodeList = transactionCode.Split(';');
                
                foreach (string t in transactionCodeList)
                {
                    transactionCodeParameter += "'" + t + "',";
                }
                transactionCodeParameter = transactionCodeParameter.Remove(transactionCodeParameter.Length - 1, 1);
            }
            
            List<TransactionMaster> listOfTransactionMaster = db.Fetch<TransactionMaster>("GetTransactionMaster", new object[] { transactionCodeParameter });
            return listOfTransactionMaster;
            
        }
        public List<Transaction> GetAllTransaction()
        {
            List<Transaction> listOfTransaction = new List<Transaction>();
            try
            {
                listOfTransaction = db.Fetch<Transaction>("GetAllTransaction", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listOfTransaction;
        }

        public ActionResult AddNewTransaction(string transactionCode, string transactionName, string sign, string description)
        {
            string ResultQuery = "Process succesfully";
            
            try
            {
                db.Execute("InsertTransactionMaster", new object[] 
                {
                   transactionCode,
                   transactionName,
                   sign,
                   description,
                   AuthorizedUser.Username,
                   DateTime.Now
                });
                //TempData["GridName"] = "PartGridView";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }

    }
}
