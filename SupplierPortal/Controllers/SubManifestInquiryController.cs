﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Portal.Models.DailyOrder;
using Portal.Models.Supplier;
using Portal.Models.Dock;
using Portal.Models.SubManifestInquiry;
using System.Data.SqlClient;
using System.Configuration;

namespace Portal.Controllers
{
    public class SubManifestInquiryController : BaseController
    {
        
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
            {
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            }
            else
            {
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public SubManifestInquiryController()
             : base("Sub Manifest Inquiry")
         {
             PageLayout.UseMessageBoard = true;
             CookieName = "fileDownload";
             CookiePath = "/";
         }

        #region Model properties
        List<SupplierName> _orderInquirySupplierModel = null;
        private List<SupplierName> _OrderInquirySupplierModel
        {
            get
            {
                if (_orderInquirySupplierModel == null)
                    _orderInquirySupplierModel = new List<SupplierName>();

                return _orderInquirySupplierModel;
            }
            set
            {
                _orderInquirySupplierModel = value;
            }
        }

        List<SupplierNameSPEX> _orderInquirySupplierSPEXModel = null;
        private List<SupplierNameSPEX> _OrderInquirySupplierSPEXModel
        {
            get
            {
                if (_orderInquirySupplierSPEXModel == null)
                    _orderInquirySupplierSPEXModel = new List<SupplierNameSPEX>();

                return _orderInquirySupplierSPEXModel;
            }
            set
            {
                _orderInquirySupplierSPEXModel = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }
        #endregion

        //Add by FID.Ridwan 22-01-2019
        #region Redirect Mode
        public static string MANIFEST_NO = "MANIFEST_NO";
        public static string SUB_MANIFEST_NO = "SUB_MANIFEST_NO";
        public static string PROCESS_ID = "PROCESS_ID";
        public static string REDIRECT_PAGE = "REDIRECT";

        public ActionResult Redirect(string manifestNo, string submanifestNo, string processId)
        {
            //ViewData[REDIRECT_PAGE] = REDIRECT_PAGE;
            //ViewData[MANIFEST_NO] = manifestNo;
            //ViewData[SUB_MANIFEST_NO] = submanifestNo;
            Session["MANIFEST_NO"] = manifestNo;
            Session["SUB_MANIFEST_NO"] = submanifestNo;
            Session["PROCESS_ID"] = processId;
            Session["REDIRECT_PAGE"] = REDIRECT_PAGE;
            Session.Timeout = 1;
            return RedirectToAction("Index");
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;

            SubManifestMapping mdl = new SubManifestMapping();
            Model.AddModel(mdl);

            ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");
        }

        #region Sub Manifest controller
        #region View controller
        public ActionResult SubManifestHeaderPartial()
        {
           // Session["paramManifestNo"] = String.IsNullOrEmpty(Request.Params["ManifestNos"]) ? Session["paramManifestNo"] : Request.Params["ManifestNos"];

            #region Required model in partial page : for OrderInquiryHeaderPartial
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestInquiry", "OIHSupplierOption");
            _OrderInquirySupplierModel = suppliers;

            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "SubManifestInquiry", "OIHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "SubManifestInquiry", "OIHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "SubManifestInquiry", "OIHDockOption");
            _OrderInquiryDockModel = DockCodes;

            Model.AddModel(_OrderInquirySupplierModel);
            Model.AddModel(_OrderInquirySupplierSPEXModel);
            Model.AddModel(_OrderInquiryDockModel);
            #endregion

            return PartialView("SubManifestHeaderPartial", Model);
        }


        public ActionResult SubManifestGridHeaderPartial()
        {
            SubManifestMapping model = Model.GetModel<SubManifestMapping>(); 
            try
            {
                #region Required model in partial page : for OrderInquiryEmergencyOrderDetailPartial
                model.SubManifestHeaderList = GetAllSubManifestExport(Request.Params["SUPPLIER"],
                                                Request.Params["SUB_SUPPLIER"],
                                                Request.Params["DOCK"],
                                                Request.Params["STATUS"],   
                                                Request.Params["MANIFEST_NO"],
                                                Request.Params["SUB_MANIFEST_NO"],
                                                Request.Params["ROUTE"],
                                                Request.Params["DATE_FROM"],
                                                Request.Params["DATE_TO"],
                                                Request.Params["ETD_FROM"],
                                                Request.Params["ETD_TO"],
                                                Request.Params["ETA_FROM"],
                                                Request.Params["ETA_TO"], 
                                                Request.Params["UPLOAD_NO"],
                                                Request.Params["REF_NO"],
                                                Request.Params["MODE"]);
                #endregion
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("SubManifestGridHeaderPartial", Model);
        }

        #region Grid Look UP
        public ActionResult PartialHeaderDockLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHDockOption GridLookup
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "SubManifestInquiry", "OIHDockOption");
            TempData["GridName"] = "OIHDockOption";
            _OrderInquiryDockModel = DockCodes;
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }

        public ActionResult PartialHeaderSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestInquiry", "OIHSupplierOption");

            TempData["GridName"] = "OIHSupplierOption";
            _OrderInquirySupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquirySupplierModel);
        }

        public ActionResult PartialHeaderSubSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "SubManifestInquiry", "OIHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "SubManifestInquiry", "OIHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();

            TempData["GridName"] = "OIHSubSupplierOption";
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;
            #endregion

            return PartialView("GridLookup/PartialGrid", _orderInquirySupplierSPEXModel);
        }

        #endregion

        public ActionResult PartialDetailSubManifest()
        {
            SubManifestMapping model = Model.GetModel<SubManifestMapping>(); 
            try
            {
                #region Required model in partial page : for OrderInquiryEmergencyOrderDetailPartial
            model.SubManifestHeaderList = GetAllSubManifestExport(Request.Params["SUPPLIER"],
                                                Request.Params["SUB_SUPPLIER"],
                                                Request.Params["DOCK"],
                                                Request.Params["STATUS"],   
                                                Request.Params["MANIFEST_NO"],
                                                Request.Params["SUB_MANIFEST_NO"],
                                                Request.Params["ROUTE"],
                                                Request.Params["DATE_FROM"],
                                                Request.Params["DATE_TO"],
                                                Request.Params["ETD_FROM"],
                                                Request.Params["ETD_TO"],
                                                Request.Params["ETA_FROM"],
                                                Request.Params["ETA_TO"], 
                                                Request.Params["UPLOAD_NO"],
                                                Request.Params["REF_NO"],
                                                Request.Params["MODE"]);
                #endregion
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("SubManifestGridHeaderPartial", Model);
        }

        public ActionResult PopupHeaderDailyDataPartSubDetail()
        {
            string manifest = Request.Params["ManifestNo"];
            return PartialView("PartialHeaderDailyDataSubDetail", ViewData["ManifestNo"]);
        }

        public ActionResult PopupDailyDataPartSubDetail()
        {
            string manifestNo = Request.Params["ManifestNo"];
            ViewData["partDailyData"] = DailyDataPartOrderFillGrid(manifestNo);
            return PartialView("DailyDataSubDetail", ViewData["partDailyData"]);
        }
        #endregion

        //#region Common controller
        //private String GetGeneralProductionMonth(String productionMonth)
        //{
        //    Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
        //    monthDictionary.Add("01", "January");
        //    monthDictionary.Add("02", "February");
        //    monthDictionary.Add("03", "March");
        //    monthDictionary.Add("04", "April");
        //    monthDictionary.Add("05", "May");
        //    monthDictionary.Add("06", "June");
        //    monthDictionary.Add("07", "July");
        //    monthDictionary.Add("08", "August");
        //    monthDictionary.Add("09", "September");
        //    monthDictionary.Add("10", "October");
        //    monthDictionary.Add("11", "November");
        //    monthDictionary.Add("12", "December");

        //    String generalProductionMonth = "";

        //    if (!String.IsNullOrEmpty(productionMonth))
        //    {
        //        String year = productionMonth.Substring(0, 4);
        //        String month = productionMonth.Substring(productionMonth.Length - 2, 2);

        //        generalProductionMonth = monthDictionary[month] + " " + year;
        //    }

        //    return generalProductionMonth;
        //}
        //#endregion

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> orderInquirySupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierName>("GetAllSupplierSPEXNEW", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<SupplierNameSPEX> GetAllSubSupplier()
        {
            List<SupplierNameSPEX> orderInquirySupplierModel = new List<SupplierNameSPEX>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierNameSPEX>("GetAllSubSupplierSPEXNEW", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            IDBContext db = DbContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return orderInquiryDockModel;
        }


        public List<SubManifest> GetAllSubManifestExport(string SUPPLIER, string SUB_SUPPLIER, string DOCK, string STATUS,
                                                         string MANIFEST_NO, string SUB_MANIFEST_NO, string ROUTE,  string DATE_FROM, 
                                                         string DATE_TO, string ETD_FROM, string ETD_TO, string ETA_FROM, string ETA_TO,
                                                         string UPLOAD_NO, string REF_NO, string mode)
        {
            //added by FID.Ridwan 
            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestInquiry", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";
            //end

            if (STATUS == "Not Completed")
            {
                STATUS = "0";
            }
            if (STATUS == "Completed")
            {
                STATUS = "1";
            }

            IDBContext db = DbContext;
            List<SubManifest> subManifestModel = new List<SubManifest>();
            if (mode == "search")
            {
                subManifestModel = db.Fetch<SubManifest>("GetSubManifestInquiryHeader", new object[] {
                SUPPLIER == null ? "" : SUPPLIER, 
                SUB_SUPPLIER == null ? "" : SUB_SUPPLIER,
                DOCK == null ? "" : DOCK,
                STATUS == null ? "" : STATUS,
                MANIFEST_NO,
                SUB_MANIFEST_NO,
                ROUTE == null ? "" : ROUTE, 
                (DATE_FROM == "" || DATE_FROM == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(DATE_FROM,"dd.MM.yyyy",null),
                (DATE_TO == "" || DATE_TO == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(DATE_TO,"dd.MM.yyyy",null),
                (ETD_FROM == "" || ETD_FROM == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETD_FROM,"dd.MM.yyyy",null),
                (ETD_TO == "" || ETD_TO == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETD_TO,"dd.MM.yyyy",null),
                (ETA_FROM == "" || ETA_FROM == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETA_FROM,"dd.MM.yyyy",null),
                (ETA_TO == "" || ETA_TO == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETA_TO,"dd.MM.yyyy",null),
                UPLOAD_NO,
                REF_NO,
                SupplierCodeLDAP
            });
            }

            
            db.Close();

            return subManifestModel;
        }

        protected List<SubManifestDetail> DailyDataPartOrderFillGrid(string manifestNo)
        {
            List<SubManifestDetail> getPartDetailManifest = new List<SubManifestDetail>();
            IDBContext db = DbContext;
            getPartDetailManifest = db.Query<SubManifestDetail>("GetSubManifestInquiryDetail", new object[] { manifestNo }).ToList<SubManifestDetail>();

            db.Close();
            return getPartDetailManifest;
        }

        private void UpdateDownloadFlagNonRegular(string ManifestNo)
        {
            IDBContext db = DbContext;

            db.Execute("UpdateDownloadFlagOrderNonRegularNEW", new Object[] { ManifestNo, AuthorizedUser.Username });
            db.Close();
        }
        #endregion
        #endregion

        #region Download excell
        public void DownloadList(string SUPPLIER, string SUB_SUPPLIER, string DOCK, string STATUS, string MANIFEST_NO, 
                                 string SUB_MANIFEST_NO, string ROUTE, string DATE_FROM, string DATE_TO, string ETD_FROM, 
                                 string ETD_TO, string ETA_FROM, string ETA_TO, string UPLOAD_NO, string REF_NO)
        {
            string fileName = "[IPPCS] SPEX Sub Manifest Download Report.xls";

            IDBContext db = DbContext;
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            //added by FID.Ridwan 
            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestInquiry", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";
            //end

            if (STATUS == "Not Completed")
            {
                STATUS = "0";
            }
            if (STATUS == "Completed")
            {
                STATUS = "1";
            }


            IEnumerable<SubManifestReport> subManifestModel = db.Fetch<SubManifestReport>("GetSubManifestInquiryHeaderDownload", new object[] {
                SUPPLIER == null ? "" : SUPPLIER, 
                SUB_SUPPLIER == null ? "" : SUB_SUPPLIER,
                DOCK == null ? "" : DOCK,
                STATUS == null ? "" : STATUS,
                MANIFEST_NO,
                SUB_MANIFEST_NO,
                ROUTE == null ? "" : ROUTE, 
                (DATE_FROM == "" || DATE_FROM == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(DATE_FROM,"dd.MM.yyyy",null),
                (DATE_TO == "" || DATE_TO == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(DATE_TO,"dd.MM.yyyy",null),
                (ETD_FROM == "" || ETD_FROM == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETD_FROM,"dd.MM.yyyy",null),
                (ETD_TO == "" || ETD_TO == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETD_TO,"dd.MM.yyyy",null),
                (ETA_FROM == "" || ETA_FROM == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETA_FROM,"dd.MM.yyyy",null),
                (ETA_TO == "" || ETA_TO == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(ETA_TO,"dd.MM.yyyy",null),
                UPLOAD_NO,
                REF_NO,
                SupplierCodeLDAP
            });
            hasil = exporter.Write(subManifestModel, "SubManifest");

            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();


        }


        #endregion


        #region DownloadPDF
        public FileContentResult DownloadPDF(string ManifestNo, string OrderType)
        {
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string filePath = "";

            try
            {
                FTPUpload vFtp = new FTPUpload();
                filePath = vFtp.Setting.FtpPath("SubManifest");
                string msg = "";

                string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, filePath);
                documentBytes = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, ManifestNo + ".zip"),
                                                       ref msg);

                UpdateDownloadFlagNonRegular(ManifestNo);

                return File(documentBytes, "application/zip", ManifestNo + ".zip");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Cancel
        public ActionResult CancelData(string iListKeyCancel)
        {
            var userLogin = AuthorizedUser.Username;
            var SUB_MANIFEST_NO = "";
            var CANCEL_BY = "";
            var CANCEL_DT = "";
            //string[] r = null;

            var listKeyHeader = iListKeyCancel.Split(';');
            foreach (var lsKeyHeader in listKeyHeader)
            {
                var listKeyDetail = lsKeyHeader.Split('|');
                SUB_MANIFEST_NO += listKeyDetail[0].ToString() + ";";
                CANCEL_BY += listKeyDetail[2].ToString() + ";";
                CANCEL_DT += listKeyDetail[3].ToString() + ";";
            }
            string resultMessage = CancelProcess(userLogin, SUB_MANIFEST_NO, CANCEL_BY, CANCEL_DT);
            var r = resultMessage.Split('|');//.Split('|');
            if (r[0] != "I")
            {
                return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
            } 
            return null;
        }
        public String ReGeneratePDF(string iListKeyCancel)
        {
            var UserLogin = AuthorizedUser.Username;
            var ManifestNo = "";
            var SubManifestNo = "";

            var listKeyHeader = iListKeyCancel.Split(';');
            foreach (var lsKeyHeader in listKeyHeader)
            {
                var listKeyDetail = lsKeyHeader.Split('|');
                SubManifestNo += listKeyDetail[0].ToString() + ";";
                ManifestNo += listKeyDetail[1].ToString() + ";";
            }
            IDBContext db = DbContext;
            string result = "";

            try
            {
                result = db.ExecuteScalar<String>("ReGeneratePDFSubManifestInquiry",
                        new object[] { ManifestNo, SubManifestNo, UserLogin });
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        public string CancelProcess(string userLogin, string SUB_MANIFEST_NO, string CANCEL_BY, string CANCEL_DT)
        {


            string username = AuthorizedUser.Username;

            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            string companyID = SupplierCodeLDAP; //AuthorizedUser.CompanyID;
            string lockingBy = companyID;

            if (lockingBy == null || lockingBy == "")
                lockingBy = username;

            string pid = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[DBContextNames.DB_PCS].ToString());

            

            SqlCommand cmd = new SqlCommand("[dbo].[SP_SPEX_SUB_MANIFEST_INQUIRY_CANCEL_PROCESS]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter outputIdParam = new SqlParameter("@TEXT", SqlDbType.VarChar,1000)
            {
                Direction = ParameterDirection.Output
            };
            cmd.Parameters.AddWithValue("@MODE", "cancel");
            cmd.Parameters.AddWithValue("@ManifestNo", SUB_MANIFEST_NO);
            cmd.Parameters.AddWithValue("@UserLogin", userLogin);
            cmd.Parameters.AddWithValue("@CANCEL_BY", CANCEL_BY);
            cmd.Parameters.AddWithValue("@CANCEL_DT", CANCEL_DT);
            cmd.Parameters.AddWithValue("@SUPPLIER_CD", lockingBy);
            cmd.Parameters.Add(outputIdParam);

            conn.Open();
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
            conn.Close();

            pid = outputIdParam.Value.ToString();

            return pid;

            //IDBContext db = DbContext;
            
            //var l = db.Fetch<string>("SubManifestInquiryCancelProcess", new object[] {
            //    "cancel", 
            //    SUB_MANIFEST_NO,
            //    userLogin,
            //    CANCEL_BY,
            //    CANCEL_DT 
            //});
             
            //db.Close();
            //return l;

        }


        #endregion

        #region Validate Supplier
        public ActionResult ValidateSubManifest(string SubManifest)
        {
            IDBContext db = DbContext;
            string result = db.SingleOrDefault<string>("ValidateSubManifest", new object[] { SubManifest , null });

            return Content(result);
        }
        #endregion
    }
    
    
}
