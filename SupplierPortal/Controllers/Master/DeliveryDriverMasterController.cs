﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;

using Portal.Models.Master;
 
namespace Portal.Controllers.Master
{
    public class DeliveryDriverMasterController : BaseController
    {
        public DeliveryDriverMasterController()
            : base("DeliveryDriverMaster", "Trucking Driver Master")
        {
        }

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            ViewData["DeliveryDriverData"] = GetDriver();
            ViewData["GridLP"] = GetAllLP();
        }

        public List<DeliveryDriver> GetDriver()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<DeliveryDriver> listDriver = new List<DeliveryDriver>();
            var QueryLog = db.Query<DeliveryDriver>("DLVDriver_GetDriverData", new object[] { "", "" }).ToList<DeliveryDriver>();
            listDriver = QueryLog.ToList<DeliveryDriver>();
            db.Close();
            return listDriver;
        }

        public List<GLTrucking> GetAllLP()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<GLTrucking> listLP = new List<GLTrucking>();
            var QueryLog = db.Query<GLTrucking>("DLVVehicle_GetLP");
            listLP = QueryLog.ToList<GLTrucking>();
            db.Close();
            return listLP;
        }

        public ActionResult CallBackLP()
        {
            TempData["GridName"] = "grLPHeader";
            ViewData["GridLP"] = GetAllLP();
            return PartialView("GridLookup/PartialGrid", ViewData["GridLP"]);
        }

        public ActionResult CallBackLPGrid()
        {
            TempData["GridName"] = "grLPGridLookup";
            ViewData["GridLP"] = GetAllLP();
            return PartialView("GridLookup/PartialGrid", ViewData["GridLP"]);
        }

        public ActionResult DeliveryDriverGridLoad(string LP_Cd, string Driver_Name)
        {
            LP_Cd = ((LP_Cd == null) ? "" : LP_Cd);
            Driver_Name = ((Driver_Name == null) ? "" : Driver_Name);

            string[] ListLP;

            ListLP = LP_Cd.Split(';');
            if (ListLP.Length > 1)
            {
                LP_Cd = "";
                for (int i = 0; i < ListLP.Length; i++)
                {
                    if (LP_Cd != "")
                    {
                        LP_Cd = LP_Cd + ", '" + ListLP[i].ToString() + "'";
                    }
                    else
                    {
                        LP_Cd = "'" + ListLP[i].ToString() + "'";
                    }
                }
            }
            else
            {
                LP_Cd = "'" + LP_Cd + "'";
            }

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryDriverModel model = new DeliveryDriverModel();

            model.DeliveryDriver = db.Query<DeliveryDriver>("DLVDriver_GetDriverData", new object[] { LP_Cd.ToUpper(), Driver_Name, "gridView" }).ToList<DeliveryDriver>();
            Model.AddModel(model);

            db.Close();

            return PartialView("DeliveryDriverGrid", Model);
        }

        public ContentResult SaveDeliveryDriverMaster(string Saving_Status, string LP_Cd, string Driver_Name, string Driver_ID)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string contentMessage = "";

            if (Saving_Status == "Add")
            {
                try
                {
                    var QueryLog = db.SingleOrDefault<CountTruck>("DLVVehicle_CheckLPValid", new object[] { LP_Cd.ToUpper(),"D","" });

                    if (QueryLog.CountLP == 0)
                    {
                        isError = 1;
                        contentMessage = "Trucking Code not valid. Please use valid Trucking Code.";
                    }
                    else
                    {
                        db.Execute("DLVDriver_SaveDriverData", new object[] { 
                                LP_Cd.ToUpper(),Driver_Name,AuthorizedUser.Username,Saving_Status,Driver_ID
                            });
                        contentMessage = "Driver data has been added";
                    }
                }
                catch
                {
                    isError = 1;
                    contentMessage = "Error when add Driver data";
                }
                return Content(((isError == 1) ? "Error|" : "Sucess|") + contentMessage);

            }
            else
            {
                try
                {
                    db.Execute("DLVDriver_SaveDriverData", new object[] { 
                                LP_Cd.ToUpper(),Driver_Name,AuthorizedUser.Username,Saving_Status,Driver_ID});

                    contentMessage = "Driver data has been updated";
                }
                catch
                {
                    isError = 1;
                    contentMessage = "Error when update Driver data";
                }
                return Content(((isError == 1) ? "Error|" : "Sucess|") + contentMessage);
            }
        }

        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }

        public ActionResult ConfirmBox()
        {
            return PartialView("ConfirmBox");
        }

        public ContentResult DeleteDriverMaster(string Key)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string contentMessage = "";

            string[] ListKey;

            if (Key != "")
            {
                ListKey = Key.Split(',');
                if (ListKey.Length > 1)
                {
                    Key = "";
                    for (int i = 0; i < ListKey.Length; i++)
                    {
                        if (Key != "")
                        {
                            Key = Key + ", '" + ListKey[i].ToString().Trim() + "'";
                        }
                        else
                        {
                            Key = "'" + ListKey[i].ToString().Trim() + "'";
                        }
                    }
                }
                else
                {
                    Key = "'" + Key + "'";
                }
            }

            try
            {
                db.Execute("DLVDriver_DeleteDriverData", new object[] { Key });
                contentMessage = "Driver data deleted";
            }
            catch (Exception exc)
            {
                isError = 1;
                contentMessage = "Error when delete driver data.";
            }

            return Content(((isError == 1) ? "Error|" : "Success|") + contentMessage);
        }
    }
}
