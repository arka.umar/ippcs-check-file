﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Master;
using Portal.Models.Delivery.Master;

namespace Portal.Controllers.Delivery.Master
{
    public class D31007Controller : BaseController
    {
        public D31007Controller()
            : base("Master Component Price Rate")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            CompPriceRateModel mdl = new CompPriceRateModel();

            ViewData["CompPriceLookup"] = GetListCompPrice();
            ViewData["TaxLookup"] = GetListTax();
            Model.AddModel(mdl);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("PartialHeader");
        }

        #region Callback Function

        public ActionResult grlCompPriceCallback()
        {

            TempData["GridName"] = "grlCompPrice";
            ViewData["CompPriceLookup"] = GetListCompPrice();

            return PartialView("PG", ViewData["CompPriceLookup"]);
        }

        public ActionResult grlTaxCallback()
        {

            TempData["GridName"] = "grlTax";
            ViewData["TaxLookup"] = GetListTax();

            return PartialView("PG", ViewData["TaxLookup"]);
        }

        public ActionResult GridCompPriceRateCallback(string CompPriceCd = "", string ConditionRule = "", string TaxCd = "")
        {
            CompPriceRateModel mdl = Model.GetModel<CompPriceRateModel>();
            mdl.MasterCompPricePub = GetGridContent(CompPriceCd, ConditionRule, TaxCd);
            Model.AddModel(mdl);

            return PartialView("CompPriceMasterGrid", Model);
        }
        
        public ActionResult grlCompPriceOnGridCallback()
        {

            TempData["GridName"] = "grlCompPriceOnGrid";
            ViewData["CompPriceLookupOnGrid"] = GetListCompPrice();

            return PartialView("PG", ViewData["CompPriceLookupOnGrid"]);
        }

        public ActionResult grlTaxOnGridCallback()
        {

            TempData["GridName"] = "grlTaxOnGrid";
            ViewData["TaxLookupOnGrid"] = GetListTax();

            return PartialView("PG", ViewData["TaxLookupOnGrid"]);
        }

        public ContentResult SaveNewRate(string CompPrice, string ConditionRule, string Price, string Tax, string SavingType)
        {
            string messageResult = "Data has been saved.";
            string messageType = "Success";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                if (Tax == null)
                    Tax = "";
                db.Execute("DLV_SaveCompPriceRate", new object[] { CompPrice, ConditionRule, Price, Tax, AuthorizedUser.Username, SavingType });
            }
            catch (Exception exc)
            {
                messageType = "Error";
                messageResult = "Error when saving data.";
            }
            db.Close();

            return Content(messageType + "|" + messageResult);
        }

        public ContentResult DeleteRate(string GridParam) 
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string messageResult = "Data has been deleted.";
            string messageType = "Success";
            string splitStringGridParam = "";

            try
            {
                if (!string.IsNullOrEmpty(GridParam))
                {
                    string[] splitList = GridParam.Split(';');

                    foreach (string r in splitList)
                    {
                        splitStringGridParam += "'" + r + "',";
                    }
                    splitStringGridParam = splitStringGridParam.Remove(splitStringGridParam.Length - 1, 1);
                }

                db.Execute("DLV_DeleteCompPriceRate", new object[] { splitStringGridParam });
            }
            catch (Exception e)
            {
                messageType = "Error";
                messageResult = "Error when deleting data.";
            }

            return Content(messageType + "|" + messageResult);
        }

        #endregion

        #region Get Data

        public List<MasterCompPrice> GetListCompPrice()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<MasterCompPrice> listCompPrice = new List<MasterCompPrice>();
            try
            {
                listCompPrice = db.Fetch<MasterCompPrice>("DLV_getCompPrice");
            }
            catch (Exception ex) { throw ex; }
            db.Close();

            return listCompPrice;
        }

        public List<MasterTaxComboList> GetListTax()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<MasterTaxComboList> listTax = new List<MasterTaxComboList>();
            try
            {
                listTax = db.Fetch<MasterTaxComboList>("DLV_getTax");
            }
            catch (Exception ex) { throw ex; }
            db.Close();

            return listTax;
        }

        public List<MasterCompPriceRate> GetGridContent(string CompPriceCd, string ConditionRule, string TaxCd)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<MasterCompPriceRate> ListGrid = new List<MasterCompPriceRate>();
            string splitStringComp = "";
            string splitStringTax = "";

            try
            {
                if (!string.IsNullOrEmpty(CompPriceCd))
                {
                    string[] splitList = CompPriceCd.Split(';');

                    foreach (string r in splitList)
                    {
                        splitStringComp += "'" + r + "',";
                    }
                    splitStringComp = splitStringComp.Remove(splitStringComp.Length - 1, 1);
                }

                if (!string.IsNullOrEmpty(TaxCd))
                {
                    string[] splitList = TaxCd.Split(';');

                    foreach (string r in splitList)
                    {
                        splitStringTax += "'" + r + "',";
                    }
                    splitStringTax = splitStringTax.Remove(splitStringTax.Length - 1, 1);
                }

                ListGrid = db.Fetch<MasterCompPriceRate>("DLV_getCompPriceRateContent", new object[] { splitStringComp, ConditionRule, splitStringTax });
            }
            catch (Exception ex) { }
            db.Close();

            return ListGrid;
        }

        #endregion

    }
}
