﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Master;
using Portal.Models.Delivery.Master;

namespace Portal.Controllers.Delivery.Master
{
    public class D61002Controller : BaseController
    {
        public DeliveryDraftRoutePriceData modelExport;

        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public D61002Controller()
            : base("Master Draft Route Price")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryDraftRoutePriceModel mdl = new DeliveryDraftRoutePriceModel();

            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();
            ViewData["ChildRouteLookup"] = GetChildRoute();
            ViewData["ParentRouteLookup"] = GetParentRoute();
            ViewData["EditChildRouteLookup"] = GetChildRoute();
            ViewData["EditParentRouteLookup"] = GetParentRoute();
            Model.AddModel(mdl);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("IndexHeader");
        }

        public ActionResult PartialListData()
        {

            string CHILD_ROUTE = String.IsNullOrEmpty(Request.Params["ChildRoute"]) ? "" : Request.Params["ChildRoute"];
            string PARENT_ROUTE = String.IsNullOrEmpty(Request.Params["ParentRoute"]) ? "" : Request.Params["ParentRoute"];
            string VALID_FROM = String.IsNullOrEmpty(Request.Params["VALID_FROM"]) ? "" : Request.Params["VALID_FROM"];
            string VALID_TO = String.IsNullOrEmpty(Request.Params["VALID_TO"]) ? "" : Request.Params["VALID_TO"];

            DeliveryDraftRoutePriceModel mdl = Model.GetModel<DeliveryDraftRoutePriceModel>();
            mdl.DeliveryDraftRoutePriceListData = GetDeliveryDraftRoutePrice(CHILD_ROUTE, PARENT_ROUTE, VALID_FROM, VALID_TO);
            db.Close();
            Model.AddModel(mdl);
            
            return PartialView("GridIndex", Model);
        }

        public List<DeliveryDraftRoutePriceData> GetDeliveryDraftRoutePrice(string CHILD_ROUTE, string PARENT_ROUTE, string VALID_FROM, string VALID_TO)
        {
            string ChildRouteParameter = string.Empty;

            if (!string.IsNullOrEmpty(CHILD_ROUTE))
            {
                string[] ChildRouteList = CHILD_ROUTE.Split(';');

                foreach (string r in ChildRouteList)
                {
                    ChildRouteParameter += "'" + r + "',";
                }
                ChildRouteParameter = ChildRouteParameter.Remove(ChildRouteParameter.Length - 1, 1);
            }

            string ParentRouteParameter = string.Empty;

            if (!string.IsNullOrEmpty(PARENT_ROUTE))
            {
                string[] ParentRouteList = PARENT_ROUTE.Split(';');

                foreach (string r in ParentRouteList)
                {
                    ParentRouteParameter += "'" + r + "',";
                }
                ParentRouteParameter = ParentRouteParameter.Remove(ParentRouteParameter.Length - 1, 1);
            }

            string validFrom = string.Empty;
            if (!string.IsNullOrEmpty(VALID_FROM))
            {
                string d = VALID_FROM.Substring(0, 2);
                string m = VALID_FROM.Substring(3, 2);
                string y = VALID_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                validFrom += "'" + tgl + "'";
            }

            string validTo = string.Empty;
            if (!string.IsNullOrEmpty(VALID_TO))
            {
                string d = VALID_TO.Substring(0, 2);
                string m = VALID_TO.Substring(3, 2);
                string y = VALID_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                validTo += "'" + tgl + "'";
                //createDateToParameter +=tgl;
            }

            List<DeliveryDraftRoutePriceData> listOfDLV_DraftRoutePrice = db.Fetch<DeliveryDraftRoutePriceData>("DLV_getGridDraftRoutePrice", new object[] { 
                ChildRouteParameter, ParentRouteParameter, validFrom, validTo
            });
            return listOfDLV_DraftRoutePrice;
        }

        public PartialViewResult UpdatePartial(LogisticPartner editable)
        {
            ViewData["LogisticPartnerGridData"] = editable;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogisticPartnerModel model = Model.GetModel<LogisticPartnerModel>();
            model.LogisticPartner = db.Query<LogisticPartner>("GetLogisticPartnerByFilter", new object[] { "", "", "G" }).ToList<LogisticPartner>();
            db.Close();
            return PartialView("GridIndex", Model);
        }

        public ContentResult AddingDraftRoutePrice(string Saving_Status, string CHILD_ROUTE, string PARENT_ROUTE, string LP_CD, string VALID_FROM)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isExistsRoute = 0;
            int isExistsLP = 0;
            int isError = 0;
            string errorMessage = "";

            string validfrom = string.Empty;
            if (!string.IsNullOrEmpty(VALID_FROM))
            {
                string d = VALID_FROM.Substring(0, 2);
                string m = VALID_FROM.Substring(3, 2);
                string y = VALID_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                //validfrom += "'" + tgl + "'";
                validfrom += tgl;
            }

            if (Saving_Status == "New")
            {

                //try
                //{
                //    db.Execute("DLV_SetDraftRoutePrice", new object[] { 
                //            CHILD_ROUTE, PARENT_ROUTE, LP_CD, validfrom, AuthorizedUser.Username
                //        });
                //    errorMessage = "Master data saved.";
                //}
                //catch (Exception e)
                //{
                //    isError = 1;
                //    errorMessage = "Error when insert draft route price data";
                //}

                try
                {

                    //---=== Check if exists on TB_M_DELIVERY_ROUTE GetValidationRoutePriceStat
                    CheckResult QueryLog = db.SingleOrDefault<CheckResult>("DLV_GetRoutePriceMasterDataCheck", new object[] { 
                    "R", PARENT_ROUTE
                    });
                    isExistsRoute = QueryLog.result;
                    //---=== Check if exists on TB_M_LOGISTIC_PARTNER
                    QueryLog = db.SingleOrDefault<CheckResult>("DLV_GetRoutePriceMasterDataCheck", new object[] { 
                    "L", LP_CD
                    });
                    isExistsLP = QueryLog.result;


                    //if ((isExistsRoute == 1) && (isExistsLP == 1))
                    if ((isExistsRoute >= 1) && (isExistsLP >= 1))
                    {
                        //db.Execute("DLV_SetMasterDraftRoutePrice", new object[] { 
                        //    CHILD_ROUTE, PARENT_ROUTE, LP_CD, validfrom, AuthorizedUser.Username
                        //});

                        db.Execute("DLV_SetDraftRoutePrice", new object[] { 
                            CHILD_ROUTE, PARENT_ROUTE, LP_CD, validfrom, AuthorizedUser.Username
                        });

                        errorMessage = "Master data saved.";
                    }
                    else
                    {
                        isError = 1;
                        errorMessage = ((isExistsRoute == 0) ? "Route" : "");
                        errorMessage = ((isExistsLP == 0) ? ((errorMessage != "") ? errorMessage + " and LP" : "LP") : errorMessage);
                        errorMessage = "Error : " + errorMessage + " not exists on master table.";
                    }

                }
                catch (Exception exc)
                {
                    isError = 1;
                    errorMessage = exc.Message;
                }
            }
            else
            {
                try
                {
                    db.Execute("DLV_UpdateMasterDraftRoutePrice", new object[] { 
                            CHILD_ROUTE, PARENT_ROUTE, LP_CD, validfrom, AuthorizedUser.Username
                        });
                    errorMessage = "Draft route price data has been updated";
                }
                catch (Exception exc)
                {
                    isError = 1;
                    errorMessage = "Error when update draft route price data";
                }
            }

               

            return Content(((isError==1)?"Error|":"Sucess|")+errorMessage);
        }

        public ContentResult DeleteDraftRoutePrice(string GRID)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string errorMessage = "";

            try
            {
                db.Execute("DLV_DeleteMasterDraftRoutePrice", new object[] { 
                            GRID, AuthorizedUser.Username
                        });
                errorMessage = "Master data deleted.";
            }
            catch (Exception e)
            {
                isError = 1;
                errorMessage = e.Message;
            }

            return Content(((isError == 1) ? "Error|" : "Sucess|") + errorMessage);

        }

        public ContentResult ApproveDraftRoutePrice(string GRID)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string errorMessage = "";

            try
            {
                db.Execute("DLV_ApproveDraftRoutePrice", new object[] { 
                            GRID, AuthorizedUser.Username
                        });
                errorMessage = "Master data has been approved.";
            }
            catch (Exception e)
            {
                isError = 1;
                errorMessage = e.Message;
            }

            return Content(((isError == 1) ? "Error|" : "Sucess|") + errorMessage);

        }

        //LOOK UP
        #region LOOK UP
        //GETTING Route FOR LOOKUP

        public ActionResult ChildRouteLookup()
        {

            TempData["GridName"] = "ChildRoute";
            ViewData["ChildRouteLookup"] = GetChildRoute();

            return PartialView("PG", ViewData["ChildRouteLookup"]);
        }

        public ActionResult ParentRouteLookup()
        {

            TempData["GridName"] = "ParentRoute";
            ViewData["ParentRouteLookup"] = GetParentRoute();

            return PartialView("PG", ViewData["ParentRouteLookup"]);
        }

        public ActionResult EditChildRouteLookup()
        {

            TempData["GridName"] = "EditChildRoute";
            ViewData["EditChildRouteLookup"] = GetChildRoute();

            return PartialView("PG", ViewData["EditChildRouteLookup"]);
        }

        public ActionResult EditParentRouteLookup()
        {

            TempData["GridName"] = "EditParentRoute";
            ViewData["EditParentRouteLookup"] = GetParentRoute();

            return PartialView("PG", ViewData["EditParentRouteLookup"]);
        }

        public List<DeliveryDraftRoutePriceDataRoute> GetChildRoute()
        {
            List<DeliveryDraftRoutePriceDataRoute> listRoute = new List<DeliveryDraftRoutePriceDataRoute>();
            try
            {
                listRoute = db.Fetch<DeliveryDraftRoutePriceDataRoute>("DLV_getChildRoute", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listRoute;
        }

        public List<DeliveryDraftRoutePriceDataRoute> GetParentRoute()
        {
            List<DeliveryDraftRoutePriceDataRoute> listRoute = new List<DeliveryDraftRoutePriceDataRoute>();
            try
            {
                listRoute = db.Fetch<DeliveryDraftRoutePriceDataRoute>("DLV_getParentRoute", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listRoute;
        }


        public ActionResult LogisticPartnerLookup()
        {

            TempData["GridName"] = "EditLP";
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            return PartialView("PG", ViewData["LogisticPartnerLookup"]);
        }

        public List<DeliveryDraftRoutePriceDataTrucking> GetLogisticPartner()
        {
            List<DeliveryDraftRoutePriceDataTrucking> listLogisticPartner = new List<DeliveryDraftRoutePriceDataTrucking>();
            try
            {
                listLogisticPartner = db.Fetch<DeliveryDraftRoutePriceDataTrucking>("DLV_getLP", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listLogisticPartner;
        }

        #endregion

    }
}
