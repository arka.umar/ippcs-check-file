﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;

using Portal.Models.Master;
 
namespace Portal.Controllers.Master
{
    public class DeliveryVehicleMasterController : BaseController
    {
        public DeliveryVehicleMasterController()
            : base("DeliveryVehicleMaster", "Trucking Vehicle Master")
        { 
        }

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            ViewData["DeliveryVehicleData"] = GetVehicle();
            ViewData["GridLP"] = GetAllLP();
        }

        public List<DeliveryVehicle> GetVehicle()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<DeliveryVehicle> listVehicle = new List<DeliveryVehicle>();
            var QueryLog = db.Query<DeliveryVehicle>("DLVVehicle_GetVehicleData", new object[] {"",""}).ToList<DeliveryVehicle>();
            listVehicle = QueryLog.ToList<DeliveryVehicle>();
            db.Close();
            return listVehicle;
        }

        public List<GridLP> GetAllLP()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<GridLP> listLP = new List<GridLP>();
            var QueryLog = db.Query<GridLP>("DLVVehicle_GetLP");
            listLP = QueryLog.ToList<GridLP>();
            db.Close();
            return listLP;
        }

        public ActionResult CallBackLP()
        {
            TempData["GridName"] = "grLPHeader";
            ViewData["GridLP"] = GetAllLP();
            return PartialView("GridLookup/PartialGrid", ViewData["GridLP"]);
        }

        public ActionResult CallBackLPGrid()
        {
            TempData["GridName"] = "grLPGridLookup";
            ViewData["GridLP"] = GetAllLP();
            return PartialView("GridLookup/PartialGrid", ViewData["GridLP"]);
        }

        public ActionResult DeliveryVehicleGridLoad(string LP_Cd, string Vehicle_No)
        {
            LP_Cd = ((LP_Cd == null) ? "" : LP_Cd);
            Vehicle_No = ((Vehicle_No == null) ? "" : Vehicle_No);

            string[] ListLP;

            ListLP = LP_Cd.Split(';');
            if (ListLP.Length > 1)
            {
                LP_Cd = "";
                for (int i = 0; i < ListLP.Length; i++)
                {
                    if (LP_Cd != "")
                    {
                        LP_Cd = LP_Cd + ", '" + ListLP[i].ToString() + "'";
                    }
                    else
                    {
                        LP_Cd = "'" + ListLP[i].ToString() + "'";
                    }
                }
            }
            else
            {
                LP_Cd = "'" + LP_Cd + "'";
            }

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryVehicleModel model = new DeliveryVehicleModel();

            model.DeliveryVehicle = db.Query<DeliveryVehicle>("DLVVehicle_GetVehicleData", new object[] { LP_Cd.ToUpper(), Vehicle_No, "gridView" }).ToList<DeliveryVehicle>();
            Model.AddModel(model);

            db.Close();

            return PartialView("DeliveryVehicleGrid", Model);

        }

        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }

        public ActionResult ConfirmBox()
        {
            return PartialView("ConfirmBox");
        }

        public ContentResult SaveDeliveryVehicleMaster(string Saving_Status, string LP_Cd, string Vehicle_No, string Vehicle_ID)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string contentMessage = "";

            string check = " ";
            bool b;

            if (Vehicle_No != null)
            {
                b = Vehicle_No.Contains(check);

                if (b)
                {
                    isError = 1;
                    contentMessage = "Please do not use space for Vehicle Number";
                    return Content(((isError == 1) ? "Error|" : "Sucess|") + contentMessage);
                }
            }

            if (Saving_Status == "Add")
            {
                try
                {
                        var QueryCheck = db.SingleOrDefault<CheckCount>("DLVVehicle_CheckLPValid", new object[] { LP_Cd.ToUpper(), "C", Vehicle_No });
                        if (QueryCheck.count > 0)
                        {
                            isError = 1;
                            contentMessage = "Vehicle Number already exist. Can not add Vehicle data.";
                        }
                        else
                        {
                            db.Execute("DLVVehicle_SaveVehicleData", new object[] { 
                                LP_Cd.ToUpper(),Vehicle_No,AuthorizedUser.Username,Saving_Status,""
                            });
                            contentMessage = "Vehicle data has been added";
                        }
                }
                catch
                {
                    isError = 1;
                    contentMessage = "Error when add Vehicle data";
                }
                return Content(((isError == 1) ? "Error|" : "Sucess|") + contentMessage);

            }
            else
            {
                try
                {
                    db.Execute("DLVVehicle_SaveVehicleData", new object[] { 
                                LP_Cd.ToUpper(),Vehicle_No,AuthorizedUser.Username,Saving_Status,Vehicle_ID});

                    contentMessage = "Vehicle data has been updated";
                }
                catch
                {
                    isError = 1;
                    contentMessage = "Error when update Vehicle data";
                }
                return Content(((isError == 1) ? "Error|" : "Sucess|") + contentMessage);
            }
        }

        public ContentResult DeleteVehicleMaster(string Key)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string contentMessage = "";

            string[] ListKey;

            if (Key != "")
            {
                ListKey = Key.Split(',');
                if (ListKey.Length > 1)
                {
                    Key = "";
                    for (int i = 0; i < ListKey.Length; i++)
                    {
                        if (Key != "")
                        {
                            Key = Key + ", '" + ListKey[i].ToString().Trim() + "'";
                        }
                        else
                        {
                            Key = "'" + ListKey[i].ToString().Trim() + "'";
                        }
                    }
                }
                else
                {
                    Key = "'" + Key + "'";
                }
            }

            try 
            {
                db.Execute("DLVVehicle_DeleteVehicleData", new object[] { Key });
                contentMessage = "Vehicle data deleted";
            }
            catch (Exception exc)
            {
                isError = 1;
                contentMessage = "Error when delete vehicle data.";
            }

            return Content(((isError == 1) ? "Error|" : "Success|") + contentMessage);
        }


    }
}
