﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Master;

namespace Portal.Controllers.Master
{
    public class LogisticPartnerMasterController : BaseController
    {
       public LogisticPartnerMasterController()
           : base("LogisticPartnerMaster", "Trucking Master")
           
       {
        
       }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            ViewData["PayMethodData"] = GetPayMethod();
            ViewData["PayTermData"] = GetPayTerm();
        }

        public ActionResult LPPartialAdditional()
        {
            TempData["GridName"] = "grlLPHeader";
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult PayMethodPartial()
        {
            TempData["GridName"] = "grlEditPayMethod";
            ViewData["PayMethodData"] = GetPayMethod();
            return PartialView("GridLookup/PartialGrid", ViewData["PayMethodData"]); 
        }

        public ActionResult PayTermPartial()
        {
            TempData["GridName"] = "grlEditPayTerm";
            ViewData["PayTermData"] = GetPayTerm();
            return PartialView("GridLookup/PartialGrid", ViewData["PayTermData"]);
        }

        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }

        public ActionResult ConfirmBox()
        {
            return PartialView("ConfirmBox");
        }


        public List<PayMethod> GetPayMethod()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<PayMethod> listPayMethod = new List<PayMethod>();
            var QueryLog = db.Query<PayMethod>("GetPaymentInfo", new object[] { "Method" });
            listPayMethod = QueryLog.ToList<PayMethod>();
            db.Close();
            return listPayMethod;
        }

        public List<PayTerm> GetPayTerm()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<PayTerm> listPayTerm = new List<PayTerm>();
            var QueryLog = db.Query<PayTerm>("GetPaymentInfo", new object[] { "Term" });
            listPayTerm = QueryLog.ToList<PayTerm>();
            db.Close();
            return listPayTerm;
        }

        public string GetPosition()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = "";
            var QueryLog = db.SingleOrDefault<UserPosition>("GetPosition", new object[] { AuthorizedUser.Username });

            if (QueryLog == null)
                result = "";
            else
                result = ((QueryLog.USER_POSITION == null) ? "" : QueryLog.USER_POSITION);

            return result;
        }

        public List<LogisticPartner> GetAllLogisticPartners()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetLogisticPartnerByFilter", new object[] { "", "", "L", GetPosition()});
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();
            db.Close();
            return listLogisticPartner; 
        }

        public ActionResult LogisticPartnerGridLoad(string LP_Cd, string Active_Flag, string Button_Flag)
        {
            string af = Active_Flag;
            string check = "\"";
            string getPosition = GetPosition();
            bool b;

            ViewData["test"] = GetPosition();

            if (Active_Flag != null)
            {
                b = af.Contains(check);

                if (b)
                {
                    af = af.Remove(af.Length - 1); 
                    af = af.Remove(0,1); 
                }
            }

            af = ((af == null) ? "" : (af == "All") ? "0,1,2" : (af == "Inactive") ? "0" : (af == "Direct") ? "1" : (af == "Indirect") ? "2" : "");

            af = ((Button_Flag == "N") ? "" : af);

            LP_Cd = ((LP_Cd == null) ? "" : LP_Cd);
            /*string[] ListLP;
            string[] ListAf;

            if (LP_Cd != "")
            { 
                ListLP = LP_Cd.Split(';');
                if (ListLP.Length > 1)
                {
                    LP_Cd = "";
                    for (int i = 0; i < ListLP.Length; i++)
                    {
                        if (LP_Cd != "")
                        {
                            LP_Cd = LP_Cd + ", '" + ListLP[i].ToString() + "'";
                        }
                        else
                        {
                            LP_Cd = "'" + ListLP[i].ToString() + "'";
                        }
                    }
                }
                else
                {
                    LP_Cd = "'" + LP_Cd + "'";
                }
            }

            if (af != "")
            {
                ListAf = af.Split(',');
                if (ListAf.Length > 1)
                {
                    af = "";
                    for (int i = 0; i < ListAf.Length; i++)
                    {
                        if (af != "")
                        {
                            af = af + ", '" + ListAf[i].ToString() + "'";
                        }
                        else
                        {
                            af = "'" + ListAf[i].ToString() + "'";
                        }
                    }
                }
                else
                {
                    af = "'" + af + "'";
                }
            }*/

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogisticPartnerModel lpm = new LogisticPartnerModel();

            lpm.LogisticPartner = db.Query<LogisticPartner>("GetLogisticPartnerByFilter", new object[] { LP_Cd.ToUpper(), af, "G", getPosition}).ToList<LogisticPartner>();
            Model.AddModel(lpm);

            db.Close();

            return PartialView("LogisticPartnerGrid", Model);
        }

        public PartialViewResult UpdatePartial(LogisticPartner editable)
        {
            ViewData["LogisticPartnerGridData"] = editable;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogisticPartnerModel model = Model.GetModel<LogisticPartnerModel>();
            model.LogisticPartner = db.Query<LogisticPartner>("GetLogisticPartnerByFilter", new object[] { "", "", "G", GetPosition()}).ToList<LogisticPartner>();
            db.Close();
            return PartialView("LogisticPartnerGrid", Model);
        }

        public ContentResult DeleteLogisticPartnerMaster(string Delete_Status, string Key)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string errorMessage = "";

            string[] ListKey;

            if (Key != "")
            {
                ListKey = Key.Split(',');
                if (ListKey.Length > 1)
                {
                    Key = "";
                    for (int i = 0; i < ListKey.Length; i++)
                    {
                        if (Key != "")
                        {
                            Key = Key + ", '" + ListKey[i].ToString().Trim() + "'";
                        }
                        else
                        {
                            Key = "'" + ListKey[i].ToString().Trim() + "'";
                        }
                    }
                }
                else
                {
                    Key = "'" + Key + "'";
                }
            }

            try
            {

                var QueryLog1 = db.SingleOrDefault<CheckResult>("CheckLPDelete", new object[] { Key, "A" });

                if (QueryLog1.result > 0)
                {
                    isError = 1;
                    errorMessage = "Trucking Code already used for transaction. Can not delete data.";
                }

                else
                {
                    var QueryLog2 = db.SingleOrDefault<CheckResult>("CheckLPDelete", new object[] { Key, "B" });
                    if (QueryLog2.result > 0)
                    {
                        isError = 1;
                        errorMessage = "Trucking Code already used for transaction. Can not delete data.";
                    }

                    else
                    {
                        var QueryLog3 = db.SingleOrDefault<CheckResult>("CheckLPDelete", new object[] { Key, "C" });
                        if (QueryLog3.result > 0)
                        {
                            isError = 1;
                            errorMessage = "Trucking Code already used for transaction. Can not delete data.";
                        }
                        else
                        {
                            var QueryLog4 = db.SingleOrDefault<CheckResult>("CheckLPDelete", new object[] { Key, "D" });
                            if (QueryLog4.result > 0)
                            {
                                isError = 1;
                                errorMessage = "Trucking Code already used for transaction. Can not delete data.";
                            }
                            else 
                            {
                                var QueryLog5 = db.SingleOrDefault<CheckResult>("CheckLPDelete", new object[] { Key, "E" });
                                if (QueryLog5.result > 0)
                                {
                                    isError = 1;
                                    errorMessage = "Trucking Code already used for transaction. Can not delete data.";
                                }
                                else
                                {
                                    var QueryLog6 = db.SingleOrDefault<CheckResult>("CheckLPDelete", new object[] { Key, "F" });
                                    if (QueryLog6.result > 0)
                                    {
                                        isError = 1;
                                        errorMessage = "Trucking Code already used for transaction. Can not delete data.";
                                    }
                                    else
                                    {
                                        var QueryLog7 = db.SingleOrDefault<CheckResult>("CheckLPDelete", new object[] { Key, "G" });
                                        if (QueryLog7.result > 0)
                                        {
                                            isError = 1;
                                            errorMessage = "Trucking Code already used for transaction. Can not delete data.";
                                        }
                                        else
                                        {
                                            db.Execute("DeleteLogisticPartnerMaster", new object[] { 
                                                    Delete_Status, Key, AuthorizedUser.Username, "D"
                                                });
                                            errorMessage = "Logistic partner deleted";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = "Error when delete logisic partner.";
            }

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }

        public ContentResult AddingLogisticPartnerMaster(string Saving_Status, string LP_Cd, string LP_Name, string Active_Flag,string LP_Address,
                                                         string City, string Postal_Cd, string Phone_Number, string Fax_Number,string NPWP,
                                                         string PayMethod, string PayTerm)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            Active_Flag = ((Active_Flag == null) ? "" : ((Active_Flag == "Inactive") ? "0" : (Active_Flag == "By Supplier") ? "1" : "2"));

            NPWP = ((NPWP == "__.___.___._-___.___") ? "" : NPWP);

            if (NPWP.Contains("_"))
            {
                NPWP = NPWP.Replace("_", "");
            }

            int isError = 0;
            string errorMessage = "";

            if (Saving_Status == "New")
            {
                try
                {
                    //---=== Check if exists on TB_M_LOGISTIC_PARTNER
                    var QueryLog = db.SingleOrDefault<CheckResult>("GetRoutePriceMasterDataCheck", new object[] { 
                        "L", LP_Cd
                    });

                    if (QueryLog.result == 1)
                    {
                        isError = 1;
                        errorMessage = "Trucking Code already exist. Please use another code.";
                    }
                    else
                    {
                        if (NPWP.Length > 0 && NPWP.Length < 20)
                        {
                            isError = 1;
                            errorMessage = "Please fill NPWP in valid format.";
                        }

                        else
                        {
                            db.Execute("SetLogisticPartnerMaster", new object[] { 
                                LP_Cd.ToUpper(),LP_Name,Convert.ToInt32(Active_Flag),LP_Address,City,Postal_Cd,Phone_Number,
                                Fax_Number,NPWP,AuthorizedUser.Username,PayMethod,PayTerm
                            });
                            errorMessage = "Trucking data has been added";
                        }
                    }

                }
                catch (Exception exc)
                {
                    isError = 1;
                    errorMessage = "Error when add Trucking data";
                }

                return Content(((isError == 1) ? "Error|" : "Sucess|") + errorMessage);
            }
            else
            {
                try
                {
                    if (NPWP.Length > 0 && NPWP.Length < 20)
                    {
                        isError = 1;
                        errorMessage = "Please fill NPWP in valid format.";
                    }
                    else
                    {
                        db.Execute("UpdateLogisticPartner", new object[] { 
                            LP_Cd.ToUpper(),LP_Name,Active_Flag,LP_Address,City,Postal_Cd,Phone_Number,
                            Fax_Number,Saving_Status,NPWP,AuthorizedUser.Username,PayMethod,PayTerm
                        });
                        errorMessage = "Trucking data has been updated";
                    }
                }
                catch (Exception exc)
                {
                    isError = 1;
                    errorMessage = "Error when update Trucking data";
                }

                return Content(((isError == 1) ? "Error|" : "Sucess|") + errorMessage);
            }
        }
    }
}
