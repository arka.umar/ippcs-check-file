﻿using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Portal.Models.AuthorizationMapping;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;

namespace Portal.Controllers
{
    public class AuthorizationMappingController : BaseController
    {
        public AuthorizationMappingController()
            : base("Authorization Mapping")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                AuthorizationMappingModel um = new AuthorizationMappingModel();
                Model.AddModel(um);

                var username = db.Query<UsernameLookup>("GetUsernameForLookup");
                var role = db.Query<RoleIDLookup>("GetRoleForLookup", new object[] { "" });
                var system = db.Query<SystemIDLookup>("GetSystemIDForLookup");

                db.Close();

                ViewData["UsernameData"] = username;
                ViewData["RoleData"] = role;
                ViewData["SystemData"] = system;

            }
            catch (Exception e)
            {


            }
        }

        public ActionResult PartialHeader()
        {
            return PartialView("PartialAuthorizationMappingHeader");
        }

        public ActionResult PartialHeaderButton()
        {
            return PartialView("PartialAuthorizationMappingButtonHeader");
        }

        public ActionResult PartialAuthDatas()
        {
            AuthorizationMappingModel um = new AuthorizationMappingModel();
            Model.AddModel(um);

            return PartialView("PartialAuthMaintenanceGrid", Model);
        }

        public ActionResult PartialAuthGridView()
        {

            try
            {
                
                string p_username = Request.Params["p_username"];
                string p_role = Request.Params["p_role"];
                string p_system = Request.Params["p_system"];

                string clear = Request.Params["clear"];
                
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                //if search button pressed, then var clear = N            
                AuthorizationMappingModel um = Model.GetModel<AuthorizationMappingModel>();
                if (clear == "N")
                {
              

                    var query = db.Query<AuthorizationData>("GetAllUserDataAuth", new object[] { p_username, p_role, p_system });
                    um.AuthorizationDatas = query.ToList<AuthorizationData>();
                }
               
                
                db.Close();

                Model.AddModel(um);

            }
            catch (Exception e)
            {
            }

            return PartialView("PartialAuthMaintenanceGrid", Model);

        }

        public ActionResult getUsernameData()
        {
            try
            {
                TempData["GridName"] = "grlUsername";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                AuthorizationMappingModel um = Model.GetModel<AuthorizationMappingModel>();

                List<UsernameLookup> companyData = db.Query<UsernameLookup>("GetUsernameForLookup").ToList();

                db.Close();

                ViewData["UsernameData"] = companyData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["UsernameData"]);


        }

        public ActionResult getSystemID()
        {
            try
            {
                string system_id = Request.Params["System_ID"] == null ? "" : Request.Params["System_ID"];
                TempData["GridName"] = "grlSystemID";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                AuthorizationMappingModel um = Model.GetModel<AuthorizationMappingModel>();

                List<SystemIDLookup> companyData = db.Query<SystemIDLookup>("GetSystemIDForLookup").ToList();

                db.Close();

                ViewData["SystemData"] = companyData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["SystemData"]);
        }

        public ActionResult getRoleID()
        {
            try
            {
                string system_id = Request.Params["p_system"] == null ? "" : Request.Params["p_system"];
                TempData["GridName"] = "grlRoleID";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                AuthorizationMappingModel um = Model.GetModel<AuthorizationMappingModel>();

                List<RoleIDLookup> companyData = db.Query<RoleIDLookup>("GetRoleForLookup", new object[] { system_id }).ToList();

                db.Close();

                ViewData["RoleData"] = companyData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["RoleData"]);
        }

        public ContentResult DeleteUser(string usernamelist)
        {
            string resultMessage = "";

            usernamelist = usernamelist.Substring(0, usernamelist.Length - 1);

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                db.Execute("deleteAuthUser", new object[] { usernamelist });
                resultMessage = "Delete Data Finish Successfully.";

            }
            catch (Exception e)
            {
                resultMessage = e.Message.ToString();
            }

            return Content(resultMessage.ToString());



        }

        public ContentResult countData(string username, string role, string system)
        {
            string resultmessage = "";

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                resultmessage = db.SingleOrDefault<string>("countDataForDownloadAuthMapping", new object[] { username, role, system });

                int numVal = Int32.Parse(resultmessage);

                if (numVal > 0)
                {
                    resultmessage = "download";
                }
                else
                {
                    resultmessage = "No Data Found.";
                }

            }
            catch (Exception e)
            {
                resultmessage = e.Message.ToString();    
            }

            return Content(resultmessage);
        }

        public ActionResult PartialPopUpFormAuthMapping()
        {
            try
            {
                //string pBranch = Request.Params["p_branch"];

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                AuthorizationMappingModel um = new AuthorizationMappingModel();
                Model.AddModel(um);

                var query = db.Query<UsernameLookup>("GetUsernameForLookup");
                var role = db.Query<RoleIDLookup>("GetRoleForLookup", new object[] { "" });
                var system = db.Query<SystemIDLookup>("GetSystemIDForLookup");
                //var classs = db.Query<ClassData>("GetClassData", new object[] { pBranch });
                //var location = db.Query<LocationData>("GetLocationData");
                //var jobfunction = db.Query<JobFunctionData>("GetJobFunctionData");
                db.Close();

                ViewData["UsernameDatas"] = query;
                ViewData["RoleDatas"] = role;
                ViewData["SystemDatas"] = system;
                //ViewData["ClassDataAdd"] = classs;
                //ViewData["LocationDataAdd"] = location;
                //ViewData["JobFunctionDataAdd"] = jobfunction;

            }
            catch (Exception ex)
            {

            }

            return PartialView("formAuthorizationMapping");

        }

        public ActionResult Save(string p_username, string p_roleID, string p_systemID, string p_mode, string p_oldUsername, string p_oldRole, string p_oldsystemID)
        {
            string result = "";

            if (p_mode == "add")
            {
                p_oldUsername = "";
                p_oldRole = "";
            }

            string User = AuthorizedUser.Username;

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                result = db.SingleOrDefault<string>("SaveEditAuth", new object[] {p_username, p_roleID, User, p_mode, p_oldUsername, p_oldRole, p_systemID, p_oldsystemID});
            }
            catch (Exception ex)
            {
                result = ex.Message.ToString();
            }

            return Content(result);
        }

        public ActionResult getUsernameLookup()
        {
            try
            {

//                string jobfunction = Request.Params["gridJobFunctionName"];

                TempData["GridName"] = "grlUsernameForm";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                //UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<UsernameLookup> UsernameData = db.Query<UsernameLookup>("GetUsernameForLookup").ToList();

                db.Close();

                ViewData["UsernameDatas"] = UsernameData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["UsernameDatas"]);


        }

        public ActionResult getSystemLookup()
        {
            try
            {

                //                string jobfunction = Request.Params["gridJobFunctionName"];

                TempData["GridName"] = "grlSysIDForm";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                //UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<SystemIDLookup> SystemData = db.Query<SystemIDLookup>("GetSystemIDForLookup").ToList();

                db.Close();

                ViewData["SystemDatas"] = SystemData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["SystemDatas"]);


        }

        public ActionResult getRoleLookup()
        {
            try
            {
                string SystemID = Request.Params["p_system"] == null ? "" : Request.Params["p_system"];

                //string jobfunction = Request.Params["gridJobFunctionName"];

                TempData["GridName"] = "grlRoleIDForm";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                //UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<RoleIDLookup> RoleData = db.Query<RoleIDLookup>("GetRoleForLookup", new object[] { SystemID }).ToList();

                db.Close();

                ViewData["RoleDatas"] = RoleData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["RoleDatas"]);
            

           /* TempData["gridname"] = "grlRoleIDForm";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<RoleIDLookup> roleid = new List<RoleIDLookup>();
            roleid = db.Query<RoleIDLookup>("GetRoleForLookup").ToList();
            return PartialView("GridLookup/PartialGrid", roleid);*/


        }

        public ActionResult getSpecifiedData(string username, string role)
        {
            string result = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                List<AuthorizationData> specifiedData = db.Fetch<AuthorizationData>("GetSpecifiedDataForEdit", new object[] { username, role });

                foreach (var data in specifiedData)
                {
                    result = data.USERNAME;
                    result += "|";
                    result +=data.ROLE_NAME;
                    result += "|";
                    result += data.ROLE_ID;
                    result += "|";
                    result += username;
                    result += "|";
                    result += role;
                    result += "|";
                    result += data.SYSTEM_NAME;
                    result += "|";
                    result += data.SYSTEM_ID;
                }
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }

            db.Close();

            return Content(result);
        }

        public void download(string username, string role, string system)
        {
            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;

                DateTime today = DateTime.Today;

                string date = today.ToString("dd-MM-yyyy");


                string filename = "Authorization_Mapping_List_" + date + ".xls";
                string zipname = "Authorization_Mapping_List_" + date + ".zip";


                documentBytes = downloadProcess(username, role, system);

                listCompress.Add(filename, documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/zip";
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipname));

                Response.BinaryWrite(documentBytes);
                Response.Flush();
                Response.End();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                
            }


        }

        public byte[] downloadProcess(string username, string role, string system)
        {
            try
            {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                string title = "Authorization Mapping";
                title = title.ToUpper();

                filesTmp = HttpContext.Request.MapPath("~/Template/Authorization_mapping_template.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;

                int row = 3;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.Alignment = HorizontalAlignment.CENTER;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.TOP;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.SetFont(Font1);



                #endregion

                List<AuthorizationData> datas = new List<AuthorizationData>();
                datas = db.Query<AuthorizationData>("GetAllUserDataAuth", new object[] { username, role, system }).ToList();
                
                //datas = GetSpecifiedLog(processidd);

                /*DateTime MyDateFrom;
                string stringDateFrom;
                MyDateFrom = new DateTime();
                MyDateFrom = DateTime.ParseExact(p_dateFrom, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);

                stringDateFrom = MyDateFrom.ToString("dd.MM.yyyy");

                DateTime MyDateTo;
                string stringDateTo;
                MyDateTo = new DateTime();
                MyDateTo = DateTime.ParseExact(p_dateTo, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);
                stringDateTo = MyDateTo.ToString("dd.MM.yyyy");*/


                ISheet sheet;

                sheet = workbook.GetSheetAt(0);
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue(title);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                Hrow.GetCell(0).CellStyle = styleContent4;


                /*Hrow = sheet.CreateRow(1);
                Hrow.CreateCell(0).SetCellValue("Prod. Date : " + stringDateFrom + " - " + stringDateTo);
                sheet.AddMergedRegion(new CellRangeAddress(1, 1, 0, 3));*/

                Hrow = sheet.CreateRow(2);


                Hrow.CreateCell(0).SetCellValue("No. ");
                sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("Branch  ");
                sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("System Name    ");
                sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Username              ");
                sheet.AutoSizeColumn(3);
                Hrow.CreateCell(4).SetCellValue("Role ID  ");
                sheet.AutoSizeColumn(4);
                Hrow.CreateCell(5).SetCellValue("Role Name                               ");
                sheet.AutoSizeColumn(5);

                Hrow.CreateCell(6).SetCellValue("Created Date    ");
                sheet.AutoSizeColumn(6);
                Hrow.CreateCell(7).SetCellValue("Created By                   ");
                sheet.AutoSizeColumn(7);

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;


                int no = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    //data.PRODUCTION_DATE.ToString("dd.MM.yyyy")
                    Hrow.CreateCell(0).SetCellValue(no);
                    Hrow.CreateCell(1).SetCellValue(data.BRANCH_ID);
                    Hrow.CreateCell(2).SetCellValue(data.SYSTEM_NAME);
                    Hrow.CreateCell(3).SetCellValue(data.USERNAME);
                    Hrow.CreateCell(4).SetCellValue(data.ROLE_ID);
                    Hrow.CreateCell(5).SetCellValue(data.ROLE_NAME);
                    Hrow.CreateCell(6).SetCellValue(data.CREATED_DATE.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(7).SetCellValue(data.CREATED_BY);

                    Hrow.GetCell(0).CellStyle = styleContent1;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent1;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = styleContent1;
                    Hrow.GetCell(7).CellStyle = styleContent3;

                    row++;
                    no++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();
                return a;
            }

        }

        //upload
        #region ACTION
        [HttpPost]
        public ActionResult AuthorizationMappingUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }
        #endregion

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }


        public FileContentResult DownloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "Authorization_mapping_template.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/AuthorizationMapping/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("AMUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }


        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/AuthorizationMapping/TempUpload/");
                return _uploadDirectory;
            }
        }

        private AuthorizationMappingModel AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            AuthorizationMappingModel _UploadModel = new AuthorizationMappingModel();

            string function = "61006";
            string module = "6";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";
            
            string tableFromTemp = "TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP";
            //string tableToAccess = "TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP";

            string sheetName = "AuthList";
           
            message = "Starting Upload Authorization Mapping";
            location = "AuthorizationMappingUpload.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP";
                location = "AuthorizationMappingUpload.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });
                //db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties = Excel 8.0";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as Username," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as RoleID," +
                                    @"IIf(IsNull(F3), Null, Cstr(F3)) as SystemID ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                //add by alira.agi 2015-04-10
                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    //Added By FID.reggy
                    //Deleting first row (column alias row), impact for using HDR:No in ConnectionString
                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    //throw new Exception("invalid excel template please download template excel");
                    //Changed By FID.Reggy
                    message = "Error : "+e.Message;
                    location = "AuthorizationMappingUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });



                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //try
                    //{
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("Username", "USERNAME");
                    bulkCopy.ColumnMappings.Add("RoleID", "ROLE_ID");
                    bulkCopy.ColumnMappings.Add("SystemID", "SYSTEM_ID");
                    
                #endregion

                    try
                    {
                        message = "Insert Data to TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP";
                        location = "AuthorizationMappingUpload.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });



                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : "+ex.Message;
                        location = "AuthorizationMappingUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });
                        //modif by alira.agi 2015-04-10
                        //throw new Exception ("Column does not match");

                        //modified by fid.intan 2015-08-24
                        throw new Exception(ex.Message);
                    }
                }
                

                // validate data in temporary uploaded table.
                string validate =  db.SingleOrDefault<string>("ValidateAuthMappingUpload", new object[] { processID });

                
                

                if (validate == "error")
                {
                    TempData["IsValid"] = "false;"+processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found.")
                {
                    TempData["IsValid"] = "empty;" + processID;
                    sts = "6";
                    message = validate;
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }


                location = "AuthorizationMappingUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            sts      // sts
                                        });
    
            }
            catch (Exception exc)
            {
                
                TempData["IsValid"] = "false;" + processID + ";" + exc.Message;


                message = "Error : "+exc.Message;
                location = "AuthorizationMappingUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

             }
            finally
            {
             

                excelConn.Close();

                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

                
               

            }
            return _UploadModel;
        }

        public ActionResult MoveAuthDataTemp(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("moveAuthDataTemp", new object[] {processId, user});
                message = "SUCCESS";
            }
            catch(Exception ex)
            {
                message = ex.Message.ToString();
            }


            return Content(message);
        }

       /* [HttpGet]
        public void UploadInvalid(String process_id)
        {
            string FileNames = "authorizationMapping_Invalid.xls";
            string SheetNames = string.Empty;
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] ResultQuery = null;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);


            SheetNames = "AuthList";
            IEnumerable<ErrorListDownload> authMapping = db.Query<ErrorListDownload>("GET_ERROR_LIST_UPLOAD_AUTH_MAPPING", new object[] { process_id });
            ResultQuery = exporter.Write(ConvertToDataTable(authMapping), SheetNames);
            

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(ResultQuery);
            Response.End();
        }
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        
        */
        //  ---
        [HttpGet]
        public void UploadInvalid(String process_id){
        try
            {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;

                filesTmp = HttpContext.Request.MapPath("~/Template/Authorization_mapping_template.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;

                int row = 1;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);


                #endregion

                List<ErrorListDownload> datas = new List<ErrorListDownload>();

                datas = db.Query<ErrorListDownload>("GET_ERROR_LIST_UPLOAD_AUTH_MAPPING", new object[] { process_id }).ToList(); ;
               
                ISheet sheet;

                sheet = workbook.GetSheetAt(0);


                
                Hrow = sheet.CreateRow(0);


                Hrow.CreateCell(0).SetCellValue("Username                         ");
                sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("Role ID         ");
                sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("System ID    ");
                sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Message                                     ");
                sheet.AutoSizeColumn(3);

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;

                int no = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    //data.PRODUCTION_DATE.ToString("dd.MM.yyyy")
                    Hrow.CreateCell(0).SetCellValue(data.USERNAME);
                    Hrow.CreateCell(1).SetCellValue(data.ROLE_ID);
                    Hrow.CreateCell(2).SetCellValue(data.SYSTEM_ID);
                    Hrow.CreateCell(3).SetCellValue(data.MSG);

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent1;
                    Hrow.GetCell(3).CellStyle = styleContent1;

                    row++;

                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();

                documentBytes = ms.ToArray();

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;

                string FileNames = "authorizationMapping_Invalid.xls";
            
                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(documentBytes);
                Response.End();


                //return ms.ToArray();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();
               
                //return a;
            }

        }

        //---
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}
