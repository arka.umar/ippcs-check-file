﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RoleArea;

namespace Portal.Controllers
{
    public class RoleAreaController : BaseController
    {
        public RoleAreaController()
            : base("Role Area Mapping Screen")
        { 
        }
        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            RoleAreaModel model = new RoleAreaModel();

            for (int i = 0; i < 21; i++)
            {
                model.RoleAreaDatas.Add(new RoleAreaData
                    {
                        Check = false,
                        AreaID = 001+(i+1),
                        AreaName = "AN"+(i+1),
                        Data = "DT"+(i+1),
                        RoleObjectID = 0002+(i+1),
                        CreatedBy = "CB"+(i+1),
                        CreatedDate = DateTime.Now,
                        ChangedBy = "CB"+(i+1),
                        ChangedDate = DateTime.Now,
                    });
            }


                Model.AddModel(model);
        }
       
        public ActionResult PartialRoleAreaDatas()
        {
            return PartialView("PartialRoleArea", Model);
        }
        public ActionResult PartialRoleAreaHeadernye()
        {
            return PartialView("PartialRoleAreaHeader", Model);
        }
        public ActionResult PartialRoleAreaGriddernye()
        {
            return PartialView("PartialRoleArea", Model);
        }
        public ActionResult PartialRoleButtonGridnyeduluan()
        {
            return PartialView("PartialRoleAreaButtonUpGrid", Model);
        }
    }
}
