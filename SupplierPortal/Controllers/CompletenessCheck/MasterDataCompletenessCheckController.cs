﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.MasterDataCompletenessCheck;
using Toyota.Common.Web.Database;
using Portal.Models.Dock;
using Toyota.Common.Web.Service;
using Portal.Models.LPOPConsolidation;
using Toyota.Common.Util.Text;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.Task;
using Toyota.Common.Task;
using Toyota.Common.Task.External;

namespace Portal.Controllers
{
    public class MasterDataCompletenessCheckController : BaseController
    {
        private string[] compMonth;
        private string prodMonth;
        private MasterDataCompletenessCheckModel model;
        private  string currentProductionMonth;        
        private IDBContext db;

        private IDBContext DBContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<MDCompletenessCheckListData> _getListMode = null;
        private List<MDCompletenessCheckListData> getListModel
        {
            get
            {
                if (_getListMode == null)
                    _getListMode = new List<MDCompletenessCheckListData>();

                return _getListMode;
            }
            set
            {
                _getListMode = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;        
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }

        public MasterDataCompletenessCheckController()
            : base("Master Data Completeness Check Screen")
        { }

        protected override void StartUp()
        {
        }

        public ActionResult HandleForm(
                string MMActivityOption,
                string MMProdMonthDate,
                string MMMatDescText,
                string MMDockOption,
                string MMCheckTimingDate,
                string MMLastRunDate,
                string MMMatNoText
            )
        {
            TempData["MMActivity"] = MMActivityOption;
            TempData["MMProdMonth"] = MMProdMonthDate;
            TempData["MMMatDesc"] = MMMatDescText;
            TempData["MMDock"] = MMDockOption;
            TempData["MMCheckTiming"] = MMCheckTimingDate;
            TempData["MMLastRun"] = MMLastRunDate;
            TempData["MMMatNo"] = MMMatNoText;
            
            model = new MasterDataCompletenessCheckModel();
            model.MDCompletenessCheckListData = GetDataCompletenessCheck(  
                MMActivityOption,
                MMMatNoText,                
                MMProdMonthDate,
                MMMatDescText,               
                MMCheckTimingDate,
                MMLastRunDate,
                MMDockOption
                );
            Model.AddModel(model);
            TempData["MDCompData"] = model;

            return View("Index", Model);
            
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            db = DBContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return orderInquiryDockModel;
        }

        private List<MDCompletenessCheckListData> GetDataCompletenessCheck(
            string mmActivityOption,
            string mmMatNoText,
            string mmProdMonthDate,
            string mmMatDescText,
            string mmCheckTimingDate,
            string mmLastRunDate,
            string mmDockOption)
        {
            if (!string.IsNullOrEmpty(mmProdMonthDate))
            {
                compMonth = mmProdMonthDate.Split('.').ToArray();
                prodMonth = compMonth[1] + compMonth[0];
            }

            db = DBContext;
            if (!string.IsNullOrEmpty(mmActivityOption))
            {
                if (mmActivityOption.Equals("LPOP"))
                {
                    getListModel = db.Fetch<MDCompletenessCheckListData>("GetDefLPOPMatDatCompCheck", new object[]{
                        mmProdMonthDate != null? prodMonth : currentProductionMonth,
                        mmMatNoText == null? "" : mmMatNoText,
                        mmMatDescText == null? "" : mmMatDescText,
                        mmDockOption == null? "" : mmDockOption
                    });
                }
                if (mmActivityOption.Equals("GR"))
                {
                    getListModel = db.Fetch<MDCompletenessCheckListData>("GetDefGRMatDatCompCheck", new object[]{
                        mmProdMonthDate != null? prodMonth : currentProductionMonth,
                        mmMatNoText == null? "" : mmMatNoText,
                        mmMatDescText == null? "" : mmMatDescText,
                        mmDockOption == null? "" : mmDockOption
                    });
                }

            }
            else
            {
                getListModel = db.Fetch<MDCompletenessCheckListData>("GetDefLPOPMatDatCompCheck", new object[]{
                        currentProductionMonth, "", "", ""
                    });
            }
            db.Close();
            return getListModel;
        }

        protected override void Init()
        {
            DateTime currentMonth = DateTime.Now;
            currentProductionMonth = Convert.ToString(currentMonth.Year) + String.Format("{0:D2}", (currentMonth.Month));
        }

        public ActionResult PartialHeaderCOmpletenessCheck()
        {
            _OrderInquiryDockModel = GetAllDock();
            Model.AddModel(_OrderInquiryDockModel);
            return PartialView("MasterDataCompletenessCheckHeader", Model);
        }

        public ActionResult UploadProgressPartial()
        {
            return PartialView("UploadProgressPartial", Model);
        }

        public ActionResult PartialMasterDataCompletenessDataGridView()
        {
            if (TempData["MDCompData"] != null)
            {
                string mmActivityOption = TempData["MMActivity"] as string;
                string mmMatNoText = TempData["MMMatNo"] as string;
                string mmLastRunDate = TempData["MMLastRun"] as string;
                string mmProdMonthDate = TempData["MMProdMonth"] as string;
                string mmMatDescText = TempData["MMMatDesc"] as string;
                string mmCheckTimingDate = TempData["MMCheckTiming"] as string;
                string mmDockOption = TempData["MMDock"] as string;

                model = new MasterDataCompletenessCheckModel();
                model.MDCompletenessCheckListData = GetDataCompletenessCheck(
                    mmActivityOption,
                    mmMatNoText,
                    mmProdMonthDate,
                    mmMatDescText,                    
                    mmCheckTimingDate,
                    mmLastRunDate,
                    mmDockOption);
                Model.AddModel(model);               

                return PartialView("MasterDataCompletenessCheckData", Model);                   
            }
            else
            {
                model = new MasterDataCompletenessCheckModel();
                model.MDCompletenessCheckListData = GetDataCompletenessCheck(
                    Request.Params["_MMActivityOption"],
                    Request.Params["_MMMatNoText"],
                    Request.Params["_MMProdMonthDate"],
                    Request.Params["_MMMatDescText"],
                    Request.Params["_MMCheckTimingDate"],
                    Request.Params["_MMLastRunDate"],  
                    Request.Params["_MMDockOption"]);
                Model.AddModel(model);

                return PartialView("MasterDataCompletenessCheckData", Model);
            }            
        }        

        public ActionResult MasterDataCompletenessGrid()
        {
            return PartialView("MasterDataCompletenessCheckGridVw", Model);
        }

        public ActionResult PartialPopUpCompletenessCheck()
        {
            return PartialView("popupCompletenessCheck", Model);
        }

        public ActionResult PartialPopUpMatNoDetails()
        {
            return PartialView("popupMaterialNoDetails", Model);
        }

        public ActionResult PartialMMDockOption()
        {
            TempData["GridName"] = "MMDockOption";
            _OrderInquiryDockModel = GetAllDock();           

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }

        #region PERFORM MASTER COMPLETENESS CHECK LPOP
        public void PerformMasterLPOP(string ProdMonth)
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("SynchOnDemanFrom", new object[] { "C", "MCC_PO_NOTIFICATION_DAILY" });
            }
            catch (Exception ex) { }
            db.Close();
            db.Dispose();

            //BackgroundTaskService service = BackgroundTaskService.GetInstance();

            //BackgroundTaskParameter taskParameter = new BackgroundTaskParameter();
            //taskParameter.Add("ProdMonth", ProdMonth);
            //taskParameter.Add("Timing", "Firm");
            //taskParameter.Add("Username", AuthorizedUser.Username);

            //BackgroundTask immediateTask = new ExternalBackgroundTask()
            //{
            //    Id = "MDCCLPOP_ID",
            //    Name = "MasterDataCompletenessCheckLPOPTaskRuntime",
            //    Description = "Background Task Master Data Completeness Check LPOP",
            //    FunctionName = "MDCCLPOP_BT",
            //    Submitter = new Toyota.Common.Credential.User() { Username = AuthorizedUser.Username },
            //    Type = TaskType.Immediate,
            //    Range = new TaskRange()
            //    {
            //        Time = new TimeSpan(1, 54, 0)
            //    }, 
            //    Parameters = taskParameter,
            //    Command = service.GetTaskPath("MasterDataCompletenessCheckLPOPTaskRuntime")
            //};
             
            //service.Register(immediateTask);
        }
        # endregion

        #region PERFORM MASTER COMPLETENESS CHECK GR
        public void PerformMasterGR(string ProdMonth)
        {
            BackgroundTaskService service = BackgroundTaskService.GetInstance();

            BackgroundTaskParameter taskParameter = new BackgroundTaskParameter();
            taskParameter.Add("ProdMonth", ProdMonth);
            taskParameter.Add("Timing", "Firm");
            taskParameter.Add("Username", AuthorizedUser.Username);

            BackgroundTask immediateTask = new ExternalBackgroundTask()
            {
                Id = "MDCCGR_ID",
                Name = "MasterDataCompletenessCheckGRTaskRuntime",
                Description = "Background Task Master Data Completeness Check GR",
                FunctionName = "MDCCGR_BT",
                Submitter = new Toyota.Common.Credential.User() { Username = AuthorizedUser.Username },
                Type = TaskType.Immediate,
                Range = new TaskRange()
                {
                    Time = new TimeSpan(1, 54, 0)
                },
                Parameters = taskParameter,
                Command = service.GetTaskPath("MasterDataCompletenessCheckGRTaskRuntime")
            };

            service.Register(immediateTask);
        }
        # endregion

        #region BACKGROUND JOB CHECK
        
        public ContentResult BackGroundJobCheck()
        {
            string ResultCheck = "";
            string ResultMessage = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                List<Portal.Models.DCLReport.SyncCheck> QueryLog = db.Fetch<Portal.Models.DCLReport.SyncCheck>("SynchOnDemanFrom", new object[] { "S", "MCC_PO_NOTIFICATION_DAILY" });
                for (int i = 0; i < QueryLog.Count; i++)
                {
                    if (QueryLog[i].Job_Name == "MCC_PO_NOTIFICATION_DAILY")
                    {
                        ResultCheck = QueryLog[i].Job_Status;
                        ResultMessage = QueryLog[i].Job_Message;
                    }
                }

                return Content(ResultCheck + "|" + ResultMessage);
            }
            catch (Exception ex)
            {
                return Content("Failed|Completeness check synchronize failed. " + ex.ToString());
            }
            db.Close();
            db.Dispose();
        }

        #endregion

    }
}
