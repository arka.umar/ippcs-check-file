﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Portal.Models.SubSupplierMappingSPEX;
using Portal.Models.Supplier;
using Portal.Models.Dock;

namespace Portal.Controllers
{
    public class SubSupplierMappingSPEXController : BaseController
    {
        public string CookieName { get; set; }

        public string CookiePath { get; set; }
        
        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
            {
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            }
            else
            {
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }
         
         private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

         public SubSupplierMappingSPEXController()
             : base("Sub Supplier Mapping SPEX")
         {
             PageLayout.UseMessageBoard = true;
             //CookieName = "fileDownload";
             //CookiePath = "/";
         }

        #region Model properties
        List<SupplierName> _SSMSPEXSupplierModel = null;
        private List<SupplierName> __SSMSPEXSupplierModel
        {
            get
            {
                if (_SSMSPEXSupplierModel == null)
                    _SSMSPEXSupplierModel = new List<SupplierName>();

                return _SSMSPEXSupplierModel;
            }
            set
            {
                _SSMSPEXSupplierModel = value;
            }
        }

        List<SupplierNameSPEX> _SSMSPEXSubSupplierModel = null;
        private List<SupplierNameSPEX> __SSMSPEXSubSupplierModel
        {
            get
            {
                if (_SSMSPEXSubSupplierModel == null)
                    _SSMSPEXSubSupplierModel = new List<SupplierNameSPEX>();

                return _SSMSPEXSubSupplierModel;
            }
            set
            {
                _SSMSPEXSubSupplierModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }
        
        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;

            SubSupplierMappingModel mdl = new SubSupplierMappingModel();
            Model.AddModel(mdl);
        }

        #region Order Inquiry controller
        #region View controller
        public ActionResult PartialHeaderSubSupplierMapping()
        {
            List<SupplierName> suppliers = GetAllSupplier();
            List<SupplierNameSPEX> subsuppliers = GetAllSubSupplier();

            __SSMSPEXSupplierModel = suppliers;
            __SSMSPEXSubSupplierModel = subsuppliers;

            Model.AddModel(__SSMSPEXSupplierModel);
            Model.AddModel(__SSMSPEXSubSupplierModel);

            return PartialView("SubSupplierMappingHeaderPartial", Model);
        }
        
        public ActionResult PartialHeaderSupplierLookupSSMSPEX()
        {
            List<SupplierName> suppliers = GetAllSupplier();

            TempData["GridName"] = "SSMHSupplierOption";
            __SSMSPEXSupplierModel = suppliers;

            return PartialView("GridLookup/PartialGrid", __SSMSPEXSupplierModel);
        }

        public ActionResult PartialHeaderSubSupplierLookupSSMSPEX()
        {
            List<SupplierNameSPEX> subsuppliers = GetAllSubSupplier();

            TempData["GridName"] = "SSMHSubSupplierOption";
            __SSMSPEXSubSupplierModel = subsuppliers;

            return PartialView("GridLookup/PartialGrid", __SSMSPEXSubSupplierModel);
        }

        public ActionResult PartialDetailSubSupplierMapping()
        {
            SubSupplierMappingModel model = Model.GetModel<SubSupplierMappingModel>();
            try
            {
                model.SubSupplierData = GetAllSubSupplierData(Request.Params["SupplierCode"], Request.Params["SubSupplierCode"]);
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("SubSupplierMappingDetailPartial", Model);
        }
        #endregion

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> orderInquirySupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierName>("GetAllSupplierSPEX", new object[] {});            
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<SupplierNameSPEX> GetAllSubSupplier()
        {
            List<SupplierNameSPEX> orderInquirySupplierModel = new List<SupplierNameSPEX>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierNameSPEX>("GetAllSubSupplierSPEX", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }
        
        private List<SubSupplierModel> GetAllSubSupplierData(string supplierCode, string SubSupplierCode)
        {
            IDBContext db = DbContext;
            
            List<SubSupplierModel> subsuppliers = db.Fetch<SubSupplierModel>("GetAllSubSupplierMappingSPEX", new object[] {
                supplierCode == null ? "" : supplierCode,
                SubSupplierCode == null ? "" : SubSupplierCode
            });
            db.Close();

            return subsuppliers;
        }
        #endregion
        #endregion
    }
}


