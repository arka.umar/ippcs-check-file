﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Download;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Notification;
using System.IO;

namespace Portal.Controllers
{
    public class DownloadScreenController : BaseController
    {
        public DownloadScreenController()
            : base("Download Screen")
        {
            
        }
         


        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            DownloadScreenModel model = new DownloadScreenModel();
            ImageApplicationIcon sai = new ImageApplicationIcon();

            FileStream imageStream = new FileStream(HttpRuntime.AppDomainAppPath + "\\Content\\Images\\Common\\Question-Mark.png", FileMode.Open);
            //Stream imageStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(HttpRuntime.AppDomainAppPath + "\\Content\\Images\\Common\\Question-Mark.png");
            MemoryStream memoryStream = new MemoryStream();
            imageStream.CopyTo(memoryStream);
            byte[] imageBytes = memoryStream.ToArray();
            memoryStream.Close();
            imageStream.Close();

            int compl;
            for (int i = 0; i < 20; i++)
            {
                compl = i * 5;
                model.DownloadScreens.Add(new DownloadScreen()
                {
                    
                    Path = "Path " + i,
                    FileName = "File Name " + i,
                    Icon = imageBytes,
                    FileSize = "File Size " + i,
                    FileType = "File Type " + i,
                    Complete = compl,
                    DownloadDate = DateTime.Now
                });
            }

            Model.AddModel(model);
        }

        public PartialViewResult DownloadScreenPartial()
        {
            return PartialView("DownloadScreenPartial", Model);
        }

        public static MvcHtmlString ImageData()
        {
            ByteApplicationIcon sai = new ByteApplicationIcon();

            return MvcHtmlString.Create(sai.GetIcon("Pdf").ToString());
        }


    }
}
