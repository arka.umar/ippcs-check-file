﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.LogTransaction;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;



namespace Portal.Controllers
{
    public class LogTransactionController : BaseController
    {
        
        public logTransactionModel  modelExport;
        public LogTransactionController()
            : base("logTransaction", "log Transaction")
        {

        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            logTransactionModel  mdl = new logTransactionModel();
            mdl.logTransaction = db.Fetch<logTransactionMaster>("getAllLogTransaction", new object[] {"","","" });

            db.Close();

            Model.AddModel(mdl);


        }

        public ActionResult Back()
        {
            return RedirectToAction("Index", "logTransaction");
        }


        protected List<logTransactionMaster> LogTransaction()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<logTransactionMaster> LogList = new List<logTransactionMaster>();
            IEnumerable<logTransactionMaster> QueryLog = db.Query<logTransactionMaster>("getAllLogTransaction", new object[] {  });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new logTransactionMaster()
                    {
                        logID=q.logID,
                        processID=q.processID,
                        manifestNo=q.manifestNo,
                        createdBy=q.createdBy,
                        createdDate=q.createdDate
                        
                    });
                }
            }
            db.Close();
            return LogList;
        }

        public ActionResult LogTransactionDetail()
        {
            string id = "";
            id=Request.Params["manifestNo"];
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            logTransactionModel model = Model.GetModel<logTransactionModel>();
            List<logTransactionMaster> loglist = new List<logTransactionMaster>();
            loglist = db.Query<logTransactionMaster>("getLogTransDetailByManifestNo", new object[] { id }).ToList();

            if (loglist.Any())
            {
                model.logTransaction.Clear();
                foreach (var q in loglist)
                {
                    model.logTransaction.Add(new logTransactionMaster()
                    {
                        logID = q.logID,
                        seqNo = q.seqNo,
                        postingDate = q.postingDate,
                        processDate = q.processDate,
                        manifestNo = q.manifestNo,
                        kanbanNo = q.kanbanNo,
                        prodPurpose = q.prodPurpose

                    });
                }
            }
            db.Close();
            Model.AddModel(model);
            return PartialView("gridDetail",Model);
        }

        public ActionResult ReloadGrid(string logID, string ProcessID, string ManifestNo)
        {
          
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            logTransactionModel model = Model.GetModel<logTransactionModel>();
            model.logTransaction = db.Fetch<logTransactionMaster>("getAllLogTransaction", new object[] { logID, ProcessID, ManifestNo });
            db.Close();
            return PartialView("GridIndex", Model);
        }


        public ActionResult IndexHeader()
        {

            return PartialView("IndexHeader", null);
        }

       

    }
}
