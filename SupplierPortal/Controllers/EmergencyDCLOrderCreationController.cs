﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;

using Portal.Models.EmergencyDCLOrderCreation;
using Portal.Models.Globals;
using Toyota.Common.Web.Credential;
using DevExpress.Web.ASPxGridView;
using Toyota.Common.Web.Database;

using DevExpress.Web.Mvc;
using System.Transactions;
using System.Globalization;
namespace Portal.Controllers
{
    public class EmergencyDCLOrderCreationController : BaseController
    {
        private EmergencyDCLOrderCreationModel mdl;
        public EmergencyDCLOrderCreationController(): base("Additional Delivery")
        {
            
        }

        protected override void StartUp()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            mdl = new EmergencyDCLOrderCreationModel();
            for (int i = 1; i <= 20; i++)
            {
                mdl.EmergencysGrid.Add(new EmergencyDCLOrderCreationDataGrid()
                {
                });
               

            }

            IEnumerable<Portal.Models.Dock.DockData> QueryDock = db.Query<Portal.Models.Dock.DockData>("GetAllDock");
            //ViewData["GridDock"] = QueryDock;

            IEnumerable<Supplier> QuerySupplier = db.Query<Supplier>("GetAllSupplierGrid");

            db.Close();
            //ViewData["GridSupplier"] = QuerySupplier;
            mdl.ListDock = QueryDock.ToList<Portal.Models.Dock.DockData>();
            mdl.GridSupplier = QuerySupplier.ToList<Supplier>();

            Model.AddModel(mdl);

            
        }
        
        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //mdl = Model.GetModel<EmergencyDCLOrderCreationModel>();
            
            
            
            List<LogisticPartner> listLP = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLP = QueryLog.ToList<LogisticPartner>();

            ViewData["LogisticPartnerData"] = listLP;

            /*List<ComboDelivery> listDelivery = new List<ComboDelivery>();
            var QueryDelivery = db.Query<ComboDelivery>("GetDeliveryNo", new object[] { DateTime.Now });
            listDelivery = QueryDelivery.ToList<ComboDelivery>();
            ViewData["DeliveryNoData"] = listDelivery;

            ViewData["ComboRoute"] = db.Query<ComboRoute>("GetRoute").ToList<ComboRoute>();
            */

            db.Close();
           
        }

        public ActionResult HeaderDCLOrderCreation()
        {
            return PartialView("HeaderDCLOrderCreation", Model);
        }
        public ActionResult LPPartialEmergency()
        {
            TempData["GridName"] = "grlLPHeader";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            //List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            //listLogisticPartner = QueryLog.ToList<LogisticPartner>();
            db.Close();
            ViewData["LogisticPartnerData"] = QueryLog;
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }
        public ActionResult DockCodePartial(string gridName)
        {
            if (gridName == null)
            {
                TempData["GridName"] = "grlDock";
            }
            else
            {
                TempData["GridName"] = gridName;
            }
            
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryDock = db.Query<Dock>("GetAllDock");
            ViewData["GridDock"] = QueryDock;
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);
        }

        public ActionResult SupplierCodePartial(string gridName, string DeliveryNo)
        {
            if (gridName == null)
            {
                TempData["GridName"] = "grlSupplier";
            }
            else
            {
                TempData["GridName"] = gridName;
            }
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            if (DeliveryNo == null || !DeliveryNo.Equals(""))
            {
                var QuerySupplier = db.Query<Supplier>("GetAllSupplierGrid");
                ViewData["GridSupplier"] = QuerySupplier;
            }
            else {
                var QuerySupplierDelivery = db.Query<Supplier>("GetAllSupplierGridByDeliveryNo");
                ViewData["GridSupplier"] = QuerySupplierDelivery;
            }
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);

        }
        

        /*public ActionResult SupplierPartial()
        {
            List<Supplier> listSupplier = new List<Supplier>();

            for (int i = 1; i <= 5; i++)
            {
                listSupplier.Add(new Supplier
                {
                    SupplierCode = "5000" + i.ToString("0##"),
                    SupplierName = "Supplier " + i.ToString("0##")
                });
            }

            ViewData["SupplierData"] = listSupplier;
            return PartialView("PartialSupplierGrid", ViewData["SupplierData"]);
        }*/

        public ActionResult PartialManifest(string Supplier, string Dock)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<Manifest> listOfManifest = new List<Manifest>();
            var QueryManifest = db.Query<Manifest>("RetrieveManifestFromDailyManifest", new Object[] { Supplier, Dock });
            listOfManifest = QueryManifest.ToList<Manifest>();
            ViewData["Manifest"] = listOfManifest;
            db.Close();
            return PartialView("DCLGridPopUpManifest", ViewData["Manifest"]);
            
        }

        

        /*public ActionResult RetrieveDataHeader(string DeliveryNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            mdl = Model.GetModel<EmergencyDCLOrderCreationModel>();

            mdl.Emergencys = db.Fetch<EmergencyDCLOrderCreationData>("GetDeliveryByDeliveryNo", new object[] { DeliveryNo });

            Model.AddModel(mdl);
            return PartialView("HeaderDCLOrderCreation", Model);

            

        }*/
        public ActionResult RetrieveRate()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<ComboRate>("GetRateByRoute", new object[] { Request.Params["Route"] });
            ViewData["ComboRate"] = QueryLog.ToList<ComboRate>();
            db.Close();
            return PartialView("PartialComboRate", ViewData["ComboRate"]);
        }

        public ActionResult RetrieveDeliveryContent(string ManifestNoContents, string DockCodeContent,
            string SupplierCodeContent, string SupplierNameContent, string ManifestNoPopup,
            string grlDockNoPopup, string grlSupplierPopup)
        {

            EmergencyDCLOrderCreationModel mdl = new EmergencyDCLOrderCreationModel();
            List<EmergencyGridContent> listEmergencyGridContent = new List<EmergencyGridContent>();
            EmergencyGridContent EmergencyContent = new EmergencyGridContent();
            EmergencyContent.ManifestNo = ManifestNoPopup;
            EmergencyContent.DockCode = grlDockNoPopup;
            EmergencyContent.SupplierCode = grlSupplierPopup==null?string.Empty:grlSupplierPopup;
            EmergencyContent.SupplierName = grlSupplierPopup == null ? string.Empty : getSupplierName(grlSupplierPopup);
            //EmergencyContent.Remark = RemarkContent == null ? string.Empty : RemarkContent;
            listEmergencyGridContent = gridContent(ManifestNoContents, DockCodeContent, SupplierCodeContent, SupplierNameContent
                );
            listEmergencyGridContent.Add(EmergencyContent);
            mdl.GridContent = listEmergencyGridContent;
            Model.AddModel(mdl);
            return PartialView("DCLEmergencyContent", Model);
        }

        public List<EmergencyGridContent> gridContent(string ManifestNoContents, string DockCodeContent,
            string SupplierCodeContent, string SupplierNameContent)
        {
            List<EmergencyGridContent> grids = new List<EmergencyGridContent>();
            if (!ManifestNoContents.Equals("")&&ManifestNoContents != null)
            {
                char[] splitchar = { ',' };
                string[] ManifestNo = ManifestNoContents.Split(splitchar);
                string[] DockCode = DockCodeContent.Split(splitchar);
                string[] SupplierCode = SupplierCodeContent.Split(splitchar);
                string[] SupplierName = SupplierNameContent.Split(splitchar);
                //string[] Remark = RemarkContent.Split(splitchar);
                

                for (int i = 0; i < ManifestNo.Length; i++)
                {
                    grids.Add(new EmergencyGridContent
                    {
                        ManifestNo = ManifestNo[i],
                        DockCode = DockCode[i],
                        SupplierCode = SupplierCode[i]=="null" ? string.Empty : SupplierCode[i],
                        SupplierName = SupplierName[i] == "null" || SupplierName[i].Equals("") ? string.Empty : SupplierName[i],
                        Remark = string.Empty
                        //Remark = Remark[i]=="null"||Remark[i].Equals("")?string.Empty : Remark[i]
                    });
                }
                
            }
            return grids;
        }

        public List<EmergencyDCLOrderCreationDataGrid> gridDataGrid(string SupplierCodePartial, string DockCodePartial, string ArrTimePartial, string DeptSuppTimePartial, string ArrPlanPartial, string DeptPlanPartial)
        {
            List<EmergencyDCLOrderCreationDataGrid> grids = new List<EmergencyDCLOrderCreationDataGrid>();
            if (!DockCodePartial.Equals("") && DockCodePartial != null)
            {
                char [] splitChar = { ',' };
                string [] SupplierCode = SupplierCodePartial.Split(splitChar);
                string [] DockCode = DockCodePartial.Split(splitChar);
                string [] ArrTime = ArrTimePartial.Split(splitChar);
                string[] DeptSuppTime = DeptSuppTimePartial.Split(splitChar);
                string[] ArrPlan = ArrPlanPartial.Split(splitChar);
                string[] DeptPlan = DeptPlanPartial.Split(splitChar);
                for (int i = 0; i < DockCode.Length; i++)
                {
                    grids.Add(new EmergencyDCLOrderCreationDataGrid
                        {
                            DockCode = DockCode[i],
                            SupplierCode = SupplierCode[i],
                            ArrTime = DateTime.Parse(ArrTime[i]),
                            DeptSuppTime = DateTime.Parse(DeptSuppTime[i]),
                            ArrPlan = DateTime.Parse(ArrPlan[i]),
                            DeptPlan = DateTime.Parse(DeptPlan[i])
                        });
                }
                
            }
            return grids;
        }

        //public JsonResult getDeliveryDetail(string Input, string Parameter)
        //{
        //    if (Parameter == "2")
        //    {
        //        Input = String.Format("{0:yyyy-dd-MM}", Convert.ToDateTime(Input));
        //    }
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    //string[] Data = new string[6];
        //    //EmergencyDCLHeader QueryLog = db.SingleOrDefault<EmergencyDCLHeader>("GetDeliveryDetailByDeliveryNo", new object[] { DeliveryNo });
        //    EmergencyDCLHeader QueryLog = db.SingleOrDefault<EmergencyDCLHeader>("GetDeliveryHeader", new object[] { Input,Parameter });
            
        //    //List<EmergencyDCLOrderCreationDataGrid> listGrid = new List<EmergencyDCLOrderCreationDataGrid>();
        //    //string DockAll = "";
        //    //string SupplierAll = "";
        //    //var QueryDetail = db.Fetch<EmergencyDCLOrderCreationDataGrid>("RetriveDeliveryDetail", new Object[] { DeliveryNo });
        //    //if (QueryDetail.Any())
        //    //{
        //    //    foreach (var s in QueryDetail)
        //    //    {
        //    //        DockAll += s.DockCode + ",";
                    
        //    //    }

        //    //}
        //    //Data[0] = QueryLog.DeliveryNo;
        //    //Data[1] = QueryLog.PickUpDate.ToString();
        //    //Data[2] = QueryLog.LPCode;
        //    //Data[3] = QueryLog.Route;
        //    //Data[4] = QueryLog.Rate;
        //    //Data[5] = QueryLog.Reason;

        //    //return Data;            
        //    //return r(QueryLog);
        //    //JsonResult jr = new JsonResult();
          
        //    return new JsonResult
        //    {
        //        Data = new
        //        {
        //            DeliveryNo = QueryLog.DELIVERY_NO,
        //            PickUpDate = QueryLog.PICKUP_DT.ToString("dd.MM.yyyy"),
        //            LPCode = QueryLog.LP_CD,
        //            Route = QueryLog.Route,
        //            Rate = QueryLog.Rate,
        //            Reason = QueryLog.DELIVERY_REASON
        //        }
        //    };
            
            
        //}

        public JsonResult getDeliveryDetail(string DeliveryNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //string[] Data = new string[6];
            EmergencyDCLHeader QueryLog = db.SingleOrDefault<EmergencyDCLHeader>("GetDeliveryDetailByDeliveryNo", new object[] { DeliveryNo });


            List<EmergencyDCLOrderCreationDataGrid> listGrid = new List<EmergencyDCLOrderCreationDataGrid>();
            string DockAll = string.Empty;
            string SupplierAll = string.Empty;
            string ArrTimeAll = string.Empty;
            string DeptSuppTimeAll = string.Empty;
            string ArrPlanAll = string.Empty;
            string DeptPlanAll = string.Empty;
          

            var QueryDetail = db.Fetch<EmergencyDCLOrderCreationDataGrid>("RetriveDeliveryDetail", new Object[] { DeliveryNo });
            if (QueryDetail.Any())
            {
                foreach (var s in QueryDetail)
                {
                    DockAll += s.DockCode + ",";
                    SupplierAll += s.SupplierCode + ",";
                    ArrTimeAll += string.Format("{0:dd.MM.yyyy HH:mm}",Convert.ToDateTime(s.ArrTime))  + ",";
                    DeptSuppTimeAll += string.Format("{0:dd.MM.yyyy HH:mm}", Convert.ToDateTime(s.DeptSuppTime)) + ",";
                    ArrPlanAll += string.Format("{0:dd.MM.yyyy HH:mm}", Convert.ToDateTime(s.ArrPlan)) + ",";
                    DeptPlanAll += string.Format("{0:dd.MM.yyyy HH:mm}", Convert.ToDateTime(s.DeptPlan)) + ",";
                }

            }

            db.Close();

            //Data[0] = QueryLog.DeliveryNo;
            //Data[1] = QueryLog.PickUpDate.ToString();
            //Data[2] = QueryLog.LPCode;
            //Data[3] = QueryLog.Route;
            //Data[4] = QueryLog.Rate;
            //Data[5] = QueryLog.Reason;

            //return Data;            
            //return Json(QueryLog);
            return new JsonResult
            {
                Data = new
                {
                    DeliveryNo = QueryLog.DELIVERY_NO,
                    PickUpDate = QueryLog.PICKUP_DT.ToString("dd.MM.yyyy"),
                    LPCode = QueryLog.LP_CD,
                    Route = QueryLog.Route,
                    Rate = QueryLog.Rate,
                    Reason = QueryLog.DELIVERY_REASON,
                    DockAll = DockAll,
                    SupplierAll = SupplierAll,
                    ArrTimeAll = ArrTimeAll,
                    DeptSuppTimeAll = DeptSuppTimeAll,
                    ArrPlanAll = ArrPlanAll,
                    DeptPlanAll = DeptPlanAll
                }
            };
        }
        public ActionResult GetDeliveryCombo(string PickupDate, string Route)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (PickupDate == "--" || PickupDate.Equals("--"))
            {
                PickupDate = "";
            }
            //PickupDate = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(PickupDate));
            List<ComboDelivery> listDelivery = new List<ComboDelivery>();
            var QueryDelivery = db.Query<ComboDelivery>("GetDeliveryHeader", new object[] {PickupDate, Route });
            listDelivery = QueryDelivery.ToList<ComboDelivery>();
            ComboDelivery itemNull = new ComboDelivery();
            itemNull.DeliveryNo = "";

            if (listDelivery.Count > 0)
            {
                listDelivery.Insert(0, itemNull);
            }
            ViewData["DeliveryNoData"] = listDelivery;
            db.Close();
            return PartialView("PartialComboDelivery", ViewData["DeliveryNoData"]);
        }
        
        //from table lookup
        public ActionResult RetrieveContentFromLookupManifest(string ManifestNoSelected, string DockCodeSelected, string SupplierCodeSelected,
            string SupplierNameSelected, string ManifestNoContents, string DockCodeContent,
            string SupplierCodeContent, string SupplierNameContent)
        {

            EmergencyDCLOrderCreationModel mdl = new EmergencyDCLOrderCreationModel();
            List<EmergencyGridContent> listEmergencyGridContent = new List<EmergencyGridContent>();
            //List<EmergencyGridContent> listEmergencyFromGrid = new List<EmergencyGridContent>();
            //List<EmergencyGridContent> listEmergencyGridManifest = new List<EmergencyGridContent>();
            
            EmergencyGridContent EmergencyContent = new EmergencyGridContent();
            listEmergencyGridContent = gridContent(ManifestNoContents, DockCodeContent, SupplierCodeContent, SupplierNameContent);
            
            //listEmergencyGridManifest = gridContent(ManifestNoSelected, DockCodeSelected, SupplierCodeSelected, SupplierNameSelected, RemarkSelected);
            //mdl.GridContent = listEmergencyGridContent;

            char[] splitchar = { ',' };
            string[] ManifestNo = ManifestNoSelected.Split(splitchar);
            string[] DockCode = DockCodeSelected.Split(splitchar);
            string[] SupplierCode = SupplierCodeSelected.Split(splitchar);
            string[] SupplierName = SupplierNameSelected.Split(splitchar);
            if (!ManifestNo[0].Equals(""))
            {
                for (int i = 0; i < ManifestNo.Length; i++)
                {
                    listEmergencyGridContent.Add(new EmergencyGridContent
                    {
                        ManifestNo = ManifestNo[i],
                        DockCode = DockCode[i],
                        SupplierCode = SupplierCode[i] == "null" ? string.Empty : SupplierCode[i],
                        SupplierName = SupplierName[i] == "null" || SupplierName[i].Equals("") ? string.Empty : SupplierName[i],
                        //Remark = string.Empty
                    });
                }
            }
            
            mdl.GridContent = listEmergencyGridContent;
            Model.AddModel(mdl);
            return PartialView("DCLEmergencyContent", Model);
        }

        //from clearItem
        public ActionResult ClearItemContent(string DockCodeAll, string SupplierCodeAll,string ManifestNoContents, string DockCodeContent, 
            string SupplierCodeContent, string SupplierNameContent) 
        {
            EmergencyDCLOrderCreationModel mdl = new EmergencyDCLOrderCreationModel();
            List<EmergencyGridContent> listGridContent = new List<EmergencyGridContent>();
            //List<EmergencyDCLOrderCreationDataGrid> listGridData = new List<EmergencyDCLOrderCreationDataGrid>();
            EmergencyGridContent gridContents = new EmergencyGridContent();

            listGridContent = gridContent(ManifestNoContents, DockCodeContent, SupplierCodeContent, SupplierNameContent);
            //string ManifestNoDeleted = string.Empty;
            //List<EmergencyGridContent> gridsDeletedContentAll = new List<EmergencyGridContent>();
            if (!ManifestNoContents.Equals("") && ManifestNoContents != null)
            {
                if (!DockCodeAll.Equals("") && DockCodeAll != null)
                {
                    char[] splitChar = { ',' };
                    string[] DockCode = DockCodeAll.Split(splitChar);
                    string[] SupplierCode = SupplierCodeAll.Split(splitChar);
                    for (int i = 0; i < DockCode.Length; i++)
                    {
                        List<EmergencyGridContent> gridsDeletedContent = new List<EmergencyGridContent>();
                        gridsDeletedContent = GetDataContentByDockCode(DockCode[i], listGridContent, SupplierCode[i]);

                        foreach (EmergencyGridContent eg in gridsDeletedContent)
                        {
                            listGridContent.Remove(eg);
                        }

                    }
                }
            }
            
           
            mdl.GridContent = listGridContent;
            Model.AddModel(mdl);
            return PartialView("DCLEmergencyContent", Model);
        }

        public ActionResult DeleteManifest(string ManifestNoContents, string DockCodeContent,
            string SupplierCodeContent, string SupplierNameContent, string RemarkContent)
        {
            EmergencyDCLOrderCreationModel mdl = new EmergencyDCLOrderCreationModel();
            mdl.GridContent = gridContent(ManifestNoContents, DockCodeContent, SupplierCodeContent, SupplierNameContent);
            Model.AddModel(mdl);
            return PartialView("DCLEmergencyContent", Model);
        }

        public ActionResult DCLEmergencyContent()
        {
            EmergencyDCLOrderCreationModel mdl = new EmergencyDCLOrderCreationModel();
            mdl.GridContent = new List<EmergencyGridContent>();
            Model.AddModel(mdl);
            return PartialView("DCLEmergencyContent", Model);
        }
        public ActionResult DCLEmergencyPartial()
        {
            EmergencyDCLOrderCreationModel mdl = new EmergencyDCLOrderCreationModel();
            for (int i = 1; i <= 20; i++)
            {
                mdl.EmergencysGrid.Add(new EmergencyDCLOrderCreationDataGrid()
                {
                });

            }
            
            Model.AddModel(mdl);

            return PartialView("DCLEmergencyPartial", Model);
        }

        public ContentResult SubmitData(string Type, string DeliveryNo, string PickUpDate, string LogisticPartner,
            string Route, string Rate, string Reason,
            string SupplierCodePartial, string DockCodePartial, string ArrTimePartial, string DeptSuppTimePartial,
            string ArrPlanPartial, string DeptPlanPartial,
            string ManifestNoContents, string DockCodeContent, string SupplierCodeContent, string SupplierNameContent)
        {
            
            string result = string.Empty;
            

            string delivery_No = string.Empty;
            string delivery_type = string.Empty;

            #region TB_R_DELIVERY_CTL_HEADER
            Int32 revise_no;
            Nullable<DateTime> pickup_dt;
            string route = string.Empty;
            string rate = string.Empty;
            string lp_cd = string.Empty;
            string driver_name = string.Empty;
            string download_by = string.Empty;
            Nullable<DateTime> download_dt;
            string download_path = string.Empty;
            string requisitioner_by = string.Empty;
            Nullable<DateTime> requisitioner_dt;
            string confirm_sts = string.Empty;
            string delivery_sts = string.Empty;
            string notify_id = string.Empty;
            string push_to_web_flag = string.Empty;
            string cancel_flag = string.Empty;
            string delivery_reason = string.Empty;
            string invoice_by = string.Empty;
            Nullable<DateTime> invoice_dt;
            string created_by = string.Empty;
            Nullable<DateTime> created_dt;
            string changed_by = string.Empty;
            Nullable<DateTime> changed_dt;
            string delivery_achievement_sta = string.Empty;
            string approve_by = string.Empty;
            Nullable<DateTime> approve_dt;
            string notify_sts = string.Empty;
            #endregion

            
            #region TB_R_DELIVERY_CTL_DETAIL
            string dock_cd = string.Empty; 
            string station = string.Empty;
            Nullable<DateTime> arrival_gate_dt;
            Nullable<DateTime> arrival_plan_dt;
            Nullable<DateTime> departure_plan_dt;
            string arrival_status = string.Empty;
            Nullable<DateTime> arrival_gap;
            Nullable<DateTime> arrival_actual_dt;
            Nullable<DateTime> departure_actual_dt;
            string departure_status = string.Empty;
            Nullable<DateTime> departure_gap;
            Nullable<DateTime> remaining;

            string supplier_plant_cd = string.Empty;
            #endregion
                
            #region TB_R_DELIVERY_CTL_ROUTE
            string supplier_cd = string.Empty;
            //DateTime arrival_plan_dt;
            //string departure_plan_dt
            //string arrival_actual_dt
            //string departure_actual_dt
            Nullable<DateTime> arrival_plan_dt_supp;
            Nullable<DateTime> departure_plan_dt_supp;
            string manifest_no = string.Empty;
            string manifest_type = string.Empty;
            #endregion
            

           
            

            #region Umum
            if (Type.Equals("Additional Pickup") || Type == "Additional Pickup")
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                var QueryAdditonalDelivery = db.Query<getDelivery>("GetDeliveryNoAdditional");
                db.Close();
                foreach (var item in QueryAdditonalDelivery)
                {
                    delivery_No = item.DeliveryAdditional;
                }
                delivery_type = "A";
                result = delivery_No;
            }
            else
            {
                delivery_No = DeliveryNo;
                delivery_type = "R";
                result = DeliveryNo;
            }
            
            
            created_by = AuthorizedUser.Username;
            created_dt = DateTime.Now;
            changed_by = AuthorizedUser.Username;
            changed_dt = DateTime.Now;
            #endregion

            //convert to list
            List<EmergencyDCLOrderCreationDataGrid> listDataGrid = new List<EmergencyDCLOrderCreationDataGrid>();
            List<EmergencyGridContent> listGridContent = new List<EmergencyGridContent>();

            listDataGrid = gridDataGrid(SupplierCodePartial, DockCodePartial, ArrTimePartial, DeptSuppTimePartial, ArrPlanPartial, DeptPlanPartial);
            listGridContent = gridContent(ManifestNoContents, DockCodeContent, SupplierCodeContent, SupplierNameContent);


            try
            {
                TimeSpan ts = new TimeSpan(0, 30, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                {
                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    if (Type.Equals("Additional Pickup") || Type == "Additional Pickup")
                    {
                        revise_no = 1;
                        pickup_dt = PickUpDate == "" ? pickup_dt = null : Convert.ToDateTime(PickUpDate);

                        route = Route;
                        rate = Rate;
                        lp_cd = LogisticPartner;
                        driver_name = string.Empty;
                        download_by = string.Empty;
                        download_dt = null;
                        download_path = string.Empty;
                        requisitioner_by = string.Empty;
                        requisitioner_dt = null;
                        confirm_sts = "0";
                        delivery_sts = "Not Yet Approved";
                        notify_id = string.Empty;
                        push_to_web_flag = "1";
                        cancel_flag = string.Empty;
                        delivery_reason = Reason;
                        invoice_by = string.Empty;
                        invoice_dt = null;
                        delivery_achievement_sta = string.Empty;
                        approve_by = string.Empty;
                        approve_dt = null;
                        notify_sts = string.Empty;

                        string supplier_name = string.Empty;
                        //execute TB_R_DELIVERY_CTL_H
                        db.Execute("InsertDeliveryHeader", new object[] { delivery_No, delivery_type, revise_no, pickup_dt,
                    route, rate, lp_cd,driver_name,download_by,download_dt,download_path,requisitioner_by,requisitioner_dt,confirm_sts,delivery_sts,notify_id,
                    push_to_web_flag,cancel_flag,delivery_reason,invoice_by,invoice_dt,created_by,created_dt,changed_by,changed_dt,delivery_achievement_sta,
                    approve_by,approve_dt,notify_sts});
                        for (int i = 0; i < listDataGrid.Count; i++)
                        {

                            dock_cd = listDataGrid[i].DockCode;
                            station = "01";
                            arrival_gate_dt = null;
                            arrival_plan_dt = listDataGrid[i].ArrTime.Equals("") ? DateTime.MinValue : listDataGrid[i].ArrTime;
                            departure_plan_dt = listDataGrid[i].DeptSuppTime.Equals("") ? DateTime.MinValue : listDataGrid[i].DeptSuppTime;
                            arrival_plan_dt_supp = listDataGrid[i].ArrPlan;
                            departure_plan_dt_supp = listDataGrid[i].DeptPlan;
                            arrival_status = "0";
                            arrival_gap = null;
                            arrival_actual_dt = null;
                            departure_actual_dt = null;
                            departure_status = "0";
                            departure_gap = null;
                            remaining = null;



                            supplier_cd = listDataGrid[i].SupplierCode;
                            supplier_name = getSupplierName(supplier_cd);
                            supplier_plant_cd = getSupplierPlantCode(supplier_cd);
                            List<EmergencyGridContent> gridContentFilter = new List<EmergencyGridContent>();

                            //execute TB_R_DELIVERY_CTL_D
                            db.Execute("InsertDeliveryDaily", new object[] { delivery_No, dock_cd,station,arrival_gate_dt,arrival_plan_dt, departure_plan_dt, 
                                arrival_status,arrival_gap,arrival_actual_dt,departure_actual_dt,departure_status,departure_gap,remaining,
                                created_by, created_dt, changed_by,changed_dt
                            });
                            //Execute TB_R_DELIVERY_CTL_ROUTE
                            db.Execute("InsertDeliveryRoute", new object[]{delivery_No, supplier_cd, arrival_plan_dt_supp, departure_plan_dt_supp, arrival_actual_dt, departure_actual_dt,
                            created_by,created_dt,changed_by,changed_dt,supplier_plant_cd});

                            //Execute TB_R_DELIVERY_CTL_SUPPLIERDOCK
                           // 
                            db.Execute("InsertSupplierDock", new object[] { delivery_No, supplier_cd, supplier_name, dock_cd, route,rate,arrival_plan_dt_supp, departure_plan_dt_supp, lp_cd, created_by, created_dt, changed_by, changed_dt });
                            gridContentFilter = GetDataContentByDockCode(dock_cd, listGridContent, supplier_cd);
                            //InsertManifest(gridContentFilter, ref delivery_No, dock_cd, station, created_by, created_dt, changed_by, changed_dt);

                           
                            foreach (EmergencyGridContent s in gridContentFilter)
                            {
                                manifest_no = s.ManifestNo;
                                manifest_type = "R";
                                //Execute TB_R_DELIVERY_CTL_MANIFEST  
                                db.Execute("InsertDeliveryManifest", new object[]{delivery_No, dock_cd, station, manifest_no, manifest_type,
                                    created_by,created_dt,changed_by,changed_dt});
                                //update delivery No
                                db.Execute("UpdateDeliveryNoSystem", new object[] { });
                                var QueryAdditonalDelivery = db.Query<getDelivery>("GetDeliveryNoAdditional");
                                foreach (var item in QueryAdditonalDelivery)
                                {
                                    delivery_No = item.DeliveryAdditional;
                                }
                            }
                        } 
                    }
                    else if (Type.Equals("Regular Pickup") || Type == "Regular Pickup")
                    {
                        
                        //update header
                        delivery_reason = Reason;
                        db.Execute("UpdateReviseNoDelivery", new object[] { delivery_No, delivery_reason, changed_by, changed_dt });
                        
                        for (int i = 0; i < listDataGrid.Count; i++)
                        {
                            List<EmergencyGridContent> gridContentFilter = new List<EmergencyGridContent>();
                            dock_cd = listDataGrid[i].DockCode;
                            supplier_cd = listDataGrid[i].SupplierCode;
                            station = "01";
                            //update TB_R_DELIVERY_CTL_D
                            arrival_plan_dt = listDataGrid[i].ArrTime.Equals("") ? DateTime.MinValue : listDataGrid[i].ArrTime;
                            departure_plan_dt = listDataGrid[i].DeptSuppTime.Equals("") ? DateTime.MinValue : listDataGrid[i].DeptSuppTime;
                            dock_cd = listDataGrid[i].DockCode;
                            //supplier_cd = listDataGrid[i].SupplierCode;


                            //db.Execute("UpdatePlanDelivery", new object[] { delivery_No, arrival_plan_dt, departure_plan_dt, dock_cd, changed_by, changed_dt });

                            gridContentFilter = GetDataContentByDockCode(dock_cd, listGridContent, supplier_cd);
                            foreach (EmergencyGridContent s in gridContentFilter)
                            {
                                manifest_no = s.ManifestNo;
                                manifest_type = "R";
                                //Execute TB_R_DELIVERY_CTL_MANIFEST  
                                db.Execute("InsertDeliveryManifest", new object[]{delivery_No, dock_cd, station, manifest_no, manifest_type,
                                created_by,created_dt,changed_by,changed_dt});
                                
                                //update delivery No-not updaed because not adding
                                //db.Execute("UpdateDeliveryNoSystem", new Object[] { });
                                //var QueryAdditonalDelivery = db.Query<getDelivery>("GetDeliveryNoAdditional");
                                //foreach (var item in QueryAdditonalDelivery)
                                //{
                                //    delivery_No = item.DeliveryAdditional;
                                //}
                            }
                        }
                    }


                    //update delivery No
                    db.Execute("UpdateDeliveryNoSystem", new Object[] { });
                    //var QueryAdditonalDelivery = db.Query<getDelivery>("GetDeliveryNoAdditional");
                    //foreach (var item in QueryAdditonalDelivery)
                    //{
                    //    delivery_No = item.DeliveryAdditional;
                    //}

                    scope.Complete();
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                //ex.Message.;
                //return Content(ex.Message.ToString());
                throw ex;
            }
            finally {
                Dispose();
            }
            
            return Content(result.ToString());
        }
        public string getSupplierName(string SupplierCode)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var Query = db.Query<Supplier>("GetSupplierNamebySupplierCode", new object[] { SupplierCode });
            List<Supplier> listSupplier = new List<Supplier>();
            listSupplier = Query.ToList<Supplier>();
            result = listSupplier[0].SupplierName;
            db.Close();
            return result;
        }
        public List<EmergencyGridContent> GetDataContentByDockCode(string DockCodeCompare, List<EmergencyGridContent> gridsAll,string SupplierCode)
        {
            List<EmergencyGridContent> listGrid = new List<EmergencyGridContent>();
            var gridV = from p in gridsAll
                        where p.DockCode == DockCodeCompare && p.SupplierCode == SupplierCode
                        select p;
            listGrid = gridV.ToList();
                        
            

            //listLP = QueryLog.ToList<LogisticPartner>();
            return listGrid;
        }

        public string getSupplierPlantCode(string SupplierCode) 
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = string.Empty;
            var QuerySupplierPlant = db.Query<getSupplierPlant>("RetrieveSupplierPlantCode", new object[]{SupplierCode});
            db.Close();
            foreach (var item in QuerySupplierPlant)
            {
                result = item.SupplierPlantCD;
            }
            return result;
        }

      
    }
    class getDelivery
    {
        public string DeliveryAdditional{set;get;}
    }
    class getSupplierPlant 
    {
        public string SupplierPlantCD { set; get; }
    }
}
