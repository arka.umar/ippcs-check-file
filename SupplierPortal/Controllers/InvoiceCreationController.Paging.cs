﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Portal.Models.Dock;
using Portal.Models.Globals;
using Portal.Models.InvoiceCreation;
using Portal.Models.Supplier;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Task;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using ServiceStack.Text;

namespace Portal.Controllers
{
    public partial class InvoiceCreationController : BaseController
    {
        #region PagingStub
        protected void bindGridViewModel()
        {
            InvoiceCreationModel mdl = Model.GetModel<InvoiceCreationModel>();
            mdl.Gridviewmodel = GridViewExtension.GetViewModel("grdData");
            if (mdl.Gridviewmodel == null)
            {
                mdl.Gridviewmodel = mdl.GetGridViewModel();
                decorateModelBindingCustom(mdl.Gridviewmodel);
            }
        }
        #endregion 



        public ActionResult Reload(string manifests)
        {
            InvoiceCreationModel m = Model.GetModel<InvoiceCreationModel>();
            if (m == null)
            {
                
                m = new InvoiceCreationModel();
                Model.AddModel(m);
            }
            
            m.Gridviewmodel.FilterExpression = "";
            decorateModelBindingCustom(m.Gridviewmodel);
                        
            return PartialView("InvoiceCreationPartial", Model);
        }

        private void decorateModelBindingCustom(GridViewModel gvmodel)
        {
            gvmodel.ProcessCustomBinding(
                GetDataRowCountAdvanced,
                GetDataAdvanced
            );
        }

        public void GetDataRowCountAdvanced(GridViewCustomBindingGetDataRowCountArgs e)
        {
            InvoiceCreationModel m = Model.GetModel<InvoiceCreationModel>();

            //int rowCount = DB.SingleOrDefault<int>("GetAllInvoiceCreationCount",
            //    new object[] { 
            //        m.Filter.ValueOf("SupplierCode",""), 
            //        m.Filter.ValueOf("DockCode",""), 
            //        m.Filter.ValueOf("SupplierManifest",""), 
            //        "", 
            //        m.Filter.ValueOf("DateFrom", ""), 
            //        m.Filter.ValueOf("DateTo", "")});
            // tix.Say("GetDataRowCountAdvanced GetAllInvoiceCreationCount = {0}", rowCount);
            //e.DataRowCount = rowCount;
        }

        public void GetDataAdvanced(GridViewCustomBindingGetDataArgs e)
        {
            InvoiceCreationModel m = Model.GetModel<InvoiceCreationModel>();
            int iPage = e.State.Pager.PageIndex;
            int iPages = e.State.Pager.PageSize;

            //List<InvoiceCreationDetail> l = DB.Fetch<InvoiceCreationDetail>(
            //    "GetAllInvoiceCreationPaging",
            //    new object[] { 
            //        m.Filter.ValueOf("SupplierCode",""), 
            //        m.Filter.ValueOf("DockCode",""), 
            //        m.Filter.ValueOf("SupplierManifest",""), 
            //        "", 
            //        m.Filter.ValueOf("DateFrom", ""), 
            //        m.Filter.ValueOf("DateTo", ""),
            //        e.StartDataRowIndex + 1, e.StartDataRowIndex + 1 + e.DataRowCount });

            //m.InvoiceCreationDetails = l;

            //e.Data = l;
        }

        #region trial functions

        private void FauxInit(InvoiceCreationModel m)
        {
            List<InvoiceCreation> lst = new List<InvoiceCreation>();
            for (int i = 1; i <= 100; i++)
            {

                lst.Add(new InvoiceCreation()
                {
                    SupplierManifest = "4001786065" + i,
                    PONo = "450035918" + RandomNumber(1, 9),
                    Currency = "IDR",
                    Amount = i * 1000000,
                    OrderNo = String.Format("2011100{0}{0}{0}", RandomNumber(1, 9)),
                    DockCode = "44",
                    SuppCode = String.Format("502{0}", RandomNumber(1, 9)),
                    SuppName = "Supplier01",
                    DocumentDate = DateTime.Now,
                    InvoiceNo = "INV000" + (100 + i),
                    InvoiceTaxType = SetInvoiceTaxType(i)
                });
                for (int j = 1; j < 25; j++)
                {
                    m.InvoiceCreationDetails.Add(new InvoiceCreationDetail()
                    {
                        MaterialNumber = "426110KF6000",
                        PackingType = "" + RandomNumber(1, 9),
                        SuffixColour = "X",
                        MaterialDescription = "WHEEL, DISC" + RandomNumber(1, 9),
                        SupplierManifest = "4001786065" + i,
                        DocumentDate = DateTime.Now,
                        DockCode = "4A" + RandomNumber(1, 9),
                        ICSQty = RandomNumber(1, 9),
                        PartPrice = 1298000,
                        Currency = "IDR",
                        Curr = 1,
                        Amount = 1560000
                    });
                }
            }

            m.InvoiceCreationLists = lst;
        }

        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        #endregion trial functions


        public ActionResult GridViewSortingAction(GridViewColumnState column, bool reset)
        {
            var viewModel = GridViewExtension.GetViewModel("grdData");
            
            viewModel.SortBy(column, reset);
            return GridCustomActionCore(viewModel);
        }

    
        public ActionResult GridViewPagingAction(GridViewPagerState pager)
        {
            
            var viewModel = GridViewExtension.GetViewModel("grdData");
            
            Model.GetModel<InvoiceCreationModel>().SetGridViewModel(viewModel);
            viewModel.Pager.Assign(pager);

            return GridCustomActionCore(viewModel);
        }

        public ActionResult GridCustomActionCore(GridViewModel gridViewModel)
        {
            InvoiceCreationModel m = new InvoiceCreationModel();

            Model.AddModel(m);

            m.SetGridViewModel(gridViewModel);
            decorateModelBindingCustom(gridViewModel);

            return PartialView("InvoiceCreationPartial", Model);
        }
    }
}
