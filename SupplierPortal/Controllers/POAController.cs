﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Compression;
using DevExpress.Web.ASPxUploadControl;
using Portal.Models.POA;

using Toyota.Common.Web.Html;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Configuration;
using System.Web.UI.WebControls;
using System.Net;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.Notification;


//created By niit.Hendra wahyudi 05/12/2012 

namespace Portal.Controllers
{

    public class POAController : BaseController
    {
        //
        // GET: /POA/

     public  POAController()
            : base("Power Of Attorney")
        { 
        
        } 

     protected override void Init()
     {
         
         /*
         IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
         POAlist listTemp = new POAlist();
         IEnumerable<POAmaster> result = db.Query<POAmaster>("SelectPOA");

         listTemp.POAlistmodel = result.ToList<POAmaster>();
         Model.AddModel(listTemp);  */
         ViewData["getAtt"] = GetGroups();
         ViewData["getAttId"] = GetAttId(UserID);
         ViewData["getUser"] = UserID;

         POAlist mdl = new POAlist();
         mdl.POAlistmodel = POAget();
         Model.AddModel(mdl);

       
     }


     public ActionResult ReloadGrid(string POAno, string Grantor, string ATTORNEY2)
     {

         IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
         POAlist model = Model.GetModel<POAlist>();
         model.POAlistmodel = db.Fetch<POAmaster>("SelectPOA", new object[] { POAno, Grantor, ATTORNEY2 });
         db.Close();
         return PartialView("POApartial", Model);

        // AuthorizedUser.Username

     }



     protected List<POAmaster> POAget()
     {
         IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
         List<POAmaster> POAListAdd = new List<POAmaster>();
         var QueryLog = db.Query<POAmaster>("SelectPOA", new object[] { "","","" });

         if (QueryLog.Any())
         {
             foreach (var q in QueryLog)
             {
                 POAListAdd.Add(new POAmaster()
                 {
                    POA_NUMBER = q.POA_NUMBER,
                    GRANTOR  = q.POA_NUMBER,
                    ATTORNEY = q.ATTORNEY,
                    REASON = q.REASON,
                    OFFICE_STATUS = q.OFFICE_STATUS,
                    CREATED_BY = q.CREATED_BY,
                    CREATED_DT = q.CREATED_DT,
                    CHANGED_BY = q.CHANGED_BY,
                    CHANGED_DT = q.CHANGED_DT
                 });
             }
         }
         db.Close();
         return POAListAdd;
     }



     protected override void StartUp()
     {

     }

     public ActionResult POApartial()
     {
       
         return PartialView("POApartial", Model);
     }
       
     [HttpPost, ValidateInput(false)]
     public ActionResult POApartialNew([ModelBinder(typeof(DevExpressEditorsBinder))]  POAmaster POAadd)
     {
        IDBContext dbContext;
        dbContext = ProviderResolver.GetInstance().Get<IDBContextManager>().GetContext(DBContextNames.DB_PCS); 
        if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Execute("InsertPOA", new object[] { POAadd.POA_NUMBER, POAadd.GRANTOR, POAadd.ATTORNEY, POAadd.REASON, POAadd.OFFICE_STATUS, AuthorizedUser.Username });
                } 
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                } 
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

        dbContext.Close();
        return PartialView("POApartial",Model);
     }

     public ActionResult UpdateCallBackPanel()
     {
      
         return PartialView("PopUpCallBackPartial");
     }

     public ActionResult PartialHeaderPOA()
     {
         return PartialView("POAHeader", Model);
     }

     public IEnumerable<POAGet> GetGroups()
     {
         IDBContext db = ProviderResolver.GetInstance().Get<IDBContextManager>().GetContext(DBContextNames.DB_PCS);
         IEnumerable<POAGet> val = db.Query<POAGet>("GetRetrievalAttorney");
         db.Close();
         return val;
     }

     public IEnumerable<POAGetID> GetAttId(string Username)
     {
         IDBContext db = ProviderResolver.GetInstance().Get<IDBContextManager>().GetContext(DBContextNames.DB_TMMIN_ROLE);
         IEnumerable<POAGetID> val = db.Query<POAGetID>("GetRetrievalAttByID",new object[]{ Username });
         db.Close();
         return val;
     }

     public void POAAdd(string POA_NUMBER, string GRANTOR, string ATTORNEY, string REASON, string OFFICE_STATUS)
     {
        IDBContext dbContext;
        dbContext = ProviderResolver.GetInstance().Get<IDBContextManager>().GetContext(DBContextNames.DB_PCS);  
        dbContext.Execute("InsertPOA", new object[] { POA_NUMBER, GRANTOR, ATTORNEY, REASON, OFFICE_STATUS, AuthorizedUser.Username});
        dbContext.Close();

     }

     public void POAEdit(string POA_NUMBER, string GRANTOR, string ATTORNEY, string REASON, string OFFICE_STATUS)
     {
         IDBContext dbContext;
         dbContext = ProviderResolver.GetInstance().Get<IDBContextManager>().GetContext(DBContextNames.DB_PCS); 
         dbContext.Execute("UpdatePOA", new object[] { POA_NUMBER, GRANTOR, ATTORNEY, REASON, OFFICE_STATUS, AuthorizedUser.Username });
         dbContext.Close();

     }

     public void POADelete(string POA_NUMBER)
     {
         IDBContext dbContext;
         dbContext = ProviderResolver.GetInstance().Get<IDBContextManager>().GetContext(DBContextNames.DB_PCS); 
         dbContext.Execute("DeletePOA", new object[] { POA_NUMBER });
         dbContext.Close();

     }

    }
}
