﻿using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Media;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;
using Portal.Models;
using Portal.Models.PO;
using Portal.Models.ScanGate;
using Toyota.Common.Web.Credential;
using System.Xml;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Security.Principal;
using System.Net.Sockets;
using Cryptography;
using System.Configuration;
using System.Xml.Serialization;
using System;
using System.Linq;
using System.Collections.Generic;
namespace Portal.Controllers
{
      [SkipUnlockController]
    public class ScanGateController : BaseController
    {
        //
        // GET: /ScanGet/

          public ScanGateController()
              : base("Scan Get")
          {
          }
          
       
          protected override void StartUp()
          {

          }

          protected override void Init()
          {
            ViewData["ScanGate"] = GetScanGate();
          }

        public string GetScanGate()
        {
            string scan_gate = "";
            List<ScanGate> ScanGates = Model.GetModel<User>().FilteringArea<ScanGate>(
                    GetAllScanGate(), "SCAN_GATE", "ScanGate", "txtGate");
            //TempData["GridName"] = "txtGate";
            //Model.AddModel(ListSupplier);

            if (ScanGates != null)
            {
                if (ScanGates.Count == 1)
                {
                    scan_gate = ScanGates[0].SCAN_GATE;
                }
            }

            return scan_gate;
            //return PartialView("GridLookup/PartialGrid", ListSupplier);
        }

        private List<ScanGate> GetAllScanGate()
        {
            List<ScanGate> ListSupplier = new List<ScanGate>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ListSupplier = db.Fetch<ScanGate>("GetScanGateList", new object[] { });

            return ListSupplier;
        }

        public ActionResult PartialDetailRecevingMores(string DELIVERY_NO)
          {
              ScanGateReceivingDB ScanDetail = new ScanGateReceivingDB();
              ScanDetail.STATUS_IN_IMAGE = "~/Content/Images/defaultScanGate.jpg";//ARRIVAL_STATUS_IMAGE = "~/Content/Images/defaultScanGate.jpg";
              ScanDetail.STATUS_OUT_IMAGE = "~/Content/Images/defaultScanGate.jpg"; //DEPARTURE_STATUS_IMAGE = "~/Content/Images/defaultScanGate.jpg";
              //ScanDetail.ARRIVAL_STATUS = "~/Content/Images/defaultScanGate.jpg";
              //ScanDetail.DEPARTURE_STATUS = "~/Content/Images/defaultScanGate.jpg";
              return PartialView("ScanGateTruckArrivalPartial", ScanDetail);
          }

          public string CheckDeliveryData(string txtDeliveryNo, string txtGATE)
          {
              IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
              string ResultQuery = "";
              try
              {
                  ResultQuery = db.ExecuteScalar<string>("CheckDeliveryDataScanGate", new object[] { txtDeliveryNo, txtGATE });
              }
              catch (Exception ex)
              {
                  ResultQuery = "Error: " + ex.Message;
              }
              db.Close();
              return (ResultQuery == null ? " " : ResultQuery);
          }

          [HttpPost]
          public ActionResult ModifReceivingData(string txtDeliveryNo,string txtGATE)
          {
              ScanGateReceivingDB QueryLogView = GetReceiving(txtDeliveryNo, txtGATE);

              return PartialView("ScanGateTruckArrivalPartial", QueryLogView);
          }
          //protected string ModifReceiving(string DELIVERY_NO)
          //{
          //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

          //    string QueryLog = db.ExecuteScalar<string>("GetScanGateModif", new object[] { DELIVERY_NO, AuthorizedUser.Username });
          //    db.Close();
          //    return QueryLog;
          //}

          protected ScanGateReceivingDB GetReceiving(string DELIVERY_NO, string txtGATE)
          {
              IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
              //dynamic args = new { DELIVERYNO = DELIVERY_NO };
              ScanGateReceivingDB QueryLog = db.Fetch<ScanGateReceivingDB>("UpdateSelectScanGate", new object[] { DELIVERY_NO, txtGATE}).FirstOrDefault();

              // Added by WOT, 27/05/2015 [start]
              db.Fetch<ScanGateReceivingDB>("SP_TC_InsertDeliveryInfoTrack", new object[] { AuthorizedUser.Username, txtGATE, DELIVERY_NO }).FirstOrDefault();

              string plantFlg = db.Fetch<string>("GetPlantGateMapping", new object[] { txtGATE, DELIVERY_NO }).FirstOrDefault();

              if (plantFlg != null)
              {
                  ViewData["viewTruckInfo"] = "true";

                  string plantCd = db.Fetch<string>("GetPlantCd", new object[] { DELIVERY_NO }).FirstOrDefault();

                  ViewData["viewPlantInfo"] = plantCd;
              }
              else
              {
                  ViewData["viewTruckInfo"] = "false";
              }
              // Added by WOT, 27/05/2015 [end]

              db.Close();
              return QueryLog;
          }

    }
}
