﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.SupplierInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;

using Portal.Models.SupplierFeedbackSummary;

namespace Portal.Controllers
{
    public class SupplierProfileController : BaseController
    {
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        /// <summary>
        /// If the current response is a FileResult (an MVC base class for files) then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }

        public SupplierProfileController()
            : base("Supplier Profile ")
        {
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        protected override void StartUp()
        {

        }
        List<SupplierICS> _supplierModel = null;
        private List<SupplierICS> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierICS>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void Init()
        {
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");
            ViewData["SupplierGridLookup"] = suppliers;
            ViewData["CurrencyGridLookup"] = GetAllCurrency();
            ViewData["CurrencyGridLookupSH"] = GetAllCurrency();
            ViewData["InvolveGridLookup"] = GetAllInvolve();
            ViewData["LocationGridLookup"] = GetAllLocationFunction();
            ViewData["SupPosGridLookup"] = GetAllSupplierPosition();
            ViewData["SupCountryGridLookupSH"] = GetAllCountry();
            ViewData["SupCountryGridLookupPP"] = GetAllCountry();
            ViewData["SupAreaGridLookup"] = GetAllSupplierArea();
            ViewData["SupplierInvoiceTaxTypeGridLookupFinan"] = GetAllInvoiceTaxType();
            ViewData["CustCategoryGridLookupCus"] = GetAllCustomerCategory();
            ViewData["SupplierInvoiceTaxTypeGridLookupFinan"] = GetAllInvoiceTaxType();
            ViewData["CurrencyGridLookupFinancial"] = GetAllCurrency();
            ViewData["SupSupplyTypeGridLookup"] = GetAllSupplyType();
            ViewData["SupTierGridLookup"] = GetAllTier();
            ViewData["AutomotiveGridLookup"] = GetAllAutomotive();
            ViewData["SupCountryGridLookupExport"] = GetAllCountry();
            ViewData["UnionCombo"] = GetAllUnion();

            SupplierModel mdl = new SupplierModel();
            mdl.SupplierAddresss = new List<SupplierAddress>();
            mdl.SupplierAwards = new List<SupplierAward>();
            mdl.SupplierBusinessRelations = new List<SupplierBusinessRelation>();
            mdl.SupplierCADITs = new List<SupplierCADIT>();
            mdl.SupplierContactPersons = new List<SupplierContactPerson>();
            mdl.SupplierCustomers = new List<SupplierCustomer>();
            mdl.SupplierEquipments = new List<SupplierEquipment>();
            mdl.SupplierExports = new List<SupplierExport>();
            mdl.SupplierFinancials = new List<SupplierFinancial>();
            mdl.SupplierGenerals = new List<SupplierGeneral>();
            mdl.SupplierLabors = new List<SupplierLabor>();
            mdl.SupplierManPowers = new List<SupplierManPower>();
            mdl.SupplierProducts = new List<SupplierProduct>();
            mdl.SupplierPurchasedParts = new List<SupplierPurchasedPart>();
            mdl.SupplierShareHolders = new List<SupplierShareHolder>();
            Model.AddModel(mdl);
        }

        public ContentResult UpdateConfirmation(string SupplierCode, string ItemNo, string PartName, string ActiveTab)
        {
            IDBContext db = DbContext;
            string messageResult = "";
            try
            {
                string[] supplierCodes = null;
                string[] itemNos = null;
                string[] partNames = null;

                string supp_cd = "";
                string item_no = "";
                string part_name = "";

                if (SupplierCode != "") supplierCodes = SupplierCode.Split(',');
                if (ItemNo != "") itemNos = ItemNo.Split(',');
                if (PartName != "") partNames = PartName.Split(',');

                for (int i = 0; i < supplierCodes.Length; i++)
                {
                    supp_cd = supplierCodes != null ? supplierCodes[i] : "";
                    item_no = itemNos != null ? itemNos[i] : "";
                    part_name = partNames != null ? partNames[i] : "";

                    db.Execute("UpdateConfirmationSupplierProfile",
                            new object[]
                            {
                                ActiveTab, supp_cd, item_no, part_name, AuthorizedUser.Username
                            }
                        );

                    //if (SupplierCode != "" && ItemNo == "" && PartNo == "" && Year == "")
                    //{
                    //    db.ExecuteScalar<string>("UpdateConfirmationSupplierProfile",
                    //        new object[] 
                    //        { 
                    //            ActiveTab, supplierCodes[i].ToString(), "", "", "", AuthorizedUser.Username, DateTime.Now 
                    //        }
                    //    );
                    //}
                    //else if (SupplierCode != "" && ItemNo != "" && PartNo == "" && Year == "")
                    //{
                    //    db.Execute("UpdateConfirmationSupplierProfile", new object[] { ActiveTab, supplierCodes[i].ToString(), itemNos[i], "", "", AuthorizedUser.Username, DateTime.Now });
                    //}
                    //else if (SupplierCode != "" && ItemNo != "" && PartNo == "" && Year != "")
                    //{
                    //    db.Execute("UpdateConfirmationSupplierProfile", new object[] { ActiveTab, supplierCodes[i].ToString(), itemNos[i], "", years[i].ToString(), AuthorizedUser.Username, DateTime.Now });
                    //}
                    //else if (SupplierCode != "" && ItemNo == "" && PartNo != "" && Year == "")
                    //{
                    //    db.Execute("UpdateConfirmationSupplierProfile", new object[] { ActiveTab, supplierCodes[i].ToString(), "", partNos[i], "", AuthorizedUser.Username, DateTime.Now });
                    //}
                }
                messageResult = "Process successfully";
            }
            catch (Exception exc)
            {
                messageResult = exc.Message;
            }

            db.Close();
            return Content(messageResult);
        }

        #region TAB SUPPLIER PROFILE

        #region Supplier General Info
        public ActionResult PartialSupplierLookupGridGeneral()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierGeneral";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public JsonResult FillSuppCodeGeneralInfo()
        {
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            return Json(suppliers.Count > 1 ? null : suppliers[0]);
        }

        public string CheckSupplierProfileGeneral(string SupplierCode)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string ResultQuery = "";
            try
            {
                ResultQuery = db.ExecuteScalar<string>("CheckSupplierProfileGeneral", new object[] { SupplierCode });
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            db.Close();
            return (ResultQuery == null ? " " : ResultQuery);
        }

        public ActionResult PartialGeneralGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierGenerals = isSearch ? getGeneralInfo(SupplierCodeLDAP) : new List<SupplierGeneral>();
            Model.AddModel(model);

            return PartialView("SupplierGeneralDataGrid", Model);
        }

        protected List<SupplierGeneral> getGeneralInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierGeneral> listGeneral = new List<SupplierGeneral>();
            listGeneral = db.Query<SupplierGeneral>("GetSupplierProfileGeneralInfo", new object[] { suppCdParam }).ToList();
            db.Close();
            return listGeneral;
        }

        public ActionResult DeleteGeneral(string SupplierCode)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileGeneral", new object[] 
                {
                   SupplierCode
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewGeneral(string SupplierCode, string SupplierName, string CompanyStatus, string CapitalCurCd,
                                          double? CapitalAmount, string YearEstablish, string YearOperation, string Website,
                                          Boolean R1st, Boolean R2nd, Boolean RConsu, string ModelInvolveCd, string TmminPIC,
                                          string MainContact, string OtherModel)
        {
            string ResultQuery = "Process succesfully";
            string User = "";
            //if (SpInfo1 == "") { SpInfo1 = null; }
            //if (SpInfo2 == "") { SpInfo2 = null; }
            //if (SpInfo3 == "") { SpInfo3 = null; }
            //if (PrintCd == "") { PrintCd = null; }
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileGeneral", new object[] 
                {
                   SupplierCode,SupplierName,CompanyStatus,CapitalCurCd,CapitalAmount,YearEstablish,YearOperation,Website,R1st,R2nd,RConsu
                   ,ModelInvolveCd,TmminPIC,MainContact,User, OtherModel
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Supplier Address
        public ActionResult PartialSupplierLookupGridAddress()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierAddress";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialAddressGrid()
        {
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierAddresss = isSearch ? getAddressInfo(SupplierCodeLDAP) : new List<SupplierAddress>();

            Model.AddModel(model);

            return PartialView("SupplierAddressDataGrid", Model);
        }

        protected List<SupplierAddress> getAddressInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierAddress> listAdress = new List<SupplierAddress>();
            listAdress = db.Query<SupplierAddress>("GetSupplierProfileAddress", new object[] { suppCdParam }).ToList();
            db.Close();
            return listAdress;
        }

        public ActionResult DeleteAddress(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileAddress", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewAddress(string SupplierCode, string ItemNo, string LocationFunctionCd, string Building,
                                          string FloorBlock, string Street, string City, string PostalCode, string Telephone,
                                          string Fax, string SpeedDial, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileAddress", new object[] 
                {
                   SupplierCode,ItemNo,LocationFunctionCd,Building,FloorBlock,Street,City,PostalCode,Telephone,Fax,SpeedDial, User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Contact Person
        public ActionResult PartialSupplierLookupGridContactPerson()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierContactPerson";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialContactPersonGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");
            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierContactPersons = isSearch ? getContactInfo(SupplierCodeLDAP) : new List<SupplierContactPerson>();

            Model.AddModel(model);

            return PartialView("SupplierContactPersonDataGrid", Model);
        }

        protected List<SupplierContactPerson> getContactInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierContactPerson> listContact = new List<SupplierContactPerson>();
            listContact = db.Query<SupplierContactPerson>("GetSupplierProfileContactPerson", new object[] { suppCdParam }).ToList();
            db.Close();
            return listContact;
        }

        public ActionResult DeleteCP(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileCP", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewCP(string SupplierCode, string ItemNo, string PositionCd, string AreaCd,
                                     string Name, string Telephone, string Mobile, string Email, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileCP", new object[] 
                {
                   SupplierCode,ItemNo,PositionCd,AreaCd,Name,Telephone,Mobile,Email,User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region ShareHolder
        public ActionResult PartialSupplierLookupGridShareHolder()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierShareHolder";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialShareHolderGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierShareHolders = isSearch ? getShareHolderInfo(SupplierCodeLDAP) : new List<SupplierShareHolder>();

            Model.AddModel(model);

            return PartialView("SupplierShareHolderDataGrid", Model);
        }

        protected List<SupplierShareHolder> getShareHolderInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierShareHolder> listSH = new List<SupplierShareHolder>();
            listSH = db.Query<SupplierShareHolder>("GetSupplierProfileShareHolder", new object[] { suppCdParam }).ToList();
            db.Close();
            return listSH;
        }

        public ActionResult DeleteSH(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileShareHolder", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewSH(string SupplierCode, string ItemNo, string CountryCd,
                                     string ShareHolder, double? SharePercentage, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileShareHolder", new object[] 
                {
                   SupplierCode,ItemNo,CountryCd,ShareHolder,SharePercentage, User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Customer
        public ActionResult PartialSupplierLookupGridCustomer()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierCustomer";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialCustomerGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierCustomers = isSearch ? getCustomerInfo(SupplierCodeLDAP) : new List<SupplierCustomer>();

            Model.AddModel(model);

            return PartialView("SupplierCustomerDataGrid", Model);
        }

        protected List<SupplierCustomer> getCustomerInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierCustomer> listCustomer = new List<SupplierCustomer>();
            listCustomer = db.Fetch<SupplierCustomer>("GetSupplierProfileCustomer", new object[] { suppCdParam }).ToList();
            db.Close();
            return listCustomer;
        }

        public ActionResult DeleteCustomer(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileCustomer", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewCustomer(string SupplierCode, string ItemNo, string CustomerCategoryCd, string CustomerName,
                                           string StartOfSupply, double? ShareOfSales)
        {
            string User = "";
            //DateTime startOfSupply = DateTime.ParseExact("01.01." + StartOfSupply, "dd.MM.yyyy", null);
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileCustomer", new object[] 
                {
                   SupplierCode,ItemNo,CustomerCategoryCd,CustomerName,StartOfSupply,ShareOfSales, User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Financial
        public string CheckFinance(string SupplierCode, string ItemNo, string Year)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string ResultQuery = "";
            try
            {
                ResultQuery = db.ExecuteScalar<string>("CheckSupplierProfileFinance", new object[] { SupplierCode, ItemNo, Year });
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            db.Close();
            return (ResultQuery == null ? " " : ResultQuery);
        }

        public ActionResult PartialSupplierLookupGridFinancial()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierFinance";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialFinancialGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierFinancials = isSearch ? getFinancialInfo(SupplierCodeLDAP) : new List<SupplierFinancial>();

            Model.AddModel(model);

            return PartialView("SupplierFinancialDataGrid", Model);
        }

        protected List<SupplierFinancial> getFinancialInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierFinancial> listFinancial = new List<SupplierFinancial>();
            listFinancial = db.Query<SupplierFinancial>("GetSupplierProfileFinancial", new object[] { suppCdParam }).ToList();
            db.Close();
            return listFinancial;
        }

        public ActionResult DeleteFinancial(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileFinancial", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewFinancial(string SupplierCode, string ItemNo, string AccFrom, string AccTo, double SalesAmountAchievement,
                        double SalesAmountForecast, double? OperatingProfitAchievement, double? OperatingProfitAchievementPercent,
                        double? OperatingProfitForecast, double? OperatingProfitForecastPercent, double SalesToTMMINAchievement,
                        double? SalesToTMMINAchievementPercent, double SalesToTMMINForecast, double? SalesToTMMINForecastPercent,
                        double SalesToADMAchievement, double? SalesToADMAchievementPercent, double SalesToADMForecast,
                        double? SalesToADMForecastPercent, double? SalesToOtherAchievement, double? SalesToOtherAchievementPercent,
                        double? SalesToOtherForecast, double? SalesToOtherForecastPercent, double? OrdinaryProfitAchievement,
                        double? OrdinaryProfitAchievementPercent, double? OrdinaryProfitForecast, double? OrdinaryProfitForecastPercent)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            string User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileFinancial", new object[] 
                {
                   SupplierCode, ItemNo, AccFrom, AccTo, SalesAmountAchievement, SalesAmountForecast, OperatingProfitAchievement, OperatingProfitAchievementPercent,
                   OperatingProfitForecast, OperatingProfitForecastPercent, SalesToTMMINAchievement, SalesToTMMINAchievementPercent, SalesToTMMINForecast,
                   SalesToTMMINForecastPercent, SalesToADMAchievement, SalesToADMAchievementPercent, SalesToADMForecast, SalesToADMForecastPercent,
                   SalesToOtherAchievement, SalesToOtherAchievementPercent, SalesToOtherForecast, SalesToOtherForecastPercent, OrdinaryProfitAchievement,
                   OrdinaryProfitAchievementPercent, OrdinaryProfitForecast, OrdinaryProfitForecastPercent, User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Man Power
        public string CheckSupplierProfileManPower(string SupplierCode)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string ResultQuery = "";
            try
            {
                ResultQuery = db.ExecuteScalar<string>("CheckSupplierProfileManPower", new object[] { SupplierCode });
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            db.Close();
            return (ResultQuery == null ? " " : ResultQuery);
        }

        public ActionResult PartialSupplierLookupGridManPower()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierManPower";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialManPowerGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierManPowers = isSearch ? getManPowerInfo(SupplierCodeLDAP) : new List<SupplierManPower>();

            Model.AddModel(model);

            return PartialView("SupplierManPowerDataGrid", Model);
        }

        protected List<SupplierManPower> getManPowerInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierManPower> listManPower = new List<SupplierManPower>();
            listManPower = db.Query<SupplierManPower>("GetSupplierProfileManPower", new object[] { suppCdParam }).ToList();
            db.Close();
            return listManPower;
        }

        public ActionResult DeleteManPower(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileManPower", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewManPower(string SupplierCode, string ItemNo, int Engineering, int ProductionControl,
                                           int ProductionOperator, int QualityAssurance, int Other, int PlantDirect,
                                           int NonPlant, int StatusPermanent, int StatusTemporer, int EducationS1S2,
                                           int EducationD3, int EducationHighSchool, int NationalityExpa,
                                           int NationalityWni, int Employee, decimal? AverageAge, string Year, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileManPower", new object[] 
                {
                    SupplierCode,  ItemNo, Engineering, ProductionControl, ProductionOperator, QualityAssurance, Other,
                    PlantDirect, NonPlant,StatusPermanent, StatusTemporer, EducationS1S2 , EducationD3 , EducationHighSchool
                    , NationalityExpa ,NationalityWni , Employee , AverageAge ,Year, User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Labor
        public ActionResult PartialSupplierLookupGridLabor()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierLabor";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialLaborGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierLabors = isSearch ? getLaborInfo(SupplierCodeLDAP) : new List<SupplierLabor>();

            Model.AddModel(model);

            return PartialView("SupplierLaborDataGrid", Model);
        }

        protected List<SupplierLabor> getLaborInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierLabor> listLabor = new List<SupplierLabor>();
            listLabor = db.Query<SupplierLabor>("GetSupplierProfileLabor", new object[] { suppCdParam }).ToList();
            db.Close();
            return listLabor;
        }

        public ActionResult DeleteLabor(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileLabor", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewLabor(string SupplierCode, string ItemNo, bool Availability, string SinglePlural,
                                        string Affiliation, string Strike3, string Strike2, string Strike1,
                                        bool CollectiveLaborAgreement, string CommunicationMedia, bool PensiunProgram,
                                        string Bonus, string SalaryIncrease, bool MealTransportAllowance,
                                        bool MedicalAllowance, bool Thr, bool Overtime, string User, string OtherUnion,
                                        string UnionLabel)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;

            //DateTime BonusDate = (Bonus == "" || Bonus == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(Bonus, "dd.MM.yyyy", null);
            //DateTime SalaryIncreaseDate = (SalaryIncrease == "" || SalaryIncrease == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(SalaryIncrease, "dd.MM.yyyy", null);

            //string[] SalaryIncreased = SalaryIncrease.Split('.');
            //if (SalaryIncreased.Length > 1)
            //{
            //    SalaryIncrease = SalaryIncreased[2] + "-" + SalaryIncreased[1] + "-" + SalaryIncreased[0];
            //}
            //else 
            //{
            //    SalaryIncrease = null;
            //}

            //switch (Affiliation)
            //{
            //    case "SPSI": unionConvert = 1; break;
            //    case "SPMI": unionConvert = 2; break;
            //    default: unionConvert = 0; break;
            //}

            try
            {
                db.Execute("InsertSupplierProfileLabor", new object[] 
                {
                    SupplierCode, ItemNo, Availability, SinglePlural, Affiliation, Strike3, CollectiveLaborAgreement,
                    CommunicationMedia, PensiunProgram, Bonus, SalaryIncrease, MealTransportAllowance, MedicalAllowance,
                    Thr, Overtime, User, Strike2, Strike1, OtherUnion, UnionLabel
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region CAD/IT
        public ActionResult PartialSupplierLookupGridCADIT()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierCADIT";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialCADITGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierCADITs = isSearch ? getCadItInfo(SupplierCodeLDAP) : new List<SupplierCADIT>();

            Model.AddModel(model);

            return PartialView("SupplierCADITDataGrid", Model);
        }

        protected List<SupplierCADIT> getCadItInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierCADIT> listCAD = new List<SupplierCADIT>();
            listCAD = db.Query<SupplierCADIT>("GetSupplierProfileCadIT", new object[] { suppCdParam }).ToList();
            db.Close();
            return listCAD;
        }

        public ActionResult DeleteCadIt(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileCadIt", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewCadIt(string SupplierCode, string ItemNo, double? TerminalAmount, Boolean CamCompleted,
                                        int EngineerCadIt, double? InternetBandwith, int OperatorCadIt, Boolean PrinterCadIt,
                                        string PrinterSize, string CADType)
        {
            string ResultQuery = "Process succesfully";
            string User = "";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileCadIt", new object[] 
                {
                    SupplierCode,  ItemNo, TerminalAmount , CamCompleted,EngineerCadIt,InternetBandwith,OperatorCadIt,PrinterCadIt,PrinterSize,User,CADType
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Purchased Part
        public ActionResult PartialSupplierLookupGridPurchasedPart()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierPurchasedPart";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialPurchasedPartGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierPurchasedParts = isSearch ? getPurchasedPartInfo(SupplierCodeLDAP) : new List<SupplierPurchasedPart>();

            Model.AddModel(model);

            return PartialView("SupplierPurchasedPartDataGrid", Model);
        }

        protected List<SupplierPurchasedPart> getPurchasedPartInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierPurchasedPart> listPurchased = new List<SupplierPurchasedPart>();
            listPurchased = db.Query<SupplierPurchasedPart>("GetSupplierProfilePurchasedPart", new object[] { suppCdParam }).ToList();
            db.Close();
            return listPurchased;
        }

        public ActionResult DeletePurchasedPart(string SupplierCode, string PartName)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfilePurchasedPart", new object[] 
                {
                   SupplierCode,
                   PartName
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewPurchasedPart(string SupplierCode, string PartNo, string Country, string SourceName)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            string SourceStatus = "";
            string User = "";
            if (Country == "ID") { SourceStatus = "Export"; }
            if (Country != "ID") { SourceStatus = "Import"; }

            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfilePurchasedPart", new object[] 
                {
                    SupplierCode,PartNo,Country,SourceStatus, User, SourceName
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        #endregion

        #region Award
        public ActionResult PartialSupplierLookupGridAward()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierAward";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialAwardGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierAwards = isSearch ? getAwardInfo(SupplierCodeLDAP) : new List<SupplierAward>();

            Model.AddModel(model);

            return PartialView("SupplierAwardDataGrid", Model);
        }

        protected List<SupplierAward> getAwardInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierAward> listAward = new List<SupplierAward>();
            listAward = db.Query<SupplierAward>("GetSupplierProfileAward", new object[] { suppCdParam }).ToList();
            db.Close();
            return listAward;
        }

        public ActionResult DeleteAward(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileAward", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewAward(string SupplierCode, string ItemNo, string Year, string Granator,
                                        string TitleAward, string Remark, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileAward", new object[] 
                {
                    SupplierCode,ItemNo,Year,Granator,TitleAward,Remark , User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }
        #endregion

        #region Product
        public ActionResult PartialSupplierLookupGridProduct()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierProduct";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialProductGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierProducts = isSearch ? getProductInfo(SupplierCodeLDAP) : new List<SupplierProduct>();

            Model.AddModel(model);

            return PartialView("SupplierProductDataGrid", Model);
        }

        protected List<SupplierProduct> getProductInfo(string suppCdParam)
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierProduct> listProduct = new List<SupplierProduct>();
            listProduct = db.Query<SupplierProduct>("GetSupplierProfileProduct", new object[] { suppCdParam }).ToList();
            db.Close();
            return listProduct;
        }

        public ActionResult DeleteProduct(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileProduct", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewProduct(string SupplierCode, string ItemNo, string ProductName, string AutomotiveCustomer,
                                          string StartProductionProduct, double? MarketShare, string ExportBiggest,
                                          double? ExportShareCompetitor, string User, string OtherAuto, string AutoLabel)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            //DateTime? StartProductionProductDate = (StartProductionProduct == "" || StartProductionProduct == null) ? null : DateTime.ParseExact(StartProductionProduct, "dd.MM.yyyy", null);

            //string[] date = StartProductionProduct.Split('.');

            //if (date.Length > 1)
            //{
            //    StartProductionProduct = date[2] + "-" + date[1] + "-" + date[0];
            //}
            //else
            //{
            //    StartProductionProduct = null;    
            //}

            try
            {
                db.Execute("InsertSupplierProfileProduct", new object[] 
                {
                    SupplierCode,ItemNo,ProductName,AutomotiveCustomer,StartProductionProduct,MarketShare,ExportBiggest,
                    ExportShareCompetitor, User, OtherAuto, AutoLabel
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }
        #endregion

        #region Business Relation
        public ActionResult PartialSupplierLookupGridBusinessRelation()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierBusinessRelation";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialBusinessRelationGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierBusinessRelations = isSearch ? getBusinessInfo(SupplierCodeLDAP) : new List<SupplierBusinessRelation>();

            Model.AddModel(model);

            return PartialView("SupplierBusinessRelationDataGrid", Model);
        }

        protected List<SupplierBusinessRelation> getBusinessInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierBusinessRelation> listBusiness = new List<SupplierBusinessRelation>();
            listBusiness = db.Query<SupplierBusinessRelation>("GetSupplierProfileBusinessRelation", new object[] { suppCdParam }).ToList();
            db.Close();
            return listBusiness;
        }

        public ActionResult DeleteBR(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileBusinessRelation", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewBR(string SupplierCode, string ItemNo, string CompanyNameBR, string ContentOfCooperation,
                                     string CompanyCategoryBR, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileBusinessRelation", new object[] 
                {
                    SupplierCode,ItemNo,CompanyNameBR,ContentOfCooperation,CompanyCategoryBR, User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }
        #endregion

        #region Equipment
        public ActionResult PartialSupplierLookupGridEquipment()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierEquipment";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialEquipmentGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }
            model.SupplierEquipments = isSearch ? getEquipmentInfo(SupplierCodeLDAP) : new List<SupplierEquipment>();

            Model.AddModel(model);

            return PartialView("SupplierEquipmentDataGrid", Model);
        }

        protected List<SupplierEquipment> getEquipmentInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierEquipment> listEquip = new List<SupplierEquipment>();
            listEquip = db.Query<SupplierEquipment>("GetSupplierProfileEquipment", new object[] { suppCdParam }).ToList();
            db.Close();
            return listEquip;
        }

        public ActionResult DeleteEquipment(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileEquipment", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewEquipment(string SupplierCode, string ItemNo, string MachineName, string DetailType,
                                            double? Tonage, int Unit, string BrandName, int Qty, string YearBuying,
                                            string YearMaking, string Remark, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileEquipment", new object[] 
                {
                    SupplierCode,ItemNo,MachineName,DetailType,Tonage,Unit,BrandName,Qty,YearBuying,YearMaking,Remark ,User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }
        #endregion

        #region Export
        public ActionResult PartialSupplierLookupGridExport()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OSPSupplierExport";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialExportGrid()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            bool isSearch = Convert.ToBoolean(Request.Params["isSearch"]);

            SupplierModel model = Model.GetModel<SupplierModel>();
            if (model == null)
            { model = new SupplierModel(); }

            model.SupplierExports = isSearch ? getExportInfo(SupplierCodeLDAP) : new List<SupplierExport>();

            Model.AddModel(model);

            return PartialView("SupplierExportDataGrid", Model);
        }

        protected List<SupplierExport> getExportInfo(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierExport> listExport = new List<SupplierExport>();
            listExport = db.Query<SupplierExport>("GetSupplierProfileExport", new object[] { suppCdParam }).ToList();
            db.Close();
            return listExport;
        }

        public ActionResult DeleteExport(string SupplierCode, string ItemNo)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteSupplierProfileExport", new object[] 
                {
                   SupplierCode,
                   ItemNo
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ActionResult AddNewExport(string SupplierCode, string ItemNo, string CountryCd, string MajorProduct,
                                         Boolean ViaTMMIN, double? ShareSales, string User)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            User = AuthorizedUser.Username;
            try
            {
                db.Execute("InsertSupplierProfileExport", new object[] 
                {
                    SupplierCode,ItemNo,CountryCd,MajorProduct,ViaTMMIN,ShareSales ,User
                });
                TempData["GridName"] = "gridGeneral";
                db.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate"))
                    ResultQuery = "Error: " + "data already exist !!!";
                else
                    ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }
        #endregion

        #endregion

        #region Header Criteria
        public ActionResult PartialHeader()
        {
            #region Required model for [Supplier lookup]
            //TempData["GridName"] = "OIHSupplierOption";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");
            _SupplierModel = suppliers;

            Model.AddModel(_SupplierModel);
            #endregion

            return PartialView("HeaderMaintenance", Model);
        }

        public ActionResult PartialHeaderSupplierLookupGridSupplierInfo()
        {
            #region Required model for [Supplier lookup]

            TempData["GridName"] = "OIHSupplierOption";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            _SupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }
        #endregion

        #region Grid Lookup
        public ActionResult PartialHeaderCurrencyGridLookupSuppInfo()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "PartCurrencyGridLookup";
            ViewData["CurrencyGridLookup"] = GetAllCurrency();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["CurrencyGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierPositionGridLookupSuppInfo()
        {
            #region Required model in partial page : for SupPosGridLookup
            TempData["GridName"] = "SupplierPosotionGridLookup";
            ViewData["SupPosGridLookup"] = GetAllSupplierPosition();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupPosGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierAreaGridLookupSuppInfo()
        {
            #region Required model in partial page : for SupPosGridLookup
            TempData["GridName"] = "SupplierAreaGridLookup";
            ViewData["SupAreaGridLookup"] = GetAllSupplierArea();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupAreaGridLookup"]);
        }

        public ActionResult PartialDetailInvolveGridLookupSuppInfo()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "PartInvolveGridLookup";
            ViewData["InvolveGridLookup"] = GetAllInvolve();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["InvolveGridLookup"]);
        }

        public ActionResult PartialHeaderCurrencyGridLookupSuppInfoFinancial()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "PartCurrencyGridLookupFinancial";
            ViewData["CurrencyGridLookupFinancial"] = GetAllCurrency();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["CurrencyGridLookupFinancial"]);
        }

        public ActionResult PartialHeaderCurrencyGridLookupSuppInfoSH()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "PartCurrencyGridLookupSH";
            ViewData["CurrencyGridLookupSH"] = GetAllCurrency();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["CurrencyGridLookupSH"]);
        }

        public ActionResult PartialHeaderLocationFunctionGridLookupSuppInfo()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "LocationFunctionGridLookup";
            ViewData["LocationGridLookup"] = GetAllLocationFunction();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["LocationGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierCountryGridLookupSuppInfoSH()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "SupplierCountryGridLookupSH";
            ViewData["SupCountryGridLookupSH"] = GetAllCountry();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupCountryGridLookupSH"]);
        }

        public ActionResult PartialHeaderSupplierCountryGridLookupSuppInfoPP()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "SupplierCountryGridLookupPP";
            ViewData["SupCountryGridLookupPP"] = GetAllCountry();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupCountryGridLookupPP"]);
        }

        public ActionResult PartialHeaderAutomotiveCustomerGridLookupSuppInfo()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "AutomotiveCustomerGridLookup";
            ViewData["AutomotiveGridLookup"] = GetAllAutomotive();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["AutomotiveGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierCountryGridLookupSuppInfoExport()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "SupplierCountryGridLookupExport";
            ViewData["SupCountryGridLookupExport"] = GetAllCountry();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupCountryGridLookupExport"]);
        }

        public ActionResult PartialHeaderCustomerCategoryGridLookupSuppInfoCust()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "CustomerCategoryGridLookupCust";
            ViewData["CustCategoryGridLookupCus"] = GetAllCustomerCategory();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["CustCategoryGridLookupCus"]);
        }

        public ActionResult PartialHeaderSupplierInvoiceGridLookupSuppInfo()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "SupplierInvoiceTaxTypeGridLookupSH";
            ViewData["SupplierInvoiceTaxTypeGridLookupFinan"] = GetAllInvoiceTaxType();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierInvoiceTaxTypeGridLookupFinan"]);
        }

        public ActionResult PartialHeaderSupplierSupplyTypeGridLookupSuppInfo()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "SupplierSupplyTypeGridLookup";
            ViewData["SupSupplyTypeGridLookup"] = GetAllSupplyType();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupSupplyTypeGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierTierGridLookupSuppInfo()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "SupplierTierGridLookup";
            ViewData["SupTierGridLookup"] = GetAllTier();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupTierGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierUnionSuppInfo()
        {
            TempData["GridName"] = "txtAffiliation";
            ViewData["UnionCombo"] = GetAllUnion();
            return PartialView("GridLookup/PartialGrid", ViewData["UnionCombo"]);
        }
        #endregion

        #region ComboBox and List
        private List<SupplierICS> GetAllSupplier()
        {
            List<SupplierICS> supplierModel = new List<SupplierICS>();
            IDBContext db = DbContext;
            try
            {
                supplierModel = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return supplierModel;
        }

        private List<Union> GetAllUnion()
        {
            List<Union> UnionModelList = new List<Union>();

            IDBContext db = DbContext;
            try
            {
                UnionModelList = db.Fetch<Union>("GetAllUnion", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return UnionModelList;
        }

        private List<Currency> GetAllCurrency()
        {
            List<Currency> CurrModelList = new List<Currency>();

            IDBContext db = DbContext;
            try
            {
                CurrModelList = db.Fetch<Currency>("GetAllCurrency", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return CurrModelList;
        }

        private List<LocationFunction> GetAllLocationFunction()
        {
            List<LocationFunction> LocFunModelList = new List<LocationFunction>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<LocationFunction>("GetAllLocationFunction", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<Automotive> GetAllAutomotive()
        {
            List<Automotive> LocFunModelList = new List<Automotive>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<Automotive>("GetAllAutomotive", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<CountryArea> GetAllCountry()
        {
            List<CountryArea> LocFunModelList = new List<CountryArea>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<CountryArea>("GetAllCountry", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<CustomerCategory> GetAllCustomerCategory()
        {
            List<CustomerCategory> LocFunModelList = new List<CustomerCategory>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<CustomerCategory>("GetAllCustomerCategory", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<InvoiceTaxType> GetAllInvoiceTaxType()
        {
            List<InvoiceTaxType> LocFunModelList = new List<InvoiceTaxType>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<InvoiceTaxType>("GetAllInvoiceTaxType", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<Tier> GetAllTier()
        {
            List<Tier> LocFunModelList = new List<Tier>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<Tier>("GetAllTier", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<SupplyType> GetAllSupplyType()
        {
            List<SupplyType> LocFunModelList = new List<SupplyType>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<SupplyType>("GetAllSupplyType", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<SupplierInvolve> GetAllInvolve()
        {
            List<SupplierInvolve> CurrModelList = new List<SupplierInvolve>();

            IDBContext db = DbContext;
            try
            {
                CurrModelList = db.Fetch<SupplierInvolve>("GetAllInvolve", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return CurrModelList;
        }

        private List<SupplierPosition> GetAllSupplierPosition()
        {
            List<SupplierPosition> LocFunModelList = new List<SupplierPosition>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<SupplierPosition>("GetAllSupplierPosition", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }

        private List<SupplierArea> GetAllSupplierArea()
        {
            List<SupplierArea> LocFunModelList = new List<SupplierArea>();

            IDBContext db = DbContext;
            try
            {
                LocFunModelList = db.Fetch<SupplierArea>("GetAllSupplierArea", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return LocFunModelList;
        }
        #endregion

        #region UPLOAD DATA

        #region UPLOAD CONFIRMATION - ACTION
        [HttpPost]
        public ActionResult HeaderTabSPConfirmation_Upload(string filePath, string ActiveTab)
        {
            string status = ProcessAllSPConfirmationUpload(filePath, ActiveTab);

            return Json(new { Status = status } );
        }
        #endregion

        #region UPLOAD CONFIRMATION - PROPERTIES
        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/EmergencyOrderCreation/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private Int32 _processId = 0;
        private Int32 ProcessId
        {
            get
            {
                if (_processId == 0)
                    _processId = Math.Abs(Environment.TickCount);

                return _processId;
            }
        }

        private Int32 _seqNo = 0;
        private Int32 SeqNo
        {
            get
            {
                return ++_seqNo;
            }
        }
        #endregion

        #region UPLOAD CONFIRMATION - REQUEST
        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();
            IDBContext db = DbContext;
            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            db.Close();

            return String.Join(",", columnNameArray.ToArray());
        }

        public DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            List<DataRow> drList = new List<DataRow>();
            foreach (DataRow dr in formattedTable.Rows)
            {
                int count = dr.ItemArray.Length;
                int nullcounter = 0;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])) || dr.ItemArray[i].ToString().Length <= 0)
                    {
                        nullcounter++;
                    }
                }

                if (nullcounter == count - 2)
                {
                    drList.Add(dr);
                }
            }

            for (int i = 0; i < drList.Count; i++)
            {
                formattedTable.Rows.Remove(drList[i]);
            }
            formattedTable.AcceptChanges();

            return formattedTable;
        }

        private string ProcessAllSPConfirmationUpload(string filePath, string ActiveTab)
        {
            string result = "";

            #region Bulk Config Properties
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.functionID = ActiveTab;

            switch (ActiveTab)
            {
                case "0": _TableDestination.sheetName = "General"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_GENERAL_INFO"; break;
                case "1": _TableDestination.sheetName = "Address"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_ADDRESS"; break;
                case "2": _TableDestination.sheetName = "ContactPerson"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_CONTACT_PERSON"; break;
                case "3": _TableDestination.sheetName = "ShareHolder"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_SHAREHOLDER"; break;
                case "4": _TableDestination.sheetName = "Customer"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_CUSTOMER"; break;
                case "5": _TableDestination.sheetName = "Financial"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_FINANCIAL"; break;
                case "6": _TableDestination.sheetName = "ManPower"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_MAN_POWER"; break;
                case "7": _TableDestination.sheetName = "Labor"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_LABOR"; break;
                case "8": _TableDestination.sheetName = "Cadit"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_CAD_IT"; break;
                case "9": _TableDestination.sheetName = "PurchasedPart"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_PURCHASED_PART"; break;
                case "10": _TableDestination.sheetName = "Award"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_AWARD"; break;
                case "11": _TableDestination.sheetName = "Product"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_PRODUCT"; break;
                case "12": _TableDestination.sheetName = "Business"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_BUSINESS_RELATION"; break;
                case "13": _TableDestination.sheetName = "Equipment"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_EQUIPMENT"; break;
                case "14": _TableDestination.sheetName = "Export"; _TableDestination.tableFromTemp = "TB_T_SUPPLIER_EXPORT"; break;
                default: break;
            }
            #endregion

            OleDbConnection excelConn = null;
            IDBContext db = DbContext;
            try
            {
                db.ExecuteScalar<String>("Clear_SupplierProfile_TempTable", new object[] { _TableDestination.tableFromTemp, AuthorizedUser.Username });
                
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties = Excel 8.0";
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                DataTable sheet = excelConn.GetSchema("Tables");
                bool sheetfound = false;
                foreach (DataRow dr in sheet.Rows)
                {
                    if (dr[2].ToString().Replace("'", "") == _TableDestination.sheetName + "$")
                        sheetfound = true;
                }

                if (!sheetfound)
                    throw new Exception("Cannot Find Sheet " + _TableDestination.sheetName + " in Uploaded File, Please use Valid Template");
                
                #region Excel OleDB Command Builder
                string OleCommand = @"select " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @"'" + AuthorizedUser.Username + "' as CREATED_BY, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as SUPPLIER_CD ";
                                    //F2 is SUPPLIER_NAME, NOT GET

                switch (ActiveTab)
                {
                    case "0":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as COMPANY_STATUS, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as WEBSITE, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as CAPITAL_CURRENCY_CD, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as CAPITAL_AMOUNT, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as YEAR_ESTABLISH, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as YEAR_OPERATION, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as MODEL_INVOLVE_CD, " +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as TMMIN_PIC, " +
                                     @"IIf(IsNull(F11), Null, CStr(F11)) as MAIN_CONTACT, " +
                                     @"IIf(IsNull(F12), Null, CStr(F12)) as RELATION_1ST_TIER, " +
                                     @"IIf(IsNull(F13), Null, CStr(F13)) as RELATION_2ND_3RD_TIER, " +
                                     @"IIf(IsNull(F14), Null, CStr(F14)) as CONSUMABLE_AND_FACILITY ";
                        break;
                    case "1":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as LOCATION_FUNCTION_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as BUILDING, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as FLOOR_BLOCK, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as STREET, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as CITY, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as POSTAL_CODE, " +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as TELEPHONE, " +
                                     @"IIf(IsNull(F12), Null, CStr(F11)) as FAX, " +
                                     @"IIf(IsNull(F12), Null, CStr(F12)) as SPEED_DIAL ";
                        break;
                    case "2":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as POSITION_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as AREA_NAME, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as NAME, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as TELEPHONE, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as MOBILE, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as EMAIL ";
                        break;
                    case "3":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as COUNTRY_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as SHAREHOLDER, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as SHARE_PERCENTAGE ";
                        break;
                    case "4":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as CUSTOMER_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as START_OF_SUPPLY, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as SHARE_OF_SALES, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as CUSTOMER_CATEGORY_NAME ";
                        break;
                    case "5":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as ACCT_PERIOD_FROM, " +
                                     //@"IIf(IsNull(F5), Null, CStr(F5)) as ACCT_PERIOD_TO, " +
                                     //@"IIf(IsNull(F6), Null, CStr(F6)) as SALES_AMOUNT_ACHIEVEMENT, " +
                                     //@"IIf(IsNull(F7), Null, CStr(F7)) as SALES_AMOUNT_FORECAST, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as OPERATING_PROFIT_ACHIEVEMENT, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as OPERATING_PROFIT_ACHIEVEMENT_PERCENT, " +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as OPERATING_PROFIT_FORECAST, " +
                                     @"IIf(IsNull(F11), Null, CStr(F11)) as OPERATING_PROFIT_FORECAST_PERCENT, " +
                                     @"IIf(IsNull(F12), Null, CStr(F12)) as SALES_TO_TMMIN_ACHIEVEMENT, " +
                                     @"IIf(IsNull(F14), Null, CStr(F14)) as SALES_TO_TMMIN_FORECAST, " +
                                     @"IIf(IsNull(F16), Null, CStr(F16)) as SALES_TO_ADM_ACHIEVEMENT, " +
                                     @"IIf(IsNull(F18), Null, CStr(F18)) as SALES_TO_ADM_FORECAST, " +
                                     @"IIf(IsNull(F20), Null, CStr(F20)) as SALES_TO_OTHER_ACHIEVEMENT, " +
                                     @"IIf(IsNull(F22), Null, CStr(F22)) as SALES_TO_OTHER_FORECAST, " +
                                     @"IIf(IsNull(F24), Null, CStr(F24)) as ORDINARY_PROFIT_ACHIEVEMENT, " +
                                     @"IIf(IsNull(F25), Null, CStr(F25)) as ORDINARY_PROFIT_ACHIEVEMENT_PERCENT, " +
                                     @"IIf(IsNull(F26), Null, CStr(F26)) as ORDINARY_PROFIT_FORECAST, " +
                                     @"IIf(IsNull(F27), Null, CStr(F27)) as ORDINARY_PROFIT_FORECAST_PERCENT ";
                        break;
                    case "6":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as [YEAR], " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as ENGINEERING, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as PRODUCTION_CONTROL, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as PRODUCTION_OPERATOR, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as QUALITY_ASSURANCE, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as LABOR_SUPPLY, " +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as PLANT_DIRECT, " +
                                     @"IIf(IsNull(F11), Null, CStr(F11)) as NON_PLANT, " +
                                     @"IIf(IsNull(F12), Null, CStr(F12)) as STATUS_PERMANENT, " +
                                     @"IIf(IsNull(F13), Null, CStr(F13)) as STATUS_TEMPORER, " +
                                     @"IIf(IsNull(F14), Null, CStr(F14)) as EDUCATION_S1_S2, " +
                                     @"IIf(IsNull(F15), Null, CStr(F15)) as EDUCATION_D3, " +
                                     @"IIf(IsNull(F16), Null, CStr(F16)) as EDUCATION_HIGH_SCHOOL, " +
                                     @"IIf(IsNull(F17), Null, CStr(F17)) as NATIONALITY_EXPATRIATE, " +
                                     @"IIf(IsNull(F18), Null, CStr(F18)) as NATIONALITY_WNI, " +
                                     @"IIf(IsNull(F19), Null, CStr(F19)) as TOTAL_EMPLOYEE, " +
                                     @"IIf(IsNull(F20), Null, CStr(F20)) as AVERAGE_AGE ";
                        break;
                    case "7":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as COMMUNICATION_MEDIA, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as UNION_NAME, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as SINGLE_PLURAL, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as BONUS, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as SALARY_INCREASE, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as HISTORY_STRIKE_3, " +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as HISTORY_STRIKE_2, " +
                                     @"IIf(IsNull(F11), Null, CStr(F11)) as HISTORY_STRIKE_1, " +
                                     //@"IIf(IsNull(F12), Null, CStr(F12)) as AVAILABILITY, " +
                                     @"IIf(IsNull(F12), Null, CStr(F12)) as PENSION_PROGRAM, " +
                                     @"IIf(IsNull(F13), Null, CStr(F13)) as THR, " +
                                     @"IIf(IsNull(F14), Null, CStr(F14)) as OVERTIME, " +
                                     @"IIf(IsNull(F15), Null, CStr(F15)) as COLLECTIVE_LABOR_AGREEMENT, " +
                                     @"IIf(IsNull(F16), Null, CStr(F16)) as MEAL_TRANSPORT_ALLOWANCE, " +
                                     @"IIf(IsNull(F17), Null, CStr(F17)) as MEDICAL_ALLOWANCE ";
                        break;
                    case "8":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as TERMINAL_AMOUNT, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as ENGINEER, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as INTERNET_BANDWITH, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as OPERATOR, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as PRINTER, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as CAM_COMPLETED, " +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as PRINTER_SIZE, " +
                                     @"IIf(IsNull(F11), Null, CStr(F11)) as CAD_TYPE";
                        break;
                    case "9":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as PART_NAME, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as COUNTRY_NAME, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as SOURCE_NAME ";
                        break;
                    case "10":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as [YEAR], " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as GRANTOR, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as TITLE_AWARD, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as REMARK ";
                        break;
                    case "11":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as PRODUCT_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as AUTOMOTIVE_CUSTOMER_LIST, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as START_PRODUCTION, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as MARKET_SHARE, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as BIGGEST_COMPETITOR, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as SHARE_OF_COMPETITOR ";
                        break;
                    case "12":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as COMPANY_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as CONTENT_OF_COOPERATION, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as COMPANY_CATEGORY ";
                        break;
                    case "13":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as MACHINE_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as DETAIL_TYPE, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as TONAGE, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as UNIT, " +
                                     @"IIf(IsNull(F8), Null, CStr(F8)) as BRAND_NAME, " +
                                     @"IIf(IsNull(F9), Null, CStr(F9)) as QTY, " +
                                     @"IIf(IsNull(F10), Null, CStr(F10)) as YEAR_BUYING, " +
                                     @"IIf(IsNull(F11), Null, CStr(F11)) as YEAR_MAKING, " +
                                     @"IIf(IsNull(F12), Null, CStr(F12)) as REMARK";
                        break;
                    case "14":
                        OleCommand = OleCommand + ", " +
                                     @"IIf(IsNull(F3), Null, CStr(F3)) as ITEM_NO, " +
                                     @"IIf(IsNull(F4), Null, CStr(F4)) as COUNTRY_NAME, " +
                                     @"IIf(IsNull(F5), Null, CStr(F5)) as MAJOR_PRODUCT, " +
                                     @"IIf(IsNull(F6), Null, CStr(F6)) as VIA_TMMIN, " +
                                     @"IIf(IsNull(F7), Null, CStr(F7)) as SHARE_SALES ";
                        break;
                    default: break;
                }

                OleCommand = OleCommand + " from [" + _TableDestination.sheetName + "$]";
                excelCommand = new OleDbCommand(OleCommand, excelConn);
                excelDataAdapter.SelectCommand = excelCommand;
                #endregion

                excelDataAdapter.Fill(dtPatterns);
                dtPatterns = DeleteEmptyRows(dtPatterns);
                ds.Tables.Add(dtPatterns);
                DataRow rowDel = ds.Tables[0].Rows[0];
                ds.Tables[0].Rows.Remove(rowDel);

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("CREATED_BY", "CREATED_BY");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CD", "SUPPLIER_CD");

                    #region BULK Mapping
                    switch (ActiveTab)
                    {
                        case "0":
                            bulkCopy.ColumnMappings.Add("COMPANY_STATUS", "COMPANY_STATUS");
                            bulkCopy.ColumnMappings.Add("WEBSITE", "WEBSITE");
                            bulkCopy.ColumnMappings.Add("CAPITAL_CURRENCY_CD", "CAPITAL_CURRENCY_CD");
                            bulkCopy.ColumnMappings.Add("CAPITAL_AMOUNT", "CAPITAL_AMOUNT");
                            bulkCopy.ColumnMappings.Add("YEAR_ESTABLISH", "YEAR_ESTABLISH");
                            bulkCopy.ColumnMappings.Add("YEAR_OPERATION", "YEAR_OPERATION");
                            bulkCopy.ColumnMappings.Add("MODEL_INVOLVE_CD", "MODEL_INVOLVE_CD");
                            bulkCopy.ColumnMappings.Add("TMMIN_PIC", "TMMIN_PIC");
                            bulkCopy.ColumnMappings.Add("MAIN_CONTACT", "MAIN_CONTACT");
                            bulkCopy.ColumnMappings.Add("RELATION_1ST_TIER", "RELATION_1ST_TIER");
                            bulkCopy.ColumnMappings.Add("RELATION_2ND_3RD_TIER", "RELATION_2ND_3RD_TIER");
                            bulkCopy.ColumnMappings.Add("CONSUMABLE_AND_FACILITY", "CONSUMABLE_AND_FACILITY");
                            break;
                        case "1":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("LOCATION_FUNCTION_NAME", "LOCATION_FUNCTION_NAME");
                            bulkCopy.ColumnMappings.Add("BUILDING", "BUILDING");
                            bulkCopy.ColumnMappings.Add("FLOOR_BLOCK", "FLOOR_BLOCK");
                            bulkCopy.ColumnMappings.Add("STREET", "STREET");
                            bulkCopy.ColumnMappings.Add("CITY", "CITY");
                            bulkCopy.ColumnMappings.Add("POSTAL_CODE", "POSTAL_CODE");
                            bulkCopy.ColumnMappings.Add("TELEPHONE", "TELEPHONE");
                            bulkCopy.ColumnMappings.Add("FAX", "FAX");
                            bulkCopy.ColumnMappings.Add("SPEED_DIAL", "SPEED_DIAL");
                            break;
                        case "2":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("POSITION_NAME", "POSITION_NAME");
                            bulkCopy.ColumnMappings.Add("AREA_NAME", "AREA_NAME");
                            bulkCopy.ColumnMappings.Add("NAME", "NAME");
                            bulkCopy.ColumnMappings.Add("TELEPHONE", "TELEPHONE");
                            bulkCopy.ColumnMappings.Add("MOBILE", "MOBILE");
                            bulkCopy.ColumnMappings.Add("EMAIL", "EMAIL");
                            break;
                        case "3":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("COUNTRY_NAME", "COUNTRY_NAME");
                            bulkCopy.ColumnMappings.Add("SHAREHOLDER", "SHAREHOLDER");
                            bulkCopy.ColumnMappings.Add("SHARE_PERCENTAGE", "SHARE_PERCENTAGE");
                            break;
                        case "4":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("CUSTOMER_NAME", "CUSTOMER_NAME");
                            bulkCopy.ColumnMappings.Add("START_OF_SUPPLY", "START_OF_SUPPLY");
                            bulkCopy.ColumnMappings.Add("SHARE_OF_SALES", "SHARE_OF_SALES");
                            bulkCopy.ColumnMappings.Add("CUSTOMER_CATEGORY_NAME", "CUSTOMER_CATEGORY_NAME");
                            break;
                        case "5":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("ACCT_PERIOD_FROM", "ACCT_PERIOD_FROM");
                            //bulkCopy.ColumnMappings.Add("ACCT_PERIOD_TO", "ACCT_PERIOD_TO");
                            //bulkCopy.ColumnMappings.Add("SALES_AMOUNT_ACHIEVEMENT", "SALES_AMOUNT_ACHIEVEMENT");
                            //bulkCopy.ColumnMappings.Add("SALES_AMOUNT_FORECAST", "SALES_AMOUNT_FORECAST");
                            bulkCopy.ColumnMappings.Add("OPERATING_PROFIT_ACHIEVEMENT", "OPERATING_PROFIT_ACHIEVEMENT");
                            bulkCopy.ColumnMappings.Add("OPERATING_PROFIT_ACHIEVEMENT_PERCENT", "OPERATING_PROFIT_ACHIEVEMENT_PERCENT");
                            bulkCopy.ColumnMappings.Add("OPERATING_PROFIT_FORECAST", "OPERATING_PROFIT_FORECAST");
                            bulkCopy.ColumnMappings.Add("OPERATING_PROFIT_FORECAST_PERCENT", "OPERATING_PROFIT_FORECAST_PERCENT");
                            bulkCopy.ColumnMappings.Add("SALES_TO_TMMIN_ACHIEVEMENT", "SALES_TO_TMMIN_ACHIEVEMENT");
                            bulkCopy.ColumnMappings.Add("SALES_TO_TMMIN_FORECAST", "SALES_TO_TMMIN_FORECAST");
                            bulkCopy.ColumnMappings.Add("SALES_TO_ADM_ACHIEVEMENT", "SALES_TO_ADM_ACHIEVEMENT");
                            bulkCopy.ColumnMappings.Add("SALES_TO_ADM_FORECAST", "SALES_TO_ADM_FORECAST");
                            bulkCopy.ColumnMappings.Add("SALES_TO_OTHER_ACHIEVEMENT", "SALES_TO_OTHER_ACHIEVEMENT");
                            bulkCopy.ColumnMappings.Add("SALES_TO_OTHER_FORECAST", "SALES_TO_OTHER_FORECAST");
                            bulkCopy.ColumnMappings.Add("ORDINARY_PROFIT_ACHIEVEMENT", "ORDINARY_PROFIT_ACHIEVEMENT");
                            bulkCopy.ColumnMappings.Add("ORDINARY_PROFIT_ACHIEVEMENT_PERCENT", "ORDINARY_PROFIT_ACHIEVEMENT_PERCENT");
                            bulkCopy.ColumnMappings.Add("ORDINARY_PROFIT_FORECAST", "ORDINARY_PROFIT_FORECAST");
                            bulkCopy.ColumnMappings.Add("ORDINARY_PROFIT_FORECAST_PERCENT", "ORDINARY_PROFIT_FORECAST_PERCENT");
                            break;
                        case "6":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("YEAR", "YEAR");
                            bulkCopy.ColumnMappings.Add("ENGINEERING", "ENGINEERING");
                            bulkCopy.ColumnMappings.Add("PRODUCTION_CONTROL", "PRODUCTION_CONTROL");
                            bulkCopy.ColumnMappings.Add("PRODUCTION_OPERATOR", "PRODUCTION_OPERATOR");
                            bulkCopy.ColumnMappings.Add("QUALITY_ASSURANCE", "QUALITY_ASSURANCE");
                            bulkCopy.ColumnMappings.Add("LABOR_SUPPLY", "LABOR_SUPPLY");
                            bulkCopy.ColumnMappings.Add("PLANT_DIRECT", "PLANT_DIRECT");
                            bulkCopy.ColumnMappings.Add("STATUS_PERMANENT", "STATUS_PERMANENT");
                            bulkCopy.ColumnMappings.Add("STATUS_TEMPORER", "STATUS_TEMPORER");
                            bulkCopy.ColumnMappings.Add("EDUCATION_S1_S2", "EDUCATION_S1_S2");
                            bulkCopy.ColumnMappings.Add("EDUCATION_D3", "EDUCATION_D3");
                            bulkCopy.ColumnMappings.Add("EDUCATION_HIGH_SCHOOL", "EDUCATION_HIGH_SCHOOL");
                            bulkCopy.ColumnMappings.Add("NATIONALITY_EXPATRIATE", "NATIONALITY_EXPATRIATE");
                            bulkCopy.ColumnMappings.Add("NATIONALITY_WNI", "NATIONALITY_WNI");
                            bulkCopy.ColumnMappings.Add("TOTAL_EMPLOYEE", "TOTAL_EMPLOYEE");
                            bulkCopy.ColumnMappings.Add("NON_PLANT", "NON_PLANT");
                            bulkCopy.ColumnMappings.Add("AVERAGE_AGE", "AVERAGE_AGE");
                            break;
                        case "7":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("COMMUNICATION_MEDIA", "COMMUNICATION_MEDIA");
                            bulkCopy.ColumnMappings.Add("UNION_NAME", "UNION_NAME");
                            bulkCopy.ColumnMappings.Add("SINGLE_PLURAL", "SINGLE_PLURAL");
                            bulkCopy.ColumnMappings.Add("BONUS", "BONUS");
                            bulkCopy.ColumnMappings.Add("SALARY_INCREASE", "SALARY_INCREASE");
                            bulkCopy.ColumnMappings.Add("HISTORY_STRIKE_1", "HISTORY_STRIKE_1");
                            bulkCopy.ColumnMappings.Add("HISTORY_STRIKE_2", "HISTORY_STRIKE_2");
                            bulkCopy.ColumnMappings.Add("HISTORY_STRIKE_3", "HISTORY_STRIKE_3");
                            //bulkCopy.ColumnMappings.Add("AVAILABILITY", "AVAILABILITY");
                            bulkCopy.ColumnMappings.Add("PENSION_PROGRAM", "PENSION_PROGRAM");
                            bulkCopy.ColumnMappings.Add("THR", "THR");
                            bulkCopy.ColumnMappings.Add("OVERTIME", "OVERTIME");
                            bulkCopy.ColumnMappings.Add("COLLECTIVE_LABOR_AGREEMENT", "COLLECTIVE_LABOR_AGREEMENT");
                            bulkCopy.ColumnMappings.Add("MEAL_TRANSPORT_ALLOWANCE", "MEAL_TRANSPORT_ALLOWANCE");
                            bulkCopy.ColumnMappings.Add("MEDICAL_ALLOWANCE", "MEDICAL_ALLOWANCE");
                            break;
                        case "8":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("TERMINAL_AMOUNT", "TERMINAL_AMOUNT");
                            bulkCopy.ColumnMappings.Add("ENGINEER", "ENGINEER");
                            bulkCopy.ColumnMappings.Add("INTERNET_BANDWITH", "INTERNET_BANDWITH");
                            bulkCopy.ColumnMappings.Add("OPERATOR", "OPERATOR");
                            bulkCopy.ColumnMappings.Add("PRINTER", "PRINTER");
                            bulkCopy.ColumnMappings.Add("CAM_COMPLETED", "CAM_COMPLETED");
                            bulkCopy.ColumnMappings.Add("PRINTER_SIZE", "PRINTER_SIZE");
                            bulkCopy.ColumnMappings.Add("CAD_TYPE", "CAD_TYPE");
                            break;
                        case "9":
                            bulkCopy.ColumnMappings.Add("PART_NAME", "PART_NAME");
                            bulkCopy.ColumnMappings.Add("COUNTRY_NAME", "COUNTRY_NAME");
                            bulkCopy.ColumnMappings.Add("SOURCE_NAME", "SOURCE_NAME");
                            break;
                        case "10":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("YEAR", "YEAR");
                            bulkCopy.ColumnMappings.Add("GRANTOR", "GRANTOR");
                            bulkCopy.ColumnMappings.Add("TITLE_AWARD", "TITLE_AWARD");
                            bulkCopy.ColumnMappings.Add("REMARK", "REMARK");
                            break;
                        case "11":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("PRODUCT_NAME", "PRODUCT_NAME");
                            bulkCopy.ColumnMappings.Add("AUTOMOTIVE_CUSTOMER_LIST", "AUTOMOTIVE_CUSTOMER_LIST");
                            bulkCopy.ColumnMappings.Add("START_PRODUCTION", "START_PRODUCTION");
                            bulkCopy.ColumnMappings.Add("MARKET_SHARE", "MARKET_SHARE");
                            bulkCopy.ColumnMappings.Add("BIGGEST_COMPETITOR", "BIGGEST_COMPETITOR");
                            bulkCopy.ColumnMappings.Add("SHARE_OF_COMPETITOR", "SHARE_OF_COMPETITOR");
                            break;
                        case "12":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("COMPANY_NAME", "COMPANY_NAME");
                            bulkCopy.ColumnMappings.Add("CONTENT_OF_COOPERATION", "CONTENT_OF_COOPERATION");
                            bulkCopy.ColumnMappings.Add("COMPANY_CATEGORY", "COMPANY_CATEGORY");
                            break;
                        case "13":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("MACHINE_NAME", "MACHINE_NAME");
                            bulkCopy.ColumnMappings.Add("DETAIL_TYPE", "DETAIL_TYPE");
                            bulkCopy.ColumnMappings.Add("TONAGE", "TONAGE");
                            bulkCopy.ColumnMappings.Add("UNIT", "UNIT");
                            bulkCopy.ColumnMappings.Add("BRAND_NAME", "BRAND_NAME");
                            bulkCopy.ColumnMappings.Add("QTY", "QTY");
                            bulkCopy.ColumnMappings.Add("YEAR_BUYING", "YEAR_BUYING");
                            bulkCopy.ColumnMappings.Add("YEAR_MAKING", "YEAR_MAKING");
                            bulkCopy.ColumnMappings.Add("REMARK", "REMARK");
                            break;
                        case "14":
                            bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                            bulkCopy.ColumnMappings.Add("COUNTRY_NAME", "COUNTRY_NAME");
                            bulkCopy.ColumnMappings.Add("MAJOR_PRODUCT", "MAJOR_PRODUCT");
                            bulkCopy.ColumnMappings.Add("VIA_TMMIN", "VIA_TMMIN");
                            bulkCopy.ColumnMappings.Add("SHARE_SALES", "SHARE_SALES");
                            break;
                        default: break;
                    }
                    #endregion

                    if (ds.Tables[0].Rows.Count <= 0)
                        throw new Exception("No Data On Excel");

                    bulkCopy.WriteToServer(ds.Tables[0]);
                }

                string columnNameList = GetColumnName();
                string validationResult = "";

                #region Validation Upload
                switch (ActiveTab)
                {
                    case "0": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_General", new object[] { _TableDestination.processID }); break;
                    case "1": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Address", new object[] { _TableDestination.processID }); break;
                    case "2": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_ContactPerson", new object[] { _TableDestination.processID }); break;
                    case "3": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_ShareHolder", new object[] { _TableDestination.processID }); break;
                    case "4": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Customer", new object[] { _TableDestination.processID }); break;
                    case "5": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Financial", new object[] { _TableDestination.processID }); break;
                    case "6": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_ManPower", new object[] { _TableDestination.processID }); break;
                    case "7": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Labor", new object[] { _TableDestination.processID }); break;
                    case "8": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Cadit", new object[] { _TableDestination.processID }); break;
                    case "9": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_PurchasedPart", new object[] { _TableDestination.processID }); break;
                    case "10": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Award", new object[] { _TableDestination.processID }); break;
                    case "11": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Product", new object[] { _TableDestination.processID }); break;
                    case "12": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_BusinessRelation", new object[] { _TableDestination.processID }); break;
                    case "13": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Equipment", new object[] { _TableDestination.processID }); break;
                    case "14": validationResult = db.ExecuteScalar<string>("Validate_SupplierProfile_Export", new object[] { _TableDestination.processID }); break;
                    default: break;
                }
                #endregion

                if (validationResult == "Error")
                {
                    result = "false;" + _TableDestination.processID;
                }
                else 
                {
                    result = "true;" + _TableDestination.processID;
                }
            }
            catch (Exception exc)
            {
                result = "exception;" +  exc.Message;
            }
            finally
            {
                db.Close();

                if(excelConn != null)
                    excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }

            return result;
        }

        public string ClearTempUpload(string processId, string ActiveTab)
        {
            IDBContext db = DbContext;
            string result = "Success";
            try
            {
                // clear data in temporary uploaded table.
                switch (ActiveTab)
                {
                    case "0": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_GENERAL_INFO" }); break;
                    case "1": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_ADDRESS" }); break;
                    case "2": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_CONTACT_PERSON" }); break;
                    case "3": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_SHAREHOLDER" }); break;
                    case "4": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_CUSTOMER" }); break;
                    case "5": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_FINANCIAL" }); break;
                    case "6": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_MAN_POWER" }); break;
                    case "7": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_LABOR" }); break;
                    case "8": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_CAD_IT" }); break;
                    case "9": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_PURCHASED_PART" }); break;
                    case "10": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_AWARD" }); break;
                    case "11": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_PRODUCT" }); break;
                    case "12": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_BUSINESS_RELATION" }); break;
                    case "13": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_EQUIPMENT" }); break;
                    case "14": db.Execute("Clear_SupplierProfile_Upload", new object[] { processId, "TB_T_SUPPLIER_EXPORT" }); break;
                    default: break;
                }
            }
            catch (Exception exc)
            {
                result = exc.Message;
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        [HttpPost]
        public void SPFileUpload_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("SPFileUpload", ValidationSettings, SPFileUploadConfirmation_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void SPFileUploadConfirmation_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Math.Abs(System.Environment.TickCount) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }
        #endregion

        #region UPLOAD CONFIRMATION - RESULT
        public string UploadValid(string processId, string ActiveTab)
        {
            IDBContext db = DbContext;
            string QueryResult = string.Empty;

            try
            {
                switch (ActiveTab)
                { 
                    case "0" : QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_General", new object[] { processId, AuthorizedUser.Username }); break;
                    case "1": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Adress", new object[] { processId, AuthorizedUser.Username }); break;
                    case "2": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_ContactPerson", new object[] { processId, AuthorizedUser.Username }); break;
                    case "3": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_ShareHolder", new object[] { processId, AuthorizedUser.Username }); break;
                    case "4": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Customer", new object[] { processId, AuthorizedUser.Username }); break;
                    case "5": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Financial", new object[] { processId, AuthorizedUser.Username }); break;
                    case "6": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_ManPower", new object[] { processId, AuthorizedUser.Username }); break;
                    case "7": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Labor", new object[] { processId, AuthorizedUser.Username }); break;
                    case "8": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Cadit", new object[] { processId, AuthorizedUser.Username }); break;
                    case "9": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_PurchasedPart", new object[] { processId, AuthorizedUser.Username }); break;
                    case "10": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Award", new object[] { processId, AuthorizedUser.Username }); break;
                    case "11": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Product", new object[] { processId, AuthorizedUser.Username }); break;
                    case "12": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_BusinessRelation", new object[] { processId, AuthorizedUser.Username }); break;
                    case "13": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Equipment", new object[] { processId, AuthorizedUser.Username }); break;
                    case "14": QueryResult = db.ExecuteScalar<string>("Update_SupplierProfile_Export", new object[] { processId, AuthorizedUser.Username }); break;
                    default: break;
                }
            }
            catch (Exception ex)
            {
                QueryResult = "Error: " + ex.Message;
            }
            finally
            {
                db.Close();
            }
            return QueryResult;
        }

        [HttpGet]
        public void DownloadInvalid(String processId, String functionId, String temporaryTableUpload, string ActiveTab)
        {
            string fileName = "";
            byte[] hasil = null;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IExcelWriter exporter = ExcelWriter.GetInstance();

            try
            {
                switch (ActiveTab)
                {
                    case "0":
                        fileName = "GeneralCreationUploadInvalid" + ".xls";
                        List<SupplierGeneralErrorSheet> general = db.Fetch<SupplierGeneralErrorSheet>("SupplierProfile_Error_General", new object[] { processId });
                        exporter.Append(general, "General");
                        break;
                    case "1":
                        fileName = "AddressCreationUploadInvalid" + ".xls";
                        List<SupplierAddressErrorSheet> address = db.Fetch<SupplierAddressErrorSheet>("SupplierProfile_Error_Address", new object[] { processId });
                        exporter.Append(address, "Address");
                        break;
                    case "2":
                        fileName = "ContactPersonCreationUploadInvalid" + ".xls";
                        List<SupplierContactPersonErrorSheet> contactperson = db.Fetch<SupplierContactPersonErrorSheet>("SupplierProfile_Error_ContactPerson", new object[] { processId });
                        exporter.Append(contactperson, "ContactPerson");
                        break;
                    case "3":
                        fileName = "ShareHolderCreationUploadInvalid" + ".xls";
                        List<SupplierShareHolderErrorSheet> shareholder = db.Fetch<SupplierShareHolderErrorSheet>("SupplierProfile_Error_ShareHolder", new object[] { processId });
                        exporter.Append(shareholder, "ShareHolder");
                        break;
                    case "4":
                        fileName = "CustomerCreationUploadInvalid" + ".xls";
                        List<SupplierCustomerErrorSheet> customer = db.Fetch<SupplierCustomerErrorSheet>("SupplierProfile_Error_Customer", new object[] { processId });
                        exporter.Append(customer, "Customer");
                        break;
                    case "5":
                        fileName = "FinancialCreationUploadInvalid" + ".xls";
                        List<SupplierFinancialErrorSheet> financial = db.Fetch<SupplierFinancialErrorSheet>("SupplierProfile_Error_Financial", new object[] { processId });
                        exporter.Append(financial, "Financial");
                        break;
                    case "6":
                        fileName = "ManPowerCreationUploadInvalid" + ".xls";
                        List<SupplierManPowerErrorSheet> manpower = db.Fetch<SupplierManPowerErrorSheet>("SupplierProfile_Error_ManPower", new object[] { processId });
                        exporter.Append(manpower, "ManPower");
                        break;
                    case "7":
                        fileName = "LaborCreationUploadInvalid" + ".xls";
                        List<SupplierLaborErrorSheet> labor = db.Fetch<SupplierLaborErrorSheet>("SupplierProfile_Error_Labor", new object[] { processId });
                        exporter.Append(labor, "Labor");
                        break;
                    case "8":
                        fileName = "CadITCreationUploadInvalid" + ".xls";
                        List<SupplierCADITErrorSheet> cadit = db.Fetch<SupplierCADITErrorSheet>("SupplierProfile_Error_CAD_IT", new object[] { processId });
                        exporter.Append(cadit, "Cadit");
                        break;
                    case "9":
                        fileName = "PurchasedPartCreationUploadInvalid" + ".xls";
                        List<SupplierPurchasedPartErrorSheet> purchasedpart = db.Fetch<SupplierPurchasedPartErrorSheet>("SupplierProfile_Error_PurchasedPart", new object[] { processId });
                        exporter.Append(purchasedpart, "PurchasedPart");
                        break;
                    case "10":
                        fileName = "AwardCreationUploadInvalid" + ".xls";
                        List<SupplierAwardErrorSheet> award = db.Fetch<SupplierAwardErrorSheet>("SupplierProfile_Error_Award", new object[] { processId });
                        exporter.Append(award, "Award");
                        break;
                    case "11":
                        fileName = "ProductCreationUploadInvalid" + ".xls";
                        List<SupplierProductErrorSheet> product = db.Fetch<SupplierProductErrorSheet>("SupplierProfile_Error_Product", new object[] { processId });
                        exporter.Append(product, "Product");
                        break;
                    case "12":
                        fileName = "BusinessRelationCreationUploadInvalid" + ".xls";
                        List<SupplierBusinessRelationErrorSheet> businessrelation = db.Fetch<SupplierBusinessRelationErrorSheet>("SupplierProfile_Error_BusinessRelation", new object[] { processId });
                        exporter.Append(businessrelation, "Business");
                        break;
                    case "13":
                        fileName = "EquipmentCreationUploadInvalid" + ".xls";
                        List<SupplierEquipmentErrorSheet> equipment = db.Fetch<SupplierEquipmentErrorSheet>("SupplierProfile_Error_Equipment", new object[] { processId });
                        exporter.Append(equipment, "Equipment");
                        break;
                    case "14":
                        fileName = "ExportCreationUploadInvalid" + ".xls";
                        List<SupplierExportErrorSheet> export = db.Fetch<SupplierExportErrorSheet>("SupplierProfile_Error_Export", new object[] { processId });
                        exporter.Append(export, "Export");
                        break;
                    default: break;
                }
            }
            catch (Exception e)
            {
                exporter.Flush();
                throw e;
            }

            hasil = exporter.Flush();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        #endregion

        #region DOWNLOAD DATA
        [HttpGet]
        public void DownloadSheet(string ActiveTab, string SupplierCode)
        {
            string fileName = "";
            byte[] resultExport = null;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplier(), "SUPP_CD", "SupplierProfile", "OIHSupplierOption");

            if (suppliers.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
            }

            IExcelWriter exporter = ExcelWriter.GetInstance();

            try
            {
                switch (ActiveTab)
                {
                    case "0":
                        fileName = "SupplierProfile_General.xls";
                        IEnumerable<SupplierGeneralSheet> listGeneral = db.Query<SupplierGeneralSheet>("GetSupplierProfileGeneralInfoSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listGeneral, "General");
                        break;
                    case "1":
                        fileName = "SupplierProfile_Address.xls";
                        IEnumerable<SupplierAddressSheet> listAdress = db.Query<SupplierAddressSheet>("GetSupplierProfileAddressSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listAdress, "Address");
                        break;
                    case "2":
                        fileName = "SupplierProfile_ContactPerson.xls";
                        IEnumerable<SupplierContactPersonSheet> listContact = db.Query<SupplierContactPersonSheet>("GetSupplierProfileContactPersonSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listContact, "ContactPerson");
                        break;
                    case "3":
                        fileName = "SupplierProfile_ShareHolder.xls";
                        IEnumerable<SupplierShareHolderSheet> listSH = db.Query<SupplierShareHolderSheet>("GetSupplierProfileShareHolderSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listSH, "ShareHolder");
                        break;
                    case "4":
                        fileName = "SupplierProfile_Customer.xls";
                        IEnumerable<SupplierCustomerSheet> listCustomer = db.Query<SupplierCustomerSheet>("GetSupplierProfileCustomerSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listCustomer, "Customer");
                        break;
                    case "5":
                        fileName = "SupplierProfile_Financial.xls";
                        IEnumerable<SupplierFinancialSheet> listFinancial = db.Query<SupplierFinancialSheet>("GetSupplierProfileFinancialSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listFinancial, "Financial");
                        break;
                    case "6":
                        fileName = "SupplierProfile_ManPower.xls";
                        IEnumerable<SupplierManPowerSheet> listManPower = db.Query<SupplierManPowerSheet>("GetSupplierProfileManPowerSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listManPower, "ManPower");
                        break;
                    case "7":
                        fileName = "SupplierProfile_Labor.xls";
                        IEnumerable<SupplierLaborSheet> listLabor = db.Query<SupplierLaborSheet>("GetSupplierProfileLaborSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listLabor, "Labor");
                        break;
                    case "8":
                        fileName = "SupplierProfile_Cadit.xls";
                        IEnumerable<SupplierCADITSheet> listCAD = db.Query<SupplierCADITSheet>("GetSupplierProfileCadITSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listCAD, "Cadit");
                        break;
                    case "9":
                        fileName = "SupplierProfile_PurchasedPart.xls";
                        IEnumerable<SupplierPurchasedPartSheet> listPurchased = db.Query<SupplierPurchasedPartSheet>("GetSupplierProfilePurchasedPartSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listPurchased, "PurchasedPart");
                        break;
                    case "10":
                        fileName = "SupplierProfile_Award.xls";
                        IEnumerable<SupplierAwardSheet> listAward = db.Query<SupplierAwardSheet>("GetSupplierProfileAwardSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listAward, "Award");
                        break;
                    case "11":
                        fileName = "SupplierProfile_Product.xls";
                        IEnumerable<SupplierProductSheet> listProduct = db.Query<SupplierProductSheet>("GetSupplierProfileProductSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listProduct, "Product");
                        break;
                    case "12":
                        fileName = "SupplierProfile_Business.xls";
                        IEnumerable<SupplierBusinessRelationSheet> listBusiness = db.Query<SupplierBusinessRelationSheet>("GetSupplierProfileBusinessRelationSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listBusiness, "Business");
                        break;
                    case "13":
                        fileName = "SupplierProfile_Equipment.xls";
                        IEnumerable<SupplierEquipmentSheet> listEquip = db.Query<SupplierEquipmentSheet>("GetSupplierProfileEquipmentSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listEquip, "Equipment");
                        break;
                    case "14":
                        fileName = "SupplierProfile_Export.xls";
                        IEnumerable<SupplierExportSheet> listExport = db.Query<SupplierExportSheet>("GetSupplierProfileExportSheet", new object[] { SupplierCode, SupplierCodeLDAP });
                        exporter.Append(listExport, "Export");
                        break;
                    default: break;
                }
            }
            catch (Exception e)
            {
                exporter.Flush();
                throw e;
            }

            resultExport = exporter.Flush();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(resultExport.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(resultExport);
            Response.End();
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        public FileContentResult DownloadTemplate(string ActiveTab)
        {
            string filePath = "";
            string fileName = "";
            byte[] documentBytes = null;

            switch (ActiveTab)
            {
                case "0":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/General_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "General_Template.xls";
                    break;
                case "1":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Address_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Address_Template.xls";
                    break;
                case "2":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/ContactPerson_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "ContactPerson_Template.xls";
                    break;
                case "3":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/ShareHolder_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "ShareHolder_Template.xls";
                    break;
                case "4":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Customer_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Customer_Template.xls";
                    break;
                case "5":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Financial_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Financial_Template.xls";
                    break;
                case "6":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/ManPower_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "ManPower_Template.xls";
                    break;
                case "7":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Labor_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Labor_Template.xls";
                    break;
                case "8":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Cadit_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Cadit_Template.xls";
                    break;
                case "9":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/PurchasedPart_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "PurchasedPart_Template.xls";
                    break;
                case "10":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Award_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Award_Template.xls";
                    break;
                case "11":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Product_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "ProductPart_Template.xls";
                    break;
                case "12":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/BusinessRelation_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "BusinessRelation_Template.xls";
                    break;
                case "13":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Equipment_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Equipment_Template.xls";
                    break;
                case "14":
                    filePath = Path.Combine(Server.MapPath("~/Views/SupplierProfile/UploadTemp/Export_Template.xls"));
                    documentBytes = StreamFile(filePath);
                    fileName = "Export_Template.xls";
                    break;
            }

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }
        #endregion
    }
}