﻿//created by : fid.goldy
//date : 28/06/2013
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Elmah;


namespace Portal.Controllers
{
    public class ErrorController : BaseController
    {

        public ErrorController()
            : base("Report")
        {
            PageLayout.UseMessageBoard = true;
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        { }
        //
        // GET: /Error/

        public void LogJavaScriptError(string message)
        {
            ErrorSignal.FromCurrentContext().Raise(new JavaScriptException(message));
        }

        public ActionResult HttpError()
        {
            return View("Error");
        }
        public ActionResult NotFound()
        {
            return View();
        }
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }


    }
}
