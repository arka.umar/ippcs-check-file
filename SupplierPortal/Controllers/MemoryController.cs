﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models;
using System.Text;

namespace Portal.Controllers
{
    public class MemoryController : Controller
    {
        //
        // GET: /Memory/

        public ActionResult Index()
        {
            return View("Memory");            
        }

        public string Del(string q = null)
        {
            if (string.IsNullOrEmpty(q))
            {
                List<string> k = new List<string>();
             
                foreach (var memi in Sing.Me.Mem)
                {
                    k.Add(memi.Key);
                    Sing.Me.Set(memi.Key, null);
                }
                return string.Join(",\r\n", k);
            }
            else
            {
                object o = Sing.Me.Get<object>(q);
                Sing.Me.Set(q, null);
                if (o is string)
                    return o as string;
                else
                    return o.AsJson();
            }
        }


        public string List(string q = null)
        {
            if (string.IsNullOrEmpty(q))
            {
                List<string> k = new List<string>();
                foreach (var item in Sing.Me.Mem)
                {
                    k.Add(item.Key);
                }
                
                return string.Join(",\r\n", k);
            }
            else
            {
                object o = Sing.Me.Get<object>(q);
                
                if (o is string)
                    return o as string;
                else
                    return o.AsJson();
            }
        }

        public string Count(string q)
        {
            if (string.IsNullOrEmpty(q))
            {
                StringBuilder b = new StringBuilder(); 
                List<string> k = new List<string>();
                foreach (var item in Sing.Me.Mem)
                {
                    if (item.Key.StartsWith("COUNT."))
                    {
                        b.Append(item.Key.Substring(6));
                        b.Append(":");
                        b.Append(Sing.Me.GetInt(item.Key));
                        b.Append(",");
                    }                        
                }
                return b.ToString();
            }
            else
            {
                int o = Sing.Me.GetInt("COUNT." + q);
                return Convert.ToString(o);               
            }
        }
    }
}
