﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using Portal.Models.WallAnnouncement;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Upload;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Messaging.Notification;
using System.Text;
using Portal.Models.WallAnnouncement;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.State;

namespace Portal.Controllers
{
    public class WallAnnouncementController: BaseController
    {
        public WallAnnouncementController() : base("Wall Announcement") { }
        private static string errorTextUpload;
       

        //Add by : FID.Goldy
        //protected override void Execute(System.Web.Routing.RequestContext requestContext)
        //{
        //    var binder = (DevExpressEditorsBinder)ModelBinders.Binders.DefaultBinder;
        //    //binder.UploadControlBinderSettings.ValidationSettings = UploadControlDemosHelper.ValidationSettings;
            
        //    var actionName = (string)requestContext.RouteData.Values["Action"];
        //    switch (actionName)
        //    {
        //        case "UploadCallback":
        //            binder.UploadControlBinderSettings.FileUploadCompleteHandler = WallAnnouncementController.updUploadFiles_FileUploadComplete;
                    
        //            break;
        //        //case "MultiSelectionImageUpload":
        //        //    binder.UploadControlBinderSettings.FileUploadCompleteHandler = UploadControlDemosHelper.ucMultiSelection_FileUploadComplete;
        //        //    break;
        //    }
        //    base.Execute(requestContext);
        //}
        //End of by : FID.goldy

        protected override void StartUp()
        {
            WallAnnouncementModel modelSupplier = new WallAnnouncementModel();
            WallAnnouncementModel modelNonSupplier = new WallAnnouncementModel();
            //model.Recipients = UserProvider.GetInstance().GetUsers();
            modelSupplier = AttachUploadedFiles(modelSupplier);
            Model.AddModel(modelSupplier);

            TempData["GridName"] = "ddlRecipient";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            modelSupplier.Recipients = db.Query<User>("getAllRecipients", new object[] {"external"}).ToList();

            modelNonSupplier.Recipients = db.Query<User>("getAllRecipients", new object[] { "internal" }).ToList();


            


            db.Close();

            ViewData["Recipients"] = modelSupplier.Recipients;
            ViewData["RecipientsNonSupplier"] = modelNonSupplier.Recipients;

        }

        protected override void Init()
        {
            
        }

        public ActionResult FormCallback()
        {
            WallAnnouncementModel model = new WallAnnouncementModel();
            model.Recipients = UserProvider.GetInstance().GetUsers();
            return PartialView("UploadHeader", model);
            //return View(model);
        }

        //Add & Modified by : FID.Goldy
        //Modified by : niit.yudha (12-sept-2013) - adding 2 parameter ValidFrom and ValidTo
        public ContentResult UploadWithoutAttachment(String ddlRecipient, String mmDescription, DateTime ValidFrom, DateTime ValidTo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string recipientUsernames = Request.Params["ddlRecipient"];
            string description = Request.Params["mmDescription"];

            string path = string.Format("IPPCS/{0}/{1}", DateTime.Now.Year, "Users");
            
            NotificationMessage notification;
            User user = SessionState.GetAuthorizedUser();
            string sender = user.Username;
            string errorMessage = String.Empty;
            List<NotificationMessage> notifications = new List<NotificationMessage>();
            
            string[] recipients;
            
            if (!string.IsNullOrEmpty(recipientUsernames))
            {
              
                if (recipientUsernames.Trim() == "All" )
                {
                    List<User> users = UserProvider.GetInstance().GetUsers();
                    recipients = new string[users.Count];
                    for (int i = users.Count - 1; i >= 0; i--)
                    {
                        recipients[i] = users[i].Username;
                    }


                }
                else
                {
                    recipients = recipientUsernames.Split(';');
                }

                for (int i = recipients.Length - 1; i >= 0; i--)
                {
                    recipients[i] = recipients[i].Trim();
                }

                if (string.IsNullOrEmpty(errorMessage))
                {
                    foreach (string rec in recipients)
                    {
                        notification = new NotificationMessage()
                        {
                            ActionUrl = "#",
                            Author = sender,
                            PostDate = DateTime.Now,
                            ValidTo = ValidTo,
                            ValidFrom = ValidFrom,
                            Recipient = rec,
                            Priority = PriorityLevel.Highest().Id,
                            Title = "You've got a Announcment " + sender
                        };

                        //attachmentLinks.Clear();
                        //attachmentLinks.Append("<ul>");
                        //foreach (UploadedFile file in files)
                        //{
                        //    fileInfo = new FileUpload()
                        //    {
                        //        FileName = file.FileName,
                        //        Description = description,
                        //        Group = "Users",
                        //        Key = sender,
                        //        Qualifier = rec,
                        //        Path = path
                        //    };
                        //    uploadFiles.Add(fileInfo);
                        //    attachmentLinks.Append("<li><a href='" + CreateAttachmentDownloadUrl(fileInfo.Path, fileInfo.FileName) + "' target='_blank'>" + fileInfo.FileName + "</a></li>");
                        //}
                        //attachmentLinks.Append("</ul>");
                        notification.Content = description;
                        notifications.Add(notification);

                        if (rec != "All")
                        {
                            var getEmail = db.SingleOrDefault<EmailRec>("GetRecEmail", new object[] { rec });

                            if (getEmail != null)
                            {
                                string EmailRec = getEmail.Email_Rec;
                                var QueryMail = db.Execute("SendWallEmail", new object[] { EmailRec, description });
                            }
                        }
                    }


                    //send email to helpdesk after loop email for supplier recipient
                    notification = new NotificationMessage()
                    {
                        ActionUrl = "#",
                        Author = sender,
                        PostDate = DateTime.Now,
                        ValidTo = ValidTo,
                        ValidFrom = ValidFrom,
                        Recipient = "ippcs.admin",
                        Priority = PriorityLevel.Highest().Id,
                        Title = "You've got a Announcment " + sender
                    };

                    notification.Content = description;
                    notifications.Add(notification);


                    var getEmailhelpdesk = db.SingleOrDefault<EmailRec>("GetRecEmail", new object[] { "ippcs.admin" });

                    if (getEmailhelpdesk != null)
                    {
                        string EmailRec = getEmailhelpdesk.Email_Rec;
                        var QueryMail = db.Execute("SendWallEmail", new object[] { EmailRec, description });
                    }
                    
                    //end of send email to  helpdesk



                    //FileUploadRegistry.GetInstance().Save(uploadFiles.ToArray(), user);
                    NotificationService.GetInstance().Save(notifications, user);

                }

                return Content("");
            } 
            else
            {
                return Content("emptyRecipients"); 
            } 
        }


        //public static void updUploadFiles_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        //{
        //    e.CallbackData = errorTextUpload;

        //}

        //public static readonly ValidationSettings ValidationSettings = new ValidationSettings
        //{
        //    AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".jpe", ".gif", ".txt",".exe" },
        //    MaxFileSize = 20971520
        //};

        public  void UploadCallback()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            DateTime validFrom = Convert.ToDateTime(EditorExtension.GetValue<string>("ValidFrom"));
            DateTime validTo = Convert.ToDateTime(EditorExtension.GetValue<string>("ValidTo"));

            string recipientUsernames = Request.Params["ddlRecipient"].Trim();
            string recipientInternal = Request.Params["ddlRecipientNonSupplier"].Trim();
            
            if(!string.IsNullOrEmpty(recipientUsernames) && !string.IsNullOrEmpty(recipientInternal))
            {
                recipientUsernames = recipientUsernames + ';' ;
            }

            recipientUsernames = recipientUsernames + recipientInternal;



            string description = Request.Params["mmDescription"];
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("updUploadFiles");
            

            if ((files == null) || (files.Length == 0))
            {
                
                //return Content("x");
               
                errorTextUpload = "x";
                
               
            }
            

            string[] recipients;
            //if (!string.IsNullOrEmpty(recipientUsernames))
            //{
            //    recipients = recipientUsernames.Split(',');
            //}
            //else
            //{
            //    List<User> users = UserProvider.GetInstance().GetUsers();
            //    recipients = new string[users.Count];
            //    for (int i = users.Count - 1; i >= 0; i--)
            //    {
            //        recipients[i] = users[i].Username;
            //    }
            //}

            //for (int i = recipients.Length - 1; i >= 0; i--)
            //{
            //    recipients[i] = recipients[i].Trim();
            //}

            string path = string.Format("IPPCS/{0}/{1}", DateTime.Now.Year, "Users");
            FTPUpload ftp = new FTPUpload();
            FTPSettings ftpSetting = ftp.Setting;
            FileUpload fileInfo;
            NotificationMessage notification;
            User user = SessionState.GetAuthorizedUser();
            string sender = user.Username;
            string errorMessage = String.Empty;
            List<NotificationMessage> notifications = new List<NotificationMessage>();
            List<FileUpload> uploadFiles = new List<FileUpload>();


            foreach (UploadedFile file in files)
            {
                errorMessage = String.Empty;
                ftp.FtpUpload(path, file.FileName, file.FileBytes, ref errorMessage);
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    break;
                }
            }
                        
            StringBuilder attachmentLinks = new StringBuilder();

            if (!string.IsNullOrEmpty(recipientUsernames))
            {

                if (recipientUsernames.Trim() == "All")
                {
                    List<User> users = UserProvider.GetInstance().GetUsers();
                    recipients = new string[users.Count];
                    for (int i = users.Count - 1; i >= 0; i--)
                    {
                        recipients[i] = users[i].Username;
                    } 
                }
                else
                {
                    recipients = recipientUsernames.Split(';');
                }

                for (int i = recipients.Length - 1; i >= 0; i--)
                {
                    recipients[i] = recipients[i].Trim();
                }

                if (string.IsNullOrEmpty(errorMessage))
                {
                    foreach (string rec in recipients)
                    {
                        notification = new NotificationMessage()
                        {
                            ActionUrl = "#",
                            Author = sender,
                            PostDate = DateTime.Now,
                            Recipient = rec,
                            Priority = PriorityLevel.Highest().Id,
                            Title = "You've got a Announcment " + sender,
                            ValidFrom = validFrom,
                            ValidTo = validTo
                        };

                        attachmentLinks.Clear();
                        attachmentLinks.Append("<ul>");
                        foreach (UploadedFile file in files)
                        {
                            fileInfo = new FileUpload()
                            {
                                FileName = file.FileName,
                                Description = description,
                                Group = "Users",
                                Key = sender,
                                Qualifier = rec,
                                Path = path
                            };
                            uploadFiles.Add(fileInfo);
                            attachmentLinks.Append("<li><a href='" + CreateAttachmentDownloadUrl(fileInfo.Path, fileInfo.FileName) + "' target='_blank'>" + fileInfo.FileName + "</a></li>");
                        }
                        attachmentLinks.Append("</ul>");
                        notification.Content = description + "<br/>- - - - - - <br/><i>Please open : <br/>" + attachmentLinks.ToString() + "</i>";
                        notifications.Add(notification);

                        if (rec != "All")
                        {
                            var getEmail = db.SingleOrDefault<EmailRec>("GetRecEmail", new object[] { rec });

                            if (getEmail != null)
                            {
                                string EmailRec = getEmail.Email_Rec;
                                var QueryMail = db.Execute("SendWallEmail", new object[] { EmailRec, description });
                            }
                        }
                    }

                    //send email to helpdesk
                    notification = new NotificationMessage()
                    {
                        ActionUrl = "#",
                        Author = sender,
                        PostDate = DateTime.Now,
                        Recipient = "ippcs.admin",
                        Priority = PriorityLevel.Highest().Id,
                        Title = "You've got a Announcment " + sender,
                        ValidFrom = validFrom,
                        ValidTo = validTo
                    };

                    attachmentLinks.Clear();
                    attachmentLinks.Append("<ul>");
                    foreach (UploadedFile file in files)
                    {
                        fileInfo = new FileUpload()
                        {
                            FileName = file.FileName,
                            Description = description,
                            Group = "Users",
                            Key = sender,
                            Qualifier = "ippcs.admin",
                            Path = path
                        };
                        uploadFiles.Add(fileInfo);
                        attachmentLinks.Append("<li><a href='" + CreateAttachmentDownloadUrl(fileInfo.Path, fileInfo.FileName) + "' target='_blank'>" + fileInfo.FileName + "</a></li>");
                    }
                    attachmentLinks.Append("</ul>");
                    notification.Content = description + "<br/>- - - - - - <br/><i>Please open : <br/>" + attachmentLinks.ToString() + "</i>";
                    notifications.Add(notification);

                    
                    var getEmailhelpdesk = db.SingleOrDefault<EmailRec>("GetRecEmail", new object[] { "ippcs.admin" });

                    if (getEmailhelpdesk != null)
                    {
                        string EmailRec = getEmailhelpdesk.Email_Rec;
                        var QueryMail = db.Execute("SendWallEmail", new object[] { EmailRec, description });
                    }
                    
                    //end of send email to helpdesk


                    FileUploadRegistry.GetInstance().Save(uploadFiles.ToArray(), user);
                    NotificationService.GetInstance().Save(notifications, user);

                    
                    
                }

                //return Content("");
                errorTextUpload = "";
            }
            //if (string.IsNullOrEmpty(errorMessage))
            //{
            //    foreach (string rec in recipients)
            //    {                    
            //        notification = new NotificationMessage()
            //        {
            //            ActionUrl = "#",
            //            Author = sender,
            //            PostDate = DateTime.Now,
            //            Recipient = rec,
            //            Priority = PriorityLevel.Highest().Id,
            //            Title = "You've got a file from " + sender
            //        };

            //        attachmentLinks.Clear();
            //        attachmentLinks.Append("<ul>");
            //        foreach (UploadedFile file in files)
            //        {
            //            fileInfo = new FileUpload()
            //            {
            //                FileName = file.FileName,
            //                Description = description,
            //                Group = "Users",
            //                Key = sender,
            //                Qualifier = rec,
            //                Path = path
            //            };
            //            uploadFiles.Add(fileInfo);
            //            attachmentLinks.Append("<li><a href='" + CreateAttachmentDownloadUrl(fileInfo.Path, fileInfo.FileName) + "' target='_blank'>" + fileInfo.FileName + "</a></li>");
            //        }
            //        attachmentLinks.Append("</ul>");
            //        notification.Content = description + "<br/>- - - - - - <br/><i>Please open : <br/>" + attachmentLinks.ToString() + "</i>";
            //        notifications.Add(notification);
            //    }

            //    FileUploadRegistry.GetInstance().Save(uploadFiles.ToArray(), user);
            //    NotificationService.GetInstance().Save(notifications, user);
            //}            

            //return null;

            else
            {
                //return Content("emptyRecipients");
                errorTextUpload = "emptyRecipients";

            }
        }

        //End of  Add & Modified by : FID.Goldy


        public ActionResult RecipientListCallback()
        {
            //WallAnnouncementModel model = new WallAnnouncementModel();
            //model.Recipients = UserProvider.GetInstance().GetUsers();
            //return PartialView("RecipientList", model);
            
            TempData["GridName"] = "ddlRecipient";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string category = "external"; //external from tmmin

            WallAnnouncementModel model = new WallAnnouncementModel();
            model.Recipients = db.Query<User>("getAllRecipients", new object[] {category}).ToList(); 

            db.Close();

            ViewData["Recipients"] = model.Recipients;

            return PartialView("GridLookup/PartialGrid", ViewData["Recipients"]);
        }


        public ActionResult RecipientListNonSupplierCallback()
        {
            
            TempData["GridName"] = "ddlRecipientNonSupplier";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string category = "internal"; //internal from tmmin

            WallAnnouncementModel model = new WallAnnouncementModel();
            model.Recipients = db.Query<User>("getAllRecipients", new object[] { category }).ToList();

            db.Close();

            ViewData["RecipientsNonSupplier"] = model.Recipients;

            return PartialView("GridLookup/PartialGrid", ViewData["RecipientsNonSupplier"]);
        }


        public ActionResult UploadedFilesGridCallback()
        {
            return PartialView("UploadedFilesGrid", AttachUploadedFiles(new WallAnnouncementModel()));
        }

        private WallAnnouncementModel AttachUploadedFiles(WallAnnouncementModel model)
        {
            model.UploadedFiles = FileUploadRegistry.GetInstance().List(new FileUploadCategory() { Group = "Users", Key = SessionState.GetAuthorizedUser().Username });
            FTPUpload ftp = new FTPUpload();
            FTPSettings ftpSetting = ftp.Setting;
            IDictionary<string, FileUpload> filesMap = new Dictionary<string, FileUpload>();
            FileUpload existingFile;
            bool addNewMapping;
            string mappingKey;
            string mappingKey2;
            foreach (FileUpload f in model.UploadedFiles)
            {
                addNewMapping = true;
                //f.Path = "ftp://" + ftpSetting.IP + "/" + f.Path + "/" + f.FileName;
                f.Path = CreateAttachmentDownloadUrl(f.Path, f.FileName);
                
                mappingKey = f.Path + f.Description;                
                if (filesMap.ContainsKey(mappingKey))
                {
                    existingFile = filesMap[mappingKey];
                    existingFile.Qualifier += ", " + f.Qualifier;
                    addNewMapping = false;
                }
                if (addNewMapping)
                {
                    filesMap.Add(mappingKey, f);
                }   
            }



            //Add By: FID.Goldy
            User user = SessionState.GetAuthorizedUser();
            List<NotificationMessage> notificationModel = new List<NotificationMessage>();
            //notificationModel = NotificationService.GetInstance().List();
            notificationModel = NotificationService.GetInstance().ListByUser(user);
            FileUpload fp = new FileUpload();


            if (notificationModel != null)
            {
                foreach (var item in notificationModel)
                {
                    fp = new FileUpload();
                    mappingKey2 = item.Title + item.Content + item.Author;
                    if (filesMap.ContainsKey(mappingKey2))
                    {

                    }
                    fp.Description = item.Content;
                    fp.Qualifier = item.Recipient;
                    fp.CreationDate = item.PostDate;
                    fp.Path = "No Attachment";



                    filesMap.Add("No Attachment - " + item.Id, fp);
                    //filesMap.Add(mappingKey2, fp);


                }

            }

            model.UploadedFiles = new List<FileUpload>();
            model.UploadedFiles.AddRange(filesMap.Values);
            model.UploadedFiles.Sort(new NotificationAndFileUploadPriorityComparer());

           

            return model;
        }

    }

    public class NotificationAndFileUploadPriorityComparer : Comparer<FileUpload>
    {
        public override int Compare(FileUpload x, FileUpload y)
        {
            return y.CreationDate.CompareTo(x.CreationDate);
            //return y.CreationDate.Subtract(x.CreationDate).TotalMilliseconds;
        }
    }
    //End Add By : FID.Goldy
}