﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Threading; 
using System.Data;
using System.Xml;

using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Excel;

using Portal.Models.Supplier;
using Portal.Models.SupplierFeedbackPart;
using Portal.Models.SupplierFeedbackSummary;

/*
 * Description      : Controller for Forecast
 * Created by/date  : fid.walim/03-jan-2013
 * Changed by/date  : fid.salman/25-peb-2013
 */

namespace Portal.Controllers
{
    public class SupplierFeedbackSummaryController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public SupplierFeedbackSummaryController()
            : base("Supplier Feedback Summary")
        {
            PageLayout.UseMessageBoard = true;
        }

        #region Model properties
        List<SupplierName> _supplierFeedbackSummarySupplierModel = null;
        private List<SupplierName> _SupplierFeedbackSummarySupplierModel
        {
            get
            {
                if (_supplierFeedbackSummarySupplierModel == null)
                    _supplierFeedbackSummarySupplierModel = new List<SupplierName>();

                return _supplierFeedbackSummarySupplierModel;
            }
            set
            {
                _supplierFeedbackSummarySupplierModel = value;
            }
        }

        List<SupplierPlan> _supplierFeedbackSummarySupplierPlantModel = null;
        private List<SupplierPlan> _SupplierFeedbackSummarySupplierPlantModel
        {
            get
            {
                if (_supplierFeedbackSummarySupplierPlantModel == null)
                    _supplierFeedbackSummarySupplierPlantModel = new List<SupplierPlan>();

                return _supplierFeedbackSummarySupplierPlantModel;
            }
            set
            {
                _supplierFeedbackSummarySupplierPlantModel = value;
            }
        }

        SupplierFeedbackSummaryModel _supplierFeedbackSummaryModel = null;
        private SupplierFeedbackSummaryModel _SupplierFeedbackSummaryModel
        {
            get
            {
                if (_supplierFeedbackSummaryModel == null)
                    _supplierFeedbackSummaryModel = new SupplierFeedbackSummaryModel();

                return _supplierFeedbackSummaryModel;
            }
            set
            {
                _supplierFeedbackSummaryModel = value;
            }
        }

        SupplierFeedbackSummaryParameter _supplierFeedbackSummaryParameterModel = null;
        private SupplierFeedbackSummaryParameter _SupplierFeedbackSummaryParameterModel
        {
            get
            {
                if (_supplierFeedbackSummaryParameterModel == null)
                    _supplierFeedbackSummaryParameterModel = new SupplierFeedbackSummaryParameter();

                return _supplierFeedbackSummaryParameterModel;
            }
            set
            {
                _supplierFeedbackSummaryParameterModel = value;
            }
        }

        SupplierFeedbackPartModel _supplierFeedbackPartModel = null;
        private SupplierFeedbackPartModel _SupplierFeedbackPartModel
        {
            get
            {
                if (_supplierFeedbackPartModel == null)
                    _supplierFeedbackPartModel = new SupplierFeedbackPartModel();

                return _supplierFeedbackPartModel;
            }
            set
            {
                _supplierFeedbackPartModel = value;
            }
        }

        SupplierFeedbackPartStatus _supplierFeedbackPartStatusModel = null;
        private SupplierFeedbackPartStatus _SupplierFeedbackPartStatusModel
        {
            get
            {
                if (_supplierFeedbackPartStatusModel == null)
                    _supplierFeedbackPartStatusModel = new SupplierFeedbackPartStatus();

                return _supplierFeedbackPartStatusModel;
            }
            set
            {
                _supplierFeedbackPartStatusModel = value;
            }
        }

        SupplierFeedbackPartParameter _supplierFeedbackPartParameterModel = null;
        private SupplierFeedbackPartParameter _SupplierFeedbackPartParameterModel
        {
            get
            {
                if (_supplierFeedbackPartParameterModel == null)
                    _supplierFeedbackPartParameterModel = new SupplierFeedbackPartParameter();

                return _supplierFeedbackPartParameterModel;
            }
            set
            {
                _supplierFeedbackPartParameterModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {}

        protected override void Init()
        {}

        #region Supplier Feedback Summary controller
        #region View controller
        public ActionResult PartialHeaderSupplierFeedbackSummary()
        {
            // get current month production month feedback summary
            DateTime currentMonth = DateTime.Now;
            String currentProductionMonth = Convert.ToString(currentMonth.Year) + String.Format("{0:D2}", (currentMonth.Month));

            #region Required model in partial page : for SupplierFeedbackSummaryHeaderPartial            
            _SupplierFeedbackSummaryParameterModel.PRODUCTION_MONTH = String.IsNullOrEmpty(Request.Params["ProductionMonth"]) ? GetGeneralProductionMonth(currentProductionMonth) : GetGeneralProductionMonth(Request.Params["ProductionMonth"]);
            _SupplierFeedbackSummaryParameterModel.VERSION = Request.Params["Timing"];
            _SupplierFeedbackSummaryParameterModel.SUPPLIER_CODE = Request.Params["SupplierCode"];
            _SupplierFeedbackSummaryParameterModel.SUPPLIER_PLANT_CD = Request.Params["SupplierPlantCode"];
            Model.AddModel(_SupplierFeedbackSummaryParameterModel);

            _SupplierFeedbackSummarySupplierModel = GetAllSupplier();
            _SupplierFeedbackSummarySupplierPlantModel = GetAllSupplierPlan(String.Empty);

            Model.AddModel(_SupplierFeedbackSummarySupplierModel);
            Model.AddModel(_SupplierFeedbackSummarySupplierPlantModel);
            #endregion

            return PartialView("SupplierFeedbackSummaryHeaderPartial", Model);
        }

        public ActionResult PartialHeaderSupplierPlantLookupGridSupplierFeedbackSummary()
        {

            #region Required model in partial page : for SFSHSupplierPlanCodeOption GridLookup
            TempData["GridName"] = "SFSHSupplierPlantCodeOption";
            _SupplierFeedbackSummarySupplierPlantModel = GetAllSupplierPlan(Request.Params["SupplierCode"]);
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierFeedbackSummarySupplierPlantModel);
        }

        public ActionResult PartialHeaderSupplierLookupGridSupplierFeedbackSummary()
        {
            #region Required model in partial page : for SFSHSupplierIDOption GridLookup
            TempData["GridName"] = "SFSHSupplierIDOption";
            _SupplierFeedbackSummarySupplierModel = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierFeedbackSummarySupplierModel);
        }

        public ActionResult PartialDetailSupplierFeedbackSummary()
        {
            #region Required model in partial page : for SupplierFeedbackSummaryDetailPartial
            // get current month production month feedback summary
            DateTime currentMonth = DateTime.Now;
            String currentProductionMonth = Convert.ToString(currentMonth.Year) + String.Format("{0:D2}", (currentMonth.Month));

            _SupplierFeedbackSummaryModel = GetAllFeedbackSummary(
                String.IsNullOrEmpty(Request.Params["ProductionMonth"]) ? currentProductionMonth : Request.Params["ProductionMonth"], 
                Request.Params["SupplierCode"], 
                Request.Params["SupplierPlantCode"], 
                Request.Params["Timing"]);
            Model.AddModel(_SupplierFeedbackSummaryModel);
            #endregion

            return PartialView("SupplierFeedbackSummaryDetailPartial", Model);
        }
        #endregion

        #region Database controller
        private SupplierFeedbackSummaryModel GetAllFeedbackSummary(String productionMonth, String supplierCode, String supplierPlanCode, String timing)
        {
            IDBContext db = DbContext;

            SupplierFeedbackSummaryModel supplierFeedbackSummaryModel = new SupplierFeedbackSummaryModel();
            supplierFeedbackSummaryModel.SupplierFeedbackSummary = db.Fetch<SupplierFeedbackSummary>("GetAllFeedbackSummary", new object[] { productionMonth, supplierCode, supplierPlanCode, timing });
            db.Close();

            return supplierFeedbackSummaryModel;
        }

        private List<SupplierName> GetAllSupplier()
        {
            IDBContext db = DbContext;

            List<SupplierName> supplierFeedbackSummarySupplierModelList = new List<SupplierName>();
            supplierFeedbackSummarySupplierModelList = db.Fetch<SupplierName>("GetAllSupplierForecast", new object[] { "" });
            db.Close();

            return supplierFeedbackSummarySupplierModelList;
        }

        private List<SupplierPlan> GetAllSupplierPlan(String supplierCode)
        {
            IDBContext db = DbContext;

            List<SupplierPlan> supplierFeedbackSummarySupplierPlantModelList = new List<SupplierPlan>();
            supplierFeedbackSummarySupplierPlantModelList = db.Fetch<SupplierPlan>("GetAllSupplierPlantForecast", new object[] { supplierCode });
            db.Close();

            return supplierFeedbackSummarySupplierPlantModelList;
        }
        #endregion

        #region Common controller
        private String GetGeneralProductionMonth(String productionMonth)
        {
            Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
            monthDictionary.Add("01" , "January");
            monthDictionary.Add("02" , "February");
            monthDictionary.Add("03" , "March");
            monthDictionary.Add("04" , "April");
            monthDictionary.Add("05" , "May");
            monthDictionary.Add("06" , "June");
            monthDictionary.Add("07" , "July");
            monthDictionary.Add("08" , "August");
            monthDictionary.Add("09" , "September");
            monthDictionary.Add("10" , "October");
            monthDictionary.Add("11" , "November");
            monthDictionary.Add("12" , "December");

            String generalProductionMonth = "";

            if (!String.IsNullOrEmpty(productionMonth))
            {
                String year = productionMonth.Substring(0, 4);
                String month = productionMonth.Substring(productionMonth.Length - 2, 2);

                generalProductionMonth = monthDictionary[month] + " " + year;
            }

            return generalProductionMonth;
        }
        #endregion

        #region Download Controller
        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /// <summary>
        /// Download feedback part data
        /// </summary>
        /// <param name="partNo"></param>
        /// <param name="productionMonth"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        public void DownloadFeedbackPart(String productionMonth, String supplierCode, String supplierPlanCode, String version, String partNo = "")
        {
            string fileName = "SupplierFeedbackPart" + (version == "T" ? "Tentative" : "Firm") + productionMonth + supplierCode + supplierPlanCode + ".xls";

            // Update download user and status
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportGetAllFeedbackPartModel> qry = db.Fetch<ReportGetAllFeedbackPartModel>("ReportGetAllFeedbackPart", new object[] { productionMonth, supplierCode, supplierPlanCode, version, partNo });

            IExcelWriter exporter = new NPOIWriter();

            byte[] hasil = exporter.Write(ConvertToDataTable(qry), "SupplierFeedbackData");

            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        /// <summary>
        /// Download feedback summary data
        /// </summary>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        //public void DownloadFeedbackSummary(String productionMonth, String supplierCode, String supplierPlanCode, String timing)
        //{
        //    string fileName = String.Empty;

        //    // Update download user and status
        //    IDBContext db = DbContext;
        //    IEnumerable<SupplierFeedbackSummaryDownload> qry;

        //    fileName = "SupplierFeedbackSummary" + productionMonth + supplierCode + supplierPlanCode + ".xls";
        //    qry = db.Fetch<SupplierFeedbackSummaryDownload>("GetAllFeedbackSummaryDownload", new object[] { productionMonth, supplierCode, supplierPlanCode, timing });

        //    IExcelWriter exporter = ExcelWriter.GetInstance();
        //    db.Close();
            
        //    byte[] hasil = exporter.Write(ConvertToDataTable(qry), "SupplierFeedbackSummary");
            
        //    Response.Clear();
        //    //Response.ContentType = result.MimeType;
        //    Response.Cache.SetCacheability(HttpCacheability.Private);
        //    Response.Expires = -1;
        //    Response.Buffer = true;
        //    /*
        //     * @Lufty: numpang nambahin, kisanak
        //     */
        //    Response.ContentType = "application/octet-stream";
        //    Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
        //    /* ------------------------------*/
        //    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
        //    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

        //    Response.BinaryWrite(hasil);
        //    Response.End();
        //}

        public void DownloadFeedbackSummary(String productionMonth, String supplierCode, String supplierPlanCode, String timing)
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;


            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierFeedbackSummary\DownloadFeedbackSummary.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlDataSource.SelectCommand = "SELECT tbl.SUPPLIER_CODE, tbl.SUPPLIER_NAME, tbl.PRODUCTION_MONTH , tbl.TIMING, tbl.SUPPLIER_PLANT_CODE, tbl.N_MONTH, tbl.N_1_MONTH, " +
                                          "tbl.N_2_MONTH, tbl.N_3_MONTH, tbl.IS_CONFIRMED, tbl.LOCK_STATUS, tbl.REPLY_BY, tbl.REPLY_DT " +
                                          "FROM (SELECT FS.[SUPPLIER_CODE], S.[SUPPLIER_NAME], FS.[PRODUCTION_MONTH], FS.[TIMING], FS.[SUPPLIER_PLANT_CODE], FS.[N_MONTH], FS.[N_1_MONTH] " +
                                          ", FS.[N_2_MONTH], FS.[N_3_MONTH] " +
	                                      ",(CASE WHEN N_MONTH IS NOT NULL AND N_1_MONTH IS NOT NULL AND N_2_MONTH IS NOT NULL AND N_3_MONTH IS NOT NULL THEN 1 ELSE 0 END) AS IS_CONFIRMED " +
                                          ",(CASE WHEN FS.[TIMING] = 'T' THEN MF.[T_LOCK_STATUS] WHEN FS.[TIMING] = 'F' THEN MF.[F_LOCK_STATUS] END) AS LOCK_STATUS " +
                                          ",(CASE WHEN FS.[TIMING] = 'T' THEN MF.[T_FEEDBACK_USER] WHEN FS.[TIMING] = 'F' THEN MF.[F_FEEDBACK_USER] END) AS REPLY_BY " +
                                          ",(CASE WHEN FS.[TIMING] = 'T' THEN (SELECT SUBSTRING(CONVERT(VARCHAR(19), MF.[T_FEEDBACK_DT], 120), 0, 5) + '.' +  " +
	                                                                           "SUBSTRING(CONVERT(VARCHAR(19), MF.[T_FEEDBACK_DT], 120), 6, 2) + '.' +  " +
	                                                                           "SUBSTRING(CONVERT(VARCHAR(19), MF.[T_FEEDBACK_DT], 120), 9, 2) + ' ' +  " +
                                                                               "SUBSTRING(CONVERT(VARCHAR(19), MF.[T_FEEDBACK_DT], 120), 12, 2) + ':' + " +
                                                                               "SUBSTRING(CONVERT(VARCHAR(19), MF.[T_FEEDBACK_DT], 120), 15, 2)) WHEN FS.[TIMING] = 'F' THEN (SELECT SUBSTRING(CONVERT(VARCHAR(19), MF.[F_FEEDBACK_DT], 120), 0, 5) + '.' +  " +
                                                                               "SUBSTRING(CONVERT(VARCHAR(19), MF.[F_FEEDBACK_DT], 120), 6, 2) + '.' +  " +
                                                                               "SUBSTRING(CONVERT(VARCHAR(19), MF.[F_FEEDBACK_DT], 120), 9, 2) + ' ' +  " +
                                                                               "SUBSTRING(CONVERT(VARCHAR(19), MF.[F_FEEDBACK_DT], 120), 12, 2) + ':' + " +
                                                                               "SUBSTRING(CONVERT(VARCHAR(19), MF.[F_FEEDBACK_DT], 120), 15, 2)) END) AS REPLY_DT " +
                                          ",ISNULL((CAST(N_MONTH AS INT) + CAST(N_1_MONTH AS INT) + CAST(N_2_MONTH AS INT) + CAST(N_3_MONTH AS INT)), 3) AS TOTALOKNG " +
                                          "FROM [dbo].[TB_R_FEEDBACK_SUMMARY] FS " +
                                          "JOIN [dbo].[TB_M_SUPPLIER] S ON " +
	                                      "  FS.[SUPPLIER_CODE] = S.[SUPPLIER_CODE] AND " +
	                                      "  FS.[SUPPLIER_PLANT_CODE] = S.[SUPPLIER_PLANT_CD] " +
                                          "JOIN [dbo].[TB_R_MONTHLY_FORECAST] MF ON " +
	                                      "  FS.[PRODUCTION_MONTH] = MF.[PRODUCTION_MONTH] AND " +
	                                      "  FS.[SUPPLIER_CODE] = MF.[SUPPLIER_CD] AND " +
	                                      "  FS.[SUPPLIER_PLANT_CODE] = MF.[SUPPLIER_PLANT] " +
                                          "WHERE " +
                                          "  ((FS.[PRODUCTION_MONTH]='" + productionMonth + "' AND isnull('" + productionMonth + "','') <> '') or (isnull('" + productionMonth + "','') = '')) AND  " +
                                          "  ((FS.[SUPPLIER_CODE]='" + supplierCode + "' AND isnull('" + supplierCode + "','') <> '') or (isnull('" + supplierCode + "','') = '')) AND  " +
                                          "  ((FS.[SUPPLIER_PLANT_CODE]='" + supplierPlanCode + "' AND isnull('" + supplierPlanCode + "','') <> '') or (isnull('" + supplierPlanCode + "','') = '')) AND " +
                                          "  ((FS.[TIMING]='" + timing + "' AND isnull('" + timing + "','') <> '') or (isnull('" + timing + "','') = ''))  " +
                                          ") tbl ORDER BY tbl.LOCK_STATUS ASC, tbl.TOTALOKNG DESC";

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "SupplierFeedbackSummary" + productionMonth + supplierCode + supplierPlanCode + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
            rpt.Dispose();
        }

        #endregion
        #endregion

        #region Supplier Feedback Part controller
        #region View controller
        public ActionResult PartialPopupForecastFeedbackPart()
        {
            #region Required model in partial page
            #region Model for ForecastFeedbackPartHeaderLeftPanel
            _SupplierFeedbackPartParameterModel.PRODUCTION_MONTH = Request.Params["ProductionMonth"];
            _SupplierFeedbackPartParameterModel.SUPPLIER_CODE = Request.Params["SupplierCode"];
            _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD = Request.Params["SupplierPlanCode"];
            _SupplierFeedbackPartParameterModel.VERSION = Request.Params["Timing"];
            _SupplierFeedbackPartParameterModel.PARENT = "0";
            Model.AddModel(_SupplierFeedbackPartParameterModel);
            #endregion

            #region Model for SupplierFeedbackPartRightHeaderPartial
            _SupplierFeedbackPartStatusModel = GetSupplierFeedbackStatus(_SupplierFeedbackPartParameterModel.PRODUCTION_MONTH, _SupplierFeedbackPartParameterModel.SUPPLIER_CODE, _SupplierFeedbackPartParameterModel.VERSION, _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD);
            Model.AddModel(_SupplierFeedbackPartStatusModel);
            #endregion

            #region Model for SFPGFSupplierFeedbackPartView
            _SupplierFeedbackPartModel = GetAllFeedbackPart(String.Empty, _SupplierFeedbackPartParameterModel.PRODUCTION_MONTH, _SupplierFeedbackPartParameterModel.SUPPLIER_CODE, _SupplierFeedbackPartParameterModel.SUPPLIER_PLANT_CD, _SupplierFeedbackPartParameterModel.VERSION);
            Model.AddModel(_SupplierFeedbackPartModel);
            #endregion
            #endregion

            return PartialView("SupplierFeedbackSummaryFeedbackPartPartial", Model);
        }

        public ActionResult PartialEmptyPopupForecastFeedbackPart()
        {
            Model.AddModel(_SupplierFeedbackPartParameterModel);
            Model.AddModel(_SupplierFeedbackPartModel);

            return PartialView("SupplierFeedbackSummaryFeedbackPartPartial", Model);
        }
        #endregion

        #region Database controller
        private SupplierFeedbackPartModel GetAllFeedbackPart(String partNo, String productionMonth, String supplierCode, String supplierPlanCode, String version)
        {
            IDBContext db = DbContext;

            SupplierFeedbackPartModel feedbackPartModel = new SupplierFeedbackPartModel();

            //This Section Doesn't need Lock Status, so Lock Status = 0
            //By Fid.Reggy
            string LockStatus = "0";

            var QueryLog = db.Fetch<SupplierFeedbackPartDetail>("GetAllFeedbackPart", new object[] { productionMonth, supplierCode, supplierPlanCode, version, LockStatus, partNo });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    feedbackPartModel.SupplierFeedbackParts.Add(new SupplierFeedbackPartDetail()
                    {
                        PART_NO = q.PART_NO,
                        SUPPLIER_CD = q.SUPPLIER_CD,
                        S_PLANT_CD = q.S_PLANT_CD,
                        DOCK_CD = q.DOCK_CD,
                        PACK_MONTH = q.PACK_MONTH,
                        VERS = q.VERS,
                        N_VOLUME = q.N_VOLUME,
                        N_FA = q.N_FA,
                        N_REPLY = q.N_REPLY,
                        N_REPLY_PLANT_CAP = q.N_REPLY_PLANT_CAP,
                        N_REPLY_RAW_COMP = q.N_REPLY_RAW_COMP,
                        N_REPLY_RAW_MAT = q.N_REPLY_RAW_MAT,
                        N_REPLY_QTY_SUPPLY_REFF = q.N_REPLY_QTY_SUPPLY_REFF,
                        N_1_VOLUME = q.N_1_VOLUME,
                        N_1_FA = q.N_1_FA,
                        N_1_REPLY = q.N_1_REPLY,
                        N_1_REPLY_PLANT_CAP = q.N_1_REPLY_PLANT_CAP,
                        N_1_REPLY_RAW_COMP = q.N_1_REPLY_RAW_COMP,
                        N_1_REPLY_RAW_MAT = q.N_1_REPLY_RAW_MAT,
                        N_1_REPLY_QTY_SUPPLY_REFF = q.N_1_REPLY_QTY_SUPPLY_REFF,
                        N_2_VOLUME = q.N_2_VOLUME,
                        N_2_FA = q.N_2_FA,
                        N_2_REPLY = q.N_2_REPLY,
                        N_2_REPLY_PLANT_CAP = q.N_2_REPLY_PLANT_CAP,
                        N_2_REPLY_RAW_COMP = q.N_2_REPLY_RAW_COMP,
                        N_2_REPLY_RAW_MAT = q.N_2_REPLY_RAW_MAT,
                        N_2_REPLY_QTY_SUPPLY_REFF = q.N_2_REPLY_QTY_SUPPLY_REFF,
                        N_3_VOLUME = q.N_3_VOLUME,
                        N_3_FA = q.N_3_FA,
                        N_3_REPLY = q.N_3_REPLY,
                        N_3_REPLY_PLANT_CAP = q.N_3_REPLY_PLANT_CAP,
                        N_3_REPLY_RAW_COMP = q.N_3_REPLY_RAW_COMP,
                        N_3_REPLY_RAW_MAT = q.N_3_REPLY_RAW_MAT,
                        N_3_REPLY_QTY_SUPPLY_REFF = q.N_3_REPLY_QTY_SUPPLY_REFF,
                        NOTICE = q.NOTICE,
                        CREATED_BY = q.CREATED_BY,
                        CREATED_DT = q.CREATED_DT,
                        N_FREQ = q.N_FREQ,
                        N_1_FREQ = q.N_1_FREQ,
                        N_2_FREQ = q.N_2_FREQ,
                        N_3_FREQ = q.N_3_FREQ,
                        D_ID = q.D_ID
                    });
                }
            }
            db.Close();

            return feedbackPartModel;
        }

        private SupplierFeedbackPartStatus GetSupplierFeedbackStatus(String productionMonth, String supplierCode, String version, String supplierPlantCode)
        {
            IDBContext db = DbContext;

            SupplierFeedbackPartStatus supplierFeedbackPartStatus = new SupplierFeedbackPartStatus();

            //This Section Doesn't need Lock Status, so Lock Status = 0
            //By Fid.Reggy
            string LockStatus = "0";

            var QueryLog = db.Query<SupplierFeedbackPartStatus>("GetFeedbackPartStatus", new object[] { productionMonth, supplierCode, supplierPlantCode, version, LockStatus });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    supplierFeedbackPartStatus.IS_N_REPLIED = q.IS_N_REPLIED;
                    supplierFeedbackPartStatus.IS_N_1_REPLIED = q.IS_N_1_REPLIED;
                    supplierFeedbackPartStatus.IS_N_2_REPLIED = q.IS_N_2_REPLIED;
                    supplierFeedbackPartStatus.IS_N_3_REPLIED = q.IS_N_3_REPLIED;
                    supplierFeedbackPartStatus.N_STATUS = q.N_STATUS;
                    supplierFeedbackPartStatus.N_1_STATUS = q.N_1_STATUS;
                    supplierFeedbackPartStatus.N_2_STATUS = q.N_2_STATUS;
                    supplierFeedbackPartStatus.N_3_STATUS = q.N_3_STATUS;
                    supplierFeedbackPartStatus.IS_CONFIRMED = q.IS_CONFIRMED;
                }
            }
            db.Close();

            return supplierFeedbackPartStatus;
        }
        #endregion
        #endregion
    }
}