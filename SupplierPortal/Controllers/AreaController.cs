﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.PartInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;
using Portal.Models.Dock;
using Portal.Models.Area;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.Plant;

namespace Portal.Controllers
{
    public class AreaController : BaseController
    {

        public AreaController()
            : base("Master Area : ")
        {
        }

        protected override void StartUp()
        {
        }     

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<SupplierName> _supplierModel = null;
        private List<SupplierName> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierName>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<PartData> _partModel = null;
        private List<PartData> _PartModel
        {
            get
            {
                if (_partModel == null)
                    _partModel = new List<PartData>();

                return _partModel;
            }
            set
            {
                _partModel = value;
            }
        }


        protected override void Init()
        {

            IDBContext db = DbContext;

            AreaModel modelAreaInfo = new AreaModel();
            modelAreaInfo.AreaDatas = getPartInfo("");
            Model.AddModel(modelAreaInfo);
            
            
        }

        [HttpGet]
       
        public ActionResult PartialHeader()
        {
       
            ViewData["GridArea"] = GetAllArea();
            

            return PartialView("PartialHeader");

        }
        public ActionResult PartialDetail()
        {
            //string SupplierCodeLDAP = "";
            //string DockCodeLDAP = "";
            //List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "UtilPlane", "OIHSupplierOptionUtilPlane");
            //List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            //List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "UtilPlane", "OIHDockCodeUtilPlane");
            
            //if (suppCode.Count == 1)
            //{
            //    SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            //}
            //else
            //{
            //    SupplierCodeLDAP = "";
            //}
            //foreach (DockData d in DockCodes)
            //{
            //    DockCodeLDAP += d.DockCode + ",";
            //}

            AreaModel model = Model.GetModel<AreaModel>();
            model.AreaDatas = getPartInfo(Request.Params["AreaCode"]);

            Model.AddModel(model);

            return PartialView("PartialDetail", Model);
        }

        protected List<AreaData> getPartInfo(string areaCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<AreaData> listTooltipDatas = new List<AreaData>();

            listTooltipDatas = db.Query<AreaData>("GetAreaSearch", new object[] { areaCdParam}).ToList();

             db.Close();
            return listTooltipDatas;
        }

        public ActionResult PartialHeaderPlantGridHeaderArea()
        {
            TempData["GridName"] = "AreaPlantGridLookup";
            List<PlantData> docks = GetAllPlant();

            ViewData["GridPlanCd"] = docks;

            return PartialView("GridLookup/PartialGrid", ViewData["GridPlanCd"]);
        }

        private List<PlantData> GetAllPlant()
        {
            List<PlantData> PartIndoDockModel = new List<PlantData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<PlantData>("GetAllPlant", new object[] { });
            db.Close();

            return PartIndoDockModel;

        }
        public ActionResult PartialPopupArea()
        {
           
            ViewData["GridPlanCd"] = GetAllPlant();

            return PartialView("PopupNew", null);
        }

        public ActionResult PartialHeaderAreaGridHeaderArea()
        {
            TempData["GridName"] = "OIHAreaCodeArea";
            List<AreaData> Areas = Model.GetModel<User>().FilteringArea<AreaData>(GetAllArea(), "AREA_CD", "Area", "OIHAreaCodeArea");
            
            ViewData["GridArea"] = Areas;

            return PartialView("GridLookup/PartialGrid", ViewData["GridArea"]);
        }

        //public ActionResult PartialGridLookUpDockUtilPlane()
        //{
        //    TempData["GridName"] = "UtilPlaneHDockGridLookup";
        //    List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DOCK_CODE", "UtilPlane", "UtilPlaneHDockGridLookup");

        //    ViewData["GridDock"] = docks;

        //    return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);

        //}

       
        private List<AreaData> GetAllArea()
        {
            List<AreaData> PartIndoAreaModel = new List<AreaData>();
            IDBContext db = DbContext;
            PartIndoAreaModel = db.Fetch<AreaData>("GetAllArea", new object[] { });
            db.Close();

            return PartIndoAreaModel;
        }
       
    
        public ActionResult AddNewArea(string AreaCode, string AreaName, string PlantCode, string PicName,
            string EmailTo, string EmailCC, string EmailBCC, string Telp, string MobilePhone,string User)
        {
            string ResultQuery = "";
           
           
            User = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                string CheckData;

                
                CheckData = db.ExecuteScalar<string>("InsertAreaMaster", new object[] 
                {
                   AreaCode,
                   AreaName,
                   PlantCode,
                   PicName,
                   EmailTo,
                   EmailCC,
                   EmailBCC,
                   Telp,
                   MobilePhone,
                   User
                });

                if (CheckData == "Alreadyexists")
                  {
                    ResultQuery = " Data Area Already Exists, Please Input Other Data Area.. ";
                  }
               else
                 {
                    TempData["GridName"] = "PartialDetailArea";
                    ResultQuery = " Process succesfully..";
                 }
                
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }

        public ActionResult AddEditArea(string AreaCode, string AreaName, string PlantCode, string PicName,
            string EmailTo, string EmailCC, string EmailBCC, string Telp, string MobilePhone, string User)
        {

            string ResultQuery = "";
            User = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                string CheckData;


                CheckData = db.ExecuteScalar<string>("UpdateAreaMaster", new object[]
                {
                   AreaCode,
                   AreaName,
                   PlantCode,
                   PicName,
                   EmailTo,
                   EmailCC,
                   EmailBCC,
                   Telp,
                   MobilePhone,
                   User

                });

                if (CheckData == "Alreadyexists")
                {
                    ResultQuery = " Data Area Master Already Exists, Please Edit Other Data Util Plane.. ";
                }
                else
                {
                    TempData["GridName"] = "PartialDetailArea";
                    ResultQuery = " Process succesfully..";
                }

                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        
        /* ------ End ------- */

        public ActionResult DeleteArea(string AreaCode)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteArea", new object[] 
                {
                   AreaCode
               
                });
                TempData["GridName"] = "PartialDetailArea";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

    }
}