﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Web.UI.WebControls;
using System.Data;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Util.Configuration;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.Mvc;
using Portal.Models.PCDownload;
using Portal.Models.Globals;
using Portal.Models.Supplier;
using System.Threading;
using Toyota.Common.Web.MessageBoard;

namespace Portal.Controllers
{
    public class PCDownloadController : BaseController
    {
        private readonly string OIDSupp = "GridLKSupplier";

        public PCDownloadController()
            : base("PC Download")
        {
            PageLayout.UseMessageBoard = true;
        }

        protected override void StartUp()
        {
        }
        List<SupplierICS> _supplierModel = null;
        private List<SupplierICS> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierICS>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        private string _suppCd = null;
        private string SuppCd
        {
            get
            {
                if (string.IsNullOrEmpty(_suppCd))
                {
                    _suppCd = SuppCodeOf(Model.GetModel<User>());
                    if (string.IsNullOrEmpty(_suppCd))
                    {
                        _suppCd = "";
                    }
                }
                return _suppCd;
            }

            set
            {
                _suppCd = value;
            }
        }

        private string SuppCodeOf(User u)
        {
            if (u == null || u.Authorization == null || u.Authorization.Count < 1) return null;

            string ag = (from authSource in u.Authorization
                         from item in authSource.AuthorizationDetails
                         where item.Screen.ToLower() == ScreenId.ToLower() &&
                             item.Object.ToLower() == OIDSupp.ToLower() &&
                             (item.AuthorizationGroup.ToLower() != "null")
                         select item.AuthorizationGroup).FirstOrDefault();
            return ag;
        }

        private string SuppCodeValid(string c)
        {
            if (string.IsNullOrEmpty(SuppCd))
                return c; // doesn't matter , always valid when authorization group is empty

            if (string.IsNullOrEmpty(c) 
                || (string.Compare(c, SuppCd) == 0))
                return SuppCd;
            
            return "XXXX"; /// return invalid
        }

        protected override void Init()
        {
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = false;

            IDBContext db = DbContext;
            PCDownloadModel mdl = new PCDownloadModel();
            
            mdl.PCDownloadDatas = GetPCDownload("", "", "", "", "-");
            Model.AddModel(mdl);

            ViewData["ListPCNoData"] = db.Query<PCNo>("GetAllPCNo");
            db.Close();
        }

        public ActionResult PartialHeaderPCDownload()
        {
            #region Required model in partial page : for PCDownloadHeaderPartial
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "PCDownload", "GridLKSupplier");

            _SupplierModel = suppliers;

            Model.AddModel(_SupplierModel);
            #endregion

            return PartialView("PCDownloadHeader", Model);
        }
        public ActionResult PCDownloadPartial(string PCDateFrom, string PCDateTo, string Confirmation)
        {
            string GridLKSupplier = SuppCodeValid(Request.Params["GridLKSupplier"]);

            PCDownloadModel model = Model.GetModel<PCDownloadModel>();

            List<PCDownloadData> ListPCDownloadDatas = GetPCDownload(PCDateFrom, PCDateTo, "", GridLKSupplier, Confirmation);
            
            model.PCDownloadDatas = ListPCDownloadDatas;
           
            return PartialView("PCDownloadPartial", Model);

        }
        public void DownloadPdf(string PcNo)
        {
            string conString = DBContextNames.DB_PCS;
            string sqlCommand = "SP_REP_PC_TO_BE_RELEASED_PRINT";
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PCNo", System.Data.DbType.String, PcNo);

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PCDownload\Rep_PC.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("pdf", rpt, null);
            String CurrDate = DateTime.Now.ToString("yyyyMMddhhmm");
            string fileName = "Price Confirmation_" + PcNo + "_" + CurrDate + ".pdf";

            UpdateDownloadFlag(PcNo);

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.Flush();
            Response.End();
        }

        public void DownloadCover(string PcNo)
        {
            string conString = DBContextNames.DB_PCS;
            string sqlCommand = "SP_REP_PC_TO_BE_RELEASED_PRINT";
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PCNo", System.Data.DbType.String, PcNo);
            parameters.Add("@Cover", System.Data.DbType.Int32, 1);
            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PCDownload\Rep_PC_Cover.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("pdf", rpt, null);
            String CurrDate = DateTime.Now.ToString("yyyyMMddhhmm");
            string fileName = "Price Confirmation Cover_" + PcNo + "_" + CurrDate + ".pdf";

            UpdateDownloadFlag(PcNo);

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.Flush();
            Response.End();
        }

        public ActionResult SupplierPartial()
        {
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "PCDownload", "GridLKSupplier");

            _SupplierModel = suppliers;
            TempData["GridName"] = "GridLKSupplier";
            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }
        public ActionResult PCNOPartial()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            TempData["GridName"] = "grlLPHeaderPCNo";
            ViewData["ListPCNoData"] = db.Query<PCNo>("GetAllPCNo").ToList<PCNo>();
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["ListPCNoData"]);
        }
        public ContentResult UpdateConfirm(string PCNo, string CreatedBy, string selected, string Reason)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IDBContext dbw = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_WORKFLOW);
            string result = "Process Succesfully";
            string pcno = string.Empty;
            string confirmflag = string.Empty;
            string lockflag = string.Empty;
            bool checkICSUserMapping = false;
            checkICSUserMapping = dbw.ExecuteScalar<bool>("CheckICSUserMapping", new object[] { AuthorizedUser.Username });

            if (checkICSUserMapping)
            {
                result = "You are not authorized to perform this action";
            }
            else
            {
                pcno = PCNo == "null" || PCNo.Equals("") ? string.Empty : PCNo.Trim();
                confirmflag = selected.Equals("Yes") ? "1" : selected.Equals("No") ? "0" : string.Empty;
                lockflag = "1";
                db.Execute("UpdateConfirmationPCDownload", new Object[] { pcno, confirmflag, lockflag });
                db.Close();

                if (selected.Equals("No"))
                {
                    MessageBoardMessage boardMessage = new MessageBoardMessage();
                    boardMessage.GroupId = db.ExecuteScalar<int>("GetLastGroupID", new object[] { "PCDownload_" + PCNo });
                    boardMessage.BoardName = "PCDownload_" + PCNo;
                    boardMessage.Sender = AuthorizedUser.Username;
                    boardMessage.Recipient = CreatedBy;
                    boardMessage.CarbonCopy = false;
                    boardMessage.Text = Reason;
                    boardMessage.Date = DateTime.Now;
                    IMessageBoardService boardService = MessageBoardService.GetInstance();
                    boardService.Save(boardMessage);

                    
                }
            }
            return Content(result);
        }
        public ContentResult Unlock(string PCNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = string.Empty;
            string pcno = string.Empty;
            string confirmflag = string.Empty;
            string lockflag = string.Empty;

            pcno = PCNo == "null" || PCNo.Equals("") ? string.Empty : PCNo.Trim();
            confirmflag = null;
            lockflag = null;
            db.Execute("UnlockPCDownload", new Object[] { pcno, confirmflag, lockflag });

            db.Close();
            return Content(result);
        }

        public void UpdateDownloadFlag(string PCNo)
        {
            IDBContext db = DbContext;
            string result = string.Empty;

            db.Execute("UpdateDownloadFlagPCDownload", new Object[] { PCNo, AuthorizedUser.Username });
            db.Close();
        }
        protected List<PCDownloadData> GetPCDownload(
            string PCDateFrom, string PCDateTo, 
            string PCNumber, 
            string SupplierCode, 
            string Confirmation 
            )
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<PCDownloadData> PCDownloadDatas = new List<PCDownloadData>();
            PCDownloadDatas = db.Fetch<PCDownloadData>("GetPCDownloadData", new object[] { PCDateFrom, PCDateTo, PCNumber, SuppCodeValid(SupplierCode), Confirmation} ); 
            
            db.Close();
            return PCDownloadDatas;
        }
        private List<SupplierICS> GetAllSupplierICS()
        {
            List<SupplierICS> lstSupplierICS = new List<SupplierICS>();
            IDBContext db = DbContext;
            lstSupplierICS = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });
            db.Close();

            return lstSupplierICS;
        }
    }
}
