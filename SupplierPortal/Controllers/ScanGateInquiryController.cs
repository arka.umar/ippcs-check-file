﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.UI.WebControls;
using Toyota.Common.Web.MVC;
using Portal.Models.ScanGateInquiry;
using Portal.Models.Globals;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Telerik.Reporting.Processing;
using System.Data;
using System.Data.OleDb;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.Mvc;
using System.Threading;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using Toyota.Common.Web.Credential;

namespace Portal.Controllers
{
    [SkipUnlockController]
    public class ScanGateInquiryController : BaseController
    {
        //
        // GET: /ScanGateInquiry/

        public ScanGateInquiryController()
              : base("Scan Gate Inquiry")
          {
          }

           protected override void StartUp()
          {

          }

          protected override void Init()
          {
              IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
              var QueryDock = db.Query<GateCode>("GateCodeScanGate");
              ViewData["GateCode"] = QueryDock;

              //var QueryDock = db.Query<Dock>("GetAllDock");

              //ViewData["GateCode"] = QueryDock;

              //List<LogisticPartner> listLP = new List<LogisticPartner>();
              //var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
              //listLP = QueryLog.ToList<LogisticPartner>();

              TempData["GridName"] = "grlLPHeaderScanGate";
              List<LogisticPartner> listLP = Model.GetModel<User>().FilteringArea<LogisticPartner>(GetAllLogisticPartner(), "LPCode", "ScanGateInquiry", "grlLPHeaderScanGate");

              if (listLP.Count > 0)
              {
                  _LogisticPartnerModel = listLP;
              }
              else
              {
                  _LogisticPartnerModel = GetAllLogisticPartner();
              }

              ViewData["LogisticPartnerData"] = _LogisticPartnerModel;
              db.Close();

              ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
              ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");


              ScanGateInquiryModel mdl = new ScanGateInquiryModel();

             // mdl.ScanGateInquiryDbModel = ScanGateget("R", listLP.Count == 1 ? listLP[0].LPCode : "", DateTime.Now.AddDays(-1), DateTime.Now, "", "", "", "", "", "");
             // mdl.DCLInquiryAdditionals = DCLget("A", listLP.Count == 1 ? listLP[0].LPCode : "", DateTime.Now.AddDays(-1), DateTime.Now, "", "", "", "", "", "");
              Model.AddModel(mdl);
          }
          List<LogisticPartner> _logisticPartnerModel = null;
          private List<LogisticPartner> _LogisticPartnerModel
          {
              get
              {
                  if (_logisticPartnerModel == null)
                      _logisticPartnerModel = new List<LogisticPartner>();

                  return _logisticPartnerModel;
              }
              set
              {
                  _logisticPartnerModel = value;
              }
          }

          public ActionResult LPPartial()
          {
              TempData["GridName"] = "grlLPHeaderScanGate";
              //ViewData["LogisticPartnerData"] = GetAllLogisticPartners();

              List<LogisticPartner> listLP = Model.GetModel<User>().FilteringArea<LogisticPartner>(GetAllLogisticPartner(), "LPCode", "DCLInquiry", "grlLPHeaderScanGate");

              if (listLP.Count > 0)
              {
                  _LogisticPartnerModel = listLP;
              }
              else
              {
                  _LogisticPartnerModel = GetAllLogisticPartner();
              }

              ViewData["LogisticPartnerData"] = _LogisticPartnerModel;

              return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
          }
          private IDBContext DbContext
          {
              get
              {
                  return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
              }
          }
          private List<LogisticPartner> GetAllLogisticPartner()
          {
              List<LogisticPartner> logisticPartnerModel = new List<LogisticPartner>();
              IDBContext db = DbContext;
              try
              {
                  logisticPartnerModel = db.Fetch<LogisticPartner>("GetAllLogisticPartner");
              }
              catch (Exception exc) { throw exc; }
              db.Close();

              return logisticPartnerModel;
          }
          public ActionResult GateCodePartial()
          {
              TempData["GridName"] = "grlGate";
              IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

              var QueryDock = db.Query<GateCode>("GateCodeScanGate");
              ViewData["GateCode"] = QueryDock;
              db.Close();
              return PartialView("GridLookup/PartialGrid", ViewData["GateCode"]);
              //PartialView("GridLookup", ViewData["GateCode"]);
          }
          public ActionResult PartialScanGateInquiryRegular()
          {
              //List<DriverNameList> listDriver = GetAllDriverName();
              //ViewData["DriverNameListData"] = listDriver;
              ScanGateInquiryModel mdl = Model.GetModel<ScanGateInquiryModel>();
              //mdl.ScanGateInquiryDbModel = SearchScanGate("R", (Request.Params["LPCode"]),
              //    Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]),
              //    Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]),
              //    Request.Params["Route"], Request.Params["Rate"], Request.Params["DockCode"],
              //    Request.Params["DeliveryNo"],
              //    //Request.Params["DeliveryStatus"] != null ? Request.Params["DeliveryStatus"] : string.Empty,
              //    Request.Params["DeliveryStatus"] = string.Empty,
              //    Request.Params["DeliveryStat2"]
              //    );
              string type=string.Empty;
              if (Request.Params["DeliveryNo"] != null)
              {
                  if (Request.Params["DeliveryNo"] != string.Empty)
                  {
                      type = Request.Params["DeliveryNo"].Trim().ElementAt(0).ToString();
                  }
              }
              mdl.ScanGateInquiryDbModel = SearchScanGate( type, Request.Params["LP_Code"] == null ? string.Empty : Request.Params["LP_Code"],
                                  Request.Params["Route"] == null ? string.Empty : Request.Params["Route"],
                                  Request.Params["Rate"] == null ? string.Empty : Request.Params["Rate"],
                                  Request.Params["DeliveryNo"] == null ? string.Empty : Request.Params["DeliveryNo"],
                                  Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]), 
                                  Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]),
                                  Request.Params["GATE_CODE"] == null ? string.Empty : Request.Params["GATE_CODE"],
                                  Request.Params["StatusIn"] == null ? string.Empty : Request.Params["StatusIn"]);
              Model.AddModel(mdl);
              return PartialView("PartialScanGateInquiryRegular", Model);
          }

          /*private List<DriverNameList> GetAllDriverName(string LP_CD = "")
          {
              IDBContext db = DbContext;
              List<DriverNameList> driverNameModel = new List<DriverNameList>();
              try
              {
                  var driverName = db.Query<DriverNameList>("GetAllDriverName", new object[] { LP_CD });
                  driverNameModel = driverName.ToList<DriverNameList>();
              }
              catch (Exception exc) { throw exc; }
              db.Close();

              return driverNameModel;
          }*/
          protected List<ScanGateInquiryDb> SearchScanGate(string DELIVERY_TYPE, string LP_CD, string ROUTE, string RATE, string DeliveryNo, DateTime PICKUP_DT1, DateTime PICKUP_DT2, string GateCode, string StatusIn)
          {
              IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
              List<ScanGateInquiryDb> ScanGateListAdd = new List<ScanGateInquiryDb>();
              //YG INI PERBAIKI QUERYNYA
              List<ScanGateInquiryDb> QueryLog = db.Fetch<ScanGateInquiryDb>("SelectScanGateInquiry", new Object[] { DELIVERY_TYPE, LP_CD, ROUTE, RATE, DeliveryNo, PICKUP_DT1, PICKUP_DT2, GateCode, StatusIn });//.ToList();
              //var QueryLog = db.Query<ScanGateInquiryDb>("SelectScanGateInquiry", new Object[] { DELIVERY_TYPE});

              if (QueryLog.Any())
              {
                  int count = 0;
                  foreach (var q in QueryLog)
                  {
                      ScanGateListAdd.Add(new ScanGateInquiryDb()
                      {
                          DELIVERY_NO = q.DELIVERY_NO,
                          //REVISE_NO = q.REVISE_NO,
                          PICKUP_DT = q.PICKUP_DT,
                          LP_CD = q.LP_CD,
                          ROUTE_RATE = q.ROUTE_RATE,
                          LOG_PARTNER_NAME = q.LOG_PARTNER_NAME,
                          DRIVER_NAME = q.DRIVER_NAME != null ? q.DRIVER_NAME : string.Empty,
                          VEHICLE_NO = q.VEHICLE_NO,
                          CODE_IN=q.CODE_IN,
                          PLAN_IN_DT=q.PLAN_IN_DT,
                          ACTUAL_IN_DT=q.ACTUAL_IN_DT,
                          GAP_IN=q.GAP_IN,

                          STATUS_IN = q.ACTUAL_IN_DT != null ? Convert.ToString(q.STATUS_IN) : string.Empty,
                          
                          STATUS_IMAGE_IN = q.STATUS_IMAGE_IN,
                          CODE_OUT = q.CODE_OUT,
                          PLAN_OUT_DT = q.PLAN_OUT_DT,
                          ACTUAL_OUT_DT = q.ACTUAL_OUT_DT
                          //added fid.deny 2015-05-19
                          ,GAP_OUT=q.GAP_OUT,

                          STATUS_OUT = q.ACTUAL_OUT_DT != null ? Convert.ToString(q.STATUS_OUT) : string.Empty,
                          
                          STATUS_IMAGE_OUT = q.STATUS_IMAGE_OUT
                          //end of added fid.deny 2015-05-19
                      });
                      count++;
                  }
              }
              db.Close();
              return ScanGateListAdd;
          }

          public void DownloadExports(string LP_CD, string ROUTE, string RATE, string DeliveryNo, DateTime PICKUP_DT1, 
              DateTime PICKUP_DT2, string GateCode, string StatusIn)
          {
              string connString = DBContextNames.DB_PCS;
              Telerik.Reporting.Report rpt;
              XmlReaderSettings settings;
              XmlReader xmlReader;
              ReportXmlSerializer xmlSerializer;
              Telerik.Reporting.SqlDataSource sqlDataSource;

              settings = new XmlReaderSettings();
              settings.IgnoreWhitespace = true;
              using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\ScanGateInquiry\DownloadScanGateInquiry.trdx", settings))
              {
                  xmlSerializer = new ReportXmlSerializer();
                  rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
              }
              sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
              sqlDataSource.ConnectionString = connString;
              sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
              sqlDataSource.SelectCommand = "SelectScanGateMonitoringDownload";
              //EXEC dbo.[SelectScanGateInquiry] @0,@1,@2,@3,@4,@5,@6,@7,@8
              sqlDataSource.Parameters.Add("@DELIVERY_TYPE", System.Data.DbType.String, "");
              sqlDataSource.Parameters.Add("@LP_CD", System.Data.DbType.String, LP_CD.Trim());
              sqlDataSource.Parameters.Add("@ROUTE", System.Data.DbType.String, ROUTE.Trim());
              sqlDataSource.Parameters.Add("@RATE", System.Data.DbType.String, RATE.Trim());
              sqlDataSource.Parameters.Add("@DELIVERY_NO", System.Data.DbType.String, DeliveryNo.Trim());
              sqlDataSource.Parameters.Add("@PICKUP_DT1", System.Data.DbType.DateTime, PICKUP_DT1);
              sqlDataSource.Parameters.Add("@PICKUP_DT2", System.Data.DbType.DateTime, PICKUP_DT2);
              sqlDataSource.Parameters.Add("@GATE_CODE", System.Data.DbType.String, GateCode.Trim());
              sqlDataSource.Parameters.Add("@STATUS_IN", System.Data.DbType.String, StatusIn.Trim());
              
              ReportProcessor reportProcessor = new ReportProcessor();
              RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
              string fileName = "ScanGateReportMonitoring" + "." + result.Extension;              
              Response.Clear();
              Response.ContentType = result.MimeType;
              Response.Cache.SetCacheability(HttpCacheability.Private);
              Response.Expires = -1;
              Response.Buffer = true;

              Response.AddHeader("Content-Disposition",
                                 string.Format("{0};FileName=\"{1}\"",
                                               "attachment",
                                               fileName));

              Response.BinaryWrite(result.DocumentBytes);
              Response.End();
              rpt.Dispose();
          }
    }
}
