﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Configuration;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.MessageBoard;
using Portal.Models.PODownload;
using Portal.Models.Globals;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;

namespace Portal.Controllers
{
    public class PODownloadController : BaseController
    {
        public PODownloadController()
            : base("PO Download")
        {
            PageLayout.UseMessageBoard = true;
        }

        protected override void StartUp()
        {
        }
        List<SupplierICS> _supplierModel = null;
        private List<SupplierICS> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierICS>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        private readonly string OIDSupp = "GridLKSupplier";
        private string _suppCd = null;
        private string SuppCd
        {
            get
            {
                if (string.IsNullOrEmpty(_suppCd))
                {
                    _suppCd = SuppCodeOf(Model.GetModel<User>());
                    if (string.IsNullOrEmpty(_suppCd))
                    {
                        _suppCd = "";
                    }
                }
                return _suppCd;
            }

            set
            {
                _suppCd = value;
            }
        }

        private string SuppCodeOf(User u)
        {
            if (u == null || u.Authorization == null || u.Authorization.Count < 1) return null;

            string ag = (from authSource in u.Authorization
                         from item in authSource.AuthorizationDetails
                         where item.Screen.ToLower() == ScreenId.ToLower() &&
                             item.Object.ToLower() == OIDSupp.ToLower() &&
                             (item.AuthorizationGroup.ToLower() != "null")
                         select item.AuthorizationGroup).FirstOrDefault();
            return ag;
        }

        private string SuppCodeValid(string c)
        {
            if (string.IsNullOrEmpty(SuppCd))
                return c; // doesn't matter , always valid when authorization group is empty

            if (string.IsNullOrEmpty(c)
                || (string.Compare(c, SuppCd) == 0))
                return SuppCd;

            return "XXXX"; /// return invalid
        }

        protected override void Init()
        {
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = false;

            IDBContext db = DbContext;
            PODownloadModel mdl = new PODownloadModel();
            mdl.PODownloadDatas = db.Fetch<PODownloadData>("GetPODownloadData", new object[] { "", "", "", "", "", "" });
            Model.AddModel(mdl);

            db.Close();
        }

        public ActionResult PartialHeaderPODownload()
        {
            #region Required model in partial page : for PODownloadHeaderPartial
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "PODownload", "GridLKSupplier");

            _SupplierModel = suppliers;
            Model.AddModel(_SupplierModel);
            #endregion

            return PartialView("PODownloadHeader", Model);
        }
        public ActionResult PODownloadPartial(string ProductionMonth, string PODateFrom, string PODateTo, string Confirmation)
        {
            string GridLKSupplier = SuppCodeValid(Request.Params["GridLKSupplier"]);
            string productionMonth = Request.Params["ProductionMonth"];
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            PODownloadModel model = Model.GetModel<PODownloadModel>();

            List<PODownloadData> ListPODownloadDatas = new List<PODownloadData>();
            ListPODownloadDatas = db.Fetch<PODownloadData>("GetPODownloadData", new object[] { PODateFrom, PODateTo, "", GridLKSupplier, Confirmation, productionMonth });
            db.Close();
            model.PODownloadDatas = ListPODownloadDatas;

            return PartialView("PODownloadPartial", Model);

        }
        public ActionResult SupplierPartial()
        {
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "PODownload", "GridLKSupplier");

            _SupplierModel = suppliers;
            TempData["GridName"] = "GridLKSupplier";
            return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }
        public ContentResult UpdateConfirm(string PONo, string CreatedBy, string selected, string Reason)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IDBContext dbw = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_WORKFLOW);
            string result = "Process Succesfully";
            string pono = string.Empty;
            string confirmflag = string.Empty;
            string lockflag = string.Empty;
            bool checkICSUserMapping = false;
            checkICSUserMapping = dbw.ExecuteScalar<bool>("CheckICSUserMapping", new object[] { AuthorizedUser.Username });

            if (checkICSUserMapping)
            {
                result = "You are not authorized to perform this action";
            }
            else
            {
                pono = PONo == "null" || PONo.Equals("") ? string.Empty : PONo.Trim();
                confirmflag = selected.Equals("Yes") ? "1" : selected.Equals("No") ? "0" : string.Empty;
                lockflag = "1";
                db.Execute("UpdateConfirmationPODownload", new Object[] { pono, confirmflag, lockflag });
                db.Close();

                if (selected.Equals("No"))
                {
                    MessageBoardMessage boardMessage = new MessageBoardMessage();
                    boardMessage.GroupId = db.ExecuteScalar<int>("GetLastGroupID", new object[] { "PODownload_" + PONo });
                    boardMessage.BoardName = "PODownload_" + PONo;
                    boardMessage.Sender = AuthorizedUser.Username;
                    boardMessage.Recipient = CreatedBy;
                    boardMessage.CarbonCopy = false;
                    boardMessage.Text = Reason;
                    boardMessage.Date = DateTime.Now;
                    IMessageBoardService boardService = MessageBoardService.GetInstance();
                    boardService.Save(boardMessage);
                }
            }
            return Content(result);
        }
        public ContentResult Unlock(string PONo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = string.Empty;
            string pono = string.Empty;
            string confirmflag = string.Empty;
            string lockflag = string.Empty;

            pono = PONo == "null" || PONo.Equals("") ? string.Empty : PONo.Trim();
            confirmflag = null;
            lockflag = null;
            db.Execute("UnlockPODownload", new Object[] { pono, confirmflag, lockflag });

            db.Close();
            return Content(result);
        }

        public void UpdateDownloadFlag(string PONo)
        {
            IDBContext db = DbContext;
            string result = string.Empty;

            db.Execute("UpdateDownloadFlagPODownload", new Object[] { PONo, AuthorizedUser.Username });
            db.Close();
        }
        //protected List<PODownloadData> GetPODownload(string PODate, string SupplierCode, string SupplierName, string DownloadSts, string DownloadBy, string DownloadDt)
        //{
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    List<PODownloadData> PODownloadDatas = new List<PODownloadData>();
        //    PODownloadDatas = db.Fetch<PODownloadData>("GetPODownloadData", new object[] { PODate, SupplierCode, SupplierName, DownloadSts, DownloadBy, DownloadDt });
            
        //    db.Close();
        //    return PODownloadDatas;
        //}
        private List<SupplierICS> GetAllSupplierICS()
        {
            List<SupplierICS> lstSupplierICS = new List<SupplierICS>();
            IDBContext db = DbContext;
            lstSupplierICS = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });
            db.Close();

            return lstSupplierICS;
        }

        public void DownloadPdf(string PoNo, string POType)
        {
            string conString = DBContextNames.DB_PCS;
            string sqlCommand = "";
            string fileName = "";
            String CurrDate = DateTime.Now.ToString("yyyyMMddhhmm");
            if (POType == "1")
            {
                fileName = "Po_Regular_Download_" + PoNo + "_" + CurrDate + ".pdf";
                sqlCommand = "SP_REP_PO_COVER";
            }
            else
            {
                fileName = "Po_CPO_Download_" + PoNo + "_" + CurrDate + ".pdf";
                sqlCommand = "SP_REP_PO_CPO";
            }
            
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PoNo", System.Data.DbType.String, PoNo);

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            if (POType == "1")
            {
                using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\POApproval\Cover_PO.trdx", settings))
                {
                    xmlSerializer = new ReportXmlSerializer();
                    rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                }
            }
            else
            {
                using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PO\Rep_CPO.trdx", settings))
                {
                    xmlSerializer = new ReportXmlSerializer();
                    rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                }
            }
            
            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("pdf", rpt, null);
            
            UpdateDownloadFlag(PoNo);

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.Flush();
            Response.End();
        }
    }
}
