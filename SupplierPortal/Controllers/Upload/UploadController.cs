﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Compression;
using Portal.Models;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using System.Web.UI;
using System.IO;
using System.Net;
using Portal.Controllers.Upload;

namespace Portal.Controllers
{
    public class UploadController : BaseController
    {
        public UploadController()
            : base("Upload File Using FTP, cuy")
        {

        }

        public ActionResult UploadFile(FormCollection form)
        {
            string opt = form["rbUploadCondition"];

            Session["Storage"] = new UploadControlFilesStorage();

            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucMultiFile", UploadModel.ValidationSettings);

            string msg = string.Empty;

            FTPUpload vFtp = new FTPUpload();

            //vFtp.Setting.IP = "127.0.0.1";
            //vFtp.Setting.Password = "";
            //vFtp.Setting.UserID = "";

            string temporaryPath = Environment.GetFolderPath(Environment.SpecialFolder.Resources) + "\\";
            System.IO.Directory.CreateDirectory(temporaryPath);
            for (int i = 0; i < files.Length; i++)
            {
                files[i].SaveAs(temporaryPath + files[i].FileName);
            }
            
            //string temporaryPath;

            String[] filenames = System.IO.Directory.GetFiles(temporaryPath);

            //temporaryPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "TempZipFolder") + "\\";

            //System.IO.Directory.CreateDirectory(temporaryPath);
            IonicZip compress = new IonicZip();

            if (!compress.Compress(filenames, temporaryPath + "test.zip", ref msg))
                return RedirectToAction("Index", "Dashboard");
            //if (!vFtp.FtpRenameFile(@"/IPPCS/2012/Module/", "120425_Sample_Bar.jpg", "Coba.jpg", ref msg))
            //    return RedirectToAction("Index", "Dashboard");

            //switch (opt)
            //{
            //    case "0":
            //        for (int i = 0; i < files.Length; i++)
            //        {
            //            if (!vFtp.FtpUpload("2011/Module", files[i].FileName, files[0].FileBytes, ref msg))
            //                break;
            //        }
            //        break;
            //    case "1":
            //        if (!vFtp.FtpUpload("IPPCS/2012/Module", "Temp.zip", files[0].FileBytes, ref msg))
            //            break;

            //        break;
            //    case "2":
            //        string temporaryPath = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache);
            //        for (int i = 0; i < files.Length; i++)
            //        {
            //            files[i].SaveAs(temporaryPath + files[i].FileName);
            //        }
            //        //break;

            //        //string[] namafiles = files.ToArray<string>();
            //        //IonicZip compress = new IonicZip();
            //        //String[] filenames = System.IO.Directory.GetFiles("E:\\Betrack");
                    
            //        //if(!compress.Compress(filenames, "E:\\Coba.zip", ref msg))
            //        //    break;
            //        //zip.Password = "makemeproud";
            //        //for (int i = 0; i < files.Length; i++)
            //        //{
            //        //    if (files[i].FileName != null)
            //        //        zip.AddEntry(files[i].FileName, files[i].FileContent);
            //        //}
            //        //zip.Save(zip.TempFileFolder + "\\Temp.zip");
            //        //if (!vFtp.FtpUpload("IPPCS/2012/Module", "Temp.zip", zip.TempFileFolder + "\\", ref msg))
            //        //    break;

            //        break;

            //}
            return RedirectToAction("Index");
        }

        public static List<string> Files
        {
            get
            {
                UploadControlFilesStorage storage = System.Web.HttpContext.Current.Session["Storage"] as UploadControlFilesStorage;
                if (storage != null)
                    return storage.Files;
                return new List<string>();
            }
        }
        public static int FileInputCount
        {
            get
            {
                UploadControlFilesStorage storage = System.Web.HttpContext.Current.Session["Storage"] as UploadControlFilesStorage;
                if (storage != null)
                    return storage.FileInputCount;
                return 2;
            }
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {

        }
    }
}
