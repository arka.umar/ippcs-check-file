﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Controllers.Upload
{
    public class UploadControlFilesStorage
    {
        List<string> files;

        public UploadControlFilesStorage()
        {
            this.files = new List<string>();
            FileInputCount = 2;
        }

        public int FileInputCount { get; set; }
        public List<string> Files { get { return files; } }
    }
}