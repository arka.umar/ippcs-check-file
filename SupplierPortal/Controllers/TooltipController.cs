﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;

using Portal.Models.Tooltip;
using Toyota.Common.Web.Credential;


namespace Portal.Controllers
{
    public class TooltipController : BaseController
    {
        public TooltipController()
            :base("Tooltip Master Screen")
        { 
        }

        protected override void StartUp()
        {

        }

        /*protected override void Init()
        {
            TooltipModel model = new TooltipModel();

            for (int i = 0; i < 21; i++)
            {
                model.TooltipDatas.Add(new TooltipData
                {
                    Check = false,
                    TooltipID =  (i + 1),
                    Description = "Description" + (i + 1),
                    UserName = "UserName" + (i + 1),
                    Message = "Message" + (i + 1),
                    CreatedBy = "CB" + (i + 1),
                    CreatedDate = DateTime.Now,
                    ChangedBy = "CB" + (i + 1),
                    ChangedDate = DateTime.Now,
                });
            }


            Model.AddModel(model);
        }*/
        protected override void Init()
        {
            TooltipModel modelTooltip = new TooltipModel();
            modelTooltip.TooltipDatas = getTooltip();
            Model.AddModel(modelTooltip);
        }

        public ActionResult Search(string TooltipID, string UserName)
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            TooltipModel model = Model.GetModel<TooltipModel>();
            model.TooltipDatas = db.Fetch<TooltipData>("GetTooltipScreenMaster", new object[] { TooltipID, UserName });
            db.Close();
            return PartialView("PartialTooltip", Model);
        }

        protected List<TooltipData> getTooltip()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<TooltipData> listTooltipDatas = new List<TooltipData>();
            var queryLog = db.Query<TooltipData>("GetTooltipScreenMaster", new object[] { "", "" });
            
            
            if (queryLog.Any())
            {
                foreach (var q in queryLog)
                {
                    listTooltipDatas.Add(new TooltipData
                        {
                            TooltipID = q.TooltipID,
                            Description = q.Description,
                            UserName = q.UserName,
                            Message = q.Message,
                            CreatedBy = q.CreatedBy,
                            CreatedDate = q.CreatedDate,
                            ChangedBy = q.ChangedBy,
                            ChangedDate = q.ChangedDate
                        });
                }
 
            }
            db.Close();
            return listTooltipDatas;
        }

        public ActionResult PartialTooltipDatas()
        {
            return PartialView("PartialTooltip", Model);
        }
        public ActionResult PartialTooltipHeadernye()
        {
            return PartialView("PartialTooltipHeader", Model);
        }
        public ActionResult PartialTooltipGriddernye()
        {
            return PartialView("PartialTooltip", Model);
        }
        public ActionResult PartialTooltipButtonGridnyeduluan()
        {
            return PartialView("PartialTooltipButtonUpGrid", Model);
        }
      
    }
}
