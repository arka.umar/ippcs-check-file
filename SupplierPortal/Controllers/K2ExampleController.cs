﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.K2Connector;
using Toyota.Common.Web.Configuration;
using Portal.Models.Sample;
using System.Timers;

namespace Portal.Controllers
{
    public class K2ExampleController : BaseController
    {
        //
        // GET: /K2Example/

        //public ActionResult Index()
        //{
        //    return View();
        //}
        public string user = "mangestya.argyaputri";
        private static string GetProcessFullName()
        {
            string processFullName = "workflow_test\\ProcessTest";
            return processFullName;
        }

        public K2ExampleController()
            : base("K2Example", "K2Example")
        {

        }

        protected override void StartUp()
        {
            string processFullName = GetProcessFullName();
            K2Manager wfConn = new K2Manager();
            K2ExampleModel model = new K2ExampleModel();
            
            // create the timer set the event handler
            System.Threading.Thread.Sleep(5000);
            
            //get user's worklist(s)
            model.Worklists = wfConn.GetAllWorklist(user, processFullName);
            model.Worklists2 = wfConn.GetAllWorklist("yogi", processFullName);

            Model.AddModel(model);

        }

        public ActionResult DoAction(string sn, string act, string username=null)
        {
            if (String.IsNullOrEmpty(username))
            {
                username = user;
            }

            // do action approve/reject or else action
            K2Manager wfConn = new K2Manager();
            wfConn.DoAction(act, sn, username);

            return RedirectToAction("Index", "K2Example");
        }

        public ActionResult Register(string Folio)
        {
            Dictionary<string,string> datafields = new Dictionary<string,string>();
            datafields["RecipientMail"] = "ondi.simamora@toyota.co.id";
            datafields["FolioNo"] = Folio;

            // start K2 process instance (start workflow)
            K2Manager workflow = new K2Manager();
            workflow.StartProcessInstance(GetProcessFullName(), Folio, user, datafields);

            return RedirectToAction("Index", "K2Example");
        }

        protected override void Init()
        {
            
        }

        public ActionResult DoRedirect(string sn, string grantor)
        {
            try
            {
                K2Manager wfConn = new K2Manager();
                wfConn.Open();
                wfConn.RedirectWorklistItem(sn, grantor, user);
                wfConn.Close();
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("Index", "K2Example");
        }
    }
}
