﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.PO;
//using Toyota.Common.Web.Notification;

namespace Portal.Controllers
{
    public class PODashboardController : BaseController
    {
        public PODashboardController()
            //: base("PO Dashboard")
            : base("PO Approval Summary Dashboard")
        {
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            POApprovalModel model = new POApprovalModel();
            /*for (int i = 0; i < 21; i++)
            {
                model.PurchasingOrders.Add(new PurchasingOrder()
                {
                    Action = " ",
                    StatusSH = " ",
                    StatusDpH = " ",
                    StatusDH = " ",
                    Description = "Monthly",
                    PODate = "2" + (i + 1) + "/06/2012",
                    Year = "2012",
                    ViewDetilom = "View Detail",
                });

                model.PurchasingOrders2.Add(new PurchasingOrder2()
                {
                    Action2 = " ",
                    StatusSH2 = " ",
                    StatusDpH2 = " ",
                    StatusDH2 = " ",
                    Description2 = "Additional",
                    PODate2 = "2" + (i + 1) + "/08/2012",
                    Year2 = "2012",
                });
            }*/
            Model.AddModel(model);

        }

        public ActionResult PoToBeReleasedPartial()
        {
            return PartialView("PoToBeReleasedPartial", Model);
        }

        public ActionResult PoToBeReleasedPartial2()
        {
            return PartialView("PoToBeReleasedPartial2", Model);
        }
        //public ActionResult PODashboardGridder()
        //{
        //    return PartialView("PODashboardGridder", Model);
        //}
    }

}
