﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Download;

namespace Portal.Controllers
{
    public class FileDownloadController : BaseController
    {

        public FileDownloadController(): base("File Download")
        {
            CheckSessionState = false;
        }

        protected override void StartUp()
        {
               
        }

        protected override void Init()
        {
            
        }

        public override ActionResult Index()
        {
            string context = Request.Params["context"];
            string path = Request.Params["path"];

            if (string.IsNullOrEmpty(context) && string.IsNullOrEmpty(path))
            {
                return null;
            }

            FileDownloadContext downloadContext = FileDownloadContextRegistry.GetInstance().Get(context);
            if (downloadContext != null)
            {
                switch (downloadContext.Type)
                {
                    case FileDownloadContextType.FTP:
                        path = downloadContext.Path + "/" + path;
                        path = path.Replace('/', '$');
                        path = path.Replace('\\', '$');
                        return DownloadAttachment(path);
                    default:
                        if (!path.StartsWith("\\"))
                        {
                            path = "\\" + path;
                        }
                        int lastSeparatorIndex = path.LastIndexOf("\\");
                        lastSeparatorIndex++;
                        string fileName = path.Substring(lastSeparatorIndex, path.Length - lastSeparatorIndex);
                        return File(downloadContext.Path + path, "application/octet-stream", fileName);
                }             
            }

            return null;
        }
    }
}
