﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Text;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MessageBoard;

using Portal.Models.Globals;
using Portal.Models.EmergencyOrderTVEST;
using Portal.ExcelUpload;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using System.Data.OleDb;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.Credential;

namespace Portal.Controllers
{
    public class EmergencyOrderCreationTVESTController : BaseController
    {

        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        /// <summary>
        /// If the current response is a FileResult (an MVC base class for files) then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }
       
      
        private IDBContext DbContext
        {
            get
            { 
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        
        public EmergencyOrderCreationTVESTController() : base("Emergency Order Creation Comp. Export")
        {
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        #region Model properties
        //List<Dock> _dockModelList = null;
        //private List<Dock> _DockModelList
        //{
        //    get
        //    {
        //        if (_dockModelList == null)
        //            _dockModelList = new List<Dock>();

        //        return _dockModelList;
        //    }
        //    set
        //    {
        //        _dockModelList = value;
        //    }
        //}

        //List<Supplier> _supplierModelList = null;
        //private List<Supplier> _SupplierModelList
        //{
        //    get
        //    {
        //        if (_supplierModelList == null)
        //            _supplierModelList = new List<Supplier>();

        //        return _supplierModelList;
        //    }
        //    set
        //    {
        //        _supplierModelList = value;
        //    }
        //}

        //List<Part> _partModelList = null;
        //private List<Part> _PartModelList
        //{
        //    get
        //    {
        //        if (_partModelList == null)
        //            _partModelList = new List<Part>();

        //        return _partModelList;
        //    }
        //    set
        //    {
        //        _partModelList = value;
        //    }
        //}

        List<EmergencyOrderDetailTVEST> _emergencyOrderDetailList = null;
        private List<EmergencyOrderDetailTVEST> _EmergencyOrderDetailList
        {
            get
            {
                if (_emergencyOrderDetailList == null)
                    _emergencyOrderDetailList = new List<EmergencyOrderDetailTVEST>();

                return _emergencyOrderDetailList;
            }
            set
            {
                _emergencyOrderDetailList = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "EmergencyOrderCreation", "EOCHSupplierGridLookup");
            List<Dock> docks = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DOCK_CODE", "EmergencyOrderCreation", "EOCHDockGridLookup");

            ViewData["SupplierGridLookup"] = suppliers;
            ViewData["DockGridLookup"] = docks;
            //ViewData["SupplierGridLookup"] = GetAllSupplier();
            //ViewData["DockGridLookup"] = GetAllDock();
            ViewData["PartGridLookup"] = GetAllPart("","","","");
        }

        #region Emergency Order Creation Controller
        #region View Controller
        public ActionResult PartialHeaderEmergencyOrderCreation()
        {
            #region Required model in partial page : for EmergencyOrderCreationHeaderPartial
            //_DockModelList = GetAllDock();
            //_SupplierModelList = GetAllSupplier();

            //Model.AddModel(_DockModelList);
            //Model.AddModel(_SupplierModelList);
            //Model.AddModel(_PartModelList);
            #endregion

            return PartialView("EmergencyOrderCreationHeaderPartial", Model);
        }

        public ActionResult PartialHeaderDockGridLookupEmergencyOrderCreation()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "EOCHDockGridLookup";
            List<Dock> docks = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DOCK_CODE", "EmergencyOrderCreation", "EOCHDockGridLookup");

            ViewData["DockGridLookup"] = docks;
            //ViewData["DockGridLookup"] = GetAllDock();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["DockGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierGridLookupEmergencyOrderCreation()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "EOCHSupplierGridLookup";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "EmergencyOrderCreation","EOCHSupplierGridLookup");

            ViewData["SupplierGridLookup"] = suppliers;
            //ViewData["SupplierGridLookup"] = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierGridLookup"]);
        }

        public ActionResult PartialPartGridLookupEmergencyOrderCreation()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "grlPartNo";
            ViewData["PartGridLookup"] = GetAllPart(
                    Request.Params["SupplierCode"],
                    Request.Params["SupplierPlant"],
                    Request.Params["DockCode"],
                    Request.Params["KanbanNo"]);
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["PartGridLookup"]);
        }

        public string CheckBOPart(string PartNo, string SupplierCode, string SupplierPlantCode, string DockCode)
        { 
            string result = "";
            IDBContext db = DbContext;
            try
            {
                result = db.SingleOrDefault<string>("CheckBOPart", new object[] { PartNo, SupplierCode, SupplierPlantCode, DockCode });
            }
            catch (Exception exc) { result = exc.Message; }

            return result; 
        }

        public ActionResult PartialDetailEmergencyOrderCreation()
        {
            #region Required model in partial page : for EmergencyOrderCreationDetailPartial
            string SupplierCode = "";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "EmergencyOrderCreation","EOCHSupplierGridLookup");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1)
            {
                SupplierCode= suppliers[0].SUPPLIER_CODE;
            }
            _EmergencyOrderDetailList = GetAllEmergencyOrderDetail(SupplierCode);

            Model.AddModel(_EmergencyOrderDetailList);
            #endregion

            return PartialView("EmergencyOrderCreationDetailPartial", Model);
        }
        #endregion

        #region Database Controller
        private List<Dock> GetAllDock()
        {
            List<Dock> dockModelList = new List<Dock>();
            IDBContext db = DbContext;
            try
            {
                dockModelList = db.Fetch<Dock>("GetAllDock", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            
            db.Close();

            return dockModelList;
        }
        private List<Supplier> GetAllSupplier()
        {
            List<Supplier> supplierModelList = new List<Supplier>();
            IDBContext db = DbContext;
            
            try
            {
                supplierModelList = db.Fetch<Supplier>("GetAllSupplierGrid", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return supplierModelList;
        }
        private List<Part> GetAllPart(string SupplierCode, string SupplierPlant, string DockCode, string KanbanNo)
        {
            List<Part> partModelList = new List<Part>();
            IDBContext db = DbContext;
            
            try
            {
                partModelList = db.Fetch<Part>("GetAllSupplierPartGrid", new object[] { SupplierCode, SupplierPlant, DockCode, KanbanNo });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return partModelList;
        }
        //private List<EmergencyOrderDetail> GetAllEmergencyOrderDetail(object orderNo, object dockCode, object supplierCode, object supplierPlant)
        //{
        //    List<EmergencyOrderDetail> emergencyOrderDetailList = new List<EmergencyOrderDetail>();

        //    try
        //    {
        //        emergencyOrderDetailList = DbContext.Fetch<EmergencyOrderDetail>("GetEmergencyOrderFillGrid", new object[] { orderNo, dockCode, supplierCode, supplierPlant });
        //    }
        //    catch (Exception exc) { throw exc; }
        //    finally { DbContext.Close(); }

        //    return emergencyOrderDetailList;
        //}
        private List<EmergencyOrderDetailTVEST> GetAllEmergencyOrderDetail(string SupplierCode)
        {
            List<EmergencyOrderDetailTVEST> emergencyOrderDetailList = new List<EmergencyOrderDetailTVEST>();
            IDBContext db = DbContext;
            try
            {
                emergencyOrderDetailList = db.Fetch<EmergencyOrderDetailTVEST>("GetEmergencyOrderPartTemporaryTVEST", new object[] { Session.SessionID, AuthorizedUser.Username, SupplierCode });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return emergencyOrderDetailList;
        }
        public JsonResult GetPartDescription(string PartNo, string SupplierCode, string SupplierPlantCode, string DockCode)
        {
            List<SupplierPartTVEST> supplierPart = new List<SupplierPartTVEST>();
            IDBContext db = DbContext;
            try
            {
                supplierPart = db.Query<SupplierPartTVEST>("GetPartDescription", new object[] { PartNo, SupplierCode, SupplierPlantCode, DockCode }).ToList();
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            //return new JsonResult
            //{
            //    Data = new
            //    {
            //        PartNo = supplierPart.PART_NO,
            //        PartName = supplierPart.PART_NAME,
            //        KanbanNo = supplierPart.KANBAN_NO,
            //        PackPerContainer = supplierPart.PACK_PER_CONTAINER
            //    }
            //};
            return Json(supplierPart);
        }

        public string GetBOMessage()
        {
            string result = "";
            IDBContext db = DbContext;
            try
            {
                result = db.ExecuteScalar<string>("GetBOMessage");
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return result;
        }

        public JsonResult GetPartDescriptionByKanban(string KanbanNo, string SupplierCode, string SupplierPlantCode, string DockCode)
        {
            List<SupplierPartTVEST> supplierPart = new List<SupplierPartTVEST>();
            IDBContext db = DbContext;
            try
            {
                supplierPart = db.Query<SupplierPartTVEST>("GetPartDescriptionByKanban", new object[] { KanbanNo, SupplierCode, SupplierPlantCode, DockCode }).ToList();
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            //return new JsonResult
            //{
            //    Data = new
            //    {
            //        PartNo = supplierPart.PART_NO,
            //        PartName = supplierPart.PART_NAME,
            //        KanbanNo = supplierPart.KANBAN_NO,
            //        PackPerContainer = supplierPart.PACK_PER_CONTAINER
            //    }
            //};
            return Json(supplierPart);
        }
        public ContentResult GetDockInformation(string DockCode)
        {
            IDBContext db = DbContext;
            string result = "";
            try
            {
                result = db.ExecuteScalar<string>("GetDockInformation", new object[] { DockCode });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return Content(result);
        }
        #endregion
        #endregion

        #region UPLOAD CONFIRMATION

        #region UPLOAD CONFIRMATION - ACTION
        [HttpPost]
        public ActionResult HeaderTabEOCConfirmation_Upload(string filePath)
        {
            ProcessAllEOCConfirmationUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }
        #endregion

        #region UPLOAD CONFIRMATION - PROPERTIES
        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/EmergencyOrderCreationTVEST/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private Int32 _processId = 0;
        private Int32 ProcessId
        {
            get
            {
                if (_processId == 0)
                    _processId = Math.Abs(Environment.TickCount);

                return _processId;
            }
        }

        private Int32 _seqNo = 0;
        private Int32 SeqNo
        {
            get
            {
                return ++_seqNo;
            }
        }
        #endregion

        #region UPLOAD CONFIRMATION - REQUEST
        private ArrayList GetUploadedFile()
        {
            ArrayList fileRowList = new ArrayList();

            try
            {
                // read uploaded excel file
                fileRowList = ExcelReader.ReaderExcel(
                    _TableDestination.filePath,
                    _TableDestination.sheetName,
                    Convert.ToInt32(_TableDestination.rowStart),
                    Convert.ToInt32(_TableDestination.columnStart),
                    Convert.ToInt32(_TableDestination.columnEnd));
            }
            catch (Exception exc)
            {
                throw new Exception("GetUploadedFile:" + exc.Message);
            }

            return fileRowList;
        }

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();
            IDBContext db = DbContext;
            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            db.Close();
        
            return String.Join(",", columnNameArray.ToArray());
        }

        private String GetColumnValue(String fileRow, Int32 fileColumnCount)
        {
            ArrayList columnValueArray = new ArrayList();
            String[] dumpArray = new String[fileColumnCount];

            try
            {
                // create cleaned (from unused field) from uploaded excel file
                Array.Copy(fileRow.Split(';'), 1, dumpArray, 0, fileColumnCount);

                // create column value parameter     
                columnValueArray.Add("'" + _TableDestination.functionID + "'");    // function id
                columnValueArray.Add("'" + _TableDestination.processID + "'");     // process id

                columnValueArray.Add(dumpArray[0]);     // action
                columnValueArray.Add(dumpArray[1]);     // d id    
                columnValueArray.Add(dumpArray[2]);     // version
                columnValueArray.Add(dumpArray[3]);     // r plant cd
                columnValueArray.Add(dumpArray[4]);     // dock cd
                columnValueArray.Add(dumpArray[5]);     // s plant cd
                columnValueArray.Add(dumpArray[6]);     // pack month
                columnValueArray.Add(dumpArray[7]);     // car cd
                columnValueArray.Add(dumpArray[8]);     // part no
                columnValueArray.Add(dumpArray[9]);     // n volume
                columnValueArray.Add(dumpArray[10]);    // n + 1 volume
                columnValueArray.Add(dumpArray[11]);    // n + 2 volume
                columnValueArray.Add(dumpArray[12]);    // n + 3 volume
                columnValueArray.Add(dumpArray[13]);    // supplier cd
                columnValueArray.Add(dumpArray[14]);    // supplier name
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnValue:" + exc.Message);
            }

            return String.Join(",", columnValueArray.ToArray());
        }

        private EmergencyOrderModelTVEST ProcessAllEOCConfirmationUpload(string filePath)
        {
            EmergencyOrderModelTVEST _UploadedEmergencyOrderModel = new EmergencyOrderModelTVEST();

            // table destination properties
            _TableDestination.functionID = "131";
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "EmergencyOrder";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "46";
            _TableDestination.tableFromTemp = "TB_T_CED_EMERGENCY_ORDER_UPLOAD";
            _TableDestination.tableToAccess = "TB_T_CED_EMERGENCY_ORDER_VALIDATE";
           
            LogProvider logProvider = new LogProvider("103", "131", AuthorizedUser);
            ILogSession sessionLPOP = logProvider.CreateSession();

            OleDbConnection excelConn = null;
            IDBContext db = DbContext;
            try
            {
                sessionLPOP.Write("MPCS00002INF", new object[] { "Upload Emergency Order Comp. Export" });

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableFromTemp });
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties = Excel 8.0";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                excelCommand = new OleDbCommand(@"select " +
                                    _TableDestination.functionID + " as FUNCTION_ID, " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @" *
                                from [" + _TableDestination.sheetName + "$]", excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");

                    bulkCopy.ColumnMappings.Add("Supplier Code", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("Supplier Plant", "SUPPLIER_PLANT");
                    bulkCopy.ColumnMappings.Add("Dock Code", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("Unique No", "KANBAN_NO");
                    bulkCopy.ColumnMappings.Add("Part No", "PART_NO");
                    bulkCopy.ColumnMappings.Add("Kanban Qty", "ORDER_LOT_SZ");

                    bulkCopy.ColumnMappings.Add("Address", "KANBAN_PRINT_ADDRESS");
                    bulkCopy.ColumnMappings.Add("Importir Info 1", "IMPORTIR_INFO");
                    bulkCopy.ColumnMappings.Add("Part Bar Code", "PART_BARCODE");
                    bulkCopy.ColumnMappings.Add("Progress Lane No", "PROGRESS_LANE_NO");
                    bulkCopy.ColumnMappings.Add("Conveyance No", "CONVEYANCE_NO");
                    bulkCopy.ColumnMappings.Add("Production Date", "PROD_DATE");
                #endregion

                    try
                    {
                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        sessionLPOP.Write("MPCS00002ERR", new string[] { ex.Source, ex.Message, "", "" });
                        Console.WriteLine(ex.Message);
                    }
                }

                String columnNameList = GetColumnName();

                // validate data in temporary uploaded table.
                db.Execute("ValidateEmergencyOrderCreationUploadTVEST", new object[] { _TableDestination.functionID, _TableDestination.processID });

                // check for invalid data.
                try
                {
                    _ErrorValidationList = db.Fetch<ErrorValidation>("GetErrorMessageUploadTVEST", new object[] { _TableDestination.functionID, _TableDestination.processID });
                }
                catch (Exception exc)
                {
                    sessionLPOP.Write("MPCS00002ERR", new string[] { exc.Source, exc.Message, "", "" });
                    throw exc;
                }

                if (_ErrorValidationList.Count == 0)
                {
                    // insert data from temporary uploaded table into temporary validated table.
                    db.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] { _TableDestination.tableToAccess, columnNameList, columnNameList, _TableDestination.tableFromTemp });

                    // read uploaded data from temporary validated table to transcation table.
                    //_UploadedLPOPConsolidationModel = GetAllLPOPConsolidationUpload(productionMonth, version, _TableDestination.processID, _TableDestination.functionID);

                    // set process validation status.
                    TempData["IsValid"] = "true;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    sessionLPOP.Write("MPCS00001INF", new object[] { @"Insert upload success info into table log detail.
                                                                        Insert data from temporary uploaded table into temporary validated table.
                                                                        Read uploaded data from temporary validated table to transcation table.
                                                                        set process validation status...!!!" });
                }
                else
                {

                    // load previous data for supplier feedback part model
                    //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail = GetAllLPOPConsolidationUpload(productionMonth, version);

                    // set process validation status.
                    TempData["IsValid"] = "false;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;

                    sessionLPOP.Write("MPCS00001INF", new object[] { @"Insert upload fail info into table log detail
                                                                        set process validation status...!!!" });
                }
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                sessionLPOP.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });

                // load previous data for supplier feedback part model
                //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail= GetAllFeedbackPart(productionMonth, supplierCode, supplierPlanCode, version);
            }
            finally
            {
                sessionLPOP.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                sessionLPOP.Commit();
                
                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }

            db.Close();
            return _UploadedEmergencyOrderModel;
        }

        [HttpPost]
        public void ClearAllFeedbackPartUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            try
            {
                // clear data in temporary uploaded table.
                db.Execute("ClearFeedbackPartUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception exc)
            {
                throw new Exception();
            }
            db.Close();
        }

        [HttpPost]
        public void EOCFileUpload_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("EOCFileUpload", ValidationSettings, EOCFileUploadConfirmation_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void EOCFileUploadConfirmation_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Math.Abs(System.Environment.TickCount) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }
        #endregion

        #region UPLOAD CONFIRMATION - RESULT
        public void UploadValid(String processId, String functionId, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            db.ExecuteScalar<string>("UpdateEmergencyOrderCreationUploadTVEST",
                            new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            AuthorizedUser.Username,
                            Session.SessionID
                        });

            db.Close();
        }

        [HttpGet]
        public void DownloadInvalid(String processId, String functionId, String temporaryTableUpload)
        {

            string fileName = "EmergencyOrderCreationUploadInvalid" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportEmergencyOrderCreationUploadErrorTVEST> qry = db.Query<ReportEmergencyOrderCreationUploadErrorTVEST>("ReportGetEmergencyOrderCreationUploadErrorTVEST", new object[] { functionId, processId });
            db.Close();
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(qry, "EmergencyOrder");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        #endregion

        #region Download controller
        /// <summary>
        /// Download template emergency order.
        /// </summary>
        /// <param name="dockCode"></param>
        /// <param name="arrivalTime"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlantCode"></param>
        ///
             private byte[] StreamFile(string filename)  
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));  

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

            public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/EmergencyOrderCreationTVEST/UploadTemp/EmergencyOrderCreation_Templates.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "EmergencyOrderCreation_Template.xls");

            //string fileName = "EmergencyOrderCreation_Template.xls";

            //// Update download user and status
            //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ////IEnumerable<ReportGetAllEmergencyOrderPartModel> qry = db.Fetch<ReportGetAllEmergencyOrderPartModel>("ReportGetAllFeedbackPart", new object[] { arrivalTime, supplierCode, supplierPlantCode, dockCode});
            //DataTable dt = new DataTable();
            //dt.Columns.Add("Supplier Code", typeof(string));
            //dt.Columns.Add("Supplier Plant", typeof(string));
            //dt.Columns.Add("Dock Code", typeof(string));
            //dt.Columns.Add("Unique No", typeof(string));
            //dt.Columns.Add("Part No", typeof(string));
            //dt.Columns.Add("Order Qty", typeof(int));

            //IExcelWriter exporter = new NPOIWriter();

            //byte[] hasil = exporter.Write(dt, "EmergencyOrder");
            //Response.Clear();
            ////Response.ContentType = result.MimeType;
            //Response.Cache.SetCacheability(HttpCacheability.Private);
            //Response.Expires = -1;
            //Response.Buffer = true;

            //Response.ContentType = "application/octet-stream";
            //Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            //Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            //Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            //Response.BinaryWrite(hasil);
            //Response.End();
        }
        #endregion

        //protected List<EmergencyOrderDetail> EmergencyOrderFillGrid(object OrderNo, object DockCode, object SupplierCode, object SupplierPlant)
        //{            
        //    List<EmergencyOrderDetail> LogList = new List<EmergencyOrderDetail>();
        //    var QueryLog = DbContext.Query<EmergencyOrderDetail>("GetEmergencyOrderFillGrid", new object[] { OrderNo, DockCode, SupplierCode, SupplierPlant });

        //    if (QueryLog.Any())
        //    {
        //        foreach (var q in QueryLog)
        //        {
        //            LogList.Add(new EmergencyOrderDetail()
        //            {
        //                SupplierCode = q.SupplierCode,
        //                OrderNo = q.OrderNo,
        //                PartNo = q.PartNo,
        //                PartName = q.PartName,
        //                SupplierPlantCode = q.SupplierPlantCode,
        //                OrderQty = q.OrderQty,
        //                PCSKanban = q.PCSKanban,
        //                ReceivePlantCode = q.ReceivePlantCode,
        //                DockCode = q.DockCode,
        //                ArrivalTime = DateTime.Now
        //            });
        //        }
        //    }
        //    DbContext.Close();
        //    return LogList;
        //}

        public ActionResult SaveCancelHeader(string orderNo, string dockCode, string supplierCode, string supplierPlantCode, string arrivalTime, string statusButton)
        {
            string result = "";
            IDBContext db = DbContext;
            if (statusButton == "Save")
            {
                EmergencyHeaderTVEST emergencyHeader = db.SingleOrDefault<EmergencyHeaderTVEST>("GetEmergencyOrderHeaderTVEST", new object[] 
                {
                    orderNo,
                    supplierCode,
                    dockCode,                    
                    supplierPlantCode 
                });

                if (emergencyHeader != null)
                    result = "Data has been exist, please verify again.";
                else
                {
                    db.Execute("InsertHeaderEmergencyOrderTVEST", new object[] 
                    {               
                        orderNo,
                        supplierCode,
                        dockCode.Substring(0,1),
                        dockCode,
                        arrivalTime, 
                        SessionState.GetAuthorizedUser().Username,
                        DateTime.Now,
                        supplierPlantCode
                    });
                }
            }
            else
            {
                db.Execute("DeleteEmergencyOrderTVEST", new object[] {               
                    orderNo,
                    supplierCode,
                    dockCode,                    
                    supplierPlantCode
                }); 
            }

            db.Close();
            return Content(result.ToString());
        }
		
		public string CheckBOSubmit()
        { 
            string result = "";
            IDBContext db = DbContext;
            try
            {
                result = db.SingleOrDefault<string>("CheckBOPartSubmit", new object[] { AuthorizedUser.Username, Session.SessionID });
            }
            catch (Exception exc) { result = exc.Message; }

            return result; 
        }
		
        public JsonResult SubmitData(string ArrivalDate, string ArrivalTime)
        {
            List<EmergencyResultTVEST> emergencyResults = new List<EmergencyResultTVEST>();
            DateTime ArrivalDateTime = DateTime.ParseExact((ArrivalDate + " " + ArrivalTime), "dd.MM.yyyy HH:mm", null);
            IDBContext db = DbContext; 
            try
            {
                emergencyResults = db.Query<EmergencyResultTVEST>("InsertHeaderEmergencyOrderTVEST", new object[] 
                    {               
                        ArrivalDateTime, 
                        AuthorizedUser.Username,
                        Session.SessionID,
                        "1"//AuthorizedUser.PlantCode
                    }).ToList();

                //foreach (EmergencyResult emergencyResult in emergencyResults)
                //{
                //    List<SupplierPart> supplierParts = new List<SupplierPart>();
                //    StringBuilder sb = new StringBuilder();
                //    supplierParts = db.Query<SupplierPart>("GetPartDetailManifestEmergency", new object[] 
                //    {               
                //        emergencyResult.MANIFEST_NO
                //    }).ToList();
                //    sb.AppendLine("Emergency Order is created at Dock " + emergencyResult.DOCK_CD + " for following Part No:");
                //    foreach(SupplierPart supplierPart in supplierParts)
                //    {
                //        sb.AppendLine("* " + supplierPart.PART_NO + " - " + supplierPart.PART_NAME);
                //    }

                //    MessageBoardMessage boardMessage = new MessageBoardMessage();
                //    boardMessage.GroupId = 0;
                //    boardMessage.BoardName = "EmergencyOrderCreation_"+ emergencyResult.MANIFEST_NO;
                //    boardMessage.Sender = AuthorizedUser.Username;
                //    boardMessage.Recipient = "supplierportal8";
                //    boardMessage.CarbonCopy = true;
                //    boardMessage.Text = sb.ToString();
                //    boardMessage.Date = DateTime.Now;
                //    IMessageBoardService boardService = MessageBoardService.GetInstance();
                //    boardService.Save(boardMessage);
                //}
                db.Close();
                return Json(emergencyResults);
            }
            catch (Exception exc) {
                String message = exc.Message;
                db.Close();
                return Json(message);
                
            }   

            //return new JsonResult
            //{
            //    Data = new
            //    {
            //        ManifestNo = emergencyResult.MANIFEST_NO,
            //        OrderNo = emergencyResult.ORDER_NO
            //    }
            //};
            
        }

        /* Start 2019-02-12 FID.Ridwan */
        [HttpPost]
        public ActionResult Confirmation_Submit() //string ArrivalDate, string ArrivalTime
        {
            //DateTime ArrivalDateTime = DateTime.ParseExact((ArrivalDate + " " + ArrivalTime), "dd.MM.yyyy HH:mm", null);
            IDBContext db = DbContext;
            String status = "";
            try
            {
                status = db.SingleOrDefault<string>("ValidateEmergencyOrderCreationSubmitTVEST", new object[] { "131", Session.SessionID, AuthorizedUser.Username });
            }
            catch (Exception ex)
            {
                status = "Exception;" + ex.Message.ToString();
            }
            
            return Json(
                new
                {
                    Status = status
                });
        }

        [HttpGet]
        public void DownloadInvalidSubmit(String SessionID, String functionId)
        {

            string fileName = "EmergencyOrderCreationUploadInvalid" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportEmergencyOrderCreationUploadErrorTVEST> qry = db.Query<ReportEmergencyOrderCreationUploadErrorTVEST>("ReportGetEmergencyOrderCreationSubmitErrorTVEST", new object[] { functionId, SessionID, AuthorizedUser.Username });
            db.Close();
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(qry, "EmergencyOrder");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        [HttpPost]
        public void CancelData()
        {
            IDBContext db = DbContext;
            try
            {
                // clear data in temporary uploaded table.
                db.Execute("CancelDataTVEST", new object[] { AuthorizedUser.Username });
            }
            catch (Exception exc)
            {
                throw new Exception();
            }
            db.Close();
        }

        /* End 2019-02-12 FID.Ridwan */

        public ActionResult AddNewPart(string Supplier, string DockCode, string PartNo, string PartName, string KanbanNo, string OrderQty,
            string QtyPerContainer, int PiecesQty, string BuildOutFlag, string PartAddress, string ImportirInfo1, string ImportirInfo2, string ImportirInfo3,
            string ImportirInfo4, string PartBarcode, string ProgressLaneNo, string ConveyanceNo, string ArrivalDate, string ArrivalTime)
        {
            string ResultQuery = "Process succesfully";
            string[] Suppliers = Supplier.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");

            DateTime ArrivalDateTime = DateTime.ParseExact((ArrivalDate + " " + ArrivalTime), "dd.MM.yyyy HH:mm", null);

            IDBContext db = DbContext;
            try
            {
                db.Execute("InsertDetailEmergencyOrderTVEST", new object[] 
                {
                    Session.SessionID,
                    AuthorizedUser.Username,
                    PartNo,
                    PartName,
                    KanbanNo,
                    OrderQty,
                    QtyPerContainer,
                    PiecesQty,
                    SupplierCode,
                    SupplierPlant,
                    DockCode,
                    BuildOutFlag,
                    PartAddress,
                    ImportirInfo1,
                    ImportirInfo2,
                    ImportirInfo3,
                    ImportirInfo4,
                    PartBarcode,
                    ProgressLaneNo,
                    ConveyanceNo,
                    ArrivalDateTime
                });

            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            finally
            { db.Close(); }
            return Content(ResultQuery);
        }
        public ActionResult DeletePart(string PartNos, string Suppliers, string Docks)
        {
            string ResultQuery = "Process succesfully";
            char[] splitchar = { ';' };
            string[] Supplier = Suppliers.Split(splitchar);
            string[] Dock = Docks.Split(splitchar);
            string[] PartNo = PartNos.Split(splitchar);

            string[] SupplierPlantCode;
            string SupplierCode = "";
            string SupplierPlant = "";

            IDBContext db = DbContext; 
            try
            {
                for (int i = 0; i < PartNo.Length; i++)
                {
                    SupplierPlantCode = Supplier[i].Split('-');
                    SupplierCode = SupplierPlantCode[0].Replace(" ", "");
                    SupplierPlant = SupplierPlantCode[1].Replace(" ", "");
                    db.Execute("DeleteDetailEmergencyOrderTVEST", new object[] 
                    {
                        Session.SessionID,
                        AuthorizedUser.Username,
                        SupplierCode,
                        SupplierPlant,
                        Dock[i],
                        PartNo[i]
                    });
                }
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            finally
            { db.Close(); }
            return Content(ResultQuery);
        }

        //public ActionResult EmergencyOrderCreation()
        //{
        //    EmergencyOrderModel model = new EmergencyOrderModel();
        //    model.EmergencyOrders = EmergencyOrderFillGrid(ViewData["OrderNoHeader"], ViewData["DockCodeHeader"], ViewData["SupplierCodeHeader"], ViewData["SupplierPlantHeader"]);
        //    Model.AddModel(model);
        //    return PartialView("EmergencyOrderDataGridNew", Model);
        //}

        public ActionResult PartialHeader()
        {
            return PartialView("HeaderEmergencyOrderCreation", Model);
        }

        /* CARA READ CSV by NIIT.DIKO */
        //private void ReadCSV(string patch)
        //{
        //    IDBContextManager dbManager = DatabaseManager.GetInstance();
        //    IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
        //    StreamReader sr = new StreamReader(@patch);
        //    string strline = "";
        //    string[] values = null;
        //    int x = 0;
        //    while (!sr.EndOfStream)
        //    {
        //        x++;
        //        strline = sr.ReadLine();
        //        values = ParseCsvRow(strline);
        //        string tempValue = "'" + values[0] + "'" + "," + "'" + values[1] + "'" + "," + "'" + values[2] + "'" + "," + "'" + values[3] + "'" + "," + "'" + values[4] + "'" + "," + "'" + values[5] + "'" + "," + "'" + values[6] + "'" + "," + "'" + values[7] + "'" + "," + "'" + values[8] + "'";

        //        dbContext.ReplaceAndQuery("InsertUploadOrderTemp", new object[] 
        //            {
        //                tempValue
        //            });
        //        dbContext.Close();
        //    }
        //    sr.Close();
        //}

        public ActionResult CallbackSupplier()
        {
            TempData["GridName"] = "grlSupplierCode";
            
            return PartialView("GridLookup/PartialGrid", ViewData["SupplierCode"]);
        }

        public ActionResult CallbackPartNo()
        {
            TempData["GridName"] = "grlPartNo";
            
            return PartialView("GridLookup/PartialGrid", ViewData["PartNo"]);
        }

        public ActionResult DockCodePartial()
        {
            TempData["GridName"] = "grlDockCode";

            return PartialView("GridLookup/PartialGrid", ViewData["DockCode"]);
        }

        public ActionResult UploadAction(string dockCode, string supplierCode, string supplierPlantCode)
        {
            string orderNo = System.DateTime.Now.ToString("yyyyMMdd") + "01E1";
            ArrayList tableDetail = new ArrayList();
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext db = DbContext; 
            TableDestination tableName = db.SingleOrDefault<TableDestination>("GetUploadDestination", new object[] { "103" });
            string[] arrayValues;
            tableDetail = ExcelReader.ReaderExcel("C:\\inetpub\\PART.xls", tableName.sheetName, Convert.ToInt32(tableName.rowStart), Convert.ToInt32(tableName.columnStart), Convert.ToInt32(tableName.columnEnd));
            foreach (string tDet in tableDetail)
            {
                string values = "'" + tDet.Remove(0, 2);
                arrayValues = ParseCsvRow(values);
                db.Execute("InsertUploadPartOrderTempTVEST", new object[]
                    {
                        orderNo,
                        dockCode,
                        supplierCode,
                        supplierPlantCode,
                        arrayValues.GetValue(0),
                        arrayValues.GetValue(1),
                        arrayValues.GetValue(2),
                        arrayValues.GetValue(3),
                    });
            }
            db.Close();
            return RedirectToAction("Index");
        }

        public static string[] ParseCsvRow(string r)
        {
            string[] c;
            List<string> resp = new List<string>();
            bool cont = false;
            string cs = "";

            c = r.Split(new char[] { ',' }, StringSplitOptions.None);

            foreach (string y in c)
            {
                string x = y;

                if (cont)
                {
                    // End of field
                    if (x.EndsWith("'"))
                    {
                        cs += "," + x.Substring(0, x.Length - 1);
                        resp.Add(cs);
                        cs = "";
                        cont = false;
                        continue;
                    }
                    else
                    {
                        // Field still not ended
                        cs += "," + x;
                        continue;
                    }
                }

                // Fully encapsulated with no comma within
                if (x.StartsWith("'") && x.EndsWith("'"))
                {
                    if ((x.EndsWith("''") && !x.EndsWith("'''")) && x != "''")
                    {
                        cont = true;
                        cs = x;
                        continue;
                    }

                    resp.Add(x.Substring(1, x.Length - 2));
                    continue;
                }

                // Start of encapsulation but comma has split it into at least next field
                if (x.StartsWith("'") && !x.EndsWith("'"))
                {
                    cont = true;
                    cs += x.Substring(1);
                    continue;
                }

                // Non encapsulated complete field
                resp.Add(x);
            }

            return resp.ToArray();
        }

        public ActionResult SetEmergencyOrderHeader()
        {
            string AnyOrder = ViewData["AnyOrderHeader"].ToString();
            string OrderNo = ViewData["OrderNoHeader"].ToString();
            string DockCode = ViewData["DockCodeHeader"].ToString();
            string SupplierCode = ViewData["SupplierCodeHeader"].ToString();
            string SupplierPlant = ViewData["SupplierPlantHeader"].ToString();
            string ArrivalTime = ViewData["ArrivalTimeHeader"].ToString();

            var datajson = new { AnyOrder, OrderNo, DockCode, SupplierCode, SupplierPlant, ArrivalTime };

            return Json(datajson, JsonRequestBehavior.AllowGet);
        }
    }

    class SupplierPartTVEST
    {
        public string PART_NO { set; get; }
        public string PART_NAME { set; get; }
        public string KANBAN_NO { set; get; }
        public Int32 PACK_PER_CONTAINER { set; get; }
        public string BUILD_OUT_FLAG { set; get; }
        public int BO_PART { set; get; }
    }

    class EmergencyResultTVEST
    {
        public string MANIFEST_NO { set; get; }
        public string ORDER_NO { set; get; }
        public string DOCK_CD { set; get; }
    }

    class EmergencyHeaderTVEST
    {
        public string OrderNo { set; get; }
        public string DockCode { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierPlant { set; get; }
        public DateTime ArrivalTime { set; get; }

    }
}
