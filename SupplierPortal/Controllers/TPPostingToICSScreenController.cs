﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.TPPostingToICSScreen;

namespace Portal.Controllers
{
    public class TPPostingToICSScreenController : BaseController
    {
        public TPPostingToICSScreenController()
            : base("TP Posting To ICS")
        {
            
        }

        protected override void StartUp()
        {
           
        }
        protected override void Init()
        {
            TPPostingToICSScreenModel mdl = new TPPostingToICSScreenModel();
            List<TPPostingToICSScreenDetail> lst = new List<TPPostingToICSScreenDetail>();

           
                for (int i = 0; i < 100; i++)
                {
                	 lst.Add(new TPPostingToICSScreenDetail()
                    {
                         CycleSeq ="1",
                         KanbanID = "18KR0004" + (i + 1),
                         DocCode =""+(i+1),
                         PrintStatus =""+(i+1),
                         ShopStatus = "" + (i + 1),
                         ShopNo ="18051801"+i,
                         PrintDate =DateTime.Now,
                         DeliveryNo = "18051801" + i ,
                         UpdateDate =DateTime.Now,
                         UpdateUser ="102"+(i+1),
                         PostToICS =1,
                         CancelQty ="",
                         CancelledQty ="",
                         Flag ="",
                         By ="",
                         CancellationDate =DateTime.Now
                    });
                };

                mdl.TPPostingToICSScreenDetails = (lst);
                Model.AddModel(mdl);
        }

        public ActionResult TPPostingToICSScreenPartial()
        {
            return PartialView("TPPostingToICSScreenPartial", Model);
        }

        public ActionResult TPPostingHeader()
        {
            return PartialView("TPPostingHeader", Model);
        }
    }
}
