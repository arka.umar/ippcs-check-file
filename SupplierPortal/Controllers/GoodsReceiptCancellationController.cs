﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.GoodsReceiptCancellation;

namespace Portal.Controllers
{
    public class GoodsReceiptCancellationController : BaseController
    {
        //
        // GET: /GoodReceiptCancellation/
        public GoodsReceiptCancellationController()
            : base("Good Receipt Cancellation Screen")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            
            GoodsReceiptCancellationModel mdl = new GoodsReceiptCancellationModel();

            

            mdl.GoodsReceiptCancellations.Add(new GoodsReceiptCancellation()
            {
                GRSlipNo = "1",
                GRSlipDateTo = DateTime.Now.ToShortDateString(),
                GRSlipDateFrom = DateTime.Now.ToShortDateString(),
                HeaderManifestNo = "1"
            });
                                                  	 



            for (int i = 100; i < 150; i++)
            {
                mdl.GoodsReceiptCancellationDetails.Add(new GoodsReceiptCancellationDetail()
                {
                    //the header
                    SupplierCode = "2012001", 
                    SupplierName = "Supplier01",
                    RouteRad = "january",
                    ManifestNo = "MAN2012"+i,
                    OrderNo = "" + i,
                    Status = "" + i,
                    DockCode = "" + i,
                    Po_No = "" + i,
                    ArrivalTimePlanned = DateTime.Now.ToShortDateString(),
                    ArrivalTimeActual = DateTime.Now.ToShortDateString(),
                    Doc_No = "DOC"+i,
                    GRCreatedTime = DateTime.Now.ToShortDateString(),
                    GRSlipDocNo = "SLIP" + i,
                    GRSlipCreatedTime = DateTime.Now.ToShortDateString(),
                    TotalItem = i,
                    TotalQuantity = i,
                    Message = "Messages Written Here",
                    InvoiceNo = "99" + i,
                    InvoiceCreatedTime = DateTime.Now.ToShortDateString()
                });

               // mdl.GoodsReceiptCancellationDetails = ;
                

            }


           //List<GoodsReceiptCancellationDetailDetail> Model = new List<GoodsReceiptCancellationDetailDetail>();

            for (int i = 1; i < 350; i++)
            {
                 long randomNumber = RandomNumber(101,109);
                ///detail detail
                 mdl.GoodsReceiptCancellationDetailDetails.Add(new GoodsReceiptCancellationDetailDetail()
                  {
                      //ManifestNo = "MAN2012100",
                      ManifestNo = "MAN2012" + (100 + (i / 7)),
                      PartNo = "P2012" + randomNumber, 
                      PartName = "Part" + randomNumber,
                      SupplierCode = "20120" + ( 100 + (i / 3) ),
                      SupplierName = "Supplier" + (100 + (i / 3)),
                      OrderQty = 3 * RandomNumber(1, 9),
                      ReceivedQty = 3 * RandomNumber(1, 9),
                      RevisedQty = RandomNumber(1, 9)
                  });
            }
            Model.AddModel(mdl);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("PartialHeader");
        }

        public ActionResult GoodsReceiptCancellationPartial()
        {
            return PartialView("GoodsReceiptCancellationPartial", Model);
        }

        public ActionResult GoodsReceiptCancellationDetailPartial(string ManifestNo)
        {
            ViewData["ManifestNo"] = ManifestNo;
            GoodsReceiptCancellationModel model = Model.GetModel<GoodsReceiptCancellationModel>();
            var GRCDetail = model.GoodsReceiptCancellationDetailDetails.Where(p => p.ManifestNo == ManifestNo).ToList<GoodsReceiptCancellationDetailDetail>();
            return PartialView("GoodsReceiptCancellationDetailPartial", GRCDetail);
        }
        
        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

    }
}
