﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.BackgroundTask;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Service;
using Toyota.Common.Web.Workflow;
using Portal.Models.LPOPConsolidation;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.Util.Converter;
using System.Data;
using Toyota.Common.Util.Text;

namespace Portal.Controllers.LPOPConsolidation
{
    public class LPOPPostingTask : ObservableBackgroundTask
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void DoWork(IDictionary<string, object> parameters)
        {
            this.Progress = 0;
            int isError = 0;
            string prodmonth = string.Empty;
            string processID = "";

            //get paramater
            String GridId = (string)parameters["GridID"];
            User AuthorizedUser = (User)parameters["User"];

            LogProvider logProvider = new LogProvider("1", "11004", AuthorizedUser);
            ILogSession sessionA = logProvider.CreateSession(true);
            processID = Convert.ToString(sessionA.GetProcessId());

            string hasil = "Done";

            List<LPOPPurchaseOrder> tempList;
            List<LPOPPurchaseOrder> list = new List<LPOPPurchaseOrder>();

            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string ProductionMonth = "";
            string Timing = "";
            string ApproveBy = "";

            //SPLIT ID WITH DELIMETER ";"
            char[] SplitChar = { ';' };
            string[] ParamUpdate = GridId.Split(SplitChar);

            //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
            IDBContext db = DbContext;

            sessionA.WriteDirectly("MPCS00002INF", new object[] { "Process Create PO to ICS" }, "Create PO.Get Create PO ICS");

            this.Progress = 5;

            sessionA.WriteDirectly("MPCS00002INF", new object[] { "Process Taking Data from TB_LPOP" }, "Create PO.Taking Data from TB_LPOP");
            for (int i = 0; i < ParamUpdate.Length; i++)
            {
                try
                {
                    //SPLIT ID BY OBJECT WITH DELIMETER "_"
                    tempList = new List<LPOPPurchaseOrder>();
                    char[] SplitId = { '_' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);
                    ProductionMonth = Convert.ToDateTime(ParamId[0]).Year.ToString() + Convert.ToDateTime(ParamId[0]).Month.ToString("0#");
                    Timing = ParamId[1].ToString().Substring(0, 1);
                    ApproveBy = ParamId[2];                  

                    tempList = db.Query<LPOPPurchaseOrder>("GetCreatePOICS", new object[] { ProductionMonth, Timing, ApproveBy }).ToList<LPOPPurchaseOrder>();

                    if (tempList.Count == 0)
                    {
                        sessionA.WriteDirectly("MPCS00006ERR", new string[] { "From Generate Posting File PO for " + ProductionMonth + ", " + Timing }, "Create PO.Taking Data from TB_LPOP");

                        isError++;
                        Progress = PROGRESS_EXCEPTION;
                    }

                    foreach (LPOPPurchaseOrder lpoppo in tempList)
                    {
                        list.Add(lpoppo);
                    }
                }
                catch (Exception ex)
                {
                    hasil += "Production Month " + ProductionMonth + " and Timing " + Timing + ": " + ex.Message + "\n";
                    sessionA.WriteDirectly("MPCS00008ERR", new string[] { hasil }, "Create PO.Taking Data from TB_LPOP");
                }
            }
            db.Close();

            this.Progress = 10;

            sessionA.WriteDirectly("MPCS00003INF", new object[] { "Process taking data from TB_LPOP" }, "Create PO.Taking Data from TB_LPOP");
            sessionA.WriteDirectly("MPCS00002INF", new object[] { "Process sending data to ICS" }, "Create PO.Sending Data to ICS");

            GatewayService.WebServiceClient gateway = new GatewayService.WebServiceClient();
            ServiceResult result;
            bool serviceReady = false;
            try
            {
                gateway.Open();
                string testStringResult = gateway.Execute("Startup", "IntegrityCheck", null);
                if (!string.IsNullOrEmpty(testStringResult))
                {
                    result = ServiceResult.Create(testStringResult);
                    serviceReady = (result.Status == ServiceResult.STATUS_READY);
                }
            }
            catch (Exception ex)
            {
                hasil = string.Empty;
                hasil = ex.Message;

                gateway.Abort();
                gateway = new GatewayService.WebServiceClient();
                gateway.Open();

                sessionA.WriteDirectly("MPCS00008ERR", new string[] { hasil }, "Create PO.Taking Data from TB_LPOP");
            }

            if (!serviceReady)
            {
                sessionA.WriteDirectly("MPCS00007ERR", new string[] { "Web Service"}, "Create PO.Posting Data to ICS");

                isError++;
                Progress = PROGRESS_EXCEPTION;
            }

            /* Start Process 2 transfer data */
            string jsonResult;
            ServiceParameters sp = new ServiceParameters();
            string sessionID;
            sp.Add("state", "init");
            result = ServiceResult.Create(gateway.Execute("creation", "purchaseOrder", sp.ToString()));
            if (result.Status != ServiceResult.STATUS_ERROR)
            {
                sessionID = result.Value.ToString();

                sp.Clear();
                sp.Add("state", "transfer");
                sp.Add("sessionId", result.Value);
                sp.Add("data", String.Empty);
                this.Progress = 20;
                const int MAX_DATA_TRANSFERRED = 10;
                int dataLength = list.Count;
                //int lastIndex = dataLength - 1;
                int lastIndex = dataLength;
                int counter = 0;
                int fetchSize;
                int indexDifference;
                bool looping = true;
                bool transferError = false;
                while (looping)
                {
                    indexDifference = (lastIndex - counter);
                    if (indexDifference < MAX_DATA_TRANSFERRED)
                    {
                        fetchSize = indexDifference;
                    }
                    else
                    {
                        fetchSize = MAX_DATA_TRANSFERRED;
                    }

                    tempList = list.GetRange(counter, fetchSize);
                    jsonResult = JSON.ToString<List<LPOPPurchaseOrder>>(tempList);

                    sp.Remove("data");
                    sp.Add("data", jsonResult);
                    sp.Add("userId", AuthorizedUser.Username);
                    result = ServiceResult.Create(gateway.Execute("creation", "purchaseOrder", sp.ToString()));
                    if (result.Status == ServiceResult.STATUS_SUCCESS)
                    {
                        counter += (fetchSize);
                    }
                    else
                    {
                        transferError = true;
                        looping = false;
                    }

                    if (counter == lastIndex)
                    {
                        looping = false;
                    }

                }

                /* End Process 2 */
                this.Progress = 30;
                if (transferError)
                {
                    sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Transfer Web Service" }, "Create PO.Posting Data to ICS");

                    isError++;
                    Progress = PROGRESS_EXCEPTION;
                }

                /* Start Process 3 process*/
                sp.Clear();
                sp.Add("state", "process");
                sp.Add("sessionId", sessionID);
                sp.Add("prodMonth", ProductionMonth);
                sp.Add("userId", AuthorizedUser.Username);
                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                result = ServiceResult.Create(gateway.Execute("creation", "purchaseOrder", sp.ToString()));
                /* End Process 3 */
                sp.Clear();
                if (result.Status != ServiceResult.STATUS_ERROR)
                {
                    /* Start Process 4 inquiry */

                    int totalPage = Convert.ToInt32(result.Value != null ? result.Value : 0);
                    sp.Clear();
                    sp.Add("state", "inquiry");
                    sp.Add("sessionId", sessionID);
                    sp.Add("processId", "101");
                    sp.Add("prodMonth", ProductionMonth);

                    int seqLogDetail = 0;
                    for (int i = 1; i <= totalPage; i++)
                    {
                        try
                        {
                            sp.Add("currentPage", i);
                            sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                            sp.Add("prodMonth", ProductionMonth);
                            result = ServiceResult.Create(gateway.Execute("creation", "purchaseOrder", sp.ToString()));
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("Timeout") || ex.Message.Contains("timeout"))
                            {
                                sp.Add("currentPage", i);
                                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                                sp.Add("prodMonth", ProductionMonth);
                                result = ServiceResult.Create(gateway.Execute("creation", "purchaseOrder", sp.ToString()));
                            }

                            sessionA.WriteDirectly("MPCS00008ERR", new string[] { ex.Message }, "Create PO.Taking Data from TB_LPOP");
                        }

                        //sp.Clear();
                        if (result.Status != ServiceResult.STATUS_ERROR)
                        {
                            if (result.Value == null)
                            {
                                sessionA.WriteDirectly("MPCS00009ERR", new string[] { "ICS" }, "Create PO.Insert Posting Data From ICS");
                                isError++;
                                Progress = PROGRESS_EXCEPTION;
                            }

                            List<LPOPPurchaseOrderFinish> values = new List<LPOPPurchaseOrderFinish>();
                            values = JSON.ToObject<List<LPOPPurchaseOrderFinish>>(result.Value.ToString());
                            //DateTime postingDate = DateTime.Now;
                            DateTime poDate;
                            
                            double totalamount;
                            foreach (LPOPPurchaseOrderFinish e in values)
                            {
                                prodmonth = Convert.ToDateTime(e.prodMonth, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID")).Year.ToString()
                                    + Convert.ToDateTime(e.prodMonth, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID")).Month.ToString().PadLeft(2, '0');
                                //postingDate = Convert.ToDateTime(e.postingDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));
                                totalamount = Convert.ToDouble(string.IsNullOrEmpty(e.qtyN) ? "0" : e.qtyN) * Convert.ToDouble(string.IsNullOrEmpty(e.poMatPrice) ? "0" : e.poMatPrice);
                                if (e.poNo != null)
                                {
                                    poDate = Convert.ToDateTime(e.docDate.Substring(6, 2) + "-" + e.docDate.Substring(4, 2) + "-" + e.docDate.Substring(0, 4), System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));

                                    db.Execute("ICSInsertPostingCreatePO", new object[] { 
                                        e.poNo, 
                                        prodmonth, 
                                        e.suppCode, 
                                        e.poCurr,
                                        e.poExchangeRate,
                                        totalamount,
                                        e.plantCode,
                                        AuthorizedUser.Username,
                                        e.poItemNo,
                                        e.prNo,
                                        e.prItem,
                                        e.matNo,
                                        e.sourceType,
                                        e.prodPurposeCode,
                                        e.plantCode,
                                        e.slocCode,
                                        e.partColorSfx,
                                        e.packingType,
                                        e.poGrQty == null ? "0" : e.poGrQty,
                                        e.poMatPrice,
                                        null, // field required_date,
                                        poDate,
                                        processID,
                                        sessionID,
                                        seqLogDetail++,
                                    });
                                    try
                                    {
                                        bool isRegistered = WorkflowFunction.RegisterWorklist(e.poNo, "1", 10, AuthorizedUser.Username);
                                    }
                                    catch (Exception ex)
                                    {
                                        sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Create Workflow For PO No " + e.poNo, ex.Message, "", "" }, "Create PO.Create Workflow");
                                    }
                                }
                                else
                                {
                                    db.Execute("InsertLPOPConfirmation_Abnormality_Log", new object[] { 
                                            sessionID,
                                            AuthorizedUser.Username,
                                            seqLogDetail++,
                                            e.matNo,
                                            e.suppCode,
                                            e.dockCode,
                                            e.plantCode,
                                            e.slocCd,
                                            e.errMsg,
                                            ProductionMonth,
                                            "FCST",
                                            Timing
                                    });

                                    db.Execute("InsertLPOPConfirmation_Abnormality", new object[] { 
                                            ProductionMonth, //@PackMonth,
	                                        "FCST", //@DID,
	                                        Timing, //@Vers,
	                                        e.matNo, //@PartNo,
	                                        e.suppCode, //@SuppCd,
	                                        e.errCode //@ErrCd
                                    });
                                }
                                
                            }
                        }
                        else
                        {
                            sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Insert Posting Data From ICS" }, "Create PO.Insert Posting Data From ICS");
                            isError++;

                            Progress = PROGRESS_EXCEPTION;
                        }

                        if (result.Status != ServiceResult.STATUS_SUCCESS)
                        {
                            transferError = true;
                        }
                    }
                    /* End Process 4 */

                    // NANTI DILANJUTIN 
                    /* Start Process 5 finish */
                    //sp.Clear();
                    //sp.Add("state", "finish");
                    //sp.Add("sessionId", sessionID);
                    //result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));
                    //sp.Clear();
                    //if (result.Status == ServiceResult.STATUS_ERROR)
                    //{
                    //    sessionA.WriteDirectly("MPCS00002ERR", new string[] { result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString(), "", "", "" });
                    //    sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_ERROR);
                    //    sessionA.Commit();

                    //    throw new Exception(result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString());
                    //}
                    /* End Process 5 */
                }
                else
                {
                    sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Insert Posting Data From ICS" }, "Create PO.Insert Posting Data From ICS");
                    isError++;

                    Progress = PROGRESS_EXCEPTION;
                }

                sessionA.WriteDirectly("MPCS00003INF", new object[] { "Process taking data from ICS" }, "Create PO.Taking Data from ICS");
            }
            else
            {
                sessionA.WriteDirectly("MPCS00008ERR", new string[] { "Insert Posting Data From ICS" }, "Create PO.Insert Posting Data From ICS");
                isError++;

                Progress = PROGRESS_EXCEPTION;
            }

            //Update Created PO
            
           
            
            if (isError == 0)
            {
                db.Execute("UpdateCreatePO", new object[] { 
                                            ProductionMonth,
                                            AuthorizedUser.Username
                                    });

                sessionA.WriteDirectly("MPCS00003INF", new object[] { "Process sending data to ICS" }, "LPOPConfirmation.CreatePo");
                sessionA.WriteDirectly("MPCS00003INF", new object[] { "Process Create PO to ICS" }, "LPOPConfirmation.CreatePo");
                sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
            }
            else
            {
                sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_ERROR);
            }
            sessionA.Commit();

            db.Close();
            gateway.Close();
        }

        public override string GetName()
        {
            return "LPOPPosting";
        }        
    }
}