﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Configuration;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.PO;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Portal.Models.Globals;
using System.Transactions;
using System.Web.Routing;
using Telerik.Reporting.XmlSerialization;
using System.Xml;
using Telerik.Reporting.Processing;
using System.Globalization;
using Toyota.Common.Web.Workflow;


namespace Portal.Controllers
{
    public class POApprovalController : BaseController
    {
        private IDBContext _db = null;
        private IDBContext DB
        {
            get
            {
                if (_db == null)
                {

                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }

        public POApprovalController()
            : base("PO Approval")
        {
            PageLayout.UseMessageBoard = true;
        }

        public string positionInit;

        private string _userFullName = null;
        public string UserFullName
        {
            get
            {
                if (string.IsNullOrEmpty(_userFullName))
                {
                    User u = Model.GetModel<User>();
                    if (u != null)
                    {
                        if (!string.IsNullOrEmpty(u.FullName))
                            _userFullName = u.FullName;
                        else
                            _userFullName = u.Username;
                    }
                    else
                        _userFullName = "";

                }
                return _userFullName;
            }
        }

        protected override void StartUp()
        {
            TempData["FullName"] = UserFullName;
        }

        protected override void Init()
        {

            PageLayout.UseSlidingBottomPane = true;
            POApprovalModel model = new POApprovalModel();
            string userNameInit = string.Empty;

            userNameInit = UserID;

            ViewData["StatusPO"] = listStatusPOApproval();

            String initPOFrom = DateTime.Now.AddDays(-7).ToString("dd.MM.yyyy");
            String initPOTo = DateTime.Now.ToString("dd.MM.yyyy");
            int defaultReject = DB.ExecuteScalar<int>("GetDefaultReject", new object[] { "1" });
            ViewData["defaultReject"] = defaultReject - 1;
            model.PurchasingOrders = listPurchasingOrder("", initPOFrom, initPOTo, "", UserFullName, "", "");
            TempData["GridName"] = "gridSuppCode";
            ViewData["ListSupplierData"] = GetAllSuppliers();
            Model.AddModel(model);
        }

        private string SetDownloadIcon(int val)
        {
            string img = string.Empty;
            string imageBytes3 = "~/Content/Images/pdf_icon.png";
            img = imageBytes3;
            return img;
        }
        public string getOtentik(string SH, string DpH, string DH)
        {
            string resultOtentik = string.Empty;
            string jabatan = positionInit;
            //if (SH.Equals(null) || SH.Equals(""))
            if (SH.Equals("1"))
            {
                switch (jabatan)
                {
                    case "SH":
                        resultOtentik = "true";
                        break;
                    case "DpH":
                        resultOtentik = "false";
                        break;
                    case "DH":
                        resultOtentik = "false";
                        break;
                }
            }
            //else if (SH.Equals("APPROVE"))
            else if (SH.Equals("2"))
            {
                //if (DpH.Equals(null) || DpH.Equals(""))
                if (DpH.Equals("1"))
                {
                    switch (jabatan)
                    {
                        case "SH":
                            resultOtentik = "false";
                            break;
                        case "DpH":
                            resultOtentik = "true";
                            break;
                        case "DH":
                            resultOtentik = "false";
                            break;
                    }
                }
                //else if (DpH.Equals("APPROVE"))
                else if (DpH.Equals("2"))
                {
                    //if (DH.Equals("null") || DH.Equals(""))
                    if (DH.Equals("1"))
                    {
                        switch (jabatan)
                        {
                            case "SH":
                                resultOtentik = "false";
                                break;
                            case "DpH":
                                resultOtentik = "false";
                                break;
                            case "DH":
                                resultOtentik = "true";
                                break;
                        }
                    }
                    else
                    {
                        switch (jabatan)
                        {
                            case "SH":
                                resultOtentik = "false";
                                break;
                            case "DpH":
                                resultOtentik = "false";
                                break;
                            case "DH":
                                resultOtentik = "false";
                                break;
                        }
                    }
                }
            }
            //else if (SH.Equals("REJECT"))
            else if (SH.Equals("3"))
            {
                switch (jabatan)
                {
                    case "SH":
                        resultOtentik = "false";
                        break;
                    case "DpH":
                        resultOtentik = "false";
                        break;
                    case "DH":
                        resultOtentik = "false";
                        break;
                }
            }
            return resultOtentik;
        }
        public string CheckPositionSH(string Position, string SH, string DpH, string DH)
        {
            string boolChecked = string.Empty;
            if (SH.Equals(null) || SH.Equals(""))
            {
                boolChecked = "true";
            }

            return boolChecked;
        }
        private string SetNoticeIconString(string val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_open_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_close_icon.png";
            string imageBytes4 = "~/Content/Images/notice_add_icon.png";

            if (Convert.ToInt16(val) % 4 == 0)
            {
                img = imageBytes4;
            }
            else if (Convert.ToInt16(val) % 4 == 1)
            {
                img = imageBytes2;
            }
            else if (Convert.ToInt16(val) % 4 == 2)
            {
                img = imageBytes1;
            }
            else if (Convert.ToInt16(val) % 4 == 3)
            {
                img = imageBytes3;
            }
            return img;
        }
        private string SetStatusApproval(int val)
        {
            string image = string.Empty;
            string ImgByteYellow = "~/Content/Images/yellow_1.png";
            string ImgByteGreen = "~/Content/Images/Green_1.png";
            string ImgByteRed = "~/Content/Images/red.png";
            switch (val)
            {
                case 1:
                    image = ImgByteYellow;
                    break;
                case 2:
                    image = ImgByteGreen;
                    break;
                case 3:
                    image = ImgByteRed;
                    break;
            }

            return image;

        }
        public string GetStatus(string PoNo)
        {
            string result = string.Empty;
            var QueryLog = DB.Query<getDummyPoApproval>("GetStatusPO", new Object[] { PoNo });
            if (QueryLog.Any())
            {
                foreach (var c in QueryLog)
                {
                    result = c.Status;
                }
            }

            return result;

        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (_db != null)
            {
                _db.Close();
                _db = null;
            }

            base.OnResultExecuted(filterContext);
        }

        public bool getBoolAll(string status)
        {
            bool isAll = false;
            char[] splitChar = { ';' };

            string[] getstringStatus = status.Split(splitChar);
            for (int i = 0; i < getstringStatus.Length; i++)
            {
                if (getstringStatus[i] == "All" || getstringStatus[i].Equals("All"))
                {
                    isAll = true;
                    break;
                }
            }
            return isAll;
        }

        private static string[,] reportParams =
            new string[,] {
                {"SP_REP_PO_COVER", "\\Report\\POApproval\\Cover_PO.trdx"},
                {"SP_REP_PO_CPO",  "\\Report\\PO\\Rep_CPO.trdx"}
            };
   
        public void DownloadPdf(string PoNo, int ActiveTab = 0)
        {
            if (ActiveTab > 1)
                ActiveTab = 1;
            else if (ActiveTab < 0)
                ActiveTab = 0;

            string conString = DBContextNames.DB_PCS;
            string sqlCommand = reportParams[ActiveTab,0];
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PoNo", System.Data.DbType.String, PoNo);

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportParams[ActiveTab, 1], settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);


            }
            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("pdf", rpt, null);
            String CurrDate = DateTime.Now.ToString("yyyyMMddhhmm");
            string fileName = "Purchase Order_" + PoNo + "_" + CurrDate + ".pdf";

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.Flush();
            Response.End();
        }


        public List<StatusPOApproval> listStatusPOApproval()
        {
            List<StatusPOApproval> getList = new List<StatusPOApproval>();
            getList.Add(new StatusPOApproval
            {
                StatusCode = "All"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Pending SH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Pending DpH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Pending DH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Approved by SH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Approved by DpH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Approved by DH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Rejected by SH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Rejected by DpH"
            });
            getList.Add(new StatusPOApproval
            {
                StatusCode = "Rejected by DH"
            });
            return getList;
        }
        private string StatusPic(string status)
        {
            string img = string.Empty;

            string[] aStatusImg = new string[] { "yellow_1", "Green_1", "red", "gray" };
            int id = 0;
            if (int.TryParse(status, out id) && id > 0 && id <= 4)
            {

                img = "~/Content/Images/" + aStatusImg[id - 1] + ".png";
            }

            else
                img = "~/Content/Images/blank.png";
            return img;
        }


        private void SetPicOfList(List<PurchasingOrder> lipo)
        {
            if (lipo == null || (lipo.Count < 1)) return;

            for (int i = 0; i < lipo.Count; i++)
            {
                PurchasingOrder p = lipo[i];
                p.SHApprovalStatusPic = StatusPic(p.SHApprovalStatus);
                p.DPHApprovalStatusPic = StatusPic(p.DPHApprovalStatus);
                p.DHApprovalStatusPic = StatusPic(p.DHApprovalStatus);
            }
        }

        //added fid.deny 2015-07-24
        public void DownloadExport(String p_POType, String p_PODate, String p_PODateTo, String p_CurrentPIC, String p_PONo, String p_SuppCode)
        {
            if (!string.IsNullOrEmpty(p_PONo))
                p_PONo = p_PONo.Trim();


            bool boolIsAll = false;
            string p_status = "";

            if (p_status != null)
            {
                boolIsAll = getBoolAll(p_status);

            }
            switch (p_POType)
            {
                case "Monthly":
                    p_POType = "1";
                    break;
                case "CPO":
                    p_POType = "3";
                    break;
            }
            if (p_PODate != null)
            {
                if (!p_PODate.Equals(""))
                {
                    //dateFrom = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(dateFrom));
                    p_PODate = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact(p_PODate, "dd.MM.yyyy", CultureInfo.InvariantCulture));
                }
            }
            else
            {
                p_PODate = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", CultureInfo.InvariantCulture));

            }

            if (p_PODateTo != null)
            {
                if (!p_PODateTo.Equals(""))
                {
                    //dateTo = string.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(dateTo));
                    p_PODateTo = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact(p_PODateTo, "dd.MM.yyyy", CultureInfo.InvariantCulture));
                }
            }

            else
            {
                p_PODateTo = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", CultureInfo.InvariantCulture));

            }



            string conString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\POApproval\POApproval.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = conString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource.SelectCommand = "GetAllPOApprovalDownload";

            sqlDataSource.Parameters.Add("@potype", System.Data.DbType.String, p_POType);
            sqlDataSource.Parameters.Add("@datefrom", System.Data.DbType.String, p_PODate);
            sqlDataSource.Parameters.Add("@dateto", System.Data.DbType.String, p_PODateTo);

            sqlDataSource.Parameters.Add("@picFullName", System.Data.DbType.String, p_CurrentPIC);
            sqlDataSource.Parameters.Add("@poNos", System.Data.DbType.String, p_PONo);
            sqlDataSource.Parameters.Add("@suppCodes", System.Data.DbType.String, p_SuppCode);

            sqlDataSource.Parameters.Add("@status", System.Data.DbType.String, p_status);
            sqlDataSource.Parameters.Add("@isAll", System.Data.DbType.String, boolIsAll);


            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "POApproval" + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                              string.Format("{0};FileName=\"{1}\"",
                                            "attachment",
                                            fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }
        //end of added 

        public List<PurchasingOrder> listPurchasingOrder(string POType, string dateFrom, string dateTo, string status, string PIC, string PONo, string suppCode)
        {
            //System.Globalization.NumberFormatInfo nfi;
            //nfi = new System.Globalization.NumberFormatInfo();
            //nfi.CurrencySymbol = "IDR"; 

            if (!string.IsNullOrEmpty(PONo))
                PONo = PONo.Trim();
            bool boolIsAll = false;
            if (status != null)
            {
                boolIsAll = getBoolAll(status);

            }
            switch (POType)
            {
                case "Monthly":
                    POType = "1";
                    break;
                case "CPO":
                    POType = "3";
                    break;
            }
            if (dateFrom != null)
            {
                if (!dateFrom.Equals(""))
                {
                    //dateFrom = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(dateFrom));
                    dateFrom = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact(dateFrom, "dd.MM.yyyy", CultureInfo.InvariantCulture));
                }
            }
            else
            {
                dateFrom = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", CultureInfo.InvariantCulture));

            }

            if (dateTo != null)
            {
                if (!dateTo.Equals(""))
                {
                    //dateTo = string.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(dateTo));
                    dateTo = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact(dateTo, "dd.MM.yyyy", CultureInfo.InvariantCulture));
                }
            }

            else
            {
                dateTo = String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", CultureInfo.InvariantCulture));

            }
            List<PurchasingOrder> getList = new List<PurchasingOrder>();
            getList = DB.Query<PurchasingOrder>("GetAllPOApproval", new Object[] { dateFrom, dateTo, POType, status, boolIsAll, PIC, PONo, suppCode }).ToList();
            ViewData["POMonthlyCount"] = getList.Count;
            User u = Model.GetModel<User>();
            string v = "";
            if (u != null && u.PositionDetails != null && u.PositionDetails.Count > 0)
            {
                string p = u.PositionDetails[0].Position;

                switch (p.ToLower())
                {
                    case "section head": v = "SH"; break;
                    case "department head": v = "DPH"; break;
                    case "division head": v = "DH"; break;
                }
            }
            ViewData["Position"] = v;
            SetPicOfList(getList);


            return getList;
        }
        public List<ApprovalOverview> ListApprovalOverview(string PoNo, string position)
        {
            List<ApprovalOverview> listOfApproval = new List<ApprovalOverview>();
            var QueryApproval = DB.Query<ApprovalOverview>("GetApprovalOverview", new object[] { position, PoNo, AuthorizedUser.FirstName + ' ' + AuthorizedUser.LastName });
            if (QueryApproval.Any())
            {
                foreach (var i in QueryApproval)
                {
                    listOfApproval.Add(new ApprovalOverview
                    {
                        Name = i.Name,
                        Status = i.Status,
                        Date = i.Date,
                        Comment = i.Comment
                    });
                }
            }
            return listOfApproval;

        }
        public List<PurchasingOrderPart> ListPurchasingOrderPart(string Po)
        {

            List<PurchasingOrderPart> getListPart = new List<PurchasingOrderPart>();
            var QueryLog = DB.Query<PurchasingOrderPart>("GetApprovalPart", new object[] { Po });

            var culture = CultureInfo.CreateSpecificCulture("en-US");
            NumberFormatInfo format = (NumberFormatInfo)culture.NumberFormat.Clone();
            format.NumberDecimalDigits = 0;
            if (QueryLog.Any())
            {
                getListPart = QueryLog.ToList();

            }

            return getListPart;

        }

        public Boolean AddRegisterPO(string FolioNo, string ModuleCd, int StatusCd)
        {

            bool test = false;

            test = WorkflowFunction.RegisterWorklist(FolioNo, ModuleCd, StatusCd, AuthorizedUser.Username);

            return test;

        }


        public ContentResult ActionPurchaseOrder(string DocNoList, string ActionBut, string commentAction, string RejectType)
        {
            string result = DocNoList;
            string userName = AuthorizedUser.Username;
            int positionLevel = Convert.ToInt16(AuthorizedUser.PositionLevel);
            string position = string.Empty;
            string action = string.Empty;
            string comment = string.Empty;
            string PoNo = string.Empty;
            string rejectType = RejectType;

            int resultDB = 0;

            List<string> resultOk = new List<string>();
            List<string> resultNG = new List<string>();
            List<string> resultNA = new List<string>();

            string resultSuccess = "";
            string resultFailed = "";
            string resultWrongUser = "";
            string resultProcess = "";
            string processType = "";
            string rOk = "";

            action = ActionBut.ToUpper();

            if (action == "APPROVE")
            {
                processType = "Approve";
            }
            else if (action == "REJECT")
            {
                processType = "Reject";
            }
            else
            {
                processType = "Redirect";
            }
            int rType = 2;
            int.TryParse(rejectType, out rType);
            string[] Documents = DocNoList.Split(',');
            //userName = "pud.dph";
            int silent = ((Documents != null) && (Documents.Length > 1)) ? 1 : 0;
            if (action == "REJECT")
            {
                // check first 
                if (rType == 1)
                {
                    for (int i = 0; i < Documents.Length; i++)
                    {
                        string DocNo = Documents[i];
                        int h = DB.ExecuteScalar<int>("PreActionCheck", new object[] { DocNo, 1, userName, 2 });
                        if (h == -2)
                            resultNG.Add(DocNo);
                        else if (h == -1)
                            resultNA.Add(DocNo);
                        else
                            resultOk.Add(DocNo);
                    }
                    if (resultOk.Count < Documents.Length)
                    {
                        // resultProcess = "Not All Rejection Can be processed" + Environment.NewLine;
                    }
                    if (resultOk.Count > 0)
                    {
                        resultSuccess = string.Join(",", resultOk.ToArray());
                        DB.Execute("SendRejectionNotification", new object[] { resultSuccess, commentAction, userName, rType, 1 });

                    }
                }
                else
                {
                    List<string> CancelDoc = new List<string>();
                    for (int i = 0; i < Documents.Length; i++)
                    {
                        string doc = Documents[i];
                        int lastStatus = DB.ExecuteScalar<int>("GetLastApprovedStatus", new object[] { doc, 1 });
                        if ((lastStatus == 0))
                            CancelDoc.Add(doc);
                    }
                    if (CancelDoc.Count > 0)
                    {
                        resultSuccess = string.Join(",", CancelDoc.ToArray());
                        DB.Execute("SendRejectionNotification", new object[] { resultSuccess, commentAction, userName, 1, 1 });
                        if (CancelDoc.Count == Documents.Length)
                            rType = 1;
                    }
                }
                resultSuccess = "";
                resultOk.Clear();
                resultNG.Clear();
                resultNA.Clear();
            }

            foreach (string doc in Documents)
            {
                if (action == "APPROVE")
                {
                    string aOut = "";
                    resultDB = WorkflowFunction.ApproveWorklist(doc, "1", userName, ref aOut, silent);
                }
                else if (action == "REJECT")
                {
                    resultDB = WorkflowFunction.RejectWorklist(doc, "1", userName, RejectType, silent);
                }
                else
                {
                    resultDB = WorkflowFunction.RedirectWorklist(doc, "1", userName, positionLevel, silent);
                }

                if (resultDB == -1)
                {
                    resultNA.Add(doc);
                }
                else if (resultDB == -2)
                {
                    resultNG.Add(doc);
                }
                else
                {
                    resultOk.Add(doc);
                }
            }

            rOk = string.Join(",", resultOk.ToArray());
            resultSuccess = rOk;
            resultFailed = string.Join(",", resultNG.ToArray());
            resultWrongUser = string.Join(",", resultNA.ToArray());

            if (resultSuccess != "")
            {
                if (resultOk.Count > 10)
                    rOk = Environment.NewLine + WorkflowFunction.SplitLines(rOk);
                resultProcess = "INFO : Success " + processType + " Data with PO Number = " + rOk + Environment.NewLine;
                if (action != "REJECT")
                    DB.Execute("SendBulkApprovalNotification", new object[] { 1, rOk });
                else if (action == "REJECT" && rType == 2)
                    DB.Execute("SendRejectionNotification", new object[] { resultSuccess, commentAction, userName, rType, 1 });
            }

            if (resultFailed != "")
            {
                if (resultNG.Count > 10)
                    resultFailed = Environment.NewLine + WorkflowFunction.SplitLines(resultFailed);
                resultProcess = resultProcess + "ERROR : Failed " + processType + " Data with PO Number = " + resultFailed + Environment.NewLine;
            }

            if (resultWrongUser != "")
            {
                if (resultNA.Count > 10)
                    resultWrongUser = Environment.NewLine + WorkflowFunction.SplitLines(resultWrongUser);
                resultProcess = resultProcess + "ERROR : User does not have authorization to " + processType + " with PO Number = " + resultWrongUser;
            }

            return Content(resultProcess);
        }



        public ActionResult StatusPurchaseOrder()
        {
            ViewData["StatusPO"] = listStatusPOApproval();
            TempData["GridName"] = "GridStatusPO";
            return PartialView("GridLookup/PartialGrid", ViewData["StatusPO"]);
        }
        public ActionResult PartialPOHeader(string ponumber)
        {
            ViewData["PONo"] = ponumber;


            POApprovalModel model = new POApprovalModel();


            POget PONODetail = new POget();
            PONODetail = DB.SingleOrDefault<POget>("GetPODetail", new Object[] { ponumber });
            if (PONODetail == null)
            {
                PONODetail = new POget
                {
                    PONo = "",
                    PODate = DateTime.Now,
                    POType = "",
                    SHApprover = "",
                    SHApprovalDate = DateTime.Now,
                    SHApprovalStatus = "",
                    DPHApprover = "",
                    DPHApprovalDate = DateTime.Now,
                    DPHApprovalStatus = "",
                    DHApprovalStatus = "",
                    DHApprover = "",
                    StatusName = "",
                    CreatedBy = "",

                    CreatedDate = DateTime.Now,
                    ChangedBy = "",
                    ChangedDate = DateTime.Now,
                    // RequiredDate = DateTime.Now,
                    SupplierCode = "",
                    SupplierName = "",
                    CurrencyCode = "",
                    TotalAmount = "1",
                    ExchangeRate = "0",
                    DHApprovalDate = DateTime.Now,
                    //NoticeStatus = ""

                };


            }

            return PartialView("PopupPOHeader", PONODetail);
        }
        public ActionResult PartialPOApprovalGrid()
        {
            string PoType = Request.Params["POType"];
            string POFrom = Request.Params["POFrom"];
            string POTo = Request.Params["POTo"];
            string Status = Request.Params["Status"];
            string currPIC = Request.Params["CurrPIC"];
            string PONo = Request.Params["PONo"];
            string suppCode = Request.Params["SuppCode"];
            POApprovalModel model = Model.GetModel<POApprovalModel>();
            model.PurchasingOrders = listPurchasingOrder(PoType, POFrom, POTo, Status, currPIC, PONo, suppCode);

            Model.AddModel(model);
            return PartialView("PartialPOApprovalGrid", Model);
        }
        public ActionResult PartialPOApprovalGridMonthly()
        {
            string PoType = "Monthly";
            string POFrom = Request.Params["POFrom"];
            string POTo = Request.Params["POTo"];
            string Status = Request.Params["Status"];
            string PIC = Request.Params["CurrentPIC"];
            string PONo = Request.Params["PONo"];
            string suppCode = Request.Params["SuppCode"];
            POApprovalModel model = Model.GetModel<POApprovalModel>();
            model.PurchasingOrders = listPurchasingOrder(PoType, POFrom, POTo, Status, PIC, PONo, suppCode);

            Model.AddModel(model);
            return PartialView("PartialPOApprovalGrid", Model);
        }

        public ActionResult PartialPOApprovalGridCPO()
        {
            string PoType = "CPO";
            string POFrom = Request.Params["POFrom"];
            string POTo = Request.Params["POTo"];
            string Status = Request.Params["Status"];
            string currPIC = Request.Params["CurrPIC"];

            string PONo = Request.Params["PONo"];
            string suppCode = Request.Params["SuppCode"];
            POApprovalModel model = Model.GetModel<POApprovalModel>();
            model.PurchasingOrders = listPurchasingOrder(PoType, POFrom, POTo, Status, currPIC, PONo, suppCode);

            Model.AddModel(model);
            return PartialView("PartialPOApprovalGridCPO", Model);
        }

        public List<Supplier> GetAllSuppliers()
        {
            List<Supplier> listSuppliers = new List<Supplier>();
            var QueryLog = DB.Query<Supplier>("GetAllSupplier");
            listSuppliers = QueryLog.ToList<Supplier>();

            return listSuppliers;
        }

        public ActionResult SupplierPartial()
        {
            TempData["GridName"] = "gridSuppCode";
            ViewData["ListSupplierData"] = GetAllSuppliers();

            return PartialView("GridLookup/PartialGrid", ViewData["ListSupplierData"]);
        }
        public ActionResult POApprovalNoGrid(string PoNo)
        {
            POApprovalModel mdl = Model.GetModel<POApprovalModel>();
            mdl.PurchasingOrderParts = ListPurchasingOrderPart(PoNo);
            Model.AddModel(mdl);
            return PartialView("POApprovalNoGrid", Model);
        }
        public ActionResult PartialHeader()
        {
            return PartialView("HeaderPOApproval", Model);
        }
        public ActionResult IndexNo(string Po, string PoDate, string Supplier, string Amount, string RequiredDate, string Oten)
        {
            string position = positionInit;
            POApprovalModel mdl = Model.GetModel<POApprovalModel>();
            mdl.PurchasingOrderParts = ListPurchasingOrderPart(Po);
            mdl.ApprovalSH = ListApprovalOverview(Po, "SH");
            mdl.ApprovalDpH = ListApprovalOverview(Po, "DpH");
            mdl.ApprovalDH = ListApprovalOverview(Po, "DH");
            Model.AddModel(mdl);

            ViewData["PONO"] = Po;
            ViewData["PoDate"] = PoDate;
            ViewData["Supplier"] = Supplier;
            ViewData["Amount"] = Amount;
            ViewData["Status"] = GetStatus(Po);
            ViewData["Otentikasi"] = Oten;

            return View("IndexPoNo", Model);
        }
        public ActionResult UpdateCallBackPanel()
        {
            ViewData["ponumber"] = Request.Params["ponumber"];
            return PartialView("PopUpCallBackPartial");
        }
        public ActionResult PartialPickUp(string ponumber)
        {
            if (ponumber == "" || ponumber == null)
            { 
                ponumber = Request.Params["PoNo"];
            }

            POApprovalModel mdl = Model.GetModel<POApprovalModel>();
            mdl.PurchasingOrderParts = ListPurchasingOrderPart(ponumber);
            mdl.ApprovalSH = ListApprovalOverview(ponumber, "SH");
            mdl.ApprovalDpH = ListApprovalOverview(ponumber, "DpH");
            mdl.ApprovalDH = ListApprovalOverview(ponumber, "DH");
            Model.AddModel(mdl);

            return PartialView("PopUpRegulerPickUp", Model);
        }
    }


    class getDummyPoApproval
    {
        public string PositionId { set; get; }
        public string Status { set; get; }
    }
    public class StatusPOApproval
    {
        public string StatusCode { get; set; }
        //public string StatusName { get; set; }
    }
}