﻿using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Portal.Models.SupplierEmailMaster;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;

namespace Portal.Controllers
{
    public class SupplierEmailMasterController : BaseController
    {
        public SupplierEmailMasterController()
            : base("Supplier Email Master")
        { 
        
        }

        protected override void StartUp()
        { 

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierLookup> datas = db.Query<SupplierLookup>("getSupplierForLookup").ToList();
            
            SupplierEmailMasterModel um = new SupplierEmailMasterModel();
            
            Model.AddModel(um);
            ViewData["PositionData"] = getAllPositionData();
            ViewData["SupplierData"] = datas;
            ViewData["SupplierDataEdit"] = datas;

            db.Close();
        }

        public ActionResult PartialDataGrid()
        {
            try {
                string p_supplier = Request.Params["p_supplier"];
                string p_position = Request.Params["p_position"];

                string clear = Request.Params["p_clear"];

                if (p_position == "0")
                {
                    p_position = "";
                }


                SupplierEmailMasterModel um = Model.GetModel<SupplierEmailMasterModel>();


                if (clear == "N")
                {
                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    var query = db.Query<SupplierEmailMaster>("GetAllSupplierEmailData", new object[] {p_supplier, p_position});
                    um.EmailDatas = query.ToList<SupplierEmailMaster>();
                    db.Close();
                }

                Model.AddModel(um);


            }
            catch(Exception e){
            
            }

            return PartialView("partialSupplierEmailMasterGrid", Model);


        }


        public ActionResult PartialHeader()
        {
            return PartialView("partialSupplierEmailMasterHeader");
        }

        public ActionResult PartialHeaderButton()
        {
            return PartialView("partialSupplierEmailMasterHeaderButton");
        }

        public ActionResult getSupplierLookupEdit()
        {
            try
            {
                TempData["GridName"] = "grlSupplierEdit";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                List<SupplierLookup> datas = db.Query<SupplierLookup>("getSupplierForLookup").ToList();
                //ViewData["PositionDataEdit"] = getAllPositionData();

                db.Close();

                ViewData["SupplierDataEdit"] = datas;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierDataEdit"]);
        }


        public ActionResult getSupplierLookup()
        {
            try {
                TempData["GridName"] = "grlSupplier";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                List<SupplierLookup> datas = db.Query<SupplierLookup>("getSupplierForLookup").ToList();
                //ViewData["PositionData"] = getAllPositionData();

                db.Close();

                ViewData["SupplierData"] = datas;

            }
            catch (Exception e){
            
            }

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierData"]);
        }


        public ActionResult getPositionLookup()
        {
            TempData["GridName"] = "grlPosition";
            ViewData["PositionData"] = getAllPositionData();
            return PartialView("GridLookup/PartialGrid", ViewData["PositionData"]);
            
            //return PartialView("PositionComboBoxPartial", ViewData["PositionData"]);
        }

        private List<PositionLookup> getAllPositionData() 
        { 
            List<PositionLookup> pl = new List<PositionLookup>();
            
            //pl.Add(new PositionLookup()
            //{
            //    POSITION_CODE = "0",
            //    POSITION_NAME = ""
            //});
            pl.Add(new PositionLookup()
                {
                    POSITION_CODE = "1",
                    POSITION_NAME = "Staff"
                });
            pl.Add(new PositionLookup()
                {
                    POSITION_CODE = "2",
                    POSITION_NAME = "Section Head"
                });
            pl.Add(new PositionLookup()
            {
                POSITION_CODE = "3",
                POSITION_NAME = "Manager"
            });
            pl.Add(new PositionLookup()
            {
                POSITION_CODE = "4",
                POSITION_NAME = "General Manager"
            });

            return pl;
        }

        public ContentResult countData(string supplier, string position) {
            string message = "";

            try
            {
                if(position == "0")
                {
                    position = "";
                }


                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                message = db.SingleOrDefault<string>("countSupplierEmailData", new object[] { supplier, position });
                int num = Int32.Parse(message);

                if (num > 0)
                {
                    message = "download";
                }
                else
                {
                    message = "data not found";
                }
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }

            return Content(message);
        
        }


        public ActionResult DeleteUser(string email)
        {
            string resultmessage = "Delete Success.";
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("deleteSupplierEmail", new object[] {email});
                
                db.Close();
            }
            catch (Exception e) {
                resultmessage = e.Message;
            }

            return Content(resultmessage);
        }


        public ActionResult AddNewEmail(string p_supplierCode, string p_areacode, string p_nameedit, string p_emailedit, string p_positionedit, string p_status)
        {
            string resultMessage = "";
            string username = AuthorizedUser.Username;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                
            try
            {
                resultMessage = db.SingleOrDefault<string>("saveSupplierEmail", new object[] { p_supplierCode, p_positionedit, p_areacode, p_nameedit, p_emailedit, username, p_status });
            }
            catch (Exception e)
            {
                resultMessage = e.Message;
            }
            finally {
                db.Close();
            }

            return Content(resultMessage);
        
        }

        public void download(string supplier, string position) 
        {
            try
            {

                if (position == "0")
                {
                    position = "";
                }

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;

                DateTime today = DateTime.Today;

                string date = today.ToString("dd-MM-yyyy");


                string filename = "Supplier_Email_" + date + ".xls";
                string zipname = "Supplier_Email_" + date + ".zip";


                documentBytes = downloadProcess(supplier, position);

                listCompress.Add(filename, documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/zip";
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipname));

                Response.BinaryWrite(documentBytes);
                Response.Flush();
                Response.End();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;

            }
        }


        public FileContentResult DownloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "Supplier_email_template.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/SupplierEmailMaster/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        public byte[] downloadProcess(string supplier, string position)
        {
            try
            {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                string title = "Supplier Email";
                title = title.ToUpper();

                filesTmp = HttpContext.Request.MapPath("~/Template/Supplier_email_template.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;

                int row = 3;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.Alignment = HorizontalAlignment.CENTER;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.TOP;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.SetFont(Font1);



                #endregion
                List<SupplierEmailMaster> datas = new List<SupplierEmailMaster>();
                datas = db.Query<SupplierEmailMaster>("GetAllSupplierEmailData", new object[] { supplier, position }).ToList();

                ISheet sheet;

                sheet = workbook.GetSheetAt(0);
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue(title);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                Hrow.GetCell(0).CellStyle = styleContent4;

                Hrow = sheet.CreateRow(2);


                Hrow.CreateCell(0).SetCellValue("No. ");
                sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("Supplier Code  ");
                sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("Supplier Name    ");
                sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Area Code");
                sheet.AutoSizeColumn(3);
                Hrow.CreateCell(4).SetCellValue("Name");
                sheet.AutoSizeColumn(4);
                Hrow.CreateCell(5).SetCellValue("Email");
                sheet.AutoSizeColumn(5);
                Hrow.CreateCell(6).SetCellValue("Position");
                sheet.AutoSizeColumn(6);
                Hrow.CreateCell(7).SetCellValue("Created Date    ");
                sheet.AutoSizeColumn(7);
                Hrow.CreateCell(8).SetCellValue("Created By                   ");
                sheet.AutoSizeColumn(8);

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;


                int no = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    Hrow.CreateCell(0).SetCellValue(no);
                    Hrow.CreateCell(1).SetCellValue(data.SUPPLIER_CD);
                    Hrow.CreateCell(2).SetCellValue(data.SUPPLIER_NAME);
                    Hrow.CreateCell(3).SetCellValue(data.AREA_CD);
                    Hrow.CreateCell(4).SetCellValue(data.NAME);
                    Hrow.CreateCell(5).SetCellValue(data.EMAIL);
                    Hrow.CreateCell(6).SetCellValue(data.POSITION_CD);
                    Hrow.CreateCell(7).SetCellValue(data.CREATED_DT.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(8).SetCellValue(data.CREATED_BY);

                    Hrow.GetCell(0).CellStyle = styleContent1;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent1;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = styleContent1;
                    Hrow.GetCell(7).CellStyle = styleContent1;
                    Hrow.GetCell(8).CellStyle = styleContent3;

                    row++;
                    no++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e)
            {
                byte[] a = null;
                return a;
            }

        }

        
        
        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("SEMUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/SupplierEmailMaster/TempUpload/");
                return _uploadDirectory;
            }
        }


        #region ACTION
        [HttpPost]
        public ActionResult SupplierEmailUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }
        #endregion


        private SupplierEmailMasterModel AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            SupplierEmailMasterModel _UploadModel = new SupplierEmailMasterModel();

            #region DECLARE XLS PROPERTIES

            string function = "61008";
            string module = "6";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_SUPPLIER_EMAIL_UPLOAD_TEMP";
            //string tableToAccess = "TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP";

            string sheetName = "SupplierEmail";

            message = "Starting Upload Supplier Email";
            location = "SupplierEmailUpload.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_SUPPLIER_EMAIL_UPLOAD_TEMP";
                location = "SupplierEmailUpload.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
                
                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as [SupplierCode]," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as [AreaCode]," +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as [Name]," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as [Email]," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as [Position] ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    message = "Error : " + e.Message;
                    location = "SupplierEmailUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });



                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //try
                    //{
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("SupplierCode", "SUPPLIER_CODE");
                    bulkCopy.ColumnMappings.Add("AreaCode", "AREA_CODE");
                    bulkCopy.ColumnMappings.Add("Name", "NAME");
                    bulkCopy.ColumnMappings.Add("Email", "EMAIL");
                    bulkCopy.ColumnMappings.Add("Position", "POSITION");



                #endregion

                    try
                    {
                        message = "Insert Data to TB_T_SUPPLIER_EMAIL_UPLOAD_TEMP";
                        location = "SupplierEmailUpload.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });



                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "SupplierEmailUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });
                        
                        throw new Exception(ex.Message);
                    }
                }


                // validate data in temporary uploaded table.
                string validate = db.SingleOrDefault<string>("ValidateSupplierEmailUpload", new object[] { processID });

                if (validate == "error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found.")
                {
                    TempData["IsValid"] = "empty;" + processID;
                    sts = "6";
                    message = validate;
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }


                location = "ValidateSupplierEmailUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            sts      // sts
                                        });

            }
            catch (Exception exc)
            {

                TempData["IsValid"] = "false;" + processID + ";" + exc.Message;


                message = "Error : " + exc.Message;
                location = "ValidateSupplierEmailUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

            }
            finally
            {


                excelConn.Close();

                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);




            }
            return _UploadModel;
        }
        #endregion


        [HttpGet]
        public void UploadInvalid(String process_id) 
        {
            try 
            {
            
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;


                string title = "Supplier Email";
                title = title.ToUpper();

                filesTmp = HttpContext.Request.MapPath("~/Template/Supplier_email_template.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;

                int row = 1;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.Alignment = HorizontalAlignment.CENTER;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.TOP;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.SetFont(Font1);



                #endregion
                List<SupplierEmailMasterErrorList> datas = new List<SupplierEmailMasterErrorList>();
                datas = db.Query<SupplierEmailMasterErrorList>("GetErrorListForSupplierEmail", new object[] { process_id }).ToList();

                ISheet sheet;

                sheet = workbook.GetSheetAt(0);
                Hrow = sheet.CreateRow(0);
                //Hrow.CreateCell(0).SetCellValue(title);
                //sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                //Hrow.GetCell(0).CellStyle = styleContent4;

                //Hrow = sheet.CreateRow(2);


                Hrow.CreateCell(0).SetCellValue("Supplier Code  ");
                //sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("Area Code");
                //sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("Name");
                //sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Email");
                //sheet.AutoSizeColumn(3);
                Hrow.CreateCell(4).SetCellValue("Position");
                //sheet.AutoSizeColumn(4);
                Hrow.CreateCell(5).SetCellValue("Message                   ");
                //sheet.AutoSizeColumn(5);

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;


                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    Hrow.CreateCell(0).SetCellValue(data.SUPPLIER_CODE);
                    Hrow.CreateCell(1).SetCellValue(data.AREA_CODE);
                    Hrow.CreateCell(2).SetCellValue(data.NAME);
                    Hrow.CreateCell(3).SetCellValue(data.EMAIL);
                    Hrow.CreateCell(4).SetCellValue(data.POSITION);
                    Hrow.CreateCell(5).SetCellValue(data.MSGG);

                    Hrow.GetCell(0).CellStyle = styleContent1;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent1;
                    Hrow.GetCell(5).CellStyle = styleContent3;

                    row++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();

                documentBytes = ms.ToArray();


                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;


                string FileNames = "supplier_email_Invalid.xls";

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(documentBytes);
                Response.End();

            
            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();

                //return a;
            }
        
        }




        public ActionResult MoveAuthDataTemp(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("moveSupplierEmailDataTemp", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }


            return Content(message);
        }




    }
}
