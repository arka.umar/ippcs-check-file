﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Xml;
using System.Data;
using System.Transactions;
using System.IO;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;
using Portal.Models.PO;
using Portal.Models.LPOPConsolidation;
using Portal.Models.ConsolidatedLPOP;
using Portal.Models.MasterDataCompletenessCheck;
using Portal.Models.Supplier;
using Portal.Models.Forecast;
using Portal.Models.SupplierFeedbackSummary;
using Portal.Models.Globals;
using Portal.ExcelUpload;
using Portal.Models;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
////using Toyota.Common.Web.Notification;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Util.Configuration;
using System.Diagnostics;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Log;
using System.Data.OleDb;
using System.Data.SqlClient;
using Toyota.Common.Web.Service;
using Toyota.Common.Web.Util.Converter;
using Toyota.Common.Web.Workflow;
using Toyota.Common.Util.Text;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Task;
using System.Configuration;
using Microsoft.Win32.TaskScheduler;

namespace Portal.Controllers
{
    public class LPOPConsolidationController : BaseController
    {
        public LPOPConsolidationController()
            : base("LPOP Consolidation")
        {
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            //var en = new System.Globalization.CultureInfo("fr-FR");
            //string ThisMonth = DateTime.ParseExact(Convert.ToString(DateTime.Now.ToShortDateString()), "M/d/yyyy", 
            //    new System.Globalization.CultureInfo("en-US")).ToString("MMMM yyyy");

            string ThisMonth = DateTime.ParseExact(DateTime.Now.ToString("MMMM yyyy"), "MMMM yyyy", new System.Globalization.CultureInfo("en-US")).ToString("MMMM yyyy");
            string NextMonth = DateTime.ParseExact(DateTime.Now.ToString("MMMM yyyy"), "MMMM yyyy", new System.Globalization.CultureInfo("en-US")).AddMonths(1).ToString("MMMM yyyy");
            ViewData["pm_consolidation"] = ThisMonth;
            ViewData["thisMonth"] = ThisMonth;
            ViewData["nextMonth"] = NextMonth;


            LPOPConsolidationModel modelLPOP = new LPOPConsolidationModel();
            modelLPOP.LPOPConsolidations = ListLPOPConsolidation();
            modelLPOP.LPOPConsolidationConfirmations = LisiLPOPConfirmation();
            Model.AddModel(modelLPOP);

            ForecastFeedbackSummaryList model1 = new ForecastFeedbackSummaryList();
            Model.AddModel(model1);
            PageLayout.UseSlidingBottomPane = true;

            ConsolidatedLPOPModel mdl1 = new ConsolidatedLPOPModel();
            MasterDataCompletenessCheckModel model = new MasterDataCompletenessCheckModel();
            Model.AddModel(mdl1);
            Model.AddModel(model);

            List<Supplier> listSupplier = new List<Supplier>();

            for (int i = 1; i <= 100; i++)
            {
                listSupplier.Add(new Supplier
                {
                    SupplierCode = "5000" + i.ToString("0##"),
                    SupplierName = "Supplier " + i
                });
            }

            ViewData["SupplierData"] = listSupplier;

        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        private string year = "";
        private string month = "";
        private int monthVal = 0;
        private string MonthNum = "";

        public string getMonth(string month)
        {
            string result = string.Empty;
            char[] splitChar = { '-' };
            string[] splitMonth = month.Split(splitChar);
            result = GetProductionMonth(splitMonth[0].Trim() + " " + splitMonth[1].Trim());
            return result;
        }

        public string GetProductionMonth(string productionMonth)
        {
            string paramStr = (productionMonth);
            int paramStrLength = (paramStr).Length;

            string year = productionMonth.Substring(paramStrLength - 4, 4);
            string month = (paramStr).Substring(0, paramStrLength - 5);

            int monthVal = DateTime.ParseExact(month, "MMMM", new System.Globalization.CultureInfo("en-US")).Month;
            string MonthNum = monthVal.ToString().PadLeft(2, '0');

            return year + MonthNum;
        }

        public List<LPOPTabConsolidation> ListLPOPConsolidation()
        {
            IDBContext db = DbContext;
            List<LPOPTabConsolidation> QueryLog = new List<LPOPTabConsolidation>();
            try
            {
                QueryLog = db.Query<LPOPTabConsolidation>("GetLPOPConsolidation", new object[] { "", "" }).ToList<LPOPTabConsolidation>();
            }
            catch (Exception exc) { }
            finally
            {
                db.Close();
            }
            return QueryLog;
        }

        public List<LPOPTabConfirmation> LisiLPOPConfirmation()
        {
            IDBContext db = DbContext;
            List<LPOPTabConfirmation> ConfirmationList = new List<LPOPTabConfirmation>();
            try
            {
                ConfirmationList = db.Fetch<LPOPTabConfirmation>("GetLPOPConfirmation", new object[] { "", "" });
            }
            catch (Exception exc) { }
            finally
            {
                db.Close();
            }
            return ConfirmationList;
        }

        List<LPOPConfirmationDetail> ListPopupDetailLPOPConsolidation_ = null;
        private List<LPOPConfirmationDetail> ListPopupDetailLPOPConsolidation
        {
            get
            {
                if (ListPopupDetailLPOPConsolidation_ == null)
                    ListPopupDetailLPOPConsolidation_ = new List<LPOPConfirmationDetail>();

                return ListPopupDetailLPOPConsolidation_;
            }
            set
            {
                ListPopupDetailLPOPConsolidation_ = value;
            }
        }

        private List<LPOPConfirmationDetail> GetAllLPOPConsolidateDetail(String productionMonth, String sourceData, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConfirmationDetail> LPOPConsolidateDetailModelList = new List<LPOPConfirmationDetail>();
            IDBContext db = DbContext;
            try
            {
                LPOPConsolidateDetailModelList = db.Fetch<LPOPConfirmationDetail>("GetLPOPConsolidationDetail", new object[] { StrProductionMonth, sourceData, strTiming });
            }
            catch (Exception exc) { }
            finally
            {
                db.Close();
            }
            return LPOPConsolidateDetailModelList;
        }

        public ActionResult HeaderTabLPOPConsolidation()
        {
            return PartialView("HeaderTabLPOPConsolidation", Model);
        }

        public ActionResult HeaderTabLPOPConfirmation()
        {
            return PartialView("HeaderTabLPOPConfirmation", Model);
        }

        public ActionResult ReloadGridLPOPTabConsolidation()
        {
            string Timing = Request.Params["TimingConsolidation"];
            string ProductionMonth = Request.Params["PMConsolidation"];

            if (ProductionMonth.Length > 0)
            {
                ProductionMonth = GetProductionMonth(ProductionMonth);
            }

            IDBContext db = DbContext;
            LPOPConsolidationModel model = Model.GetModel<LPOPConsolidationModel>();
            model.LPOPConsolidations = db.Fetch<LPOPTabConsolidation>("GetLPOPConsolidation", new object[] { Timing, ProductionMonth });
            Model.AddModel(model);
            db.Close();
            return PartialView("PartialGridTabLPOPConsolidation", Model);
        }

        public ActionResult ReloadGridLPOPTabConfirmation()
        {
            string Timing = Request.Params["TimingConsolidation"];
            string ProductionMonth = Request.Params["PMConfirmation"];
            if (ProductionMonth.Length > 0)
            {
                ProductionMonth = GetProductionMonth(ProductionMonth);
            }

            IDBContext db = DbContext;
            LPOPConsolidationModel model = Model.GetModel<LPOPConsolidationModel>();
            model.LPOPConsolidationConfirmations = db.Query<LPOPTabConfirmation>("GetLPOPConfirmation", new object[] { ProductionMonth, Timing }).ToList<LPOPTabConfirmation>();
            Model.AddModel(model);
            db.Close();
            return PartialView("PartialGridTabLPOPConfirmation", Model);
        }

        public ActionResult PopupLPOPConsolidationDetail()
        {
            ListPopupDetailLPOPConsolidation = GetAllLPOPConsolidateDetail(Request.Params["ProductionMonth"], Request.Params["SourceData"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConsolidation);

            return PartialView("PopupLPOPConsolidationDetail", Model);
        }

        #region DETAIL POPUP SPEX
        List<LPOPConsolidationSPEXDetail> ListPopupDetailLPOPConsolidationSPEX_ = null;
        private List<LPOPConsolidationSPEXDetail> ListPopupDetailLPOPConsolidationSPEX
        {
            get
            {
                if (ListPopupDetailLPOPConsolidationSPEX_ == null)
                    ListPopupDetailLPOPConsolidationSPEX_ = new List<LPOPConsolidationSPEXDetail>();

                return ListPopupDetailLPOPConsolidationSPEX_;
            }
            set
            {
                ListPopupDetailLPOPConsolidationSPEX_ = value;
            }
        }
        private List<LPOPConsolidationSPEXDetail> GetPopupLPOPConsolidationDetail_SPEX(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConsolidationSPEXDetail> LPOPConsolidateDetailSPEXList = new List<LPOPConsolidationSPEXDetail>();
            IDBContext db = DbContext;
            LPOPConsolidateDetailSPEXList = db.Fetch<LPOPConsolidationSPEXDetail>("GetLPOPConsolidation_SPEX", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConsolidateDetailSPEXList;
        }
        public ActionResult PopupLPOPConsolidationDetail_SPEX()
        {
            ListPopupDetailLPOPConsolidationSPEX = GetPopupLPOPConsolidationDetail_SPEX(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConsolidationSPEX);

            return PartialView("PopupLPOPConsolidationSPEXDetail", Model);
        }
        #endregion

        #region DETAIL POPUP NCS
        List<LPOPConsolidationNCSDetail> ListPopupDetailLPOPConsolidationNCS_ = null;
        private List<LPOPConsolidationNCSDetail> ListPopupDetailLPOPConsolidationNCS
        {
            get
            {
                if (ListPopupDetailLPOPConsolidationNCS_ == null)
                    ListPopupDetailLPOPConsolidationNCS_ = new List<LPOPConsolidationNCSDetail>();

                return ListPopupDetailLPOPConsolidationNCS_;
            }
            set
            {
                ListPopupDetailLPOPConsolidationNCS_ = value;
            }
        }
        private List<LPOPConsolidationNCSDetail> GetPopupLPOPConsolidationDetail_NCS(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConsolidationNCSDetail> LPOPConsolidateDetailNCSList = new List<LPOPConsolidationNCSDetail>();
            IDBContext db = DbContext;
            LPOPConsolidateDetailNCSList = db.Fetch<LPOPConsolidationNCSDetail>("GetLPOPConsolidation_NCS", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConsolidateDetailNCSList;
        }
        public ActionResult PopupLPOPConsolidationDetail_NCS()
        {
            ListPopupDetailLPOPConsolidationNCS = GetPopupLPOPConsolidationDetail_NCS(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConsolidationNCS);

            return PartialView("PopupLPOPConsolidationNCSDetail", Model);
        }
        #endregion

        #region DETAIL POPUP TNQC

        List<LPOPConsolidationTNQCDetail> ListPopupDetailLPOPConsolidationTNQC_ = null;
        private List<LPOPConsolidationTNQCDetail> ListPopupDetailLPOPConsolidationTNQC
        {
            get
            {
                if (ListPopupDetailLPOPConsolidationTNQC_ == null)
                    ListPopupDetailLPOPConsolidationTNQC_ = new List<LPOPConsolidationTNQCDetail>();

                return ListPopupDetailLPOPConsolidationTNQC_;
            }
            set
            {
                ListPopupDetailLPOPConsolidationTNQC_ = value;
            }
        }
        private List<LPOPConsolidationTNQCDetail> GetPopupLPOPConsolidationDetail_TNQC(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConsolidationTNQCDetail> LPOPConsolidateDetailTNQCList = new List<LPOPConsolidationTNQCDetail>();
            IDBContext db = DbContext;
            LPOPConsolidateDetailTNQCList = db.Fetch<LPOPConsolidationTNQCDetail>("GetLPOPConsolidation_TNQC", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConsolidateDetailTNQCList;
        }
        public ActionResult PopupLPOPConsolidationDetail_TNQC()
        {
            ListPopupDetailLPOPConsolidationTNQC = GetPopupLPOPConsolidationDetail_TNQC(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConsolidationTNQC);

            return PartialView("PopupLPOPConsolidationTNQCDetail", Model);
        }

        #endregion

        #region DETAIL POPUP FCST

        List<LPOPConsolidationFCSTDetail> ListPopupDetailLPOPConsolidationFCST_ = null;
        private List<LPOPConsolidationFCSTDetail> ListPopupDetailLPOPConsolidationFCST
        {
            get
            {
                if (ListPopupDetailLPOPConsolidationFCST_ == null)
                    ListPopupDetailLPOPConsolidationFCST_ = new List<LPOPConsolidationFCSTDetail>();

                return ListPopupDetailLPOPConsolidationFCST_;
            }
            set
            {
                ListPopupDetailLPOPConsolidationFCST_ = value;
            }
        }
        private List<LPOPConsolidationFCSTDetail> GetPopupLPOPConsolidationDetail_FCST(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConsolidationFCSTDetail> LPOPConsolidateDetailFCSTList = new List<LPOPConsolidationFCSTDetail>();
            IDBContext db = DbContext;
            LPOPConsolidateDetailFCSTList = db.Fetch<LPOPConsolidationFCSTDetail>("GetLPOPConsolidation_FCST", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConsolidateDetailFCSTList;
        }
        public ActionResult PopupLPOPConsolidationDetail_FCST()
        {
            ListPopupDetailLPOPConsolidationFCST = GetPopupLPOPConsolidationDetail_FCST(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConsolidationFCST);

            return PartialView("PopupLPOPConsolidationFCSTDetail", Model);
        }

        #endregion

        #region DETAIL POPUP CONFIRMATION DETAIL

        List<LPOPConfirmationDetail> ListPopupDetailLPOPConfirmation_ = null;
        private List<LPOPConfirmationDetail> ListPopupDetailLPOPConfirmation
        {
            get
            {
                if (ListPopupDetailLPOPConfirmation_ == null)
                    ListPopupDetailLPOPConfirmation_ = new List<LPOPConfirmationDetail>();

                return ListPopupDetailLPOPConfirmation_;
            }
            set
            {
                ListPopupDetailLPOPConfirmation_ = value;
            }
        }
        private List<LPOPConfirmationDetail> GetPopupLPOPConfirmationDetail(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConfirmationDetail> LPOPConfirmationDetailList = new List<LPOPConfirmationDetail>();
            IDBContext db = DbContext;
            LPOPConfirmationDetailList = db.Fetch<LPOPConfirmationDetail>("GetLPOPConfirmationUnion", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConfirmationDetailList;
        }
        public ActionResult PopupLPOPConfirmationDetail()
        {
            ListPopupDetailLPOPConfirmation = GetPopupLPOPConfirmationDetail(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConfirmation);

            return PartialView("PopupLPOPConfirmationDetail", Model);
        }

        #endregion

        #region DETAIL POPUP ABNORMALITY

        List<LPOPConfirmationAbnormalityDetail> ListPopupDetailLPOPConfirmationAbnormality_ = null;
        private List<LPOPConfirmationAbnormalityDetail> ListPopupDetailLPOPConfirmationAbnormality
        {
            get
            {
                if (ListPopupDetailLPOPConfirmationAbnormality_ == null)
                    ListPopupDetailLPOPConfirmationAbnormality_ = new List<LPOPConfirmationAbnormalityDetail>();

                return ListPopupDetailLPOPConfirmationAbnormality_;
            }
            set
            {
                ListPopupDetailLPOPConfirmationAbnormality_ = value;
            }
        }
        private List<LPOPConfirmationAbnormalityDetail> GetPopupLPOPConfirmationDetail_Abnormality(String productionMonth, string timing)
        {
            string StrProductionMonth = "";
            string strTiming = "";

            if (productionMonth != string.Empty && productionMonth != null)
            {
                if (productionMonth.Length != 6)
                {
                    StrProductionMonth = Convert.ToDateTime(productionMonth).ToString("yyyyMM");
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
                else
                {
                    StrProductionMonth = productionMonth;
                    strTiming = "";
                    if (timing == "Firm")
                    { strTiming = "F"; }
                    else
                    { strTiming = "T"; }
                }
            }

            List<LPOPConfirmationAbnormalityDetail> LPOPConfirmationDetailAbnormalityList = new List<LPOPConfirmationAbnormalityDetail>();
            IDBContext db = DbContext;
            LPOPConfirmationDetailAbnormalityList = db.Fetch<LPOPConfirmationAbnormalityDetail>("GetLPOPConfirmation_Abnormality", new object[] { StrProductionMonth, strTiming });
            db.Close();
            return LPOPConfirmationDetailAbnormalityList;
        }
        public ActionResult PopupLPOPConfirmationAbnormality()
        {
            ListPopupDetailLPOPConfirmationAbnormality = GetPopupLPOPConfirmationDetail_Abnormality(Request.Params["ProductionMonth"], Request.Params["Timing"]);
            Model.AddModel(ListPopupDetailLPOPConfirmationAbnormality);

            return PartialView("PopupLPOPConfirmationAbnormality", Model);
        }

        #endregion

        #region DELETE ON TAB CONSOLIDATION HEADER
        public ContentResult DeleteLPOPConsolidation(string GridId)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string ProductionMonth = "";
            string SourceData = "";
            string Timing = "";

            IDBContext db = DbContext;
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);

                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    //SPLIT ID BY OBJECT WITH DELIMETER "_"
                    char[] SplitId = { '_' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);
                    ProductionMonth = ParamId[0];
                    SourceData = ParamId[1];
                    Timing = ParamId[2].ToString().Substring(0, 1);

                    //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
                    year = ProductionMonth.Substring(ProductionMonth.Length - 4, 4);
                    month = (ProductionMonth).Substring(0, ProductionMonth.Length - 7);
                    monthVal = DateTime.ParseExact(ProductionMonth, "MMMM-yyyy", new System.Globalization.CultureInfo("en-US")).Month;
                    MonthNum = monthVal.ToString().PadLeft(2, '0');

                    //UPDATE DATA
                    //Result += ParamUpdate[i] + ": ";
                    db.ExecuteScalar<string>("DeleteLPOPConsolidation", new object[] { year + MonthNum, SourceData, Timing });
                    //Result += "\n";
                }
                return Content("Succes To Deleted Data");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally { db.Close(); }
        }
        #endregion

        #region DELETE ON TAB CONFIRMATION HEADER
        public ContentResult DeleteLPOPConfirmation(string GridId)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string ProductionMonth = "";
            string Timing = "";

            IDBContext db = DbContext;
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                char[] SplitChar = { ';' };
                string[] ParamUpdate = GridId.Split(SplitChar);

                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    //SPLIT ID BY OBJECT WITH DELIMETER "_"
                    char[] SplitId = { '_' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);
                    ProductionMonth = ParamId[0];
                    Timing = ParamId[1].ToString().Substring(0, 1);

                    //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
                    year = ProductionMonth.Substring(ProductionMonth.Length - 4, 4);
                    month = (ProductionMonth).Substring(0, ProductionMonth.Length - 5);
                    monthVal = DateTime.ParseExact(month, "MMMM", new System.Globalization.CultureInfo("en-US")).Month;
                    MonthNum = monthVal.ToString().PadLeft(2, '0');

                    //UPDATE DATA
                    //Result += ParamUpdate[i] + ": ";
                    db.ExecuteScalar<string>("DeleteLPOPConfirmation", new object[] { year + MonthNum, Timing });
                    //Result += "\n";
                }
                return Content("Succes To Deleted Data");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally { db.Close(); }
        }
        #endregion

        #region CREATE PO ON TAB CONFIRMATION HEADER TO ICS
        public ContentResult CreatePO(string GridId)
        {
            string ProductionMonth = "";
            string Timing = "";
            string resultMessage = string.Empty;
            string module = "1";
            string function = "11009";
            String processID = "0";
            const string msg1 = "PO Creation {0} with Process ID {1}"; 
            IDictionary<string, object> parameters = new Dictionary<string, object>();
            parameters["GridID"] = GridId;
            parameters["User"] = SessionState.GetAuthorizedUser();

            IDBContext db = DbContext;

            char[] SplitChar = { ';' };
            string[] ParamUpdate = GridId.Split(SplitChar);
            int tempList = 0;

            for (int i = 0; i < ParamUpdate.Length; i++)
            {
                char[] SplitId = { '_' };
                string[] ParamId = ParamUpdate[i].Split(SplitId);

                ProductionMonth = ParamId[0];
                Timing = ParamId[1].ToString().Substring(0, 1);

                tempList = tempList + db.ExecuteScalar<int>("CheckCreatePOHasBeenCreated", new object[] { GetProductionMonth(ProductionMonth) });

                
            }

            if (ParamUpdate.Length == tempList)
            {
                resultMessage = "PO has been created";
            }
            try
            {
                //BackgroundTaskService service = BackgroundTaskService.GetInstance();

                //BackgroundTaskParameter taskParameter = new BackgroundTaskParameter();
                //taskParameter.Add("GridId", GridId);
                //taskParameter.Add("Username", AuthorizedUser.Username);

                //BackgroundTask immediateTask = new ExternalBackgroundTask()
                //{
                //    Id = "CPOPTR_ID",
                //    Name = "CreatePOPostingTaskRuntime",
                //    Description = "Background Task Posting Create PO",
                //    FunctionName = "CPOPTR_BT",
                //    Submitter = new Toyota.Common.Credential.User() { Username = AuthorizedUser.Username },
                //    Type = TaskType.Immediate,
                //    Range = new TaskRange()
                //    {
                //        Time = new TimeSpan(1, 54, 0)
                //    },
                //    //Parameters = new string[] { JSON.ToString<List<IDictionary<string, string>>>(dataList), process, AuthorizedUser.Username },
                //    Parameters = taskParameter,
                //    Command = service.GetTaskPath("CreatePOPostingTaskRuntime")
                //};

                //service.Register(immediateTask);
                string message = "Start Process Create PO from IPPCS";
                string location = "LPOP Create PO";

                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            AuthorizedUser.Username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                
                string ret = db.ExecuteScalar<string>("PutPCSLocalOrder", new object[] { GetProductionMonth(ProductionMonth), Timing, processID, AuthorizedUser.Username });
                
                resultMessage = string.Format(msg1, "Started", processID);
            }
            catch (Exception ex)
            {
                resultMessage = string.Format(msg1, "ERROR", processID);
                ex.PutLog(ex.GetType().Name, AuthorizedUser.Username, "LPOPConsolidation.CreatePO", Convert.ToInt64(processID), "MSTD0001INF", "INF", null, null, 1);              
            }
            return Content(resultMessage);
        }
        # endregion

        #region DOWNLOAD TEMPLATE (SCREEN HIDE BY USER) *
        public void DownloadTemplate()
        {
            String connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            //settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\Forecast\ForecastDetailData.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;

            sqlDataSource.SelectCommand = @"select * from TB_R_LPOP where 1=0";

            ReportProcessor reportProcessor = new ReportProcessor();

            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "LPOPConsolidationTemplate" + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }
        #endregion

        #region DOWNLOAD XLS ON GRID CONSOLIDATION
        public void DownloadXLS(string ProductionMonth, string SourceData, string Timing)
        {
            //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
            string YearNow = DateTime.Now.ToString("yyyy");
            year = YearNow.Substring(0, 2) + ProductionMonth.Substring(ProductionMonth.Length - 2, 2);
            month = (ProductionMonth).Substring(0, ProductionMonth.Length - 3);
            monthVal = DateTime.ParseExact(month, "MMM", new System.Globalization.CultureInfo("en-US")).Month;
            MonthNum = monthVal.ToString().PadLeft(2, '0');

            string valProdMonth = year + MonthNum;
            string valTiming = Timing.Substring(0, 1);

            string fileName = "LPOPConsolidation" + (Timing == "T" ? "Tentative" : "Firm") + year + MonthNum + SourceData + ".xls";

            //DOWNLOAD DATA
            IDBContext db = DbContext;
            IEnumerable<LPOPConsolidationDetail> qry = null;

            switch (SourceData)
            {
                case "FCST":
                    qry = db.Fetch<LPOPConsolidationDetail>("GetLPOPConsolidation_FCST", new object[] { valProdMonth, valTiming });
                    break;
                case "TNQC":
                    qry = db.Fetch<LPOPConsolidationDetail>("GetLPOPConsolidation_TNQC", new object[] { valProdMonth, valTiming });
                    break;
                case "NCS":
                    qry = db.Fetch<LPOPConsolidationDetail>("GetLPOPConsolidation_NCS", new object[] { valProdMonth, valTiming });
                    break;
                case "SPEX":
                    qry = db.Fetch<LPOPConsolidationDetail>("GetLPOPConsolidation_SPEX", new object[] { valProdMonth, valTiming });
                    break;
            }

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(ConvertToDataTable(qry), "LPOPConsolidationData");

            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
            #region NOT USE AGAIN CAUSE TELERIK SO LATE
            /*
            //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
            string YearNow = DateTime.Now.ToString("yyyy");
            year = YearNow.Substring(0, 2) + ProductionMonth.Substring(ProductionMonth.Length - 2, 2);
            month = (ProductionMonth).Substring(0, ProductionMonth.Length - 3);
            monthVal = DateTime.ParseExact(month, "MMM", new System.Globalization.CultureInfo("en-US")).Month;
            MonthNum = monthVal.ToString().PadLeft(2, '0');

            String connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\LPOPConsolidation\RepLPOPConsolidation_DownloadXLS.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;

            if (SourceData.Trim() == "FCST")
            {
                sqlDataSource.SelectCommand = @"select D_ID , VERS , REV_NO ,COMP_CD ,R_PLANT_CD ,DOCK_CD ,S_PLANT_CD ,SHIP_CD ,ORD_TYPE ,PACK_MONTH ,CAR_CD ,
                                        REC_CD = REX_CD,SRC ,PART_NO ,ORDER_TYPE ,ORDER_LOT_SZ ,KANBAN_NUMBER ,N_VOLUME ,AICO_CEPT ,N_1_VOLUME ,AICO_CEPT1 ,
                                        N_2_VOLUME ,AICO_CEPT2 ,N_3_VOLUME ,AICO_CEPT3 ,
                                        N_D01 ,N_D02 ,N_D03 ,N_D04 ,N_D05 ,N_D06 ,N_D07 ,N_D08 ,N_D09 ,N_D10 ,N_D11 ,N_D12 ,N_D13 ,N_D14 ,N_D15 ,
	                                    N_D16 ,N_D17 ,N_D18 ,N_D19 ,N_D20 ,N_D21 ,N_D22 ,N_D23 ,N_D24 ,N_D25 ,N_D26 ,N_D27 ,N_D28 ,N_D29 ,N_D30 ,N_D31 ,
	                                    N_1_D01 ,N_1_D02 ,N_1_D03 ,N_1_D04 ,N_1_D05 ,N_1_D06 ,N_1_D07 ,N_1_D08 ,N_1_D09 ,N_1_D10 ,N_1_D11 ,N_1_D12 ,N_1_D13 ,N_1_D14 ,N_1_D15 ,
	                                    N_1_D16 ,N_1_D17 ,N_1_D18 ,N_1_D19 ,N_1_D20 ,N_1_D21 ,N_1_D22 ,N_1_D23 ,N_1_D24 ,N_1_D25 ,N_1_D26 ,N_1_D27 ,N_1_D28 ,N_1_D29 ,N_1_D30 ,N_1_D31 ,
	                                    SYSTEM_DATE ,SERIES ,LIFE_CYCLE ,B_A_SIGN ,B_A_QUANTITY ,R_U_PREVIOUS ,R_U_PRESENT ,SUPPLIER_CD ,SUPPLIER_NAME ,PART_NAME ,
	                                    DUMMY ,TERM ,CREATED_BY ,CREATED_DT ,CHANGED_BY ,CHANGED_DT  
                                        from TB_R_FORECAST " +
                    "where PACK_MONTH = '" + year + MonthNum + "' and D_ID = '" + SourceData + "' and VERS = '" + Timing.Substring(0, 1) + "' ";
            }
            else if (SourceData.Trim() == "NCS")
            {
                sqlDataSource.SelectCommand = @"select D_ID , VERS , REV_NO ,COMP_CD ,R_PLANT_CD ,DOCK_CD ,S_PLANT_CD ,SHIP_CD ,ORD_TYPE ,PACK_MONTH ,CAR_CD ,
                                        REC_CD = REX_CD,SRC ,PART_NO ,ORDER_TYPE ,ORDER_LOT_SZ ,KANBAN_NUMBER ,N_VOLUME ,AICO_CEPT ,N_1_VOLUME ,AICO_CEPT1 ,
                                        N_2_VOLUME ,AICO_CEPT2 ,N_3_VOLUME ,AICO_CEPT3 ,
                                        N_D01 ,N_D02 ,N_D03 ,N_D04 ,N_D05 ,N_D06 ,N_D07 ,N_D08 ,N_D09 ,N_D10 ,N_D11 ,N_D12 ,N_D13 ,N_D14 ,N_D15 ,
	                                    N_D16 ,N_D17 ,N_D18 ,N_D19 ,N_D20 ,N_D21 ,N_D22 ,N_D23 ,N_D24 ,N_D25 ,N_D26 ,N_D27 ,N_D28 ,N_D29 ,N_D30 ,N_D31 ,
	                                    N_1_D01 ,N_1_D02 ,N_1_D03 ,N_1_D04 ,N_1_D05 ,N_1_D06 ,N_1_D07 ,N_1_D08 ,N_1_D09 ,N_1_D10 ,N_1_D11 ,N_1_D12 ,N_1_D13 ,N_1_D14 ,N_1_D15 ,
	                                    N_1_D16 ,N_1_D17 ,N_1_D18 ,N_1_D19 ,N_1_D20 ,N_1_D21 ,N_1_D22 ,N_1_D23 ,N_1_D24 ,N_1_D25 ,N_1_D26 ,N_1_D27 ,N_1_D28 ,N_1_D29 ,N_1_D30 ,N_1_D31 ,
	                                    SYSTEM_DATE ,SERIES ,LIFE_CYCLE ,B_A_SIGN ,B_A_QUANTITY ,R_U_PREVIOUS ,R_U_PRESENT ,SUPPLIER_CD ,SUPPLIER_NAME ,PART_NAME ,
	                                    DUMMY ,TERM ,CREATED_BY ,CREATED_DT ,CHANGED_BY ,CHANGED_DT 
                                        from TB_R_NCS " +
                                        "where PACK_MONTH = '" + year + MonthNum + "' and D_ID = '" + SourceData + "' and VERS = '" + Timing.Substring(0, 1) + "' ";
            }
            else if (SourceData.Trim() == "TNQC")
            {
                sqlDataSource.SelectCommand = @"select D_ID , VERS , REV_NO ,COMP_CD ,R_PLANT_CD ,DOCK_CD=DOCK_CODE ,S_PLANT_CD ,SHIP_CD ,ORD_TYPE='' ,PACK_MONTH ,CAR_CD ,
                                        REC_CD = REX_CD,SRC ,PART_NO ,ORDER_TYPE ,ORDER_LOT_SZ=ORDER_LOT_SIZE ,KANBAN_NUMBER ,N_VOLUME=N_VOL ,AICO_CEPT='' ,N_1_VOLUME=N_1_VOL ,AICO_CEPT1 ,
                                        N_2_VOLUME=N_2_VOL ,AICO_CEPT2='' ,N_3_VOLUME=N_3_VOL ,AICO_CEPT3='' ,
                                        N_D01 ,N_D02 ,N_D03 ,N_D04 ,N_D05 ,N_D06 ,N_D07 ,N_D08 ,N_D09 ,N_D10 ,N_D11 ,N_D12 ,N_D13 ,N_D14 ,N_D15 ,
	                                    N_D16 ,N_D17 ,N_D18 ,N_D19 ,N_D20 ,N_D21 ,N_D22 ,N_D23 ,N_D24 ,N_D25 ,N_D26 ,N_D27 ,N_D28 ,N_D29 ,N_D30 ,N_D31 ,
	                                    N_1_D01 ,N_1_D02 ,N_1_D03 ,N_1_D04 ,N_1_D05 ,N_1_D06 ,N_1_D07 ,N_1_D08 ,N_1_D09 ,N_1_D10 ,N_1_D11 ,N_1_D12 ,N_1_D13 ,N_1_D14 ,N_1_D15 ,
	                                    N_1_D16 ,N_1_D17 ,N_1_D18 ,N_1_D19 ,N_1_D20 ,N_1_D21 ,N_1_D22 ,N_1_D23 ,N_1_D24 ,N_1_D25 ,N_1_D26 ,N_1_D27 ,N_1_D28 ,N_1_D29 ,N_1_D30 ,N_1_D31 ,
	                                    SYSTEM_DATE ,SERIES ,LIFE_CYCLE ,B_A_SIGN ,B_A_QUANTITY=B_A_QTY ,R_U_PREVIOUS=0 ,R_U_PRESENT=0 ,SUPPLIER_CD ,SUPPLIER_NAME ,PART_NAME ,
	                                    DUMMY ,TERM ,CREATED_BY ,CREATED_DT ,CHANGED_BY ,CHANGED_DT 
                                        from TB_R_TNQC " +
                    "where PACK_MONTH = '" + year + MonthNum + "' and D_ID = '" + SourceData + "' and VERS = '" + Timing.Substring(0, 1) + "' ";
            }


            ReportProcessor reportProcessor = new ReportProcessor();

            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = year + MonthNum + "-" + SourceData + "-" + Timing + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
            */
            #endregion
        }
        #endregion

        #region GridConfirmationDownloadPOXLS
        public ContentResult GridConfirmationDownloadPOXLS(string ProductionMonth)
        {

            string YearNow = DateTime.Now.ToString("yyyy");
            year = YearNow.Substring(0, 2) + ProductionMonth.Substring(ProductionMonth.Length - 2, 2);
            month = (ProductionMonth).Substring(0, ProductionMonth.Length - 3);
            monthVal = DateTime.ParseExact(month, "MMM", new System.Globalization.CultureInfo("en-US")).Month;
            MonthNum = monthVal.ToString().PadLeft(2, '0');

            string valProdMonth = year + MonthNum;
            string valProdmonthTitle = year + "-" + MonthNum;

            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\PO\DownloadListPO.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlDataSource.SelectCommand = "SELECT row_number() OVER (ORDER BY SUPPLIER_NAME) AS SeqNbr, * FROM ( " +
	                                      "     SELECT DISTINCT POH.SUPPLIER_CD AS SUPPLIER_CD, CASE WHEN CHARINDEX('(', S.SUPPLIER_NAME) > 0 THEN SUBSTRING(S.SUPPLIER_NAME, 0, CHARINDEX('(', S.SUPPLIER_NAME)) ELSE S.SUPPLIER_NAME END AS SUPPLIER_NAME, POH.PO_NO AS PO_NO, " +
	                                      "                     CAST(YEAR(POH.CREATED_DT) AS VARCHAR) + '.' + CASE WHEN MONTH(POH.CREATED_DT) < 10 THEN  '0'+CAST(MONTH(POH.CREATED_DT) AS VARCHAR) ELSE CAST(MONTH(POH.CREATED_DT) AS VARCHAR) END + " +
	                                      "                     '.' + CASE WHEN DAY(POH.CREATED_DT) < 10 THEN  '0'+CAST(DAY(POH.CREATED_DT) AS VARCHAR) ELSE CAST(DAY(POH.CREATED_DT) AS VARCHAR) END + ' ' + " +
	                                      "                     SUBSTRING(CAST(CONVERT(TIME, POH.CREATED_DT) AS VARCHAR), 0, 6) AS PO_DATE, " +
                                          "                     UPPER(DATENAME(MONTH, '" + valProdmonthTitle + "-01')) + ' " + year + "' AS PRODUCTION_MONTH " +
                                          "     FROM TB_R_PO_H POH " +
		                                  "         LEFT JOIN TB_M_SUPPLIER S ON S.SUPPLIER_CODE = POH.SUPPLIER_CD " +
                                          "     WHERE POH.PRODUCTION_MONTH = '" + valProdMonth + "' AND POH.PO_NO IS NOT NULL " +
                                          ") tbl";

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "PO_List_" + ProductionMonth + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
            rpt.Dispose();
            return Content("Succes");
        }
        #endregion

        #region APPROVE DATA ON TAB CONFIRMATION HEADER
        public ContentResult ApproveLPOPConfirmation(string GridId)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string ProductionMonth = "";
            string Timing = "";
            string ApproveBy = "";
            string Result = "";

            //SPLIT ID WITH DELIMETER ";"
            char[] SplitChar = { ';' };
            string[] ParamUpdate = GridId.Split(SplitChar);

            //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
            IDBContext db = DbContext;
            for (int i = 0; i < ParamUpdate.Length; i++)
            {
                try
                {
                    //SPLIT ID BY OBJECT WITH DELIMETER "_"
                    char[] SplitId = { '_' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);
                    ProductionMonth = ParamId[0];
                    Timing = ParamId[1].ToString().Substring(0, 1);
                    ApproveBy = AuthorizedUser.Username; //ParamId[2];

                    TimeSpan ts = new TimeSpan(0, 30, 0);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                    {
               
                        db.Execute("InsertLPOPConfirmation_Sum", new string[] { GetProductionMonth(ProductionMonth), Timing, AuthorizedUser.Username });
                        db.ExecuteScalar<string>("InsertLPOPConfirmation_CreatePO", new string[] { AuthorizedUser.Username, GetProductionMonth(ProductionMonth), Timing, ApproveBy });
                        db.Execute("InsertSupplierFeedbackFromLPOP", new object[] { "FCST", Timing, GetProductionMonth(ProductionMonth) });
                        
                        //DOWNLOAD DATA
                             db.ExecuteScalar<string>("UpdateLPOPConfirmation_Approve", new string[] { AuthorizedUser.Username, GetProductionMonth(ProductionMonth), Timing });
                        scope.Complete();
                                                
                        //Approval Notification Email to Supplier  (Mini)
                        string prodMonth = GetProductionMonth(ProductionMonth);
                        


                        Result += "Production Month " + ProductionMonth + " and Timing " + Timing + ": success\n";

                    }
                }
                catch (Exception ex)
                {
                    string msg = "Error while Approve Production Month " + ProductionMonth + " and Timing " + Timing;

                    Result += msg;
                    ex.PutLog(msg, AuthorizedUser.Username, "ApproveLPOPConfirmation", 0, "MSTD0001INF", "INF", "0", "11005", 1);       
                }
            }
            db.Close();
            return Content(Result);
        }
        #endregion

        #region DOWNLOAD XLS ON GRID CONFIRMATION

        public void DownloadCompletenessCheck(string ProductionMonth)
        {
            //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
            string YearNow = DateTime.Now.ToString("yyyy");
            year = YearNow.Substring(0, 2) + ProductionMonth.Substring(ProductionMonth.Length - 2, 2);
            month = (ProductionMonth).Substring(0, ProductionMonth.Length - 3);
            monthVal = DateTime.ParseExact(month, "MMM", new System.Globalization.CultureInfo("en-US")).Month;
            MonthNum = monthVal.ToString().PadLeft(2, '0');

            string valProdMonth = year + MonthNum;
            string fileName = "LPOPCompletenessCheck_" + valProdMonth + ".xls";

            IDBContext db = DbContext;
            IEnumerable<LPOPCompletenessCheck> qry = db.Query<LPOPCompletenessCheck>("RepLPOPCompletenessCheck", new object[] { valProdMonth });

            IExcelWriter exporter = ExcelWriter.GetInstance();

            //List<string> footerText = new List<string>();
            //footerText.Add("NOTE :");
            //footerText.Add("1. Fill Action column with value (Blank/0 : Ignored; 1 : Insert, 2 : Edit, 3 : Delete");

            byte[] hasil = exporter.Write(qry, "LPOPCompletenessCheck");
            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        public void GridConfirmationDownloadXLS(string ProductionMonth, string Timing)
        {
            //SYNCRONIZE CONTROL MONTH YEAR WITH DATE FORMAT IN DATABASE
            string YearNow = DateTime.Now.ToString("yyyy");
            year = YearNow.Substring(0, 2) + ProductionMonth.Substring(ProductionMonth.Length - 2, 2);
            month = (ProductionMonth).Substring(0, ProductionMonth.Length - 3);
            monthVal = DateTime.ParseExact(month, "MMM", new System.Globalization.CultureInfo("en-US")).Month;
            MonthNum = monthVal.ToString().PadLeft(2, '0');

            string valProdMonth = year + MonthNum;
            string valTiming = Timing.Substring(0, 1);

            string fileName = "LPOPConfirmation" + Timing + valProdMonth + ".xls";

            IDBContext db = DbContext;
            IEnumerable<LPOPConfirmationReportUnion> qry = db.Query<LPOPConfirmationReportUnion>("RepLPOPConfirmationUnion", new object[] { valProdMonth, valTiming });

            IExcelWriter exporter = ExcelWriter.GetInstance();

            List<string> footerText = new List<string>();
            footerText.Add("NOTE :");
            footerText.Add("1. Fill Action column with value (Blank/0 : Ignored; 1 : Insert, 2 : Edit, 3 : Delete");

            byte[] hasil = exporter.Write(qry, "LPOPConfirmationData", footerText.ToArray());
            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        

        #region CONSOLIDATE DATA

        public ContentResult ConsolidatedData(string ProductionMonth, string Source1, string Source2, string Timing, string Source3 = "")
        {
            LogProvider logProvider = new LogProvider("11001", "1", AuthorizedUser);
            ILogSession sessionA = logProvider.CreateSession();


            sessionA.Write("MPCS00002INF", new object[] { "Process Consolidating Data" });
            string contentResult = string.Empty;
            IDBContext db = DbContext;
            if (!ProductionMonth.Equals("") && !ProductionMonth.Equals("null"))
            {
                try
                {
                    TimeSpan ts = new TimeSpan(0, 30, 0);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
                    {

                        string monthFix = string.Empty;
                        string timingFix = string.Empty;

                        //Insert HeaderLog
                        monthFix = getMonth(ProductionMonth);
                        timingFix = Timing.Substring(0, 1);

                        string selectSql = string.Empty;
                        if ((Source3 != "") && (Source3 != null))
                        {
                            if ((Source1.Trim() == "NCS" && Source2.Trim() == "FCST" && Source3.Trim() == "SPEX")
                                || (Source1.Trim() == "FCST" && Source2.Trim() == "NCS" && Source3.Trim() == "SPEX")
                                || (Source1.Trim() == "SPEX" && Source2.Trim() == "FCST" && Source3.Trim() == "NCS")
                                || (Source1.Trim() == "SPEX" && Source2.Trim() == "NCS" && Source3.Trim() == "FCST")
                                || (Source1.Trim() == "FCST" && Source2.Trim() == "SPEX" && Source3.Trim() == "NCS")
                                || (Source1.Trim() == "NCS" && Source2.Trim() == "SPEX" && Source3.Trim() == "FCST"))
                            {
                                selectSql = "SelectNCSForecast";

                            }
                        }
                        else {
                            if ((Source1.Trim() == "TNQC" && Source2.Trim() == "FCST") || (Source1.Trim() == "FCST" && Source2.Trim() == "TNQC"))
                            {
                                selectSql = "SelectTNQCForecast";
                            }
                        }
                        //Insert DetailLog

                        //Insert TB_R_LPOP
                        db.Execute(selectSql, new object[] { monthFix, timingFix, AuthorizedUser.Username });
                        //db.Execute("InsertSupplierFeedbackFromLPOP", new object[] { "FCST", timingFix, monthFix });

                        //Update HeaderLog
                        scope.Complete();
                    }

                }
                catch (Exception ex)
                {
                    sessionA.Write("MPCS00002ERR", new string[] { ex.Message, "", "", "" });
                    sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_ERROR);
                    sessionA.Commit();
                    throw ex;
                }
                finally
                {
                    db.Close();
                    Dispose();
                }

            }
            //
            contentResult = "succes";

            sessionA.Write("MPCS00003INF", new object[] { "Process Consolidating Data" });
            sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
            sessionA.Commit();

            return Content(contentResult);
        }

        #endregion

        #region UPLOAD CONFIRMATION

        #region UPLOAD CONFIRMATION - ACTION
        [HttpPost]
        public ActionResult HeaderTabLPOPConfirmation_Upload(String productionMonth, String version, string filePath)
        {
            ProcessAllLPOPConfirmationUpload(productionMonth, version, filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }
        #endregion

        #region UPLOAD CONFIRMATION - PROPERTIES
        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/LPOPConsolidation/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;
        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;
        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private long _processId = 0;
        private long ProcessId
        {
            get
            {
                if (_processId == 0)
                {
                    IDBContext db = DbContext;

                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); }

                    return _processId;

                    //_processId = Math.Abs(Environment.TickCount);
                }

                return _processId;
            }
        }

        private Int32 _seqNo = 0;
        private Int32 SeqNo
        {
            get
            {
                return ++_seqNo;
            }
        }
        #endregion

        #region UPLOAD CONFIRMATION - REQUEST
        private ArrayList GetUploadedFile()
        {
            ArrayList fileRowList = new ArrayList();

            try
            {
                // read uploaded excel file
                fileRowList = ExcelReader.ReaderExcel(
                    _TableDestination.filePath,
                    _TableDestination.sheetName,
                    Convert.ToInt32(_TableDestination.rowStart),
                    Convert.ToInt32(_TableDestination.columnStart),
                    Convert.ToInt32(_TableDestination.columnEnd));
            }
            catch (Exception exc)
            {
                throw new Exception("GetUploadedFile:" + exc.Message);
            }

            return fileRowList;
        }

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();

            IDBContext db = DbContext;
            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }

                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            db.Close();

            return String.Join(",", columnNameArray.ToArray());
        }

        private String GetColumnValue(String fileRow, Int32 fileColumnCount)
        {
            ArrayList columnValueArray = new ArrayList();
            String[] dumpArray = new String[fileColumnCount];

            try
            {
                // create cleaned (from unused field) from uploaded excel file
                Array.Copy(fileRow.Split(';'), 1, dumpArray, 0, fileColumnCount);

                // create column value parameter     
                columnValueArray.Add("'" + _TableDestination.functionID + "'");    // function id
                columnValueArray.Add("'" + _TableDestination.processID + "'");     // process id

                columnValueArray.Add(dumpArray[0]);     // action
                columnValueArray.Add(dumpArray[1]);     // d id    
                columnValueArray.Add(dumpArray[2]);     // version
                columnValueArray.Add(dumpArray[3]);     // r plant cd
                columnValueArray.Add(dumpArray[4]);     // dock cd
                columnValueArray.Add(dumpArray[5]);     // s plant cd
                columnValueArray.Add(dumpArray[6]);     // pack month
                columnValueArray.Add(dumpArray[7]);     // car cd
                columnValueArray.Add(dumpArray[8]);     // part no
                columnValueArray.Add(dumpArray[9]);     // n volume
                columnValueArray.Add(dumpArray[10]);    // n + 1 volume
                columnValueArray.Add(dumpArray[11]);    // n + 2 volume
                columnValueArray.Add(dumpArray[12]);    // n + 3 volume
                columnValueArray.Add(dumpArray[13]);    // supplier cd
                columnValueArray.Add(dumpArray[14]);    // supplier name
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnValue:" + exc.Message);
            }

            return String.Join(",", columnValueArray.ToArray());
        }

        private LPOPConsolidationModel ProcessAllLPOPConfirmationUpload(String productionMonth, String version, string filePath)
        {
            LPOPConsolidationModel _UploadedLPOPConsolidationModel = new LPOPConsolidationModel();

            // table destination properties
            _TableDestination.functionID = "121";
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "LPOPConfirmationData";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "46";
            _TableDestination.tableFromTemp = "TB_T_LPOP_UPLOAD";
            _TableDestination.tableToAccess = "TB_T_LPOP_VALIDATE";

            LogProvider logProvider = new LogProvider("102", "121", AuthorizedUser);
            ILogSession sessionLPOP = logProvider.CreateSession();

            OleDbConnection excelConn = null;

            IDBContext db = DbContext;
            try
            {
                sessionLPOP.Write("MPCS00004INF", new object[] { "Process Upload has been started..!!!" });

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableFromTemp });
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties = Excel 8.0";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                excelCommand = new OleDbCommand(@"select " +
                                    _TableDestination.functionID + " as FUNCTION_ID, " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @" *
                                from [" + _TableDestination.sheetName + "$]", excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");

                    bulkCopy.ColumnMappings.Add("ACTION", "ACTION");
                    bulkCopy.ColumnMappings.Add("D ID", "D_ID");
                    bulkCopy.ColumnMappings.Add("VERS", "VERS");

                    bulkCopy.ColumnMappings.Add("REV NO", "REV_NO");
                    bulkCopy.ColumnMappings.Add("COMP CD", "COMP_CD");
                    bulkCopy.ColumnMappings.Add("R PLANT CD", "R_PLANT_CD");
                    bulkCopy.ColumnMappings.Add("DOCK CD", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("S PLANT CD", "S_PLANT_CD");

                    bulkCopy.ColumnMappings.Add("SHIP CD", "SHIP_CD");
                    bulkCopy.ColumnMappings.Add("ORD TYPE", "ORD_TYPE");
                    bulkCopy.ColumnMappings.Add("PACK MONTH", "PACK_MONTH");

                    bulkCopy.ColumnMappings.Add("CAR CD", "CAR_CD");
                    bulkCopy.ColumnMappings.Add("REC CD", "REC_CD");
                    bulkCopy.ColumnMappings.Add("SRC", "SRC");

                    bulkCopy.ColumnMappings.Add("PART NO", "PART_NO");
                    bulkCopy.ColumnMappings.Add("ORDER TYPE", "ORDER_TYPE");
                    bulkCopy.ColumnMappings.Add("ORDER LOT SZ", "ORDER_LOT_SZ");
                    bulkCopy.ColumnMappings.Add("KANBAN NUMBER", "KANBAN_NUMBER");

                    bulkCopy.ColumnMappings.Add("N VOLUME", "N_VOLUME");
                    bulkCopy.ColumnMappings.Add("AICO CEPT", "AICO_CEPT");
                    bulkCopy.ColumnMappings.Add("N 1 VOLUME", "N_1_VOLUME");
                    bulkCopy.ColumnMappings.Add("AICO CEPT1", "AICO_CEPT1");
                    bulkCopy.ColumnMappings.Add("N 2 VOLUME", "N_2_VOLUME");
                    bulkCopy.ColumnMappings.Add("AICO CEPT2", "AICO_CEPT2");
                    bulkCopy.ColumnMappings.Add("N 3 VOLUME", "N_3_VOLUME");
                    bulkCopy.ColumnMappings.Add("AICO CEPT3", "AICO_CEPT3");

                    bulkCopy.ColumnMappings.Add("N D01", "N_D01");
                    bulkCopy.ColumnMappings.Add("N D02", "N_D02");
                    bulkCopy.ColumnMappings.Add("N D03", "N_D03");
                    bulkCopy.ColumnMappings.Add("N D04", "N_D04");
                    bulkCopy.ColumnMappings.Add("N D05", "N_D05");
                    bulkCopy.ColumnMappings.Add("N D06", "N_D06");
                    bulkCopy.ColumnMappings.Add("N D07", "N_D07");
                    bulkCopy.ColumnMappings.Add("N D08", "N_D08");
                    bulkCopy.ColumnMappings.Add("N D09", "N_D09");
                    bulkCopy.ColumnMappings.Add("N D10", "N_D10");
                    bulkCopy.ColumnMappings.Add("N D11", "N_D11");
                    bulkCopy.ColumnMappings.Add("N D12", "N_D12");
                    bulkCopy.ColumnMappings.Add("N D13", "N_D13");
                    bulkCopy.ColumnMappings.Add("N D14", "N_D14");
                    bulkCopy.ColumnMappings.Add("N D15", "N_D15");
                    bulkCopy.ColumnMappings.Add("N D16", "N_D16");
                    bulkCopy.ColumnMappings.Add("N D17", "N_D17");
                    bulkCopy.ColumnMappings.Add("N D18", "N_D18");
                    bulkCopy.ColumnMappings.Add("N D19", "N_D19");
                    bulkCopy.ColumnMappings.Add("N D20", "N_D20");
                    bulkCopy.ColumnMappings.Add("N D21", "N_D21");
                    bulkCopy.ColumnMappings.Add("N D22", "N_D22");
                    bulkCopy.ColumnMappings.Add("N D23", "N_D23");
                    bulkCopy.ColumnMappings.Add("N D24", "N_D24");
                    bulkCopy.ColumnMappings.Add("N D25", "N_D25");
                    bulkCopy.ColumnMappings.Add("N D26", "N_D26");
                    bulkCopy.ColumnMappings.Add("N D27", "N_D27");
                    bulkCopy.ColumnMappings.Add("N D28", "N_D28");
                    bulkCopy.ColumnMappings.Add("N D29", "N_D29");
                    bulkCopy.ColumnMappings.Add("N D30", "N_D30");
                    bulkCopy.ColumnMappings.Add("N D31", "N_D31");

                    bulkCopy.ColumnMappings.Add("N 1 D01", "N_1_D01");
                    bulkCopy.ColumnMappings.Add("N 1 D02", "N_1_D02");
                    bulkCopy.ColumnMappings.Add("N 1 D03", "N_1_D03");
                    bulkCopy.ColumnMappings.Add("N 1 D04", "N_1_D04");
                    bulkCopy.ColumnMappings.Add("N 1 D05", "N_1_D05");
                    bulkCopy.ColumnMappings.Add("N 1 D06", "N_1_D06");
                    bulkCopy.ColumnMappings.Add("N 1 D07", "N_1_D07");
                    bulkCopy.ColumnMappings.Add("N 1 D08", "N_1_D08");
                    bulkCopy.ColumnMappings.Add("N 1 D09", "N_1_D09");
                    bulkCopy.ColumnMappings.Add("N 1 D10", "N_1_D10");
                    bulkCopy.ColumnMappings.Add("N 1 D11", "N_1_D11");
                    bulkCopy.ColumnMappings.Add("N 1 D12", "N_1_D12");
                    bulkCopy.ColumnMappings.Add("N 1 D13", "N_1_D13");
                    bulkCopy.ColumnMappings.Add("N 1 D14", "N_1_D14");
                    bulkCopy.ColumnMappings.Add("N 1 D15", "N_1_D15");
                    bulkCopy.ColumnMappings.Add("N 1 D16", "N_1_D16");
                    bulkCopy.ColumnMappings.Add("N 1 D17", "N_1_D17");
                    bulkCopy.ColumnMappings.Add("N 1 D18", "N_1_D18");
                    bulkCopy.ColumnMappings.Add("N 1 D19", "N_1_D19");
                    bulkCopy.ColumnMappings.Add("N 1 D20", "N_1_D20");
                    bulkCopy.ColumnMappings.Add("N 1 D21", "N_1_D21");
                    bulkCopy.ColumnMappings.Add("N 1 D22", "N_1_D22");
                    bulkCopy.ColumnMappings.Add("N 1 D23", "N_1_D23");
                    bulkCopy.ColumnMappings.Add("N 1 D24", "N_1_D24");
                    bulkCopy.ColumnMappings.Add("N 1 D25", "N_1_D25");
                    bulkCopy.ColumnMappings.Add("N 1 D26", "N_1_D26");
                    bulkCopy.ColumnMappings.Add("N 1 D27", "N_1_D27");
                    bulkCopy.ColumnMappings.Add("N 1 D28", "N_1_D28");
                    bulkCopy.ColumnMappings.Add("N 1 D29", "N_1_D29");
                    bulkCopy.ColumnMappings.Add("N 1 D30", "N_1_D30");
                    bulkCopy.ColumnMappings.Add("N 1 D31", "N_1_D31");

                    bulkCopy.ColumnMappings.Add("SYSTEM DATE", "SYSTEM_DATE");
                    bulkCopy.ColumnMappings.Add("SERIES", "SERIES");
                    bulkCopy.ColumnMappings.Add("LIFE CYCLE", "LIFE_CYCLE");

                    bulkCopy.ColumnMappings.Add("B A SIGN", "B_A_SIGN");
                    bulkCopy.ColumnMappings.Add("B A QUANTITY", "B_A_QUANTITY");
                    bulkCopy.ColumnMappings.Add("R U PREVIOUS", "R_U_PREVIOUS");
                    bulkCopy.ColumnMappings.Add("R U PRESENT", "R_U_PRESENT");

                    bulkCopy.ColumnMappings.Add("SUPPLIER CD", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("SUPPLIER NAME", "SUPPLIER_NAME");
                    bulkCopy.ColumnMappings.Add("PART NAME", "PART_NAME");

                    bulkCopy.ColumnMappings.Add("DUMMY", "DUMMY");
                    bulkCopy.ColumnMappings.Add("TERM", "TERM");
                    bulkCopy.ColumnMappings.Add("CREATED BY", "CREATED_BY");
                    bulkCopy.ColumnMappings.Add("CREATED DT", "CREATED_DT");
                    bulkCopy.ColumnMappings.Add("CHANGED BY", "CHANGED_BY");
                    bulkCopy.ColumnMappings.Add("CHANGED DT", "CHANGED_DT");
                #endregion

                    try
                    {
                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        sessionLPOP.Write("MPCS00002ERR", new string[] { ex.Source, ex.Message, "", "" });
                        Console.WriteLine(ex.Message);
                    }
                }

                productionMonth = Convert.ToString(ds.Tables[0].Rows[0]["PACK MONTH"]);
                version = Convert.ToString(ds.Tables[0].Rows[0]["VERS"]);

                String columnNameList = GetColumnName();

                // validate data in temporary uploaded table.
                db.Execute("ValidateLPOPConfirmationUpload", new object[] { _TableDestination.functionID, _TableDestination.processID, productionMonth, version });

                // check for invalid data.
                try
                {
                    _ErrorValidationList = db.Fetch<ErrorValidation>("GetErrorMessageUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });
                }
                catch (Exception exc)
                {
                    sessionLPOP.Write("MPCS00002ERR", new string[] { exc.Source, exc.Message, "", "" });
                    throw exc;
                }

                if (_ErrorValidationList.Count == 0)
                {
                    // insert data from temporary uploaded table into temporary validated table.
                    db.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] { _TableDestination.tableToAccess, columnNameList, columnNameList, _TableDestination.tableFromTemp });

                    // read uploaded data from temporary validated table to transcation table.
                    //_UploadedLPOPConsolidationModel = GetAllLPOPConsolidationUpload(productionMonth, version, _TableDestination.processID, _TableDestination.functionID);

                    // set process validation status.
                    TempData["IsValid"] = "true;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    sessionLPOP.Write("MPCS00004INF", new object[] { @"Insert upload success info into table log detail.
                                                                        Insert data from temporary uploaded table into temporary validated table.
                                                                        Read uploaded data from temporary validated table to transcation table.
                                                                        set process validation status...!!!" });
                }
                else
                {

                    // load previous data for supplier feedback part model
                    //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail = GetAllLPOPConsolidationUpload(productionMonth, version);

                    // set process validation status.
                    TempData["IsValid"] = "false;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;

                    sessionLPOP.Write("MPCS00004INF", new object[] { @"Insert upload fail info into table log detail
                                                                        set process validation status...!!!" });
                }
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                sessionLPOP.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });

                // load previous data for supplier feedback part model
                //_UploadedLPOPConsolidationModel.LPOPConfirmationReportDetail= GetAllFeedbackPart(productionMonth, supplierCode, supplierPlanCode, version);
            }
            finally
            {
                sessionLPOP.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                sessionLPOP.Commit();

                excelConn.Close();

                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }
            db.Close();
            return _UploadedLPOPConsolidationModel;
        }

        [HttpPost]
        public void ClearAllFeedbackPartUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            try
            {
                // clear data in temporary uploaded table.
                db.Execute("ClearFeedbackPartUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception exc)
            {
                throw new Exception();
            }
            db.Close();
        }

        [HttpPost]
        public void UplLPOPConfirmation_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("UplLPOPConfirmation", ValidationSettings, UplLPOPConfirmation_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void UplLPOPConfirmation_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }
        #endregion

        #region UPLOAD CONFIRMATION - RESULT
        public void UploadValid(String processId, String functionId, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            db.ExecuteScalar<string>("UpdateLPOPConfirmationUpload",
                            new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            AuthorizedUser.Username
                        });

            db.Close();
        }

        [HttpGet]
        public void DownloadInvalid(String processId, String functionId, String temporaryTableUpload)
        {

            string fileName = "LPOPConfirmationUploadInvalid" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportLPOPConfirmationUploadError> qry = db.Query<ReportLPOPConfirmationUploadError>("ReportGetLPOPUploadError", new object[] { functionId, processId });

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(ConvertToDataTable(qry), "LPOPConfirmationData");

            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        #endregion

        #region UPDATE TIME

        public ContentResult UpdateDataGrid(string feedbackDueDate, string lastRemindingDate, string prodMonth, string timing)
        {
            string resultMessage = string.Empty;
            string module = "1";
            string function = "11005";
            string processID = "0";

            prodMonth = GetProductionMonth(prodMonth);
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

     
            try
            {
                string message = "Start Process Register Job LPOP Confirmation";
                string location = "LPOP Confirmation";

                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                            message,     // what
                            AuthorizedUser.Username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                db.Execute("UpdateRemindingTime", new object[] { prodMonth, ((timing == "Firm") ? "F" : "T"), formatingShortDate(feedbackDueDate), formatingShortDate(lastRemindingDate), processID });


                return Content("Success");
            }
            catch (Exception ex)
            {
                string msg = "Error, failed update";

                ex.PutLog(msg, AuthorizedUser.Username, "UpdateDataGrid", 0, "MSTD0001INF", "INF", "0", "11005", 1);
                return Content(msg);
            }
            finally
            {
                db.Close();
            }
        }

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                result = extractString[2] + "/" + extractString[1] + "/" + extractString[0];
            }

            return result;
        }

        #endregion

        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}