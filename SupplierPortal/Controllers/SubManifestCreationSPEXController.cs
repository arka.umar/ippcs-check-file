﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using System.Data.OleDb;
using System.Data.SqlClient;
using Portal.Models.Supplier;
using Portal.Models.SubManifestCreationSPEX;

namespace Portal.Controllers
{
    public class SubManifestCreationSPEXController : BaseController
    {
        public SubManifestCreationSPEXController() : base("Sub Manifest Creation") { }

        private IDBContext dbcontext()
        {
            return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            try
            {
                IDBContext db = dbcontext();

                var supplierList = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");

                List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
                List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "SubManifestCreationSPEX", "gridSubSupplier");
                List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSubSupplier");
                subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

                List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                    .Select(x => new SupplierNameSPEX()
                                    {
                                        SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                        SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                    }).ToList();

                var subsupplierDatas = distinctSubSuppCD;
                db.Close();

                ViewData["supplierDatas"] = supplierList;
                ViewData["subsupplierDatas"] = subsupplierDatas;
                ViewData["ETD"] = DateTime.Now.ToString("dd.MM.yyyy");
            }
            catch (Exception ex)
            {

            }
        }

        #region preparing screen

        public ActionResult PartialHeader()
        {
            return PartialView("PartialSubManifestCreationHeader");
        }

        public ActionResult PartialHeaderButton()
        {
            return PartialView("PartialSubManifestCreationButton");
        }

        public ActionResult PartialHeaderCriteria()
        {
            return PartialView("PartialSubManifestCreationCriteriaSubmit");
        }

        public ActionResult PartialGrid()
        {
            return PartialView("PartialSubManifestCreationGrid");
        }

        public ActionResult getSupplierData()
        {
            IDBContext db = dbcontext();
            List<SupplierName> supplierList = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            db.Close();

            TempData["GridName"] = Request.Params["p_gridName"];

            ViewData["supplierDatas"] = supplierList;
            return PartialView("GridLookup/PartialGrid", ViewData["supplierDatas"]);
        }

        public ActionResult getSubSupplierData()
        {
            IDBContext db = dbcontext();
            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "SubManifestCreationSPEX", "gridSubSupplier");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSubSupplier");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();
            db.Close();

            TempData["GridName"] = Request.Params["p_gridName"];

            ViewData["subsupplierDatas"] = distinctSubSuppCD;
            return PartialView("GridLookup/PartialGrid", ViewData["subsupplierDatas"]);
        }

        public ActionResult SubManifestCreationSPEXAction()
        {
            try
            {
                List<SubManifestCreationSPEX> model = new List<SubManifestCreationSPEX>();

                model = GetAllSubManifestCreation(Request.Params["SupplierCode"], Request.Params["ManifestNo"], Request.Params["SubSupplierCode"], Request.Params["SubManifestNo"]);

                List<SubManifestCreationSPEX> modelDistinct = model.GroupBy(y => new { y.MANIFEST_NO, y.MANIFEST_NO_DT, y.SUPPLIER_CD, y.SUB_SUPP_CD, y.SUB_ROUTE_CD, y.DOCK_CD })
                                .Select(x => new SubManifestCreationSPEX()
                                {
                                    MANIFEST_NO = x.Key.MANIFEST_NO,
                                    MANIFEST_NO_DT = x.Key.MANIFEST_NO_DT,
                                    SUPPLIER_CD = x.Key.SUPPLIER_CD,
                                    SUB_SUPP_CD = x.Key.SUB_SUPP_CD,
                                    SUB_ROUTE_CD = x.Key.SUB_ROUTE_CD,
                                    DOCK_CD = x.Key.DOCK_CD
                                }).ToList();

                ViewData["gridData"] = model;

                ViewData["DetManifestNo"] = modelDistinct.SingleOrDefault().MANIFEST_NO.ToString();
                ViewData["DetManifestNoDt"] = modelDistinct.SingleOrDefault().MANIFEST_NO_DT.ToString();
                ViewData["DetSupplierCd"] = modelDistinct.SingleOrDefault().SUPPLIER_CD.ToString();
                ViewData["DetSubSupplierCd"] = modelDistinct.SingleOrDefault().SUB_SUPP_CD.ToString();
                ViewData["DetRoute"] = modelDistinct.SingleOrDefault().SUB_ROUTE_CD.ToString();
                ViewData["DetDockCd"] = modelDistinct.SingleOrDefault().DOCK_CD.ToString();
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("PartialSubManifestCreationGrid", ViewData["gridData"]);
        }

        public ActionResult RetrieveRoute()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string param = Convert.ToDateTime(Request.Params["ETD"].Substring(4, 20)).ToString("yyyy-MM-dd");
            var QueryLog = db.Query<ComboRoute>("GetRoutebyETD", new object[] { param });
            ViewData["ComboRoute"] = QueryLog.ToList<ComboRoute>();
            db.Close();
            return PartialView("PartialComboRoute", ViewData["ComboRoute"]);
        }
        #endregion

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> SubManifestSupplierModel = new List<SupplierName>();
            IDBContext db = dbcontext();
            SubManifestSupplierModel = db.Fetch<SupplierName>("GetAllSupplierSPEXNEW", new object[] { });
            db.Close();

            return SubManifestSupplierModel;
        }

        private List<SupplierNameSPEX> GetAllSubSupplier()
        {
            List<SupplierNameSPEX> SubManifestSupplierModel = new List<SupplierNameSPEX>();
            IDBContext db = dbcontext();
            SubManifestSupplierModel = db.Fetch<SupplierNameSPEX>("GetAllSubSupplierSPEXNEW", new object[] { });
            db.Close();

            return SubManifestSupplierModel;
        }

        private List<SubManifestCreationSPEX> GetAllSubManifestCreation(String supplierCode, String manifestNo, string SubSupplierCode, string SubManifestNo)
        {
            IDBContext db = dbcontext();
            string SupplierCodeLDAP = "";
            string SubSupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            List<SubManifestCreationSPEX> SubManifestCreationModel = db.Fetch<SubManifestCreationSPEX>("GetAllSubManifestCreation", new object[] {
                supplierCode == null ? "" : supplierCode,
                manifestNo == null ? "" : manifestNo,
                SubSupplierCode ,
                SupplierCodeLDAP,
                SubManifestNo                 
                //,//SubSupplierCodeLDAP
            });
            db.Close();

            return SubManifestCreationModel;
        }

        #endregion

        #region upload
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public FileContentResult downloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "sub_manifest_creation_template.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/SubManifestCreationSPEX/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("SMCUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/SubManifestCreationSPEX/TempUpload/");
                return _uploadDirectory;
            }
        }

        public ActionResult SMCUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        private SubManifestCreationSPEX AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            SubManifestCreationSPEX _UploadModel = new SubManifestCreationSPEX();

            string function = "81001";
            string module = "8";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;

            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            string companyID = SupplierCodeLDAP; //AuthorizedUser.CompanyID;
            string sts = "0";
            string lockingBy = companyID;

            if (lockingBy == null || lockingBy == "")
                lockingBy = username;

            string tableFromTemp = "TB_T_SPEX_SUB_MANIFEST";

            string sheetName = "SubManifestCreation";

            long lockPid = db.SingleOrDefault<long>("GetLockByReff", new object[] { function, lockingBy });
            if (lockPid != 0)
            {
                TempData["IsValid"] = "lock;" + processID + ";" + "Process is being locked by another user.";
            }
            else
            {
                message = "Starting Upload Sub Manifest Creation";
                location = "SubManifestCreationUpload.init";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                string lockReff = lockingBy + "_" + processID;

                int getLock = db.SingleOrDefault<int>("InsertLock", new object[] { function, lockReff, processID });
                //if (getLock != 1)
                //{
                //    TempData["IsValid"] = "false;" + processID + ";" + "Unable to Acquire Lock, Cancel Upload failed";
                //}

                OleDbConnection excelConn = null;

                try
                {

                    message = "Clear TB_T_SPEX_SUB_MANIFEST";
                    location = "SubManifestCreationUpload.process";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                    // clear temporary upload table.
                    db.ExecuteScalar<String>("ClearSubManifestTempUpload", new object[] { tableFromTemp, username });

                    #region EXECUTE EXCEL AS TEMP_DB
                    // read uploaded excel file,
                    // get temporary upload table column name, 
                    // get uploaded excel file column value and
                    // insert uploaded excel file value into temporary upload table.
                    DataSet ds = new DataSet();
                    OleDbCommand excelCommand = new OleDbCommand();
                    OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                    //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                    string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                    excelConn = new OleDbConnection(excelConnStr);
                    excelConn.Open();
                    DataSet dsExcel = new DataSet();
                    DataTable dtPatterns = new DataTable();

                    string OleCommand = @"select " +
                                        @"IIf(IsNull(F1), Null, CStr(F1)) as DataId," +
                                        processID + " as PROCESS_ID, " +
                                        @"IIf(IsNull(F2), Null, CStr(F2)) as ManifestNo," +
                                        @"IIf(IsNull(F3), Null, CStr(F3)) as partNo," +
                                        @"IIf(IsNull(F4), Null, CStr(F4)) as itemNo," +
                                        @"IIf(IsNull(F5), Null, CStr(F5)) as QtyDelivery," +
                                        @"IIf(IsNull(F6), Null, CStr(F6)) as CaseNo," +
                                        @"IIf(IsNull(F7), Null, CStr(F7)) as kanbanId," +
                                        @"IIf(IsNull(F8), Null, CStr(F8)) as etd," +
                                        @"IIf(IsNull(F9), Null, CStr(F9)) as supref1," +
                                        @"IIf(IsNull(F10), Null, CStr(F10)) as supref2 ";
                    OleCommand = OleCommand + "from [" + sheetName + "$]";

                    excelCommand = new OleDbCommand(OleCommand, excelConn);

                    excelDataAdapter.SelectCommand = excelCommand;

                    try
                    {
                        excelDataAdapter.Fill(dtPatterns);
                        ds.Tables.Add(dtPatterns);

                        DataRow rowDel = ds.Tables[0].Rows[0];
                        ds.Tables[0].Rows.Remove(rowDel);
                        ds.Tables[0].Columns.Add("username", typeof(string));

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            row["username"] = username;
                        }
                    }
                    catch (Exception e)
                    {

                        message = "Error : " + e.Message;
                        location = "SubManifestCreationUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });



                        throw new Exception(e.Message);
                    }

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                    {
                        //try
                        //{
                        bulkCopy.DestinationTableName = tableFromTemp;
                        bulkCopy.ColumnMappings.Clear();

                        bulkCopy.ColumnMappings.Add("DataId", "DATA_ID");
                        bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                        bulkCopy.ColumnMappings.Add("ManifestNo", "MANIFEST_NO");
                        bulkCopy.ColumnMappings.Add("partNo", "PART_NO");
                        bulkCopy.ColumnMappings.Add("itemNo", "ITEM_NO");
                        bulkCopy.ColumnMappings.Add("QtyDelivery", "QTY_DELIVERY");
                        bulkCopy.ColumnMappings.Add("CaseNo", "CASE_NO");
                        bulkCopy.ColumnMappings.Add("kanbanId", "KANBAN_ID");
                        bulkCopy.ColumnMappings.Add("etd", "ETD");
                        bulkCopy.ColumnMappings.Add("supref1", "SUPPLIER_REF_1");
                        bulkCopy.ColumnMappings.Add("supref2", "SUPPLIER_REF_2");
                        bulkCopy.ColumnMappings.Add("username", "USER_ID");

                    #endregion

                        try
                        {
                            message = "Insert Data to TB_T_SPEX_SUB_MANIFEST";
                            location = "SubManifestCreationUpload.process";

                            processID = db.ExecuteScalar<string>(
                                                "GenerateProcessId",
                                                new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                            // Write from the source to the destination.
                            // Check if the data set is not null and tables count > 0 etc
                            bulkCopy.WriteToServer(ds.Tables[0]);
                        }
                        catch (Exception ex)
                        {
                            message = "Error : " + ex.Message;
                            location = "SubManifestCreationUpload.finish";

                            processID = db.ExecuteScalar<string>(
                                                "GenerateProcessId",
                                                new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                            throw new Exception(ex.Message);
                        }
                    }


                    // validate data in temporary uploaded table.
                    string validate = db.SingleOrDefault<string>("ValidateSMCUpload", new object[] { processID, companyID });

                    if (validate == "error")
                    {
                        TempData["IsValid"] = "false;" + processID;
                        sts = "6";
                        message = "Upload finish with error.";
                    }
                    else if (validate == "No Data Found.")
                    {
                        TempData["IsValid"] = "empty";
                        sts = "6";
                        message = validate;
                        int delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                    }
                    else
                    {
                        TempData["IsValid"] = "true;" + processID + ";" + lockReff;
                        sts = "0";
                        message = "Validation data in table TB_T_SPEX_SUB_MANIFEST";
                    }

                    location = "SubManifestCreationUpload.process";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            sts      // sts
                                        });


                    //location = "SubManifestCreationUpload.finish";

                    //processID = db.ExecuteScalar<string>(
                    //                    "GenerateProcessId",
                    //                    new object[] { 
                    //                        message,     // what
                    //                        username,     // user
                    //                        location,     // where
                    //                        processID,     // pid OUTPUT
                    //                        "MPCS00004INF",     // id
                    //                        "",     // type
                    //                        module,     // module
                    //                        function,     // function
                    //                        sts      // sts
                    //                    });

                }
                catch (Exception exc)
                {

                    TempData["IsValid"] = "false;" + processID + ";" + exc.Message;


                    message = "Error : " + exc.Message;
                    location = "SubManifestCreationUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

                    int delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });

                }
                finally
                {

                    excelConn.Close();

                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);

                }
            }

            return _UploadModel;
        }

        public ActionResult MoveSMCDataTemp(string processId)
        {
            string message = "";
            string msgSP = "";
            string user = AuthorizedUser.Username;
            string function = "81001";
            string module = "8";
            string location = "";
            string username = AuthorizedUser.Username;

            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            string companyID = SupplierCodeLDAP; //AuthorizedUser.CompanyID;
            string lockingBy = companyID;
            if (lockingBy == null || lockingBy == "")
                lockingBy = username;

            string lockReff = lockingBy + "_" + processId;//AuthorizedUser.CompanyID + "_" + processId;
            string sts = "0";
            string proccessSubmit = "";

            IDBContext db = dbcontext();

            try
            {
                msgSP = "Sub Manifest Creation Batch process is started";
                location = "SubManifestCreationUpload.process";

                processId = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            msgSP,     // what
                                            username,     // user
                                            location,     // where
                                            processId,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                proccessSubmit = db.SingleOrDefault<string>("moveSMCDataTemp", new object[] { processId, user, companyID }); //db.Execute("moveSMCDataTemp", new object[] { processId, user });
                var ItemPar = proccessSubmit.Split('|');
                if (ItemPar[0] == "SUCCESS")
                {
                    message = proccessSubmit + ";" + processId;
                    msgSP = "Sub Manifest Creation process is finished sucessfully";
                    sts = "0";
                    int delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                }
                else
                {
                    message = "ERROR|" + ItemPar[1];
                    msgSP = ItemPar[1];
                    sts = "6";
                    int delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                }

                location = "SubManifestCreationUpload.finish";

                processId = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                        msgSP,     // what
                                        username,     // user
                                        location,     // where
                                        processId,     // pid OUTPUT
                                        "MPCS00004INF",     // id
                                        "",     // type
                                        module,     // module
                                        function,     // function
                                        sts      // sts
                                    });

            }
            catch (Exception ex)
            {
                message = "ERROR|" + ex.Message;
                msgSP = ex.Message;

                location = "SubManifestCreationUpload.finish";

                processId = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            msgSP,     // what
                                            username,     // user
                                            location,     // where
                                            processId,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

                int delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
            }

            return Content(message);
        }

        public void UploadInvalid(string process_id)
        {
            string username = AuthorizedUser.Username;
            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            string lockingBy = SupplierCodeLDAP;
            if (lockingBy == null || lockingBy == "")
                lockingBy = username;

            string lockReff = lockingBy + "_" + process_id;//AuthorizedUser.CompanyID + "_" + processId;
            string function = "81001";

            IDBContext db = dbcontext();
            int delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<SMCListError> boForExcel = new List<SMCListError>();

            boForExcel = db.Query<SMCListError>("getSMCErrorList", new object[] { process_id }).ToList();

            fileName = "SubManifestCreationListError" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            data = exporter.Write(ConvertToDataTable(boForExcel), "SubManifestCreation");

            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }
        #endregion

        #region submit
        //manifest|part no|item no|remain qty|del qty
        //P118004011|759240K13000|2|1|1;P118004011|759560K02000|3|1|1;
        public ActionResult SubmitData(String iListKeySubmit, String ETD, String Ref1, String Ref2) //String iListQty, 
        {
            IDBContext db = dbcontext();
            string function = "81001";
            string module = "8";
            string location = "";
            string message = "";
            string processID = "";
            string msgSP = "";
            string username = AuthorizedUser.Username;

            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            string companyID = SupplierCodeLDAP; //AuthorizedUser.CompanyID;
            string lockingBy = companyID;
            if (lockingBy == null || lockingBy == "")
                lockingBy = username;

            string sts = "0";
            int iIndex = 0;
            List<SubManifestCreationSPEX> listDataModel = new List<SubManifestCreationSPEX>();
            SubManifestCreationSPEX dataModel = new SubManifestCreationSPEX();
            var listKeyHeader = iListKeySubmit.Split(';');
            //string[] listQty = iListQty.Split(';');
            string tableFromTemp = "TB_T_SPEX_SUB_MANIFEST";
            int delLock = 0;

            long lockPid = db.SingleOrDefault<long>("GetLockByReff", new object[] { function, lockingBy });

            if (lockPid != 0)
            {
                message = "ERROR|Lock";
                //TempData["IsValid"] = "lock;" + processID + ";" + "Process is being locked by another user.";
            }
            else
            {
                message = "Starting Sub Manifest Creation Batch";
                location = "SubManifestCreationBatch.init";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                string lockReff = lockingBy + "_" + processID;

                int getLock = db.SingleOrDefault<int>("InsertLock", new object[] { function, lockReff, processID });

                try
                {
                    message = "Clear TB_T_SPEX_SUB_MANIFEST";
                    location = "SubManifestCreationBatch.process";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                    // clear temporary upload table.
                    db.ExecuteScalar<String>("ClearSubManifestTempUpload", new object[] { tableFromTemp, username });

                    #region Create Data Table
                    DataTable table = new DataTable();

                    table.Columns.Add("DATA_ID");
                    table.Columns.Add("PROCESS_ID");
                    table.Columns.Add("MANIFEST_NO");
                    table.Columns.Add("PART_NO");
                    table.Columns.Add("ITEM_NO");
                    table.Columns.Add("QTY_DELIVERY");
                    table.Columns.Add("CASE_NO");
                    table.Columns.Add("KANBAN_ID");
                    table.Columns.Add("ETD");
                    table.Columns.Add("SUPPLIER_REF_1");
                    table.Columns.Add("SUPPLIER_REF_2");
                    table.Columns.Add("USER_ID");
                    #endregion

                    foreach (var lsKeyHeader in listKeyHeader)
                    {
                        if (lsKeyHeader == "")
                        {
                            continue;
                        }
                        else
                        {
                            var listKeyDetail = lsKeyHeader.Split('|');
                            DataRow row = table.NewRow();
                            row["DATA_ID"] = "D";
                            row["PROCESS_ID"] = processID;
                            row["MANIFEST_NO"] = listKeyDetail[0].ToString();
                            row["PART_NO"] = listKeyDetail[1].ToString();
                            row["ITEM_NO"] = listKeyDetail[2].ToString();
                            row["QTY_DELIVERY"] = listKeyDetail[4].ToString(); //listQty[iIndex].ToString();
                            row["CASE_NO"] = "";
                            row["KANBAN_ID"] = "";
                            row["ETD"] = ETD;
                            row["SUPPLIER_REF_1"] = Ref1;
                            row["SUPPLIER_REF_2"] = Ref2;
                            row["USER_ID"] = username;

                            table.Rows.Add(row);
                        }
                        iIndex++;
                    }

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                    {
                        //try
                        //{
                        bulkCopy.DestinationTableName = tableFromTemp;
                        bulkCopy.ColumnMappings.Clear();

                        bulkCopy.ColumnMappings.Add("DATA_ID", "DATA_ID");
                        bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                        bulkCopy.ColumnMappings.Add("MANIFEST_NO", "MANIFEST_NO");
                        bulkCopy.ColumnMappings.Add("PART_NO", "PART_NO");
                        bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                        bulkCopy.ColumnMappings.Add("QTY_DELIVERY", "QTY_DELIVERY");
                        bulkCopy.ColumnMappings.Add("CASE_NO", "CASE_NO");
                        bulkCopy.ColumnMappings.Add("KANBAN_ID", "KANBAN_ID");
                        bulkCopy.ColumnMappings.Add("ETD", "ETD");
                        bulkCopy.ColumnMappings.Add("SUPPLIER_REF_1", "SUPPLIER_REF_1");
                        bulkCopy.ColumnMappings.Add("SUPPLIER_REF_2", "SUPPLIER_REF_2");
                        bulkCopy.ColumnMappings.Add("USER_ID", "USER_ID");

                        try
                        {
                            message = "Insert Data to TB_T_SPEX_SUB_MANIFEST";
                            location = "SubManifestCreationBatch.process";

                            processID = db.ExecuteScalar<string>(
                                                "GenerateProcessId",
                                                new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                            // Write from the source to the destination.
                            // Check if the data set is not null and tables count > 0 etc
                            bulkCopy.WriteToServer(table);
                        }
                        catch (Exception ex)
                        {
                            message = "Error : " + ex.Message;
                            location = "SubManifestCreationBatch.finish";

                            processID = db.ExecuteScalar<string>(
                                                "GenerateProcessId",
                                                new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                            throw new Exception(ex.Message);
                        }
                    }

                    string validateSubmit = db.SingleOrDefault<string>("ValidateSubManifestCreateBatch", new object[] { processID, username }); //db.Execute("moveSMCDataTemp", new object[] { processId, user });
                    var resValidate = validateSubmit.Split('|');

                    if (resValidate[0] == "SUCCESS")
                    {

                        // validate data in temporary uploaded table.
                        string proccessSubmit = db.SingleOrDefault<string>("moveSMCDataTemp", new object[] { processID, username, companyID }); //db.Execute("moveSMCDataTemp", new object[] { processId, user });
                        var ItemPar = proccessSubmit.Split('|');
                        if (ItemPar[0] == "SUCCESS")
                        {
                            message = proccessSubmit;
                            msgSP = "Sub Manifest Creation process is finished sucessfully";
                            sts = "0";
                            delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                        }
                        else
                        {
                            message = "ERROR|" + ItemPar[1];
                            msgSP = ItemPar[1];
                            sts = "6";
                            delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                        }
                    }
                    else {
                        message = "ERROR|" + resValidate[1];
                        msgSP = resValidate[1];
                        sts = "6";
                        delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });                    
                    }
                    location = "SubManifestCreationUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                        msgSP,     // what
                                        username,     // user
                                        location,     // where
                                        processID,     // pid OUTPUT
                                        "MPCS00004INF",     // id
                                        "",     // type
                                        module,     // module
                                        function,     // function
                                        sts      // sts
                                    });


                }
                catch (Exception ex)
                {
                    message = "ERROR|" + ex.Message;

                    location = "SubManifestCreationBatch.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

                    delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                }


            }

            return Content(message);
        }
        #endregion 


        #region edit
        //manifest|part no|item no|remain qty|del qty
        //P118004011|759240K13000|2|1|1;P118004011|759560K02000|3|1|1;
        public ActionResult EditData(String iListKeySubmit, String ETD, String Ref1, String Ref2) //String iListQty, 
        {
            IDBContext db = dbcontext();
            string function = "81002";
            string module = "8";
            string location = "";
            string message = "";
            string processID = "";
            string msgSP = "";
            string username = AuthorizedUser.Username;

            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreationSPEX", "gridSupplier");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            string companyID = SupplierCodeLDAP; //AuthorizedUser.CompanyID;
            string lockingBy = companyID;
            if (lockingBy == null || lockingBy == "")
                lockingBy = username;

            string sts = "0";
            int iIndex = 0;
            List<SubManifestCreationSPEX> listDataModel = new List<SubManifestCreationSPEX>();
            SubManifestCreationSPEX dataModel = new SubManifestCreationSPEX();
            var listKeyHeader = iListKeySubmit.Split(';');
            //string[] listQty = iListQty.Split(';');
            string tableFromTemp = "TB_T_SPEX_UPDATE_SUB_MANIFEST";
            int delLock = 0;

            long lockPid = db.SingleOrDefault<long>("GetLockByReff", new object[] { function, lockingBy });

            if (lockPid != 0)
            {
                message = "ERROR|Lock";
                //TempData["IsValid"] = "lock;" + processID + ";" + "Process is being locked by another user.";
            }
            else
            {
                message = "Starting Sub Manifest Update Batch";
                location = "SubManifestCreationBatch.init";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                string lockReff = companyID + "_" + processID;

                int getLock = db.SingleOrDefault<int>("InsertLock", new object[] { function, lockReff, processID });

                try
                {
                    message = "Clear TB_T_SPEX_UPDATE_SUB_MANIFEST";
                    location = "SubManifestCreationBatch.process";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                    // clear temporary upload table.
                    db.ExecuteScalar<String>("ClearSubManifestTempUpload", new object[] { tableFromTemp, username });

                    #region Create Data Table
                    DataTable table = new DataTable();

                    table.Columns.Add("DATA_ID");
                    table.Columns.Add("PROCESS_ID");
                    table.Columns.Add("MANIFEST_NO");
                    table.Columns.Add("SUB_MANIFEST_NO");
                    table.Columns.Add("PART_NO");
                    table.Columns.Add("ITEM_NO");
                    table.Columns.Add("QTY_DELIVERY");
                    table.Columns.Add("ETD");
                    table.Columns.Add("USER_ID");
                    #endregion

                    foreach (var lsKeyHeader in listKeyHeader)
                    {
                        if (lsKeyHeader == "")
                        {
                            continue;
                        }
                        else
                        {
                            var listKeyDetail = lsKeyHeader.Split('|');
                            DataRow row = table.NewRow();
                            row["DATA_ID"] = "D";
                            row["PROCESS_ID"] = processID;
                            row["MANIFEST_NO"] = listKeyDetail[0].ToString();
                            row["SUB_MANIFEST_NO"] = listKeyDetail[5].ToString();
                            row["PART_NO"] = listKeyDetail[1].ToString();
                            row["ITEM_NO"] = listKeyDetail[2].ToString();
                            row["QTY_DELIVERY"] = listKeyDetail[4].ToString(); //listQty[iIndex].ToString();
                            row["ETD"] = ETD;
                            row["USER_ID"] = username;

                            table.Rows.Add(row);
                        }
                        iIndex++;
                    }

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                    {
                        //try
                        //{
                        bulkCopy.DestinationTableName = tableFromTemp;
                        bulkCopy.ColumnMappings.Clear();

                        bulkCopy.ColumnMappings.Add("DATA_ID", "DATA_ID");
                        bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                        bulkCopy.ColumnMappings.Add("MANIFEST_NO", "MANIFEST_NO");
                        bulkCopy.ColumnMappings.Add("SUB_MANIFEST_NO", "SUB_MANIFEST_NO");
                        bulkCopy.ColumnMappings.Add("PART_NO", "PART_NO");
                        bulkCopy.ColumnMappings.Add("ITEM_NO", "ITEM_NO");
                        bulkCopy.ColumnMappings.Add("QTY_DELIVERY", "QTY_DELIVERY");
                        bulkCopy.ColumnMappings.Add("ETD", "ETD");
                        bulkCopy.ColumnMappings.Add("USER_ID", "USER_ID");

                        try
                        {
                            message = "Insert Data to TB_T_SPEX_UPDATE_SUB_MANIFEST";
                            location = "SubManifestCreationBatch.process";

                            processID = db.ExecuteScalar<string>(
                                                "GenerateProcessId",
                                                new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                            // Write from the source to the destination.
                            // Check if the data set is not null and tables count > 0 etc
                            bulkCopy.WriteToServer(table);
                        }
                        catch (Exception ex)
                        {
                            message = "Error : " + ex.Message;
                            location = "SubManifestCreationBatch.finish";

                            processID = db.ExecuteScalar<string>(
                                                "GenerateProcessId",
                                                new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                            throw new Exception(ex.Message);
                        }
                    }

                    string validateSubmit = db.SingleOrDefault<string>("ValidateSubManifestUpdateBatch", new object[] { processID, username }); //db.Execute("moveSMCDataTemp", new object[] { processId, user });
                    var resValidate = validateSubmit.Split('|');

                    if (resValidate[0] == "SUCCESS") {
                        // validate data in temporary uploaded table.
                        string proccessSubmit = db.SingleOrDefault<string>("SubManifestUpdateBatch", new object[] { processID, username }); //db.Execute("moveSMCDataTemp", new object[] { processId, user });
                        var ItemPar = proccessSubmit.Split('|');
                        if (ItemPar[0] == "SUCCESS")
                        {
                            message = proccessSubmit;
                            msgSP = "Sub Manifest Creation process is finished sucessfully";
                            sts = "0";
                            delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                        }
                        else
                        {
                            message = "ERROR|" + ItemPar[1];
                            msgSP = ItemPar[1];
                            sts = "6";
                            delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                        }


                    }
                    else {
                        message = "ERROR|" + resValidate[1];
                        msgSP = resValidate[1];
                        sts = "6";
                        delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                    }


                    location = "SubManifestCreationUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                        msgSP,     // what
                                        username,     // user
                                        location,     // where
                                        processID,     // pid OUTPUT
                                        "MPCS00004INF",     // id
                                        "",     // type
                                        module,     // module
                                        function,     // function
                                        sts      // sts
                                    });


                }
                catch (Exception ex)
                {
                    message = "ERROR|" + ex.Message;

                    location = "SubManifestCreationBatch.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

                    delLock = db.SingleOrDefault<int>("DeleteLock", new object[] { function, lockReff });
                }


            }

            return Content(message);
        }
        #endregion 


        #region Download excell
        public void DownloadList(String supplierCode, String manifestNo, string SubSupplierCode, string subManifest)
        {
            string fileName = "SubManifestCreation.xls";

            IDBContext db = dbcontext();
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;
              
            string SupplierCodeLDAP = "";
            string SubSupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SubManifestCreation", "SMCHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            List<SubManifestCreationSPEXDownload> SubManifestCreationModel = db.Fetch<SubManifestCreationSPEXDownload>("GetAllSubManifestCreationDownload", new object[] {
                supplierCode == null ? "" : supplierCode,
                manifestNo == null ? "" : manifestNo,
                SubSupplierCode ,
                SupplierCodeLDAP,
                subManifest
                //SubSupplierCodeLDAP
            });

            hasil = exporter.Write(SubManifestCreationModel, "SubManifestCreationSPEX");
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            fileName = "SubManifestCreation_" + date + ".xls";

            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();


        }


        #endregion

        #region Redirect from Sub Manifest Inquiry

        public static string REDIRECT_PAGE = "REDIRECT";
        public static string MANIFEST_NO = "MANIFEST_NO";

        public ActionResult Redirect(string manifestNo,string subManifestNo)
        { 
            Session["MANIFEST_NO"] = manifestNo;
            Session["SUB_MANIFEST_NO"] = subManifestNo;
            Session["MODE"] = "EDIT";             
            Session["REDIRECT_PAGE"] = REDIRECT_PAGE;
            Session.Timeout = 1;
            return RedirectToAction("Index","SubManifestCreationSPEX");
        }
        #endregion

        #region Validate Supplier
        public ActionResult ValidateSupplier(string suppParam, string subSuppParam)
        {
            IDBContext db = dbcontext();
            string result = db.SingleOrDefault<string>("ValidateSupplier", new object[] { suppParam, subSuppParam });

            return Content(result);
        }
        #endregion

    }
}
