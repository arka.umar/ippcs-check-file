﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using DevExpress.Web.Mvc;
using Portal.Models.SupplierMapping;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class SupplierMappingController : BaseController
    {
        //
        // GET: /SupplierMapping/
        public SupplierMappingController()
            : base("LPOP Supplier Mapping : ")
        {
        }

        public PartialViewResult PartialSupplierCode()
        {
            List<Supplier> lst = new List<Supplier>();
            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lst = DbContext.Fetch<Supplier>("GetAllSupplier");
            DbContext.Close();

            ViewData["SupplierData"] = lst;
            return PartialView("PartialSupplierCode", ViewData["SupplierData"]);
        }

        public PartialViewResult PartialSupplierCodeHeader()
        {
            List<Supplier> lst = new List<Supplier>();
            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lst = DbContext.Fetch<Supplier>("GetAllSupplier");
            DbContext.Close();

            ViewData["SupplierData"] = lst;
            return PartialView("PartialSupplierCodeHeader", ViewData["SupplierData"]);
        }

        public PartialViewResult PartialDockCode()
        {
            List<Dock> lst = new List<Dock>();
            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lst = DbContext.Fetch<Dock>("GetAllDock");
            DbContext.Close();

            ViewData["DockData"] = lst;
            return PartialView("PartialDockCode", ViewData["DockData"]);
        }

        public PartialViewResult PartialDockCodeHeader()
        {
            List<Dock> lst = new List<Dock>();
            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lst = DbContext.Fetch<Dock>("GetAllDock");
            DbContext.Close();

            ViewData["DockData"] = lst;
            return PartialView("PartialDockCodeHeader", ViewData["DockData"]);
        }

        public PartialViewResult PartialSupplierMappingData()
        {
            SupplierMappingModel mdl = new SupplierMappingModel();

            string plantCode = Request.Params["RPlantCode"] == "" ? null : Request.Params["RPlantCode"];
            string dockCode = Request.Params["HDockCode"] == "" ? null : Request.Params["HDockCode"];
            string supplierCode = Request.Params["HSupplierCode"] == "" ? null : Request.Params["HSupplierCode"];
            //Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]),
            //    Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]),
            DateTime from = DateTime.ParseExact(Request.Params["HValidFrom"], "dd.MM.yyyy", null);
            DateTime to = DateTime.ParseExact(Request.Params["HValidTo"], "dd.MM.yyyy", null);

            mdl.SupplierMappings = GetSupplierMapping(plantCode, dockCode, supplierCode, from, to);

            Model.AddModel(mdl);
            return PartialView("PartialSupplierMappingData", Model);
        }

        public PartialViewResult PartialPlantCode()
        {
            List<Plant> lst = new List<Plant>();
            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lst = DbContext.Fetch<Plant>("GetAllPlant");
            DbContext.Close();

            ViewData["PlantData"] = lst;
            return PartialView("PartialPlantCode", ViewData["PlantData"]);
        }

        public PartialViewResult PartialPlantCodeHeader()
        {
            List<Plant> lst = new List<Plant>();
            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lst = DbContext.Fetch<Plant>("GetAllPlant");
            DbContext.Close();

            ViewData["PlantData"] = lst;
            return PartialView("PartialPlantCodeHeader", ViewData["PlantData"]);
        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            List<Dock> lstDock = new List<Dock>();
            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lstDock = DbContext.Fetch<Dock>("GetAllDock");

            ViewData["DockData"] = lstDock;

            List<Plant> lstPlant = new List<Plant>();
            lstPlant = DbContext.Fetch<Plant>("GetAllPlant");

            ViewData["PlantData"] = lstPlant;

            List<Supplier> lstSupplier = new List<Supplier>();
            lstSupplier = DbContext.Fetch<Supplier>("GetAllSupplier");
            

            ViewData["SupplierData"] = lstSupplier;

            DbContext.Close();

            SupplierMappingModel modelSuppInfo = new SupplierMappingModel();
            DateTime from = DateTime.Now.AddDays(-1);
            DateTime to = DateTime.Now;

            //mdl.SupplierMappings = GetSupplierMapping(plantCode, plantCode, supplierCode, from, to);
            modelSuppInfo.SupplierMappings = GetSupplierMapping("", "", "", from, to);
            Model.AddModel(modelSuppInfo);

        }

        #region CRUD
        public ActionResult OnUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] SupplierMappingData supplierMapping)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    db.Execute("SMUpdate", new object[] {               
                        supplierMapping.PlantCode, 
                        supplierMapping.DockCode,
                        supplierMapping.SupplierCode,
                        supplierMapping.SupplierName,
                        supplierMapping.ValidFrom,
                        supplierMapping.ValidTo,
                        supplierMapping.ExcludeFlag,
                        AuthorizedUser.Username
                    });
                    db.Close();
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = ex.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";
                ViewData["SupplierTable"] = supplierMapping;
            }

            SupplierMappingModel modelSupplierMapping = new SupplierMappingModel();
            string plantCode = Request.Params["RPlantCode"] == "" ? null : Request.Params["RPlantCode"];
            string dockCode = Request.Params["HDockCode"] == "" ? null : Request.Params["HDockCode"];
            string supplierCode = Request.Params["HSupplierCode"] == "" ? null : Request.Params["HSupplierCode"];
            //Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]),
            //    Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]),
            DateTime from = DateTime.ParseExact(Request.Params["HValidFrom"], "dd.MM.yyyy", null);
            DateTime to = DateTime.ParseExact(Request.Params["HValidTo"], "dd.MM.yyyy", null);

            modelSupplierMapping.SupplierMappings = GetSupplierMapping(plantCode, dockCode, supplierCode, from, to);
            //modelSupplierMapping.SupplierMappings = getSuppInfo("", "");
            Model.AddModel(modelSupplierMapping);

            return PartialView("PartialSupplierMappingData", Model);
        }

        public ActionResult OnInsert([ModelBinder(typeof(DevExpressEditorsBinder))] SupplierMappingData supplierMapping)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    db.Execute("SMInsert", new object[] {               
                        supplierMapping.PlantCode, 
                        supplierMapping.DockCode,
                        supplierMapping.SupplierCode,
                        supplierMapping.SupplierName,
                        supplierMapping.ValidFrom,
                        supplierMapping.ValidTo,
                        supplierMapping.ExcludeFlag,
                        AuthorizedUser.Username
                    });
                    db.Close();
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = ex.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";
                ViewData["SupplierTable"] = supplierMapping;
            }

            SupplierMappingModel modelSupplierMapping = new SupplierMappingModel();
            string plantcode = Request.Params["RPlantCode"] == "" ? null : Request.Params["RPlantCode"];
            string dockcode = Request.Params["HDockCode"] == "" ? null : Request.Params["HDockCode"];
            string suppliercode = Request.Params["HSupplierCode"] == "" ? null : Request.Params["HSupplierCode"];

            DateTime from = DateTime.ParseExact(Request.Params["HValidFrom"], "dd.MM.yyyy", null);
            DateTime to = DateTime.ParseExact(Request.Params["HValidTo"], "dd.MM.yyyy", null);

            modelSupplierMapping.SupplierMappings = GetSupplierMapping(plantcode, dockcode, suppliercode, from, to);
            Model.AddModel(modelSupplierMapping); 

            return PartialView("PartialSupplierMappingData", Model);
        }

        public ActionResult OnDelete([ModelBinder(typeof(DevExpressEditorsBinder))] SupplierMappingData supplierMapping)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User user = SessionState.GetAuthorizedUser();
                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    db.Execute("SMDelete", new string[] {               
                        supplierMapping.PlantCode, 
                        supplierMapping.DockCode
                    });
                    db.Close();
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = ex.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";               
            }

            SupplierMappingModel modelSupplierMapping = new SupplierMappingModel();
            string plantCode = Request.Params["RPlantCode"] == "" ? null : Request.Params["RPlantCode"];
            string dockCode = Request.Params["HDockCode"] == "" ? null : Request.Params["HDockCode"];
            string supplierCode = Request.Params["HSupplierCode"] == "" ? null : Request.Params["HSupplierCode"];

            DateTime from = DateTime.ParseExact(Request.Params["HValidFrom"], "dd.MM.yyyy", null);
            DateTime to = DateTime.ParseExact(Request.Params["HValidTo"], "dd.MM.yyyy", null);

            modelSupplierMapping.SupplierMappings = GetSupplierMapping(plantCode, dockCode, supplierCode, from, to);
            Model.AddModel(modelSupplierMapping);

            return PartialView("PartialSupplierMappingData", Model);
        }

        public ActionResult OnBulkDelete(string gridKeyField)
        {
            //"HSupplierCode" "SupplierPlantCd" "PartNo" "HDockCode").ToString();
            //string[] separRow = new string[] { "," };
            //string[] separField = new string[] { ";" };
            //string[] rowData = gridKeyField.Split(separRow, StringSplitOptions.RemoveEmptyEntries);
            //string[] fieldData;
            //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, ts))
            //{
            //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            //    for (int i = 0; i < rowData.Count(); i++)
            //    {
            //        fieldData = rowData[i].Split(separField, StringSplitOptions.RemoveEmptyEntries);

            //        db.Execute("DeleteSupplierInfo", new object[] {               
            //            fieldData[0], 
            //            fieldData[1],
            //            fieldData[2],
            //            fieldData[3]
            //        });
            //    }
            //    db.Close();
            //    scope.Complete();
            //}
            //SupplierInfoModel modelSuppInfo = new SupplierInfoModel();
            //modelSuppInfo.SupplierDatas = getSuppInfo("", "");
            //Model.AddModel(GetAllSupplier());
            //Model.AddModel(modelSuppInfo); ;

            return PartialView("PartialSupplierMappingData", Model);
        }
        #endregion

        public List<SupplierMappingData> GetSupplierMapping(string plantCode, string dockCode, string supplierCode, DateTime from, DateTime to)
        {
            List<SupplierMappingData> lst = new List<SupplierMappingData>();

            IDBContext DbContext = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            lst = DbContext.Fetch<SupplierMappingData>("SMGetAll", new object[] 
                    { 
                        plantCode, dockCode, supplierCode, from, to 
                    });
            DbContext.Close();

            return lst;
        }
    }
}
