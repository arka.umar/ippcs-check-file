﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Portal.Models;
using Portal.Models.Globals;
using Portal.Models.PriceConfirmation;
using Portal.Models.Supplier;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;
namespace Portal.Controllers
{
    public class PCDetailController : BaseController
    {
        private readonly string ModuleId = "2";
        private readonly string FunctionId = "25126";
        private readonly string ScreenId = "PCDetail";
        readonly string EffectiveDtFmt = "MM.yyyy";
        readonly string df104 = "dd.MM.yyyy"; 

        private IDBContext _db = null;
        private IDBContext DB
        {
            get
            {
                if (_db == null)
                {
                    _db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                }
                return _db;
            }
        }

        private string Usrnm
        {
            get
            {
                var u = Model.GetModel<User>();
                if (u != null)
                    return u.Username;
                else
                    return ScreenId; 
            }
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (_db != null)
            {
                _db.Close();
                _db = null;
            }
            base.OnResultExecuted(filterContext);
        }

        public PCDetailController()
            : base("Price Confirmation Detail")
        {
        }
        public PriceConfirmationModel model
        {
            get
            {
                var m = Model.GetModel<PriceConfirmationModel>();
                if (m == null)
                {
                    m = new PriceConfirmationModel();
                    Model.AddModel(m);
                }
                return m;
            }

            set
            {
                var p = Model.GetModel<PriceConfirmationModel>();
                if (p != null)
                    Model.RemoveModel<PriceConfirmationModel>();
                Model.AddModel(value);
            }
        }

        protected override void StartUp()
        {
            PriceConfirmationModel m = new PriceConfirmationModel(); 
            string pcno = Request.Params["PCNo"];
            string mode = Request.Params["Mode"];

            TempData["PCNo"] = pcno;
            TempData["mode"] = mode; 
            m.Mode = mode;
            m.Detail = Head(pcno);
            if (m.Detail == null) m.Detail = new PriceConfirmation() { PCNo = pcno, EffectiveDate = DateTime.Now };
            m.Material = Get(new PriceConfirmationParam() {  
                PCNo= pcno, 
                SupplierCode = m.Detail.SupplierCode, 
                EffectiveDate = m.Detail.EffectiveDate.ToString(EffectiveDtFmt) 
            });

            model = m; 
        }

        public string GetHeader(string PCNo, string SupplierCode, string EffectiveDate)
        {
            string r = "";
            PriceConfirmation p = null;
            try
            {
                p = Head(PCNo, SupplierCode, EffectiveDate);
            }
            catch (Exception ex)
            {
                r = ex.Message;
                ex.PutLog("Err Get PC Data", User.Identity.Name, "PCDetail.GetHeader");
            }
            if (p != null)
                r = (new { p.PCNo, EffectiveDate= p.EffectiveDate.ToString(EffectiveDtFmt), PCDate = p.PCDate.ToString(df104) 
                    , p.SupplierCode
                    , SupplierName = p.SupplierName ?? ""
                    , SupplierAddress=p.SupplierAddress ??""
                    , SupplierAttention=p.SupplierAttention ?? ""
                }).AsJson();
            return r;
        }

        List<SupplierName> _supplierModel = null;
        private List<SupplierName> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierName>();
                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }
        [Obsolete]
        protected override void Init()
        {
        }

        public string Messages(string msgid)
        {
            return Sing.Me.Msg.Delimited(msgid);
        }

        private List<SupplierICS> GetAllSupplierICSBuyerMapped()
        {
            User u = Model.GetModel<User>();
            string NOREG = u.NoReg;
            string SUPP_CD = null;
            if (u != null && u.Username != null && u.Username.IndexOf('.') > 0)
            {
                string[] ur = u.Username.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (ur.Length > 0)
                    SUPP_CD = ur[0];
            }
            List<SupplierICS> supplierModel = new List<SupplierICS>();
            try
            {
                supplierModel = DB.Fetch<SupplierICS>("GetAllSupplierICSBuyerMapped", new object[] { new { NOREG, SUPP_CD } });
            }
            catch (Exception exc)
            {
                exc.PutLog("", u.Username, ScreenId + ".GetAllSupplier");
                throw;
            }

            return supplierModel;
        }

        public ActionResult PartialHeaderSupplierLookup()
        {
            return LookupPart<SupplierICS>(ScreenId,
                    "OIHSupplierOption",
                    "GridSupplier",
                    "SUPP_CD",
                    GetAllSupplierICSBuyerMapped);
        }
        
        public ActionResult PartialHeader()
        {
            string pcno = Convert.ToString(TempData["PCNo"]);
            
            model.Detail = Head(pcno);
            LookupPart<SupplierICS>(ScreenId, "OIHSupplierOption", "GridSupplier", "SUPP_CD", GetAllSupplierICSBuyerMapped, null);
            return PartialView("PartialHeader", Model);
        }


        public ActionResult PartialDetail()
        {
            var u = Model.GetModel<User>();

            PriceConfirmationParam prm = Request.Params.AsObject<PriceConfirmationParam>();
  
           
            prm = Request.Params.AsObject<PriceConfirmationParam>();
            model.Material = Get(prm);
            
            return PartialView("PartialDetail", Model);
        }

        public ActionResult ItemDetail()
        {
            var u = Model.GetModel<User>();
            PriceConfirmationParam prm = new PriceConfirmationParam();
            //{
            //    PCNo = model.Detail.PCNo, 
            //    SupplierCode = model.Detail.SupplierCode, 
            //    EffectiveDate = model.Detail.EffectiveDate.ToString(EffectiveDtFmt)  
            //};

            prm = Request.Params.AsObject<PriceConfirmationParam>();
            prm.Username = Usrnm;
            prm.PCNo = null; 
            // model.Detail = Head(prm.PCNo);
            model.Draft = GetItem(prm);
            return PartialView("ItemDetail", Model); 
        }

        public ActionResult PopupItem()
        {
            PriceConfirmationParam prm = new PriceConfirmationParam()
            {
                PCNo = model.Detail.PCNo,
                SupplierCode = model.Detail.SupplierCode,
                EffectiveDate = model.Detail.EffectiveDate.ToString(EffectiveDtFmt)
            };
            prm = Request.Params.AsObject<PriceConfirmationParam>();
            model.Detail = Head(prm.PCNo);
            PriceConfirmationParam pi = Request.Params.AsObject<PriceConfirmationParam>();
            pi.Username = Usrnm;
            pi.PCNo = null; 
            model.Draft = GetItem(pi);
            return PartialView("PopupItem", Model);
        }

        protected PriceConfirmation Head(string PCNo, string SupplierCode = "",  string EffectiveDate = "")
        {
            return DB.SingleOrDefault<PriceConfirmation>("GetPCDetailHead", new object[] { new { PCNo, SupplierCode,  EffectiveDate } });
        }

        protected List<PCDetail> Get(PriceConfirmationParam p)
        {
            List<PCDetail> listn = new List<PCDetail>();

            p.Validate(1);

            listn = DB.FetchStatement<PCDetail>("exec pcs_GetPriceConfirmationDetail @PCNo, @SupplierCode, @EffectiveDate, @BusinessArea", new object[] { p });
            return listn;
        }

        protected List<PCDetail> GetItem(PriceConfirmationParam p)
        {
            List<PCDetail> listn = new List<PCDetail>();
           
            //p.Validate(1);
            listn = DB.FetchStatement<PCDetail>("exec pcs_GetPriceConfirmationItem @PCNo, @SupplierCode, @EffectiveDate, @BusinessArea, @Username", new object[] { p });
            return listn;
        }

        public string PutDraftMaterial(string PCNo, string Items, string SupplierCode)
        {
            string ResultQuery = "SUCCESS";
            try
            {
                ResultQuery = DB.FetchStatement<string>("exec pcs_PutDraftMaterial  @Items,@PCNo, @SupplierCode, @Username"
                    , new object[] { new { Items, PCNo, SupplierCode, Username = Usrnm} })
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
                ex.PutLog(ResultQuery, Usrnm, "PutDraftMaterial");
            }
            return ResultQuery; 
        }

        public string Delete(string PCNo, string MatNo, string SourceType, string ProductionPurpose, string PackingType, string PartColorSuffix)
        {
            string ResultQuery = "Process succesfully";
            try
            {
                DB.Execute("DeleteMaterialPrice", new object[]
                {
                   new { PCNo, MatNo, SourceType, ProductionPurpose, PackingType, PartColorSuffix, Username=Usrnm }
                });
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
                ex.PutLog("", Usrnm, "Delete");
            }
            return ResultQuery;
        }
       
        public string Lock(string PCNo, int Act = 0)
        {
            string r = DB.ExecuteScalar<string>("SetLock", new object[] { new { act = Act, usr = Usrnm, key = PCNo, fn = FunctionId, desc = ScreenId } });
            return r; 
        }
        
    }
}
