﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Portal.Models.RouteCapacity;
using Portal.Models.CapacityPlanning;
using Toyota.Common.Reporting;
using Toyota.Common.Reporting.Services;
using System.Net.Mail;
using Toyota.Common.Web.Excel;
using System.Data;
using System.Threading.Tasks;

namespace Portal.Controllers
{
    public class CapacityPlanningViewController : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public CapacityPlanningViewController()
            : base("Load Capacity Monitoring")
        {
            PageLayout.UseMessageBoard = true;
        }
        protected override void StartUp()
        {
            ViewData["EnableButton"] = CheckButtonRecalculation();
        }
          

        List<CapacityPlanning> list;
        string ParamUnderload = "Underload";
        string ParamOK = "OK";
        string ParamOverload = "Overload";
        double underload, ok, overload;


        protected override void Init()
        {
            List<CapacityPlanning> LisCp=new List<CapacityPlanning>();
            ViewData["ListDetail"] = LisCp;
            list = GetCapacityPlanning(GetCondition("", "", DateTime.Now.AddDays(-8), DateTime.Now.AddDays(+8))).ToList();
            GetViewData(list);
            ViewData["ListRouteCapacity"] = list;
        }

        public ActionResult GridcapacityCallback(string value, string key, string datefrom, string dateto)
        { 
            if ((datefrom == null) || (dateto == null) || (datefrom == "") || (dateto == ""))
            {
                return PartialView("Capacityplanninggrid", GetCapacityPlanning("").ToList());
            }
            else
            {
                return PartialView("Capacityplanninggrid", GetCapacityPlanning(GetCondition(value, key, ConvertoDatetime(datefrom), ConvertoDatetime(dateto))).ToList());
            }
            
        }

        private DateTime ConvertoDatetime(string value)
        {
            DateTime result;
            string[] arrdate = value.Split('-');
            string NewDate;
            NewDate = arrdate[1] + '/' + arrdate[0] + '/' + arrdate[2];
            result = System.Convert.ToDateTime(NewDate);
            return result;
        }

        public ActionResult ChartcapacityCallback(string value, string key, string datefrom, string dateto)
        {
            if ((datefrom == null) || (dateto == null) || (datefrom == "") || (dateto == ""))
            {
                list = GetCapacityPlanning("").ToList();
            }
            else
            {
                list = GetCapacityPlanning(GetCondition(value, key, ConvertoDatetime(datefrom), ConvertoDatetime(dateto))).ToList();
            }
            
            underload = Countplanning(list, ParamUnderload);
            ok = Countplanning(list, ParamOK);
            overload = Countplanning(list, ParamOverload);
            ViewData["Underload"] = underload;
            ViewData["OK"] = ok;
            ViewData["Overload"] = overload;
            return PartialView("Planningchart", GetSummaryData(underload, ok, overload));
        }

        private string GetCondition(string value, string key, DateTime datefrom, DateTime dateto)
        {
            string where = " WHERE PICKUP_DT BETWEEN '" + datefrom + "' AND '" + dateto + "'";

            if ((key == "") || (key == null))
            {
                return where;
            }
            else
            {
                where = where + " AND ";
                string[] Arrkey = key.Split(';');
                string rplkey = "";
                foreach (string keyvalue in Arrkey)
                {
                    //didapat dari view di grid
                    if (keyvalue == "Delivery No")
                    {
                        rplkey = "DELIVERY_NO";
                    }
                    else if (keyvalue == "LP Code")
                    {
                        rplkey = "LP_CD";
                    }
                    else if (keyvalue == "Truck Type")
                    {
                        rplkey = "TRUCK_TYPE";
                    }
                    else if (keyvalue == "Route Code")
                    {
                        rplkey = "ROUTE_CD";
                    }
                    where = where + rplkey + " like '%" + value + "%' OR ";
                }
                where = where.Substring(0, (where.Length - 3));
                return where;
            }
        }

        private void GetViewData(List<CapacityPlanning> list)
        {
            underload = Countplanning(list, ParamUnderload);
            ok = Countplanning(list, ParamOK);
            overload = Countplanning(list, ParamOverload);
            ViewData["Underload"] = underload;
            ViewData["OK"] = ok;
            ViewData["Overload"] = overload;
            ViewData["ListSummary"] = GetSummaryData(underload, ok, overload);//untuk menampilkan chart
        }

        public ReportResult GenerateReport(string data, string reportPath, string namafile, string rpttype)
        {
            List<DataTableSource> dataSource = new List<DataTableSource>();
            dataSource.Add(new DataTableSource()
            {
                DataTable = ReportHelper.ConvertToDataTable<CapacityPlanning>(GetCapacityPlanning(data)),
                Name = namafile
            });

            ReportResult result;
            Toyota.Common.Reporting.Services.MicrosoftReport reportService = new MicrosoftReport(namafile, reportPath, dataSource, rpttype);
            result = reportService.Generate();
            return result;
        }

        private IEnumerable<CapacityPlanning> GetCapacityPlanning(string condition)
        {
            IEnumerable<CapacityPlanning> listdata;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            listdata = db.Query<CapacityPlanning>("CapacityPlanning", new object[] { condition });
            db.Close();
            return listdata;
        }

        private double Countplanning(List<CapacityPlanning> listcapacity,string Param)
        {
            return list.Count(item => item.Cstatus == Param);
        }

        private List<CapacityPlanning> GetSummaryData(double under, double ok, double over)
        {
            List<CapacityPlanning> Listdata = new List<CapacityPlanning>();
            CapacityPlanning obj = new CapacityPlanning();
            obj.status = "OK";
            obj.name = "Efficiency distribution";
            obj.value = ok;
            Listdata.Add(obj);

            CapacityPlanning obj2 = new CapacityPlanning();
            obj2.status = "Overload";
            obj2.name = "Efficiency distribution";
            obj2.value = over;
            Listdata.Add(obj2);

            CapacityPlanning obj1 = new CapacityPlanning();
            obj1.status = "UnderLoad";
            obj1.name = "Efficiency distribution";
            obj1.value = under;
            Listdata.Add(obj1);
            return Listdata;
        }

        public ActionResult GetDefaultdate()
        {
            string from1,from2, to1,to2;
            from1 = DateTime.Now.AddDays(-8).ToShortDateString();
            from2 = String.Format("{0:dd-MM-yyyy}", DateTime.Now.AddDays(-8));

            to1 = DateTime.Now.AddDays(+8).ToShortDateString();
            to2 = String.Format("{0:dd-MM-yyyy}", DateTime.Now.AddDays(+8));
            return Json(from1 + ';' + from2 + ';' + to1 + ';' + to2);
        }

        public ActionResult Sendmail()
        {
            string msg;
            try
            {
                string MailTo,MailFrom,PasswordMailFrom,MailSubject,MailBody = "";

                MailTo = "arkamaya.mulyawan@toyota.co.id";
                MailFrom = "arkamaya.septareo@toyota.co.id";
                PasswordMailFrom = "toyota123";
                MailSubject="Status Capacity";
                MailBody="Your capacity plant is overload";

                SmtpClient SmtpServer = new SmtpClient("mail-relay.toyota.co.id");
                SmtpServer.EnableSsl = false;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(MailFrom, PasswordMailFrom);
                SmtpServer.Send(MailFrom, MailTo, MailSubject, MailBody);
                msg = "Status capacity hass been send";
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DetailGrid(string DeliveryNo)
        {
            ViewData["Delivery"] = DeliveryNo;

            string valu = Request.Params["DeliveryNo"];
            return PartialView("DetailGrid", GetDataDetailCapacity("'" + DeliveryNo + "'"));
        }

        private IEnumerable<DetailCapacity> GetDataDetailCapacity(string delivery)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<DetailCapacity> result = db.Query<DetailCapacity>("CapacityPlanningDetail", new object[] { delivery });
            db.Close();
            return result;
        }

        private bool CheckButtonRecalculation()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            bool result = db.SingleOrDefault<bool>("CapacityPlanningCheckButton");
            return result;
        }

        public JsonResult Recalculation(string from, string to)//0 proccess--1 off
        {
            string message;
            if (CheckButtonRecalculation())
            {
                //Task<string> task = Task.Factory.StartNew<string>
                //(() => ProcessReCalculation());
                //string result = task.Result;
                message=ProcessReCalculation(ConvertoDatetime(from), ConvertoDatetime(to));
            }
            else
            {
                message = "On other processing";
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        private string ProcessReCalculation(DateTime from, DateTime to)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS); 
            db.Execute("CapacityPlanningReCalculationProcess", new object[] { from,to, });
            return "On processing ";
        }


        #region NewDownload
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadFile(string value, string key, string datefrom, string dateto)
        {
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;
            string fileName = String.Empty;
            List<CapacityPlanning> List = GetCapacityPlanning(GetCondition(value, key, ConvertoDatetime(datefrom), ConvertoDatetime(dateto))).ToList();
            fileName = "CapacityPlanningView.xls";
            hasil = exporter.Write(ConvertToDataTable(List), "Sheet1");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
            Response.BinaryWrite(hasil);
            Response.End();
        }

        public ActionResult CekChangeddownload(string data)
        { 
            List<CapacityPlanning> areas = Toyota.Common.Util.Text.JSON.ToObject<List<CapacityPlanning>>(data);
            return Json(areas.Count, JsonRequestBehavior.AllowGet);
        }

        public void OnDownloadDetail(string data)
        {
            string deliveryno = "'";
            List<DetailCapacity> areas = Toyota.Common.Util.Text.JSON.ToObject<List<DetailCapacity>>(data);
            foreach (DetailCapacity cp in areas)
            {
                deliveryno = deliveryno + cp.DeliveryNo + "','";
            }
            deliveryno = deliveryno.Substring(0, deliveryno.Length - 2);

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;
            string fileName = String.Empty;
            List<DetailCapacity> List = GetDataDetailCapacity(deliveryno).ToList();
            fileName = "DetailCapacityPlanningView.xls";
            hasil = exporter.Write(ConvertToDataTable(List), "Sheet1");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
            Response.BinaryWrite(hasil);
            Response.End(); 
        }

        #endregion NewDownload
    }
}

//public void DownloadFile(string value, string key, string datefrom, string dateto)
//{
//    string filename = "CapacityPlanning";
//    ReportResult result = null;
//    try
//    {
//        if ((datefrom == null) || (dateto == null) || (datefrom == "") || (dateto == ""))
//        {
//            result = GenerateReport("", Server.MapPath("~/ReportingServices/" + filename + ".rdlc"), filename, MicrosoftReportType.Excel.ToString());
//        }
//        else
//        {
//            result = GenerateReport(GetCondition(value, key, ConvertoDatetime(datefrom), ConvertoDatetime(dateto)), Server.MapPath("~/ReportingServices/" + filename + ".rdlc"), filename, MicrosoftReportType.Excel.ToString());
//        }


//        Response.Clear();
//        Response.Cache.SetCacheability(HttpCacheability.Private);
//        Response.Expires = -1;
//        Response.Buffer = true;
//        Response.ContentType = "application/octet-stream";
//        Response.AddHeader("Content-Length", Convert.ToString(result.DocumentBytes.Length));
//        Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", result.Name));
//        Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
//        Response.BinaryWrite(result.DocumentBytes);
//        Response.End();
//    }
//    catch (Exception e)
//    {
//        string erore = e.Message;
//    }
//}
