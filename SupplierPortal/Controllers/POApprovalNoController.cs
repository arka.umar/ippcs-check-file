﻿using Toyota.Common.Web.MVC;
using System.Collections.Generic;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.PO;
using System;
using System.Linq;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using System.Web.Routing;
using System.Net;
using System.Web;

namespace Portal.Controllers
{
    public class POApprovalNoController : BaseController
    {
        RouteCollection routes = new RouteCollection();
        string purchase;
        public POApprovalNoController()
            : base("PO Approval By Number")
        {
        }



        protected override void StartUp()
        {
            string Po = Request.QueryString["Po"];
            POApprovalModel model = new POApprovalModel();
            model.PurchasingOrderParts = ListPurchasingOrderPart(Po);
            Model.AddModel(model);
        }

        
        protected override void Init()
        {
            
            //RouteCollection routes = new RouteCollection();
            //routes.MapRoute("Default", "{Controller}/{Action}/{Id}", new )
            //string c = purchase;
            //if(HttpContext.Session ["PO"]==null)
            //{
                
            //}
            //TempData["GridName"] = "grlLPHeader";
            //var a = TempData["PO"];
            //string PO = Request.QueryString["POApprovalNo"];
            

            //string test = System.Web.HttpContext.Current.Session["PO"].ToString();
            POApprovalModel model = Model.GetModel<POApprovalModel>();
            //POApprovalModel model = new POApprovalModel();
         
            //string a = Request.QueryString["Po"];
            //string url = HttpContext.Request.Url.AbsolutePath;
            /*for (int i = 0; i < 21; i++)
            {
                PurchasingOrder po = new PurchasingOrder()
                {
                    No = i + 1,
                    MaterialNo = "521590K270" + (i + 1),
                    MaterialDesc = "Cover Bumper, RR",
                    ColorSuffix = "E0",
                    PackingType = "0",
                    Qty = "10",
                    ReqDate = String.Format("{0:dd.MM.yyyy}", DateTime.Now),
                    Price = "300,000,000",
                    Vendor = "Nama Vendor",
                    ViewDetail = "View detail"
                };
                //po.Amount = po.Qty * po.Price;
                //po.Amount = Convert.ToString("po.Qty * po.Price");
                po.Amount = "3000,000,000";
                model.PurchasingOrders.Add(po);
            }*/
           
            
            //var ss = RouteData.Values["id"];
            
            //routes.MapRoute("Default", "{controller}/{action}/{Po}", new { controller = "PoApprovalNo", action = "Index", Po = UrlParameter.Optional });
            //ViewData["PO"] = Po;
            //if (RouteData.Values["Po"] == null)
            //{
            //}
            //var ss = RouteData.Values["Po"];
            //var queryValues = Request.RequestUri.ParseQueryString();
            //routes.MapRoute("Default", "{controller}/{action}/{Po}", new { controller = "PoApprovalNo", action = "Index", Po = Po });


            //POApprovalModel model = new POApprovalModel();
            //Model.AddModel(model);
        }
        
       /* public ActionResult PartialIndex(string Po)
        {
            purchase = Po;
            POApprovalModel mdl = new POApprovalModel();
            mdl.PurchasingOrderParts = ListPurchasingOrderPart(Po);
            Model.AddModel(mdl);


            
            RouteCollection routes = new RouteCollection();
            routes.MapRoute("Default", "{controller}/{action}/{Po}", new { controller = "PoApprovalNo", action = "POApprovalNoGrid", Po = UrlParameter.Optional });
            
            return RedirectToAction("index");
        }*/

        public List<PurchasingOrderPart> ListPurchasingOrderPart(string Po)
        {

            List<PurchasingOrderPart> getListPart = new List<PurchasingOrderPart>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<PurchasingOrderPart>("GetApprovalPart", new object[] { Po });
            int count = 0;
            if (QueryLog.Any())
            {
                getListPart = QueryLog.ToList();
                //foreach (var q in QueryLog)
                //{
                //    count++;
                //    getListPart.Add(new PurchasingOrderPart
                //        {
                //            No = count,
                //            PartNo = q.PartNo,
                //            PartName = q.PartName,
                //            ColorSuffix = q.ColorSuffix,
                //            PackingType = q.PackingType,
                //            Qty = q.Qty,
                //            Price = q.Price,
                //            Amount = q.Amount
                //        });
                //}
            }
            db.Close();
            return getListPart;

        }
        public ActionResult PartialHeader()
        {
            return PartialView("HeaderPOApprovalNo", Model);
        }

        public ActionResult PartialFooter()
        {
            return PartialView("FooterApproval", Model);
        }
        public ActionResult POApprovalNoGrid()
        {
            POApprovalModel model = Model.GetModel<POApprovalModel>();
            Model.AddModel(model);
            return PartialView("POApprovalNoGrid", Model);
        }
        
        public ActionResult POApprovalNobuttonlain()
        {
            return PartialView("POApprovalNobuttonlain", Model);
        }
    }
}
