﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.PackingSpec;
using Toyota.Common.Web.Database;
using DevExpress.Web.Mvc;
using Toyota.Common.Reporting;
using Toyota.Common.Reporting.Services;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.ComponentModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Data;
using System.Web.UI;
using System.Data.OleDb;
using Toyota.Common.Web.Excel;

namespace Portal.Controllers 
{
    public class PackingSpecMasterController : BaseController
    {
        public PackingSpecMasterController()
            : base("Packing Spec Master")
        {
            PageLayout.UseMessageBoard = true;
        }

        string msg = "";
        #region ActionPage
        protected override void StartUp()
        {

        }
         
        protected override void Init()
        {
            List<PackingSpecData> mdl = new List<PackingSpecData>();
            ViewData["ListPackingSpec"] = GetDataPackingMaster(GetDataCondition("",""));
        }

        public ActionResult GridPAckingCallback( string value, string key)
        {
            return PartialView("PackingSpecGrid", GetDataPackingMaster(GetDataCondition(value,key)));
        }
                
        public ActionResult PackingSpecAdd([ModelBinder(typeof(DevExpressEditorsBinder))]PackingSpecData obj)
        {
            //if (ModelState.IsValid)
            //{
                try
                {
                    ExecuteInsertUpdatePackingMaster(
                obj.PackingSpecId, obj.SupplierCode, obj.SupplierPlant, obj.ShippingDock,
                obj.CompanyCode, obj.RcvPlantCode, obj.DockCode, obj.PartNo,
                obj.PalleteType, obj.PalleteLength, obj.PalleteWidth, obj.PalleteHeight, AuthorizedUser.Username, 'Y');
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            //}
            //else
            //{
            //    ViewData["EditError"] = "Please, correct all errors.";

                //}
                ModelState.Clear();
                return PartialView("PackingSpecGrid", GetDataPackingMaster(null));
        }

        public ActionResult PackingSpecUpdate([ModelBinder(typeof(DevExpressEditorsBinder))]PackingSpecData obj)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ExecuteInsertUpdatePackingMaster(
                obj.PackingSpecId, obj.SupplierCode, obj.SupplierPlant, obj.ShippingDock,
                obj.CompanyCode, obj.RcvPlantCode, obj.DockCode, obj.PartNo,
                obj.PalleteType, obj.PalleteLength, obj.PalleteWidth, obj.PalleteHeight, AuthorizedUser.Username, 'N');
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                ViewData["EditError"] = "Please, correct all errors.";

            }
            return PartialView("PackingSpecGrid", GetDataPackingMaster(null));
        }

        public ActionResult RemoveMaster(string data)
        {
            List<PackingSpecData> areas = Toyota.Common.Util.Text.JSON.ToObject<List<PackingSpecData>>(data);
            int ret = areas.Count();
            foreach (PackingSpecData pack in areas)
            {
                ret = DeletePackingMaster(pack.PackingSpecId);
            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        #endregion ActionPage

        #region DownloadPackingSpec

        //public void DownloadFile(string value, string key)
        //{
        //    var workbook = new HSSFWorkbook();
        //    var sheet = workbook.CreateSheet("Sheet1");

        //    // Siap siap buat header
        //    var rowIndex = 0;
        //    var row = sheet.CreateRow(rowIndex);

        //    sheet.AutoSizeColumn(0);
        //    sheet.AutoSizeColumn(1);
        //    sheet.AutoSizeColumn(2);
        //    sheet.AutoSizeColumn(3);
        //    sheet.AutoSizeColumn(4);
        //    sheet.AutoSizeColumn(5);
        //    sheet.AutoSizeColumn(6);
        //    sheet.AutoSizeColumn(7);
        //    sheet.AutoSizeColumn(8);
        //    sheet.AutoSizeColumn(9);
        //    sheet.AutoSizeColumn(10);

        //    var cell0 = sheet.CreateRow(0).CreateCell(0);
        //    var cell1 = sheet.CreateRow(0).CreateCell(1);
        //    var cell2 = sheet.CreateRow(0).CreateCell(2);
        //    var cell3 = sheet.CreateRow(0).CreateCell(3);
        //    var cell4 = sheet.CreateRow(0).CreateCell(4);
        //    var cell5 = sheet.CreateRow(0).CreateCell(5);
        //    var cell6 = sheet.CreateRow(0).CreateCell(6);
        //    var cell7 = sheet.CreateRow(0).CreateCell(7);
        //    var cell8 = sheet.CreateRow(0).CreateCell(8);
        //    var cell9 = sheet.CreateRow(0).CreateCell(9);
        //    var cell10 = sheet.CreateRow(0).CreateCell(10);

        //    // Set warna cell header background
        //    var warna = workbook.CreateCellStyle();
        //    warna.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
        //    warna.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
        //    warna.FillPattern = FillPatternType.SOLID_FOREGROUND;

        //    // Set font
        //    var font = workbook.CreateFont();
        //    font.FontHeightInPoints = 10;                                                   // Font size
        //    font.FontName = "Times New Roman";                                              // Font type
        //    font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;                 // Font weight

        //    var cell = row.CreateCell(0);
        //    cell.CellStyle = workbook.CreateCellStyle();
        //    cell.CellStyle.SetFont(font);
                 

        //    // Freeze row header
        //    sheet.CreateFreezePane(0, 1, 0, 1);

        //    // Buat header
        //    cell0.CellStyle = warna;
        //    cell0.SetCellValue("Supplier Code");
        //    cell0.CellStyle.SetFont(font);

        //    cell1.CellStyle = warna;
        //    cell1.SetCellValue("Supplier Plant");

        //    cell2.CellStyle = warna;
        //    cell2.SetCellValue("Shipping Dock");

        //    cell3.CellStyle = warna;
        //    cell3.SetCellValue("Company Code");

        //    cell4.CellStyle = warna;
        //    cell4.SetCellValue("Rcv Plant Code");

        //    cell5.CellStyle = warna;
        //    cell5.SetCellValue("Dock Code");

        //    cell6.CellStyle = warna;
        //    cell6.SetCellValue("Part No");

        //    cell7.CellStyle = warna;
        //    cell7.SetCellValue("Pal Type");

        //    cell8.CellStyle = warna;
        //    cell8.SetCellValue("Pal Length");

        //    cell9.CellStyle = warna;
        //    cell9.SetCellValue("Pal Width");

        //    cell10.CellStyle = warna;
        //    cell10.SetCellValue("Pal Height");

        //    rowIndex++;

        //    List<PackingSpecData> List = GetDataPackingMaster(GetDataCondition(value, key)).ToList();
        //    // Buat row content
        //    foreach (PackingSpecData obj in List)
        //    {
        //        row = sheet.CreateRow(rowIndex);
        //        row.CreateCell(0, CellType.STRING).SetCellValue(obj.SupplierCode);
        //        row.CreateCell(1, CellType.STRING).SetCellValue(obj.SupplierPlant);
        //        row.CreateCell(2, CellType.STRING).SetCellValue(obj.ShippingDock);
        //        row.CreateCell(3, CellType.STRING).SetCellValue(obj.CompanyCode);
        //        row.CreateCell(4, CellType.STRING).SetCellValue(obj.RcvPlantCode);
        //        row.CreateCell(5, CellType.STRING).SetCellValue(obj.DockCode);
        //        row.CreateCell(6, CellType.STRING).SetCellValue(obj.PartNo);
        //        row.CreateCell(7, CellType.STRING).SetCellValue(obj.PalleteType);
        //        row.CreateCell(8, CellType.STRING).SetCellValue(obj.PalleteLength);
        //        row.CreateCell(9, CellType.STRING).SetCellValue(obj.PalleteWidth);
        //        row.CreateCell(10, CellType.STRING).SetCellValue(obj.PalleteHeight); 
        //        rowIndex++;
        //    }

        //    // Simpan Workbook di MemoryStream lalu panggil di Client
        //    using (var exportData = new MemoryStream())
        //    {
        //        workbook.Write(exportData);

        //        string saveAsFileName = string.Format("PackingSpec-{0:d}.xls", DateTime.Now).Replace("/", "-");

        //        Response.ClearContent();
        //        Response.Buffer = true;
        //        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
        //        Response.ContentType = "application/vnd.ms-excel";
        //        Response.BinaryWrite(exportData.GetBuffer());
        //        Response.Flush();
        //        Response.End();
        //    }
        //    //string filename = "PackingSpec";
        //    //try
        //    //{
        //    //    ReportResult result = GenerateReport(GetDataCondition(value, key), Server.MapPath("~/ReportingServices/" + filename + ".rdlc"), filename, MicrosoftReportType.Excel.ToString());
        //    //    Response.Clear();
        //    //    Response.Cache.SetCacheability(HttpCacheability.Private);
        //    //    Response.Expires = -1;
        //    //    Response.Buffer = true;
        //    //    Response.ContentType = "application/octet-stream";
        //    //    Response.AddHeader("Content-Length", Convert.ToString(result.DocumentBytes.Length));
        //    //    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", result.Name));
        //    //    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
        //    //    Response.BinaryWrite(result.DocumentBytes);
        //    //    Response.End();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    string erore = e.Message;
        //    //}
        //}

        //public ReportResult GenerateReport(string data, string reportPath, string namafile, string rpttype)
        //{
        //    List<DataTableSource> dataSource = new List<DataTableSource>();
        //    dataSource.Add(new DataTableSource()
        //    {
        //        DataTable = ReportHelper.ConvertToDataTable<PackingSpecData>(GetDataPackingMaster(data)),
        //        Name = namafile
        //    });

        //    ReportResult result;
        //    Toyota.Common.Reporting.Services.MicrosoftReport reportService = new MicrosoftReport(namafile, reportPath, dataSource, rpttype);
        //    result = reportService.Generate();
        //    return result;
        //}
        
        #endregion DownloadPackingSpec

        //#region UploadPackingSpec
         
        //public ActionResult CallbackUpload()
        //{
        //    UploadControlExtension.GetUploadedFiles("ucCallbacks", UploadControlHelper.ValidationSettings, ucCallbacks_FileUploadComplete);
        //    return null;
        //}

        //public class UploadControlHelper
        //{
        //    public static readonly ValidationSettings ValidationSettings = new ValidationSettings
        //    {
        //        AllowedFileExtensions = new string[] { ".xls" },
        //        MaxFileSize = 60751520
        //    };
        //}

        //public void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        //{
        //    UploadPackingSpec(new MemoryStream(e.UploadedFile.FileBytes));
        //    IUrlResolutionService urlResolver = sender as IUrlResolutionService;
        //    if (urlResolver != null)
        //        e.CallbackData = msg;
        //}

        //public void UploadPackingSpec(Stream filestream)
        //{   
        //    HSSFWorkbook hssfworkbook;
        //    List<PackingSpecData> list = new List<PackingSpecData>(); 
        //    hssfworkbook = new HSSFWorkbook(filestream);
        //    ISheet sheet = hssfworkbook.GetSheet("Sheet1");

        //    int lasrow = sheet.LastRowNum;

        //    for (int i =1; i <= lasrow; i++)// row dimulai dari 0
        //    {
        //        try
        //        {
        //            IRow row = sheet.GetRow(i);
        //            PackingSpecData obj = new PackingSpecData();
        //            obj.SupplierCode = GetValueCell(row.GetCell(0));
        //            obj.SupplierPlant = GetValueCell(row.GetCell(1));
        //            obj.ShippingDock = GetValueCell(row.GetCell(2));
        //            obj.CompanyCode = GetValueCell(row.GetCell(3));
        //            obj.RcvPlantCode = GetValueCell(row.GetCell(4));
        //            obj.DockCode = GetValueCell(row.GetCell(5));
        //            obj.PartNo = GetValueCell(row.GetCell(6));
        //            obj.PalleteType = GetValueCell(row.GetCell(7));
        //            obj.PalleteLength = Convert.ToInt32(GetValueCell(row.GetCell(8)));
        //            obj.PalleteWidth = Convert.ToInt32(GetValueCell(row.GetCell(9)));
        //            obj.PalleteHeight = Convert.ToInt32(GetValueCell(row.GetCell(10))); 
        //            list.Add(obj);
        //        }
        //        catch (Exception e)
        //        {
        //            msg = msg + "\nRow ("+ (i+1) + ") error : " + e.Message;
        //        }
        //    }
        //    if (msg == "")
        //        ProsesUploadToDatabase(list);
        //}

        //private string GetFileExtension(string sFileName)
        //{
        //    sFileName = sFileName.Trim();
        //    if (String.IsNullOrEmpty(sFileName))
        //    {
        //        return String.Empty;
        //    }
        //    string sExtension = String.Empty;
        //    char[] cArr = sFileName.ToCharArray();
        //    int iIndex = 0;
        //    for (int i = cArr.Length - 1; i > -1; i--)
        //    {
        //        if (cArr[i].Equals('.'))
        //        {
        //            iIndex = i;
        //            break;
        //        }
        //    }
        //    if (iIndex > 0)
        //    {
        //        for (int i = iIndex + 1; i < cArr.Length; i++)
        //        {
        //            sExtension += cArr[i];
        //        }
        //    }
        //    return sExtension.ToLower();

        //}

        //private string GetValueCell(ICell cell)
        //{
        //    if (cell.CellType.ToString() == "NUMERIC")
        //    {
        //        return Convert.ToString(cell.NumericCellValue);
        //    }
        //    else if (cell.CellType.ToString() == "STRING")
        //    {
        //        return cell.StringCellValue;
        //    }
        //    else {
        //        return Convert.ToString(cell.DateCellValue);
        //    }
        //}

        //private void ProsesUploadToDatabase(List<PackingSpecData> ObjList)
        //{
        //    foreach (PackingSpecData obj in ObjList)
        //    {
        //        UploadPackingMaster(
        //        obj.PackingSpecId, obj.SupplierCode, obj.SupplierPlant, obj.ShippingDock,
        //        obj.CompanyCode, obj.RcvPlantCode, obj.DockCode, obj.PartNo,
        //        obj.PalleteType, obj.PalleteLength, obj.PalleteWidth, obj.PalleteHeight, AuthorizedUser.Username);
        //    }
        //    msg = "Upload data success";
        //}

        //#endregion UploadFile

        #region CRUD
        private string GetDataCondition(string value, string key)
        {
            if ((key == "") || (key == null))
            {
                return "";
            }
            string[] Arrkey = key.Split(';');
            string where = " WHERE ";
            string rplkey = "";
            foreach (string keyvalue in Arrkey)
            {
                //didapat dari view di grid
                if (keyvalue == "Supplier Code")
                {
                    rplkey = "SUPPLIER_CD";
                }
                else if (keyvalue == "Supplier Plant")
                {
                    rplkey = "SUPPLIER_PLANT";
                }
                else if (keyvalue == "Shipping Dock")
                {
                    rplkey = "SHIPPING_DOCK";
                }
                else if (keyvalue == "Company")
                {
                    rplkey = "COMPANY_CD";
                }
                else if (keyvalue == "Rcv Plant")
                {
                    rplkey = "RCV_PLANT_CD";
                }

                else if (keyvalue == "Dock")
                {
                    rplkey = "DOCK_CD";
                }

                else if (keyvalue == "Part No")
                {
                    rplkey = "PART_NO";
                }
                where = where + rplkey + " like '%" + value + "%' OR ";
            }
            where = where.Substring(0, (where.Length - 3));
            return where;
        }

        public PackingSpecData GetSingleDataPackingMaster(int packingspecid)
        {
            PackingSpecData data;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            data = db.SingleOrDefault<PackingSpecData>("PackingMasterSingleGet", new object[] { packingspecid });
            db.Close();
            return data;

        }

        public IEnumerable<PackingSpecData> GetDataPackingMaster(string packingspecid)
        {
            if (packingspecid == null)
            {
                packingspecid = "";
            }
            IEnumerable<PackingSpecData> listdata ;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            listdata = db.Query<PackingSpecData>("PackingSpecGet", new object[] { packingspecid });
            db.Close();
            return listdata;
            
        }

        public int UploadPackingMaster(
           int packingspecid,
           string supplierCode,
           string supplierPlant,
           string shippingDock,
           string companyCode,
           string rcvPlantCode,
           string dockCode,
           string partNo,
           string palleteType,
           double palleteLength,
           double palleteWidth,
           double palleteHeight,
           string oprt
           )
        {
            int result;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            result = db.Execute("PackingSpecUpload", new object[] 
                { 
                    packingspecid,
                    supplierCode,
                    supplierPlant,
                    shippingDock,
                    companyCode,
                    rcvPlantCode,
                    dockCode,
                    partNo,
                    palleteType,
                    palleteLength,
                    palleteWidth,
                    palleteHeight,
                    oprt
                });
            db.Close();
            return result;
        }

        public int ExecuteInsertUpdatePackingMaster( 
            int packingspecid,
            string supplierCode,
            string supplierPlant,
            string shippingDock,
            string companyCode,
            string rcvPlantCode,
            string dockCode,
            string partNo,
            string palleteType,
            double palleteLength,
            double palleteWidth,
            double palleteHeight,
            string oprt,
            char inserting
            )
        {
            int result;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            if (inserting == 'Y')
            {
                result = db.Execute("PackingSpecInsert", new object[] 
                { 
                    packingspecid,
                    supplierCode,
                    supplierPlant,
                    shippingDock,
                    companyCode,
                    rcvPlantCode,
                    dockCode,
                    partNo,
                    palleteType,
                    palleteLength,
                    palleteWidth,
                    palleteHeight,
                    oprt
                });
            }
            else
            {
                result = db.Execute("PackingSpecUpdate", new object[] 
                { 
                    packingspecid,
                    supplierCode,
                    supplierPlant,
                    shippingDock,
                    companyCode,
                    rcvPlantCode,
                    dockCode,
                    partNo,
                    palleteType,
                    palleteLength,
                    palleteWidth,
                    palleteHeight,
                    oprt
                });
            }
            db.Close();
            return result;
        }

        public int DeletePackingMaster(int packingspecid)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int result = db.Execute("PackingSpecDelete", new object[] { packingspecid });
            db.Close();
            return result;
        }

        #endregion CRUD

        #region NewUpload
        private String _uploadDirectory = "";
        private String _filePath = "";
        string MSG = "";

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        private long _processId = 0;
        private long ProcessId
        {
            get
            {
                if (_processId == 0)
                {
                    IDBContext db = DbContext;
                    try
                    {
                        _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
                    }
                    catch (Exception exc) { _processId = 0; }
                    finally { db.Close(); }
                    return _processId;
                }
                return _processId;
            }
        }
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/PackingSpecMaster/UploadTemp/");
                return _uploadDirectory;
            }
        }
        public class UploadControlHelper
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 60751520
            };
        }

        private void ReadingExcelInServer()
        {
            string conStr = "";
            conStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source='" + _FilePath + "'" + "; Extended Properties ='Excel 8.0;HDR=Yes'";
            conStr = string.Format(conStr, _FilePath);
            OleDbConnection con = new OleDbConnection(conStr);
            try
            {
                List<PackingSpecData> listobj = new List<PackingSpecData>();
                OleDbDataReader dr = null;
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = "Select * from [Sheet1$]";
                dr = cmd.ExecuteReader();
                 
                ////for show field name in datareader
                //var columns = new List<string>();
                //for (int i = 0; i < dr.FieldCount; i++)
                //{
                //    columns.Add(dr.GetName(i));
                //}
                while (dr.Read())
                {
                    PackingSpecData obj = new PackingSpecData();
                    if (!dr.IsDBNull(dr.GetOrdinal("SupplierCode"))) obj.SupplierCode = Convert.ToString(dr.GetValue(dr.GetOrdinal("SupplierCode")));
                    if (!dr.IsDBNull(dr.GetOrdinal("SupplierPlant"))) obj.SupplierPlant = Convert.ToString(dr.GetValue(dr.GetOrdinal("SupplierPlant")));
                    if (!dr.IsDBNull(dr.GetOrdinal("ShippingDock"))) obj.ShippingDock = Convert.ToString(dr.GetValue(dr.GetOrdinal("ShippingDock")));
                    if (!dr.IsDBNull(dr.GetOrdinal("CompanyCode"))) obj.CompanyCode = Convert.ToString(dr.GetValue(dr.GetOrdinal("CompanyCode")));
                    if (!dr.IsDBNull(dr.GetOrdinal("RcvPlantCode"))) obj.RcvPlantCode = Convert.ToString(dr.GetValue(dr.GetOrdinal("RcvPlantCode")));
                    if (!dr.IsDBNull(dr.GetOrdinal("DockCode"))) obj.DockCode = Convert.ToString(dr.GetValue(dr.GetOrdinal("DockCode")));
                    if (!dr.IsDBNull(dr.GetOrdinal("PartNo"))) obj.PartNo = Convert.ToString(dr.GetValue(dr.GetOrdinal("PartNo")));
                    if (!dr.IsDBNull(dr.GetOrdinal("PalleteType"))) obj.PalleteType = Convert.ToString(dr.GetValue(dr.GetOrdinal("PalleteType")));
                    if (!dr.IsDBNull(dr.GetOrdinal("PalleteLength"))) obj.PalleteLength = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("PalleteLength")));
                    if (!dr.IsDBNull(dr.GetOrdinal("PalleteWidth"))) obj.PalleteWidth = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("PalleteWidth")));
                    if (!dr.IsDBNull(dr.GetOrdinal("PalleteHeight"))) obj.PalleteHeight = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("PalleteHeight")));
                    listobj.Add(obj);
                }
                dr.Close();
                //proses upload to database
                ProsesUploadToDatabase(listobj);
            }
            catch (Exception ex)
            {
                MSG += "\nField " + ex.Message + " not match in database!";
            }
            finally
            {
                con.Dispose();
            }
        }
        public void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            DataSet ds = new DataSet();
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;
                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
                ReadingExcelInServer();
            }
            if (System.IO.File.Exists(_filePath))
            {
                System.IO.File.Delete(_filePath);
            }
            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = MSG;
        }
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("ucCallbacks", UploadControlHelper.ValidationSettings, ucCallbacks_FileUploadComplete);
            return null;
        }
        private void ProsesUploadToDatabase(List<PackingSpecData> ObjList)
        {
            foreach (PackingSpecData obj in ObjList)
            {
                UploadPackingMaster(
                 obj.PackingSpecId, obj.SupplierCode, obj.SupplierPlant, obj.ShippingDock,
                 obj.CompanyCode, obj.RcvPlantCode, obj.DockCode, obj.PartNo,
                 obj.PalleteType, obj.PalleteLength, obj.PalleteWidth, obj.PalleteHeight, AuthorizedUser.Username);
            }
            MSG = "Upload data success";
        } 
        #endregion NewUpload

        #region NewDownload
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadFile(string value, string key)
        {
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;
            string fileName = String.Empty;
            List<PackingSpecData> List = GetDataPackingMaster(GetDataCondition(value, key)).ToList();
            fileName = "PackingSpec.xls";
            hasil = exporter.Write(ConvertToDataTable(List), "Sheet1");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
            Response.BinaryWrite(hasil);
            Response.End();
        }

        #endregion NewDownload
    }
}

//public ActionResult CallOpenPopUp(int ids)
//{
//    PackingSpecData packing = GetSingleDataPackingMaster(ids);
//    return Json(packing, JsonRequestBehavior.AllowGet);
//}

//public ActionResult InsertUpdateDataMaster(int packingspecid,string supplierCode,string supplierPlant,string shippingDock,string companyCode,string rcvPlantCode,string dockCode,string partNo,string palleteType,double palleteLength,double palleteWidth,double palleteHeight,double palleteVolume,char inserting)
//{
//    result = ExecuteInsertUpdatePackingMaster(
//        packingspecid, supplierCode, supplierPlant, shippingDock,
//        companyCode,rcvPlantCode, dockCode,partNo,
//        palleteType,palleteLength,palleteWidth,palleteHeight,palleteVolume,AuthorizedUser.Username,inserting);
//    return Json(result, JsonRequestBehavior.AllowGet);
//}

//public ActionResult DeleteDataMaster(string spectId)
//{
//    //result = DeletePackingMaster(spectId);
//    //return Json(result, JsonRequestBehavior.AllowGet);
//}