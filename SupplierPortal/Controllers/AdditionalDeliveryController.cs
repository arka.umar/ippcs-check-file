﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//////////////////////
using System.Xml;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;
using System.Transactions;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Portal.Models.DCLInquiry;
using Portal.Models.AdditionalDelivery;
using Portal.Models.Globals;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.Mvc;

namespace Portal.Controllers
{
    public class AdditionalDeliveryController : BaseController
    {
        private AdditionalDeliveryModel mdl;
        public AdditionalDeliveryController()
            : base("Additional Delivery")
        {
            PageLayout.UseMessageBoard = true;
        }

        protected override void StartUp()
        {
            if (TempData["SesDataMain"] != null)
                TempData["SesDataMain"] = "";

            if (TempData["SesAllDataMain"] != null)
                TempData["SesAllDataMain"] = "";

            if (TempData["SesAllDataMainOrder"] != null)
                TempData["SesAllDataMainOrder"] = "";

            if (TempData["SesDataSourceMain"] != null)
                TempData["SesDataSourceMain"] = "";

            if (TempData["SesDataDestinationMain"] != null)
                TempData["SesDataDestinationMain"] = "";
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            //---=== Load data combo route
            ViewData["ComboRoute"] = db.Query<ComboRoute>("GetRoute").ToList<ComboRoute>();

            //---=== Load data logistic partner
            List<LogisticPartner> listLP = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLP = QueryLog.ToList<LogisticPartner>();

            ViewData["LogisticPartnerData"] = listLP;

            //---=== Load grid
            AdditionalDeliveryModel adm = new AdditionalDeliveryModel();
            adm.GridContent = adm.GridContent.ToList<AdditionalDeliveryGridContent>();
            Model.AddModel(adm);

            //---=== Load dock & supplier grid
            AdditionalDeliveryModel modelSource = Model.GetModel<AdditionalDeliveryModel>();
            modelSource.GridSourceContent = modelSource.GridSourceContent.ToList<AdditionalDeliverySourceGridContent>();
            Model.AddModel(modelSource);

            AdditionalDeliveryModel modelDestination = Model.GetModel<AdditionalDeliveryModel>();
            modelDestination.GridDestinationContent = modelDestination.GridDestinationContent.ToList<AdditionalDeliveryDestinationGridContent>();
            Model.AddModel(modelDestination);

            //---=== Load data supplier
            List<Supplier> listSup = new List<Supplier>();
            var QuerySup = db.Query<Supplier>("GetAllSupplier");
            listSup = QuerySup.ToList<Supplier>();
            ViewData["PopupSupplierData"] = listSup.ToList<Supplier>();
            ViewData["WoSupplierData"] = listSup.ToList<Supplier>();

            //---=== Load data dock
            List<DockPlant> listDock = new List<DockPlant>();
            var QueryDock = db.Query<DockPlant>("GetAllDockAndPlant");
            listDock = QueryDock.ToList<DockPlant>();
            ViewData["PopupDockData"] = listDock.ToList<DockPlant>();
            ViewData["WoDockData"] = listDock.ToList<DockPlant>();

            //---=== Load Popup grid
            AdditionalDeliveryModel PopupAdm = new AdditionalDeliveryModel();
            PopupAdm.GridContentPopup = PopupAdm.GridContentPopup.ToList<PopupAdditionalDeliveryGridContent>();
            //PopupAdm.GridContentPopup = PopupADget();
            db.Close();
            Model.AddModel(PopupAdm);
        }

        #region Saving Data
        public ContentResult SavingDeliveryOrder(
                string DeliveryType, string deliveryNo, string PRoute, string PRate, string LogisticPartner, string Reason,
                string GridId, string arrivalPlanDate, string ArrivalDepatureDate, string PickupDate, string MaxApproveDuration, string orderType)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            //---=== Variable for Parameter ===---//
            string VarRoute = "";
            string VarRate = "";
            char[] SplitHead = { ';' };
            char[] SplitDetail = { ',' };
            string[] MainGridExtract;
            string[] DetailGridExtract;
            string[] SourceGridExtract;
            string[] DetailSourceGridExtract;
            string[] DestGridExtract;
            string[] DetailDestGridExtract;
            //---=== End Variable for Parameter ===---//

            //---=== Variable for Process ===---//
            string varTempDataSource = "";
            string varTempDataDestination = "";

            string ApprovalStatus = "";
            string RequestStatus = "";
            string DockCode = "";
            string DeliveryStatus = "D";
            string SupplierCode = "";
            string SupplierPlant = "";
            string[] extract = new string[2];

            string originCode = "";
            string originPlant = "";
            string originCompany = "";
            string destinationCode = "";
            string destinationPlant = "";
            string destinationCompany = "";

            Nullable<DateTime> pickup_dt;
            Nullable<DateTime> arrival_plan_dt;
            Nullable<DateTime> departure_plan_dt;
            Nullable<DateTime> arrival_actual_dt;
            Nullable<DateTime> max_approve_duration_dt;
            Nullable<DateTime> OrderReleaseDate;

            Nullable<DateTime> origin_arrival_plan_dt;
            Nullable<DateTime> origin_departure_plan_dt;
            Nullable<DateTime> destination_arrival_plan_dt;
            Nullable<DateTime> destination_departure_plan_dt;

            string created_by;
            DateTime created_dt;
            string changed_by;
            Nullable<DateTime> changed_dt;

            string ProcessId = "0";
            string Result = "";
            //---=== End Variable for Process ===---//

            #region Get Grid Source & Destination
            if (TempData["SesDataSourceMain"] != null)
            {
                if (TempData["SesDataSourceMain"].ToString() != "")
                {
                    varTempDataSource = TempData["SesDataSourceMain"].ToString();
                }
            }

            if (TempData["SesDataDestinationMain"] != null)
            {
                if (TempData["SesDataDestinationMain"].ToString() != "")
                {
                    varTempDataDestination = TempData["SesDataDestinationMain"].ToString();
                }
            }
            #endregion

            #region Setting Static Variable
            created_by = AuthorizedUser.Username;
            created_dt = DateTime.Now;
            changed_by = null;
            changed_dt = (DateTime?)null;
            pickup_dt = PickupDate == "" ? pickup_dt = null : Convert.ToDateTime(formatingShortDate(PickupDate));
            DateTime OfficeHourOffStart = DateTime.Parse(DateTime.Now.ToShortDateString() + " 20:30:00");
            DateTime OfficeHourOffEnd = DateTime.Parse(DateTime.Now.ToShortDateString() + " 06:00:00");
            extract = MaxApproveDuration.Split(' ');
            max_approve_duration_dt = extract[0] == "" ? max_approve_duration_dt = null : Convert.ToDateTime(formatingShortDateTime(MaxApproveDuration));
            #endregion

            try
            {
                if (DeliveryType == "Add New Delivery")
                {
                    ProcessId = "1"; //---=== Insert to TB_R_DELIVERY_CTL_H
                    //---=== New Delivery ===---//
                    #region Generate Delivery Number
                    ProcessId = "1A"; //---=== Generate Delivery Number
                    var QueryAdditonalDelivery = db.Query<getDelivery>("GetDeliveryNoAdditionalDelivery");
                    foreach (var item in QueryAdditonalDelivery)
                    {
                        deliveryNo = item.DeliveryAdditional;
                    }
                    #endregion

                    #region Get Route & Rate
                    ProcessId = "1B"; //---=== Get Route & Rate
                    MainGridExtract = GridId.Split(SplitHead);
                    for (int iExtr = 0; iExtr < (MainGridExtract.Length); iExtr++)
                    {
                        DetailGridExtract = MainGridExtract[iExtr].Split(SplitDetail);
                        if (orderType == "W")
                        {
                            //VarRoute = "-";
                            //VarRate = "-";
                            var Qrate = db.Fetch<AdditionalDeliveryExtraRouteRate>("NewExtraTruckRate", new object[] { formatingShortDate(PickupDate), PRoute, LogisticPartner });
                            string newRate = (from q in Qrate select q).FirstOrDefault().extra_rate;

                            VarRoute = PRoute;
                            VarRate = newRate;
                        }
                        else
                        {
                            VarRoute = DetailGridExtract[7].ToString();
                            VarRate = DetailGridExtract[8].ToString();
                        }
                    }
                    #endregion

                    #region Approval Status
                    ProcessId = "1C"; //---=== Approval Status
                    if ((DateTime.Now >= OfficeHourOffStart) || (DateTime.Now <= OfficeHourOffEnd))
                    {
                        ApprovalStatus = "Auto Approved";
                        RequestStatus = "Approved By system";
                    }
                    else
                    {
                        if (max_approve_duration_dt <= DateTime.Now)
                        {
                            ApprovalStatus = "Auto Approved";
                            RequestStatus = "Approved By system";
                        }
                        else
                        {
                            ApprovalStatus = "Waiting Approval";
                            RequestStatus = null;
                        }
                    }

                    //---=== bypass sementara
                    //ApprovalStatus = "Auto Approved";
                    //RequestStatus = "Approved By system";

                    DeliveryStatus = "N";
                    #endregion

                    #region Insert Header
                    ProcessId = "1D"; //---=== Insert Header
                    db.Execute("InsertDeliveryHeaderAdditionalDelivery", new object[] { 
                                "I", deliveryNo, "A", formatingShortDate(PickupDate),
                                VarRoute, VarRate, LogisticPartner, Reason, ((ApprovalStatus=="Auto Approved")?1:0), 
                                created_by, created_dt, changed_by, changed_dt });
                    #endregion

                    #region Insert Approval
                    ProcessId = "1E"; //---=== Insert Header Approval
                    db.Execute("InsertAdditionalDeliveryApproval", new object[] { 
                                    "H", deliveryNo, "N", RequestStatus, pickup_dt, VarRoute.Trim(), VarRate, LogisticPartner,
                                    Reason, max_approve_duration_dt, ApprovalStatus, created_by, created_dt, ""
                                });
                    #endregion

                    ProcessId = "2"; //---=== Insert to TB_R_DELIVERY_CTL_D
                    if (orderType == "O")
                    {
                        #region Insert Detail, Approval, order, route, manifest, supplierDock

                        //---=== Checking Datetime Value=== ---//
                        arrival_plan_dt = (DateTime?)null;
                        departure_plan_dt = (DateTime?)null;
                        arrival_actual_dt = (DateTime?)null;

                        #region Set Arrival Time & Departure Time
                        //---=== Jika With Order
                        if (varTempDataSource != "")
                        {
                            SourceGridExtract = varTempDataSource.Split(SplitHead);
                            for (int iSource = 0; iSource < SourceGridExtract.Length; iSource++)
                            {
                                DetailSourceGridExtract = SourceGridExtract[iSource].Split(SplitDetail);
                                arrival_plan_dt = ((DetailSourceGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[2].ToString()));
                                departure_plan_dt = ((DetailSourceGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[3].ToString()));
                                ProcessId = "2A"; //---=== Insert to Detail
                                #region Insert Detail
                                db.Execute("InsertDeliveryDailyAdditionalDelivery", new object[] { 
                                            deliveryNo, DetailSourceGridExtract[0].ToString(), arrival_plan_dt, departure_plan_dt, 
                                            arrival_actual_dt, created_by, created_dt, changed_by, changed_dt
                                        });
                                #endregion
                            }
                        }
                        #endregion
                        //---=== End Checking Datetime Value=== ---//

                        MainGridExtract = GridId.Split(SplitHead);
                        for (int iDetExtr = 0; iDetExtr < (MainGridExtract.Length); iDetExtr++)
                        {
                            DetailGridExtract = MainGridExtract[iDetExtr].Split(SplitDetail);

                            //---=== Checking Datetime Value=== ---//
                            arrival_plan_dt = (DateTime?)null;
                            departure_plan_dt = (DateTime?)null;

                            origin_arrival_plan_dt = (DateTime?)null;
                            origin_departure_plan_dt = (DateTime?)null;
                            destination_arrival_plan_dt = (DateTime?)null;
                            destination_departure_plan_dt = (DateTime?)null;

                            #region Get Arrival Time & Departure Time
                            //---=== Jika With Order
                            if (varTempDataSource != "")
                            {
                                SourceGridExtract = varTempDataSource.Split(SplitHead);
                                for (int iSource = 0; iSource < SourceGridExtract.Length; iSource++)
                                {
                                    DetailSourceGridExtract = SourceGridExtract[iSource].Split(SplitDetail);
                                    if (DetailGridExtract[10].ToString() == DetailSourceGridExtract[0].ToString())
                                    {
                                        arrival_plan_dt = ((DetailSourceGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[2].ToString()));
                                        departure_plan_dt = ((DetailSourceGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[3].ToString()));
                                    }

                                    origin_arrival_plan_dt = ((DetailSourceGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[2].ToString()));
                                    origin_departure_plan_dt = ((DetailSourceGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[3].ToString()));

                                    if (varTempDataDestination != "")
                                    {
                                        DestGridExtract = varTempDataDestination.Split(SplitHead);
                                        for (int iDest = 0; iDest < DestGridExtract.Length; iDest++)
                                        {
                                            DetailDestGridExtract = DestGridExtract[iDest].Split(SplitDetail);

                                            destination_arrival_plan_dt = ((DetailDestGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailDestGridExtract[2].ToString()));
                                            destination_departure_plan_dt = ((DetailDestGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailDestGridExtract[3].ToString()));
                                        }

                                    }

                                }
                            }
                            #endregion

                            arrival_actual_dt = (DateTime?)null;
                            //---=== End Checking Datetime Value=== ---//

                            ProcessId = "2B"; //---=== Insert to Approval
                            #region Insert Approval
                            originCode = getFormatDock(DetailGridExtract[10].ToString()).Split(',')[0].ToString();
                            originPlant = getFormatDock(DetailGridExtract[10].ToString()).Split(',')[1].ToString();
                            originCompany = getFormatDock(DetailGridExtract[10].ToString()).Split(',')[2].ToString();

                            if (getDetailApprovalUnique(deliveryNo, originCompany, originPlant, originCode, DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), "-") == "0")
                            {
                                db.Execute("InsertAdditionalDeliveryApproval", new object[] { 
                                        "D", deliveryNo, originCompany, originPlant, originCode, DetailGridExtract[5].ToString(), 
                                        DetailGridExtract[6].ToString(), "-", origin_arrival_plan_dt, origin_departure_plan_dt, destination_arrival_plan_dt, destination_departure_plan_dt, created_by, created_dt
                                    });
                            }
                            #endregion

                            ProcessId = "2C"; //---=== Insert to Insert Order, Route, Manifest
                            #region Insert Order, Route, Manifest
                            OrderReleaseDate = ((DetailGridExtract[4].ToString() == "") ? (DateTime?)null : DateTime.Parse(formattingFullToShortDate(DetailGridExtract[4].ToString())));
                            db.Execute("InsertDataTableDeliveryOrder", new object[] { 
                                    deliveryNo, DetailGridExtract[2].ToString(), DetailGridExtract[1].ToString(), DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), 
                                    DetailGridExtract[3].ToString(), OrderReleaseDate, VarRoute.Trim(), VarRate, DetailGridExtract[9].ToString(), DetailGridExtract[10].ToString(), 
                                    DetailGridExtract[11].ToString(), DetailGridExtract[12].ToString(), DetailGridExtract[13].ToString(), arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                    created_by, created_dt, changed_by, changed_dt
                                });

                            db.Execute("InsertDataTableDeliveryRoute", new object[] { 
                                    deliveryNo, DetailGridExtract[2].ToString(), DetailGridExtract[1].ToString(), DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), 
                                    DetailGridExtract[3].ToString(), OrderReleaseDate, VarRoute.Trim(), VarRate, DetailGridExtract[9].ToString(), DetailGridExtract[10].ToString(), 
                                    DetailGridExtract[11].ToString(), DetailGridExtract[12].ToString(), DetailGridExtract[13].ToString(), arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                    created_by, created_dt, changed_by, changed_dt
                                });

                            db.Execute("InsertDataTableDeliveryManifest", new object[] { 
                                    deliveryNo, DetailGridExtract[2].ToString(), DetailGridExtract[1].ToString(), DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), 
                                    DetailGridExtract[3].ToString(), OrderReleaseDate, VarRoute.Trim(), VarRate, DetailGridExtract[9].ToString(), DetailGridExtract[10].ToString(), 
                                    DetailGridExtract[11].ToString(), DetailGridExtract[12].ToString(), DetailGridExtract[13].ToString(), arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                    created_by, created_dt, changed_by, changed_dt
                                });
                            #endregion
                            ProcessId = "2D"; //---=== Insert to Insert Supplier Dock
                            #region Insert SupplierDock
                            db.Execute("InsertAdditionalDeliverySupplier", new object[] { 
                                                deliveryNo, pickup_dt, DetailGridExtract[5].ToString(), getSupplierName(DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString()), DetailGridExtract[6].ToString(),
                                                DetailGridExtract[10].ToString(), VarRoute.Trim(), VarRate, arrival_plan_dt, departure_plan_dt, LogisticPartner, created_by, created_dt
                                            });
                            #endregion

                        }
                        #endregion
                    }
                    else
                    {
                        ProcessId = "2"; //---=== Insert to TB_R_DELIVERY_CTL_D
                        //---=== Without Order ===---//
                        VarRoute = "-";
                        VarRate = "-";

                        string tempDock = "";
                        int sameDock = 0;

                        //---=== Checking Datetime Value=== ---//
                        arrival_plan_dt = (DateTime?)null;
                        departure_plan_dt = (DateTime?)null;
                        arrival_actual_dt = (DateTime?)null;

                        origin_arrival_plan_dt = (DateTime?)null;
                        origin_departure_plan_dt = (DateTime?)null;
                        destination_arrival_plan_dt = (DateTime?)null;
                        destination_departure_plan_dt = (DateTime?)null;

                        #region Set Arrival Time & Departure Time
                        //---=== Jika With Order
                        if (varTempDataSource != "" || varTempDataDestination != "")
                        {
                            SourceGridExtract = varTempDataSource.Split(SplitHead);
                            for (int iSource = 0; iSource < SourceGridExtract.Length; iSource++)
                            {
                                DetailSourceGridExtract = SourceGridExtract[iSource].Split(SplitDetail);
                                arrival_plan_dt = ((DetailSourceGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[2].ToString()));
                                departure_plan_dt = ((DetailSourceGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[3].ToString()));

                                origin_arrival_plan_dt = ((DetailSourceGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[2].ToString()));
                                origin_departure_plan_dt = ((DetailSourceGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[3].ToString()));

                                if (varTempDataDestination != "")
                                {
                                    DestGridExtract = varTempDataDestination.Split(SplitHead);
                                    for (int iDest = 0; iDest < DestGridExtract.Length; iDest++)
                                    {
                                        DetailDestGridExtract = DestGridExtract[iDest].Split(SplitDetail);

                                        destination_arrival_plan_dt = ((DetailDestGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailDestGridExtract[2].ToString()));
                                        destination_departure_plan_dt = ((DetailDestGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailDestGridExtract[3].ToString()));
                                    }

                                }
                            }
                        }
                        #endregion
                        //---=== End Checking Datetime Value=== ---//

                        if (varTempDataSource != "" || varTempDataDestination != "")
                        {
                            tempDock = "";
                            sameDock = 0;
                            //---=== Approval ===---//                            
                            SourceGridExtract = varTempDataSource.Split(SplitHead);
                            for (int iExtr = 0; iExtr < (SourceGridExtract.Length); iExtr++)
                            {
                                DetailSourceGridExtract = SourceGridExtract[iExtr].Split(SplitDetail);
                                DestGridExtract = varTempDataDestination.Split(SplitHead);
                                for (int iExtrDest = 0; iExtrDest < (DestGridExtract.Length); iExtrDest++)
                                {
                                    DetailDestGridExtract = DestGridExtract[iExtrDest].Split(SplitDetail);

                                    if (DetailSourceGridExtract[1].ToString() == "D")
                                    {
                                        //---=== Dock place berada di source
                                        originCode = getFormatDock(DetailSourceGridExtract[0].ToString()).Split(',')[0].ToString();
                                        originPlant = getFormatDock(DetailSourceGridExtract[0].ToString()).Split(',')[1].ToString();
                                        originCompany = getFormatDock(DetailSourceGridExtract[0].ToString()).Split(',')[2].ToString();
                                        DockCode = originCode;
                                    }
                                    else
                                    {
                                        originCompany = DetailSourceGridExtract[0].ToString().Split('-')[0].ToString();
                                        originPlant = DetailSourceGridExtract[0].ToString().Split('-')[1].ToString();
                                        originCode = "-";
                                    }

                                    if (DetailDestGridExtract[1].ToString() == "D")
                                    {
                                        destinationCompany = getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[2].ToString();
                                        destinationPlant = getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[1].ToString();
                                        destinationCode = getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[0].ToString();
                                        DockCode = destinationCode;
                                    }
                                    else
                                    {
                                        destinationCompany = DetailDestGridExtract[0].ToString().Split('-')[0].ToString();
                                        destinationPlant = DetailDestGridExtract[0].ToString().Split('-')[1].ToString();
                                        destinationCode = "-";
                                    }

                                    if ((DetailSourceGridExtract[1].ToString() == "S") && (DetailDestGridExtract[1].ToString() == "S"))
                                    {
                                        DockCode = "-";
                                    }

                                    ProcessId = "2A"; //---=== Insert to Detail
                                    #region Insert Detail

                                    if (tempDock == "")
                                    {
                                        tempDock = DockCode;
                                    }
                                    else
                                    {
                                        string[] extractDock = tempDock.Split(';');
                                        for (int iDock = 0; iDock < extractDock.Length; iDock++)
                                        {
                                            if (extractDock[iDock].ToString() == DockCode)
                                                sameDock++;
                                            tempDock = tempDock + ";" + DockCode;
                                        }
                                    }

                                    if (sameDock == 0)
                                    {
                                        db.Execute("InsertDeliveryDailyAdditionalDelivery", new object[] { 
                                            deliveryNo, DockCode, arrival_plan_dt, departure_plan_dt, 
                                            arrival_actual_dt, created_by, created_dt, changed_by, changed_dt
                                        });
                                    }
                                    
                                    #endregion

                                    ProcessId = "2B"; //---=== Insert to Approval
                                    #region Insert Approval
                                    if (getDetailApprovalUnique(deliveryNo, originCompany, originPlant, originCode, destinationCompany, destinationPlant, destinationCode) == "0")
                                    {
                                        db.Execute("InsertAdditionalDeliveryApproval", new object[] { 
                                            "D", deliveryNo, originCompany, originPlant, originCode, 
                                            destinationCompany, destinationPlant, destinationCode,
                                            origin_arrival_plan_dt, origin_departure_plan_dt, destination_arrival_plan_dt, destination_departure_plan_dt, created_by, created_dt
                                        });
                                    }
                                    #endregion
                                }
                            }
                            //---=== End Approval ===---//

                            SourceGridExtract = varTempDataSource.Split(SplitHead);
                            for (int iExtr = 0; iExtr < (SourceGridExtract.Length); iExtr++)
                            {
                                DetailSourceGridExtract = SourceGridExtract[iExtr].Split(SplitDetail);
                                DestGridExtract = varTempDataDestination.Split(SplitHead);
                                for (int iExtrDest = 0; iExtrDest < (DestGridExtract.Length); iExtrDest++)
                                {
                                    DetailDestGridExtract = DestGridExtract[iExtrDest].Split(SplitDetail);

                                    if (DetailSourceGridExtract[1].ToString() == "D")
                                    {
                                        //---=== Dock place berada di source
                                        DockCode = DetailSourceGridExtract[0].ToString();
                                    }
                                    else
                                    {
                                        SupplierCode = DetailSourceGridExtract[0].ToString().Split('-')[0].ToString();
                                        SupplierPlant = DetailSourceGridExtract[0].ToString().Split('-')[1].ToString();
                                    }

                                    if (DetailDestGridExtract[1].ToString() == "D")
                                    {   
                                        DockCode = DetailDestGridExtract[0].ToString();
                                    }
                                    else
                                    {
                                        SupplierCode = DetailDestGridExtract[0].ToString().Split('-')[0].ToString();
                                        SupplierPlant = DetailDestGridExtract[0].ToString().Split('-')[1].ToString();
                                    }

                                    ProcessId = "2C"; //---=== Insert to Insert Order, Route, Manifest

                                    #region Insert Order
                                    OrderReleaseDate = (DateTime?)null;

                                    if ((DetailSourceGridExtract[1].ToString() == "D") && (DetailDestGridExtract[1].ToString() == "D"))
                                    { 
                                        //---=== supplier code + plant code diisi company code + plant
                                        db.Execute("InsertDataTableDeliveryOrder", new object[] { 
                                            deliveryNo, "-", "-", getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[2].ToString(), getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[0].ToString(), 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });

                                        db.Execute("InsertDataTableDeliveryRoute", new object[] { 
                                            deliveryNo, "-", "-", getFormatDock(DetailSourceGridExtract[0].ToString()).Split(',')[2].ToString(), getFormatDock(DetailSourceGridExtract[0].ToString()).Split(',')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", getFormatDock(DetailSourceGridExtract[0].ToString()).Split(',')[0].ToString(), 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });

                                        db.Execute("InsertDataTableDeliveryRoute", new object[] { 
                                            deliveryNo, "-", "-", getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[2].ToString(), getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[0].ToString(), 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });

                                        db.Execute("InsertDataTableDeliveryManifest", new object[] { 
                                            deliveryNo, "-", "-", getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[2].ToString(), getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", getFormatDock(DetailDestGridExtract[0].ToString()).Split(',')[0].ToString(), 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });
                                    }
                                    else if ((DetailSourceGridExtract[1].ToString() == "S") && (DetailDestGridExtract[1].ToString() == "S"))
                                    {
                                        //---=== supplier code + plant code diisi company code + plant
                                        db.Execute("InsertDataTableDeliveryOrder", new object[] { 
                                            deliveryNo, "-", "-", DetailDestGridExtract[0].ToString().Split('-')[0].ToString(), DetailDestGridExtract[0].ToString().Split('-')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", "-", 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });

                                        db.Execute("InsertDataTableDeliveryRoute", new object[] { 
                                            deliveryNo, "-", "-", DetailSourceGridExtract[0].ToString().Split('-')[0].ToString(), DetailSourceGridExtract[0].ToString().Split('-')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", "-", 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });

                                        db.Execute("InsertDataTableDeliveryRoute", new object[] { 
                                            deliveryNo, "-", "-", DetailDestGridExtract[0].ToString().Split('-')[0].ToString(), DetailDestGridExtract[0].ToString().Split('-')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", "-", 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });

                                        db.Execute("InsertDataTableDeliveryManifest", new object[] { 
                                            deliveryNo, "-", "-", DetailDestGridExtract[0].ToString().Split('-')[0].ToString(), DetailDestGridExtract[0].ToString().Split('-')[1].ToString(), "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", "-", 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });
                                    }
                                    else
                                    {
                                        db.Execute("InsertDataTableDeliveryOrder", new object[] { 
                                            deliveryNo, "-", "-", SupplierCode, SupplierPlant, "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", DockCode, 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });
                                        db.Execute("InsertDataTableDeliveryRoute", new object[] { 
                                            deliveryNo, "-", "-", SupplierCode, SupplierPlant, "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", DockCode, 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });
                                        db.Execute("InsertDataTableDeliveryManifest", new object[] { 
                                            deliveryNo, "-", "-", SupplierCode, SupplierPlant, "-", OrderReleaseDate, VarRoute.Trim(), VarRate, "-", DockCode, 
                                            "0", "0", "0", arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                            created_by, created_dt, changed_by, changed_dt
                                        });
                                    }
                                    #endregion

                                    ProcessId = "2D"; //---=== Insert to Insert Supplier Dock
                                    #region Insert SupplierDock
                                    db.Execute("InsertAdditionalDeliverySupplier", new object[] { 
                                            deliveryNo, pickup_dt, SupplierCode, getSupplierName(SupplierCode, SupplierPlant), SupplierPlant,
                                            DockCode, VarRoute.Trim(), VarRate, arrival_plan_dt, departure_plan_dt, LogisticPartner, created_by, created_dt
                                        });
                                    #endregion

                                }

                            }
                        }
                        //---=== End Without Order ===---//
                    }
                    //---=== End New Delivery ===---//
                }
                else
                {
                    //---=== Daily Delivery ===---//
                    ApprovalStatus = null;
                    RequestStatus = null;
                    VarRoute = "-";
                    VarRate = "-";

                    ProcessId = "1"; //---=== Update to TB_R_DELIVERY_CTL_H
                    #region Update Header
                    db.Execute("InsertDeliveryHeaderAdditionalDelivery", new object[] { 
                                               "U", deliveryNo, created_by, created_dt, "", "", "", "", "", "", "",
                                               "", "" });
                    #endregion

                    ProcessId = "2"; //---=== Insert to Order & Approval

                    MainGridExtract = GridId.Split(SplitHead);
                    for (int iDetExtr = 0; iDetExtr < (MainGridExtract.Length); iDetExtr++)
                    {
                        DetailGridExtract = MainGridExtract[iDetExtr].Split(SplitDetail);
                        //---=== Checking Datetime Value=== ---//
                        arrival_plan_dt = (DateTime?)null;
                        departure_plan_dt = (DateTime?)null;

                        #region Get Arrival Time & Departure Time
                        //---=== Jika With Order
                        if (varTempDataSource != "")
                        {
                            SourceGridExtract = varTempDataSource.Split(SplitHead);
                            for (int iSource = 0; iSource < SourceGridExtract.Length; iSource++)
                            {
                                DetailSourceGridExtract = SourceGridExtract[iSource].Split(SplitDetail);
                                if (DetailGridExtract[10].ToString() == DetailSourceGridExtract[0].ToString())
                                {
                                    arrival_plan_dt = ((DetailSourceGridExtract[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[2].ToString()));
                                    departure_plan_dt = ((DetailSourceGridExtract[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(DetailSourceGridExtract[3].ToString()));
                                }
                            }
                        }
                        #endregion

                        arrival_actual_dt = (DateTime?)null;
                        //---=== End Checking Datetime Value=== ---//

                        ProcessId = "2B"; //---=== Insert to Approval
                        #region Insert into Approval
                        //db.Execute("InsertAdditionalDeliveryApproval", new object[] { 
                        //        VarRoute, ApprovalStatus, RequestStatus, max_approve_duration_dt, pickup_dt, arrival_plan_dt, departure_plan_dt,
                        //        DetailGridExtract[10].ToString(), LogisticPartner, getLogisticPartnerName(LogisticPartner), VarRate, deliveryNo, DeliveryStatus,
                        //        DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), getSupplierName(DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString()), 
                        //        Reason, created_by, created_dt
                        //    });
                        #endregion

                        ProcessId = "2C"; //---=== Insert to Order
                        #region Insert Order, Route, Manifest
                        OrderReleaseDate = ((DetailGridExtract[4].ToString() == "") ? (DateTime?)null : DateTime.Parse(formattingFullToShortDate(DetailGridExtract[4].ToString())));
                        db.Execute("InsertDataTableDeliveryOrder", new object[] { 
                                    deliveryNo, DetailGridExtract[2].ToString(), DetailGridExtract[1].ToString(), DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), 
                                    DetailGridExtract[3].ToString(), OrderReleaseDate, VarRoute.Trim(), VarRate, DetailGridExtract[9].ToString(), DetailGridExtract[10].ToString(), 
                                    DetailGridExtract[11].ToString(), DetailGridExtract[12].ToString(), DetailGridExtract[13].ToString(), arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                    created_by, created_dt, changed_by, changed_dt
                                });
                        db.Execute("InsertDataTableDeliveryRoute", new object[] { 
                                    deliveryNo, DetailGridExtract[2].ToString(), DetailGridExtract[1].ToString(), DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), 
                                    DetailGridExtract[3].ToString(), OrderReleaseDate, VarRoute.Trim(), VarRate, DetailGridExtract[9].ToString(), DetailGridExtract[10].ToString(), 
                                    DetailGridExtract[11].ToString(), DetailGridExtract[12].ToString(), DetailGridExtract[13].ToString(), arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                    created_by, created_dt, changed_by, changed_dt
                                });
                        db.Execute("InsertDataTableDeliveryManifest", new object[] { 
                                    deliveryNo, DetailGridExtract[2].ToString(), DetailGridExtract[1].ToString(), DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString(), 
                                    DetailGridExtract[3].ToString(), OrderReleaseDate, VarRoute.Trim(), VarRate, DetailGridExtract[9].ToString(), DetailGridExtract[10].ToString(), 
                                    DetailGridExtract[11].ToString(), DetailGridExtract[12].ToString(), DetailGridExtract[13].ToString(), arrival_plan_dt, arrival_actual_dt, departure_plan_dt, 
                                    created_by, created_dt, changed_by, changed_dt
                                });
                        #endregion

                        ProcessId = "2D"; //---=== Insert to Insert Supplier Dock
                        #region Insert SupplierDock
                        db.Execute("InsertAdditionalDeliverySupplier", new object[] { 
                                            deliveryNo, pickup_dt, DetailGridExtract[5].ToString(), getSupplierName(DetailGridExtract[5].ToString(), DetailGridExtract[6].ToString()), DetailGridExtract[6].ToString(),
                                            DetailGridExtract[10].ToString(), VarRoute.Trim(), VarRate, arrival_plan_dt, departure_plan_dt, LogisticPartner, created_by, created_dt
                                        });
                        #endregion
                    }
                    //---=== End Daily Delivery ===---//
                }
                ProcessId = "0"; //---=== Finish & success process
            }
            catch (Exception ex)
            {
                Result = "Error add new delivery number. " + ex.ToString();

                //---=== Rollback
                if (DeliveryType == "Add New Delivery")
                {
                    #region Rollback New Delivery
                    db.Execute("RollbackAdditionalDelivery", new object[] { deliveryNo, "N", created_by, created_dt });
                    #endregion
                }
                else
                {
                    #region Rollback Daily Delivery
                    db.Execute("RollbackAdditionalDelivery", new object[] { deliveryNo, "D", created_by, created_dt });
                    #endregion
                }
                //db.Execute("spAdditionalDeliverySendingMail", new object[] { 
                //            Result
                //        });
            }
            db.Close();

            #region Result Message
            Result = "";
            switch (ProcessId)
            {
                case "0": Result = "Process delivery " + deliveryNo + " successful.";
                    break;
                case "1": Result = "Process delivery " + deliveryNo + " failed. Error when insert to delivery header.";
                    break;
                case "1A": Result = "Process delivery " + deliveryNo + " failed. Error when generate delivery number.";
                    break;
                case "1B": Result = "Process delivery " + deliveryNo + " failed. Error when get route & rate.";
                    break;
                case "1C": Result = "Process delivery " + deliveryNo + " failed. Error when get approval status.";
                    break;
                case "1D": Result = "Process delivery " + deliveryNo + " failed. Error when insert to delivery header.";
                    break;
                case "1E": Result = "Process delivery " + deliveryNo + " failed. Error when insert to delivery header approval.";
                    break;
                case "2": Result = "Process delivery " + deliveryNo + " failed. Error when insert to delivery detail.";
                    break;
                case "2A": Result = "Process delivery " + deliveryNo + " failed. Error when insert to delivery detail.";
                    break;
                case "2B": Result = "Process delivery " + deliveryNo + " failed. Error when insert to delivery approval.";
                    break;
                case "2C": Result = "Process delivery " + deliveryNo + " failed. Error when insert to delivery order.";
                    break;
                case "2D": Result = "Process delivery " + deliveryNo + " failed. Error when insert to supplier dock.";
                    break;
                default: Result = "";
                    break;
            }
            #endregion

            TempData["SesDataDestinationMain"] = null;
            TempData["SesDataSourceMain"] = null;
            TempData["SesAllDataMainOrder"] = null;
            TempData["SesAllDataMain"] = null;
            TempData["SesDataMain"] = null;

            return Content(Result);
        }

        private string formatingShortDateTime(string dateString)
        {
            string result = "";
            string[] extract = new string[2];
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extract = dateString.Split(' ');
                extractString = extract[0].Split('.');
                result = extractString[2] + "/" + extractString[1] + "/" + extractString[0] + " " + extract[1];
            }

            return result;
        }

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                result = extractString[2] + "/" + extractString[1] + "/" + extractString[0];
            }

            return result;
        }

        private string formattingFullToShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];
            //Sat May 04 2013 00:00:00 GMT+0700 (SE Asia Standard Time)
            extractString = dateString.Split(' ');
            result = extractString[3] + "-" + getMonthNumber(extractString[1]) + "-" + extractString[2] + " 00:00:00";

            return result;
        }

        private string getMonthNumber(string month)
        {
            string result = "";

            switch (month)
            {
                case "January":
                case "Jan": result = "01"; break;
                case "February":
                case "Feb": result = "02"; break;
                case "March":
                case "Mar": result = "03"; break;
                case "April":
                case "Apr": result = "04"; break;
                case "May": result = "05"; break;
                case "June":
                case "Jun": result = "06"; break;
                case "July":
                case "Jul": result = "07"; break;
                case "August":
                case "Aug": result = "08"; break;
                case "September":
                case "Sep": result = "09"; break;
                case "October":
                case "Oct": result = "10"; break;
                case "November":
                case "Nov": result = "11"; break;
                case "December":
                case "Dec": result = "12"; break;
                default: result = "01"; break;
            }

            return result;
        }

        private string getLogisticPartnerName(string LogisticPartnerCode)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (LogisticPartnerCode != "")
            {
                LogisticPartner QueryLog = db.SingleOrDefault<LogisticPartner>("GetLogisticPartnerNamebyCode", new object[] { LogisticPartnerCode });
                result = QueryLog.LPName;
            }
            db.Close();
            return result;
        }

        private string getSupplierName(string SupplierCode, string SupplierPlant)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (SupplierCode != "")
            {
                Supplier QueryLog = db.SingleOrDefault<Supplier>("GetSupplierNamebySupplierCodePlant", new object[] { SupplierCode, SupplierPlant });
                result = QueryLog.SupplierName;
            }
            db.Close();
            return result;
        }

        private string getFormatDock(string DockCode)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (DockCode != "")
            {
                DockCompanyClass QueryLog = db.SingleOrDefault<DockCompanyClass>("GetDockCompany", new object[] { DockCode });
                result = QueryLog.DockCompany;
            }
            db.Close();
            return result;
        }

        private string getDetailApprovalUnique(string DeliveryNo, string OriginCompany, string OriginPlant, string OriginCode,
                                               string DestCompany, string DestPlant, string DestCode)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (DeliveryNo != "")
            {
                ResultClass QueryLog = db.SingleOrDefault<ResultClass>("GetDetailLPApproval", new object[] { 
                    DeliveryNo, OriginCompany, OriginPlant, OriginCode, DestCompany, DestPlant, DestCode
                });
                result = QueryLog.ResultProccess;
            }
            db.Close();
            return result;
        }

        #endregion

        #region Header

        public ActionResult HeaderAdditionalDelivery()
        {
            return PartialView("HeaderAdditionalDelivery", Model);
        }

        #region header grid

        public ActionResult AdditionalDeliverySourceGrid()
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string trackingPoint = "";
            string trackingType = "";
            Nullable<DateTime> arrivalTime = null;
            Nullable<DateTime> departureTime = null;

            AdditionalDeliveryModel modelSource = Model.GetModel<AdditionalDeliveryModel>();
            modelSource.GridSourceContent = modelSource.GridSourceContent.ToList<AdditionalDeliverySourceGridContent>();
            Model.AddModel(modelSource);

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };

                //---=== Looping session history
                if (TempData["SesDataSourceMain"] != null)
                {
                    if (TempData["SesDataSourceMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataSourceMain"].ToString().Split(splitchar);
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);

                            trackingPoint = ParamIdLast[0].ToString();
                            trackingType = ParamIdLast[1].ToString();
                            arrivalTime = ((ParamIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[2].ToString()));
                            departureTime = ((ParamIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[3].ToString()));

                            Model.GetModel<AdditionalDeliveryModel>().GridSourceContent.Add(new AdditionalDeliverySourceGridContent()
                            {
                                TrackingPoint = trackingPoint,
                                TrackingType = trackingType,
                                ArrivalTime = arrivalTime,
                                DepartureTime = departureTime
                            });
                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        }
                    }
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("AdditionalDeliverySourceGrid", Model);
        }

        public ActionResult AdditionalDeliveryDestinationGrid()
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string trackingPoint = "";
            string trackingType = "";
            Nullable<DateTime> arrivalTime = null;
            Nullable<DateTime> departureTime = null;

            AdditionalDeliveryModel modelSource = Model.GetModel<AdditionalDeliveryModel>();
            modelSource.GridDestinationContent = modelSource.GridDestinationContent.ToList<AdditionalDeliveryDestinationGridContent>();
            Model.AddModel(modelSource);

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };

                //---=== Looping session history
                if (TempData["SesDataDestinationMain"] != null)
                {
                    if (TempData["SesDataDestinationMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataDestinationMain"].ToString().Split(splitchar);
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);

                            trackingPoint = ParamIdLast[0].ToString();
                            trackingType = ParamIdLast[1].ToString();
                            arrivalTime = ((ParamIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[2].ToString()));
                            departureTime = ((ParamIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[3].ToString()));

                            Model.GetModel<AdditionalDeliveryModel>().GridDestinationContent.Add(new AdditionalDeliveryDestinationGridContent()
                            {
                                TrackingPoint = trackingPoint,
                                TrackingType = trackingType,
                                ArrivalTime = arrivalTime,
                                DepartureTime = departureTime
                            });
                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        }
                    }
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("AdditionalDeliveryDestinationGrid", Model);
        }

        public PartialViewResult UpdatePartialSource(AdditionalDeliverySourceGridData editable)
        {
            ViewData["AdditionalDeliverySource"] = editable;

            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string trackingPoint = "";
            string trackingType = "";
            Nullable<DateTime> arrivalTime = null;
            Nullable<DateTime> departureTime = null;

            AdditionalDeliveryModel modelSource = Model.GetModel<AdditionalDeliveryModel>();
            modelSource.GridSourceContent = modelSource.GridSourceContent.ToList<AdditionalDeliverySourceGridContent>();
            Model.AddModel(modelSource);

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };

                //---=== Looping session history
                if (TempData["SesDataSourceMain"] != null)
                {
                    if (TempData["SesDataSourceMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataSourceMain"].ToString().Split(splitchar);
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);

                            trackingPoint = ParamIdLast[0].ToString();
                            trackingType = ParamIdLast[1].ToString();
                            arrivalTime = ((ParamIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[2].ToString()));
                            departureTime = ((ParamIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[3].ToString()));

                            Model.GetModel<AdditionalDeliveryModel>().GridSourceContent.Add(new AdditionalDeliverySourceGridContent()
                            {
                                TrackingPoint = trackingPoint,
                                TrackingType = trackingType,
                                ArrivalTime = arrivalTime,
                                DepartureTime = departureTime
                            });
                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        }
                    }
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("AdditionalDeliverySourceGrid", Model);
        }

        public PartialViewResult UpdatePartialDestination(AdditionalDeliveryDestinationGridData editable)
        {
            ViewData["AdditionalDeliveryDestination"] = editable;

            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string trackingPoint = "";
            string trackingType = "";
            Nullable<DateTime> arrivalTime = null;
            Nullable<DateTime> departureTime = null;

            AdditionalDeliveryModel modelSource = Model.GetModel<AdditionalDeliveryModel>();
            modelSource.GridDestinationContent = modelSource.GridDestinationContent.ToList<AdditionalDeliveryDestinationGridContent>();
            Model.AddModel(modelSource);

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };

                //---=== Looping session history
                if (TempData["SesDataDestinationMain"] != null)
                {
                    if (TempData["SesDataDestinationMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataDestinationMain"].ToString().Split(splitchar);
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);

                            trackingPoint = ParamIdLast[0].ToString();
                            trackingType = ParamIdLast[1].ToString();
                            arrivalTime = ((ParamIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[2].ToString()));
                            departureTime = ((ParamIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[3].ToString()));

                            Model.GetModel<AdditionalDeliveryModel>().GridDestinationContent.Add(new AdditionalDeliveryDestinationGridContent()
                            {
                                TrackingPoint = trackingPoint,
                                TrackingType = trackingType,
                                ArrivalTime = arrivalTime,
                                DepartureTime = departureTime
                            });
                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        }
                    }
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("AdditionalDeliveryDestinationGrid", Model);
        }

        public ActionResult UpdateAdditionalDeliverySourceGrid(string TrackingPointSource, string TrackingTypeSource,
                                                               string ArrivalPlanDate, string DeptPlanDate)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string varTempData = "";
            string trackingPoint = "";
            string trackingType = "";
            Nullable<DateTime> arrivalTime = null;
            Nullable<DateTime> departureTime = null;

            AdditionalDeliveryModel modelSource = Model.GetModel<AdditionalDeliveryModel>();
            modelSource.GridSourceContent = modelSource.GridSourceContent.ToList<AdditionalDeliverySourceGridContent>();
            Model.AddModel(modelSource);

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };

                //---=== Looping session history
                if (TempData["SesDataSourceMain"] != null)
                {
                    if (TempData["SesDataSourceMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataSourceMain"].ToString().Split(splitchar);
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);

                            if ((TrackingPointSource + "-" + TrackingTypeSource) == (ParamIdLast[0].ToString() + "-" + ParamIdLast[1].ToString()))
                            {
                                trackingPoint = TrackingPointSource;
                                trackingType = TrackingTypeSource;
                                arrivalTime = ((ArrivalPlanDate == "") ? (DateTime?)null : DateTime.Parse(formatingShortDateTime(ArrivalPlanDate)));
                                departureTime = ((DeptPlanDate == "") ? (DateTime?)null : DateTime.Parse(formatingShortDateTime(DeptPlanDate)));
                            }
                            else
                            {
                                trackingPoint = ParamIdLast[0].ToString();
                                trackingType = ParamIdLast[1].ToString();
                                arrivalTime = ((ParamIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[2].ToString()));
                                departureTime = ((ParamIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[3].ToString()));
                            }

                            Model.GetModel<AdditionalDeliveryModel>().GridSourceContent.Add(new AdditionalDeliverySourceGridContent()
                            {
                                TrackingPoint = trackingPoint,
                                TrackingType = trackingType,
                                ArrivalTime = arrivalTime,
                                DepartureTime = departureTime
                            });
                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());

                            if (varTempData == "")
                            {
                                varTempData = trackingPoint + "," + trackingType + "," +
                                              arrivalTime + "," + departureTime;
                            }
                            else
                            {
                                varTempData = varTempData + ";" + trackingPoint + "," + trackingType + "," +
                                              arrivalTime + "," + departureTime;
                            }

                        }
                    }
                    TempData["SesDataSourceMain"] = varTempData;
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
            }
            catch (Exception ex)
            { }

            return PartialView("AdditionalDeliverySourceGrid", Model);
        }

        public ActionResult UpdateAdditionalDeliveryDestinationGrid(string TrackingPointDestination, string TrackingTypeDestination,
                                                               string ArrivalPlanDate, string DeptPlanDate)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string varTempData = "";
            string trackingPoint = "";
            string trackingType = "";
            Nullable<DateTime> arrivalTime = null;
            Nullable<DateTime> departureTime = null;

            AdditionalDeliveryModel modelDestination = Model.GetModel<AdditionalDeliveryModel>();
            modelDestination.GridDestinationContent = modelDestination.GridDestinationContent.ToList<AdditionalDeliveryDestinationGridContent>();
            Model.AddModel(modelDestination);

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };

                //---=== Looping session history
                if (TempData["SesDataDestinationMain"] != null)
                {
                    if (TempData["SesDataDestinationMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataDestinationMain"].ToString().Split(splitchar);
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);

                            if ((TrackingPointDestination + "-" + TrackingTypeDestination) == (ParamIdLast[0].ToString() + "-" + ParamIdLast[1].ToString()))
                            {
                                trackingPoint = TrackingPointDestination;
                                trackingType = TrackingTypeDestination;
                                arrivalTime = ((ArrivalPlanDate == "") ? (DateTime?)null : DateTime.Parse(formatingShortDateTime(ArrivalPlanDate)));
                                departureTime = ((DeptPlanDate == "") ? (DateTime?)null : DateTime.Parse(formatingShortDateTime(DeptPlanDate)));
                            }
                            else
                            {
                                trackingPoint = ParamIdLast[0].ToString();
                                trackingType = ParamIdLast[1].ToString();
                                arrivalTime = ((ParamIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[2].ToString()));
                                departureTime = ((ParamIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamIdLast[3].ToString()));
                            }

                            Model.GetModel<AdditionalDeliveryModel>().GridDestinationContent.Add(new AdditionalDeliveryDestinationGridContent()
                            {
                                TrackingPoint = trackingPoint,
                                TrackingType = trackingType,
                                ArrivalTime = arrivalTime,
                                DepartureTime = departureTime
                            });
                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());

                            if (varTempData == "")
                            {
                                varTempData = trackingPoint + "," + trackingType + "," +
                                              arrivalTime + "," + departureTime;
                            }
                            else
                            {
                                varTempData = varTempData + ";" + trackingPoint + "," + trackingType + "," +
                                              arrivalTime + "," + departureTime;
                            }

                        }
                    }
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = varTempData;
                }
            }
            catch (Exception ex)
            { }

            return PartialView("AdditionalDeliveryDestinationGrid", Model);
        }

        #endregion

        public ActionResult EDPartialAdditional()
        {
            TempData["GridName"] = "grlEDHeader";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryLogED = db.Query<ExistingDeliveryList>("GetAllExistingDeliveryList");

            db.Close();
            ViewData["ExistingDeliveryData"] = QueryLogED;
            return PartialView("GridLookup/PartialGrid", ViewData["ExistingDeliveryData"]);
        }

        public ActionResult LPPartialAdditional()
        {
            TempData["GridName"] = "grlLPHeader";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            db.Close();
            ViewData["LogisticPartnerData"] = QueryLog;
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult LPPartialRoute()
        {
            TempData["GridName"] = "grlLPHeaderRoute";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<ComboRoute>("GetRoute").ToList<ComboRoute>();
            db.Close();
            ViewData["ComboRoute"] = QueryLog;
            return PartialView("GridLookup/PartialGrid", ViewData["ComboRoute"]);
        }

        public ContentResult DeleteSelectedOrderData()
        {
            //TempData["SesDataMain"] = null;
            try
            {
                AdditionalDeliveryModel adm = new AdditionalDeliveryModel();
                adm.GridContent = adm.GridContent.ToList<AdditionalDeliveryGridContent>();
                Model.AddModel(adm);
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        public ActionResult RetrieveRate()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<ComboRate>("GetRateByRoute", new object[] { Request.Params["Route"], formatingShortDate(Request.Params["PickupDate"]) });
            ViewData["ComboRate"] = QueryLog.ToList<ComboRate>();
            db.Close();
            return PartialView("PartialComboRate", ViewData["ComboRate"]);
        }

        #endregion

        #region Grid

        public ActionResult OrderGrid()
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string OrderNo = "";
            string ManifestNo = "";
            string OrderType = "";
            DateTime OrderReleaseDate;
            string SupplierCode = "";
            string SupplierPlant = "";
            string Route = "";
            string Rate = "";
            string ReceivePlantCode = "";
            string ReceiveDockCode = "";

            int TotalItem = 0;
            int TotalQty = 0;
            int ErrorQty = 0;
            DateTime ArrivalPlanDate;
            Nullable<DateTime> ArrivalActualDate;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            AdditionalDeliveryModel model = Model.GetModel<AdditionalDeliveryModel>();
            //model.GridContent = db.Query<AdditionalDeliveryGridContent>("GetDailyManifestOrder").ToList<AdditionalDeliveryGridContent>();
            model.GridContent = model.GridContent.ToList<AdditionalDeliveryGridContent>();
            Model.AddModel(model);

            if (TempData["SesDataMain"] != null)
            {
                try
                {
                    char[] splitchar = { ';' };
                    string[] ParamUpdate = TempData["SesDataMain"].ToString().Split(splitchar);
                    //Loop by count of array wa split ";"
                    for (int i = 0; i < ParamUpdate.Length; i++)
                    {
                        //Split ID by object with delimeter "_"
                        char[] SplitId = { ',' };
                        string[] ParamId = ParamUpdate[i].Split(SplitId);

                        OrderNo = ParamId[1].ToString();
                        ManifestNo = ParamId[2].ToString();
                        OrderType = ParamId[3].ToString();
                        OrderReleaseDate = DateTime.Parse(formattingFullToShortDate(ParamId[4].ToString()));
                        SupplierCode = ParamId[5].ToString();
                        SupplierPlant = ParamId[6].ToString();
                        Route = ParamId[7].ToString();
                        Rate = ParamId[8].ToString();
                        ReceivePlantCode = ParamId[9].ToString();
                        ReceiveDockCode = ParamId[10].ToString();
                        TotalItem = int.Parse(ParamId[11].ToString());
                        TotalQty = int.Parse(ParamId[12].ToString());
                        ErrorQty = int.Parse(ParamId[13].ToString());
                        ArrivalPlanDate = DateTime.Parse(formattingFullToShortDate(ParamId[14].ToString()));
                        if ((ParamId[15].ToString() == "null") || (ParamId[15].ToString() == ""))
                        {
                            ArrivalActualDate = null;
                        }
                        else
                        {
                            ArrivalActualDate = DateTime.Parse(formattingFullToShortDate(ParamId[15].ToString()));
                        }

                        Model.GetModel<AdditionalDeliveryModel>().GridContent.Add(new AdditionalDeliveryGridContent()
                        {
                            OrderNo = OrderNo,
                            ManifestNo = ManifestNo,
                            OrderType = OrderType,
                            OrderReleaseDate = OrderReleaseDate,
                            SupplierCode = SupplierCode,
                            SupplierPlant = SupplierPlant,
                            Route = Route,
                            Rate = Rate,
                            ReceivePlantCode = ReceivePlantCode,
                            ReceiveDockCode = ReceiveDockCode,
                            TotalItem = TotalItem,
                            TotalQty = TotalQty,
                            ErrorQty = ErrorQty,
                            ArrivalPlanDate = ArrivalPlanDate,
                            ArrivalActualDate = ArrivalActualDate
                        });
                        Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        //---=== End adding to model
                    }
                }
                catch (Exception ex) { }
            }

            TempData["SesAllDataMainOrder"] = null;
            string OrderReleaseDateString = "";
            string ArrivalPlanDateString = "";
            string ArrivalActualDateString = "";
            int j = 0;

            foreach (var dataRow in model.GridContent)
            {

                if ((dataRow.OrderReleaseDate.ToString() != null) || (dataRow.OrderReleaseDate.ToString() != "null"))
                    OrderReleaseDateString = dataRow.OrderReleaseDate.ToString("ddd MMM dd yyyy HH:mm:ss ") + "GMT+0700 (SE Asia Standard Time)";
                else
                    OrderReleaseDateString = null;

                if ((dataRow.ArrivalPlanDate.ToString() != null) || (dataRow.ArrivalPlanDate.ToString() != "null"))
                    ArrivalPlanDateString = dataRow.ArrivalPlanDate.ToString("ddd MMM dd yyyy HH:mm:ss ") + "GMT+0700 (SE Asia Standard Time)";
                else
                    ArrivalPlanDateString = null;

                if (dataRow.ArrivalActualDate != null)
                    ArrivalActualDateString = DateTime.Parse(dataRow.ArrivalActualDate.ToString()).ToString("ddd MMM dd yyyy HH:mm:ss ") + "GMT+0700 (SE Asia Standard Time)";
                else
                    ArrivalActualDateString = null;

                if (TempData["SesAllDataMainOrder"] != null)
                    TempData["SesAllDataMainOrder"] += ";" + j.ToString() + "," + dataRow.OrderNo + "," + dataRow.ManifestNo + "," + dataRow.OrderType + "," + OrderReleaseDateString + "," +
                                                 dataRow.SupplierCode + "," + dataRow.SupplierPlant + "," + dataRow.Route + "," + dataRow.Rate + "," + dataRow.ReceivePlantCode + "," +
                                                 dataRow.ReceiveDockCode + "," + dataRow.TotalItem + "," + dataRow.TotalQty + "," + dataRow.ErrorQty + "," + ArrivalPlanDateString + "," +
                                                 ArrivalActualDateString;
                else
                    TempData["SesAllDataMainOrder"] = j.ToString() + "," + dataRow.OrderNo + "," + dataRow.ManifestNo + "," + dataRow.OrderType + "," + OrderReleaseDateString + "," +
                                                 dataRow.SupplierCode + "," + dataRow.SupplierPlant + "," + dataRow.Route + "," + dataRow.Rate + "," + dataRow.ReceivePlantCode + "," +
                                                 dataRow.ReceiveDockCode + "," + dataRow.TotalItem + "," + dataRow.TotalQty + "," + dataRow.ErrorQty + "," + ArrivalPlanDateString + "," +
                                                 ArrivalActualDateString;

                j++;
            }

            if (TempData["SesDataMain"] != null)
                TempData["SesDataMain"] = TempData["SesDataMain"];

            if (TempData["SesDataSourceMain"] != null)
                TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];

            if (TempData["SesDataDestinationMain"] != null)
                TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];

            db.Close();
            return PartialView("OrderGrid", Model);
        }

        public ContentResult ClearGrid()
        {
            try
            {
                TempData["SesDataDestinationMain"] = null;
                TempData["SesDataSourceMain"] = null;
                TempData["SesDataMain"] = null;
                AdditionalDeliveryModel adm = new AdditionalDeliveryModel();
                adm.GridContent = adm.GridContent.ToList<AdditionalDeliveryGridContent>();
                Model.AddModel(adm);
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        #endregion

        #region json callback

        public JsonResult getDeliveryDetail(string DeliveryNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ExistingDeliveryList QueryLog = db.SingleOrDefault<ExistingDeliveryList>("GetDeliveryByDeliveryNo", new object[] { DeliveryNo });

            db.Close();
            return new JsonResult
            {
                Data = new
                {
                    DeliveryNo = QueryLog.DeliveryNo,
                    LPCode = QueryLog.LPCode,
                    Route = QueryLog.Route,
                    Rate = QueryLog.TripNo,
                    PickUpDate = QueryLog.PickupDate.ToString("dd.MM.yyyy")
                }
            };
        }

        public JsonResult GetDeliveryNo(string Route, string Rate, string Pickupdate)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ExistingDeliveryNoList QueryLog = db.SingleOrDefault<ExistingDeliveryNoList>("GetDDeliveryNoByRouteRate", new object[] { Route, Rate, formatingShortDate(Pickupdate).Replace('/', '-') });

            db.Close();
            if (QueryLog != null)
            {
                return new JsonResult
                {
                    Data = new
                    {
                        DeliveryNo = QueryLog.DeliveryNo,
                        LPCode = QueryLog.LPCode
                    }
                };
            }
            else
            {
                return new JsonResult
                {
                    Data = new
                    {
                        DeliveryNo = "no-data",
                        LPCode = "no-data"
                    }
                };
            }
        }

        #endregion

        #region Popup

        public ActionResult GetSupplierCombo()
        {
            TempData["GridName"] = "grlSupplier";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<Supplier> QueryLog = db.Fetch<Supplier>("GetAllSupplier");

            db.Close();
            ViewData["PopupSupplierData"] = QueryLog;
            return PartialView("GridLookup/PartialGrid", ViewData["PopupSupplierData"]);
        }

        public ActionResult GetSupplierComboWo()
        {
            TempData["GridName"] = "grlSupplierWo";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<Supplier> QueryLog = db.Fetch<Supplier>("GetAllSupplier");

            db.Close();
            ViewData["WoSupplierData"] = QueryLog;
            return PartialView("GridLookup/PartialGrid", ViewData["WoSupplierData"]);
        }

        public ActionResult GetDockCombo()
        {
            TempData["GridName"] = "grlDock";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<DockPlant> QueryLog = db.Fetch<DockPlant>("GetAllDockAndPlant");

            db.Close();
            ViewData["PopupDockData"] = QueryLog;
            return PartialView("GridLookup/PartialGrid", ViewData["PopupDockData"]);
        }

        public ActionResult GetDockComboWo()
        {
            TempData["GridName"] = "grlDockWo";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<DockPlant> QueryLog = db.Fetch<DockPlant>("GetAllDockAndPlant");

            db.Close();
            ViewData["WoDockData"] = QueryLog;
            return PartialView("GridLookup/PartialGrid", ViewData["WoDockData"]);
        }

        public ActionResult PopupOrderGrid(string SupplierCode, string DockCode, string ManifestNo, string OrderNo, string OrderRelease, string OrderTo)
        {
            TempData["SesAllDataMain"] = null;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            AdditionalDeliveryModel model = Model.GetModel<AdditionalDeliveryModel>();
            model.GridContent = db.Query<AdditionalDeliveryGridContent>("GetDailyManifestOrder", new object[] {
                SupplierCode, 
                DockCode, 
                ManifestNo, 
                OrderNo,
                OrderRelease, 
                OrderTo
            }).ToList<AdditionalDeliveryGridContent>();
            //model.GridContent = model.GridContent.ToList<AdditionalDeliveryGridContent>();
            Model.AddModel(model);

            TempData["SesAllDataMain"] = null;
            string OrderReleaseDate = "";
            string ArrivalPlanDate = "";
            string ArrivalActualDate = "";
            int i = 0;

            foreach (var dataRow in model.GridContent)
            {

                if ((dataRow.OrderReleaseDate.ToString() != null) || (dataRow.OrderReleaseDate.ToString() != "null"))
                    OrderReleaseDate = dataRow.OrderReleaseDate.ToString("ddd MMM dd yyyy HH:mm:ss ") + "GMT+0700 (SE Asia Standard Time)";
                else
                    OrderReleaseDate = null;

                if ((dataRow.ArrivalPlanDate.ToString() != null) || (dataRow.ArrivalPlanDate.ToString() != "null"))
                    ArrivalPlanDate = dataRow.ArrivalPlanDate.ToString("ddd MMM dd yyyy HH:mm:ss ") + "GMT+0700 (SE Asia Standard Time)";
                else
                    ArrivalPlanDate = null;

                if (dataRow.ArrivalActualDate != null)
                    ArrivalActualDate = DateTime.Parse(dataRow.ArrivalActualDate.ToString()).ToString("ddd MMM dd yyyy HH:mm:ss ") + "GMT+0700 (SE Asia Standard Time)";
                else
                    ArrivalActualDate = null;

                if (TempData["SesAllDataMain"] != null)
                    TempData["SesAllDataMain"] += ";" + i.ToString() + "," + dataRow.OrderNo + "," + dataRow.ManifestNo + "," + dataRow.OrderType + "," + OrderReleaseDate + "," +
                                                 dataRow.SupplierCode + "," + dataRow.SupplierPlant + "," + dataRow.Route + "," + dataRow.Rate + "," + dataRow.ReceivePlantCode + "," +
                                                 dataRow.ReceiveDockCode + "," + dataRow.TotalItem + "," + dataRow.TotalQty + "," + dataRow.ErrorQty + "," + ArrivalPlanDate + "," +
                                                 ArrivalActualDate;
                else
                    TempData["SesAllDataMain"] = i.ToString() + "," + dataRow.OrderNo + "," + dataRow.ManifestNo + "," + dataRow.OrderType + "," + OrderReleaseDate + "," +
                                                 dataRow.SupplierCode + "," + dataRow.SupplierPlant + "," + dataRow.Route + "," + dataRow.Rate + "," + dataRow.ReceivePlantCode + "," +
                                                 dataRow.ReceiveDockCode + "," + dataRow.TotalItem + "," + dataRow.TotalQty + "," + dataRow.ErrorQty + "," + ArrivalPlanDate + "," +
                                                 ArrivalActualDate;

                i++;
            }

            db.Close();
            return PartialView("popupOrderGrid", Model);
        }

        public ActionResult PopupOrderPartGrid(string ManifestNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            AdditionalDeliveryModel model = Model.GetModel<AdditionalDeliveryModel>();
            model.GridOrderPartContent = db.Query<PopupAdditionalDeliveryGridOrderPartContent>("GetOrderPartDelivery", new object[] {
                ManifestNo
            }).ToList<PopupAdditionalDeliveryGridOrderPartContent>();
            //model.GridContent = model.GridContent.ToList<AdditionalDeliveryGridContent>();
            Model.AddModel(model);
            db.Close();
            return PartialView("popupOrderPartGrid", Model);
        }

        public ActionResult AddingListOrder(string GridId, string GridSelection)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string OrderNo = "";
            string ManifestNo = "";
            string OrderType = "";
            DateTime OrderReleaseDate;
            string SupplierCode = "";
            string SupplierPlant = "";
            string Route = "";
            string Rate = "";
            string ReceivePlantCode = "";
            string ReceiveDockCode = "";

            int TotalItem = 0;
            int TotalQty = 0;
            int ErrorQty = 0;
            DateTime ArrivalPlanDate;
            Nullable<DateTime> ArrivalActualDate;

            try
            {
                if (GridSelection == "All")
                    GridId = TempData["SesAllDataMain"].ToString();
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);
                //Session["SesDataMain"] = Session["SesDataMain"] + GridId;

                //---=== Looping session history
                if (TempData["SesDataMain"] != null)
                {
                    if (TempData["SesDataMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataMain"].ToString().Split(splitchar);
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);

                            OrderNo = ParamIdLast[1].ToString();
                            ManifestNo = ParamIdLast[2].ToString();
                            OrderType = ParamIdLast[3].ToString();
                            OrderReleaseDate = DateTime.Parse(formattingFullToShortDate(ParamIdLast[4].ToString()));
                            SupplierCode = ParamIdLast[5].ToString();
                            SupplierPlant = ParamIdLast[6].ToString();
                            Route = ParamIdLast[7].ToString();
                            Rate = ParamIdLast[8].ToString();
                            ReceivePlantCode = ParamIdLast[9].ToString();
                            ReceiveDockCode = ParamIdLast[10].ToString();
                            TotalItem = int.Parse(ParamIdLast[11].ToString());
                            TotalQty = int.Parse(ParamIdLast[12].ToString());
                            ErrorQty = int.Parse(ParamIdLast[13].ToString());
                            ArrivalPlanDate = DateTime.Parse(formattingFullToShortDate(ParamIdLast[14].ToString()));
                            if ((ParamIdLast[15].ToString() == "null") || (ParamIdLast[15].ToString() == ""))
                            {
                                ArrivalActualDate = null;
                            }
                            else
                            {
                                ArrivalActualDate = DateTime.Parse(formattingFullToShortDate(ParamIdLast[15].ToString()));
                            }

                            Model.GetModel<AdditionalDeliveryModel>().GridContent.Add(new AdditionalDeliveryGridContent()
                            {
                                OrderNo = OrderNo,
                                ManifestNo = ManifestNo,
                                OrderType = OrderType,
                                OrderReleaseDate = OrderReleaseDate,
                                SupplierCode = SupplierCode,
                                SupplierPlant = SupplierPlant,
                                Route = Route,
                                Rate = Rate,
                                ReceivePlantCode = ReceivePlantCode,
                                ReceiveDockCode = ReceiveDockCode,
                                TotalItem = TotalItem,
                                TotalQty = TotalQty,
                                ErrorQty = ErrorQty,
                                ArrivalPlanDate = ArrivalPlanDate,
                                ArrivalActualDate = ArrivalActualDate
                            });
                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        }
                    }
                }
                //---=== End looping session history
                if (TempData["SesDataMain"] != null)
                    TempData["SesDataMain"] = TempData["SesDataMain"] + ";" + GridId;
                else
                    TempData["SesDataMain"] = TempData["SesDataMain"] + GridId;
                //Loop by count of array wa split ";"
                for (int i = 0; i < (ParamUpdate.Length); i++)
                {
                    //Split ID by object with delimeter "_"
                    char[] SplitId = { ',' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);

                    OrderNo = ParamId[1].ToString();
                    ManifestNo = ParamId[2].ToString();
                    OrderType = ParamId[3].ToString();
                    OrderReleaseDate = DateTime.Parse(formattingFullToShortDate(ParamId[4].ToString()));
                    SupplierCode = ParamId[5].ToString();
                    SupplierPlant = ParamId[6].ToString();
                    Route = ParamId[7].ToString();
                    Rate = ParamId[8].ToString();
                    ReceivePlantCode = ParamId[9].ToString();
                    ReceiveDockCode = ParamId[10].ToString();
                    TotalItem = int.Parse(ParamId[11].ToString());
                    TotalQty = int.Parse(ParamId[12].ToString());
                    ErrorQty = int.Parse(ParamId[13].ToString());
                    ArrivalPlanDate = DateTime.Parse(formattingFullToShortDate(ParamId[14].ToString()));
                    if ((ParamId[15].ToString() == "null") || (ParamId[15].ToString() == ""))
                    {
                        ArrivalActualDate = null;
                    }
                    else
                    {
                        ArrivalActualDate = DateTime.Parse(formattingFullToShortDate(ParamId[15].ToString()));
                    }

                    Model.GetModel<AdditionalDeliveryModel>().GridContent.Add(new AdditionalDeliveryGridContent()

                    {
                        OrderNo = OrderNo,
                        ManifestNo = ManifestNo,
                        OrderType = OrderType,
                        OrderReleaseDate = OrderReleaseDate,
                        SupplierCode = SupplierCode,
                        SupplierPlant = SupplierPlant,
                        Route = Route,
                        Rate = Rate,
                        ReceivePlantCode = ReceivePlantCode,
                        ReceiveDockCode = ReceiveDockCode,
                        TotalItem = TotalItem,
                        TotalQty = TotalQty,
                        ErrorQty = ErrorQty,
                        ArrivalPlanDate = ArrivalPlanDate,
                        ArrivalActualDate = ArrivalActualDate
                    });
                    Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                    //---=== End adding to model
                }

                return PartialView("OrderGrid", Model);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        public ActionResult addingTrackingFromPopup(string DockCode, string SupplierPlant, string FormSelect, string ArrivalTime, string DepartureTime)
        {
            string selectionTrackingPoint = ((DockCode == "") ? "S" : "D");
            int isSameNew = 0;
            int countLast = 0;
            string varTempData = "";
            string[] ParamUpdate;
            string[] ParamUpdateIdLast;

            ArrivalTime = formatingShortDateTime(ArrivalTime);
            DepartureTime = formatingShortDateTime(DepartureTime);

            try
            {
                char[] splitChar = { ';' };
                char[] SplitLastChar = { ',' };

                if (TempData["SesDataSourceMain"] == null && FormSelect == "S")
                    TempData["SesDataSourceMain"] = "";

                if (TempData["SesDataDestinationMain"] == null && FormSelect == "D")
                    TempData["SesDataDestinationMain"] = "";

                if (TempData["SesDataSourceMain"] != "" || TempData["SesDataDestinationMain"] != "")
                {
                    if (FormSelect == "S")
                    {
                        ParamUpdate = TempData["SesDataSourceMain"].ToString().Split(splitChar);
                        if (TempData["SesDataSourceMain"].ToString() == "")
                            countLast = 0;
                        else
                            countLast = ParamUpdate.Length;
                    }
                    else
                    {
                        ParamUpdate = TempData["SesDataDestinationMain"].ToString().Split(splitChar);
                        if (TempData["SesDataDestinationMain"].ToString() == "")
                            countLast = 0;
                        else
                            countLast = ParamUpdate.Length;
                    }

                    for (int i = 0; i < countLast; i++)
                    {
                        ParamUpdateIdLast = ParamUpdate[i].ToString().Split(SplitLastChar);
                        if (ParamUpdateIdLast[0].ToString() == ((selectionTrackingPoint == "D") ? DockCode : SupplierPlant))
                            isSameNew = isSameNew + 1;

                        if (FormSelect == "S")
                        {
                            //---=== Add to source grid
                            Model.GetModel<AdditionalDeliveryModel>().GridSourceContent.Add(new AdditionalDeliverySourceGridContent()
                            {
                                TrackingPoint = ParamUpdateIdLast[0].ToString(),
                                TrackingType = ParamUpdateIdLast[1].ToString(),
                                ArrivalTime = ((ParamUpdateIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateIdLast[2].ToString())),
                                DepartureTime = ((ParamUpdateIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateIdLast[3].ToString()))
                            });
                        }
                        else
                        {
                            //---=== Add to destination grid
                            Model.GetModel<AdditionalDeliveryModel>().GridDestinationContent.Add(new AdditionalDeliveryDestinationGridContent()
                            {
                                TrackingPoint = ParamUpdateIdLast[0].ToString(),
                                TrackingType = ParamUpdateIdLast[1].ToString(),
                                ArrivalTime = ((ParamUpdateIdLast[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateIdLast[2].ToString())),
                                DepartureTime = ((ParamUpdateIdLast[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateIdLast[3].ToString()))
                            });
                        }

                        if (varTempData == "")
                        {
                            varTempData = ParamUpdateIdLast[0].ToString() + "," +
                                          ParamUpdateIdLast[1].ToString() + "," +
                                          ParamUpdateIdLast[2].ToString() + "," +
                                          ParamUpdateIdLast[3].ToString();
                        }
                        else
                        {
                            varTempData = varTempData + ";" +
                                          ParamUpdateIdLast[0].ToString() + "," +
                                          ParamUpdateIdLast[1].ToString() + "," +
                                          ParamUpdateIdLast[2].ToString() + "," +
                                          ParamUpdateIdLast[3].ToString();
                        }

                        Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                    }
                }

                if (isSameNew == 0)
                {
                    if (FormSelect == "S")
                    {
                        //---=== Add to source grid
                        Model.GetModel<AdditionalDeliveryModel>().GridSourceContent.Add(new AdditionalDeliverySourceGridContent()
                        {
                            TrackingPoint = ((selectionTrackingPoint == "D") ? DockCode : SupplierPlant),
                            TrackingType = selectionTrackingPoint,
                            ArrivalTime = ((ArrivalTime == "") ? (DateTime?)null : DateTime.Parse(ArrivalTime)),
                            DepartureTime = ((DepartureTime == "") ? (DateTime?)null : DateTime.Parse(DepartureTime))
                        });
                    }
                    else
                    {
                        //---=== Add to destination grid
                        Model.GetModel<AdditionalDeliveryModel>().GridDestinationContent.Add(new AdditionalDeliveryDestinationGridContent()
                        {
                            TrackingPoint = ((selectionTrackingPoint == "D") ? DockCode : SupplierPlant),
                            TrackingType = selectionTrackingPoint,
                            ArrivalTime = ((ArrivalTime == "") ? (DateTime?)null : DateTime.Parse(ArrivalTime)),
                            DepartureTime = ((DepartureTime == "") ? (DateTime?)null : DateTime.Parse(DepartureTime))
                        });
                    }

                    if (varTempData == "")
                    {
                        varTempData = ((selectionTrackingPoint == "D") ? DockCode : SupplierPlant) + "," + selectionTrackingPoint + "," +
                                      ArrivalTime + "," + DepartureTime;
                    }
                    else
                    {
                        varTempData = varTempData + ";" + ((selectionTrackingPoint == "D") ? DockCode : SupplierPlant) + "," + selectionTrackingPoint + "," +
                                      ArrivalTime + "," + DepartureTime;
                    }

                    Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                }

                if (FormSelect == "S")
                {
                    TempData["SesDataSourceMain"] = varTempData;
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
                else
                {
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = varTempData;
                }

                if (TempData["SesDataMain"] != null)
                    TempData["SesDataMain"] = TempData["SesDataMain"];

            }
            catch (Exception ex) { }

            if (FormSelect == "S")
                return PartialView("AdditionalDeliverySourceGrid", Model);
            else
                return PartialView("AdditionalDeliveryDestinationGrid", Model);
        }

        public ActionResult addToListTracking(string GridId, string FormSelect, string TrackingType)
        {
            int isSameNew = 0;
            int isFirstId = 0;
            int countLast = 0;
            int countMain = 0;

            string varTempData = "";
            string varTempGrid = "";

            string[] paramGrid;
            string[] paramGridId;

            string[] ParamUpdate;
            string[] ParamUpdateId;
            string[] ParamUpdateIdLast;

            try
            {
                char[] splitChar = { ';' };
                char[] SplitLastChar = { ',' };

                if (TempData["SesDataSourceMain"] == null && FormSelect == "S")
                    TempData["SesDataSourceMain"] = "";

                if (TempData["SesDataDestinationMain"] == null && FormSelect == "D")
                    TempData["SesDataDestinationMain"] = "";

                //---=== Repeat GridId
                if (GridId != "")
                {
                    paramGrid = GridId.Split(splitChar);
                    if (GridId == "")
                        countMain = 0;
                    else
                        countMain = paramGrid.Length;

                    for (int iCopy = 0; iCopy < countMain; iCopy++)
                    {
                        paramGridId = paramGrid[iCopy].ToString().Split(SplitLastChar);
                        if (FormSelect == "S")
                        {
                            if (varTempGrid != "")
                                varTempGrid = varTempGrid + ";" + paramGridId[10].ToString() + ",D";
                            else
                                varTempGrid = paramGridId[10].ToString() + ",D";
                        }
                        else
                        {
                            if (varTempGrid != "")
                                varTempGrid = varTempGrid + ";" + (paramGridId[5].ToString() + "-" + paramGridId[6].ToString()) + ",S";
                            else
                                varTempGrid = (paramGridId[5].ToString() + "-" + paramGridId[6].ToString()) + ",S";
                        }
                    }

                    if (FormSelect == "S")
                    {
                        if (TempData["SesDataSourceMain"].ToString() == "")
                        {
                            countLast = 0;
                            if (varTempGrid != "")
                            {
                                countLast = varTempGrid.ToString().Split(splitChar).Length;
                            }
                        }
                        else
                        {
                            varTempGrid = TempData["SesDataSourceMain"].ToString() + ";" + varTempGrid;
                            countLast = varTempGrid.ToString().Split(splitChar).Length;
                        }
                    }
                    else
                    {
                        if (TempData["SesDataDestinationMain"].ToString() == "")
                        {
                            countLast = 0;
                            if (varTempGrid != "")
                            {
                                countLast = varTempGrid.ToString().Split(splitChar).Length;
                            }
                        }
                        else
                        {
                            varTempGrid = TempData["SesDataDestinationMain"].ToString() + ";" + varTempGrid;
                            countLast = varTempGrid.ToString().Split(splitChar).Length;
                        }
                    }

                    //---=== join last data & new data then filter same data as one data
                    ParamUpdate = varTempGrid.Split(splitChar);

                    //---=== Filter same data as one data
                    isSameNew = 0;
                    for (var i = 0; i < countLast; i++)
                    {
                        ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                        for (var j = 0; j < countLast; j++)
                        {
                            ParamUpdateIdLast = ParamUpdate[j].ToString().Split(SplitLastChar);
                            if ((ParamUpdateId[0].ToString() + "-" + ParamUpdateId[1].ToString()) == (ParamUpdateIdLast[0].ToString() + "-" + ParamUpdateIdLast[1].ToString()))
                            {
                                isSameNew++;
                                isFirstId = j;
                            }
                        }

                        if ((isSameNew == 1) || (isSameNew > 1 && isFirstId == i))
                        {
                            if (FormSelect == "S")
                            {
                                //---=== Add to source grid
                                Model.GetModel<AdditionalDeliveryModel>().GridSourceContent.Add(new AdditionalDeliverySourceGridContent()
                                {
                                    TrackingPoint = ParamUpdateId[0].ToString(),
                                    TrackingType = "D",
                                    ArrivalTime = (DateTime?)null,
                                    DepartureTime = (DateTime?)null
                                });
                            }
                            else
                            {
                                //---=== Add to destination grid
                                Model.GetModel<AdditionalDeliveryModel>().GridDestinationContent.Add(new AdditionalDeliveryDestinationGridContent()
                                {
                                    TrackingPoint = ParamUpdateId[0].ToString(),
                                    TrackingType = "S",
                                    ArrivalTime = (DateTime?)null,
                                    DepartureTime = (DateTime?)null
                                });
                            }

                            if (varTempData == "")
                            {
                                varTempData = ParamUpdateId[0].ToString() + "," + ParamUpdateId[1].ToString() + "," +
                                              "" + "," + "";
                            }
                            else
                            {
                                varTempData = varTempData + ";" + ParamUpdateId[0].ToString() + "," + ParamUpdateId[1].ToString() + "," +
                                              "" + "," + "";
                            }

                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        }
                        isSameNew = 0;
                    }
                    //---=== End filter
                }
                //---=== End Repeat GridId

                if (FormSelect == "S")
                {
                    TempData["SesDataSourceMain"] = varTempData;
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
                else
                {
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = varTempData;
                }

                //---=== Last Check Double

                //---=== End Last Check Double

            }
            catch (Exception ex) { }

            if (FormSelect == "S")
                return PartialView("AdditionalDeliverySourceGrid", Model);
            else
                return PartialView("AdditionalDeliveryDestinationGrid", Model);
        }

        public ContentResult GetPopupListOrder()
        {
            if (TempData["SesDataMain"] != null)
            {
                return Content(TempData["SesDataMain"].ToString());
            }
            else
            {
                return Content("no-data");
            }
        }

        public ActionResult DeletingListOrder(string GridId, string GridSelection)
        {
            string tempGrid = "";
            string tempVisibleIndex = "";
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            bool StatEntry = true;
            string OrderNo = "";
            string ManifestNo = "";
            string OrderType = "";
            DateTime OrderReleaseDate;
            string OrderReleaseDateOri = "";
            string SupplierCode = "";
            string SupplierPlant = "";
            string Route = "";
            string Rate = "";
            string ReceivePlantCode = "";
            string ReceiveDockCode = "";

            int TotalItem = 0;
            int TotalQty = 0;
            int ErrorQty = 0;
            DateTime ArrivalPlanDate;
            string ArrivalPlanDateOri;
            Nullable<DateTime> ArrivalActualDate;
            string ArrivalActualDateOri;
            TempData["SesDataSourceMain"] = null;
            TempData["SesDataDestinationMain"] = null;

            try
            {
                if (GridSelection == "All")
                    GridId = TempData["SesAllDataMainOrder"].ToString();
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);

                //Session["SesDataMain"] = Session["SesDataMain"] + GridId;

                //---=== Looping session history
                if (TempData["SesDataMain"] != null)
                {
                    if (TempData["SesDataMain"].ToString() != "")
                    {
                        string[] ParamUpdateLast = TempData["SesDataMain"].ToString().Split(splitchar);
                        TempData["SesDataMain"] = null;
                        for (int i = 0; i < ParamUpdateLast.Length; i++)
                        {
                            char[] SplitIdLast = { ',' };
                            string[] ParamIdLast = ParamUpdateLast[i].Split(SplitIdLast);
                            tempVisibleIndex = ParamIdLast[0].ToString();
                            OrderNo = ParamIdLast[1].ToString();
                            ManifestNo = ParamIdLast[2].ToString();
                            OrderType = ParamIdLast[3].ToString();
                            OrderReleaseDate = DateTime.Parse(formattingFullToShortDate(ParamIdLast[4].ToString()));
                            OrderReleaseDateOri = ParamIdLast[4].ToString();
                            SupplierCode = ParamIdLast[5].ToString();
                            SupplierPlant = ParamIdLast[6].ToString();
                            Route = ParamIdLast[7].ToString();
                            Rate = ParamIdLast[8].ToString();
                            ReceivePlantCode = ParamIdLast[9].ToString();
                            ReceiveDockCode = ParamIdLast[10].ToString();
                            TotalItem = int.Parse(ParamIdLast[11].ToString());
                            TotalQty = int.Parse(ParamIdLast[12].ToString());
                            ErrorQty = int.Parse(ParamIdLast[13].ToString());
                            ArrivalPlanDate = DateTime.Parse(formattingFullToShortDate(ParamIdLast[14].ToString()));
                            ArrivalPlanDateOri = ParamIdLast[14].ToString();
                            if (ParamIdLast[15].ToString() == "null")
                            {
                                ArrivalActualDate = null;
                                ArrivalActualDateOri = "null";
                            }
                            else
                            {
                                ArrivalActualDate = DateTime.Parse(formattingFullToShortDate(ParamIdLast[15].ToString()));
                                ArrivalActualDateOri = ParamIdLast[15].ToString();
                            }

                            //---=== Checking deleting grid id
                            StatEntry = true;
                            char[] SplitId = { ',' };
                            for (int j = 0; j < ParamUpdate.Length; j++)
                            {
                                string[] ParamId = ParamUpdate[j].Split(SplitId);
                                if ((ParamId[1].ToString() == ParamIdLast[1].ToString()) &&
                                    (ParamId[2].ToString() == ParamIdLast[2].ToString()) &&
                                    (ParamId[10].ToString() == ParamIdLast[10].ToString()))
                                    StatEntry = false;
                            }
                            //---=== End checking deleting grid id

                            if (StatEntry)
                            {
                                Model.GetModel<AdditionalDeliveryModel>().GridContent.Add(new AdditionalDeliveryGridContent()
                                {
                                    OrderNo = OrderNo,
                                    ManifestNo = ManifestNo,
                                    OrderType = OrderType,
                                    OrderReleaseDate = OrderReleaseDate,
                                    SupplierCode = SupplierCode,
                                    SupplierPlant = SupplierPlant,
                                    Route = Route,
                                    Rate = Rate,
                                    ReceivePlantCode = ReceivePlantCode,
                                    ReceiveDockCode = ReceiveDockCode,
                                    TotalItem = TotalItem,
                                    TotalQty = TotalQty,
                                    ErrorQty = ErrorQty,
                                    ArrivalPlanDate = ArrivalPlanDate,
                                    ArrivalActualDate = ArrivalActualDate
                                });

                                addingTrackingFromPopup(ReceiveDockCode, "", "D", "", "");
                                addingTrackingFromPopup("", (SupplierCode + "-" + SupplierPlant), "S", "", "");

                                Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                                tempGrid = tempVisibleIndex + "," + OrderNo + "," + ManifestNo + "," + OrderType + "," + OrderReleaseDateOri + "," + SupplierCode + "," +
                                           SupplierPlant + "," + Route + "," + Rate + "," + ReceivePlantCode + "," + ReceiveDockCode + "," + TotalItem + "," + TotalQty + "," +
                                           ErrorQty + "," + ArrivalPlanDateOri + "," + ArrivalActualDateOri;
                                if (TempData["SesDataMain"] != null)
                                    TempData["SesDataMain"] = TempData["SesDataMain"] + ";" + tempGrid;
                                else
                                    TempData["SesDataMain"] = TempData["SesDataMain"] + tempGrid;
                            }
                        }
                        //if (TempData["SesDataMain"] != null)
                        //    TempData["SesDataMain"] = TempData["SesDataMain"] + ";" + GridId;
                        //else
                        //    TempData["SesDataMain"] = TempData["SesDataMain"] + GridId;
                    }
                }
                //---=== End looping session history
                return PartialView("OrderGrid", Model);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        public ActionResult DeletingListTrackingPoint(string GridId, string FormSelect)
        {
            int isSame = 0;
            int countLast = 0;
            int countLastCheck = 0;
            string varTempData = "";
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string[] ParamUpdateCheck;
            string[] ParamUpdateIdCheck;

            try
            {
                char[] splitChar = { ';' };
                char[] SplitLastChar = { ',' };

                if (TempData["SesDataSourceMain"] == null && FormSelect == "S")
                    TempData["SesDataSourceMain"] = "";

                if (TempData["SesDataDestinationMain"] == null && FormSelect == "D")
                    TempData["SesDataDestinationMain"] = "";

                if (TempData["SesDataSourceMain"] != "" || TempData["SesDataDestinationMain"] != "")
                {
                    if (FormSelect == "S")
                    {
                        ParamUpdate = TempData["SesDataSourceMain"].ToString().Split(splitChar);
                        if (TempData["SesDataSourceMain"].ToString() == "")
                            countLast = 0;
                        else
                            countLast = ParamUpdate.Length;
                    }
                    else
                    {
                        ParamUpdate = TempData["SesDataDestinationMain"].ToString().Split(splitChar);
                        if (TempData["SesDataDestinationMain"].ToString() == "")
                            countLast = 0;
                        else
                            countLast = ParamUpdate.Length;
                    }

                    for (int i = 0; i < countLast; i++)
                    {
                        ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                        isSame = 0;
                        //---=== Cross check 
                        ParamUpdateCheck = GridId.Split(splitChar);
                        if (GridId == "")
                            countLastCheck = 0;
                        else
                            countLastCheck = ParamUpdateCheck.Length;

                        for (int j = 0; j < countLastCheck; j++)
                        {
                            ParamUpdateIdCheck = ParamUpdateCheck[j].ToString().Split(SplitLastChar);
                            if ((ParamUpdateIdCheck[1].ToString() + '-' + ParamUpdateIdCheck[2].ToString()) == (ParamUpdateId[0].ToString() + '-' + ParamUpdateId[1].ToString()))
                                isSame = isSame + 1;
                        }
                        //---=== End cross check

                        //---=== Adding
                        if (isSame == 0)
                        {
                            if (FormSelect == "S")
                            {
                                //---=== Add to source grid
                                Model.GetModel<AdditionalDeliveryModel>().GridSourceContent.Add(new AdditionalDeliverySourceGridContent()
                                {
                                    TrackingPoint = ParamUpdateId[0].ToString(),
                                    TrackingType = ParamUpdateId[1].ToString(),
                                    ArrivalTime = ((ParamUpdateId[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateId[2].ToString())),
                                    DepartureTime = ((ParamUpdateId[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateId[3].ToString()))
                                });
                            }
                            else
                            {
                                //---=== Add to destination grid
                                Model.GetModel<AdditionalDeliveryModel>().GridDestinationContent.Add(new AdditionalDeliveryDestinationGridContent()
                                {
                                    TrackingPoint = ParamUpdateId[0].ToString(),
                                    TrackingType = ParamUpdateId[1].ToString(),
                                    ArrivalTime = ((ParamUpdateId[2].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateId[2].ToString())),
                                    DepartureTime = ((ParamUpdateId[3].ToString() == "") ? (DateTime?)null : DateTime.Parse(ParamUpdateId[3].ToString()))
                                });
                            }

                            if (varTempData == "")
                            {
                                varTempData = ParamUpdateId[0].ToString() + "," +
                                              ParamUpdateId[1].ToString() + "," +
                                              ParamUpdateId[2].ToString() + "," +
                                              ParamUpdateId[3].ToString();
                            }
                            else
                            {
                                varTempData = varTempData + ";" +
                                              ParamUpdateId[0].ToString() + "," +
                                              ParamUpdateId[1].ToString() + "," +
                                              ParamUpdateId[2].ToString() + "," +
                                              ParamUpdateId[3].ToString();
                            }

                            Model.AddModel(Model.GetModel<AdditionalDeliveryModel>());
                        }
                        //---=== End adding
                    }
                }

                if (FormSelect == "S")
                {
                    TempData["SesDataSourceMain"] = varTempData;
                    TempData["SesDataDestinationMain"] = TempData["SesDataDestinationMain"];
                }
                else
                {
                    TempData["SesDataSourceMain"] = TempData["SesDataSourceMain"];
                    TempData["SesDataDestinationMain"] = varTempData;
                }

            }
            catch (Exception ex) { }

            if (FormSelect == "S")
                return PartialView("AdditionalDeliverySourceGrid", Model);
            else
                return PartialView("AdditionalDeliveryDestinationGrid", Model);
        }

        #endregion

        #region function 
            
        public List<LogisticPartner> GetAllLogisticPartners()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();
            db.Close();
            return listLogisticPartner;
        }

        #endregion

    }
}