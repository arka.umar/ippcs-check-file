﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RoleMaster;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;

namespace Portal.Controllers
{
    public class RoleMasterController : BaseController
    {
        public RoleMasterController()
            : base("Role Master Screen")
        { 
        }

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
        }

        public ActionResult PartialRoleMasterHeader()
        {
            return PartialView("PartialUserRoleHeader");
        }

        public ActionResult PartialPopUpRoleMaster()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            
            List<gridSystem> branchData = db.Query<gridSystem>("GetSystemIDForLookup").ToList();

            db.Close();

            ViewData["SystemData"] = branchData;

            return PartialView("PartialPopUpRoleMaster");
        }


        public ActionResult PartialPopUpRoleMasterEdit()
        {
            return PartialView("PartialPopUpEditRoleMaster");
        }
        




        public ActionResult PartialRoleMasterHeaderButton()
        {
            return PartialView("PartialUserRoleHeaderButton");
        }

        public ActionResult PartialRoleMasterData()
        {
            string roleName = Request.Params["p_roleName"];
            string mode = Request.Params["mode"];

            RoleModel rd = new RoleModel();

            if(mode == "search")
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                var datas = db.Query<RoleData>("getDataRoleMaster" , new object[]{roleName});
                rd.RoleDatas = datas.ToList<RoleData>();
                db.Close();
            }

            Model.AddModel(rd);           

            return PartialView("PartialRoleMasterData", Model);
        }


        public ActionResult PartialRoleMasterDataInit()
        {
            string roleName = Request.Params["p_roleName"];

            RoleModel rd = new RoleModel();
            Model.AddModel(rd);

            return PartialView("PartialRoleMasterData", Model);
        }

        public ContentResult DeleteData(string IDs)
        {
            string returnmessage = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("deleteRoleMaster", new object[] {IDs});
                returnmessage = "Success deleting the data.";
            }
            catch (Exception ex)
            {
                returnmessage = "Failed when try to delete the data, " + ex.Message;
            }
            finally {
                db.Close();
            }

            return Content(returnmessage.ToString());
        }

        public ContentResult getDataRoleForEdit(string idedit)
        {
            string rowData = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            try
            {
                rowData = db.SingleOrDefault<string>("getRoleDataForEdit", new object[] { idedit });
            }
            catch (Exception ex)
            {
                rowData = "F|"+ex.Message;
            }

            return Content(rowData);
        }

        public ContentResult AddRole(string auth, string systemId, string roleName, string authName, string authDesc)
        { 
            string returnMessage = "";
            string User = AuthorizedUser.Username;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                returnMessage = db.SingleOrDefault<string>("addNewRole", new object[] { auth, systemId, roleName, authName, authDesc, User });
            }
            catch (Exception ex)
            {
                returnMessage = "F|Failed when try to save the data, " + ex.Message;
            }

            return Content(returnMessage);
        }

        public ContentResult EditRole(string role, string rolename)
        {
            string returnMessage = "";
            string User = AuthorizedUser.Username;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                returnMessage = db.SingleOrDefault<string>("updateRole", new object[] { role, rolename, User });
            }
            catch (Exception ex)
            {
                returnMessage = "F|Failed when try to update the data, " + ex.Message;
            }

            return Content(returnMessage);
        }

        [HttpGet]
        public void download(string roleName)
        {
            string fileName = "RoleMaster" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<RoleData> qry = db.Query<RoleData>("getDataRoleMaster", new object[] { roleName });
            db.Close();
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(qry, "Role Master");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }



        public ActionResult getSystemAdd()
        {
            TempData["GridName"] = "grlSystemAdd";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
         
            List<gridSystem> branchData = db.Query<gridSystem>("GetSystemIDForLookup").ToList();

            db.Close();

            ViewData["SystemData"] = branchData;


            return PartialView("GridLookup/PartialGrid", ViewData["SystemData"]);
        }
   
        
    }
}
