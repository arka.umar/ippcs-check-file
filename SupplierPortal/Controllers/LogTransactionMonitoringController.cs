﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.LogTransactionMonitoring;

namespace Portal.Controllers
{
    public class LogTransactionMonitoringController : BaseController
    {

        public LogTransactionMonitoringController()
            : base("Log Transaction Monitoring")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            LogTransactionMonitoringModel mdl = new LogTransactionMonitoringModel();
            mdl.LogTransactionMonitoring = LogTransactionMonitoring();
            Model.AddModel(mdl);
        }

        public ActionResult ReloadGrid(string moduleID,string dateFrom, string dateTo, string function, string status)
        {
            if (!String.IsNullOrEmpty(dateFrom))
            {
                dateFrom = Convert.ToDateTime(dateFrom.Substring(4, 20)).ToString("yyyy-MM-dd");
            }
            if (!String.IsNullOrEmpty(dateTo))
            {
                dateTo = Convert.ToDateTime(dateTo.Substring(4, 20)).ToString("yyyy-MM-dd");
            }
            
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogTransactionMonitoringModel model = Model.GetModel<LogTransactionMonitoringModel>();
            model.LogTransactionMonitoring = db.Fetch<LogTransactionMonitoringMaster>("GetLogTransactionHeader", new object[] { moduleID, dateFrom, dateTo, function, status });
            db.Close();
            return PartialView("GridIndex", Model);
        }

        protected List<LogTransactionMonitoringMaster> LogTransactionMonitoring()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogTransactionMonitoringMaster> LogList = new List<LogTransactionMonitoringMaster>();
            var QueryLog = db.Query<LogTransactionMonitoringMaster>("GetLogTransactionHeader", new object[] { "", "", "", "", "" });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new LogTransactionMonitoringMaster()
                    {
                        LogIDH = q.LogIDH,
                        ModuleID = q.ModuleID,
                        FunctionID = q.FunctionID,
                        StartDate = q.StartDate,
                        EndDate = q.EndDate,
                        LogStatus = q.LogStatus
                    });
                }
            }
            db.Close();
            return LogList;
        }

        public ActionResult Back()
        {
            return RedirectToAction("Index", "LogTransactionMonitoring");
        }


        public ActionResult LogTransactionMonitoringDetails(int id)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            LogTransactionMonitoringModel modelTransaction = Model.GetModel<LogTransactionMonitoringModel>();
            List<LogTransactionMonitoringMaster> logList = new List<LogTransactionMonitoringMaster>();
            logList = db.Query<LogTransactionMonitoringMaster>("GetLogTransactionDetailsByLogIDH", new object[] { id }).ToList();

            if (logList.Any())
            {
                modelTransaction.LogTransactionMonitoring.Clear();
                foreach (var q in logList)
                {
                    modelTransaction.LogTransactionMonitoring.Add(new LogTransactionMonitoringMaster()
                   {
                       LogIDD = q.LogIDD,
                       SequenceNumber = q.SequenceNumber,
                       Type = q.Type,
                       Location = q.Location,
                       Message = q.Message
                   });
                }
            }
            db.Close();
            Model.AddModel(modelTransaction);
            return PartialView("GridIndexDetails", Model);
        }

        //protected List<LogTransactionMonitoringMaster> LogTransactionMonitoringDetails(int logIDH)
        //{
        //    //int id;
        //    //int.TryParse(logIDH, out id);
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    List<LogTransactionMonitoringMaster> LogList = new List<LogTransactionMonitoringMaster>();
        //    var QueryLog = db.Query<LogTransactionMonitoringMaster>("GetLogTransactionDetailsByLogIDH", new object[] { logIDH });

        //    if (QueryLog.Any())
        //    {
        //        foreach (var q in QueryLog)
        //        {
        //            LogList.Add(new LogTransactionMonitoringMaster()
        //            {
        //                LogIDD = q.LogIDD,
        //                SequenceNumber = q.SequenceNumber,
        //                Type = q.Type,
        //                Location = q.Location,
        //                Message = q.Message
        //            });
        //        }
        //    }
        //    db.Close();
        //    return LogList;
        //}

        public ActionResult IndexDetails(string logIDH)
        {
            //ViewData["LOGIDH"] = logIDH;
            //LogTransactionMonitoringDetailsModel modelDetails = new LogTransactionMonitoringDetailsModel();
            //modelDetails.LogTransactionMonitoringDetails = LogTransactionMonitoringDetails(Convert.ToInt32(logIDH));
            //Model.AddModel(modelDetails);

            //return View("IndexDetails", Model);

            //LogTransactionMonitoringDetailsModel modelDetails = new LogTransactionMonitoringDetailsModel();
            //modelDetails.LogTransactionMonitoringDetails = LogTransactionMonitoringDetails(Convert.ToInt32(logIDH));
            //Model.AddModel(modelDetails);

            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            LogTransactionMonitoringMaster logHeader = dbContext.SingleOrDefault<LogTransactionMonitoringMaster>("GetLogTransactionHeaderByLogIDH", new object[] { Convert.ToInt32(logIDH) });

            LogTransactionMonitoringMaster listRow = new LogTransactionMonitoringMaster();
            if (logHeader != null)
            {
                listRow.LogIDH = logHeader.LogIDH;
                listRow.ModuleID = logHeader.ModuleID;
                listRow.FunctionID = logHeader.FunctionID;
                listRow.StartDate = logHeader.StartDate;
                listRow.EndDate = logHeader.EndDate;
                listRow.LogStatus = logHeader.LogStatus;
            };
            Model.AddModel(listRow);
            return View("IndexDetails", Model);
        }
    }
}