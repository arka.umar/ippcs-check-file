﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;

using Portal.Models.ConsolidatedLPOP;
using Toyota.Common.Web.Credential;
//using Toyota.Common.Web.Notification;

namespace Portal.Controllers
{
    public class ConsolidatedLPOPController : BaseController
    {
        //
        // GET: /LPOPConsolidation/
        public ConsolidatedLPOPController()
            : base("Consolidated LPOP ")
        {
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            ConsolidatedLPOPModel mdl = new ConsolidatedLPOPModel();
            mdl.ConsolidatedLPOPs.Add(new ConsolidatedLPOPMaster()
            {

                ID = "201210",
                Production_Year = "2012",
                Production_Month = "October",
                LPOP_Type = "Tentative",
                PO_Number = "",
                Created_Time = DateTime.Now
                
            });
            mdl.ConsolidatedLPOPs.Add(new ConsolidatedLPOPMaster()
            {

                ID = "201210",
                Production_Year = "2012",
                Production_Month = "October",
                LPOP_Type = "Firm",
                PO_Number = "",
                Created_Time = DateTime.Now
            });
            mdl.ConsolidatedLPOPs.Add(new ConsolidatedLPOPMaster()
            {

                ID = "201211",
                Production_Year = "2012",
                Production_Month = "November",
                LPOP_Type = "Tentative",
                PO_Number = "",
                Created_Time = DateTime.Now
            });
            mdl.ConsolidatedLPOPs.Add(new ConsolidatedLPOPMaster()
            {

                ID = "201211",
                Production_Year = "2012",
                Production_Month = "November",
                LPOP_Type = "Firm",
                PO_Number = "",
                Created_Time = DateTime.Now
            });
            for (int i = 0; i < 50; i++)
            {

                /// dummy dummy detail detail
                mdl.ConsolidatedTNQCDetails.Add(new ConsolidatedTNQCDetail()
                {
                    D_ID = "FCST", /// sampel ID dimulai dari 201200 sama seperti header
                    Vers = "T",
                    Rev_No = "0001",
                    Comp_Code = "807B",
                    R_Plant_Code = "1",

                    Dock_Code = "18",
                    Supp_Code = "722S",
                    Ship_Code = "722S",
                    Ord_Type = "",
                    Pack_Month = "201210",
                    Car_Code = "272W",

                    Rex_Code = "",

                    Src = "1",

                    Part_No = "421080K01000",
                    Order_Type = "T",
                    Order_Lot_Cz = "500",
                    Kanban_Number = "M001",
                    Parts_Matching_Key = "",
                    AICO_Cept = "",
                    N_Volume = "11392",
                    AICO_Cept1 = "",
                    N1_Vol = "9600",
                    AICO_Cept2 = "",
                    N2_Vol = "12000",
                    AICO_Cept3 = "",
                    N3_Vol = "10368",
                    N_D01 = "480",
                    N_D02 = "480",
                    N_D03 = "240",
                    N_D04 = "",
                    N_D05 = "",
                    N_D06 = "",
                    N_D07 = "",
                    N_D08 = "",
                    N_D09 = "",
                    N_D10 = "",
                    N_D11 = "",
                    N_D12 = "",
                    N_D13 = "",
                    N_D14 = "",
                    N_D15 = "",
                    N_D16 = "",
                    N_D17 = "",
                    N_D18 = "",
                    N_D19 = "",
                    N_D20 = "",
                    N_D21 = "",
                    N_D22 = "",
                    N_D23 = "",
                    N_D24 = "",
                    N_D25 = "",
                    N_D26 = "",
                    N_D27 = "",
                    N_D28 = "",
                    N_D29 = "",
                    N_D30 = "",
                    N_D31 = "",
                    N1_D01 = "",
                    N1_D02 = "",
                    N1_D03 = "",
                    N1_D04 = "",
                    N1_D05 = "",
                    N1_D06 = "",
                    N1_D07 = "",
                    N1_D08 = "",
                    N1_D09 = "",
                    N1_D10 = "",
                    N1_D11 = "",
                    N1_D12 = "",
                    N1_D13 = "",
                    N1_D14 = "",
                    N1_D15 = "",
                    N1_D16 = "",
                    N1_D17 = "",
                    N1_D18 = "",
                    N1_D19 = "",
                    N1_D20 = "",
                    N1_D21 = "",
                    N1_D22 = "",
                    N1_D23 = "",
                    N1_D24 = "",
                    N1_D25 = "",
                    N1_D26 = "",
                    N1_D27 = "",
                    N1_D28 = "",
                    N1_D29 = "",
                    N1_D30 = "0",
                    N1_D31 = "0",
                    System_Date = "20121012",
                    Series = "",
                    Life_Cycle = "0",
                    BA_Sign = "",
                    BA_Quantity = "",
                    RU_Previous = "",
                    RU_Present = "",
                    Supplier_Name = "SIAM THAILAND MOTOR",
                    Part_Name = "Bushing Valve Guide",
                    Created_By = "",
                    Created_Date = "",
                    Changed_By = "",
                    Changed_Date = ""
                });
            }

            for (int i = 0; i < 50; i++)
            {

                /// dummy dummy detail detail
                mdl.ConsolidatedTNQCDetails.Add(new ConsolidatedTNQCDetail()
                {
                    D_ID = "FCST", /// sampel ID dimulai dari 201200 sama seperti header
                    Vers = "F",
                    Rev_No = "0001",
                    Comp_Code = "807B",
                    R_Plant_Code = "1",

                    Dock_Code = "18",
                    Supp_Code = "722S",
                    Ship_Code = "722S",
                    Ord_Type = "",
                    Pack_Month = "201210",
                    Car_Code = "272W",

                    Rex_Code = "",

                    Src = "1",

                    Part_No = "421080K01000",
                    Order_Type = "T",
                    Order_Lot_Cz = "500",
                    Kanban_Number = "M001",
                    Parts_Matching_Key = "",
                    AICO_Cept = "",
                    N_Volume = "11392",
                    AICO_Cept1 = "",
                    N1_Vol = "9600",
                    AICO_Cept2 = "",
                    N2_Vol = "12000",
                    AICO_Cept3 = "",
                    N3_Vol = "10368",
                    N_D01 = "480",
                    N_D02 = "480",
                    N_D03 = "240",
                    N_D04 = "",
                    N_D05 = "",
                    N_D06 = "",
                    N_D07 = "",
                    N_D08 = "",
                    N_D09 = "",
                    N_D10 = "",
                    N_D11 = "",
                    N_D12 = "",
                    N_D13 = "",
                    N_D14 = "",
                    N_D15 = "",
                    N_D16 = "",
                    N_D17 = "",
                    N_D18 = "",
                    N_D19 = "",
                    N_D20 = "",
                    N_D21 = "",
                    N_D22 = "",
                    N_D23 = "",
                    N_D24 = "",
                    N_D25 = "",
                    N_D26 = "",
                    N_D27 = "",
                    N_D28 = "",
                    N_D29 = "",
                    N_D30 = "",
                    N_D31 = "",
                    N1_D01 = "",
                    N1_D02 = "",
                    N1_D03 = "",
                    N1_D04 = "",
                    N1_D05 = "",
                    N1_D06 = "",
                    N1_D07 = "",
                    N1_D08 = "",
                    N1_D09 = "",
                    N1_D10 = "",
                    N1_D11 = "",
                    N1_D12 = "",
                    N1_D13 = "",
                    N1_D14 = "",
                    N1_D15 = "",
                    N1_D16 = "",
                    N1_D17 = "",
                    N1_D18 = "",
                    N1_D19 = "",
                    N1_D20 = "",
                    N1_D21 = "",
                    N1_D22 = "",
                    N1_D23 = "",
                    N1_D24 = "",
                    N1_D25 = "",
                    N1_D26 = "",
                    N1_D27 = "",
                    N1_D28 = "",
                    N1_D29 = "",
                    N1_D30 = "0",
                    N1_D31 = "0",
                    System_Date = "20121012",
                    Series = "",
                    Life_Cycle = "0",
                    BA_Sign = "",
                    BA_Quantity = "",
                    RU_Previous = "",
                    RU_Present = "",
                    Supplier_Name = "SIAM THAILAND MOTOR",
                    Part_Name = "Bushing Valve Guide",
                    Created_By = "",
                    Created_Date = "",
                    Changed_By = "",
                    Changed_Date = ""
                });
            }
            for (int i = 0; i < 50; i++)
            {

                /// dummy dummy detail detail
                mdl.ConsolidatedTNQCDetails.Add(new ConsolidatedTNQCDetail()
                {
                    D_ID = "FCST", /// sampel ID dimulai dari 201200 sama seperti header
                    Vers = "T",
                    Rev_No = "0001",
                    Comp_Code = "807B",
                    R_Plant_Code = "1",

                    Dock_Code = "18",
                    Supp_Code = "722S",
                    Ship_Code = "722S",
                    Ord_Type = "",
                    Pack_Month = "201211",
                    Car_Code = "272W",

                    Rex_Code = "",

                    Src = "1",

                    Part_No = "421080K01000",
                    Order_Type = "T",
                    Order_Lot_Cz = "500",
                    Kanban_Number = "M001",
                    Parts_Matching_Key = "",
                    AICO_Cept = "",
                    N_Volume = "11392",
                    AICO_Cept1 = "",
                    N1_Vol = "9600",
                    AICO_Cept2 = "",
                    N2_Vol = "12000",
                    AICO_Cept3 = "",
                    N3_Vol = "10368",
                    N_D01 = "480",
                    N_D02 = "480",
                    N_D03 = "240",
                    N_D04 = "",
                    N_D05 = "",
                    N_D06 = "",
                    N_D07 = "",
                    N_D08 = "",
                    N_D09 = "",
                    N_D10 = "",
                    N_D11 = "",
                    N_D12 = "",
                    N_D13 = "",
                    N_D14 = "",
                    N_D15 = "",
                    N_D16 = "",
                    N_D17 = "",
                    N_D18 = "",
                    N_D19 = "",
                    N_D20 = "",
                    N_D21 = "",
                    N_D22 = "",
                    N_D23 = "",
                    N_D24 = "",
                    N_D25 = "",
                    N_D26 = "",
                    N_D27 = "",
                    N_D28 = "",
                    N_D29 = "",
                    N_D30 = "",
                    N_D31 = "",
                    N1_D01 = "",
                    N1_D02 = "",
                    N1_D03 = "",
                    N1_D04 = "",
                    N1_D05 = "",
                    N1_D06 = "",
                    N1_D07 = "",
                    N1_D08 = "",
                    N1_D09 = "",
                    N1_D10 = "",
                    N1_D11 = "",
                    N1_D12 = "",
                    N1_D13 = "",
                    N1_D14 = "",
                    N1_D15 = "",
                    N1_D16 = "",
                    N1_D17 = "",
                    N1_D18 = "",
                    N1_D19 = "",
                    N1_D20 = "",
                    N1_D21 = "",
                    N1_D22 = "",
                    N1_D23 = "",
                    N1_D24 = "",
                    N1_D25 = "",
                    N1_D26 = "",
                    N1_D27 = "",
                    N1_D28 = "",
                    N1_D29 = "",
                    N1_D30 = "0",
                    N1_D31 = "0",
                    System_Date = "20121012",
                    Series = "",
                    Life_Cycle = "0",
                    BA_Sign = "",
                    BA_Quantity = "",
                    RU_Previous = "",
                    RU_Present = "",
                    Supplier_Name = "SIAM THAILAND MOTOR",
                    Part_Name = "Bushing Valve Guide",
                    Created_By = "",
                    Created_Date = "",
                    Changed_By = "",
                    Changed_Date = ""
                });
            }

            for (int i = 0; i < 50; i++)
            {

                /// dummy dummy detail detail
                mdl.ConsolidatedTNQCDetails.Add(new ConsolidatedTNQCDetail()
                {
                    D_ID = "FCST", /// sampel ID dimulai dari 201200 sama seperti header
                    Vers = "F",
                    Rev_No = "0001",
                    Comp_Code = "807B",
                    R_Plant_Code = "1",

                    Dock_Code = "18",
                    Supp_Code = "722S",
                    Ship_Code = "722S",
                    Ord_Type = "",
                    Pack_Month = "201211",
                    Car_Code = "272W",

                    Rex_Code = "",

                    Src = "1",

                    Part_No = "421080K01000",
                    Order_Type = "T",
                    Order_Lot_Cz = "500",
                    Kanban_Number = "M001",
                    Parts_Matching_Key = "",
                    AICO_Cept = "",
                    N_Volume = "11392",
                    AICO_Cept1 = "",
                    N1_Vol = "9600",
                    AICO_Cept2 = "",
                    N2_Vol = "12000",
                    AICO_Cept3 = "",
                    N3_Vol = "10368",
                    N_D01 = "480",
                    N_D02 = "480",
                    N_D03 = "240",
                    N_D04 = "",
                    N_D05 = "",
                    N_D06 = "",
                    N_D07 = "",
                    N_D08 = "",
                    N_D09 = "",
                    N_D10 = "",
                    N_D11 = "",
                    N_D12 = "",
                    N_D13 = "",
                    N_D14 = "",
                    N_D15 = "",
                    N_D16 = "",
                    N_D17 = "",
                    N_D18 = "",
                    N_D19 = "",
                    N_D20 = "",
                    N_D21 = "",
                    N_D22 = "",
                    N_D23 = "",
                    N_D24 = "",
                    N_D25 = "",
                    N_D26 = "",
                    N_D27 = "",
                    N_D28 = "",
                    N_D29 = "",
                    N_D30 = "",
                    N_D31 = "",
                    N1_D01 = "",
                    N1_D02 = "",
                    N1_D03 = "",
                    N1_D04 = "",
                    N1_D05 = "",
                    N1_D06 = "",
                    N1_D07 = "",
                    N1_D08 = "",
                    N1_D09 = "",
                    N1_D10 = "",
                    N1_D11 = "",
                    N1_D12 = "",
                    N1_D13 = "",
                    N1_D14 = "",
                    N1_D15 = "",
                    N1_D16 = "",
                    N1_D17 = "",
                    N1_D18 = "",
                    N1_D19 = "",
                    N1_D20 = "",
                    N1_D21 = "",
                    N1_D22 = "",
                    N1_D23 = "",
                    N1_D24 = "",
                    N1_D25 = "",
                    N1_D26 = "",
                    N1_D27 = "",
                    N1_D28 = "",
                    N1_D29 = "",
                    N1_D30 = "0",
                    N1_D31 = "0",
                    System_Date = "20121012",
                    Series = "",
                    Life_Cycle = "0",
                    BA_Sign = "",
                    BA_Quantity = "",
                    RU_Previous = "",
                    RU_Present = "",
                    Supplier_Name = "SIAM THAILAND MOTOR",
                    Part_Name = "Bushing Valve Guide",
                    Created_By = "",
                    Created_Date = "",
                    Changed_By = "",
                    Changed_Date = ""
                });
            }
                for (int i = 0; i < 5; i++)
                {

                    /// dummy dummy detail detail
                    mdl.MasterCompletenessResults.Add(new MasterCompletenessResult()
                    {
                       
                             RecordID	="222" + i,		
                             PONumber ="",								
                             POItemNo	="",			
                             DockCode	="43",						
                             SupplierCode	="514" +i,							
                             MaterialNo		="861600K230",				
                             ProdPurposeCD	="D",						
                             SourceType		="1",				
                             PartColorSFX		="00",				
                             MaterialDescription	="SPEAKER ASSY, RADIO",
                             ProdMonth ="201210",	
                             Qty_n			="54792",
                             Qty_n1			="45768",	
                             Qty_n2			="54674",	
                             Qty_n3				="52388",
                             PlantCode		="4000",				
                             SlocCode			="1500",		 
                             PaymentMethodCode	="T",												
                             PaymentTermCode	="ZD00",										
                             MatPrice			="",		
                             ValStats ="NG",
                             Message ="Material Price doesn't exist on Transaction Price Master"
                    });
                }
            
            
            
            
        
            Model.AddModel(mdl);
        }
        public ActionResult ConsolidatedMaster()
        {
            return PartialView("ConsolidatedMaster", Model);
        }

        public ActionResult SearchConsolidatedDetail(string Name,string Type)
        {

            ViewData["Name"] = Name;
            ViewData["LPOP_Type"] = Type;
            ConsolidatedLPOPModel model = Model.GetModel<ConsolidatedLPOPModel>(); 
            var LPOPDetail = model.ConsolidatedTNQCDetails.Where(p => p.Pack_Month == Name && p.Vers == Type.Substring(0, 1)).ToList<ConsolidatedTNQCDetail>();
            return PartialView("ConsolidatedTNQCDetail", LPOPDetail);
            

        }

        // Invoke the Changed event; called whenever list changes
        protected virtual void OnAuthorizationValidate(object sender, User e)
        {
        }
        
    }

}
