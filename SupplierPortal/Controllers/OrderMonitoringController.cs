﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;

using Portal.Models.OrderMonitoring;

namespace Portal.Controllers
{
    public class OrderMonitoringController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public OrderMonitoringController()
             : base("Order Monitoring")
         {
             PageLayout.UseMessageBoard = true;
         }
        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;
            OrderMonitoringModel mdl = new OrderMonitoringModel();
            Model.AddModel(mdl);

            ViewData["CreationDate"] = DateTime.Now.ToString("dd.MM.yyyy");
        }

        #region Order Monitoring controller
        #region View controller
        public ActionResult OrderMonitoringDetailPartial()
        {
            #region Required model in partial page : for OrderMonitoringDetailPartial
            OrderMonitoringModel model = Model.GetModel<OrderMonitoringModel>();
            model.OrderMonitorings = GetAllOrderMonitoring(String.IsNullOrEmpty(Request.Params["OrderCreationDate"]) == true ? DateTime.Now.ToString("dd.MM.yyyy") : Request.Params["OrderCreationDate"]);
            #endregion

            return PartialView("OrderMonitoringDetailPartial", Model);
        }

        public ActionResult OrderMonitoringControlPartial()
        {
            #region Required model in partial page : for OrderMonitoringDetailPartial
            OrderMonitoringModel model = Model.GetModel<OrderMonitoringModel>();
            model.OrderMonitorings = GetAllOrderMonitoringControl();
            #endregion

            return PartialView("OrderMonitoringControlPartial", Model);
        }

        public ActionResult PopupHeaderOrderMonitoringDetail() //for refresh
        {
            return PartialView("PartialHeaderOrderMonitoringDetail", Model);
        }
        public ActionResult PopupGridOrderMonitoringDetail()
        {
            string ProcessType = Request.Params["ProcessType"];
            string OrderCreationDate = Request.Params["OrderCreationDate"];
            string PlantName = Request.Params["PlantName"];

            ViewData["LogDetailData"] = LogDetailFillGrid(ProcessType, OrderCreationDate, PlantName);
            return PartialView("PartialGridOrderMonitoringDetail", ViewData["LogDetailData"]);
        }
        public ContentResult UpdateKCI(string StatusCode)
        {
            string Result = "";
            string user = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                Result = db.ExecuteScalar<string>("UpdateKCIStatus", new object[] { StatusCode, user });
            }
            catch (Exception exc) { throw exc; }
            finally { 
                db.Close(); 
            }
            return Content(Result);
        }
        public ContentResult GetStatusKCI()
        {
            string Result = "";
            IDBContext db = DbContext;
            try
            {
                Result = db.ExecuteScalar<string>("GetStatusKCI");
            }
            catch (Exception exc) { throw exc; }
            finally
            {
                db.Close();
            }
            return Content(Result);
        }
        #endregion

        #region Database controller
        private List<OrderMonitoring> GetAllOrderMonitoring(String OrderCreationDate)
        {
            List<OrderMonitoring> orderMonitoringModel = new List<OrderMonitoring>();
            IDBContext db = DbContext;

            orderMonitoringModel = db.Fetch<OrderMonitoring>("GetAllOrderMonitoring", new object[] { 
                (OrderCreationDate == "" || OrderCreationDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderCreationDate,"dd.MM.yyyy",null)
            }).ToList();
            db.Close();

            return orderMonitoringModel;
        }

        private List<OrderMonitoring> GetAllOrderMonitoringControl()
        {
            List<OrderMonitoring> orderMonitoringModel = new List<OrderMonitoring>();
            IDBContext db = DbContext;

            orderMonitoringModel = db.Fetch<OrderMonitoring>("OrderMonitoringGetControlData").ToList();
            db.Close();

            return orderMonitoringModel;
        }

        private List<OrderMonitoringDetail> LogDetailFillGrid(String ProcessType,
            String OrderCreationDate,
            String PlantName)
        {
            List<OrderMonitoringDetail> LogDetailList = new List<OrderMonitoringDetail>();
            IDBContext db = DbContext;
            LogDetailList = db.Fetch<OrderMonitoringDetail>("GetLogDetail", new object[] { 
                ProcessType,
                (OrderCreationDate == "" || OrderCreationDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderCreationDate,"dd.MM.yyyy",null),
                PlantName
            });
            db.Close();

            return LogDetailList;
        }
        #endregion
        #endregion

        #region ControlTab
        public ContentResult ControlAction(string PlantName, string Flag)
        {
            string result = string.Empty;

            char PlantNm = PlantName[PlantName.Length - 1];

            IDBContext db = DbContext;
            db.Execute("OrderMonitoringControlAction", new Object[] { PlantNm, Flag, AuthorizedUser.Username });
            db.Close();

            return Content(result);
        }
        #endregion


        public ContentResult ControlActionRelease(string PlantName, string pickedDate)
        {
            string Result = "";
            string user = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                Result = db.ExecuteScalar<string>("orderMonitorRelease", new object[] { PlantName, pickedDate });
            }
            catch (Exception exc) { throw exc; }
            finally
            {
                db.Close();
            }
            return Content(Result);
        }


        public ContentResult CheckKCIProcess()
        {
            string Result = "";
            string user = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                Result = db.ExecuteScalar<string>("orderMonitorCheckKCIProcess");
            }
            catch (Exception exc) { throw exc; }
            finally
            {
                db.Close();
            }
            return Content(Result);
        }


    }
}
