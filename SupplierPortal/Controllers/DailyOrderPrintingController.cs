using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Management;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Util.Converter;

using Portal.Models.DailyOrder;
using Portal.Models.DailyOrderPrinting;
using Portal.Models.Supplier;
using Portal.Models.Dock;
using Portal.Models.Globals;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.FTP;
using Telerik.Reporting.Processing;
using Toyota.Common.Web.Credential;
using System.IO;
using Toyota.Common.Web.Excel;
using Toyota.Common.Util.Text;

using System.Drawing;
using System.Drawing.Imaging;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using ThoughtWorks.QRCode.Codec.Util;


namespace Portal.Controllers
{
    public class DailyOrderPrintingController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public DailyOrderPrintingController()
            : base("Order Printing")
        {
        }

        #region Model properties
        List<Supplier> _orderInquirySupplierModel = null;
        private List<Supplier> _OrderInquirySupplierModel
        {
            get
            {
                if (_orderInquirySupplierModel == null)
                    _orderInquirySupplierModel = new List<Supplier>();

                return _orderInquirySupplierModel;
            }
            set
            {
                _orderInquirySupplierModel = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }

        #endregion

        protected override void StartUp()
        {
        }
        protected override void Init()
        {
            DailyOrderPrintingModel mdl = new DailyOrderPrintingModel();
            DailyOrderPrintingDetailModel mdl2 = new DailyOrderPrintingDetailModel();
            //mdl.RegularOrders = GetAllDailyOrder("", "", "", "", DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy"), DateTime.Now.ToString("dd.MM.yyyy"), "1", "");
            //mdl.EmergencyOrders = GetAllDailyOrder("", "", "", "", DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy"), DateTime.Now.ToString("dd.MM.yyyy"), "2", "");
            //mdl.ComponentPartOrders = GetAllDailyOrder("", "", "", "", DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy"), DateTime.Now.ToString("dd.MM.yyyy"), "3", "");
            //mdl.JunbikiOrders = GetAllDailyOrder("", "", "", "", DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy"), DateTime.Now.ToString("dd.MM.yyyy"), "4", "");
            Model.AddModel(mdl);
            Model.AddModel(mdl2);
            ViewData["Date"] = DateTime.Now.ToString("dd.MM.yyyy");
            ViewData["onlyGetPrinterList"] = "true";
            //Session["WSFTPPath"] = System.Configuration.ConfigurationManager.AppSettings["WSFTPPath"];
            ViewData["DateHeaderSchedule"] = DateTime.Now.ToString("dd.MM.yyyy");
        }

        #region Order Printing controller
        #region View controller
        public ActionResult PartialHeaderOrderPrinting()
        {
            #region Required model in partial page : for OrderInquiryHeaderPartial
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrinting","OPHSupplierOption");

            _OrderInquirySupplierModel = suppliers;
            //_OrderInquirySupplierModel = GetAllSupplier();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            _OrderInquiryDockModel = DockCodes;

            Model.AddModel(_OrderInquirySupplierModel);
            Model.AddModel(_OrderInquiryDockModel);
            #endregion

            return PartialView("OrderPrintingHeaderPartial", Model);
        }
        public ActionResult PartialHeaderPrintSetup()
        {
            //#region Required model in partial page : for OrderPrintSetupPartial
            //SelectQuery selectQuery = new SelectQuery("Win32_PrinterDriver");
            //ManagementObjectSearcher searcher = new ManagementObjectSearcher(selectQuery);

            //foreach (ManagementObject printerDriver in searcher.Get())
            //{
            //    // Your code here.
            //}
            //#endregion

            return PartialView("OrderPrintSetupPartial", Model);
        }
        
        private FileContentResult ExportToPDF(Telerik.Reporting.Report reportToExport)
        {
            ReportProcessor reportProcessor = new ReportProcessor();
            Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
            instanceReportSource.ReportDocument = reportToExport;
            RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

            string fileName = "test-" + result.DocumentName + "." + result.Extension;

            FileContentResult fileresult = new FileContentResult(result.DocumentBytes, result.MimeType);
            fileresult.FileDownloadName = fileName;

            return fileresult;
        }

        public ActionResult PartialHeaderDockLookupGridOrderPrinting()
        {
            #region Required model in partial page : for OIHDockOption GridLookup
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            TempData["GridName"] = "OPHDockOption";
           // _OrderInquiryDockModel = DockCodes;
            _OrderInquiryDockModel = GetAllDock(); ;
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }
        public ActionResult PartialHeaderSupplierLookupGridOrderPrinting()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrinting", "OPHSupplierOption");
            TempData["GridName"] = "OPHSupplierOption";
            //_OrderInquirySupplierModel = suppliers;
            _OrderInquirySupplierModel = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquirySupplierModel);
        }
        public ActionResult PartialOrderPrintSchedule(string printingDateFrom, string printingDateTo, string Supplier, string DockCode, string OrderNo, string OrderType)
        {
            ViewData["DateHeaderSchedule"] = printingDateFrom + " To " + printingDateTo;
            DateTime PrintingDateFrom = DateTime.ParseExact(printingDateFrom, "dd.MM.yyyy", null);
            DateTime PrintingDateTo = DateTime.ParseExact(printingDateTo, "dd.MM.yyyy", null);
            string SupplierCode = "";
            string SupplierPlant = "";

            if (Supplier != "" && Supplier != null)
            {
                string[] SupplierPlantCode = Supplier.Split('-');
                SupplierCode = SupplierPlantCode[0].Replace(" ","");
                SupplierPlant = SupplierPlantCode[1].Replace(" ", "");
            }
            #region Required model in partial page : for OrderPrintingSchedulePartial Partial View
            List<DailyOrderPrintingSchedule> DailyOrderPrintingSchedules = new List<DailyOrderPrintingSchedule>();
            IDBContext db = DbContext;
            DailyOrderPrintingSchedules = db.Fetch<DailyOrderPrintingSchedule>("GetDailyOrderPrintSchedule",
                    new object[] { PrintingDateFrom, PrintingDateTo, SupplierCode, SupplierPlant, DockCode, OrderNo, OrderType }).ToList();
            db.Close();
            #endregion
            return PartialView("OrderPrintingSchedulePartial", DailyOrderPrintingSchedules);
        }

        [Obsolete]
        public void PrintReport(string DockCode, string SupplierCode, string SupplierPlantCode, string OrderNo)
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DailyOrder\Rep_SupplierManifest.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlDataSource.SelectCommand = @"SELECT ARRIVAL_PLAN_DT=isnull(m.ARRIVAL_PLAN_DT,'1900-01-01')
	                                         , ARRIVAL_ACTUAL_DT=isnull(m.ARRIVAL_ACTUAL_DT,'1900-01-01')
	                                         , m.DOCK_CD
	                                         , ROUTE=m.P_LANE_CD
	                                         , RATE=m.P_LANE_SEQ
	                                         , m.MANIFEST_NO
	                                         , m.SUPPLIER_PLANT
	                                         , m.RCV_PLANT_CD
	                                         , PRINT_FLAG=IsNull(m.PRINT_FLAG,0)
	                                         , s.SUPPLIER_NAME
	                                         , s.SUPPLIER_CODE
	                                         , s.SUPPLIER_PLANT_CD
	                                         , m.ORDER_NO
	                                         , m.TOTAL_ITEM
	                                         , m.TOTAL_QTY
	                                         , p.ORDER_QTY
	                                         , p.ORDER_PLAN_QTY_LOT
	                                         , p.ITEM_NO
	                                         , p.PART_NO
	                                         , p.PART_NAME
                                             , p.PACKAGE_TYPE
	                                         , p.KANBAN_NO
	                                         , PCS_PER_CONTAINER = p.QTY_PER_CONTAINER
	                                         , SUPPLIER_ROUTE = m.SUB_ROUTE_CD
	                                         , SUPPLIER_RATE = m.SUB_ROUTE_SEQ
	                                         , m.FST_X_DOCK_ROUTE_GRP_CD
                                             , m.fst_x_dock_seq_no
                                             , fst_x_dock_arrival_dt=isnull(m.fst_x_dock_arrival_dt,'1900-01-01')
                                             , fst_x_dock_depart_dt=isnull(m.fst_x_dock_depart_dt,'1900-01-01')
                                             , m.SND_X_DOCK_ROUTE_GRP_CD
                                             , m.snd_x_dock_seq_no
                                             , snd_x_dock_arrival_dt=isnull(m.snd_x_dock_arrival_dt,'1900-01-01')
                                             , snd_x_dock_depart_dt=isnull(m.snd_x_dock_depart_dt,'1900-01-01')
	                                         , m.CONVEYANCE_ROUTE
	                                         , SHIPPING_DT=isnull(m.SHIPPING_DT,'1900-01-01')
                                       FROM
                                          TB_R_DAILY_ORDER_MANIFEST m
                                          INNER JOIN TB_R_DAILY_ORDER_PART p
                                            ON p.MANIFEST_NO = m.MANIFEST_NO
                                          INNER JOIN TB_M_SUPPLIER s
                                            ON s.SUPPLIER_CODE = m.SUPPLIER_CD AND s.SUPPLIER_PLANT_CD = m.SUPPLIER_PLANT
                                        WHERE
                                          m.ORDER_NO ='" + OrderNo + @"' AND
                                          m.DOCK_CD = '" + DockCode + @"' AND
                                          m.SUPPLIER_CD = '" + SupplierCode + @"' AND
                                          m.SUPPLIER_PLANT = '" + SupplierPlantCode + @"'
                                        ORDER BY
                                          m.SUPPLIER_CD
                                        , m.SUPPLIER_PLANT
                                        , ARRIVAL_PLAN_DT";

            Telerik.Reporting.IReportDocument myReport = rpt;

            // Obtain the settings of the default printer
            System.Drawing.Printing.PrinterSettings printerSettings
                = new System.Drawing.Printing.PrinterSettings();
            //printerSettings.PrinterName = "S1-ISTD-Project";
            printerSettings.PrinterName = "PDF reDirect v2";
            //printerSettings.DefaultPageSettings.PaperSize.Kind = System.Drawing.Printing.PaperKind.Letter;
            //printerSettings.DefaultPageSettings.PaperSize.PaperName = "Letter";
            //printerSettings.PrintToFile = true;

            // The standard print controller comes with no UI
            System.Drawing.Printing.PrintController standardPrintController =
                new System.Drawing.Printing.StandardPrintController();

            // Print the report using the custom print controller
            Telerik.Reporting.Processing.ReportProcessor reportProcessor
                = new Telerik.Reporting.Processing.ReportProcessor();

            reportProcessor.PrintController = standardPrintController;

            Telerik.Reporting.InstanceReportSource instanceReportSource =
                new Telerik.Reporting.InstanceReportSource();

            instanceReportSource.ReportDocument = myReport;

            ViewBag["report"] = myReport;

            reportProcessor.PrintReport(instanceReportSource, printerSettings);
        }

        public ContentResult SavePrinterSetup(string PrinterNameManifest, string PrinterNameOtherDocuments, string PaperType)
        {
            IDBContext db = DbContext;
            db.Execute("InsertDailyOrderPrinterSetup", new object[] { AuthorizedUser.Username, PrinterNameManifest, PrinterNameOtherDocuments, PaperType });

            db.Close();
            return Content("Sukses");
        }

        public JsonResult GetPrinterSetup()
        {
            PrinterInfo printerInfo = new PrinterInfo();

            IDBContext db = DbContext;
            try
            {
                printerInfo = db.SingleOrDefault<PrinterInfo>("GetDailyOrderPrinterSetup", new object[] { AuthorizedUser.Username });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return new JsonResult
            {
                Data = new
                {
                    ManifestPrinterName = printerInfo.MANIFEST_PRINTER_NAME,
                    OtherPrinterName = printerInfo.OTHER_PRINTER_NAME,
                    PaperType = printerInfo.PAPER_TYPE
                }
            };
        }
        #endregion

        #region Common controller
        private String GetGeneralProductionMonth(String productionMonth)
        {
            Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
            monthDictionary.Add("01", "January");
            monthDictionary.Add("02", "February");
            monthDictionary.Add("03", "March");
            monthDictionary.Add("04", "April");
            monthDictionary.Add("05", "May");
            monthDictionary.Add("06", "June");
            monthDictionary.Add("07", "July");
            monthDictionary.Add("08", "August");
            monthDictionary.Add("09", "September");
            monthDictionary.Add("10", "October");
            monthDictionary.Add("11", "November");
            monthDictionary.Add("12", "December");

            String generalProductionMonth = "";

            if (!String.IsNullOrEmpty(productionMonth))
            {
                String year = productionMonth.Substring(0, 4);
                String month = productionMonth.Substring(productionMonth.Length - 2, 2);

                generalProductionMonth = monthDictionary[month] + " " + year;
            }

            return generalProductionMonth;
        }
        #endregion

        #region Database controller
        private List<Supplier> GetAllSupplier()
        {
            List<Supplier> orderInquirySupplierModel = new List<Supplier>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<Supplier>("GetAllSupplierGrid", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }
        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            IDBContext db = DbContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();
            return orderInquiryDockModel;
        }
        private List<DailyOrderPrinting> GetAllDailyOrderPrinting(String supplier, 
                                String dockCode, 
                                String orderNo,
                                String printDateFrom,
                                String printDateTo,
                                String timeFrom,
                                String timeTo,
                                String OrderType,
                                String printFlag)
        {
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<DailyOrderPrinting> dailyOrderModel = new List<DailyOrderPrinting>();
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrinting","OPHSupplierOption");
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            string supplierCode = "";
            string supplierPlantCode = "";
            if (supplier != "" && supplier != null)
            {
                string[] Supplier = supplier.Split('-');
                supplierCode = Supplier[0].Replace(" ", "");
                supplierPlantCode = Supplier[1].Replace(" ", "");
            }

            printFlag = (printFlag == null) ? "0" : printFlag;

            DateTime timefrom = (timeFrom == "" || timeFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null)  : DateTime.ParseExact(printDateFrom + " " + timeFrom + ":00","dd.MM.yyyy HH:mm",null);
            DateTime timeto =  (timeTo == "" || timeTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(printDateTo + " " + timeTo + ":59","dd.MM.yyyy HH:mm",null);

            IDBContext db = DbContext;
            dailyOrderModel = db.Fetch<DailyOrderPrinting>("GetAllDailyOrderPrinting", new object[] { 
                supplierCode == null ? "" : supplierCode.Trim(), 
                supplierPlantCode == null ? "" : supplierPlantCode.Trim(),
                dockCode == null ? "" : dockCode,
                orderNo == null ? "" : orderNo,
                timefrom,
                timeto,
                OrderType,
                SupplierCodeLDAP,
                printFlag,
                DockCodeLDAP
            });
            db.Close();

            return dailyOrderModel;
        }
        private void UpdatePrintFlag(string ManifestNos)
        {
            IDBContext db = DbContext;
            db.Execute("UpdateDailyOrderPrintFlag", new object[] { ManifestNos, AuthorizedUser.Username });
            db.Close();
        }
        #endregion
        #endregion

        #region Order Printing Detail controller
        #region View controller
        public ActionResult PopupHeaderDataPartDetail()
        {
            string manifest = Request.Params["ManifestNo"];
            return PartialView("OrderPrintingHeaderPartPartial", ViewData["ManifestNo"]);
        }
        public ActionResult PopupDataPartDetail()
        {
            string manifestNo = Request.Params["ManifestNo"];
            string problemFlag = Request.Params["ProblemFlag"];
            ViewData["partDailyData"] = DataPartOrderFillGrid(manifestNo, problemFlag);
            return PartialView("OrderPrintingDetailPartPartial", ViewData["partDailyData"]);
        }

        //ucup
        public ActionResult PopupHeaderDataPartDetailDetail()
        {
            return PartialView("OrderPrintingHeaderPartPartialDetail");
        }

        public ActionResult PopupDataPartDetailDetail()
        {
            string ORDER_NO = Request.Params["ORDER_NO"];
            string PART_NO = Request.Params["PART_NO"];
            string KANBAN_NO = Request.Params["KANBAN_NO"];
            DailyOrderPrintingDetailModel mdl2 = new DailyOrderPrintingDetailModel();
            mdl2.orderListData = getOrderListDetail(ORDER_NO, PART_NO, KANBAN_NO);
            Model.AddModel(mdl2);
            return PartialView("OrderPrintingDetailPartPartialDetail", Model);
        }


        private List<DailyOrderPrintingDetail> getOrderListDetail(string ORDER_NO, string PART_NO, string KANBAN_NO)
        {
            List<DailyOrderPrintingDetail> listKanban = new List<DailyOrderPrintingDetail>();
            IDBContext db = this.DbContext;
                listKanban = db.Fetch<DailyOrderPrintingDetail>("DailyOrderPrintingGetKanban", new object[] { 
                    ORDER_NO,
                    PART_NO,
                    KANBAN_NO
                });
            return listKanban;
        }
        //end of ucup
        #endregion

        #region Database controller
        protected List<PartDetailManifest> DataPartOrderFillGrid(string manifestNo, string problemFlag)
        {
            List<PartDetailManifest> getPartDetailManifest = new List<PartDetailManifest>();
            IDBContext db = DbContext;
            getPartDetailManifest = db.Query<PartDetailManifest>("GetPartDetailManifest", new object[] { manifestNo, problemFlag }).ToList<PartDetailManifest>();

            db.Close();
            return getPartDetailManifest;
        }
        
        #endregion
        #endregion

        #region Tab controller
        #region View controller
        public ActionResult PartialDetailOrderPrintingEmergencyOrder()
        {
            #region Required model in partial page : for OrderPrintingEmergencyOrderDetailPartial
            DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
            model.EmergencyOrders = GetAllDailyOrderPrinting(Request.Params["Supplier"],
                                                Request.Params["DockCode"],
                                                Request.Params["OrderNo"],
                                                Request.Params["PrintDateFrom"],
                                                Request.Params["PrintDateTo"],
                                                Request.Params["TimeFrom"],
                                                Request.Params["TimeTo"],
                                                "2",
                                                Request.Params["PrintFlag"]);
            #endregion

            return PartialView("OrderPrintingEmergencyOrderDetailPartial", Model);
        }
        public ActionResult PartialDetailOrderPrintingDailyOrder()
        {
            string search = Request.Params["search"];
            if (search == "Y")
            {
                #region Required model in partial page : for OrderPrintingDailyOrderDetailPartial
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
                model.RegularOrders = GetAllDailyOrderPrinting(Request.Params["Supplier"],
                                                    Request.Params["DockCode"],
                                                    Request.Params["OrderNo"],
                                                    Request.Params["PrintDateFrom"],
                                                    Request.Params["PrintDateTo"],
                                                    Request.Params["TimeFrom"],
                                                    Request.Params["TimeTo"],
                                                    "1",
                                                    Request.Params["PrintFlag"]);
                #endregion
            }
            else
            {
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
            }

            return PartialView("OrderPrintingDailyOrderDetailPartial", Model);
        }
        public ActionResult PartialDetailOrderPrintingComponentPartOrder()
        {
            string search = Request.Params["search"];
            if (search == "Y")
            {
                #region Required model in partial page : for OrderPrintingCPOOrderDetailPartial
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
                model.ComponentPartOrders = GetAllDailyOrderPrinting(Request.Params["Supplier"],
                                                    Request.Params["DockCode"],
                                                    Request.Params["OrderNo"],
                                                    Request.Params["PrintDateFrom"],
                                                    Request.Params["PrintDateTo"],
                                                    Request.Params["TimeFrom"],
                                                    Request.Params["TimeTo"],
                                                   "99",
                                                   Request.Params["PrintFlag"]);
                #endregion
            }
            else
            {
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
            }

            return PartialView("OrderPrintingComponentPartOrderDetailPartial", Model);
        }
        public ActionResult PartialDetailOrderPrintingJunbikiOrder()
        {
            string search = Request.Params["search"];
            if (search == "Y")
            {
                #region Required model in partial page : for OrderPrintingJunbikiOrderDetailPartial
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
                model.JunbikiOrders = GetAllDailyOrderPrinting(Request.Params["Supplier"],
                                                    Request.Params["DockCode"],
                                                    Request.Params["OrderNo"],
                                                    Request.Params["PrintDateFrom"],
                                                    Request.Params["PrintDateTo"],
                                                    Request.Params["TimeFrom"],
                                                    Request.Params["TimeTo"],
                                                   "4",
                                                   Request.Params["PrintFlag"]);
                #endregion
            }
            else
            {
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
            }

            return PartialView("OrderPrintingJunbikiOrderDetailPartial", Model);
        }
        public ActionResult PartialDetailOrderPrintingProblemPartOrder()
        {
            string search = Request.Params["search"];
            if (search == "Y")
            {
                #region Required model in partial page : for OrderPrintingProblemPartOrderDetailPartial
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
                model.ProblemPartOrders = GetAllDailyOrderPrinting(Request.Params["Supplier"],
                                                    Request.Params["DockCode"],
                                                    Request.Params["OrderNo"],
                                                    Request.Params["PrintDateFrom"],
                                                    Request.Params["PrintDateTo"],
                                                    Request.Params["TimeFrom"],
                                                    Request.Params["TimeTo"],
                                                    "5",
                                                    Request.Params["PrintFlag"]);
                #endregion
            }
            else
            {
                DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
            }

            return PartialView("OrderPrintingProblemPartOrderDetailPartial", Model);
        }
        public ActionResult PartialDetailOrderPrintingServicePartExport()
        {
            #region Required model in partial page : for OrderPrintingServicePartExportDetailPartial
            DailyOrderPrintingModel model = Model.GetModel<DailyOrderPrintingModel>();
            model.ServicePartExport = GetAllDailyOrderPrinting(Request.Params["Supplier"],
                                                Request.Params["DockCode"],
                                                Request.Params["OrderNo"],
                                                Request.Params["PrintDateFrom"],
                                                Request.Params["PrintDateTo"],
                                                Request.Params["TimeFrom"],
                                                Request.Params["TimeTo"],
                                                "3",
                                                Request.Params["PrintFlag"]);
            #endregion

            return PartialView("OrderPrintingServicePartExportDetailPartial", Model);
        }
        #endregion
        #endregion

        //print kanban baru
        public JsonResult PrintKanbanWeb()
        {

            string message = "OK";
            string ORDER_NO = Request.Params["ORDER_NO"];
            string PART_NO = Request.Params["PART_NO"];
            string KANBAN_NO = Request.Params["KANBAN_NO"];
            string KBN_ID = Request.Params["KBN_ID"];
            string[] x = KBN_ID.Split(';');
            //List<DailyOrderPrintingDetail> listOrder = new List<DailyOrderPrintingDetail>();
            //try
            //{
            //    IDBContext db = DbContext;
            //    listOrder = db.Fetch<DailyOrderPrintingDetail>("DailyOrderPrintingGetKanban", new object[] { "2014060201", "851430K01000", "2375" });
            //    message = "Data successfuly loaded";
            //}
            //catch (Exception e)
            //{
            //    message = e.Message;
            //}

            foreach (string KANBAN_ID in x)
            {
                //Console.WriteLine(word);
                try
                {
                    QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                    Image image;

                    image = qrCodeEncoder.Encode(KANBAN_ID);
                    //Console.WriteLine("Creating file " + dr2["KANBAN_ID"].ToString()+".jpg");
                    image.Save("D:/QRCODE/" + KANBAN_ID + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.Message);
                    message = e.Message;
                }

            }
            Telerik.Reporting.ReportParameterCollection reportParameters = new Telerik.Reporting.ReportParameterCollection();
            string reportPath = @"\Report\DailyOrder\Rep_ManifestKanban_Download_new.trdx";
            string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameter.Add("@fromWeb", System.Data.DbType.String, "1");
            parameter.Add("@OrderNo", System.Data.DbType.String, ORDER_NO);
            parameter.Add("@PartNo", System.Data.DbType.String, PART_NO);
            parameter.Add("@KanbanNo", System.Data.DbType.String, KANBAN_NO);
            parameter.Add("@KanbanId", System.Data.DbType.String, KBN_ID);
            reportParameters.Add("QRpath", Telerik.Reporting.ReportParameterType.String, "D:/QRCODE/");
            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameter);
            rpt.ReportParameters.AddRange(reportParameters);


            //return message;
            string url = SavePDFReportToFTP(rpt, Guid.NewGuid().ToString()+"_"+ORDER_NO.Trim(' ')+ "_2", "PrintKanban" + "/Kanban", "1", "");

            string[] array1 = Directory.GetFiles(@"" + "D:/QRCODE/");
            foreach (string name in array1)
            {
                Console.WriteLine("Deleting file : " + name);
                System.IO.File.Delete(name);
            }
            return Json(new { url = url }, JsonRequestBehavior.AllowGet);
        }

        private string filePath = "";
        private string localFilePath = "/Content/DailyOrderPrinting/tmp/";
        private string SavePDFReportToFTP(Telerik.Reporting.Report rpt, string FileName, string FolderName, string OrderType, string UploadFilePath = "")
        {
            //string LocalFileName = FileName + "-" + DateTime.Now.Ticks.ToString() + ".pdf";
            string LocalFileName = FileName + ".pdf";
            string ContextFTP = "";
            FileName = FileName + ".pdf";
            
            FTPUpload vFtp = new FTPUpload();
            switch (OrderType)
            {
                case "1":
                    filePath = vFtp.Setting.FtpPath("DailyOrderPrinting");
                    ContextFTP = "dailyorder";
                    break;
                case "2":
                    filePath = vFtp.Setting.FtpPath("EmergencyOrder");
                    ContextFTP = "emergencyorder";
                    break;
                case "3":
                    filePath = vFtp.Setting.FtpPath("CPOOrder");
                    ContextFTP = "cpoorder";
                    break;
                case "4":
                    filePath = vFtp.Setting.FtpPath("JunbikiOrder");
                    ContextFTP = "junbikiorder";
                    break;
                case "5":
                    filePath = vFtp.Setting.FtpPath("ProblemPartOrder");
                    ContextFTP = "problempartorder";
                    break;
            }
            string msg = "";

            if(!vFtp.IsDirectoryExist(filePath, UploadFilePath))
            {
                vFtp.CreateDirectory(filePath + UploadFilePath);
            }
            filePath = filePath + UploadFilePath;

            if (vFtp.directoryExists(filePath+"/"+FolderName))
            {
                if (!vFtp.DirectoryList(filePath + FolderName).Contains((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName))
                {
                    ReportProcessor reportProcess = new ReportProcessor();
                    RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                    bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                    rpt.Dispose();
                }
                else 
                {   
                    byte[] file = vFtp.FtpDownloadByte((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName, ref msg);

                    if (file.Length <= 1)
                    {
                        ReportProcessor reportProcess = new ReportProcessor();
                        RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                        bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                        rpt.Dispose();
                    }

                    if (file.Length > 1)
                    {
                        if (file[0] == 0)
                        {
                            ReportProcessor reportProcess = new ReportProcessor();
                            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                            bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                            rpt.Dispose();
                        }
                    }
                }
            }
            else
            {
                ReportProcessor reportProcess = new ReportProcessor();
                RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                rpt.Dispose();
            }

            //string fullFtpPath = "ftp://" + vFtp.Setting.IP + filePath + FolderName + "/";
            //string fullLocalPath = AppDomain.CurrentDomain.BaseDirectory + localFilePath;
            //if (!Directory.Exists(fullLocalPath))
            //{
            //    Directory.CreateDirectory(fullLocalPath);
            //}
            //string serverPath = "";
            //if (vFtp.FtpDownload(fullFtpPath + FileName, fullLocalPath + LocalFileName, ref msg))
            //{
            //    //serverPath = String.Format("{0}{1}{2}", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, ""), localFilePath, LocalFileName);
            //    //serverPath = String.Format("{0}{1}{2}", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, ""), "/DailyOrderPrinting/GetFile?FileName=", LocalFileName);
            //    serverPath = ApplicationUrl + "/FileDownload?context=dailyorder&path=" + FolderName + "/" + FileName;
            //}
            //else
            //{
            //    throw new Exception("Can't retrieve file.");
            //}
            string serverPath = "";
            serverPath = ApplicationUrl + "/FileDownload?context=" + ContextFTP + "&path=" + FolderName + "/" + FileName;

            return serverPath;
        }

        //end print kanban baru

        #region PrintPDF
        //private string filePath = "";
        //private string localFilePath = "/Content/DailyOrderPrinting/tmp/";
        //private string SavePDFReportToFTP(Telerik.Reporting.Report rpt, string FileName, string FolderName, string OrderType, string UploadFilePath = "")
        //{
        //    //string LocalFileName = FileName + "-" + DateTime.Now.Ticks.ToString() + ".pdf";
        //    string LocalFileName = FileName + ".pdf";
        //    string ContextFTP = "";
        //    FileName = FileName + ".pdf";
            
        //    FTPUpload vFtp = new FTPUpload();
        //    switch (OrderType)
        //    {
        //        case "1":
        //            filePath = vFtp.Setting.FtpPath("DailyOrderPrinting");
        //            ContextFTP = "dailyorder";
        //            break;
        //        case "2":
        //            filePath = vFtp.Setting.FtpPath("EmergencyOrder");
        //            ContextFTP = "emergencyorder";
        //            break;
        //        case "3":
        //            filePath = vFtp.Setting.FtpPath("CPOOrder");
        //            ContextFTP = "cpoorder";
        //            break;
        //        case "4":
        //            filePath = vFtp.Setting.FtpPath("JunbikiOrder");
        //            ContextFTP = "junbikiorder";
        //            break;
        //        case "5":
        //            filePath = vFtp.Setting.FtpPath("ProblemPartOrder");
        //            ContextFTP = "problempartorder";
        //            break;
        //    }
        //    string msg = "";

        //    if(!vFtp.IsDirectoryExist(filePath, UploadFilePath))
        //    {
        //        vFtp.CreateDirectory(filePath + UploadFilePath);
        //    }
        //    filePath = filePath + UploadFilePath;

        //    if (vFtp.directoryExists(filePath+"/"+FolderName))
        //    {
        //        if (!vFtp.DirectoryList(filePath + FolderName).Contains((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName))
        //        {
        //            ReportProcessor reportProcess = new ReportProcessor();
        //            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
        //            bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
        //            rpt.Dispose();
        //        }
        //    }
        //    else
        //    {
        //        ReportProcessor reportProcess = new ReportProcessor();
        //        RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
        //        bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
        //        rpt.Dispose();
        //    }

        //    //string fullFtpPath = "ftp://" + vFtp.Setting.IP + filePath + FolderName + "/";
        //    //string fullLocalPath = AppDomain.CurrentDomain.BaseDirectory + localFilePath;
        //    //if (!Directory.Exists(fullLocalPath))
        //    //{
        //    //    Directory.CreateDirectory(fullLocalPath);
        //    //}
        //    //string serverPath = "";
        //    //if (vFtp.FtpDownload(fullFtpPath + FileName, fullLocalPath + LocalFileName, ref msg))
        //    //{
        //    //    //serverPath = String.Format("{0}{1}{2}", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, ""), localFilePath, LocalFileName);
        //    //    //serverPath = String.Format("{0}{1}{2}", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, ""), "/DailyOrderPrinting/GetFile?FileName=", LocalFileName);
        //    //    serverPath = ApplicationUrl + "/FileDownload?context=dailyorder&path=" + FolderName + "/" + FileName;
        //    //}
        //    //else
        //    //{
        //    //    throw new Exception("Can't retrieve file.");
        //    //}
        //    string serverPath = "";
        //    serverPath = ApplicationUrl + "/FileDownload?context=" + ContextFTP + "&path=" + FolderName + "/" + FileName;

        //    return serverPath;
        //}

        public FileResult GetFile(string FileName)
        {
            string fullLocalPath = AppDomain.CurrentDomain.BaseDirectory + localFilePath;
            
            byte[] buffer;
            FileStream fileStream = new FileStream((fullLocalPath + FileName), FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }

            return File(buffer, "application/force-download", FileName);
        }

        private Telerik.Reporting.Report CreateReport(string reportPath,
            string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }

        [HttpPost]
        public ContentResult DeleteFile(string FilePath)
        {
            FileInfo file = new FileInfo((AppDomain.CurrentDomain.BaseDirectory + FilePath.Replace(Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "/"), "")));
            if (file.Exists)
                file.Delete();

            return Content("");                
        }

        public ContentResult PrintDailyOrderLoop(string ManifestNo, string ManifestPrinter, string OtherDocumentPrinter, string Type, string PaperType,
            string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string jsonresult = "";
            Session["WSFTPPath"] = System.Configuration.ConfigurationManager.AppSettings["WSFTPPath"];
            try
            {   
                List<PrintParameter> printParameters = new List<PrintParameter>();
                string UrlPrint = "";
                string FileName = ManifestNo + "-";

                if (Type == "Manifest")
                {
                    //Manifest
                    UrlPrint = PrintManifestReport(ManifestNo, PaperType, FileName + PaperType +"-", OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType);
                    printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = ManifestPrinter });
                }

                if (Type == "KanbanSkid")
                {
                    //Kanban & Skid
                    //UrlPrint = PrintKanbanSkidReport(ManifestNo, FileName, OrderDate, SupplierCode, SupplierPlant);
                    //printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                    UrlPrint = PrintSelectionKanbanReport(ManifestNo, FileName, OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType);
                    printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });

                    UrlPrint = PrintSkidReport(ManifestNo, FileName, OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType);
                    printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });

                    //UrlPrint = PrintSplitterReport(FileName, OrderDate, SupplierCode, SupplierPlant, OrderType);
                    //printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                }
                else if (Type == "Kanban")
                {
                    UrlPrint = PrintSelectionKanbanReport(ManifestNo, FileName, OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType);
                    printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                }
                else if (Type == "Skid")
                {
                    UrlPrint = PrintSkidReport(ManifestNo, FileName, OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType);
                    printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                }
                else if (Type == "Splitter")
                {
                    UrlPrint = PrintSplitterReport(FileName, OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType);
                    printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                }

                UpdatePrintFlag(ManifestNo.Replace("'", ""));
                jsonresult = JSON.ToString<List<PrintParameter>>(printParameters);
                return Content(jsonresult);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ContentResult PrintDailyOrderLoopNew(string ManifestNo, string ManifestPrinter, string OtherDocumentPrinter, string PaperType,
            string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType, string TotalQty, bool Manifest, bool Kanban, bool Skid, string PrintFlag)
        {
            string jsonresult = "";
            List<string> ManifestNos = ManifestNo.Split(';').ToList();
            List<string> OrderDates = OrderDate.Split(';').ToList();
            List<string> SupplierCodes = SupplierCode.Split(';').ToList();
            List<string> SupplierPlants = SupplierPlant.Split(';').ToList();
            List<string> RcvPlantCodes = RcvPlantCode.Split(';').ToList();
            List<string> TotalQtys = TotalQty.Split(';').ToList();
            
            try
            {
                List<PrintParameter> printParameters = new List<PrintParameter>();
                string UrlPrint = "";
                for (int i = 0; i < ManifestNos.Count; i++)
                {
                    string FileName = ManifestNos[i] + "-";

                    if (TotalQtys[i] != "0")
                    {
                        if (Session["PrevSupplierCode"] == null)
                        {
                            Session["PrevSupplierCode"] = SupplierCodes[i];
                            Session["PrevSupplierPlant"] = SupplierPlants[i];
                            Session["PrevRcvPlantCode"] = RcvPlantCodes[i];
                        }
                        else
                        {
                            if (SupplierCodes[i] != Session["PrevSupplierCode"].ToString())
                            {
                                UrlPrint = PrintSplitterReport(FileName, OrderDates[i], Session["PrevSupplierCode"].ToString(), Session["PrevSupplierPlant"].ToString(), Session["PrevRcvPlantCode"].ToString(), OrderType);
                                printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });

                                Session["PrevSupplierCode"] = SupplierCodes[i];
                                Session["PrevSupplierPlant"] = SupplierPlants[i];
                                Session["PrevRcvPlantCode"] = RcvPlantCodes[i];
                            }
                        }
                    }

                    //Manifest
                    if (Manifest)
                    {
                        if(PrintFlag == "0") UrlPrint = PrintManifestReport(ManifestNos[i], PaperType, FileName + PaperType + "-", OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);
                        else UrlPrint = PrintManifestReport(ManifestNos[i], PaperType, FileName + PaperType + "-Duplicate-", OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);

                        printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = ManifestPrinter });
                    }

                    if (TotalQtys[i] != "0")
                    {
                        if (Skid)
                        {
                            UrlPrint = PrintSkidReport(ManifestNos[i], FileName, OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);
                            printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                        }

                        if (Kanban)
                        {
                            UrlPrint = PrintSelectionKanbanReport(ManifestNos[i], FileName, OrderDates[i], SupplierCodes[i], SupplierPlants[i], RcvPlantCodes[i], OrderType);
                            printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
                        }
                    }
                    
                    UpdatePrintFlag(ManifestNos[i].Replace("'", ""));                
                }
            
                jsonresult = JSON.ToString<List<PrintParameter>>(printParameters);
                return Content(jsonresult);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string PrintManifestReport(string ManifestNos, string PaperType, string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string FolderName = "";
            if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            string reportPath = @"\Report\DailyOrder\Rep_SupplierManifest.trdx";
            if (PaperType == "1")
                reportPath = @"\Report\DailyOrder\Rep_SupplierManifest_WithoutBorder.trdx";

            string sqlCommand = "SP_Rep_DailyOrder_Manifest_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
            if (OrderType == "5") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameters);

            if (PaperType == "1")
            {
                return SavePDFReportToFTP(rpt, FileName + "1", FolderName + "/ManifestPrePrinted", OrderType, "");
            }
            else
            {
                return SavePDFReportToFTP(rpt, FileName + "1", FolderName + "/ManifestPlain", OrderType, "");
            }
        }
        private string PrintSkidReport(string ManifestNos, string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string FolderName = "";
            if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;
            
            string reportPath = @"\Report\DailyOrder\Rep_ManifestSkid_Download.trdx";
            string sqlCommand = "SP_Rep_DailyOrder_Skid_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
            if (OrderType == "5") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameters);

            return SavePDFReportToFTP(rpt, FileName + "3", FolderName + "/Skid", OrderType, "");
        }
        private string PrintKanbanSkidReport(string ManifestNos, String FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string FolderName = "";
            if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;
            else FolderName = "DailyOrderProblemPart";
            
            string kanbanReportPath = @"\Report\DailyOrder\Rep_ManifestKanban.trdx";
            string kanbanSqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection kanbanParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            kanbanParameters.Add("@ManifestNo", System.Data.DbType.String, "=Parameters.ManifestNo.Value");
            kanbanParameters.Add("@PartNo", System.Data.DbType.String, null);
            kanbanParameters.Add("@OrderReleaseDate", System.Data.DbType.String, null);
            kanbanParameters.Add("@SupplierCode", System.Data.DbType.String, null);
            kanbanParameters.Add("@SupplierPlant", System.Data.DbType.String, null);
            Telerik.Reporting.Report kanbanReport = CreateReport(kanbanReportPath, kanbanSqlCommand, kanbanParameters);

            string skidReportPath = @"\Report\DailyOrder\Rep_ManifestSkid.trdx";
            string skidSqlCommand = "SP_Rep_DailyOrder_Skid_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection skidParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            skidParameters.Add("@ManifestNo", System.Data.DbType.String, "=Parameters.ManifestNo.Value");
            Telerik.Reporting.Report skidReport = CreateReport(skidReportPath, skidSqlCommand, skidParameters);

            string kanbanSkidReportPath = @"\Report\DailyOrder\Rep_KanbanSkid.trdx";
            string kanbanSkidSqlCommand = "SP_Rep_DailyOrder_KanbanSkid_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection kanbanSkidParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            kanbanSkidParameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
            Telerik.Reporting.Report rpt = CreateReport(kanbanSkidReportPath, kanbanSkidSqlCommand, kanbanSkidParameters);

            Telerik.Reporting.SubReport rptSub2 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport1", true)[0];
            rptSub2.ReportSource = new Telerik.Reporting.InstanceReportSource { ReportDocument = kanbanReport };
            rptSub2.ReportSource.Parameters.Add("ManifestNo", "=Fields.[MANIFEST_NO]");

            Telerik.Reporting.SubReport rptSub1 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport2", true)[0];
            rptSub1.ReportSource = new Telerik.Reporting.InstanceReportSource { ReportDocument = skidReport };
            rptSub1.ReportSource.Parameters.Add("ManifestNo", "=Fields.[MANIFEST_NO]");

            return SavePDFReportToFTP(rpt, FileName + "4", FolderName + "/KanbanSkid", OrderType, "");

        }
        private string PrintSelectionKanbanReport(string ManifestNo, string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType, string ItemNos = null, string OrderPlanQtyLot = null)
        {
            string FolderName = "";
            if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;
            
            ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";

            string dock = GetData(ManifestNo);

            if (dock == "1")
            {
                string reportPath = @"\Report\DailyOrder\Rep_ManifestKanban_Sunter.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print_Sunter";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                parameter.Add("@ItemNo", System.Data.DbType.String, ItemNos);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);
                if (OrderPlanQtyLot != null) parameter.Add("@ORDER_PLAN_QTY_LOT", System.Data.DbType.String, OrderPlanQtyLot);
                if (OrderType == "5") parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                return SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
            else
            {
                //string reportPath = @"\Report\DailyOrder\Rep_ManifestKanban_Download.trdx";
                //string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
                //Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                //parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                //parameter.Add("@ItemNo", System.Data.DbType.String, ItemNos);
                //parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                //parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                //parameter.Add("@SupplierPlant", System.Data.DbType.String, null);
                //if (OrderPlanQtyLot != null) parameter.Add("@ORDER_PLAN_QTY_LOT", System.Data.DbType.String, OrderPlanQtyLot);
                //if (OrderType == "5") parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                //Telerik.Reporting.Report rpt = CreateReport(reportPath,
                //    sqlCommand,
                //    parameter);

                //return SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType, "");

                string reportPath = @"\Report\DailyOrder\Rep_ManifestKanban_Download.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                parameter.Add("@ItemNo", System.Data.DbType.String, ItemNos);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);
                if (OrderPlanQtyLot != null) parameter.Add("@ORDER_PLAN_QTY_LOT", System.Data.DbType.String, OrderPlanQtyLot);
                if (OrderType == "5") parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                return SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }

        }
        public string PrintKanban(string ManifestNo, string ItemNos, string OtherDocumentPrinter, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType, string OrderPlanQtyLot)
        {
            List<PrintParameter> printParameters = new List<PrintParameter>();
            try
            {
                string UrlPrint = "";

                if (OrderPlanQtyLot != null) UrlPrint = PrintSelectionKanbanReport(ManifestNo, ManifestNo + "-" + ItemNos + "-" + OrderPlanQtyLot + "-", OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType, ItemNos, OrderPlanQtyLot);
                else UrlPrint = PrintSelectionKanbanReport(ManifestNo, ManifestNo + "-" + ItemNos + "-", OrderDate, SupplierCode, SupplierPlant, RcvPlantCode, OrderType, ItemNos);

                printParameters.Add(new PrintParameter { urlFile = UrlPrint, targetPrinter = OtherDocumentPrinter });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return JSON.ToString<List<PrintParameter>>(printParameters);
        }
        private string PrintSplitterReport(string FileName, string OrderDate, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderType)
        {
            string FolderName = "";
            if (OrderType == "2") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;
            
            string reportPath = @"\Report\DailyOrder\Rep_Splitter.trdx";
            string sqlCommand = "SP_Rep_DailyOrder_Splitter_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameter.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameter.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameter);

            return SavePDFReportToFTP(rpt, FileName + "5", FolderName, OrderType, "");
        }

        private string GetData(string Manifest)
        {
            string statusDock = "";
            IDBContext db = DbContext;
            Manifest = Manifest.Replace("\'", "");
            try
            {
                string DOCK_LIST = db.SingleOrDefault<string>("GetDockPacking");

                List<string> Dock = new List<string>();
                Dock = DOCK_LIST.Split(',').ToList();

                string DOCK_CD = db.SingleOrDefault<string>("GetManifestDock", new object[] { Manifest });

                for (int i = 0; i < Dock.Count; i++)
                {
                    //if (Dock[i].Trim() == dr44["DOCK_CD"].ToString())
                    if (Dock[i].Trim() == DOCK_CD)
                    {
                        statusDock = "1";
                        break;
                    }
                    else
                    {
                        statusDock = "2";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return statusDock;
        }

        [OutputCache(NoStore = true, Duration = 0 , VaryByParam = "*")]
        public ActionResult PrintManifest()
        {
            Session["onlyGetPrinterList"] = "false";
            return View("PrintManifest");
        }
        #endregion

        #region Download
        public void DownloadList(string Supplier,
                            string DockCode,
                            string OrderNo,
                            string PrintDateFrom,
                            string PrintDateTo,
                            string TimeFrom,
                            string TimeTo,
                            string tabActived,
                            string printFlag)
        {
            string SupplierCodeLDAP = "";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderPrinting", "OPHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            
            string DockCodeLDAP = "";
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderPrinting", "OPHDockOption");
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            
            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }

            string supplierCode = "";
            string supplierPlantCode = "";
            if (Supplier != "" && Supplier != null)
            {
                string[] supplier = Supplier.Split('-');
                supplierCode = supplier[0].Replace(" ", "");
                supplierPlantCode = supplier[1].Replace(" ", "");
            }

            printFlag = "1";
            string fileName = "DailyOrderPrinting.xls";

            IDBContext db = DbContext;
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            if (tabActived == "2")
            {
                IEnumerable<ReportEmergencyOrder> qry = db.Fetch<ReportEmergencyOrder>("GetAllDailyOrderPrinting", new object[] { 
                    supplierCode == null ? "" : supplierCode.Trim(), 
                    supplierPlantCode == null ? "" : supplierPlantCode.Trim(),
                    DockCode == null ? "" : DockCode,
                    OrderNo == null ? "" : OrderNo,
                    (TimeFrom == "" || TimeFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PrintDateFrom + " " + TimeFrom + ":00","dd.MM.yyyy HH:mm",null),
                    (TimeTo == "" || TimeTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PrintDateTo + " " + TimeTo + ":59","dd.MM.yyyy HH:mm",null),
                    tabActived,
                    SupplierCodeLDAP,
                    printFlag,
                    DockCodeLDAP
                });
                hasil = exporter.Write(qry, "DailyOrderPrintingData");
            }
            else
            {
                IEnumerable<ReportOrderPrinting> qry = db.Fetch<ReportOrderPrinting>("GetAllDailyOrderPrinting", new object[] { 
                    supplierCode == null ? "" : supplierCode.Trim(), 
                    supplierPlantCode == null ? "" : supplierPlantCode.Trim(),
                    DockCode == null ? "" : DockCode,
                    OrderNo == null ? "" : OrderNo,
                    (TimeFrom == "" || TimeFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PrintDateFrom + " " + TimeFrom + ":00","dd.MM.yyyy HH:mm",null),
                    (TimeTo == "" || TimeTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PrintDateTo + " " + TimeTo + ":59","dd.MM.yyyy HH:mm",null),
                    tabActived,
                    SupplierCodeLDAP,
                    printFlag,
                    DockCodeLDAP
                });
                hasil = exporter.Write(qry, "DailyOrderPrintingData");
            }
            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        class PrinterInfo
        {
            public string MANIFEST_PRINTER_NAME { set; get; }
            public string OTHER_PRINTER_NAME { set; get; }
            public Int32 PAPER_TYPE { set; get; }
        }

        class PrintParameter
        {
            public string urlFile { set; get; }
            public string targetPrinter { set; get; }
        }
    }
}
