﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.SupplierInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;

using Portal.Models.SupplierFeedbackSummaryDB;
//fid.marini
//modify by Andrew
namespace Portal.Controllers
{
    public class SupplierDbController : BaseController
    {
        public SupplierDbController()
            : base("Supplier Database Inquiry")
        {
        }

        protected override void StartUp()
        {

        }
        SupplierFeedbackSummaryParameterDB _supplierFeedbackSummaryParameterModelDB = null;
        private SupplierFeedbackSummaryParameterDB _SupplierFeedbackSummaryParameterModelDB
        {
            get
            {
                if (_supplierFeedbackSummaryParameterModelDB == null)
                    _supplierFeedbackSummaryParameterModelDB = new SupplierFeedbackSummaryParameterDB();

                return _supplierFeedbackSummaryParameterModelDB;
            }
            set
            {
                _supplierFeedbackSummaryParameterModelDB = value;
            }
        }

        List<Supplier> _supplierModel = null;
        private List<Supplier> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<Supplier>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }
        public ActionResult PartialHeader()
        {
            #region Required model for [Supplier lookup]
            TempData["GridName"] = "OIHSupplierOptionDb";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierDB", "OIHSupplierOptionDb");

            if (suppliers.Count > 0)
            {
                _SupplierModel = suppliers;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }
           
            Model.AddModel(_SupplierModel);
            #endregion

            return PartialView("HeaderMaintenance", Model);

            //return PartialView("GridLookup/PartialGrid", _SupplierModel);
        }

        public ActionResult PartialHeaderSupplierLookupGridSupplierDb()
        {
            #region Required model for [Supplier lookup]

            TempData["GridName"] = "OIHSupplierOptionDb";

            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierDB", "OIHSupplierOptionDb");

            if (suppliers.Count > 0)
            {
                _SupplierModel = suppliers;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }
            #endregion

            return PartialView("GridLookup/PartialGrid", _SupplierModel);


        }

        private List<Supplier> GetAllSupplier()
        {
            List<Supplier> supplierModel = new List<Supplier>();
            IDBContext db = DbContext;
            try
            {
                supplierModel = db.Fetch<Supplier>("GetAllSupplierForecast", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return supplierModel;
        }

        protected override void Init()
        {
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierDB", "OIHSupplierOptionDb");

            IDBContext db = DbContext;

            SupplierModel mdl = new SupplierModel();

            //mdl.SupplierGenerals = GetGeneralPartial("","","");
            //mdl.SupplierDbUpdate = GetUpdatePartial("","");
            Model.AddModel(mdl);
            //db.Close();

        }
        [HttpGet]
        public void DownloadSheet(string SupplierCode)
        {

            string fileName = "SupplierProfile" + ".xls";

           // _SupplierFeedbackSummaryParameterModelDB.SUPPLIER_CODE = Request.Params["SUPPLIER_CODE"];
            
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierInfoData> listTooltipDatas = new List<SupplierInfoData>();
            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplierTextbox(), "SUPPLIER_CODE", "SupplierProfile", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SUPPLIER_CODE"]) ? "" : Request.Params["SUPPLIER_CODE"];
            }

            IEnumerable<SupplierGeneralSheet> listGeneral = db.Query<SupplierGeneralSheet>("GetSupplierProfileGeneralInfoSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierAddressSheet> listAdress = db.Query<SupplierAddressSheet>("GetSupplierProfileAddressSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierContactPersonSheet> listContact = db.Query<SupplierContactPersonSheet>("GetSupplierProfileContactPersonSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierShareHolderSheet> listSH = db.Query<SupplierShareHolderSheet>("GetSupplierProfileShareHolderSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierCustomerSheet> listCustomer = db.Query<SupplierCustomerSheet>("GetSupplierProfileCustomerSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierFinancialSheet> listFinancial = db.Query<SupplierFinancialSheet>("GetSupplierProfileFinancialSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierManPowerSheet> listManPower = db.Query<SupplierManPowerSheet>("GetSupplierProfileManPowerSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierLaborSheet> listLabor = db.Query<SupplierLaborSheet>("GetSupplierProfileLaborSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierCADITSheet> listCAD = db.Query<SupplierCADITSheet>("GetSupplierProfileCadITSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierPurchasedPartSheet> listPurchased = db.Query<SupplierPurchasedPartSheet>("GetSupplierProfilePurchasedPartSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierAwardSheet> listAward = db.Query<SupplierAwardSheet>("GetSupplierProfileAwardSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierProductSheet> listProduct = db.Query<SupplierProductSheet>("GetSupplierProfileProductSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierBusinessRelationSheet> listBusiness = db.Query<SupplierBusinessRelationSheet>("GetSupplierProfileBusinessRelationSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierEquipmentSheet> listEquip = db.Query<SupplierEquipmentSheet>("GetSupplierProfileEquipmentSheet", new object[] { SupplierCode, SupplierCodeLDAP });
            IEnumerable<SupplierExportSheet> listExport = db.Query<SupplierExportSheet>("GetSupplierProfileExportSheet", new object[] { SupplierCode, SupplierCodeLDAP });


            IExcelWriter exporter = ExcelWriter.GetInstance();

            exporter.Append(listGeneral, "GeneralInfo");
            exporter.Append(listAdress, "Address");
            exporter.Append(listContact, "Contact");
            exporter.Append(listSH, "ShareHolder");
            exporter.Append(listCustomer, "Customer");
            exporter.Append(listFinancial, "Financial");
            exporter.Append(listManPower, "ManPower");
            exporter.Append(listLabor, "Labor");
            exporter.Append(listCAD, "CadIT");
            exporter.Append(listPurchased, "PurchasedPart");
            exporter.Append(listAward, "Award");
            exporter.Append(listProduct, "ProductPart");
            exporter.Append(listBusiness, "Business");
            exporter.Append(listEquip, "Equipment");
            exporter.Append(listExport, "Export");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();
        }

        private List<SupplierName> GetAllSupplierTextbox()
        {
            List<SupplierName> SISupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            SISupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return SISupplierModel;
        }

        public ActionResult GeneralPartial()
        {
            _SupplierFeedbackSummaryParameterModelDB.SUPPLIER_CODE = Request.Params["SUPPLIER_CODE"];

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierInfoData> listTooltipDatas = new List<SupplierInfoData>();
            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplierTextbox(), "SUPPLIER_CODE", "SupplierDB", "OIHSupplierOptionDb");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                //SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
                SupplierCodeLDAP = _SupplierFeedbackSummaryParameterModelDB.SUPPLIER_CODE;
            }
            //string SuppCd = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];

            SupplierModel model = Model.GetModel<SupplierModel>();
            
            model.SupplierGenerals = GetGeneralPartial(Request.Params["SUPPLIER_CODE"], Request.Params["Flag"], SupplierCodeLDAP);

            Model.AddModel(model);

            return PartialView("GeneralPartial", Model);
        }

        public ActionResult UpdatePartial()
        {
            _SupplierFeedbackSummaryParameterModelDB.SUPPLIER_CODE = Request.Params["SUPPLIER_CODE"];
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierInfoData> listTooltipDatas = new List<SupplierInfoData>();
            string SupplierCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplierTextbox(), "SUPPLIER_CODE", "SupplierDB", "OIHSupplierOptionDb");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                //SupplierCodeLDAP = String.IsNullOrEmpty(Request.Params["SupplierCode"]) ? "" : Request.Params["SupplierCode"];
                SupplierCodeLDAP = _SupplierFeedbackSummaryParameterModelDB.SUPPLIER_CODE;
            }

            SupplierModel model = Model.GetModel<SupplierModel>();
            model.SupplierDbUpdate = GetUpdatePartial(SupplierCodeLDAP);

            Model.AddModel(model);

            return PartialView("UpdatePartial", Model);
        }


        protected List<SupplierGeneral> GetGeneralPartial(string suppCdParam, string Flag,string suppCdLdap)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierGeneral> listAdress = new List<SupplierGeneral>();
            listAdress = db.Query<SupplierGeneral>("GetSupplierDbGeneral", new object[] { suppCdParam ,Flag, suppCdLdap}).ToList();
            db.Close();
            return listAdress;
        }

        protected List<SupplierDbUpdate> GetUpdatePartial(string suppCdParam)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<SupplierDbUpdate> listAdress = new List<SupplierDbUpdate>();
            listAdress = db.Query<SupplierDbUpdate>("GetSupplierDbUpdate", new object[] { suppCdParam}).ToList();
            db.Close();
            return listAdress;
        }

        public ContentResult UpdateConfirmation(string SupplierCode, string ActiveTab)
        {
            IDBContext db = DbContext;
            string messageResult = "";
            try
            {
                string[] supplierCodes = null;
            

                if (SupplierCode != "") supplierCodes = SupplierCode.Split(',');
               




                for (int i = 0; i < supplierCodes.Length; i++)
                {
                   

                    if (SupplierCode != "" )
                    {
                        db.ExecuteScalar<string>("UpdateConfirmationSupplierDB",
                            new object[] 
                            { 
                                ActiveTab, supplierCodes[i].ToString(), AuthorizedUser.Username, DateTime.Now 
                            }
                        );
                    }



                }

                #region update logic to TB_M_SUPPLIER at ICS &  TB_M_SUPPLIER_ICS IPPCS FOR PO & PC
                // ICS
                db.ExecuteScalar<string>("UpdateTB_M_SUPPLIER_ICS",
                            new object[] 
                            { 
                                 ActiveTab, SupplierCode, AuthorizedUser.Username, DateTime.Now 

                            }
                        );

                // PO & PC
                db.ExecuteScalar<string>("UpdateTB_M_SUPPLIER_ICS_IPPCS",
                           new object[] 
                            { 
                                 ActiveTab, SupplierCode, AuthorizedUser.Username, DateTime.Now 

                            }
                       );

                #endregion

                messageResult = "Process successfully";
            }

            catch (Exception exc)
            {
                messageResult = exc.Message;
            }

            db.Close();
            return Content(messageResult);
        }
       
    }
}
