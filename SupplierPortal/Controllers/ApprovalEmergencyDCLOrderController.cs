﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;
using Portal.Models.ApprovalEmergencyDCLOrder;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.Reporting;

namespace Portal.Controllers
{
    public class ApprovalEmergencyDCLOrderController : BaseController
    { 
        private DataTable data;
        
        //
        // GET: /ApprovalEmergencyDCLOrder/


        public ApprovalEmergencyDCLOrderController()
            : base("Additional Pick Up Approval Screen")
        {
            
        }

        protected override void StartUp()
        {
            //if (Session["dataWizard2"] != null)
            //    Session["dataWizard2"] = null;

            //ApprovalEmergencyDCLOrderModel AE = new ApprovalEmergencyDCLOrderModel();

            //DownloadToolsModel mdl = new DownloadToolsModel();
            //objectDB = new DBQueryRetrieval();

            //data = new DataTable();

            //Session.Add("AdditionalApproval", data);
            //AE.DataAE = data;
            //mdl.DataDownloads = data;
            //Model.AddModel(AE);
        }

        [Obsolete]
        protected override void Init()
        {
            // Random random = new Random();
            ApprovalEmergencyDCLOrderModel mdl = new ApprovalEmergencyDCLOrderModel();

            mdl.Apps = DCLget();
            Model.AddModel(mdl);

            //---=== Load data logistic partner
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLP = new List<LogisticPartner>();
            var QueryLogLP = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLP = QueryLogLP.ToList<LogisticPartner>();

            db.Close();
            ViewData["LogisticPartnerData"] = listLP.ToList<LogisticPartner>();
        }           


        protected List<ApprovalEmergencyDCLOrderData> DCLget()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<ApprovalEmergencyDCLOrderData>("GetDCLApproval", new object[] { "","" });
            db.Close();
            return QueryLog.ToList<ApprovalEmergencyDCLOrderData>();
        }

        public ActionResult PartialLPView()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLP = new List<LogisticPartner>();
            var QueryLogLP = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLP = QueryLogLP.ToList<LogisticPartner>();

            Model.AddModel(listLP);

            db.Close();
            return PartialView("PartialLogisticPartner", Model);
        }

        public ActionResult LPPartialAdditional()
        {
            TempData["GridName"] = "grlLogisticPartnerHeader";
            //TempData["GridName"] = CbName;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");

            ViewData["LogisticPartnerData"] = QueryLog;

            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
            //    return PartialView("ApprovalEmergencyDCLOrderGrid", ViewData["LogisticPartnerData"]);
        }

        // Add by Goldy : modify by Rahmat Setiawan (Arkamaya)
        #region Load & Update Gridview

        public PartialViewResult GridViewPartial(string TermFrom, string TermTo)
        {

            if (!String.IsNullOrEmpty(TermFrom))
            {
                TermFrom = Convert.ToDateTime(TermFrom.Substring(4, 20)).ToString("yyyy-MM-dd");
            }
            if (!String.IsNullOrEmpty(TermTo))
            {
                TermTo = Convert.ToDateTime(TermTo.Substring(4, 20)).ToString("yyyy-MM-dd");
            }

            //IDBContext db2 = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //ApprovalEmergencyDCLOrderModel model = Model.GetModel<ApprovalEmergencyDCLOrderModel>();
            //model.Apps = db.Fetch<ApprovalEmergencyDCLOrderData>("GetDCLApproval", new object[] { TermFrom, TermTo });
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ApprovalEmergencyDCLOrderModel model = Model.GetModel<ApprovalEmergencyDCLOrderModel>();
            List<ApprovalEmergencyDCLOrderData> DCLOrderData = new List<ApprovalEmergencyDCLOrderData>();
            var QueryLog = db.Query<ApprovalEmergencyDCLOrderData>("GetDCLApproval", new Object[] { TermFrom, TermTo });
            if (QueryLog.Any())
            {
                int count = 0;
                foreach (var q in QueryLog)
                {
                    DCLOrderData.Add(new ApprovalEmergencyDCLOrderData()
                    {
                        ROUTE = q.ROUTE,
                        APPROVAL_STATUS_EDIT = q.APPROVAL_STATUS_EDIT,
                        APPROVAL_STATUS = q.APPROVAL_STATUS,
                        REQUEST_STATUS = q.REQUEST_STATUS,
                        MAX_APPROVE_DURATION = q.MAX_APPROVE_DURATION,
                        PICK_UP_DATE = q.PICK_UP_DATE,
                        REQUEST_DT = q.REQUEST_DT,
                        //DOCK = q.DOCK,
                        LP_CD = q.LP_CD,
                        LP_NAME = q.LP_NAME,
                        TRIP_NO = q.TRIP_NO,
                        //SUPPLIER_CD = q.SUPPLIER_CD,
                        //SUPPLIER_PLANT_CD = q.SUPPLIER_PLANT_CD,
                        //SUPPLIER_NAME = q.SUPPLIER_NAME,
                        REASON = q.REASON,
                        ARRIVAL_DATE = (((bool)q.APPROVAL_STATUS_EDIT) ? getArrivalPlanDate(q.DELIVERY_NO) : null),
                        DEPT_DATE = (((bool)q.APPROVAL_STATUS_EDIT) ? getDeparturePlanDate(q.DELIVERY_NO) : null),
                        DELIVERY_NO = q.DELIVERY_NO,
                        DELIVERY_STATUS = q.DELIVERY_STATUS,
                        DELIVERY_STATUS_STRING = q.DELIVERY_STATUS_STRING,
                        SUPPLIER_PLANT_STRING = q.SUPPLIER_PLANT_STRING
                    });
                    count++;
                }
            }
            db.Close();
            model.Apps = DCLOrderData;
            Model.AddModel(model);


            return PartialView("ApprovalEmergencyDCLOrderGrid", Model);
        }

        private Nullable<DateTime> getArrivalPlanDate(string deliveryNo)
        {
            Nullable<DateTime> result = null;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if ((deliveryNo != "") || (deliveryNo != null))
            {
                ArrivalDepartureData QueryLog = db.SingleOrDefault<ArrivalDepartureData>("GetArrivalDepartureDate", new object[] { deliveryNo });
                result = QueryLog.ARRIVAL_DATE;
            }
            db.Close();
            return result;
        }

        private Nullable<DateTime> getDeparturePlanDate(string deliveryNo)
        {
            Nullable<DateTime> result = null;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (deliveryNo != "")
            {
                ArrivalDepartureData QueryLog = db.SingleOrDefault<ArrivalDepartureData>("GetArrivalDepartureDate", new object[] { deliveryNo });
                result = QueryLog.DEPT_DATE;
            }
            db.Close();
            return result;
        }

        public PartialViewResult UpdatePartial(ApprovalEmergencyDCLOrderData editable)
        {
            ViewData["AdditionalDelivery"] = editable;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ApprovalEmergencyDCLOrderModel model = Model.GetModel<ApprovalEmergencyDCLOrderModel>();
            model.Apps = db.Fetch<ApprovalEmergencyDCLOrderData>("GetDCLApproval", new object[] { "", "" });
            db.Close();
            return PartialView("ApprovalEmergencyDCLOrderGrid", Model);

        }

        public ContentResult UpdateDataGrid(string PickupDate, string LPCode, string RouteEdit, string TripNoEdit,
                                            string DeliveryNo, string DeliveryStatus, string Route, string TripNo, 
                                            string LPCD, string SupplierCode, string SupplierPlant)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            Nullable<DateTime> pickup_dt;
            string[] extract = new string[2];

            pickup_dt = PickupDate == "" ? pickup_dt = null : Convert.ToDateTime(formatingShortDate(PickupDate));
            string changed_by = AuthorizedUser.Username;

            try
            {
                db.Execute("UpdateGridGVApprovalScreen", new object[] { 
                    pickup_dt, LPCode, getLogisticPartnerName(LPCode), RouteEdit, TripNoEdit,
                    DeliveryNo, DeliveryStatus, Route, TripNo, LPCD, SupplierCode, SupplierPlant, changed_by
                });
                return Content("Succesfully Delivery No " + DeliveryNo + " updating.");
            }
            catch (Exception ex)
            {
                return Content("Error, failed to update Delivery No " + DeliveryNo + " ! ");
            }

            db.Close();
        }

        private string formatingShortDateTime(string dateString)
        {
            string result = "";
            string[] extract = new string[2];
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extract = dateString.Split(' ');
                extractString = extract[0].Split('.');
                result = extractString[2] + "/" + extractString[1] + "/" + extractString[0] + " " + extract[1];
            }

            return result;
        }

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                result = extractString[2] + "/" + extractString[1] + "/" + extractString[0];
            }

            return result;
        }
        #endregion
        // End Add by Goldy

        #region Approval & Rejection

        //---=== Add by Rahmat Setiawan (Arkamaya)
        public ContentResult ApproveListData(string GridId) {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //---=== Declare Variables
            string SupplierCode = "";
            string SupplierPlant = "";
            string deliveryNo = "";

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    char[] SplitId = { ',' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);

                    SupplierCode = ParamId[11].ToString().Split('-')[0];
                    SupplierPlant = ParamId[11].ToString().Split('-')[1];
                    if (deliveryNo == "")
                        deliveryNo = ParamId[10].ToString();
                    else
                        deliveryNo += ", " + ParamId[10].ToString();

                    db.Execute("UpdateApproveRejectDelivery", new object[] { 
                        "Approve", ParamId[10].ToString(), ParamId[3].ToString(), ParamId[8].ToString(), ParamId[9].ToString(), ParamId[6].ToString(), 
                        SupplierCode.Trim(), SupplierPlant.Trim(), AuthorizedUser.Username
                    });
                    db.Close();
                }
                return Content("Succesfully approved delivery no " + deliveryNo + " by " + AuthorizedUser.Username + ".");
            }
            catch (Exception ex) {
                return Content("Error, " + ex.Message);
            }           
        }

        //---=== Add by Rahmat Setiawan (Arkamaya)
        public ContentResult UnApproveListData(string GridId)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //---=== Declare Variables
            string SupplierCode = "";
            string SupplierPlant = "";
            string deliveryNo = "";

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    char[] SplitId = { ',' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);

                    SupplierCode = ParamId[11].ToString().Split('-')[0];
                    SupplierPlant = ParamId[11].ToString().Split('-')[1];
                    if (deliveryNo == "")
                        deliveryNo = ParamId[10].ToString();
                    else
                        deliveryNo += ", " + ParamId[10].ToString();

                    db.Execute("UpdateApproveRejectDelivery", new object[] { 
                        "UnApprove", ParamId[10].ToString(), ParamId[3].ToString(), ParamId[8].ToString(), ParamId[9].ToString(), ParamId[6].ToString(), 
                        SupplierCode.Trim(), SupplierPlant.Trim(), AuthorizedUser.Username
                    });
                    db.Close();
                }
                return Content("Succesfully un-approved delivery no " + deliveryNo + " by " + AuthorizedUser.Username + ".");
            }
            catch (Exception ex)
            {
                return Content("Error, " + ex.Message);
            }
        }

        //---=== Add by Rahmat Setiawan (Arkamaya)
        public ContentResult RejectListData(string GridId)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //---=== Declare Variables
            string SupplierCode = "";
            string SupplierPlant = "";
            string deliveryNo = "";

            try
            {
                //Split ID with delimeter ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);
                for (int i = 0; i < ParamUpdate.Length; i++)
                {
                    char[] SplitId = { ',' };
                    string[] ParamId = ParamUpdate[i].Split(SplitId);

                    SupplierCode = ParamId[11].ToString().Split('-')[0];
                    SupplierPlant = ParamId[11].ToString().Split('-')[1];
                    if (deliveryNo == "")
                        deliveryNo = ParamId[10].ToString();
                    else
                        deliveryNo += ", " + ParamId[10].ToString();

                    db.Execute("UpdateApproveRejectDelivery", new object[] { 
                        "Reject", ParamId[10].ToString(), ParamId[3].ToString(), ParamId[8].ToString(), ParamId[9].ToString(), ParamId[6].ToString(), 
                        SupplierCode.Trim(), SupplierPlant.Trim(), AuthorizedUser.Username
                    });
                    db.Close();
                }
                return Content("Succesfully rejected delivery no " + deliveryNo + " by " + AuthorizedUser.Username + ".");
            }
            catch (Exception ex)
            {
                return Content("Error, " + ex.Message);
            }
        }
        #endregion


        //public ActionResult Search(string TermFrom, string TermTo)
        //{

        //    if (!String.IsNullOrEmpty(TermFrom))
        //    {
        //        TermFrom = Convert.ToDateTime(TermFrom.Substring(4, 20)).ToString("yyyy-MM-dd");
        //    }
        //    if (!String.IsNullOrEmpty(TermTo))
        //    {
        //        TermTo = Convert.ToDateTime(TermTo.Substring(4, 20)).ToString("yyyy-MM-dd");
        //    }

        //    //  DateTime dateFromFormat = Convert.ToDateTime(TermFrom.Substring(4, 20));
        //    //  DateTime dateToFormat = Convert.ToDateTime(TermTo.Substring(4, 20));
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    ApprovalEmergencyDCLOrderModel model = Model.GetModel<ApprovalEmergencyDCLOrderModel>();
        //    model.Apps = db.Fetch<ApprovalEmergencyDCLOrderData>("GetDCLApproval", new object[] { TermFrom, TermTo });
        //    return PartialView("ApprovalEmergencyDCLOrderGrid", Model);
        //}

        //public ActionResult ChangeStatus()
        //{
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    ApprovalEmergencyDCLOrderModel model = Model.GetModel<ApprovalEmergencyDCLOrderModel>();
        //    model.Apps = db.Fetch<ApprovalEmergencyDCLOrderData>("GetDCLApproval", new object[] { TermFrom, TermTo });
        //    return PartialView("ApprovalEmergencyDCLOrderGrid", Model);
        //}

        public ActionResult ApprovalMaster()
        {
            return PartialView("ApprovalEmergencyDCLOrderMaster", Model);
        }

        public ActionResult ApprovalEmergencyDCLOrderHeader()
        {
            return PartialView("ApprovalEmergencyDCLOrderHeader", Model);
        }


        public ActionResult ApprovalEmergencyDCLOrderHeaderApproval()
        {
            return PartialView("ApprovalEmergencyDCLOrderHeaderApproval", Model);
        }


        public ActionResult ApprovalEmergencyDCLOrderHeaderBottom()
        {
            return PartialView("ApprovalEmergencyDCLOrderHeaderBottom", Model);
        }

        public ActionResult PopupDeliveryOrderGrid(string DeliveryNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            ApprovalEmergencyDCLOrderModel model = Model.GetModel<ApprovalEmergencyDCLOrderModel>();
            model.PopUp = db.Query<DeliveryLPApprovalData>("GetDeliveryOrderByDeliveryNo", new object[] {
                DeliveryNo
            }).ToList<DeliveryLPApprovalData>();
            //model.GridContent = model.GridContent.ToList<AdditionalDeliveryGridContent>();
            Model.AddModel(model);
            db.Close();
            return PartialView("popupDeliveryNoGrid", Model);
        }


        //Add By Goldy
        public string getLogisticPartnerName(string LogisticPartnerCode)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if (LogisticPartnerCode != "")
            {
                LogisticPartner QueryLog = db.SingleOrDefault<LogisticPartner>("GetLogisticPartnerNamebyCode", new object[] { LogisticPartnerCode });
                result = QueryLog.LPName;
            }
            db.Close();
            return result;
        }
        //End Add By Goldy
    }
}
