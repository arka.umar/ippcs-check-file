﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Compression;
using DevExpress.Web.ASPxUploadControl;
//using Portal.Models.POA;

using Toyota.Common.Web.Html;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Configuration;
using System.Web.UI.WebControls;
using System.Net;
using DevExpress.Web.Mvc;
using Toyota.Common.Web.Notification;
using Portal.Models.Truck;
using Portal.Models.DCLInquiry;
using Telerik.Reporting.Processing;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class TruckController : BaseController
    {
        //
        // GET: /Truck/

        public TruckController()
            : base("Truck Arrival Monitoring")
        {
            PageLayout.UseMessageBoard = true;
        }

        private IDBContextManager dbManager;
        private IDBContext dbContext;

        protected override void Init()
        {
            TruckModel mdl = new TruckModel();

            mdl.Trucks = GetDCLDetails();
            
            ViewData["DeliveryNo"] = mdl.Trucks[0].DELIVERY_NO;
            //ViewData["SupplierData"] = listSupplier;
            //ViewData["PickUp"] = setDummyPickUp();
            Model.AddModel(mdl);
        }

        protected List<TruckData> GetDCLDetails()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<TruckData> DCLDetailList = new List<TruckData>();

            // GAD: pake fetch aja , auto mapping, penjagaannya di query aja
            //var QueryLog = db.Query<TruckData>("GetDCLTruckMonitoring", new object[] { "", "" });
            DCLDetailList = db.Fetch<TruckData>("GetDCLTruckMonitoring", new object[] { "", "" });
            
            //if (QueryLog.Any())
            //{
            //    foreach (var q in QueryLog)
            //    {
            //        DCLDetailList.Add(new TruckData()
            //        {
            //            DELIVERY_NO = Convert.ToString(q.DELIVERY_NO),
            //            PICKUP_DT = q.PICKUP_DT,
            //            ROUTERATE = Convert.ToString(q.ROUTERATE),
            //            LP_CD = Convert.ToString(q.LP_CD),
            //            LOG_PARTNER_NAME = Convert.ToString(q.LOG_PARTNER_NAME),
            //            DRIVER_NAME = Convert.ToString(q.DRIVER_NAME),
            //            DOCK_CD = Convert.ToString(q.DOCK_CD),
            //            STATION = Convert.ToString(q.STATION),
            //            ARRIVAL_PLAN_DT = Convert.ToString(q.ARRIVAL_PLAN_DT),
            //            ARRIVAL_ACTUAL_DT = Convert.ToString(q.ARRIVAL_ACTUAL_DT),
            //            ARRIVAL_STATUS = Convert.ToString(q.ARRIVAL_STATUS),
            //            ARRIVAL_GAP = Convert.ToString(q.ARRIVAL_GAP),
            //            DEPARTURE_PLAN_DT = Convert.ToString(q.DEPARTURE_PLAN_DT),
            //            DEPARTURE_ACTUAL_DT = Convert.ToString(q.DEPARTURE_ACTUAL_DT),
            //            DEPARTURE_STATUS = Convert.ToString(q.DEPARTURE_STATUS),
            //            DEPARTURE_GAP = Convert.ToString(q.DEPARTURE_GAP),
            //            NOTICE = SetNoticeIconString(Convert.ToInt32(q.NOTICE))
            //        });
            //    }
            //}
            db.Close();
            return DCLDetailList;
        }
        
        protected override void StartUp()
        {

        }

        public ActionResult HeaderTopDCLTruck()
        {
            return PartialView("HeaderTopDCLTruck", Model);
        }

        public ActionResult GridTruckMonitoring()
        {
            return PartialView("GridTruckMonitoring", Model);
        }
        
        public ActionResult UpdateCallBackPanel()
        {
            ViewData["DeliveryNo"] = Request.Params["DeliveryNo"];
            //return PartialView("PopUpRegulerOrder", ViewData["DeliveryNo"]);
            return PartialView("PopUpCallBackPartial");
        }

        public ActionResult PartialDCLInquiry(string DeliveryNo)
        {
            ViewData["DeliveryNo"] = DeliveryNo;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            DCLInquiry DeliveryDetail = new DCLInquiry();
            DeliveryDetail = db.SingleOrDefault<DCLInquiry>("GetDCLDetail", new Object[] { DeliveryNo });
            if (DeliveryDetail == null)
            {
                DeliveryDetail = new DCLInquiry
                {
                    DeliverNo = "",
                    PickUpDate = "",//DateTime.Now,
                    RouteRate = "",
                    DriverName = "",
                    DriverManifest = "",
                    DownloadBy = "",
                    DownloadTime = DateTime.Now,
                    LogisticPartner = "",
                    ApprovalStatus = "",
                    DeliveryStatus = "",
                    LPConfirmation = "",

                    Notice = "",
                    DeliveryReason = "",
                    ReviseBy = "",
                    ReviseDate = DateTime.Now,
                    Approver = "",
                    ApprovalDate = DateTime.Now,

                    Status = "",
                    DeliveryBy = "",
                    DeliveryDate = DateTime.Now,
                    InvoiceBy = "",
                    InvoiceDate = DateTime.Now,
                    Requisitioner = "",
                    RequisitionDate = DateTime.Now,
                    LPName = ""
                };
            }
            db.Close();
            return PartialView("PopupRegulerOrderHeader", DeliveryDetail);
        }

        public ActionResult PartialSupplier(string DeliveryNo)
        {
            List<Supplier> listSupplier = new List<Supplier>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<Supplier>("GetDCLRoute", new object[] { DeliveryNo });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    listSupplier.Add(new Supplier()
                    {
                        SupplierCode = q.SupplierCode,
                        SupplierName = q.SupplierName
                    });
                }
            }
            db.Close();
            ViewData["SupplierData"] = listSupplier;
            return PartialView("PopUpRegulerDetailRoute", ViewData["SupplierData"]);
        }

        public ActionResult PartialPickUp(string DeliveryNo)
        {
            List<DCLPickUp> listPickUp = new List<DCLPickUp>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<DCLPickUp>("GetDCLManifest", new object[] { DeliveryNo });

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    listPickUp.Add(new DCLPickUp()
                    {
                        ReceiveingDock = q.ReceiveingDock,
                        Station = q.Station,
                        ArrivalPlan = q.ArrivalPlan,
                        ArrivalActual = q.ArrivalActual,
                        ArrivalStatus = q.ArrivalStatus,
                        ArrivalGap = q.ArrivalGap,
                        DeparturePlan = q.DeparturePlan,
                        DepartureActual = q.DepartureActual,
                        DepartureStatus = q.DepartureStatus,
                        DepartureGap = q.DepartureGap,
                        CarriedManifest = q.CarriedManifest,
                        ManifestType = q.ManifestType
                    });
                }
            }
            db.Close();
            ViewData["PickUp"] = listPickUp;
            return PartialView("PopUpRegulerPickUp", ViewData["PickUp"]);
        }

        public ActionResult PartialManifestPart(string DeliveryNo)
        {
            List<ManifestPartData> listManifestPart = new List<ManifestPartData>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<ManifestPartData>("GetDCLManifestPart", new object[] { DeliveryNo });

            listManifestPart = QueryLog.ToList<ManifestPartData>();

            //if (QueryLog.Any())
            //{
            //    foreach (var q in QueryLog)
            //    {
            //        listManifestPart.Add(new ManifestPartData()
            //        {
            //            MANIFEST_NO = q.MANIFEST_NO,
            //            SUPPLIER_CD = q.SUPPLIER_CD,
            //            SUPPLIER_NAME = q.SUPPLIER_NAME,
            //            PART_NO = q.PART_NO,
            //            PARTS_NAME = q.PARTS_NAME
            //        });
            //    }
            //}
            db.Close();
            ViewData["Manifest"] = listManifestPart;
            return PartialView("PopUpRegulerManifest", ViewData["Manifest"]);
        }

        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_close_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_open_icon.png";
            
            if (val == 0 || val == 3)
            {
                img = imageBytes1;
            }
            else if (val == 1)
            {
                img = imageBytes2;
            }
            else if (val == 2)
            {
                img = imageBytes3;
            }
            return img;
        }
    }
}
