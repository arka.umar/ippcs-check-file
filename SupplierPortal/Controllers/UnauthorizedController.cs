﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.MVC.Filter;

namespace Portal.Controllers
{
    [MarkAsAuthorizedController]
    public class UnauthorizedController : BaseController
    {
        public UnauthorizedController()
            : base("Unauthorized Access")
        {  }

        protected override void StartUp()
        {  }

        protected override void Init()
        {  }
    }
}
