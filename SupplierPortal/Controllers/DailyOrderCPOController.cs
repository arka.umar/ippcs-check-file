﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Portal.Models.DailyOrder;
using Portal.Models.Supplier;
using Portal.Models.Dock;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using System.Xml;
using System.Diagnostics;


namespace Portal.Controllers
{
    public class DailyOrderCPOController : BaseController
    {
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
            {
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            }
            else
            {
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }
         
         private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

         public DailyOrderCPOController()
             : base("CPO Order Inquiry")
         {
             PageLayout.UseMessageBoard = true;
             CookieName = "fileDownload";
             CookiePath = "/";
         }


         #region Model properties
         List<SupplierName> _orderInquirySupplierModel = null;
         private List<SupplierName> _OrderInquirySupplierModel
         {
             get
             {
                 if (_orderInquirySupplierModel == null)
                     _orderInquirySupplierModel = new List<SupplierName>();

                 return _orderInquirySupplierModel;
             }
             set
             {
                 _orderInquirySupplierModel = value;
             }
         }

         List<SupplierName> _orderInquirySupplierCPOModel = null;
         private List<SupplierName> _OrderInquirySupplierCPOModel
         {
             get
             {
                 if (_orderInquirySupplierCPOModel == null)
                     _orderInquirySupplierCPOModel = new List<SupplierName>();

                 return _orderInquirySupplierCPOModel;
             }
             set
             {
                 _orderInquirySupplierCPOModel = value;
             }
         }

         List<DockData> _orderInquiryDockModel = null;
         private List<DockData> _OrderInquiryDockModel
         {
             get
             {
                 if (_orderInquiryDockModel == null)
                     _orderInquiryDockModel = new List<DockData>();

                 return _orderInquiryDockModel;
             }
             set
             {
                 _orderInquiryDockModel = value;
             }
         }
         #endregion

         protected override void StartUp()
         {
         }

         protected override void Init()
         {
             PageLayout.UseSlidingBottomPane = true;

             DailyOrderModel mdl = new DailyOrderModel();
             Model.AddModel(mdl);

             ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
             ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");
         }

         #region Order Inquiry controller
         #region View controller
         public ActionResult PartialHeaderOrderInquiry()
         {
             Session["paramManifestNo"] = String.IsNullOrEmpty(Request.Params["ManifestNos"]) ? Session["paramManifestNo"] : Request.Params["ManifestNos"];

             #region Required model in partial page : for OrderInquiryHeaderPartial
             List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderCPO", "OIHSupplierOption");
             _OrderInquirySupplierModel = suppliers;

             List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderCPO", "OIHDockOption");
             _OrderInquiryDockModel = DockCodes;

             Model.AddModel(_OrderInquirySupplierModel);
             Model.AddModel(_OrderInquiryDockModel);
             #endregion

             return PartialView("OrderInquiryHeaderPartial", Model);
         }

         public ActionResult PartialHeaderDockLookupGridOrderInquiry()
         {
             #region Required model in partial page : for OIHDockOption GridLookup
             List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderCPO", "OIHDockOption");
             TempData["GridName"] = "OIHDockOption";
             _OrderInquiryDockModel = DockCodes;
             #endregion

             return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
         }

         public ActionResult PartialHeaderSupplierLookupGridOrderInquiry()
         {
             #region Required model in partial page : for OIHSupplierOption GridLookup
             List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderCPO", "OIHSupplierOption");

             TempData["GridName"] = "OIHSupplierOption";
             _OrderInquirySupplierModel = suppliers;
             #endregion

             return PartialView("GridLookup/PartialGrid", _OrderInquirySupplierModel);
         }

         public ActionResult PartialDetailOrderInquiryServicePartExport()
         {
             DailyOrderModel model = Model.GetModel<DailyOrderModel>();
             model.ComponentPartOrders = GetAllNonDailyOrderPlan(Request.Params["SupplierCode"],
                                                Request.Params["DockCode"],
                                                Request.Params["ManifestNo"],
                                                Request.Params["DateFrom"],
                                                Request.Params["DateTo"],
                                                "6",
                                                Request.Params["Capability"]);

             return PartialView("OrderInquiryServicePartExportDetailPartial", Model);
         }

         public ActionResult PopupHeaderDailyDataPartSubDetail()
         {
             string manifest = Request.Params["ManifestNo"];
             return PartialView("PartialHeaderDailyDataSubDetail", ViewData["ManifestNo"]);
         }

         public ActionResult PopupDailyDataPartSubDetail()
         {
             string manifestNo = Request.Params["ManifestNo"];
             string problemFlag = Request.Params["ProblemFlag"];
             ViewData["partDailyData"] = DailyDataPartOrderFillGrid(manifestNo, problemFlag);
             return PartialView("DailyDataSubDetail", ViewData["partDailyData"]);
         }
         #endregion

         #region Common controller
         private String GetGeneralProductionMonth(String productionMonth)
         {
             Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
             monthDictionary.Add("01", "January");
             monthDictionary.Add("02", "February");
             monthDictionary.Add("03", "March");
             monthDictionary.Add("04", "April");
             monthDictionary.Add("05", "May");
             monthDictionary.Add("06", "June");
             monthDictionary.Add("07", "July");
             monthDictionary.Add("08", "August");
             monthDictionary.Add("09", "September");
             monthDictionary.Add("10", "October");
             monthDictionary.Add("11", "November");
             monthDictionary.Add("12", "December");

             String generalProductionMonth = "";

             if (!String.IsNullOrEmpty(productionMonth))
             {
                 String year = productionMonth.Substring(0, 4);
                 String month = productionMonth.Substring(productionMonth.Length - 2, 2);

                 generalProductionMonth = monthDictionary[month] + " " + year;
             }

             return generalProductionMonth;
         }
         #endregion

         #region Database controller
         private List<SupplierName> GetAllSupplier()
         {
             List<SupplierName> orderInquirySupplierModel = new List<SupplierName>();
             IDBContext db = DbContext;
             orderInquirySupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
             db.Close();

             return orderInquirySupplierModel;
         }

         private List<DockData> GetAllDock()
         {
             List<DockData> orderInquiryDockModel = new List<DockData>();
             IDBContext db = DbContext;
             orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
             db.Close();

             return orderInquiryDockModel;
         }

         protected List<PartDetailManifest> DailyDataPartOrderFillGrid(string manifestNo, string problemFlag)
         {
             List<PartDetailManifest> getPartDetailManifest = new List<PartDetailManifest>();
             IDBContext db = DbContext;
             getPartDetailManifest = db.Query<PartDetailManifest>("GetPartDetailManifest", new object[] { manifestNo, problemFlag }).ToList<PartDetailManifest>();

             db.Close();
             return getPartDetailManifest;
         }

         private List<NonDailyOrder> GetAllNonDailyOrderPlan(String supplierCode, String dockCode, String manifestNo, String dateFrom, String dateTo, string OrderType, string manifestNos)
         {
             IDBContext db = DbContext;
             List<NonDailyOrder> dailyOrderModel = db.Fetch<NonDailyOrder>("GetAllNonDailyOrderPlan", new object[] { 
                supplierCode == null ? "" : supplierCode, 
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType,
                manifestNos
            });
             db.Close();

             return dailyOrderModel;
         }

         private void UpdateDownloadFlagNonRegular(string ManifestNo)
         {
             IDBContext db = DbContext;

             db.Execute("UpdateDownloadFlagOrderNonRegular", new Object[] { ManifestNo, AuthorizedUser.Username });
             db.Close();
         }
         #endregion
         #endregion

         #region DownloadPDF
         public FileContentResult DownloadPDF(string ManifestNo, string OrderType)
         {
             Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
             Stream output = new MemoryStream();
             byte[] documentBytes;
             string filePath = "";

             try
             {
                 FTPUpload vFtp = new FTPUpload();
                 filePath = vFtp.Setting.FtpPath("CPOOrder");
                 string msg = "";

                 string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, filePath);
                 documentBytes = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, ManifestNo + ".zip"),
                                                        ref msg);

                 UpdateDownloadFlagNonRegular(ManifestNo);

                 return File(documentBytes, "application/zip", ManifestNo + ".zip");
             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
         #endregion

         #region Download Raw TXT
         private byte[] StreamFile(string filename)
         {
             FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
             byte[] ImageData = new byte[fs.Length];
             fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
             fs.Close();
             return ImageData;
         }

         public FileContentResult PrintDailyOrderTXTCPO(string ManifestNo, string OrderDate, string SupplierCode, string OrderType)
         {
             ICompression compress = ZipCompress.GetInstance();
             Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();

             Stream output = new MemoryStream();
             string fileName = "Order_" + OrderDate + ".txt";
             string outputName = "";

             char[] splitchar = { '-' };
             string[] Supplier = SupplierCode.Split(splitchar);
             DateTime OrdDate = DateTime.ParseExact(OrderDate, "dd.MM.yyyy hh:mm", null);

             if (OrderType == "6") OrderType = "C";
             else OrderType = "XXX";

             IDBContext db = DbContext;

             string filePath = Path.Combine(Server.MapPath("~/Views/DailyOrderCPO/DownloadTemp/"), Supplier[0] + Supplier[1] + ".txt");
             string filePathZip = Path.Combine(Server.MapPath("~/Views/DailyOrderCPO/DownloadTemp/"), Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");

             using (StreamWriter writer = new StreamWriter(filePath))
             {
                 try
                 {
                     if (OrderType != "C")
                     {
                         throw new Exception("Wrong Data Selected, Please Refresh The Page Before Continue");
                     }

                     List<CPODailyOrderRAWData> rows = db.Fetch<CPODailyOrderRAWData>("GetTXTRawSPEX", new object[] { ManifestNo, OrdDate.ToString("yyyyMMdd"), Supplier[0], Supplier[1] });

                     string tab = "";
                     StringBuilder strHeader = new StringBuilder();
                     StringBuilder str = new StringBuilder();

                     int orderQty = 0;
                     string[] columns;
                     foreach (CPODailyOrderRAWData r in rows)
                     {
                         tab = "";
                         columns = r.DATA.Split('|');
                         for (int j = 0; j < columns.Length; j++)
                         {
                             //83 == PACKAGE_TYPE
                             //if (j == 83) str.Append(tab + (columns[j] == null ? "" : columns[j].ToString()));
                             //else 
                             str.Append(tab + (columns[j] == null ? "" : columns[j].ToString().TrimEnd()));

                             if (r.LVL > 1)
                             {
                                 tab = "\t";
                             }
                             else
                             {
                                 tab = "|";
                             }
                         }

                         str.AppendLine();
                         //orderQty = orderQty + r.ORDER_QTY;
                     }

                     writer.Write(str.ToString());
                     writer.Dispose();

                     outputName = Supplier[0] + Supplier[1];
                     outputName = db.ExecuteScalar<string>("Get_CPO_SuppbySupp", new object[] { ManifestNo, OrdDate.ToString("yyyyMMdd"), Supplier[0], Supplier[1] });

                     byte[] documentBytes = StreamFile(filePath);
                     listCompress.Add(outputName + ".txt", documentBytes);

                     compress.Compress(listCompress, output);
                     documentBytes = new byte[output.Length];
                     output.Position = 0;
                     output.Read(documentBytes, 0, documentBytes.Length);

                     UpdateDownloadFlagNonRegular(ManifestNo);
                     return File(documentBytes, "application/zip", outputName + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");
                 }
                 catch (Exception ex)
                 {
                     writer.Dispose();
                     throw ex;
                 }
             }
         }
         #endregion

         #region Download
         public void DownloadList(string SupplierCode,
                             string DockCode,
                             string ManifestNo,
                             string OrderReleaseDateFrom,
                             string OrderReleaseDateTo,
                             string tabActived,
                             string partName,
                             string partStatus)
         {
             string SupplierCodeLDAP = "";
             string DockCodeLDAP = "";
             List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderCPO", "OIHSupplierOption");
             List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();


             List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderCPO", "OIHDockOption");

             if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
             else SupplierCodeLDAP = "";

             foreach (DockData d in DockCodes)
             {
                 DockCodeLDAP += d.DockCode + ",";
             }

             string fileName = "DailyOrderCPO.xls";

             IDBContext db = DbContext;
             IExcelWriter exporter = new NPOIWriter();
             byte[] hasil;

             IEnumerable<ReportGetAllNonDailyOrderModelCPO> qry = db.Fetch<ReportGetAllNonDailyOrderModelCPO>("ReportGetAllNonDailyOrderCPO", new object[] {
                SupplierCode == null ? "" : SupplierCode,
                DockCode == null ? "" : DockCode,
                ManifestNo == null ? "" : ManifestNo,
                (OrderReleaseDateFrom == "" || OrderReleaseDateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateFrom,"dd.MM.yyyy",null),
                (OrderReleaseDateTo == "" || OrderReleaseDateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(OrderReleaseDateTo,"dd.MM.yyyy",null),
                tabActived,
                SupplierCodeLDAP,
                DockCodeLDAP,
                partName,
                partStatus
            });
             hasil = exporter.Write(qry, "CPO");
             fileName = "ComponentPartOrder.xls";

             db.Close();

             Response.Clear();
             //Response.ContentType = result.MimeType;
             Response.Cache.SetCacheability(HttpCacheability.Private);
             Response.Expires = -1;
             Response.Buffer = true;

             Response.ContentType = "application/octet-stream";
             Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

             Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
             Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

             Response.BinaryWrite(hasil);
             Response.End();
         }
         #endregion

    }
}
