﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;

using Toyota.Common.Web.MVC; 
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database; 
using Toyota.Common.Web.Reporting;
using Portal.Models.Globals;
using Portal.Models.LSRRecording;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Collections;
using Portal.ExcelUpload;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;

namespace Portal.Controllers
{
    public class ProblemPartController : BaseController
    {
        #region PRIVATE VARIABLE
        private static List<LSRRecordingPartListTemp> listTempPartList; 

        private IDBContext DbContext
        {
            get 
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        private Int32 ProcessId_ = 0;
        private Int32 ProcessId
        {
            get
            {
                if (ProcessId_ == 0)
                    ProcessId_ = Math.Abs(Environment.TickCount);

                return ProcessId_;
            }
        }

        private TableDestination TableDest_ = null;
        private TableDestination TableDest
        {
            get
            {
                if (TableDest_ == null)
                    TableDest_ = new TableDestination();

                return TableDest_;
            }
            set
            {
                TableDest_ = value;
            }
        }

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();

            IDBContext db = DbContext;
            try
            {
                // get temporary upload table column name list and create column name parameter
                List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { TableDest.tableFromTemp });
                foreach (ExcelReaderColumnName columnName in columnNameList)
                {
                    columnNameArray.Add(columnName.ColumnName);
                }
                columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnName:" + exc.Message);
            }
            finally
            {
                db.Close();
            }

            return String.Join(",", columnNameArray.ToArray());
        }

        private List<ErrorValidation> ErrValidationList_ = null;
        private List<ErrorValidation> ErrValidationList
        {
            get
            {
                if (ErrValidationList_ == null)
                    ErrValidationList_ = new List<ErrorValidation>();

                return ErrValidationList_;
            }
            set
            {
                ErrValidationList_ = value;
            }
        }

        private String FilePath_ = "";
        private String FilePath
        {
            get
            {
                return FilePath_;
            }
            set
            {
                FilePath_ = value;
            }
        }

        private String UploadDirectory_ = "";
        private String UploadDirectory
        {
            get
            {
                UploadDirectory_ = Server.MapPath("~/Views/LSRRecording/UploadTemp/");
                return UploadDirectory_;
            }
        }
        #endregion

        public ProblemPartController()
            : base("Problem Part Maintenance")
        {
            PageLayout.UseMessageBoard = true;
            
        }

        #region Model Parameter
        List<LSRRecordingPartListModel> _lSRRecordingPartListModelList = null;
        List<LSRRecordingPartListModel> _LSRRecordingPartListModelList
        {
            get
            {
                if (_lSRRecordingPartListModelList == null)
                    _lSRRecordingPartListModelList = new List<LSRRecordingPartListModel>();

                return _lSRRecordingPartListModelList;
            }
            set
            {
                _lSRRecordingPartListModelList = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
            listTempPartList = new List<LSRRecordingPartListTemp>();
        }

        [Obsolete]
        protected override void Init()
        {
            //Set Defaut Date Now
            ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");

            IDBContext db = DbContext;
            List<Manifest> listManifest = new List<Manifest>();
            var QueryLog = db.Query<Manifest>("GetAllLSRRecordingManifest");
            listManifest = QueryLog.ToList<Manifest>();

            //Bind to Combobox Manifest No on Header Part List
            ViewData["ManifestNo"] = listManifest;

            //For Gridlookup Part
            List<Part> listPart = new List<Part>();
            var QueryPart = db.Query<Part>("GetAllPartNoGrid");
            listPart = QueryPart.ToList<Part>();

            ViewData["PartData"] = listPart;
            db.Close();
        }

        #region LSRRecording Part List Controller

        #region View Controller

        public ActionResult PartialLSRRecordingPartList()
        {
            _LSRRecordingPartListModelList = GetAllLSRRecordingPartList(
                Request.Params["DateFrom"], 
                Request.Params["DateTo"], 
                Request.Params["Reason"], 
                Request.Params["ManifestNo"], 
                Request.Params["RegisteredBy"], 
                Request.Params["PartNo"]
                );
            Model.AddModel(_LSRRecordingPartListModelList);

            return PartialView("LSRRecordingPartListPartial", Model);
        }

        public ActionResult PartialDetailDetailLSRRecordingPartList()
        {
            _LSRRecordingPartListModelList = GetAllLSRRecordingPartList(
                Request.Params["DateFrom"],
                Request.Params["DateTo"],
                Request.Params["Reason"],
                Request.Params["ManifestNo"],
                Request.Params["RegisteredBy"],
                Request.Params["PartNo"]
                );
            Model.AddModel(_LSRRecordingPartListModelList);

            return PartialView("LSRRecordingPartListDetailDetailPartial", Model);
        }

        public ActionResult PartialPopupLSRRecordingPartListDetailAdd()
        {
            return PartialView("LSRRecordingPartListDetailAddPopupPartial", Model);
        }

        public ActionResult PartialDetailPopupLSRRecordingPartListDetailAdd()
        {
            return PartialView("LSRRecordingPartListDetailAddPopupDetailPartial", Model);
        }

        public List<Part> GetAllPartData()
        {
            IDBContext db = DbContext;
            List<Part> ListPartData = new List<Part>();
            var QueryLog = db.Query<Part>("GetAllPartNoGrid");
            ListPartData = QueryLog.ToList<Part>();
            db.Close();
            return ListPartData;
        }

        public ActionResult PartNoPartial()
        {
            TempData["PartNo"] = "grlPartNo";
            ViewData["PartData"] = GetAllPartData();
            return PartialView("GridLookup/PartialGrid", ViewData["PartData"]);
        }

        public ActionResult PartNoPartial2()
        {
            TempData["PartNo"] = "grlPartNo2";
            ViewData["PartData2"] = GetAllPartData();
            return PartialView("GridLookup/PartialGrid", ViewData["PartData2"]);
        }
        #endregion

        #region Database Controller

        List<LSRRecordingPartListModel> GetAllLSRRecordingPartList
            (string DateFrom, string DateTo, string Reason, string ManifestNo, string RegisteredBy, string PartNo)
        {
            List<LSRRecordingPartListModel> lSRRecordingPartListModel = new List<LSRRecordingPartListModel>();
            IDBContext db = DbContext;
            try
            {
                lSRRecordingPartListModel = db.Fetch<LSRRecordingPartListModel>
                    ("GetAllLSRRecordingPartList", new object[] { DateFrom, DateTo, Reason, ManifestNo, RegisteredBy, PartNo });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return lSRRecordingPartListModel;
        }

        public ActionResult GetAllManifestNo()
        {
            IDBContext db = DbContext;
            IEnumerable<Manifest> listManifest = db.Query<Manifest>("GetAllLSRRecordingManifest");
            db.Close();

            //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //List<Manifest> listManifest = new List<Manifest>();
            //var QueryLog = db.Query<Manifest>("GetAllLSRRecordingManifest");
            //listManifest = QueryLog.ToList<Manifest>();

            //Bind to Combobox Manifest No on Header Part List
            ViewData["ManifestNo"] = listManifest;
            return PartialView("PartialManifest", ViewData["ManifestNo"]);
        }

        #endregion

        #region DOWNLOAD TO XLS ON HEADER
        public void DownloadOnPartList
            (string DateFrom, string DateTo, string Reason, string ManifestNo, string RegisteredBy, string PartNo)
        {
           
            string fileName = "Part List " + DateFrom + ".xls";

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] ByteFile = exporter.Write(GetAllLSRRecordingPartList(DateFrom, DateTo, Reason, ManifestNo, RegisteredBy, PartNo), "PartList");


            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(ByteFile.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(ByteFile);
            Response.End();
        }
        #endregion

        #region DELETE DATA ON HEADER
        public ContentResult DeletePartList(string gridId)
        {
            IDBContext db = DbContext;

            char[] splitchar = { ';' };
            string[] strGridId = gridId.Split(splitchar);
            string manifestNo = "";
            string partNo = "";
            string Result = "";

            for (int i = 0; i < strGridId.Length; i++)
            {
                manifestNo = strGridId[i].Split('_')[0];
                partNo = strGridId[i].Split('_')[1];

                Result += "Data with Manifest No: ";
                Result += manifestNo;
                Result += " and Part No : ";
                Result += partNo;
                Result += " was deleted";
                Result += db.ExecuteScalar<string>("DeleteLSRRecordingPartList", new object[] { manifestNo, partNo, AuthorizedUser.Username });
                Result += "\n";
            }

            db.Close();
            return Content(Result.ToString());
        }
        #endregion

        #region UPLOAD FILE XLS
        #region On Click Button Upload
        public ActionResult CompleteUpload(string ManifestNo, string PartNo, string FilePath)
        {
            GetProcessUpload(FilePath);
            String status = Convert.ToString(TempData["IsValid"]);
            return Json(
                new
                {
                    Status = status
                });
        }
        #endregion

        #region Get All Process Upload
        private LSRRecordingModel GetProcessUpload(string FilePath)
        {
            LSRRecordingModel UploadModel = new LSRRecordingModel();

            // Property Table Destination
            TableDest.functionID = "141";
            TableDest.processID = Convert.ToString(ProcessId);
            TableDest.filePath = FilePath;
            TableDest.sheetName = "PartList";
            TableDest.rowStart = "2";
            TableDest.columnStart = "1";
            TableDest.columnEnd = "10";
            TableDest.tableFromTemp = "TB_T_PROBLEM_PART_UPLOAD";
            TableDest.tableToAccess = "TB_T_PROBLEM_PART_VALIDATE";

            LogProvider LogProv = new LogProvider("102", "141", AuthorizedUser);
            ILogSession SessionUpload = LogProv.CreateSession();

            OleDbConnection excelConn = null;

            IDBContext db = DbContext;
            try
            {
                SessionUpload.Write("MPCS00001INF", new object[] { "Process Upload has been started..!!!" });

                // Clear Temporary Table
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { TableDest.tableFromTemp });
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { TableDest.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB
                //xls Connection String
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + FilePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";
                //Open and Read xls File
                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();
                //Query for xls File
                excelCommand = new OleDbCommand(@"SELECT " +
                                    TableDest.functionID + " as FUNCTION_ID, " +
                                    TableDest.processID + " as PROCESS_ID, " +
                                    @"`MANIFEST NO` as MANIFEST_NO, 
                                    `PART NO` as PART_NO,  
                                    `PART NAME` as PART_NAME, 
                                    `ORDER QTY` as ORDER_QTY, 
                                    `RECEIVED QTY` as RECEIVED_QTY,
                                    `QTY` as QTY, 
                                    `SHORTAGE QTY` as SHORTAGE_QTY,  
                                    `MISSPART QTY` as MISSPART_QTY, 
                                    `DAMAGE QTY` as DAMAGE_QTY, 
                                    `PCS PER CONTAINER` as PCS_PER_CONTAINER 
                                    FROM [" + TableDest.sheetName + "$]", excelConn);
                //Execute xls Data
                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);
                //Copy xls Data to Bulk
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = TableDest.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("MANIFEST_NO", "MANIFEST_NO");
                    bulkCopy.ColumnMappings.Add("PART_NO", "PART_NO");
                    bulkCopy.ColumnMappings.Add("PART_NAME", "PART_NAME");
                    bulkCopy.ColumnMappings.Add("ORDER_QTY", "ORDER_QTY");
                    bulkCopy.ColumnMappings.Add("RECEIVED_QTY", "RECEIVED_QTY");
                    bulkCopy.ColumnMappings.Add("QTY", "QTY");
                    bulkCopy.ColumnMappings.Add("SHORTAGE_QTY", "SHORTAGE_QTY");
                    bulkCopy.ColumnMappings.Add("MISSPART_QTY", "MISSPART_QTY");
                    bulkCopy.ColumnMappings.Add("DAMAGE_QTY", "DAMAGE_QTY");
                    bulkCopy.ColumnMappings.Add("PCS_PER_CONTAINER", "PCS_PER_CONTAINER");
                #endregion

                    try
                    {

                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        SessionUpload.Write("MPCS00002ERR", new string[] { ex.Source, ex.Message, "", "" });
                        Console.WriteLine(ex.Message);
                    }
                }

                String columnNameList = GetColumnName();

                // validate data in temporary uploaded table.
                db.Execute("ValidateLSRRecordingUpload", new object[] { TableDest.functionID, TableDest.processID});

                // check for invalid data.
                try
                {
                    ErrValidationList = db.Fetch<ErrorValidation>("GetErrorMessageUpload", new object[] { TableDest.functionID, TableDest.processID });
                }
                catch (Exception exc)
                {
                    SessionUpload.Write("MPCS00002ERR", new string[] { exc.Source, exc.Message, "", "" });
                    throw exc;
                }

                if (ErrValidationList.Count == 0)
                {
                    // insert data from temporary uploaded table into temporary validated table.
                    db.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] { TableDest.tableToAccess, columnNameList, columnNameList, TableDest.tableFromTemp });

                    // read uploaded data from temporary validated table to transcation table.

                    // set process validation status.
                    TempData["IsValid"] = "true;" + TableDest.processID + ";" + TableDest.functionID + ";" + TableDest.tableFromTemp + ";" + TableDest.tableToAccess;
                    SessionUpload.Write("MPCS00001INF", new object[] { @"Insert upload success info into table log detail.
                                                                        Insert data from temporary uploaded table into temporary validated table.
                                                                        Read uploaded data from temporary validated table to transcation table.
                                                                        set process validation status...!!!" });
                }
                else
                {

                    // load previous data for supplier feedback part model

                    // set process validation status.
                    TempData["IsValid"] = "false;" + TableDest.processID + ";" + TableDest.functionID + ";" + TableDest.tableFromTemp + ";" + TableDest.tableToAccess;

                    SessionUpload.Write("MPCS00001INF", new object[] { @"Insert upload fail info into table log detail
                                                                        set process validation status...!!!" });
                }
            }
            catch (Exception exc)
            {
                // insert read process error info into table log detail.
                SessionUpload.Write("MPCS00002ERR", new string[] { "SFPUERR", "UPLD ERROR", "insert read process error info into table log detail", exc.Message });
            }
            finally
            {
                SessionUpload.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_SUCCESS);
                SessionUpload.Commit();
                db.Close();

                excelConn.Close();

                if (System.IO.File.Exists(TableDest.filePath))
                    System.IO.File.Delete(TableDest.filePath);
            }

            return UploadModel;
        }
        #endregion

        #region Clear Upload
        [HttpPost]
        public void ClearUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            try
            {
                // clear data in temporary uploaded table.
                db.Execute("ClearTempTableOnUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception)
            {
                throw new Exception();
            }
            finally { db.Close(); }
        }
        #endregion

        #region File Upload Validation
        [HttpPost]
        public void UploadCallback()
        {
            UploadControlExtension.GetUploadedFiles("LRPLDHUploadControl", ValidationSettings, FileUploadCompleted);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadCompleted(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                FilePath = UploadDirectory + Math.Abs(System.Environment.TickCount) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(FilePath, true);
                e.CallbackData = FilePath;
            }
        }
        #endregion

        #region Result Upload File
        public void UploadValid(String processId, String functionId, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            db.ExecuteScalar<string>("UpdateLPOPConfirmationUpload",
                            new object[] { 
                            functionId,   // upload function id
                            processId,   // upload process id
                            AuthorizedUser.Username
                        });
            db.Close();
        }

        [HttpGet]
        public void DownloadInvalid(String processId, String functionId, String temporaryTableUpload)
        {

            string fileName = "Part List Invalid " + DateTime.Now.ToString("dd MMM yyyy") + ".xls";
            IDBContext db = DbContext;
            IEnumerable<LSRRecordingReportInvalid> qry = db.Query<LSRRecordingReportInvalid>("ReportLSRRecordingInvalidUpload", new object[] { functionId, processId });

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(qry, "PartListError");

            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        #endregion

        #region ADD DATA ON POPUP - TEMP PARTLIST
        public ActionResult PartialPartList(string valGrid)
        {
            try
            {
            if (valGrid != null)
            {
                char[] splitchar = { ';' };
                string[] strGridId = valGrid.Split(splitchar);

                for (int i = 0; i < strGridId.Length; i++)
                {
                    listTempPartList.Add(new LSRRecordingPartListTemp()
                    {
                        NO_INDEX = listTempPartList.Count > 0 ? listTempPartList.Count + 1 : 1,
                        PART_NO = strGridId[i].Split('_')[0],
                        PART_NAME = strGridId[i].Split('_')[1],
                        QTY = strGridId[i].Split('_')[2] != "" ? Convert.ToInt32(strGridId[i].Split('_')[2]) : 0,
                        //AREA_NAME = strGridId[i].Split('_')[3],
                        REASON = strGridId[i].Split('_')[4] != "null" ? strGridId[i].Split('_')[4] : string.Empty
                    });
                }
            }
            TempData["PartList"] = listTempPartList;
            return PartialView("LSRRecordingPartListDetailAddPopupDetailPartial", TempData["PartList"]);
            }
            catch (Exception ex)
            {
                return Content (ex.ToString());
            }
        }
        #endregion

        #region DELETE DATA ON POPUP - TEMP PARTLIST
        public ActionResult DeletePartialPartList(int noIndex)
        {
            if (listTempPartList.Count > 0)
            {
                LSRRecordingPartListTemp listTemp = null;
                foreach (LSRRecordingPartListTemp listTempPart in listTempPartList)
                {
                    if (listTempPart.NO_INDEX == noIndex)
                    {
                        listTemp = listTempPart;
                        break;
                    }
                }

                if (listTemp != null)
                {
                    listTempPartList.Remove(listTemp);
                }

                for (int i = listTempPartList.Count - 1; i >= 0; i-- )
                {
                    listTempPartList[i].NO_INDEX = i + 1;
                }
            }
            TempData["PartList"] = listTempPartList;
            return PartialView("LSRRecordingPartListDetailAddPopupDetailPartial", TempData["PartList"]);
        }
        #endregion

        #region EDIT DATA ON POPUP - TEMP PARTLIST
        public ActionResult EditPartialPartList(int noIndex, string partNo, string partName, string qty, string reason)
        {
            LSRRecordingPartListTemp listTemp = null;
            //foreach (LSRRecordingPartListTemp listTempPart in listTempPartList)
            //{
            //    if (listTempPart.NO_INDEX == noIndex)
            //    {
                    listTemp = new LSRRecordingPartListTemp()
                    {
                        NO_INDEX = noIndex + 1,
                        PART_NO = partNo,
                        PART_NAME = partName,
                        QTY = qty != "" ? Convert.ToInt32(qty) : 0,
                        //AREA_NAME = area,
                        REASON = reason != null ? reason : ""
                    };
                    listTempPartList.RemoveAt(noIndex);
                    listTempPartList.Insert(noIndex, listTemp);
                    //listTemp = listTempPart;
                    //break;
            //    }
            //}

            if (listTemp != null)
            {
                listTemp.PART_NO = partNo;

            }


            TempData["PartList"] = listTempPartList;
            return PartialView("LSRRecordingPartListDetailAddPopupDetailPartial", TempData["PartList"]);
        }
        #endregion

        #region SAVE DATA ON POPUP - TEMP PARTLIST
        public ContentResult SavePartList(string valGrid, string area)
        {
            IDBContext db = DbContext; 
            char[] splitchar = { ';' };
            string[] strGridId = valGrid.Split(splitchar);
            string partNo = "";
            string strPartNo = "";
            string strPartNo_ = "";
            string qty = "";
            string reason = "";
            string Result = "";
            string ManifestNo = "";

            //Get Manifest Number
            ManifestNo = db.ExecuteScalar<string>("GetManifestNoLSRRecording", new object[] { area });

            //Cek if any same Problem Part
            for (int i = 0; i < strGridId.Length; i++)
            {
                strPartNo_ = strGridId[i].Split('_')[0] + ";";
                if (strPartNo.IndexOf(strPartNo_) > 0)
                {
                    Result = "Cannot insert Duplicate Part Number";
                    break;
                }
                strPartNo = strPartNo + strPartNo_;
            }

            //Insert Into Table Problem Part
            if (Result != "Cannot insert Duplicate Part Number")
            {
                for (int i = 0; i < strGridId.Length; i++)
                {
                    partNo = strGridId[i].Split('_')[0];
                    qty = strGridId[i].Split('_')[2];
                    reason = strGridId[i].Split('_')[3];
                    Result += db.ExecuteScalar<string>("InsertLSRRecordingPartList", new object[] { ManifestNo, partNo, qty, area, reason, AuthorizedUser.Username });
                    Result = "Succes to save data";
                }
            }

            db.Close();
            return Content(ManifestNo + ";" + Result.ToString());
        }
        #endregion

        public void ClearTempData()
        {
            listTempPartList.Clear();
            TempData["PartList"] = new Dictionary<int, LSRRecordingPartListTemp>();
        }

        #endregion

        #region LSRRecording Registered Manifest Controller
        #region View Controller
        public ActionResult PartialLSRRecordingRegisteredManifest()
        {
            return PartialView("LSRRecordingRegisteredManifestPartial", Model);
        }

        public ActionResult PartialDetailLSRRecordingRegisteredManifest()
        {
            return PartialView("LSRRecordingRegisteredManifestDetailPartial", Model);
        }
        #endregion

        #region Database Controller
        #endregion
        #endregion
    }
}
