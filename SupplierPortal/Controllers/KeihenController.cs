﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Portal.Models.Keihen;
using System.IO;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Management;

namespace Portal.Controllers
{
    public class KeihenController : BaseController
    {
        public KeihenController()
            : base("Order Change")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            KeihenData KData = new KeihenData();
            CalculatedKeihenData CData = new CalculatedKeihenData();
            Model.AddModel(KData);
            Model.AddModel(CData);
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        #region Consolidation (Upload Keihen)
        public ActionResult UploadKeihenTab()
        {
            return PartialView("PartialUploadKeihenTab");
        }

        public ActionResult ReloadUploadKeihenGrid(string PACK_MONTH = "x", string isSearch = "N")
        { 
            KeihenData KData = Model.GetModel<KeihenData>();
            IDBContext db = DbContext;
            List<KeihenModel> KModel = new List<KeihenModel>();

            try
            {
                if (isSearch == "Y")
                {
                    KModel = db.Fetch<KeihenModel>("GetKeihenData", new object[] { PACK_MONTH });
                }
                KData.KModel = KModel;
                Model.AddModel(KData);
            }
            catch(Exception e)
            {
                TempData["EXCEPTION"] = e.Message;
            }

            return PartialView("PartialUploadKeihenGrid", Model);
        }

        public void DownloadKeihenData(string PACK_MONTH = "x")
        {
            IDBContext db = DbContext;
            IExcelWriter Exporter = ExcelWriter.GetInstance();
            byte[] ResultExport = null;
            string FileNames = "KeihenData_" + PACK_MONTH + ".xls";
            List<KeihenUploadModel> KUModels = new List<KeihenUploadModel>();

            try
            {
                KUModels = db.Fetch<KeihenUploadModel>("GetKeihenDataForDownload", new object[] { PACK_MONTH });

                Exporter.Append(KUModels, "KeihenData");
                ResultExport = Exporter.Flush();

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;

                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Length", Convert.ToString(ResultExport.Length));

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(ResultExport);
                Response.End();
            }
            catch(Exception e)
            {
                Exporter.Flush();
                throw (e);
            }
        }

        public FileContentResult DownloadUploadKeihenTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "upload_keihen_template.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/Keihen/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] ImageData = new byte[fs.Length];
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return ImageData; //return the byte data
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/Keihen/TempUpload/");
                return _uploadDirectory;
            }
        }

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("FileUpload", ValidationSettings, FileUploadComplete);
        }

        public ActionResult DownloadCallback()
        {
            return null;
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                if (!Directory.Exists(_UploadDirectory))
                    Directory.CreateDirectory(_UploadDirectory);

                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        [HttpPost]
        public ActionResult KeihenDataUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        private void AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string function = "99998";
            string module = "9";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_KEIHEN_UPLOAD";
            string sheetName = "KeihenData";

            message = "Starting Upload Keihen Data";
            location = "Keihen.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_KEIHEN_UPLOAD";
                location = "Keihen.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                db.ExecuteScalar<String>("ClearKeihenTempUpload", new object[] { tableFromTemp, username });

                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as PACK_MONTH, " +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as PART_NO, " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as DOCK_CD, " +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as CAR_CD, " +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as SUPPLIER_CD, " +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as S_PLANT_CD, " +
                                    @"IIf(IsNull(F7), Null, CStr(F7)) as N_1_QTY_SIGN, " +
                                    @"IIf(IsNull(F8), Null, CStr(F8)) as N_1_QTY_GAP, " +
                                    @"'" + username + "' as CREATED_BY ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    dtPatterns = DeleteEmptyRows(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    message = "Error : " + e.Message;
                    location = "Keihen.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //try
                    //{
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PACK_MONTH", "PACK_MONTH");
                    bulkCopy.ColumnMappings.Add("PART_NO", "PART_NO");
                    bulkCopy.ColumnMappings.Add("DOCK_CD", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("CAR_CD", "CAR_CD");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CD", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("S_PLANT_CD", "S_PLANT_CD");
                    bulkCopy.ColumnMappings.Add("N_1_QTY_SIGN", "N_1_QTY_SIGN");
                    bulkCopy.ColumnMappings.Add("N_1_QTY_GAP", "N_1_QTY_GAP");
                    bulkCopy.ColumnMappings.Add("CREATED_BY", "CREATED_BY");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");

                    try
                    {
                        message = "Insert Data to TB_T_KEIHEN_UPLOAD";
                        location = "Keihen.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "Keihen.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });
                        throw new Exception(ex.Message);
                    }
                }

                string validate = db.SingleOrDefault<string>("ValidateKeihenUpload", new object[] { processID });

                if (validate == "Error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found")
                {
                    TempData["IsValid"] = "empty;" + processID;
                    sts = "6";
                    message = "No Data Found";
                }
                else if (validate == "Already Exists")
                {
                    TempData["IsValid"] = "false;" + processID + ";Data with Same Production Month Already Exist in Kaihen Data";
                    sts = "6";
                    message = "Data with Same Production Month Already Exist in Kaihen Data";
                }
                else if (validate == "Multiple Month")
                {
                    TempData["IsValid"] = "false;" + processID + ";One File Upload Must Consist only 1 Production Month";
                    sts = "6";
                    message = "Multiple Month";
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }

                message = "Upload Finish";
                location = "Keihen.finish";
                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                                message,     // what
                                username,     // user
                                location,     // where
                                processID,     // pid OUTPUT
                                "MPCS00004INF",     // id
                                "",     // type
                                module,     // module
                                function,     // function
                                sts      // sts
                            });

            }
            catch (Exception exc)
            {

                TempData["IsValid"] = "false;" + processID + ";" + exc.Message;
                message = "Error : " + exc.Message;
                location = "Keihen.finish";
                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                                message,     // what
                                username,     // user
                                location,     // where
                                processID,     // pid OUTPUT
                                "MPCS00004INF",     // id
                                "",     // type
                                module,     // module
                                function,     // function
                                6      // sts
                            });
            }
            finally
            {
                excelConn.Close();
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);
            }
        }

        public DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            List<DataRow> drList = new List<DataRow>();
            foreach (DataRow dr in formattedTable.Rows)
            {
                int count = dr.ItemArray.Length;
                int nullcounter = 0;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])) || dr.ItemArray[i].ToString().Length <= 0)
                    {
                        nullcounter++;
                    }
                }

                //if (dr.ItemArray[1] == "NOTE : 1. Fill Action column with value (Blank/0 : Ignored; 1 : Insert, 2 : Edit, 3 : Delete")
                //{
                //    drList.Add(dr);
                //}

                if ((nullcounter == count - 2) || (nullcounter == count - 3))
                {
                    drList.Add(dr);
                }
            }

            for (int i = 0; i < drList.Count; i++)
            {
                formattedTable.Rows.Remove(drList[i]);
            }
            formattedTable.AcceptChanges();

            return formattedTable;
        }

        public string CalculateKeihen(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("CalculateKeihenSummary", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }

            return message;
        }

        public void DownloadInvalidKeihenData(string process_id)
        {
            IDBContext db = DbContext;
            IExcelWriter Exporter = ExcelWriter.GetInstance();
            byte[] ResultExport = null;
            string FileNames = "KeihenDataInvalid_" + process_id + ".xls";
            List<KeihenDataInvalidModel> KUModels = new List<KeihenDataInvalidModel>();

            try
            {
                KUModels = db.Fetch<KeihenDataInvalidModel>("GetInvalidKeihenData", new object[] { process_id });

                Exporter.Append(KUModels, "KeihenData");
                ResultExport = Exporter.Flush();

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;

                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Length", Convert.ToString(ResultExport.Length));

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(ResultExport);
                Response.End();
            }
            catch (Exception e)
            {
                Exporter.Flush();
                throw (e);
            }
        }
        #endregion

        #region Validation (Revision Keihen)
        public ActionResult CalculatedKeihenTab()
        {
            return PartialView("PartialCalculatedKeihenTab");
        }

        public ActionResult ReloadCalculatedKeihenGrid(string PACK_MONTH = "x", string isSearch = "N")
        {
            CalculatedKeihenData CData = Model.GetModel<CalculatedKeihenData>();
            IDBContext db = DbContext;
            List<CalculatedKeihenModel> CModel = new List<CalculatedKeihenModel>();

            try
            {
                if (isSearch == "Y")
                {
                    CModel = db.Fetch<CalculatedKeihenModel>("GetCalculatedKeihenData", new object[] { PACK_MONTH });
                }
                CData.CModel = CModel;
                Model.AddModel(CData);
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message;
            }

            return PartialView("PartialCalculatedKeihenGrid", Model);
        }

        public string UpdateReminder(string pack_month, string reminder_start, string reminder_end)
        {
            string result = "";
            IDBContext db = DbContext;

            try
            {
                result = db.ExecuteScalar<string>("UpdateKeihenReminder", new object[] { pack_month, reminder_start, reminder_end });
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        public void DownloadCalculatedKeihenData(string PACK_MONTH = "x")
        {
            IDBContext db = DbContext;
            IExcelWriter Exporter = ExcelWriter.GetInstance();
            byte[] ResultExport = null;
            string FileNames = "KeihenValidationData_" + PACK_MONTH + ".xls";
            List<CalculatedKeihenUploadModel> KUModels = new List<CalculatedKeihenUploadModel>();
            List<string> footerText = new List<string>();

            
                

            try
            {
                footerText.Add("NOTE : 1. Fill Action column with value (Blank/0 : Ignored; 1 : Insert, 2 : Edit, 3 : Delete");

                KUModels = db.Fetch<CalculatedKeihenUploadModel>("GetCalculatedKeihenDataForDownload", new object[] { PACK_MONTH });

                ResultExport = Exporter.Write(KUModels, "KeihenValidation", footerText.ToArray());
                //Exporter.Append(KUModels, "KeihenValidation");
                //ResultExport = Exporter.Flush();
                Exporter.Flush();

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;

                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Length", Convert.ToString(ResultExport.Length));

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(ResultExport);
                Response.End();
            }
            catch(Exception e)
            {
                Exporter.Flush();
                throw (e);
            }
        }

        [HttpPost]
        public void OnUploadRevisionCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("RevisionFileUpload", ValidationSettings, FileUploadComplete);
        }

        [HttpPost]
        public ActionResult RevisionKeihenDataUpload(string filePath)
        {
            AllProcessRevisionUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        private void AllProcessRevisionUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string function = "99997";
            string module = "9";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_KEIHEN_UPLOAD_REVISION";
            string sheetName = "KeihenValidation";

            message = "Starting Upload Keihen Revision Data";
            location = "Keihen.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_KEIHEN_UPLOAD_REVISION";
                location = "Keihen.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                db.ExecuteScalar<String>("ClearKeihenTempUpload", new object[] { tableFromTemp, username });

                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as R_ACTION, " +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as PACK_MONTH, " +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as PART_NO, " +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as PART_NAME, " +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as DOCK_CD, " +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as CAR_CD, " +
                                    @"IIf(IsNull(F7), Null, CStr(F7)) as SUPPLIER_CD, " +
                                    @"IIf(IsNull(F8), Null, CStr(F8)) as S_PLANT_CD, " +
                                    @"IIf(IsNull(F9), Null, CStr(F9)) as ORIGINAL_VOLUME_N_1, " +
                                    @"IIf(IsNull(F10), Null, CStr(F10)) as NEW_VOLUME_N_1, " +
                                    @"IIf(IsNull(F11), Null, CStr(F11)) as FLUCTUATION, " +
                                    @"'" + username + "' as CREATED_BY ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    dtPatterns = DeleteEmptyRows(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    message = "Error : " + e.Message;
                    location = "Keihen.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("R_ACTION", "ACTION");
                    bulkCopy.ColumnMappings.Add("PACK_MONTH", "PACK_MONTH");
                    bulkCopy.ColumnMappings.Add("PART_NO", "PART_NO");
                    bulkCopy.ColumnMappings.Add("PART_NAME", "PART_NAME");
                    bulkCopy.ColumnMappings.Add("DOCK_CD", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("CAR_CD", "CAR_CD");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CD", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("S_PLANT_CD", "S_PLANT_CD");
                    bulkCopy.ColumnMappings.Add("ORIGINAL_VOLUME_N_1", "ORIGINAL_VOLUME_N_1");
                    bulkCopy.ColumnMappings.Add("NEW_VOLUME_N_1", "NEW_VOLUME_N_1");
                    bulkCopy.ColumnMappings.Add("FLUCTUATION", "FLUCTUATION");
                    bulkCopy.ColumnMappings.Add("CREATED_BY", "CREATED_BY");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");

                    try
                    {
                        message = "Insert Data to TB_T_KEIHEN_UPLOAD_REVISION";
                        location = "Keihen.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "Keihen.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });
                        throw new Exception(ex.Message);
                    }
                }

                string validate = db.SingleOrDefault<string>("ValidateKeihenRevisionUpload", new object[] { processID });

                if (validate == "Error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found")
                {
                    TempData["IsValid"] = "empty;" + processID;
                    sts = "6";
                    message = "No Data Found";
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }

                message = "Upload Finish";
                location = "Keihen.finish";
                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                                message,     // what
                                username,     // user
                                location,     // where
                                processID,     // pid OUTPUT
                                "MPCS00004INF",     // id
                                "",     // type
                                module,     // module
                                function,     // function
                                sts      // sts
                            });

            }
            catch (Exception exc)
            {

                TempData["IsValid"] = "false;" + processID + ";" + exc.Message;
                message = "Error : " + exc.Message;
                location = "Keihen.finish";
                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                                message,     // what
                                username,     // user
                                location,     // where
                                processID,     // pid OUTPUT
                                "MPCS00004INF",     // id
                                "",     // type
                                module,     // module
                                function,     // function
                                6      // sts
                            });
            }
            finally
            {
                excelConn.Close();
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);
            }
        }

        public void DownloadInvalidRevisionKeihenData(string process_id)
        {
            IDBContext db = DbContext;
            IExcelWriter Exporter = ExcelWriter.GetInstance();
            byte[] ResultExport = null;
            string FileNames = "KeihenDataInvalid_" + process_id + ".xls";
            List<CalculatedKeihenUploadInvalidModel> KUModels = new List<CalculatedKeihenUploadInvalidModel>();
            List<string> footerText = new List<string>();

            try
            {
                footerText.Add("NOTE : 1. Fill Action column with value (Blank/0 : Ignored; 1 : Insert, 2 : Edit, 3 : Delete");

                KUModels = db.Fetch<CalculatedKeihenUploadInvalidModel>("GetInvalidRevisionKeihenData", new object[] { process_id });

                ResultExport = Exporter.Write(KUModels, "KeihenValidation", footerText.ToArray());
                //Exporter.Append(KUModels, "KeihenValidation");
                //ResultExport = Exporter.Flush();
                Exporter.Flush();

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;

                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Length", Convert.ToString(ResultExport.Length));

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(ResultExport);
                Response.End();
            }
            catch (Exception e)
            {
                Exporter.Flush();
                throw (e);
            }
        }

        public string ReviseKeihen(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("ReviseKeihenData", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }

            return message;
        }

        public string ApproveKeihen(string pack_month)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                message = db.ExecuteScalar<string>("ApproveKeihenData", new object[] { pack_month, user });
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }

            return message;
        }
        #endregion
    }
}