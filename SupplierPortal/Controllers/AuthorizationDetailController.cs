﻿using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Portal.Models.AuthorizationDetail;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;

namespace Portal.Controllers
{
    public class AuthorizationDetailController : BaseController
    {
        public AuthorizationDetailController()
            : base("Authorization Detail")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {

                AuthorizationDetailModel authorizationmodel = new AuthorizationDetailModel();
                Model.AddModel(authorizationmodel);

                var rolelookup = db.Fetch<RoleLookup>("GetRoleForLookup", new object[] { "" });
                var screenlookup = db.Fetch<ScreenLookup>("GetScreenLookup");

                ViewData["RoleLookup"] = rolelookup;
                ViewData["ScreenLookup"] = screenlookup;
            }
            catch (Exception e)
            {
                //Let Elmah Catch it
            }
            finally
            {
                db.Close();
            }
        }

        public ActionResult PartialHeader()
        {
            return PartialView("PartialAuthorizationDetailHeader");
        }

        public ActionResult PartialHeaderButton()
        {
            return PartialView("PartialAuthorizationDetailButtonHeader");
        }

        public ActionResult PartialAuthDatas()
        {
            AuthorizationDetailModel um = new AuthorizationDetailModel();
            Model.AddModel(um);

            return PartialView("PartialAuthorizationDetailGrid", Model);
        }

        public ActionResult getRoleLookup()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                string p_system = Request.Params["p_system"] == null ? "" : Request.Params["p_system"];

                TempData["GridName"] = "grlRole";
                List<RoleLookup> RoleLookup = db.Fetch<RoleLookup>("GetRoleForLookup", new object[] { p_system });
                ViewData["RoleLookup"] = RoleLookup;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["RoleLookup"]);
        }

        public ActionResult getScreenLookup()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                TempData["GridName"] = "grlScreen";
                List<ScreenLookup> RoleLookup = db.Fetch<ScreenLookup>("GetScreenLookup");
                ViewData["ScreenLookup"] = RoleLookup;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["ScreenLookup"]);
        }

        public ActionResult getSystemLookup()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                TempData["GridName"] = "grlSystem";
                List<SystemLookup> SystemData = db.Fetch<SystemLookup>("GetSystemIDForLookup");
                ViewData["SystemLookup"] = SystemData;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["SystemLookup"]);
        }

        public ActionResult getObjectLookup()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                string p_screen = Request.Params["p_screen"] == null ? "" : Request.Params["p_screen"];

                TempData["GridName"] = "grlObject";
                List<ObjectLookup> ObjectData = db.Fetch<ObjectLookup>("GetObjectLookup", new object[] { p_screen });
                ViewData["ObjectLookup"] = ObjectData;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["ObjectLookup"]);
        }

        public ActionResult getActionLookup()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                TempData["GridName"] = "grlAction";
                List<ActionLookup> ActionData = db.Fetch<ActionLookup>("GetActionLookup");
                ViewData["ActionLookup"] = ActionData;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["ActionLookup"]);
        }

        public ActionResult getAuthGroupLookup()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                TempData["GridName"] = "grlAuthGroup";
                List<AuthGroupLookup> AuthGroupData = db.Fetch<AuthGroupLookup>("GetAuthGroupLookup");
                ViewData["AuthGroupLookup"] = AuthGroupData;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["AuthGroupLookup"]);
        }

        public ActionResult getRoleLookupAddForm()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                string p_system = Request.Params["p_system"] == null ? "" : Request.Params["p_system"];

                TempData["GridName"] = "grlRoleAdd";
                List<RoleLookup> RoleLookup = db.Fetch<RoleLookup>("GetRoleForLookup", new object[] { p_system });
                ViewData["RoleLookup"] = RoleLookup;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["RoleLookup"]);
        }

        public ActionResult getScreenLookupAddForm()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                TempData["GridName"] = "grlScreenAdd";
                List<ScreenLookup> RoleLookup = db.Fetch<ScreenLookup>("GetScreenLookup");
                ViewData["ScreenLookup"] = RoleLookup;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("GridLookup/PartialGrid", ViewData["ScreenLookup"]);
        }

        public ActionResult PartialAuthGridView()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                string p_role = Request.Params["p_role"];
                string p_screen = Request.Params["p_screen"];

                string clear = Request.Params["clear"];
         
                AuthorizationDetailModel um = Model.GetModel<AuthorizationDetailModel>();
                if (clear == "N")
                {
                    um.AuthDetailData = db.Fetch<AuthorizationDetailData>("GetAuthorizationDetailData", new object[] { p_role, p_screen });
                }
                Model.AddModel(um);
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }

            return PartialView("PartialAuthorizationDetailGrid", Model);
        }

        public ActionResult PartialPopUpFormAuthDetail()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                AuthorizationDetailModel um = new AuthorizationDetailModel();
                Model.AddModel(um);

                var rolelookup = db.Fetch<RoleLookup>("GetRoleForLookup", new object[] { "" });
                var screenlookup = db.Fetch<ScreenLookup>("GetScreenLookup");
                var SystemData = db.Fetch<SystemLookup>("GetSystemIDForLookup");
                var ObjectData = db.Fetch<ObjectLookup>("GetObjectLookup", new object[] { "" });
                var ActionData = db.Fetch<ActionLookup>("GetActionLookup");
                var AuthGroupData = db.Fetch<AuthGroupLookup>("GetAuthGroupLookup");

                ViewData["AuthGroupLookup"] = AuthGroupData;
                ViewData["ActionLookup"] = ActionData;
                ViewData["ObjectLookup"] = ObjectData;
                ViewData["SystemLookup"] = SystemData;
                ViewData["RoleLookup"] = rolelookup;
                ViewData["ScreenLookup"] = screenlookup;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Close();
            }
            return PartialView("PartialFormAuthDetail");

        }

        public string GetEditData(string p_system, string p_role, string p_screen, string p_object, string p_action) 
        {
            string result = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                result = db.SingleOrDefault<string>("GetEditedAuthDetail", new object[] { p_system, p_role, p_screen, p_object, p_action });
            }
            catch (Exception e)
            {
                result = "Err|" + e.Message;
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public string Save(string p_System, string p_Role, string p_Screen, string p_ScreenAuth, 
                           string p_Action, string p_ActionAuth, string p_Object, string p_ObjectAuth,
                           string p_AuthGroup, string p_Mode)
        { 
            string result = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                result = db.SingleOrDefault<string>("SaveAuthDetail", new object[] { 
                                            p_System,
                                            p_Role,
                                            p_Screen,
                                            p_ScreenAuth,
                                            p_Object,
                                            p_ObjectAuth,
                                            p_Action,
                                            p_ActionAuth,
                                            p_AuthGroup,
                                            AuthorizedUser.Username,
                                            p_Mode
                                    });
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public string Delete(string p_deleteparams)
        {
            string result = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                result = db.SingleOrDefault<string>("DeleteAuthDetail", new object[] { p_deleteparams });
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public FileContentResult DownloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "authorization_detail_template.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/AuthorizationDetail/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] ImageData = new byte[fs.Length];
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return ImageData; //return the byte data
        }

        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private byte[] Pack(byte[] b, int method)
        {
            MemoryStream m = new MemoryStream();
            byte[] x = null;
            switch (method)
            {
                case 1:
                    using (GZipStream g = new GZipStream(m, CompressionMode.Compress))
                    {
                        g.Write(b, 0, b.Length);
                    }
                    x = m.ToArray();
                    break;
                case 2:
                    using (DeflateStream d = new DeflateStream(m, CompressionMode.Compress))
                    {
                        d.Write(b, 0, b.Length);
                    }
                    x = m.ToArray();
                    break;
                default:
                    x = new byte[b.Length];
                    b.CopyTo(x, 0);
                    break;
            }
            return x;
        }
        
        public void DownloadAuthData(string p_role, string p_screen)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            byte[] datas = null;
            //IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;
            
            try
            {
                //datas = exporter.Flush();

                List<AuthorizationDetailData> authdata = db.Fetch<AuthorizationDetailData>("GetAuthorizationDetailData", new object[] { p_role, p_screen });

                //if (authdata.Count > 30000)
                //{
                //    List<List<AuthorizationDetailData>> listauth = new List<List<AuthorizationDetailData>>();
                //    fileName = "AuthorizationDetailData_" + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + ".xls";

                //    listauth = authdata.Select((x, i) => new { Index = i, Value = x })
                //                       .GroupBy(x => Convert.ToInt32(Decimal.Ceiling(x.Index / 30000)))
                //                       .Select(x => x.Select(v => v.Value).ToList()).ToList();

                //    int index = 1;
                //    foreach (var d in listauth)
                //    {
                //        exporter.Append(d, "AuthDetail_" + index.ToString());
                //        index++;
                //    }

                //    datas = exporter.Flush();
                //}
                //else
                //{
                //    fileName = "AuthorizationDetailData_" + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + ".xls";
                //    datas = exporter.Write(authdata, "AuthorizationDetail");

                //    Response.Clear();
                //    Response.Cache.SetCacheability(HttpCacheability.Private);
                //    Response.Expires = -1;
                //    Response.Buffer = true;

                //    Response.ContentType = "application/octet-stream";
                //    Response.AddHeader("Content-Length", Convert.ToString(datas.Length));

                //    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
                //    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                //    Response.BinaryWrite(datas);
                //    Response.End();
                //}

                fileName = "AuthorizationDetailData_" + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + ".csv";
                StringBuilder csv = new StringBuilder();
                string[] columnName = { "System Name", "Role Name", "Screen Name", "Screen Auth.",
                                            "Object Name", "Object Auth.", "Action Name", "Action Auth.",
                                            "Created By", "Created Dt.", "Changed By", "Changed Dt."};

                for (int j = 0; j < columnName.Length; j++)
                {
                    csv.Append(columnName[j]);
                    csv.Append(",");
                }

                csv.Append(Environment.NewLine);

                foreach (var d in authdata)
                {
                    csv.Append(d.system_name); csv.Append(",");
                    csv.Append(d.role_name); csv.Append(",");
                    csv.Append(d.screen_name); csv.Append(",");
                    csv.Append(d.screen_auth); csv.Append(",");
                    csv.Append(d.object_name); csv.Append(",");
                    csv.Append(d.object_auth); csv.Append(",");
                    csv.Append(d.action_name); csv.Append(",");
                    csv.Append(d.action_auth); csv.Append(",");
                    csv.Append(d.created_by); csv.Append(",");
                    csv.Append(d.created_dt); csv.Append(",");
                    csv.Append(d.changed_by); csv.Append(",");
                    csv.Append(d.changed_dt);
                    csv.Append(Environment.NewLine);
                }

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;
                datas = UTF8Encoding.UTF8.GetBytes(csv.ToString());
                var encodings = Request.Headers["Accept-Encoding"];
                if (encodings.Contains("gzip"))
                {
                    Response.AddHeader("Content-encoding", "gzip");
                    datas = Pack(datas, 1);
                }
                else if (encodings.Contains("deflate"))
                {
                    Response.AddHeader("Content-encoding", "deflate");
                    datas = Pack(datas, 2);
                }

                Response.ContentType = "application/octet-stream";

                Response.AddHeader("Content-Length", Convert.ToString(datas.Length));
                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(datas);
                Response.End();

                result = "Success Download " + authdata.Count.ToString() + " row(s) data";
            }
            catch (Exception e)
            {
                result = e.Message;
                //datas = exporter.Flush();
            }
            finally
            {
                db.Close();
                //datas = exporter.Flush();
            }
        }

        [HttpPost]
        public ActionResult AuthorizationDetailUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        public DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            List<DataRow> drList = new List<DataRow>();
            foreach (DataRow dr in formattedTable.Rows)
            {
                int count = dr.ItemArray.Length;
                int nullcounter = 0;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])) || dr.ItemArray[i].ToString().Length <= 0)
                    {
                        nullcounter++;
                    }
                }

                if (nullcounter == count - 2)
                {
                    drList.Add(dr);
                }
            }

            for (int i = 0; i < drList.Count; i++)
            {
                formattedTable.Rows.Remove(drList[i]);
            }
            formattedTable.AcceptChanges();

            return formattedTable;
        }

        private void AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            AuthorizationDetailModel UploadModel = new AuthorizationDetailModel();

            string function = "99999";
            string module = "9";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_AUTHORIZATION_DETAIL_UPLOAD";
            string sheetName = "AuthDetail";

            message = "Starting Upload Authorization Mapping";
            location = "AuthorizationDetailUpload.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_AUTHORIZATION_DETAIL_UPLOAD";
                location = "AuthorizationDetailUpload.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });

                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
                
                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as SYSTEM_ID," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as ROLE_ID," +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as SCREEN_ID," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as ACTION_ID," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as OBJECT_ID," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as AUTHORIZATION_GROUP_ID," +
                                    @"IIf(IsNull(F7), Null, CStr(F7)) as SCREEN_AUTH," +
                                    @"IIf(IsNull(F8), Null, CStr(F8)) as ACTION_AUTH," +
                                    @"IIf(IsNull(F9), Null, CStr(F9)) as OBJECT_AUTH ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    dtPatterns = DeleteEmptyRows(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    message = "Error : " + e.Message;
                    location = "AuthorizationDetailUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //try
                    //{
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("SYSTEM_ID", "SYSTEM_ID");
                    bulkCopy.ColumnMappings.Add("ROLE_ID", "ROLE_ID");
                    bulkCopy.ColumnMappings.Add("SCREEN_ID", "SCREEN_ID");
                    bulkCopy.ColumnMappings.Add("ACTION_ID", "ACTION_ID");
                    bulkCopy.ColumnMappings.Add("OBJECT_ID", "OBJECT_ID");
                    bulkCopy.ColumnMappings.Add("AUTHORIZATION_GROUP_ID", "AUTHORIZATION_GROUP_ID");
                    bulkCopy.ColumnMappings.Add("SCREEN_AUTH", "SCREEN_AUTH");
                    bulkCopy.ColumnMappings.Add("ACTION_AUTH", "ACTION_AUTH");
                    bulkCopy.ColumnMappings.Add("OBJECT_AUTH", "OBJECT_AUTH");

                    try
                    {
                        message = "Insert Data to TB_T_AUTHORIZATION_DETAIL_UPLOAD";
                        location = "AuthorizationDetailUpload.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "AuthorizationDetailUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });
                        throw new Exception(ex.Message);
                    }
                }

                string validate = db.SingleOrDefault<string>("ValidateAuthDetailUpload", new object[] { processID });

                if (validate == "Error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found")
                {
                    TempData["IsValid"] = "empty;" + processID;
                    sts = "6";
                    message = "No Data Found";
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }

                message = "Upload Finish";
                location = "AuthorizationDetailUpload.finish";
                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                                message,     // what
                                username,     // user
                                location,     // where
                                processID,     // pid OUTPUT
                                "MPCS00004INF",     // id
                                "",     // type
                                module,     // module
                                function,     // function
                                sts      // sts
                            });

            }
            catch (Exception exc)
            {

                TempData["IsValid"] = "false;" + processID + ";" + exc.Message;
                message = "Error : " + exc.Message;
                location = "AuthorizationDetailUpload.finish";
                processID = db.ExecuteScalar<string>(
                        "GenerateProcessId",
                        new object[] { 
                                message,     // what
                                username,     // user
                                location,     // where
                                processID,     // pid OUTPUT
                                "MPCS00004INF",     // id
                                "",     // type
                                module,     // module
                                function,     // function
                                6      // sts
                            });
            }
            finally
            {
                excelConn.Close();
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);
            }
        }

        public string MoveAuthDetail(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("MoveAuthDetail", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }

            return message;
        }

        public void UploadInvalid(string processId)
        {
            string result = "";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            byte[] datas = null;
            string fileName = String.Empty;
            IExcelWriter exporter = ExcelWriter.GetInstance();

            try
            {
                datas = exporter.Flush();
                List<AuthorizationDetailError> authdata = db.Fetch<AuthorizationDetailError>("GetAuthorizationDetailDataError", new object[] { processId });

                fileName = "AuthorizationDetailError_" + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + ".xls";
                datas = exporter.Write(authdata, "AuthDetail");

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;

                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Length", Convert.ToString(datas.Length));

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(datas);
                Response.End();

                //fileName = "AuthorizationDetailDataError_" + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + ".csv";
                //StringBuilder csv = new StringBuilder();
                //string[] columnName = { "System ID", "Role ID", "Screen ID", "Action ID", "Object ID", "Screen Auth.",
                //                        "Object Auth.",  "Action Auth.", "Authorization Group ID", "Message"};

                //for (int j = 0; j < columnName.Length; j++)
                //{
                //    csv.Append(columnName[j]);
                //    csv.Append(",");
                //}

                //csv.Append(Environment.NewLine);

                //foreach (var d in authdata)
                //{
                //    csv.Append(d.system_id); csv.Append(",");
                //    csv.Append(d.role_id); csv.Append(",");
                //    csv.Append(d.screen_id); csv.Append(",");
                //    csv.Append(d.action_id); csv.Append(",");
                //    csv.Append(d.object_id); csv.Append(",");
                //    csv.Append(d.screen_auth); csv.Append(",");
                //    csv.Append(d.action_auth); csv.Append(",");
                //    csv.Append(d.object_auth); csv.Append(",");
                //    csv.Append(d.authorization_group_id); csv.Append(",");
                //    csv.Append(d.message); csv.Append(",");
                //    csv.Append(Environment.NewLine);
                //}

                //Response.Clear();
                //Response.Cache.SetCacheability(HttpCacheability.Private);
                //Response.Expires = -1;
                //Response.Buffer = true;
                //datas = UTF8Encoding.UTF8.GetBytes(csv.ToString());
                //var encodings = Request.Headers["Accept-Encoding"];
                //if (encodings.Contains("gzip"))
                //{
                //    Response.AddHeader("Content-encoding", "gzip");
                //    datas = Pack(datas, 1);
                //}
                //else if (encodings.Contains("deflate"))
                //{
                //    Response.AddHeader("Content-encoding", "deflate");
                //    datas = Pack(datas, 2);
                //}

                //Response.ContentType = "application/octet-stream";

                //Response.AddHeader("Content-Length", Convert.ToString(datas.Length));
                //Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
                //Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                //Response.BinaryWrite(datas);
                //Response.End();
            }
            catch (Exception e)
            {
                result = e.Message;
                datas = exporter.Flush();
            }
            finally
            {
                db.Close();
                datas = exporter.Flush();
            }
        }

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("ADUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/AuthorizationDetail/TempUpload/");
                return _uploadDirectory;
            }
        }
    }
}
