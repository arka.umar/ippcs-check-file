﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.ARS;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;

using Portal.Models.Supplier;
using Portal.Models.Dock;
using Portal.Models.SupplierFeedbackSummary;

namespace Portal.Controllers
{
    public class ARSController : BaseController
    {
        public ARSController()
            : base("ARS Detail : ")
        {
        }

        protected override void StartUp()
        {

        }

        List<DockData> _orderInquiryDockModel = null;
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }


        List<Supplier> _supplierModel = null;
        private List<Supplier> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<Supplier>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        SupplierFeedbackSummary _supplierFeedbackSummaryParameterModel = null;
        private SupplierFeedbackSummary _SupplierFeedbackSummaryParameterModel
        {
            get
            {
                if (_supplierFeedbackSummaryParameterModel == null)
                    _supplierFeedbackSummaryParameterModel = new SupplierFeedbackSummary();

                return _supplierFeedbackSummaryParameterModel;
            }
            set
            {
                _supplierFeedbackSummaryParameterModel = value;
            }
        }


        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

       

        protected override void Init()
        {
            IDBContext db = DbContext;

            ARSModel modelArsInfo = new ARSModel();
            modelArsInfo.ARSDatas = getArsInfo("","","");
            Model.AddModel(modelArsInfo);
        }

        [HttpGet]
        public void DownloadSheet()
        {

            string fileName = "ARS" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
             
            IEnumerable<ARSData> listGeneral = db.Query<ARSData>("GetARSInfo", new object[] { });
           IExcelWriter exporter = ExcelWriter.GetInstance();
           exporter.Append(listGeneral, "ARS");
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();
        }

        public ActionResult PartialHeaderDockGridHeaderARS()
        {
            TempData["GridName"] = "OIHDockCodeARS";
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DOCK_CODE", "ARS", "OIHDockCodeARS");

            ViewData["GridDock"] = docks;

            //TempData["GridName"] = "OIHDockCodePartInfo";
            //ViewData["GridDock"] = GetAllDock();

            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> PartIndoDockModel = new List<DockData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return PartIndoDockModel;
        }


        public ActionResult PartialHeader()
        {
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrder", "OIHDockOption");
            _OrderInquiryDockModel = DockCodes;

            Model.AddModel(_OrderInquiryDockModel);

            ViewData["GridDock"] = _OrderInquiryDockModel;
            return PartialView("PartialHeader",Model);

        }


        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> SISupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            SISupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return SISupplierModel;

        }
        public ActionResult PartialDetail()
        {

            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "PartInfo", "OIHSupplierOptionPartInfo");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "PartInfo", "OIHDockCodePartInfo");

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            ARSModel model = Model.GetModel<ARSModel>();
            model.ARSDatas = getArsInfo(Request.Params["DockCode"],SupplierCodeLDAP, DockCodeLDAP);

            Model.AddModel(model);

            return PartialView("PartialDetail", Model);
        }

        protected List<ARSData> getArsInfo(string dockCdParam, string SupplierCodeLdap, string DockCdLdap)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<ARSData> listTooltipDatas = new List<ARSData>();
            
            listTooltipDatas = db.Query<ARSData>("GetARSInfo", new object[] { dockCdParam,SupplierCodeLdap,DockCdLdap}).ToList();

             db.Close();
            return listTooltipDatas;
        }



    }
}