﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.InvoiceTaxCreationByMonth;
using Portal.Models.Globals;
//using Toyota.Common.Web.Notification;

namespace Portal.Controllers
{
    public class InvoiceTaxCreationByMonthController : BaseController
    {
        public InvoiceTaxCreationByMonthController() : base("Monthly Invoice Tax Creation")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            List<Supplier> listSupplier = new List<Supplier>();

            for (int i = 1; i <= 100; i++)
            {
                listSupplier.Add(new Supplier
                {
                    //SupplierCode = "5000" + i.ToString("0##"),
                    SupplierName = "Supplier " + i
                });
            }

            ViewData["SupplierData"] = listSupplier;
            
            InvoiceTaxCreationByMonthModel mdl = new InvoiceTaxCreationByMonthModel();

            List<InvoiceTaxCreationByMonthDetail> lst = new List<InvoiceTaxCreationByMonthDetail>();

            mdl.InvoiceTaxCreationByMonths.Add(new InvoiceTaxCreationByMonth()
            {
                InvoiceTaxNo="1",
                InvoiceTaxDate = DateTime.Now,
                InvoiceDate = DateTime.Now,
            });

            for (int i = 1; i < 100; i++)
            {
                lst.Add(new InvoiceTaxCreationByMonthDetail()
                {
                   //Editors = "New Edit Delete",
                    InvoiceNo = i.ToString().PadLeft(3,'0') + "/TMMIN/12",
                    InvoiceDate = DateTime.Now,
                    InvoiceTaxAmount = "500.00",
                    TurnOverCurrency = "IDR" ,
                    TurnOverAmount = "5,000.00",
                    TaxStatus = "Created",
                });
            }

            mdl.InvoiceTaxCreationByMonthDetails = lst;

            InvoiceTaxPopupModel InvoiceTaxPopupModel = new InvoiceTaxPopupModel();
            InvoiceTaxPopupModel.InvoiceTaxCreationPopups.Add(new InvoiceTaxPopup
            {

            });

            Model.AddModel(mdl);
            Model.AddModel(InvoiceTaxPopupModel);
        }

        public ActionResult InvoiceTaxCreationByMonthPartial()
        {
            return PartialView("InvoiceTaxCreationByMonthPartial", Model);
        }

        public ActionResult InvoiceTaxCreationByMonthInlineAddNew()
        {
            return PartialView("InvoiceTaxCreationByMonthPartial", Model);
        }

        public ActionResult InvoiceTaxCreationByMonthInlineUpdate()
        {
            return PartialView("InvoiceTaxCreationByMonthPartial", Model);
        }

        public ActionResult InvoiceTaxCreationByMonthInlineDelete()
        {
            return PartialView("InvoiceTaxCreationByMonthPartial", Model);
        }

        public ActionResult InvoiceTaxPopup()
        {
            return PartialView("InvoiceTaxPopup", Model);
        }
    }
}
