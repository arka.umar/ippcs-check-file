using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Portal.ExcelUpload;
using Portal.Models;
using Portal.Models.EmergencyOrder;
using Portal.Models.Globals;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.MVC;

namespace Portal.Controllers
{
    public class EmergencyOrderCreationController : BaseController
    {
        public string CookieName { get; set; }
        public string CookiePath { get; set; }
        public const string FN = "23005";
        public const string FNup = "23001";
        public const string MO = "2";
        public const string ERR_ID = "MPCS11000ERR";
        public string Uid
        {
            get
            {
                string u = AuthorizedUser.Username;
                if (string.IsNullOrEmpty(u))
                    u = "no0ne";
                return u;
            }
        }

        /// <summary>
        /// If the current response is a FileResult (an MVC base class for files) then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public EmergencyOrderCreationController()
            : base("Emergency Order Creation")
        {
            CookieName = "fileDownload";
            CookiePath = "/";
        }

        #region Model properties

        private List<EmergencyOrderDetail> _emergencyOrderDetailList = null;

        private List<EmergencyOrderDetail> _EmergencyOrderDetailList
        {
            get
            {
                if (_emergencyOrderDetailList == null)
                    _emergencyOrderDetailList = new List<EmergencyOrderDetail>();

                return _emergencyOrderDetailList;
            }
            set
            {
                _emergencyOrderDetailList = value;
            }
        }

        #endregion Model properties

        protected override void StartUp()
        {
            ViewData["EOT"] = GetEOType();
            
        }

        protected override void Init()
        {
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "EmergencyOrderCreation", "EOCHSupplierGridLookup");
            List<Dock> docks = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DOCK_CODE", "EmergencyOrderCreation", "EOCHDockGridLookup");

            ViewData["SupplierGridLookup"] = suppliers;
            ViewData["DockGridLookup"] = docks;
            ViewData["PartGridLookup"] = GetAllPart("", "", "", "");
        }

        #region Emergency Order Creation Controller

        #region View Controller

        public ActionResult PartialHeaderEmergencyOrderCreation()
        {
            return PartialView("EmergencyOrderCreationHeaderPartial", Model);
        }

        public ActionResult PartialHeaderDockGridLookupEmergencyOrderCreation()
        {
            #region Required model in partial page : for EOCHDockGridLookup

            TempData["GridName"] = "EOCHDockGridLookup";
            List<Dock> docks = Model.GetModel<User>().FilteringArea<Dock>(GetAllDock(), "DOCK_CODE", "EmergencyOrderCreation", "EOCHDockGridLookup");

            ViewData["DockGridLookup"] = docks;

            #endregion Required model in partial page : for EOCHDockGridLookup

            return PartialView("GridLookup/PartialGrid", ViewData["DockGridLookup"]);
        }

        public ActionResult PartialHeaderSupplierGridLookupEmergencyOrderCreation()
        {
            #region Required model in partial page : for EOCHDockGridLookup

            TempData["GridName"] = "EOCHSupplierGridLookup";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "EmergencyOrderCreation", "EOCHSupplierGridLookup");

            ViewData["SupplierGridLookup"] = suppliers;

            #endregion Required model in partial page : for EOCHDockGridLookup

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierGridLookup"]);
        }

        public ActionResult PartialPartGridLookupEmergencyOrderCreation()
        {
            #region Required model in partial page : for EOCHDockGridLookup

            TempData["GridName"] = "grlPartNo";
            ViewData["PartGridLookup"] = GetAllPart(
                    Request.Params["SupplierCode"],
                    Request.Params["SupplierPlant"],
                    Request.Params["DockCode"],
                    Request.Params["KanbanNo"]);

            #endregion Required model in partial page : for EOCHDockGridLookup

            return PartialView("GridLookup/PartialGrid", ViewData["PartGridLookup"]);
        }

        public string CheckBOPart(string PartNo, string SupplierCode, string SupplierPlantCode, string DockCode)
        {
            return Dbutil.GetValue("CheckBOPart", new object[] { PartNo, SupplierCode, SupplierPlantCode, DockCode });            
        }

        public ActionResult PartialDetailEmergencyOrderCreation()
        {
            #region Required model in partial page : for EmergencyOrderCreationDetailPartial

            string SupplierCode = "";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "EmergencyOrderCreation", "EOCHSupplierGridLookup");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            if (suppCode.Count == 1)
            {
                SupplierCode = suppliers[0].SUPPLIER_CODE;
            }
            _EmergencyOrderDetailList = GetAllEmergencyOrderDetail(SupplierCode);

            Model.AddModel(_EmergencyOrderDetailList);

            #endregion Required model in partial page : for EmergencyOrderCreationDetailPartial

            return PartialView("EmergencyOrderCreationDetailPartial", Model);
        }

        #endregion View Controller

        #region Database Controller
        private IEnumerable<ComboData> GetEOType()
        {
            IEnumerable<ComboData> coda = new List<ComboData>();
            coda = Dbutil.GetList<ComboData>("GetEOType", new object[] { });
            return coda;
        }


        private List<Dock> GetAllDock()
        {
            return Dbutil.GetList<Dock>("GetAllDock", new object[] { "" });
        }

        private List<Supplier> GetAllSupplier()
        {
            return Dbutil.GetList<Supplier>("GetAllSupplierGrid", new object[] { "" });
        }

        private List<Part> GetAllPart(string SupplierCode, string SupplierPlant, string DockCode, string KanbanNo)
        {
            return Dbutil.GetList<Part>("GetAllSupplierPartGrid", new object[] { SupplierCode, SupplierPlant, DockCode, KanbanNo });
        }

        private List<EmergencyOrderDetail> GetAllEmergencyOrderDetail(string SupplierCode)
        {
            List<EmergencyOrderDetail> emergencyOrderDetailList = new List<EmergencyOrderDetail>();
            IDBContext db = DbContext;
            try
            {
                emergencyOrderDetailList = db.Fetch<EmergencyOrderDetail>("GetEmergencyOrderPartTemporary", new object[] { Session.SessionID, AuthorizedUser.Username, SupplierCode });
            }
            catch (Exception exc) { exc.PutLog("", Uid, "GetEmergencyOrderPartTemporary", 0, ERR_ID, null, MO, FN, 1); }
            db.Close();

            return emergencyOrderDetailList;
        }

        public JsonResult GetPartDescription(string PartNo, string SupplierCode, string SupplierPlantCode, string DockCode)
        {
            return Json(Dbutil.GetList<SupplierPart>("GetPartDescription", new object[] { PartNo, SupplierCode, SupplierPlantCode, DockCode }));
        }

        public string GetBOMessage()
        {
             return Dbutil.GetValue("GetBOMessage");
        }

        public JsonResult GetPartDescriptionByKanban(string KanbanNo, string SupplierCode, string SupplierPlantCode, string DockCode)
        {
            return Json(Dbutil.GetList<SupplierPart>("GetPartDescriptionByKanban", new object[] { KanbanNo, SupplierCode, SupplierPlantCode, DockCode }));
        }

        public ContentResult GetDockInformation(string DockCode)
        {
            return Content(Dbutil.GetValue("GetDockInformation", new object[] { DockCode }));
        }

        #endregion Database Controller

        #endregion Emergency Order Creation Controller

        #region UPLOAD CONFIRMATION

        #region UPLOAD CONFIRMATION - ACTION

        [HttpPost]
        public ActionResult HeaderTabEOCConfirmation_Upload(string filePath)
        {
            ProcessAllEOCConfirmationUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        #endregion UPLOAD CONFIRMATION - ACTION

        #region UPLOAD CONFIRMATION - PROPERTIES

        private String _uploadDirectory = "";

        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/EmergencyOrderCreation/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";

        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private TableDestination _tableDestination = null;

        private TableDestination _TableDestination
        {
            get
            {
                if (_tableDestination == null)
                    _tableDestination = new TableDestination();

                return _tableDestination;
            }
            set
            {
                _tableDestination = value;
            }
        }

        private List<ErrorValidation> _errorValidationList = null;

        private List<ErrorValidation> _ErrorValidationList
        {
            get
            {
                if (_errorValidationList == null)
                    _errorValidationList = new List<ErrorValidation>();

                return _errorValidationList;
            }
            set
            {
                _errorValidationList = value;
            }
        }

        private long _processId = 0;

        private long ProcessId
        {
            get
            {
                //if (_processId == 0)
                //    _processId = Math.Abs(Environment.TickCount);

                return _processId;
            }
            set
            {
                _processId = value;
            }
        }

        private Int32 _seqNo = 0;

        private Int32 SeqNo
        {
            get
            {
                return ++_seqNo;
            }
        }

        #endregion UPLOAD CONFIRMATION - PROPERTIES

        #region UPLOAD CONFIRMATION - REQUEST

        private ArrayList GetUploadedFile()
        {
            ArrayList fileRowList = new ArrayList();

            try
            {
                // read uploaded excel file
                fileRowList = ExcelReader.ReaderExcel(
                    _TableDestination.filePath,
                    _TableDestination.sheetName,
                    Convert.ToInt32(_TableDestination.rowStart),
                    Convert.ToInt32(_TableDestination.columnStart),
                    Convert.ToInt32(_TableDestination.columnEnd));
            }
            catch (Exception exc)
            {
                throw new Exception("GetUploadedFile:" + exc.Message);
            }

            return fileRowList;
        }

        private String GetColumnName()
        {
            ArrayList columnNameArray = new ArrayList();

            List<ExcelReaderColumnName> columnNameList = Dbutil.GetList<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
            foreach (ExcelReaderColumnName columnName in columnNameList)
            {
                columnNameArray.Add(columnName.ColumnName);
            }
            columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
            return String.Join(",", columnNameArray.ToArray());
        }

        private String GetColumnValue(String fileRow, Int32 fileColumnCount)
        {
            ArrayList columnValueArray = new ArrayList();
            String[] dumpArray = new String[fileColumnCount];

            try
            {
                // create cleaned (from unused field) from uploaded excel file
                Array.Copy(fileRow.Split(';'), 1, dumpArray, 0, fileColumnCount);

                // create column value parameter
                columnValueArray.Add("'" + _TableDestination.functionID + "'");    // function id
                columnValueArray.Add("'" + _TableDestination.processID + "'");     // process id

                columnValueArray.Add(dumpArray[0]);     // action
                columnValueArray.Add(dumpArray[1]);     // d id
                columnValueArray.Add(dumpArray[2]);     // version
                columnValueArray.Add(dumpArray[3]);     // r plant cd
                columnValueArray.Add(dumpArray[4]);     // dock cd
                columnValueArray.Add(dumpArray[5]);     // s plant cd
                columnValueArray.Add(dumpArray[6]);     // pack month
                columnValueArray.Add(dumpArray[7]);     // car cd
                columnValueArray.Add(dumpArray[8]);     // part no
                columnValueArray.Add(dumpArray[9]);     // n volume
                columnValueArray.Add(dumpArray[10]);    // n + 1 volume
                columnValueArray.Add(dumpArray[11]);    // n + 2 volume
                columnValueArray.Add(dumpArray[12]);    // n + 3 volume
                columnValueArray.Add(dumpArray[13]);    // supplier cd
                columnValueArray.Add(dumpArray[14]);    // supplier name
            }
            catch (Exception exc)
            {
                throw new Exception("GetColumnValue:" + exc.Message);
            }

            return String.Join(",", columnValueArray.ToArray());
        }

        private EmergencyOrderModel ProcessAllEOCConfirmationUpload(string filePath)
        {
            IDBContext db = DbContext;
            string uid = (AuthorizedUser!=null)? AuthorizedUser.Username:"";
            ProcessId = ExceptionalHelper.PutLog("Upload Emergency Order", uid, "ProcessAllEOConfirmationUpload" , 0, "MPCS00002INF", "INF", "2", "25001", 0); 
            
            EmergencyOrderModel _UploadedEmergencyOrderModel = new EmergencyOrderModel();

            // table destination properties
            _TableDestination.functionID = "131";
            _TableDestination.processID = Convert.ToString(ProcessId);
            _TableDestination.filePath = filePath;
            _TableDestination.sheetName = "EmergencyOrder";
            _TableDestination.rowStart = "2";
            _TableDestination.columnStart = "1";
            _TableDestination.columnEnd = "46";
            _TableDestination.tableFromTemp = "TB_T_EMERGENCY_ORDER_UPLOAD";
            _TableDestination.tableToAccess = "TB_T_EMERGENCY_ORDER_VALIDATE";
            
            OleDbConnection excelConn = null;
           
            try
            {
               
                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableFromTemp });
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB

                // read uploaded excel file,
                // get temporary upload table column name,
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";
                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                excelCommand = new OleDbCommand(@"select " +
                                    _TableDestination.functionID + " as FUNCTION_ID, " +
                                    _TableDestination.processID + " as PROCESS_ID, " +
                                    @" *
                                from [" + _TableDestination.sheetName + "$]", excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);
                ds.Tables.Add(dtPatterns);

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    bulkCopy.DestinationTableName = _TableDestination.tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();
                    bulkCopy.ColumnMappings.Add("FUNCTION_ID", "FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");

                    bulkCopy.ColumnMappings.Add("Supplier Code", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("Supplier Plant", "SUPPLIER_PLANT");
                    bulkCopy.ColumnMappings.Add("Dock Code", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("Unique No", "KANBAN_NO");
                    bulkCopy.ColumnMappings.Add("Part No", "PART_NO");
                    bulkCopy.ColumnMappings.Add("Kanban Qty", "ORDER_LOT_SZ");

                    bulkCopy.ColumnMappings.Add("Address", "KANBAN_PRINT_ADDRESS");
                    bulkCopy.ColumnMappings.Add("Importir Info 1", "IMPORTIR_INFO");
                    bulkCopy.ColumnMappings.Add("Part Bar Code", "PART_BARCODE");
                    bulkCopy.ColumnMappings.Add("Progress Lane No", "PROGRESS_LANE_NO");
                    bulkCopy.ColumnMappings.Add("Conveyance No", "CONVEYANCE_NO");
                    bulkCopy.ColumnMappings.Add("Emergency Order Type", "EO_TYPE");
                #endregion EXECUTE EXCEL AS TEMP_DB

                    try
                    {
                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        ex.PutLog(null, uid, "Upload to Temp table", ProcessId, "MPCS00002ERR", "INF", "2", "25001", 1);                       
                        
                    }
                }

                String columnNameList = GetColumnName();

                // validate data in temporary uploaded table.
                db.Execute("ValidateEmergencyOrderCreationUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });

                // check for invalid data.
                try
                {
                    _ErrorValidationList = db.Fetch<ErrorValidation>("GetErrorMessageUpload", new object[] { _TableDestination.functionID, _TableDestination.processID });
                }
                catch (Exception exc)
                {
                    exc.PutLog(null, uid, "Upload to Temp table", ProcessId, "MPCS00002ERR", "INF", "2", "25001", 1);                      
                }


                if (_ErrorValidationList.Count == 0)
                {
                    db.ExecuteScalar<String>("InsertTempToDestinationTableUpload", new object[] {
                             _TableDestination.tableToAccess
                            , columnNameList, columnNameList
                            , _TableDestination.tableFromTemp });

                    TempData["IsValid"] = "true;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    ExceptionalHelper.PutLog(@"Insert upload success info into table log detail.
                        Insert data from temporary uploaded table into temporary validated table.
                        Read uploaded data from temporary validated table to transcation table.
                        set process validation status...!!!"
                        , uid, "Insert Temp to Upload Table", ProcessId, "MPCS00001INF", "INF", "2", FN, 0);
                }
                else
                {
                    // set process validation status.
                    TempData["IsValid"] = "false;" + _TableDestination.processID + ";" + _TableDestination.functionID + ";" + _TableDestination.tableFromTemp + ";" + _TableDestination.tableToAccess;
                    ExceptionalHelper.PutLog(@"Insert upload fail info into table log detail set process validation status...!!!"
                        , uid, "Validation fails", ProcessId, "MPCS00001INF", "INF", "2", FN, 2);
                }
                ExceptionalHelper.PutLog("Upload DONE", uid, "Emergency Upload Done", ProcessId, "MPCS00001INF", "INF", "2", FN, 0);
            }
            catch (Exception exc)
            {
                exc.PutLog("insert read process error info into table log detail", uid, "UPLD ERROR", ProcessId, "MPCS00002ERR", "ERR", "2", FN, 1);                
            }
            finally
            {
                excelConn.Close();

                db.Close();
                if (System.IO.File.Exists(_TableDestination.filePath))
                    System.IO.File.Delete(_TableDestination.filePath);
            }

            return _UploadedEmergencyOrderModel;
        }

        [HttpPost]
        public void ClearAllFeedbackPartUpload(String processId, String functionId, String temporaryTableUpload, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            try
            {
                // clear data in temporary uploaded table.
                db.Execute("ClearFeedbackPartUpload", new object[] { processId, functionId, temporaryTableUpload, temporaryTableValidate });
            }
            catch (Exception ex)
            {
                ex.PutLog(null, UserID, "ClearFeedbackUpload", Convert.ToInt64(processId), "MPCS00002ERR");
                Response.StatusCode = 500;
                Response.AddHeader("ERR", ex.Message);
            }
            finally
            {
                db.Close();
            }
        }

        [HttpPost]
        public void EOCFileUpload_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("EOCFileUpload", ValidationSettings, EOCFileUploadConfirmation_FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void EOCFileUploadConfirmation_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + Math.Abs(System.Environment.TickCount) + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        #endregion UPLOAD CONFIRMATION - REQUEST

        #region UPLOAD CONFIRMATION - RESULT

        public void UploadValid(String processId, String functionId, String temporaryTableValidate)
        {
            IDBContext db = DbContext;
            db.ExecuteScalar<string>("UpdateEmergencyOrderCreationUpload",
                            new object[] {
                            functionId,   // upload function id
                            processId,   // upload process id
                            AuthorizedUser.Username,
                            Session.SessionID
                        });

            db.Close();
        }

        [HttpGet]
        public void DownloadInvalid(String processId, String functionId, String temporaryTableUpload)
        {
            string fileName = "EmergencyOrderCreationUploadInvalid" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<ReportEmergencyOrderCreationUploadError> qry = db.Query<ReportEmergencyOrderCreationUploadError>("ReportGetEmergencyOrderCreationUploadError", new object[] { functionId, processId });
            db.Close();
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil = exporter.Write(qry, "EmergencyOrder");

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        #endregion UPLOAD CONFIRMATION - RESULT

        #endregion UPLOAD CONFIRMATION

        #region Download controller

        /// <summary>
        /// Download template emergency order.
        /// </summary>
        /// <param name="dockCode"></param>
        /// <param name="arrivalTime"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlantCode"></param>
        ///
        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        public FileContentResult DownloadTemplate()
        {
            string filePath = Path.Combine(Server.MapPath("~/Views/EmergencyOrderCreation/UploadTemp/EmergencyOrderCreation_Templates.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "EmergencyOrderCreation_Template.xls");
        }

        #endregion Download controller

        public ActionResult SaveCancelHeader(string orderNo, string dockCode, string supplierCode, string supplierPlantCode, string arrivalTime, string statusButton)
        {
            string result = "";
            IDBContext db = DbContext;
            if (statusButton == "Save")
            {
                EmergencyHeader emergencyHeader = db.SingleOrDefault<EmergencyHeader>("GetEmergencyOrderHeader", new object[]
                {
                    orderNo,
                    supplierCode,
                    dockCode,
                    supplierPlantCode
                });

                if (emergencyHeader != null)
                    result = "Data has been exist, please verify again.";
                else
                {
                    db.Execute("InsertHeaderEmergencyOrder", new object[]
                    {
                        orderNo,
                        supplierCode,
                        dockCode.Substring(0,1),
                        dockCode,
                        arrivalTime,
                        SessionState.GetAuthorizedUser().Username,
                        DateTime.Now,
                        supplierPlantCode
                    });
                }
            }
            else
            {
                db.Execute("DeleteEmergencyOrder", new object[] {
                    orderNo,
                    supplierCode,
                    dockCode,
                    supplierPlantCode
                });
            }

            db.Close();
            return Content(result.ToString());
        }

        public JsonResult SubmitData(string ArrivalDate, string ArrivalTime, string EOID="")
        {
            List<EmergencyResult> emergencyResults = new List<EmergencyResult>();
            DateTime ArrivalDateTime = DateTime.ParseExact((ArrivalDate + " " + ArrivalTime), "dd.MM.yyyy HH:mm", null);
            IDBContext db = DbContext;
            try
            {
                emergencyResults = db.Query<EmergencyResult>("InsertHeaderEmergencyOrder", new object[]
                    {
                        ArrivalDateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                        AuthorizedUser.Username,
                        Session.SessionID,
                        "1",//AuthorizedUser.PlantCode
                        EOID
                    }).ToList();

                db.Close();
                return Json(emergencyResults);
            }
            catch (Exception exc)
            {
                String message = exc.Message;
                db.Close();
                return Json(message);
            }
        }

        public ActionResult AddNewPart(string Supplier, string DockCode, string PartNo, string PartName, string KanbanNo, string OrderQty,
            string QtyPerContainer, int PiecesQty, string BuildOutFlag, string PartAddress, string ImportirInfo1, string ImportirInfo2, string ImportirInfo3,
            string ImportirInfo4, string PartBarcode, string ProgressLaneNo, string ConveyanceNo, string EOT)
        {
            string ResultQuery = "Process succesfully";
            string[] Suppliers = Supplier.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");            
            
            IDBContext db = DbContext;
            try
            {
                string userName = Model.GetModel<User>().Username;
                db.Execute("InsertDetailEmergencyOrder", new object[]
                {
                    Session.SessionID,
                    AuthorizedUser.Username,
                    PartNo,
                    PartName,
                    KanbanNo,
                    OrderQty,
                    QtyPerContainer,
                    PiecesQty,
                    SupplierCode,
                    SupplierPlant,
                    DockCode,
                    BuildOutFlag,
                    PartAddress,
                    ImportirInfo1,
                    ImportirInfo2,
                    ImportirInfo3,
                    ImportirInfo4,
                    PartBarcode,
                    ProgressLaneNo,
                    ConveyanceNo, 
                    EOT, 
                    userName
                });
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            finally
            { db.Close(); }
            return Content(ResultQuery);
        }

        public ActionResult DeletePart(string PartNos, string Suppliers, string Docks)
        {
            string ResultQuery = "Process succesfully";
            char[] splitchar = { ';' };
            string[] Supplier = Suppliers.Split(splitchar);
            string[] Dock = Docks.Split(splitchar);
            string[] PartNo = PartNos.Split(splitchar);

            string[] SupplierPlantCode;
            string SupplierCode = "";
            string SupplierPlant = "";

            IDBContext db = DbContext;
            try
            {
                for (int i = 0; i < PartNo.Length; i++)
                {
                    SupplierPlantCode = Supplier[i].Split('-');
                    SupplierCode = SupplierPlantCode[0].Replace(" ", "");
                    SupplierPlant = SupplierPlantCode[1].Replace(" ", "");
                    db.Execute("DeleteDetailEmergencyOrder", new object[]
                    {
                        Session.SessionID,
                        AuthorizedUser.Username,
                        SupplierCode,
                        SupplierPlant,
                        Dock[i],
                        PartNo[i]
                    });
                }
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            finally
            { db.Close(); }
            return Content(ResultQuery);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("HeaderEmergencyOrderCreation", Model);
        }

        public ActionResult CallbackSupplier()
        {
            TempData["GridName"] = "grlSupplierCode";

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierCode"]);
        }

        public ActionResult CallbackPartNo()
        {
            TempData["GridName"] = "grlPartNo";

            return PartialView("GridLookup/PartialGrid", ViewData["PartNo"]);
        }

        public ActionResult DockCodePartial()
        {
            TempData["GridName"] = "grlDockCode";

            return PartialView("GridLookup/PartialGrid", ViewData["DockCode"]);
        }

        public ActionResult UploadAction(string dockCode, string supplierCode, string supplierPlantCode)
        {
            string orderNo = System.DateTime.Now.ToString("yyyyMMdd") + "01E1";
            ArrayList tableDetail = new ArrayList();
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext db = DbContext;
            TableDestination tableName = db.SingleOrDefault<TableDestination>("GetUploadDestination", new object[] { "103" });
            string[] arrayValues;
            tableDetail = ExcelReader.ReaderExcel("C:\\inetpub\\PART.xls", tableName.sheetName, Convert.ToInt32(tableName.rowStart), Convert.ToInt32(tableName.columnStart), Convert.ToInt32(tableName.columnEnd));
            foreach (string tDet in tableDetail)
            {
                string values = "'" + tDet.Remove(0, 2);
                arrayValues = ParseCsvRow(values);
                db.Execute("InsertUploadPartOrderTemp", new object[]
                    {
                        orderNo,
                        dockCode,
                        supplierCode,
                        supplierPlantCode,
                        arrayValues.GetValue(0),
                        arrayValues.GetValue(1),
                        arrayValues.GetValue(2),
                        arrayValues.GetValue(3),
                    });
            }
            db.Close();
            return RedirectToAction("Index");
        }

        public static string[] ParseCsvRow(string r)
        {
            string[] c;
            List<string> resp = new List<string>();
            bool cont = false;
            string cs = "";

            c = r.Split(new char[] { ',' }, StringSplitOptions.None);

            foreach (string y in c)
            {
                string x = y;

                if (cont)
                {
                    // End of field
                    if (x.EndsWith("'"))
                    {
                        cs += "," + x.Substring(0, x.Length - 1);
                        resp.Add(cs);
                        cs = "";
                        cont = false;
                        continue;
                    }
                    else
                    {
                        // Field still not ended
                        cs += "," + x;
                        continue;
                    }
                }

                // Fully encapsulated with no comma within
                if (x.StartsWith("'") && x.EndsWith("'"))
                {
                    if ((x.EndsWith("''") && !x.EndsWith("'''")) && x != "''")
                    {
                        cont = true;
                        cs = x;
                        continue;
                    }

                    resp.Add(x.Substring(1, x.Length - 2));
                    continue;
                }

                // Start of encapsulation but comma has split it into at least next field
                if (x.StartsWith("'") && !x.EndsWith("'"))
                {
                    cont = true;
                    cs += x.Substring(1);
                    continue;
                }

                // Non encapsulated complete field
                resp.Add(x);
            }

            return resp.ToArray();
        }

        public ActionResult SetEmergencyOrderHeader()
        {
            string AnyOrder = ViewData["AnyOrderHeader"].ToString();
            string OrderNo = ViewData["OrderNoHeader"].ToString();
            string DockCode = ViewData["DockCodeHeader"].ToString();
            string SupplierCode = ViewData["SupplierCodeHeader"].ToString();
            string SupplierPlant = ViewData["SupplierPlantHeader"].ToString();
            string ArrivalTime = ViewData["ArrivalTimeHeader"].ToString();

            var datajson = new { AnyOrder, OrderNo, DockCode, SupplierCode, SupplierPlant, ArrivalTime };

            return Json(datajson, JsonRequestBehavior.AllowGet);
        }
    }

    internal class SupplierPart
    {
        public string PART_NO { set; get; }
        public string PART_NAME { set; get; }
        public string KANBAN_NO { set; get; }
        public Int32 PACK_PER_CONTAINER { set; get; }
        public string BUILD_OUT_FLAG { set; get; }
        public int BO_PART { set; get; }
    }

    internal class EmergencyResult
    {
        public string MANIFEST_NO { set; get; }
        public string ORDER_NO { set; get; }
        public string DOCK_CD { set; get; }
    }

    internal class EmergencyHeader
    {
        public string OrderNo { set; get; }
        public string DockCode { set; get; }
        public string SupplierCode { set; get; }
        public string SupplierPlant { set; get; }
        public DateTime ArrivalTime { set; get; }
    }
}