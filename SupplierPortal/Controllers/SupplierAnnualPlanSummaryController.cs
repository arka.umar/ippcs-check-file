﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Threading;
using System.Data;
using System.Xml;

using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;

using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;

using Portal.Models.Supplier;
using Portal.Models.SupplierAnnualPlanSummary;

namespace Portal.Controllers
{
    public class SupplierAnnualPlanSummaryController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public SupplierAnnualPlanSummaryController()
            : base("Supplier Annual Plan Summary")
        {
            PageLayout.UseMessageBoard = true;
        }

        #region Model properties
        List<SupplierName> _annualSupplierModel = null;
        private List<SupplierName> _AnnualSupplierModel
        {
            get
            {
                if (_annualSupplierModel == null)
                    _annualSupplierModel = new List<SupplierName>();

                return _annualSupplierModel;
            }
            set
            {
                _annualSupplierModel = value;
            }
        }

        List<SupplierPlan> _annualSupplierPlanModel = null;
        private List<SupplierPlan> _AnnualSupplierPlanModel
        {
            get
            {
                if (_annualSupplierPlanModel == null)
                    _annualSupplierPlanModel = new List<SupplierPlan>();

                return _annualSupplierPlanModel;
            }
            set
            {
                _annualSupplierPlanModel = value;
            }
        }

        AnnualPlanSummaryModel _annualModel = null;
        private AnnualPlanSummaryModel _AnnualModel
        {
            get
            {
                if (_annualModel == null)
                    _annualModel = new AnnualPlanSummaryModel();

                return _annualModel;
            }
            set
            {
                _annualModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
        }

        #region Supplier Annual Plan Summary controller
        #region View controller
        public ActionResult PartialHeaderSupplierAnnualPlanSummary()
        {
            #region Required model in partial page : for SupplierAnnualPlanSummaryHeaderPartial
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierAnnualPlanSummary", "SAPSHSupplierIDLookup");

            if (suppliers.Count > 0)
            {
                _AnnualSupplierModel = suppliers;
            }
            else
            {
                _AnnualSupplierModel = GetAllSupplier();
            }

            Model.AddModel(_AnnualSupplierModel);
            #endregion

            return PartialView("SupplierAnnualPlanSummaryHeaderPartial", Model);
        }

        public ActionResult PartialHeaderSupplierLookupGridSupplierAnnualPlanSummary()
        {
            #region Required model in partial page : for SAPSHSupplierIDLookup GridLookup
            TempData["GridName"] = "SAPSHSupplierIDLookup";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierAnnualPlanSummary", "SAPSHSupplierIDLookup");
            if (suppliers.Count > 0)
                _AnnualSupplierModel = suppliers;
            else
                _AnnualSupplierModel = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", _AnnualSupplierModel);
        }

        public ActionResult PartialDetailSupplierAnnualPlanSummary()
        {
            #region Required model in partial page : for SupplierAnnualPlanSummaryDetailPartial
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "SupplierAnnualPlanSummary", "SAPSHSupplierIDLookup");
            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {
                supplierCode = suppliers[0].SUPPLIER_CODE;
            }

            _AnnualModel.AnnualPlanHeaderList = GetAllSupplierAnnualPlanSummary(String.IsNullOrEmpty(supplierCode) ? Request.Params["SupplierCode"] : supplierCode, Request.Params["Version"], Request.Params["ProductionYear"]);

            Model.AddModel(_AnnualModel);
            #endregion

            return PartialView("SupplierAnnualPlanSummaryDetailPartial", Model);
        }
        #endregion

        #region Database controller
        private List<AnnualPlanHeaderData> GetAllSupplierAnnualPlanSummary(String supplierCode, String version, String productionYear)
        {
            List<AnnualPlanHeaderData> AnnualPlanHeaderData = new List<AnnualPlanHeaderData>();
            IDBContext db = DbContext;
            try
            {
                AnnualPlanHeaderData = db.Fetch<AnnualPlanHeaderData>("GetAllSupplierAnnualPlanSummary", new object[] { supplierCode, productionYear, version });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return AnnualPlanHeaderData;
        }

        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> supplierNameList = new List<SupplierName>();
            IDBContext db = DbContext;
            supplierNameList = db.Fetch<SupplierName>("GetAllSupplierForecast", new object[] { });
            db.Close();

            return supplierNameList;
        }
        #endregion

        #region Download Controller
        /// <summary>
        /// Download feedback part data
        /// </summary>
        /// <param name="partNo"></param>
        /// <param name="productionMonth"></param>
        /// <param name="supplierCode"></param>
        /// <param name="supplierPlanCode"></param>
        public void DownloadFeedbackPart(String partNo, String productionMonth, String supplierCode, String supplierPlanCode)
        {
            String connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\SupplierFeedbackSummary\SupplierFeedbackSummaryFeedbackPart.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;

            sqlDataSource.SelectCommand =
                      @"SELECT
	                    L.PART_NO,
                        L.PART_NAME,
	                    L.N_VOLUME,
	                    L.N_1_VOLUME,
	                    L.N_2_VOLUME,
	                    L.N_3_VOLUME,
	                    L.VERS,
	                    L.PACK_MONTH,
	                    L.SUPPLIER_CD,
	                    L.S_PLANT_CD,
	                    L.DOCK_CD,
	                    SF.N_FA,
	                    SF.N_1_FA,
	                    SF.N_2_FA,
	                    SF.N_3_FA,
	                    SF.N_MONTH as N_REPLY,
	                    SF.N_1_MONTH as N_1_REPLY,
	                    SF.N_2_MONTH as N_2_REPLY,
	                    SF.N_3_MONTH as N_3_REPLY,
	                    SF.CREATED_BY,
	                    SF.CREATED_DT,
	                    '' as NOTICE,
	                    CONVERT(BIT, 1) as UNLOCK
                    FROM
	                    TB_R_LPOP L
	                    JOIN TB_R_SUPPLIER_FEEDBACK SF ON
		                    L.PART_NO = SF.PART_NO AND
		                    L.PACK_MONTH = SF.PRODUCTION_MONTH AND
		                    L.SUPPLIER_CD = SF.SUPPLIER_CD AND
		                    L.VERS = SF.FORECAST_TYPE
                    WHERE 
	                    ((L.PART_NO='" + partNo + "' AND isnull('" + partNo + "','') <> '') or (isnull('" + partNo + "','') = '')) AND " +
                        @"((L.PACK_MONTH='" + productionMonth + "' AND isnull('" + productionMonth + "','') <> '') or (isnull('" + productionMonth + "','') = '')) AND " +
                        @"((L.SUPPLIER_CD='" + supplierCode + "' AND isnull('" + supplierCode + "','') <> '') or (isnull('" + supplierCode + "','') = '')) AND " +
                        @"((L.S_PLANT_CD='" + supplierPlanCode + "' AND isnull('" + supplierPlanCode + "','') <> '') or (isnull('" + supplierPlanCode + "','') = '')) " +
                    @"ORDER BY L.PART_NO";

            ReportProcessor reportProcessor = new ReportProcessor();

            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);

            string fileName = "SupplierFeedbackPart" + productionMonth + supplierCode + supplierPlanCode + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }
        #endregion
        #endregion
    }
}
