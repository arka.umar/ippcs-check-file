﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.PO;
using Portal.Models.Plant;

namespace Portal.Controllers
{
    public class PlantController : BaseController
    {
        public PlantController()
            : base("Plant Master Inquiry Screen")
        {

        }

        protected override void StartUp()
        {
    
        }

        protected override void Init()
        {
            PlantModel model = new PlantModel();
            for (int i = 0; i < 21; i++)
            {
                model.PlantsData.Add(new PlantData()
                    {
                        PlantCode = "PC" + (i+1),
                        PlantName = "Plant" + (i+1)
                    });
            }

            Model.AddModel(model);
        }

        public ActionResult PartialPlantDatas()
        {
            return PartialView("PartialPlantData", Model);
        }
        public ActionResult PartialPlantBerudull()
        {
            return PartialView("PartialPlantHeader", Model);
        }
        public ActionResult PartialGridVw()
        {
            return PartialView("PartialPlantGridVw", Model);
        }
    }
}
