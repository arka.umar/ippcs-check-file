﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Portal.Models.Globals;
using Portal.Models.InvoiceAndPaymentInquiry;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Globalization;
using System.Configuration;
using DevExpress.Web.ASPxUploadControl;

namespace Portal.Controllers
{
    public class InvoiceAndPaymentInquiryController : BaseController
    {
        private readonly string OIDSupp = "OIPSupplierOption";
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

         public InvoiceAndPaymentInquiryController()
            : base("Invoice Inquiry")
        {
            PageLayout.UseMessageBoard = true;
        }

         #region Model properties
         List<SupplierICS> _invoiceInquirySupplierModel = null;
         private List<SupplierICS> _InvoiceInquirySupplierModel
         {
             get
             {
                 if (_invoiceInquirySupplierModel == null)
                     _invoiceInquirySupplierModel = new List<SupplierICS>();

                 return _invoiceInquirySupplierModel;
             }
             set
             {
                 _invoiceInquirySupplierModel = value;
             }
         }

         #endregion

        protected override void StartUp()
        {
            
        }

        private string SuppCodeOf(User u)
        {
            if (u == null || u.Authorization == null || u.Authorization.Count < 1) return null;

            string ag = (from authSource in u.Authorization
                         from item in authSource.AuthorizationDetails
                         where item.Screen.ToLower() == ScreenId.ToLower() &&
                             item.Object.ToLower() == OIDSupp.ToLower() &&
                             (item.AuthorizationGroup.ToLower() != "null")
                         select item.AuthorizationGroup).FirstOrDefault();
            return ag;
        }
        public const int NULL_YEAR = 1900;
        public static DateTime DateDef(string s, string fmt, DateTime? v = null)
        {
            DateTime r = new DateTime();
            if (!DateTime.TryParseExact(s, fmt, CultureInfo.InvariantCulture,
                DateTimeStyles.AssumeLocal, out r))
                r = v ?? new DateTime(NULL_YEAR, 1, 1);
            return r;
        }


        private string _suppCd = null;
        private string SuppCd
        {
            get
            {
                if (string.IsNullOrEmpty(_suppCd))
                {
                    _suppCd = SuppCodeOf(Model.GetModel<User>());
                    if (string.IsNullOrEmpty(_suppCd))
                    {
                        _suppCd = "";
                    }
                }
                return _suppCd;
            }

            set
            {
                _suppCd = value;
            }
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;
            InvoiceAndPaymentInquiryModel md2 = new InvoiceAndPaymentInquiryModel();
            List<InvoiceAndPaymentInquiryDetail> lst = new List<InvoiceAndPaymentInquiryDetail>();
            List<InvoiceAndPaymentDetail> detail = new List<InvoiceAndPaymentDetail>();

            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPP_CD).Distinct().ToList<string>();

            if (suppCode.Count == 1) 
                ViewData["GridSupplierCode"] = suppliers[0].SUPP_CD;
            else 
                ViewData["GridSupplierCode"] = ""; 

            ViewData["SupplierGridLookup"] = suppliers;
            ViewData["PaymentTermGridLookup"] = GetAllTermOfPayment();
            ViewData["PaymentMethodGridLookup"] = GetAllPaymentMethod();
            ViewData["InvoiceStatusGridLookup"] = GetAllInvoiceStatus();
            ViewData["PartnerBankGridLookup"] = GetAllPartnerBank("","");
            ViewData["WithholdingTaxGridLookup"] = GetAllWithHoldingTax();
            ViewData["TaxGridLookup"] = GetAllTax();
            ViewData["GLAccountGridLookup"] = GetAllGLAccount();

            md2.InvoiceAndPaymentInquiryDetails = lst;
            md2.InvoiceAndPaymentDetails = detail;
            Model.AddModel(md2);

            ViewData["DateFrom"] = DateTime.Now.AddMonths(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");
        }
        #region Invoice And Payment Inquiry controller
        #region View controller
        public ActionResult PartialHeaderInvoiceInquiry()
        {
            #region Required model in partial page : for OrderInquiryHeaderPartial
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");

            _InvoiceInquirySupplierModel = suppliers;

            Model.AddModel(_InvoiceInquirySupplierModel);
            #endregion

            return PartialView("HeaderInvoiceAndPayment", Model);
        }
        public ActionResult PartialHeaderSupplierLookupGridInvoiceInquiry()
        {
            #region Required model in partial page : for OIPSupplierOption GridLookup
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierOption");

            TempData["GridName"] = "OIPSupplierOption";
            _InvoiceInquirySupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _InvoiceInquirySupplierModel);
        }
        public ActionResult PartialPaymentTermGridLookup()
        {
            #region Required model in partial page : for OIPTermPaymentGridLookup GridLookup
            ViewData["PaymentTermGridLookup"]=GetAllTermOfPayment();

            TempData["GridName"] = "OIPTermPaymentGridLookup";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["PaymentTermGridLookup"]);
        }
        public ActionResult PartialPaymentMethodGridLookup()
        {
            #region Required model in partial page : for OIPPaymentMethodGridLookup GridLookup
            ViewData["PaymentMethodGridLookup"] = GetAllPaymentMethod();

            TempData["GridName"] = "OIPPaymentMethodGridLookup";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["PaymentMethodGridLookup"]);
        }
        public ActionResult PartialInvoiceStatusGridLookup()
        {
            #region Required model in partial page : for OIPStatusOption GridLookup
            ViewData["InvoiceStatusGridLookup"] = GetAllInvoiceStatus();

            TempData["GridName"] = "OIPStatusOption";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["InvoiceStatusGridLookup"]);
        }
        public ActionResult PartialPartnerBankGridLookup()
        {
            #region Required model in partial page : for OIPPartnerBankGridLookup GridLookup
            ViewData["PartnerBankGridLookup"] = GetAllPartnerBank(SuppCodeValid(Request.Params["SupplierCode"]), Request.Params["Currency"]);

            TempData["GridName"] = "OIPPartnerBankGridLookup";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["PartnerBankGridLookup"]);
        }
        public ActionResult PartialWithholdingTaxGridLookup()
        {
            #region Required model in partial page : for OIPWitholdingTaxGridLookup GridLookup
            ViewData["WithholdingTaxGridLookup"] = GetAllWithHoldingTax();

            TempData["GridName"] = "OIPWitholdingTaxGridLookup";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["WithholdingTaxGridLookup"]);
        }
        public ActionResult PartialTaxGridLookup()
        {
            #region Required model in partial page : for OIPTaxGridLookup GridLookup
            ViewData["TaxGridLookup"] = GetAllTax();

            TempData["GridName"] = "OIPTaxGridLookup";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["TaxGridLookup"]);
        }
        public ActionResult PartialGLAccountGridLookup()
        {
            #region Required model in partial page : for OIPGLAccountGridLookup GridLookup
            ViewData["GLAccountGridLookup"] = GetAllGLAccount();

            TempData["GridName"] = "OIPGLAccountGridLookup";
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["GLAccountGridLookup"]);
        }

        private string SuppCodeValid(string c) {
            if (string.IsNullOrEmpty(SuppCd))
                return c; // doesn't matter , always valid when authorization group is empty

            if (string.IsNullOrEmpty(c)
                || (string.Compare(c, SuppCd) == 0))
                return SuppCd;

            return "XXXX"; /// return invalid
        }

        public ActionResult InvoiceAndPaymentInquiryPartial()
        {
            #region Required model in partial page : for InvoiceAndPaymentInquiryPartial
            InvoiceAndPaymentInquiryModel model = Model.GetModel<InvoiceAndPaymentInquiryModel>();

            model.InvoiceAndPaymentInquiryDetails = GetAllInvoice(
                                        SuppCodeValid(Request.Params["SupplierCode"]),
                                        Request.Params["UploadDateFrom"],
                                        Request.Params["UploadDateTo"],
                                        Request.Params["InvoiceDateFrom"],
                                        Request.Params["InvoiceDateTo"],
                                        Request.Params["InvoiceNo"],
                                        Request.Params["StatusCode"],
                                        Request.Params["PlanPaymentDateFrom"],
                                        Request.Params["PlanPaymentDateTo"],
                                        Request.Params["Automatic"]);
            #endregion

            return PartialView("InvoiceAndPaymentInquiryPartial", Model);
        }
        public ActionResult InvoiceAndPaymentInquiryGrid()
        {
            return PartialView("InvoiceAndPaymentInquiry", Model);
        }
        public ActionResult InvoiceAndPaymentDetailGrid()
        {
            string invoiceNo = Request.Params["InvoiceNo"];
            string invoiceDate = Request.Params["InvoiceDate"];
            string supplierCode = SuppCodeValid(Request.Params["SupplierCode"]);
            ViewData["InvoicePaymentDetail"] = GetInvoicePaymentDetail(invoiceNo, invoiceDate, supplierCode);

            return PartialView("InvoiceAndPaymentDetailGrid", ViewData["InvoicePaymentDetail"]);
        }

        public ContentResult GetMonthPostingDate()
        {
            string monthPostingDate;
            IDBContext db = DbContext;
            monthPostingDate = db.ExecuteScalar<string>("GetMonthPostingDate", new object[] { });
            db.Close();

            return Content(monthPostingDate);
        }
        public ContentResult SaveInvoicePayment(
            string SupplierInvoiceNo,
            string SupplierCode,
            string PostingDate,
            string InvoiceDate,
            string BaselineDate,
            string TermOfPayment,
            string PaymentMethod,
            string WithHoldingTaxCode,
            string BaseAmount,
            string PartnerBank,
            bool CalculateTax,
            string Amount,
            string InvoiceNote,
            string HeaderText,
            string Assignment,
            string TaxCode,
            string TaxDate,
            string GLAccount,
            string PPVAmount)
        {
            string resultMessage = "Save successfully";
            IDBContext db = DbContext;
            String username = AuthorizedUser.Username;
                
            try
            {
                if (string.Compare(SuppCd, SupplierCode) != 0 && !string.IsNullOrEmpty(SuppCd))
                    throw new Exception(SuppCd + " supplier cannot save for " + SupplierCode);

                SaveDataInvoicePayment(SupplierInvoiceNo,
                                SupplierCode,
                                PostingDate,
                                InvoiceDate,
                                BaselineDate,
                                TermOfPayment,
                                PaymentMethod,
                                WithHoldingTaxCode,
                                BaseAmount,
                                PartnerBank,
                                CalculateTax,
                                Amount,
                                InvoiceNote,
                                HeaderText,
                                Assignment,
                                TaxCode,
                                TaxDate,
                                GLAccount,
                                PPVAmount);
            }
            catch (Exception exc)
            {
                resultMessage = "Save Data Failed: (" + exc.Message + "), please contact IPPCS administrator!";
            }

            db.Close();

            return Content(resultMessage.ToString());
        }
        public ContentResult PostInvoicePayment(
            string SupplierInvoiceNo,
            string SupplierCode,
            string PostingDate,
            string InvoiceDate,
            string BaselineDate,
            string TermOfPayment,
            string PaymentMethod,
            string WithHoldingTaxCode,
            string BaseAmount,
            string PartnerBank,
            bool CalculateTax,
            string Amount,
            string InvoiceNote,
            string HeaderText,
            string Assignment,
            string TaxCode,
            string TaxDate,
            string GLAccount,
            string PPVAmount)
        {
            string resultMessage = "";
            IDBContext db = DbContext;

            String message = "";
            String location = "";
            String module = "5";
            String function = "53002";
            String username = AuthorizedUser.Username;
            String processID = "";

            try
            {
                #region Posting New Structure
                resultMessage = "LIV Posting in progress";


                #region Initial Phase : update field Invoice Upload
                SaveDataInvoicePayment(SupplierInvoiceNo,
                                SupplierCode,
                                PostingDate,
                                InvoiceDate,
                                BaselineDate,
                                TermOfPayment,
                                PaymentMethod,
                                WithHoldingTaxCode,
                                BaseAmount,
                                PartnerBank,
                                CalculateTax,
                                Amount,
                                InvoiceNote,
                                HeaderText,
                                Assignment,
                                TaxCode,
                                TaxDate,
                                GLAccount,
                                PPVAmount);
                #endregion

                #region Phase 1 : generate process id
                message = "Start Process Invoice Payment Posting from IPPCS";
                location = "Invoice Inquiry";

                processID = db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                #endregion

                #region Phase 2 : generate posting invoice
                message = "Start Process Generate Data Posting";
                location = "Generate Posting File";

                db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00002INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                db.Execute(
                     "ICSGeneratePostingInvoicePayment",
                     new object[] { 
                            username, 
                            SupplierInvoiceNo, 
                            SupplierCode,
                            processID,
                            (InvoiceDate == "" || InvoiceDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(InvoiceDate,"dd.MM.yyyy",null)
                        });

                message = "Finish Process Generate Data Posting";

                db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00003INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                #endregion
                #endregion
            }
            catch (Exception exc)
            {
                resultMessage = "Approval Failed for Process ID : " + processID + "(" + exc.Message + "), please contact IPPCS administrator!";
            }

            db.Close();

            return Content(resultMessage.ToString());
        }
        public ContentResult CancelInvoice(string SupplierInvoiceNo, string SupplierCode, string InvoiceDate, string StatusCd, string InvTaxNo)
        {
            string resultMessage = "Cancel successfully";
            IDBContext db = DbContext;
            List<string> RoleFinance = new List<string>();
            RoleFinance = db.Fetch<string>("GetRoleCancelInvoice", new object[] { });
            DateTime invoiceDate = DateDef(InvoiceDate, "yyyyMMddhhmm");
            string SQLMessage = "";
            try
            {
                if (string.Compare(SuppCd, SupplierCode) != 0 && !string.IsNullOrEmpty(SuppCd))
                    throw new Exception(SuppCd + " supplier cannot Cancel for " + SupplierCode);

                if (StatusCd == "3" || StatusCd == "-4" || StatusCd == "-5")
                {
                    foreach (Authorization r in AuthorizedUser.Authorization)
                    {                        
                        foreach (string roleFinance in RoleFinance)
                        {
                            if (r.AuthorizationID == roleFinance)
                            {
                                SQLMessage = db.ExecuteScalar<string>(
                                        "CancelInvoicePayment",
                                        new object[] {
                                            SupplierInvoiceNo,
                                            SupplierCode,
                                            invoiceDate,
                                            AuthorizedUser.Username,
                                            InvTaxNo,
                                            StatusCd
                                    });

                                db.Close();

                                //if (SQLMessage == "OutstandingError")
                                    //resultMessage = SQLMessage;

                                return Content(resultMessage.ToString());
                            }
                        }
                    }
                    resultMessage = "Invoice can't be cancelled";
                }
                //modif by alira.agi 2015-05-21 Bug Id 533 Invoice Inquiry cancelation
                //Add logic cancelation allow status='1' to accomodate cancelation for partial invoice creation
                //else if (StatusCd == "2" || StatusCd == "-1" || StatusCd == "-2")
                else if (StatusCd == "2" || StatusCd == "-1" || StatusCd == "-2" || StatusCd == "1")
                {
                    SQLMessage = db.ExecuteScalar<string>(
                                    "CancelInvoicePayment",
                                    new object[] {
                                            SupplierInvoiceNo,
                                            SupplierCode,
                                            invoiceDate,
                                            AuthorizedUser.Username,
                                            InvTaxNo,
                                            StatusCd
                                    });
                    if (SQLMessage == "OutstandingError")
                        resultMessage = "Outstanding Invoice can be Cancelled if its Created More Than 24 Hours from Now";
                    else if (SQLMessage == "CreatedDateNULL")
                        resultMessage = "Created Date is Null, Invoice Cannot Be Cancelled";
                }
                else
                {
                    resultMessage = "Invoice can't be cancelled";
                }

          
            }
            catch (Exception exc)
            {
                resultMessage = "Cancel Failed, please contact IPPCS administrator!";
            }

            db.Close();

            return Content(resultMessage.ToString());
        }
        public ContentResult ReverseInvoicePayment(string InvoiceNo, string SupplierCode, string InvoiceDate, string InvTaxNo)
        {
            string resultMessage = "";
            IDBContext db = DbContext;

            String message = "";
            String location = "";
            String module = "5";
            String function = "53004";
            String username = AuthorizedUser.Username;
            String processID = "";

            try
            {
                if (string.Compare(SuppCd, SupplierCode) != 0 && !string.IsNullOrEmpty(SuppCd))
                    throw new Exception(SuppCd + " supplier cannot Reverse efor " + SupplierCode);


                #region Posting New Structure
                resultMessage = "Reverse is Finish";

                #region Phase 1 : generate process id
                message = "Start Process Invoice Payment Posting from IPPCS";
                location = "Invoice Inquiry";

                processID = db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                #endregion

                #region Phase 2 : generate posting invoice
                message = "Start Process Generate Data Posting";
                location = "Generate Posting File";

                db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00002INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

                db.Execute(
                     "ReverseInvoicePaymentInquiry",
                     new object[] { 
                            InvoiceNo,
                            username, 
                            processID,
                            InvTaxNo,
                            SupplierCode
                        });

                message = "Finish Process Generate Data Posting";

                db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            processID,     // pid OUTPUT
                            "MPCS00003INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });
                #endregion
                #endregion
            }
            catch (Exception exc)
            {
                resultMessage = "Approval Failed for Process ID : " + processID + ", please contact IPPCS administrator!";
            }

            db.Close();

            return Content(resultMessage.ToString());
        }

        //Added By Fid.Reggy
        public String ReProcessInvoice(string SupplierInvoiceNo, string SuppCD)
        {
            IDBContext db = DbContext;
            string result = "";

            try
            {
                result = db.ExecuteScalar<String>("PerformReprocessInvoice",
                        new object[] { SupplierInvoiceNo, SuppCD });
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        public JsonResult GetInvoicePaymentDetailPosting(string SupplierInvoiceNo, string SupplierCode, string InvoiceDate)
        {
            List<InvoiceAndPaymentInquiry> InvoiceAndPaymentInquiryInfo = new List<InvoiceAndPaymentInquiry>();
            DateTime InvoiceDateDt = (InvoiceDate == "" || InvoiceDate == null) 
                ? DateTime.ParseExact("190001011200", "yyyyMMddhhmm", null) 
                : DateTime.ParseExact(InvoiceDate, "yyyyMMddhhmm", null);

            IDBContext db = DbContext;
            try
            {
                TempData["SUPP_INV_NO"] = SupplierInvoiceNo;
                TempData["SUPP_CD"] = SuppCodeValid(SupplierCode);

                Session["SUPP_INV_NO"] = SupplierInvoiceNo;
                Session["SUPP_CD"] = SuppCodeValid(SupplierCode); ;

                InvoiceAndPaymentInquiryInfo = db.Query<InvoiceAndPaymentInquiry>("GetInvoiceAndPaymentPostDetail", 
                    new object[] { SupplierInvoiceNo, SuppCodeValid(SupplierCode), InvoiceDateDt }).ToList();

                foreach(var i in InvoiceAndPaymentInquiryInfo)
                {
                    i.PPV_EVIDENCE = GetUploadedTempPPVEvidenceFiles(i.SUPP_CD, i.SUPP_INV_NO);
                }
            }
            catch (Exception exc) 
            { 
                throw exc; 
            }
            finally { db.Close(); }

            return Json(InvoiceAndPaymentInquiryInfo);
        }
        #endregion

        #region Database controller
        private List<SupplierICS> GetAllSupplierICS()
        {
            List<SupplierICS> lstSupplierICS = new List<SupplierICS>();
            IDBContext db = DbContext;
            lstSupplierICS = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });
            db.Close();

            return lstSupplierICS;
        }
        private List<TermOfPayment> GetAllTermOfPayment()
        {
            List<TermOfPayment> lstTermOfPayment = new List<TermOfPayment>();
            IDBContext db = DbContext;
            lstTermOfPayment = db.Fetch<TermOfPayment>("GetAllTermOfPayment", new object[] { });
            db.Close();

            return lstTermOfPayment;
        }
        private List<PaymentMethod> GetAllPaymentMethod()
        {
            List<PaymentMethod> lstPaymentMethod = new List<PaymentMethod>();
            IDBContext db = DbContext;
            lstPaymentMethod = db.Fetch<PaymentMethod>("GetAllPaymentMethod", new object[] { });
            db.Close();

            return lstPaymentMethod;
        }
        private List<InvoiceStatus> GetAllInvoiceStatus()
        {
            List<InvoiceStatus> lstInvoiceStatus = new List<InvoiceStatus>();
            IDBContext db = DbContext;
            lstInvoiceStatus = db.Fetch<InvoiceStatus>("GetAllInvoiceStatus", new object[] { });
            db.Close();

            return lstInvoiceStatus;
        }
        private List<PartnerBank> GetAllPartnerBank(string SupplierCode, string Currency)
        {
            List<PartnerBank> lstPartnerBank = new List<PartnerBank>();
            IDBContext db = DbContext;
            lstPartnerBank = db.Fetch<PartnerBank>("GetAllSupplierBank", new object[] { SuppCodeValid(SupplierCode), Currency });
            db.Close();

            return lstPartnerBank;
        }
        private List<WithHoldingTax> GetAllWithHoldingTax()
        {
            List<WithHoldingTax> lstWithHoldingTax = new List<WithHoldingTax>();
            IDBContext db = DbContext;
            lstWithHoldingTax = db.Fetch<WithHoldingTax>("GetAllWithholdingTax", new object[] { });
            db.Close();

            return lstWithHoldingTax;
        }
        private List<Tax> GetAllTax()
        {
            List<Tax> lstTax = new List<Tax>();
            IDBContext db = DbContext;
            lstTax = db.Fetch<Tax>("GetAllTax", new object[] { });
            db.Close();

            return lstTax;
        }
        private List<GLAccount> GetAllGLAccount()
        {
            List<GLAccount> lstGLAccount = new List<GLAccount>();
            IDBContext db = DbContext;
            lstGLAccount = db.Fetch<GLAccount>("GetAllGLAccount", new object[] { });
            db.Close();

            return lstGLAccount;
        }
        private List<InvoiceAndPaymentInquiryDetail> GetAllInvoice(
            string SupplierCode,
            string UploadDateFrom,
            string UploadDateTo,
            string InvoiceDateFrom,
            string InvoiceDateTo,
            string InvoiceNo,
            string StatusCode,
            string PlanPaymentDateFrom,
            string PlanPaymentDateTo,
            string Automatic)
        {
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierGridLookup");
            List<string> suppCode = suppliers.Select(x => x.SUPP_CD).Distinct().ToList<string>(); 
            List<InvoiceAndPaymentInquiryDetail> invoice_payment_detail = new List<InvoiceAndPaymentInquiryDetail>();
            IDBContext db = DbContext;

            if (suppCode.Count == 1) 
                SupplierCodeLDAP = suppliers[0].SUPP_CD;
            else 
                SupplierCodeLDAP = "";

            if (!string.IsNullOrEmpty(SuppCd))
            {
                SupplierCode = SuppCodeValid(SupplierCode);
            }
     
            object[] aParam = new object[] { new {
                    SupplierCode, 
                    InvoiceDateFrom = InvoiceDateFrom.DateReFormat("dd.MM.yyyy", "yyyyMMdd"),
                    InvoiceDateTo = InvoiceDateTo.DateReFormat("dd.MM.yyyy", "yyyyMMdd"), 
                    InvoiceNo, 
                    StatusCode, 
                    PlanPaymentDateFrom =  PlanPaymentDateFrom.DateReFormat("dd.MM.yyyy","yyyyMMdd"), 
                    PlanPaymentDateTo=  PlanPaymentDateTo.DateReFormat("dd.MM.yyyy","yyyyMMdd"), 
                    SupplierCodeLDAP, 
                    UploadDateFrom=UploadDateFrom.DateReFormat("dd.MM.yyyy","yyyyMMdd"),
                    UploadDateTo=UploadDateTo.DateReFormat("dd.MM.yyyy","yyyyMMdd"),
                    Automatic}
            };

            ExceptionalHelper.PutLog(aParam.AsJson(), User.Identity.Name, "GetAllInvoice");
  
            invoice_payment_detail = db.Fetch<InvoiceAndPaymentInquiryDetail>("GetAllInvoice", aParam);
            db.Close();

            return invoice_payment_detail;
        }
        protected List<InvoiceAndPaymentDetail> GetInvoicePaymentDetail(string InvoiceNo, string InvoiceDate, string SupplierCode)
        {
            List<InvoiceAndPaymentDetail> LogList = new List<InvoiceAndPaymentDetail>();
            IDBContext db = DbContext;

            LogList = db.Query<InvoiceAndPaymentDetail>("GetInvoiceAndPaymentDetail", new object[] { InvoiceNo, InvoiceDate, SuppCodeValid(SupplierCode) })
                .ToList<InvoiceAndPaymentDetail>();

            db.Close();
            return LogList;
        }
        protected void SaveDataInvoicePayment(
            string SupplierInvoiceNo,
            string SupplierCode,
            string PostingDate,
            string InvoiceDate,
            string BaselineDate,
            string TermOfPayment,
            string PaymentMethod,
            string WithHoldingTaxCode,
            string BaseAmount,
            string PartnerBank,
            bool CalculateTax,
            string Amount,
            string InvoiceNote,
            string HeaderText,
            string Assignment,
            string TaxCode,
            string TaxDate,
            string GLAccount,
            string PPVAmount)
        {
            IDBContext db = DbContext;
            String username = AuthorizedUser.Username;

            try
            {
                if (string.Compare(SuppCd, SupplierCode) != 0 && !string.IsNullOrEmpty(SuppCd))
                    throw new Exception(SuppCd + " supplier cannot Posting for " + SupplierCode);

                db.ExecuteScalar<string>(
                                "PostingInvoicePaymentInquiry",
                                new object[] {
                                            SupplierCode,
                                            SupplierInvoiceNo,
                                            (PostingDate == "" || PostingDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PostingDate,"dd.MM.yyyy",null),
                                            (InvoiceDate == "" || InvoiceDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(InvoiceDate,"dd.MM.yyyy",null),
                                            (BaselineDate == "" || BaselineDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(BaselineDate,"dd.MM.yyyy",null),
                                            TermOfPayment,
                                            PaymentMethod,
                                            WithHoldingTaxCode,
                                            BaseAmount,
                                            PartnerBank,
                                            CalculateTax,
                                            Amount,
                                            InvoiceNote,
                                            HeaderText,
                                            Assignment,
                                            TaxCode,
                                            (TaxDate == "" || TaxDate == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(TaxDate,"dd.MM.yyyy",null),
                                            GLAccount,
                                            PPVAmount,
                                            AuthorizedUser.Username
                                    });
            }
            catch (Exception exc)
            {
                throw exc;
            }

            db.Close();
        }
        #endregion
        #endregion

        #region Download
        public void DownloadList(string SupplierCode,
            string UploadDateFrom,
            string UploadDateTo,
            string InvoiceDateFrom,
            string InvoiceDateTo,
            string InvoiceNo,
            string StatusCode,
            string PlanPaymentDateFrom,
            string PlanPaymentDateTo)
        {
            string SupplierCodeLDAP = "";
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(GetAllSupplierICS(), "SUPP_CD", "InvoiceAndPaymentInquiry", "OIPSupplierGridLookup");
            List<string> suppCode = suppliers.Select(x => x.SUPP_CD).Distinct().ToList<string>();

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPP_CD;
            string fileName = "InvoiceInquiry.xls";

            IDBContext db = DbContext;
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            if (!string.IsNullOrEmpty(SuppCd))
            {
                SupplierCode = SuppCodeValid(SupplierCode);
            }

            IEnumerable<InvoiceAndPaymentInquiryDetail> qry = db.Fetch<InvoiceAndPaymentInquiryDetail>("GetAllInvoiceAndPaymentInquiry", new object[] { 
                    SupplierCode == null ? "" : SupplierCode, 
                    (InvoiceDateFrom == "" || InvoiceDateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(InvoiceDateFrom,"dd.MM.yyyy",null),
                    (InvoiceDateTo == "" || InvoiceDateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(InvoiceDateTo,"dd.MM.yyyy",null),
                    InvoiceNo == null ? "" : InvoiceNo,
                    StatusCode == null ? "" : StatusCode,
                    (PlanPaymentDateFrom == "" || PlanPaymentDateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PlanPaymentDateFrom,"dd.MM.yyyy",null),
                    (PlanPaymentDateTo == "" || PlanPaymentDateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(PlanPaymentDateTo,"dd.MM.yyyy",null),
                    SupplierCodeLDAP,
                    (UploadDateFrom == "" || UploadDateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(UploadDateFrom,"dd.MM.yyyy",null),
                    (UploadDateTo == "" || UploadDateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(UploadDateTo,"dd.MM.yyyy",null)
                });
            hasil = exporter.Write(qry, "InvoiceInquiryData");
            db.Close();

            Response.Clear();
            
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        public void DownloadDetail(string InvoiceNo, string InvoiceDate, string SupplierCode)
        {
            if (!string.IsNullOrEmpty(SuppCd))
            {
                SupplierCode = SuppCodeValid(SupplierCode);
            }

            string fileName = "InvoiceDetail_" + InvoiceNo + "_" + InvoiceDate + "_" + SupplierCode + ".xls";

            IDBContext db = DbContext;
            DateTime InvoiceDateDt = (InvoiceDate == "" || InvoiceDate == null) ? DateTime.ParseExact("190001011200", "yyyyMMddhhmm", null) : DateTime.ParseExact(InvoiceDate, "yyyyMMddhhmm", null);
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            IEnumerable<InvoiceAndPaymentDetail> qry = db.Fetch<InvoiceAndPaymentDetail>("GetInvoiceAndPaymentDetail", 
                    new object[] { InvoiceNo, InvoiceDateDt, SupplierCode });
            hasil = exporter.Write(qry, "InvoiceDetailData");
            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        #region DownloadCSV

        public void WriteCSV<T>(IEnumerable<T> items, Stream stream)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(string.Join("; ", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join("; ", props.Select(p => p.GetValue(item, null))));
                }
            }
        }
        public FileContentResult DownloadListCSVNew(
            string SupplierCode,
            string UploadDateFrom,
            string UploadDateTo,
            string InvoiceDateFrom,
            string InvoiceDateTo,
            string InvoiceNo,
            string StatusCode,
            string PlanPaymentDateFrom,
            string PlanPaymentDateTo,
            string Automatic)
        {
            Stream output = new MemoryStream();
            string fileName = "InvoicePayment.csv";
            List<InvoiceAndPaymentInquiryDetail> lstInvoiceAndPaymentInquiryDetail = new List<InvoiceAndPaymentInquiryDetail>();
            byte[] documentBytes;

            try
            {
                lstInvoiceAndPaymentInquiryDetail = GetAllInvoice(
                                SuppCodeValid(SupplierCode),
                                UploadDateFrom,
                                UploadDateTo,
                                InvoiceDateFrom,
                                InvoiceDateTo,
                                InvoiceNo,
                                StatusCode,
                                PlanPaymentDateFrom,
                                PlanPaymentDateTo, 
                                Automatic);

                using (MemoryStream buffer = new MemoryStream())
                {
                    WriteCSV(lstInvoiceAndPaymentInquiryDetail, buffer);
                    documentBytes = buffer.GetBuffer();
                }
                
                return File(documentBytes, "text/csv", fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DownloadPDF
        public string CheckBeforePrintCertificate(string InvoiceNos, string InvoiceDates, string SupplierCodes)
        {
            string result = "";

            try
            {
                string role = "";
                foreach (Authorization r in AuthorizedUser.Authorization)
                {
                    role = role + r.AuthorizationID + ",";
                }

                List<string> lstInvoiceDatesDt = new List<string>();
                string resultInvoiceDates = "";
                lstInvoiceDatesDt = InvoiceDates.Split(',').ToList<string>();

                foreach (string s in lstInvoiceDatesDt)
                {
                    DateTime InvoiceDatesDt = (s == "" || s == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(s, "dd.MM.yyyy", null);
                    resultInvoiceDates += InvoiceDatesDt.ToString("yyyyMMdd") + ",";
                }

                result = DbContext.ExecuteScalar<string>("CheckBeforePrintCertificate",
                        new object[] { InvoiceNos, resultInvoiceDates, SupplierCodes, role });
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally
            {
                DbContext.Close();
            }

            return result;
        }

        public FileContentResult PrintCertificate(string InvoiceNos, string InvoiceDates, string SupplierCodes)
        {
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string fileName;
            string dateString = DateTime.Now.ToString("yyyyMMddHHmmss");

            if (!string.IsNullOrEmpty(SupplierCodes) && !string.IsNullOrEmpty(SuppCd))
            {
                if (SupplierCodes.IndexOfAny(new char[] { ',', ';' }) > 0)
                {
                    string[] suppCodes = SupplierCodes.Split(',', ';');

                    for (int i = 0; i< suppCodes.Length; i++)
                        suppCodes[i] = SuppCodeValid(suppCodes[i]);
                    SupplierCodes = string.Join(",", suppCodes);                            
                }
            }

            try
            {
                String processID = "";
                //fileName = "InvoicePayment_" + InvoiceNos + "_" + InvoiceDates + "_" + SupplierCodes + ".pdf";
                fileName = "CreateInvoiceCertificateID_" + dateString;
                processID = DbContext.ExecuteScalar<string>(
                       "GenerateProcessId",
                       new object[] { 
                            "Print Certificate ID InvoicePayment_" + InvoiceNos + "_" + InvoiceDates + "_" + SupplierCodes + ".pdf is started", 
                            AuthorizedUser.Username, 
                            "InvoiceAndPaymentInquiry.PrintCertificateID",
                            "",
                            "MPCS00002INF", "INF", "5", "53001",     // function
                            "0"      // sts
                        });
                documentBytes = PrintCertificateReport(InvoiceNos, InvoiceDates, SupplierCodes);
                processID = DbContext.ExecuteScalar<string>(
                       "GenerateProcessId",
                       new object[] { 
                            "Print Certificate ID InvoicePayment_" + InvoiceNos + "_" + InvoiceDates + "_" + SupplierCodes + ".pdf is finished", 
                            AuthorizedUser.Username, 
                            "InvoiceAndPaymentInquiry.PrintCertificateID",
                            processID,
                            "MPCS00003INF", "INF", "5", "53001",     // function
                            "0"      // sts
                        });

                return File(documentBytes, "application/pdf", fileName + ".pdf");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Telerik.Reporting.Report CreateReport(string reportPath,
            string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }

        private byte[] PrintCertificateReport(string InvoiceNos, string InvoiceDates, string SupplierCodes)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            Telerik.Reporting.ReportParameterCollection reportParameters = new Telerik.Reporting.ReportParameterCollection();
            if (!string.IsNullOrEmpty(SuppCd) && !string.IsNullOrEmpty(SupplierCodes))
            {
                List<string> a = SupplierCodes.Split(',').ToList();
                for (int i = 0; i < a.Count; i++)
                {
                    a[i] = SuppCodeValid(a[i]);
                }
                SupplierCodes = string.Join(",", a);
                // SupplierCodes = SuppCodeValid(SupplierCodes);
            }
            List<string> lstInvoiceDatesDt = new List<string>();
            string resultInvoiceDates = "";
            lstInvoiceDatesDt = InvoiceDates.Split(',').ToList<string>();

            foreach (string s in lstInvoiceDatesDt)
            {
                DateTime InvoiceDatesDt = (s == "" || s == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(s, "dd.MM.yyyy", null);
                resultInvoiceDates += InvoiceDatesDt.ToString("yyyyMMdd") + ",";
            }
            parameters.Add("@InvoiceNos", System.Data.DbType.String, InvoiceNos);
            parameters.Add("@InvoiceDates", System.Data.DbType.String, resultInvoiceDates);
            parameters.Add("@SupplierCodes", System.Data.DbType.String, SupplierCodes);
            reportParameters.Add("UserName", Telerik.Reporting.ReportParameterType.String, AuthorizedUser.Username);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\InvoiceAndPayment\RepInvoice_Certificate.trdx", "SP_RepInvoice_Certificate_Print", parameters);
            rpt.ReportParameters.AddRange(reportParameters);

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            
            return result.DocumentBytes;
        }
        #endregion
        private byte[] SetNoticeIcon(int val)
        {
            byte[] img = null;
            string imageBytes1 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkJJREFUeNpkUk1rFEEQfdXT85XsGkPcHMSD6Aa8evCoSO4BT+JJ8A8oguLBHPwDIip4EPwDCmpAES8eDILKJsegHrwEURKy2Y/JZmdnusvq3llItKAoerqr3nv1hlqtFlwYpIiwNTOLL5eP8relmv11JuBRHRxlAH2H6r8B7T6X2gaGADOo1VqTxgRz+LB0glbux1wuoFQyjf0DHyRnIkBlP6F/3wKyV5ArVaKOeaxcOU3PXsc8tYA8APIeUOxKdoDSZVeGDaSmp1DMvwSrq2AD6qydb9a5va64WUcuDaNtwBqBs3DTPSIkSQNBIpkC4fYA4eY5PUOrN2Av1lHI5OEfQcjloTSiouwK05g6FzLT3dWmEIY3Ndvji2TiqnFfHpRVX9U8CX+0YxZ2WljXLmhiNFDK9uxedclV0v8DnBwyvhLbWc0oumR6DY9qnQV2TBGV1gms27yTYx19LV8Gfb2DxU/H7GoTlsd67ci7Xnl0AFaaVCRVFkdDWfjgqwL1HkH3C79NEptEh6PFPkW/S2+q8zqUKqhhR76WD9Uc3q1zNLiDSFBVOEY4GP4HcYNlqVoyyVCGvWWx+bP2q1HmAdJ2Dq7dg40aTjd5jcF4oKOrFTjOemWcLYvyx06Rpo7QSIREWD5RSf9kUBy5DZ6WBtEdOCLUhR5u2ih/XyrzVEb+mJDStK9gFGGo+Gxa0DVvR2hQxvmGCsx1xbwhy92y5P/2Q6EFPhQGl9Kc7uqR2jHJ3sdCm7cmwIuE0T9k+z/xV4ABAP9iEDZwmfEuAAAAAElFTkSuQmCCAA==";
            string imageBytes2 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAbZJREFUeNp8Uz1Lw1AUfUlfShFa65BdpAVX/4CDqxScxElwFiqCxUF3pyA6KPQnKKiDDq6CICKOFl3dkloaI/1Kk3pOyINH/HhweC8355x7372J4TiO4DJNU4RhOB0EwWq/36/hPD+ZTIqGYXwBr+Px+Ho0Gp0h3omiKNFIJex2u7VOp+PgXAVZQJgQ0r2Sy+WWLctqwGQHz5eJTkop2u32muu6V/l8vsogMvwARAIGc4VC4QLm6zSVrVarEsdxs1QqmcPhUBB8oTKzCrWDRwOBJKe9Xu9J+r6/Zdt2kc6DwSAhKKG+9Kug2ilcb1uijCW6oUmCjdCzZpcmZgWLJs42sxJ/CfXmaZwZiTJ9CG1mVSPQ76oWr8MYQR40gSyXy/eYbUUR/rszR0qQA4NHE0M/BkLlqsr7zYhCcjC6GOIj0/O8Z5SwSyJfZrOpnU0l2BuMcx+mD2ZaxiECm9g9ElR5BDtLpJ/vJ8ZZh/Ag6bq6AxxPEJzFB9AgmbF0+SjxHbgFmnh+Uy+kKg3kBWTdUKOA2QvCdYC7i1iUbSJ/DAtYQbY9VPGB0u6Q4Qbkc5gF2ZHp61uAAQA8ZE6FNcNYIAAAAABJRU5ErkJggg==";
            string imageBytes3 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAl9JREFUeNpcUj1rVEEUvfPx3m7W7IriglZaRNHSIq1FGkEIWImd+AcMglaxsLMSUcHCUhBEC5tY2CpBUIhYGNBChCiJcbNfb/d9zMy9453dWRJ8cJk3H+fcc86M8N7D7MusP/x1gFd+lX654+CsFaKZKjkSAr5lBa71cnyZ5a5bWJycFzPw545d/tCl+7qmToMSYEAA8ZbgPZ7y6GFU0I/tXnUrK/G1EBG8vmOuvu/i85NHU9l3AF3rwTEw0M7ANSmgrgDGOcLP3fKaRf9M3Ps0WCi12jh3vNb8YwB2qgD0QLAPllxJAHPNa4BOz+Rbf6tFvdGxK0tn6s0h2/hdEpSsFT2L3I8CgkRJBEaEdSZoqEaixE19oqWXaqmALQaOHU3l+mg2dg8yAgEJzxYENNnHfF1e0KykXSCHYWkC8tErHOg8mTIBsX7mB5R8DvwRbdAP+pbaoWsZNqJHGeVCJPORoOKx5BobyvTisWT9y9gtgJJQYSSIXcWBztPEAdJEgjEe8tx9lMPCPcpGziYTP4GeA+NwbAiIyRxN7UjunTI4nBsMDRUVPlTppZVtIBrVUnVR8D2akDZNWwfpmrUH0BwzN8LjGTnY26tWmfCFTjkx5/yDfq+qGq3k7pyW7VloQUkABrmKF/O+GQ779g46/zjkoVVLARYYPDxhklOtdu12kx8ChVfCCoSlgbV+a5i7t6bAp8zxXcQwtKxLcBWCNXg+bejrxBtoCIrMbnryN4QUm+RgFx0hiBhiDFTzT8IHLtdbyapuyL2sX70zOb7Bil7VD+lMyumVhfrv6uGfAAMA1xJkKTxEsyUAAAAASUVORK5CYII=";
            string imageBytes4 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAbpJREFUeNqMkj9rFFEUxc+9783urG4wu8QixFUQsRBELXRLo6VdPkZInUQQey3yESwCNn4BW6tUmkYsRAiJIIp/YGfZJLN5f71vYiWvmGEGZt6c3z33nvcImev2o/Xt0Xh9Y16fgDoXcfRh983Bu53N/3U6B6tyeK0yg5F1GlovgMql6zkd5xZd8JY4gDmCVQQo2NawyEFMzcNKJERo7+ytIVYgAdOD4LLOJOE85XK47INvHIPcl66OV/Xgzt0YLVSni9PJ5y/V4d5baaAxYyls6+o3Pdn+OKnsYJFlRvxrM3oDH0zqHTERRReq2xModUJQSsGZ6UzX8+OZpLp4Hg7DiL3xEYHSzLKmGeIDFb38Py9OUsSZ45nmTr9fFAvSckQtpJEGKInJQWkFVRCKskTRuyDfUkhAlnVTc18f7e++4t7l0Zl1sF72WKj+lXv3u8NbN5XyDXg2Pfj659P7PXFkaowFPpn8zG7ByuOtnQfPf8Xxs8P48GUVb6y9eJ3T5U+YKjSn8MRGpYB0R7eGJSokmCI1c6bD0hqWRHWaK+XMsi3yXrSG3Xz6w7npdwo19JxgTiffcrq/AgwAN6+YiWduV/UAAAAASUVORK5CYIIA";

            if (val % 4 == 0)
            {
                img = Convert.FromBase64String(imageBytes1);
            }
            else if (val % 4 == 1)
            {
                img = Convert.FromBase64String(imageBytes2);
            }
            else if (val % 4 == 2)
            {
                img = Convert.FromBase64String(imageBytes3);
            }
            else if (val % 4 == 3)
            {
                img = Convert.FromBase64String(imageBytes4);
            }

            return img;
        }

        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_close_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_open_icon.png";
            string imageBytes4 = "~/Content/Images/notice_add_icon.png";

            if (val % 3 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 3 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 3 == 2)
            {
                img = imageBytes3;
            }


            return img;
        }
        private string SetGRStatus(int val)
        {
            string img = string.Empty;

            if (val % 4 == 0)
            {
                img = "Not Yet Scanned";
            }
            else if (val % 4 == 1)
            {
                img = "Scanned OK";
            }
            else if (val % 4 == 2)
            {
                img = "Scanned NG";
            }
            else if (val % 4 == 3)
            {
                img = "Approved";
            }

            return img;
        }

        #region Upload PPV Evidence
        public ActionResult PPVEVFileUpload_CallbackRouteValues(IEnumerable<UploadedFile> ucCallbacks)
        {
            DevExpress.Web.Mvc.UploadControlExtension.GetUploadedFiles("PPVEVFileUpload", ValidationSettings, FileUploadComplete);

            return null;
        }

        public ActionResult DownloadCallback()
        {
            return null;
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls", ".xlsx", ".pdf", ".doc", ".docx", ".txt", ".jpg", ".gif", ".png", ".zip" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        //private string _FilePath = "";
        private string UploadDir
        {
            get
            {
                string domain = ConfigurationManager.AppSettings["LdapDomain"];
                string app = ConfigurationManager.AppSettings["DeploymentContext"];

                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), domain, app);
            }
        }

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (!e.UploadedFile.IsValid) return;

            User u = Model.GetModel<User>();
            string uid = (u != null) ? u.Username : "?";

            string Supp_Inv_No = TempData["SUPP_INV_NO"] != null ? TempData["SUPP_INV_NO"].ToString() : (Session["SUPP_INV_NO"] != null ? Session["SUPP_INV_NO"] as string : "");
            string Supp_Cd = TempData["SUPP_CD"] != null ? TempData["SUPP_CD"].ToString() : (Session["SUPP_CD"] != null ? Session["SUPP_CD"] as string : "");

            string fName = e.UploadedFile.FileName;
            string fPath = Path.Combine(UploadDir, "InvoicePPVEvidence", Supp_Cd, Supp_Inv_No.Trim());

            if (!Directory.Exists(fPath))
            {
                Directory.CreateDirectory(fPath);
            }
            else
            {
                Directory.Delete(fPath, true);
                Directory.CreateDirectory(fPath);
            }

            string ufile = Path.Combine(fPath, e.UploadedFile.FileName);
            FileStream f = new FileStream(ufile, FileMode.CreateNew, FileAccess.ReadWrite);

            f.Write(e.UploadedFile.FileBytes, 0, e.UploadedFile.FileBytes.Length);
            f.Close();

            IDBContext db = DbContext;
            db.Execute("SaveTempPPVEvidenceFile", new object[] { Supp_Inv_No, Supp_Cd, e.UploadedFile.FileName, u.Username });
        }

        public string GetUploadedTempPPVEvidenceFiles(string supp_cd, string supp_inv_no)
        {
            IDBContext db = DbContext;
            string FilePath = db.ExecuteScalar<string>("GetTempFilePPVEvidence", new object[] { supp_inv_no, supp_cd });

            if (FilePath != null && FilePath != "")
            {
                return FilePath;
            }
            else
            {
                return "";
            }
        }

        public string DeletePPVEvidence(string supp_cd, string supp_inv_no, string filepath)
        {
            string fPath = Path.Combine(UploadDir, "InvoicePPVEvidence", supp_cd, supp_inv_no.Trim());
            string ufile = Path.Combine(fPath, filepath);

            if (System.IO.File.Exists(ufile))
            {
                System.IO.File.Delete(ufile);
            }

            IDBContext db = DbContext;
            string result = db.ExecuteScalar<string>("DeleteTempFilePPVEvidence", new object[] { supp_inv_no, supp_cd, filepath });

            return result;
        }

        public FileContentResult DownloadPPVEvidence(string supp_cd, string supp_inv_no, string filepath)
        {
            byte[] documentBytes = new byte[] { 1 };
            string fPath = Path.Combine(UploadDir, "InvoicePPVEvidence", supp_cd, supp_inv_no.Trim());
            string ufile = Path.Combine(fPath, filepath);

            if (System.IO.File.Exists(ufile))
            {
                documentBytes = StreamFile(ufile);
            }

            return File(documentBytes, "application/octet-stream", filepath);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] ImageData = new byte[fs.Length];
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return ImageData; //return the byte data
        }
        #endregion
    }
}

