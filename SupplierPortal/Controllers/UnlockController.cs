﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using System.Web.Routing;
using Cryptography;
using Toyota.Common.Web;
using Toyota.Common.Web.MVC.Filter;


namespace Portal.Controllers
{
    [MarkAsUnlockController]
    public class UnlockController : BaseController
    {

        public UnlockController()
            : base("Unlock Screen")
        { }

        protected override void Init()
        { }

        protected override void StartUp()
        {
            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();
            if (user != null)
            {
                ViewData["UserID"] = user.Username; 
            }
        }

        [UnlockIgnoreAction]
        [HttpPost]
        public ActionResult UnlockApp(string Username, string Password)
        {
            Crypter c = new Crypter(new ConfigKeySym());
            if (c.Decrypt((string)Session[ResourceSystemNames.__CRED_P]) == Password)
            { 
                SessionState.SetUnlockState(false);  
                PreviousHandler handler = Lookup.Get<PreviousHandler>();
                handler.SetLastActionTimeSpan();
                ActionExecuting action;
                for (int i = 0; i < handler.MaxItem; i++)
                {
                    action = handler.Get(i);
                    if (action == null || i == handler.MaxItem)
                    {
                        RedirectToRoute(new RouteValueDictionary(new { controller = "dashboard" }));
                        break;
                    }
                    if (action != null)
                    {
                        if (action.Controller.ToLower() != "unlock")
                        {
                            RedirectToRoute(new RouteValueDictionary(new { controller = action.Controller }));
                            break;
                        }
                    }
                }
                 
            }
            else
            {
                SessionState.SetUnlockState(true);
                ViewData["ErrorMessage"] = "Invalid password.";
            }
            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();
            if (user != null)
            {
                ViewData["UserID"] = user.Username;
                ViewData["Username"] = user.FullName;
            }
            return View("Index", Model);
        }
         
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        { 
            PreviousHandler handler = Lookup.Get<PreviousHandler>();
            if (handler != null)
            {
                Toyota.Common.Web.Credential.User user = Lookup.Get<ISessionState>().GetAuthorizedUser();
                if (user != null)
                { 
                    TimeSpan timer = handler.LastAction;
                    TimeSpan timerTimeOut = new TimeSpan(timer.Ticks).Add(TimeSpan.FromMinutes((double)user.UnlockTimeOut));
                    TimeSpan timerTimeNow = new TimeSpan(DateTime.Now.Ticks);
                    if (timerTimeNow < timerTimeOut)
                    {
                        string ctrlName, actionName;
                        ActionExecuting action;
                        for (int i = 0; i < handler.MaxItem; i++)
                        {
                            action = handler.Get(i);
                            if (action == null || i == handler.MaxItem)
                            {
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "dashboard" }));
                                break;
                            }
                            if (action != null)
                            {
                                ctrlName = action.Controller;
                                actionName = action.Action;
                                if (ctrlName.ToLower() != "unlock")
                                {
                                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = ctrlName }));
                                    break;
                                }
                            }
                        }
                    }
                }
            } 
        } 
       
    }
}
