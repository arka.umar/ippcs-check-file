﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;
using Portal.Models.Delivery.PurchaseOrder;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;

namespace Portal.Controllers
{
    public class D31202Controller : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

        //public POCreation modelExport;

        public D31202Controller()
            : base("D31202", "Delivery Purchase Order Creation")
           
        {

        }
        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            MainModel mdl = new MainModel();
            ViewData["viewPartnerRef"] = FillPartner();
            ViewData["StatusLookup"] = GetStatus();
            Model.AddModel(mdl);
        }

        public ActionResult SectionHead()
        {
            return PartialView("IndexHeader");
        }

        public ActionResult SectionGrid()
        {
            
            string vPO_MONTH = String.IsNullOrEmpty(Request.Params["pPO_MONTH"]) ? "" : Request.Params["pPO_MONTH"];
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NUMBER"]) ? "" : Request.Params["pPO_NUMBER"];
            string vLP_CD = String.IsNullOrEmpty(Request.Params["pLP_CD"]) ? "" : Request.Params["pLP_CD"];
            string vPR_NO = String.IsNullOrEmpty(Request.Params["pPR_NUMBER"]) ? "" : Request.Params["pPR_NUMBER"];
            string vSTATUS = String.IsNullOrEmpty(Request.Params["pSTATUS"]) ? "" : Request.Params["pSTATUS"];
            
            MainModel mdl = Model.GetModel<MainModel>();
            mdl.MainDatas = GetPurchaseOrder(vPO_MONTH, vPO_NO, vLP_CD, vPR_NO, vSTATUS);
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        protected List<MainData> GetPurchaseOrder(string vPO_MONTH, string vPO_NO, string vLP_CD, string vPR_NO, string vSTATUS)
        {

            string lpcdparameter = string.Empty;
            if (!string.IsNullOrEmpty(vLP_CD))
            {
                string[] lpcdlist = vLP_CD.Split(';');
                foreach (string r in lpcdlist)
                {
                    lpcdparameter += "'" + r + "',";
                }
                lpcdparameter = lpcdparameter.Remove(lpcdparameter.Length - 1, 1);
            }

            string statusparameter = string.Empty;
            if (!string.IsNullOrEmpty(vSTATUS)) 
            {
                string[] statuslist = vSTATUS.Split(';');
                foreach (string r2 in statuslist)
                {
                    statusparameter += "'" + r2 + "',";
                }
                statusparameter = statusparameter.Remove(statusparameter.Length -1,1);
            }

            List<MainData> listOfRouteMaster = db.Fetch<MainData>("DLV_GetMainDataPO", new object[] { 
                vPO_MONTH, vPO_NO, lpcdparameter, vPR_NO, statusparameter
            });
            return listOfRouteMaster;

        }
        public ActionResult viewPartnerRef()
        {
            TempData["GridName"] = "gridPartnerRefView";
            ViewData["viewPartnerRef"] = FillPartner();

            return PartialView("PG", ViewData["viewPartnerRef"]);
        }

        public List<MainData> FillPartner()
        {
            List<MainData> listLogisticPartner = new List<MainData>();
            try
            {
                listLogisticPartner = db.Fetch<MainData>("DLV_GetAllLogisticPartner", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listLogisticPartner;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        #region Export To Excel
        public ActionResult exportToExcel()
        {
            string vPO_MONTH = String.IsNullOrEmpty(Request.Params["pPO_MONTH"]) ? "" : Request.Params["pPO_MONTH"];
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NUMBER"]) ? "" : Request.Params["pPO_NUMBER"];
            string vLP_CD = String.IsNullOrEmpty(Request.Params["pLP_CD"]) ? "" : Request.Params["pLP_CD"];
            string vPR_NO = String.IsNullOrEmpty(Request.Params["pPR_NUMBER"]) ? "" : Request.Params["pPR_NUMBER"];
            string vSTATUS = String.IsNullOrEmpty(Request.Params["pSTATUS"]) ? "" : Request.Params["pSTATUS"];

            if (vPO_MONTH != "")
            {
                if (IsDateTime(vPO_MONTH))
                {
                    string date = vPO_MONTH;
                    DateTime dt = Convert.ToDateTime(date);
                    vPO_MONTH = dt.ToString("yyyyMM");
                }
            }

            MainModel mdl = Model.GetModel<MainModel>();
            mdl.MainDatas = GetPurchaseOrder(vPO_MONTH, vPO_NO, vLP_CD, vPR_NO, vSTATUS);

            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Worksheets.Add("Delivery Purchase Order");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Name = "Delivery Purchase Order";
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";
                //header
                string excelLink = "";
                int columncount = 12;
                string successStatus = "";
                ws.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA"; // Heading Name
                ws.Cells[1, 1, 1, columncount].Merge = true; //Merge columns start and end range
                ws.Cells[1, 1, 1, columncount].Style.Font.Bold = true; //Font should be bold
                ws.Cells[1, 1, 1, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                ws.Cells[2, 1].Value = "Delivery Purchase Order"; // Heading Name
                ws.Cells[2, 1, 2, columncount].Merge = true; //Merge columns start and end range
                ws.Cells[2, 1, 2, columncount].Style.Font.Bold = true; //Font should be bold
                ws.Cells[2, 1, 2, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                //set header kolom
                #region kolom


                ws.Cells.AutoFitColumns(20, 40);
                ws.Cells[4, 1].Value = "PO Number";
                ws.Cells[4, 1].Style.Font.Bold = true;
                ws.Cells[4, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ws.Cells[4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 1, 5, 1].Merge = true;
                ws.Cells[4, 1, 5, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                ws.Cells[4, 2].Value = "PO Month";
                ws.Cells[4, 2].Style.Font.Bold = true;
                ws.Cells[4, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ws.Cells[4, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 2, 5, 2].Merge = true;
                ws.Cells[4, 2, 5, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ws.Cells[4, 3].Value = "Trucking";
                ws.Cells[4, 3].AutoFitColumns();
                ws.Cells[4, 3].Style.Font.Bold = true;
                ws.Cells[4, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ws.Cells[4, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 3, 4, 4].Merge = true;
                ws.Cells[4, 3, 4, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ws.Cells[5, 3].Value = "Code";
                ws.Cells[5, 3].Style.Font.Bold = true;
                ws.Cells[5, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[5, 4].Value = "Name";
                ws.Cells[5, 4].Style.Font.Bold = true;
                ws.Cells[5, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[4, 5].Value = "Total Route PO";
                ws.Cells[4, 5].Style.Font.Bold = true;
                ws.Cells[4, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ws.Cells[4, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 5, 5, 5].Merge = true;
                ws.Cells[4, 5, 5, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                

                ws.Cells[4, 6].Value = "Remaining Route Unprocessed";
                ws.Cells[4, 6].Style.Font.Bold = true;
                ws.Cells[4, 6].Style.WrapText = true;
                ws.Cells[4, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ws.Cells[4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 6, 5, 6].Merge = true;
                ws.Cells[4, 6, 5, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ws.Cells[4, 7].Value = "PR Number";
                ws.Cells[4, 7].Style.Font.Bold = true;
                ws.Cells[4, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ws.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 7, 5, 7].Merge = true;
                ws.Cells[4, 7, 5, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ws.Cells[4, 8].Value = "Status";
                ws.Cells[4, 8].Style.Font.Bold = true;
                ws.Cells[4, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                ws.Cells[4, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 8, 5, 8].Merge = true;
                ws.Cells[4, 8, 5, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ws.Cells[4, 9].Value = "Created";
                ws.Cells[4, 9].Style.Font.Bold = true;
                ws.Cells[4, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 9, 4, 10].Merge = true;
                ws.Cells[4, 9, 4, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ws.Cells[5, 9].Value = "By";
                ws.Cells[5, 9].Style.Font.Bold = true;
                ws.Cells[5, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[5, 10].Value = "Date";
                ws.Cells[5, 10].Style.Font.Bold = true;
                ws.Cells[5, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[4, 11].Value = "Changed";
                ws.Cells[4, 11].Style.Font.Bold = true;
                ws.Cells[4, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 11, 4, 12].Merge = true;
                ws.Cells[4, 11, 4, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ws.Cells[5, 11].Value = "By";
                ws.Cells[5, 11].Style.Font.Bold = true;
                ws.Cells[5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[5, 12].Value = "Date";
                ws.Cells[5, 12].Style.Font.Bold = true;
                ws.Cells[5, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 12].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                #endregion

                #region detailnya

                List<MainData> m = mdl.MainDatas.ToList<MainData>();

                int i = 0;
                foreach (MainData x in m)
                {

                    string changedBy = m[i].CHANGE_DT.ToString();
                    var changedBy1 = "";

                    if (changedBy == "1/1/0001 12:00:00 AM")
                    {
                        changedBy1 = "";
                    }
                    else
                    {
                        changedBy1 = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CHANGE_DT);
                    }

                    string createdBy = m[i].CREATE_DT.ToString();
                    var createdBy1 = "";

                    if (createdBy == "1/1/0001 12:00:00 AM")
                    {
                        createdBy1 = "";
                    }
                    else
                    {
                        createdBy1 = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CREATE_DT);
                    }

                    ws.Cells[i + 6, 1].Value = m[i].PO_NUMBER;
                    ws.Cells[i + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 2].Value = m[i].PRODUCTION_MONTH;
                    ws.Cells[i + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 3].Value = m[i].CODE;
                    ws.Cells[i + 6, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 4].Value = m[i].LOG_PARTNER_NAME;
                    //ws.Cells[i + 6, 3].AutoFitColumns();
                    ws.Cells[i + 6, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 5].Value = m[i].TOTAL_ROUTE;
                    ws.Cells[i + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    if (m[i].REMAINING_ROUTE == "0")
                    {
                        ws.Cells[i + 6, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[i + 6, 6].Style.Fill.BackgroundColor.SetColor(Color.White);
                    }
                    else
                    {
                        ws.Cells[i + 6, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[i + 6, 6].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    }

                    ws.Cells[i + 6, 6].Value = m[i].REMAINING_ROUTE;
                    ws.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 7].Value = m[i].PR_NUMBER;
                    ws.Cells[i + 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    if (m[i].STATUS == "")
                    {
                        ws.Cells[i + 6, 8].Value = m[i].STATUS;
                        ws.Cells[i + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    else
                    {
                        ws.Cells[i + 6, 8].Value = m[i].STATUS;
                        ws.Cells[i + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    //ws.Cells[i + 6, 6].AutoFitColumns();
                    ws.Cells[i + 6, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 9].Value = m[i].CREATE_BY;
                    //ws.Cells[i + 6, 7].AutoFitColumns();
                    ws.Cells[i + 6, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 10].Value = createdBy1;
                    ws.Cells[i + 6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 11].Value = m[i].CHANGE_BY;
                    //ws.Cells[i + 6, 7].AutoFitColumns();
                    ws.Cells[i + 6, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    //ws.Cells[i + 6, 12].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CHANGE_DT);
                    ws.Cells[i + 6, 12].Value = changedBy1;
                    ws.Cells[i + 6, 12].AutoFitColumns();
                    ws.Cells[i + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[i + 6, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    i++;
                }

                #endregion

                try
                {
                    Byte[] bin = p.GetAsByteArray();
                    string fn = "DeliveryPurchaseOrder"+ "-" + AuthorizedUser.Username + ".xlsx";
                    string DownloadDirectory = "Content/Download/D31202/";
                    string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
                    var pathfile = Path.Combine(downdir, fn);

                    excelLink = "Content/Download/D31202/" + fn + "";
                    successStatus = "True";
                    System.IO.File.WriteAllBytes(pathfile, bin);

                    //================[ UPDATE DOWNLOAD DATA ]================
                    //i = 0;
                    //   foreach (MainData x in m)
                    //   {
                    //       UpdateDownloadedInfo(m[i].PO_NUMBER, m[i].PRODUCTION_MONTH, (AuthorizedUser.Username.Length > 20) ? AuthorizedUser.Username.Substring(0, 20) : AuthorizedUser.Username);
                    //   }

                }
                catch (Exception err)
                {
                    //write log
                    successStatus = "False";
                }
                return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);


            }
        }

        private void DownloadFile(string fname)
        {
            bool forceDownload = true;
            string path = fname;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            string type = "";
            // set known types based on file extension  
            if (ext != null)
            {
                switch (ext.ToLower())
                {

                    case ".xlsx":
                    case ".xls":
                        type = "Application/msexcel";
                        break;
                }
            }
            if (forceDownload)
            {
                Response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            if (type != "")
                Response.ContentType = type;
            Response.WriteFile(path);
            Response.End();
        }


        public ContentResult UpdateDownloadedInfo(string stpono, string stmonth, string stusername)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string errorMessage = "";

            try
            {
                db.Execute("DLV_UpdateDownloadPO", new object[] { 
                            stpono,stmonth, stusername});
                errorMessage = "Master data saved.";

            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = "Failed on updating process.";
            }

            return Content(((isError == 1) ? "Error|" : "Sucess|") + errorMessage);
        }
        #endregion
        //-----------------------------------------------------------------------------------------------------------------------


        //-------------------------------------[ POPUP PO ]---------------------------------------------------------
        public ActionResult PopupDetailPO()
        {
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NO_DET"]) ? "" : Request.Params["pPO_NO_DET"];
            TempData["GridName"] = "gridmonPopupPODetail";
            ViewData["PopupDetailPO"] = FillviewPODetail(vPO_NO);
            return PartialView("monPopupPODetail", ViewData["PopupDetailPO"]);
        }
        public List<PODetailData> FillviewPODetail(string vPO_NO)
        {
            List<PODetailData> listOfData = new List<PODetailData>();
            try
            {
                listOfData = db.Fetch<PODetailData>("DLV_getPODetail", new object[] { vPO_NO });
            }
            catch (Exception ex) { throw ex; }
            return listOfData;
        }



        //-----------------------------------------------------------------------------
        public ActionResult PopupDetailPOHeader()
        {
            List<PODetailData> HeaderData = new List<PODetailData>();

            try
            {
                string vPO_NO = String.IsNullOrEmpty(Request.Params["PO_NO"]) ? "" : Request.Params["PO_NO"];
                HeaderData = db.Query<PODetailData>("DLV_getPODetail", new object[] { vPO_NO }).ToList();
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return Json(HeaderData);
        }

        public ActionResult monPopupPOHead()
        {
            return PartialView("monPopupPOHead");
        }

        public ActionResult monPopupPODetail()
        {
            return PartialView("monPopupPODetail");
        }


        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }

        public ActionResult monPopupPRHead()
        {
            return PartialView("monPopupPRHead");
        }

        public ActionResult monPopupPRDetail()
        {
            return PartialView("monPopupPRDetail");
        }

        public ActionResult PopupDetailPR()
        {
            string vPR_NO = String.IsNullOrEmpty(Request.Params["pPR_NO_DET"]) ? "" : Request.Params["pPR_NO_DET"];
            TempData["GridName"] = "gridmonPopupPRDetail";
            ViewData["PopupDetailPR"] = FillviewPRDetail(vPR_NO);
            return PartialView("monPopupPRDetail", ViewData["PopupDetailPR"]);
        }
        public List<PRDetailData_PO> FillviewPRDetail(string vPR_NO)
        {
            List<PRDetailData_PO> listOfData = new List<PRDetailData_PO>();
            try
            {
                listOfData = db.Fetch<PRDetailData_PO>("DLV_getPRDetailPO", new object[] { vPR_NO });
            }
            catch (Exception ex) { throw ex; }
            return listOfData;
        }


        public ActionResult PopupDetailPRHeader()
        {
            List<PRDetailData_PO> HeaderData = new List<PRDetailData_PO>();

            try
            {
                string vPR_NO = String.IsNullOrEmpty(Request.Params["PR_NO"]) ? "" : Request.Params["PR_NO"];
                HeaderData = db.Query<PRDetailData_PO>("DLV_getPRDetailPO", new object[] { vPR_NO }).ToList();
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return Json(HeaderData);
        }

        public bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            txtDate = txtDate.Replace(".", "-");
            return DateTime.TryParse(txtDate, out tempDate) ? true : false;
        }

        public string SetFormatDate(string txtDate, string onformat = "yyyy-MM-dd")
        {
            string date = txtDate;
            if (IsDateTime(date))
            {
                DateTime dt = Convert.ToDateTime(date);
                date = dt.ToString(onformat);
            }
            return date;
        }

        public string EnglishDate(string txtDate)
        {
            try
            {
                txtDate = txtDate.Replace(".", "-");
                string[] date = txtDate.Split('-');
                return date[2] + '-' + date[1] + '-' + date[0];
            }
            catch (Exception e)
            {
                return "";
            }
        }


        //GETTING Status FOR LOOKUP

        public ActionResult StatusLookup()
        {

            TempData["GridName"] = "PO_STATUS";
            ViewData["StatusLookup"] = GetStatus();

            return PartialView("PG", ViewData["StatusLookup"]);
        }

        public List<PODataStatus> GetStatus()
        {
            List<PODataStatus> listStatus = new List<PODataStatus>();
            try
            {
                listStatus = db.Fetch<PODataStatus>("DLV_getPOStatus2", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listStatus;
        }


        //Perform Create PO--------------
        public ContentResult performCreate(string PR_NO, string PROD_MONTH)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = "Sucess| ";
            try
            {

                    db.Execute("DLV_PO_performCreatePO", new object[] { PR_NO, AuthorizedUser.Username, PROD_MONTH });
                    result = "Sucess| Succes to Create Purchase Order";
                
            }
            catch (Exception ex)
            {
                result = "Error|" + ex.Message;
            }

            db.Close();
            return Content(result);
        }


        #region checkProgressCreatePO
        public ContentResult checkProgressCreatePO()
        {
            int isError = 0;
            //int msg = 0;
            //string status = "";
            string message = "";

            try
            {
                isError = 0;
                var QLog = db.SingleOrDefault<CheckProgress>("DLV_CheckMsgCreatePO", new object[] { });

                if (QLog.messageLog == "PO Creation process has been started")
                {
                    message = "PO Creation process has been started";
                }
                else if ((QLog.messageLog == "PO creation has been finished") ||
                   (QLog.messageLog == "Email notification failed sent to all PIC") ||
                   (QLog.messageLog == "Prepare To Send Mail") ||
                   (QLog.messageLog == "send e-mail has been finished."))
                {
                    message = "PO creation has been finished";
                }
                else if (QLog.messageLog == "Create PO failed with current PR, PR not in released status or PO was created")
                {
                    message = "Create PO failed with current PR, PR not in released status or PO was created";
                }
                else if (QLog.messageLog == "Logistic Partner Code not found in Trucking Scheme")
                {
                    message = "Logistic Partner Code not found in Trucking Scheme";
                }
                else if (QLog.messageLog == "Price Not Found")
                {
                    message = "Price Not Found";
                }
                else if (QLog.messageLog == "PO cannot create with this production month")
                {
                    message = "PO cannot create with this production month";
                }
                else if (QLog.messageLog == "Several PO successfully created,but several PR failed to create PO.")
                {
                    message = "Several PO successfully created,but several PR failed to create PO.";
                }
                else if (QLog.messageLog == "You are not authorized to perform this action.")
                {
                    message = "You are not authorized to perform this action.";
                }
                else
                {
                    //message = QLog.messageLog;
                    message = "Verifying. Please wait...";
                }
                //message = QLog.messageLog;
                //}

            }
            catch (Exception exc)
            {
                isError = 1;
                message = "Checking process error.|End";
                //message = "Error";
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + message);
        }
        #endregion

    }
}
