﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;
using Portal.Models.Delivery.POApproval;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;

namespace Portal.Controllers.Delivery.D31203
{
    public class D31203Controller : BaseController
    {
        //
        // GET: /D31104/
        public POApprovalData modelExport;
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public D31203Controller()
            : base("D31203", "Delivery Purchase Order Approval")
        {

        }


        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            POApprovalModel mdl = new POApprovalModel();
            ViewData["StatusLookup"] = GetStatus();
            Model.AddModel(mdl);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("IndexHeader");
        }

        //GETTING MESSAGE BOX
        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }

        //-------------------------------------[ MAIN GRID ]--------------------------------------------------------
        public ActionResult PartialDatas()
        {
            string vPO_MONTH = String.IsNullOrEmpty(Request.Params["pPO_MONTH"]) ? "" : Request.Params["pPO_MONTH"];
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NO"]) ? "" : Request.Params["pPO_NO"];
            string vPO_DATE1 = String.IsNullOrEmpty(Request.Params["pPODate1"]) ? "" : Request.Params["pPODate1"];
            string vPO_DATE2 = String.IsNullOrEmpty(Request.Params["pPODate2"]) ? "" : Request.Params["pPODate2"];
            string vPO_STATUS = String.IsNullOrEmpty(Request.Params["pPO_STATUS"]) ? "" : Request.Params["pPO_STATUS"];
           

            POApprovalModel mdl = Model.GetModel<POApprovalModel>();
            mdl.POApprovalDatas = GetPOApproval(vPO_MONTH,vPO_NO, vPO_DATE1, vPO_DATE2, vPO_STATUS);
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        protected List<POApprovalData> GetPOApproval(string vPO_MONTH, string vPO_NO, string vPO_DATE1, string vPO_DATE2, string vPO_STATUS)
        {
            string statusparameter = string.Empty;
            if (!string.IsNullOrEmpty(vPO_STATUS))
            {
                string[] statuslist = vPO_STATUS.Split(';');
                foreach (string r2 in statuslist)
                {
                    statusparameter += "'" + r2 + "',";
                }
                statusparameter = statusparameter.Remove(statusparameter.Length - 1, 1);
            }

            List<POApprovalData> listOfRouteMaster = db.Fetch<POApprovalData>("DLV_GetPOApproval", new object[] { 
                vPO_MONTH,
                vPO_NO,
                vPO_DATE1,
                vPO_DATE2,
                statusparameter

            });
            return listOfRouteMaster;

        }

        public ActionResult monPopupPOHead()
        {
            return PartialView("monPopupPOHead");
        }

        public ActionResult monPopupPODetail()
        {
            return PartialView("monPopupPODetail");
        }

        //-------------------------------------[ POPUP PO ]---------------------------------------------------------
        public ActionResult PopupDetailPO()
        {
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NO_DET"]) ? "" : Request.Params["pPO_NO_DET"];
            TempData["GridName"] = "gridmonPopupPODetail";
            ViewData["PopupDetailPO"] = FillviewPODetail(vPO_NO);
            return PartialView("monPopupPODetail", ViewData["PopupDetailPO"]);
        }
        public List<PODetailData> FillviewPODetail(string vPO_NO)
        {
            List<PODetailData> listOfData = new List<PODetailData>();
            try
            {
                listOfData = db.Fetch<PODetailData>("DLV_getPODetail", new object[] { vPO_NO });
            }
            catch (Exception ex) { throw ex; }
            return listOfData;
        }



        //-----------------------------------------------------------------------------
        public ActionResult PopupDetailPOHeader()
        {
            List<PODetailData> HeaderData = new List<PODetailData>();

            try
            {
                string vPO_NO = String.IsNullOrEmpty(Request.Params["PO_NO"]) ? "" : Request.Params["PO_NO"];
                HeaderData = db.Query<PODetailData>("DLV_getPODetail", new object[] { vPO_NO }).ToList();
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return Json(HeaderData);
        }

        #region Export To Excel
        public ActionResult exportToExcel(string PO_MONTH, string PO_NO, string PO_DATE1, string PO_DATE2, string PO_STATUS)
        {
            string ePO_MONTH = String.IsNullOrEmpty(Request.Params["PO_MONTH"]) ? "" : Request.Params["PO_MONTH"];
            string ePO_NO = String.IsNullOrEmpty(Request.Params["PO_NO"]) ? "" : Request.Params["PO_NO"];
            string ePO_DATE1 = String.IsNullOrEmpty(Request.Params["PODATE1"]) ? "" : Request.Params["PODATE1"];
            string ePO_DATE2 = String.IsNullOrEmpty(Request.Params["PODATE2"]) ? "" : Request.Params["PODATE2"];
            string ePO_STATUS = String.IsNullOrEmpty(Request.Params["PO_STATUS"]) ? "" : Request.Params["PO_STATUS"];

            string successStatus = "";

            POApprovalModel mdl = Model.GetModel<POApprovalModel>();
            mdl.POApprovalDatas = GetPOApproval(ePO_MONTH, ePO_NO, ePO_DATE1, ePO_DATE2, PO_STATUS);


            var mc = mdl.POApprovalDatas.ToList<POApprovalData>();
            int? totals = mc.Count;

            if (totals == 0)
            {
                successStatus = "False";
                var excelLink = "Data not found!";
                return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                using (ExcelPackage p = new ExcelPackage())
                {

                    p.Workbook.Worksheets.Add("Delivery Purchase Order");
                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                    ws.Name = "Delivery Purchasing Order";
                    ws.Cells.Style.Font.Size = 11;
                    ws.Cells.Style.Font.Name = "Calibri";
                    //header
                    string excelLink = "";
                    int columncount = 18;
                    //string successStatus = "";
                    ws.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA"; // Heading Name
                    ws.Cells[1, 1, 1, columncount].Merge = true; //Merge columns start and end range
                    ws.Cells[1, 1, 1, columncount].Style.Font.Bold = true; //Font should be bold
                    ws.Cells[1, 1, 1, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                    ws.Cells[2, 1].Value = "Delivery Purchase Order Approval"; // Heading Name
                    ws.Cells[2, 1, 2, columncount].Merge = true; //Merge columns start and end range
                    ws.Cells[2, 1, 2, columncount].Style.Font.Bold = true; //Font should be bold
                    ws.Cells[2, 1, 2, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                    //set header kolom
                    #region kolom


                    ws.Cells.AutoFitColumns(20, 40);
                    ws.Cells[4, 1].Value = "Production Month";
                    ws.Cells[4, 1].Style.Font.Bold = true;
                    ws.Cells[4, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 1, 5, 1].Merge = true;
                    ws.Cells[4, 1, 5, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                    ws.Cells[4, 2].Value = "PO Number";
                    ws.Cells[4, 2].Style.Font.Bold = true;
                    ws.Cells[4, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 2, 5, 2].Merge = true;
                    ws.Cells[4, 2, 5, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 3].Value = "Status";
                    ws.Cells[4, 3].Style.Font.Bold = true;
                    ws.Cells[4, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 3, 4, 5].Merge = true;
                    ws.Cells[4, 3, 4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[5, 3].Value = "SH";
                    ws.Cells[5, 3].Style.Font.Bold = true;
                    ws.Cells[5, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 4].Value = "DpH";
                    ws.Cells[5, 4].Style.Font.Bold = true;
                    ws.Cells[5, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 5].Value = "DH";
                    ws.Cells[5, 5].Style.Font.Bold = true;
                    ws.Cells[5, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[4, 6].Value = "Current PIC";
                    ws.Cells[4, 6].Style.Font.Bold = true;
                    ws.Cells[4, 6].AutoFitColumns(20, 50);
                    ws.Cells[4, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 6, 5, 6].Merge = true;
                    ws.Cells[4, 6, 5, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 7].Value = "PO Date";
                    ws.Cells[4, 7].Style.Font.Bold = true;
                    ws.Cells[4, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 7, 5, 7].Merge = true;
                    ws.Cells[4, 7, 5, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 8].Value = "Trucking";
                    ws.Cells[4, 8].AutoFitColumns();
                    ws.Cells[4, 8].Style.Font.Bold = true;
                    ws.Cells[4, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 8, 4, 9].Merge = true;
                    ws.Cells[4, 8, 4, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[5, 8].Value = "Code";
                    ws.Cells[5, 8].AutoFitColumns();
                    ws.Cells[5, 8].Style.Font.Bold = true;
                    ws.Cells[5, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 9].Value = "Name";
                    ws.Cells[5, 9].AutoFitColumns(30, 150);
                    ws.Cells[5, 9].Style.Font.Bold = true;
                    ws.Cells[5, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[4, 10].Value = "Release By";
                    ws.Cells[4, 10].Style.Font.Bold = true;
                    ws.Cells[4, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 10, 4, 12].Merge = true;
                    ws.Cells[4, 10, 4, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[5, 10].Value = "SH";
                    ws.Cells[5, 10].Style.Font.Bold = true;
                    ws.Cells[5, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 11].Value = "DpH";
                    ws.Cells[5, 11].Style.Font.Bold = true;
                    ws.Cells[5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 12].Value = "DH";
                    ws.Cells[5, 12].Style.Font.Bold = true;
                    ws.Cells[5, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 12].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                    ws.Cells[4, 13].Value = "Release Date";
                    //ws.Cells[4, 13].AutoFitColumns();
                    ws.Cells[4, 13].Style.Font.Bold = true;
                    ws.Cells[4, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 13].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 13, 4, 15].Merge = true;
                    ws.Cells[4, 13, 4, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[5, 13].Value = "SH";
                    ws.Cells[5, 13].AutoFitColumns(20, 70);
                    ws.Cells[5, 13].Style.Font.Bold = true;
                    ws.Cells[5, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 13].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 14].Value = "DpH";
                    ws.Cells[5, 14].AutoFitColumns(20, 70);
                    ws.Cells[5, 14].Style.Font.Bold = true;
                    ws.Cells[5, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 14].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 15].Value = "DH";
                    ws.Cells[5, 15].AutoFitColumns(20, 70);
                    ws.Cells[5, 15].Style.Font.Bold = true;
                    ws.Cells[5, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 15].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 15].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                    ws.Cells[4, 16].Value = "PO Rejection";
                    ws.Cells[4, 16].Style.Font.Bold = true;
                    ws.Cells[4, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 16].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 16, 4, 18].Merge = true;
                    ws.Cells[4, 16, 4, 18].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[5, 16].Value = "By";
                    ws.Cells[5, 16].AutoFitColumns(20, 100);
                    ws.Cells[5, 16].Style.Font.Bold = true;
                    ws.Cells[5, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 16].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 17].Value = "Position";
                    ws.Cells[5, 17].AutoFitColumns(20, 40);
                    ws.Cells[5, 17].Style.Font.Bold = true;
                    ws.Cells[5, 17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 17].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 17].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 18].Value = "Date";
                    ws.Cells[5, 18].AutoFitColumns(20, 70);
                    ws.Cells[5, 18].Style.Font.Bold = true;
                    ws.Cells[5, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 18].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 18].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 18].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    #endregion

                    #region detailnya

                    List<POApprovalData> m = mdl.POApprovalDatas.ToList<POApprovalData>();

                    int i = 0;
                    foreach (POApprovalData x in m)
                    {

                        string dph = m[i].DPH_RELEASE.ToString();
                        string dh = m[i].DH_RELEASE.ToString();
                        string sh = m[i].SH_RELEASE.ToString();

                        string reject_dt = m[i].REJECTED_DT.ToString();

                        var dphVal = "";
                        var dhVal = "";
                        var shVal = "";

                        var rejectVal = "";

                        if (dph == "1/1/0001 12:00:00 AM")
                        {
                            dphVal = "";
                        }
                        else
                        {
                            dphVal = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].DPH_RELEASE);
                        }
                        if (dh == "1/1/0001 12:00:00 AM")
                        {
                            dhVal = "";
                        }
                        else
                        {
                            dhVal = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].DH_RELEASE);
                        }
                        if (sh == "1/1/0001 12:00:00 AM")
                        {
                            shVal = "";
                        }
                        else
                        {
                            shVal = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].SH_RELEASE);
                        }

                        if (reject_dt == "1/1/0001 12:00:00 AM")
                        {
                            rejectVal = "";
                        }
                        else
                        {
                            rejectVal = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].REJECTED_DT);
                        }


                        ws.Cells[i + 6, 1].Value = m[i].PROD_MONTH;
                        ws.Cells[i + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 2].Value = m[i].PO_NO;
                        ws.Cells[i + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        //ws.Cells[i + 6, 3].Value = (char)251;
                        //ws.Cells[i + 6, 3].Value = m[i].SH_APPROVED_DT;
                        //ws.Cells[i + 6, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        //ws.Cells[i + 6, 4].Value = m[i].DPH_APPROVED_DT;
                        //ws.Cells[i + 6, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        //ws.Cells[i + 6, 5].Value = m[i].DH_APPROVED_DT;
                        //ws.Cells[i + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                        //------------------------------------------------------------------------------
                        if (m[i].SH_APPROVED_DT == "gray_1")
                        {
                            ws.Cells[i + 6, 3].Value = "Skip";
                            ws.Cells[i + 6, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[i + 6, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        }
                        else
                            if (m[i].SH_APPROVED_DT == "yellow_1")
                            {
                                ws.Cells[i + 6, 3].Value = "Pending";
                                ws.Cells[i + 6, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 3].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                            }

                            else if (m[i].SH_APPROVED_DT == "green_1")
                            {
                                ws.Cells[i + 6, 3].Value = "Approved";
                                ws.Cells[i + 6, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            }
                            else
                            {
                                ws.Cells[i + 6, 3].Value = "Rejected";
                                ws.Cells[i + 6, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 3].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            }

                        ws.Cells[i + 6, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        //------------------------------------------------------------------------------

                        if (m[i].DPH_APPROVED_DT == "gray_1")
                        {

                            ws.Cells[i + 6, 4].Value = "Skip";
                            ws.Cells[i + 6, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[i + 6, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        }
                        else
                            if (m[i].DPH_APPROVED_DT == "yellow_1")
                            {
                                ws.Cells[i + 6, 4].Value = "Pending";
                                ws.Cells[i + 6, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 4].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                            }
                            else if (m[i].DPH_APPROVED_DT == "green_1")
                            {
                                ws.Cells[i + 6, 4].Value = "Approved";
                                ws.Cells[i + 6, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            }
                            else
                            {
                                ws.Cells[i + 6, 4].Value = "Rejected";
                                ws.Cells[i + 6, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 4].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            }

                        ws.Cells[i + 6, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        //------------------------------------------------------------------------------

                        if (m[i].DH_APPROVED_DT == "gray_1")
                        {
                            ws.Cells[i + 6, 5].Value = "Skip";
                            ws.Cells[i + 6, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[i + 6, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        }
                        else
                            if (m[i].DH_APPROVED_DT == "yellow_1")
                            {
                                ws.Cells[i + 6, 5].Value = "Pending";
                                ws.Cells[i + 6, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 5].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                            }
                            else if (m[i].DH_APPROVED_DT == "green_1")
                            {
                                ws.Cells[i + 6, 5].Value = "Approved";
                                ws.Cells[i + 6, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            }
                            else
                            {
                                ws.Cells[i + 6, 5].Value = "Rejected";
                                ws.Cells[i + 6, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[i + 6, 5].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            }
                        ws.Cells[i + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        //------------------------------------------------------------------------------

                        ws.Cells[i + 6, 6].Value = m[i].CREATED_BY;
                        ws.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ws.Cells[i + 6, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 7].Value = string.Format("{0:dd.MM.yyyy}", m[i].CREATED_DT);
                        //ws.Cells[i + 6, 7].AutoFitColumns();
                        ws.Cells[i + 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 8].Value = m[i].LP_CD;
                        ws.Cells[i + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 9].Value = m[i].LOG_PARTNER_NAME;
                        //ws.Cells[i + 6, 9].AutoFitColumns(20,100);
                        ws.Cells[i + 6, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                        ws.Cells[i + 6, 10].Value = m[i].SH_RELEASE_BY;
                        ws.Cells[i + 6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ws.Cells[i + 6, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 11].Value = m[i].DPH_RELEASE_BY;
                        ws.Cells[i + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ws.Cells[i + 6, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 12].Value = m[i].DH_RELEASE_BY;
                        ws.Cells[i + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ws.Cells[i + 6, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                        ws.Cells[i + 6, 13].Value = shVal;
                        ws.Cells[i + 6, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 14].Value = dphVal;
                        ws.Cells[i + 6, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 15].Value = dhVal;
                        ws.Cells[i + 6, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);



                        ws.Cells[i + 6, 16].Value = m[i].REJECTED_BY;
                        ws.Cells[i + 6, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ws.Cells[i + 6, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 17].Value = m[i].REJECTED_POSITION;
                        ws.Cells[i + 6, 17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 18].Value = rejectVal;
                        ws.Cells[i + 6, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 18].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        i++;
                    }

                    #endregion

                    try
                    {


                        Byte[] bin = p.GetAsByteArray();
                        string fn = "DeliveryPurchaseOrderApproval" + "-" + AuthorizedUser.Username + ".xlsx";
                        string DownloadDirectory = "Content/Download/DeliveryPurchaseOrderApproval/";
                        string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
                        var pathfile = Path.Combine(downdir, fn);

                        excelLink = "Content/Download/DeliveryPurchaseOrderApproval/" + fn + "";
                        successStatus = "True";
                        System.IO.File.WriteAllBytes(pathfile, bin);


                    }
                    catch (Exception err)
                    {
                        //write log
                        successStatus = "False";
                    }
                    return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);


                }
            }
        }

        private void DownloadFile(string fname)
        {
            bool forceDownload = true;
            string path = fname;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            string type = "";
            // set known types based on file extension  
            if (ext != null)
            {
                switch (ext.ToLower())
                {

                    case ".xlsx":
                    case ".xls":
                        type = "Application/msexcel";
                        break;
                }
            }
            if (forceDownload)
            {
                Response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            if (type != "")
                Response.ContentType = type;
            Response.WriteFile(path);
            Response.End();
        }
        #endregion

        public ContentResult performApproval(string PO_NO)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = "Sucess| ";
            try
            {
                db.Execute("DLV_PO_performApproval", new object[] {
                    PO_NO, AuthorizedUser.Username
                });

                result = "Sucess| Succes to approve PO.";
            }
            catch (Exception ex)
            {
                result = "Error|" + ex.Message;
            }

            db.Close();
            return Content(result);
        }

        public ContentResult performReject(string PO_NO)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string result = "Sucess| ";

            try
            {
                db.Execute("DLV_PO_performReject", new object[] {
                    PO_NO, AuthorizedUser.Username
                });

                result = "Sucess| Succes to reject PO.";
            }
            catch (Exception ex)
            {
                result = "Error|" + ex.Message;
            }

            db.Close();
            return Content(result);
        }


        //GETTING Status FOR LOOKUP

        public ActionResult StatusLookup()
        {

            TempData["GridName"] = "PO_STATUS";
            ViewData["StatusLookup"] = GetStatus();

            return PartialView("PG", ViewData["StatusLookup"]);
        }

        public List<PODataStatus> GetStatus()
        {
            List<PODataStatus> listStatus = new List<PODataStatus>();
            try
            {
                listStatus = db.Fetch<PODataStatus>("DLV_getPOStatus", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listStatus;
        }
    }

}
