﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;
using ServiceStack.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Task;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using Toyota.Common.Web.Log;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ServiceStack.Text;
using Toyota.Common.Web.Compression;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using System.Reflection;
using System.Drawing;
using Portal.Models.Globals;
using Portal.Models.Delivery.Invoice;
using Portal.Models.InvoiceAndPaymentInquiry;
using Portal.Models.InvoiceCreation;
using Portal.Models.InvoiceTaxCreationByMonth;


namespace Portal.Controllers.Delivery.Invoice
{
    public class D31401Controller : BaseController
    {
        //
        // GET: /D31401/

        //public ActionResult Index()
        //{
        //    return View();
        //}



        private IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

        public D31401Controller()
            : base("Delivery Invoice Creation Screen")
        {

        }

        protected override void StartUp()
        {
            //Add By Arka.Taufik 2022-03-22
            ViewData["TaxGridLookup"] = GetTaxListStart();

            ViewData["InvoiceTax"] = GetInvoiceTaxData();
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryInvoiceCreationModel mdl = new DeliveryInvoiceCreationModel();
            DeliveryInvoiceCreationDownloadModel mdl2 = new DeliveryInvoiceCreationDownloadModel();
            DeliveryInvoiceCreationDetailDownloadModel mdl3 = new DeliveryInvoiceCreationDetailDownloadModel();
            //ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            Model.AddModel(mdl);
            Model.AddModel(mdl2);
            Model.AddModel(mdl3);
        }

        public int GetInvoiceDateRange()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int range = 0;
            try
            {
                string function_id = "31401";
                range = db.ExecuteScalar<int>("GetInvoiceDateRange", new object[] { function_id });
            }
            catch (Exception e)
            {
                range = 0;
                throw (e);
            }
            return range;
        }

        //HEADER
        public ActionResult PartialHeader()
        {

            #region Required model for [lp lookup]

            List<DeliveryInvoiceDataLogPartner> trucking =
                Model.GetModel<User>()
                    .FilteringArea<DeliveryInvoiceDataLogPartner>(GetAllLP(), "LP_CD", "D31401", "LogisticPartnerOption");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            Model.AddModel(_LPModel);

            #endregion

            return PartialView("IndexHeader", Model);
        }

        //GRID DATA
        public ActionResult PartialListData()
        {
            string DOC_DATE_FROM = String.IsNullOrEmpty(Request.Params["DOC_DATE_FROM"])
                ? ""
                : Request.Params["DOC_DATE_FROM"];
            string DOC_DATE_TO = String.IsNullOrEmpty(Request.Params["DOC_DATE_TO"])
                ? ""
                : Request.Params["DOC_DATE_TO"];
            string LP_CD = String.IsNullOrEmpty(Request.Params["LP_CD"]) ? "" : Request.Params["LP_CD"];
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"])
                ? ""
                : Request.Params["DELIVERY_NO"];
            string ShowUploaded = String.IsNullOrEmpty(Request.Params["ShowUploaded"])
                ? ""
                : Request.Params["ShowUploaded"];

            DeliveryInvoiceCreationModel mdl = Model.GetModel<DeliveryInvoiceCreationModel>();
            if (DOC_DATE_FROM != "" && DOC_DATE_TO != "")
            {
                mdl.DeliveryInvoiceCreationListData = GetDLVInvoiceCreationData(DOC_DATE_FROM, DOC_DATE_TO, DELIVERY_NO,
                    LP_CD, ShowUploaded);
            }
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        protected List<DeliveryInvoiceCreationData> GetDLVInvoiceCreationData(string DOC_DATE_FROM, string DOC_DATE_TO,
            string DELIVERY_NO, string LP_CD, string ShowUploaded)
        {

            string docDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(DOC_DATE_FROM))
            {
                string d = DOC_DATE_FROM.Substring(0, 2);
                string m = DOC_DATE_FROM.Substring(3, 2);
                string y = DOC_DATE_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                docDateFromParameter += "'" + tgl + "'";
            }

            string docDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(DOC_DATE_TO))
            {
                string d = DOC_DATE_TO.Substring(0, 2);
                string m = DOC_DATE_TO.Substring(3, 2);
                string y = DOC_DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                docDateToParameter += "'" + tgl + "'";
            }


            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            List<DeliveryInvoiceCreationData> listOfDLV_INV_creation =
                db.Fetch<DeliveryInvoiceCreationData>("DLV_INV_getCreationGrid", new object[]
                {
                    docDateFromParameter, docDateToParameter, DELIVERY_NO, logisticCDParameter, ShowUploaded
                });
            return listOfDLV_INV_creation;

        }

        public ActionResult PreviewInvoice()
        {
            //string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            string PO_NO = String.IsNullOrEmpty(Request.Params["PO_NO"]) ? "" : Request.Params["PO_NO"];
            string LP_INV_NO = String.IsNullOrEmpty(Request.Params["LP_INV_NO"]) ? "" : Request.Params["LP_INV_NO"];
            DeliveryInvoiceCreationModel mdl = Model.GetModel<DeliveryInvoiceCreationModel>();

            mdl.DeliveryInvoiceCreationListData = GetPreviewInvoice(PO_NO, LP_INV_NO);
            Model.AddModel(mdl);

            /*var invoiceTaxModel = GetTaxInvoiceModel(PO_NO, LP_INV_NO);
            Model.AddModel(invoiceTaxModel);*/

            return PartialView("DetailPreviewInvoice", Model);
        }

        protected List<DeliveryInvoiceCreationData> GetPreviewInvoice(string PO_NO, string LP_INV_NO)
        {
            List<DeliveryInvoiceCreationData> listOfDLV_INV_creation =
                db.Fetch<DeliveryInvoiceCreationData>("DLV_INV_PrevInvoice", new object[]
                {
                    PO_NO, LP_INV_NO
                });
            return listOfDLV_INV_creation;
        }

        // ADDED BY fid.intan 2015-08-06
        // Get tolerance value for invoice creation from TB_M_SYSTEM
        public decimal GetToleranceVal()
        {
            decimal toleranceVal = 0;
            toleranceVal = db.SingleOrDefault<decimal>("GetToleranceValue");
            return toleranceVal;
        }

        //-------------------------------------[ POPUP Detail ]---------------------------------------------------------

        public ActionResult PartialListDetailData()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            DeliveryInvoiceCreationModel mdl = Model.GetModel<DeliveryInvoiceCreationModel>();
            mdl.DeliveryInvoiceCreationListData = getDetailData(DELIVERY_NO);
            Model.AddModel(mdl);
            return PartialView("GridDetail", Model);
        }


        public List<DeliveryInvoiceCreationData> getDetailData(string DELIVERY_NO)
        {
            string deliveryNoParameter = string.Empty;
            if (!string.IsNullOrEmpty(DELIVERY_NO))
            {
                string[] deliveryNoList = DELIVERY_NO.Split(';');

                foreach (string r in deliveryNoList)
                {
                    deliveryNoParameter += "'" + r + "',";
                }
                deliveryNoParameter = deliveryNoParameter.Remove(deliveryNoParameter.Length - 1, 1);
            }

            List<DeliveryInvoiceCreationData> listOfDetail =
                db.Fetch<DeliveryInvoiceCreationData>("DLV_INV_getCreationDetailGrid", new object[]
                {
                    deliveryNoParameter
                });
            return listOfDetail;
        }



        #region LookUp

        //GETTING Logistic partner FOR LOOKUP

        public ActionResult LogisticPartnerLookup()
        {

            TempData["GridName"] = "LogisticPartnerOption";
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            return PartialView("PG", ViewData["LogisticPartnerLookup"]);
        }

        public List<DeliveryInvoiceDataLogPartner> GetLogisticPartner()
        {
            List<DeliveryInvoiceDataLogPartner> listLogisticPartner = new List<DeliveryInvoiceDataLogPartner>();
            try
            {
                listLogisticPartner = db.Fetch<DeliveryInvoiceDataLogPartner>("DLV_getLP", new object[] { });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            db.Close();
            return listLogisticPartner;
        }

        #endregion


        //GETTING MESSAGE BOX
        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }


        public JsonResult checkDataInvoice(string DOC_DATE_FROM, string DOC_DATE_TO, string DELIVERY_NO, string LP_CD,
            string DELIVERY_NO2, string PO_NO, string showUploaded)
        {

            List<DeliveryInvoiceCreationDownloadData> res = new List<DeliveryInvoiceCreationDownloadData>();
            res = GetDLVInvoiceCreationDataDownload(DOC_DATE_FROM, DOC_DATE_TO, DELIVERY_NO, LP_CD, DELIVERY_NO2, PO_NO,
                showUploaded);

            int? total = res.Count;

            return Json(new { Data = total }, JsonRequestBehavior.AllowGet);
        }

        public void WriteCSV<T>(IEnumerable<T> items, Stream stream)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(string.Join(";", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join(";", props.Select(p => p.GetValue(item, null))));
                }
            }
        }

        public FileContentResult DownloadListCSVNew(string DOC_DATE_FROM, string DOC_DATE_TO, string DELIVERY_NO,
            string LP_CD, string DELIVERY_NO2, string PO_NO, string showUploaded)
        {
            Stream output = new MemoryStream();
            string fileName = "LIV_Invoice_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + AuthorizedUser.Username +
                              ".csv";
            List<DeliveryInvoiceCreationDownloadData> listOfDLV_INV_creation =
                new List<DeliveryInvoiceCreationDownloadData>();
            byte[] documentBytes;

            try
            {
                listOfDLV_INV_creation = GetDLVInvoiceCreationDataDownload(DOC_DATE_FROM, DOC_DATE_TO, DELIVERY_NO,
                    LP_CD, DELIVERY_NO2, PO_NO, showUploaded);

                using (MemoryStream buffer = new MemoryStream())
                {
                    WriteCSV(listOfDLV_INV_creation, buffer);
                    documentBytes = buffer.GetBuffer();
                }

                return File(documentBytes, "text/csv", fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected List<DeliveryInvoiceCreationDownloadData> GetDLVInvoiceCreationDataDownload(string DOC_DATE_FROM,
            string DOC_DATE_TO, string DELIVERY_NO, string LP_CD, string DELIVERY_NO2, string PO_NO, string showUploaded)
        {

            string docDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(DOC_DATE_FROM))
            {
                string d = DOC_DATE_FROM.Substring(0, 2);
                string m = DOC_DATE_FROM.Substring(3, 2);
                string y = DOC_DATE_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                docDateFromParameter += "'" + tgl + "'";
            }

            string docDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(DOC_DATE_TO))
            {
                string d = DOC_DATE_TO.Substring(0, 2);
                string m = DOC_DATE_TO.Substring(3, 2);
                string y = DOC_DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                docDateToParameter += "'" + tgl + "'";
            }


            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            List<DeliveryInvoiceCreationDownloadData> listOfDLV_INV_creation =
                db.Fetch<DeliveryInvoiceCreationDownloadData>("DLV_INV_CreationBaseDownload", new object[]
                {
                    docDateFromParameter, docDateToParameter, DELIVERY_NO, logisticCDParameter, DELIVERY_NO2, PO_NO,
                    showUploaded
                });
            return listOfDLV_INV_creation;

        }

        public IEnumerable<string> ReadAllLines(string filename, Encoding encoding)
        {
            using (var reader = new StreamReader(filename, encoding))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }


        public const string UploadDirectory = "Content/Upload/Invoice/";
        public const string TemplateFName = "TemplateInvoice";

        public class UploadControlHelper
        {

            public const string UploadDirectory = "Content/Upload/Invoice/";
            public const string TemplateFName = "TemplateInvoice";

            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };

        }

        public ActionResult CallbacksUpload()
        {
            //userLogin = AuthorizedUser.Username;
            UploadControlExtension.GetUploadedFiles("ucCallbacks", UploadControlHelper.ValidationSettings,
                ucCallbacks_FileUploadComplete);

            return null;
        }

        public void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {

            if (e.UploadedFile.IsValid)
            {

                String processID = "";

                processID = db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[]
                    {
                        "Upload invoice started.",
                        //"Print Certificate ID InvoicePayment_" + invoice_no + ".pdf is started", 
                        AuthorizedUser.Username,
                        "DeliveryInvoiceCreation.Upload",
                        "",
                        "MPCS00002INF", "INF", "3", "31401", // function
                        "0" // sts
                    });

                string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                var pathfile = Path.Combine(updir,
                    TemplateFName + "-" + processID + Path.GetExtension(e.UploadedFile.FileName));
                e.UploadedFile.SaveAs(pathfile);

                processID = db.ExecuteScalar<string>(
                    "GenerateProcessId",
                    new object[]
                    {
                        "Upload invoice Finished.",
                        //"Print Certificate ID InvoicePayment_" + invoice_no + ".pdf is started", 
                        AuthorizedUser.Username,
                        "DeliveryInvoiceCreation.Upload",
                        "",
                        "MPCS00003INF", "INF", "3", "31401", // function
                        "0" // sts
                    });

                ReadCSV(pathfile);
            }

        }

        public void ReadCSV(string filename)
        {

            string pid;



            pid = regQueue();
            List<string> listA = new List<string>();
            List<string> listB = new List<string>();

            string LP = "";

            StreamReader csvreader = new StreamReader(filename);

            while (!csvreader.EndOfStream)
            {
                string dt = "";
                string m1 = "";
                string m2 = "";
                var line = csvreader.ReadLine();
                line = line.Trim();
                line = line.Replace("\0", "");
                if (line != "                               " || line != "")
                {
                    //string[] values = line.Split(',');
                    string x = line;
                    string[] s = line.Split(';');

                    if (x != "")
                    {
                        dt = s[7];
                        m1 = s[10];
                        m2 = s[12];

                        //string[] ma = m1.Split('.');
                        //string[] mb = m2.Split('.');

                        //m1 = ma[0];
                        //m2 = mb[0];

                        m1 = m1.Replace(",", "");
                        m2 = m2.Replace(",", "");


                        if (dt == "DOC_DT" || dt == "")
                        {

                        }
                        else
                        {
                            string d = dt.Substring(0, 2);
                            string m = dt.Substring(3, 2);
                            string y = dt.Substring(6, 4);
                            dt = y + "-" + m + "-" + d;
                            listA.Add(s[0] + "," + s[1] + "," + s[2] + "," + s[3] + "," + s[4] + "," + s[5] + "," + s[6] +
                                      "," + dt + "," + s[8] + "," + s[9] + "," + m1 + "," + s[11] + "," + m2 + "," + pid);
                            //listA.Add(s[0] + "," + s[1] + "," + s[2] + "," + s[3] + "," + s[4] + "," + s[5] + "," + s[6] + "," + dt + "," + s[8] + "," + s[9] + "," + s[10] + "," + s[11] + "," + s[12] + "," + pid);

                        }
                    }
                }
            }

            //int error;

            for (int i = 0; i < listA.Count(); i++)
            {
                string[] sub = listA.ElementAt(i).Split(',');
                string LP_CD = sub[1];
                string PO_NO = sub[2];
                int PO_ITEM_NO = Convert.ToInt32(sub[3]);
                string ROUTE_CD = sub[4];
                string REFF_NO = sub[5];
                string COMP_PRICE_CD = sub[6];
                DateTime DOC_DT = Convert.ToDateTime(sub[7]);
                int TRANSACTION_QTY = Convert.ToInt32(sub[8]);
                string LP_INV_NO = sub[9];
                string PO_DETAIL_PRICE = string.Format("{0:0.##}", sub[10]);
                string TRANSACTION_CURR = sub[11];
                Decimal AMOUNT = Convert.ToDecimal(sub[12]);
                Int64 PROCESS_ID = Convert.ToInt64(sub[13]);

                db.ExecuteScalar<string>("DLV_INV_Create_TB_T_DLV_INV_UPLOAD",
                    new object[]
                    {
                        LP_CD, PO_NO, PO_ITEM_NO, ROUTE_CD, REFF_NO, COMP_PRICE_CD, DOC_DT, TRANSACTION_QTY, LP_INV_NO,
                        PO_DETAIL_PRICE, TRANSACTION_CURR, AMOUNT, PROCESS_ID, AuthorizedUser.Username
                    });
                LP = LP_CD;

            }

            // db.Execute("DLV_INV_RegTask", new object[] { AuthorizedUser.Username, pid });
            db.Execute("DLV_INV_CreateSyncGRIR", new object[] { LP, AuthorizedUser.Username, pid });



        }

        public JsonResult getProcessStatus()
        {
            string name = AuthorizedUser.Username;
            name = name.Substring(0, 3);
            List<DeliveryInvoiceUploadProgressData> progress =
                db.Fetch<DeliveryInvoiceUploadProgressData>("DLV_INV_UploadProcessStatus", new object[] { name });
            return Json(new { Data = progress }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getLP()
        {
            string name = AuthorizedUser.Username;
            name = name.Substring(0, 3);
            List<DeliveryInvoiceUploadProgressData> process =
                db.Fetch<DeliveryInvoiceUploadProgressData>("DLV_INV_LP_EXIST", new object[] { name });
            //return Json(new { Data = process[0].PROCESS_ID }, JsonRequestBehavior.AllowGet);
            return Json(new { Data = process }, JsonRequestBehavior.AllowGet);
        }




        public string regQueue()
        {
            List<DeliveryInvoiceUploadProgressData> process =
                db.Fetch<DeliveryInvoiceUploadProgressData>("DLV_INV_RegQUEUE", new object[] { AuthorizedUser.Username });
            //return Json(new { Data = process[0].PROCESS_ID }, JsonRequestBehavior.AllowGet);
            return process[0].PROCESS_ID;
        }

        public JsonResult getLastPid()
        {

            List<DeliveryInvoiceUploadProgressData> progress =
                db.Fetch<DeliveryInvoiceUploadProgressData>("DLV_INV_UploadPid", new object[] { });
            return Json(new { Data = progress }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getProgress()
        {
            string PID = String.IsNullOrEmpty(Request.Params["PID"]) ? "" : Request.Params["PID"];
            List<DeliveryInvoiceUploadProgressData> progress =
                db.Fetch<DeliveryInvoiceUploadProgressData>("DLV_INV_UploadProgress", new object[] { PID });
            return Json(new { Data = progress }, JsonRequestBehavior.AllowGet);
        }



        public JsonResult getTotalAmount()
        {
            string PO_NO = String.IsNullOrEmpty(Request.Params["PO_NO"]) ? "" : Request.Params["PO_NO"];
            string DeliveryNo = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            string LP_INV_NO = String.IsNullOrEmpty(Request.Params["LP_INV_NO"]) ? "" : Request.Params["LP_INV_NO"];
            List<DeliveryInvoiceCreationInvoiceAmountData> total =
                db.Fetch<DeliveryInvoiceCreationInvoiceAmountData>("DLV_INV_creationTotalAmount",
                    new object[] { PO_NO, DeliveryNo, LP_INV_NO });
            return Json(new { Data = total }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveInvoice()
        {
            string PO_NO = String.IsNullOrEmpty(Request.Params["PO_NO"]) ? "" : Request.Params["PO_NO"];
            string LP_INV_NO = String.IsNullOrEmpty(Request.Params["LP_INV_NO"]) ? "" : Request.Params["LP_INV_NO"];
            string INV_NO = String.IsNullOrEmpty(Request.Params["INV_NO"]) ? "" : Request.Params["INV_NO"];
            string INV_DT = String.IsNullOrEmpty(Request.Params["INV_DT"]) ? "" : Request.Params["INV_DT"];
            string INV_TAX_NO = String.IsNullOrEmpty(Request.Params["INV_TAX_NO"]) ? "" : Request.Params["INV_TAX_NO"];
            string INV_TAX_DT = String.IsNullOrEmpty(Request.Params["INV_TAX_DT"]) ? "" : Request.Params["INV_TAX_DT"];

            //Add By Arka.Taufik 2022-03-16
            string INV_TAX_CD = String.IsNullOrEmpty(Request.Params["INV_TAX_CD"]) ? "" : Request.Params["INV_TAX_CD"];
            string INV_TAX_RATE = String.IsNullOrEmpty(Request.Params["INV_TAX_RATE"]) ? "" : Request.Params["INV_TAX_RATE"];

            string DLV_NO = String.IsNullOrEmpty(Request.Params["DLV_NO"]) ? "" : Request.Params["DLV_NO"];

            string LP_INV_NO_GRID = String.IsNullOrEmpty(Request.Params["LP_INV_NO_GRID"])
                ? ""
                : Request.Params["LP_INV_NO_GRID"];

            string invDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(INV_DT))
            {
                string d = INV_DT.Substring(0, 2);
                string m = INV_DT.Substring(3, 2);
                string y = INV_DT.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                invDateParameter += tgl;
            }

            string taxDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(INV_TAX_DT))
            {
                string d = INV_TAX_DT.Substring(0, 2);
                string m = INV_TAX_DT.Substring(3, 2);
                string y = INV_TAX_DT.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                taxDateParameter += tgl;
            }



            string STATUS_CD = String.IsNullOrEmpty(Request.Params["STATUS_CD"]) ? "" : Request.Params["STATUS_CD"];
            //db.Execute("DLV_INV_creationSaveInvoice", new object[] { PO_NO, LP_INV_NO, INV_NO, invDateParameter, INV_TAX_CD, taxDateParameter, STATUS_CD, AuthorizedUser.Username, DLV_NO});

            /* commented fid.deny 2015-08-03, changed to below
             * List<DeliveryInvoiceCreationData> model =
                setSaveInvoice(PO_NO, LP_INV_NO, INV_NO, invDateParameter, INV_TAX_CD, taxDateParameter, STATUS_CD,
                    DLV_NO, LP_INV_NO_GRID).ToList();
            return Json(new {data = model}, JsonRequestBehavior.AllowGet); 
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
             */

            //check if there is a process from same LP running or not
            string username = "";
            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();

            if (user != null) { username = user.Username.Substring(0, 3); }

            List<DeliveryInvoiceUploadProgressData> progress = db.Fetch<DeliveryInvoiceUploadProgressData>("DLV_INV_checkLock", new object[] {username});

            if (progress[0].MESSAGE == "NO LOCK")
            {
                string pidd =
                setSaveInvoice(PO_NO, LP_INV_NO, INV_NO, invDateParameter, INV_TAX_NO, taxDateParameter, STATUS_CD, DLV_NO, LP_INV_NO_GRID, INV_TAX_CD, INV_TAX_RATE);

                progress = db.Fetch<DeliveryInvoiceUploadProgressData>("DLV_INV_UploadProgress", new object[] { pidd });
            }

            return Json(new { data = progress }, JsonRequestBehavior.AllowGet);

        }

        //protected List<DeliveryInvoiceCreationData> setSaveInvoice(string PO_NO, string LP_INV_NO, string INV_NO,
        protected string setSaveInvoice(string PO_NO, string LP_INV_NO, string INV_NO,
            string invDateParameter, string INV_TAX_NO, string taxDateParameter, string STATUS_CD, string DLV_NO,
            string LP_INV_NO_GRID, string INV_TAX_CD, string INV_TAX_RATE)
        {
            
            string l = db.ExecuteScalar<string>("DLV_INV_creationSaveInvoice",
                new object[]
                {
                    PO_NO, LP_INV_NO, INV_NO, invDateParameter, INV_TAX_NO, taxDateParameter, INV_TAX_CD, INV_TAX_RATE, STATUS_CD,
                    AuthorizedUser.Username, DLV_NO, LP_INV_NO_GRID
                });

            string pid = db.ExecuteScalar<string>("DLV_INV_UploadPid", new object[] { });

            db.Close();
            return pid;

            /*var l = db.Fetch<DeliveryInvoiceCreationData>("DLV_INV_creationSaveInvoice",
                new object[]
                {
                    PO_NO, LP_INV_NO, INV_NO, invDateParameter, INV_TAX_CD, taxDateParameter, STATUS_CD,
                    AuthorizedUser.Username, DLV_NO, LP_INV_NO_GRID
                });
            db.Close();
            return l.ToList();*/
        }

        protected List<DeliveryInvoiceCreationDetailDownloadData> GetInvoiceDetail(string DLV_NO, string LP_INV_NO)
        {
            List<DeliveryInvoiceCreationDetailDownloadData> inv =
                db.Fetch<DeliveryInvoiceCreationDetailDownloadData>("DLV_INV_getCreationDetailDownload", new object[]
                {
                    DLV_NO, LP_INV_NO
                });
            return inv;
        }


        public ActionResult exportToExcel()
        {
            string DLV_NO = String.IsNullOrEmpty(Request.Params["DLV_NO"]) ? "" : Request.Params["DLV_NO"];
            string LP_INV_NO = String.IsNullOrEmpty(Request.Params["LP_INV_NO"]) ? "" : Request.Params["LP_INV_NO"];

            DeliveryInvoiceCreationDetailDownloadModel mdl3 =
                Model.GetModel<DeliveryInvoiceCreationDetailDownloadModel>();
            mdl3.DeliveryInvoiceCreationDetailListData = GetInvoiceDetail(DLV_NO, LP_INV_NO);

            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Worksheets.Add("Invoice Creation Detail Data");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Name = "Invoice Creation";
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";
                //header
                string excelLink = "";
                int columncount = 5;
                string successStatus = "";
                ws.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA"; // Heading Name
                ws.Cells[1, 1, 1, columncount].Merge = true; //Merge columns start and end range
                ws.Cells[1, 1, 1, columncount].Style.Font.Bold = true; //Font should be bold
                ws.Cells[1, 1, 1, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Aligmnet is center

                ws.Cells[2, 1].Value = "Invoice Creation Detail Data"; // Heading Name
                ws.Cells[2, 1, 2, columncount].Merge = true; //Merge columns start and end range
                ws.Cells[2, 1, 2, columncount].Style.Font.Bold = true; //Font should be bold
                ws.Cells[2, 1, 2, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Aligmnet is center

                ws.Cells.AutoFitColumns(30, 130);

                ws.Cells[3, 1].Value = "Delivery No";
                ws.Cells[3, 1].Style.Font.Bold = true;
                ws.Cells[3, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[3, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[3, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[3, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[3, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[3, 1].AutoFitColumns(20, 100);

                ws.Cells[3, 2].Value = "Invoice No";
                ws.Cells[3, 2].Style.Font.Bold = true;
                ws.Cells[3, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[3, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[3, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[3, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[3, 2].AutoFitColumns(20, 100);

                ws.Cells[3, 3].Value = "Price Component";
                ws.Cells[3, 3].Style.Font.Bold = true;
                ws.Cells[3, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[3, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[3, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[3, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[3, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[3, 3].AutoFitColumns(30, 130);
                //ws.Cells[4, 1, 5, 1].Merge = true;

                ws.Cells[3, 4].Value = "Value";
                ws.Cells[3, 4].Style.Font.Bold = true;
                ws.Cells[3, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[3, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[3, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[3, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[3, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[3, 4].AutoFitColumns(20, 100);



                //ws.Cells[4, 2, 5, 2].Merge = true;

                ws.Cells[3, 5].Value = "Qty";
                ws.Cells[3, 5].Style.Font.Bold = true;
                ws.Cells[3, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[3, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[3, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[3, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[3, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[3, 5].AutoFitColumns();

                ws.Cells[3, 6].Value = "Amount";
                ws.Cells[3, 6].Style.Font.Bold = true;
                ws.Cells[3, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[3, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[3, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[3, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[3, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[3, 6].AutoFitColumns(20, 100);


                List<DeliveryInvoiceCreationDetailDownloadData> m =
                    mdl3.DeliveryInvoiceCreationDetailListData.ToList<DeliveryInvoiceCreationDetailDownloadData>();

                int i = 0;
                foreach (DeliveryInvoiceCreationDetailDownloadData x in m)
                {
                    ws.Cells[i + 4, 1].Value = m[i].REFF_NO;
                    // ws.Cells[i + 6, 1].AutoFitColumns();
                    ws.Cells[i + 4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 4, 2].Value = m[i].LP_INV_NO;
                    // ws.Cells[i + 6, 1].AutoFitColumns();
                    ws.Cells[i + 4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 4, 3].Value = m[i].COMP_PRICE_TEXT;
                    // ws.Cells[i + 6, 1].AutoFitColumns();
                    ws.Cells[i + 4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 4, 4].Value = m[i].VALUE;
                    //ws.Cells[i + 6, 2].AutoFitColumns();
                    ws.Cells[i + 4, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ws.Cells[i + 4, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 4, 4].Style.Numberformat.Format = "#,##0";

                    ws.Cells[i + 4, 5].Value = m[i].TRANSACTION_QTY;
                    //ws.Cells[i + 6, 2].AutoFitColumns();
                    ws.Cells[i + 4, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ws.Cells[i + 4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 4, 6].Value = m[i].AMOUNT;
                    //ws.Cells[i + 6, 3].AutoFitColumns();
                    ws.Cells[i + 4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ws.Cells[i + 4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 4, 6].Style.Numberformat.Format = "#,##0";
                    i++;
                }

                try
                {
                    Byte[] bin = p.GetAsByteArray();
                    string fn = "InvoiceCreationDetail-" + Guid.NewGuid().ToString() + "-" + AuthorizedUser.Username +
                                ".xlsx";
                    string DownloadDirectory = "Content/Download/InvoiceCreation/";
                    string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
                    var pathfile = Path.Combine(downdir, fn);

                    excelLink = "Content/Download/InvoiceCreation/" + fn + "";
                    successStatus = "True";
                    System.IO.File.WriteAllBytes(pathfile, bin);
                }
                catch (Exception err)
                {
                    //write log
                    successStatus = "False";
                }

                return Json(new { success = successStatus, excelLink = excelLink, count = m.Count() },
                    JsonRequestBehavior.AllowGet);
            }
        }

        private void DownloadFile(string fname)
        {
            bool forceDownload = true;
            string path = fname;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            string type = "";
            // set known types based on file extension  
            if (ext != null)
            {
                switch (ext.ToLower())
                {

                    case ".xlsx":
                    case ".xls":
                        type = "Application/msexcel";
                        break;
                }
            }
            if (forceDownload)
            {
                Response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            if (type != "")
                Response.ContentType = type;
            Response.WriteFile(path);
            Response.End();
        }

        public JsonResult cancel()
        {
            string DeliveryNo = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            //db.Execute("DLV_INV_InvoiceCreationCancel", new object[] { DeliveryNo, AuthorizedUser.Username});
            //return Json(new { data = "OK"}, JsonRequestBehavior.AllowGet);

            List<DeliveryInvoiceCancelData> model = getCancel(DeliveryNo, AuthorizedUser.Username).ToList();
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
        }


        protected List<DeliveryInvoiceCancelData> getCancel(string DeliveryNo, string User)
        {
            string user = AuthorizedUser.Username;
            var l = db.Fetch<DeliveryInvoiceCancelData>("DLV_INV_InvoiceCreationCancel", new object[]
            {
                DeliveryNo,
                User
            });
            db.Close();
            return l.ToList();
        }




        // New 
        private List<DeliveryInvoiceDataLogPartner> _lpModel = null;

        private List<DeliveryInvoiceDataLogPartner> _LPModel
        {
            get
            {
                if (_lpModel == null)
                    _lpModel = new List<DeliveryInvoiceDataLogPartner>();

                return _lpModel;
            }
            set { _lpModel = value; }
        }


        public ActionResult LPLookup()
        {

            TempData["GridName"] = "LogisticPartnerOption";

            List<DeliveryInvoiceDataLogPartner> trucking =
                Model.GetModel<User>()
                    .FilteringArea<DeliveryInvoiceDataLogPartner>(GetAllLP(), "LP_CD", "D31401", "LogisticPartnerOption");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            return PartialView("PG", _LPModel);
        }

        private List<DeliveryInvoiceDataLogPartner> GetAllLP()
        {
            List<DeliveryInvoiceDataLogPartner> lpModel = new List<DeliveryInvoiceDataLogPartner>();

            try
            {
                lpModel = db.Fetch<DeliveryInvoiceDataLogPartner>("DLV_getLP", new object[] { "" });
            }
            catch (Exception exc)
            {
                throw exc;
            }
            db.Close();

            return lpModel;
        }

        private String GetVendorCodeFromUsername()
        {
            String[] splittedUsername = AuthorizedUser.Username.Split('.');
            String vendorCode = AuthorizedUser.Username.Substring(0, 4);
            if (splittedUsername.Length > 0)
                vendorCode = splittedUsername[0];

            return vendorCode;
        }

        private List<InvoiceCreationPreview> GetInvoiceTaxData()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            String vendorCode = GetVendorCodeFromUsername();
            var invoiceTaxModel = db.Fetch<InvoiceCreationPreview>("GetInvoiceTaxData", new object[] { vendorCode });
            db.Close();

            return invoiceTaxModel;
        }

        public ActionResult InvoiceTaxNoPartial()
        {
            TempData["GridName"] = "INV_TAX_NO";

            List<InvoiceCreationPreview> invoiceTaxModel = GetInvoiceTaxData();
            return PartialView("GridLookup/PartialGrid", invoiceTaxModel);
        }

        //Add By Arka.Taufik 2022-03-22
        #region GetTaxList
        public List<DeliveryInvoiceCreationTax> GetTaxListStart()
        {
            List<DeliveryInvoiceCreationTax> TaxLists = new List<DeliveryInvoiceCreationTax>();
            try
            {
                TaxLists = db.Fetch<DeliveryInvoiceCreationTax>("DLV_INV_getTax");
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                db.Close();
            }
            return TaxLists.ToList();
        }
        public List<DeliveryInvoiceCreationTax> GetTaxList()
        {
            List<DeliveryInvoiceCreationTax> TaxLists = new List<DeliveryInvoiceCreationTax>();
            try
            {
                TaxLists = db.Fetch<DeliveryInvoiceCreationTax>("DLV_INV_getTax");
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                db.Close();
            }
            return TaxLists;
        }

        public ActionResult PartialTaxGridLookup()
        {
            ViewData["TaxGridLookup"] = GetTaxList();

            TempData["GridName"] = "TaxCodeCombo";

            return PartialView("GridLookup/PartialGrid", ViewData["TaxGridLookup"]);
        }
        #endregion


    }
}