﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Configuration;

using Portal.Models.Delivery.Invoice;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace Portal.Controllers.Delivery
{
    public class D31408Controller : BaseController
    {
        //
        // GET: /D31408/
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public D31408Controller()
            : base("Delivery Journal")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            DeliveryJournalData DJPostData = new DeliveryJournalData();
            TempData["EXCEPTION"] = "";
            Model.AddModel(DJPostData);
        }

        public string GetMessage()
        {
            string Message = TempData["EXCEPTION"] == null ? "" : TempData["EXCEPTION"].ToString();
            TempData["EXCEPTION"] = "";
            return Message;
        }

        public ActionResult JournalPostTab()
        {
            DeliveryJournalData DJPostData = Model.GetModel<DeliveryJournalData>();

            try
            {
                string isSearch = Request.Params["isSearch"] != null ? Request.Params["isSearch"].Trim() : "";

                if (isSearch == "Y")
                {
                    DJPostData.DJPostHeader = RetrieveJournalPostData(Request.Params["DOC_NO"], Request.Params["DOC_DT_FROM"],
                                                                      Request.Params["DOC_DT_TO"], Request.Params["DOC_TYPE"],
                                                                      Request.Params["DOC_STATUS"]);
                }
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("DeliveryJournalPostGridPartial", Model);
        }

        public ActionResult JournalPostDetailData()
        {
            DeliveryJournalData DJPostData = Model.GetModel<DeliveryJournalData>();

            try
            {
                string isSearch = Request.Params["isSearch"] != null ? Request.Params["isSearch"].Trim() : "";

                if (isSearch == "Y")
                {
                    DJPostData.DJPostDetail = RetrieveJournalPostDetailData(Request.Params["DOC_NO"].ToString());
                }
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("DeliveryJournalDetailGridPartial", Model);
        }

        public ActionResult JournalTransTab()
        {
            DeliveryJournalData DJPostData = Model.GetModel<DeliveryJournalData>();

            try
            {
                string isSearch = Request.Params["isSearch"] != null ? Request.Params["isSearch"].Trim() : "";

                if (isSearch == "Y")
                {
                    DJPostData.DJTransData = RetrieveJournalTransData(Request.Params["DOC_NO"], Request.Params["DOC_DT_FROM"],
                                                                      Request.Params["DOC_DT_TO"], Request.Params["DOC_TYPE"],
                                                                      Request.Params["DOC_STATUS"]);
                }
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("DeliveryJournalTransDataPartial", Model);
        }

        public ActionResult JournalPostTabHeader()
        {
            DeliveryJournalData DJPostData = Model.GetModel<DeliveryJournalData>();

            try
            {
                DJPostData.DJSysData = GetDockType();
                DJPostData.DJStatusData = GetDocStatus();
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("DeliveryJournalHeaderPartial", Model);
        }

        public ActionResult RetrieveDocType()
        {
            TempData["GridName"] = "DOC_TYPE";
            List<DeliveryJournalSystemMaster> sysData = new List<DeliveryJournalSystemMaster>();

            try
            {
                sysData = GetDockType();
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("PG", sysData);
        }

        public ActionResult RetrieveDocStatus()
        {
            TempData["GridName"] = "DOC_STATUS";
            List<DeliveryJournalSystemMaster> sysData = new List<DeliveryJournalSystemMaster>();

            try
            {
                sysData = GetDocStatus();
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("PG", sysData);
        }

        public List<DeliveryJournalSystemMaster> GetDockType()
        {
            List<DeliveryJournalSystemMaster> sysData = new List<DeliveryJournalSystemMaster>();
            IDBContext db = DbContext;

            try
            {
                sysData = db.Fetch<DeliveryJournalSystemMaster>("DLV_Journal_SystemMaster", new object[] { "DOCTYPE_%" });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return sysData;
        }

        public List<DeliveryJournalSystemMaster> GetDocStatus()
        {
            List<DeliveryJournalSystemMaster> sysData = new List<DeliveryJournalSystemMaster>();
            IDBContext db = DbContext;

            try
            {
                sysData = db.Fetch<DeliveryJournalSystemMaster>("DLV_Journal_SystemMaster", new object[] { "DOCSTATUS_%" });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return sysData;
        }

        public int ConvertExcelWidth(double ExcelWidth)
        {
            const double Widther = 0.00314;
            int NpoiWidth = Convert.ToInt32(Math.Ceiling(ExcelWidth / Widther));

            return NpoiWidth;
        }

        public int ExcelWidthByContentLength(int CharLength)
        {
            const double Lengther = 1.2;
            int ExcelWidth = Convert.ToInt32(Math.Ceiling(CharLength * Lengther));

            return ExcelWidth;
        }

        public int SetMaxWidth(string data, int maxwidth)
        {
            return data != null ?
                    (data.Trim().Length > maxwidth ?
                     data.Trim().Length : maxwidth) :
                     maxwidth;
        }

        public string SetDateString(DateTime data)
        {
            return data != null ? ((data.ToString("dd.MM.yyyy").Trim() == "01.01.1900"
                   || data.ToString("dd.MM.yyyy").Trim() == "01.01.0001")
                   ? DateTime.Now.ToString("dd.MM.yyyy").Trim() : 
                   data.ToString("dd.MM.yyyy").Trim()) : "";
        }

        public string SetIntString(object data)
        {
            return data != null ? data.ToString().Trim() : "";
        }

        public string DownloadJournalTransData(string DOC_NO, string DOC_YEAR, string DOC_DT, string DOC_ITEM_NO,
                                            string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS,
                                            string isall, string TEXT)
        {
            string message = "";

            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;
                string zipname = "Delivery_Journal_Trans_" + DateTime.Now.ToShortDateString().Replace("-", "") + ".zip";
                string filename = "";

                filename = "Delivery_Journal_Trans.csv";
                documentBytes = DownloadTransHeader(DOC_NO, DOC_YEAR, DOC_DT, DOC_ITEM_NO, isall, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS, TEXT);
                listCompress.Add(filename, documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/zip";
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipname));

                Response.BinaryWrite(documentBytes);
                Response.Flush();
                Response.End();
            }
            catch (Exception e)
            {
                message = e.Message.ToString();
            }

            return message;
        }

        private byte[] DownloadTransHeader(string DOC_NO, string DOC_YEAR, string DOC_DT, string DOC_ITEM_NO, string isall,
                                        string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS, string TEXT)
        {
            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                //filesTmp = HttpContext.Request.MapPath("~/Template/DeliveryJournal.xls");
                //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook();

                IRow Hrow;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.CENTER;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.CENTER;

                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.TOP;
                styleContent5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.RED.index;
                styleContent5.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.SetFont(Font4);

                ICellStyle styleContent6 = workbook.CreateCellStyle();
                styleContent6.VerticalAlignment = VerticalAlignment.TOP;
                styleContent6.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.YELLOW.index;
                styleContent6.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent6.Alignment = HorizontalAlignment.CENTER;
                styleContent6.SetFont(Font4);
                #endregion

                List<DeliveryJournalTransactionData> datas = new List<DeliveryJournalTransactionData>();

                if (isall == "true")
                {
                    datas = RetrieveJournalTransData(DOC_NO, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS);
                }
                else
                {
                    datas = RetrieveJournalTransData(TEXT);
                }

                ISheet sheet;

                sheet = workbook.CreateSheet("DeliveryJournalTrans");

                Hrow = sheet.CreateRow(0);

                Hrow.CreateCell(0).SetCellValue("DOC_NO");
                Hrow.CreateCell(1).SetCellValue("DOC_YEAR");
                Hrow.CreateCell(2).SetCellValue("DOC_ITEM");
                Hrow.CreateCell(3).SetCellValue("DOC_DT");
                Hrow.CreateCell(4).SetCellValue("TRANSACTION_CD");
                Hrow.CreateCell(5).SetCellValue("CONDITION_TYPE");
                Hrow.CreateCell(6).SetCellValue("DEBIT_CREDIT_INDICATOR");
                Hrow.CreateCell(7).SetCellValue("GL_ACCOUNT");
                Hrow.CreateCell(8).SetCellValue("ROUTE_CD");
                Hrow.CreateCell(9).SetCellValue("QTY");
                Hrow.CreateCell(10).SetCellValue("AMT_DOC");
                Hrow.CreateCell(11).SetCellValue("DOC_CURR");
                Hrow.CreateCell(12).SetCellValue("AMT_LOCAL");
                Hrow.CreateCell(13).SetCellValue("LOCAL_CURR");
                Hrow.CreateCell(14).SetCellValue("PO_NO");
                Hrow.CreateCell(15).SetCellValue("TAX_CD");
                Hrow.CreateCell(16).SetCellValue("EXCHANGE_RATE");
                Hrow.CreateCell(17).SetCellValue("COST_CENTER_CD");
                Hrow.CreateCell(18).SetCellValue("DOC_TYPE");
                Hrow.CreateCell(19).SetCellValue("GL_BALANCE_FLAG");
                Hrow.CreateCell(20).SetCellValue("WBS_NO");
                Hrow.CreateCell(21).SetCellValue("UOM");
                Hrow.CreateCell(22).SetCellValue("ITEM_TEXT");
                Hrow.CreateCell(23).SetCellValue("CREATED_BY");
                Hrow.CreateCell(24).SetCellValue("CREATED_DT");
                Hrow.CreateCell(25).SetCellValue("CHANGED_BY");
                Hrow.CreateCell(26).SetCellValue("CHANGED_DT");
                Hrow.CreateCell(27).SetCellValue("PROCESS_ID");
                Hrow.CreateCell(28).SetCellValue("POSTING_STS");

                int maxCol0Length = ("DOC_NO").Length;
                int maxCol1Length = ("DOC_YEAR").Length;
                int maxCol2Length = ("DOC_ITEM").Length;
                int maxCol3Length = ("DOC_DT").Length;
                int maxCol4Length = ("TRANSACTION_CD").Length;
                int maxCol5Length = ("CONDITION_TYPE").Length;
                int maxCol6Length = ("DEBIT_CREADIT_INDICATOR").Length;
                int maxCol7Length = ("GL_ACCOUNT").Length;
                int maxCol8Length = ("ROUTE_CD").Length;
                int maxCol9Length = ("QTY").Length;
                int maxCol10Length = ("AMT_DOC").Length;
                int maxCol11Length = ("DOC_CURR").Length;
                int maxCol12Length = ("AMT_LOCAL").Length;
                int maxCol13Length = ("LOCAL_CURR").Length;
                int maxCol14Length = ("PO_NO").Length;
                int maxCol15Length = ("TAX_CD").Length;
                int maxCol16Length = ("EXCHANGE_RATE").Length;
                int maxCol17Length = ("COST_CENTER_CD").Length;
                int maxCol18Length = ("DOC_TYPE").Length;
                int maxCol19Length = ("GL_BALANCE_FLAG").Length;
                int maxCol20Length = ("WBS_NO").Length;
                int maxCol21Length = ("UOM").Length;
                int maxCol22Length = ("ITEM_TEXT").Length;
                int maxCol23Length = ("CREATED_BY").Length;
                int maxCol24Length = ("CREATED_DT").Length;
                int maxCol25Length = ("CHANGED_BY").Length;
                int maxCol26Length = ("CHANGED_DT").Length;
                int maxCol27Length = ("PROCESS_ID").Length;
                int maxCol28Length = ("POSTING_STS").Length;

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(21).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent2;
                Hrow.GetCell(23).CellStyle = styleContent2;
                Hrow.GetCell(24).CellStyle = styleContent2;
                Hrow.GetCell(25).CellStyle = styleContent2;
                Hrow.GetCell(26).CellStyle = styleContent2;
                Hrow.GetCell(27).CellStyle = styleContent2;
                Hrow.GetCell(28).CellStyle = styleContent2;

                int row = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    Hrow.CreateCell(0).SetCellValue(data.DOC_NO != null ? data.DOC_NO.Trim() : "");
                    Hrow.CreateCell(1).SetCellValue(data.DOC_YEAR != null ? data.DOC_YEAR.Trim() : "");
                    Hrow.CreateCell(2).SetCellValue(data.DOC_ITEM != null ? data.DOC_ITEM.Trim() : "");
                    Hrow.CreateCell(3).SetCellValue(SetDateString(data.DOC_DT));
                    Hrow.CreateCell(4).SetCellValue(data.TRANSACTION_CD != null ? data.TRANSACTION_CD.Trim() : "");
                    Hrow.CreateCell(5).SetCellValue(data.CONDITION_TYPE != null ? data.CONDITION_TYPE.Trim() : "");
                    Hrow.CreateCell(6).SetCellValue(data.DEBIT_CREDIT_INDICATOR != null ? data.DEBIT_CREDIT_INDICATOR.Trim() : "");
                    Hrow.CreateCell(7).SetCellValue(data.GL_ACCOUNT != null ? data.GL_ACCOUNT.Trim() : "");
                    Hrow.CreateCell(8).SetCellValue(data.ROUTE_CD != null ? data.ROUTE_CD.Trim() : "");
                    Hrow.CreateCell(9).SetCellValue(SetIntString(data.QTY));
                    Hrow.CreateCell(10).SetCellValue(SetIntString(data.AMT_DOC));
                    Hrow.CreateCell(11).SetCellValue(data.DOC_CURR != null ? data.DOC_CURR.Trim() : "");
                    Hrow.CreateCell(12).SetCellValue(SetIntString(data.AMT_LOCAL));
                    Hrow.CreateCell(13).SetCellValue(SetIntString(data.LOCAL_CURR));
                    Hrow.CreateCell(14).SetCellValue(data.PO_NO != null ? data.PO_NO.Trim() : "");
                    Hrow.CreateCell(15).SetCellValue(data.TAX_CD != null ? data.TAX_CD.Trim() : "");
                    Hrow.CreateCell(16).SetCellValue(SetIntString(data.EXCHANGE_RATE));
                    Hrow.CreateCell(17).SetCellValue(data.COST_CENTER_CD != null ? data.COST_CENTER_CD.Trim() : "");
                    Hrow.CreateCell(18).SetCellValue(data.DOC_TYPE != null ? data.DOC_TYPE.Trim() : "");
                    Hrow.CreateCell(19).SetCellValue(data.GL_BALANCE_FLAG != null ? data.GL_BALANCE_FLAG.Trim() : "");
                    Hrow.CreateCell(20).SetCellValue(data.WBS_NO != null ? data.WBS_NO.Trim() : "");
                    Hrow.CreateCell(21).SetCellValue(data.UOM != null ? data.UOM.Trim() : "");
                    Hrow.CreateCell(22).SetCellValue(data.ITEM_TEXT != null ? data.ITEM_TEXT.Trim() : "");
                    Hrow.CreateCell(23).SetCellValue(data.CREATED_BY != null ? data.CREATED_BY.Trim() : "");
                    Hrow.CreateCell(24).SetCellValue(SetDateString(data.CREATED_DT));
                    Hrow.CreateCell(25).SetCellValue(data.CHANGED_BY != null ? data.CHANGED_BY.Trim() : "");
                    Hrow.CreateCell(26).SetCellValue(SetDateString(data.CHANGED_DT));
                    Hrow.CreateCell(27).SetCellValue(SetIntString(data.PROCESS_ID) == "0" ? "" : SetIntString(data.PROCESS_ID));
                    Hrow.CreateCell(28).SetCellValue(data.POSTING_STS != null ? data.POSTING_STS.Trim() : "");

                    maxCol0Length = SetMaxWidth(data.DOC_NO, maxCol0Length);
                    maxCol1Length = SetMaxWidth(data.DOC_YEAR, maxCol1Length);
                    maxCol2Length = SetMaxWidth(data.DOC_ITEM, maxCol2Length);
                    maxCol3Length = SetMaxWidth(SetDateString(data.DOC_DT), maxCol3Length);
                    maxCol4Length = SetMaxWidth(data.TRANSACTION_CD, maxCol4Length);
                    maxCol5Length = SetMaxWidth(data.CONDITION_TYPE, maxCol5Length);
                    maxCol6Length = SetMaxWidth(data.DEBIT_CREDIT_INDICATOR, maxCol6Length);
                    maxCol7Length = SetMaxWidth(data.GL_ACCOUNT, maxCol7Length);
                    maxCol8Length = SetMaxWidth(data.ROUTE_CD, maxCol8Length);
                    maxCol9Length = SetMaxWidth(SetIntString(data.QTY), maxCol9Length);
                    maxCol10Length = SetMaxWidth(SetIntString(data.AMT_DOC), maxCol10Length);
                    maxCol11Length = SetMaxWidth(data.DOC_CURR, maxCol11Length);
                    maxCol12Length = SetMaxWidth(SetIntString(data.AMT_LOCAL), maxCol12Length);
                    maxCol13Length = SetMaxWidth(SetIntString(data.LOCAL_CURR), maxCol3Length);
                    maxCol14Length = SetMaxWidth(data.PO_NO, maxCol4Length);
                    maxCol15Length = SetMaxWidth(data.TAX_CD, maxCol5Length);
                    maxCol16Length = SetMaxWidth(SetIntString(data.EXCHANGE_RATE), maxCol16Length);
                    maxCol17Length = SetMaxWidth(data.COST_CENTER_CD, maxCol17Length);
                    maxCol18Length = SetMaxWidth(data.DOC_TYPE, maxCol18Length);
                    maxCol19Length = SetMaxWidth(data.GL_BALANCE_FLAG, maxCol19Length);
                    maxCol20Length = SetMaxWidth(data.WBS_NO, maxCol20Length);
                    maxCol21Length = SetMaxWidth(data.UOM, maxCol21Length);
                    maxCol22Length = SetMaxWidth(data.ITEM_TEXT, maxCol22Length);
                    maxCol23Length = SetMaxWidth(data.CREATED_BY, maxCol23Length);
                    maxCol24Length = SetMaxWidth(SetDateString(data.CREATED_DT), maxCol24Length);
                    maxCol25Length = SetMaxWidth(data.CHANGED_BY, maxCol25Length);
                    maxCol26Length = SetMaxWidth(SetDateString(data.CHANGED_DT), maxCol26Length);
                    maxCol27Length = SetMaxWidth(SetIntString(data.PROCESS_ID), maxCol27Length);
                    maxCol28Length = SetMaxWidth(data.POSTING_STS, maxCol28Length);

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent3;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent3;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = styleContent3;
                    Hrow.GetCell(7).CellStyle = styleContent3;
                    Hrow.GetCell(8).CellStyle = styleContent3;
                    Hrow.GetCell(9).CellStyle = styleContent3;
                    Hrow.GetCell(10).CellStyle = styleContent3;
                    Hrow.GetCell(11).CellStyle = styleContent3;
                    Hrow.GetCell(12).CellStyle = styleContent3;
                    Hrow.GetCell(13).CellStyle = styleContent3;
                    Hrow.GetCell(14).CellStyle = styleContent3;
                    Hrow.GetCell(15).CellStyle = styleContent3;
                    Hrow.GetCell(16).CellStyle = styleContent3;
                    Hrow.GetCell(17).CellStyle = styleContent3;
                    Hrow.GetCell(18).CellStyle = styleContent3;
                    Hrow.GetCell(19).CellStyle = styleContent3;
                    Hrow.GetCell(20).CellStyle = styleContent3;
                    Hrow.GetCell(21).CellStyle = styleContent3;
                    Hrow.GetCell(22).CellStyle = styleContent3;
                    Hrow.GetCell(23).CellStyle = styleContent3;
                    Hrow.GetCell(24).CellStyle = styleContent3;
                    Hrow.GetCell(25).CellStyle = styleContent3;
                    Hrow.GetCell(26).CellStyle = styleContent3;
                    Hrow.GetCell(27).CellStyle = styleContent3;
                    Hrow.GetCell(28).CellStyle = data.POSTING_STS.ToString() == "ERROR POSTING" ? styleContent5 : 
                        (data.POSTING_STS.ToString() == "IN PROGRESS" ? styleContent6 : styleContent3);

                    row++;
                }

                sheet.SetColumnWidth(0, ConvertExcelWidth(ExcelWidthByContentLength(maxCol0Length)));
                sheet.SetColumnWidth(1, ConvertExcelWidth(ExcelWidthByContentLength(maxCol1Length)));
                sheet.SetColumnWidth(2, ConvertExcelWidth(ExcelWidthByContentLength(maxCol2Length)));
                sheet.SetColumnWidth(3, ConvertExcelWidth(ExcelWidthByContentLength(maxCol3Length)));
                sheet.SetColumnWidth(4, ConvertExcelWidth(ExcelWidthByContentLength(maxCol4Length)));
                sheet.SetColumnWidth(5, ConvertExcelWidth(ExcelWidthByContentLength(maxCol5Length)));
                sheet.SetColumnWidth(6, ConvertExcelWidth(ExcelWidthByContentLength(maxCol6Length)));
                sheet.SetColumnWidth(7, ConvertExcelWidth(ExcelWidthByContentLength(maxCol7Length)));
                sheet.SetColumnWidth(8, ConvertExcelWidth(ExcelWidthByContentLength(maxCol8Length)));
                sheet.SetColumnWidth(9, ConvertExcelWidth(ExcelWidthByContentLength(maxCol9Length)));
                sheet.SetColumnWidth(10, ConvertExcelWidth(ExcelWidthByContentLength(maxCol10Length)));
                sheet.SetColumnWidth(11, ConvertExcelWidth(ExcelWidthByContentLength(maxCol11Length)));
                sheet.SetColumnWidth(12, ConvertExcelWidth(ExcelWidthByContentLength(maxCol12Length)));
                sheet.SetColumnWidth(13, ConvertExcelWidth(ExcelWidthByContentLength(maxCol13Length)));
                sheet.SetColumnWidth(14, ConvertExcelWidth(ExcelWidthByContentLength(maxCol14Length)));
                sheet.SetColumnWidth(15, ConvertExcelWidth(ExcelWidthByContentLength(maxCol15Length)));
                sheet.SetColumnWidth(16, ConvertExcelWidth(ExcelWidthByContentLength(maxCol16Length)));
                sheet.SetColumnWidth(17, ConvertExcelWidth(ExcelWidthByContentLength(maxCol17Length)));
                sheet.SetColumnWidth(18, ConvertExcelWidth(ExcelWidthByContentLength(maxCol18Length)));
                sheet.SetColumnWidth(19, ConvertExcelWidth(ExcelWidthByContentLength(maxCol19Length)));
                sheet.SetColumnWidth(20, ConvertExcelWidth(ExcelWidthByContentLength(maxCol20Length)));
                sheet.SetColumnWidth(21, ConvertExcelWidth(ExcelWidthByContentLength(maxCol21Length)));
                sheet.SetColumnWidth(22, ConvertExcelWidth(ExcelWidthByContentLength(maxCol22Length)));
                sheet.SetColumnWidth(23, ConvertExcelWidth(ExcelWidthByContentLength(maxCol23Length)));
                sheet.SetColumnWidth(24, ConvertExcelWidth(ExcelWidthByContentLength(maxCol24Length)));
                sheet.SetColumnWidth(25, ConvertExcelWidth(ExcelWidthByContentLength(maxCol25Length)));
                sheet.SetColumnWidth(26, ConvertExcelWidth(ExcelWidthByContentLength(maxCol26Length)));
                sheet.SetColumnWidth(27, ConvertExcelWidth(ExcelWidthByContentLength(maxCol27Length)));
                sheet.SetColumnWidth(28, ConvertExcelWidth(ExcelWidthByContentLength(maxCol28Length)));

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                return ms.ToArray();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string DownloadJournalPostData(string DOC_NO, string DOC_YEAR, string DOC_DT, string isall,
                                            string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS)
        {
            string message = "";

            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;
                string zipname = "Delivery_Journal_" + DateTime.Now.ToShortDateString().Replace("-", "") + ".zip";
                string filename = "";

                filename = "Delivery_Journal_Header.csv";
                documentBytes = DownloadHeader(DOC_NO, DOC_YEAR, DOC_DT, isall, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS);
                listCompress.Add(filename, documentBytes);

                filename = "Delivery_Journal_Detail.csv";
                documentBytes = DownloadDetail(DOC_NO, DOC_YEAR, DOC_DT, isall, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS);
                listCompress.Add(filename, documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/zip";
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipname));

                Response.BinaryWrite(documentBytes);
                Response.Flush();
                Response.End();
            }
            catch (Exception e)
            {
                message = e.Message.ToString();
            }

            return message;
        }

        private byte[] DownloadHeader(string DOC_NO, string DOC_YEAR, string DOC_DT, string isall,
                                      string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS)
        {
            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                //filesTmp = HttpContext.Request.MapPath("~/Template/DeliveryJournal.xls");
                //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook();

                IRow Hrow;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.CENTER;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.CENTER;

                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.TOP;
                styleContent5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.RED.index;
                styleContent5.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.SetFont(Font4);

                ICellStyle styleContent6 = workbook.CreateCellStyle();
                styleContent6.VerticalAlignment = VerticalAlignment.TOP;
                styleContent6.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.YELLOW.index;
                styleContent6.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent6.Alignment = HorizontalAlignment.CENTER;
                styleContent6.SetFont(Font4);
                #endregion

                List<DeliveryJournalPostHeaderData> datas = new List<DeliveryJournalPostHeaderData>();

                if (isall == "true")
                {
                    datas = RetrieveJournalPostData(DOC_NO, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS);
                }
                else
                {
                    datas = RetrieveJournalPostData(DOC_NO, DOC_YEAR, DOC_DT);
                }

                ISheet sheet;

                sheet = workbook.CreateSheet("DeliveryHeader");

                Hrow = sheet.CreateRow(0);

                Hrow.CreateCell(0).SetCellValue("DOC_NO");
                Hrow.CreateCell(1).SetCellValue("DOC_YEAR");
                Hrow.CreateCell(2).SetCellValue("DOC_DT");
                Hrow.CreateCell(3).SetCellValue("POSTING_DT");
                Hrow.CreateCell(4).SetCellValue("DOC_HEADER_TEXT");
                Hrow.CreateCell(5).SetCellValue("REFF_NO");
                Hrow.CreateCell(6).SetCellValue("LP_CD");
                Hrow.CreateCell(7).SetCellValue("CUSTOMER_CD");
                Hrow.CreateCell(8).SetCellValue("PAYMENT_TERM_CD");
                Hrow.CreateCell(9).SetCellValue("PAYMENT_METHOD_CD");
                Hrow.CreateCell(10).SetCellValue("PARTNER_BANK_KEY");
                Hrow.CreateCell(11).SetCellValue("ACCT_DOC_NO");
                Hrow.CreateCell(12).SetCellValue("ACCT_DOC_YEAR");
                Hrow.CreateCell(13).SetCellValue("HEADER_TYPE");
                Hrow.CreateCell(14).SetCellValue("POSTING_STS");
                Hrow.CreateCell(15).SetCellValue("ERROR_POSTING");
                Hrow.CreateCell(16).SetCellValue("CREATED_BY");
                Hrow.CreateCell(17).SetCellValue("CREATED_DT");
                Hrow.CreateCell(18).SetCellValue("CHANGED_BY");
                Hrow.CreateCell(19).SetCellValue("CHANGED_DT");

                int maxCol0Length = ("DOC_NO").Length;
                int maxCol1Length = ("DOC_YEAR").Length;
                int maxCol2Length = ("DOC_DT").Length;
                int maxCol3Length = ("POSTING_DT").Length;
                int maxCol4Length = ("DOC_HEADER_TEXT").Length;
                int maxCol5Length = ("REFF_NO").Length;
                int maxCol6Length = ("LP_CD").Length;
                int maxCol7Length = ("CUSTOMER_CD").Length;
                int maxCol8Length = ("PAYMENT_TERM_CD").Length;
                int maxCol9Length = ("PAYMENT_METHOD_CD").Length;
                int maxCol10Length = ("PARTNER_BANK_KEY").Length;
                int maxCol11Length = ("ACCT_DOC_NO").Length;
                int maxCol12Length = ("ACCT_DOC_YEAR").Length;
                int maxCol13Length = ("HEADER_TYPE").Length;
                int maxCol14Length = ("POSTING_STS").Length;
                int maxCol15Length = ("ERROR_POSTING").Length;
                int maxCol16Length = ("CREATED_BY").Length;
                int maxCol17Length = ("CREATED_DT").Length;
                int maxCol18Length = ("CHANGED_BY").Length;
                int maxCol19Length = ("CHANGED_DT").Length;

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;

                int row = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    Hrow.CreateCell(0).SetCellValue(data.DOC_NO != null ? data.DOC_NO.Trim() : "");
                    Hrow.CreateCell(1).SetCellValue(data.DOC_YEAR != null ? data.DOC_YEAR.Trim() : "");
                    Hrow.CreateCell(2).SetCellValue(SetDateString(data.DOC_DT));
                    Hrow.CreateCell(3).SetCellValue(SetDateString(data.POSTING_DT));
                    Hrow.CreateCell(4).SetCellValue(data.DOC_HEADER_TXT != null ? data.DOC_HEADER_TXT.Trim() : "");
                    Hrow.CreateCell(5).SetCellValue(data.REFF_NO != null ? data.REFF_NO.Trim() : "");
                    Hrow.CreateCell(6).SetCellValue(data.LP_CD != null ? data.LP_CD.Trim() : "");
                    Hrow.CreateCell(7).SetCellValue(data.CUSTOMER_CD != null ? data.CUSTOMER_CD.Trim() : "");
                    Hrow.CreateCell(8).SetCellValue(data.PAYMENT_TERM_CD != null ? data.PAYMENT_TERM_CD.Trim() : "");
                    Hrow.CreateCell(9).SetCellValue(data.PAYMENT_METHOD_CD != null ? data.PAYMENT_METHOD_CD.Trim() : "");
                    Hrow.CreateCell(10).SetCellValue(data.PARTNER_BANK_KEY != null ? data.PARTNER_BANK_KEY.Trim() : "");
                    Hrow.CreateCell(11).SetCellValue(data.ACCT_DOC_NO != null ? data.ACCT_DOC_NO.Trim() : "");
                    Hrow.CreateCell(12).SetCellValue(data.ACCT_DOC_YEAR != null ? data.ACCT_DOC_YEAR.Trim() : "");
                    Hrow.CreateCell(13).SetCellValue(data.HEADER_TYPE != null ? data.HEADER_TYPE.Trim() : "");
                    Hrow.CreateCell(14).SetCellValue(data.POSTING_STS != null ? data.POSTING_STS.Trim() : "");
                    Hrow.CreateCell(15).SetCellValue(data.ERROR_POSTING != null ? data.ERROR_POSTING.Trim() : "");
                    Hrow.CreateCell(16).SetCellValue(data.CREATED_BY != null ? data.CREATED_BY.Trim() : "");
                    Hrow.CreateCell(17).SetCellValue(SetDateString(data.CREATED_DT));
                    Hrow.CreateCell(18).SetCellValue(data.CHANGED_BY != null ? data.CHANGED_BY.Trim() : "");
                    Hrow.CreateCell(19).SetCellValue(SetDateString(data.CHANGED_DT));

                    maxCol0Length = SetMaxWidth(data.DOC_NO, maxCol0Length);
                    maxCol1Length = SetMaxWidth(data.DOC_YEAR, maxCol1Length);
                    maxCol2Length = SetMaxWidth(SetDateString(data.DOC_DT), maxCol2Length);
                    maxCol3Length = SetMaxWidth(SetDateString(data.POSTING_DT), maxCol3Length);
                    maxCol4Length = SetMaxWidth(data.DOC_HEADER_TXT, maxCol4Length);
                    maxCol5Length = SetMaxWidth(data.REFF_NO, maxCol5Length);
                    maxCol6Length = SetMaxWidth(data.LP_CD, maxCol6Length);
                    maxCol7Length = SetMaxWidth(data.CUSTOMER_CD, maxCol7Length);
                    maxCol8Length = SetMaxWidth(data.PAYMENT_TERM_CD, maxCol8Length);
                    maxCol9Length = SetMaxWidth(data.PAYMENT_METHOD_CD, maxCol9Length);
                    maxCol10Length = SetMaxWidth(data.PARTNER_BANK_KEY, maxCol10Length);
                    maxCol11Length = SetMaxWidth(data.ACCT_DOC_NO, maxCol11Length);
                    maxCol12Length = SetMaxWidth(data.ACCT_DOC_YEAR, maxCol12Length);
                    maxCol13Length = SetMaxWidth(data.HEADER_TYPE, maxCol13Length);
                    maxCol14Length = SetMaxWidth(data.POSTING_STS, maxCol14Length);
                    maxCol15Length = SetMaxWidth(data.ERROR_POSTING, maxCol15Length);
                    maxCol16Length = SetMaxWidth(data.CREATED_BY, maxCol16Length);
                    maxCol17Length = SetMaxWidth(SetDateString(data.CREATED_DT), maxCol17Length);
                    maxCol18Length = SetMaxWidth(data.CHANGED_BY, maxCol18Length);
                    maxCol19Length = SetMaxWidth(SetDateString(data.CHANGED_DT), maxCol19Length);

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent3;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent3;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = styleContent3;
                    Hrow.GetCell(7).CellStyle = styleContent3;
                    Hrow.GetCell(8).CellStyle = styleContent3;
                    Hrow.GetCell(9).CellStyle = styleContent3;
                    Hrow.GetCell(10).CellStyle = styleContent3;
                    Hrow.GetCell(11).CellStyle = styleContent3;
                    Hrow.GetCell(12).CellStyle = styleContent3;
                    Hrow.GetCell(13).CellStyle = styleContent3;
                    Hrow.GetCell(14).CellStyle = data.POSTING_STS.ToString() == "ERROR POSTING" ? styleContent5 :
                        (data.POSTING_STS.ToString() == "IN PROGRESS" ? styleContent6 : styleContent3);
                    Hrow.GetCell(15).CellStyle = styleContent3;
                    Hrow.GetCell(16).CellStyle = styleContent3;
                    Hrow.GetCell(17).CellStyle = styleContent3;
                    Hrow.GetCell(18).CellStyle = styleContent3;
                    Hrow.GetCell(19).CellStyle = styleContent3;

                    row++;
                }

                sheet.SetColumnWidth(0, ConvertExcelWidth(ExcelWidthByContentLength(maxCol0Length)));
                sheet.SetColumnWidth(1, ConvertExcelWidth(ExcelWidthByContentLength(maxCol1Length)));
                sheet.SetColumnWidth(2, ConvertExcelWidth(ExcelWidthByContentLength(maxCol2Length)));
                sheet.SetColumnWidth(3, ConvertExcelWidth(ExcelWidthByContentLength(maxCol3Length)));
                sheet.SetColumnWidth(4, ConvertExcelWidth(ExcelWidthByContentLength(maxCol4Length)));
                sheet.SetColumnWidth(5, ConvertExcelWidth(ExcelWidthByContentLength(maxCol5Length)));
                sheet.SetColumnWidth(6, ConvertExcelWidth(ExcelWidthByContentLength(maxCol6Length)));
                sheet.SetColumnWidth(7, ConvertExcelWidth(ExcelWidthByContentLength(maxCol7Length)));
                sheet.SetColumnWidth(8, ConvertExcelWidth(ExcelWidthByContentLength(maxCol8Length)));
                sheet.SetColumnWidth(9, ConvertExcelWidth(ExcelWidthByContentLength(maxCol9Length)));
                sheet.SetColumnWidth(10, ConvertExcelWidth(ExcelWidthByContentLength(maxCol10Length)));
                sheet.SetColumnWidth(11, ConvertExcelWidth(ExcelWidthByContentLength(maxCol11Length)));
                sheet.SetColumnWidth(12, ConvertExcelWidth(ExcelWidthByContentLength(maxCol12Length)));
                sheet.SetColumnWidth(13, ConvertExcelWidth(ExcelWidthByContentLength(maxCol13Length)));
                sheet.SetColumnWidth(14, ConvertExcelWidth(ExcelWidthByContentLength(maxCol14Length)));
                sheet.SetColumnWidth(15, ConvertExcelWidth(ExcelWidthByContentLength(maxCol15Length)));
                sheet.SetColumnWidth(16, ConvertExcelWidth(ExcelWidthByContentLength(maxCol16Length)));
                sheet.SetColumnWidth(17, ConvertExcelWidth(ExcelWidthByContentLength(maxCol17Length)));
                sheet.SetColumnWidth(18, ConvertExcelWidth(ExcelWidthByContentLength(maxCol18Length)));
                sheet.SetColumnWidth(19, ConvertExcelWidth(ExcelWidthByContentLength(maxCol19Length)));

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                return ms.ToArray();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private byte[] DownloadDetail(string DOC_NO, string DOC_YEAR, string DOC_DT, string isall,
                                      string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS)
        {
            try
            {
                //string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                //filesTmp = HttpContext.Request.MapPath("~/Template/DeliveryJournal.xls");

                //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook();

                IRow Hrow;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.CENTER;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.CENTER;

                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);
                #endregion

                List<DeliveryJournalPostDetailData> datas = new List<DeliveryJournalPostDetailData>();

                if (isall == "true")
                {
                    datas = RetrieveJournalPostDetailData(DOC_NO, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS);
                }
                else
                {
                    datas = RetrieveJournalPostDetailData(DOC_NO, DOC_YEAR, DOC_DT);
                }

                ISheet sheet;

                sheet = workbook.CreateSheet("Delivery_Detail");

                Hrow = sheet.CreateRow(0);

                Hrow.CreateCell(0).SetCellValue("DOC_NO");
                Hrow.CreateCell(1).SetCellValue("DOC_YEAR");
                Hrow.CreateCell(2).SetCellValue("DOC_ITEM_NO");
                Hrow.CreateCell(3).SetCellValue("CONDITION_TYPE");
                Hrow.CreateCell(4).SetCellValue("GL_ACCOUNT");
                Hrow.CreateCell(5).SetCellValue("ROUTE_CD");
                Hrow.CreateCell(6).SetCellValue("DOC_QTY");
                Hrow.CreateCell(7).SetCellValue("DOC_AMT");
                Hrow.CreateCell(8).SetCellValue("DOC_CURR_CD");
                Hrow.CreateCell(9).SetCellValue("PO_NO");
                Hrow.CreateCell(10).SetCellValue("TAX_CD");
                Hrow.CreateCell(11).SetCellValue("EXCHANGE_RATE");
                Hrow.CreateCell(12).SetCellValue("COST_CENTER_CD");
                Hrow.CreateCell(13).SetCellValue("TRANSACTION_NO");
                Hrow.CreateCell(14).SetCellValue("WBS_NO");
                Hrow.CreateCell(15).SetCellValue("UOM");
                Hrow.CreateCell(16).SetCellValue("ITEM_TEXT");
                Hrow.CreateCell(17).SetCellValue("CREATED_BY");
                Hrow.CreateCell(18).SetCellValue("CREATED_DT");
                Hrow.CreateCell(19).SetCellValue("CHANGED_BY");
                Hrow.CreateCell(20).SetCellValue("CHANGED_DT");

                int maxCol0Length = ("DOC_NO").Length;
                int maxCol1Length = ("DOC_YEAR").Length;
                int maxCol2Length = ("DOC_ITEM_NO").Length;
                int maxCol3Length = ("CONDITION_TYPE").Length;
                int maxCol4Length = ("GL_ACCOUNT").Length;
                int maxCol5Length = ("ROUTE_CD").Length;
                int maxCol6Length = ("DOC_QTY").Length;
                int maxCol7Length = ("DOC_AMT").Length;
                int maxCol8Length = ("DOC_CURR_CD").Length;
                int maxCol9Length = ("PO_NO").Length;
                int maxCol10Length = ("TAX_CD").Length;
                int maxCol11Length = ("EXCHANGE_RATE").Length;
                int maxCol12Length = ("COST_CENTER_CD").Length;
                int maxCol13Length = ("TRANSACTION_NO").Length;
                int maxCol14Length = ("WBS_NO").Length;
                int maxCol15Length = ("UOM").Length;
                int maxCol16Length = ("ITEM_TEXT").Length;
                int maxCol17Length = ("CREATED_BY").Length;
                int maxCol18Length = ("CREATED_DT").Length;
                int maxCol19Length = ("CHANGED_BY").Length;
                int maxCol20Length = ("CHANGED_DT").Length;

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;

                //int no = 1;
                int row = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    Hrow.CreateCell(0).SetCellValue(data.DOC_NO != null ? data.DOC_NO.Trim() : "");
                    Hrow.CreateCell(1).SetCellValue(data.DOC_YEAR != null ? data.DOC_YEAR.Trim() : "");
                    Hrow.CreateCell(2).SetCellValue(data.DOC_ITEM_NO != null ? data.DOC_ITEM_NO.Trim() : "");
                    Hrow.CreateCell(3).SetCellValue(data.CONDITION_TYPE != null ? data.CONDITION_TYPE.Trim() : "");
                    Hrow.CreateCell(4).SetCellValue(data.GL_ACCOUNT != null ? data.GL_ACCOUNT.Trim() : "");
                    Hrow.CreateCell(5).SetCellValue(data.ROUTE_CD != null ? data.ROUTE_CD.Trim() : "");
                    Hrow.CreateCell(6).SetCellValue(data.DOC_QTY != null ? data.DOC_QTY.Trim() : "");
                    Hrow.CreateCell(7).SetCellValue(data.DOC_AMT != null ? data.DOC_AMT.Trim() : "");
                    Hrow.CreateCell(8).SetCellValue(data.DOC_CURR_CD != null ? data.DOC_CURR_CD.Trim() : "");
                    Hrow.CreateCell(9).SetCellValue(data.PO_NO != null ? data.PO_NO.Trim() : "");
                    Hrow.CreateCell(10).SetCellValue(data.TAX_CD != null ? data.TAX_CD.Trim() : "");
                    Hrow.CreateCell(11).SetCellValue(data.EXCHANGE_RATE != null ? data.EXCHANGE_RATE.Trim() : "");
                    Hrow.CreateCell(12).SetCellValue(data.COST_CENTER_CD != null ? data.COST_CENTER_CD.Trim() : "");
                    Hrow.CreateCell(13).SetCellValue(data.TRANSACTION_NO != null ? data.TRANSACTION_NO.Trim() : "");
                    Hrow.CreateCell(14).SetCellValue(data.WBS_NO != null ? data.WBS_NO.Trim() : "");
                    Hrow.CreateCell(15).SetCellValue(data.UOM != null ? data.UOM.Trim() : "");
                    Hrow.CreateCell(16).SetCellValue(data.ITEM_TEXT != null ? data.ITEM_TEXT.Trim() : "");
                    Hrow.CreateCell(17).SetCellValue(data.CREATED_BY != null ? data.CREATED_BY.Trim() : "");
                    Hrow.CreateCell(18).SetCellValue(SetDateString(data.CREATED_DT));
                    Hrow.CreateCell(19).SetCellValue(data.CHANGED_BY != null ? data.CHANGED_BY.Trim() : "");
                    Hrow.CreateCell(20).SetCellValue(SetDateString(data.CHANGED_DT));

                    maxCol0Length = SetMaxWidth(data.DOC_NO, maxCol0Length);
                    maxCol1Length = SetMaxWidth(data.DOC_YEAR, maxCol1Length);
                    maxCol2Length = SetMaxWidth(data.DOC_ITEM_NO, maxCol2Length);
                    maxCol3Length = SetMaxWidth(data.CONDITION_TYPE, maxCol3Length);
                    maxCol4Length = SetMaxWidth(data.GL_ACCOUNT, maxCol4Length);
                    maxCol5Length = SetMaxWidth(data.ROUTE_CD, maxCol5Length);
                    maxCol6Length = SetMaxWidth(data.DOC_QTY, maxCol6Length);
                    maxCol7Length = SetMaxWidth(data.DOC_AMT, maxCol7Length);
                    maxCol8Length = SetMaxWidth(data.DOC_CURR_CD, maxCol8Length);
                    maxCol9Length = SetMaxWidth(data.PO_NO, maxCol9Length);
                    maxCol10Length = SetMaxWidth(data.TAX_CD, maxCol10Length);
                    maxCol11Length = SetMaxWidth(data.EXCHANGE_RATE, maxCol11Length);
                    maxCol12Length = SetMaxWidth(data.COST_CENTER_CD, maxCol12Length);
                    maxCol13Length = SetMaxWidth(data.TRANSACTION_NO, maxCol13Length);
                    maxCol14Length = SetMaxWidth(data.WBS_NO, maxCol14Length);
                    maxCol15Length = SetMaxWidth(data.UOM, maxCol15Length);
                    maxCol16Length = SetMaxWidth(data.ITEM_TEXT, maxCol16Length);
                    maxCol17Length = SetMaxWidth(data.CREATED_BY, maxCol17Length);
                    maxCol18Length = SetMaxWidth(SetDateString(data.CREATED_DT), maxCol18Length);
                    maxCol19Length = SetMaxWidth(data.CHANGED_BY, maxCol19Length);
                    maxCol20Length = SetMaxWidth(SetDateString(data.CHANGED_DT), maxCol20Length);

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent3;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent3;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = styleContent3;
                    Hrow.GetCell(7).CellStyle = styleContent3;
                    Hrow.GetCell(8).CellStyle = styleContent3;
                    Hrow.GetCell(9).CellStyle = styleContent3;
                    Hrow.GetCell(10).CellStyle = styleContent3;
                    Hrow.GetCell(11).CellStyle = styleContent3;
                    Hrow.GetCell(12).CellStyle = styleContent3;
                    Hrow.GetCell(13).CellStyle = styleContent3;
                    Hrow.GetCell(14).CellStyle = styleContent3;
                    Hrow.GetCell(15).CellStyle = styleContent3;
                    Hrow.GetCell(16).CellStyle = styleContent3;
                    Hrow.GetCell(17).CellStyle = styleContent3;
                    Hrow.GetCell(18).CellStyle = styleContent3;
                    Hrow.GetCell(19).CellStyle = styleContent3;
                    Hrow.GetCell(20).CellStyle = styleContent3;

                    row++;

                }

                sheet.SetColumnWidth(0, ConvertExcelWidth(ExcelWidthByContentLength(maxCol0Length)));
                sheet.SetColumnWidth(1, ConvertExcelWidth(ExcelWidthByContentLength(maxCol1Length)));
                sheet.SetColumnWidth(2, ConvertExcelWidth(ExcelWidthByContentLength(maxCol2Length)));
                sheet.SetColumnWidth(3, ConvertExcelWidth(ExcelWidthByContentLength(maxCol3Length)));
                sheet.SetColumnWidth(4, ConvertExcelWidth(ExcelWidthByContentLength(maxCol4Length)));
                sheet.SetColumnWidth(5, ConvertExcelWidth(ExcelWidthByContentLength(maxCol5Length)));
                sheet.SetColumnWidth(6, ConvertExcelWidth(ExcelWidthByContentLength(maxCol6Length)));
                sheet.SetColumnWidth(7, ConvertExcelWidth(ExcelWidthByContentLength(maxCol7Length)));
                sheet.SetColumnWidth(8, ConvertExcelWidth(ExcelWidthByContentLength(maxCol8Length)));
                sheet.SetColumnWidth(9, ConvertExcelWidth(ExcelWidthByContentLength(maxCol9Length)));
                sheet.SetColumnWidth(10, ConvertExcelWidth(ExcelWidthByContentLength(maxCol10Length)));
                sheet.SetColumnWidth(11, ConvertExcelWidth(ExcelWidthByContentLength(maxCol11Length)));
                sheet.SetColumnWidth(12, ConvertExcelWidth(ExcelWidthByContentLength(maxCol12Length)));
                sheet.SetColumnWidth(13, ConvertExcelWidth(ExcelWidthByContentLength(maxCol13Length)));
                sheet.SetColumnWidth(14, ConvertExcelWidth(ExcelWidthByContentLength(maxCol14Length)));
                sheet.SetColumnWidth(15, ConvertExcelWidth(ExcelWidthByContentLength(maxCol15Length)));
                sheet.SetColumnWidth(16, ConvertExcelWidth(ExcelWidthByContentLength(maxCol16Length)));
                sheet.SetColumnWidth(17, ConvertExcelWidth(ExcelWidthByContentLength(maxCol17Length)));
                sheet.SetColumnWidth(18, ConvertExcelWidth(ExcelWidthByContentLength(maxCol18Length)));
                sheet.SetColumnWidth(19, ConvertExcelWidth(ExcelWidthByContentLength(maxCol19Length)));
                sheet.SetColumnWidth(20, ConvertExcelWidth(ExcelWidthByContentLength(maxCol20Length)));

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                //ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<DeliveryJournalPostHeaderData> RetrieveJournalPostData(string DOC_NO, string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS)
        {
            List<DeliveryJournalPostHeaderData> DJPostData = new List<DeliveryJournalPostHeaderData>();

            IDBContext db = DbContext;

            try
            {
                DJPostData = db.Fetch<DeliveryJournalPostHeaderData>("DLV_Journal_PostData", new object[] { DOC_NO, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return DJPostData;
        }

        private List<DeliveryJournalPostHeaderData> RetrieveJournalPostData(string DOC_NO, string DOC_YEAR, string DOC_DT)
        {
            List<DeliveryJournalPostHeaderData> DJPostData = new List<DeliveryJournalPostHeaderData>();

            IDBContext db = DbContext;

            try
            {
                DJPostData = db.Fetch<DeliveryJournalPostHeaderData>("DLV_Journal_DownloadHeaderData", new object[] { DOC_NO });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return DJPostData;
        }

        private List<DeliveryJournalPostDetailData> RetrieveJournalPostDetailData(string DOC_NO, string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS)
        {
            List<DeliveryJournalPostDetailData> DJPostDetailData = new List<DeliveryJournalPostDetailData>();

            IDBContext db = DbContext;

            try
            {
                DJPostDetailData = db.Fetch<DeliveryJournalPostDetailData>("DLV_Journal_DownloadAllDetailData", new object[] { DOC_NO, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return DJPostDetailData;
        }

        private List<DeliveryJournalPostDetailData> RetrieveJournalPostDetailData(string DOC_NO, string DOC_YEAR, string DOC_DT)
        {
            List<DeliveryJournalPostDetailData> DJPostDetailData = new List<DeliveryJournalPostDetailData>();

            IDBContext db = DbContext;

            try
            {
                DJPostDetailData = db.Fetch<DeliveryJournalPostDetailData>("DLV_Journal_DownloadDetailData", new object[] { DOC_NO });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return DJPostDetailData;
        }

        private List<DeliveryJournalPostDetailData> RetrieveJournalPostDetailData(string DOC_NO)
        {
            List<DeliveryJournalPostDetailData> DJPostDetailData = new List<DeliveryJournalPostDetailData>();

            IDBContext db = DbContext;

            try
            {
                DJPostDetailData = db.Fetch<DeliveryJournalPostDetailData>("DLV_Journal_PostDetailData", new object[] { DOC_NO });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return DJPostDetailData;
        }

        private List<DeliveryJournalTransactionData> RetrieveJournalTransData(string DOC_NO, string DOC_DT_FROM, string DOC_DT_TO, string DOC_TYPE, string DOC_STATUS)
        {
            List<DeliveryJournalTransactionData> DJTransData = new List<DeliveryJournalTransactionData>();

            IDBContext db = DbContext;

            try
            {
                DJTransData = db.Fetch<DeliveryJournalTransactionData>("DLV_Journal_TransData", new object[] { DOC_NO, DOC_DT_FROM, DOC_DT_TO, DOC_TYPE, DOC_STATUS });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return DJTransData;
        }

        private List<DeliveryJournalTransactionData> RetrieveJournalTransData(string text)
        {
            List<DeliveryJournalTransactionData> DJTransData = new List<DeliveryJournalTransactionData>();

            IDBContext db = DbContext;

            try
            {
                DJTransData = db.Fetch<DeliveryJournalTransactionData>("DLV_Journal_DownloadTransData", new object[] { text });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return DJTransData;
        }
    }
}
