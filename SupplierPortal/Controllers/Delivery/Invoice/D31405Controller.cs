﻿using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Data;
using System;
using System.Collections.Generic;
using Portal.Models;
using Portal.Models.InvoiceAndPaymentInquiry;
using Portal.Models.Supplier;
using Portal.Models.Globals;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Compression;
using System.IO;
using System.Data.SqlClient;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using Toyota.Common.Util.Text;
using System.Text;
using System.Reflection;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Credential;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;

using Portal.Models.Globals;
using Portal.Models.Delivery.Invoice;

using System.Text.RegularExpressions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

using DocumentFormat.OpenXml;

namespace Portal.Controllers.Delivery.Invoice
{
    public class D31405Controller : BaseController
    {
        //
        // GET: /D31405/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);


        public D31405Controller()
            : base("Delivery Invoice Inquiry")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryInvoiceAndPaymentModel mdl =  new DeliveryInvoiceAndPaymentModel();            

            //ViewData["LogisticPartnerLookup"] = GetLogisticPartner();
            ViewData["TermPaymentLookup"] = GetTermofPayment();
            ViewData["PaymentMethodLookup"] = GetPaymentMethod();
            ViewData["WithholdingTaxLookup"] = GetWithholdingTax();
            ViewData["GLAccountLookup"] = GetGLAccount();
            ViewData["TaxLookup"] = GetTax();
            ViewData["PartnerBankLookup"] = GetPartnerBank();
            ViewData["StatusLookup"] = GetStatus();

            Model.AddModel(mdl);
        }



        //HEADER
        public ActionResult PartialHeader()
        {
            #region Required model for [lp lookup]
            List<DeliveryInvoiceDataLogPartner> trucking = Model.GetModel<User>().FilteringArea<DeliveryInvoiceDataLogPartner>(GetAllLP(), "LP_CD", "D31405", "LogisticPartnerOption");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            Model.AddModel(_LPModel);
            #endregion

            return PartialView("IndexHeader", Model);
        }
        //GRID DATA

        List<DeliveryInvoiceAndPaymentData> _deliveryInvoiceAndPaymentDataModel = null;
        private List<DeliveryInvoiceAndPaymentData> _DeliveryInvoiceAndPaymentDataModel
        {
            get
            {
                if (_deliveryInvoiceAndPaymentDataModel == null)
                    _deliveryInvoiceAndPaymentDataModel = new List<DeliveryInvoiceAndPaymentData>();

                return _deliveryInvoiceAndPaymentDataModel;
            }
            set
            {
                _deliveryInvoiceAndPaymentDataModel = value;
            }
        }

        #region PartialListData
        public ActionResult PartialListData()
        {

            //string userinput = Regex.Replace(vPR_NO, "[^A-Za-z0-9$]", "");
            
            string CREATED_DT_FROM = String.IsNullOrEmpty(Request.Params["CREATED_DT_FROM"]) ? "" : Request.Params["CREATED_DT_FROM"];
            string CREATED_DT_TO = String.IsNullOrEmpty(Request.Params["CREATED_DT_TO"]) ? "" : Request.Params["CREATED_DT_TO"];
            string INVOICE_DT_FROM = String.IsNullOrEmpty(Request.Params["INVOICE_DT_FROM"]) ? "" : Request.Params["INVOICE_DT_FROM"];
            string INVOICE_DT_TO = String.IsNullOrEmpty(Request.Params["INVOICE_DT_TO"]) ? "" : Request.Params["INVOICE_DT_TO"];
            string PAYMENT_DT_FROM = String.IsNullOrEmpty(Request.Params["PAYMENT_DT_FROM"]) ? "" : Request.Params["PAYMENT_DT_FROM"];
            string PAYMENT_DT_TO = String.IsNullOrEmpty(Request.Params["PAYMENT_DT_TO"]) ? "" : Request.Params["PAYMENT_DT_TO"];
            string LP_CD = String.IsNullOrEmpty(Request.Params["LP_CD"]) ? "" : Request.Params["LP_CD"];
            string StatusOption = String.IsNullOrEmpty(Request.Params["StatusOption"]) ? "" : Request.Params["StatusOption"];
            string DELIVERY_NO = Regex.Replace(String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"], "[^A-Za-z0-9$]", "");
            string INVOICE_NO = Regex.Replace(String.IsNullOrEmpty(Request.Params["INVOICE_NO"]) ? "" : Request.Params["INVOICE_NO"], "[^A-Za-z0-9$]", "");

            DeliveryInvoiceAndPaymentModel mdl = Model.GetModel<DeliveryInvoiceAndPaymentModel>();
            //mdl.DeliveryInvoiceAndPaymentListData = GetDLVInvoiceAndPayData(LP_CD, CREATED_DT_FROM, CREATED_DT_TO, INVOICE_DT_FROM, INVOICE_DT_TO, PAYMENT_DT_FROM, PAYMENT_DT_TO, StatusOption, DELIVERY_NO, INVOICE_NO);

            _deliveryInvoiceAndPaymentDataModel = GetDLVInvoiceAndPayData(LP_CD, CREATED_DT_FROM, CREATED_DT_TO, INVOICE_DT_FROM, INVOICE_DT_TO, PAYMENT_DT_FROM, PAYMENT_DT_TO, StatusOption, DELIVERY_NO, INVOICE_NO);
            Model.AddModel(_deliveryInvoiceAndPaymentDataModel);

            if (Request.Params["visible"] != null)
            {
                if (Boolean.Parse(Request.Params["visible"]) == true)
                {
                    ViewBag.BooleanValue = false;
                }
                else
                {
                    ViewBag.BooleanValue = true;
                }
            }
            
            //Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }
        #endregion

        #region GetDLVInvoiceAndPayData
        private List<DeliveryInvoiceAndPaymentData> GetDLVInvoiceAndPayData(string LP_CD, string CREATED_DT_FROM, string CREATED_DT_TO, string INVOICE_DT_FROM, string INVOICE_DT_TO, string PAYMENT_DT_FROM, string PAYMENT_DT_TO, string StatusOption, string DELIVERY_NO, string INVOICE_NO)
        {

            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(StatusOption))
            {
                string[] statusList = StatusOption.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            string createDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(CREATED_DT_FROM))
            {
                string d = CREATED_DT_FROM.Substring(0, 2);
                string m = CREATED_DT_FROM.Substring(3, 2);
                string y = CREATED_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                createDateFromParameter += "'" +tgl + "'";
                //createDateFromParameter += tgl;
            }

            string createDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(CREATED_DT_TO))
            {
                string d = CREATED_DT_TO.Substring(0, 2);
                string m = CREATED_DT_TO.Substring(3, 2);
                string y = CREATED_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                createDateToParameter += "'" + tgl + "'";
                //createDateToParameter +=tgl;
            }

            string invoiceDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(INVOICE_DT_FROM))
            {
                string d = INVOICE_DT_FROM.Substring(0, 2);
                string m = INVOICE_DT_FROM.Substring(3, 2);
                string y = INVOICE_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                invoiceDateFromParameter += "'" + tgl + "'";
                //invoiceDateFromParameter +=tgl;
            }

            string invoiceDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(INVOICE_DT_TO))
            {
                string d = INVOICE_DT_TO.Substring(0, 2);
                string m = INVOICE_DT_TO.Substring(3, 2);
                string y = INVOICE_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                invoiceDateToParameter += "'" + tgl + "'";
                //invoiceDateToParameter += tgl;
            }

            string paymentDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(PAYMENT_DT_FROM))
            {
                string d = PAYMENT_DT_FROM.Substring(0, 2);
                string m = PAYMENT_DT_FROM.Substring(3, 2);
                string y = PAYMENT_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                paymentDateFromParameter += "'" + tgl + "'";
                //paymentDateFromParameter += tgl;
            }

            string paymentDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(PAYMENT_DT_TO))
            {
                string d = PAYMENT_DT_TO.Substring(0, 2);
                string m = PAYMENT_DT_TO.Substring(3, 2);
                string y = PAYMENT_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                paymentDateToParameter += "'" + tgl + "'";
                //paymentDateToParameter += tgl;
            }

            List<DeliveryInvoiceAndPaymentData> listOfDLV_INV_andPay = db.Fetch<DeliveryInvoiceAndPaymentData>("DLV_INV_getInvoiceAndPayGrid", new object[] { 
                logisticCDParameter,
                createDateFromParameter,
                createDateToParameter,
                invoiceDateFromParameter,
                invoiceDateToParameter,
                paymentDateFromParameter,
                paymentDateToParameter,
                statusParameter,
                DELIVERY_NO,
                INVOICE_NO
            });

            return listOfDLV_INV_andPay;
        }
        #endregion

        public ActionResult PartialListDetailData()
        {
            string INVOICE_NO = String.IsNullOrEmpty(Request.Params["InvoiceNo"]) ? "" : Request.Params["InvoiceNo"];
            DeliveryInvoiceAndPaymentModel mdl = Model.GetModel<DeliveryInvoiceAndPaymentModel>();
            mdl.DeliveryInvoiceAndPaymentListDetailData = GetDLVInvoiceAndPayDetailData(INVOICE_NO);
            Model.AddModel(mdl);
            return PartialView("GridDetail", Model);
        }


        protected List<DeliveryInvoiceAndPaymentDetailData> GetDLVInvoiceAndPayDetailData(string INVOICE_NO)
        {

            List<DeliveryInvoiceAndPaymentDetailData> listOfDLV_INV_andPay = db.Fetch<DeliveryInvoiceAndPaymentDetailData>("DLV_INV_getInvoiceAndPayGridDetail", new object[] { 
                INVOICE_NO
            });
            return listOfDLV_INV_andPay;
        }


        #region cancel invoice

        private String GetVendorCodeFromUsername()
        {
            String[] splittedUsername = AuthorizedUser.Username.Split('.');
            String vendorCode = AuthorizedUser.Username.Substring(0, 4);
            if (splittedUsername.Length > 0)
                vendorCode = splittedUsername[0];

            return vendorCode;
        }

        public ContentResult performCancel(string invoice_no)
        {

            int isError = 0;
            string errorMessage = "";
            string processID = "";

            //string GridId = invoice_no;
            //string GridIdUpdate = "'" + invoice_no + "'";
             
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            
            //12.06.2014 10.03
            //try
            //{
            //    db.Execute("DLV_INV_performCancel", new object[] {
            //        invoice_no, AuthorizedUser.Username
            //    });
            //    errorMessage = "Invoice cancel process has been finished.";
            //}
            //catch (Exception exc)
            //{
            //    isError = 1;
            //    errorMessage = exc.Message;
            //}

            try
            {
                processID = db.ExecuteScalar<string>("DLV_GenProcessID", new object[] { 
                                "Cancel Invoice Starting",
                                AuthorizedUser.Username, 
                                "DeliveryInvoiceInquiry.CancelInvoice",
                                "",
                                "MPCS00002INF", "INF", "3", "31405",     // function
                                "0"      
                            });

                db.Execute("DLV_INV_performCancel", new object[] {
                    invoice_no, AuthorizedUser.Username, processID
                });
                errorMessage = "Invoice cancel process has been finished.";
            }
            catch (Exception exc)
            {
                db.Execute("DLV_Update_Queue", new object[] { processID, "QU4" });
                isError = 1;
                errorMessage = exc.Message;
                //errorMessage = "You are not authorized to perform this action.";
            }

            db.Close();
            db.Dispose();
            
            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }
        #endregion

        #region reverse invoice
        public ContentResult performReverse(string invoice_no)
        {

            int isError = 0;
            string errorMessage = "";
            string processID = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                processID = db.ExecuteScalar<string>("DLV_GenProcessID", new object[] { 
                                "Reverse Invoice Starting",
                                AuthorizedUser.Username, 
                                "DeliveryInvoiceInquiry.ReverseInvoice",
                                "",
                                "MPCS00002INF", "INF", "3", "31405",     // function
                                "0"      
                            });

                db.Execute("DLV_INV_performReverse", new object[] {
                    invoice_no, AuthorizedUser.Username, processID
                });
                errorMessage = "Invoice reverse process has been finished.";
            }
            catch (Exception exc)
            {
                db.Execute("DLV_Update_Queue", new object[] { processID, "QU4" });

                isError = 1;
                errorMessage = exc.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }
        #endregion


        #region posting And Save invoice

        public JsonResult GetPostingDetail(string SupplierInvoiceNo)
        {
            List<DeliveryInvoiceAndPaymentData> InvoiceAndPaymentInquiryInfo = new List<DeliveryInvoiceAndPaymentData>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                InvoiceAndPaymentInquiryInfo = db.Query<DeliveryInvoiceAndPaymentData>("DLV_INV_getInvoiceAndPayPosting", new object[] { SupplierInvoiceNo }).ToList();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally { db.Close(); }

            return Json(InvoiceAndPaymentInquiryInfo);
        }
        public ContentResult SavePosting(string INV_NO, string PARTNER_BANK, string BASELINE_DATE, string TAX_CODE_2, string TERM_PAYMENT_2, string PAYMENT_METHOD_2, string INVOICE_NOTE)
        {
            string BaseLineDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(BASELINE_DATE))
            {
                string d = BASELINE_DATE.Substring(0, 2);
                string m = BASELINE_DATE.Substring(3, 2);
                string y = BASELINE_DATE.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                BaseLineDateParameter += "" + tgl + "";
            }

            
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int isError = 0;
            string errorMessage = "";

            try
            {
                db.Execute("DLV_INV_SAVE_POSTING", new object[] {
                    INV_NO, PARTNER_BANK, BaseLineDateParameter, TAX_CODE_2, TERM_PAYMENT_2, PAYMENT_METHOD_2, INVOICE_NOTE, AuthorizedUser.Username
                });
                errorMessage = "Invoice Has Been Saved";

            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = exc.Message;
            }

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }
        public ContentResult performPosting(string INV_NO, string PARTNER_BANK, string POSTING_DATE, string BASELINE_DATE, string TAX_CODE_2, string TERM_PAYMENT_2, string PAYMENT_METHOD_2, string INVOICE_NOTE)
        {

            int isError = 0;
            string errorMessage = "";
            string processID = "";

            string PostingDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(BASELINE_DATE))
            {
                string d = POSTING_DATE.Substring(0, 2);
                string m = POSTING_DATE.Substring(3, 2);
                string y = POSTING_DATE.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                PostingDateParameter += "" + tgl + "";
            }

            string BaseLineDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(BASELINE_DATE))
            {
                string d = BASELINE_DATE.Substring(0, 2);
                string m = BASELINE_DATE.Substring(3, 2);
                string y = BASELINE_DATE.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                BaseLineDateParameter += "" + tgl + "";
            }



            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                processID = db.ExecuteScalar<string>("DLV_GenProcessID", new object[] { 
                                "Posting Invoice Starting",
                                AuthorizedUser.Username, 
                                "DeliveryInvoiceInquiry.PostingInvoice",
                                "",
                                "MPCS00002INF", "INF", "3", "31405",     // function
                                "0"      
                            });

                db.Execute("DLV_INV_performPosting", new object[] {
                    INV_NO, PARTNER_BANK, PostingDateParameter, BaseLineDateParameter, TAX_CODE_2, TERM_PAYMENT_2, PAYMENT_METHOD_2, INVOICE_NOTE, AuthorizedUser.Username, processID
                });
                errorMessage = "Invoice posting process has been started.";
            }
            catch (Exception exc)
            {
                db.Execute("DLV_Update_Queue", new object[] { processID, "QU4" });
                isError = 1;
                errorMessage = exc.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }
        #endregion

        #region checkProgressPosting
        public ContentResult checkProgressPosting()
        {
            int isError = 0;
            string message = "";

            try
            {
                isError = 0;
                var QLog = db.SingleOrDefault<CheckProgress>("DLV_INV_CheckMsgPosting", new object[] { });

                message = QLog.messageLog;
                

            }
            catch (Exception exc)
            {
                isError = 1;
                message = "Checking process error.|End";
                //message = "Error";
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + message);
        }
        #endregion

        #region LOOKUP
        //GETTING Logistic partner FOR LOOKUP

        public ActionResult LogisticPartnerLookup()
        {

            TempData["GridName"] = "LogisticPartnerOption";
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            return PartialView("PG", ViewData["LogisticPartnerLookup"]);
        }
        public ActionResult LPLookup()
        {

            TempData["GridName"] = "LP_Option";
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            return PartialView("PG", ViewData["LogisticPartnerLookup"]);
        }
        public List<DeliveryInvoiceDataLogPartner> GetLogisticPartner()
        {
            List<DeliveryInvoiceDataLogPartner> listLogisticPartner = new List<DeliveryInvoiceDataLogPartner>();
            try
            {
                listLogisticPartner = db.Fetch<DeliveryInvoiceDataLogPartner>("DLV_getLP", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listLogisticPartner;
        }

        //GETTING Status FOR LOOKUP

        public ActionResult StatusLookup()
        {

            TempData["GridName"] = "StatusOption";
            ViewData["StatusLookup"] = GetStatus();

            return PartialView("PG", ViewData["StatusLookup"]);
        }

        public List<DeliveryInvoiceDataStatus> GetStatus()
        {
            List<DeliveryInvoiceDataStatus> listStatus = new List<DeliveryInvoiceDataStatus>();
            try
            {
                listStatus = db.Fetch<DeliveryInvoiceDataStatus>("DLV_INV_getStatus", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listStatus;
        }

        //GETTING Term of Payment FOR LOOKUP

        public ActionResult TermPaymentLookup()
        {

            TempData["GridName"] = "PayTermOption";
            ViewData["TermPaymentLookup"] = GetTermofPayment();

            return PartialView("PG", ViewData["TermPaymentLookup"]);
        }

        public List<DeliveryInvoiceDataTermofPayment> GetTermofPayment()
        {
            List<DeliveryInvoiceDataTermofPayment> listTermofPayment = new List<DeliveryInvoiceDataTermofPayment>();
            try
            {
                listTermofPayment = db.Fetch<DeliveryInvoiceDataTermofPayment>("DLV_INV_getTermofPayment", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listTermofPayment;
        }

        //GETTING Payment Method FOR LOOKUP

        public ActionResult PaymentMethodLookup()
        {

            TempData["GridName"] = "PayMethodOption";
            ViewData["PaymentMethodLookup"] = GetPaymentMethod();

            return PartialView("PG", ViewData["PaymentMethodLookup"]);
        }

        public List<DeliveryInvoiceDataPaymentMethod> GetPaymentMethod()
        {
            List<DeliveryInvoiceDataPaymentMethod> listPaymentMethod = new List<DeliveryInvoiceDataPaymentMethod>();
            try
            {
                listPaymentMethod = db.Fetch<DeliveryInvoiceDataPaymentMethod>("DLV_INV_getPaymentMethod", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listPaymentMethod;
        }

        //GETTING Withholding Tax FOR LOOKUP

        public ActionResult WithholdingTaxLookup()
        {

            TempData["GridName"] = "WithholdingTaxOption";
            ViewData["WithholdingTaxLookup"] = GetWithholdingTax();

            return PartialView("PG", ViewData["WithholdingTaxLookup"]);
        }

        public List<DeliveryInvoiceDataWithholdingTax> GetWithholdingTax()
        {
            List<DeliveryInvoiceDataWithholdingTax> listWithholdingTax = new List<DeliveryInvoiceDataWithholdingTax>();
            try
            {
                listWithholdingTax = db.Fetch<DeliveryInvoiceDataWithholdingTax>("DLV_INV_getWithholdingTax", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listWithholdingTax;
        }

        //GETTING GL Account FOR LOOKUP

        public ActionResult GLAccountLookup()
        {

            TempData["GridName"] = "GLAccountOption";
            ViewData["GLAccountLookup"] = GetGLAccount();

            return PartialView("PG", ViewData["GLAccountLookup"]);
        }

        public List<DeliveryInvoiceDataGLAccount> GetGLAccount()
        {
            List<DeliveryInvoiceDataGLAccount> listGLAccount = new List<DeliveryInvoiceDataGLAccount>();
            try
            {
                listGLAccount = db.Fetch<DeliveryInvoiceDataGLAccount>("DLV_INV_getGLAccount", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listGLAccount;
        }

        //GETTING Tax FOR LOOKUP

        public ActionResult TaxLookup()
        {

            TempData["GridName"] = "TaxOption";
            ViewData["TaxLookup"] = GetTax();

            return PartialView("PG", ViewData["TaxLookup"]);
        }

        public List<DeliveryInvoiceDataTax> GetTax()
        {
            List<DeliveryInvoiceDataTax> listTax = new List<DeliveryInvoiceDataTax>();
            try
            {
                listTax = db.Fetch<DeliveryInvoiceDataTax>("DLV_INV_getTax", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listTax;
        }

        //GETTING Partner Bank FOR LOOKUP

        public ActionResult PartnerBankLookup()
        {

            TempData["GridName"] = "PartnerBankOption";
            ViewData["PartnerBankLookup"] = GetPartnerBank();

            return PartialView("PG", ViewData["PartnerBankLookup"]);
        }

        public List<DeliveryinvoiceDataPartnerBank> GetPartnerBank()
        {
            List<DeliveryinvoiceDataPartnerBank> listPartnerBank = new List<DeliveryinvoiceDataPartnerBank>();
            try
            {
                listPartnerBank = db.Fetch<DeliveryinvoiceDataPartnerBank>("DLV_INV_getPartnerBank", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listPartnerBank;
        }

        #endregion

        //GETTING MESSAGE BOX
        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }


        #region GetDLVInvoiceAndPayDataDownload CSV
        //private List<DeliveryInvoiceAndPaymentData> GetDLVInvoiceAndPayDataDownload(string LP_CD, string CREATED_DT_FROM, string CREATED_DT_TO, string INVOICE_DT_FROM,
        //    string INVOICE_DT_TO, string PAYMENT_DT_FROM, string PAYMENT_DT_TO, string StatusOption, string DELIVERY_NO, string INVOICE_NO,
        //    string inv_no,
        //    string inv_dt,
        //    string curr,
        //    string total_amt,
        //    string amt_before,
        //    string tax_amt,
        //    string status,
        //    string lp_cd2,
        //    string lp_name,
        //    string plan,
        //    string actual,
        //    string progress,
        //    string createdby,
        //    string createddt
        //    )
        //{

        //    #region split

        //    string logisticCDParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(LP_CD))
        //    {
        //        string[] logisticCDList = LP_CD.Split(';');
        //        foreach (string r in logisticCDList)
        //        {
        //            logisticCDParameter += "'" + r + "',";
        //        }
        //        logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
        //    }

        //    string statusParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(StatusOption))
        //    {
        //        string[] statusList = StatusOption.Split(';');
        //        foreach (string r in statusList)
        //        {
        //            statusParameter += "'" + r + "',";
        //        }
        //        statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
        //    }

        //    string createDateFromParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(CREATED_DT_FROM))
        //    {
        //        string d = CREATED_DT_FROM.Substring(0, 2);
        //        string m = CREATED_DT_FROM.Substring(3, 2);
        //        string y = CREATED_DT_FROM.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        createDateFromParameter += "'" + tgl + "'";
        //        //createDateFromParameter += tgl;
        //    }

        //    string createDateToParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(CREATED_DT_TO))
        //    {
        //        string d = CREATED_DT_TO.Substring(0, 2);
        //        string m = CREATED_DT_TO.Substring(3, 2);
        //        string y = CREATED_DT_TO.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        createDateToParameter += "'" + tgl + "'";
        //        //createDateToParameter +=tgl;
        //    }

        //    string invoiceDateFromParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(INVOICE_DT_FROM))
        //    {
        //        string d = INVOICE_DT_FROM.Substring(0, 2);
        //        string m = INVOICE_DT_FROM.Substring(3, 2);
        //        string y = INVOICE_DT_FROM.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        invoiceDateFromParameter += "'" + tgl + "'";
        //        //invoiceDateFromParameter +=tgl;
        //    }

        //    string invoiceDateToParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(INVOICE_DT_TO))
        //    {
        //        string d = INVOICE_DT_TO.Substring(0, 2);
        //        string m = INVOICE_DT_TO.Substring(3, 2);
        //        string y = INVOICE_DT_TO.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        invoiceDateToParameter += "'" + tgl + "'";
        //        //invoiceDateToParameter += tgl;
        //    }

        //    string paymentDateFromParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(PAYMENT_DT_FROM))
        //    {
        //        string d = PAYMENT_DT_FROM.Substring(0, 2);
        //        string m = PAYMENT_DT_FROM.Substring(3, 2);
        //        string y = PAYMENT_DT_FROM.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        paymentDateFromParameter += "'" + tgl + "'";
        //        //paymentDateFromParameter += tgl;
        //    }

        //    string paymentDateToParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(PAYMENT_DT_TO))
        //    {
        //        string d = PAYMENT_DT_TO.Substring(0, 2);
        //        string m = PAYMENT_DT_TO.Substring(3, 2);
        //        string y = PAYMENT_DT_TO.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        paymentDateToParameter += "'" + tgl + "'";
        //        //paymentDateToParameter += tgl;
        //    }

        //    #endregion

        //    List<DeliveryInvoiceAndPaymentData> listOfDLV_INV_andPay = db.Fetch<DeliveryInvoiceAndPaymentData>("DLV_INV_getInvoiceAndPayGridDownload", new object[] { 
        //        logisticCDParameter,
        //        createDateFromParameter,
        //        createDateToParameter,
        //        invoiceDateFromParameter,
        //        invoiceDateToParameter,
        //        paymentDateFromParameter,
        //        paymentDateToParameter,
        //        statusParameter,
        //        DELIVERY_NO,
        //        INVOICE_NO,

        //        inv_no, inv_dt, curr, total_amt, amt_before, tax_amt, status, lp_cd2, lp_name, plan, actual,
        //        progress, createdby, createddt
        //    });

        //    return listOfDLV_INV_andPay;
        //}
        #endregion

        #region checkDownloadData
        public ContentResult checkDownloadData(string LP_CD,
            string CREATED_DT_FROM,
            string CREATED_DT_TO,
            string INVOICE_DT_FROM,
            string INVOICE_DT_TO,
            string PAYMENT_DT_FROM,
            string PAYMENT_DT_TO,
            string StatusOption,
            string DELIVERY_NO,
            string INVOICE_NO,
            
            string inv_no,
            string inv_dt,
            string curr,
            string total_amt,
            string amt_before,
            string tax_amt,
            string status,
            //string lp_cd2,
            //string lp_name,
            string plan,
            string actual,
            string progress,
            string createdby,
            string createddt
            )
        {

            int isError = 0;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            
            DeliveryInvoiceAndPaymentModel mdl = Model.GetModel<DeliveryInvoiceAndPaymentModel>();
            mdl.DeliveryInvoiceAndPaymentListData = GetDLVInvoiceAndPayDataDownload(LP_CD, CREATED_DT_FROM, CREATED_DT_TO,
                INVOICE_DT_FROM, INVOICE_DT_TO, PAYMENT_DT_FROM, PAYMENT_DT_TO, StatusOption, DELIVERY_NO, INVOICE_NO,
                 inv_no, inv_dt, curr, total_amt, amt_before, tax_amt, status, 
                //lp_cd2, lp_name, 
                plan, actual,
             progress, createdby, createddt);


            var mc = mdl.DeliveryInvoiceAndPaymentListData.ToList<DeliveryInvoiceAndPaymentData>();
            int? totals = mc.Count;

            return Content(((isError == 1) ? "Error|" : "Success|") + totals);
        }
        #endregion


        #region DownloadCSV

        public void WriteCSV<T>(IEnumerable<T> items, Stream stream)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(string.Join("; ", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join("; ", props.Select(p => p.GetValue(item, null))));
                }
            }
        }

        public FileContentResult DownloadData(
            string LP_CD,
            string CREATED_DT_FROM,
            string CREATED_DT_TO,
            string INVOICE_DT_FROM,
            string INVOICE_DT_TO,
            string PAYMENT_DT_FROM,
            string PAYMENT_DT_TO,
            string StatusOption,
            string DELIVERY_NO,
            string INVOICE_NO,

            string inv_no,
            string inv_dt,
            string curr,
            string total_amt,
            string amt_before,
            string tax_amt,
            string status,
            //string lp_cd2,
            //string lp_name,
            string plan,
            string actual,
            string progress,
            string createdby,
            string createddt)
        {
            Stream output = new MemoryStream();
            string fileName = "InvoiceAndPayment.csv";
            List<DeliveryInvoiceAndPaymentData> lstInvoiceAndPayment = new List<DeliveryInvoiceAndPaymentData>();
            byte[] documentBytes;

            //DeliveryInvoiceAndPaymentModel mdl = Model.GetModel<DeliveryInvoiceAndPaymentModel>();
            //var mc = mdl.DeliveryInvoiceAndPaymentListData.ToList<DeliveryInvoiceAndPaymentData>();
            //int? totals = mc.Count;

            //string successStatus = "";
            //if (totals == 0)
            //{
            //    successStatus = "False";
            //    var excelLink = "Data not found!";
            //    return Json(new { success = successStatus }, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
                try
                {
                    lstInvoiceAndPayment = GetDLVInvoiceAndPayDataDownload(LP_CD, CREATED_DT_FROM, CREATED_DT_TO, INVOICE_DT_FROM, INVOICE_DT_TO, PAYMENT_DT_FROM, PAYMENT_DT_TO, StatusOption, DELIVERY_NO, INVOICE_NO,
                        inv_no, inv_dt, curr, total_amt, amt_before, tax_amt, status, 
                        //lp_cd2, lp_name, 
                        plan, actual,
                        progress, createdby, createddt
                        );

                    using (MemoryStream buffer = new MemoryStream())
                    {
                        WriteCSV(lstInvoiceAndPayment, buffer);
                        documentBytes = buffer.GetBuffer();
                    }

                    return File(documentBytes, "text/csv", fileName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            //}
        }
        #endregion

        #region DownloadDeatilCSV

        //public void WriteDetailCSV<T>(IEnumerable<T> items, Stream stream)
        //{
        //    Type itemType = typeof(T);
        //    var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

        //    using (var writer = new StreamWriter(stream))
        //    {
        //        writer.WriteLine(string.Join("; ", props.Select(p => p.Name)));

        //        foreach (var item in items)
        //        {
        //            writer.WriteLine(string.Join("; ", props.Select(p => p.GetValue(item, null))));
        //        }
        //    }
        //}

        //public FileContentResult DownloadDetailData(string INVOICE_NO)
        //{
        //    Stream output = new MemoryStream();
        //    string fileName = "InvoiceAndPaymentDetail.csv";
        //    List<DeliveryInvoiceAndPaymentDetailData> lstInvoiceAndPaymentDetail = new List<DeliveryInvoiceAndPaymentDetailData>();
        //    byte[] documentBytes;

        //    try
        //    {
        //        lstInvoiceAndPaymentDetail = GetDLVInvoiceAndPayDetailData(INVOICE_NO);

        //        using (MemoryStream buffer = new MemoryStream())
        //        {
        //            WriteDetailCSV(lstInvoiceAndPaymentDetail, buffer);
        //            documentBytes = buffer.GetBuffer();
        //        }

        //        return File(documentBytes, "text/csv", fileName);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion




        #region checkStatus
        public ContentResult checkStatus(string Grid)
        {

            int isError = 0;
            string errorMessage = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //db.Execute("DLV_INV_checkStatus", new object[] {
                //    Grid
                //});
                //errorMessage = "2";

                //---=== Check if exists on
                var QueryLog = db.SingleOrDefault<CheckResult>("DLV_INV_checkStatus", new object[] { 
                    Grid
                });

                if (QueryLog.result == "TRUE")
                {
                    isError = 0;
                    errorMessage = "true";
                }
                else
                {
                    isError = 0;
                    errorMessage = "false";
                }

            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = exc.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }
        #endregion

        #region print certificate
        public FileContentResult PrintCertificate(string Grid)
        {
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string fileName;

            //char[] splitChar = { ';' };
            //char[] SplitLastChar = { '|' };
            //string[] ParamUpdate;
            //string[] ParamUpdateId;

            //ParamUpdate = Grid.Split(splitChar);
            //int total = ParamUpdate.Length - 1;

            //string invoice_no = "";
            //string invoice_dt = "";
            //string lp_cd = "";

            try
            {

                //for (int i = 0; i < total; i++)
                //{
                //    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                //    invoice_no = ParamUpdateId[0].ToString();
                //    invoice_dt = ParamUpdateId[1].ToString();
                //    lp_cd = ParamUpdateId[2].ToString();
                //}

                //invoice_no = "TEST0001,test/02";
                //invoice_dt = "";
                string name = AuthorizedUser.Username;
                name = name.Substring(0, 3);
                String processID = "";
                //fileName = "InvoicePayment_" + invoice_no +".pdf"; ;
                fileName = name + "_InvoicePayment_"+ DateTime.Now.ToString("yyyyMMdd_HHmmss") +"_.pdf"; 
                processID = db.ExecuteScalar<string>(
                       "GenerateProcessId",
                       new object[] { 
                            "Print Certificate ID InvoicePayment.pdf is started",
                            //"Print Certificate ID InvoicePayment_" + invoice_no + ".pdf is started", 
                            AuthorizedUser.Username, 
                            "DeliveryInvoiceAndPaymentInquiry.PrintCertificateID",
                            "",
                            "MPCS00002INF", "INF", "3", "31402",     // function
                            "0"      // sts
                        });
                documentBytes = PrintCertificateReport(Grid);
                processID = db.ExecuteScalar<string>(
                       "GenerateProcessId",
                       new object[] { 
                            "Print Certificate ID InvoicePayment.pdf is finished", 
                            //"Print Certificate ID InvoicePayment_" + invoice_no  + ".pdf is finished", 
                            AuthorizedUser.Username, 
                            "DeliveryInvoiceAndPaymentInquiry.PrintCertificateID",
                            processID,
                            "MPCS00003INF", "INF", "3", "31402",     // function
                            "0"      // sts
                        });

                return File(documentBytes, "application/pdf", fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }





        private Telerik.Reporting.Report CreateReport(string reportPath,
            string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }

        private byte[] PrintCertificateReport(string Grid)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            Telerik.Reporting.ReportParameterCollection reportParameters = new Telerik.Reporting.ReportParameterCollection();

            //List<string> lstInvoiceDatesDt = new List<string>();
            //string resultInvoiceDates = "";
            //lstInvoiceDatesDt = invoice_dt.Split(',').ToList<string>();

            //foreach (string s in lstInvoiceDatesDt)
            //{
            //    DateTime InvoiceDatesDt = (s == "" || s == null) ? DateTime.ParseExact("01.01.1900", "dd.MM.yyyy", null) : DateTime.ParseExact(s, "dd.MM.yyyy", null);
            //    resultInvoiceDates += InvoiceDatesDt.ToString("yyyyMMdd") + ",";
            //}


            parameters.Add("@Grid", System.Data.DbType.String, Grid );
            //parameters.Add("@InvoiceNos", System.Data.DbType.String, invoice_no);
            //parameters.Add("@InvoiceDates", System.Data.DbType.String, invoice_dt);
            //parameters.Add("@lp_codes", System.Data.DbType.String, lp_cd);

            reportParameters.Add("UserName", Telerik.Reporting.ReportParameterType.String, AuthorizedUser.Username);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\Delivery\RepDeliveryInvoice_Certificate.trdx", "SP_DLV_INV_CertificatePrint", parameters);
            rpt.ReportParameters.AddRange(reportParameters);

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);

            return result.DocumentBytes;
        }


        #endregion


        #region Export To Excel

        protected List<DeliveryInvoiceAndPaymentData> GetDLVInvoiceAndPayDataDownload(string LP_CD, string CREATED_DT_FROM, string CREATED_DT_TO, string INVOICE_DT_FROM,
            string INVOICE_DT_TO, string PAYMENT_DT_FROM, string PAYMENT_DT_TO, string StatusOption, string DELIVERY_NO, string INVOICE_NO,
            string inv_no,
            string inv_dt,
            string curr,
            string total_amt,
            string amt_before,
            string tax_amt,
            string status,
            //string lp_cd2,
            //string lp_name,
            string plan,
            string actual,
            string progress,
            string createdby,
            string createddt
            )
        {

            #region split

            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(StatusOption))
            {
                string[] statusList = StatusOption.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            string createDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(CREATED_DT_FROM))
            {
                string d = CREATED_DT_FROM.Substring(0, 2);
                string m = CREATED_DT_FROM.Substring(3, 2);
                string y = CREATED_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                createDateFromParameter += "'" + tgl + "'";
                //createDateFromParameter += tgl;
            }

            string createDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(CREATED_DT_TO))
            {
                string d = CREATED_DT_TO.Substring(0, 2);
                string m = CREATED_DT_TO.Substring(3, 2);
                string y = CREATED_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                createDateToParameter += "'" + tgl + "'";
                //createDateToParameter +=tgl;
            }

            string invoiceDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(INVOICE_DT_FROM))
            {
                string d = INVOICE_DT_FROM.Substring(0, 2);
                string m = INVOICE_DT_FROM.Substring(3, 2);
                string y = INVOICE_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                invoiceDateFromParameter += "'" + tgl + "'";
                //invoiceDateFromParameter +=tgl;
            }

            string invoiceDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(INVOICE_DT_TO))
            {
                string d = INVOICE_DT_TO.Substring(0, 2);
                string m = INVOICE_DT_TO.Substring(3, 2);
                string y = INVOICE_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                invoiceDateToParameter += "'" + tgl + "'";
                //invoiceDateToParameter += tgl;
            }

            string paymentDateFromParameter = string.Empty;
            if (!string.IsNullOrEmpty(PAYMENT_DT_FROM))
            {
                string d = PAYMENT_DT_FROM.Substring(0, 2);
                string m = PAYMENT_DT_FROM.Substring(3, 2);
                string y = PAYMENT_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                paymentDateFromParameter += "'" + tgl + "'";
                //paymentDateFromParameter += tgl;
            }

            string paymentDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(PAYMENT_DT_TO))
            {
                string d = PAYMENT_DT_TO.Substring(0, 2);
                string m = PAYMENT_DT_TO.Substring(3, 2);
                string y = PAYMENT_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                paymentDateToParameter += "'" + tgl + "'";
                //paymentDateToParameter += tgl;
            }

            #endregion


            List<DeliveryInvoiceAndPaymentData> listOfDLV_INV_andPay = db.Fetch<DeliveryInvoiceAndPaymentData>("DLV_INV_getInvoiceAndPayGridDownload", new object[] { 
                logisticCDParameter,
                createDateFromParameter,
                createDateToParameter,
                invoiceDateFromParameter,
                invoiceDateToParameter,
                paymentDateFromParameter,
                paymentDateToParameter,
                statusParameter,
                DELIVERY_NO,
                INVOICE_NO,

                inv_no, inv_dt, curr, total_amt, amt_before, tax_amt, status, 
                //lp_cd2, lp_name,
                plan, actual,
                progress, createdby, createddt
            });

            return listOfDLV_INV_andPay;

        }


        #region exportExcel
        public ActionResult exportToExcel(
            string LP_CD, 
            string CREATED_DT_FROM, 
            string CREATED_DT_TO, 
            string INVOICE_DT_FROM,
            string INVOICE_DT_TO, 
            string PAYMENT_DT_FROM, 
            string PAYMENT_DT_TO, 
            string StatusOption, 
            string DELIVERY_NO, 
            string INVOICE_NO,
            string inv_no,
            string inv_dt,
            string curr,
            string total_amt,
            string amt_before,
            string tax_amt,
            string status,
            string lp_cd2,
            string lp_name,
            string plan,
            string actual,
            string progress,
            string createdby,
            string createddt
        )
        {

            string successStatus = "";

            DeliveryInvoiceAndPaymentModel mdl = Model.GetModel<DeliveryInvoiceAndPaymentModel>();
            mdl.DeliveryInvoiceAndPaymentListData = GetDLVInvoiceAndPayDataDownload(
                LP_CD, 
                CREATED_DT_FROM, 
                CREATED_DT_TO,
                INVOICE_DT_FROM, 
                INVOICE_DT_TO, 
                PAYMENT_DT_FROM, 
                PAYMENT_DT_TO, 
                StatusOption, 
                DELIVERY_NO, 
                INVOICE_NO,
                inv_no, 
                inv_dt, 
                curr, 
                total_amt, 
                amt_before, 
                tax_amt, 
                status, 
                //lp_cd2, lp_name, 
                plan, 
                actual,
                progress, 
                createdby, 
                createddt);


            var mc = mdl.DeliveryInvoiceAndPaymentListData.ToList<DeliveryInvoiceAndPaymentData>();
            int? totals = mc.Count;


            if (totals == 0)
            {
                successStatus = "False";
                var excelLink = "Data not found!";
                return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                using (ExcelPackage p = new ExcelPackage())
                {
                    p.Workbook.Worksheets.Add("Delivery Invoice Inquiry");
                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                    ws.Name = "Delivery Invoice Inquiry";
                    ws.Cells.Style.Font.Size = 11;
                    ws.Cells.Style.Font.Name = "Calibri";
                    //header
                    string excelLink = "";
                    int columncount = 14;
                    //string successStatus = "";
                    ws.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA"; // Heading Name
                    ws.Cells[1, 1, 1, columncount].Merge = true; //Merge columns start and end range
                    ws.Cells[1, 1, 1, columncount].Style.Font.Bold = true; //Font should be bold
                    ws.Cells[1, 1, 1, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                    ws.Cells[2, 1].Value = "Delivery Invoice Inquiry"; // Heading Name
                    ws.Cells[2, 1, 2, columncount].Merge = true; //Merge columns start and end range
                    ws.Cells[2, 1, 2, columncount].Style.Font.Bold = true; //Font should be bold
                    ws.Cells[2, 1, 2, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                    //set header kolom
                    #region kolom

                    //ws.Cells[4, 1].AutoFitColumns();
                    ws.Cells.AutoFitColumns(20, 40);
                    ws.Cells[4, 1].Value = "Invoice No";
                    ws.Cells[4, 1].Style.Font.Bold = true;
                    ws.Cells[4, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 1, 5, 1].Merge = true;
                    ws.Cells[4, 1, 5, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 2].Value = "Invoice Date";
                    ws.Cells[4, 2].AutoFitColumns();
                    ws.Cells[4, 2].Style.Font.Bold = true;
                    ws.Cells[4, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 2, 5, 2].Merge = true;
                    ws.Cells[4, 2, 5, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 3].Value = "Currency";
                    ws.Cells[4, 3].AutoFitColumns();
                    ws.Cells[4, 3].Style.Font.Bold = true;
                    ws.Cells[4, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 3, 5, 3].Merge = true;
                    ws.Cells[4, 3, 5, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 4].Value = "Total Amount";
                    //ws.Cells[4, 2].AutoFitColumns();
                    ws.Cells[4, 4].Style.Font.Bold = true;
                    ws.Cells[4, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 4, 5, 4].Merge = true;
                    ws.Cells[4, 4, 5, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 5].Value = "Amount Before Tax";
                    //ws.Cells[4, 2].AutoFitColumns();
                    ws.Cells[4, 5].Style.Font.Bold = true;
                    ws.Cells[4, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 5, 5, 5].Merge = true;
                    ws.Cells[4, 5, 5, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 6].Value = "Tax Amount";
                    //ws.Cells[4, 2].AutoFitColumns();
                    ws.Cells[4, 6].Style.Font.Bold = true;
                    ws.Cells[4, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 6, 5, 6].Merge = true;
                    ws.Cells[4, 6, 5, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 7].Value = "Status";
                    //ws.Cells[4, 2].AutoFitColumns();
                    ws.Cells[4, 7].Style.Font.Bold = true;
                    ws.Cells[4, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 7, 5, 7].Merge = true;
                    ws.Cells[4, 7, 5, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 8].Value = "Trucking";
                    //ws.Cells[4, 2].AutoFitColumns();
                    ws.Cells[4, 8].Style.Font.Bold = true;
                    ws.Cells[4, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 8, 4, 9].Merge = true;
                    ws.Cells[4, 8, 4, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[5, 8].Value = "Code";
                    ws.Cells[5, 8].AutoFitColumns();
                    ws.Cells[5, 8].Style.Font.Bold = true;
                    ws.Cells[5, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[5, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                    ws.Cells[5, 9].Value = "Name";
                    ws.Cells[5, 9].AutoFitColumns(50, 160);
                    ws.Cells[5, 9].Style.Font.Bold = true;
                    ws.Cells[5, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[5, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                    ws.Cells[4, 10].Value = "Payment";
                    ws.Cells[4, 10].AutoFitColumns(20, 70);
                    ws.Cells[4, 10].Style.Font.Bold = true;
                    ws.Cells[4, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 10, 4, 11].Merge = true;
                    ws.Cells[4, 10, 4, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                    ws.Cells[5, 10].Value = "Plan";
                    ws.Cells[5, 10].AutoFitColumns(20, 70);
                    ws.Cells[5, 10].Style.Font.Bold = true;
                    ws.Cells[5, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[5, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                    ws.Cells[5, 11].Value = "Actual";
                    ws.Cells[5, 11].AutoFitColumns();
                    ws.Cells[5, 11].Style.Font.Bold = true;
                    ws.Cells[5, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[4, 12].Value = "Progress Status";
                    ws.Cells[4, 12].AutoFitColumns();
                    ws.Cells[4, 12].Style.Font.Bold = true;
                    ws.Cells[4, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 12].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 12, 5, 12].Merge = true;
                    ws.Cells[4, 12, 5, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 13].Value = "Created";
                    //ws.Cells[4, 9].AutoFitColumns();
                    ws.Cells[4, 13].Style.Font.Bold = true;
                    ws.Cells[4, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 13].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 13, 4, 14].Merge = true;
                    ws.Cells[4, 13, 4, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[5, 13].Value = "By";
                    ws.Cells[5, 13].AutoFitColumns(20, 100);
                    ws.Cells[5, 13].Style.Font.Bold = true;
                    ws.Cells[5, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 13].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[5, 14].Value = "Date";
                    ws.Cells[5, 14].AutoFitColumns(20, 70);
                    ws.Cells[5, 14].Style.Font.Bold = true;
                    ws.Cells[5, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 14].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    //ws.Cells[4, 11].Value = "Changed";
                    ////ws.Cells[4, 11].AutoFitColumns();
                    //ws.Cells[4, 11].Style.Font.Bold = true;
                    //ws.Cells[4, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //ws.Cells[4, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    //ws.Cells[4, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //ws.Cells[4, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    //ws.Cells[4, 11, 4, 12].Merge = true;
                    //ws.Cells[4, 11, 4, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    //ws.Cells[5, 11].Value = "By";
                    ////ws.Cells[5, 11].AutoFitColumns();
                    //ws.Cells[5, 11].Style.Font.Bold = true;
                    //ws.Cells[5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    //ws.Cells[5, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //ws.Cells[5, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    //ws.Cells[5, 12].Value = "Date";
                    ////ws.Cells[5, 12].AutoFitColumns();
                    //ws.Cells[5, 12].Style.Font.Bold = true;
                    //ws.Cells[5, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //ws.Cells[5, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    //ws.Cells[5, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //ws.Cells[5, 12].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    #endregion

                    #region detailnya

                    List<DeliveryInvoiceAndPaymentData> m = mdl.DeliveryInvoiceAndPaymentListData.ToList<DeliveryInvoiceAndPaymentData>();

                    int i = 0;
                    foreach (DeliveryInvoiceAndPaymentData x in m)
                    {


                        //string changedBy = m[i].CHANGED_DT.ToString();
                        //var changedBy1 = "";

                        //if (changedBy == "1/1/0001 12:00:00 AM")
                        //{
                        //    changedBy1 = "";
                        //}
                        //else
                        //{
                        //    changedBy1 = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CHANGED_DT);
                        //}

                        

                        ws.Cells[i + 6, 1].Value = m[i].INVOICE_NO;
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ws.Cells[i + 6, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 2].Value = string.Format("{0:dd.MM.yyyy}", m[i].INVOICE_DT);
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 3].Value = m[i].CURRENCY;
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        
                        ws.Cells[i + 6, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                        ws.Cells[i + 6, 4].Value = m[i].TOTAL_AMOUNT;
                        ws.Cells[i + 6, 4].Style.Numberformat.Format = "#,##";
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[i + 6, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        
                        ws.Cells[i + 6, 5].Value = m[i].AMOUNT_BEFORE;
                        ws.Cells[i + 6, 5].Style.Numberformat.Format = "#,##";
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[i + 6, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 6].Value = m[i].TAX_AMOUNT;
                        ws.Cells[i + 6, 6].Style.Numberformat.Format = "#,##";
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[i + 6, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 7].Value = m[i].STATUS;
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ws.Cells[i + 6, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 8].Value = m[i].LP_CD;
                        ws.Cells[i + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 5].AutoFitColumns();
                        ws.Cells[i + 6, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 9].Value = m[i].LP_NAME;
                        //ws.Cells[i + 6, 5].AutoFitColumns();
                        ws.Cells[i + 6, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 10].Value = string.Format("{0:dd.MM.yyyy}", m[i].PLAN);
                        ws.Cells[i + 6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 3].AutoFitColumns();
                        ws.Cells[i + 6, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 11].Value = string.Format("{0:dd.MM.yyyy}", m[i].ACTUAL);
                        ws.Cells[i + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 4].AutoFitColumns();
                        ws.Cells[i + 6, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                        ws.Cells[i + 6, 12].Value = m[i].PROGRESS_STATUS;
                        ws.Cells[i + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 13].Value = m[i].CREATED_BY;
                        //ws.Cells[i + 6, 7].AutoFitColumns();
                        ws.Cells[i + 6, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 14].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CREATED_DT);
                        //ws.Cells[i + 6, 8].AutoFitColumns();
                        ws.Cells[i + 6, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        //ws.Cells[i + 6, 11].Value = m[i].CHANGED_BY;
                        ////ws.Cells[i + 6, 7].AutoFitColumns();
                        //ws.Cells[i + 6, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);



                        //ws.Cells[i + 6, 12].Value = changedBy1;
                        ////ws.Cells[i + 6, 8].AutoFitColumns();
                        //ws.Cells[i + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        i++;
                    }

                    #endregion

                    try
                    {
                        Byte[] bin = p.GetAsByteArray();
                        
                        //string fn = "DeliveryPurchaseRequisition-" + Guid.NewGuid().ToString() + "-" + AuthorizedUser.Username + ".xlsx";
                        string fn = "DeliveryInvoiceInquiry-" + AuthorizedUser.Username + "_"+ DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                        string DownloadDirectory = "Content/Download/DeliveryPurchaseRequest/";
                        string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
                        var pathfile = Path.Combine(downdir, fn);

                        excelLink = "Content/Download/DeliveryPurchaseRequest/" + fn + "";
                        successStatus = "True";
                        System.IO.File.WriteAllBytes(pathfile, bin);


                    }
                    catch (Exception err)
                    {
                        //write log
                        successStatus = "False";
                    }
                    return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);


                }

            }

        }
        #endregion



        private void DownloadFile(string fname)
        {
            bool forceDownload = true;
            string path = fname;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            string type = "";
            // set known types based on file extension  
            if (ext != null)
            {
                switch (ext.ToLower())
                {

                    case ".xlsx":
                    case ".xls":
                        type = "Application/msexcel";
                        break;
                }
            }
            if (forceDownload)
            {
                Response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            if (type != "")
                Response.ContentType = type;
            Response.WriteFile(path);
            Response.End();
        }
        #endregion              


        #region Export To Excel - Detail grid


        protected List<DeliveryInvoiceAndPaymentDetailData> GetDLVInvoiceAndPayDataDetailDownload(string INVOICE_NO,
            string delivery_no, string route_cd, string route_name,string doc_dt, string qty, string price, string currency, string amount,string comp_price
            )
        {
            List<DeliveryInvoiceAndPaymentDetailData> listOfDLV_INV_andPay = db.Fetch<DeliveryInvoiceAndPaymentDetailData>("DLV_INV_getInvoiceAndPayGridDetailDownload", new object[] { 
                INVOICE_NO, delivery_no, route_cd, route_name,doc_dt, qty, price, currency, amount, comp_price
            });
            return listOfDLV_INV_andPay;
        }


        #region exportDetailExcel
        public ActionResult exportDetailToExcel(string INVOICE_NO,
            string delivery_no, string route_cd, string route_name, string doc_dt, string qty, string price, string currency, string amount, string comp_price
            )
        {

            string successStatus = "";

            DeliveryInvoiceAndPaymentModel mdl = Model.GetModel<DeliveryInvoiceAndPaymentModel>();
            mdl.DeliveryInvoiceAndPaymentListDetailData = GetDLVInvoiceAndPayDataDetailDownload(
                INVOICE_NO, delivery_no, route_cd, route_name, doc_dt, qty, price, currency, amount, comp_price
                );


            var mc = mdl.DeliveryInvoiceAndPaymentListDetailData.ToList<DeliveryInvoiceAndPaymentDetailData>();
            
            int? totals = mc.Count;


            if (totals == 0)
            {
                successStatus = "False";
                var excelLink = "Data not found!";
                return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                using (ExcelPackage p = new ExcelPackage())
                {
                    p.Workbook.Worksheets.Add("Delivery Detail Invoice Inquiry");
                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                    ws.Name = "Delivery Detail Invoice Inquiry";
                    ws.Cells.Style.Font.Size = 11;
                    ws.Cells.Style.Font.Name = "Calibri";
                    //header
                    string excelLink = "";
                    int columncount = 9;
                    //string successStatus = "";
                    ws.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA"; // Heading Name
                    ws.Cells[1, 1, 1, columncount].Merge = true; //Merge columns start and end range
                    ws.Cells[1, 1, 1, columncount].Style.Font.Bold = true; //Font should be bold
                    ws.Cells[1, 1, 1, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                    ws.Cells[2, 1].Value = "Delivery Detail Invoice Inquiry"; // Heading Name
                    ws.Cells[2, 1, 2, columncount].Merge = true; //Merge columns start and end range
                    ws.Cells[2, 1, 2, columncount].Style.Font.Bold = true; //Font should be bold
                    ws.Cells[2, 1, 2, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                    //set header kolom
                    #region kolom

                    //ws.Cells[4, 1].AutoFitColumns();
                    ws.Cells.AutoFitColumns(20, 40);
                    ws.Cells[4, 1].Value = "Delivery No";
                    ws.Cells[4, 1].Style.Font.Bold = true;
                    ws.Cells[4, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 1, 5, 1].Merge = true;
                    ws.Cells[4, 1, 5, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                    ws.Cells[4, 2].Value = "Route";
                    ws.Cells[4, 2].AutoFitColumns();
                    ws.Cells[4, 2].Style.Font.Bold = true;
                    ws.Cells[4, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 2, 4, 3].Merge = true;
                    ws.Cells[4, 2, 4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                    ws.Cells[5, 2].Value = "Code";
                    ws.Cells[5, 2].AutoFitColumns();
                    ws.Cells[5, 2].Style.Font.Bold = true;
                    ws.Cells[5, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[5, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                    ws.Cells[5, 3].Value = "Name";
                    ws.Cells[5, 3].AutoFitColumns(50, 150);
                    ws.Cells[5, 3].Style.Font.Bold = true;
                    ws.Cells[5, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[5, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[5, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[5, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[4, 4].Value = "Document Date";
                    ws.Cells[4, 4].AutoFitColumns();
                    ws.Cells[4, 4].Style.Font.Bold = true;
                    ws.Cells[4, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 4, 5, 4].Merge = true;
                    ws.Cells[4, 4, 5, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 5].Value = "Component Price Code";
                    ws.Cells[4, 5].AutoFitColumns();
                    ws.Cells[4, 5].Style.Font.Bold = true;
                    ws.Cells[4, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 5, 5, 5].Merge = true;
                    ws.Cells[4, 5, 5, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 6].Value = "Qty";
                    ws.Cells[4, 6].AutoFitColumns();
                    ws.Cells[4, 6].Style.Font.Bold = true;
                    ws.Cells[4, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 6, 5, 6].Merge = true;
                    ws.Cells[4, 6, 5, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 7].Value = "Price";
                    ws.Cells[4, 7].AutoFitColumns();
                    ws.Cells[4, 7].Style.Font.Bold = true;
                    ws.Cells[4, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 7, 5, 7].Merge = true;
                    ws.Cells[4, 7, 5, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 8].Value = "Currency";
                    ws.Cells[4, 8].AutoFitColumns();
                    ws.Cells[4, 8].Style.Font.Bold = true;
                    ws.Cells[4, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 8, 5, 8].Merge = true;
                    ws.Cells[4, 8, 5, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[4, 9].Value = "Amount";
                    ws.Cells[4, 9].AutoFitColumns();
                    ws.Cells[4, 9].Style.Font.Bold = true;
                    ws.Cells[4, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[4, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[4, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[4, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[4, 9, 5, 9].Merge = true;
                    ws.Cells[4, 9, 5, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    #endregion

                    #region detailnya

                    List<DeliveryInvoiceAndPaymentDetailData> m = mdl.DeliveryInvoiceAndPaymentListDetailData.ToList<DeliveryInvoiceAndPaymentDetailData>();

                    int i = 0;
                    foreach (DeliveryInvoiceAndPaymentDetailData x in m)
                    {



                        ws.Cells[i + 6, 1].Value = m[i].DELIVERY_NO;
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 2].Value = m[i].ROUTE_CD;
                        ws.Cells[i + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //ws.Cells[i + 6, 5].AutoFitColumns();
                        ws.Cells[i + 6, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 3].Value = m[i].ROUTE_NAME;
                        //ws.Cells[i + 6, 5].AutoFitColumns();
                        ws.Cells[i + 6, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 4].Value = string.Format("{0:dd.MM.yyyy}", m[i].DOC_DT);
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 5].Value = m[i].COMP_PRICE_CD;
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 6].Value = m[i].QTY;
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[i + 6, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 7].Value = m[i].PRICE_AMT;
                        ws.Cells[i + 6, 7].Style.Numberformat.Format = "#,##";
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[i + 6, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                        ws.Cells[i + 6, 8].Value = m[i].PAYMENT_CURR;
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[i + 6, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                        ws.Cells[i + 6, 9].Value = m[i].TAX_AMT;
                        ws.Cells[i + 6, 9].Style.Numberformat.Format = "#,##";
                        // ws.Cells[i + 6, 1].AutoFitColumns();
                        ws.Cells[i + 6, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[i + 6, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        i++;
                    }

                    #endregion

                    try
                    {
                        Byte[] bin = p.GetAsByteArray();
                        //string fn = "DeliveryPurchaseRequisition-" + Guid.NewGuid().ToString() + "-" + AuthorizedUser.Username + ".xlsx";
                        string fn = "DeliveryInvoiceInquiryDetail-" + AuthorizedUser.Username + ".xlsx";
                        string DownloadDirectory = "Content/Download/DeliveryPurchaseRequest/";
                        string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
                        var pathfile = Path.Combine(downdir, fn);

                        excelLink = "Content/Download/DeliveryPurchaseRequest/" + fn + "";
                        successStatus = "True";
                        System.IO.File.WriteAllBytes(pathfile, bin);


                    }
                    catch (Exception err)
                    {
                        //write log
                        successStatus = "False";
                    }
                    return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);


                }

            }

        }
        #endregion



        
        #endregion


        // New 
        List<DeliveryInvoiceDataLogPartner> _lpModel = null;
        private List<DeliveryInvoiceDataLogPartner> _LPModel
        {
            get
            {
                if (_lpModel == null)
                    _lpModel = new List<DeliveryInvoiceDataLogPartner>();

                return _lpModel;
            }
            set
            {
                _lpModel = value;
            }
        }


        public ActionResult TruckingLookup()
        {

            TempData["GridName"] = "LogisticPartnerOption";

            List<DeliveryInvoiceDataLogPartner> trucking = Model.GetModel<User>().FilteringArea<DeliveryInvoiceDataLogPartner>(GetAllLP(), "LP_CD", "D31405", "LogisticPartnerOption");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            return PartialView("PG", _LPModel);
        }

        private List<DeliveryInvoiceDataLogPartner> GetAllLP()
        {
            List<DeliveryInvoiceDataLogPartner> lpModel = new List<DeliveryInvoiceDataLogPartner>();

            try
            {
                lpModel = db.Fetch<DeliveryInvoiceDataLogPartner>("DLV_getLP", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return lpModel;
        }

    }
}
