﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.UI.WebControls;
using System.Diagnostics;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Credential;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;

using Portal.Models.Globals;
using Portal.Models.Delivery.Invoice;

namespace Portal.Controllers.Delivery.Invoice
{
    public class D31403Controller : BaseController
    {
        //
        // GET: /D31403/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

        public D31403Controller()
            : base("Delivery Invoice Receiving screen")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryInvoiceReceivingModel mdl = new DeliveryInvoiceReceivingModel();
            InvoiceDocumentModel mdl2 = new InvoiceDocumentModel(); 
            Model.AddModel(mdl);
            Model.AddModel(mdl2);
        }

        //HEADER
        public ActionResult PartialHeader()
        {
            return PartialView("IndexHeader");
        }
        //GRID DATA
        public ActionResult PartialListData()
        {
            DeliveryInvoiceReceivingModel mdl = Model.GetModel<DeliveryInvoiceReceivingModel>();
            mdl.DeliveryInvoiceReceivingListData = GetDLVInvoiceReceivingData();
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        protected List<DeliveryInvoiceReceivingData> GetDLVInvoiceReceivingData()
        {

            List<DeliveryInvoiceReceivingData> listOfDLV_INV_receiving = db.Fetch<DeliveryInvoiceReceivingData>("DLV_INV_getReceivingGrid", new object[] { });
            return listOfDLV_INV_receiving;

        }

        public ActionResult CheckDocument()
        {
            List<InvoiceDocumentData> HeaderData = new List<InvoiceDocumentData>();

            try
            {
                string vCERTIFICATE_ID = String.IsNullOrEmpty(Request.Params["CERTIFICATE_ID"]) ? "" : Request.Params["CERTIFICATE_ID"];
                HeaderData = db.Query<InvoiceDocumentData>("DLV_INV_getInvoiceDocument", new object[] { vCERTIFICATE_ID }).ToList();
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return Json(HeaderData);
        }


        //GETTING MESSAGE BOX
        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }


        public ContentResult performAccept(string CERTIFICATE_ID)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = "Sucess| ";
            try
            {
                db.Execute("DLV_INV_performAccept", new object[] {
                    CERTIFICATE_ID, AuthorizedUser.Username
                });

                result = "Sucess| Certificate ID Accepted";
            }
            catch (Exception ex)
            {
                result = "Error|" + ex.Message;
            }

            db.Close();
            return Content(result);
        }


        public ContentResult performReject(string CERTIFICATE_ID, string NOTES)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = "Sucess| ";
            try
            {
                db.Execute("DLV_INV_performReject", new object[] {
                    CERTIFICATE_ID, NOTES, AuthorizedUser.Username
                });

                result = "Sucess| Certificate ID Rejected";
            }
            catch (Exception ex)
            {
                result = "Error|" + ex.Message;
            }

            db.Close();
            return Content(result);
        }
    }
}
