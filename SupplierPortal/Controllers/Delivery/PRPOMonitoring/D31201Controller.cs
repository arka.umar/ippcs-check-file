﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Text;
using System.Reflection;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.MessageBoard;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Compression;
using Toyota.Common.Util.Text;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;

using Portal.Models.Globals;
using Portal.Models.Delivery.PurchaseOrder;

//add by Agung
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using Telerik.Reporting.Charting;
//end add Agung


namespace Portal.Controllers.D31201
{
    public class D31201Controller : BaseController
    {
        public PurchaseOrderData modelExport;

        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public D31201Controller() : base("D31201","PR - PO Monitoring")
        {

        }
        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            PurchaseOrderModel mdl = new PurchaseOrderModel();
            PurchaseOrderPODetailModel mdl2 = new PurchaseOrderPODetailModel();
            PurchaseOrderDetailModel mdl3 = new PurchaseOrderDetailModel();
            ViewData["StatusLookupPO"] = GetStatusPO();
            ViewData["StatusLookupPR"] = GetStatusPR();
            ViewData["viewPartnerRef"] = FillPartner();
            Model.AddModel(mdl);
            Model.AddModel(mdl2);
            Model.AddModel(mdl3);

        }

        public ActionResult PartialHeader()
        {
            #region Required model for [lp lookup]
            List<PurchaseOrderData> trucking = Model.GetModel<User>().FilteringArea<PurchaseOrderData>(GetAllLP(), "LP_CD", "D31201", "gridPartnerRefView");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            Model.AddModel(_LPModel);
            #endregion

            return PartialView("IndexHeader", Model);
        }

        //-----------------[ GETTING Logistic partner FOR LOOKUP ]-----------------------------------------------------

        public ActionResult viewPartnerRef()
        {
            TempData["GridName"] = "gridPartnerRefView";
            ViewData["viewPartnerRef"] = FillPartner();

            return PartialView("PG", ViewData["viewPartnerRef"]);
        }

        public List<PurchaseOrderData> FillPartner()
        {
            List<PurchaseOrderData> listLogisticPartner = new List<PurchaseOrderData>();
            try
            {
                listLogisticPartner = db.Fetch<PurchaseOrderData>("DLV_GetAllLogisticPartner", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listLogisticPartner;
        }
        //GETTING Status FOR LOOKUP



        // New 
        List<PurchaseOrderData> _lpModel = null;
        private List<PurchaseOrderData> _LPModel
        {
            get
            {
                if (_lpModel == null)
                    _lpModel = new List<PurchaseOrderData>();

                return _lpModel;
            }
            set
            {
                _lpModel = value;
            }
        }

        public ActionResult LPLookup()
        {

            TempData["GridName"] = "gridPartnerRefView";

            List<PurchaseOrderData> trucking = Model.GetModel<User>().FilteringArea<PurchaseOrderData>(GetAllLP(), "LP_CD", "D31201", "gridPartnerRefView");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            return PartialView("PG", _LPModel);
        }

        private List<PurchaseOrderData> GetAllLP()
        {
            List<PurchaseOrderData> lpModel = new List<PurchaseOrderData>();

            try
            {
                lpModel = db.Fetch<PurchaseOrderData>("DLV_GetAllLogisticPartner", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return lpModel;
        }





        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }

        public bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            txtDate = txtDate.Replace(".", "-");
            return DateTime.TryParse(txtDate, out tempDate) ? true : false;
        }

        public string SetFormatDate(string txtDate, string onformat = "yyyy-MM-dd")
        {
            string date = txtDate;
            if (IsDateTime(date))
            {
                DateTime dt = Convert.ToDateTime(date);
                date = dt.ToString(onformat);
            }
            return date;
        }

        public string EnglishDate(string txtDate)
        {
            try
            {
                txtDate = txtDate.Replace(".", "-");
                string[] date = txtDate.Split('-');
                return date[2] + '-' + date[1] + '-' + date[0];
            }
            catch (Exception e)
            {
                return "";
            }
        }

        //-------------------------------------[ MAIN GRID ]--------------------------------------------------------
        public ActionResult PartialDatas()
        {
            string vPO_MONTH = String.IsNullOrEmpty(Request.Params["pPO_MONTH"]) ? "" : Request.Params["pPO_MONTH"]; 
            string vPR_NO = String.IsNullOrEmpty(Request.Params["pPR_NO"]) ? "" : Request.Params["pPR_NO"];
            string vPR_STATUS = String.IsNullOrEmpty(Request.Params["pPR_STATUS"]) ? "" : Request.Params["pPR_STATUS"];
            string vPR_DATE = String.IsNullOrEmpty(Request.Params["pPR_DATE"]) ? "" : Request.Params["pPR_DATE"];
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NO"]) ? "" : Request.Params["pPO_NO"];
            string vPO_STATUS = String.IsNullOrEmpty(Request.Params["pPO_STATUS"]) ? "" : Request.Params["pPO_STATUS"];
            string vPO_DATE = String.IsNullOrEmpty(Request.Params["pPO_DATE"]) ? "" : Request.Params["pPO_DATE"];
            string vLP_CD = String.IsNullOrEmpty(Request.Params["pLP_CD"]) ? "" : Request.Params["pLP_CD"];

            vPR_DATE = SetFormatDate(EnglishDate(vPR_DATE));
            vPO_DATE = SetFormatDate(EnglishDate(vPO_DATE));

            PurchaseOrderModel mdl = Model.GetModel<PurchaseOrderModel>();
            mdl.PurchaseOrderDatas = GetPurchaseOrder(vPO_MONTH, vPR_NO, vPR_STATUS, vPR_DATE, vPO_NO, vPO_STATUS, vPO_DATE, vLP_CD);
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        protected List<PurchaseOrderData> GetPurchaseOrder(string vPO_MONTH, string vPR_NO, string vPR_STATUS, string vPR_DATE, string vPO_NO, string vPO_STATUS, string vPO_DATE, string vLP_CD)
        {
            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(vLP_CD))
            {
                string[] logisticCDList = vLP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            string POStatusParameter = string.Empty;
            if (!string.IsNullOrEmpty(vPO_STATUS))
            {
                string[] POStatusList = vPO_STATUS.Split(';');
                foreach (string r2 in POStatusList)
                {
                    POStatusParameter += "'" + r2 + "',";
                }
                POStatusParameter = POStatusParameter.Remove(POStatusParameter.Length - 1, 1);
            }

            string PRStatusParameter = string.Empty;
            if (!string.IsNullOrEmpty(vPR_STATUS))
            {
                string[] PRStatusList = vPR_STATUS.Split(';');
                foreach (string r3 in PRStatusList)
                {
                    PRStatusParameter += "'" + r3 + "',";
                }
                PRStatusParameter = PRStatusParameter.Remove(PRStatusParameter.Length - 1, 1);
            }

            List<PurchaseOrderData> listOfRouteMaster = db.Fetch<PurchaseOrderData>("DLV_GetAllDataPRPOMonitoring", new object[] { 
                vPO_MONTH, vPR_NO, PRStatusParameter, vPR_DATE, vPO_NO, POStatusParameter, vPO_DATE,logisticCDParameter
            });
            return listOfRouteMaster;

        }

        //-------------------------------------------------------------------------------------------------------------------------------
        public ActionResult PartialDetailDatas()
        {
            string vPR_NO = String.IsNullOrEmpty(Request.Params["pPR_NO"]) ? "" : Request.Params["pPR_NO"];
            string vPR_STATUS = String.IsNullOrEmpty(Request.Params["pPR_STATUS"]) ? "" : Request.Params["pPR_STATUS"];
            string vPR_DATE = String.IsNullOrEmpty(Request.Params["pPR_DATE"]) ? "" : Request.Params["pPR_DATE"];
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NO"]) ? "" : Request.Params["pPO_NO"];
            string vPO_STATUS = String.IsNullOrEmpty(Request.Params["pPO_STATUS"]) ? "" : Request.Params["pPO_STATUS"];
            string vPO_DATE = String.IsNullOrEmpty(Request.Params["pPO_DATE"]) ? "" : Request.Params["pPO_DATE"];
            string vLP_CD = String.IsNullOrEmpty(Request.Params["pLP_CD"]) ? "" : Request.Params["pLP_CD"];


            if (vPR_DATE != "")
            {
                if (IsDateTime(vPR_DATE))
                {
                    string date = vPR_DATE;
                    DateTime dt = Convert.ToDateTime(date);

                    vPR_DATE = dt.ToString("yyyy-MM-dd");
                }
            }

            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(vLP_CD))
            {
                string[] logisticCDList = vLP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }
            PurchaseOrderDetailModel mdl3 = Model.GetModel<PurchaseOrderDetailModel>();
            mdl3.PurchaseOrderDetailDatas = GetPurchaseOrderDetail(vPR_NO, vPR_STATUS, vPR_DATE, vPO_NO, vPO_STATUS, vPO_DATE, logisticCDParameter);
            Model.AddModel(mdl3);
            return PartialView("monPopupViewDetail", Model);
        }

        protected List<PurchaseOrderDetailData> GetPurchaseOrderDetail(string vPR_NO, string vPR_STATUS, string vPR_DATE, string vPO_NO, string vPO_STATUS, string vPO_DATE, string vLP_CD)
        {
            List<PurchaseOrderDetailData> listOfData = db.Fetch<PurchaseOrderDetailData>("DLV_getPurchaseOrderMonitoringDetail", new object[] { 
                vPR_NO, vPR_STATUS, vPR_DATE, vPO_NO, vPO_STATUS, vPO_DATE,vLP_CD
            });
            return listOfData;

        }
        //-------------------------------------[ POPUP PO ]---------------------------------------------------------
        public ActionResult PopupDetailPO()
        {
            string vPO_NO = String.IsNullOrEmpty(Request.Params["pPO_NO_DET"]) ? "" : Request.Params["pPO_NO_DET"];
            TempData["GridName"] = "gridmonPopupPODetail";
            ViewData["PopupDetailPO"] = FillviewPODetail(vPO_NO);
            return PartialView("monPopupPODetail", ViewData["PopupDetailPO"]);
        }
        public List<PurchaseOrderPODetailData> FillviewPODetail(string vPO_NO)
        {
            List<PurchaseOrderPODetailData> listOfData = new List<PurchaseOrderPODetailData>();
            try
            {
                listOfData = db.Fetch<PurchaseOrderPODetailData>("DLV_getPODetail", new object[] { vPO_NO });
            }
            catch (Exception ex) { throw ex; }
            return listOfData;
        }



        //-----------------------------------------------------------------------------
        public ActionResult PopupDetailPOHeader()
        {
            List<PurchaseOrderPODetailData> HeaderData = new List<PurchaseOrderPODetailData>();

            try
            {
                string vPO_NO = String.IsNullOrEmpty(Request.Params["PO_NO"]) ? "" : Request.Params["PO_NO"];
                HeaderData = db.Query<PurchaseOrderPODetailData>("DLV_getPODetail", new object[] { vPO_NO }).ToList();
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return Json(HeaderData);
        }




        //-------------------------------------[ POPUP PR ]---------------------------------------------------------

        public ActionResult PopupDetailPR()
        {
            string vPR_NO = String.IsNullOrEmpty(Request.Params["pPR_NO_DET"]) ? "" : Request.Params["pPR_NO_DET"];
            TempData["GridName"] = "gridmonPopupPRDetail";
            ViewData["PopupDetailPR"] = FillviewPRDetail(vPR_NO);
            return PartialView("monPopupPRDetail", ViewData["PopupDetailPR"]);
        }
        public List<PRDetailData_PO> FillviewPRDetail(string vPR_NO)
        {
            List<PRDetailData_PO> listOfData = new List<PRDetailData_PO>();
            try
            {
                listOfData = db.Fetch<PRDetailData_PO>("DLV_getPRDetailPO", new object[] { vPR_NO });
            }
            catch (Exception ex) { throw ex; }
            return listOfData;
        }


        public ActionResult PopupDetailPRHeader()
        {
            List<PRDetailData_PO> HeaderData = new List<PRDetailData_PO>();

            try
            {
                string vPR_NO = String.IsNullOrEmpty(Request.Params["PR_NO"]) ? "" : Request.Params["PR_NO"];
                HeaderData = db.Query<PRDetailData_PO>("DLV_getPRDetailPO", new object[] { vPR_NO }).ToList();
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }
            return Json(HeaderData);
        }

        public ActionResult monPopupPOHead()
        {
            return PartialView("monPopupPOHead");
        }
        public ActionResult monPopupPODetail()
        {
            return PartialView("monPopupPODetail");
        }

        public ActionResult monPopupPRHead()
        {
            return PartialView("monPopupPRHead");
        }
        public ActionResult monPopupPRDetail()
        {
            return PartialView("monPopupPRDetail");
        }

        public ActionResult monPopupViewDetail()
        {
            return PartialView("monPopupViewDetail");
        }

        //-----STATUS PO-----------------------------------------
        public ActionResult StatusLookupPO()
        {

            TempData["GridName"] = "PO_STATUS";
            ViewData["StatusLookupPO"] = GetStatusPO();

            return PartialView("PG", ViewData["StatusLookupPO"]);
        }

        public List<PODataStatus> GetStatusPO()
        {
            List<PODataStatus> listStatus = new List<PODataStatus>();
            try
            {
                listStatus = db.Fetch<PODataStatus>("DLV_getPOStatus", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listStatus;
        }


        //STATUS PR -----------------------------------------------------


        public ActionResult StatusLookupPR()
        {

            TempData["GridName"] = "PR_STATUS";
            ViewData["StatusLookup"] = GetStatusPR();

            return PartialView("PG", ViewData["StatusLookupPR"]);
        }

        public List<DataStatusPR> GetStatusPR()
        {
            List<DataStatusPR> listStatus = new List<DataStatusPR>();
            try
            {
                listStatus = db.Fetch<DataStatusPR>("DLV_getPRStatus", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listStatus;
        }

        //PDF PRINT---------------------------------------------------------------------------------------------------------
        private void pbDH_ItemDataBound(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.PictureBox picturebox = sender as
                Telerik.Reporting.Processing.PictureBox;

            if (picturebox != null)
            {
                string img = Server.MapPath("~") + @"\Report\DHSign.png";
                //string img = System.IO.Path.Combine(ConfigurationManager.AppSettings["SignatureDirectory"], "DHSign.png");

                picturebox.Image = System.Drawing.Image.FromFile(img);
            }
        }

        #region CREATE REPORT ON BYTE TYPE
        private byte[] ReportOnByte(string PO_NO)
        {
            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            #region REPORT PDF

            Telerik.Reporting.Report rep0;

            string conString = DBContextNames.DB_PCS;

            string fileName = "";
            fileName = PO_NO + ".pdf";
            
            string sqlCommand = "";
            String CurrDate = DateTime.Now.ToString("yyyyMMddhhmm");
            sqlCommand = "SP_DLV_PO_LETTER";

            Telerik.Reporting.SqlDataSource sqlDataSource1;
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PO_NO", System.Data.DbType.String, PO_NO);

            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\Delivery\PO\PO_LETTER.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rep0 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                //rep0.Items["labelsGroupFooter"].Items["DHSignPic"].ItemDataBound += new EventHandler(pbDH_ItemDataBound);
            }


            #endregion

            sqlDataSource1 = (Telerik.Reporting.SqlDataSource)rep0.DataSource;
            sqlDataSource1.ConnectionString = conString;
            sqlDataSource1.SelectCommandType = commandType;
            sqlDataSource1.SelectCommand = sqlCommand;

            if (parameters != null)
                sqlDataSource1.Parameters.AddRange(parameters);

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rep0, null);

            UpdateDownloadFlag(PO_NO); //update status PO

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "", fileName));
            Response.BinaryWrite(result.DocumentBytes);
            Response.Flush();
            Response.End();

            return result.DocumentBytes;
        }
        #endregion

        #region PREVIEW REPORT
        public FileContentResult Reporting(string id)
        {
            
            return File(ReportOnByte(id), "application/pdf");
        }
        #endregion

        #region UPDATE PO STATUS DOWNLOAD
        public void UpdateDownloadFlag(string PONo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string result = string.Empty;

            db.Execute("DLV_UpdateDownloadFlagPODownload", new Object[] { PONo, AuthorizedUser.Username });
            db.Close();
        }
        #endregion
    }
}
