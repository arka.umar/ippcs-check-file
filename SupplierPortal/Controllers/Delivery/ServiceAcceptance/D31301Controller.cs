﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Text;
using System.Reflection;


using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.MessageBoard;
using Toyota.Common.Web.Compression;
using Toyota.Common.Util.Text;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Portal.Models.Globals;
using Portal.Models.Delivery.ServiceAcceptance;

namespace Portal.Controllers.Delivery.ServiceAcceptance
{
    public class D31301Controller : BaseController
    {
        #region Preparing the controller
        public ServiceAcceptanceData modelExport;
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public D31301Controller()
            : base("D31301","Service Acceptance")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            ServiceAcceptanceModel mdl = new ServiceAcceptanceModel();
            //ViewData["DeliveryNoLookup"] = GetDeliveryNo();
            ViewData["DeliveryTypeLookup"] = GetDeliveryType();
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();
            ViewData["RouteLookup"] = GetRoute();
            ViewData["StatusLookup"] = GetStatus();
            //mdl.RouteMaster = GetRouteMaster(string.Empty);
            Model.AddModel(mdl);
            

        }

        public ActionResult PartialHeader()
        {
            #region Required model for [lp lookup]
            List<ServiceAcceptanceData> trucking = Model.GetModel<User>().FilteringArea<ServiceAcceptanceData>(GetAllLP(), "LP_CD", "D31301", "LogisticPartnerOption");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            Model.AddModel(_LPModel);
            #endregion

            return PartialView("IndexHeader", Model);
        }
        #endregion

        

        #region MAIN GRID

        #region this function will be called by the grid


        List<ServiceAcceptanceData> _serviceAcceptanceDataModel = null;
        private List<ServiceAcceptanceData> _ServiceAcceptanceDataModel
        {
            get
            {
                if (_serviceAcceptanceDataModel == null)
                    _serviceAcceptanceDataModel = new List<ServiceAcceptanceData>();

                return _serviceAcceptanceDataModel;
            }
            set
            {
                _serviceAcceptanceDataModel = value;
            }
        }

        public ActionResult PartialDatas()
        {
            string IsDownload = "no";
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            string DELIVERY_TYPE = String.IsNullOrEmpty(Request.Params["DeliveryType"]) ? "" : Request.Params["DeliveryType"];
            string LP_CD = String.IsNullOrEmpty(Request.Params["LogisticPartner"]) ? "" : Request.Params["LogisticPartner"];
            string ROUTE = String.IsNullOrEmpty(Request.Params["Route"]) ? "" : Request.Params["Route"];
            string STATUS = String.IsNullOrEmpty(Request.Params["Status"]) ? "" : Request.Params["Status"];
            string ROUTE_DATE = String.IsNullOrEmpty(Request.Params["RouteDate"]) ? "" : Request.Params["RouteDate"];
            string ROUTE_DATE_TO = String.IsNullOrEmpty(Request.Params["RouteDateTo"]) ? "" : Request.Params["RouteDateTo"];
            string PO_NO = String.IsNullOrEmpty(Request.Params["PoNo"]) ? "" : Request.Params["PoNo"];

            IsDownload = Request.Params["IsDownload"];
            ServiceAcceptanceModel mdl = Model.GetModel<ServiceAcceptanceModel>();
            //mdl.ServiceAcceptanceDatas = GetServiceAcceptance(DELIVERY_NO, DELIVERY_TYPE, LP_CD, ROUTE, STATUS, ROUTE_DATE, ROUTE_DATE_TO, PO_NO);
            _serviceAcceptanceDataModel = GetServiceAcceptance(DELIVERY_NO, DELIVERY_TYPE, LP_CD, ROUTE, STATUS, ROUTE_DATE, ROUTE_DATE_TO, PO_NO);
            //Model.AddModel(mdl);
            Model.AddModel(_serviceAcceptanceDataModel);
            if (Request.Params["visible"] != null)
            {
                if (Boolean.Parse(Request.Params["visible"]) == true)
                {
                    ViewBag.BooleanValue = false;
                }
                else
                {
                    ViewBag.BooleanValue = true;
                }
            }

            return PartialView("GridIndex", Model);
        }
        #endregion

        #region this function will be called from PartialDatas() to start selecting the datas from the table, it  has some params to filter the records
        protected List<ServiceAcceptanceData> GetServiceAcceptance(string DELIVERY_NO, string DELIVERY_TYPE, string LP_CD, string ROUTE, string STATUS, string ROUTE_DATE, string ROUTE_DATE_TO, string PO_NO)
        {
            #region splitting the string if there's any delimiter on DELIVERY_NO
            string deliveryNoParameter = string.Empty;
            if (!string.IsNullOrEmpty(DELIVERY_NO))
            {
                deliveryNoParameter += "'%" + DELIVERY_NO + "%'";
            }
            #endregion

            #region splitting the string if there's any delimiter on DELIVERY_TYPE
            string deliveryTypeParameter = string.Empty;
            if (!string.IsNullOrEmpty(DELIVERY_TYPE))
            {
                string[] deliveryTypeList = DELIVERY_TYPE.Split(';');
                foreach (string r in deliveryTypeList)
                {
                    string rs;
                    if (r == "Non TLMS")
                    {
                        rs = "N";
                    }
                    else if (r == "Regular")
                    {
                        rs = "R";
                    }
                    else
                    {
                        rs = "A";
                    }
                    deliveryTypeParameter += "'" + rs + "',";
                }
                deliveryTypeParameter = deliveryTypeParameter.Remove(deliveryTypeParameter.Length - 1, 1);
            }
            #endregion

            if (STATUS == "")
            {
                LP_CD = string.Empty;
            }

            #region splitting the string if there's any delimiter on LP_CD
            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on ROUTE
            string routeParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE))
            {
                string[] routeList = ROUTE.Split(';');
                foreach (string r in routeList)
                {
                    routeParameter += "'" + r + "',";
                }
                routeParameter = routeParameter.Remove(routeParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on STATUS

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(STATUS))
            {
                string[] statusList = STATUS.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            #endregion

            

            string routeDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE))
            {
                string d = ROUTE_DATE.Substring(0, 2);
                string m = ROUTE_DATE.Substring(3, 2);
                string y = ROUTE_DATE.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateParameter += "'" + tgl + "'";
            }

            string routeDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE_TO))
            {
                string d = ROUTE_DATE_TO.Substring(0, 2);
                string m = ROUTE_DATE_TO.Substring(3, 2);
                string y = ROUTE_DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateToParameter += "'" + tgl + "'";
            }

            string poNoParameter = string.Empty;
            if (!string.IsNullOrEmpty(PO_NO))
            {
                poNoParameter += "'%" + PO_NO + "%'";
            }
            
            List<ServiceAcceptanceData> listSA = db.Fetch<ServiceAcceptanceData>("DLV_ServiceAcceptanceData", new object[] { 
                deliveryNoParameter,
                deliveryTypeParameter,
                logisticCDParameter,
                routeParameter,
                statusParameter,
                routeDateParameter,
                routeDateToParameter,
                poNoParameter
            });
            return listSA;

        }
        #endregion
        #endregion

        #region search

        #region get delivery no records for option in the search section of the screen
        //public ActionResult DeliveryNoLookup()
        //{

        //    TempData["GridName"] = "DeliveryNoOption";
        //    ViewData["DeliveryNoLookup"] = GetDeliveryNo();

        //    return PartialView("GridLookup/PartialGrid", ViewData["DeliveryNoLookup"]);
        //}

        //public List<ServiceAcceptanceData> GetDeliveryNo()
        //{

        //    List<ServiceAcceptanceData> listServiceAcceptance = new List<ServiceAcceptanceData>();
        //    try
        //    {
        //        listServiceAcceptance = db.Fetch<ServiceAcceptanceData>("DLV_ServiceAcceptanceDeliveryNo", new object[] { });
        //    }
        //    catch (Exception ex) { throw ex; }
        //    return listServiceAcceptance;
        //}
        #endregion

        #region get delivery type records for option in the search section of the screen

        public ActionResult DeliveryTypeLookup()
        {

            TempData["GridName"] = "DeliveryTypeOption";
            ViewData["DeliveryTypeLookup"] = GetDeliveryType();

            return PartialView("PG", ViewData["DeliveryTypeLookup"]);
        }

        public List<ServiceAcceptanceData> GetDeliveryType()
        {
            /*List<ServiceAcceptanceData> listDeliveryType = new List<ServiceAcceptanceData>();
            try
            {
                listDeliveryType = db.Fetch<ServiceAcceptanceData>("getDeliveryType", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listDeliveryType;*/

            List<ServiceAcceptanceData> data = new List<ServiceAcceptanceData>();
            data.Add(new ServiceAcceptanceData()
            {
                DELIVERY_TYPE = "A",
                DELIVERY_TYPE_DESC = "Additional"
            });

            data.Add(new ServiceAcceptanceData()
            {
                DELIVERY_TYPE = "R",
                DELIVERY_TYPE_DESC = "Regular"
            });

            data.Add(new ServiceAcceptanceData()
            {
                DELIVERY_TYPE = "N",
                DELIVERY_TYPE_DESC = "Non TLMS"
            });
            return data;
        }

        #endregion

        #region get logistic partner records for option in the search section of the screen
        public ActionResult LogisticPartnerLookup()
        {

            TempData["GridName"] = "LogisticPartnerOption";
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            return PartialView("PG", ViewData["LogisticPartnerLookup"]);
        }

        public List<ServiceAcceptanceData> GetLogisticPartner()
        {

            List<ServiceAcceptanceData> listLogisticPartner = new List<ServiceAcceptanceData>();
            try
            {
                listLogisticPartner = db.Fetch<ServiceAcceptanceData>("DLV_ServiceAcceptanceLogisticPartner", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listLogisticPartner;
        }

        // New 
        List<ServiceAcceptanceData> _lpModel = null;
        private List<ServiceAcceptanceData> _LPModel
        {
            get
            {
                if (_lpModel == null)
                    _lpModel = new List<ServiceAcceptanceData>();

                return _lpModel;
            }
            set
            {
                _lpModel = value;
            }
        }

        public ActionResult LPLookup()
        {

            TempData["GridName"] = "LogisticPartnerOption";

            List<ServiceAcceptanceData> trucking = Model.GetModel<User>().FilteringArea<ServiceAcceptanceData>(GetAllLP(), "LP_CD", "D31301", "LogisticPartnerOption");

            if (trucking.Count > 0)
            {
                _LPModel = trucking;
            }
            else
            {
                _LPModel = GetAllLP();
            }

            return PartialView("PG", _LPModel);
        }

        private List<ServiceAcceptanceData> GetAllLP()
        {
            List<ServiceAcceptanceData> lpModel = new List<ServiceAcceptanceData>();

            try
            {
                lpModel = db.Fetch<ServiceAcceptanceData>("DLV_ServiceAcceptanceLogisticPartner", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return lpModel;
        }



        #endregion

        #region get route records for option in the search section of the screen
        public ActionResult RouteLookup()
        {

            TempData["GridName"] = "RouteOption";
            ViewData["RouteLookup"] = GetRoute();
            return PartialView("PG", ViewData["RouteLookup"]);
        }

        public List<ServiceAcceptanceData> GetRoute()
        {
            List<ServiceAcceptanceData> listRoute = new List<ServiceAcceptanceData>();
            try
            {
                listRoute = db.Fetch<ServiceAcceptanceData>("DLV_ServiceAcceptanceRoute", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listRoute;
        }
        #endregion

        #region get delivery status records for option in the search section of the screen
        public ActionResult StatusLookup()
        {
            TempData["GridName"] = "StatusOption";
            ViewData["StatusLookup"] = GetStatus();

            return PartialView("PG2", ViewData["StatusLookup"]);
        }

        public List<ServiceAcceptanceStatus> GetStatus()
        {
            List<ServiceAcceptanceStatus> listStatus = new List<ServiceAcceptanceStatus>();
            try
            {
                listStatus = db.Fetch<ServiceAcceptanceStatus>("DLV_SA_getStatus", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listStatus;
        }
        #endregion

        #endregion

        #region Toggle detail
        //GRID DETAIL
        public ActionResult PartialDetailDatas()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            ServiceAcceptanceModel mdl = new ServiceAcceptanceModel();
            mdl.ServiceAcceptanceDatas = GetServiceAcceptanceDetail(DELIVERY_NO);
            Model.AddModel(mdl);
            return PartialView("GridIndexDetail", Model);
        }



        protected List<ServiceAcceptanceData> GetServiceAcceptanceDetail(string DELIVERY_NO)
        {
            string deliveryNoParameter = string.Empty;
            //if (!string.IsNullOrEmpty(DELIVERY_NO))
            //{
            //    string[] deliveryNoList = DELIVERY_NO.Split(';');

            //    foreach (string r in deliveryNoList)
            //    {
            //        deliveryNoParameter += "'" + r + "',";
            //    }
            //    deliveryNoParameter = deliveryNoParameter.Remove(deliveryNoParameter.Length - 1, 1);
            //}
            deliveryNoParameter += "'" + DELIVERY_NO + "'";
            List<ServiceAcceptanceData> listOfDetail = db.Fetch<ServiceAcceptanceData>("DLV_ServiceAcceptanceDataDetail", new object[] { 
                deliveryNoParameter
            });
            return listOfDetail;

        }

        public ActionResult PartialDetailErrorDatas()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            ServiceAcceptanceModel mdl = new ServiceAcceptanceModel();
            mdl.ServiceAcceptanceDatas = GetServiceAcceptanceDetailError(DELIVERY_NO);
            Model.AddModel(mdl);
            return PartialView("GridIndexDetailError", Model);
        }

        protected List<ServiceAcceptanceData> GetServiceAcceptanceDetailError(string DELIVERY_NO)
        {
            string deliveryNoParameter = string.Empty;
            /* if (!string.IsNullOrEmpty(DELIVERY_NO))
             {
                 string[] deliveryNoList = DELIVERY_NO.Split(';');

                 foreach (string r in deliveryNoList)
                 {
                     deliveryNoParameter += "'" + r + "',";
                 }
                 deliveryNoParameter = deliveryNoParameter.Remove(deliveryNoParameter.Length - 1, 1);
             }
             */
            deliveryNoParameter += "'" + DELIVERY_NO + "'";
            List<ServiceAcceptanceData> listOfDetail = db.Fetch<ServiceAcceptanceData>("DLV_ServiceAcceptanceDetailError", new object[] { 
                deliveryNoParameter
            });
            return listOfDetail;

        }


        #endregion

        #region Export To Excel
        #region Export To Excel
        public ActionResult exportToExcel()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            string DELIVERY_TYPE = String.IsNullOrEmpty(Request.Params["DeliveryType"]) ? "" : Request.Params["DeliveryType"];
            string LP_CD = String.IsNullOrEmpty(Request.Params["LogisticPartner"]) ? "" : Request.Params["LogisticPartner"];
            string ROUTE = String.IsNullOrEmpty(Request.Params["Route"]) ? "" : Request.Params["Route"];
            string STATUS = String.IsNullOrEmpty(Request.Params["Status"]) ? "" : Request.Params["Status"];
            string ROUTE_DATE = String.IsNullOrEmpty(Request.Params["RouteDate"]) ? "" : Request.Params["RouteDate"];
            string ROUTE_DATE_TO = String.IsNullOrEmpty(Request.Params["RouteDateTo"]) ? "" : Request.Params["RouteDateTo"];
            string PO_NO = String.IsNullOrEmpty(Request.Params["PoNo"]) ? "" : Request.Params["PoNo"];
            ServiceAcceptanceModel mdl = Model.GetModel<ServiceAcceptanceModel>();
            mdl.ServiceAcceptanceDatas = GetServiceAcceptance(DELIVERY_NO, DELIVERY_TYPE, LP_CD, ROUTE, STATUS, ROUTE_DATE, ROUTE_DATE_TO, PO_NO);




            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Worksheets.Add("Service Acceptance");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Name = "Service Acceptance";
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";
                //header
                string excelLink = "";
                int columncount = 17;
                string successStatus = "";
                ws.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA"; // Heading Name
                ws.Cells[1, 1, 1, columncount].Merge = true; //Merge columns start and end range
                ws.Cells[1, 1, 1, columncount].Style.Font.Bold = true; //Font should be bold
                ws.Cells[1, 1, 1, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                ws.Cells[2, 1].Value = "Service Acceptance"; // Heading Name
                ws.Cells[2, 1, 2, columncount].Merge = true; //Merge columns start and end range
                ws.Cells[2, 1, 2, columncount].Style.Font.Bold = true; //Font should be bold
                ws.Cells[2, 1, 2, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                //set header kolom
                #region kolom


                ws.Cells.AutoFitColumns(20, 40);
                ws.Cells[4, 1].Value = "Delivery No";
                ws.Cells[4, 1].Style.Font.Bold = true;
                ws.Cells[4, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //ws.Cells[4, 1].AutoFitColumns();
                ws.Cells[4, 1, 5, 1].Merge = true;

                ws.Cells[4, 2].Value = "Delivery Type";
                ws.Cells[4, 2].Style.Font.Bold = true;
                ws.Cells[4, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[4, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //ws.Cells[4, 2].AutoFitColumns();
                ws.Cells[4, 2, 5, 2].Merge = true;

                ws.Cells[4, 3].Value = "Trucking";
                ws.Cells[4, 3].Style.Font.Bold = true;
                ws.Cells[4, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[4, 3].AutoFitColumns();
                ws.Cells[4, 3, 4, 4].Merge = true;

                ws.Cells[5, 3].Value = "Code";
                ws.Cells[5, 3].Style.Font.Bold = true;
                ws.Cells[5, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[5, 3].AutoFitColumns();

                ws.Cells[5, 4].Value = "Name";
                ws.Cells[5, 4].Style.Font.Bold = true;
                ws.Cells[5, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                ws.Cells[5, 4].AutoFitColumns(50, 150);


                ws.Cells[4, 5].Value = "Route - Rate";
                ws.Cells[4, 5].Style.Font.Bold = true;
                ws.Cells[4, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[4, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[4, 5].AutoFitColumns();
                ws.Cells[4, 5, 5, 5].Merge = true;

                ws.Cells[4, 6].Value = "Delivery Status";
                ws.Cells[4, 6].Style.Font.Bold = true;
                ws.Cells[4, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[4, 6].AutoFitColumns();
                ws.Cells[4, 6, 5, 6].Merge = true;

                ws.Cells[4, 7].Value = "Achievement";
                ws.Cells[4, 7].Style.Font.Bold = true;
                ws.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[4, 7].AutoFitColumns();
                ws.Cells[4, 7, 4, 8].Merge = true;

                ws.Cells[5, 7].Value = "Arrival";
                ws.Cells[5, 7].Style.Font.Bold = true;
                ws.Cells[5, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //  ws.Cells[5, 7].AutoFitColumns();

                ws.Cells[5, 8].Value = "Departure";
                ws.Cells[5, 8].Style.Font.Bold = true;
                ws.Cells[5, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[5, 8].AutoFitColumns();

                ws.Cells[4, 9].Value = "PO No.";
                ws.Cells[4, 9].Style.Font.Bold = true;
                ws.Cells[4, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                ws.Cells[4, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[4, 9].AutoFitColumns();
                ws.Cells[4, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 9, 5, 9].Merge = true;

                ws.Cells[4, 10].Value = "Financial Document";
                ws.Cells[4, 10].Style.Font.Bold = true;
                ws.Cells[4, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[4, 7].AutoFitColumns();
                ws.Cells[4, 10, 4, 11].Merge = true;

                ws.Cells[5, 10].Value = "MatDoc No.";
                ws.Cells[5, 10].Style.Font.Bold = true;
                ws.Cells[5, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //  ws.Cells[5, 7].AutoFitColumns();

                ws.Cells[5, 11].Value = "Posting Date";
                ws.Cells[5, 11].Style.Font.Bold = true;
                ws.Cells[5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[4, 12].Value = "Supplier Invoice";
                ws.Cells[4, 12].Style.Font.Bold = true;
                ws.Cells[4, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 12].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[4, 7].AutoFitColumns();
                ws.Cells[4, 12, 4, 14].Merge = true;

                ws.Cells[5, 12].Value = "Invoice No.";
                ws.Cells[5, 12].Style.Font.Bold = true;
                ws.Cells[5, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 12].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //  ws.Cells[5, 7].AutoFitColumns();

                ws.Cells[5, 13].Value = "Invoice Date";
                ws.Cells[5, 13].Style.Font.Bold = true;
                ws.Cells[5, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 13].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[5, 14].Value = "PIC";
                ws.Cells[5, 14].Style.Font.Bold = true;
                ws.Cells[5, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 14].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[4, 15].Value = "Cancellation";
                ws.Cells[4, 15].Style.Font.Bold = true;
                ws.Cells[4, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[4, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[4, 15].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[4, 15].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                // ws.Cells[4, 7].AutoFitColumns();
                ws.Cells[4, 15, 4, 17].Merge = true;

                ws.Cells[5, 15].Value = "MatDoc No.";
                ws.Cells[5, 15].Style.Font.Bold = true;
                ws.Cells[5, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 15].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 15].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //  ws.Cells[5, 7].AutoFitColumns();

                ws.Cells[5, 16].Value = "Cancel Date";
                ws.Cells[5, 16].Style.Font.Bold = true;
                ws.Cells[5, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 16].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                ws.Cells[5, 17].Value = "PIC";
                ws.Cells[5, 17].Style.Font.Bold = true;
                ws.Cells[5, 17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[5, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ws.Cells[5, 17].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[5, 17].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                #endregion

                #region detailnya

                List<ServiceAcceptanceData> m = mdl.ServiceAcceptanceDatas.ToList<ServiceAcceptanceData>();

                int i = 0;
                foreach (ServiceAcceptanceData x in m)
                {
                    ws.Cells[i + 6, 1].Value = m[i].DELIVERY_NO;
                    // ws.Cells[i + 6, 1].AutoFitColumns();
                    ws.Cells[i + 6, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    ws.Cells[i + 6, 2].Value = m[i].DELIVERY_TYPE;
                    //ws.Cells[i + 6, 2].AutoFitColumns();
                    ws.Cells[i + 6, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    ws.Cells[i + 6, 3].Value = m[i].LP_CD;
                    //ws.Cells[i + 6, 3].AutoFitColumns();
                    ws.Cells[i + 6, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    ws.Cells[i + 6, 4].Value = m[i].LP_NAME;
                    //ws.Cells[i + 6, 4].AutoFitColumns();
                    ws.Cells[i + 6, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    ws.Cells[i + 6, 5].Value = m[i].ROUTE_RATE;
                    //ws.Cells[i + 6, 5].AutoFitColumns();
                    ws.Cells[i + 6, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    if (m[i].DELIVERY_STS == "Posting Error")
                    {
                        ws.Cells[i + 6, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[i + 6, 6].Style.Fill.BackgroundColor.SetColor(Color.Red);
                        ws.Cells[i + 6, 6].Style.Font.Color.SetColor(Color.White);
                    }
                    ws.Cells[i + 6, 6].Value = m[i].DELIVERY_STS;
                    //ws.Cells[i + 6, 6].AutoFitColumns();
                    ws.Cells[i + 6, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    if (string.Format("{0:dd.MM.yyyy HH:mm}", m[i].ARRIVAL) != "01.01.0001 00:00")
                    {
                        ws.Cells[i + 6, 7].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].ARRIVAL);
                    }
                    //ws.Cells[i + 6, 7].AutoFitColumns();
                    ws.Cells[i + 6, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                    if (string.Format("{0:dd.MM.yyyy HH:mm}", m[i].DEPARTURE) != "01.01.0001 00:00")
                    {
                        ws.Cells[i + 6, 8].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].DEPARTURE);
                    }
                    //ws.Cells[i + 6, 8].AutoFitColumns();
                    ws.Cells[i + 6, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    ws.Cells[i + 6, 9].Value = m[i].PO_NO;
                    //ws.Cells[i + 6, 9].AutoFitColumns();
                    ws.Cells[i + 6, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                    ws.Cells[i + 6, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 10].Value = m[i].MATDOC_NO;
                    ws.Cells[i + 6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    ws.Cells[i + 6, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    if (string.Format("{0:dd.MM.yyyy HH:mm}", m[i].POSTING_DT) != "01.01.0001 00:00")
                    {
                        ws.Cells[i + 6, 11].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].POSTING_DT);
                    }
                    ws.Cells[i + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    ws.Cells[i + 6, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 12].Value = m[i].INV_NO;
                    ws.Cells[i + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    ws.Cells[i + 6, 13].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    if (string.Format("{0:dd.MM.yyyy HH:mm}", m[i].INV_DT) != "01.01.0001 00:00")
                    {
                        ws.Cells[i + 6, 13].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].INV_DT);
                    }
                    ws.Cells[i + 6, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ws.Cells[i + 6, 14].Value = m[i].INV_POSTING_BY;

                    if (m[i].DELIVERY_STS == "Posted")
                    {
                        ws.Cells[i + 6, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                        ws.Cells[i + 6, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    }
                    else
                    {
                        ws.Cells[i + 6, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        ws.Cells[i + 6, 15].Value = m[i].MATDOC_NO2;
                        ws.Cells[i + 6, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        ws.Cells[i + 6, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        if (string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CANCEL_DT) != "01.01.0001 00:00")
                        {
                            ws.Cells[i + 6, 16].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CANCEL_DT);
                        }
                        ws.Cells[i + 6, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        ws.Cells[i + 6, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        ws.Cells[i + 6, 17].Value = m[i].PIC2;
                    }

                    i++;
                }

                #endregion

                try
                {
                    Byte[] bin = p.GetAsByteArray();
                    //string fn = "ServiceAcceptance-" + Guid.NewGuid().ToString() + "-" + AuthorizedUser.Username + ".xlsx";
                    string fn = "ServiceAcceptance - " + AuthorizedUser.Username + ".xlsx";
                    string DownloadDirectory = "Content/Download/ServiceAcceptance/";
                    string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
                    var pathfile = Path.Combine(downdir, fn);

                    excelLink = "Content/Download/ServiceAcceptance/" + fn + "";
                    successStatus = "True";
                    System.IO.File.WriteAllBytes(pathfile, bin);


                }
                catch (Exception err)
                {
                    //write log
                    successStatus = "False";
                }
                return Json(new { success = successStatus, excelLink = excelLink, count = m.Count() }, JsonRequestBehavior.AllowGet);


            }
        }

        private void DownloadFile(string fname)
        {
            bool forceDownload = true;
            string path = fname;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            string type = "";
            // set known types based on file extension  
            if (ext != null)
            {
                switch (ext.ToLower())
                {

                    case ".xlsx":
                    case ".xls":
                        type = "Application/msexcel";
                        break;
                }
            }
            if (forceDownload)
            {
                Response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            if (type != "")
                Response.ContentType = type;
            Response.WriteFile(path);
            Response.End();
        }
        #endregion



        public JsonResult SetApprove()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            string isCheckall = String.IsNullOrEmpty(Request.Params["CheckAll"]) ? "" : Request.Params["CheckAll"];
            
            //filter
            string DELIVERY_TYPE = String.IsNullOrEmpty(Request.Params["DeliveryType"]) ? "" : Request.Params["DeliveryType"];
            string LP_CD = String.IsNullOrEmpty(Request.Params["LogisticPartner"]) ? "" : Request.Params["LogisticPartner"];
            string ROUTE = String.IsNullOrEmpty(Request.Params["Route"]) ? "" : Request.Params["Route"];
            string STATUS = String.IsNullOrEmpty(Request.Params["Status"]) ? "" : Request.Params["Status"];
            string ROUTE_DATE = String.IsNullOrEmpty(Request.Params["RouteDate"]) ? "" : Request.Params["RouteDate"];
            string ROUTE_DATE_TO = String.IsNullOrEmpty(Request.Params["RouteDateTo"]) ? "" : Request.Params["RouteDateTo"];
            string PO_NO = String.IsNullOrEmpty(Request.Params["PoNo"]) ? "" : Request.Params["PoNo"];

            #region splitting the string if there's any delimiter on DELIVERY_TYPE
            string deliveryTypeParameter = string.Empty;
            if (!string.IsNullOrEmpty(DELIVERY_TYPE))
            {
                string[] deliveryTypeList = DELIVERY_TYPE.Split(';');
                foreach (string r in deliveryTypeList)
                {
                    string rs;
                    if (r == "Non TLMS")
                    {
                        rs = "N";
                    }
                    else if (r == "Regular")
                    {
                        rs = "R";
                    }
                    else
                    {
                        rs = "A";
                    }
                    deliveryTypeParameter += "'" + rs + "',";
                }
                deliveryTypeParameter = deliveryTypeParameter.Remove(deliveryTypeParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on LP_CD
            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on ROUTE
            string routeParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE))
            {
                string[] routeList = ROUTE.Split(';');
                foreach (string r in routeList)
                {
                    routeParameter += "'" + r + "',";
                }
                routeParameter = routeParameter.Remove(routeParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on STATUS

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(STATUS))
            {
                string[] statusList = STATUS.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            #endregion

            string routeDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE))
            {
                string d = ROUTE_DATE.Substring(0, 2);
                string m = ROUTE_DATE.Substring(3, 2);
                string y = ROUTE_DATE.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateParameter += "'" + tgl + "'";
            }

            string routeDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE_TO))
            {
                string d = ROUTE_DATE_TO.Substring(0, 2);
                string m = ROUTE_DATE_TO.Substring(3, 2);
                string y = ROUTE_DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateToParameter += "'" + tgl + "'";
            }

            string poNoParameter = string.Empty;
            if (!string.IsNullOrEmpty(PO_NO))
            {
                poNoParameter += PO_NO;
            }

            //end of filter

           
            //string 
            string DN = "";
            if (!string.IsNullOrEmpty(DELIVERY_NO))
            {
                string[] statusList = DELIVERY_NO.Split(',');
                foreach (string r in statusList)
                {
                    DN += r + ",";
                }
            }
            if (isCheckall=="true")
            {
                DN = DELIVERY_NO;
            }
            else
            {
                DN = DN.Remove(DN.Length - 1, 1);
                //statusParameter = statusParameter.Substring(1, statusParameter.Length - 3);
                DN = DN.Replace("\"", "");
                
            }
             
            //statusParameter = "'" + DELIVERY_NO + "'";
             
            List<ApprovalData> model = getApproval(DN, deliveryTypeParameter, logisticCDParameter, routeParameter, statusParameter, routeDateParameter, routeDateToParameter, poNoParameter, isCheckall).ToList();
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
        }


        protected List<ApprovalData> getApproval(string DELIVERY_NO, string DELIVERY_TYPE, string LP_CD, string ROUTE_CD, string STATUS, string DATEFROM, string DATETO, string PONO, string ischeckall)
        {
            string user = AuthorizedUser.Username;
            var l = db.Fetch<ApprovalData>("DLV_ServiceAcceptanceApproval", new object[] { 
                user,
                DELIVERY_NO,
                DELIVERY_TYPE,
                LP_CD,
                ROUTE_CD,
                STATUS,
                DATEFROM,
                DATETO,
                PONO,
                ischeckall
            });
            db.Close();
            return l.ToList();
        }

        public JsonResult SetCancel()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            string isCheckall = String.IsNullOrEmpty(Request.Params["CheckAll"]) ? "" : Request.Params["CheckAll"];

            //filter
            string DELIVERY_TYPE = String.IsNullOrEmpty(Request.Params["DeliveryType"]) ? "" : Request.Params["DeliveryType"];
            string LP_CD = String.IsNullOrEmpty(Request.Params["LogisticPartner"]) ? "" : Request.Params["LogisticPartner"];
            string ROUTE = String.IsNullOrEmpty(Request.Params["Route"]) ? "" : Request.Params["Route"];
            string STATUS = String.IsNullOrEmpty(Request.Params["Status"]) ? "" : Request.Params["Status"];
            string ROUTE_DATE = String.IsNullOrEmpty(Request.Params["RouteDate"]) ? "" : Request.Params["RouteDate"];
            string ROUTE_DATE_TO = String.IsNullOrEmpty(Request.Params["RouteDateTo"]) ? "" : Request.Params["RouteDateTo"];
            string PO_NO = String.IsNullOrEmpty(Request.Params["PoNo"]) ? "" : Request.Params["PoNo"];

            #region splitting the string if there's any delimiter on DELIVERY_TYPE
            string deliveryTypeParameter = string.Empty;
            if (!string.IsNullOrEmpty(DELIVERY_TYPE))
            {
                string[] deliveryTypeList = DELIVERY_TYPE.Split(';');
                foreach (string r in deliveryTypeList)
                {
                    string rs;
                    if (r == "Non TLMS")
                    {
                        rs = "N";
                    }
                    else if (r == "Regular")
                    {
                        rs = "R";
                    }
                    else
                    {
                        rs = "A";
                    }
                    deliveryTypeParameter += "'" + rs + "',";
                }
                deliveryTypeParameter = deliveryTypeParameter.Remove(deliveryTypeParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on LP_CD
            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on ROUTE
            string routeParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE))
            {
                string[] routeList = ROUTE.Split(';');
                foreach (string r in routeList)
                {
                    routeParameter += "'" + r + "',";
                }
                routeParameter = routeParameter.Remove(routeParameter.Length - 1, 1);
            }
            #endregion

            #region splitting the string if there's any delimiter on STATUS

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(STATUS))
            {
                string[] statusList = STATUS.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            #endregion

            string routeDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE))
            {
                string d = ROUTE_DATE.Substring(0, 2);
                string m = ROUTE_DATE.Substring(3, 2);
                string y = ROUTE_DATE.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateParameter += "'" + tgl + "'";
            }

            string routeDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE_TO))
            {
                string d = ROUTE_DATE_TO.Substring(0, 2);
                string m = ROUTE_DATE_TO.Substring(3, 2);
                string y = ROUTE_DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateToParameter += "'" + tgl + "'";
            }

            string poNoParameter = string.Empty;
            if (!string.IsNullOrEmpty(PO_NO))
            {
                poNoParameter += PO_NO;
            }

            //end of filter


            //string 
            string DN = "";
            if (!string.IsNullOrEmpty(DELIVERY_NO))
            {
                string[] statusList = DELIVERY_NO.Split(',');
                foreach (string r in statusList)
                {
                    DN += r + ",";
                }
            }
            if (isCheckall == "true")
            {
                DN = DELIVERY_NO;
            }
            else
            {
                DN = DN.Remove(DN.Length - 1, 1);
                //statusParameter = statusParameter.Substring(1, statusParameter.Length - 3);
                DN = DN.Replace("\"", "");

            }

            //statusParameter = "'" + DELIVERY_NO + "'";
            List<ApprovalData> model = getCancel(DN, deliveryTypeParameter, logisticCDParameter, routeParameter, statusParameter, routeDateParameter, routeDateToParameter, poNoParameter, isCheckall).ToList();
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
        }



        protected List<ApprovalData> getCancel(string DELIVERY_NO, string DELIVERY_TYPE, string LP_CD, string ROUTE_CD, string STATUS, string DATEFROM, string DATETO, string PONO, string ischeckall)
        {
            string user = AuthorizedUser.Username;
            var l = db.Fetch<ApprovalData>("DLV_ServiceAcceptanceCancelation", new object[] { 
                user,
                DELIVERY_NO,
                DELIVERY_TYPE,
                LP_CD,
                ROUTE_CD,
                STATUS,
                DATEFROM,
                DATETO,
                PONO,
                ischeckall
            });
            db.Close();
            return l.ToList();
        }

        

        public JsonResult saveNotes()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DeliveryNo"]) ? "" : Request.Params["DeliveryNo"];
            string SA_NOTES = String.IsNullOrEmpty(Request.Params["Notes"]) ? "" : Request.Params["Notes"];

            //statusParameter = "'" + DELIVERY_NO + "'";
            string user = AuthorizedUser.Username;
            var l = db.Fetch<note_sa>("DLV_ServiceAcceptanceSaveNotes", new object[] {
                user,
                DELIVERY_NO,
                SA_NOTES
            });

            string mstatus = (from q in l select q).FirstOrDefault().MSTATUS;
            string mnote = (from q in l select q).FirstOrDefault().MNOTE;

            db.Close();
            return Json(new { status = mstatus, message = mnote}, JsonRequestBehavior.AllowGet);
        }


        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadServiceAcceptance(
            string DELIVERY_NO = "",
            string DELIVERY_TYPE = "",
            string LP_CD = "",
            string ROUTE = "",
            string STATUS = "",
            string ROUTE_DATE = "",
            string ROUTE_DATE_TO = "",
            string PO_NO = "",
            string colDeliveryNo = "",
            string colDeliveryType = "",
            string colLPCode = "",
            string colLPName = "",
            string colRouteRate = "",
            string colPickupDate = "",
            string colStatus = "",
            string colSANote = "",
            string colArrival = "",
            string colDeparture = "",
            string colPONo = ""
            )
        {
            string fileName = String.Empty;

            // Update download user and status
            IEnumerable<ServiceAcceptanceDataDownload> qry;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;

            /*------COLUMN FILTER------*/
            string DeliveryNo = string.Empty;
            if (!string.IsNullOrEmpty(colDeliveryNo))
            {
                DeliveryNo += "'%" + colDeliveryNo + "%'";
            }

            string LPCode = string.Empty;
            if (!string.IsNullOrEmpty(colLPCode))
            {
                LPCode += "'%" + colLPCode + "%'";
            }

            string LPName = string.Empty;
            if (!string.IsNullOrEmpty(colLPName))
            {
                LPName += "'%" + colLPName + "%'";
            }

            string RouteRate = string.Empty;
            if (!string.IsNullOrEmpty(colRouteRate))
            {
                RouteRate += "'%" + colRouteRate + "%'";
            }

            string PickupDate = string.Empty;
            if (!string.IsNullOrEmpty(colPickupDate) || colPickupDate != "01.01.0100")
            {
                string d = colPickupDate.Substring(0, 2);
                string m = colPickupDate.Substring(3, 2);
                string y = colPickupDate.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                PickupDate += "'" + tgl + "'";
            }

            string Status = string.Empty;
            if (!string.IsNullOrEmpty(colStatus))
            {
                colStatus += "'%" + colStatus + "%'";
            }

            string SANote = string.Empty;
            if (!string.IsNullOrEmpty(colSANote))
            {
                SANote += "'%" + colSANote + "%'";
            }

            string ArrivalDate = string.Empty;
            if (!string.IsNullOrEmpty(colArrival) || colArrival != "01.01.0100")
            {
                string d = colArrival.Substring(0, 2);
                string m = colArrival.Substring(3, 2);
                string y = colArrival.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                ArrivalDate += "'" + tgl + "'";
            }

            string DepartureDate = string.Empty;
            if (!string.IsNullOrEmpty(colDeparture) || colDeparture != "01.01.0100")
            {
                string d = colDeparture.Substring(0, 2);
                string m = colDeparture.Substring(3, 2);
                string y = colDeparture.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                DepartureDate += "'" + tgl + "'";
            }
            /*-----END OF COLUMN FILTER------*/


            //IDBContext db = DbContext;
            string deliveryNoParameter = string.Empty;
            if (!string.IsNullOrEmpty(DELIVERY_NO))
            {
                deliveryNoParameter += "'%" + DELIVERY_NO + "%'";
            }

            string deliveryTypeParameter = string.Empty;
            if (!string.IsNullOrEmpty(DELIVERY_TYPE))
            {
                string[] deliveryTypeList = DELIVERY_TYPE.Split(';');
                foreach (string r in deliveryTypeList)
                {
                    string rs;
                    if (r == "Non TLMS")
                    {
                        rs = "N";
                    }
                    else if (r == "Regular")
                    {
                        rs = "R";
                    }
                    else
                    {
                        rs = "A";
                    }
                    deliveryTypeParameter += "'" + rs + "',";
                }
                deliveryTypeParameter = deliveryTypeParameter.Remove(deliveryTypeParameter.Length - 1, 1);
            }

            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            string routeParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE))
            {
                string[] routeList = ROUTE.Split(';');
                foreach (string r in routeList)
                {
                    routeParameter += "'" + r + "',";
                }
                routeParameter = routeParameter.Remove(routeParameter.Length - 1, 1);
            }

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(STATUS))
            {
                string[] statusList = STATUS.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            string routeDateParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE))
            {
                string d = ROUTE_DATE.Substring(0, 2);
                string m = ROUTE_DATE.Substring(3, 2);
                string y = ROUTE_DATE.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateParameter += "'" + tgl + "'";
            }

            string routeDateToParameter = string.Empty;
            if (!string.IsNullOrEmpty(ROUTE_DATE_TO))
            {
                string d = ROUTE_DATE_TO.Substring(0, 2);
                string m = ROUTE_DATE_TO.Substring(3, 2);
                string y = ROUTE_DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                routeDateToParameter += "'" + tgl + "'";
            }

            string poNoParameter = string.Empty;
            if (!string.IsNullOrEmpty(PO_NO))
            {
                poNoParameter += "'%" + PO_NO + "%'";
            }

            qry = db.Fetch<ServiceAcceptanceDataDownload>(
               "DLV_ServiceAcceptanceDataDownload",
                new object[] { 
                        deliveryNoParameter,
                        deliveryTypeParameter,
                        logisticCDParameter,
                        routeParameter,
                        statusParameter,
                        routeDateParameter,
                        routeDateToParameter,
                        poNoParameter,
                        DeliveryNo,
                        LPCode,
                        LPName,
                        RouteRate,
                        PickupDate,
                        Status,
                        SANote,
                        ArrivalDate,
                        DepartureDate
                   });
            string dt = Convert.ToString(DateTime.Now);
            dt = dt.Replace(" ", "");
            dt = dt.Replace("/", "");
            dt = dt.Replace(":", "");

            //fileName = "GoodsReceiptInquiry" + arrivalTimeFrom.Replace("-", "") + arrivalTimeTo.Replace("-", "") + dockCode.Trim().ToUpper() + supplierCode.Trim().ToUpper() + rcvPlantCode.Trim().ToUpper() + routeRate.Trim().ToUpper() + manifestNo.Trim() + manifestReceiveFlag.Trim() + orderNo.Trim() + ".xls";
            fileName = "ServiceAcceptance_" + dt + "_" + AuthorizedUser.Username + ".xls";
            hasil = exporter.Write(ConvertToDataTable(qry), fileName.Replace("_", " "));


            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
    }

}
        #endregion