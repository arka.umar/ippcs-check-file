﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
//using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;
using Portal.Models.Delivery.DeliveryPurchaseRequest;

//using Toyota.Common.Web.Task;
//using Toyota.Common.Task;
//using Toyota.Common.Task.External;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;

namespace Portal.Controllers.Delivery.PurchaseRequest
{
    public class D31102Controller : BaseController
    {
        //
        // GET: /D31102/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public DeliveryPurchaseRequestData modelExport;

        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public D31102Controller()
            : base("Delivery Purchase Requisition Creation")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DeliveryPurchaseRequestModel mdl = new DeliveryPurchaseRequestModel();

            //ViewData["Status"] = GetAllManifestStatus();

            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();
            ViewData["StatusLookup"] = GetStatus();
            ViewData["RouteLookup"] = GetRoute();
            Model.AddModel(mdl);

        }

        public ActionResult PartialHeader()
        {
            return PartialView("IndexHeader");
        }

        //GRID DATA
        public ActionResult PartialListData()
        {
            string PR_PO_MONTH = String.IsNullOrEmpty(Request.Params["PRMonth"]) ? "" : Request.Params["PRMonth"];
            //string PR_NO = String.IsNullOrEmpty(Request.Params["PRNo"]) ? "" : Request.Params["PRNo"];
            string LP_CD = String.IsNullOrEmpty(Request.Params["LogisticPartnerOption"]) ? "" : Request.Params["LogisticPartnerOption"];
            string STATUS = String.IsNullOrEmpty(Request.Params["StatusOption"]) ? "" : Request.Params["StatusOption"];
            
            DeliveryPurchaseRequestModel mdl = Model.GetModel<DeliveryPurchaseRequestModel>();
            mdl.DeliveryPurchaseRequestListData = GetDeliveryPurchaseRequest(PR_PO_MONTH, LP_CD, STATUS);
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        public List<DeliveryPurchaseRequestData> GetDeliveryPurchaseRequest(string PR_PO_MONTH, string LP_CD, string STATUS)
        {
            string PrPoMonthParameter = string.Empty;

            if (!string.IsNullOrEmpty(PR_PO_MONTH))
            {
                string[] PrPoMonthList = PR_PO_MONTH.Split(';');

                foreach (string r in PrPoMonthList)
                {
                    PrPoMonthParameter += "'" + r + "',";
                }
                PrPoMonthParameter = PrPoMonthParameter.Remove(PrPoMonthParameter.Length - 1, 1);
            }

            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(STATUS))
            {
                string[] statusList = STATUS.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            List<DeliveryPurchaseRequestData> listOfDLV_PR = db.Fetch<DeliveryPurchaseRequestData>("DLV_getGridPR", new object[] { 
                PrPoMonthParameter,
                logisticCDParameter,
                statusParameter
            });
            return listOfDLV_PR;

        }

        #region CREATE PR
        public ContentResult CreatePR(string PR_PO_MONTH, string NOTES, string GridId)
        {
            //bool result = true;
            int isError = 0;
            string errorMessage = "";


            GridId.Replace('"', ' ');
            char[] splitChar = { ';' };
            char[] SplitLastChar = { '|' };
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string processID = "";
            ParamUpdate = GridId.Split(splitChar);

            

            string PrPoMonthParameter = string.Empty;

            if (!string.IsNullOrEmpty(PR_PO_MONTH))
            {
                string[] PrPoMonthList = PR_PO_MONTH.Split(';');

                foreach (string r in PrPoMonthList)
                {
                    PrPoMonthParameter += "" + r + ",";
                }
                PrPoMonthParameter = PrPoMonthParameter.Remove(PrPoMonthParameter.Length - 1, 1);
            }

            string NotesParameter = string.Empty;

            if (!string.IsNullOrEmpty(NOTES))
            {
                string[] NotesList = NOTES.Split(';');

                foreach (string r in NotesList)
                {
                    NotesParameter += "" + r + ",";
                }
                NotesParameter = NotesParameter.Remove(NotesParameter.Length - 1, 1);
            }



            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);          

            try
            {
                //for (int i = 0; i < ParamUpdate.Length; i++)
                //{
                //    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                //    string prodMonth = ParamUpdateId[0].ToString();
                //    string lp_cd = ParamUpdateId[1].ToString();
                //    string route_cd = ParamUpdateId[2].ToString();
                //    string source = ParamUpdateId[3].ToString();

                //    db.Execute("DLV_PR_UpdateProcessID", new object[] {
                //        prodMonth,lp_cd,route_cd,source, processID, AuthorizedUser.Username
                //    });
                //}


                processID = db.ExecuteScalar<string>("DLV_GenProcessID", new object[] { 
                                "Purchase Request Creation Starting",
                                AuthorizedUser.Username, 
                                "DeliveryPurchaseRequest.CreatePR",
                                "",
                                "MPCS00002INF", "INF", "3", "31103",     // function
                                "0"      
                            });

                db.Execute("DLV_CreatePR", new object[] {
                        PrPoMonthParameter,NotesParameter, AuthorizedUser.Username, GridId, processID
                    });


                errorMessage = "PR Creation process has been finished.";

            }
            catch (Exception exc)
            {
                db.Execute("DLV_Update_Queue", new object[] { processID, "QU4" });

                isError = 1;
                errorMessage = exc.Message;
            }
            db.Close();
            db.Dispose();
            
            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }
        # endregion



        public ContentResult AddingDeliveryPurchaseRequest(string PR_PO_MONTH, string ROUTE_CD, string LP_CD, string PR_QTY, string SOURCE)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string errorMessage = "";

            //try
            //{
            //    //---=== Check if exists on
            //    var QueryLog = db.SingleOrDefault<CheckResult>("DLV_CheckTblRetrieval", new object[] { 
            //        PR_PO_MONTH, ROUTE_CD, LP_CD, SOURCE
            //    });

            //    if (QueryLog.result == 1)
            //    {
            //        isError = 1;
            //        errorMessage = "Route is already exist, can not add manual PR";
            //    }
            //    else
            //    {

            //        db.Execute("DLV_SetPR", new object[] {
            //            PR_PO_MONTH, ROUTE_CD, LP_CD, SOURCE, PR_QTY, AuthorizedUser.Username
            //        });
            //        errorMessage = "Success to add manual PR";
            //    }

            //}
            //catch (Exception exc)
            //{
            //    isError = 1;
            //    errorMessage = exc.Message;
            //}

            //return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);

            try
            {
                db.Execute("DLV_PR_AddManual", new object[] { 
                    PR_PO_MONTH, ROUTE_CD, LP_CD, SOURCE, PR_QTY, AuthorizedUser.Username
                });
                errorMessage = "Success to add manual PR";
            }
            catch (Exception ex)
            {
                isError = 1;
                errorMessage = ex.Message;
            }

            db.Close();
            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }

        public ContentResult UpdateDeliveryPurchaseRequest(string PR_PO_MONTH, string ROUTE_CD, string LP_CD, string SOURCE, string PR_QTY)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;
            string errorMessage = "";

            try
            {
                db.Execute("DLV_UpdatePR", new object[] {
                    //PR_PO_MONTH, ROUTE_CD, LP_CD, PR_QTY,SOURCE, AuthorizedUser.Username
                    PR_PO_MONTH, ROUTE_CD, LP_CD, PR_QTY,SOURCE, AuthorizedUser.Username
                });
                errorMessage = "PR has been updated.";

            }
            catch (Exception exc)
            {
                isError = 1;
                errorMessage = exc.Message;
            }

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }

        public ContentResult DeleteDeliveryPurchaseRequest(string GridId)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            int isError = 0;


            char[] splitChar = { ';' };
            char[] SplitLastChar = { '|' };
            string[] ParamUpdate;
            string[] ParamUpdateId;
            string result = "Success| PR item deleted.";
            ParamUpdate = GridId.Split(splitChar);
            int total = ParamUpdate.Length-1;
            string processID = ""; 
            try
            {
                //for (int i = 0; i < total; i++)
                //{
                //    ParamUpdateId = ParamUpdate[i].ToString().Split(SplitLastChar);
                //    string prodMonth    = ParamUpdateId[0].ToString();
                //    string lp_cd        = ParamUpdateId[1].ToString();
                //    string route_cd     = ParamUpdateId[2].ToString();
                //    string source       = ParamUpdateId[3].ToString();
                //    string pr_qty       = ParamUpdateId[4].ToString();

                //    //db.Execute("DLV_DeletePR", new object[] { 
                //    //    //Prod_month, lp_cd, route, source, pr_qty
                //    //    //prodMonth, lp_cd, route_cd, source, pr_qty
                //    //    GridId
                //    //});
                    
                //}


                processID = db.ExecuteScalar<string>("DLV_GenProcessID", new object[] { 
                                "Delete Purchase Request",
                                AuthorizedUser.Username, 
                                "DeliveryPurchaseRequest.DeletePR",
                                "",
                                "MPCS00002INF", "INF", "3", "31102",     // function
                                "0"      
                            });


                db.Execute("DLV_DeletePR", new object[] { 
                    GridId, AuthorizedUser.Username, processID
                });
                result = "Success to delete PR.";
            }
            catch (Exception ex)
            {

                db.Execute("DLV_Update_Queue", new object[] { processID, "QU4" });

                isError = 1;
                result = ex.Message;
            }

            db.Close();
            return Content(((isError == 1) ? "Error|" : "Success|") + result);
        }



        //LOOK UP
        #region LOOK UP



        //GETTING Logistic partner FOR LOOKUP

        public ActionResult LogisticPartnerLookup()
        {

            TempData["GridName"] = "LogisticPartnerOption";
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            //return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerLookup"]);
            return PartialView("PG", ViewData["LogisticPartnerLookup"]);
        }
        public ActionResult LPLookupP()
        {

            TempData["GridName"] = "LP_OptionP";
            ViewData["LogisticPartnerLookup"] = GetLogisticPartner();

            //return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerLookup"]);
            return PartialView("PG", ViewData["LogisticPartnerLookup"]);
        }
        public List<DeliveryPurchaseRequestDataLogPartner> GetLogisticPartner()
        {
            List<DeliveryPurchaseRequestDataLogPartner> listLogisticPartner = new List<DeliveryPurchaseRequestDataLogPartner>();
            try
            {
                listLogisticPartner = db.Fetch<DeliveryPurchaseRequestDataLogPartner>("DLV_getLP", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listLogisticPartner;
        }

        //GETTING Route FOR LOOKUP

        public ActionResult OriRouteLookup()
        {

            TempData["GridName"] = "OriRouteP";
            ViewData["RouteLookup"] = GetRoute();

            return PartialView("PG", ViewData["RouteLookup"]);
        }

        public ActionResult ConvertedRouteLookup()
        {

            TempData["GridName"] = "ConvertedRouteP";
            ViewData["RouteLookup"] = GetRoute();

            return PartialView("PG", ViewData["RouteLookup"]);
        }

        public List<DeliveryPurchaseRequestDataRoute> GetRoute()
        {
            List<DeliveryPurchaseRequestDataRoute> listRoute = new List<DeliveryPurchaseRequestDataRoute>();
            try
            {
                listRoute = db.Fetch<DeliveryPurchaseRequestDataRoute>("DLV_getRoute", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listRoute;
        }


        //GETTING Status FOR LOOKUP

        public ActionResult StatusLookup()
        {

            TempData["GridName"] = "StatusOption";
            ViewData["StatusLookup"] = GetStatus();

            //return PartialView("GridLookup/PartialGrid", ViewData["StatusLookup"]);
            return PartialView("PG", ViewData["StatusLookup"]);
        }

        public List<DeliveryPurchaseRequestDataStatus> GetStatus()
        {
            List<DeliveryPurchaseRequestDataStatus> listStatus = new List<DeliveryPurchaseRequestDataStatus>();
            try
            {
                listStatus = db.Fetch<DeliveryPurchaseRequestDataStatus>("DLV_getPRStatus", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            db.Close();
            return listStatus;
        }

        #endregion

        //GETTING MESSAGE BOX
        public ActionResult MessageBox()
        {
            return PartialView("MessageBox");
        }


        #region Export To Excel

        protected List<DeliveryPurchaseRequestData> GetDataDLVPurchaseRequest(
            string PR_PO_MONTH, string LP_CD, string STATUS,
            string CHANGED_BY, string CHANGED_DT, string CREATED_BY, string CREATED_DT,
            string LP_CD2, string LP_NM, string QTY, string ROUTE_CD, string ROUTE_NM, string SOURCE, string STATUS2
            )
        {

            string logisticCDParameter = string.Empty;
            if (!string.IsNullOrEmpty(LP_CD))
            {
                string[] logisticCDList = LP_CD.Split(';');
                foreach (string r in logisticCDList)
                {
                    logisticCDParameter += "'" + r + "',";
                }
                logisticCDParameter = logisticCDParameter.Remove(logisticCDParameter.Length - 1, 1);
            }

            string statusParameter = string.Empty;
            if (!string.IsNullOrEmpty(STATUS))
            {
                string[] statusList = STATUS.Split(';');
                foreach (string r in statusList)
                {
                    statusParameter += "'" + r + "',";
                }
                statusParameter = statusParameter.Remove(statusParameter.Length - 1, 1);
            }

            List<DeliveryPurchaseRequestData> listOfDLV_PR = db.Fetch<DeliveryPurchaseRequestData>("DLV_GetDataPR", new object[] { 
                PR_PO_MONTH, 
                logisticCDParameter, statusParameter,
                //LP_CD, STATUS,
                CHANGED_BY, CHANGED_DT, CREATED_BY, CREATED_DT,
                LP_CD2,	LP_NM, QTY,	ROUTE_CD, ROUTE_NM,	SOURCE, STATUS2
                
            });
            return listOfDLV_PR;

        }


        #region exportExcel
        public ActionResult exportToExcel(string PR_PO_MONTH, string LP_CD, string STATUS,
            string CHANGED_BY, string CHANGED_DT, string CREATED_BY, string CREATED_DT,
            string LP_CD2, string LP_NM, string QTY, string ROUTE_CD, string ROUTE_NM, string SOURCE, string STATUS2)
        {
            //string PR_PO_MONTH = String.IsNullOrEmpty(Request.Params["PRMonth"]) ? "" : Request.Params["PRMonth"];
            //string LP_CD = String.IsNullOrEmpty(Request.Params["LogisticPartnerOption"]) ? "" : Request.Params["LogisticPartnerOption"];
            //string STATUS = String.IsNullOrEmpty(Request.Params["StatusOption"]) ? "" : Request.Params["StatusOption"];

            //check QUEUE
            //var excelLink = "";
            string successStatus = "";
            String processID = "";

            try
            {
                //---=== Check 
                var QueryLog = db.SingleOrDefault<CheckResult>("DLV_CheckQueue", new object[] {});

                if (QueryLog.result == 1)
                {
                    var excelLink = "Other process is still running by another user";
                    successStatus = "Other process is still running by another user";
                    return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    #region create excel

                    DeliveryPurchaseRequestModel mdl = Model.GetModel<DeliveryPurchaseRequestModel>();
                    mdl.DeliveryPurchaseRequestListData = GetDataDLVPurchaseRequest(PR_PO_MONTH, LP_CD, STATUS,
                        CHANGED_BY, CHANGED_DT, CREATED_BY, CREATED_DT,
                        LP_CD2, LP_NM, QTY, ROUTE_CD, ROUTE_NM, SOURCE, STATUS2);

                    var mc = mdl.DeliveryPurchaseRequestListData.ToList<DeliveryPurchaseRequestData>();
                    int? totals = mc.Count;

                    processID = db.ExecuteScalar<string>("DLV_GenProcessID", new object[] { 
                                    "Download data PR is started",
                                    AuthorizedUser.Username, 
                                    "DeliveryPurchaseRequest.DownloadExcel",
                                    "",
                                    "MPCS00002INF", "INF", "3", "31102",     // function
                                    "0"      
                                });

                    db.Execute("DLV_Insert_Queue", new object[] { processID, "3", "31102", "QU1", AuthorizedUser.Username });

                    if (totals == 0)
                    {

                        processID = db.ExecuteScalar<string>("GenerateProcessId", new object[] { 
                                    "Download data PR is finished",
                                    AuthorizedUser.Username, 
                                    "DeliveryPurchaseRequest.DownloadExcel",
                                    processID,
                                    "MPCS00002INF", "INF", "3", "31102",     // function
                                    "0"      
                                });

                        db.Execute("DLV_Update_Queue", new object[] { processID, "QU3" });

                        successStatus = "False";
                        var excelLink = "Data not found!";
                        return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        using (ExcelPackage p = new ExcelPackage())
                        {
                            p.Workbook.Worksheets.Add("Delivery Purchase Requisition");
                            ExcelWorksheet ws = p.Workbook.Worksheets[1];
                            ws.Name = "Delivery Purchase Requisition";
                            ws.Cells.Style.Font.Size = 11;
                            ws.Cells.Style.Font.Name = "Calibri";
                            //header
                            string excelLink = "";
                            int columncount = 12;
                            //string successStatus = "";
                            ws.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA"; // Heading Name
                            ws.Cells[1, 1, 1, columncount].Merge = true; //Merge columns start and end range
                            ws.Cells[1, 1, 1, columncount].Style.Font.Bold = true; //Font should be bold
                            ws.Cells[1, 1, 1, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                            ws.Cells[2, 1].Value = "Delivery Purchase Requisition"; // Heading Name
                            ws.Cells[2, 1, 2, columncount].Merge = true; //Merge columns start and end range
                            ws.Cells[2, 1, 2, columncount].Style.Font.Bold = true; //Font should be bold
                            ws.Cells[2, 1, 2, columncount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Aligmnet is center

                            //set header kolom
                            #region kolom

                            //ws.Cells[4, 1].AutoFitColumns();
                            ws.Cells.AutoFitColumns(20, 40);
                            ws.Cells[4, 1].Value = "Production Month";
                            ws.Cells[4, 1].Style.Font.Bold = true;
                            ws.Cells[4, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 1, 5, 1].Merge = true;
                            ws.Cells[4, 1, 5, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                            ws.Cells[4, 2].Value = "Route";
                            //ws.Cells[4, 2].AutoFitColumns();
                            ws.Cells[4, 2].Style.Font.Bold = true;
                            ws.Cells[4, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 2, 4, 3].Merge = true;
                            ws.Cells[4, 2, 4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                            ws.Cells[5, 2].Value = "Code";
                            ws.Cells[5, 2].AutoFitColumns();
                            ws.Cells[5, 2].Style.Font.Bold = true;
                            ws.Cells[5, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[5, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                            ws.Cells[5, 3].Value = "Name";
                            ws.Cells[5, 3].AutoFitColumns(50, 160);
                            ws.Cells[5, 3].Style.Font.Bold = true;
                            ws.Cells[5, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[5, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                            ws.Cells[4, 4].Value = "Trucking";
                            ws.Cells[4, 4].AutoFitColumns();
                            ws.Cells[4, 4].Style.Font.Bold = true;
                            ws.Cells[4, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[4, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 4, 4, 5].Merge = true;
                            ws.Cells[4, 4, 4, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                            ws.Cells[5, 4].Value = "Code";
                            ws.Cells[5, 4].AutoFitColumns();
                            ws.Cells[5, 4].Style.Font.Bold = true;
                            ws.Cells[5, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[5, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


                            ws.Cells[5, 5].Value = "Name";

                            ws.Cells[5, 5].AutoFitColumns(50, 150);
                            ws.Cells[5, 5].Style.Font.Bold = true;
                            ws.Cells[5, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[5, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                            ws.Cells[4, 6].Value = "Qty";
                            ws.Cells[4, 6].AutoFitColumns();
                            ws.Cells[4, 6].Style.Font.Bold = true;
                            ws.Cells[4, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 6, 5, 6].Merge = true;
                            ws.Cells[4, 6, 5, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                            ws.Cells[4, 7].Value = "Source";
                            //ws.Cells[4, 7].AutoFitColumns();
                            ws.Cells[4, 7].Style.Font.Bold = true;
                            ws.Cells[4, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 7, 5, 7].Merge = true;
                            ws.Cells[4, 7, 5, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                            ws.Cells[4, 8].Value = "Status";
                            //ws.Cells[4, 8].AutoFitColumns();
                            ws.Cells[4, 8].Style.Font.Bold = true;
                            ws.Cells[4, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[4, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 8].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 8, 5, 8].Merge = true;
                            ws.Cells[4, 8, 5, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                            ws.Cells[4, 9].Value = "Created";
                            //ws.Cells[4, 9].AutoFitColumns();
                            ws.Cells[4, 9].Style.Font.Bold = true;
                            ws.Cells[4, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 9, 4, 10].Merge = true;
                            ws.Cells[4, 9, 4, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                            ws.Cells[5, 9].Value = "By";
                            //ws.Cells[5, 9].AutoFitColumns();
                            ws.Cells[5, 9].Style.Font.Bold = true;
                            ws.Cells[5, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 9].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                            ws.Cells[5, 10].Value = "Date";
                            //ws.Cells[5, 10].AutoFitColumns();
                            ws.Cells[5, 10].Style.Font.Bold = true;
                            ws.Cells[5, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 10].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                            ws.Cells[4, 11].Value = "Changed";
                            //ws.Cells[4, 11].AutoFitColumns();
                            ws.Cells[4, 11].Style.Font.Bold = true;
                            ws.Cells[4, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[4, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[4, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[4, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            ws.Cells[4, 11, 4, 12].Merge = true;
                            ws.Cells[4, 11, 4, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                            ws.Cells[5, 11].Value = "By";
                            //ws.Cells[5, 11].AutoFitColumns();
                            ws.Cells[5, 11].Style.Font.Bold = true;
                            ws.Cells[5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 11].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                            ws.Cells[5, 12].Value = "Date";
                            //ws.Cells[5, 12].AutoFitColumns();
                            ws.Cells[5, 12].Style.Font.Bold = true;
                            ws.Cells[5, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[5, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            ws.Cells[5, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[5, 12].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            #endregion

                            #region detailnya

                            List<DeliveryPurchaseRequestData> m = mdl.DeliveryPurchaseRequestListData.ToList<DeliveryPurchaseRequestData>();

                            int i = 0;
                            foreach (DeliveryPurchaseRequestData x in m)
                            {


                                string changedBy = m[i].CHANGED_DT.ToString();
                                var changedBy1 = "";

                                if (changedBy == "1/1/0001 12:00:00 AM")
                                {
                                    changedBy1 = "";
                                }
                                else
                                {
                                    changedBy1 = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CHANGED_DT);
                                }


                                ws.Cells[i + 6, 1].Value = m[i].PROD_MONTH;
                                // ws.Cells[i + 6, 1].AutoFitColumns();
                                ws.Cells[i + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                ws.Cells[i + 6, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 2].Value = m[i].ROUTE_CD;
                                ws.Cells[i + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //ws.Cells[i + 6, 3].AutoFitColumns();
                                ws.Cells[i + 6, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 3].Value = m[i].ROUTE_NAME;
                                //ws.Cells[i + 6, 4].AutoFitColumns();
                                ws.Cells[i + 6, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 4].Value = m[i].LP_CD;
                                ws.Cells[i + 6, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //ws.Cells[i + 6, 5].AutoFitColumns();
                                ws.Cells[i + 6, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 5].Value = m[i].LP_NAME;
                                //ws.Cells[i + 6, 5].AutoFitColumns();
                                ws.Cells[i + 6, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 6].Value = m[i].PR_QTY;
                                ws.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                //ws.Cells[i + 6, 6].AutoFitColumns();
                                ws.Cells[i + 6, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 7].Value = m[i].SOURCE;
                                //ws.Cells[i + 6, 6].AutoFitColumns();
                                ws.Cells[i + 6, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 8].Value = m[i].STATUS;
                                ws.Cells[i + 6, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 9].Value = m[i].CREATED_BY;
                                //ws.Cells[i + 6, 7].AutoFitColumns();
                                ws.Cells[i + 6, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 10].Value = string.Format("{0:dd.MM.yyyy HH:mm}", m[i].CREATED_DT);
                                //ws.Cells[i + 6, 8].AutoFitColumns();
                                ws.Cells[i + 6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                ws.Cells[i + 6, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                ws.Cells[i + 6, 11].Value = m[i].CHANGED_BY;
                                //ws.Cells[i + 6, 7].AutoFitColumns();
                                ws.Cells[i + 6, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);



                                ws.Cells[i + 6, 12].Value = changedBy1;
                                //ws.Cells[i + 6, 8].AutoFitColumns();
                                ws.Cells[i + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                ws.Cells[i + 6, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                i++;
                            }

                            #endregion

                            try
                            {

                                

                                Byte[] bin = p.GetAsByteArray();
                                //string fn = "DeliveryPurchaseRequisition-" + Guid.NewGuid().ToString() + "-" + AuthorizedUser.Username + ".xlsx";
                                string fn = "DeliveryPurchaseRequisition-" + AuthorizedUser.Username + ".xlsx";
                                string DownloadDirectory = "Content/Download/DeliveryPurchaseRequest/";
                                string downdir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
                                var pathfile = Path.Combine(downdir, fn);

                                excelLink = "Content/Download/DeliveryPurchaseRequest/" + fn + "";
                                successStatus = "True";
                                System.IO.File.WriteAllBytes(pathfile, bin);

                                processID = db.ExecuteScalar<string>("GenerateProcessId", new object[] { 
                                    "Download data PR is finished",
                                    AuthorizedUser.Username, 
                                    "DeliveryPurchaseRequest.DownloadExcel",
                                    processID,
                                    "MPCS00002INF", "INF", "3", "31102",     // function
                                    "0"      
                                });

                                db.Execute("DLV_Update_Queue", new object[] { processID, "QU3" });
                                

                            }
                            catch (Exception err)
                            {

                                //write log
                                successStatus = "False";
                            }
                            return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);


                        }


                    }
                    #endregion
 
                }

            }
            catch (Exception exc)
            {
                

                var excelLink = exc.Message;
                successStatus = exc.Message;
                return Json(new { success = successStatus, excelLink = excelLink }, JsonRequestBehavior.AllowGet);
            }

                
            
        }
        #endregion



        private void DownloadFile(string fname)
        {
            bool forceDownload = true;
            string path = fname;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            string type = "";
            // set known types based on file extension  
            if (ext != null)
            {
                switch (ext.ToLower())
                {

                    case ".xlsx":
                    case ".xls":
                        type = "Application/msexcel";
                        break;
                }
            }
            if (forceDownload)
            {
                Response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            if (type != "")
                Response.ContentType = type;
            Response.WriteFile(path);
            Response.End();
        }
        #endregion



        #region PERFORM RETRIEVE v2
        public ContentResult PerformRetrievePR(string PR_PO_MONTH, string PR_PO_MONTH_LAST)
        {

            int isError = 0;
            string errorMessage = "";
            
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //var QueryLog = db.ExecuteScalar<getPid>("RetrieveDataPR", new object[] { PrPoMonthParameter, PrPoMonthLastParameter, AuthorizedUser.Username });
                
                db.Execute("DLV_RetrieveDataPR", new object[] { PR_PO_MONTH, PR_PO_MONTH_LAST, AuthorizedUser.Username });

                errorMessage = "Synchronize finished";

            }
            catch (Exception ex)
            {
                //errorMessage = "Failed " + ex.Message;
                isError = 1;
                errorMessage = ex.Message;
            }
            db.Close();
            db.Dispose();

            //return Content(errorMessage);
            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
        }
        # endregion

        #region CheckProgress
        public ContentResult CheckProgress()
        {
            int isError = 0;
            //int msg = 0;
            //string status = "";
            string message = "";

            try
            {
                //---=== Check 
                var QueryLog = db.SingleOrDefault<CheckResult>("DLV_CheckProcessRetrieve", new object[] { });
                
                if (QueryLog.result != 1)
                {
                    isError = 0;
                    message = "Checking process. Please wait...|Finish";
                }
                else
                {
                    isError = 0;
                    var QLog = db.SingleOrDefault<CheckProgress>("DLV_CheckMsgRetrieve", new object[] { });

                    if ((QLog.messageLog == "Get data from PR Carry Over started.") || 
                       (QLog.messageLog == "Delete data from table  TB_T_PR_RETRIEVAL.") ||
                       (QLog.messageLog == "Synchronize process started."))
                    {
                        message = "Retrieval on progress 1 of 3 item.|Process";
                    }
                    else if (QLog.messageLog == "Get data from TLMS started.")
                    {
                        message = "Retrieval on progress 2 of 3 item.|Process";
                    }
                    else if (QLog.messageLog == "Get data from Non-TLMS started.")
                    {
                        message = "Retrieval on progress 3 of 3 item.|Process";
                    }
                    else
                    {
                        message = "Retrieval data has been finished.|Finish";
                    }
                    //message = "Checking process. Please wait...";
                }

            }
            catch (Exception exc)
            {
                isError = 1;
                message = "Checking process error.|End";
                //message = "Error";
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + message);
        }
        #endregion

        #region checkProgressCreatePR
        public ContentResult checkProgressCreatePR()
        {
            int isError = 0;
            //int msg = 0;
            //string status = "";
            string message = "";

            try
            {
                //---=== Check 
                //var QueryLog = db.SingleOrDefault<CheckResult>("DLV_CheckProcessRetrieve", new object[] { });

                //if (QueryLog.result != 1)
                //{
                //    isError = 1;
                //    message = "Checking process. Please wait...|Finish";
                //}
                //else
                //{
                    isError = 0;
                    var QLog = db.SingleOrDefault<CheckProgress>("DLV_CheckMsgCreatePR", new object[] { });

                    if (QLog.messageLog == "PR Creation process starting.")
                    {
                        message = "PR Creation process starting.";
                    }
                    else if ((QLog.messageLog == "PR creation has been finished.") ||
                       (QLog.messageLog == "Email notification failed sent to all PIC.") ||
                       (QLog.messageLog == "send e-mail has been finished."))
                    {
                        message = "PR creation has been finished.";
                    }
                    else if (QLog.messageLog == "PR already created.")
                    {
                        message = "PR already created.";
                    }
                    else if (QLog.messageLog == "Cannot create PR with this production month.")
                    {

                        message = QLog.messageLog;
                    }
                    else
                    {
                        message = "Verifying. Please wait...";
                    }
                    //message = QLog.messageLog;
                //}

            }
            catch (Exception exc)
            {
                isError = 1;
                message = "Checking process error.|End";
                //message = "Error";
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + message);
        }
        #endregion

        public JsonResult getProgress()
        {
            string SEQ = String.IsNullOrEmpty(Request.Params["SEQ"]) ? "" : Request.Params["SEQ"];
            string PID = String.IsNullOrEmpty(Request.Params["PID"]) ? "" : Request.Params["PID"];
            List<getLog> progress = db.Fetch<getLog>("DLV_PR_getProgress", new object[] { SEQ, PID });
            return Json(new { data = progress }, JsonRequestBehavior.AllowGet);
        }

        #region PERFORM RETRIEVE V1
        public ContentResult getDataCaryOver(string PR_PO_MONTH, string PR_PO_MONTH_LAST)
        {

            int isError = 0;
            string errorMessage = "";            

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("DLV_getDataCaryOver", new object[] { PR_PO_MONTH, PR_PO_MONTH_LAST, AuthorizedUser.Username });
                
                errorMessage = "success";

            }
            catch (Exception ex)
            {
                isError = 1;
                errorMessage = "Failed " + ex.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
            //return Content(errorMessage);
        }


        public ContentResult getDataTLMS(string PR_PO_MONTH, string PR_PO_MONTH_LAST)
        {
            int isError = 0;
            string errorMessage = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("DLV_getDataTLMS", new object[] { PR_PO_MONTH, PR_PO_MONTH_LAST, AuthorizedUser.Username });
                errorMessage = "success";

            }
            catch (Exception ex)
            {
                isError = 1;
                errorMessage = "Failed " + ex.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
            //return Content(errorMessage);
        }

        public ContentResult getDataNonTLMS(string PR_PO_MONTH, string PR_PO_MONTH_LAST)
        {
            int isError = 0;
            string errorMessage = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("DLV_getDataNonTLMS", new object[] { PR_PO_MONTH, PR_PO_MONTH_LAST, AuthorizedUser.Username });
                errorMessage = "success";

            }
            catch (Exception ex)
            {
                isError = 1;
                errorMessage = "Failed " + ex.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
            //return Content(errorMessage);
        }



        public ContentResult deleteDataRetrieval(string PR_PO_MONTH)
        {

            int isError = 0;
            string errorMessage = "";

           
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("DLV_PR_DeleteDataRetrieval", new object[] { PR_PO_MONTH });
                errorMessage = "success";

            }
            catch (Exception ex)
            {
                isError = 1;
                errorMessage = "Failed " + ex.Message;
            }
            db.Close();
            db.Dispose();

            return Content(((isError == 1) ? "Error|" : "Success|") + errorMessage);
            //return Content(errorMessage);
        }
        # endregion


    }
}
