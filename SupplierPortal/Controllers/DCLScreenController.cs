﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;

using Portal.Models.DCLScreen;
using Toyota.Common.Web.Credential;
//using Toyota.Common.Web.Notification;

/*
 * niit.hendra
 * 
 */

namespace Portal.Controllers
{
    public class DCLScreenController : BaseController
    {
         
        public DCLScreenController():base("DCL Inquiry")
        {
            
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        { 
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = true;

            DCLScreenModel mdls = new DCLScreenModel();
            mdls.DCLScreenMain = DCLget();
            mdls.DCLScreenAddMain = DCLGetAddMaster();
            Model.AddModel(new Search { });
            mdls.Combo = dclGetCombo();
            Model.AddModel(mdls);

            
        }

        protected List<DCLScreenCombo> dclGetCombo()
        {
            DCLScreenModel mdl = new DCLScreenModel();
            List<DCLScreenCombo> combo = new List<DCLScreenCombo>();
            for (int i = 1; i <= 5; i++)
            {
                combo.Add(new DCLScreenCombo()
                {
                    LPCode = "MML" + i,
                    LPName = "Mitra mandiri logistik",
                    Route = "RS2" + i,
                    TripNo = "2",
                    SuppCode = "AT Indonesia" + i,
                    OrderNo = "201212" + i + "02",
                    Reason = "Emergency Order"

                });
            }
            return combo;
        }
        protected List<DCLScreenMaster> DCLget()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<DCLScreenMaster> DCLListAdd = new List<DCLScreenMaster>();
            var QueryLog = db.Query<DCLScreenMaster>("GetDCLscreenmaster", new object[] { "","" });
            int count = 0;
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    DCLListAdd.Add(new DCLScreenMaster()
                    {
                      date  = q.date,
                      Route = q.Route,
                      Rate  = q.Rate,
                      TYPE  = q.TYPE,
                      LPCode = q.LPCode,
                      DRIVER_NAME = q.DRIVER_NAME,
                      //STATUS  = q.STATUS ,
                      TRUCK_NO = q.TRUCK_NO,
                      DownloadBy = "TMMIN SUNTER",
                      DownloadTime = DateTime.Now,
                      //SKID_NO  = q.SKID_NO
                      Download = SetDownload(),
                      SKID_NO = SetNoticeIconString(count)
                    });
                    count++;
                }
            }
            db.Close();
            return DCLListAdd;
        }

        protected List<DCLScreenAddMaster> DCLGetAddMaster()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<DCLScreenAddMaster> DCLListAdd = new List<DCLScreenAddMaster>();
            var QueryLog = db.Query<DCLScreenAddMaster>("GetDCLscreenmaster", new object[] { "", "" });
            int count = 0;
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    DCLListAdd.Add(new DCLScreenAddMaster
                        {
                            date = q.date,
                            Route = q.Route,
                            Rate = q.Rate,
                            TYPE = q.TYPE,
                            LPCode = q.LPCode,
                            DRIVER_NAME = q.DRIVER_NAME,
                            STATUS = q.STATUS,
                            TRUCK_NO = q.TRUCK_NO,
                            //SKID_NO  = q.SKID_NO
                            //Download = "",
                            DownloadBy = "TMMIN SUNTER",
                            DownloadTime = DateTime.Now,
                            Download = SetDownload(),
                            SKID_NO = SetNoticeIconString(count)
                        });
                    count++;
                }
            }
            db.Close();
            return DCLListAdd;
        }


        public ActionResult Search(string TermFrom,string TermTo)
        {
            DateTime dateFromFormat = Convert.ToDateTime(TermFrom);
            DateTime dateToFormat = Convert.ToDateTime(TermTo);
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DCLScreenModel model = Model.GetModel<DCLScreenModel>();
            model.DCLScreenMain = db.Fetch<DCLScreenMaster>("GetDCLscreenmaster", new object[] { dateFromFormat.ToString("yyyy-MM-dd"), dateToFormat.ToString("yyyy-MM-dd") });
            db.Close();
            return PartialView("Index", Model);
        }

        public ActionResult DCLScreenHeader()
        {
            return PartialView("DCLScreenHeader", Model);
        }

        public ActionResult DCLScreenHeaderBottom()
        {
            return PartialView("DCLScreenHeaderBottom", Model);
        }


        public ActionResult DCLScreenMaster()
        {
            return PartialView("DCLScreenMaster", Model);
        }
   

        public ActionResult DCLScreenAddMaster()
        {
            return PartialView("DCLScreenAddMaster", Model);
        }

        
        private string SetDownload()
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/csv_icon.png";
            string imageBytes2 = "~/Content/Images/pdf_icon.png";
            img = imageBytes1 + imageBytes2;
            return imageBytes2;
        }

        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_open_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_close_icon.png";

            if (val % 3 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 3 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 3 == 2)
            {
                img = imageBytes3;
            }


            return img;
        }
        

        public void ReportExports()
        {
        }

        public ActionResult DCLScreenDetail()
        {
            return PartialView("DCLScreenDetail", Model);
        }

        public ActionResult DCLScreenDetailAdditional()
        {
            return PartialView("DCLScreenDetailAdditional", Model);
        }
      
        public ActionResult DCLScreenLogisticPartner()
        {
            return PartialView("DCLScreenLogisticPartner", Model);
        }

        #region grid concern
        public ActionResult partialDCLScreen(string Date)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DCLScreenModel model = Model.GetModel<DCLScreenModel>();
            model.DCLScreenMainDetail = db.Fetch<DCLScreenMasterDetail>("GetDCLscreenmasterDetail", new object[] { Date });
            Model.AddModel(model);
            db.Close();
            return PartialView("DCLScreenDetailGrid", Model);
        }

        public ActionResult partialDCLScreenAdditional(string Date)
        {
            /*IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DCLScreenModel model = Model.GetModel<DCLScreenModel>();
            model.DCLScreenMainDetailAdditional = db.Fetch<DCLScreenMasterDetailAdditional>("GetDCLscreenmasterDetail", new object[] { Date });
            Model.AddModel(model);*/
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DCLScreenModel model = Model.GetModel<DCLScreenModel>();
            model.DCLScreenMainDetail = db.Fetch<DCLScreenMasterDetail>("GetDCLscreenmasterDetail", new object[] { Date });
            Model.AddModel(model);
            db.Close();
            return PartialView("DCLScreenDetailAdditionalGrid", Model);

        }
        #endregion


        /*#region Data Dummy
        protected override void Init()
        {
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = true;

            

            DCLScreenModel mdls = new DCLScreenModel();
            mdls.DCLScreenMain = DCLgetDummy();
            mdls.DCLScreenAddMain = DCLGetAddMasterDummy();
            //Model.AddModel(new Search { });
            mdls.Combo = dclGetCombo();
            Model.AddModel(mdls);

            
        }
        protected List<DCLScreenMaster> DCLgetDummy()
        {
            DCLScreenModel mdls = new DCLScreenModel();
            List<DCLScreenMaster> DCLListAdd = new List<DCLScreenMaster>();
            for (int a = 0; a < 5; a++)
            {
                DCLListAdd.Add(new DCLScreenMaster()
                    {
                        date = DateTime.Now,
                        Route = "10023",
                        
                        TYPE = "R",
                        LPCode = "YAI",
                        DRIVER_NAME = "Lavi Budiman",
                        Rate = "02",
                        //STATUS  = q.STATUS ,
                        TRUCK_NO = "23",
                        DownloadBy = "niit",
                        DownloadTime = DateTime.Now,
                        //SKID_NO  = q.SKID_NO
                        Download = SetDownload(),
                        SKID_NO = SetNoticeIconString(a)
                    });
            }
            return DCLListAdd;
        }

        protected List<DCLScreenAddMaster> DCLGetAddMasterDummy()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DCLScreenModel mdls = new DCLScreenModel();
            List<DCLScreenAddMaster> DCLListAdd = new List<DCLScreenAddMaster>();
            for(int i=0;i<5;i++)
            {
                DCLListAdd.Add(new DCLScreenAddMaster()
                    {
                        date = DateTime.Now,
                        Route = "10023",
                        
                        TYPE = "R",
                        LPCode = "YAI",
                        DRIVER_NAME = "Lavi Budiman",
                        Rate = "02",
                        //STATUS  = q.STATUS ,
                        TRUCK_NO = "23",
                        DownloadBy = "niit",
                        DownloadTime = DateTime.Now,
                        //SKID_NO  = q.SKID_NO
                        Download = SetDownload(),
                        SKID_NO = SetNoticeIconString(i)
                    });
                
            } 
            return DCLListAdd;
        }

        protected List<DCLScreenMasterDetail> DCLGetGridRegulerDummy()
        {
            DCLScreenModel mdls = new DCLScreenModel();
            List<DCLScreenMasterDetail> DCLListAdd = new List<DCLScreenMasterDetail>();
            for (int b = 0; b < 5; b++)
            {
                DCLListAdd.Add(new DCLScreenMasterDetail()
                {
                    SUPPCD = "00001",
                    SUPPLIER_NAME = "Astra Daihatsu Motor",
                    ARRVDATETIME = "07:25:00",
                    DPTDATETIME = "07:25:00",
                    ORDDATE = "20121109",
                    RCVCOMPDOCKCD = "1A"

                });
            }
            return DCLListAdd;
        }

        protected List<DCLScreenMasterDetailAdditional> DCLGetGridAdditionalDummy()
        {
            DCLScreenModel mdls = new DCLScreenModel();
            List<DCLScreenMasterDetailAdditional> DCLListAdd = new List<DCLScreenMasterDetailAdditional>();
            for (int b = 0; b < 5; b++)
            {
                DCLListAdd.Add(new DCLScreenMasterDetailAdditional()
                {
                    SUPPCDAdditional = "00001",
                    SUPPLIER_NAMEAdditional = "Astra Daihatsu Motor",
                    ARRVDATETIMEAdditional = "07:25:00",
                    DPTDATETIMEAdditional = "07:25:00",
                    ORDDATEAdditional = "20121109",
                    RCVCOMPDOCKCDAdditional = "1A"

                });
            }
            return DCLListAdd;
        }
         public ActionResult partialDCLScreen(string Date)
        {
            DCLScreenModel mdls = new DCLScreenModel();
            mdls.DCLScreenMainDetail = DCLGetGridRegulerDummy();
            Model.AddModel(mdls);

            return PartialView("DCLScreenDetailGrid", Model);
            
        }

        public ActionResult partialDCLScreenAdditional(string Date)
        {
            DCLScreenModel mdls = new DCLScreenModel();
            mdls.DCLScreenMainDetailAdditional = DCLGetGridAdditionalDummy();
            Model.AddModel(mdls);

            return PartialView("DCLScreenDetailAdditionalGrid", Model);
        }
        
        #endregion
        #region proses lama
        public ActionResult SearchDCLDetail(string Name)
        {         
            ViewData["Name"] = Name;
             IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DCLScreenModel model = Model.GetModel<DCLScreenModel>();
            model.DCLScreenMainDetail = db.Fetch<DCLScreenMasterDetail>("GetDCLscreenmasterDetail", new object[] { Name });

            var DCL = model.DCLScreenMainDetail.Where(p => p.date.ToString("yyyy-MM-dd") == Name).ToList<DCLScreenMasterDetail>();

            return PartialView("DCLScreenDetail", DCL);
        }

        public ActionResult SearchDCLAddDetail(string Name)
        {
            ViewData["Name"] = Name;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            DCLScreenModel model = Model.GetModel<DCLScreenModel>();
            model.DCLScreenMainDetail = db.Fetch<DCLScreenMasterDetail>("GetDCLscreenmasterDetail", new object[] { Name });

            var DCL = model.DCLScreenMainDetail.Where(p => p.date.ToString("yyyy-MM-dd") == Name).ToList<DCLScreenMasterDetail>();

            return PartialView("DCLScreenAddDetail", DCL);

        }
        #endregion
         */
    }
}
