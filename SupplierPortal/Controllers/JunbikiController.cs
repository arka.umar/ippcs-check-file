﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.Junbiki;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class JunbikiController : BaseController
    {
       private JunbikiModel mar = new JunbikiModel();
       public JunbikiController()
            : base("Junbiki Order")
        {
            PageLayout.UseMessageBoard = true;
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            
            JunbikiModel mdl = new JunbikiModel();
            mdl.GetJunbikiMaster = listOfJunbikiData("","","");
            Model.AddModel(mdl);
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QuerySupplier = db.Query<Supplier>("GetAllSupplierGrid");
            db.Close();
            ViewData["GridSupplier"] = QuerySupplier;
        }

        #region getList of Data
        public List<JunbikiMaster> listOfJunbikiData(string supplierCode, string dateFrom, string dateTo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<JunbikiMaster> getListJunbiki = new List<JunbikiMaster>();
            var QueryLog = db.Query<JunbikiMaster>("GetDataJunbikiOrder", new object[] { supplierCode, dateFrom, dateTo });

            int sequence = 0;
            if (QueryLog.Any()) 
            {
                foreach (var q in QueryLog)
                {
                    getListJunbiki.Add(new JunbikiMaster()
                    {
                        OrderDate = q.OrderDate,
                        ManifestNo = q.ManifestNo,
                        SequenceNo = sequence,
                        OrderNo = q.OrderNo,
                        ShiftCode = " ",
                        TotalItem = q.TotalItem,
                        TotalQty = q.TotalQty,
                        DownIcon = "~/Content/Images/pdf_icon.png",
                        NoticeIcon = SetNoticeIconString(sequence)
                    });
                    sequence++;
                }
            }
            db.Close();
            return getListJunbiki;
        }

        public List<JunbikiDetail> listOfJunbikiDetail(string ManifestNo) 
        {
            List<JunbikiDetail> getListofJunbiki = new List<JunbikiDetail>();
            //for (int i = 1; i < 10; i++)
            //{


            //    for (int j = 1; j <= 5; j++)
            //    {
            //        getListofJunbiki.Add(new JunbikiDetail()
            //        {
            //            Manifest = "J001015661" + j,
            //            PartName = "Carpet Assy Front",
            //            PartNo = "58510-0K811" + j,
            //            Suffix = "E" + j,
            //            Status = "Replacement",
            //            Color = "MIDL. Grege",
            //            Katashiki = "MIDL. Grege",
            //            Modelz = "GHS",
            //            Qty = i + j
            //        });

            //    }
            //}
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<JunbikiDetail>("GetJunbikiPartDetail", new object[]{ManifestNo});
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog) 
                {
                    getListofJunbiki.Add(new JunbikiDetail 
                    {
                        Color = "MIDL. Grege",
                        PartNo = q.PartNo,
                        Suffix = q.Suffix,
                        Qty = q.Qty,
                        Katashiki = "MIDL. Grege",
                        Modelz = "GHS",
                        Status = "Replacement",
                    });
                }
            }
            db.Close();
            return getListofJunbiki;
        }
        #endregion
        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_close_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_open_icon.png";
            string imageBytes4 = "~/Content/Images/notice_add_icon.png";

            if (val % 4 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 4 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 4 == 2)
            {
                img = imageBytes3;
            }
            else if (val % 4 == 3)
            {
                img = imageBytes4;
            }

            return img;
        }
        
        public ActionResult PartialHeaderJunbiki()
        {
            return PartialView("HeaderJunbiki", Model);
        }

        public ActionResult GridJunbikiMasters()
        {
            string supplier = string.Empty;
            string orderFrom = string.Empty;
            string orderTo = string.Empty;

            supplier = Request.Params["Supplier"];
            orderFrom = Request.Params["OrderFrom"].Equals("--")?string.Empty:Request.Params["OrderFrom"];
            orderTo = Request.Params["OrderTo"].Equals("--")?string.Empty:Request.Params["OrderTo"];

            if (!supplier.Equals(string.Empty) || !orderFrom.Equals(string.Empty) || !orderTo.Equals(string.Empty))
            {
                JunbikiModel mdl = Model.GetModel<JunbikiModel>();
                mdl.GetJunbikiMaster = listOfJunbikiData(supplier,orderFrom,orderTo);
            }
            

            return PartialView("OrderMaster", Model);
        }

        public ActionResult CallbackSupplier()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            TempData["GridName"] = "LookupSupplier";
            var QuerySupplier = db.Query<Supplier>("GetAllSupplierGrid");
            ViewData["GridSupplier"] = QuerySupplier;
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);
        }

        /*
        public ActionResult PartialHeaderPopup()
        {
            return PartialView("HeaderPopup", Model);
        }

      

        public ActionResult GridJunbikiDetail2()
        {
            ViewData["Manifest"] = "J0010156611";
            JunbikiModel model = Model.GetModel<JunbikiModel>();
            var OrderDetail = model.GetJunbikiDetail.ToList<JunbikiDetail>();
            Model.AddModel(model);
            return PartialView("OrderPopup", Model);
        }
        */

        #region popup
        public ActionResult UpdateCallBackPanel()
        {
            ViewData["ManifestNo"] = Request.Params["ManifestNo"];
            return PartialView("PopupPartDetail", ViewData["OrderReleaseDate"]);
        }

        public ActionResult PopupHeaderPartDetail()
        { 
            ViewData["ManifestNo"] = Request.Params["ManifestNo"];
            return PartialView("PartialHeaderPartDetail", ViewData["ManifestNo"]);
        }
        public ActionResult PopupPartDetail(string ManifestNo)
        {
            string manifestNo = Request.Params["ManifestNo"];
            ViewData["ListJunbikiDetail"] = listOfJunbikiDetail(manifestNo);
            return PartialView("OrderDetail", ViewData["ListJunbikiDetail"]);
        }
        
        #endregion
    }
}
