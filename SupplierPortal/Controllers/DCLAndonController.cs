﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;
using Portal.Models.PO;
using Portal.Models.DCLAndon;
using Toyota.Common.Web.Credential;

namespace Portal.Controllers
{
    public class DCLAndonController : BaseController
    {

        public DCLAndonController()
            : base("Andon")
        {
        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            //throw new NotImplementedException();
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = true;

            DCLAndonModel dam = new DCLAndonModel();
            dam.DCLAndons = DCLget();
            dam.Combonye = dclTarikCombonye();
            Model.AddModel(dam);
        }

        protected List<DCLAndon> DCLget()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<DCLAndon> DCLListAdd = new List<DCLAndon>();
            var QueryLog = db.Query<DCLAndon>("GetDCLAndon", new object[] { "", "" });
            int count = 0;
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    DCLListAdd.Add(new DCLAndon()
                    {
                        No = q.No,
                        RouteDate = q.RouteDate,
                        Dock = q.Dock,
                        Station = q.Station,
                        RouteName = q.RouteName,
                        TripNo = q.TripNo,
                        ArrivalPlan = q.ArrivalPlan,
                        ActualArrival = q.ActualArrival,
                        Status = q.Status,
                        GAPARRV = q.GAPARRV,
                        DeparturePlan = q.DeparturePlan,
                        ActualDeparture = q.ActualDeparture,
                        StatusDept = q.StatusDept,
                        GAPDEPT = q.GAPDEPT,
                        LPNAME = q.LPNAME,
                        Notif = SetNoticeIconString(count)

                    });
                    count++;
                }
            }
            db.Close();
            return DCLListAdd;
        }
        protected List<DCLAndonCombo> dclTarikCombonye()
        {
            DCLAndonModel mdl = new DCLAndonModel();
            List<DCLAndonCombo> combo = new List<DCLAndonCombo>();
            for (int i = 1; i <= 5; i++)
            {
                combo.Add(new DCLAndonCombo()
                {
                    LPCode = "MML" + i,
                    LPName = "Cipta Mapan logistik",
                    Route = "RS2" + i,
                    TripNo = "2",
                    SuppCode = "AT Indonesia" + i,
                    OrderNo = "201212" + i + "02",
                    Reason = "Emergency Order"

                });
            }
            return combo;
        }
        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_open_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_close_icon.png";

            if (val % 3 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 3 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 3 == 2)
            {
                img = imageBytes3;
            }


            return img;
        }
        public ActionResult DCLAndonScreenLogisticPartner()
        {
            return PartialView("DCLAndonScreenLogisticPartner", Model);
        }

        public ActionResult HeaderDCLAndon()
        {
            return PartialView("HeaderDCLAndon", Model);
        }

        public ActionResult PartialHeaderTop()
        {
            return PartialView("HeaderTopDCLAndon", Model);
        }

        public ActionResult PartialHeaderRight()
        {
            return PartialView("HeaderRightDCLReceiving", Model);
        }


        public ActionResult DCLAndonPartial()
        {
            return PartialView("DCLAndonPartial", Model);
        }

        public ActionResult DCLAndonScanBarcodePartial()
        {
            return PartialView("DCLAndonScanBarcodePartial", Model);
        }

        public ActionResult AndonDeliveryInformationDetail()
        {
            return PartialView("AndonDeliveryInformationDetail", Model);
        }

        public ActionResult AndonTimingInformationDetail()
        {
            return PartialView("AndonTimingInformationDetail", Model);
        }

        public ActionResult AndonTimingIndicatorDetail()
        {
            return PartialView("AndonTimingIndicatorDetail", Model);
        }

        public ActionResult DCLAndonScanBarcodeDetailPartial()
        {
            return PartialView("DCLAndonScanBarcodeDetailPartial", Model);
        }

        public ActionResult DCLAndonBarcodePartial()
        {
            return PartialView("DCLAndonBarcodePartial", Model);
        }
        /**public ActionResult DCLDockandRate()
        {
            return PartialView("DCLDockandRate", Model);
        }*/
    }
}
