﻿#region "Get All Using Library"
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Web.UI.WebControls;
using System.Threading;
using Portal.Models;
using Portal.Models.Dock;
using Portal.Models.Globals;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.MVC;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.Mvc;
using Portal.Models.Plant;
using System.Threading;
using System.IO;
using OfficeOpenXml;
using Toyota.Common.Web.Excel;
using DevExpress.Web.ASPxUploadControl;
using System.Data.OleDb;
#endregion

namespace Portal.Controllers
{
    public class DockController : BaseController
    {
        #region "Public DockControl(As The Form Name) and private static IDBContext DbContext"
        
        public DockController()
            : base("Dock Master Inquiry Screen")
        {
            PageLayout.UseMessageBoard = true;
        }
        #endregion

        #region "protected override viod startup"
        protected override void StartUp()
        {
            
        }
        #endregion

        private IDBContext dbcontext()
        {
            return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        }

        #region "Protected Void Initialized / Init() eventargs"
        protected override void Init()
        {
            try
            {
                DockModel dm = new DockModel();

                dm.DockDbModel = DockGet(String.Empty, String.Empty, String.Empty, String.Empty,String.Empty,String.Empty);
                Model.AddModel(dm);
                
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                
                var QueryDock = db.Query<Dock>("SelectAllDockParams", new object[] { String.Empty});
                ViewData["DockCode"] = QueryDock;

                PlantModel pm = new PlantModel();

                pm.PlantsData = PlantGet(String.Empty, String.Empty);
                Model.AddModel(pm);


                var QueryPlantDock = db.Query<PlantData>("GetAllPlantDock", new object[] { String.Empty });
                ViewData["GridPlantDock"] = QueryPlantDock;

                db.Close();

                //Model.AddModel(GetAllDock());
            }
            catch (Exception excum)
            {
                Console.WriteLine(excum.Message + excum.Source + excum.StackTrace + excum.TargetSite);
            }
        }
        #endregion

        

        #region "All Public ActionResult"
        public ActionResult PartialDockDatas()
        {
            return PartialView("PartialDockHeader", Model);
        }
        public ActionResult PartialHeaders()
        {
            return PartialView("PartialDockGridHeader", Model);
        }
        public ActionResult PartialGrid()
        {
            return PartialView("PartialDockGrid", Model);
        }

        public ActionResult PartialPopupDockCode()
        {
            return PartialView("PopupNewDockCode", Model);
        }

        public ActionResult PartialDockBottom()
        {
            return PartialView("PartialDockBottom", Model);
        }
        // this is used for dataload in main gridview
        private string[] dockCodeList;
        private String dockCodeParameter = "";
        public ActionResult PartialGridView()
        {
            try
            {
                dockCodeList = Request.Params["DockCode"].Split(';');
                foreach (string dockCode in dockCodeList)
                {
                    dockCodeParameter += dockCode + ",";
                }

                dockCodeParameter = dockCodeParameter.Remove(dockCodeParameter.Length - 1, 1);

                if (dockCodeParameter == "''")
                {
                    dockCodeParameter = string.Empty;
                }

                DockModel dm = Model.GetModel<DockModel>();

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                var QueryDock = db.Query<DockData>("SelectAllDockParams", new object[] { dockCodeParameter });

                // error checklist sampe disini, pencarian by criteria error, dari casenya deket where ada yang error, coba cek dan selesaikan cepat

                dm.DocksData = QueryDock.ToList<DockData>();
                db.Close();

                dm.DockDbModel = DockGet(dockCodeParameter, (Request.Params["PlantCD"]), (Request.Params["DockName"]), (Request.Params["STATUS_DOCK"]), (Request.Params["Email"]),(Request.Params["Emailcc"]));
                Model.AddModel(dm);

            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message + error.Source + error.StackTrace + error.TargetSite);
            }

            return PartialView("PartialDockGrid", Model);
        }
        public ActionResult DockCodePartial()
        {
            try
            {
                // untuk buat form refresh = NamaGridViewnya.PerformCallback();

                TempData["GridName"] = "grlDock";
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                var QueryDock = db.Query<DockData>("GetAllDockActiveStatus");

                db.Close();
                ViewData["GridDock"] = QueryDock;
            }
            catch (Exception errordockcodepartial)
            {
                Console.WriteLine(errordockcodepartial.Message + errordockcodepartial.Source + errordockcodepartial.StackTrace + errordockcodepartial.TargetSite);
                return RedirectToAction("ShowError", errordockcodepartial.Message);
            }

            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);
        }
        public ActionResult PlantCodePartialLookupForm()
        {
            try
            {
                TempData["GridName"] = "grlPlantDock";
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                var QueryPlantDock = db.Query<PlantData>("GetAllPlantDock");

                db.Close();
                ViewData["GridPlantDock"] = QueryPlantDock;
            }
            catch (Exception explantcode)
            {
                Console.WriteLine(explantcode.Message + explantcode.Source + explantcode.StackTrace + explantcode.TargetSite);
                return RedirectToAction("ShowError", explantcode.Message);
            }
            return PartialView("GridLookup/PartialGrid", ViewData["GridPlantDock"]);
        }
        public ActionResult ShowError(String sErrorMessage)
        {
            ViewBag.sErrMssg = sErrorMessage;
            return PartialView("ErrorMessageView");
        }
        public ActionResult PartialPopupNewDockCode()
        {
            #region Required model in partial page
            #region Model for PartialPopupNewDockCode
            try
            {
                Model.AddModel(GetAllDock());
            }
            catch (Exception errordockcodepartial)
            {
                Console.WriteLine(errordockcodepartial.Message + errordockcodepartial.Source + errordockcodepartial.StackTrace + errordockcodepartial.TargetSite);
            }
            #endregion
            #endregion

            return PartialView("PopupNewDockCode", Model);
        }
        public ActionResult DockPlantCodeLookUp(string GridNames)
        {
            try
            {
                if (GridNames == null)
                {
                    TempData["GridName"] = "grlPlant";
                }
                else
                {
                    TempData["GridName"] = GridNames;
                }

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                var QueriPlantDock = db.Query<Dock>("GetAllPlantDock");
                db.Close();
                ViewData["GridPlantDock"] = QueriPlantDock;
            }
            catch (Exception onerrordockplantcodegrid)
            {
                Console.WriteLine(onerrordockplantcodegrid.Message + onerrordockplantcodegrid.Source + onerrordockplantcodegrid.StackTrace + onerrordockplantcodegrid.TargetSite);
            }
            return PartialView("GridLookup/PartialGrid", ViewData["GridPlantDock"]);
        }
        #region "This is the error code not used anymore"
        //public ActionResult DockCodePartialLookUp(string gridName)
        //{
        //    if (gridName == null)
        //    {
        //        TempData["GridName"] = "grlPlant";
        //    }
        //    else
        //    {
        //        TempData["GridName"] = gridName;
        //    }

        //    /*IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

        //    //var QueryDock = db.Query<Dock>("GetAllDock");
        //    //ViewData["GridDock"] = QueryDock;*/

        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    var QueryPlant = db.Query<DockData>("GetAllPlant");

        //    db.Close();
        //    ViewData["PlantData"] = QueryPlant.ToList<DockData>();

        //    return PartialView("GridLookup/PartialGrid", ViewData["PlantData"]);
        //}
        #endregion
        #endregion

        #region "All public ContentResult"
        // here is the code specific for insert data eventhandler
        public ContentResult InsertMasterDock(string DOCK_CD, string PLANT_CD, string DOCK_NM, string CREATED_BY, string EMAIL_TO,string STATUS_DOCK, string status,string EMAIL_CC)
        {
            string resultMessage = "";
            string allowDelete = "";
            string dockCd = "";
            try
            {
                dockCd = DOCK_CD;
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                allowDelete = db.ExecuteScalar<String>("CheckDock", new object[] { dockCd });

                if (allowDelete == "1")
                {
                    //EMAIL_CC = "test";
                    resultMessage = db.ExecuteScalar<string>("UpdateDataMasterDock", new object[] { dockCd, PLANT_CD, DOCK_NM, EMAIL_TO, EMAIL_CC, AuthorizedUser.Username});
                    resultMessage = "Saving Data Has Finish Successfully.";
                    db.Close();
                }
                else
                {
                    resultMessage = db.ExecuteScalar<string>("InsertMasterDock", new object[] { DOCK_CD, PLANT_CD, DOCK_NM, AuthorizedUser.Username, EMAIL_TO,EMAIL_CC });
                    resultMessage = "Saving Data Has Finish Successfully.";
                    db.Close();
                }
                //resultMessage = db.ExecuteScalar<string>("InsertMasterDock", new object[] { DOCK_CD, PLANT_CD, DOCK_NM, AuthorizedUser.Username, EMAIL_TO });
                //resultMessage = "Saving Data Has Finish Successfully.";
                //db.Close();
            }
            catch (Exception excumalerterrorinsertmasterdock)
            {
                Console.WriteLine(excumalerterrorinsertmasterdock.Message + excumalerterrorinsertmasterdock.Source + excumalerterrorinsertmasterdock.StackTrace + excumalerterrorinsertmasterdock.TargetSite);
                resultMessage = "System Has Failed To Executed Saving Data, Please Retry.";

            }
            return Content(resultMessage.ToString());
        }
        #endregion


        public ContentResult DeleteMasterDock(string DOCK_CD)
        {
            string resultMessage = "";
            string allowDelete = "";
            string dockCd = "";

            dockCd = DOCK_CD.Substring(0, DOCK_CD.Length - 1);

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                allowDelete = db.ExecuteScalar<String>("checkAllowDeleteDock", new object[] { dockCd });

                if (allowDelete == "ALLOWED")
                {
                    db.Execute("deleteDock", new object[] { DOCK_CD });
                    resultMessage = "Delete Data Finish Successfully.";
                }
                else
                {
                    resultMessage = "System Fail To Delete Data";
                }
            }
            catch (Exception e) {
                resultMessage = "Error Occurred";
            }

            return Content(resultMessage.ToString());



        }

        #region "Public and Private List"
        private List<DockData> GetAllDock()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<DockData> forecastSupplierModelList = new List<DockData>();
            var QueryLog = db.Query<DockData>("GetAllDock", new object[] { "" });
            int count = 0;

            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    forecastSupplierModelList.Add(new DockData()
                    {
                        DockCode = q.DockCode,
                        DockName = q.DockName,
                        //Email=q.Email
                    });
                    count++;
                }
            }
            db.Close();

            return forecastSupplierModelList;
        }
        protected List<DockData> DockGet(string DockCode, string PlantCD, string DockName, string STATUS_DOCK,string Email,string Emailcc)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<DockData> DockListAdd = new List<DockData>();
            var QueryLog = db.Query<DockData>("SelectAllDockParams", new Object[] { DockCode, PlantCD, DockName, Email,Emailcc });
            if (QueryLog.Any())
            {
                try
                {
                    int count = 0;
                    foreach (var q in QueryLog)
                    {
                        DockListAdd.Add(new DockData()
                        {
                            DockCode = q.DockCode,
                            PlantCD = q.PlantCD,
                            DockName = q.DockName,
                            STATUS_DOCK = q.STATUS_DOCK,
                            Email=q.Email,
                            Emailcc=q.Emailcc
                        });
                        count++;
                    }
                }
                catch (Exception errorlistdockget)
                {
                    Console.WriteLine(errorlistdockget.Message + errorlistdockget.Source + errorlistdockget.StackTrace + errorlistdockget.TargetSite);
                }
            }
            db.Close();
            return DockListAdd;
        }
        protected List<PlantData> PlantGet(string PlantCode, string PlantName)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<PlantData> PlantListAdd = new List<PlantData>();
            var QueryLog = db.Query<PlantDockDats>("GetAllPlantDock", new Object[] { PlantCode, PlantName });
            if (QueryLog.Any())
            {
                try
                {
                    int count = 0;
                    foreach (var xx in QueryLog)
                    {
                        PlantListAdd.Add(new PlantData()
                        {
                            PlantCode = xx.PlantCode,
                            PlantName = xx.PlantName
                        });
                        count++;
                    }
                }
                catch (Exception errorlistdockget)
                {
                    Console.WriteLine(errorlistdockget.Message + errorlistdockget.Source + errorlistdockget.StackTrace + errorlistdockget.TargetSite);
                }
            }
            db.Close();
            return PlantListAdd;
        }
        public List<DockData> SearchDockData()
        {
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                var QueryDock = db.Query<DockData>("GetAllDock");
                db.Close();
                return QueryDock.ToList<DockData>();
            }
            catch (Exception errorSearchdockdatalist)
            {
                Console.WriteLine(errorSearchdockdatalist.Message + errorSearchdockdatalist.Source + errorSearchdockdatalist.StackTrace + errorSearchdockdatalist.TargetSite);
            }
            return null;
        }
        #endregion


        #region "donwload and upload xls"
        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        public FileContentResult downloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "MasterTemplate.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/Dock/TemplateDock/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        public void download(string dockcode, string plancode, string namecode, string email, string emailcc)
        {
            IDBContext db = dbcontext();
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<DockList> boForExcel = new List<DockList>();

            boForExcel = db.Query<DockList>("getDockForDownload", new object[] {plancode, dockcode, namecode, email, emailcc }).ToList();

            fileName = "MasterDockList" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            data = exporter.Write(ConvertToDataTable(boForExcel), "Dock");

            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
       
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("DockUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }


        public ActionResult DockUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/Dock/TempUpload/");
                return _uploadDirectory;
            }
        }

        private DockList AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            DockList _UploadModel = new DockList();

            string function = "81016";
            string module = "8";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_DOCK_TEMP";

            string sheetName = "Dock";
            message = "Starting Upload Build Out";
            location = "MasterDockUpload.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });

            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_DOCK_TEMP";
                location = "MasterDockUpload.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });

                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as DOCK_CODE," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as PLAN_CODE," +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as DOCK_NAME," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as EMAIL_TO," + //kanban_no = unique no
                                    @"IIf(IsNull(F5), Null, Cstr(F5)) as EMAIL_CC ";
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;

                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    message = "Error : " + e.Message;
                    location = "MasterDockUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {

                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("DOCK_CODE", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("PLAN_CODE", "PLANT_CD");
                    bulkCopy.ColumnMappings.Add("DOCK_NAME", "DOCK_NM");
                    bulkCopy.ColumnMappings.Add("EMAIL_TO", "EMAIL_TO");
                    bulkCopy.ColumnMappings.Add("EMAIL_CC", "EMAIL_CC");

                    try
                    {
                        message = "Insert Data to TB_T_DOCK_TEMP";
                        location = "MasterDockUpload.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });

                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "MasterDockUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });

                        throw new Exception(ex.Message);
                    }
                    //bulkCopy.WriteToServer(ds.Tables[0]);
                }

                string validate = db.SingleOrDefault<string>("ValidateDockUpload", new object[] { processID });

                if (validate == "error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found.")
                {
                    TempData["IsValid"] = "empty";
                    sts = "6";
                    message = validate;
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }

                location = "MasterDockUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            sts      // sts
                                        });

            }
            catch (Exception e) 
            {

                TempData["IsValid"] = "false;" + processID + ";" + e.Message;


                message = "Error : " + e.Message;
                location = "MasterDockUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });
                throw new Exception(e.Message);
            }
            finally
            {

                excelConn.Close();

                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

            }
            return _UploadModel;
        }

        public ActionResult MoveDockDataTemp(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            IDBContext db = dbcontext();

            try
            {

                db.Execute("moveDockDataTemp", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return Content(message);
        }

        public void UploadInvalid(string process_id)
        {
            IDBContext db = dbcontext();
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<DockListError> boForExcel = new List<DockListError>();

            //boForExcel = db.Query<DockList>("getErrorList", new object[] { process_id }).ToList();

            fileName = "MasterDockListError" + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            data = exporter.Write(ConvertToDataTable(boForExcel), "Dock");

            db.Close();

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        #endregion

      

        #region "Public Void PrintDockCode EventHandler(Telerik Injection Eventhandl)"
        public void PrintDockCode(string DockCode)
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\Dock\DockBarcode.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommand = String.Format(@"SELECT [DOCK_CD] DockCode
                                            ,[PLANT_CD] PlantCode
                                            ,[DOCK_NM] DockName
                                            FROM [TB_M_DOCK]
                                            WHERE [DOCK_CD] IN({0})", SplitDockCode(DockCode));

            ReportProcessor reportProcessor = new ReportProcessor();

            RenderingResult result = reportProcessor.RenderReport("Pdf", rpt, null);

            string fileName = "DockMasterBarcode" + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }
        #endregion

        #region "String SplitDockCode"
        string SplitDockCode(string val)
        {
            string[] separator = { @"," };
            string[] arrDockCode = val.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            string[] hasil = new string[arrDockCode.Length];

            for (int i = 0; i < arrDockCode.Length; i++)
            {
                hasil[i] = "'" + arrDockCode[i] + "'";
            }

            return string.Join(",", hasil);
        }
        #endregion
    }
}
