﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.IO;
using System.IO.Compression;
using System.Web.UI.WebControls;
using System.Threading;
using Portal.Models.RetroInquiry;
using Portal.Models.Globals;
using Portal.Models.Supplier;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Html.Helper.DevExpress;
using Toyota.Common.Web.Util.Converter;
using Toyota.Common.Web.Service;
using Toyota.Common.Web.Log;
using Toyota.Common.Web.Excel;
using Toyota.Common.Util.Text;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using DevExpress.Web.Mvc;
using System.Text.RegularExpressions;
using System.Text;

namespace Portal.Controllers
{
    public class RetroInquiryController : BaseController
    {

        public RetroInquiryController()
            : base("Retro Inquiry")
        {
            PageLayout.UseMessageBoard = true;
        }

        private readonly string screenID = "RetroInquiry";
        private readonly string osuppID = "GrlSupplier";

        private string _suppCd = null;
        private string SuppCd
        {
            get
            {
                if (string.IsNullOrEmpty(_suppCd))
                {
                    _suppCd = SuppCodeOf(Model.GetModel<User>());
                    //if (string.IsNullOrEmpty(_supplierCode))
                    //{
                    //    _supplierCode = Request.Params["SupplierCode"];
                    //}
                }
                return _suppCd;
            }

            set
            {
                _suppCd = value;
            }
        }

        private string SuppCodeOf(User u)
        {
            if (u == null || u.Authorization == null || u.Authorization.Count < 1) return null;

            string ag = (from authSource in u.Authorization
                         from item in authSource.AuthorizationDetails
                         where item.Screen.ToLower() == screenID.ToLower() &&
                             item.Object.ToLower() == osuppID.ToLower() &&
                             (item.AuthorizationGroup.ToLower() != "null")
                         select item.AuthorizationGroup).FirstOrDefault();
            return ag;
        }

        private string SuppCodeOf(string uid)
        {
            string sCode = "";
            Regex rx = new Regex("([0-9]+)(\\.)(.*)");
            MatchCollection mx = rx.Matches(uid);
            if (mx.Count > 0)
            {
                sCode = mx[0].Groups[1].Value;
            }
            return sCode;
        }


        private string ValidateSuppCode(string sCode)
        {
            if (string.IsNullOrEmpty(sCode)) return null;

            List<string> a = sCode.Split(';').ToList();
            for (int i = 0; i < a.Count; i++)
            {
                a[i] = SuppCodeValid(a[i]);
            }
            return string.Join(",", a);
        }
        private string SuppCodeValid(string c)
        {
            if (string.IsNullOrEmpty(SuppCd))
                return c; // doesn't matter , always valid when authorization group is empty

            if (string.IsNullOrEmpty(c)
                || (string.Compare(c, SuppCd) == 0))
                return SuppCd;

            return "XXXX"; /// return invalid
        }

        protected List<SupplierICS> Suppliers
        {
            get
            {
                List<SupplierICS> x = Model.GetModel<List<SupplierICS>>();
                if (x == null)
                {
                    x = Model.GetModel<User>().FilteringArea<SupplierICS>(
                        GetAllSupplier(), "SUPP_CD", screenID, osuppID);
                }
                return x;
            }
        }

        protected override void StartUp()
        {
            TempData["SupplierCode"] = SuppCd;
            Model.AddModel(Suppliers);
            //LstCurr();
        }

        protected override void Init()
        {
            /*
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<Supplier> listSupplier = new List<Supplier>();
            listSupplier = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SUPPLIER_CODE", "RetroInquiry", "GrlSuppier");

            if (listSupplier.Count > 0)
            {
                _SupplierModel = listSupplier;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }

            ViewData["SupplierData"] = _SupplierModel;
            
            LstCurr();

            db.Close();
            */
            LstCurr();
        }

        public IEnumerable<Currency> LstCurr()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            IEnumerable<Currency> QueryData = db.Query<Currency>("GetAllCurrency", new Object[] { "", "" });
            db.Close();
            Model.AddModel(QueryData);
            return QueryData;
        }

        List<Supplier> _supplierModel = null;
        private List<Supplier> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<Supplier>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<RetroInquiryDetail> ListRetroInquiryDetail_ = null;
        private List<RetroInquiryDetail> ListRetroInquiryDetail
        {
            get
            {
                if (ListRetroInquiryDetail_ == null)
                    ListRetroInquiryDetail_ = new List<RetroInquiryDetail>();

                return ListRetroInquiryDetail_;
            }
            set
            {
                ListRetroInquiryDetail_ = value;
            }
        }

        List<RetroInquiryDetailPopup> ListRetroInquiryDetailPopup_ = null;
        private List<RetroInquiryDetailPopup> ListRetroInquiryDetailPopup
        {
            get
            {
                if (ListRetroInquiryDetailPopup_ == null)
                    ListRetroInquiryDetailPopup_ = new List<RetroInquiryDetailPopup>();

                return ListRetroInquiryDetailPopup_;
            }
            set
            {
                ListRetroInquiryDetailPopup_ = value;
            }
        }

        #region RELOAD GRID DETAIL
        protected List<RetroInquiryDetail> GetAllDetailData(string SupplierCode, DateTime PeriodeFrom, DateTime PeriodeTo, string RetroDoc, string StatusRetro)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<RetroInquiryDetail> AllDetailDeta = new List<RetroInquiryDetail>();
            var QueryDetail = db.Query<RetroInquiryDetail>("GetRetroInquiryDetail", new Object[] { ValidateSuppCode(SupplierCode), PeriodeFrom, PeriodeTo, RetroDoc, StatusRetro });
            if (QueryDetail.Any())
            {
                int count = 0;
                foreach (var q in QueryDetail)
                {
                    AllDetailDeta.Add(new RetroInquiryDetail()
                    {
                        RETRO_DOC_NO = q.RETRO_DOC_NO,
                        PERIOD_FROM = q.PERIOD_FROM,
                        PERIOD_TO = q.PERIOD_TO,
                        CURRENCY_CD = q.CURRENCY_CD,
                        RETRO_AMOUNT = q.RETRO_AMOUNT == "0" || String.IsNullOrEmpty(q.RETRO_AMOUNT) ? q.RETRO_AMOUNT : String.Format("{0:###,###.##}", Convert.ToDouble(q.RETRO_AMOUNT)),
                        RETRO_RATE = q.RETRO_RATE + "%",
                        SUPPLIER_CD = q.SUPPLIER_CD,
                        SUPPLIER_NAME = q.SUPPLIER_NAME,
                        REMAINING_INV_NO = q.REMAINING_INV_NO,
                        REMAINING_INV_DATE = q.REMAINING_INV_DATE,
                        REMAINING_AMOUNT = q.REMAINING_AMOUNT == "" || String.IsNullOrEmpty(q.REMAINING_AMOUNT) ? q.REMAINING_AMOUNT : String.Format("{0:###,###.##}", Convert.ToDouble(q.REMAINING_AMOUNT)),
                        RETRO_REMAINING_AMOUNT = q.RETRO_REMAINING_AMOUNT == "" || String.IsNullOrEmpty(q.RETRO_REMAINING_AMOUNT) ? q.RETRO_REMAINING_AMOUNT : String.Format("{0:###,###.##}", Convert.ToDouble(q.RETRO_REMAINING_AMOUNT)),
                        RETRO_STATUS = q.RETRO_STATUS
                    });
                    count++;
                }
            }
            db.Close();

            #region DUMMY DATA USING LIST
            //List<RetroInquiryDetail> AllDetailDeta = new List<RetroInquiryDetail>();
            //for (int i = 1; i <= 100; i++)
            //{
            //    AllDetailDeta.Add(new RetroInquiryDetail
            //    {
            //        RETRO_DOC_NO = "C001",
            //        PERIOD_FROM = "24.09.2013",
            //        PERIOD_TO = "26.09.2013",
            //        CURRENCY_CD = "IDR",
            //        AMOUNT = "476,507,630",
            //        SUPPLIER_CD = "MRA",
            //        REFFERENCE_TMMIN_NO = "875NTC121" + i.ToString("##"),
            //        REFFERENCE_SUPPLIER_NO = "C001" + i.ToString("##"),
            //        STATUS_CD = i % 2 != 0 ? "0" : "1"
            //    });
            //}
            #endregion

            return AllDetailDeta;
        }

        public void Download()
        {
            ReloadData();
            IExcelWriter x = ExcelWriter.GetInstance();
            string fn = "RetroInquiry";
            byte[] b = x.Write<RetroInquiryDetail>(ListRetroInquiryDetail, fn);
            
            StringBuilder bFN = new StringBuilder(fn);
            string[] px = new string[] { "Supplier", "PeriodeFrom", "PeriodeTo", "RetroDoc", "Status" };
            string sx;
            for (int i = 0; i < px.Length; i++)
            {
                sx = Request.Params[px[i]];
                if (!string.IsNullOrEmpty(sx))
                {
                    bFN.Append("_");
                    bFN.Append(sx);
                }
            }
            bFN.Append(".xls");
            fn = bFN.ToString();

            SendResponse(fn, b);
        }

        public void ReloadData()
        {
            string SupplierCode = ValidateSuppCode(Request.Params["Supplier"]);
            DateTime PeriodeFrom = Convert.ToDateTime(Request.Params["PeriodeFrom"] == null || Request.Params["PeriodeFrom"] == "" || Request.Params["PeriodeFrom"] == "--"
                ? "1900-01-01 00:00:00" : Request.Params["PeriodeFrom"]);
            DateTime PeriodeTo = Convert.ToDateTime(Request.Params["PeriodeTo"] == null || Request.Params["PeriodeTo"] == "" || Request.Params["PeriodeTo"] == "--"
                ? "1900-01-01 00:00:00" : Request.Params["PeriodeTo"]);
            string RetroDoc = Request.Params["RetroDoc"] == null || Request.Params["RetroDoc"] == ""
                ? "" : Request.Params["RetroDoc"];
            string StatusRetro = Request.Params["Status"] == null || Request.Params["Status"] == ""
                ? "" : Request.Params["Status"];

            ListRetroInquiryDetail = GetAllDetailData(SupplierCode, PeriodeFrom, PeriodeTo, RetroDoc, StatusRetro);
        }


        public ActionResult ReloadGridDetailData()
        {
            ReloadData(); 

            Model.AddModel(ListRetroInquiryDetail);
            return PartialView("RetroInquiryDetail", Model);
        }

        private byte[] Pack(byte[] b, int method)
        {
            MemoryStream m = new MemoryStream();
            byte[] x = null;
            switch (method)
            {
                case 1:
                    using (GZipStream g = new GZipStream(m, CompressionMode.Compress))
                    {
                        g.Write(b, 0, b.Length);
                    }
                    x = m.ToArray();
                    break;
                case 2:
                    using (DeflateStream d = new DeflateStream(m, CompressionMode.Compress))
                    {
                        d.Write(b, 0, b.Length);
                    }
                    x = m.ToArray();
                    break;
                default:
                    x = new byte[b.Length];
                    b.CopyTo(x, 0);
                    break;
            }
            return x;
        }

        private void SendResponse(string fileName, byte[] b)
        {
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            //byte[] b = UTF8Encoding.UTF8.GetBytes(contents.ToString());
            var encodings = Request.Headers["Accept-Encoding"];
            if (encodings.Contains("gzip"))
            {
                Response.AddHeader("Content-encoding", "gzip");
                b = Pack(b, 1);
            }
            else if (encodings.Contains("deflate"))
            {
                Response.AddHeader("Content-encoding", "deflate");
                b = Pack(b, 2);
            }

            Response.ContentType = "application/octet-stream";

            Response.AddHeader("Content-Length", Convert.ToString(b.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(b);
            Response.End();
        }


        public ActionResult GetAllCurrency()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                List<Currency> ListCurrency = new List<Currency>();
                ListCurrency = db.Fetch<Currency>("GetCurrency", new Object[] { "", "" });
            }
            catch (Exception ex) { throw (ex); }

            db.Close();
            return PartialView("RetroInquiryDetail", Model);
        }

        #endregion

        #region GRID LOOKUP SUPPLIER

        private List<SupplierICS> GetAllSupplier()
        {
            List<SupplierICS> orderInquirySupplierModel = new List<SupplierICS>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            orderInquirySupplierModel = db.Fetch<SupplierICS>("GetAllSupplierICS", new object[] { });

            return orderInquirySupplierModel;
        }

        public ActionResult GridLookupSupplier()
        {
            List<SupplierICS> suppliers = Model.GetModel<User>().FilteringArea<SupplierICS>(
                    GetAllSupplier(), "SUPP_CD", screenID, osuppID);
            TempData["GridName"] = osuppID;
            TempData["PeriodFrom"] = DateTime.Now.AddMonths(-1);
            TempData["SupplierCode"] = SuppCd;

            return PartialView("GridLookup/PartialGrid", suppliers);
        }

        #endregion

        #region UPDATE GRId - Retro Rate

        public ActionResult UpdateGrid(string RetroDocNo, string RetroRate)
        {
            string QueryResult = string.Empty;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            try
            {
                QueryResult = db.ExecuteScalar<string>("UpdateRetroInquiryGrid", new object[] { RetroDocNo, RetroRate });
            }
            catch (Exception ex)
            {
                QueryResult = "Error: " + ex.Message;
            }
            finally
            {
                db.Close();
            }

            return Content(QueryResult);
        }

        #endregion

        #region POPUP RETRO DETAIL
        protected List<RetroInquiryDetailPopup> GetAllDetailDataPopup(string RetroDoc)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<RetroInquiryDetailPopup> AllDetailDeta = new List<RetroInquiryDetailPopup>();
            AllDetailDeta = db.Fetch<RetroInquiryDetailPopup>("GetRetroDetailPopup", new string[] { RetroDoc });
            db.Close();
            return AllDetailDeta;
        }

        public ActionResult ReloadGridRetroDetail()
        {
            string RetroDoc = Request.Params["RetroDocNo"];
            ListRetroInquiryDetailPopup = GetAllDetailDataPopup(RetroDoc);
            Model.AddModel(ListRetroInquiryDetailPopup);
            return PartialView("PopupGridRetroDetail", Model);
        }
        #endregion
    }
}
