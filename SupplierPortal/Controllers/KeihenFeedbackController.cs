﻿using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Portal.Models.KeihenFeedback;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;

namespace Portal.Controllers
{
    public class KeihenFeedbackController : BaseController
    {
        //
        // GET: /KeihenFeedback/

        public KeihenFeedbackController()
            : base("Keihen Feedback")
        {

        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void StartUp() { 
        
        }

        protected override void Init()
        {
            /*
            IDBContext db = DbContext;
            try
            {
                string supp = string.Empty;
                List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(getSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");
                //KeihenFeedbackModel models = new KeihenFeedbackModel();
                if (suppliers.Count > 0)
                {
                    ViewData["suppliers"] = suppliers;
                    ViewData["supplierPlant"] = db.Fetch<SupplierPlant>("GetAllSupplierPlantForecast", new object[] { suppliers[0].SUPPLIER_PLANT_CD });
                }
                else {
                    ViewData["suppliers"] = getSupplier();
                    ViewData["supplierPlant"] = db.Fetch<SupplierPlant>("GetAllSupplierPlantForecast", new object[] { supp });
                }

                //ViewData["suppliers"] = db.Fetch<SupplierName>("GetAllSupplierForecast");
                //ViewData["supplierPlant"] = db.Fetch<SupplierPlant>("GetAllSupplierPlantForecast", new object[] { supp });

                //ViewData["test"] = Model.GetModel<User>().IsAuthorized("Forecast", "TentativeUnlock");
                //return PartialView("KeihenFeedbackHeaderParam", )


            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                db.Close();
            }
             * */
        }

    


        public List<SupplierName> getSupplier() {
            IDBContext db = DbContext;
            List<SupplierName> list = db.Fetch<SupplierName>("GetAllSupplierForecast");
            return list;
            
        }

        public ActionResult PartialHeaderKeihenFeedbackHeader()
        {
            IDBContext db = DbContext;
            try
            {
                string supp = string.Empty;
                List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(getSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");
                //KeihenFeedbackModel models = new KeihenFeedbackModel();
                if (suppliers.Count > 0)
                {
                    Model.AddModel(suppliers);
                    supp = suppliers[0].SUPPLIER_CODE;
                }
                else
                {
                    Model.AddModel(getSupplier());
                }
                
                Model.AddModel(db.Fetch<SupplierPlant>("GetAllSupplierPlantForecast", new object[] { supp }));
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                db.Close();
            }

           return PartialView("KeihenFeedbackHeaderParam", Model);
        }

        public ActionResult PartialHeaderSupplierLookupGrid()
        {

            IDBContext db = DbContext;
            List<SupplierName> suppliers = new List<SupplierName>();
            try
            {
                TempData["GridName"] = "FHSupplierIDOption";

                suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(getSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");

                if (suppliers.Count > 0) {
                    suppliers = getSupplier();
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                db.Close();
            }

            return PartialView("GridLookup/PartialGrid", suppliers);
        
        }


        public ActionResult PartialHeaderSupplierPlantLookupGrid()
        {
            IDBContext db = DbContext;
            string supp = string.Empty;
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(getSupplier(), "SUPPLIER_CODE", "Forecast", "FHSupplierIDOption");
            List<SupplierPlant> plant = new List<SupplierPlant>();

            if(suppliers.Count > 1) 
            {
                supp = suppliers[0].SUPPLIER_CODE;
            }

            supp = (Request.Params["SupplierCode"] == "" || Request.Params["SupplierCode"] == null) ? supp : Request.Params["SupplierCode"];


            try
            {
                TempData["GridName"] = "FHSupplierPlanCodeOption";

                plant = db.Fetch<SupplierPlant>("GetAllSupplierPlantForecast", new object[] { supp });
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                db.Close();
            }

            return PartialView("GridLookup/PartialGrid", plant);

        }



        public ActionResult PartialKeihenFeedbackGrid() 
        {
            KeihenFeedbackModel KH = new KeihenFeedbackModel();
            Model.AddModel(KH);

            return PartialView("KeihenFeedbackHeaderGrid", Model);
        }

        public ContentResult countDataResume(string SupplierCode, string SupplierPlantCode, string ProdMonth) {
            string result = "";
            int count = 0;
            IDBContext db = DbContext;

            try
            {
                count = db.SingleOrDefault<int>("CountKeihenResume", new object[] {SupplierCode,SupplierPlantCode, ProdMonth});
                if (count > 0)
                {
                    result = "download";
                }
                else {
                    result = "no data";
                }
            }
            catch (Exception ex) {
                result = ex.Message;
            }
            finally{
                db.Close();
            }

            return Content(result);
        }


        public ActionResult UnlockKeihen(string prodMonth, string suppCd, string plant) {
            IDBContext db = DbContext;
            string message = "";
            try
            {
                message = db.SingleOrDefault<string>("unlockKeihenFeedback", new object[]{prodMonth, suppCd, plant});
            }
            catch (Exception ex) {
                message = ex.Message;
            }
            finally{
                db.Close();
            }

            return Content(message);

        }

        public void downloadDataResume(string SupplierCode, string SupplierPlantCode, string ProdMonth) {
            ICompression compress = ZipCompress.GetInstance();
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
            Stream output = new MemoryStream();
            byte[] documentBytes;

            DateTime today = DateTime.Today;

            string date = today.ToString("dd-MM-yyyy");


            string filename = "Resume_Keihen_Feedback_" + date + ".xls";
            string zipname = "Resume_Keihen_Feedback_" + date + ".zip";


            documentBytes = downloadProcess(SupplierCode, SupplierPlantCode, ProdMonth);

            listCompress.Add(filename, documentBytes);

            compress.Compress(listCompress, output);
            documentBytes = new byte[output.Length];
            output.Position = 0;
            output.Read(documentBytes, 0, documentBytes.Length);

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/zip";
            Response.Cache.SetCacheability(HttpCacheability.Private);

            Response.Expires = 0;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipname));
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(documentBytes);
            Response.Flush();
            Response.End(); 


        }


        public byte[] downloadProcess(string suppCode, string suppPlantCode, string prodMonth)
        {
            try
            {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                string title = "Keihen Feedback";
                title = title.ToUpper();

                //filesTmp = HttpContext.Request.MapPath("~/Template/User_maintenance_template.xls");

                //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook();

                IRow Hrow;

                int row = 3;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);


                #endregion

                List<KeihenResume> datas = new List<KeihenResume>();
                datas = db.Fetch<KeihenResume>("GetDataForKeihenFeedbackResumeDownload", new object[] { suppCode, suppPlantCode, prodMonth });

                ISheet sheet;

                sheet = workbook.CreateSheet("KeihenResumeData");
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue(title);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                Hrow.GetCell(0).CellStyle = styleContent4;

                Hrow = sheet.CreateRow(2);

                //sheet.AutoSizeColumn(0);
                Hrow.CreateCell(0).SetCellValue("Supplier Plant Code");
                Hrow.CreateCell(1).SetCellValue("Supplier Name");
                Hrow.CreateCell(2).SetCellValue("Production Month");
                Hrow.CreateCell(3).SetCellValue("CFC");
                Hrow.CreateCell(4).SetCellValue("Keihen");
                Hrow.CreateCell(5).SetCellValue("Confirmation");
                Hrow.CreateCell(6).SetCellValue("Lock Status");
                Hrow.CreateCell(7).SetCellValue("Reply By");
                Hrow.CreateCell(8).SetCellValue("Reply Date");
                Hrow.CreateCell(9).SetCellValue("File Downloaded");
                Hrow.CreateCell(10).SetCellValue("Downloaded By");

                for (int cell = 0; cell < 11; cell++)
                {
                    Hrow.GetCell(cell).CellStyle = styleContent2;
                }

                int no = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(data.SS_PLANT_CD);
                    Hrow.CreateCell(1).SetCellValue(data.SUPPLIER_NAME);
                    Hrow.CreateCell(2).SetCellValue(data.PACK_MONTH);
                    Hrow.CreateCell(3).SetCellValue(data.CFC);
                    Hrow.CreateCell(4).SetCellValue(data.KEIHENVOLUME);
                    Hrow.CreateCell(5).SetCellValue(data.CONFIRMATION);
                    Hrow.CreateCell(6).SetCellValue(data.LOCK);
                    Hrow.CreateCell(7).SetCellValue(data.FEEDBACK_BY);
                    Hrow.CreateCell(8).SetCellValue((data.FEEDBACK_DT.ToString("dd.MM.yyyy") == "01.01.0001") ? "" : data.FEEDBACK_DT.ToString("dd.MM.yyyy HH:mm"));
                    Hrow.CreateCell(9).SetCellValue((data.DOWNLOADED_BY != "") ? "Downloaded" : "");
                    Hrow.CreateCell(10).SetCellValue(data.DOWNLOADED_BY);
                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent1;

                    for (int cel = 2; cel < 11; cel++)
                    {
                        Hrow.GetCell(cel).CellStyle = styleContent3;
                    }

                    row++;
                    no++;
                }





                //for (int autosize = 0; autosize < 9; autosize++)
                //{
                   //sheet.AutoSizeColumn(4);
                   //sheet.AutoSizeColumn(autosize);

                //}


                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                //ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();
                return a;
            }
        }


        public byte[] downloadProcessForDetail(string prodMonth, string plantCd, string suppCd, string modeDownload)
        {
            try
            {
                string filesTmp = "";
                string username = AuthorizedUser.Username;

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                string title = "";
                int maxColumn = 0;
                if (modeDownload == "keihenRevisionOrder")
                {
                    title = "Keihen Revision Order - Supplier";
                    maxColumn = 8;
                }
                else { 
                    title = "Keihen Revision Order - TMMIN";
                    maxColumn = 13;
                }

                title = title.ToUpper();

                //filesTmp = HttpContext.Request.MapPath("~/Template/User_maintenance_template.xls");

                //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook();

                IRow Hrow;

                int row = 3;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);


                #endregion

                List<keihenRevision> datas = new List<keihenRevision>();
                datas = db.Fetch<keihenRevision>("GetDataForKeihenFeedbackDetailDownload", new object[] { prodMonth, suppCd, plantCd });

                ISheet sheet;

                if (modeDownload == "keihenRevisionOrder")
                {
                    sheet = workbook.CreateSheet("KeihenData");

                    db.Execute("updateKeihenFeedbackDownload", new object[] { prodMonth, suppCd, plantCd, username});

                }
                else
                {
                    sheet = workbook.CreateSheet("KeihenFeedbackResult");
                }
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue(title);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                Hrow.GetCell(0).CellStyle = styleContent4;

                Hrow = sheet.CreateRow(2);

                //sheet.AutoSizeColumn(0);
                Hrow.CreateCell(0).SetCellValue("PACK MONTH");
                Hrow.CreateCell(1).SetCellValue("PART_NO");
                Hrow.CreateCell(2).SetCellValue("PART NAME");
                Hrow.CreateCell(3).SetCellValue("SUPPLIER CODE");
                Hrow.CreateCell(4).SetCellValue("SUPPLIER PLANT");
                Hrow.CreateCell(5).SetCellValue("DOCK CODE");
                Hrow.CreateCell(6).SetCellValue("ORIGINAL VOLUME");
                Hrow.CreateCell(7).SetCellValue("NEW VOLUME");
                Hrow.CreateCell(8).SetCellValue("FLUCT");

                if (modeDownload != "keihenRevisionOrder")
                {
                    Hrow.CreateCell(9).SetCellValue("REPLY");
                    Hrow.CreateCell(10).SetCellValue("REPLY PLANT CAP");
                    Hrow.CreateCell(11).SetCellValue("REPLY RAW COMP");
                    Hrow.CreateCell(12).SetCellValue("REPLY RAW MAT");
                    Hrow.CreateCell(13).SetCellValue("REPLY QTY SUPPLY REFF");
                }

                for (int cell = 0; cell <= maxColumn; cell++)
                {
                    Hrow.GetCell(cell).CellStyle = styleContent2;
                }

                int no = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(data.PACK_MONTH);
                    Hrow.CreateCell(1).SetCellValue(data.PART_NO);
                    Hrow.CreateCell(2).SetCellValue(data.PART_NAME);
                    Hrow.CreateCell(3).SetCellValue(data.SUPPLIER_CD);
                    Hrow.CreateCell(4).SetCellValue(data.S_PLANT_CD);
                    Hrow.CreateCell(5).SetCellValue(data.DOCK_CD);
                    Hrow.CreateCell(6).SetCellValue(data.N_1_ORIGINAL);
                    Hrow.CreateCell(7).SetCellValue(data.N_1_NEW);
                    Hrow.CreateCell(8).SetCellValue(data.FLUCTUATION);

                    if (modeDownload != "keihenRevisionOrder")
                    {
                        Hrow.CreateCell(9).SetCellValue(data.REPLY);
                        Hrow.CreateCell(10).SetCellValue(data.PLANT_CAP);
                        Hrow.CreateCell(11).SetCellValue(data.RAW_COMP);
                        Hrow.CreateCell(12).SetCellValue(data.RAW_MAT);
                        Hrow.CreateCell(13).SetCellValue(data.QTY_SUPP_REFF);
                    }

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent1;

                    for (int cel = 2; cel <= maxColumn; cel++)
                    {
                        Hrow.GetCell(cel).CellStyle = styleContent3;
                    }

                    row++;
                    no++;
                }

                for (int autosize = 0; autosize <= maxColumn; autosize++)
                {
                    //sheet.AutoSizeColumn(4);
                    sheet.AutoSizeColumn(autosize);

                }


                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                //ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();
                return a;
            }
        }


        public void downloadkeihen(string prodMonth, string plantCd, string suppCd, string modeDownload)
        {
            
            //ICompression compress = ZipCompress.GetInstance();
            //Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
            //Stream output = new MemoryStream();
            byte[] documentBytes;

            DateTime today = DateTime.Today;

            string date = today.ToString("dd-MM-yyyy");
            string filename = "";

            if (modeDownload == "keihenRevisionOrder")
            {
                filename = "KeihenData_" + prodMonth +"_"+ suppCd + "-" + plantCd + ".xls";
            
            }
            else {
                filename = "ResumeKeihenFeedback_" + prodMonth + "_" + suppCd + "-" + plantCd + ".xls";
            }


            documentBytes = downloadProcessForDetail(prodMonth, plantCd, suppCd, modeDownload);

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/octet-stream";
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = 0;
            Response.Buffer = true;
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");
            Response.AddHeader("Content-Disposition",
                                string.Format("{0};FileName=\"{1}\"",
                                                "attachment",
                                                filename));
            Response.BinaryWrite(documentBytes);
            Response.Flush();
            Response.End();
        }



        public ActionResult PartialKeihenFeedbackGridReload()
        {
            KeihenFeedbackModel KH = new KeihenFeedbackModel();
            //ViewData["userlogin"] = AuthorizedUser.Username;

            string mode = Request.Params["clearMode"];
            string supp = Request.Params["SupplierCode"];
            string suppPlant = Request.Params["SupplierPlantCode"];
            string prodMonth = Request.Params["ProdMonth"];

            IDBContext db = DbContext;
            try
            {
                if (mode == "N")
                {
                    KH.keihenData = db.Fetch<keihenHeaderData>("getKeihenFeedbackHeaderData", new object[] { supp, suppPlant, prodMonth });
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                db.Close();
            }

            Model.AddModel(KH);

            return PartialView("KeihenFeedbackHeaderGrid", Model);
        }



        public ActionResult PartialEmptyKeihenFeedbackDetail() 
        {
            KeihenFeedbackModel kh = new KeihenFeedbackModel();
            Model.AddModel(kh);
            return PartialView("DetailKeihenFeedbackContainer", Model);
        }

        public ActionResult PartialKeihenFeedbackDetail()
        {
            try
            {
                IDBContext db = DbContext;
                string prodMonth = Request.Params["prodMonth"];
                string suppCd = Request.Params["suppCd"];
                string plantCd = Request.Params["suppPlant"];

                KeihenFeedbackModel dm = new KeihenFeedbackModel();

                dm.detailKeihen = db.Fetch<detailKeihen>("getSpecifiedDetailKeihen", new object[] { prodMonth, suppCd, plantCd });

                Model.AddModel(dm);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DbContext.Close();
            }

            return PartialView("DetailKeihenFeedbackContainer", Model);
        
        }




        public ActionResult PartialKeihenFeedbackDetailGridReload()
        {
            try
            {
                IDBContext db = DbContext;
                string prodMonth = Request.Params["prodMonth"];
                string suppCd = Request.Params["suppCd"];
                string plantCd = Request.Params["suppPlant"];

                KeihenFeedbackModel dm = new KeihenFeedbackModel();

                dm.detailKeihen = db.Fetch<detailKeihen>("getSpecifiedDetailKeihen", new object[] { prodMonth, suppCd, plantCd });

                Model.AddModel(dm);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally {
                DbContext.Close();
            }

            return PartialView("keihenDetailGrid", Model);

        
        }

        [HttpPost]
        public void uploadKeihen_CallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("uploadKeihenFeedbackDetail", ValidationSettings, KeihenDetail_FileUploadComplete);
        
        }

        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/KeihenFeedback/UploadTemp/");
                return _uploadDirectory;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };
        protected void KeihenDetail_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }


        public ActionResult saveDataUpload(string filePath, string PACK_MONTH, string SUPPLIER_CD, string SUPPLIER_PLANT) {
            string message = "";
            IDBContext db = DbContext;

            string FUNCTION_ID = "999999";
            string LOC = "KeihenFeedback.Upload";
            string MODULE_ID = "1";
            Int64 PROCESS_ID = 0;

            try
            {
                message = "success";




                PROCESS_ID = db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                                    "Upload Supplier Feedback Data Started for Supplier " + 
                                                                    SUPPLIER_CD+ ", Plant " + 
                                                                    SUPPLIER_PLANT + ", Pack Month " +
                                                                    PACK_MONTH,
                                                                    AuthorizedUser.Username,
                                                                    LOC,
                                                                    PROCESS_ID,
                                                                    "MSG001INF",
                                                                    "INF",
                                                                    MODULE_ID,
                                                                    FUNCTION_ID,
                                                                    0
                                                                });

                OleDbConnection excelConn = null;
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;IMEX=1;ImpoerMixedTypes=Text\"";
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();


                string OleCommand = @"SELECT " + FUNCTION_ID + " " + " as FUNCTION_ID, " + " "
                                               + PROCESS_ID + " " + " as PROCESS_ID, " + " " +
                                               @"IIf(IsNull(F1), Null, CStr(F1)) as PACK_MONTH," +
                                               @"IIf(IsNull(F2), Null, CStr(F2)) as DOCK_CD," +
                                               @"IIf(IsNull(F3), Null, CStr(F3)) as PART_NO," +
                                               @"IIf(IsNull(F5), Null, CStr(F5)) as SUPPLIER_CD," +
                                               @"IIf(IsNull(F6), Null, CStr(F6)) as S_PLANT_CD," +

                                               @"IIf(IsNull(F7), Null, CStr(F7)) as N_1_ORIGINAL," +
                                               @"IIf(IsNull(F8), Null, CStr(F8)) as N_1_NEW," +
                                               @"IIf(IsNull(F9), Null, CStr(F9)) as FLUCTUATION," +

                                               @"IIf(IsNull(F10), Null, CStr(F10)) as REPLY," +
                                               @"IIf(IsNull(F11), Null, CStr(F11)) as PLANT_CAP," +
                                               @"IIf(IsNull(F12), Null, CStr(F12)) as RAW_COMP," +
                                               @"IIf(IsNull(F13), Null, CStr(F13)) as RAW_MAT," +
                                               @"IIf(IsNull(F14), Null, CStr(F14)) as QTY_SUPP_REFF"
                                               + " "
                                               + "FROM [Keihen Feedback$] ";

                excelCommand = new OleDbCommand(OleCommand, excelConn);
                excelDataAdapter.SelectCommand = excelCommand;
                excelDataAdapter.Fill(dtPatterns);

                dtPatterns = DeleteEmptyRows(dtPatterns);

                ds.Tables.Add(dtPatterns);

                DataRow rowDel = ds.Tables[0].Rows[0];
                ds.Tables[0].Rows.Remove(rowDel);

                string t_prodMonth = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                string t_supplier = ds.Tables[0].Rows[0].ItemArray[5].ToString();
                string t_plant = ds.Tables[0].Rows[0].ItemArray[6].ToString();

                if (PACK_MONTH != t_prodMonth || SUPPLIER_CD != t_supplier || SUPPLIER_PLANT != t_plant)
                {
                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                            "Error occured, File not valid because parameter on screen not match with file.",
                            AuthorizedUser.Username,
                            LOC,
                            PROCESS_ID,
                            "MSG001INF",
                            "INF",
                            MODULE_ID,
                            FUNCTION_ID,
                            2
                        });


                    return Json("errorNotMatch|File invalid, Please upload file with PACK_MONTH: " + PACK_MONTH + " , SUPPLIER-CD: " + SUPPLIER_CD + "-" + SUPPLIER_PLANT);
                }


                db.ExecuteScalar<Int64>("Putlog", new object[] { 
                            "Start copy data from file to TB_T_KEIHEN_FEEDBACK",
                            AuthorizedUser.Username,
                            LOC,
                            PROCESS_ID,
                            "MSG001INF",
                            "INF",
                            MODULE_ID,
                            FUNCTION_ID,
                            2
                        });

                //using (SqlBulkCopy bulkCopy = new SqlBulkCopy("Server=10.16.20.230;Database=PCS_DB;User ID=K2;Password=K2pass!;Trusted_Connection=false;MultipleActiveResultSets=true;"))
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //bulkCopy.DestinationTableName = "dbo.TB_T_SUPPLIER_FEEDBACK_UPLOAD_JKS";
                    bulkCopy.DestinationTableName = "TB_T_KEIHEN_FEEDBACK";
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("PACK_MONTH", "PACK_MONTH");
                    bulkCopy.ColumnMappings.Add("PART_NO", "PART_NO");
                    bulkCopy.ColumnMappings.Add("SUPPLIER_CD", "SUPPLIER_CD");
                    bulkCopy.ColumnMappings.Add("S_PLANT_CD", "S_PLANT_CD");
                    bulkCopy.ColumnMappings.Add("DOCK_CD", "DOCK_CD");
                    bulkCopy.ColumnMappings.Add("REPLY", "REPLY");
                    bulkCopy.ColumnMappings.Add("PLANT_CAP", "PLANT_CAP");
                    bulkCopy.ColumnMappings.Add("RAW_COMP", "RAW_COMP");
                    bulkCopy.ColumnMappings.Add("RAW_MAT", "RAW_MAT");
                    bulkCopy.ColumnMappings.Add("QTY_SUPP_REFF", "QTY_SUPP_REFF");

                    bulkCopy.ColumnMappings.Add("N_1_ORIGINAL", "N_1_ORIGINAL");
                    //bulkCopy.ColumnMappings.Add("N_1_NEW", "N_1_NEW");
                    bulkCopy.ColumnMappings.Add("FLUCTUATION", "FLUCTUATION");

                    // Write from the source to the destination.
                    // Check if the data set is not null and tables count > 0 etc
                    bulkCopy.WriteToServer(ds.Tables[0]);

                }

                db.ExecuteScalar<Int64>("Putlog", new object[] {"Success copy data from file to TB_T_KEIHEN_FEEDBACK",AuthorizedUser.Username,LOC,PROCESS_ID,
                            "MSG001INF","INF",MODULE_ID,FUNCTION_ID,2});

                //uploaded data must match the data at screen (same pack month, supplier + supplier plant, rows, and part
                message = db.ExecuteScalar<string>("validateUploadedDataMatchScreen", new object[] { PACK_MONTH, SUPPLIER_CD, SUPPLIER_PLANT, PROCESS_ID });


                if (message == "success") // [do a basic validate]
                {
                    #region log basic validation
                    db.ExecuteScalar<Int64>("Putlog", new object[] {"Start process basic validation for uploaded data",AuthorizedUser.Username,LOC,PROCESS_ID,"MSG001INF",
                            "INF",MODULE_ID,FUNCTION_ID,2
                        });
                    #endregion
                    message = basicValidation(PROCESS_ID);

                    if (message == "success") // [if success do a basic validate, then do business validate]
                    {
                        #region log business validation
                        db.ExecuteScalar<Int64>("Putlog", new object[] {"Basic validation success, Start process business validation for uploaded data",AuthorizedUser.Username,LOC,
                            PROCESS_ID,"MSG001INF","INF",MODULE_ID,FUNCTION_ID,2});
                        #endregion
                        message = validateDataUpload(PROCESS_ID);

                        if (message == "success")//copy data from tb_t_to tb_r
                        {
                            #region log update data tb_t_ to tb_r_
                            db.ExecuteScalar<Int64>("Putlog", new object[] {"Start update data to TB_R_KEIHEN_FEEDBACK from TB_T_KEIHEN_FEEDBACK",AuthorizedUser.Username,LOC,PROCESS_ID,
                            "MSG001INF","INF",MODULE_ID,FUNCTION_ID,2});
                            #endregion

                            //copy data to tb_r_keihen_feedback
                            db.Execute("updateKeihenFeedbackFromUpload", new object[] { PROCESS_ID, AuthorizedUser.Username });
                        }
                        else // [fail when do a business validation]
                        {
                            db.ExecuteScalar<Int64>("Putlog", new object[] { "Fail when do a bussiness validate",AuthorizedUser.Username,LOC,PROCESS_ID,"MSG001INF","INF",
                            MODULE_ID,FUNCTION_ID,2});
                        }
                    }
                    else // [fail when do a basic validate]
                    {
                        db.ExecuteScalar<Int64>("Putlog", new object[] {"Fail when do a basic validate for the uploaded data",AuthorizedUser.Username,LOC,PROCESS_ID,"MSG001INF",
                            "INF",MODULE_ID,FUNCTION_ID,2});
                    }
                }
                else // [error data not match]
                {
                    #region log data not match
                    db.ExecuteScalar<Int64>("Putlog", new object[] {"Data between uploaded file and screen not match",AuthorizedUser.Username,LOC,PROCESS_ID,"MSG001INF","INF",
                            MODULE_ID,FUNCTION_ID,2
                        });
                    #endregion
                }

                if (message == "success")
                {
                    db.ExecuteScalar<Int64>("Putlog", new object[] {"Finish update data to TB_R_KEIHEN_FEEDBACK from TB_T_KEIHEN_FEEDBACK",AuthorizedUser.Username,LOC,PROCESS_ID,
                            "MSG001INF","INF",MODULE_ID, FUNCTION_ID, 2});


                    db.ExecuteScalar<Int64>("Putlog", new object[] {"Delete temporary data",AuthorizedUser.Username,LOC,PROCESS_ID,
                            "MSG001INF","INF",MODULE_ID, FUNCTION_ID, 2});


                    db.Execute("deleteKeihenFeedbackTemporaryData", new object[] { PROCESS_ID });

                    db.ExecuteScalar<Int64>("Putlog", new object[] {"Finish delete temporary data",AuthorizedUser.Username,LOC,PROCESS_ID,
                            "MSG001INF","INF",MODULE_ID, FUNCTION_ID, 2});
                }


                message = message + '|' + PROCESS_ID;


            }
            catch (Exception ex)
            {
                message = ex.Message;

                db.ExecuteScalar<Int64>("Putlog", new object[] { 
                            "Error occured, "+message,
                            AuthorizedUser.Username,
                            LOC,
                            PROCESS_ID,
                            "MSG001INF",
                            "INF",
                            MODULE_ID,
                            FUNCTION_ID,
                            2
                        });
            }
            finally {
                
                db.ExecuteScalar<Int64>("Putlog", new object[] {"Process finish", AuthorizedUser.Username, LOC, PROCESS_ID, "MSG001INF",
                            "INF", MODULE_ID, FUNCTION_ID, 2});

                db.Close();
            }

            return Json(message);
        
        }

        public DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            List<DataRow> drList = new List<DataRow>();
            foreach (DataRow dr in formattedTable.Rows)
            {
                int count = dr.ItemArray.Length;
                int nullcounter = 0;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])) || dr.ItemArray[i].ToString().Length <= 0)
                    {
                        nullcounter++;
                    }
                }

                if (nullcounter == count - 2)
                {
                    drList.Add(dr);
                }
            }

            for (int i = 0; i < drList.Count; i++)
            {
                formattedTable.Rows.Remove(drList[i]);
            }
            formattedTable.AcceptChanges();

            return formattedTable;
        }

        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else if (pi.PropertyType == typeof(Int32?))
                            dt.Columns.Add(pi.Name, typeof(Int32));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public JsonResult SaveData(DataDetailFeedbackModel pTable)
        {
            string FUNCTION_ID = "999999";
            string LOC = "KeihenFeedback.SaveFromScreen";
            string MODULE_ID = "1";
            Int64 PROCESS_ID = 0;
            string result = "success";
            

            IDBContext db = DbContext;

            if (pTable.detailTemporary == null) {
                pTable.detailTemporary = new List<detailKeihenTemporary>();
            }

            if (pTable.detailTemporary.Count == 0) {
                return Json("No Data Found");
            }

            string mode = pTable.detailTemporary[0].OVERALLMODE;

            try
            {

                PROCESS_ID = db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                                    "Update Supplier Feedback Data Started for Supplier " + 
                                                                    pTable.detailTemporary[0].SUPPLIER_CD+ ", Plant " + 
                                                                    pTable.detailTemporary[0].SUPPLIER_PLANT + ", Pack Month " +
                                                                    pTable.detailTemporary[0].PACK_MONTH,
                                                                    AuthorizedUser.Username,
                                                                    LOC,
                                                                    PROCESS_ID,
                                                                    "MSG001INF",
                                                                    "INF",
                                                                    MODULE_ID,
                                                                    FUNCTION_ID,
                                                                    0
                                                                });


                pTable.detailTemporary.ForEach(x => x.PROCESS_ID = PROCESS_ID);
                pTable.detailTemporary.ForEach(x => x.FEEDBACK_BY = AuthorizedUser.Username);

                DataTable dt = ConvertToDataTable(pTable.detailTemporary);

                using (SqlBulkCopy bcp = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {

                    bcp.BulkCopyTimeout = 30;
                    bcp.DestinationTableName = "TB_T_KEIHEN_FEEDBACK";
                    bcp.ColumnMappings.Clear();
                    bcp.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bcp.ColumnMappings.Add("PACK_MONTH", "PACK_MONTH");
                    bcp.ColumnMappings.Add("SUPPLIER_CD", "SUPPLIER_CD");
                    bcp.ColumnMappings.Add("SUPPLIER_PLANT", "S_PLANT_CD");
                    bcp.ColumnMappings.Add("DOCK_CD", "DOCK_CD");
                    bcp.ColumnMappings.Add("PART_NO", "PART_NO");
                    bcp.ColumnMappings.Add("REPLY", "REPLY");
                    bcp.ColumnMappings.Add("PLANT_CAP", "PLANT_CAP");
                    bcp.ColumnMappings.Add("RAW_COMP", "RAW_COMP");
                    bcp.ColumnMappings.Add("RAW_MAT", "RAW_MAT");
                    bcp.ColumnMappings.Add("QTY_SUPP_REFF", "QTY_SUPP_REFF");
                    bcp.ColumnMappings.Add("FEEDBACK_BY", "FEEDBACK_BY");

                    bcp.WriteToServer(dt);
                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Success Perform Bulk Insert to TB_T_KEIHEN_FEEDBACK",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });

                }



                if (mode == "manual")
                {
                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Do validate for data",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });

                    result = validateDataManualPick(PROCESS_ID);
                }
                else if(mode == "NG") {
                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Validate for data",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });
                    result = validateDataUsingOverallNG(PROCESS_ID);
                }

                if (result == "success")
                {
                    //abc
                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Validate success, Update data TB_R_KEIHEN_FEEDBACK",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });

                    db.Execute("updateKeihenFeedbackFromScreen", new object[] { PROCESS_ID, AuthorizedUser.Username });


                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Success update data TB_R_KEIHEN_FEEDBACK",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });


                }
                else {
                    db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Fail when validate the data",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;


                db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Error occured, "+ex.Message,
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });
                //if (PROCESS_ID == 0) {
                //   result = ex.Message;
                //}

            }
            finally {

                db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Deleting Temporary data in TB_T_KEIHEN_FEEDBACK",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });

                db.Execute("deleteKeihenFeedbackTemporaryData", new object[] { PROCESS_ID });

                db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Success delete Temporary data in TB_T_KEIHEN_FEEDBACK",
                                                    AuthorizedUser.Username,
                                                    LOC,
                                                    PROCESS_ID,
                                                    "MSG001INF",
                                                    "INF",
                                                    MODULE_ID,
                                                    FUNCTION_ID,
                                                    1
                                                });

                db.Close();
            }

            return Json(result);
        }

        //validate when the data are manually inputed at screen
        public string validateDataManualPick(Int64 PID) {
            IDBContext db = DbContext;            
            string result = "";
            result = db.ExecuteScalar<string>("ValidateDataKeihenFeedbackManualPick", new object[] { PID });
            db.Close();
            return result;
        }

        public string validateDataUsingOverallNG(Int64 PID)
        {
            IDBContext db = DbContext;
            string result = "";
            result = db.ExecuteScalar<string>("ValidateDataKeihenFeedbackUsingOverallNG", new object[] { PID });
            db.Close();

            return result;
        }

        
        //basic validation for upload
        public string basicValidation(Int64 PID)
        {
            IDBContext db = DbContext;
            string result = "";
            result = db.ExecuteScalar<string>("ValidateDataKeihenFeedbackDataViaUpload", new object[] { PID });
            db.Close();
            return result;
        }
        
        //business validation for upload
        public string validateDataUpload(Int64 PID)
        {
            IDBContext db = DbContext;
            string result = "";
            result = db.ExecuteScalar<string>("BusinessValidateDataKeihenFeedbackDataViaUpload", new object[] { PID });
            db.Close();
            return result;
        }


        public void DownloadTemplate(string productionMonth, string supplierCode, string supplierPlanCode)
        {
            IDBContext db = DbContext;

            string filename = "KeihenFeedbackTemplate.xls";
            IList<downloaddetailKeihen> datas = db.Fetch<downloaddetailKeihen>("getSpecifiedDetailKeihenTemplate", new object[] { productionMonth, supplierCode, supplierPlanCode });

            IExcelWriter exporter = new NPOIWriter();

            exporter.Append(datas, "Keihen Feedback");
            byte[] result = exporter.Flush();

            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(result.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", filename));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(result);
            Response.End();
        
        }



        public void downloadError(string pid)
        {
            IDBContext db = DbContext;


            string filename = "InvalidKeihenUpload.xls";
            IList<downloaddetailKeihenError> datas = db.Fetch<downloaddetailKeihenError>("getDetailKeihenErrorUpload", new object[] { pid });


            //delete data
            db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Deleting Temporary data in TB_T_KEIHEN_FEEDBACK",
                                                    AuthorizedUser.Username,
                                                    "downloadError",
                                                    pid,
                                                    "MSG001INF",
                                                    "INF",
                                                    "1",
                                                    "99999",
                                                    1
                                                });

            db.Execute("deleteKeihenFeedbackTemporaryData", new object[] { pid });

            db.ExecuteScalar<Int64>("Putlog", new object[] { 
                                                    "Success delete Temporary data in TB_T_KEIHEN_FEEDBACK",
                                                    AuthorizedUser.Username,
                                                    "downloadError",
                                                    pid,
                                                    "MSG001INF",
                                                    "INF",
                                                    "1",
                                                    "99999",
                                                    1
                                                });


            IExcelWriter exporter = new NPOIWriter();

            exporter.Append(datas, "Keihen Feedback");

            byte[] result = exporter.Flush();

            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(result.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", filename));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(result);
            Response.End();
            


        }



    }
}
