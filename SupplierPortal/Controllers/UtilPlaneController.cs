﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.PartInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;
using Portal.Models.Dock;
using Portal.Models.UtilPlane;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.Plant;

namespace Portal.Controllers
{
    public class UtilplaneController : BaseController
    {

        public UtilplaneController()
            : base("Util Plane : ")
        {
        }

        protected override void StartUp()
        {

        }     

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<SupplierName> _supplierModel = null;
        private List<SupplierName> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<SupplierName>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<PartData> _partModel = null;
        private List<PartData> _PartModel
        {
            get
            {
                if (_partModel == null)
                    _partModel = new List<PartData>();

                return _partModel;
            }
            set
            {
                _partModel = value;
            }
        }


        protected override void Init()
        {

            IDBContext db = DbContext;

            UtilPlaneModel modelUtilPlaneInfo = new UtilPlaneModel();
            modelUtilPlaneInfo.UtilPlaneDatas = getPartInfo("","","");
            Model.AddModel(modelUtilPlaneInfo);
            
            
        }

        [HttpGet]
        public void DownloadSheet(string DockCode)
        {

            string fileName = "UtilPlane" + ".xls";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "UtilPlane", "OIHSupplierOptionUtilPlane");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "UtilPlane", "OIHDockCodeUtilPlane");

            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }
            IEnumerable<UtilPlaneData> listGeneral = db.Query<UtilPlaneData>("GetUtilPlane", new object[] { DockCode,SupplierCodeLDAP, DockCodeLDAP });
           IExcelWriter exporter = ExcelWriter.GetInstance();
           exporter.Append(listGeneral, "UtilPlane");
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            byte[] allHasil = exporter.Flush();
            Response.BinaryWrite(allHasil);
            Response.End();
        }

        public ActionResult PartialHeader()
        {
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "UtilPlane", "OIHSupplierOptionUtilPlane");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            ViewData["GridPart"] = GetAllPart();
            ViewData["GridDock"] = GetAllDock();
            ViewData["GridRcv"] = GetAllPlant();

            return PartialView("PartialHeader");

        }
        public ActionResult PartialDetail()
        {
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "UtilPlane", "OIHSupplierOptionUtilPlane");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "UtilPlane", "OIHDockCodeUtilPlane");
            
            if (suppCode.Count == 1)
            {
                SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            }
            else
            {
                SupplierCodeLDAP = "";
            }
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            UtilPlaneModel model = Model.GetModel<UtilPlaneModel>();
            model.UtilPlaneDatas = getPartInfo(Request.Params["DockCode"],
                                              SupplierCodeLDAP,
                                              DockCodeLDAP);

            Model.AddModel(model);

            return PartialView("PartialDetail", Model);
        }

        protected List<UtilPlaneData> getPartInfo(string dockCdParam, string SupplierCodeLdap, string DockCdLdap)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<UtilPlaneData> listTooltipDatas = new List<UtilPlaneData>();

            listTooltipDatas = db.Query<UtilPlaneData>("GetUtilPlane", new object[] { dockCdParam, SupplierCodeLdap, DockCdLdap }).ToList();

             db.Close();
            return listTooltipDatas;
        }

        public ActionResult PartialHeaderSupplierLookupGridUtilPlane()
        {
            TempData["GridName"] = "UtilPlaneHSupplierGridLookup";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "UtilPlane", "OIHSupplierOptionUtilPlane");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }

            return PartialView("GridLookup/PartialGrid", ViewData["GridSupplier"]);

        }

        public ActionResult PartialHeaderRcvPlantGridHeaderUtilPlane()
        {
            TempData["GridName"] = "UtilPlaneHRcvPlantGridLookup";
            List<PlantData> docks = GetAllPlant();

            ViewData["GridRcv"] = docks;

            return PartialView("GridLookup/PartialGrid", ViewData["GridRcv"]);
        }

        private List<PartData> GetAllPart()
        {
            IDBContext db = DbContext;

            List<PartData> partModel = new List<PartData>();
            partModel = db.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
            db.Close();

            return partModel;

        }
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> SISupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            SISupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return SISupplierModel;
        }

        public ActionResult PartialPopupUtilPlane()
        {
            ViewData["GridPart"] = GetAllPart();
            ViewData["GridDock"] = GetAllDock();
            ViewData["GridRcv"] = GetAllPlant();

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "UtilPlane", "OIHSupplierOptionUtilPlane");
            if (suppliers.Count > 0)
            {
                ViewData["GridSupplier"] = suppliers;
            }
            else
            {
                ViewData["GridSupplier"] = GetAllSupplier();
            }
            return PartialView("PopupNew", null);
        }

        public ActionResult PartialHeaderPartLookupGridUtilPlane()
        {
            TempData["GridName"] = "OIHPartOptionUtilPlane";
             ViewData["GridPart"] = GetAllPart();
            
            return PartialView("GridLookup/PartialGrid", ViewData["GridPart"]);
        }

        public ActionResult PartialHeaderDockGridHeaderUtilPlane()
        {
            TempData["GridName"] = "OIHDockCodeUtilPlane";
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DOCK_CODE", "UtilPlane", "OIHDockCodeUtilPlane");
            
            ViewData["GridDock"] = docks;

            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);
        }

        public ActionResult PartialGridLookUpDockUtilPlane()
        {
            TempData["GridName"] = "UtilPlaneHDockGridLookup";
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DOCK_CODE", "UtilPlane", "UtilPlaneHDockGridLookup");

            ViewData["GridDock"] = docks;

            return PartialView("GridLookup/PartialGrid", ViewData["GridDock"]);

        }

        private List<PlantData> GetAllPlant()
        {
            List<PlantData> PartIndoDockModel = new List<PlantData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<PlantData>("GetAllPlant", new object[] { });
            db.Close();

            return PartIndoDockModel;

        }
        private List<DockData> GetAllDock()
        {
            List<DockData> PartIndoDockModel = new List<DockData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return PartIndoDockModel;
        }
       
        /*
        Date: January 8, 2019
        Note: update code for deploy

        start
        */
        public ActionResult AddNewUtilPlane(string Supplier, string DockCd, string ShippingDock, string RcvPlantCd,
            string PatPlFlag, string User)
        {
            string ResultQuery = "";
            string[] Suppliers = Supplier.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");
           
            User = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                string CheckData;

                
                CheckData = db.ExecuteScalar<string>("InsertUtilPlane", new object[] 
                {
                   SupplierCode,
                   SupplierPlant,
                   ShippingDock,
                   RcvPlantCd,
                   DockCd,
                   PatPlFlag,
                   User
                });

                if (CheckData == "Alreadyexists")
                  {
                    ResultQuery = " Data Util Plane Already Exists, Please Input Other Data Util Plane.. ";
                  }
               else
                 {
                    TempData["GridName"] = "PartialDetailUtilPlane";
                    ResultQuery = " Process succesfully..";
                 }
                
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }

        public ActionResult AddEditUtilPlane(string Supplier, string DockCd, string ShippingDock, string RcvPlantCd,
           String PatPlFlag, string User, string ShippingDockOld,string PatPlFlagOld)
        {
            string ResultQuery = "";
            string[] Suppliers = Supplier.Split('-');
            string SupplierCode = Suppliers[0].Replace(" ", "");
            string SupplierPlant = Suppliers[1].Replace(" ", "");

            User = AuthorizedUser.Username;
            IDBContext db = DbContext;
            try
            {
                string CheckData;


                CheckData = db.ExecuteScalar<string>("UpdateUtilPlane", new object[]
                {
                   SupplierCode,//0
                   SupplierPlant,//1
                   ShippingDock,//2
                   RcvPlantCd,//3
                   DockCd,//4
                   PatPlFlag,//5
                   User,//6
                   ShippingDockOld,//7
                   PatPlFlagOld //8

                });

                if (CheckData == "Alreadyexists")
                {
                    ResultQuery = " Data Util Plane Already Exists, Please Edit Other Data Util Plane.. ";
                }
                else
                {
                    TempData["GridName"] = "PartialDetailUtilPlane";
                    ResultQuery = " Process succesfully..";
                }

                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
        
        /* ------ End ------- */

        public ActionResult DeleteUtilPlane(string SupplierCode, string SupplierPlant, string ShippingDock,string RcvPlantCd, string DockCd, string PatPlFlag)
        {
            string ResultQuery = "Process succesfully";
            IDBContext db = DbContext;
            try
            {
                db.Execute("DeleteUtilPlane", new object[] 
                {
                   SupplierCode,
                   SupplierPlant,
                   ShippingDock,
                   RcvPlantCd,
                   DockCd,
                   PatPlFlag

                });
                TempData["GridName"] = "PartialDetailUtilPlane";
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

    }
}