﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.MessageBoard;
using Portal.Models.DriverDataTlms;
using Portal.Models.Supplier;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class DriverDataTlmsController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public DriverDataTlmsController()
            : base("Driver Data TLMS")
        {
        }
        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            DriverDataTlms md2 = new DriverDataTlms();
            List<DriverDataTlms> lst = new List<DriverDataTlms>();

            DriverDataTlmsModel model = new DriverDataTlmsModel();
            //model.DriverDataTlmss =
            //    GetAllDriverDataTlms("", "","","","","");
            
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");
           
            
            ViewData["supplierGridLookup"] = suppliers;
            ViewData["LogistickPartner"] = GetAllLogistick();
            ViewData["RouteGridLookup"] = GetAllRoute();
                //md2.DriverDataTlmsx = lst;
                Model.AddModel(model);



            }

        private List<DriverDataTlms> GetAllDriverDataTlms(string APPLTERMFROM,

            string APPLTERMTO, string RTEDATE, string LOGPARTNERCD, string LOGPTCD, string RTEGRPCD)
        {
            List<DriverDataTlms> DriverDataTlms = new List<DriverDataTlms>();
            IDBContext db = DbContext;

            DriverDataTlms = db.Fetch<DriverDataTlms>("GetAllDriverDataTlms", new object[] { 
                (APPLTERMFROM == "" || APPLTERMFROM == null) ? "" : formatingShortDate(APPLTERMFROM), 
                (APPLTERMTO == "" || APPLTERMTO == null) ? ""  : formatingShortDate(APPLTERMTO),
                //(RTEDATE),
                (RTEDATE== "" || RTEDATE == null) ? "" : formatingShortDate(RTEDATE),
                (LOGPARTNERCD),
                (LOGPTCD),
                (RTEGRPCD)

                
                
            });
            db.Close();

            return DriverDataTlms;
        }

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                result = extractString[2] + "-" + extractString[1] + "-" + extractString[0];
            }

            return result;
        }
        public ActionResult GetDriverDataGrid()
        {
            
            string cc = Request.Params["RTEGRPCD"];
            string bb = Request.Params["LOGPARTNERCD"];
            string aa = Request.Params["LOGPTCD"];
            string zz = Request.Params["RTEDATE"];
            #region Required model in partial page : for InvoiceAndPaymentInquiryPartial
            DriverDataTlmsModel model = Model.GetModel<DriverDataTlmsModel>();
            model.DriverDataTlmss = GetAllDriverDataTlms(Request.Params["APPLTERMFROM"],
                                        Request.Params["APPLTERMTO"],
                                        Request.Params["RTEDATE"],
                                        '%' + Request.Params["LOGPARTNERCD"]+'%',
            '%'+Request.Params["LOGPTCD"]+'%',
            '%' + Request.Params["RTEGRPCD"]+'%');
                                       // Request.Params["RTEDATE"],
                                        //"",
                                        //"",
                                        //Request.Params["RTEGRPCD"]);
                                        
            #endregion

            return PartialView("DriverDataTlmsGrid", Model);
        
        }

        //penambahan
        public List<Route> GetAllRoute()
        {
            List<Route> listOfRoute = new List<Route>();
            try
            {
                listOfRoute = DbContext.Fetch<Route>("GetAllRoute", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listOfRoute;
        }
        public ActionResult PartialHeaderRouteGridLookupPart()
        {

            TempData["GridName"] = "OIHRouteOption";
            ViewData["RouteGridLookup"] = GetAllRoute();

            return PartialView("GridLookup/PartialGrid", ViewData["RouteGridLookup"]);
        }

        // sampai sini penambahan nya

        public ActionResult PartialSupplierGridLookup()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");

            TempData["GridName"] = "logptcd";
            ViewData["supplierGridLookup"] = suppliers;
            //_OrderInquirySupplierModel = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["supplierGridLookup"]);
        }

        public ActionResult PartialLogistickGridLookup()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            TempData["GridName"] = "LOGPARTNERCD"; 
            ViewData["LogistickPartner"] = GetAllLogistick();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["LogistickPartner"]);
        }
        private List<LogisticPartner> GetAllLogistick()
        {
            List<LogisticPartner> orderInquirySupplierModel = new List<LogisticPartner>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<LogisticPartner>("GetAllLogisticPartner", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> orderInquirySupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierName>("GetAllSupplier", new object[] { });
            db.Close();

            return orderInquirySupplierModel;

        }
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public void dwonloaddriverdata(
            string APPLTERMFROM,
            string APPLTERMTO,
            string RTEDATE,
            string LOGPARTNERCD,
            string LOGPTCD,
            string RTEGRPCD)
        {
            string fileName = String.Empty;
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;


            DriverDataTlmsModel model = Model.GetModel<DriverDataTlmsModel>();
            model.DriverDataTlmss = GetAllDriverDataTlms
                    (APPLTERMFROM,APPLTERMTO, RTEDATE, LOGPARTNERCD, LOGPTCD, RTEGRPCD);
            fileName = "FileDriverDataTlms.xls";
            hasil = exporter.Write(ConvertToDataTable(model.DriverDataTlmss), "FileDataDriver");

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            /*
             * @Lufty: numpang nambahin, kisanak
             */
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();

        }


    }
}