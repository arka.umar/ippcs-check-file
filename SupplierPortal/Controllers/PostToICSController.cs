﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.PO;
using Portal.Models.PostToICS;
using Toyota.Common.Web.Credential;
namespace Portal.Controllers
{
    public class PostToICSController : BaseController
    {
        //
        // GET: /PostICS/

        public PostToICSController()
            : base("Post To ICS")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            PostToICSModel mdl = new PostToICSModel();
            for (int i=1;i<=40;i++)
            {
                mdl.PostToICSs.Add(new PostToICSData()
                {
                    //dummy data
                    InvoiceNo = "1101112928",
                    SupplierCode = "5000-"+i,
                    PlantCode = "1",
                    DockCode = "1L",
                    PartNumber = "450250K01000",
                    PartName = "Cover Sub-Assy Steering Column Hole",
                    OrderQty = 10,
                    OrderQtyLot = 1,
                    QtyPerContainer = 10,
                    DropStatusDate = DateTime.Now,
                    ReceivedQty = 10,
                    ICSFlag = 0,
                    CancelQty = 0,
                    CancelledQty = 0,
                    CancellationFlag = "",
                    CancellationBy = "",
                    CancellationDate = DateTime.Now
                });
            }
            Model.AddModel(mdl);
        }

        public ActionResult PartialPostToICS()
        {
            return PartialView("PartialPostData", Model);
        }
    }
}
