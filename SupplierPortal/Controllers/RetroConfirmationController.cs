﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.RetroConfirmation;

namespace Portal.Controllers
{
    public class RetroConfirmationController : BaseController
    {
        public RetroConfirmationController()
            : base("Retro Confirmation")
        {
            
        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;
            RetroConfirmationModel mdl = new RetroConfirmationModel();
            List<RetroConfirmationDetail> lst = new List<RetroConfirmationDetail>();
            List<RetroApprovaldetail> App = new List<RetroApprovaldetail>();

            mdl.RetroConfirmations.Add(new RetroConfirmation()
            {
                supplierCode="",
                RetroDocNo=""
            });

            for (int a = 1; a < 50; a++)
            {
                lst.Add(new RetroConfirmationDetail()
                {
                    RetroDocNo=""+a,
                    From=DateTime.Now,
                    To=DateTime.Now,
                    Currency="IDR",
                    Amount="5,000,000",
                    SupplierCode="5025",
                    SupplierName="AT Indonesia",
                    TMMIN="Upload  Download",
                    Supplier="",
                    InvoiceNo="001/TMMIN/12",
                    Status="-",
                    Notice = SetNoticeIconString(a)
                });
                
            }

            for (int i = 1; i < 50; i++)
            {
                App.Add(new RetroApprovaldetail()
                {
                    RetroDocNo = "" + i,
                    Amount= "5,000,000",
                    Currency="IDR",
                    PICFinance="Faridah",
                    DateFinance=DateTime.Now,
                    PICSupplier="Agus",
                    DateSupplier=DateTime.Now,
                    PICPUD="Tony",
                    DatePUD=DateTime.Now
                });
                
            }
            mdl.RetroConfirmationDetails = lst;
            mdl.RetroApprovalDetails = App;
            Model.AddModel(mdl);

        }
        public ActionResult RetroConfirmationPartial()
        {
            return PartialView("RetroConfirmationPartial", Model);
        }
        public ActionResult RetroApprovalPartial()
        {
            return PartialView("RetroApprovalPartial", Model);
        }

        private byte[] SetNoticeIcon(int val)
        {
            byte[] img = null;
            string imageBytes1 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkJJREFUeNpkUk1rFEEQfdXT85XsGkPcHMSD6Aa8evCoSO4BT+JJ8A8oguLBHPwDIip4EPwDCmpAES8eDILKJsegHrwEURKy2Y/JZmdnusvq3llItKAoerqr3nv1hlqtFlwYpIiwNTOLL5eP8relmv11JuBRHRxlAH2H6r8B7T6X2gaGADOo1VqTxgRz+LB0glbux1wuoFQyjf0DHyRnIkBlP6F/3wKyV5ArVaKOeaxcOU3PXsc8tYA8APIeUOxKdoDSZVeGDaSmp1DMvwSrq2AD6qydb9a5va64WUcuDaNtwBqBs3DTPSIkSQNBIpkC4fYA4eY5PUOrN2Av1lHI5OEfQcjloTSiouwK05g6FzLT3dWmEIY3Ndvji2TiqnFfHpRVX9U8CX+0YxZ2WljXLmhiNFDK9uxedclV0v8DnBwyvhLbWc0oumR6DY9qnQV2TBGV1gms27yTYx19LV8Gfb2DxU/H7GoTlsd67ci7Xnl0AFaaVCRVFkdDWfjgqwL1HkH3C79NEptEh6PFPkW/S2+q8zqUKqhhR76WD9Uc3q1zNLiDSFBVOEY4GP4HcYNlqVoyyVCGvWWx+bP2q1HmAdJ2Dq7dg40aTjd5jcF4oKOrFTjOemWcLYvyx06Rpo7QSIREWD5RSf9kUBy5DZ6WBtEdOCLUhR5u2ih/XyrzVEb+mJDStK9gFGGo+Gxa0DVvR2hQxvmGCsx1xbwhy92y5P/2Q6EFPhQGl9Kc7uqR2jHJ3sdCm7cmwIuE0T9k+z/xV4ABAP9iEDZwmfEuAAAAAElFTkSuQmCCAA==";
            string imageBytes2 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAbZJREFUeNp8Uz1Lw1AUfUlfShFa65BdpAVX/4CDqxScxElwFiqCxUF3pyA6KPQnKKiDDq6CICKOFl3dkloaI/1Kk3pOyINH/HhweC8355x7372J4TiO4DJNU4RhOB0EwWq/36/hPD+ZTIqGYXwBr+Px+Ho0Gp0h3omiKNFIJex2u7VOp+PgXAVZQJgQ0r2Sy+WWLctqwGQHz5eJTkop2u32muu6V/l8vsogMvwARAIGc4VC4QLm6zSVrVarEsdxs1QqmcPhUBB8oTKzCrWDRwOBJKe9Xu9J+r6/Zdt2kc6DwSAhKKG+9Kug2ilcb1uijCW6oUmCjdCzZpcmZgWLJs42sxJ/CfXmaZwZiTJ9CG1mVSPQ76oWr8MYQR40gSyXy/eYbUUR/rszR0qQA4NHE0M/BkLlqsr7zYhCcjC6GOIj0/O8Z5SwSyJfZrOpnU0l2BuMcx+mD2ZaxiECm9g9ElR5BDtLpJ/vJ8ZZh/Ag6bq6AxxPEJzFB9AgmbF0+SjxHbgFmnh+Uy+kKg3kBWTdUKOA2QvCdYC7i1iUbSJ/DAtYQbY9VPGB0u6Q4Qbkc5gF2ZHp61uAAQA8ZE6FNcNYIAAAAABJRU5ErkJggg==";
            string imageBytes3 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAl9JREFUeNpcUj1rVEEUvfPx3m7W7IriglZaRNHSIq1FGkEIWImd+AcMglaxsLMSUcHCUhBEC5tY2CpBUIhYGNBChCiJcbNfb/d9zMy9453dWRJ8cJk3H+fcc86M8N7D7MusP/x1gFd+lX654+CsFaKZKjkSAr5lBa71cnyZ5a5bWJycFzPw545d/tCl+7qmToMSYEAA8ZbgPZ7y6GFU0I/tXnUrK/G1EBG8vmOuvu/i85NHU9l3AF3rwTEw0M7ANSmgrgDGOcLP3fKaRf9M3Ps0WCi12jh3vNb8YwB2qgD0QLAPllxJAHPNa4BOz+Rbf6tFvdGxK0tn6s0h2/hdEpSsFT2L3I8CgkRJBEaEdSZoqEaixE19oqWXaqmALQaOHU3l+mg2dg8yAgEJzxYENNnHfF1e0KykXSCHYWkC8tErHOg8mTIBsX7mB5R8DvwRbdAP+pbaoWsZNqJHGeVCJPORoOKx5BobyvTisWT9y9gtgJJQYSSIXcWBztPEAdJEgjEe8tx9lMPCPcpGziYTP4GeA+NwbAiIyRxN7UjunTI4nBsMDRUVPlTppZVtIBrVUnVR8D2akDZNWwfpmrUH0BwzN8LjGTnY26tWmfCFTjkx5/yDfq+qGq3k7pyW7VloQUkABrmKF/O+GQ779g46/zjkoVVLARYYPDxhklOtdu12kx8ChVfCCoSlgbV+a5i7t6bAp8zxXcQwtKxLcBWCNXg+bejrxBtoCIrMbnryN4QUm+RgFx0hiBhiDFTzT8IHLtdbyapuyL2sX70zOb7Bil7VD+lMyumVhfrv6uGfAAMA1xJkKTxEsyUAAAAASUVORK5CYII=";
            string imageBytes4 = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAbpJREFUeNqMkj9rFFEUxc+9783urG4wu8QixFUQsRBELXRLo6VdPkZInUQQey3yESwCNn4BW6tUmkYsRAiJIIp/YGfZJLN5f71vYiWvmGEGZt6c3z33nvcImev2o/Xt0Xh9Y16fgDoXcfRh983Bu53N/3U6B6tyeK0yg5F1GlovgMql6zkd5xZd8JY4gDmCVQQo2NawyEFMzcNKJERo7+ytIVYgAdOD4LLOJOE85XK47INvHIPcl66OV/Xgzt0YLVSni9PJ5y/V4d5baaAxYyls6+o3Pdn+OKnsYJFlRvxrM3oDH0zqHTERRReq2xModUJQSsGZ6UzX8+OZpLp4Hg7DiL3xEYHSzLKmGeIDFb38Py9OUsSZ45nmTr9fFAvSckQtpJEGKInJQWkFVRCKskTRuyDfUkhAlnVTc18f7e++4t7l0Zl1sF72WKj+lXv3u8NbN5XyDXg2Pfj659P7PXFkaowFPpn8zG7ByuOtnQfPf8Xxs8P48GUVb6y9eJ3T5U+YKjSn8MRGpYB0R7eGJSokmCI1c6bD0hqWRHWaK+XMsi3yXrSG3Xz6w7npdwo19JxgTiffcrq/AgwAN6+YiWduV/UAAAAASUVORK5CYIIA";

            if (val % 4 == 0)
            {
                img = Convert.FromBase64String(imageBytes1);
            }
            else if (val % 4 == 1)
            {
                img = Convert.FromBase64String(imageBytes2);
            }
            else if (val % 4 == 2)
            {
                img = Convert.FromBase64String(imageBytes3);
            }
            else if (val % 4 == 3)
            {
                img = Convert.FromBase64String(imageBytes4);
            }

            return img;
        }

        private string SetNoticeIconString(int val)
        {
            string img = string.Empty;
            ////string imageBytes1 = "~/Content/Images/notice_open_icon.png";
            ////string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            ////string imageBytes3 = "~/Content/Images/notice_close_icon.png";
            string imageBytes1 = "~/Content/Images/notice_close_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_open_icon.png";
            string imageBytes4 = "~/Content/Images/notice_add_icon.png";

            if (val % 3 == 0)
            {
                img = imageBytes1;
            }
            else if (val % 3 == 1)
            {
                img = imageBytes2;
            }
            else if (val % 3 == 2)
            {
                img = imageBytes3;
            }


            return img;
        }
        private string SetGRStatus(int val)
        {
            string img = string.Empty;

            if (val % 4 == 0)
            {
                img = "Not Yet Scanned";
            }
            else if (val % 4 == 1)
            {
                img = "Scanned OK";
            }
            else if (val % 4 == 2)
            {
                img = "Scanned NG";
            }
            else if (val % 4 == 3)
            {
                img = "Approved";
            }

            return img;
        }
    }
}
