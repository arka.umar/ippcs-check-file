﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;

using Portal.Models.OrderCompare;
using Portal.Models.OrderCompareSummary;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Toyota.Common.Web.Compression;
using NPOI.SS.Util;


namespace Portal.Controllers
{
    public class OrderCompareController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public OrderCompareController() : base("KCI - Orderplan Compare")
         {
         }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            Model.AddModel(new OrderCompareModel());
            //Model.AddModel(new OrderCompareSummary());
        }

        public ActionResult OrderCompareAllDataPartial()
        {
            //#region Required model in partial page : show all data
            OrderCompareModel model = Model.GetModel<OrderCompareModel>();
            
            String dateFrom = String.IsNullOrEmpty(Request.Params["dateProdFrom"]) == true ? "" : Request.Params["dateProdFrom"];
            String dateTo = String.IsNullOrEmpty(Request.Params["dateProdTo"]) == true ? "" : Request.Params["dateProdTo"];
            String type = "all";

            if (dateFrom != "" && dateTo != "")
            {
                model.OrderCompare = GetAllOrderCompareControl(dateFrom, dateTo, type);
            }
            //#endregion

            
            return PartialView("TabAllDataPartial", Model);
        }

        public ActionResult OrderCompareMatchDataPartial()
        {
            //#region Required model in partial page : show match data
            OrderCompareModel model = Model.GetModel<OrderCompareModel>();

            String dateFrom = String.IsNullOrEmpty(Request.Params["dateProdFrom"]) == true ? "" : Request.Params["dateProdFrom"];
            String dateTo = String.IsNullOrEmpty(Request.Params["dateProdTo"]) == true ? "" : Request.Params["dateProdTo"];
            String type = "match";

            if (dateFrom != "" && dateTo != "")
            {
                model.OrderCompare = GetAllOrderCompareControl(dateFrom, dateTo, type);
            }
            //#endregion

            return PartialView("TabMatchDataPartial", Model);
        }

        public ActionResult OrderCompareDifferentDataPartial()
        {
            //#region Required model in partial page : show difference data
            OrderCompareModel model = Model.GetModel<OrderCompareModel>();

            String dateFrom = String.IsNullOrEmpty(Request.Params["dateProdFrom"]) == true ? "" : Request.Params["dateProdFrom"];
            String dateTo = String.IsNullOrEmpty(Request.Params["dateProdTo"]) == true ? "" : Request.Params["dateProdTo"];
            String type = "difference";

            if (dateFrom != "" && dateTo != "")
            {
                model.OrderCompare = GetAllOrderCompareControl(dateFrom, dateTo, type);
            }
            //#endregion
            return PartialView("TabDifferentDataPartial", Model);
        }


        private List<OrderCompare> GetAllOrderCompareControl(String dateFrom, String dateTo, String type)
        {
            List<OrderCompare> orderCompareModel = new List<OrderCompare>();
            IDBContext db = DbContext;

            orderCompareModel = db.Fetch<OrderCompare>("GetOrderCompare", new object[] { dateFrom, dateTo, type }).ToList();

            db.Close();

            return orderCompareModel;
        }

        private OrderCompareSummary GetCountSummary(String dateFrom, String dateTo) 
        {
            var summary = new OrderCompareSummary();

            IDBContext db = DbContext;

            summary = db.SingleOrDefault<OrderCompareSummary>("GetOrderCompareSummary", new object[] {dateFrom, dateTo});

            db.Close();

            return summary;
        }

        public ContentResult getSummary(string dateFrom, string dateTo) {
            IDBContext db = DbContext;
            
            string summaryResult="";

            if (dateFrom != "" && dateTo != "")
            {
                List<OrderCompareSummary> summary = db.Fetch<OrderCompareSummary>("GetOrderCompareSummary", new object[] { dateFrom, dateTo }).ToList();

                for (int i = 0; i < summary.Count; i++)
                {
                    summaryResult = summary[i].countAll.ToString() + '|' + summary[i].countMatch.ToString() + '|' + summary[i].countDiff.ToString();
                }
            }
            else {
                summaryResult = "0|0|0";
            }
            db.Close();

            return Content(summaryResult);
        }


        public void download(string p_dateFrom, string p_dateTo, string p_type)
        {
            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;

                string fileName = "orderCompare.xls";
                string zipName = "orderCompare.zip";

                documentBytes = download_process(p_dateFrom, p_dateTo, p_type);


                listCompress.Add(fileName, documentBytes);


                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/zip";
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition",
                                    string.Format("{0};FileName=\"{1}\"",
                                                    "attachment",
                                                    zipName));
                Response.BinaryWrite(documentBytes);
                Response.Flush();
                Response.End();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;

            }

        }

        public byte[] download_process(string p_dateFrom, string p_dateTo, string p_type) {
            try {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                string title = "Order Compare (" + p_type + ")";
                title = title.ToUpper();


                filesTmp = HttpContext.Request.MapPath("~/Template/orderCompare.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;

                int row = 4;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.CENTER;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.CENTER;

                #endregion

                List<OrderCompare> datas = new List<OrderCompare>();
                datas = GetAllOrderCompareControl(p_dateFrom, p_dateTo, p_type);

                DateTime MyDateFrom;
                string stringDateFrom;
                MyDateFrom = new DateTime();
                MyDateFrom = DateTime.ParseExact(p_dateFrom, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);

                stringDateFrom = MyDateFrom.ToString("dd.MM.yyyy");

                DateTime MyDateTo;
                string stringDateTo;
                MyDateTo = new DateTime();
                MyDateTo = DateTime.ParseExact(p_dateTo, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);
                stringDateTo = MyDateTo.ToString("dd.MM.yyyy");


                ISheet sheet;

                sheet = workbook.GetSheetAt(0);
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue(title);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                Hrow = sheet.CreateRow(1);
                Hrow.CreateCell(0).SetCellValue("Prod. Date : " + stringDateFrom + " - " + stringDateTo);
                sheet.AddMergedRegion(new CellRangeAddress(1, 1, 0, 3));

                Hrow = sheet.CreateRow(3);

                Hrow.CreateCell(0).SetCellValue("No. ");
                sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("    Production Date    ");
                sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("Seq No");
                sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Dock");
                sheet.AutoSizeColumn(3);
                Hrow.CreateCell(4).SetCellValue("          Supplier - Plant          ");
                sheet.AutoSizeColumn(4);
                Hrow.CreateCell(5).SetCellValue("             Part No             ");
                sheet.AutoSizeColumn(5);
                Hrow.CreateCell(6).SetCellValue("  IPPCS Qty  ");
                sheet.AutoSizeColumn(6);
                Hrow.CreateCell(7).SetCellValue("  KCI Qty  ");
                sheet.AutoSizeColumn(7);

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;

                int no = 1;

                foreach (var data in datas) {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(no);
                    Hrow.CreateCell(1).SetCellValue(data.PRODUCTION_DATE.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(2).SetCellValue(data.ARRIVAL_SEQ);
                    Hrow.CreateCell(3).SetCellValue(data.DOCK_CD);
                    Hrow.CreateCell(4).SetCellValue(data.SUPPLIER);
                    Hrow.CreateCell(5).SetCellValue(data.PART_NO);
                    Hrow.CreateCell(6).SetCellValue(data.IPPCS_ORDER_QTY);
                    Hrow.CreateCell(7).SetCellValue(data.ORDER_QTY_KCI);

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent3;
                    Hrow.GetCell(5).CellStyle = styleContent1;
                    Hrow.GetCell(6).CellStyle = styleContent1;
                    Hrow.GetCell(7).CellStyle = styleContent1;

                    row++;
                    no++;
                
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e) {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();
                return a;
            }
        
        }

    }
}
