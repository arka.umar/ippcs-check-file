﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.IO.Compression;

using System.Linq;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;
using ServiceStack.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Task;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using Toyota.Common.Web.Log;

using ServiceStack.Text;
using Toyota.Common.Web.Compression;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using System.Reflection;
using System.Drawing;

//TESTING
//using System.Drawing;
//using System.Drawing.Imaging;
//using ThoughtWorks.QRCode.Codec;
//using ThoughtWorks.QRCode.Codec.Data;
//using ThoughtWorks.QRCode.Codec.Util;
//using Toyota.Common.Web.FTP;
//END TESTING

using Portal.Models.Globals;
using Portal.Models.Receiving.TruckReceiving;

namespace Portal.Controllers.Receiving.TruckReceiving
{
    public class TruckReceivingController : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

        public TruckReceivingController()
            : base("TrucReceiving", "Truck Receiving")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {//t
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            TruckReceivingDetailModel mdl = new TruckReceivingDetailModel();
            ViewData["DockLookup"] = GetDock();
            //ViewData["RouteLookup"] = GetRoute();
            Model.AddModel(mdl);
        }

        public ActionResult PartialHeader()
        {
            List<TruckReceivingDetail> trucking = Model.GetModel<User>().FilteringArea<TruckReceivingDetail>(GetDock(), "DOCK_CD", "TruckReceiving", "DockOption");

            if (trucking.Count > 0)
            {
                _DOCKModel = trucking;
            }
            else
            {
                _DOCKModel = GetDock();
            }
            Model.AddModel(_DOCKModel);
            return PartialView("IndexHeader");
        }

        public ActionResult PartialData()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            string DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            string RCV_PLANT_CD = String.IsNullOrEmpty(Request.Params["RCV_PLANT_CD"]) ? "" : Request.Params["RCV_PLANT_CD"];
            TruckReceivingDetailModel mdl = Model.GetModel<TruckReceivingDetailModel>();
            if (DELIVERY_NO != "" || DOCK_CD != "" || RCV_PLANT_CD != "")
            {
                mdl.truckReceivingListData = getTruckReceivingData(DELIVERY_NO, DOCK_CD, RCV_PLANT_CD);
            }
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }


        public List<TruckReceivingDetail> getTruckReceivingData(string DELIVERY_NO, string DOCK_CD, string RCV_PLANT_CD)
        {
            List<TruckReceivingDetail> listTruck = new List<TruckReceivingDetail>();
            if(DELIVERY_NO!="" || DOCK_CD != "" || RCV_PLANT_CD != "")
            {
                listTruck = db.Fetch<TruckReceivingDetail>("TruckReceivingDetail", new object[] { 
                  DELIVERY_NO,
                  DOCK_CD,
                  RCV_PLANT_CD
                });
            }
            
            return listTruck;
        }

        public JsonResult getTruckReceivingHeader(TruckReceivingHeader m)
        {
            string message = "";
            Boolean success = false;
            List<TruckReceivingHeader> listTruck = new List<TruckReceivingHeader>();
            try
            {
                listTruck = db.Fetch<TruckReceivingHeader>("TruckReceivingHeader", new object[] { m.DELIVERY_NO, m.DOCK_CD });
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listTruck }, JsonRequestBehavior.AllowGet);
        }

        List<TruckReceivingDetail> _dockModel = null;
        private List<TruckReceivingDetail> _DOCKModel
        {
            get
            {
                if (_dockModel == null)
                    _dockModel = new List<TruckReceivingDetail>();

                return _dockModel;
            }
            set
            {
                _dockModel = value;
            }
        }
        //public List<TruckReceivingDockData> GetDock()
        //{
        //    List<TruckReceivingDockData> listDock = new List<TruckReceivingDockData>();
        //    try
        //    {
        //        listDock = db.Fetch<TruckReceivingDockData>("TruckReceivingGetDock", new object[] {});
        //    }
        //    catch (Exception ex) { throw ex; }
        //    return listDock;
        //}
        public List<TruckReceivingDetail> GetDock()
        {
            List<TruckReceivingDetail> listDock = new List<TruckReceivingDetail>();
            try
            {
                listDock = db.Fetch<TruckReceivingDetail>("TruckReceivingGetDock", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listDock;
        }


        //public ActionResult DockLookup(TruckReceivingDockData m)
        //{
        //    TempData["GridName"] = "DockOption";
        //    //ViewData["DockLookup"] = GetDock();
        //    //return PartialView("PG", ViewData["DockLookup"]);
        //    List<TruckReceivingDetail> trucking = Model.GetModel<User>().FilteringArea<TruckReceivingDetail>(GetDock(), "DOCK_CD", "TruckReceiving", "DockOption");

        //}

        public ActionResult DockLookup()
        {
            TempData["GridName"] = "DockOption";
            List<TruckReceivingDetail> trucking = Model.GetModel<User>().FilteringArea<TruckReceivingDetail>(GetDock(), "DOCK_CD", "TruckReceiving", "DockOption");

            if (trucking.Count > 0)
            {
                _DOCKModel = trucking;
            }
            else
            {
                _DOCKModel = GetDock();
            }

            return PartialView("PG", _DOCKModel);
        }


        public List<TruckReceivingRouteData> GetRoute()
        {
            List<TruckReceivingRouteData> listRoute = new List<TruckReceivingRouteData>();
            try
            {
                listRoute = db.Fetch<TruckReceivingRouteData>("TruckReceivingGetRoute", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listRoute;
        }

        public ActionResult RouteLookup(TruckReceivingRouteData m)
        {
            TempData["GridName"] = "RouteOption";
            ViewData["RouteLookup"] = GetRoute();
            return PartialView("PG", ViewData["RouteLookup"]);
        }


        public JsonResult getShortage(TruckReceivingDetail m)
        {
            string message = "";
            Boolean success = false;
            List<TruckReceivingDetail> listOrder = new List<TruckReceivingDetail>();
            try
            {
                listOrder = db.Fetch<TruckReceivingDetail>("TruckReceivingProgress", new object[] { m.DELIVERY_NO, m.DOCK_CD, m.RCV_PLANT_CD});
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listOrder, message = message, success = success, count = listOrder.Count }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveRemark(TruckReceivingDetail m)
        {
            List<TruckReceivingDetail> l = db.Fetch<TruckReceivingDetail>("KanbanTruckReceivingRemark", new object[] {
               m.MANIFEST_NO,
               m.ORDER_NO,
               m.REMARK
            });
            db.Close();
            return Json(new { status = l[0].ERROR, message = l[0].MESSAGE }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getRemark(TruckReceivingDetail m)
        {
            List<TruckReceivingDetail> l = db.Fetch<TruckReceivingDetail>("KanbanTruckReceivingGetRemark", new object[] {
                m.MANIFEST_NO,
                m.ORDER_NO
            });
            db.Close();
            if (l.Count > 0)
            {
                return Json(new { status = l[0].ERROR, message = "success", remark = l[0].REMARK }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "success", message = "success", remark = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //public string PrintKanbanWeb()
        //{
            
        //    string message = "OK";
 

        //    List<DailyOrderPrintingDetail> listOrder = new List<DailyOrderPrintingDetail>();
        //    try
        //    {
        //        listOrder = db.Fetch<DailyOrderPrintingDetail>("DailyOrderPrintingGetKanban", new object[] { "2014060201", "851430K01000","2375"});
        //        message = "Data successfuly loaded";
        //    }
        //    catch (Exception e)
        //    {
        //        message = e.Message;
        //    }

        //    //string FolderName = "";
        //    //if (OrderType == "1") FolderName = OrderDate + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

        //    //ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";


        //    //foreach (string KANBAN_ID in x)
        //    //{
        //    //    //Console.WriteLine(word);
        //    //    try
        //    //    {
        //    //        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
        //    //        qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
        //    //        qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
        //    //        Image image;

        //    //        image = qrCodeEncoder.Encode(KANBAN_ID);
        //    //        //Console.WriteLine("Creating file " + dr2["KANBAN_ID"].ToString()+".jpg");
        //    //        image.Save("D:/QRCODE/" + KANBAN_ID + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
        //    //    }
        //    //    catch (Exception e)
        //    //    {
        //    //        //Console.WriteLine(e.Message);
        //    //        message = e.Message;
        //    //    }

        //    //}


        //    foreach (var l in listOrder)
        //    {
        //        //Console.WriteLine(word);
        //        try
        //        {
        //            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
        //            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
        //            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
        //            Image image;

        //            image = qrCodeEncoder.Encode(l.KANBAN_ID);
        //            //Console.WriteLine("Creating file " + dr2["KANBAN_ID"].ToString()+".jpg");
        //            image.Save("D:/QRCODE/"+ l.KANBAN_ID + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
        //        }
        //        catch (Exception e)
        //        {
        //            //Console.WriteLine(e.Message);
        //            message = e.Message;
        //        }

        //    }
        //    Telerik.Reporting.ReportParameterCollection reportParameters = new Telerik.Reporting.ReportParameterCollection();
        //    string reportPath = @"\Report\DailyOrder\Rep_ManifestKanban_Download_new.trdx";
        //    string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
        //    Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
        //    parameter.Add("@fromWeb", System.Data.DbType.String, "1");
        //    parameter.Add("@OrderNo", System.Data.DbType.String, "2014060201");
        //    parameter.Add("@PartNo", System.Data.DbType.String, "851430K01000");
        //    parameter.Add("@KanbanNo", System.Data.DbType.String, "2375");
        //    reportParameters.Add("QRpath", Telerik.Reporting.ReportParameterType.String, "D:/QRCODE/");
        //    Telerik.Reporting.Report rpt = CreateReport(reportPath,
        //        sqlCommand,
        //        parameter);
        //    rpt.ReportParameters.AddRange(reportParameters);

            
        //    //return message;
        //   string url =  SavePDFReportToFTP(rpt, "2014060201" + "2", "PrintKanban" + "/Kanban", "1", "");
            
        //    string[] array1 = Directory.GetFiles(@"" + "D:/QRCODE/");
        //    foreach (string name in array1)
        //    {
        //        Console.WriteLine("Deleting file : " + name);
        //        System.IO.File.Delete(name);
        //    }
        //    return url;
        //}
        //private string filePath = "";
        //private string localFilePath = "/Content/DailyOrderPrinting/tmp/";
        //private string SavePDFReportToFTP(Telerik.Reporting.Report rpt, string FileName, string FolderName, string OrderType, string UploadFilePath = "")
        //{
        //    //string LocalFileName = FileName + "-" + DateTime.Now.Ticks.ToString() + ".pdf";
        //    string LocalFileName = FileName + ".pdf";
        //    string ContextFTP = "";
        //    FileName = FileName + ".pdf";

        //    FTPUpload vFtp = new FTPUpload();
        //    vFtp.Setting.IP = "127.0.0.1";
        //    vFtp.Setting.UserID = "nightwalker";
        //    vFtp.Setting.Password = "satu2tiga";
        //    switch (OrderType)
        //    {
        //        case "1":
        //            filePath = vFtp.Setting.FtpPath("DailyOrderPrinting");
        //            ContextFTP = "dailyorder";
        //            break;
        //        case "2":
        //            filePath = vFtp.Setting.FtpPath("EmergencyOrder");
        //            ContextFTP = "emergencyorder";
        //            break;
        //        case "3":
        //            filePath = vFtp.Setting.FtpPath("CPOOrder");
        //            ContextFTP = "cpoorder";
        //            break;
        //        case "4":
        //            filePath = vFtp.Setting.FtpPath("JunbikiOrder");
        //            ContextFTP = "junbikiorder";
        //            break;
        //        case "5":
        //            filePath = vFtp.Setting.FtpPath("ProblemPartOrder");
        //            ContextFTP = "problempartorder";
        //            break;
        //    }
        //    string msg = "";

        //    if (!vFtp.IsDirectoryExist(filePath, UploadFilePath))
        //    {
        //        vFtp.CreateDirectory(filePath + UploadFilePath);
        //    }
        //    filePath = filePath + UploadFilePath;

        //    if (vFtp.directoryExists(filePath + "/" + FolderName))
        //    {
        //        if (!vFtp.DirectoryList(filePath + FolderName).Contains((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName))
        //        {
        //            ReportProcessor reportProcess = new ReportProcessor();
        //            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
        //            bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
        //            rpt.Dispose();
        //        }
        //    }
        //    else
        //    {
        //        ReportProcessor reportProcess = new ReportProcessor();
        //        RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
        //        bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
        //        rpt.Dispose();
        //    }

        //    //string fullFtpPath = "ftp://" + vFtp.Setting.IP + filePath + FolderName + "/";
        //    //string fullLocalPath = AppDomain.CurrentDomain.BaseDirectory + localFilePath;
        //    //if (!Directory.Exists(fullLocalPath))
        //    //{
        //    //    Directory.CreateDirectory(fullLocalPath);
        //    //}
        //    //string serverPath = "";
        //    //if (vFtp.FtpDownload(fullFtpPath + FileName, fullLocalPath + LocalFileName, ref msg))
        //    //{
        //    //    //serverPath = String.Format("{0}{1}{2}", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, ""), localFilePath, LocalFileName);
        //    //    //serverPath = String.Format("{0}{1}{2}", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, ""), "/DailyOrderPrinting/GetFile?FileName=", LocalFileName);
        //    //    serverPath = ApplicationUrl + "/FileDownload?context=dailyorder&path=" + FolderName + "/" + FileName;
        //    //}
        //    //else
        //    //{
        //    //    throw new Exception("Can't retrieve file.");
        //    //}
        //    string serverPath = "";
        //    serverPath = ApplicationUrl + "/FileDownload?context=" + ContextFTP + "&path=" + FolderName + "/" + FileName;

        //    return serverPath;
        //}

        private Telerik.Reporting.Report CreateReport(string reportPath,
           string sqlCommand,
           Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
           Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
       )
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }
    }
}