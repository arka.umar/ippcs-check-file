﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;
using ServiceStack.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Task;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using Toyota.Common.Web.Log;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ServiceStack.Text;
using Toyota.Common.Web.Compression;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using System.Reflection;
using System.Drawing;

using Portal.Models.Globals;
using Portal.Models.Receiving.KanbanPartShortage;
using Portal.Models.Receiving.KanbanSummaryPartReceiving;
namespace Portal.Controllers.Receiving.KanbanPartShortage
{
    public class KanbanPartShortageController : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public KanbanPartShortageController()
            : base("KanbanPartShortage", "Part Shortage")
        {

        }
        protected override void StartUp()
        {

        }
        protected override void Init()
        {
            //t
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            KanbanPartShortageModel mdl = new KanbanPartShortageModel();
            KanbanSummaryPartReceivingModel mdl2 = new KanbanSummaryPartReceivingModel();
            ViewData["LaneLookup"] = GetLane();
            ViewData["SupplierLookup"] = GetSupplier();
            ViewData["StatusLookup"] = GetStatus();
            ViewData["RouteLookup"] = GetRoute();
            ViewData["DockLookup"] = GetDock();
            Model.AddModel(mdl2);
            Model.AddModel(mdl);
        }

        List<KanbanPartShortageData> _kanbanPartShortageDataModel = null;
        private List<KanbanPartShortageData> _KanbanPartShortageDataModel
        {
            get
            {
                if (_kanbanPartShortageDataModel == null)
                    _kanbanPartShortageDataModel = new List<KanbanPartShortageData>();

                return _kanbanPartShortageDataModel;
            }
            set
            {
                _kanbanPartShortageDataModel = value;
            }
        }

        public ActionResult PartialHeader()
        {
            ViewBag.MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
            ViewBag.KANBAN_NO = String.IsNullOrEmpty(Request.Params["KANBAN_NO"]) ? "" : Request.Params["KANBAN_NO"];
            ViewBag.DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            ViewBag.DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            ViewBag.RECEIVING_PLANT = String.IsNullOrEmpty(Request.Params["RECEIVING_PLANT"]) ? "" : Request.Params["RECEIVING_PLANT"];
            ViewBag.SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
            ViewBag.P_LANE_CD = String.IsNullOrEmpty(Request.Params["P_LANE_CD"]) ? "" : Request.Params["P_LANE_CD"];
            ViewBag.ROUTE = String.IsNullOrEmpty(Request.Params["ROUTE"]) ? "" : Request.Params["ROUTE"];
            ViewBag.RATE = String.IsNullOrEmpty(Request.Params["RATE"]) ? "" : Request.Params["RATE"];
            return PartialView("IndexHeader");
        }

        //public ActionResult PartialData()
        //{
        //    string ARRIVAL_DATE = String.IsNullOrEmpty(Request.Params["ARRIVAL_DATE"]) ? "" : Request.Params["ARRIVAL_DATE"];
        //    string ARRIVAL_DATE_TO = String.IsNullOrEmpty(Request.Params["ARRIVAL_DATE_TO"]) ? "" : Request.Params["ARRIVAL_DATE_TO"];
        //    KanbanPartShortageModel mdl = Model.GetModel<KanbanPartShortageModel>();

        //    if (ARRIVAL_DATE != "" && ARRIVAL_DATE_TO != "")
        //    {
        //        mdl.KanbanPartShortageListData = getKanbanReceivingData(ARRIVAL_DATE, ARRIVAL_DATE_TO);
        //    }
        //    Model.AddModel(mdl);
        //    return PartialView("GridIndex", Model);
        //}

        //public List<KanbanPartShortageData> getKanbanReceivingData(string ARRIVAL_DATE, string ARRIVAL_DATE_TO)
        //{
        //    string arrivalDateParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(ARRIVAL_DATE))
        //    {
        //        string d = ARRIVAL_DATE.Substring(0, 2);
        //        string m = ARRIVAL_DATE.Substring(3, 2);
        //        string y = ARRIVAL_DATE.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        arrivalDateParameter += "'" + tgl + "'";
        //    }

        //    string arrivalDateToParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(ARRIVAL_DATE_TO))
        //    {
        //        string d = ARRIVAL_DATE_TO.Substring(0, 2);
        //        string m = ARRIVAL_DATE_TO.Substring(3, 2);
        //        string y = ARRIVAL_DATE_TO.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        arrivalDateToParameter += "'" + tgl + "'";
        //    }
        //    List<KanbanPartShortageData> listKanban = db.Fetch<KanbanPartShortageData>("KanbanPartShortageData_new", new object[] { 
        //        arrivalDateParameter,
        //        arrivalDateToParameter
        //    });
        //    return listKanban;
        //}

        public ActionResult PartialData()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            string ORDER_NO = String.IsNullOrEmpty(Request.Params["ORDER_NO"]) ? "" : Request.Params["ORDER_NO"];
            string RCV_PLANT_CD = String.IsNullOrEmpty(Request.Params["RCV_PLANT_CD"]) ? "" : Request.Params["RCV_PLANT_CD"];
            string DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            string SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
            string KANBAN_NO = String.IsNullOrEmpty(Request.Params["KANBAN_NO"]) ? "" : Request.Params["KANBAN_NO"];
            string MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
            string ROUTE = String.IsNullOrEmpty(Request.Params["ROUTE"]) ? "" : Request.Params["ROUTE"];
            string RIT = String.IsNullOrEmpty(Request.Params["RIT"]) ? "" : Request.Params["RIT"];
            string P_LANE_SEQ = String.IsNullOrEmpty(Request.Params["P_LANE_SEQ"]) ? "" : Request.Params["P_LANE_SEQ"];
            string DATE_FROM = String.IsNullOrEmpty(Request.Params["DATE_FROM"]) ? "" : Request.Params["DATE_FROM"];
            string DATE_TO = String.IsNullOrEmpty(Request.Params["DATE_TO"]) ? "" : Request.Params["DATE_TO"];
            string STATUS = String.IsNullOrEmpty(Request.Params["STATUS"]) ? "" : Request.Params["STATUS"];
            KanbanPartShortageModel mdl = Model.GetModel<KanbanPartShortageModel>();
            _kanbanPartShortageDataModel = getKanbanReceivingData(DELIVERY_NO, ORDER_NO, RCV_PLANT_CD, DOCK_CD, SUPPLIER_CD, KANBAN_NO, MANIFEST_NO, ROUTE, RIT, P_LANE_SEQ, DATE_FROM, DATE_TO, STATUS);
            Model.AddModel(_kanbanPartShortageDataModel);
            return PartialView("GridIndex", Model);
        }

        public List<KanbanPartShortageData> getKanbanReceivingData(string DELIVERY_NO, string ORDER_NO, string RCV_PLANT_CD, string DOCK_CD, string SUPPLIER_CD, string KANBAN_NO, string MANIFEST_NO, string ROUTE, string RIT, string P_LANE_SEQ, string DATE_FROM, string DATE_TO, string STATUS)
        {
            if (!string.IsNullOrEmpty(DATE_FROM))
            {
                string d = DATE_FROM.Substring(0, 2);
                string m = DATE_FROM.Substring(3, 2);
                string y = DATE_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                DATE_FROM = tgl;
            }

            if (!string.IsNullOrEmpty(DATE_TO))
            {
                string d = DATE_TO.Substring(0, 2);
                string m = DATE_TO.Substring(3, 2);
                string y = DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                DATE_TO =  tgl;
            }
            List<KanbanPartShortageData> listKanban = new List<KanbanPartShortageData>();
            if(DELIVERY_NO != "" || ORDER_NO != "" || RCV_PLANT_CD != "" || DOCK_CD != "" || SUPPLIER_CD != "" || KANBAN_NO != "" || MANIFEST_NO != "" || ROUTE != "" || RIT != "" ||  P_LANE_SEQ != "" || DATE_FROM != "" || DATE_TO != "" || STATUS != "")
            {
                listKanban = db.Fetch<KanbanPartShortageData>("KanbanPartShortageData_new", new object[] { 
                    DELIVERY_NO,
                    ORDER_NO,
                    RCV_PLANT_CD,
                    DOCK_CD,
                    SUPPLIER_CD,
                    KANBAN_NO,
                    MANIFEST_NO,        
                    ROUTE,
                    RIT,
                    P_LANE_SEQ,
                    DATE_FROM,
                    DATE_TO,
                    STATUS
                });
            }
                
            return listKanban;
        }

        public JsonResult saveRemark(KanbanPartShortageRemark m)
        {
            //statusParameter = "'" + DELIVERY_NO + "'";
            //string user = AuthorizedUser.Username;
            List<KanbanPartShortageRemark> l = db.Fetch<KanbanPartShortageRemark>("KanbanPartShortageRemark", new object[] {
                m.KANBAN_NO,
                m.MANIFEST_NO,
                m.ORDER_NO,
                m.PART_NO,
                m.REMARK
            });
            db.Close();
            return Json(new { status = l[0].ERROR, message = l[0].MESSAGE }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getRemark(KanbanPartShortageRemark m)
        {
            List<KanbanPartShortageRemark> l = db.Fetch<KanbanPartShortageRemark>("KanbanPartShortageGetRemark", new object[] {
                m.KANBAN_NO,
                m.MANIFEST_NO,
                m.ORDER_NO,
                m.PART_NO,
            });
            db.Close();
            if(l.Count>0)
            {
                return Json(new { status = l[0].ERROR, message = "success", remark = l[0].REMARK }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "success", message = "success", remark = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Receive(KanbanPartShortageData m)
        {
            List<KanbanPartShortageRemark> l = db.Fetch<KanbanPartShortageRemark>("KanbanPartShortageReceive", new object[] {
                m.RECEIVE,
                m.KANBAN_NO,
                m.PART_NO,
                m.MANIFEST_NO
            });
            db.Close();
            return Json(new { status = "success", message = "Receiving completed" }, JsonRequestBehavior.AllowGet);
        }


        public List<p_lane_data> GetLane()
        {
            List<p_lane_data> listLane = new List<p_lane_data>();
            try
            {
                listLane = db.Fetch<p_lane_data>("KanbanLaneSeq", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listLane;
        }

        public ActionResult LaneLookup(p_lane_data m)
        {
            TempData["GridName"] = "PLaneOption";
            ViewData["LaneLookup"] = GetLane();
            return PartialView("PG", ViewData["LaneLookup"]);
        }


        public List<SupplierData> GetSupplier()
        {
            List<SupplierData> listLane = new List<SupplierData>();
            try
            {
                listLane = db.Fetch<SupplierData>("KanbanSupplierData", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listLane;
        }

        public ActionResult DockLookup()
        {
            TempData["GridName"] = "DockOption";
            ViewData["DockLookup"] = GetDock();
            return PartialView("PG", ViewData["DockLookup"]);
        }

        public List<DockData> GetDock()
        {
            List<DockData> listDock = new List<DockData>();
            try
            {
                listDock = db.Fetch<DockData>("KanbanGetDock", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listDock;
        }

        public ActionResult SupplierLookup()
        {
            TempData["GridName"] = "SupplierOption";
            ViewData["SupplierLookup"] = GetSupplier();
            return PartialView("PG", ViewData["SupplierLookup"]);
        }

        public List<RouteData> GetRoute()
        {
            List<RouteData> listRoute = new List<RouteData>();
            try
            {
                listRoute = db.Fetch<RouteData>("KanbanRoute2", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listRoute;
        }

        public ActionResult RouteLookup()
        {
            TempData["GridName"] = "RouteOption";
            ViewData["RouteLookup"] = GetRoute();
            return PartialView("PG", ViewData["RouteLookup"]);
        }


        public ActionResult StatusLookup()
        {
            TempData["GridName"] = "StatusOption";
            ViewData["StatusLookup"] = GetStatus();
            return PartialView("PG", ViewData["StatusLookup"]);
        }

        public List<StatusData> GetStatus()
        {
            List<StatusData> data = new List<StatusData>();

            data.Add(new StatusData()
            {
                STATUS = "Ordered"
            });

            data.Add(new StatusData()
            {
                STATUS = "Shortage"
            });

            data.Add(new StatusData()
            {
                STATUS = "Completed"
            });
            return data;
        }

        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadKanbanPartShortage(string DELIVERY_NO, string ORDER_NO, string RCV_PLANT_CD, string DOCK_CD, string SUPPLIER_CD, string KANBAN_NO, string MANIFEST_NO, string ROUTE, string RIT, string P_LANE_SEQ, string DATE_FROM, string DATE_TO, string STATUS)
        {
            string fileName = String.Empty;

            // Update download user and status
            IEnumerable<KanbanPartShortageData> qry;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;

            if(KANBAN_NO == "")
            {
                KANBAN_NO = null;
            }

            if (!string.IsNullOrEmpty(DATE_FROM))
            {
                string d = DATE_FROM.Substring(0, 2);
                string m = DATE_FROM.Substring(3, 2);
                string y = DATE_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                DATE_FROM =tgl;
            }

            if (!string.IsNullOrEmpty(DATE_TO))
            {
                string d = DATE_TO.Substring(0, 2);
                string m = DATE_TO.Substring(3, 2);
                string y = DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                DATE_TO = tgl;
            }

            qry = db.Fetch<KanbanPartShortageData>("KanbanPartShortageData_new", new object[] { 
                DELIVERY_NO,
                ORDER_NO,
                RCV_PLANT_CD,
                DOCK_CD,
                SUPPLIER_CD,
                KANBAN_NO,
                MANIFEST_NO,        
                ROUTE,
                RIT,
                P_LANE_SEQ,
                DATE_FROM,
                DATE_TO,
                STATUS
            });

            string dt = Convert.ToString(DateTime.Now);
            dt = dt.Replace(" ", "");
            dt = dt.Replace("/", "");
            dt = dt.Replace(":", "");

            
            fileName = "KanbanPartShortage_" + dt + "_" + AuthorizedUser.Username + ".xls";
            hasil = exporter.Write(ConvertToDataTable(qry), "Data");


            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
    }
}