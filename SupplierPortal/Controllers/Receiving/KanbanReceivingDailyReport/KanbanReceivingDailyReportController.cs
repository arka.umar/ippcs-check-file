﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;
using ServiceStack.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Task;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using Toyota.Common.Web.Log;

using ServiceStack.Text;
using Toyota.Common.Web.Compression;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using System.Reflection;
using System.Drawing;

using Portal.Models.Globals;
using Portal.Models.Receiving.KanbanReceivingDailyReport;
using Portal.Models.Receiving.TruckReceiving;
namespace Portal.Controllers.Receiving.KanbanReceivingDailyReport
{
    public class KanbanReceivingDailyReportController : BaseController
    {

        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public KanbanReceivingDailyReportController()
            : base("KanbanReceivingDailyReport", "Daily Report")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {//t
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            KanbanReceivingDailyReportModel mdl = new KanbanReceivingDailyReportModel();
            Model.AddModel(mdl);
            ViewData["DockLookup"] = GetDock();
        }

        public List<TruckReceivingDockData> GetDock()
        {
            List<TruckReceivingDockData> listDock = new List<TruckReceivingDockData>();
            try
            {
                listDock = db.Fetch<TruckReceivingDockData>("TruckReceivingGetDock", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listDock;
        }

        public ActionResult DockLookup(TruckReceivingDockData m)
        {
            TempData["GridName"] = "DockOption";
            ViewData["DockLookup"] = GetDock();
            return PartialView("PG", ViewData["DockLookup"]);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("IndexHeader");
        }

        public ActionResult PartialData()
        {
            KanbanReceivingDailyReportModel mdl = new KanbanReceivingDailyReportModel();
            string PROCESS_DT_FROM = String.IsNullOrEmpty(Request.Params["PROCESS_DT_FROM"]) ? "" : Request.Params["PROCESS_DT_FROM"];
            string PROCESS_DT_TO = String.IsNullOrEmpty(Request.Params["PROCESS_DT_TO"]) ? "" : Request.Params["PROCESS_DT_TO"];
            string PRODUCTION_DT_FROM = String.IsNullOrEmpty(Request.Params["PRODUCTION_DT_FROM"]) ? "" : Request.Params["PRODUCTION_DT_FROM"];
            string PRODUCTION_DT_TO = String.IsNullOrEmpty(Request.Params["PRODUCTION_DT_TO"]) ? "" : Request.Params["PRODUCTION_DT_TO"];
            string RCV_PLANT_CD = String.IsNullOrEmpty(Request.Params["RCV_PLANT_CD"]) ? "" : Request.Params["RCV_PLANT_CD"];
            string DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            string KANBAN_NO = String.IsNullOrEmpty(Request.Params["KANBAN_NO"]) ? "" : Request.Params["KANBAN_NO"];
            string PART_NO = String.IsNullOrEmpty(Request.Params["PART_NO"]) ? "" : Request.Params["PART_NO"];
            string SUPPLIER_PLANT = String.IsNullOrEmpty(Request.Params["SUPPLIER_PLANT"]) ? "" : Request.Params["SUPPLIER_PLANT"];
            string SUPPLIER_CODE = String.IsNullOrEmpty(Request.Params["SUPPLIER_CODE"]) ? "" : Request.Params["SUPPLIER_CODE"];
            string MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
            string ORDER_NO = String.IsNullOrEmpty(Request.Params["ORDER_NO"]) ? "" : Request.Params["ORDER_NO"];
            mdl.KanbanReceivingListData = getKanbanReceivingData(PROCESS_DT_FROM, PROCESS_DT_TO, PRODUCTION_DT_FROM, PRODUCTION_DT_TO, RCV_PLANT_CD, DOCK_CD, KANBAN_NO, PART_NO, SUPPLIER_PLANT, SUPPLIER_CODE, MANIFEST_NO, ORDER_NO);
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        public List<KanbanReceivingDailyReportData> getKanbanReceivingData(string PROCESS_DT_FROM, string PROCESS_DT_TO, string PRODUCTION_DT_FROM, string PRODUCTION_DT_TO, string RCV_PLANT_CD, string DOCK_CD, string KANBAN_NO, string PART_NO, string SUPPLIER_PLANT, string SUPPLIER_CODE, string MANIFEST_NO, string ORDER_NO)
        {
            if (!string.IsNullOrEmpty(PROCESS_DT_FROM))
            {
                string d = PROCESS_DT_FROM.Substring(0, 2);
                string m = PROCESS_DT_FROM.Substring(3, 2);
                string y = PROCESS_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                PROCESS_DT_FROM = tgl;
            }

            if (!string.IsNullOrEmpty(PROCESS_DT_TO))
            {
                string d = PROCESS_DT_TO.Substring(0, 2);
                string m = PROCESS_DT_TO.Substring(3, 2);
                string y = PROCESS_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                PROCESS_DT_TO = tgl;
            }

            if (!string.IsNullOrEmpty(PRODUCTION_DT_FROM))
            {
                string d = PRODUCTION_DT_FROM.Substring(0, 2);
                string m = PRODUCTION_DT_FROM.Substring(3, 2);
                string y = PRODUCTION_DT_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                PRODUCTION_DT_FROM = tgl;
            }

            if (!string.IsNullOrEmpty(PRODUCTION_DT_TO))
            {
                string d = PRODUCTION_DT_TO.Substring(0, 2);
                string m = PRODUCTION_DT_TO.Substring(3, 2);
                string y = PRODUCTION_DT_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                PRODUCTION_DT_TO = tgl;
            }

            List<KanbanReceivingDailyReportData> listKanban = db.Fetch<KanbanReceivingDailyReportData>("KanbanReceivingDailyReportData", new object[] { 
                PROCESS_DT_FROM, PROCESS_DT_TO, PRODUCTION_DT_FROM, PRODUCTION_DT_TO, RCV_PLANT_CD, DOCK_CD, KANBAN_NO, PART_NO, SUPPLIER_PLANT, SUPPLIER_CODE, MANIFEST_NO, ORDER_NO
            });
            return listKanban;
        }
    }
}
