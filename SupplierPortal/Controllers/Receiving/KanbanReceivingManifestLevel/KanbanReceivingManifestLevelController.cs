﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;
using ServiceStack.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Task;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using Toyota.Common.Web.Log;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ServiceStack.Text;
using Toyota.Common.Web.Compression;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using System.Reflection;
using System.Drawing;

using Portal.Models.Globals;
using Portal.Models.Receiving.KanbanReceivingManifestLevel;


namespace Portal.Controllers.Receiving.KanbanReceivingManifestLevel
{
    public class KanbanReceivingManifestLevelController : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public KanbanReceivingManifestLevelController() : base("KanbanReceivingManifestLevel","Kanban Receiving Manifest Level")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {//t
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            KanbanReceivingManifestLevelModel mdl = new KanbanReceivingManifestLevelModel();
            Model.AddModel(mdl);
        }
        
        public ActionResult PartialHeader()
        {
            
            ViewBag.MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
            ViewBag.SUPPLIER_PLANT_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_PLANT_CD"]) ? "" : Request.Params["SUPPLIER_PLANT_CD"];
            ViewBag.SUPPLIER_CODE = String.IsNullOrEmpty(Request.Params["SUPPLIER_CODE"]) ? "" : Request.Params["SUPPLIER_CODE"];
            ViewBag.SUPPLIER_NAME = String.IsNullOrEmpty(Request.Params["SUPPLIER_NAME"]) ? "" : Request.Params["SUPPLIER_NAME"];
            ViewBag.RCV_PLANT_CD = String.IsNullOrEmpty(Request.Params["RCV_PLANT_CD"]) ? "" : Request.Params["RCV_PLANT_CD"];
            ViewBag.DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            
            return PartialView("IndexHeader");
        }

        public ActionResult PartialData()
        {
            string MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
            
             
            KanbanReceivingManifestLevelModel mdl = Model.GetModel<KanbanReceivingManifestLevelModel>();

            if (MANIFEST_NO != "")
            {
                mdl.KanbanReceivingManifestLevelListData = getKanbanReceivingManifestLevelData(MANIFEST_NO);
            }
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        public List<KanbanReceivingManifestLevelData> getKanbanReceivingManifestLevelData(string MANIFEST_NO)
        {
            List<KanbanReceivingManifestLevelData> listKanban = db.Fetch<KanbanReceivingManifestLevelData>("KanbanReceivingManifestLevelData", new object[] { 
              MANIFEST_NO
            });
            return listKanban;
        }


        public ActionResult PartialDataDetail()
        {
            string PART_NO = String.IsNullOrEmpty(Request.Params["PART_NO"]) ? "" : Request.Params["PART_NO"];
            string MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];

            KanbanReceivingManifestLevelModel mdl = Model.GetModel<KanbanReceivingManifestLevelModel>();

            if (PART_NO != "")
            {
                mdl.KanbanReceivingManifestLevelListData = getKanbanList(PART_NO, MANIFEST_NO);
            }
            Model.AddModel(mdl);
            return PartialView("GridIndexDetail", Model);
        }

        public List<KanbanReceivingManifestLevelData> getKanbanList(string PART_NO, string MANIFEST_NO)
        {
            List<KanbanReceivingManifestLevelData> listKanban = db.Fetch<KanbanReceivingManifestLevelData>("KanbanListToReceive", new object[] { 
              PART_NO,
              MANIFEST_NO
            });
            return listKanban;
        }

        public JsonResult ReceiveAll(KanbanReceivingManifestLevelData m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingManifestLevelData> listOrder = new List<KanbanReceivingManifestLevelData>();
            try
            {
                listOrder = db.Fetch<KanbanReceivingManifestLevelData>("KanbanReceiveAll", new object[] { m.PART_NO, m.MANIFEST_NO });
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listOrder, message = message, success = success, count = listOrder.Count }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Receive(KanbanReceivingManifestLevelData m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingManifestLevelData> listOrder = new List<KanbanReceivingManifestLevelData>();
            try
            {
                listOrder = db.Fetch<KanbanReceivingManifestLevelData>("KanbanReceive", new object[] {m.KANBAN_ID});
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listOrder, message = message, success = success, count = listOrder.Count }, JsonRequestBehavior.AllowGet);
        }
    }
}