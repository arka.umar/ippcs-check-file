﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Text;
using System.Reflection;


using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.MessageBoard;
using Toyota.Common.Web.Compression;
using Toyota.Common.Util.Text;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Portal.Models.Globals;
using Portal.Models.Receiving.ReceivingProgress;
namespace Portal.Controllers.Receiving.ReceivingProgress
{
    public class ReceivingProgressController : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public ReceivingProgressController()
            : base("ReceivingProgress", "Receiving Progress")
        {

        }
        protected override void StartUp()
        {
            
        }
        protected override void Init()
        {//t
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //KanbanSummaryPartReceivingModel mdl = new KanbanSummaryPartReceivingModel();
            //ViewData["LaneLookup"] = GetLane();
            //ViewData["SupplierLookup"] = GetSupplier();
            //ViewData["StatusLookup"] = GetStatus();
            //ViewData["RouteLookup"] = GetRoute();
            //Model.AddModel(mdl);
           
        }

        public ActionResult PartialData()
        {
            ViewBag.ListProgress = getProgress();
            return PartialView("Progress");
        }

        public ActionResult PartialData2()
        {
            ViewBag.ListProgress = getProgress2();
            return PartialView("Progress2");
        }

        public List<ReceivingProgressData> getProgress()
        {
            List<ReceivingProgressData> list = new List<ReceivingProgressData>();
            list = db.Fetch<ReceivingProgressData>("KanbanReceivingProgress", new object[] { "56", "2014-05-23" });
            return list;
        }

        public List<ReceivingProgressData> getProgress2()
        {

            List<ReceivingProgressData> list = new List<ReceivingProgressData>();
            list = db.Fetch<ReceivingProgressData>("KanbanReceivingProgress2", new object[] { "56", "2014-05-23" });
            return list;
        }

        public JsonResult csv()
        {
            List<string> data = null;
            string path = "D:/Counter1.csv";
            if(System.IO.File.Exists(path))
            {
               data = readCSV(path);
            }
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        public List<string> readCSV(string filename)
        {
            List<string> listData = new List<string>();
            List<string> list = new List<string>();
            try
            {
               
                StreamReader csvreader = new StreamReader(filename);
                while (!csvreader.EndOfStream)
                {
                    var line = csvreader.ReadLine();
                    line = line.Trim();
                    line = line.Replace("\0", "");
                    if (line != "")
                    {
                        string[] s = line.Split(',');
                        listData.Add(s[0] + "|" + s[1]);
                        //string DOCK_CD = s[0];
                        //string KANBAN_ID = s[1];
                        //string RECEIVING_FLG = s[2];


                        //if (s[0] != "DOCK_CD")
                        //{
                        //    //list.Add(s[0] + "," + s[1] + "," + s[2]);
                        //    SqlConnection con = new SqlConnection(cstring);
                        //    try
                        //    {
                        //        con.Open();
                        //    }
                        //    catch (SqlException sqle)
                        //    {
                        //        Console.WriteLine(sqle.Message);
                        //    }

                        //    Console.WriteLine(s[1]);
                        //    try
                        //    {
                        //        string sql = string.Format("UPDATE TB_R_RECEIVING_D SET RECEIVING_FLG = '{0}' WHERE KANBAN_ID = '{1}'", s[2], s[1]);
                        //        Console.WriteLine(sql);
                        //        SqlCommand myCommand = new SqlCommand();
                        //        myCommand.Connection = con;
                        //        myCommand.CommandText = sql;
                        //        myCommand.ExecuteNonQuery();
                        //    }
                        //    catch (SqlException sqle)
                        //    {
                        //        Console.WriteLine(sqle.Message);
                        //    }

                        //    try
                        //    {
                        //        con.Close();
                        //    }
                        //    catch (SqlException sqle)
                        //    {
                        //        Console.WriteLine(sqle.Message);
                        //    }
                        //}
                    }
                }
                csvreader.Close();
            }
            catch(Exception e)
            {
                listData.Add(e.Message);
            }
            return listData.ToList();
            //File.Delete(filename);
        }
    }
}