﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Text;
using System.Reflection;


using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Util.Configuration;
using Toyota.Common.Web.MessageBoard;
using Toyota.Common.Web.Compression;
using Toyota.Common.Util.Text;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

using Portal.Models.Globals;
using Portal.Models.Receiving.KanbanSummaryPartReceiving;
using Portal.Models.Receiving.KanbanReceiving;

namespace Portal.Controllers.Receiving.KanbanSummaryPartReceiving
{
    public class KanbanSummaryPartReceivingController : BaseController
    {
         IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
         public KanbanSummaryPartReceivingController()
            : base("KanbanSummaryPartReceiving", "Summary Part Receiving")
        {

        }
        protected override void StartUp()
        {

        }
        protected override void Init()
        {//t
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            KanbanSummaryPartReceivingModel mdl = new KanbanSummaryPartReceivingModel();
            ViewData["LaneLookup"] = GetLane();
            ViewData["SupplierLookup"] = GetSupplier();
            ViewData["StatusLookup"] = GetStatus();
            ViewData["RouteLookup"] = GetRoute();
            ViewData["DockLookup"] = GetDock();
            Model.AddModel(mdl);
        }

        public ActionResult PartialHeader()
        {
            ViewBag.DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            ViewBag.DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            ViewBag.RECEIVING_PLANT = String.IsNullOrEmpty(Request.Params["RECEIVING_PLANT"]) ? "" : Request.Params["RECEIVING_PLANT"];
            ViewBag.SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
            //ViewBag.P_LANE_CD = String.IsNullOrEmpty(Request.Params["P_LANE_CD"]) ? "" : Request.Params["P_LANE_CD"];
            ViewBag.MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
            ViewBag.ROUTE = String.IsNullOrEmpty(Request.Params["ROUTE"]) ? "" : Request.Params["ROUTE"];
            ViewBag.RATE = String.IsNullOrEmpty(Request.Params["RATE"]) ? "" : Request.Params["RATE"];
            return PartialView("IndexHeader");
        }

        //public ActionResult PartialData()
        //{
        //    string ORDER_NO = "";
        //    string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
        //    if (DELIVERY_NO != "")
        //    {
        //        ORDER_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
        //        ORDER_NO = ORDER_NO.Substring(1, ORDER_NO.Length - 3);
        //    }
        //    else
        //    {
        //        ORDER_NO = String.IsNullOrEmpty(Request.Params["ORDER_NO"]) ? "" : Request.Params["ORDER_NO"];
        //    }
        //    string RECEIVING_PLANT = String.IsNullOrEmpty(Request.Params["RECEIVING_PLANT"]) ? "" : Request.Params["RECEIVING_PLANT"];
        //    string DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
        //    string SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
        //    string P_LANE_CD = String.IsNullOrEmpty(Request.Params["P_LANE_CD"]) ? "" : Request.Params["P_LANE_CD"];
        //    string KANBAN_NO = String.IsNullOrEmpty(Request.Params["KANBAN_NO"]) ? "" : Request.Params["KANBAN_NO"];
        //    KanbanSummaryPartReceivingModel mdl = Model.GetModel<KanbanSummaryPartReceivingModel>();
        //    mdl.KanbanSummaryPartReceivingListData = getKanbanReceivingData(ORDER_NO, RECEIVING_PLANT, DOCK_CD, SUPPLIER_CD, P_LANE_CD, KANBAN_NO);
        //    Model.AddModel(mdl);
        //    return PartialView("GridIndex", Model);
        //}

        //public List<KanbanSummaryPartReceivingData> getKanbanReceivingData(string ORDER_NO, string RECEIVING_PLANT, string DOCK_CD, string SUPPLIER_CD, string P_LANE_CD, string KANBAN_NO)
        //{
        //    if( KANBAN_NO=="" || KANBAN_NO== null)
        //    {
        //        KANBAN_NO = null;
        //    }
        //    List<KanbanSummaryPartReceivingData> listKanban = new List<KanbanSummaryPartReceivingData>();
        //    if (ORDER_NO!="" && RECEIVING_PLANT!="" && DOCK_CD !="" && SUPPLIER_CD !="" && P_LANE_CD != "")
        //    {
        //        listKanban = db.Fetch<KanbanSummaryPartReceivingData>("KanbanSummaryPartReceivingData_new", new object[] { 
        //            ORDER_NO,
        //            RECEIVING_PLANT,
        //            DOCK_CD,
        //            SUPPLIER_CD,
        //            P_LANE_CD,
        //            KANBAN_NO
        //        });
        //    }
        //    return listKanban;
        //}


        public ActionResult PartialData()
        {
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            string ORDER_NO = String.IsNullOrEmpty(Request.Params["ORDER_NO"]) ? "" : Request.Params["ORDER_NO"];
            string RCV_PLANT_CD = String.IsNullOrEmpty(Request.Params["RCV_PLANT_CD"]) ? "" : Request.Params["RCV_PLANT_CD"];
            string DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            string SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
            string KANBAN_NO = String.IsNullOrEmpty(Request.Params["KANBAN_NO"]) ? "" : Request.Params["KANBAN_NO"];
            string MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
            string ROUTE = String.IsNullOrEmpty(Request.Params["ROUTE"]) ? "" : Request.Params["ROUTE"];
            string RIT = String.IsNullOrEmpty(Request.Params["RIT"]) ? "" : Request.Params["RIT"];
            string P_LANE_SEQ = String.IsNullOrEmpty(Request.Params["P_LANE_SEQ"]) ? "" : Request.Params["P_LANE_SEQ"];
            string DATE_FROM = String.IsNullOrEmpty(Request.Params["DATE_FROM"]) ? "" : Request.Params["DATE_FROM"];
            string DATE_TO = String.IsNullOrEmpty(Request.Params["DATE_TO"]) ? "" : Request.Params["DATE_TO"];
            string STATUS = String.IsNullOrEmpty(Request.Params["STATUS"]) ? "" : Request.Params["STATUS"];
            string SUPPLIER_PLANT = String.IsNullOrEmpty(Request.Params["SUPPLIER_PLANT"]) ? "" : Request.Params["SUPPLIER_PLANT"];
            KanbanSummaryPartReceivingModel mdl = Model.GetModel<KanbanSummaryPartReceivingModel>();
            mdl.KanbanSummaryPartReceivingListData = getKanbanReceivingData(DELIVERY_NO, ORDER_NO, RCV_PLANT_CD, DOCK_CD, SUPPLIER_CD, KANBAN_NO, MANIFEST_NO, ROUTE, RIT, P_LANE_SEQ, DATE_FROM, DATE_TO, STATUS, SUPPLIER_PLANT);
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        public List<KanbanSummaryPartReceivingData> getKanbanReceivingData(string DELIVERY_NO, string ORDER_NO, string RCV_PLANT_CD, string DOCK_CD, string SUPPLIER_CD, string KANBAN_NO, string MANIFEST_NO, string ROUTE, string RIT, string P_LANE_SEQ, string DATE_FROM, string DATE_TO, string STATUS, string SUPPLIER_PLANT)
        {
           if (!string.IsNullOrEmpty(DATE_FROM))
           {
                string d = DATE_FROM.Substring(0, 2);
                string m = DATE_FROM.Substring(3, 2);
                string y = DATE_FROM.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                DATE_FROM = tgl;
           }

           if (!string.IsNullOrEmpty(DATE_TO))
           {
                string d = DATE_TO.Substring(0, 2);
                string m = DATE_TO.Substring(3, 2);
                string y = DATE_TO.Substring(6, 4);
                string tgl = y + "-" + m + "-" + d;
                DATE_TO = tgl;
           }
           List<KanbanSummaryPartReceivingData> listKanban = new List<KanbanSummaryPartReceivingData>();
           if(DELIVERY_NO !="" || ORDER_NO !="" || RCV_PLANT_CD != "" || DOCK_CD != "" || SUPPLIER_CD != "" || KANBAN_NO != "" || MANIFEST_NO != "" || ROUTE!= "" || RIT != "" || P_LANE_SEQ != "" || DATE_FROM != "" || DATE_TO != "" || STATUS != "")
           {
                listKanban = db.Fetch<KanbanSummaryPartReceivingData>("KanbanSummaryPartReceivingData_new", new object[] { 
                    DELIVERY_NO,
                    ORDER_NO,
                    RCV_PLANT_CD,
                    DOCK_CD,
                    SUPPLIER_CD,
                    KANBAN_NO,
                    MANIFEST_NO,        
                    ROUTE,
                    RIT,
                    P_LANE_SEQ,
                    DATE_FROM,
                    DATE_TO,
                    STATUS,
                    SUPPLIER_PLANT
                });
           }
           
           return listKanban;
        }

        public JsonResult getShortage(KanbanReceivingData m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingData> listOrder = new List<KanbanReceivingData>();
            try
            {
                listOrder = db.Fetch<KanbanReceivingData>("KanbanSummaryPartReceivingShortage", new object[] { m.ORDER_NO, m.RCV_PLANT_CD, m.DOCK_CD, m.SUPPLIER_CD, m.P_LANE_CD, m.KANBAN_NO});
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listOrder, message = message, success = success, count = listOrder.Count }, JsonRequestBehavior.AllowGet);
        }

        #region unused
        //public ActionResult PartialData()
        //{
        //    string ARRIVAL_DATE = String.IsNullOrEmpty(Request.Params["ARRIVAL_DATE"]) ? "" : Request.Params["ARRIVAL_DATE"];
        //    string ARRIVAL_DATE_TO = String.IsNullOrEmpty(Request.Params["ARRIVAL_DATE_TO"]) ? "" : Request.Params["ARRIVAL_DATE_TO"];
        //    string P_LANE_SEQ = String.IsNullOrEmpty(Request.Params["P_LANE_SEQ"]) ? "" : Request.Params["P_LANE_SEQ"];
        //    string SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
        //    string MANIFEST_NO = String.IsNullOrEmpty(Request.Params["MANIFEST_NO"]) ? "" : Request.Params["MANIFEST_NO"];
        //    string ROUTE_RATE = String.IsNullOrEmpty(Request.Params["ROUTE_RATE"]) ? "" : Request.Params["ROUTE_RATE"];
        //    string ORDER_NO = String.IsNullOrEmpty(Request.Params["ORDER_NO"]) ? "" : Request.Params["ORDER_NO"];
        //    string KANBAN_NO = String.IsNullOrEmpty(Request.Params["KANBAN_NO"]) ? "" : Request.Params["KANBAN_NO"];
        //    string STATUS = String.IsNullOrEmpty(Request.Params["STATUS"]) ? "" : Request.Params["STATUS"];

            

        //    string DateParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(ARRIVAL_DATE))
        //    {
        //        string d = ARRIVAL_DATE.Substring(0, 2);
        //        string m = ARRIVAL_DATE.Substring(3, 2);
        //        string y = ARRIVAL_DATE.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        DateParameter += "'" + tgl + "'";
        //    }

        //    ARRIVAL_DATE = DateParameter;

        //    string DateToParameter = string.Empty;
        //    if (!string.IsNullOrEmpty(ARRIVAL_DATE_TO))
        //    {
        //        string d = ARRIVAL_DATE_TO.Substring(0, 2);
        //        string m = ARRIVAL_DATE_TO.Substring(3, 2);
        //        string y = ARRIVAL_DATE_TO.Substring(6, 4);
        //        string tgl = y + "-" + m + "-" + d;
        //        DateToParameter += "'" + tgl + "'";
        //    }

        //    ARRIVAL_DATE_TO = DateToParameter;

        //    string status = string.Empty;
        //    if (!string.IsNullOrEmpty(STATUS))
        //    {
        //        string[] statuslist = STATUS.Split(';');
        //        foreach (string r in statuslist)
        //        {
        //            status += "'" + r + "',";
        //        }
        //        status = status.Remove(status.Length - 1, 1);
        //    }

        //    STATUS = status;

        //    string lane = string.Empty;
        //    if (!string.IsNullOrEmpty(P_LANE_SEQ))
        //    {
        //        string[] lanelist = P_LANE_SEQ.Split(';');
        //        foreach (string r in lanelist)
        //        {
        //            lane += "'" + r + "',";
        //        }
        //        lane = lane.Remove(lane.Length - 1, 1);
        //    }

        //    P_LANE_SEQ = lane;

        //    string routerate = string.Empty;
        //    if (!string.IsNullOrEmpty(ROUTE_RATE))
        //    {
        //        string[] routeratelist = ROUTE_RATE.Split(';');
        //        foreach (string r in routeratelist)
        //        {
        //            routerate += "'" + r + "',";
        //        }
        //        routerate = routerate.Remove(routerate.Length - 1, 1);
        //    }

        //    ROUTE_RATE = routerate;

        //    string suppliercd = string.Empty;
        //    if (!string.IsNullOrEmpty(SUPPLIER_CD))
        //    {
        //        string[] suppliercdlist = SUPPLIER_CD.Split(';');
        //        foreach (string r in suppliercdlist)
        //        {
        //            suppliercd += "'" + r.Substring(0,4) + "',";
        //        }
        //        suppliercd = suppliercd.Remove(suppliercd.Length - 1, 1);
        //    }

        //    SUPPLIER_CD = suppliercd;

        //    KanbanSummaryPartReceivingModel mdl = Model.GetModel<KanbanSummaryPartReceivingModel>();
        //    mdl.KanbanSummaryPartReceivingListData = getKanbanReceivingData(ARRIVAL_DATE, ARRIVAL_DATE_TO, P_LANE_SEQ, SUPPLIER_CD, MANIFEST_NO, ROUTE_RATE, ORDER_NO, KANBAN_NO, STATUS);
        //    Model.AddModel(mdl);
        //    return PartialView("GridIndex", Model);
        //}


        //public List<KanbanSummaryPartReceivingData> getKanbanReceivingData(
        //    string ARRIVAL_DATE, 
        //    string ARRIVAL_DATE_TO, 
        //    string P_LANE_SEQ, 
        //    string SUPPLIER_CD, 
        //    string MANIFEST_NO, 
        //    string ROUTE_RATE, 
        //    string ORDER_NO, 
        //    string KANBAN_NO, 
        //    string STATUS
        //    )
        //{
        //    List<KanbanSummaryPartReceivingData> listKanban = new List<KanbanSummaryPartReceivingData>();
        //    if(ARRIVAL_DATE!="" || ARRIVAL_DATE_TO !="" || P_LANE_SEQ != "" || SUPPLIER_CD != "" || MANIFEST_NO != "" || ROUTE_RATE != "" || ORDER_NO != "" || KANBAN_NO != "" || STATUS != "")
        //    {
        //         listKanban = db.Fetch<KanbanSummaryPartReceivingData>("KanbanSummaryPartReceivingData", new object[] { 
        //            ARRIVAL_DATE,
        //            ARRIVAL_DATE_TO,
        //            P_LANE_SEQ,
        //            SUPPLIER_CD,
        //            MANIFEST_NO,
        //            ROUTE_RATE,
        //            ORDER_NO,
        //            KANBAN_NO,
        //            STATUS
        //        });
               
        //    }
        //    return listKanban;

        //}
        #endregion


        public List<p_lane_data> GetLane()
        {
            List<p_lane_data> listLane = new List<p_lane_data>();
            try
            {
                listLane = db.Fetch<p_lane_data>("KanbanLaneSeq", new object[] {});
            }
            catch (Exception ex) { throw ex; }
            return listLane;
        }

        public ActionResult LaneLookup(p_lane_data m)
        {
            TempData["GridName"] = "PLaneOption";
            ViewData["LaneLookup"] = GetLane();
            return PartialView("PG", ViewData["LaneLookup"]);
        }

        public ActionResult SupplierLookup()
        {
            TempData["GridName"] = "SupplierOption";
            ViewData["SupplierLookup"] = GetSupplier();
            return PartialView("PG", ViewData["SupplierLookup"]);
        }
        public List<SupplierData> GetSupplier()
        {
            List<SupplierData> listLane = new List<SupplierData>();
            try
            {
                listLane = db.Fetch<SupplierData>("KanbanSupplierData", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listLane;
        }

        public ActionResult DockLookup()
        {
            TempData["GridName"] = "DockOption";
            ViewData["DockLookup"] = GetDock();
            return PartialView("PG", ViewData["DockLookup"]);
        }

        public List<DockData> GetDock()
        {
            List<DockData> listDock = new List<DockData>();
            try
            {
                listDock = db.Fetch<DockData>("KanbanGetDock", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listDock;
        }

        

        public List<RouteData> GetRoute()
        {
            List<RouteData> listRoute = new List<RouteData>();
            try
            {
                listRoute = db.Fetch<RouteData>("KanbanRoute2", new object[] {});
            }
            catch (Exception ex) { throw ex; }
            return listRoute;
        }

        public ActionResult RouteLookup()
        {
            TempData["GridName"] = "RouteOption";
            ViewData["RouteLookup"] = GetRoute();
            return PartialView("PG", ViewData["RouteLookup"]);
        }


        public ActionResult StatusLookup()
        {
            TempData["GridName"] = "StatusOption";
            ViewData["StatusLookup"] = GetStatus();
            return PartialView("PG", ViewData["StatusLookup"]);
        }

        public List<StatusData> GetStatus()
        {
            List<StatusData> data = new List<StatusData>();

            data.Add(new StatusData()
            {
                STATUS = "Ordered"
            });

            data.Add(new StatusData()
            {
                STATUS = "Shortage"
            });

            data.Add(new StatusData()
            {
                STATUS = "Completed"
            });
            return data;
        }
        
        public JsonResult getKanbanReceivingHeader(KanbanReceivingHeaderData m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingHeaderData> listTruck = new List<KanbanReceivingHeaderData>();
            try
            {
                listTruck = db.Fetch<KanbanReceivingHeaderData>("KanbanSummaryPartReceivingHeader", new object[] { m.DELIVERY_NO, m.DOCK_CD, m.SUPPLIER_CD, m.RCV_PLANT_CD, m.P_LANE_CD});
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listTruck }, JsonRequestBehavior.AllowGet);
        }


        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadKanbanSummaryPartReceiving(
                string DELIVERY_NO,
                string ORDER_NO,
                string RCV_PLANT_CD,
                string DOCK_CD,
                string SUPPLIER_CD,
                string KANBAN_NO,
                string MANIFEST_NO,        
                string ROUTE,
                string RIT,
                string P_LANE_SEQ,
                string DATE_FROM,
                string DATE_TO,
                string STATUS,
                string SUPPLIER_PLANT
            )
        {
            string fileName = String.Empty;

            // Update download user and status
            IEnumerable<KanbanSummaryPartReceivingDataDownload> qry;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;
            if(KANBAN_NO == "")
            {
                KANBAN_NO = null;
            }
            qry = db.Fetch<KanbanSummaryPartReceivingDataDownload>("KanbanSummaryPartReceivingData_new", new object[] { 
                    DELIVERY_NO,
                    ORDER_NO,
                    RCV_PLANT_CD,
                    DOCK_CD,
                    SUPPLIER_CD,
                    KANBAN_NO,
                    MANIFEST_NO,        
                    ROUTE,
                    RIT,
                    P_LANE_SEQ,
                    DATE_FROM,
                    DATE_TO,
                    STATUS,
                    SUPPLIER_PLANT
                });
            string dt = Convert.ToString(DateTime.Now);
            dt = dt.Replace(" ", "");
            dt = dt.Replace("/", "");
            dt = dt.Replace(":", "");

            
            fileName = "KanbanSummaryPartReceivingData_" + dt + "_" + AuthorizedUser.Username + ".xls";
            hasil = exporter.Write(ConvertToDataTable(qry), "Data");


            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        
    }
}