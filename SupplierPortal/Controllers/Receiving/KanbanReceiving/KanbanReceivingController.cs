﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;
using ServiceStack.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Task;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Logging;
using Toyota.Common.Web.Log;

using ServiceStack.Text;
using Toyota.Common.Web.Compression;
using System.Xml;
using Telerik.Reporting.XmlSerialization;
using Telerik.Reporting.Processing;
using System.Reflection;
using System.Drawing;

using Portal.Models.Globals;
using Portal.Models.Receiving.KanbanReceiving;
using Portal.Models.Receiving.KanbanSummaryPartReceiving;
namespace Portal.Controllers.Receiving.KanbanReceiving
{
    public class KanbanReceivingController : BaseController
    {
        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        public KanbanReceivingController() : base("KanbanReceiving","Kanban Receiving")
        {

        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            //t
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            KanbanReceivingModel mdl = new KanbanReceivingModel();
            KanbanReceivingDownloadModel mdl2 = new KanbanReceivingDownloadModel();
            Model.AddModel(mdl);
            Model.AddModel(mdl2);
            ViewData["SupplierLookup"] = GetSupplier();
        }

        public ActionResult PartialHeader()
        {
            ViewBag.DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            ViewBag.DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            ViewBag.RECEIVING_PLANT = String.IsNullOrEmpty(Request.Params["RECEIVING_PLANT"]) ? "" : Request.Params["RECEIVING_PLANT"];
            ViewBag.SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
            ViewBag.SUPPLIER_PLANT_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_PLANT_CD"];
            return PartialView("IndexHeader");
        }
        public ActionResult PartialHeader2()
        {
            return PartialView("IndexHeader2");
        }

        public ActionResult PartialData()
        {
            //string ORDER_NO = "";
            //string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            //if(DELIVERY_NO != "")
            //{
            //     ORDER_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            //     ORDER_NO = ORDER_NO.Substring(1, ORDER_NO.Length - 3);
            //}
            //else
            //{
            //    ORDER_NO = String.IsNullOrEmpty(Request.Params["ORDER_NO"]) ? "" : Request.Params["ORDER_NO"];
            //}
            string DELIVERY_NO = String.IsNullOrEmpty(Request.Params["DELIVERY_NO"]) ? "" : Request.Params["DELIVERY_NO"];
            string DOCK_CD = String.IsNullOrEmpty(Request.Params["DOCK_CD"]) ? "" : Request.Params["DOCK_CD"];
            string RECEIVING_PLANT = String.IsNullOrEmpty(Request.Params["RECEIVING_PLANT"]) ? "" : Request.Params["RECEIVING_PLANT"];
            string SUPPLIER_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_CD"]) ? "" : Request.Params["SUPPLIER_CD"];
            string SUPPLIER_PLANT_CD = String.IsNullOrEmpty(Request.Params["SUPPLIER_PLANT_CD"]) ? "" : Request.Params["SUPPLIER_PLANT_CD"];
            //string ROUTE_RATE = String.IsNullOrEmpty(Request.Params["ROUTE_RATE"]) ? "" : Request.Params["ROUTE_RATE"];
            KanbanReceivingModel mdl = Model.GetModel<KanbanReceivingModel>();

            
                mdl.KanbanReceivingListData = getKanbanReceivingData(DELIVERY_NO, DOCK_CD, RECEIVING_PLANT, SUPPLIER_CD, SUPPLIER_PLANT_CD);
    
            Model.AddModel(mdl);
            return PartialView("GridIndex", Model);
        }

        public ActionResult SupplierLookup()
        {
            TempData["GridName"] = "SupplierOption";
            ViewData["SupplierLookup"] = GetSupplier();
            return PartialView("PG", ViewData["SupplierLookup"]);
        }
        public List<SupplierData> GetSupplier()
        {
            List<SupplierData> listLane = new List<SupplierData>();
            try
            {
                listLane = db.Fetch<SupplierData>("KanbanSupplierData", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listLane;
        }
        public List<KanbanReceivingData> getKanbanReceivingData(string DELIVERY_NO, string DOCK_CD, string RECEIVING_PLANT, string SUPPLIER_CD, string SUPPLIER_PLANT_CD)
        {
            List<KanbanReceivingData> listKanban = new List<KanbanReceivingData>();
            if(DELIVERY_NO != "" || DOCK_CD != "" || RECEIVING_PLANT != "" || SUPPLIER_CD != "" || SUPPLIER_PLANT_CD != "")
            {
                listKanban = db.Fetch<KanbanReceivingData>("KanbanReceivingData_new", new object[] { 
                  DELIVERY_NO,
                  DOCK_CD,
                  RECEIVING_PLANT,
                  SUPPLIER_CD,
                  SUPPLIER_PLANT_CD
                });
            }
            
            return listKanban;
        }

        public ActionResult PartialDataUpload()
        {
            KanbanReceivingDownloadModel mdl2 = Model.GetModel<KanbanReceivingDownloadModel>();
            mdl2.KanbanReceivingListDownloadData = getKanbanUploadedData();
            Model.AddModel(mdl2);
            return PartialView("GridPopup", Model);
        }

        public List<KanbanReceivingDataDownload> getKanbanUploadedData()
        {
            List<KanbanReceivingDataDownload> listKanban = db.Fetch<KanbanReceivingDataDownload>("KanbanUploadedData", new object[] { });
            return listKanban;
        }

        public JsonResult updateReceivingFlag()
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingDataHeaderUpload> listHeader = new List<KanbanReceivingDataHeaderUpload>();
            try
            {
                listHeader = db.Fetch<KanbanReceivingDataHeaderUpload>("KanbanUpdateReceivingFlagFromUpload", new object[] { });
                message = "Data sucessfuly updated";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { success = success, message = message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult cancelReceivingFlag()
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingDataHeaderUpload> listHeader = new List<KanbanReceivingDataHeaderUpload>();
            try
            {
                listHeader = db.Fetch<KanbanReceivingDataHeaderUpload>("KanbanCancelUpdateUploadedData", new object[] { });
                message = "Data sucessfuly canceled";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { success = success, message = message }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getKanbanUploadedHeader()
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingDataHeaderUpload> listHeader = new List<KanbanReceivingDataHeaderUpload>();
            try
            {
                listHeader = db.Fetch<KanbanReceivingDataHeaderUpload>("KanbanUploadedHeader",new object[]{});
                message = "Data successfuly loaded";
                success = true;
            }
            catch(Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { success = success, data = listHeader }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getOrder(KanbanReceivingOrderData m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingOrderData> listOrder = new List<KanbanReceivingOrderData>();
            try
            {
                listOrder = db.Fetch<KanbanReceivingOrderData>("KanbanReceivingOrder", new object[] { m.ORDER_NO, m.DOCK_CD });
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listOrder, message = message, success = success, count = listOrder.Count }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getKanbanReceivingHeader(KanbanReceivingHeaderData m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingHeaderData> listTruck = new List<KanbanReceivingHeaderData>();
            try
            {
                listTruck = db.Fetch<KanbanReceivingHeaderData>("TruckReceivingHeader", new object[] { m.DELIVERY_NO, m.DOCK_CD, m.RCV_PLANT_CD, m.SUPPLIER_CD, m.SUPPLIER_PLANT });
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listTruck }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getShortage(KanbanReceivingData m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingData> listOrder = new List<KanbanReceivingData>();
            try
            {
                listOrder = db.Fetch<KanbanReceivingData>("KanbanReceivingGetShortage", new object[] { m.ORDER_NO, m.DOCK_CD, m.RCV_PLANT_CD, m.SUPPLIER_CD, m.SUPPLIER_PLANT});
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listOrder, message = message, success = success, count = listOrder.Count }, JsonRequestBehavior.AllowGet);
        }

     
        public List<KanbanReceivingRouteData> GetRoute(string ORDER_NO, string DOCK_CD)
        {
            List<KanbanReceivingRouteData> listRoute = new List<KanbanReceivingRouteData>();
            try
            {
                listRoute = db.Fetch<KanbanReceivingRouteData>("KanbanReceivingRoute", new object[] { ORDER_NO, DOCK_CD });
            }
            catch (Exception ex) { throw ex; }
            return listRoute;
        }

        public ActionResult RouteLookup(KanbanReceivingRouteData m)
        {
            TempData["GridName"] = "RouteOption";
            ViewData["RouteLookup"] = GetRoute(m.ORDER_NO, m.DOCK_CD);
            return PartialView("PG", ViewData["RouteLookup"]);
        }


        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        //public void DownloadKanbanReceiving(
        //    string ORDER_NO="", 
        //    string DOCK_CD="",
        //    string RECEIVING_PLANT = "",
        //    string SUPPLIER_CD = "", 
        //    string SUPPLIER_PLANT_CD = ""
        //    )
        //{
        //    string fileName = String.Empty;

        //    // Update download user and status
        //    IEnumerable<KanbanReceivingDataDownload> qry;

        //    IExcelWriter exporter = ExcelWriter.GetInstance();
        //    byte[] hasil;

        //    qry = db.Fetch<KanbanReceivingDataDownload>(
        //       "KanbanReceivingDataDownload_new",
        //        new object[] { 
        //              ORDER_NO,
        //              DOCK_CD,
        //              RECEIVING_PLANT,
        //              SUPPLIER_CD,
        //              SUPPLIER_PLANT_CD
        //           });
        //    string dt = Convert.ToString(DateTime.Now);
        //    dt = dt.Replace(" ", "");
        //    dt = dt.Replace("/", "");
        //    dt = dt.Replace(":", "");

        //    fileName = "KanbanReceiving_" + dt + "_" + AuthorizedUser.Username + ".xls";
        //    hasil = exporter.Write(ConvertToDataTable(qry), "Data");


        //    db.Close();
        //    Response.Clear();
        //    //Response.ContentType = result.MimeType;
        //    Response.Cache.SetCacheability(HttpCacheability.Private);
        //    Response.Expires = -1;
        //    Response.Buffer = true;

        //    Response.ContentType = "application/octet-stream";
        //    Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
        //    /* ------------------------------*/
        //    Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
        //    Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

        //    Response.BinaryWrite(hasil);
        //    Response.End();
        //}


        public void DownloadKanbanReceiving(
            string DELIVERY_NO = "",
            string ORDER_NO = "",
            string RCV_PLANT_CD = "",
            string DOCK_CD = "",
            string SUPPLIER_CD = "",
            string KANBAN_NO = "",
            string MANIFEST_NO = "",
            string ROUTE = "",
            string RIT = "",
            string P_LANE_SEQ = "",
            string DATE_FROM = "",
            string DATE_TO = "",
            string STATUS = ""
           )
        {
            string fileName = String.Empty;

            // Update download user and status
            IEnumerable<KanbanReceivingDataDownload> qry;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;

            qry = db.Fetch<KanbanReceivingDataDownload>(
               "KanbanReceivingDataDownload_new",
                new object[] { 
                     DELIVERY_NO,
                     ORDER_NO,
                     RCV_PLANT_CD,
                     DOCK_CD,
                     SUPPLIER_CD,
                     KANBAN_NO,
                     MANIFEST_NO,
                     ROUTE,
                     RIT,
                     P_LANE_SEQ,
                     DATE_FROM,
                     DATE_TO,
                     STATUS
                   });
            string dt = Convert.ToString(DateTime.Now);
            dt = dt.Replace(" ", "");
            dt = dt.Replace("/", "");
            dt = dt.Replace(":", "");

            fileName = "KanbanReceiving_" + dt + "_" + AuthorizedUser.Username + ".xls";
            hasil = exporter.Write(ConvertToDataTable(qry), "Data");


            db.Close();
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }


        public class UploadControlHelper
        {

            //public const string UploadDirectory = "Content/Upload/Invoice/";
            //public const string TemplateFName = "TemplateInvoice";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls",".xlsx" },
                MaxFileSize = 20971520
            };

        }

        public ActionResult CallbacksUpload()
        {
            //userLogin = AuthorizedUser.Username;
            UploadControlExtension.GetUploadedFiles("ucCallbacks", UploadControlHelper.ValidationSettings, ucCallbacks_FileUploadComplete);

            return null;
        }


        public void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string connectingString = "";
                string path = Path.GetFullPath(e.UploadedFile.PostedFile.FileName);
                string fileName =
                Path.GetFileName(e.UploadedFile.PostedFile.FileName);

                string fileExtension = Path.GetExtension(e.UploadedFile.PostedFile.FileName);

                string fileLocation = Server.MapPath("~/Content/Upload/" + fileName);
                e.UploadedFile.SaveAs(fileLocation);

                if (fileExtension == ".xls")
                {
                    connectingString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                }

                else if (fileExtension == ".xlsx")
                {
                    connectingString =
                        "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";
                }

                using (OleDbConnection con = new OleDbConnection(connectingString))
                {
                    try
                    {
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        con.Open();
                        DataTable dtExcelSheetName = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        int sheets = 0;
                        while(sheets < dtExcelSheetName.Rows.Count)
                        {
                            string getExcelSheetName = dtExcelSheetName.Rows[sheets]["Table_Name"].ToString();
                            cmd.CommandText = "Select * FROM [" + getExcelSheetName + "]";
                            dAdapter.SelectCommand = cmd;
                            dAdapter.Fill(ds);
                            string KANBAN_ID; 
                            string KANBAN_NO;  
                            string PART_NO; 
                            string PART_NAME; 
                            string SUPPLIER_CD; 
                            string SUPPLIER_NAME; 
                            string ORDER_NO; 
                            string MANIFEST_NO; 
                            string RCV_PLANT_CD;
                            string DOCK_CD;
                            string P_LANE_NO; 
                            string ROUTE; 
                            string RIT; 
                            string ARRIVAL_PLAN_DT; 
                            string ARRIVAL_ACTUAL_DT; 
                            string STATUS;
                            string UPDATE;
                            int rows = 0;
                            while(rows < ds.Tables[sheets].Rows.Count)
                            {
                                KANBAN_ID = ds.Tables[sheets].Rows[rows][1].ToString();
                                KANBAN_NO = ds.Tables[sheets].Rows[rows][0].ToString();
                                PART_NO = ds.Tables[sheets].Rows[rows][2].ToString();
                                PART_NAME = ds.Tables[sheets].Rows[rows][3].ToString();
                                SUPPLIER_CD = ds.Tables[sheets].Rows[rows][4].ToString();
                                SUPPLIER_NAME = ds.Tables[sheets].Rows[rows][5].ToString();
                                ORDER_NO = ds.Tables[sheets].Rows[rows][6].ToString();
                                MANIFEST_NO = ds.Tables[sheets].Rows[rows][7].ToString();
                                RCV_PLANT_CD = ds.Tables[sheets].Rows[rows][8].ToString();
                                DOCK_CD = ds.Tables[sheets].Rows[rows][9].ToString();
                                P_LANE_NO = ds.Tables[sheets].Rows[rows][10].ToString();
                                ROUTE =  ds.Tables[sheets].Rows[rows][11].ToString();
                                RIT = ds.Tables[sheets].Rows[rows][12].ToString();
                                ARRIVAL_PLAN_DT = ds.Tables[sheets].Rows[rows][13].ToString();
                                ARRIVAL_ACTUAL_DT = ds.Tables[sheets].Rows[rows][14].ToString();
                                STATUS = ds.Tables[sheets].Rows[rows][15].ToString();
                                UPDATE = ds.Tables[sheets].Rows[rows][16].ToString();

                                int rcv;
                                int upd;

                                if(STATUS == "1")
                                {
                                    rcv = 1;
                                }
                                else
                                {
                                    rcv = 0;
                                }

                                if (UPDATE == "1")
                                {
                                    upd = 1;
                                }
                                else
                                {
                                    upd = 0;
                                }

                                string user = AuthorizedUser.Username;
                                //var l = db.Fetch<KanbanReceivingDataDownload>("KanbanReceivingUpdateReceivingFlg", new object[] { 
                                //    KANBAN_NO,
                                //    PART_NO,
                                //    KANBAN_ID,
                                //    rcv,
                                //    upd,
                                //    user
                                //});
                                try
                                {
                                    var l = db.Fetch<KanbanReceivingDataDownload>("KanbanReceivingUpdateReceivingFlg", new object[] { 
                                        KANBAN_ID,
                                        KANBAN_NO,
                                        PART_NO,
                                        PART_NAME,
                                        SUPPLIER_CD,
                                        SUPPLIER_NAME,
                                        ORDER_NO,
                                        MANIFEST_NO,
                                        RCV_PLANT_CD,
                                        DOCK_CD,
                                        P_LANE_NO,
                                        ROUTE,
                                        RIT,
                                        ARRIVAL_PLAN_DT,
                                        ARRIVAL_ACTUAL_DT,
                                        rcv,
                                        upd
                                    });
                                    db.Close();
                                }
                                catch(Exception exc)
                                {
                                    Console.WriteLine(exc.Message);
                                }
                                
                                rows++;
                            }
                            sheets++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                if(System.IO.File.Exists(fileLocation))
                {
                    System.IO.File.Delete(fileLocation);
                }
            }

        }

        public JsonResult get_receiving(KanbanReceivingDetailForMobile m)
        {
            string message = "";
            Boolean success = false;
            List<KanbanReceivingDetailForMobile> listOrder = new List<KanbanReceivingDetailForMobile>();
            try
            {
                listOrder = db.Fetch<KanbanReceivingDetailForMobile>("KanbanReceivingDetailForMobile", new object[] { m.DOCK_CD });
                message = "Data successfuly loaded";
                success = true;
            }
            catch (Exception e)
            {
                message = e.Message;
                success = false;
            }
            return Json(new { data = listOrder, message = message, success = success, count = listOrder.Count }, JsonRequestBehavior.AllowGet);
        }
    }
 }