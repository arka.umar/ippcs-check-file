﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.MessageBoard;
using Portal.Models.Globals;
using Portal.Models.NewSystem;
using Portal.Models.Function;
using Telerik.Reporting.XmlSerialization;
using System.Xml;
using Telerik.Reporting.Processing;

namespace Portal.Controllers
{
    public class NewSystemController : BaseController
    {
        public NewSystemController()
            : base("System Master")
        {
        }

        #region Property
        List<NewSystem> _listmodel = null;

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        private List<NewSystem> ListModel
        {
            get
            {
                if (_listmodel == null)
                    _listmodel = new List<NewSystem>();
                return _listmodel;
            }
            set
            {
                _listmodel = value;
            }
        }
        #endregion

        #region ListGrid

        private List<NewSystem> GetList(string functid  , string systemcd , string systemrem, string systemval )
        //private List<NewSystem> GetList(string functid = "[]><", string systemcd = "[]//", string systemrem = "||?<xxx", string systemval = "||?<xxx")
        {
            
            List<NewSystem> VarList = new List<NewSystem>();
            IDBContext db = DbContext;
            VarList = db.Fetch<NewSystem>("GetAllNewSystem", new object[] { functid, systemcd,systemrem, systemval});
            return VarList;
        }

        public ActionResult ReloadNewSystemGrid()
        {
            var functid = Request.Params["functid"];
            string[] functionId;

            functid = ((functid == null) ? "": functid);

            if (functid != "")
            {
                functionId = functid.Split(';');

                if (functionId.Length > 1)
                {
                    functid = "";
                    for (int i = 0; i < functionId.Length; i++)
                    {
                        if (functid != "")
                        {
                            functid = functid + ",'" + functionId[i].ToString() + "'";
                        }
                        else
                        {
                            functid = "'" + functionId[i].ToString() + "'";

                        }

                    }

                }
                else
                {
                    functid = "'" + functid + "'";
                }
            }
            NewSystemModel mdls = Model.GetModel<NewSystemModel>();
            mdls.NewSystemList = GetList(functid,
                                Request.Params["systemcd"]
                                , Request.Params["systemrem"]
                                ,Request.Params["systemval"]
                                );

            Model.AddModel(mdls);
            return PartialView("NewSystemGrid", Model);
        }

       
        #endregion

        public ActionResult PartialFuncGridLookup()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            //List<RouteCodeMaster> routemaster = Model.GetModel<User>().FilteringArea<RouteCodeMaster>(GetAllRoute(), "SUPPLIER_CODE", "DailyOrder", "OIHSupplierOption");

            TempData["GridName"] = "txtFuncID";
            ViewData["funcGridLookup"] = GetAllFungsi();

            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["funcGridLookup"]);

        }

        private List<Function> GetAllFungsi()
        {
            List<Function> listSupp = new List<Function>();
            IDBContext db = DbContext;
            listSupp = db.Fetch<Function>("GetFungsi", new object[] { });
            db.Close();

            return listSupp;
        }

        

        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public void DownloadTBM(string functid, string systemcd, string systemrem, string systemval)
        {
            string fileName = String.Empty;
            IExcelWriter exporter = ExcelWriter.GetInstance();
            byte[] hasil;


            NewSystemModel model = Model.GetModel<NewSystemModel>();
            model.NewSystemList = GetAllNewSystem(functid, systemcd , systemrem, systemval);
            fileName = "SystemMaster.xls";
            hasil = exporter.Write(ConvertToDataTable(model.NewSystemList), "FileDownloadNew");

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            /*
             * @Lufty: numpang nambahin, kisanak
             */
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();

        }

        private List<NewSystem> GetAllNewSystem(string Fung, string Code, string Rem, string Val)
        {
            List<NewSystem> NS = new List<NewSystem>();
            IDBContext db = DbContext;

            NS = db.Fetch<NewSystem>("GetAllNewSystem", new object[] { 
             
               //(RTEDATE == "" || RTEDATE == null)? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(RTEDATE,"dd.MM.yyyy",null) 
               //RTEGRPCD,SUPPCD
               Fung == null ? "" : Fung 
               ,Code  == null ? "" : Code
                ,Val == null ? "" : Val
                ,Rem == null ? "" : Rem
                //RTEGRPCD == null ? "" : RTEGRPCD,
            });
            db.Close();

            return NS;
        }



        protected override void StartUp()
        {

        }

        public ActionResult PopupBaruPartial()
        {
            return PartialView("PopUpNew", Model);
        }

     

        protected override void Init()
        {
            NewSystemModel mdl = new NewSystemModel();
            //mdl.NewSystemList = GetList("","","");
            Model.AddModel(mdl);

            ViewData["funcGridLookup"] = GetAllFungsi();
        }


 
        public ContentResult SaveData(string status , string functid, string syscd, string sysrem, string sysval, string pfunctid, string psyscd, string psysrem, string psysval)
        {
            IDBContext db = DbContext;
            String ResultQuery = "";
            string Query = string.Empty;
            string user = AuthorizedUser.Username;
            try
            {
                if (status == "update")
                {
                    Query = db.ExecuteScalar<string>("UpdateSystem", new object[] { functid, syscd, sysrem, sysval, AuthorizedUser.Username });
                    ResultQuery = "Data Has Been Updated";
                }
                if(status == "new")
                {
                    Query = db.ExecuteScalar<string>("InsertAllNewSystem", new object[] { functid,  syscd,  sysrem,  sysval, user });
                    ResultQuery = "Data Has Been Saved";
                    
                }

               
                
            }
            catch (Exception)
            {
                ResultQuery = "Error";
            }
            finally { db.Close(); }
            return Content(ResultQuery);
        }


       

//        public void DownloadTBM(string funcid)
//        {
//            string connString = DBContextNames.DB_PCS;
//            Telerik.Reporting.Report laporan;

//            XmlReaderSettings settings;
//            XmlReader xmlReader;
//            ReportXmlSerializer xmlSerializer;
//            Telerik.Reporting.SqlDataSource sqlDataSource;

//            settings = new XmlReaderSettings();
//            settings.IgnoreWhitespace = true;
//            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\TBMSystem\ReportTBMSystem.trdx", settings))
//            {
//                xmlSerializer = new ReportXmlSerializer();
//                laporan = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
//            }

//            sqlDataSource = (Telerik.Reporting.SqlDataSource)laporan.DataSource;

//            sqlDataSource.ConnectionString = connString;
//            sqlDataSource.SelectCommand = String.Format(@"SELECT[FUNCTION_ID] FUNCTION_ID,
//		                                                [SYSTEM_CD] SYSTEM_CD,
//		                                                [SYSTEM_VALUE] SYSTEM_VALUE,
//		                                                [SYSTEM_REMARK] SYSTEM_REMARK,
//		                                                [CREATED_BY] CREATED_BY,
//		                                                [CREATED_DT] CREATED_DT,
//		                                                [CHANGED_BY] CHANGED_BY,
//		                                                [CHANGED_DT] CHANGED_DT
//	                                                    FROM [TB_M_SYSTEM] WHERE [FUNCTION_ID] IN({0})", SplitTBM(funcid));

//            ReportProcessor reportProcessor = new ReportProcessor();

//            RenderingResult result = reportProcessor.RenderReport("Xls", laporan, null);

//            string fileName = "TBM_System" + "." + result.Extension;

//            Response.Clear();
//            Response.ContentType = result.MimeType;
//            Response.Cache.SetCacheability(HttpCacheability.Private);
//            Response.Expires = -1;
//            Response.Buffer = true;

//            Response.AddHeader("Content-Disposition",
//                               string.Format("{0};FileName=\"{1}\"",
//                                             "attachment",
//                                             fileName));

//            Response.BinaryWrite(result.DocumentBytes);
//            Response.End();
//        }

//        string SplitTBM(string val)
//        {

//            string[] separato = { @"," };
//            string[] arrtbm = val.Split(separato, StringSplitOptions.RemoveEmptyEntries);

//            string[] hasil = new string[arrtbm.Length];

//            for (int i = 0; i < arrtbm.Length; i++)
//            {
//                hasil[i] = "'" + arrtbm[i] + "'";
//            }


//            return string.Join(",", hasil);
//        }



       

        


        #region DELETE BUTTON


        public ContentResult DeleteNewSystem(string selectedKey)
        {
            string ResultQuery = string.Empty;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                if (!string.IsNullOrEmpty(selectedKey))
                {
                    string[] rows;
                    string[] cells = null;
                    rows = selectedKey.Split(',');
                    for (int i = 0; i < rows.Length; i++)
                    {
                        cells = rows[i].Split('|');
                        //functid, string syscd, string sysrem, string sysval,  string createBy
                        string functid = cells[0];
                        string syscd = cells[1];
                        string sysrem = cells[2];
                        string sysval = cells[3];


                        if (cells != null)
                        {
                            ResultQuery = db.ExecuteScalar<string>("DeleteNewSystem", new object[] { functid, syscd, sysrem , AuthorizedUser.Username });

                        }
                    }

                } db.Close();
                ResultQuery = "Success to delete data";
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);
        }
    


        
        #endregion

    }
}



