﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.Sample;
using Toyota.Common.Web.Configuration;

namespace Portal.Controllers
{
    public class GridSampleController : BaseController
    {
        public GridSampleController(): base("Grid Sample")
        {
            Model.AddModel(new GridSampleModel());
        }

        protected override void StartUp()
        {
            Model.GetModel<GridSampleModel>().Configurations = ApplicationConfiguration.GetInstance().GetAll();
        }

        protected override void Init()
        {
            
        }

        public ActionResult UpdateGrid()
        {
            Model.GetModel<GridSampleModel>().Configurations = ApplicationConfiguration.GetInstance().GetAll();
            return PartialView("GridSamplePartial", Model);
        }
    }
}
