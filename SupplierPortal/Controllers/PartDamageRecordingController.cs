﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using Portal.Models.PartDamageRecording;
using System;

namespace Portal.Controllers
{
    public class PartDamageRecordingController : BaseController
    {
        //
        // GET: /PartDamageRecording/

        public PartDamageRecordingController() : base("LSR Recording") { }

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            Random random = new Random();
            PartDamageRecordingModel mdl = new PartDamageRecordingModel();

            //dummy for master
            for (int i = 1; i <= 20; i++)
            {
                if (i < 10)
                {
                    if (i == 1 || i == 3 || i == 5 || i == 8)
                    {
                        mdl.PartDamageRecordingDatas.Add(new PartDamageRecordingData()
                        {
                            PlantName = "Sunter 1",
                            Line = "Assy Set",
                            PartNo = "400202566"+i,
                            UniqueNo = "345"+i+"A",
                            PartName = "Bear Brake R2",
                            SupplierCode = "500"+i,
                            SupplierName = "Nusa Toyotetsu Corp.",
                            PartReceiveStatus = "Item Fall",
                            PartErrorQty = "1",
                            Status = "F0",
                            FoundedDate = i + "/12/2012",

                        });
                        Model.AddModel(mdl);
                    }
                    else
                    {
                        mdl.PartDamageRecordingDatas.Add(new PartDamageRecordingData()
                        {
                            PlantName = "Karawang",
                            Line = "Assy PxP",
                            PartNo = "400202566" + i,
                            UniqueNo = "345" + i + "A",
                            PartName = "Tire",
                            SupplierCode = "500" + i,
                            SupplierName = "3M Indonesia",
                            PartReceiveStatus = "Item Corroded",
                            PartErrorQty = "3",
                            Status = "D0",
                            FoundedDate = i + "/12/2012",

                        });
                        Model.AddModel(mdl);
                    }
                }
                else
                {
                    if (i == 11 || i == 13 || i == 15 || i == 18)
                    {
                        mdl.PartDamageRecordingDatas.Add(new PartDamageRecordingData()
                        {
                            PlantName = "Karawang",
                            Line = "Assy PxP",
                            PartNo = "400202567" + (i-10),
                            UniqueNo = "346" + (i - 10) + "A",
                            PartName = "Suspension",
                            SupplierCode = "50" + i,
                            SupplierName = "Aisin Indonesia",
                            PartReceiveStatus = "Wrong Item",
                            PartErrorQty = "2",
                            Status = "D0",
                            FoundedDate = i + "/12/2012",

                        });
                        Model.AddModel(mdl);
                    }
                    else
                    {
                        mdl.PartDamageRecordingDatas.Add(new PartDamageRecordingData()
                        {
                            PlantName = "Sunter 1",
                            Line = "Assy Set",
                            PartNo = "400202567" + (i - 10),
                            UniqueNo = "346" + (i - 10) + "A",
                            PartName = "Dashboard",
                            SupplierCode = "50" + i,
                            SupplierName = "Asalta Mandiri Agung",
                            PartReceiveStatus = "Part Damage",
                            PartErrorQty = "1",
                            Status = "F0",
                            FoundedDate = i + "/12/2012",

                        });
                        Model.AddModel(mdl);
                    }
                }
            }
        }

        public ActionResult PartDamageSubmitGV()
        {
            return PartialView("PartDamageSubmitGV", Model);
        }

        public ActionResult PartialSubmitPartDamage()
        {
            return PartialView("PartialSubmitPartDamage", Model);
        }

        public ActionResult PartDamageDownloadGV()
        {
            return PartialView("PartDamageDownloadGV", Model);
        }

        public ActionResult PartialDownloadPartDamage()
        {
            return PartialView("PartialDownloadPartDamage", Model);
        }

    }
}
