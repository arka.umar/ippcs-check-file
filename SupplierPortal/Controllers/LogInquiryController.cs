﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Html;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Log;

namespace Portal.Controllers
{
    public class LogInquiryController : MaintenanceController
    {
        public LogInquiryController(): base ("Log Inquiry")
        {
        }

        protected override void InitComponents()
        {            
            AddSearchParameter(new HtmlTextComponent("SearchProcessDate", "Process Date", typeof(DateTime)));
            AddSearchParameter(new HtmlTextComponent("SearchProcessID", "Process ID", typeof(long)));
            AddSearchParameter(new HtmlTextComponent("SearchModuleID", "Module ID", typeof(String)));
            AddSearchParameter(new HtmlTextComponent("SearchFunctionID", "Function ID", typeof(String)));
            AddSearchParameter(new HtmlTextComponent("SearchProcessStatus", "Process Status", typeof(String)));
            AddSearchParameter(new HtmlTextComponent("SearchUserID", "User ID", typeof(String)));

            SetDataGrid(new HtmlDataGridComponent("logTable", "Log Table", typeof(Log)));
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.KeyFieldName = "ProcessID";            
            grid.Columns.Add(new HtmlTextComponent("Process_id", "Process ID", typeof(long), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Function_id", "Function ID", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Module_id", "Module ID", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Process_dt", "Start Time", typeof(DateTime), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlMemoComponent("End_dt", "End Time", typeof(DateTime), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlMemoComponent("Process_sts", "Status", typeof(string), Unit.Pixel(100)));
            grid.Columns.Add(new HtmlMemoComponent("User_id", "User ID", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlMemoComponent("Remarks", "Remarks", typeof(string), Unit.Pixel(200)) { Rows = 10 });

            grid.DataSource = GetLog();
        }

        protected override void OnUpdatingSearchResult(Dictionary<string, string> requestParams)
        {
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.DataSource = GetLog();
        }
        
        protected override void PerformUpdate(Dictionary<string, string> requestParams)
        {
            //string ProcessID = requestParams.ContainsKey("Process_id") ? requestParams["Process_id"] : null;
            //string FunctionID = requestParams.ContainsKey("Function_id") ? requestParams["Function_id"] : null;
            //string ModuleID = requestParams.ContainsKey("Module_id") ? requestParams["Module_id"] : null;
            //string ProcessDate = requestParams.ContainsKey("Process_dt") ? requestParams["Process_dt"] : null;
            //string EndDate = requestParams.ContainsKey("End_dt") ? requestParams["End_dt"] : null;
            //string ProcessStatus = requestParams.ContainsKey("Process_sts") ? requestParams["Process_sts"] : null;
            //string UserID = requestParams.ContainsKey("User_id") ? requestParams["User_id"] : null;
            //string Remarks = requestParams.ContainsKey("Remarks") ? requestParams["Remarks"] : null;

            //if (!string.IsNullOrEmpty(Title) && !string.IsNullOrEmpty(Content))
            //{
            //    User user = SystemState.GetAuthorizedUser();
            //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //    db.Execute("UpdateNews", new object[] 
            //    {
            //        Title, 
            //        Content,
            //        user.Username,
            //        DateTime.Now,
            //        Id
            //    });
            //    db.Close();
            //}
        }

        protected override void PerformDelete(Dictionary<string, string> requestParams)
        {
            //string selectedKey = requestParams.ContainsKey("Key;Qualifier") ? requestParams["Key;Qualifier"] : null;
            //if (!string.IsNullOrEmpty(selectedKey))
            //{
            //    string[] keys = selectedKey.Split('|');
            //    if ((keys != null) && (keys.Length == 2))
            //    {
            //        IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //        db.Execute("DeleteConfiguration", new object[] { keys[0], keys[1] });
            //        db.Close();
            //    }
            //}
        }

        protected override void PerformInsert(Dictionary<string, string> requestParams)
        {
            //string key = requestParams.ContainsKey("Key") ? requestParams["Key"] : null;
            //string qualifier = requestParams.ContainsKey("Qualifier") ? requestParams["Qualifier"] : null;
            //string value = requestParams.ContainsKey("Value") ? requestParams["Value"] : null;
            //string textValue = requestParams.ContainsKey("TextValue") ? requestParams["TextValue"] : null;

            //if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(qualifier))
            //{
            //    return;
            //}

            //User user = SystemState.GetAuthorizedUser();
            //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            //db.Execute("InsertConfiguration", new object[] {               
            //        key,
            //        qualifier,
            //        value, 
            //        textValue,
            //        user.Username,
            //        DateTime.Now                    
            //    });
            //db.Close();
        }

        protected override void PerformBulkDelete(List<string[]> selectedKeys)
        {
            //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);            
            //foreach (string[] keys in selectedKeys)
            //{
            //    db.Execute("DeleteConfiguration", new object[] { keys[0].Trim(), keys[1].Trim() });
            //}
            //db.Close();
        }

        protected List<Log> GetLog()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<Log> LogList = new List<Log>();
            var QueryLog = db.Query<Log>("SelectAllLog");
            if (QueryLog.Any())
            {
                foreach (var q in QueryLog)
                {
                    LogList.Add(new Log()
                    {
                        Process_id = q.Process_id,
                        Module_id = q.Module_id,
                        Function_id = q.Function_id,
                        Process_dt = q.Process_dt,
                        Process_sts = q.Process_sts,
                        End_dt = q.End_dt,
                        User_id = q.User_id
                    });
                }
            }
            db.Close();
            return LogList;
        }

        protected override void OnUpdatingDetailSearchResult(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailUpdate(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailDelete(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformBulkDetailDelete(List<string[]> selectedKeys)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailInsert(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }
    }

}
