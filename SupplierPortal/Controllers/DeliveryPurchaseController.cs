﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.DeliveryPurchase;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class DeliveryPurchaseController : BaseController
    {
        public DeliveryPurchaseMaster modelExport;

        public DeliveryPurchaseController()
            : base("DeliveryPurchase", "Delivery Purchase Requisition")
           
       {

       }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            DeliveryPurchaseModel mdl = new DeliveryPurchaseModel();

            mdl.DeliveryPurchase = DeliveryPurchaseMaster();
            Model.AddModel(mdl);
            ViewData["LogisticPartnerData"] = GetAllLogisticPartners();
        }

        public List<LogisticPartner> GetAllLogisticPartners()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();
            db.Close();
            return listLogisticPartner;
        }

        protected List<DeliveryPurchaseMaster> DeliveryPurchaseMaster()
        {
            List<DeliveryPurchaseMaster> LogList = new List<DeliveryPurchaseMaster>();

            LogList.Add(new DeliveryPurchaseMaster()
            {
                PRNo="1",
                RouteConvertion="Route A",
                RouteCode = "RS28",
                RouteName = "Route Sunter 28",
                Trucking = "ALS",
                Qty="100",
                Status="PR Proposed",
                CreatedDate = "2013.09.21 18:32",
                CreatedBy = "Ondi Simamora",
                ChangeDate = "2013.09.21 18:32",
                ChangeBy = "Ondi Simamora",
            });

            LogList.Add(new DeliveryPurchaseMaster()
            {
                PRNo = "2",
                RouteConvertion = "Route A",
                RouteCode = "RS28",
                RouteName = "Route Sunter 28",
                Trucking = "ALS",
                Qty = "100",
                Status = "PR Proposed",
                CreatedDate = "2013.09.21 18:32",
                CreatedBy = "Ondi Simamora",
                ChangeDate = "2013.09.21 18:32",
                ChangeBy = "Ondi Simamora",
            });

            LogList.Add(new DeliveryPurchaseMaster()
            {
                PRNo = "3",
                RouteConvertion = "Route A",
                RouteCode = "RS28",
                RouteName = "Route Sunter 28",
                Trucking = "ALS",
                Qty = "100",
                Status = "PR Proposed",
                CreatedDate = "2013.09.21 18:32",
                CreatedBy = "Ondi Simamora",
                ChangeDate = "2013.09.21 18:32",
                ChangeBy = "Ondi Simamora",
            });

            LogList.Add(new DeliveryPurchaseMaster()
            {
                PRNo = "4",
                RouteConvertion = "Route A",
                RouteCode = "RS28",
                RouteName = "Route Sunter 28",
                Trucking = "",
                Qty = "100",
                Status = "PR Proposed",
                CreatedDate = "2013.09.21 18:32",
                CreatedBy = "Ondi Simamora",
                ChangeDate = "2013.09.21 18:32",
                ChangeBy = "Ondi Simamora",
            });

            return LogList;
        }
      

    }
}
