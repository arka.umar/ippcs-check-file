﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Portal.Models.ProblemReport;
using Portal.Models.Supplier;
using Portal.Models.Dock;
using Portal.Models.ProblemCategory;
using System.Net;
using Telerik.Reporting.Processing;
using System.Configuration;
using Telerik.Reporting.XmlSerialization;
using System.Xml;
using Ionic.Zip;
using System.IO;

namespace Portal.Controllers
{
    public class ProblemReportInquiryController : BaseController
    {
        
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
            {
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            }
            else
            {
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public ProblemReportInquiryController()
             : base("Problem Report Inquiry")
         {
             PageLayout.UseMessageBoard = true;
             CookieName = "fileDownload";
             CookiePath = "/";
         }

        #region Model properties
        List<SupplierName> _orderInquirySupplierModel = null;
        private List<SupplierName> _OrderInquirySupplierModel
        {
            get
            {
                if (_orderInquirySupplierModel == null)
                    _orderInquirySupplierModel = new List<SupplierName>();

                return _orderInquirySupplierModel;
            }
            set
            {
                _orderInquirySupplierModel = value;
            }
        }

        List<SupplierNameSPEX> _orderInquirySupplierSPEXModel = null;
        private List<SupplierNameSPEX> _OrderInquirySupplierSPEXModel
        {
            get
            {
                if (_orderInquirySupplierSPEXModel == null)
                    _orderInquirySupplierSPEXModel = new List<SupplierNameSPEX>();

                return _orderInquirySupplierSPEXModel;
            }
            set
            {
                _orderInquirySupplierSPEXModel = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }

        List<ProblemCategoryData> _problemCategoryModel = null;
        private List<ProblemCategoryData> _ProblemCategoryModel
        {
            get
            {
                if (_problemCategoryModel == null)
                    _problemCategoryModel = new List<ProblemCategoryData>();

                return _problemCategoryModel;
            }
            set
            {
                _problemCategoryModel = value;
            }
        }

        #endregion

        protected override void StartUp()
        {
            //PrintSelectionProblemReport("DOA119A24001");
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;

            ProblemReportModel mdl = new ProblemReportModel();
            Model.AddModel(mdl);

            ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");

            ProblemReportModel model = Model.GetModel<ProblemReportModel>();
            model.ProblemReportStatus = GetAllProblemReportStatus();
            ProblemReportStatus prStatus = new ProblemReportStatus();
            prStatus.SYSTEM_CD = "";
            prStatus.SYSTEM_VALUE = "--Select--";
            model.ProblemReportStatus.Insert(0, prStatus);
            ViewData["STATUS_PROBLEM"] = model.ProblemReportStatus;

        }

        #region Order Inquiry controller
        #region View controller
        public ActionResult PartialHeaderProblemReport()
        {
            Session["paramManifestNo"] = String.IsNullOrEmpty(Request.Params["ManifestNos"]) ? Session["paramManifestNo"] : Request.Params["ManifestNos"];

            #region Required model in partial page : for OrderInquiryHeaderPartial
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "ProblemReportInquiry", "OIHSupplierOption");
            _OrderInquirySupplierModel = suppliers;

            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "ProblemReportInquiry", "OIHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "ProblemReportInquiry", "OIHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "ProblemReportInquiry", "OIHDockOption");
            _OrderInquiryDockModel = DockCodes;

            List<ProblemCategoryData> ProblemCategoryData = Model.GetModel<User>().FilteringArea<ProblemCategoryData>(GetAllProblemCategory(), "PROBLEM_CATEGORY_CD", "ProblemReportInquiry", "OIHProblemCategoryOption");
            _ProblemCategoryModel = ProblemCategoryData;
            
            Model.AddModel(_OrderInquirySupplierModel);
            Model.AddModel(_OrderInquirySupplierSPEXModel);
            Model.AddModel(_OrderInquiryDockModel);
            Model.AddModel(_ProblemCategoryModel);
            #endregion

            return PartialView("ProblemReportHeaderPartial", Model);
        }

        public ActionResult PartialHeaderProblemCategoryLookupGridOrderInquiry()
        {
            #region Required model in partial page : for ProblemCategoryData GridLookup
            List<ProblemCategoryData> ProblemCategoryData = Model.GetModel<User>().FilteringArea<ProblemCategoryData>(GetAllProblemCategory(), "PROBLEM_CATEGORY_CD", "ProblemReportInquiry", "OIHProblemCategoryOption");
            TempData["GridName"] = "OIHProblemCategoryOption";
            _ProblemCategoryModel = ProblemCategoryData;
            #endregion

            return PartialView("GridLookup/PartialGrid", _ProblemCategoryModel);
        }
        
        public ActionResult PartialHeaderDockLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHDockOption GridLookup
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "ProblemReportInquiry", "OIHDockOption");
            TempData["GridName"] = "OIHDockOption";
            _OrderInquiryDockModel = DockCodes;
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }

        public ActionResult PartialHeaderSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "ProblemReportInquiry", "OIHSupplierOption");

            TempData["GridName"] = "OIHSupplierOption";
            _OrderInquirySupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquirySupplierModel);
        }

        public ActionResult PartialHeaderSubSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "ProblemReportInquiry", "OIHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "ProblemReportInquiry", "OIHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();

            TempData["GridName"] = "OIHSubSupplierOption";
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;
            #endregion

            return PartialView("GridLookup/PartialGrid", _orderInquirySupplierSPEXModel);
        }

        public ActionResult PartialDetailProblemReport()
        {
            ProblemReportModel model = Model.GetModel<ProblemReportModel>();
            try
            {
                #region Required model in partial page : for OrderInquiryEmergencyOrderDetailPartial
                model.ProblemReports = GetAllProblemReport(Request.Params["I_PROBLEM_SHEET_NO"],
                                                                Request.Params["I_PROBLEM_SHEET_ISSUANCE_FROM"],
                                                                Request.Params["I_PROBLEM_SHEET_ISSUANCE_TO"],
                                                                Request.Params["I_STATUS_PROBLEM"],
                                                                Request.Params["I_DOCK_CD"],
                                                                Request.Params["I_PROBLEM_CATEGORY"],
                                                                Request.Params["I_SUPPLIER"],
                                                                Request.Params["I_SUB_SUPPLIER"],
                                                                Request.Params["I_PART_NO"],
                                                                Request.Params["I_SUB_MANIFEST_NO"]);
                #endregion
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("ProblemReportDetailPartial", Model);
        }

        public ActionResult PopupHeaderDailyDataPartSubDetail()
        {
            string manifest = Request.Params["ManifestNo"];
            return PartialView("PartialHeaderDailyDataSubDetail", ViewData["ManifestNo"]);
        }

        public ActionResult PopupDailyDataPartSubDetail()
        {
            string manifestNo = Request.Params["ManifestNo"];
            string problemFlag = Request.Params["ProblemFlag"];
            //ViewData["partDailyData"] = DailyDataPartOrderFillGrid(manifestNo, problemFlag);
            return PartialView("DailyDataSubDetail", ViewData["partDailyData"]);
        }
        #endregion

        #region Common controller
        private String GetGeneralProductionMonth(String productionMonth)
        {
            Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
            monthDictionary.Add("01", "January");
            monthDictionary.Add("02", "February");
            monthDictionary.Add("03", "March");
            monthDictionary.Add("04", "April");
            monthDictionary.Add("05", "May");
            monthDictionary.Add("06", "June");
            monthDictionary.Add("07", "July");
            monthDictionary.Add("08", "August");
            monthDictionary.Add("09", "September");
            monthDictionary.Add("10", "October");
            monthDictionary.Add("11", "November");
            monthDictionary.Add("12", "December");

            String generalProductionMonth = "";

            if (!String.IsNullOrEmpty(productionMonth))
            {
                String year = productionMonth.Substring(0, 4);
                String month = productionMonth.Substring(productionMonth.Length - 2, 2);

                generalProductionMonth = monthDictionary[month] + " " + year;
            }

            return generalProductionMonth;
        }
        #endregion

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> orderInquirySupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierName>("GetAllSupplierSPEXNEW", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<SupplierNameSPEX> GetAllSubSupplier()
        {
            List<SupplierNameSPEX> orderInquirySupplierModel = new List<SupplierNameSPEX>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierNameSPEX>("GetAllSubSupplierSPEXNEW", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            IDBContext db = DbContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return orderInquiryDockModel;
        }

        private List<ProblemCategoryData> GetAllProblemCategory()
        {
            List<ProblemCategoryData> problemCategoryModel = new List<ProblemCategoryData>();
            IDBContext db = DbContext;
            problemCategoryModel = db.Fetch<ProblemCategoryData>("GetAllProblemCategory", new object[] { });
            db.Close();

            return problemCategoryModel;
        }


        private List<ProblemReport> GetAllProblemReport(String I_PROBLEM_SHEET_NO, String I_PROBLEM_SHEET_ISSUANCE_FROM, String I_PROBLEM_SHEET_ISSUANCE_TO, String I_STATUS_PROBLEM, string I_DOCK_CD, String I_PROBLEM_CATEGORY, String I_SUPPLIER, String I_SUB_SUPPLIER, String I_PART_NO,  String I_SUB_MANIFEST_NO)
        {
            IDBContext db = DbContext;
            string SupplierCodeLDAP = "";
            string DockCodeLDAP = "";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "ProblemReportInquiry", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();


            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "ProblemReportInquiry", "OIHDockOption");

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

         
            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            List<ProblemReport> problemReportModel = db.Fetch<ProblemReport>("GetAllProblemReportInquiry", new object[] {
                I_PROBLEM_SHEET_NO,
                I_PROBLEM_SHEET_ISSUANCE_FROM,
                I_PROBLEM_SHEET_ISSUANCE_TO,
                I_STATUS_PROBLEM,
                I_DOCK_CD,
                I_PROBLEM_CATEGORY,
                I_SUPPLIER,
                I_SUB_SUPPLIER,
                I_PART_NO,
                I_SUB_MANIFEST_NO,
                SupplierCodeLDAP,
                DockCodeLDAP
            });
            db.Close();

            return problemReportModel;
        }

        private List<ProblemReportStatus> GetAllProblemReportStatus()
        {
            IDBContext db = DbContext;
            List<ProblemReportStatus> problemReportStatusModel = db.Fetch<ProblemReportStatus>("GetAllProblemReportStatus", new object[] { });
            db.Close();
            return problemReportStatusModel;
        }


        /*
        protected List<PartDetailManifest> DailyDataPartOrderFillGrid(string manifestNo, string problemFlag)
        {
            List<PartDetailManifest> getPartDetailManifest = new List<PartDetailManifest>();
            IDBContext db = DbContext;
            getPartDetailManifest = db.Query<PartDetailManifest>("GetPartDetailManifestNEW", new object[] { manifestNo, problemFlag }).ToList<PartDetailManifest>();

            db.Close();
            return getPartDetailManifest;
        }
        */
        private void UpdateDownloadFlagNonRegular(string ManifestNo)
        {
            IDBContext db = DbContext;

            db.Execute("UpdateDownloadFlagOrderNonRegular", new Object[] { ManifestNo, AuthorizedUser.Username });
            db.Close();
        }
        #endregion
        #endregion




        //public ActionResult DownloadPDF()
        //{
        //    try
        //    {
        //        var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://10.165.8.80/Portal/ProblemReport/DailyProblemReport/FOB119A02001.pdf"));
        //        ftpRequest.Credentials = new NetworkCredential("uuspcs00", "uu$pcs00");
        //        ftpRequest.UseBinary = true;
        //        ftpRequest.UsePassive = true;
        //        ftpRequest.KeepAlive = true;
        //        ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
        //        var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
        //        var ftpStream = ftpResponse.GetResponseStream();

        //        const string contentType = "application/pdf";
        //        const string fileNameDisplayedToUser = "FileName.pdf";

        //        return File(ftpStream, contentType, fileNameDisplayedToUser);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return null;
        //}


        public void DownloadList(String I_PROBLEM_SHEET_NO, String I_PROBLEM_SHEET_ISSUANCE_FROM, String I_PROBLEM_SHEET_ISSUANCE_TO, String I_STATUS_PROBLEM, string I_DOCK_CD, String I_PROBLEM_CATEGORY, String I_SUPPLIER, String I_SUB_SUPPLIER, String I_PART_NO, String I_SUB_MANIFEST_NO)
        {
            string SupplierCodeLDAP = "";
            string SubSupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "ProblemReportInquiry", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "ProblemReportInquiry", "OIHDockOption");

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            string fileName = "DailyOrder.xls";

            IDBContext db = DbContext;
            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;


            IEnumerable<ProblemReport> qry = db.Fetch<ProblemReport>("GetAllProblemReportInquiry", new object[] {
                I_PROBLEM_SHEET_NO,
                I_PROBLEM_SHEET_ISSUANCE_FROM,
                I_PROBLEM_SHEET_ISSUANCE_TO,
                I_STATUS_PROBLEM,
                I_DOCK_CD,
                I_PROBLEM_CATEGORY,
                I_SUPPLIER,
                I_SUB_SUPPLIER,
                I_PART_NO,
                I_SUB_MANIFEST_NO,
                SupplierCodeLDAP,
                DockCodeLDAP
            });


            hasil = exporter.Write(qry, "SPEX");
            fileName = "ProblemReport.xls";

            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }



        #region DownloadPDF
        public FileContentResult DownloadPDF(string PR_CD)
        {

            PrintSelectionProblemReport(PR_CD);

            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
            Stream output = new MemoryStream();
            byte[] documentBytes;
            byte[] documentBytesKanban;
            string filePath = "";

            try
            {
                MemoryStream memoryStreamOfFile = new MemoryStream();
                FTPUpload vFtp = new FTPUpload();
                filePath = vFtp.Setting.FtpPath("ProblemReport");
                string msg = "";

                string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, filePath);
                documentBytes = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, PR_CD + ".pdf"), ref msg);
                documentBytesKanban = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, PR_CD + "_KANBAN.pdf"), ref msg);


                using (ZipFile zip = new ZipFile())
                {
                    zip.AddEntry(PR_CD + ".pdf", documentBytes);
                    zip.AddEntry(PR_CD + "_KANBAN.pdf", documentBytesKanban);
                    
                    zip.Save(memoryStreamOfFile);
                    //memoryStreamOfFile.Seek(0, SeekOrigin.Begin);
                    //memoryStreamOfFile.Flush();
                }



                return File(memoryStreamOfFile.ToArray(), "application/zip", PR_CD + ".zip");
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        private string filePath = "";
        private string SavePDFReportToFTP(
                    Telerik.Reporting.Report rpt, string FileName, string FolderName)
        {
            string msg = "";
            string LocalFileName = FileName + ".pdf";
            string ContextFTP = "";
            FileName = FileName + ".pdf";

            FTPUpload vFtp = new FTPUpload();
            filePath = vFtp.Setting.FtpPath("ProblemReport");
            ContextFTP = "problemreport";

            if (!vFtp.IsDirectoryExist(filePath, ""))
            {
                vFtp.CreateDirectory(filePath);
            }

            if (vFtp.directoryExists(filePath + "/" + FolderName))
            {
                if (!vFtp.DirectoryList(filePath + FolderName).Contains((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName))
                {
                    ReportProcessor reportProcess = new ReportProcessor();
                    RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                    bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                    rpt.Dispose();
                }
                else
                {
                    byte[] file = vFtp.FtpDownloadByte((filePath + FolderName).Split('/').Last().ToString() + "/" + FileName, ref msg);
                    if (file.Length <= 1)
                    {
                        ReportProcessor reportProcess = new ReportProcessor();
                        RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                        bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                        rpt.Dispose();
                    }

                    if (file.Length > 1)
                    {
                        if (file[0] == 0)
                        {
                            ReportProcessor reportProcess = new ReportProcessor();
                            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                            bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                            rpt.Dispose();
                        }
                    }
                }
            }
            else
            {
                ReportProcessor reportProcess = new ReportProcessor();
                RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                bool resultUpload = vFtp.FtpUpload(filePath + FolderName, FileName, result.DocumentBytes, ref msg);
                rpt.Dispose();
            }

            string serverPath = "";
            serverPath = ApplicationUrl + "/FileDownload?context=" + ContextFTP + "&path=" + FolderName + "/" + FileName;

            return serverPath;
        }



        private void PrintSelectionProblemReport(string PR_CD)
        {
            string FolderName = "";
            string reportPath = @"\Report\ProblemReport\Rep_ProblemReport.trdx";
            string sqlCommand = "SP_Rep_Problem_Report";

            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PROBLEM_SHEET", System.Data.DbType.String, PR_CD);

            Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);
            SavePDFReportToFTP(rpt, PR_CD,FolderName);



            string reportPathKanban = @"\Report\ProblemReport\Rep_SubManifest_Kanban_Spex.trdx";
            string sqlCommandKanban = "SP_Rep_SubManifest_Kanban_Problem";

            Telerik.Reporting.SqlDataSourceParameterCollection parametersKanban = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parametersKanban.Add("@SubManifestNo", System.Data.DbType.String, PR_CD);

            Telerik.Reporting.Report rptKanban = CreateReport(reportPathKanban, sqlCommandKanban, parametersKanban);
            SavePDFReportToFTP(rptKanban, PR_CD + "_KANBAN", FolderName);

            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();


            //Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();      
        }


        private Telerik.Reporting.Report CreateReport(
        string reportPath, string sqlCommand,
        Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
        Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure)
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }





    }
}
