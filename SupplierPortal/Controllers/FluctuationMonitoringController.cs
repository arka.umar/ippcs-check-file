﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Configuration;

using Portal.Models.Supplier;
using Portal.Models.FluctuationMonitoring;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace Portal.Controllers
{
    public class FluctuationMonitoringController : BaseController
    {
        //
        // GET: /FluctuationMonitoring/

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public FluctuationMonitoringController()
            : base("Fluctuation Monitoring")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            FluctuationMonitoringData FMData = new FluctuationMonitoringData();



            Model.AddModel(FMData);
        }

        public string GetMessage()
        {
            string Message = TempData["EXCEPTION"] == null ? "" : TempData["EXCEPTION"].ToString();
            TempData["EXCEPTION"] = "";
            return Message;
        }

        public ActionResult FluctuationHeaderCriteria()
        {
            FluctuationMonitoringData FMData = Model.GetModel<FluctuationMonitoringData>();
            try
            {
                FMData.FMSystem = GetFluctuationCategory();
                FMData.FMSupplier = GetAllSupplier();
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("FluctuationHeader", Model);
        }

        public ActionResult FluctuationGrid()
        {
            FluctuationMonitoringData FMData = Model.GetModel<FluctuationMonitoringData>();

            try
            {
                string isSearch = Request.Params["isSearch"] != null ? Request.Params["isSearch"].Trim() : "";

                if (isSearch == "Y")
                {
                    FMData.FMGridData = GetFluctuationData(Request.Params["PROD_MONTH"].ToString(), 
                                                           Request.Params["TIMING"].ToString(), 
                                                           Request.Params["CATEGORY"].ToString(),
                                                           Request.Params["SUPPLIER"].ToString());
                }
            }
            catch (Exception e)
            {
                TempData["EXCEPTION"] = e.Message.ToString();
            }

            return PartialView("FluctuationGridPartial", Model);
        }

        public string DownloadFluctuation(string PROD_MONTH, string TIMING, string CATEGORY, string SUPPLIER)
        {
            string message = "";

            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;
                string filename = "";

                filename = "Fluctuation_" + PROD_MONTH + ".xls";
                documentBytes = ExcelFormat(PROD_MONTH, TIMING, CATEGORY, SUPPLIER);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/octet-stream";
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", filename));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(documentBytes);
                //Response.Flush();
                Response.End();
            }
            catch (Exception e)
            {
                message = e.Message.ToString();
            }

            return message;
        }

        public byte[] ExcelFormat(string PROD_MONTH, string TIMING, string CATEGORY, string SUPPLIER)
        {
            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                HSSFWorkbook workbook = new HSSFWorkbook();

                IRow Hrow;

                IDBContext db = DbContext;

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                //ICellStyle styleContent1 = workbook.CreateCellStyle();
                //styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                //styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                //styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                //styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                //styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                //styleContent1.SetFont(Font4);

                /*Header Column (Merged)*/
                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.CENTER;
                styleContent2.SetFont(Font1);

                /*Header Column (Not-Merged)*/
                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.TOP;
                styleContent5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent5.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.SetFont(Font1);

                /*Header Column (Not-Merged-Wrapped)*/
                ICellStyle styleContent6 = workbook.CreateCellStyle();
                styleContent6.VerticalAlignment = VerticalAlignment.TOP;
                styleContent6.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent6.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent6.Alignment = HorizontalAlignment.CENTER;
                styleContent6.WrapText = true;
                styleContent6.SetFont(Font1);

                /*Data with Align Center*/
                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.CENTER;

                /*Data with Align Left*/
                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.SetFont(Font4);
                styleContent4.Alignment = HorizontalAlignment.LEFT;

                /*Data with Align Center and Background Yellow*/
                ICellStyle styleContent7 = workbook.CreateCellStyle();
                styleContent7.VerticalAlignment = VerticalAlignment.TOP;
                styleContent7.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.YELLOW.index;
                styleContent7.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent7.SetFont(Font4);
                styleContent7.Alignment = HorizontalAlignment.CENTER;

                /*Data with Align Center and Background Red*/
                ICellStyle styleContent8 = workbook.CreateCellStyle();
                styleContent8.VerticalAlignment = VerticalAlignment.TOP;
                styleContent8.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.RED.index;
                styleContent8.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent8.SetFont(Font4);
                styleContent8.Alignment = HorizontalAlignment.CENTER;

                //ICellStyle styleContent4 = workbook.CreateCellStyle();
                //styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                //styleContent4.SetFont(Font1);
                #endregion

                List<FluctuationData> datas = new List<FluctuationData>();
                datas = GetFluctuationData(PROD_MONTH, TIMING, CATEGORY, SUPPLIER);

                ISheet sheet;

                sheet = workbook.CreateSheet("Fluctuation_Data");

                DateTime parseMonth;
                String monthName = "";

                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue("Supplier CD.");
                Hrow.CreateCell(1).SetCellValue("Supplier Name");
                Hrow.CreateCell(2).SetCellValue("Part No.");
                Hrow.CreateCell(3).SetCellValue("Part Name");
                Hrow.CreateCell(4).SetCellValue("Dock");

                parseMonth = DateTime.ParseExact(PROD_MONTH, "yyyyMM", System.Globalization.CultureInfo.InvariantCulture);
                monthName = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetMonthName(parseMonth.Month);
                Hrow.CreateCell(5).SetCellValue(monthName.Substring(0, 3) + " '" + parseMonth.Year.ToString().Substring(2, 2) + " (N)");
                Hrow.CreateCell(6).SetCellValue("");
                Hrow.CreateCell(7).SetCellValue("");
                Hrow.CreateCell(8).SetCellValue("");
                Hrow.CreateCell(9).SetCellValue("");

                parseMonth = DateTime.ParseExact(PROD_MONTH, "yyyyMM", System.Globalization.CultureInfo.InvariantCulture).AddMonths(1);
                monthName = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetMonthName(parseMonth.Month);
                Hrow.CreateCell(10).SetCellValue(monthName.Substring(0, 3) + " '" + parseMonth.Year.ToString().Substring(2, 2) + " (N + 1)");
                Hrow.CreateCell(11).SetCellValue("");
                Hrow.CreateCell(12).SetCellValue("");
                Hrow.CreateCell(13).SetCellValue("");
                Hrow.CreateCell(14).SetCellValue("");

                parseMonth = DateTime.ParseExact(PROD_MONTH, "yyyyMM", System.Globalization.CultureInfo.InvariantCulture).AddMonths(2);
                monthName = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetMonthName(parseMonth.Month);
                Hrow.CreateCell(15).SetCellValue(monthName.Substring(0, 3) + " '" + parseMonth.Year.ToString().Substring(2, 2) + " (N + 2)");
                Hrow.CreateCell(16).SetCellValue("");
                Hrow.CreateCell(17).SetCellValue("");
                Hrow.CreateCell(18).SetCellValue("");
                Hrow.CreateCell(19).SetCellValue("");

                parseMonth = DateTime.ParseExact(PROD_MONTH, "yyyyMM", System.Globalization.CultureInfo.InvariantCulture).AddMonths(3);
                monthName = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetMonthName(parseMonth.Month);
                Hrow.CreateCell(20).SetCellValue(monthName.Substring(0, 3) + " '" + parseMonth.Year.ToString().Substring(2, 2) + " (N + 3)");
                Hrow.CreateCell(21).SetCellValue("");
                Hrow.CreateCell(22).SetCellValue("");
                Hrow.CreateCell(23).SetCellValue("");
                Hrow.CreateCell(24).SetCellValue("");

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(21).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent2;
                Hrow.GetCell(23).CellStyle = styleContent2;
                Hrow.GetCell(24).CellStyle = styleContent2;

                Hrow = sheet.CreateRow(1);
                Hrow.CreateCell(0).SetCellValue("");
                Hrow.CreateCell(1).SetCellValue("");
                Hrow.CreateCell(2).SetCellValue("");
                Hrow.CreateCell(3).SetCellValue("");
                Hrow.CreateCell(4).SetCellValue("");
                Hrow.CreateCell(5).SetCellValue("Qty");
                Hrow.CreateCell(6).SetCellValue("F/A Firm");
                Hrow.CreateCell(7).SetCellValue("Fluct. Category");
                Hrow.CreateCell(8).SetCellValue("F/A Tentative");
                Hrow.CreateCell(9).SetCellValue("Fluct. Category");
                Hrow.CreateCell(10).SetCellValue("Qty");
                Hrow.CreateCell(11).SetCellValue("F/A Firm");
                Hrow.CreateCell(12).SetCellValue("Fluct. Category");
                Hrow.CreateCell(13).SetCellValue("F/A Tentative");
                Hrow.CreateCell(14).SetCellValue("Fluct. Category");
                Hrow.CreateCell(15).SetCellValue("Qty");
                Hrow.CreateCell(16).SetCellValue("F/A Firm");
                Hrow.CreateCell(17).SetCellValue("Fluct. Category");
                Hrow.CreateCell(18).SetCellValue("F/A Tentative");
                Hrow.CreateCell(19).SetCellValue("Fluct. Category");
                Hrow.CreateCell(20).SetCellValue("Qty");
                Hrow.CreateCell(21).SetCellValue("F/A Firm");
                Hrow.CreateCell(22).SetCellValue("Fluct. Category");
                Hrow.CreateCell(23).SetCellValue("F/A Tentative");
                Hrow.CreateCell(24).SetCellValue("Fluct. Category");

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(21).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent2;
                Hrow.GetCell(23).CellStyle = styleContent2;
                Hrow.GetCell(24).CellStyle = styleContent2;

                sheet.AddMergedRegion(new CellRangeAddress(0, 1, 0, 0));
                sheet.AddMergedRegion(new CellRangeAddress(0, 1, 1, 1));
                sheet.AddMergedRegion(new CellRangeAddress(0, 1, 2, 2));
                sheet.AddMergedRegion(new CellRangeAddress(0, 1, 3, 3));
                sheet.AddMergedRegion(new CellRangeAddress(0, 1, 4, 4));
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 5, 9));
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 10, 14));
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 15, 19));
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 20, 24));

                int row = 2;
                int maxCol0Length = ("Supplier CD.").Length;
                int maxCol1Length = ("Supplier Name").Length;
                int maxCol2Length = ("Part No.").Length;
                int maxCol3Length = ("Part Name").Length;
                int maxCol4Length = ("Dock").Length;
                //int maxCol5Length = 10;
                //int maxCol6Length = ("Fluctuation").Length;
                //int maxCol7Length = ("Fluct. Category").Length;
                //int maxCol8Length = maxCol5Length;
                //int maxCol9Length = maxCol6Length;
                //int maxCol10Length = maxCol7Length;
                //int maxCol11Length = maxCol5Length;
                //int maxCol12Length = maxCol6Length;
                //int maxCol13Length = maxCol7Length;
                //int maxCol14Length = maxCol5Length;
                //int maxCol15Length = maxCol6Length;
                //int maxCol16Length = maxCol7Length;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(data.SUPPLIER_CD != null ? data.SUPPLIER_CD.Trim() : "");
                    Hrow.CreateCell(1).SetCellValue(data.SUPPLIER_NAME != null ? data.SUPPLIER_NAME.Trim() : "");
                    Hrow.CreateCell(2).SetCellValue(data.PART_NO != null ? data.PART_NO.Trim() : "");
                    Hrow.CreateCell(3).SetCellValue(data.PART_NAME != null ? data.PART_NAME.Trim() : "");
                    Hrow.CreateCell(4).SetCellValue(data.DOCK_CD != null ? data.DOCK_CD.Trim() : "");
                    Hrow.CreateCell(5).SetCellValue(data.N_VOLUME != null ? data.N_VOLUME.Trim() : "");
                    Hrow.CreateCell(6).SetCellValue(data.N_FA != null ? data.N_FA.Trim() : "");
                    Hrow.CreateCell(7).SetCellValue(data.N_FLUCTUATION != null ? data.N_FLUCTUATION.Trim() : "");
                    Hrow.CreateCell(8).SetCellValue(data.T_N_FA != null ? data.T_N_FA.Trim() : "");
                    Hrow.CreateCell(9).SetCellValue(data.T_N_FLUCTUATION != null ? data.T_N_FLUCTUATION.Trim() : "");
                    Hrow.CreateCell(10).SetCellValue(data.N_1_VOLUME != null ? data.N_1_VOLUME.Trim() : "");
                    Hrow.CreateCell(11).SetCellValue(data.N_1_FA != null ? data.N_1_FA.Trim() : "");
                    Hrow.CreateCell(12).SetCellValue(data.N_1_FLUCTUATION != null ? data.N_1_FLUCTUATION.Trim() : "");
                    Hrow.CreateCell(13).SetCellValue(data.T_N_1_FA != null ? data.T_N_1_FA.Trim() : "");
                    Hrow.CreateCell(14).SetCellValue(data.T_N_1_FLUCTUATION != null ? data.T_N_1_FLUCTUATION.Trim() : "");
                    Hrow.CreateCell(15).SetCellValue(data.N_2_VOLUME != null ? data.N_2_VOLUME.Trim() : "");
                    Hrow.CreateCell(16).SetCellValue(data.N_2_FA != null ? data.N_2_FA.Trim() : "");
                    Hrow.CreateCell(17).SetCellValue(data.N_2_FLUCTUATION != null ? data.N_2_FLUCTUATION.Trim() : "");
                    Hrow.CreateCell(18).SetCellValue(data.T_N_2_FA != null ? data.T_N_2_FA.Trim() : "");
                    Hrow.CreateCell(19).SetCellValue(data.T_N_2_FLUCTUATION != null ? data.T_N_2_FLUCTUATION.Trim() : "");
                    Hrow.CreateCell(20).SetCellValue(data.N_3_VOLUME != null ? data.N_3_VOLUME.Trim() : "");
                    Hrow.CreateCell(21).SetCellValue(data.N_3_FA != null ? data.N_3_FA.Trim() : "");
                    Hrow.CreateCell(22).SetCellValue(data.N_3_FLUCTUATION != null ? data.N_3_FLUCTUATION.Trim() : "");
                    Hrow.CreateCell(23).SetCellValue(data.T_N_3_FA != null ? data.T_N_3_FA.Trim() : "");
                    Hrow.CreateCell(24).SetCellValue(data.T_N_3_FLUCTUATION != null ? data.T_N_3_FLUCTUATION.Trim() : "");

                    maxCol0Length = data.SUPPLIER_CD != null ? (data.SUPPLIER_CD.Trim().Length > maxCol0Length ? data.SUPPLIER_CD.Trim().Length : maxCol0Length) : maxCol0Length;
                    maxCol1Length = data.SUPPLIER_NAME != null ? (data.SUPPLIER_NAME.Trim().Length > maxCol1Length ? data.SUPPLIER_NAME.Trim().Length : maxCol1Length) : maxCol1Length;
                    maxCol2Length = data.PART_NO != null ? (data.PART_NO.Trim().Length > maxCol2Length ? data.PART_NO.Trim().Length : maxCol2Length) : maxCol2Length;
                    maxCol3Length = data.PART_NAME != null ? (data.PART_NAME.Trim().Length > maxCol3Length ? data.PART_NAME.Trim().Length : maxCol3Length) : maxCol3Length;
                    maxCol4Length = data.DOCK_CD != null ? (data.DOCK_CD.Trim().Length > maxCol4Length ? data.DOCK_CD.Trim().Length : maxCol4Length) : maxCol4Length;

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent4;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent4;
                    Hrow.GetCell(4).CellStyle = styleContent3;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = data.N_FLUCTUATION == null ? styleContent3 : (data.N_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(7).CellStyle = data.N_FLUCTUATION == null ? styleContent3 : (data.N_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);
                    Hrow.GetCell(8).CellStyle = data.T_N_FLUCTUATION == null ? styleContent3 : (data.T_N_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(9).CellStyle = data.T_N_FLUCTUATION == null ? styleContent3 : (data.T_N_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);
                    Hrow.GetCell(10).CellStyle = styleContent3;
                    Hrow.GetCell(11).CellStyle = data.N_1_FLUCTUATION == null ? styleContent3 : (data.N_1_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(12).CellStyle = data.N_1_FLUCTUATION == null ? styleContent3 : (data.N_1_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);
                    Hrow.GetCell(13).CellStyle = data.T_N_1_FLUCTUATION == null ? styleContent3 : (data.T_N_1_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(14).CellStyle = data.T_N_1_FLUCTUATION == null ? styleContent3 : (data.T_N_1_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);
                    Hrow.GetCell(15).CellStyle = styleContent3;
                    Hrow.GetCell(16).CellStyle = data.N_2_FLUCTUATION == null ? styleContent3 : (data.N_2_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(17).CellStyle = data.N_2_FLUCTUATION == null ? styleContent3 : (data.N_2_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);
                    Hrow.GetCell(18).CellStyle = data.T_N_2_FLUCTUATION == null ? styleContent3 : (data.T_N_2_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(19).CellStyle = data.T_N_2_FLUCTUATION == null ? styleContent3 : (data.T_N_2_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);
                    Hrow.GetCell(20).CellStyle = styleContent3;
                    Hrow.GetCell(21).CellStyle = data.N_3_FLUCTUATION == null ? styleContent3 : (data.N_3_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(22).CellStyle = data.N_3_FLUCTUATION == null ? styleContent3 : (data.N_3_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);
                    Hrow.GetCell(23).CellStyle = data.T_N_3_FLUCTUATION == null ? styleContent3 : (data.T_N_3_FLUCTUATION.ToString() == "HIGH" ? styleContent7 : styleContent3);
                    Hrow.GetCell(24).CellStyle = data.T_N_3_FLUCTUATION == null ? styleContent3 : (data.T_N_3_FLUCTUATION.ToString() == "HIGH" ? styleContent8 : styleContent3);

                    row++;
                }

                sheet.SetColumnWidth(0, ConvertExcelWidth(maxCol0Length <= 0 ? 11.71 : ExcelWidthByContentLength(maxCol0Length)));
                sheet.SetColumnWidth(1, ConvertExcelWidth(maxCol1Length <= 0 ? 44.43 : ExcelWidthByContentLength(maxCol1Length)));
                sheet.SetColumnWidth(2, ConvertExcelWidth(maxCol2Length <= 0 ? 13.86 : ExcelWidthByContentLength(maxCol2Length)));
                sheet.SetColumnWidth(3, ConvertExcelWidth(maxCol3Length <= 0 ? 49.00 : ExcelWidthByContentLength(maxCol3Length)));
                sheet.SetColumnWidth(4, ConvertExcelWidth(maxCol4Length <= 0 ? 11.29 : ExcelWidthByContentLength(maxCol4Length)));
                sheet.SetColumnWidth(5, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(6, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(7, ConvertExcelWidth(14.14));
                sheet.SetColumnWidth(8, ConvertExcelWidth(10.43));
                sheet.SetColumnWidth(9, ConvertExcelWidth(14.14));
                sheet.SetColumnWidth(10, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(11, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(12, ConvertExcelWidth(14.14));
                sheet.SetColumnWidth(13, ConvertExcelWidth(10.43));
                sheet.SetColumnWidth(14, ConvertExcelWidth(14.14));
                sheet.SetColumnWidth(15, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(16, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(17, ConvertExcelWidth(14.14));
                sheet.SetColumnWidth(18, ConvertExcelWidth(10.43));
                sheet.SetColumnWidth(19, ConvertExcelWidth(14.14));
                sheet.SetColumnWidth(20, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(21, ConvertExcelWidth(9.43));
                sheet.SetColumnWidth(22, ConvertExcelWidth(14.14));
                sheet.SetColumnWidth(23, ConvertExcelWidth(10.43));
                sheet.SetColumnWidth(24, ConvertExcelWidth(14.14));

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                return ms.ToArray();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int ConvertExcelWidth(double ExcelWidth)
        {
            const double Widther = 0.00314;
            int NpoiWidth = Convert.ToInt32(Math.Ceiling(ExcelWidth/Widther));

            return NpoiWidth;
        }

        public int ExcelWidthByContentLength(int CharLength)
        {
            const double Lengther = 1.2;
            int ExcelWidth = Convert.ToInt32(Math.Ceiling(CharLength * Lengther));

            return ExcelWidth;
        }

        public List<FluctuationData> GetFluctuationData(string ProdMonth, string Timing, string Category, string Supplier)
        {
            List<FluctuationData> FMGrid = new List<FluctuationData>();
            IDBContext db = DbContext;

            try
            {
                FMGrid = db.Fetch<FluctuationData>("FluctuationMonitoring_GetAllFluctuationData", new object[] { ProdMonth, Timing, Category, Supplier });
            }
            catch (Exception e)
            {
                throw e;
            }
            finally 
            {
                db.Close();
            }

            return FMGrid;
        }

        public List<FluctuationSystemMapping> GetFluctuationCategory()
        {
            List<FluctuationSystemMapping> sysData = new List<FluctuationSystemMapping>();
            IDBContext db = DbContext;

            try
            {
                sysData = db.Fetch<FluctuationSystemMapping>("FluctuationMonitoring_GetCategory");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            return sysData;
        }

        public List<SupplierName> GetAllSupplier()
        {
            IDBContext db = DbContext;

            List<SupplierName> supplierFeedbackSummarySupplierModelList = new List<SupplierName>();
            supplierFeedbackSummarySupplierModelList = db.Fetch<SupplierName>("GetAllSupplierForecast", new object[] { "" });
            db.Close();

            return supplierFeedbackSummarySupplierModelList;
        }

        public ActionResult PartialSupplier()
        {
            #region Required model in partial page : for SFSHSupplierIDOption GridLookup
            FluctuationMonitoringData FMData = Model.GetModel<FluctuationMonitoringData>();
            TempData["GridName"] = "FMSupplierGrid";
            FMData.FMSupplier = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", FMData.FMSupplier);
        }
    }
}
