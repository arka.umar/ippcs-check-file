﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Web.UI.WebControls;
using Toyota.Common.Web.MVC;
using Portal.Models.DCLInquiry;
using Portal.Models.DCLReport;
using Portal.Models.Globals;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Telerik.Reporting.Processing;
using System.Data;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.Mvc;
using System.Threading;
using Toyota.Common.Web.Util.Configuration;
using System.IO;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Credential;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using Telerik.Reporting;
//using NPOI.XSSF.UserModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Portal.Controllers
{
    public class DCLReportController : BaseController
    {
        public DCLReportController()
            : base("Report")
        {
            PageLayout.UseMessageBoard = true;
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        List<LogisticPartner> _logisticPartnerModel = null;
        private List<LogisticPartner> _LogisticPartnerModel
        {
            get
            {
                if (_logisticPartnerModel == null)
                    _logisticPartnerModel = new List<LogisticPartner>();

                return _logisticPartnerModel;
            }
            set
            {
                _logisticPartnerModel = value;
            }
        }

        List<Supplier> _supplierModel = null;
        private List<Supplier> _SupplierModel
        {
            get
            {
                if (_supplierModel == null)
                    _supplierModel = new List<Supplier>();

                return _supplierModel;
            }
            set
            {
                _supplierModel = value;
            }
        }

        List<DeliveryType> _deliveryTypeModel = null;
        private List<DeliveryType> _DeliveryTypeModel
        {
            get
            {
                if (_deliveryTypeModel == null)
                    _deliveryTypeModel = new List<DeliveryType>();

                return _deliveryTypeModel;
            }
            set
            {
                _deliveryTypeModel = value;
            }
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = false;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryDock1 = db.Query<DockPlant>("GetAllDockAndPlant");
            ViewData["DockCode"] = QueryDock1;

            //var QueryDock3 = db.Query<Supplier>("GetAllSupplier");
            //ViewData["SupplierCode"] = QueryDock3;

            List<DeliveryType> listType = Model.GetModel<User>().FilteringArea<DeliveryType>(GetAllDeliveryType(), "DeliveryTypeName", "DCLReport", "cmbDeliveryType");

            if (listType.Count > 0)
            {
                _DeliveryTypeModel = listType;
            }
            else
            {
                _DeliveryTypeModel = GetAllDeliveryType();
            }

            ViewData["DeliveryType"] = _DeliveryTypeModel;

            TempData["GridName"] = "grlSupplier";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SupplierCode", "DCLReport", "grlSupplier");

            if (suppliers.Count > 0)
            {
                _SupplierModel = suppliers;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }
            ViewData["SupplierCode"] = _SupplierModel;

            //List<LogisticPartner> listLP = new List<LogisticPartner>();
            //var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            //listLP = QueryLog.ToList<LogisticPartner>();
            //ViewData["LogisticPartnerData"] = listLP;

            TempData["GridName"] = "grlLPHeader";
            List<LogisticPartner> listLP = Model.GetModel<User>().FilteringArea<LogisticPartner>(GetAllLogisticPartner(), "LPCode", "DCLReport", "grlLPHeader");

            if (listLP.Count > 0)
            {
                _LogisticPartnerModel = listLP;
            }
            else
            {
                _LogisticPartnerModel = GetAllLogisticPartner();
            }

            ViewData["LogisticPartnerData"] = _LogisticPartnerModel;

            //ViewData["RepDateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            //ViewData["RepDateTo"] = DateTime.Now.ToString("dd.MM.yyyy");

            ViewData["MonthNow"] = DateTime.Now.ToString("MMMM yyyy");

            db.Close();
        }

        private List<DeliveryType> GetAllDeliveryType()
        {
            List<DeliveryType> dlt = new List<DeliveryType>();

            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Monthly Summary Delivery by LP"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Monthly Summary Delivery by Route"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Monthly Summary Delivery Detail"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Monthly Route Planning by LP"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Monthly Route Planning by Supplier"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Okamochi Title"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "P-Lane Content"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Truck Sheet"
            });
            //dlt.Add(new DeliveryType()
            //{
            //    DeliveryTypeName = "Empty Job Card"
            //});
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Delivery Control List"
            });
            dlt.Add(new DeliveryType()
            {
                DeliveryTypeName = "Change Over Report"
            });
            return dlt;
        }

        public ActionResult DeliveryTypePartial()
        {
            ViewData["DeliveryType"] = GetAllDeliveryType();
            return PartialView("DeliveryTypePartial", ViewData["DeliveryType"]);
        }

        public ActionResult DockCodePartial()
        {
            TempData["GridName"] = "grlDock";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryDock = db.Query<DockPlant>("GetAllDockAndPlant");
            ViewData["DockCode"] = QueryDock;
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["DockCode"]);
        }

        public ActionResult PlantCodePartial()
        {
            TempData["GridName"] = "grlPlant";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryDock = db.Query<Plant>("GetAllPlant");
            ViewData["PlantCode"] = QueryDock;
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["PlantCode"]);
        }

        public ActionResult SupplierCodePartial()
        {
            //TempData["GridName"] = "grlSupplier";
            //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            //var QueryDock = db.Query<Supplier>("GetAllSupplier");
            //ViewData["SupplierCodePlant"] = QueryDock;
            //db.Close();

            TempData["GridName"] = "grlSupplier";
            List<Supplier> suppliers = Model.GetModel<User>().FilteringArea<Supplier>(GetAllSupplier(), "SupplierCode", "DCLReport", "grlSupplier");

            if (suppliers.Count > 0)
            {
                _SupplierModel = suppliers;
            }
            else
            {
                _SupplierModel = GetAllSupplier();
            }
            ViewData["SupplierCode"] = _SupplierModel;

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierCode"]);
        }

        public ActionResult RouteReportPartial()
        {
            TempData["GridName"] = "grlRouteReport";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryDock = db.Query<RouteRate>("GetDCLRouteReport");
            ViewData["Routerate"] = QueryDock;
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["Routerate"]);
        }

        private List<LogisticPartner> GetAllLogisticPartner()
        {
            List<LogisticPartner> logisticPartnerModel = new List<LogisticPartner>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                logisticPartnerModel = db.Fetch<LogisticPartner>("GetAllLogisticPartner");
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return logisticPartnerModel;
        }

        private List<Supplier> GetAllSupplier()
        {
            List<Supplier> supplierModel = new List<Supplier>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                supplierModel = db.Fetch<Supplier>("GetAllSupplier");
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return supplierModel;
        }

        //public List<LogisticPartner> GetAllLogisticPartners()
        //{
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
        //    var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
        //    listLogisticPartner = QueryLog.ToList<LogisticPartner>();
        //    db.Close();
        //    return listLogisticPartner;
        //}



        public ActionResult HeaderDCLReport()
        {
            return PartialView("HeaderDCLReport", Model);
        }

        #region SWITCH CASE DELIVERY PRINT/ DOWNLOAD BY DELIVERY RYD
        //public void PrintReport(string DeliveryMonth, string CurrentPhaseFrom, string CurrentPhaseTo, string NextPhaseFrom, string NextPhaseTo, string LPCode, string SupplierCode, string DockCode, string DeliveryType)
        public void PrintReport(string DeliveryMonth, string ProductionMonth, string LPCode, string SupplierCode, string DockCode,
                                string DeliveryType, string ProductionDateFrom, string ProductionDateTo, string RepDay1anext,
                                string RepDay1bnext, string RepDay2anext, string RepDay2bnext, string RepDay1acurrent,
                                string RepDay1bcurrent, string RepDay2acurrent, string RepDay2bcurrent, string ExportType, string Style)
        {
            ICompression compress = ZipCompress.GetInstance();
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();

            Stream output = new MemoryStream();
            byte[] documentBytes;
            string fileName = "";
            string zipName = "";
            string[] Suppliers;
            //string res = "";
            Suppliers = SupplierCode.Split(';');

            if ((DeliveryType != "Okamochi Title") || (DeliveryType != "Truck Sheet") || (DeliveryType != "P-Lane Content") || (DeliveryType != "Empty Job Card"))
            {
                string[] Docks;
                Docks = DockCode.Split(';');
            }

            string[] LPs;
            LPs = LPCode.Split(';');

            string[] pmonth = ProductionMonth.Split(' ');

            string PhaseParam1 = pmonth[1].ToString();
            string PhaseParam2 = formatingNameDate(pmonth[0].ToString());

            DateTime dtlastdate = DateTime.Parse(PhaseParam1 + "-" + PhaseParam2 + "-01").AddMonths(1).AddDays(-1);

            switch (DeliveryType)
            {
                case "Monthly Summary Delivery by LP":
                    //for (int x = 0; x < Docks.Length; x++)
                    //{
                    //    fileName = "SummaryByLP_Dock " + Docks[x].ToString() + ".xls";
                    //    zipName = "Monthly Summary Delivery by LP.zip";
                    //    documentBytes = PrintDCLReport_SummaryLP(DeliveryMonth, PhaseParam1 + "-" + PhaseParam2 + "-01", String.Format("{0:yyyy-MM-dd}",dtlastdate), Docks[x]);
                    //    listCompress.Add(fileName, documentBytes);
                    //}
                    fileName = "SummaryByLP_Dock " + DockCode.ToString() + ".xls";
                    zipName = "Monthly Summary Delivery by LP.zip";
                    documentBytes = PrintDCLReport_SummaryLP(DeliveryMonth, PhaseParam1 + "-" + PhaseParam2 + "-01", String.Format("{0:yyyy-MM-dd}", dtlastdate), DockCode.ToString());
                    listCompress.Add(fileName, documentBytes);
                    break;
                case "Monthly Summary Delivery by Route":
                    //for (int x = 0; x < Docks.Length; x++)
                    //{
                    //    fileName = "MonthlySummaryByRoute_Dock " + Docks[x].ToString() + ".xls";
                    //    zipName = "Monthly Summary Delivery by Route.zip";
                    //    documentBytes = PrintDCLReport_SummaryRoute(DeliveryMonth, PhaseParam1 + "-" + PhaseParam2 + "-01", String.Format("{0:yyyy-MM-dd}", dtlastdate), Docks[x]);
                    //    listCompress.Add(fileName, documentBytes);
                    //}
                    fileName = "MonthlySummaryByRoute_Dock " + DockCode.ToString() + ".xls";
                    zipName = "Monthly Summary Delivery by Route.zip";
                    documentBytes = PrintDCLReport_SummaryRoute(DeliveryMonth, PhaseParam1 + "-" + PhaseParam2 + "-01", String.Format("{0:yyyy-MM-dd}", dtlastdate), DockCode);
                    listCompress.Add(fileName, documentBytes);
                    break;
                case "Monthly Summary Delivery Detail":
                    //for (int x = 0; x < Docks.Length; x++)
                    //{
                    //    fileName = "MonthlySummaryDetail_Dock " + Docks[x].ToString() + ".xls";
                    //    zipName = "Monthly Summary Delivery Detail.zip";
                    //    documentBytes = PrintDCLReport_SummaryDetail(DeliveryMonth, PhaseParam1 + "-" + PhaseParam2 + "-01", String.Format("{0:yyyy-MM-dd}", dtlastdate), Docks[x]);
                    //    listCompress.Add(fileName, documentBytes);
                    //}
                    fileName = "MonthlySummaryDetail_Dock " + DockCode.ToString() + ".xls";
                    zipName = "Monthly Summary Delivery Detail.zip";
                    documentBytes = PrintDCLReport_SummaryDetail(DeliveryMonth, PhaseParam1 + "-" + PhaseParam2 + "-01", String.Format("{0:yyyy-MM-dd}", dtlastdate), DockCode);
                    listCompress.Add(fileName, documentBytes);
                    break;
                case "Monthly Route Planning by LP":
                    for (int x = 0; x < LPs.Length; x++)
                    {
                        fileName = "LP_" + LPs[x].ToString() + ((ExportType == "P") ? ".pdf" : ".xls");
                        zipName = "Monthly Route Planning by LP.zip";
                        documentBytes = PrintDCLReport_byLP(RepDay1anext, RepDay1bnext, RepDay2anext, RepDay2bnext, RepDay1acurrent, RepDay1bcurrent, RepDay2acurrent, RepDay2bcurrent, LPs[x], SupplierCode, ProductionDateFrom, ProductionDateTo, ExportType);
                        listCompress.Add(fileName, documentBytes);
                    }
                    break;
                case "Monthly Route Planning by Supplier":
                    for (int x = 0; x < Suppliers.Length; x++)
                    {
                        fileName = "Supplier_" + Suppliers[x] + ((ExportType == "P") ? ".pdf" : ".xls");
                        zipName = "Monthly Route Planning by Supplier.zip";
                        documentBytes = PrintDCLReport(RepDay1anext, RepDay1bnext, RepDay2anext, RepDay2bnext, RepDay1acurrent, RepDay1bcurrent, RepDay2acurrent, RepDay2bcurrent, LPCode, Suppliers[x], ProductionDateFrom, ProductionDateTo, ExportType);
                        listCompress.Add(fileName, documentBytes);
                    }
                    break;
                case "Okamochi Title":
                    string style = "";

                    if (Style == "A")
                    { style = "Side_A"; }
                    else if (Style == "B")
                    { style = "Side_B"; }
                    else { style = "Both"; }

                    string[] okamochi_docks;
                    okamochi_docks = DockCode.Split(';');

                    //added for formating date
                    DateTime MyDateFrom;
                    MyDateFrom = new DateTime();
                    MyDateFrom = DateTime.ParseExact(ProductionDateFrom, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);
                    DateTime MyDateTo;
                    MyDateTo = new DateTime();
                    MyDateTo = DateTime.ParseExact(ProductionDateTo, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);
                    for (int x = 0; x < okamochi_docks.Length; x++)
                    {
                        fileName = "Okamochi_" + okamochi_docks[x] + "_" + MyDateFrom.ToString("yyyyMMdd") + "_" + MyDateTo.ToString("yyyyMMdd") + "_" + style + ".xls";
                        zipName = "Okamochi_" + MyDateFrom.ToString("yyyyMMdd") + "_" + MyDateTo.ToString("yyyyMMdd") + "_" + style + ".zip";
                        documentBytes = PrintOkamochi(okamochi_docks[x], ProductionDateFrom, ProductionDateTo, Style, RepDay1anext, RepDay1bnext);
                        listCompress.Add(fileName, documentBytes);
                    }
                    break;
                //change over
                case "Change Over Report":

                    //string[] CO_docks;
                    //CO_docks = DockCode.Split(';');

                    //added for formating date
                    //DateTime CO_MyDateFrom;
                    //MyDateFrom = new DateTime();
                    //MyDateFrom = DateTime.ParseExact(ProductionDateFrom, "MM/dd/yyyy",
                    //                                System.Globalization.CultureInfo.InvariantCulture);
                    //DateTime CO_MyDateTo;
                    //MyDateTo = new DateTime();
                    //MyDateTo = DateTime.ParseExact(ProductionDateTo, "MM/dd/yyyy",
                    //                                 System.Globalization.CultureInfo.InvariantCulture);

                    MyDateTo = new DateTime();
                    MyDateTo = DateTime.ParseExact(RepDay1anext, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);

                    fileName = "Change_Over_" + MyDateTo.ToString("yyyyMMdd") + ".xls";
                    zipName = "Change_Over_" + MyDateTo.ToString("yyyyMMdd") + ".zip";
                    documentBytes = PrintChangeOver(DockCode, RepDay1anext, RepDay1acurrent);
                    listCompress.Add(fileName, documentBytes);

                    /*for (int x = 0; x < CO_docks.Length; x++)
                    {
                        //fileName = "Change_Over_" + CO_docks[x] + "_" + MyDateFrom.ToString("yyyyMMdd") + "_" + MyDateTo.ToString("yyyyMMdd") +".xls";
                        //zipName = "Change_Over_" + MyDateFrom.ToString("yyyyMMdd") + "_" + MyDateTo.ToString("yyyyMMdd") + ".zip";
                        fileName = "Change_Over_" + CO_docks[x] + "_" + MyDateTo.ToString("yyyyMMdd") + ".xls";
                        zipName = "Change_Over_" + MyDateTo.ToString("yyyyMMdd") + ".zip";
                        documentBytes = PrintChangeOver(CO_docks[x], /*ProductionDateFrom, ProductionDateTo, RepDay1anext);
                        listCompress.Add(fileName, documentBytes);
                    }*/
                    break;


                case "Truck Sheet":
                    //added format date
                    DateTime MyDateFromTS;
                    MyDateFromTS = new DateTime();
                    MyDateFromTS = DateTime.ParseExact(ProductionDateFrom, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);

                    DateTime MyDateToTS;
                    MyDateToTS = new DateTime();
                    MyDateToTS = DateTime.ParseExact(ProductionDateTo, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);


                    string p_dock = "";
                    string[] p_docks;
                    IDBContext db = DbContext;
                    List<TruckSheet> modelTruck = new List<TruckSheet>();
                    modelTruck = DbContext.Fetch<TruckSheet>("GetTruckDock", new object[] { DockCode }); // select header
                    foreach (var t in modelTruck)
                    {
                        p_dock = t.PDock;
                    }
                    p_docks = p_dock.Split(',');
                    for (int x = 0; x < p_docks.Length; x++)
                    {
                        fileName = "Truck_Sheet_" + p_docks[x] + "_" + MyDateFromTS.ToString("yyyyMMdd") + "_" + MyDateToTS.ToString("yyyyMMdd") + ".xls";
                        zipName = "TruckSheet" + "_" + MyDateFromTS.ToString("yyyyMMdd") + "_" + MyDateToTS.ToString("yyyyMMdd") + ".zip";
                        documentBytes = PrintTruckSheet(p_docks[x], ProductionDateFrom, ProductionDateTo, RepDay1anext, RepDay1bnext);
                        listCompress.Add(fileName, documentBytes);
                    }
                    break;
                case "P-Lane Content":
                    DateTime MyDateFromP;
                    MyDateFromP = new DateTime();
                    MyDateFromP = DateTime.ParseExact(ProductionDateFrom, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);

                    DateTime MyDateToP;
                    MyDateToP = new DateTime();
                    MyDateToP = DateTime.ParseExact(ProductionDateTo, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);

                    string[] plane_docks;
                    plane_docks = DockCode.Split(';');


                    for (int x = 0; x < plane_docks.Length; x++)
                    {
                        fileName = "Plane_" + plane_docks[x] + "_" + MyDateFromP.ToString("yyyyMMdd") + "_" + MyDateToP.ToString("yyyyMMdd") + ".xls";
                        //fileName = "Plane_" + MyDateFromP.ToString("yyyyMMdd") + "_" + MyDateToP.ToString("yyyyMMdd") + ".xls";
                        zipName = "Plane_" + MyDateFromP.ToString("yyyyMMdd") + "_" + MyDateToP.ToString("yyyyMMdd") + ".zip";
                        documentBytes = PrintPlane(plane_docks[x], ProductionDateFrom, ProductionDateTo, RepDay1anext, RepDay1bnext);
                        listCompress.Add(fileName, documentBytes);
                    }
                    break;

                case "Empty Job Card":
                    DateTime MyDateFromEJC;
                    MyDateFromEJC = new DateTime();
                    MyDateFromEJC = DateTime.ParseExact(ProductionDateFrom, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);

                    DateTime MyDateToEJC;
                    MyDateToEJC = new DateTime();
                    MyDateToEJC = DateTime.ParseExact(ProductionDateTo, "MM/dd/yyyy",
                                                     System.Globalization.CultureInfo.InvariantCulture);

                    fileName = "Empty_Job_Card_" + MyDateFromEJC.ToString("yyyyMMdd") + "_" + MyDateToEJC.ToString("yyyyMMdd") + ".xls";
                    zipName = "Empty_Job_Card_" + MyDateFromEJC.ToString("yyyyMMdd") + "_" + MyDateToEJC.ToString("yyyyMMdd") + ".zip";
                    documentBytes = PrintEmptyJobCard(DockCode, ProductionDateFrom, ProductionDateTo, ProductionMonth);
                    listCompress.Add(fileName, documentBytes);
                    break;

                case "Delivery Control List":
                    fileName = "Delivery_Control_List_" + ProductionDateFrom.Replace("/", "") + "_" + ProductionDateTo.Replace("/", "") + ".xlsx";
                    zipName = "Delivery_Control_List_" + ProductionDateFrom.Replace("/", "") + "_" + ProductionDateTo.Replace("/", "") + ".zip";
                    documentBytes = PrintDeliveryControlList(ProductionDateFrom, ProductionDateTo);
                    listCompress.Add(fileName, documentBytes);
                    break;

            }

            compress.Compress(listCompress, output);
            documentBytes = new byte[output.Length];
            output.Position = 0;
            output.Read(documentBytes, 0, documentBytes.Length);

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();

            Response.Cache.SetCacheability(HttpCacheability.Private);

            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(documentBytes.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");


            Response.BinaryWrite(documentBytes);
            Response.Flush();
            Response.End();
        }
        #endregion

        #region PRINT DELIVERY SUMMARY BY ROUTE
        /// <remarks>
        /// changed by   : fid.salman
        /// changed date : 22.05.2013
        /// description  : 
        /// </remarks>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DeliveryMonth"></param>
        /// <param name="DockCode"></param>
        /// <returns></returns>
        private byte[] PrintDCLReport_SummaryRoute(string DeliveryMonth, string PickupDateFrom, string PickupDateTo, string DockCode)
        {
            Telerik.Reporting.Report rpt;
            string connString = DBContextNames.DB_PCS;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportSummary_Route.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource.SelectCommand = "SP_Rep_DCLReport_SummaryRoute";

            sqlDataSource.Parameters.Add("@DeliveryMonth", System.Data.DbType.String, DeliveryMonth);
            sqlDataSource.Parameters.Add("@PickupDateFrom", System.Data.DbType.String, PickupDateFrom);
            sqlDataSource.Parameters.Add("@PickupDateTo", System.Data.DbType.String, PickupDateTo);
            sqlDataSource.Parameters.Add("@DockCode", System.Data.DbType.String, DockCode.Trim());

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("xls", rpt, null);
            return result.DocumentBytes;
        }
        #endregion

        #region PRINT DELIVERY SUMMARY BY LP
        private byte[] PrintDCLReport_SummaryLP(string DeliveryMonth, string PickupDateFrom, string PickupDateTo, string DockCode)
        {
            Telerik.Reporting.Report rpt;
            string connString = DBContextNames.DB_PCS;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportSummary_LP.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource.SelectCommand = "SP_Rep_DCLReport_SummaryLP";

            sqlDataSource.Parameters.Add("@DeliveryMonth", System.Data.DbType.String, DeliveryMonth);
            sqlDataSource.Parameters.Add("@PickupDateFrom", System.Data.DbType.String, PickupDateFrom);
            sqlDataSource.Parameters.Add("@PickupDateTo", System.Data.DbType.String, PickupDateTo);
            sqlDataSource.Parameters.Add("@DockCode", System.Data.DbType.String, DockCode.Trim());

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("xls", rpt, null);
            return result.DocumentBytes;
        }
        #endregion

        #region PRINT DETAIL DELIVERY DETAIL
        private byte[] PrintDCLReport_SummaryDetail(string DeliveryMonth, string PickupDateFrom, string PickupDateTo, string DockCode)
        {
            Telerik.Reporting.Report rpt;
            string connString = DBContextNames.DB_PCS;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportSummary_Detail.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource.SelectCommand = "SP_Rep_DCLReport_SummaryDetail";

            sqlDataSource.Parameters.Add("@DeliveryMonth", System.Data.DbType.String, DeliveryMonth);
            sqlDataSource.Parameters.Add("@PickupDateFrom", System.Data.DbType.String, PickupDateFrom);
            sqlDataSource.Parameters.Add("@PickupDateTo", System.Data.DbType.String, PickupDateTo);
            sqlDataSource.Parameters.Add("@DockCode", System.Data.DbType.String, DockCode.Trim());

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("xls", rpt, null);
            return result.DocumentBytes;
        }
        #endregion

        #region PRINT DELIVERY PLANNING BY SUPPLIER
        private byte[] PrintDCLReport(string RepDay1anext, string RepDay1bnext, string RepDay2anext, string RepDay2bnext, string RepDay1acurrent,
                                      string RepDay1bcurrent, string RepDay2acurrent, string RepDay2bcurrent, string LPCode, string SupplierCode,
                                      string DateFrom, string DateTo, string ExportType)
        {
            //string ProductionYear = "2014";
            //string ProductionMonth = "12";

            if (RepDay1bcurrent == "MM/dd/yyyy" || RepDay1bcurrent == "yyyy-MM-dd") RepDay1bcurrent = "";
            if (RepDay1bnext == "MM/dd/yyyy" || RepDay1bnext == "yyyy-MM-dd") RepDay1bnext = "";
            if (RepDay2acurrent == "MM/dd/yyyy" || RepDay2acurrent == "yyyy-MM-dd") RepDay2acurrent = "";
            if (RepDay2anext == "MM/dd/yyyy" || RepDay2anext == "yyyy-MM-dd") RepDay2anext = "";
            if (RepDay2bcurrent == "MM/dd/yyyy" || RepDay2bcurrent == "yyyy-MM-dd") RepDay2bcurrent = "";
            if (RepDay2bnext == "MM/dd/yyyy" || RepDay2bnext == "yyyy-MM-dd") RepDay2bnext = "";

            string nextMonth = DateTime.ParseExact(RepDay1anext, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MMMM") + "(P" + DateTime.ParseExact(RepDay1anext, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).Month + ")";
            string thisMonth = DateTime.ParseExact(RepDay1acurrent, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MMMM") + "(P" + DateTime.ParseExact(RepDay1acurrent, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).Month + ")";


            string rep1aNext = DateTime.ParseExact(RepDay1anext, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            string rep1aCurrent = DateTime.ParseExact(RepDay1acurrent, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");

            DateTime MyDateFrom;
            MyDateFrom = new DateTime();
            MyDateFrom = DateTime.ParseExact(DateFrom, "MM/dd/yyyy",
                                             System.Globalization.CultureInfo.InvariantCulture);

            DateTime MyDateTo;
            MyDateTo = new DateTime();
            MyDateTo = DateTime.ParseExact(DateTo, "MM/dd/yyyy",
                                             System.Globalization.CultureInfo.InvariantCulture);


            string Date = (DateFrom == DateTo) ? MyDateFrom.ToString("yyyy-MM-dd") : MyDateFrom.ToString("yyyy-MM-dd") + " ~ " + MyDateTo.ToString("yyyy-MM-dd");


            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;
            Telerik.Reporting.Report rptThis;
            Telerik.Reporting.Report rptNext;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;
            Telerik.Reporting.SqlDataSource sqlDataSource_This;
            Telerik.Reporting.SqlDataSource sqlDataSource_Next;

            Telerik.Reporting.ReportParameter rep_this = new Telerik.Reporting.ReportParameter();
            rep_this.Name = "curr_month";
            rep_this.Value = "(" + rep1aCurrent + ") " + thisMonth;

            Telerik.Reporting.ReportParameter rep_next = new Telerik.Reporting.ReportParameter();
            rep_next.Name = "next_month";
            rep_next.Value = "(" + rep1aNext + ") " + nextMonth;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_Header" + ((ExportType == "P") ? "" : "Excel") + ".trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_SubThis" + ((ExportType == "P") ? "" : "Excel") + ".trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rptThis = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                rptThis.ReportParameters.Add(rep_this);
            }
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_SubNext" + ((ExportType == "P") ? "" : "Excel") + ".trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rptNext = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                rptNext.ReportParameters.Add(rep_next);
            }

            Telerik.Reporting.SubReport rptSub1 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport1", true)[0];
            rptSub1.ReportSource = rptThis;

            Telerik.Reporting.SubReport rptSub2 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport2", true)[0];
            rptSub2.ReportSource = rptNext;

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource_This = (Telerik.Reporting.SqlDataSource)rptThis.DataSource;
            sqlDataSource_Next = (Telerik.Reporting.SqlDataSource)rptNext.DataSource;

            sqlDataSource.ConnectionString = connString;
            //asd
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            //sqlDataSource.SelectCommand = "SELECT TOP 1 RTRIM(SUPPLIER_NAME) AS SupplierName, '" + DateFrom + " - " + DateTo + "' AS MIN_DATE, NULL AS MAX_DATE " +
            //added suppliercode on suppliername fid.deny 2015-04-08
            sqlDataSource.SelectCommand = "SELECT TOP 1 RTRIM(SUPPLIER_NAME) + ' (" + SupplierCode + ")' AS SupplierName, '" + Date + "' AS MIN_DATE, NULL AS MAX_DATE " +
                                          "FROM TB_M_SUPPLIER WHERE SUPPLIER_CODE + '-' + SUPPLIER_PLANT_CD = '" + SupplierCode + "'";

            sqlDataSource_Next.ConnectionString = connString;
            sqlDataSource_Next = (Telerik.Reporting.SqlDataSource)rptNext.DataSource;
            sqlDataSource_Next.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource_Next.SelectCommand = "SP_Rep_DCLReport_Next_NEW";
            sqlDataSource_Next.Parameters.Add("@Date1a", System.Data.DbType.String, RepDay1anext);
            sqlDataSource_Next.Parameters.Add("@Date1b", System.Data.DbType.String, RepDay1bnext);
            sqlDataSource_Next.Parameters.Add("@Date2a", System.Data.DbType.String, RepDay2anext);
            sqlDataSource_Next.Parameters.Add("@Date2b", System.Data.DbType.String, RepDay2bnext);
            sqlDataSource_Next.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            sqlDataSource_Next.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);

            //---=== Return to this current month
            /*if (ProductionMonth.Substring(0, 1) == "0")
            {
                ProductionMonth = "0" + (int.Parse(ProductionMonth.Substring(1, 1)) - 1).ToString();
                if (ProductionMonth == "00")
                {
                    ProductionYear = (int.Parse(ProductionYear) - 1).ToString();
                    ProductionMonth = "12";
                }
            }
            else
            {
                ProductionMonth = (int.Parse(ProductionMonth) - 1).ToString();
            }*/

            sqlDataSource_This.ConnectionString = connString;
            sqlDataSource_This = (Telerik.Reporting.SqlDataSource)rptThis.DataSource;
            sqlDataSource_This.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource_This.SelectCommand = "SP_Rep_DCLReport_Current_NEW";
            sqlDataSource_This.Parameters.Add("@Date1a", System.Data.DbType.String, RepDay1acurrent);
            sqlDataSource_This.Parameters.Add("@Date1b", System.Data.DbType.String, RepDay1bcurrent);
            sqlDataSource_This.Parameters.Add("@Date2a", System.Data.DbType.String, RepDay2acurrent);
            sqlDataSource_This.Parameters.Add("@Date2b", System.Data.DbType.String, RepDay2bcurrent);
            sqlDataSource_This.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            sqlDataSource_This.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport(((ExportType == "P") ? "pdf" : "xls"), rpt, null);
            return result.DocumentBytes;

        }
        #endregion

        #region PRINT DELIVERY PLANNING BY LP

        private byte[] PrintDCLReport_byLP(string RepDay1anext, string RepDay1bnext, string RepDay2anext, string RepDay2bnext, string RepDay1acurrent,
                                string RepDay1bcurrent, string RepDay2acurrent, string RepDay2bcurrent, string LPCode, string SupplierCode,
                                string DateFrom, string DateTo, string ExportType)
        {
            //string ProductionYear = "2014";
            //string ProductionMonth = "12";

            if (RepDay1bcurrent == "MM/dd/yyyy" || RepDay1bcurrent == "yyyy-MM-dd") RepDay1bcurrent = "";
            if (RepDay1bnext == "MM/dd/yyyy" || RepDay1bnext == "yyyy-MM-dd") RepDay1bnext = "";
            if (RepDay2acurrent == "MM/dd/yyyy" || RepDay2acurrent == "yyyy-MM-dd") RepDay2acurrent = "";
            if (RepDay2anext == "MM/dd/yyyy" || RepDay2anext == "yyyy-MM-dd") RepDay2anext = "";
            if (RepDay2bcurrent == "MM/dd/yyyy" || RepDay2bcurrent == "yyyy-MM-dd") RepDay2bcurrent = "";
            if (RepDay2bnext == "MM/dd/yyyy" || RepDay2bnext == "yyyy-MM-dd") RepDay2bnext = "";

            DateTime MyDateFrom;
            MyDateFrom = new DateTime();
            MyDateFrom = DateTime.ParseExact(DateFrom, "MM/dd/yyyy",
                                             System.Globalization.CultureInfo.InvariantCulture);

            DateTime MyDateTo;
            MyDateTo = new DateTime();
            MyDateTo = DateTime.ParseExact(DateTo, "MM/dd/yyyy",
                                             System.Globalization.CultureInfo.InvariantCulture);


            string Date = (DateFrom == DateTo) ? MyDateFrom.ToString("yyyy-MM-dd") : MyDateFrom.ToString("yyyy-MM-dd") + " ~ " + MyDateTo.ToString("yyyy-MM-dd");



            string nextMonth = DateTime.ParseExact(RepDay1anext, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MMMM") + "(P" + DateTime.ParseExact(RepDay1anext, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).Month + ")";
            string thisMonth = DateTime.ParseExact(RepDay1acurrent, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MMMM") + "(P" + DateTime.ParseExact(RepDay1acurrent, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).Month + ")";

            string rep1aNext = DateTime.ParseExact(RepDay1anext, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            string rep1aCurrent = DateTime.ParseExact(RepDay1acurrent, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");


            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;
            Telerik.Reporting.Report rptThis;
            Telerik.Reporting.Report rptNext;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;
            Telerik.Reporting.SqlDataSource sqlDataSource_This;
            Telerik.Reporting.SqlDataSource sqlDataSource_Next;

            Telerik.Reporting.ReportParameter rep_this = new Telerik.Reporting.ReportParameter();
            rep_this.Name = "curr_month";
            rep_this.Value = "(" + rep1aCurrent + ") " + thisMonth;

            Telerik.Reporting.ReportParameter rep_next = new Telerik.Reporting.ReportParameter();
            rep_next.Name = "next_month";
            rep_next.Value = "(" + rep1aNext + ") " + nextMonth;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_Header_byLP" + ((ExportType == "P") ? "" : "Excel") + ".trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_SubThis_byLP" + ((ExportType == "P") ? "" : "Excel") + ".trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rptThis = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                rptThis.ReportParameters.Add(rep_this);
            }
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_SubNext_byLP" + ((ExportType == "P") ? "" : "Excel") + ".trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rptNext = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                rptNext.ReportParameters.Add(rep_next);
            }

            Telerik.Reporting.SubReport rptSub1 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport1", true)[0];
            rptSub1.ReportSource = rptThis;

            Telerik.Reporting.SubReport rptSub2 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport2", true)[0];
            rptSub2.ReportSource = rptNext;

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource_This = (Telerik.Reporting.SqlDataSource)rptThis.DataSource;
            sqlDataSource_Next = (Telerik.Reporting.SqlDataSource)rptNext.DataSource;

            sqlDataSource.ConnectionString = connString;

            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
            sqlDataSource.SelectCommand = "SELECT TOP 1 RTRIM(LOG_PARTNER_NAME) + ' (" + LPCode + ")' AS LPName, '" + Date + "' AS MIN_DATE, NULL AS MAX_DATE " +
                                          "FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = '" + LPCode.Trim() + "'";

            sqlDataSource_Next.ConnectionString = connString;
            sqlDataSource_Next = (Telerik.Reporting.SqlDataSource)rptNext.DataSource;
            sqlDataSource_Next.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource_Next.SelectCommand = "SP_Rep_DCLReport_Next_NEW";
            sqlDataSource_Next.Parameters.Add("@Date1a", System.Data.DbType.String, RepDay1anext);
            sqlDataSource_Next.Parameters.Add("@Date1b", System.Data.DbType.String, RepDay1bnext);
            sqlDataSource_Next.Parameters.Add("@Date2a", System.Data.DbType.String, RepDay2anext);
            sqlDataSource_Next.Parameters.Add("@Date2b", System.Data.DbType.String, RepDay2bnext);
            sqlDataSource_Next.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            sqlDataSource_Next.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);

            sqlDataSource_This.ConnectionString = connString;
            sqlDataSource_This = (Telerik.Reporting.SqlDataSource)rptThis.DataSource;
            sqlDataSource_This.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource_This.SelectCommand = "SP_Rep_DCLReport_Current_NEW";
            sqlDataSource_This.Parameters.Add("@Date1a", System.Data.DbType.String, RepDay1acurrent);
            sqlDataSource_This.Parameters.Add("@Date1b", System.Data.DbType.String, RepDay1bcurrent);
            sqlDataSource_This.Parameters.Add("@Date2a", System.Data.DbType.String, RepDay2acurrent);
            sqlDataSource_This.Parameters.Add("@Date2b", System.Data.DbType.String, RepDay2bcurrent);
            sqlDataSource_This.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
            sqlDataSource_This.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport(((ExportType == "P") ? "pdf" : "xls"), rpt, null);
            return result.DocumentBytes;

        }

        //private byte[] PrintDCLReport_byLP(string DeliveryMonth, string CurrentPhaseFrom, string CurrentPhaseTo, string NextPhaseFrom, string NextPhaseTo, string LPCode, string SupplierCode)
        //{
        //    string thisMonth = DateTime.ParseExact(CurrentPhaseFrom, "yyyy/MM/dd", null).ToString("MMMM");
        //    string thisnextMonth = DateTime.ParseExact(CurrentPhaseTo, "yyyy/MM/dd", null).ToString("MMMM");

        //    string nextMonth = DateTime.ParseExact(NextPhaseFrom, "yyyy/MM/dd", null).ToString("MMMM");
        //    string nextNextMonth = DateTime.ParseExact(NextPhaseTo, "yyyy/MM/dd", null).ToString("MMMM");

        //    //if (thisMonth != thisnextMonth)
        //    //    thisMonth = thisMonth + " - " + thisnextMonth;

        //    //if (nextMonth != nextNextMonth)
        //    //    nextMonth = nextMonth + " - " + nextNextMonth;

        //    //---=== Get month code ===---//
        //    thisMonth = thisMonth + "(P" + DateTime.ParseExact(CurrentPhaseFrom, "yyyy/MM/dd", null).ToString("MM") + ")";
        //    nextMonth = nextMonth + "(P" + DateTime.ParseExact(NextPhaseFrom, "yyyy/MM/dd", null).ToString("MM") + ")";
        //    //---=== End Get month code ===---//

        //    string connString = DBContextNames.DB_PCS;
        //    Telerik.Reporting.Report rpt;
        //    Telerik.Reporting.Report rptThis;
        //    Telerik.Reporting.Report rptNext;

        //    XmlReaderSettings settings;
        //    XmlReader xmlReader;
        //    ReportXmlSerializer xmlSerializer;
        //    Telerik.Reporting.SqlDataSource sqlDataSource;
        //    Telerik.Reporting.SqlDataSource sqlDataSource_This;
        //    Telerik.Reporting.SqlDataSource sqlDataSource_Next;

        //    Telerik.Reporting.ReportParameter rep_this = new Telerik.Reporting.ReportParameter();
        //    rep_this.Name = "curr_month";
        //    rep_this.Value = thisMonth;

        //    Telerik.Reporting.ReportParameter rep_next = new Telerik.Reporting.ReportParameter();
        //    rep_next.Name = "curr_month";
        //    rep_next.Value = nextMonth;

        //    settings = new XmlReaderSettings();
        //    settings.IgnoreWhitespace = true;
        //    using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_Header_byLP.trdx", settings))
        //    {
        //        xmlSerializer = new ReportXmlSerializer();
        //        rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
        //    }
        //    using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_SubThis_byLP.trdx", settings))
        //    {
        //        xmlSerializer = new ReportXmlSerializer();
        //        rptThis = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
        //        rptThis.ReportParameters.Add(rep_this);
        //    }
        //    using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLReport\RepDCL_ReportMontly_SubNext_byLP.trdx", settings))
        //    {
        //        xmlSerializer = new ReportXmlSerializer();
        //        rptNext = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
        //        rptNext.ReportParameters.Add(rep_next);
        //    }

        //    Telerik.Reporting.SubReport rptSub1 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport1", true)[0];
        //    rptSub1.ReportSource = rptThis;

        //    Telerik.Reporting.SubReport rptSub2 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport2", true)[0];
        //    rptSub2.ReportSource = rptNext;

        //    sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
        //    sqlDataSource_This = (Telerik.Reporting.SqlDataSource)rptThis.DataSource;
        //    sqlDataSource_Next = (Telerik.Reporting.SqlDataSource)rptNext.DataSource;

        //    string dateTo = Convert.ToDateTime(DeliveryMonth).ToString("yyyy-MM-dd");

        //    sqlDataSource.ConnectionString = connString;

        //    sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.Text;
        //    sqlDataSource.SelectCommand = "SELECT m.LOG_PARTNER_NAME AS LPName, " +
        //        "CONVERT (VARCHAR(11), CAST('" + NextPhaseFrom + "' AS DATE), 106) AS MIN_DATE, " +
        //        "CONVERT (VARCHAR(11), CAST('" + NextPhaseTo + "' AS DATE), 106) AS MAX_DATE " +
        //        "FROM TB_R_DELIVERY_CTL_SUPPLIERDOCK r " +
        //        "INNER JOIN TB_M_LOGISTIC_PARTNER m ON m.LOG_PARTNER_CD= r.LP_CD " +
        //        "WHERE " +
        //            //"(DATENAME(MONTH, r.PICKUP_DT) + ' ' + CAST(YEAR(r.PICKUP_DT)AS varchar) = '" + DeliveryMonth + "') " +
        //            "(r.PICKUP_DT >= CAST('" + NextPhaseFrom + "' AS DATE) AND r.PICKUP_DT <= CAST('" + NextPhaseTo + "' AS DATE)) " +
        //            "AND ((r.SUPPLIER_CD + '-' + r.SUPP_PLANT_CD = '" + SupplierCode + "' " +
        //            "AND isnull('" + SupplierCode + "', '') <> '' " +
        //            "OR (isnull('" + SupplierCode + "', '') = ''))) " +
        //            "AND ((r.LP_CD = '" + LPCode + "' " +
        //            "AND isnull('" + LPCode + "', '') <> '' " +
        //            "OR (isnull('" + LPCode + "', '') = '')))";

        //    sqlDataSource_This.ConnectionString = connString;
        //    sqlDataSource_This = (Telerik.Reporting.SqlDataSource)rptThis.DataSource;
        //    sqlDataSource_This.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
        //    sqlDataSource_This.SelectCommand = "SP_Rep_DCLReport_Current";
        //    sqlDataSource_This.Parameters.Add("@CurrentPhaseFrom", System.Data.DbType.String, CurrentPhaseFrom);
        //    sqlDataSource_This.Parameters.Add("@CurrentPhaseTo", System.Data.DbType.String, CurrentPhaseTo);
        //    sqlDataSource_This.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
        //    sqlDataSource_This.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);

        //    sqlDataSource_Next.ConnectionString = connString;
        //    sqlDataSource_Next = (Telerik.Reporting.SqlDataSource)rptNext.DataSource;
        //    sqlDataSource_Next.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
        //    sqlDataSource_Next.SelectCommand = "SP_Rep_DCLReport_Next";
        //    sqlDataSource_Next.Parameters.Add("@NextPhaseFrom", System.Data.DbType.String, NextPhaseFrom);
        //    sqlDataSource_Next.Parameters.Add("@NextPhaseTo", System.Data.DbType.String, NextPhaseTo);
        //    sqlDataSource_Next.Parameters.Add("@LPCode", System.Data.DbType.String, LPCode.Trim());
        //    sqlDataSource_Next.Parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);

        //    ReportProcessor reportProcess = new ReportProcessor();
        //    RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
        //    return result.DocumentBytes;

        //}
        #endregion

        #region VALIDATE DATA IF EMPTY WHEN DOWNLOAD REPORT
        //public ContentResult ValidateIsNull(string DeliveryType, string DeliveryMonth, string CurrentPhaseFrom, string CurrentPhaseTo, string NextPhaseFrom, string NextPhaseTo, string DockCode, string SupplierCode, string LPCode)
        public ContentResult ValidateIsNull(string DeliveryType, string DeliveryMonth, string ProductionMonth, string DockCode, string SupplierCode, string LPCode,
                                            string RepDay1anext, string RepDay1bnext, string RepDay2anext, string RepDay2bnext, string RepDay1acurrent,
                                            string RepDay1bcurrent, string RepDay2acurrent, string RepDay2bcurrent, string ProductionDateFrom, string ProductionDateTo)
        {
            //DECLARE VARIABLE
            string result = "";
            if (RepDay1bcurrent == "yyyy-MM-dd") RepDay1bcurrent = "";
            if (RepDay1bnext == "yyyy-MM-dd") RepDay1bnext = "";
            if (RepDay2acurrent == "yyyy-MM-dd") RepDay2acurrent = "";
            if (RepDay2anext == "yyyy-MM-dd") RepDay2anext = "";
            if (RepDay2bcurrent == "yyyy-MM-dd") RepDay2bcurrent = "";
            if (RepDay2bnext == "yyyy-MM-dd") RepDay2bnext = "";

            //VARIABLE TO RETURN 
            string ResultValidate = string.Empty;
            //VARIABLE FROM GRID LOOKUP CONTROL

            string[] Suppliers;
            Suppliers = SupplierCode.Split(';');

            //if ((DeliveryType != "Okamochi Title") || (DeliveryType != "Truck Sheet") || (DeliveryType != "P-Lane Content") || (DeliveryType != "Empty Job Card"))
            //{
            string[] Docks;
            Docks = DockCode.Split(';');
            //}

            string[] LPs;
            LPs = LPCode.Split(';');

            string ResultFind = string.Empty;
            string ResultEmpty = string.Empty;

            string[] pmonth = ProductionMonth.Split(' ');

            string PhaseParam1 = pmonth[1].ToString();
            string PhaseParam2 = formatingNameDate(pmonth[0].ToString());

            if (DeliveryType == "Monthly Route Planning by LP")
            {
                for (int x = 0; x < LPs.Length; x++)
                {
                    IDBContext dbConn = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    ResultValidate = dbConn.ExecuteScalar<string>("RepDCLReport_ValidateIsNull", new object[] { DeliveryMonth, DockCode, 
                                                                  DeliveryType, SupplierCode, LPs[x], PhaseParam1, "P"+PhaseParam2, RepDay1anext,
                                                                  RepDay1acurrent, RepDay1bnext, RepDay1bcurrent, RepDay2anext, RepDay2acurrent,
                                                                  RepDay2bnext, RepDay2bcurrent, ProductionDateFrom, ProductionDateTo});
                    dbConn.Close();
                    /*if (ResultValidate.ToString() == "Data Is Empty in LP Code " + LPs[x].ToString())
                    {
                        ResultEmpty = ResultEmpty + LPs[x].ToString() + ", ";
                    }
                    else
                    {
                        ResultFind = ResultFind + LPs[x].ToString() + ", ";
                    }*/
                }



                //ResultValidate = 
                //(string.IsNullOrEmpty(ResultEmpty) ? "Find Data Route Planning" : ("Data is empty on LP Code " + ResultEmpty.Substring(0, ResultEmpty.Length - 2) +
                //"\n" +
                //(string.IsNullOrEmpty(ResultFind) ? ResultFind : "and data find on LP Code " + ResultFind.Substring(0, ResultFind.Length - 2))));
            }
            else if (DeliveryType == "Monthly Route Planning by Supplier")
            {
                for (int x = 0; x < Suppliers.Length; x++)
                {
                    result = "";
                    IDBContext dbConn = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    result = dbConn.ExecuteScalar<string>("RepDCLReport_ValidateIsNull", new object[] { DeliveryMonth, DockCode, 
                                                                  DeliveryType, Suppliers[x], LPCode, PhaseParam1, "P"+PhaseParam2, RepDay1anext,
                                                                  RepDay1acurrent, RepDay1bnext, RepDay1bcurrent, RepDay2anext, RepDay2acurrent,
                                                                  RepDay2bnext, RepDay2bcurrent, ProductionDateFrom, ProductionDateTo });
                    dbConn.Close();

                    if (result != "Find Data Route Planning")
                    {
                        ResultValidate = ResultValidate + "" + result;
                    }

                    /*if (ResultValidate.ToString() == "Data Is Empty in Supplier Code " + Suppliers[x].ToString())
                    {
                        ResultEmpty = ResultEmpty + Suppliers[x].ToString() + ", ";
                    }
                    else
                    {
                        ResultFind = ResultFind + Suppliers[x].ToString() + ", ";
                    }*/
                }

                if (ResultValidate == "") { ResultValidate = "Find Data Route Planning"; }
                //ResultValidate =
                //(string.IsNullOrEmpty(ResultEmpty) ? "Find Data Route Planning" : ("Data is empty on Supplier Code " + ResultEmpty.Substring(0, ResultEmpty.Length - 2) +
                //"\n" +
                //(string.IsNullOrEmpty(ResultFind) ? ResultFind : "and data find on Supplier Code " + ResultFind.Substring(0, ResultFind.Length - 2))));
            }
            else if ((DeliveryType == "Okamochi Title") || (DeliveryType == "Truck Sheet") || (DeliveryType == "P-Lane Content") || (DeliveryType == "Empty Job Card") || (DeliveryType == "Change Over Report"))
            {
                for (int x = 0; x < Docks.Length; x++)
                {
                    //Need Check for Nullable Dock / Date
                    IDBContext dbConn = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    ResultValidate = dbConn.ExecuteScalar<string>("RepDCLReport_ValidateIsNull", new object[] { DeliveryMonth, Docks[x], 
                                                                  DeliveryType, Suppliers, LPCode, PhaseParam1, "P"+PhaseParam2, RepDay1anext,
                                                                  RepDay1acurrent, RepDay1bnext, RepDay1bcurrent, RepDay2anext, RepDay2acurrent,
                                                                  RepDay2bnext, RepDay2bcurrent, ProductionDateFrom, ProductionDateTo});
                    if (ResultValidate == "Find Data")
                    {
                        ResultFind = ResultValidate;
                    }
                    else
                    {
                        ResultEmpty = ResultValidate;
                    }
                    dbConn.Close();
                }
                if (ResultEmpty != String.Empty)
                {
                    ResultValidate = ResultEmpty;
                }
                else
                {
                    ResultValidate = ResultFind;
                }
            }
            else
            {
                DateTime dtlastdate = DateTime.Parse(PhaseParam1 + "-" + PhaseParam2 + "-01").AddMonths(1).AddDays(-1);
                for (int x = 0; x < Docks.Length; x++)
                {
                    IDBContext dbConn = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    ResultValidate = dbConn.ExecuteScalar<string>("RepDCLReport_ValidateIsNull", new object[] { DeliveryMonth, Docks[x], 
                                                                  DeliveryType, SupplierCode, LPCode, PhaseParam1+"-"+PhaseParam2+"-01", 
                                                                  String.Format("{0:yyyy-MM-dd}",dtlastdate), RepDay1anext, RepDay1acurrent, 
                                                                  RepDay1bnext, RepDay1bcurrent, RepDay2anext, RepDay2acurrent, RepDay2bnext, 
                                                                  RepDay2bcurrent, ProductionDateFrom, ProductionDateTo });
                    dbConn.Close();
                    if (ResultValidate.ToString() == "Data Is Empty on Dock " + Docks[x].ToString())
                    {
                        ResultEmpty = ResultEmpty + Docks[x].ToString() + ", ";
                    }
                    else
                    {
                        ResultFind = ResultFind + Docks[x].ToString() + ", ";
                    }
                }
                ResultValidate =
                (string.IsNullOrEmpty(ResultEmpty) ? "Find Data Summary" : ("Data is empty on Dock " + ResultEmpty.Substring(0, ResultEmpty.Length - 2) +
                "\n" +
                (string.IsNullOrEmpty(ResultFind) ? ResultFind : "and data find on Dock " + ResultFind.Substring(0, ResultFind.Length - 2))));
            }

            return Content(ResultValidate.ToString());
        }

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                result = extractString[2] + "/" + extractString[1] + "/" + extractString[0];
            }

            return result;
        }

        private string formatingNameDate(string dateName)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateName != "")
            {
                switch (dateName)
                {
                    case "January": result = "01"; break;
                    case "February": result = "02"; break;
                    case "March": result = "03"; break;
                    case "April": result = "04"; break;
                    case "May": result = "05"; break;
                    case "June": result = "06"; break;
                    case "July": result = "07"; break;
                    case "August": result = "08"; break;
                    case "September": result = "09"; break;
                    case "October": result = "10"; break;
                    case "November": result = "11"; break;
                    case "December": result = "12"; break;
                }
            }

            return result;
        }

        private string formatingDateName(string Month)
        {
            string result = "";

            if (Month != "")
            {
                switch (Month)
                {
                    case "01": result = "January"; break;
                    case "02": result = "February"; break;
                    case "03": result = "March"; break;
                    case "04": result = "April"; break;
                    case "05": result = "May"; break;
                    case "06": result = "June"; break;
                    case "07": result = "July"; break;
                    case "08": result = "August"; break;
                    case "09": result = "September"; break;
                    case "10": result = "October"; break;
                    case "11": result = "November"; break;
                    case "12": result = "December"; break;
                }
            }

            return result;
        }
        #endregion

        #region SYNCHRONIZE
        public ContentResult Synchronize(string SyncType, string dateFrom, string dateTo)
        {
            string username = "";
            string syncType = "";
            string dateParam = dateFrom + ";" + dateTo;

            Toyota.Common.Web.Credential.User user = SessionState.GetAuthorizedUser();
            if (user != null)
            {
                username = user.Username;
            }

            if (SyncType == "Sync DCL")
            {
                syncType = "TLMS_INTERFACE_FOR_DCL";
            }
            else if (SyncType == "Sync Supplier Meeting")
            {
                syncType = "TLMS_INTERFACE_FOR_SM";
            }
            else if (SyncType == "Sync P-Lane")
            {
                syncType = "TLMS_INTERFACE_FOR_PLANE";
            }


            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //insert dateFrom and dateTo into tb_m_system
                db.Execute("DCLSyncParam", new object[] { dateParam, username, SyncType });
                //db.Execute("SynchOnDemanFrom", new object[] { "C", "SYNCHRONIZE_DCL_SUPPLIER_MEET" });
                db.Execute("SynchOnDemanFrom", new object[] { "C", syncType });
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content("Failed");
            }
            db.Close();
            db.Dispose();
        }

        public ContentResult SynchronizeCheck(string SyncType)
        {
            string system_cd = "";

            if (SyncType == "Sync DCL")
            {
                system_cd = "PROCESS_ID DCL";
            }
            else if (SyncType == "Sync Supplier Meeting")
            {
                system_cd = "PROCESS_ID SM";
            }
            else if (SyncType == "Sync P-Lane")
            {
                system_cd = "PROCESS_ID PLANE";
            }


            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                String result = db.ExecuteScalar<String>("checkProcessSync", new object[] { system_cd });

                return Content(result);
            }
            catch (Exception e)
            {
                return Content("FAIL|" + e.Message.ToString());
            }
            db.Close();
            db.Dispose();


            /*
            string jobs = "";
            string ResultCheck = "";
            string ResultMessage = "";
            string ResultLastStepMessage = "";
            string formattedToken = "";
            string Outcome = ""; //added 2015-03-19


            if (SyncType == "Sync DCL")
            { 
                jobs = "TLMS_INTERFACE_FOR_DCL";
            }
            else if (SyncType == "Sync Supplier Meeting")
            {
                jobs = "TLMS_INTERFACE_FOR_SM";
            }
            else if (SyncType == "Sync P-Lane")
            {
                jobs = "TLMS_INTERFACE_FOR_PLANE";
            }



            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //List<SyncCheck> QueryLog = db.Fetch<SyncCheck>("SynchOnDemanFrom", new object[] { "S", "SYNCHRONIZE_DCL_SUPPLIER_MEET" });
                List<SyncCheck> QueryLog = db.Fetch<SyncCheck>("SynchOnDemanFrom", new object[] { "S", jobs });
                for (int i = 0; i < QueryLog.Count; i++ )
                {
                    //if (QueryLog[i].Job_Name == "SYNCHRONIZE_DCL_SUPPLIER_MEET")
                    if (QueryLog[i].Job_Name == jobs)
                    {
                        ResultCheck = QueryLog[i].Job_Status;
                        ResultMessage = QueryLog[i].Job_Message;
                        Outcome = QueryLog[i].Outcome;
                        ResultLastStepMessage = QueryLog[i].Last_Step_Message;
                    }
                }

                string[] tokens = ResultLastStepMessage.Split(new string[] { "." }, StringSplitOptions.None);
                formattedToken = tokens[0] + ".";
                if ((ResultCheck.ToUpper() == "FAILED" || ResultCheck.ToUpper() == "SUCCESS") && ResultMessage != formattedToken)
                {
                    ResultCheck = "IN PROGRESS";
                }

                return Content(ResultCheck+"|"+ResultMessage+"|"+Outcome);
            }
            catch (Exception ex)
            {
                return Content("Failed|Synchronize failed. "+ex.ToString());
            }
            db.Close();
            db.Dispose();
             
            */
        }
        #endregion

        //added for start dcl job
        public ContentResult startDCLCreation()
        {
            string sp = "TLMS_DCL_Creation_Job";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //insert dateFrom and dateTo into tb_m_system
                //db.Execute("DCLSyncParam", new object[] { dateParam });
                db.Execute("SynchOnDemanFrom", new object[] { "C", sp });
                //db.Execute("DCL_RunDCLCreation");
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content("Failed");
            }
            db.Close();
            db.Dispose();
        }


        //added for sending email
        public ContentResult sendNotifEmail(string dtFrom, string dtTo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("DCLSendNotifEmail", new object[] { dtFrom, dtTo });
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content("Failed");
            }
            db.Close();
            db.Dispose();
        }

        //checking for unmapped delivery station (doorcd)
        //checkUnmappedStation
        public ContentResult checkUnmappedStation()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                String result = db.ExecuteScalar<String>("checkingUnmappedStation");
                return Content(result);
            }
            catch (Exception E)
            {
                return Content("Failed");
            }
            db.Close();
            db.Dispose();
        }


        //added for checking finished sync data exists or not
        public ContentResult checkTemporaryDCLTable()
        {
            string status = string.Empty;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                status = db.ExecuteScalar<string>("checkTemporaryDCLTable");
                return Content(status);
            }
            catch (Exception ex)
            {
                return Content("Failed");
            }

            db.Close();
            db.Dispose();

        }



        //added for validate dcl
        public ContentResult validateStatus(string dateFrom, string dateTo)
        {
            string status = string.Empty;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                status = db.ExecuteScalar<string>("DCLValidateStatus", new object[] { dateFrom, dateTo });
                return Content(status);
            }
            catch (Exception ex)
            {
                return Content("Failed");
            }
            db.Close();
            db.Dispose();
        }

        //end of added

        //added for validate lock
        public ContentResult validateLock(string sync)
        {
            string status = string.Empty;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                status = db.ExecuteScalar<string>("DCLCheckStatusLock", new object[] { sync });
                return Content(status);
            }
            catch (Exception ex)
            {
                return Content("Failed");
            }
            db.Close();
            db.Dispose();
        }
        //end of added




        private byte[] PrintOkamochi(string p_Dock, string p_DateFrom, string p_DateTo, string Style, string p_rep_a, string p_rep_b)
        {
            try
            {
                string prodDate = null;
                if (p_DateFrom == p_DateTo)
                {
                    prodDate = p_DateFrom;
                }


                #region Initialization
                CellRangeAddress rowH;
                string filesTmp = "";
                short RowHeight = 430;
                int cell;
                int row1 = 1;
                int row2 = row1 + 12;
                double x;
                double page;
                int r;
                int detailNum = 0;

                if (Style == "C")
                {
                    row2 = row1 + 12;
                }
                else if (Style == "B")
                {
                    row2 = 1;
                }

                int over = 0;
                int counter = 1;

                //added for formating date
                DateTime MyDateFrom;
                MyDateFrom = new DateTime();
                MyDateFrom = DateTime.ParseExact(p_DateFrom, "MM/dd/yyyy",
                                                 System.Globalization.CultureInfo.InvariantCulture);

                DateTime MyDateTo;
                MyDateTo = new DateTime();
                MyDateTo = DateTime.ParseExact(p_DateTo, "MM/dd/yyyy",
                                                 System.Globalization.CultureInfo.InvariantCulture);


                string Date = (p_DateFrom == p_DateTo) ? MyDateFrom.ToString("yyyy-MM-dd") : MyDateFrom.ToString("yyyy-MM-dd") + " / " + MyDateTo.ToString("yyyy-MM-dd");

                List<OkamochiHeader> model = new List<OkamochiHeader>();
                List<OkamochiDetail> modeldetail = new List<OkamochiDetail>();
                IDBContext db = DbContext;
                #endregion

                #region Get Template
                if (Style == "C")
                {
                    filesTmp = HttpContext.Request.MapPath("~/Template/Dummy.xls");
                }
                if (Style == "A")
                {
                    filesTmp = HttpContext.Request.MapPath("~/Template/Dummy_Left.xls");
                }
                if (Style == "B")
                {
                    filesTmp = HttpContext.Request.MapPath("~/Template/Dummy_Right.xls");
                }

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);
                IRow Hrow;
                ISheet sheet = workbook.GetSheet("Okamochi");

                sheet.ForceFormulaRecalculation = true;
                #endregion

                #region Style Initialization
                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 25;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font2 = workbook.CreateFont();
                Font2.FontHeightInPoints = 20;
                Font2.FontName = "Arial";
                Font2.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font3 = workbook.CreateFont();
                Font3.FontHeightInPoints = 14;
                Font3.FontName = "Arial";
                Font3.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 12;
                Font4.FontName = "Arial";
                Font4.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font41 = workbook.CreateFont();
                Font41.FontHeightInPoints = 16;
                Font41.FontName = "Arial";
                //Font41.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font5 = workbook.CreateFont();
                Font5.FontHeightInPoints = 10;
                Font5.FontName = "Arial";

                IFont Font51 = workbook.CreateFont();
                Font51.FontHeightInPoints = 10;
                Font51.FontName = "Arial";
                Font51.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.YELLOW.index;
                styleContent1.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.SetFont(Font4);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent3.Alignment = HorizontalAlignment.CENTER;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent3.SetFont(Font1);

                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent4.Alignment = HorizontalAlignment.CENTER;
                styleContent4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent4.BorderTop = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent4.BorderRight = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent4.SetFont(Font2);

                ICellStyle styleContent41 = workbook.CreateCellStyle();
                styleContent41.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent41.Alignment = HorizontalAlignment.CENTER;
                styleContent41.BorderLeft = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent41.BorderBottom = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent41.BorderTop = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent41.BorderRight = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent41.SetFont(Font41);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent5.BorderTop = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent5.BorderRight = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent5.SetFont(Font3);

                ICellStyle styleContent51 = workbook.CreateCellStyle();
                styleContent51.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent51.Alignment = HorizontalAlignment.CENTER;
                styleContent51.BorderLeft = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent51.BorderBottom = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent51.BorderTop = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent51.BorderRight = NPOI.SS.UserModel.BorderStyle.THICK;
                styleContent51.SetFont(Font5);

                ICellStyle styleContent6 = workbook.CreateCellStyle();
                styleContent6.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent6.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.TURQUOISE.index;
                styleContent6.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent6.SetFont(Font4);

                ICellStyle styleContent7 = workbook.CreateCellStyle();
                styleContent7.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent7.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.SetFont(Font4);

                ICellStyle styleContent8 = workbook.CreateCellStyle();
                styleContent8.BorderBottom = NPOI.SS.UserModel.BorderStyle.DASHED;

                ICellStyle styleContent9 = workbook.CreateCellStyle();
                styleContent9.SetFont(Font5);
                #endregion
                //change p_DateFrom to p_rep_a, change p_DateTo to p_rep_b -- fid.deny 2015-04-01
                model = DbContext.Fetch<OkamochiHeader>("GetHeaderOkamochi", new object[] { p_Dock, p_rep_a, p_rep_b }); // select header

                foreach (var result in model)
                {
                    cell = 1;
                    over = 0;

                    #region Fetch Header
                    string R = String.IsNullOrEmpty(result.R) ? "" : result.R.ToString();
                    string No = String.IsNullOrEmpty(result.No) ? "" : result.No.ToString();
                    string DockCD = String.IsNullOrEmpty(result.AllDock) ? "" : result.AllDock.ToString();
                    string SDock = String.IsNullOrEmpty(result.SDock) ? "" : result.SDock.ToString();
                    string Route = String.IsNullOrEmpty(result.Route) ? "" : result.Route.ToString();
                    string Terminal = String.IsNullOrEmpty(result.Terminal) ? "" : result.Terminal.ToString();
                    string Arr = String.IsNullOrEmpty(result.Arr) ? "" : result.Arr.ToString();
                    string ArrFri = String.IsNullOrEmpty(result.ArrFri) ? "" : "(Friday : " + result.ArrFri.ToString() + " ) ";
                    string Dep = String.IsNullOrEmpty(result.Dep) ? "" : result.Dep.ToString();
                    string DepFri = String.IsNullOrEmpty(result.DepFri) ? "" : "(Friday : " + result.DepFri.ToString() + " ) ";
                    string Logp = String.IsNullOrEmpty(result.Logp) ? "" : result.Logp.ToString();
                    string Cycle = String.IsNullOrEmpty(result.Cycle) ? "" : result.Cycle.ToString();
                    string Trip = String.IsNullOrEmpty(result.Trip) ? "" : result.Trip.ToString();
                    #endregion

                    //change p_DateFrom to p_rep_a, change p_DateTo to p_rep_b -- fid.deny 2015-04-01
                    modeldetail = DbContext.Fetch<OkamochiDetail>("GetOkamochiDetail", new object[] { SDock, p_rep_a, p_rep_b, R, Trip, prodDate });

                    #region Fetch data to template
                    int z = 0, y = 0;
                    if (Style == "A")
                    {
                        z = 10;
                        y = 10;
                    }
                    else if (Style == "C")
                    {
                        z = 22;
                        y = 32;
                    }

                    if (row1 < z || row2 < y)
                    {
                        if ((Style == "A") || (Style == "C"))
                        {
                            //yang atas dlu, nama varnya ngasal
                            sheet.GetRow(row1).GetCell(21).SetCellValue(No);
                            sheet.GetRow(row1).GetCell(21).CellStyle.SetFont(Font1);
                            sheet.GetRow(row1).GetCell(17).SetCellValue(DockCD);
                            sheet.GetRow(row1 + 1).GetCell(17).SetCellValue(Route);
                            sheet.GetRow(row1 + 2).GetCell(17).SetCellValue(Terminal);
                            sheet.GetRow(row1 + 3).GetCell(17).SetCellValue(Arr + " " + ArrFri);
                            sheet.GetRow(row1 + 4).GetCell(17).SetCellValue(Dep + " " + DepFri);
                            sheet.GetRow(row1 + 2).GetCell(11).SetCellValue(Date);
                            sheet.GetRow(row1 + 3).GetCell(11).SetCellValue(Logp);
                            sheet.GetRow(row1 + 4).GetCell(11).SetCellValue(Cycle);
                        }
                        //yang bawah
                        if ((Style == "B") || (Style == "C"))
                        {
                            sheet.GetRow(row2).GetCell(1).SetCellValue(No);
                            sheet.GetRow(row2).GetCell(1).CellStyle.SetFont(Font1);
                            sheet.GetRow(row2).GetCell(4).SetCellValue(DockCD);
                            sheet.GetRow(row2 + 1).GetCell(4).SetCellValue(Route);
                            sheet.GetRow(row2 + 2).GetCell(4).SetCellValue(Terminal);
                            sheet.GetRow(row2 + 3).GetCell(4).SetCellValue(Arr + " " + ArrFri);
                            sheet.GetRow(row2 + 4).GetCell(4).SetCellValue(Dep + " " + DepFri);
                            sheet.GetRow(row2 + 2).GetCell(10).SetCellValue(Date);
                            sheet.GetRow(row2 + 3).GetCell(10).SetCellValue(Logp);
                            sheet.GetRow(row2 + 4).GetCell(10).SetCellValue(Cycle);
                        }

                        if ((Style == "A") || (Style == "C"))
                        {
                            row1 = row1 + 5;
                        }
                        if ((Style == "B") || (Style == "C"))
                        {
                            row2 = row2 + 5;
                        }
                        counter++;
                    }
                    #endregion

                    #region Create new row for header data
                    else
                    {
                        #region Set Header
                        if ((Style == "A") || (Style == "C"))
                        {
                            #region Side A
                            sheet.CreateRow(row1).CreateCell(15).SetCellValue("DOCK");
                            sheet.GetRow(row1).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row1, row1, 15, 16));
                            sheet.GetRow(row1).CreateCell(17).SetCellValue(DockCD);
                            sheet.AddMergedRegion(new CellRangeAddress(row1, row1, 17, 20));
                            sheet.GetRow(row1).CreateCell(21).SetCellValue(No);
                            //sheet.GetRow(row1).GetCell(20).CellStyle.SetFont(Font1);
                            sheet.AddMergedRegion(new CellRangeAddress(row1, row1 + 4, 21, 21));
                            sheet.CreateRow(row1 + 1).CreateCell(15).SetCellValue("ROUTE");
                            sheet.GetRow(row1 + 1).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 1, row1 + 1, 15, 16));
                            sheet.GetRow(row1 + 1).CreateCell(17).SetCellValue(Route);
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 1, row1 + 1, 17, 20));
                            sheet.CreateRow(row1 + 2).CreateCell(9).SetCellValue("PROD. DATE");
                            sheet.GetRow(row1 + 2).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 9, 10));
                            sheet.GetRow(row1 + 2).CreateCell(11).SetCellValue(Date);
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 11, 14));
                            sheet.GetRow(row1 + 2).CreateCell(15).SetCellValue("TRUCK STATION");
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 15, 16));
                            sheet.GetRow(row1 + 2).CreateCell(17).SetCellValue(Terminal);
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 17, 20));
                            sheet.CreateRow(row1 + 3).CreateCell(9).SetCellValue("LOGISTIC PARTNER");
                            sheet.GetRow(row1 + 3).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 9, 10));
                            sheet.GetRow(row1 + 3).CreateCell(11).SetCellValue(Logp);
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 11, 14));
                            sheet.GetRow(row1 + 3).CreateCell(15).SetCellValue("ARRIVAL");
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 15, 16));
                            sheet.GetRow(row1 + 3).CreateCell(17).SetCellValue(Arr + " " + ArrFri);
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 17, 20));
                            sheet.CreateRow(row1 + 4).CreateCell(9).SetCellValue("CYCLE ISSUE");
                            sheet.GetRow(row1 + 4).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 9, 10));
                            sheet.GetRow(row1 + 4).CreateCell(11).SetCellValue(Cycle);
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 11, 14));
                            sheet.GetRow(row1 + 4).CreateCell(15).SetCellValue("DEPARTURE");
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 15, 16));
                            sheet.GetRow(row1 + 4).CreateCell(17).SetCellValue(Dep + " " + DepFri);
                            sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 17, 20));
                            if (Style == "C") sheet.GetRow(row1 + 4).CreateCell(1).SetCellValue("Side A");
                            #endregion

                            #region Side A Styling
                            sheet.GetRow(row1).GetCell(15).CellStyle = styleContent1;
                            sheet.GetRow(row1).CreateCell(16).CellStyle = styleContent2;
                            sheet.GetRow(row1).GetCell(17).CellStyle = styleContent6;
                            sheet.GetRow(row1).CreateCell(18).CellStyle = styleContent7;
                            sheet.GetRow(row1).CreateCell(19).CellStyle = styleContent7;
                            sheet.GetRow(row1).CreateCell(20).CellStyle = styleContent7;
                            sheet.GetRow(row1).GetCell(21).CellStyle = styleContent3;
                            sheet.GetRow(row1 + 1).GetCell(15).CellStyle = styleContent1;
                            sheet.GetRow(row1 + 1).CreateCell(16).CellStyle = styleContent2;
                            sheet.GetRow(row1 + 1).GetCell(17).CellStyle = styleContent6;
                            sheet.GetRow(row1 + 1).CreateCell(18).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 1).CreateCell(19).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 1).CreateCell(20).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 1).CreateCell(21).CellStyle = styleContent3;
                            sheet.GetRow(row1 + 2).GetCell(9).CellStyle = styleContent1;
                            sheet.GetRow(row1 + 2).CreateCell(10).CellStyle = styleContent2;
                            sheet.GetRow(row1 + 2).GetCell(11).CellStyle = styleContent6;
                            sheet.GetRow(row1 + 2).CreateCell(12).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 2).CreateCell(13).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 2).CreateCell(14).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 2).GetCell(15).CellStyle = styleContent1;
                            sheet.GetRow(row1 + 2).CreateCell(16).CellStyle = styleContent2;
                            sheet.GetRow(row1 + 2).GetCell(17).CellStyle = styleContent6;
                            sheet.GetRow(row1 + 2).CreateCell(18).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 2).CreateCell(19).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 2).CreateCell(20).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 2).CreateCell(21).CellStyle = styleContent3;
                            sheet.GetRow(row1 + 3).GetCell(9).CellStyle = styleContent1;
                            sheet.GetRow(row1 + 3).CreateCell(10).CellStyle = styleContent2;
                            sheet.GetRow(row1 + 3).GetCell(11).CellStyle = styleContent6;
                            sheet.GetRow(row1 + 3).CreateCell(12).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 3).CreateCell(13).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 3).CreateCell(14).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 3).GetCell(15).CellStyle = styleContent1;
                            sheet.GetRow(row1 + 3).CreateCell(16).CellStyle = styleContent2;
                            sheet.GetRow(row1 + 3).GetCell(17).CellStyle = styleContent6;
                            sheet.GetRow(row1 + 3).CreateCell(18).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 3).CreateCell(19).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 3).CreateCell(20).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 3).CreateCell(21).CellStyle = styleContent3;
                            sheet.GetRow(row1 + 4).GetCell(9).CellStyle = styleContent1;
                            sheet.GetRow(row1 + 4).CreateCell(10).CellStyle = styleContent2;
                            sheet.GetRow(row1 + 4).GetCell(11).CellStyle = styleContent6;
                            sheet.GetRow(row1 + 4).CreateCell(12).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 4).CreateCell(13).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 4).CreateCell(14).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 4).GetCell(15).CellStyle = styleContent1;
                            sheet.GetRow(row1 + 4).CreateCell(16).CellStyle = styleContent2;
                            sheet.GetRow(row1 + 4).GetCell(17).CellStyle = styleContent6;
                            sheet.GetRow(row1 + 4).CreateCell(18).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 4).CreateCell(19).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 4).CreateCell(20).CellStyle = styleContent7;
                            sheet.GetRow(row1 + 4).CreateCell(21).CellStyle = styleContent3;
                            if (Style == "C") sheet.GetRow(row1 + 4).GetCell(1).CellStyle = styleContent9;
                            #endregion
                        }

                        if ((Style == "B") || (Style == "C"))
                        {
                            #region Side B
                            sheet.CreateRow(row2).CreateCell(1).SetCellValue(No);
                            sheet.GetRow(row2).Height = RowHeight;
                            //sheet.GetRow(row2).GetCell(1).CellStyle.SetFont(Font1);
                            sheet.AddMergedRegion(new CellRangeAddress(row2, row2 + 4, 1, 1));
                            sheet.GetRow(row2).CreateCell(2).SetCellValue("DOCK");
                            sheet.AddMergedRegion(new CellRangeAddress(row2, row2, 2, 3));
                            sheet.GetRow(row2).CreateCell(4).SetCellValue(DockCD);
                            sheet.AddMergedRegion(new CellRangeAddress(row2, row2, 4, 7));
                            sheet.CreateRow(row2 + 1).CreateCell(2).SetCellValue("ROUTE");
                            sheet.GetRow(row2 + 1).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 1, row2 + 1, 2, 3));
                            sheet.GetRow(row2 + 1).CreateCell(4).SetCellValue(Route);
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 1, row2 + 1, 4, 7));
                            sheet.CreateRow(row2 + 2).CreateCell(2).SetCellValue("TRUCK STATION");
                            sheet.GetRow(row2 + 2).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 2, 3));
                            sheet.GetRow(row2 + 2).CreateCell(4).SetCellValue(Terminal);
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 4, 7));
                            sheet.GetRow(row2 + 2).CreateCell(8).SetCellValue("PROD. DATE");
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 8, 9));
                            sheet.GetRow(row2 + 2).CreateCell(10).SetCellValue(Date);
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 10, 13));
                            sheet.CreateRow(row2 + 3).CreateCell(2).SetCellValue("ARRIVAL");
                            sheet.GetRow(row2 + 3).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 2, 3));
                            sheet.GetRow(row2 + 3).CreateCell(4).SetCellValue(Arr + " " + ArrFri);
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 4, 7));
                            sheet.GetRow(row2 + 3).CreateCell(8).SetCellValue("LOGISTIC PARTNER");
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 8, 9));
                            sheet.GetRow(row2 + 3).CreateCell(10).SetCellValue(Logp);
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 10, 13));
                            sheet.CreateRow(row2 + 4).CreateCell(2).SetCellValue("DEPARTURE");
                            sheet.GetRow(row2 + 4).Height = RowHeight;
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 2, 3));
                            sheet.GetRow(row2 + 4).CreateCell(4).SetCellValue(Dep + " " + DepFri);
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 4, 7));
                            sheet.GetRow(row2 + 4).CreateCell(8).SetCellValue("CYCLE ISSUE");
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 8, 9));
                            sheet.GetRow(row2 + 4).CreateCell(10).SetCellValue(Cycle);
                            sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 10, 13));
                            if (Style == "C") sheet.GetRow(row2 + 4).CreateCell(21).SetCellValue("Side B");
                            #endregion

                            #region Side B Styling
                            sheet.GetRow(row2).GetCell(2).CellStyle = styleContent1;
                            sheet.GetRow(row2).CreateCell(3).CellStyle = styleContent2;
                            sheet.GetRow(row2).GetCell(4).CellStyle = styleContent6;
                            sheet.GetRow(row2).CreateCell(5).CellStyle = styleContent7;
                            sheet.GetRow(row2).CreateCell(6).CellStyle = styleContent7;
                            sheet.GetRow(row2).CreateCell(7).CellStyle = styleContent7;
                            sheet.GetRow(row2).GetCell(1).CellStyle = styleContent3;
                            sheet.GetRow(row2 + 1).GetCell(2).CellStyle = styleContent1;
                            sheet.GetRow(row2 + 1).CreateCell(3).CellStyle = styleContent2;
                            sheet.GetRow(row2 + 1).GetCell(4).CellStyle = styleContent6;
                            sheet.GetRow(row2 + 1).CreateCell(5).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 1).CreateCell(6).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 1).CreateCell(7).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 1).CreateCell(1).CellStyle = styleContent3;
                            sheet.GetRow(row2 + 2).GetCell(8).CellStyle = styleContent1;
                            sheet.GetRow(row2 + 2).CreateCell(9).CellStyle = styleContent2;
                            sheet.GetRow(row2 + 2).GetCell(10).CellStyle = styleContent6;
                            sheet.GetRow(row2 + 2).CreateCell(11).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 2).CreateCell(12).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 2).CreateCell(13).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 2).GetCell(2).CellStyle = styleContent1;
                            sheet.GetRow(row2 + 2).CreateCell(3).CellStyle = styleContent2;
                            sheet.GetRow(row2 + 2).GetCell(4).CellStyle = styleContent6;
                            sheet.GetRow(row2 + 2).CreateCell(5).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 2).CreateCell(6).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 2).CreateCell(7).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 2).CreateCell(1).CellStyle = styleContent3;
                            sheet.GetRow(row2 + 3).GetCell(8).CellStyle = styleContent1;
                            sheet.GetRow(row2 + 3).CreateCell(9).CellStyle = styleContent2;
                            sheet.GetRow(row2 + 3).GetCell(10).CellStyle = styleContent6;
                            sheet.GetRow(row2 + 3).CreateCell(11).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 3).CreateCell(12).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 3).CreateCell(13).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 3).GetCell(2).CellStyle = styleContent1;
                            sheet.GetRow(row2 + 3).CreateCell(3).CellStyle = styleContent2;
                            sheet.GetRow(row2 + 3).GetCell(4).CellStyle = styleContent6;
                            sheet.GetRow(row2 + 3).CreateCell(5).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 3).CreateCell(6).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 3).CreateCell(7).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 3).CreateCell(1).CellStyle = styleContent3;
                            sheet.GetRow(row2 + 4).GetCell(8).CellStyle = styleContent1;
                            sheet.GetRow(row2 + 4).CreateCell(9).CellStyle = styleContent2;
                            sheet.GetRow(row2 + 4).GetCell(10).CellStyle = styleContent6;
                            sheet.GetRow(row2 + 4).CreateCell(11).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 4).CreateCell(12).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 4).CreateCell(13).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 4).GetCell(2).CellStyle = styleContent1;
                            sheet.GetRow(row2 + 4).CreateCell(3).CellStyle = styleContent2;
                            sheet.GetRow(row2 + 4).GetCell(4).CellStyle = styleContent6;
                            sheet.GetRow(row2 + 4).CreateCell(5).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 4).CreateCell(6).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 4).CreateCell(7).CellStyle = styleContent7;
                            sheet.GetRow(row2 + 4).CreateCell(1).CellStyle = styleContent3;
                            if (Style == "C") sheet.GetRow(row2 + 4).GetCell(21).CellStyle = styleContent9;
                            #endregion
                        }
                        #endregion

                        #region Create new row for Detail
                        if ((Style == "A") || (Style == "C"))
                        {
                            row1 = row1 + 5;
                        }
                        if ((Style == "B") || (Style == "C"))
                        {
                            row2 = row2 + 5;
                        }

                        if ((Style == "A") || (Style == "C"))
                        {
                            Hrow = sheet.CreateRow(row1);
                            sheet.GetRow(row1).Height = RowHeight;
                            Hrow = sheet.CreateRow(row1 + 1);
                            sheet.GetRow(row1 + 1).Height = RowHeight;
                            Hrow = sheet.CreateRow(row1 + 2);
                            sheet.GetRow(row1 + 2).Height = RowHeight;
                            Hrow = sheet.CreateRow(row1 + 3);
                            sheet.GetRow(row1 + 3).Height = RowHeight;
                            Hrow = sheet.CreateRow(row1 + 4);
                            sheet.GetRow(row1 + 4).Height = RowHeight;

                            sheet.GetRow(row1).CreateCell(21).SetCellValue("SUPP CODE");
                            sheet.GetRow(row1 + 1).CreateCell(21).SetCellValue("SUPP NAME");
                            sheet.GetRow(row1 + 2).CreateCell(21).SetCellValue("DOCK");
                            sheet.GetRow(row1 + 3).CreateCell(21).SetCellValue("#ORDER");
                            sheet.GetRow(row1 + 4).CreateCell(21).SetCellValue("NO");
                            sheet.GetRow(row1).GetCell(21).CellStyle = styleContent51;
                            sheet.GetRow(row1 + 1).GetCell(21).CellStyle = styleContent51;
                            sheet.GetRow(row1 + 2).GetCell(21).CellStyle = styleContent51;
                            sheet.GetRow(row1 + 3).GetCell(21).CellStyle = styleContent51;
                            sheet.GetRow(row1 + 4).GetCell(21).CellStyle = styleContent51;
                        }

                        if ((Style == "B") || (Style == "C"))
                        {
                            Hrow = sheet.CreateRow(row2);
                            sheet.GetRow(row2).Height = RowHeight;
                            Hrow = sheet.CreateRow(row2 + 1);
                            sheet.GetRow(row2 + 1).Height = RowHeight;
                            Hrow = sheet.CreateRow(row2 + 2);
                            sheet.GetRow(row2 + 1).Height = RowHeight;
                            Hrow = sheet.CreateRow(row2 + 3);
                            sheet.GetRow(row2 + 1).Height = RowHeight;
                            Hrow = sheet.CreateRow(row2 + 4);
                            sheet.GetRow(row2 + 4).Height = RowHeight;

                            sheet.GetRow(row2).CreateCell(1).SetCellValue("SUPP CODE");
                            sheet.GetRow(row2 + 1).CreateCell(1).SetCellValue("SUPP NAME");
                            sheet.GetRow(row2 + 2).CreateCell(1).SetCellValue("DOCK");
                            sheet.GetRow(row2 + 3).CreateCell(1).SetCellValue("#ORDER");
                            sheet.GetRow(row2 + 4).CreateCell(1).SetCellValue("NO");
                            sheet.GetRow(row2).GetCell(1).CellStyle = styleContent51;
                            sheet.GetRow(row2 + 1).GetCell(1).CellStyle = styleContent51;
                            sheet.GetRow(row2 + 2).GetCell(1).CellStyle = styleContent51;
                            sheet.GetRow(row2 + 3).GetCell(1).CellStyle = styleContent51;
                            sheet.GetRow(row2 + 4).GetCell(1).CellStyle = styleContent51;
                        }
                        #endregion

                        counter++;
                    }
                    #endregion

                    detailNum = 1;

                    #region Detail Okamochi
                    foreach (var resultdetail in modeldetail)
                    {

                        #region Detail Fetching
                        string SuppCD = String.IsNullOrEmpty(resultdetail.SupplierCD) ? "" : resultdetail.SupplierCD.ToString();
                        string SuppAbbr = String.IsNullOrEmpty(resultdetail.SupplierAbbr) ? "" : resultdetail.SupplierAbbr.ToString();
                        string Dock = String.IsNullOrEmpty(resultdetail.Dock) ? "" : resultdetail.Dock.ToString();
                        string Ord = String.IsNullOrEmpty(resultdetail.Ord) ? "" : resultdetail.Ord.ToString();
                        #endregion

                        #region Fetch to template detail data
                        if (row1 < z || row2 < y)
                        {
                            if ((Style == "A") || (Style == "C"))
                            {
                                sheet.GetRow(row1).GetCell(21 - cell).SetCellValue(SuppCD);
                                sheet.GetRow(row1 + 1).GetCell(21 - cell).SetCellValue(SuppAbbr);
                                sheet.GetRow(row1 + 2).GetCell(21 - cell).SetCellValue(Dock);
                                sheet.GetRow(row1 + 3).GetCell(21 - cell).SetCellValue(Ord);
                                sheet.GetRow(row1 + 4).GetCell(21 - cell).SetCellValue(detailNum);
                            }
                            if ((Style == "B") || (Style == "C"))
                            {
                                sheet.GetRow(row2).GetCell(cell + 1).SetCellValue(SuppCD);
                                sheet.GetRow(row2 + 1).GetCell(cell + 1).SetCellValue(SuppAbbr);
                                sheet.GetRow(row2 + 2).GetCell(cell + 1).SetCellValue(Dock);
                                sheet.GetRow(row2 + 3).GetCell(cell + 1).SetCellValue(Ord);
                                sheet.GetRow(row2 + 4).GetCell(cell + 1).SetCellValue(detailNum);
                            }
                        }
                        #endregion

                        #region Create new row for detail data
                        else
                        {
                            if ((cell == 1) && (over == 1))
                            {
                                #region Set Header if Detail Data more than 20
                                if ((Style == "A") || (Style == "C"))
                                {
                                    #region Side A
                                    sheet.CreateRow(row1).CreateCell(15).SetCellValue("DOCK");
                                    sheet.GetRow(row1).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row1, row1, 15, 16));
                                    sheet.GetRow(row1).CreateCell(17).SetCellValue(DockCD);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1, row1, 17, 20));
                                    sheet.GetRow(row1).CreateCell(21).SetCellValue(No);
                                    //sheet.GetRow(row1).GetCell(20).CellStyle.SetFont(Font1);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1, row1 + 4, 21, 21));
                                    sheet.CreateRow(row1 + 1).CreateCell(15).SetCellValue("ROUTE");
                                    sheet.GetRow(row1 + 1).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 1, row1 + 1, 15, 16));
                                    sheet.GetRow(row1 + 1).CreateCell(17).SetCellValue(Route);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 1, row1 + 1, 17, 20));
                                    sheet.CreateRow(row1 + 2).CreateCell(9).SetCellValue("PROD. DATE");
                                    sheet.GetRow(row1 + 2).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 9, 10));
                                    sheet.GetRow(row1 + 2).CreateCell(11).SetCellValue(Date);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 11, 14));
                                    sheet.GetRow(row1 + 2).CreateCell(15).SetCellValue("TRUCK STATION");
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 15, 16));
                                    sheet.GetRow(row1 + 2).CreateCell(17).SetCellValue(Terminal);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 2, row1 + 2, 17, 20));
                                    sheet.CreateRow(row1 + 3).CreateCell(9).SetCellValue("LOGISTIC PARTNER");
                                    sheet.GetRow(row1 + 3).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 9, 10));
                                    sheet.GetRow(row1 + 3).CreateCell(11).SetCellValue(Logp);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 11, 14));
                                    sheet.GetRow(row1 + 3).CreateCell(15).SetCellValue("ARRIVAL");
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 15, 16));
                                    sheet.GetRow(row1 + 3).CreateCell(17).SetCellValue(Arr + " " + ArrFri);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 3, row1 + 3, 17, 20));
                                    sheet.CreateRow(row1 + 4).CreateCell(9).SetCellValue("CYCLE ISSUE");
                                    sheet.GetRow(row1 + 4).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 9, 10));
                                    sheet.GetRow(row1 + 4).CreateCell(11).SetCellValue(Cycle);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 11, 14));
                                    sheet.GetRow(row1 + 4).CreateCell(15).SetCellValue("DEPARTURE");
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 15, 16));
                                    sheet.GetRow(row1 + 4).CreateCell(17).SetCellValue(Dep + " " + DepFri);
                                    sheet.AddMergedRegion(new CellRangeAddress(row1 + 4, row1 + 4, 17, 20));
                                    if (Style == "C") sheet.GetRow(row1 + 4).CreateCell(1).SetCellValue("Side A");
                                    #endregion

                                    #region Side A Styling
                                    sheet.GetRow(row1).GetCell(15).CellStyle = styleContent1;
                                    sheet.GetRow(row1).CreateCell(16).CellStyle = styleContent2;
                                    sheet.GetRow(row1).GetCell(17).CellStyle = styleContent6;
                                    sheet.GetRow(row1).CreateCell(18).CellStyle = styleContent7;
                                    sheet.GetRow(row1).CreateCell(19).CellStyle = styleContent7;
                                    sheet.GetRow(row1).CreateCell(20).CellStyle = styleContent7;
                                    sheet.GetRow(row1).GetCell(21).CellStyle = styleContent3;
                                    sheet.GetRow(row1 + 1).GetCell(15).CellStyle = styleContent1;
                                    sheet.GetRow(row1 + 1).CreateCell(16).CellStyle = styleContent2;
                                    sheet.GetRow(row1 + 1).GetCell(17).CellStyle = styleContent6;
                                    sheet.GetRow(row1 + 1).CreateCell(18).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 1).CreateCell(19).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 1).CreateCell(20).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 1).CreateCell(21).CellStyle = styleContent3;
                                    sheet.GetRow(row1 + 2).GetCell(9).CellStyle = styleContent1;
                                    sheet.GetRow(row1 + 2).CreateCell(10).CellStyle = styleContent2;
                                    sheet.GetRow(row1 + 2).GetCell(11).CellStyle = styleContent6;
                                    sheet.GetRow(row1 + 2).CreateCell(12).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 2).CreateCell(13).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 2).CreateCell(14).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 2).GetCell(15).CellStyle = styleContent1;
                                    sheet.GetRow(row1 + 2).CreateCell(16).CellStyle = styleContent2;
                                    sheet.GetRow(row1 + 2).GetCell(17).CellStyle = styleContent6;
                                    sheet.GetRow(row1 + 2).CreateCell(18).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 2).CreateCell(19).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 2).CreateCell(20).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 2).CreateCell(21).CellStyle = styleContent3;
                                    sheet.GetRow(row1 + 3).GetCell(9).CellStyle = styleContent1;
                                    sheet.GetRow(row1 + 3).CreateCell(10).CellStyle = styleContent2;
                                    sheet.GetRow(row1 + 3).GetCell(11).CellStyle = styleContent6;
                                    sheet.GetRow(row1 + 3).CreateCell(12).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 3).CreateCell(13).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 3).CreateCell(14).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 3).GetCell(15).CellStyle = styleContent1;
                                    sheet.GetRow(row1 + 3).CreateCell(16).CellStyle = styleContent2;
                                    sheet.GetRow(row1 + 3).GetCell(17).CellStyle = styleContent6;
                                    sheet.GetRow(row1 + 3).CreateCell(18).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 3).CreateCell(19).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 3).CreateCell(20).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 3).CreateCell(21).CellStyle = styleContent3;
                                    sheet.GetRow(row1 + 4).GetCell(9).CellStyle = styleContent1;
                                    sheet.GetRow(row1 + 4).CreateCell(10).CellStyle = styleContent2;
                                    sheet.GetRow(row1 + 4).GetCell(11).CellStyle = styleContent6;
                                    sheet.GetRow(row1 + 4).CreateCell(12).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 4).CreateCell(13).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 4).CreateCell(14).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 4).GetCell(15).CellStyle = styleContent1;
                                    sheet.GetRow(row1 + 4).CreateCell(16).CellStyle = styleContent2;
                                    sheet.GetRow(row1 + 4).GetCell(17).CellStyle = styleContent6;
                                    sheet.GetRow(row1 + 4).CreateCell(18).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 4).CreateCell(19).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 4).CreateCell(20).CellStyle = styleContent7;
                                    sheet.GetRow(row1 + 4).CreateCell(21).CellStyle = styleContent3;
                                    if (Style == "C") sheet.GetRow(row1 + 4).GetCell(1).CellStyle = styleContent9;
                                    #endregion
                                }

                                if ((Style == "B") || (Style == "C"))
                                {
                                    #region Side B
                                    sheet.CreateRow(row2).CreateCell(1).SetCellValue(No);
                                    sheet.GetRow(row2).Height = RowHeight;
                                    //sheet.GetRow(row2).GetCell(1).CellStyle.SetFont(Font1);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2, row2 + 4, 1, 1));
                                    sheet.GetRow(row2).CreateCell(2).SetCellValue("DOCK");
                                    sheet.AddMergedRegion(new CellRangeAddress(row2, row2, 2, 3));
                                    sheet.GetRow(row2).CreateCell(4).SetCellValue(DockCD);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2, row2, 4, 7));
                                    sheet.CreateRow(row2 + 1).CreateCell(2).SetCellValue("ROUTE");
                                    sheet.GetRow(row2 + 1).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 1, row2 + 1, 2, 3));
                                    sheet.GetRow(row2 + 1).CreateCell(4).SetCellValue(Route);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 1, row2 + 1, 4, 7));
                                    sheet.CreateRow(row2 + 2).CreateCell(2).SetCellValue("TRUCK STATION");
                                    sheet.GetRow(row2 + 2).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 2, 3));
                                    sheet.GetRow(row2 + 2).CreateCell(4).SetCellValue(Terminal);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 4, 7));
                                    sheet.GetRow(row2 + 2).CreateCell(8).SetCellValue("PROD. DATE");
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 8, 9));
                                    sheet.GetRow(row2 + 2).CreateCell(10).SetCellValue(Date);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 2, row2 + 2, 10, 13));
                                    sheet.CreateRow(row2 + 3).CreateCell(2).SetCellValue("ARRIVAL");
                                    sheet.GetRow(row2 + 3).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 2, 3));
                                    sheet.GetRow(row2 + 3).CreateCell(4).SetCellValue(Arr + " " + ArrFri);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 4, 7));
                                    sheet.GetRow(row2 + 3).CreateCell(8).SetCellValue("LOGISTIC PARTNER");
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 8, 9));
                                    sheet.GetRow(row2 + 3).CreateCell(10).SetCellValue(Logp);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 3, row2 + 3, 10, 13));
                                    sheet.CreateRow(row2 + 4).CreateCell(2).SetCellValue("DEPARTURE");
                                    sheet.GetRow(row2 + 4).Height = RowHeight;
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 2, 3));
                                    sheet.GetRow(row2 + 4).CreateCell(4).SetCellValue(Dep + " " + DepFri);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 4, 7));
                                    sheet.GetRow(row2 + 4).CreateCell(8).SetCellValue("CYCLE ISSUE");
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 8, 9));
                                    sheet.GetRow(row2 + 4).CreateCell(10).SetCellValue(Cycle);
                                    sheet.AddMergedRegion(new CellRangeAddress(row2 + 4, row2 + 4, 10, 13));
                                    if (Style == "C") sheet.GetRow(row2 + 4).CreateCell(21).SetCellValue("Side B");
                                    #endregion

                                    #region Side B Styling
                                    sheet.GetRow(row2).GetCell(2).CellStyle = styleContent1;
                                    sheet.GetRow(row2).CreateCell(3).CellStyle = styleContent2;
                                    sheet.GetRow(row2).GetCell(4).CellStyle = styleContent6;
                                    sheet.GetRow(row2).CreateCell(5).CellStyle = styleContent7;
                                    sheet.GetRow(row2).CreateCell(6).CellStyle = styleContent7;
                                    sheet.GetRow(row2).CreateCell(7).CellStyle = styleContent7;
                                    sheet.GetRow(row2).GetCell(1).CellStyle = styleContent3;
                                    sheet.GetRow(row2 + 1).GetCell(2).CellStyle = styleContent1;
                                    sheet.GetRow(row2 + 1).CreateCell(3).CellStyle = styleContent2;
                                    sheet.GetRow(row2 + 1).GetCell(4).CellStyle = styleContent6;
                                    sheet.GetRow(row2 + 1).CreateCell(5).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 1).CreateCell(6).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 1).CreateCell(7).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 1).CreateCell(1).CellStyle = styleContent3;
                                    sheet.GetRow(row2 + 2).GetCell(8).CellStyle = styleContent1;
                                    sheet.GetRow(row2 + 2).CreateCell(9).CellStyle = styleContent2;
                                    sheet.GetRow(row2 + 2).GetCell(10).CellStyle = styleContent6;
                                    sheet.GetRow(row2 + 2).CreateCell(11).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 2).CreateCell(12).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 2).CreateCell(13).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 2).GetCell(2).CellStyle = styleContent1;
                                    sheet.GetRow(row2 + 2).CreateCell(3).CellStyle = styleContent2;
                                    sheet.GetRow(row2 + 2).GetCell(4).CellStyle = styleContent6;
                                    sheet.GetRow(row2 + 2).CreateCell(5).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 2).CreateCell(6).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 2).CreateCell(7).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 2).CreateCell(1).CellStyle = styleContent3;
                                    sheet.GetRow(row2 + 3).GetCell(8).CellStyle = styleContent1;
                                    sheet.GetRow(row2 + 3).CreateCell(9).CellStyle = styleContent2;
                                    sheet.GetRow(row2 + 3).GetCell(10).CellStyle = styleContent6;
                                    sheet.GetRow(row2 + 3).CreateCell(11).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 3).CreateCell(12).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 3).CreateCell(13).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 3).GetCell(2).CellStyle = styleContent1;
                                    sheet.GetRow(row2 + 3).CreateCell(3).CellStyle = styleContent2;
                                    sheet.GetRow(row2 + 3).GetCell(4).CellStyle = styleContent6;
                                    sheet.GetRow(row2 + 3).CreateCell(5).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 3).CreateCell(6).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 3).CreateCell(7).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 3).CreateCell(1).CellStyle = styleContent3;
                                    sheet.GetRow(row2 + 4).GetCell(8).CellStyle = styleContent1;
                                    sheet.GetRow(row2 + 4).CreateCell(9).CellStyle = styleContent2;
                                    sheet.GetRow(row2 + 4).GetCell(10).CellStyle = styleContent6;
                                    sheet.GetRow(row2 + 4).CreateCell(11).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 4).CreateCell(12).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 4).CreateCell(13).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 4).GetCell(2).CellStyle = styleContent1;
                                    sheet.GetRow(row2 + 4).CreateCell(3).CellStyle = styleContent2;
                                    sheet.GetRow(row2 + 4).GetCell(4).CellStyle = styleContent6;
                                    sheet.GetRow(row2 + 4).CreateCell(5).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 4).CreateCell(6).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 4).CreateCell(7).CellStyle = styleContent7;
                                    sheet.GetRow(row2 + 4).CreateCell(1).CellStyle = styleContent3;
                                    if (Style == "C") sheet.GetRow(row2 + 4).GetCell(21).CellStyle = styleContent9;
                                    #endregion
                                }
                                #endregion

                                #region Create new row for Detail
                                if ((Style == "A") || (Style == "C"))
                                {
                                    row1 = row1 + 5;
                                }
                                if ((Style == "B") || (Style == "C"))
                                {
                                    row2 = row2 + 5;
                                }

                                if ((Style == "A") || (Style == "C"))
                                {
                                    Hrow = sheet.CreateRow(row1);
                                    sheet.GetRow(row1).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row1 + 1);
                                    sheet.GetRow(row1 + 1).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row1 + 2);
                                    sheet.GetRow(row1 + 2).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row1 + 3);
                                    sheet.GetRow(row1 + 3).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row1 + 4);
                                    sheet.GetRow(row1 + 4).Height = RowHeight;

                                    sheet.GetRow(row1).CreateCell(21).SetCellValue("SUPP CODE");
                                    sheet.GetRow(row1 + 1).CreateCell(21).SetCellValue("SUPP NAME");
                                    sheet.GetRow(row1 + 2).CreateCell(21).SetCellValue("DOCK");
                                    sheet.GetRow(row1 + 3).CreateCell(21).SetCellValue("#ORDER");
                                    sheet.GetRow(row1 + 4).CreateCell(21).SetCellValue("NO");
                                    sheet.GetRow(row1).GetCell(21).CellStyle = styleContent51;
                                    sheet.GetRow(row1 + 1).GetCell(21).CellStyle = styleContent51;
                                    sheet.GetRow(row1 + 2).GetCell(21).CellStyle = styleContent51;
                                    sheet.GetRow(row1 + 3).GetCell(21).CellStyle = styleContent51;
                                    sheet.GetRow(row1 + 4).GetCell(21).CellStyle = styleContent51;
                                }

                                if ((Style == "B") || (Style == "C"))
                                {
                                    Hrow = sheet.CreateRow(row2);
                                    sheet.GetRow(row2).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row2 + 1);
                                    sheet.GetRow(row2 + 1).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row2 + 2);
                                    sheet.GetRow(row2 + 1).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row2 + 3);
                                    sheet.GetRow(row2 + 1).Height = RowHeight;
                                    Hrow = sheet.CreateRow(row2 + 4);
                                    sheet.GetRow(row2 + 4).Height = RowHeight;

                                    sheet.GetRow(row2).CreateCell(1).SetCellValue("SUPP CODE");
                                    sheet.GetRow(row2 + 1).CreateCell(1).SetCellValue("SUPP NAME");
                                    sheet.GetRow(row2 + 2).CreateCell(1).SetCellValue("DOCK");
                                    sheet.GetRow(row2 + 3).CreateCell(1).SetCellValue("#ORDER");
                                    sheet.GetRow(row2 + 4).CreateCell(1).SetCellValue("NO");
                                    sheet.GetRow(row2).GetCell(1).CellStyle = styleContent51;
                                    sheet.GetRow(row2 + 1).GetCell(1).CellStyle = styleContent51;
                                    sheet.GetRow(row2 + 2).GetCell(1).CellStyle = styleContent51;
                                    sheet.GetRow(row2 + 3).GetCell(1).CellStyle = styleContent51;
                                    sheet.GetRow(row2 + 4).GetCell(1).CellStyle = styleContent51;
                                }
                                #endregion

                                counter++;
                            }

                            #region Fetch Detail Data
                            if ((Style == "A") || (Style == "C"))
                            {
                                sheet.GetRow(row1).CreateCell(21 - cell).SetCellValue(SuppCD);
                                sheet.GetRow(row1 + 1).CreateCell(21 - cell).SetCellValue(SuppAbbr);
                                sheet.GetRow(row1 + 2).CreateCell(21 - cell).SetCellValue(Dock);
                                sheet.GetRow(row1 + 3).CreateCell(21 - cell).SetCellValue(Ord);
                                sheet.GetRow(row1 + 4).CreateCell(21 - cell).SetCellValue(detailNum);
                                sheet.GetRow(row1).GetCell(21 - cell).CellStyle = styleContent5;
                                sheet.GetRow(row1 + 1).GetCell(21 - cell).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 2).GetCell(21 - cell).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 3).GetCell(21 - cell).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 4).GetCell(21 - cell).CellStyle = styleContent41;
                            }

                            if ((Style == "B") || (Style == "C"))
                            {
                                sheet.GetRow(row2).CreateCell(cell + 1).SetCellValue(SuppCD);
                                sheet.GetRow(row2 + 1).CreateCell(cell + 1).SetCellValue(SuppAbbr);
                                sheet.GetRow(row2 + 2).CreateCell(cell + 1).SetCellValue(Dock);
                                sheet.GetRow(row2 + 3).CreateCell(cell + 1).SetCellValue(Ord);
                                sheet.GetRow(row2 + 4).CreateCell(cell + 1).SetCellValue(detailNum);
                                sheet.GetRow(row2).GetCell(cell + 1).CellStyle = styleContent5;
                                sheet.GetRow(row2 + 1).GetCell(cell + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 2).GetCell(cell + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 3).GetCell(cell + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 4).GetCell(cell + 1).CellStyle = styleContent41;
                            }
                            #endregion
                        }
                        #endregion

                        cell++;

                        #region Summary Detail Checking
                        modeldetail.Last();
                        if ((cell > 20) && (!resultdetail.Equals(modeldetail.Last())))
                        {
                            #region Create Dashed Border
                            if ((Style == "A") || (Style == "C"))
                            {
                                sheet.CreateRow(row1 + 5).RowStyle = styleContent8;
                                sheet.GetRow(row1 + 5).Height = RowHeight;
                            }
                            if ((Style == "B") || (Style == "C"))
                            {
                                sheet.CreateRow(row2 + 5).RowStyle = styleContent8;
                                sheet.GetRow(row2 + 5).Height = RowHeight;
                            }
                            #endregion

                            if (Style == "C")
                            {
                                row1 = row1 + 19;
                                row2 = row2 + 19;
                            }
                            if (Style == "A")
                            {
                                row1 = row1 + 7;
                            }
                            if (Style == "B")
                            {
                                row2 = row2 + 7;
                            }
                            cell = 1;
                            over = 1;
                        }
                        #endregion

                        detailNum++;
                    }
                    #endregion

                    #region Styling Empty Detail Column
                    if (modeldetail.Count() == 0)
                    {
                        for (int i = 1; i <= 20; i++)
                        {
                            if ((Style == "A") || (Style == "C"))
                            {
                                sheet.GetRow(row1).CreateCell(21 - i).CellStyle = styleContent5;
                                sheet.GetRow(row1 + 1).CreateCell(21 - i).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 2).CreateCell(21 - i).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 3).CreateCell(21 - i).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 4).CreateCell(21 - i).CellStyle = styleContent4;
                            }
                            if ((Style == "B") || (Style == "C"))
                            {
                                sheet.GetRow(row2).CreateCell(i + 1).CellStyle = styleContent5;
                                sheet.GetRow(row2 + 1).CreateCell(i + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 2).CreateCell(i + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 3).CreateCell(i + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 4).CreateCell(i + 1).CellStyle = styleContent4;
                            }
                        }
                    }
                    #endregion

                    #region Styling Empty Detail Column (detail < 20)
                    if ((cell < 21) && (modeldetail.Count() > 0) && (cell != 1))
                    {
                        for (int i = cell; i <= 20; i++)
                        {
                            if ((Style == "A") || (Style == "C"))
                            {
                                sheet.GetRow(row1).CreateCell(21 - i).CellStyle = styleContent5;
                                sheet.GetRow(row1 + 1).CreateCell(21 - i).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 2).CreateCell(21 - i).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 3).CreateCell(21 - i).CellStyle = styleContent4;
                                sheet.GetRow(row1 + 4).CreateCell(21 - i).CellStyle = styleContent4;
                            }
                            if ((Style == "B") || (Style == "C"))
                            {
                                sheet.GetRow(row2).CreateCell(i + 1).CellStyle = styleContent5;
                                sheet.GetRow(row2 + 1).CreateCell(i + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 2).CreateCell(i + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 3).CreateCell(i + 1).CellStyle = styleContent4;
                                sheet.GetRow(row2 + 4).CreateCell(i + 1).CellStyle = styleContent4;
                            }
                        }
                    }
                    #endregion

                    #region Create Dashed Border
                    if ((Style == "A") || (Style == "C"))
                    {
                        sheet.CreateRow(row1 + 5).RowStyle = styleContent8;
                        sheet.GetRow(row1 + 5).Height = RowHeight;
                    }
                    if ((Style == "B") || (Style == "C"))
                    {
                        sheet.CreateRow(row2 + 5).RowStyle = styleContent8;
                        sheet.GetRow(row2 + 5).Height = RowHeight;
                    }
                    #endregion

                    #region Get Next Row Number
                    if (Style == "C")
                    {
                        row1 = row1 + 19;
                        row2 = row2 + 19;
                    }
                    if (Style == "A")
                    {
                        row1 = row1 + 7;
                    }
                    if (Style == "B")
                    {
                        row2 = row2 + 7;
                    }
                    #endregion

                }

                #region Insert Page Number
                if (Style == "C") { x = (int)Math.Ceiling(((counter - 1) * 2) / 4M); }
                else { x = (int)Math.Ceiling((counter - 1) / 4M); }

                page = Math.Ceiling(x);
                r = 1;

                for (int l = 1; l <= page; l++)
                {
                    if ((Style == "C") || (Style == "A"))
                    {
                        if (sheet.GetRow(r) == null)
                        {
                            sheet.CreateRow(r).CreateCell(1).SetCellValue("Page " + l + "/" + page);
                            sheet.GetRow(r).GetCell(1).CellStyle = styleContent9;
                        }
                        sheet.GetRow(r).CreateCell(1).SetCellValue("Page " + l + "/" + page);
                        sheet.GetRow(r).GetCell(1).CellStyle = styleContent9;
                    }
                    else
                    {
                        if (sheet.GetRow(r) == null)
                        {
                            sheet.CreateRow(r).CreateCell(21).SetCellValue("Page " + l + "/" + page);
                            sheet.GetRow(r).GetCell(21).CellStyle = styleContent9;
                        }
                        sheet.GetRow(r).CreateCell(21).SetCellValue("Page " + l + "/" + page);
                        sheet.GetRow(r).GetCell(21).CellStyle = styleContent9;
                    }
                    r = r + 48;
                    sheet.SetRowBreak(r - 2);
                }
                #endregion

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();
            }
            catch (Exception err)
            {
                string x = err.ToString();
                byte[] a = null;
                x = err.ToString();
                return a;
            }
        }

        private byte[] PrintTruckSheet(string p_Dock, string p_DateFrom, string p_DateTo, string p_rep_a, string p_rep_b)
        {
            try
            {
                #region Initialization
                string filesTmp = "";
                filesTmp = HttpContext.Request.MapPath("~/Template/TruckSheet.xls");
                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);
                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);
                IRow Hrow;
                IDBContext db = DbContext;
                #endregion

                #region Styling
                //Row Data --> StyleContent1
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                //Not Used
                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 9;
                Font1.FontName = "Arial";

                //Header White Forklift (White) --> StyleContent3
                IFont Font2 = workbook.CreateFont();
                Font2.FontHeightInPoints = 18;
                Font2.FontName = "Arial";
                Font2.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                //Arrival Time White Forklift (Front) --> StyleContent34
                IFont Font21 = workbook.CreateFont();
                Font21.FontHeightInPoints = 14;
                Font21.FontName = "Arial";
                Font21.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                //Friday Arrival Time White Forklift (Front) --> StyleContent35
                IFont Font22 = workbook.CreateFont();
                Font22.FontHeightInPoints = 10;
                Font22.FontName = "Arial";
                Font22.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                //Sequence No --> StyleContent5
                IFont Font3 = workbook.CreateFont();
                Font3.FontHeightInPoints = 8;
                Font3.FontName = "Arial";

                //Detail Black Forklift (Rear) --> StyleContent8
                IFont Font5 = workbook.CreateFont();
                Font5.FontHeightInPoints = 9;
                Font5.FontName = "Arial";
                Font5.Color = HSSFColor.WHITE.index;

                //Header Black Forklift (Rear) --> StyleContent8
                IFont Font6 = workbook.CreateFont();
                Font6.FontHeightInPoints = 18;
                Font6.FontName = "Arial";
                Font6.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                Font6.Color = HSSFColor.WHITE.index;

                //Arrival Time Black Forklift --> StyleContent81
                IFont Font61 = workbook.CreateFont();
                Font61.FontHeightInPoints = 14;
                Font61.FontName = "Arial";
                Font61.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                Font61.Color = HSSFColor.WHITE.index;

                //Friday Arrival Time Black Forklift --> StyleContent82
                IFont Font62 = workbook.CreateFont();
                Font62.FontHeightInPoints = 10;
                Font62.FontName = "Arial";
                Font62.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                Font62.Color = HSSFColor.WHITE.index;

                //Row Data
                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                //Detail White Forklift (Front)
                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.SetFont(Font1);

                //'ARV' Forklift White (Front)
                ICellStyle styleContent23 = workbook.CreateCellStyle();
                styleContent23.VerticalAlignment = VerticalAlignment.BOTTOM;
                styleContent23.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent23.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent23.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent23.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent23.SetFont(Font1);
                styleContent23.Rotation = 90;

                //Header White Forklift (White)
                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.Alignment = HorizontalAlignment.CENTER;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font2);

                //Arrival Time White Forklift (Front)
                ICellStyle styleContent34 = workbook.CreateCellStyle();
                styleContent34.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent34.Alignment = HorizontalAlignment.CENTER;
                styleContent34.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent34.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent34.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent34.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent34.SetFont(Font21);

                //Friday Arrival Time White Forklift (Front)
                ICellStyle styleContent35 = workbook.CreateCellStyle();
                styleContent35.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent35.Alignment = HorizontalAlignment.CENTER;
                styleContent35.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent35.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent35.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent35.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent35.SetFont(Font22);

                //DASHED BORDER FOR SEPARATOR each Forklift Row
                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.BorderBottom = NPOI.SS.UserModel.BorderStyle.DASHED;

                //Sequence Number For each Forklift (Front and Rear)
                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.SetFont(Font3);

                //Vertical Right Dashed Border For Vertical Separator Forklift
                ICellStyle styleContent6 = workbook.CreateCellStyle();
                styleContent6.BorderRight = NPOI.SS.UserModel.BorderStyle.DASHED;
                styleContent6.BorderBottom = NPOI.SS.UserModel.BorderStyle.DASHED;

                //Vertical Left Dashed Border For Vertical Separator Forklift
                ICellStyle styleContent66 = workbook.CreateCellStyle();
                styleContent66.BorderLeft = NPOI.SS.UserModel.BorderStyle.DASHED;
                styleContent66.BorderBottom = NPOI.SS.UserModel.BorderStyle.DASHED;

                //Detail Black Forklift (Rear)
                ICellStyle styleContent7 = workbook.CreateCellStyle();
                styleContent7.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent7.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent7.TopBorderColor = HSSFColor.WHITE.index;
                styleContent7.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent7.RightBorderColor = HSSFColor.WHITE.index;
                styleContent7.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
                styleContent7.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent7.SetFont(Font5);

                //'ARV' Black Forklift (Rear) Style
                ICellStyle styleContent77 = workbook.CreateCellStyle();
                styleContent77.VerticalAlignment = VerticalAlignment.BOTTOM;
                styleContent77.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent77.TopBorderColor = HSSFColor.WHITE.index;
                styleContent77.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent77.RightBorderColor = HSSFColor.WHITE.index;
                styleContent77.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent77.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent77.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent77.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent77.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
                styleContent77.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent77.Rotation = 90;
                styleContent77.SetFont(Font5);

                //Header Black Forklift (Rear)
                ICellStyle styleContent8 = workbook.CreateCellStyle();
                styleContent8.VerticalAlignment = VerticalAlignment.TOP;
                styleContent8.Alignment = HorizontalAlignment.CENTER;
                styleContent8.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent8.TopBorderColor = HSSFColor.WHITE.index;
                styleContent8.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent8.RightBorderColor = HSSFColor.WHITE.index;
                styleContent8.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent8.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
                styleContent8.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent8.SetFont(Font6);

                //Arrival Time Black Forklift (Rear)
                ICellStyle styleContent81 = workbook.CreateCellStyle();
                styleContent81.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent81.Alignment = HorizontalAlignment.CENTER;
                styleContent81.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent81.TopBorderColor = HSSFColor.WHITE.index;
                styleContent81.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent81.RightBorderColor = HSSFColor.WHITE.index;
                styleContent81.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent81.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent81.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent81.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent81.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
                styleContent81.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent81.SetFont(Font61);

                //Friday Arrival Time Black Forklift (Rear)
                ICellStyle styleContent82 = workbook.CreateCellStyle();
                styleContent82.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent82.Alignment = HorizontalAlignment.CENTER;
                styleContent82.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent82.TopBorderColor = HSSFColor.WHITE.index;
                styleContent82.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent82.RightBorderColor = HSSFColor.WHITE.index;
                styleContent82.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent82.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent82.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent82.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent82.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
                styleContent82.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent82.SetFont(Font62);
                #endregion

                #region Truck Sheet
                ISheet sheet = workbook.GetSheet("TS");
                sheet.ForceFormulaRecalculation = true;

                int row = 1;
                int longcount = 1;

                List<TruckSheet> modelTruck = new List<TruckSheet>();

                modelTruck = DbContext.Fetch<TruckSheet>("GetTruckSheet", new object[] { p_Dock, p_rep_a, p_rep_b }); // select header
                foreach (var result in modelTruck)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(row);
                    Hrow.CreateCell(1).SetCellValue(result.Route);
                    Hrow.CreateCell(2).SetCellValue(result.Del_No);
                    Hrow.CreateCell(3).SetCellValue(result.Door_CD);
                    Hrow.CreateCell(4).SetCellValue(result.Physical_Station);
                    Hrow.CreateCell(5).SetCellValue(result.Arrvdatetime);
                    Hrow.CreateCell(6).SetCellValue(result.Dptdatetime);
                    Hrow.CreateCell(7).SetCellValue(result.ArrvFri);
                    Hrow.CreateCell(8).SetCellValue(result.DeptFri);
                    Hrow.CreateCell(9).SetCellValue(result.LogpartnerCD);
                    Hrow.CreateCell(10).SetCellValue(result.Supplier);

                    Hrow.GetCell(0).CellStyle = styleContent1;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent1;
                    Hrow.GetCell(3).CellStyle = styleContent1;
                    Hrow.GetCell(4).CellStyle = styleContent1;
                    Hrow.GetCell(5).CellStyle = styleContent1;
                    Hrow.GetCell(6).CellStyle = styleContent1;
                    Hrow.GetCell(7).CellStyle = styleContent1;
                    Hrow.GetCell(8).CellStyle = styleContent1;
                    Hrow.GetCell(9).CellStyle = styleContent1;
                    Hrow.GetCell(10).CellStyle = styleContent1;

                    row = row + 1;
                }
                #endregion

                #region Forklift A
                ISheet sheet2 = workbook.GetSheet("Forklift_A");
                sheet2.ForceFormulaRecalculation = true;
                List<TruckSheet> modelFork = new List<TruckSheet>();
                List<TruckSheet> detailFork = new List<TruckSheet>();

                modelFork = DbContext.Fetch<TruckSheet>("GetForkHeader", new object[] { p_Dock, p_rep_a, p_rep_b });
                row = 1;
                int cell = 1;
                int sup = 1;
                int c = 1;
                int countA = 0;

                short HeightHead = 430;
                short HeightDetail = 215;

                Hrow = sheet2.CreateRow(row - 1);
                sheet2.GetRow(row - 1).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row);
                sheet2.GetRow(row).Height = HeightHead;
                Hrow = sheet2.CreateRow(row + 1);
                sheet2.GetRow(row + 1).Height = HeightHead;
                Hrow = sheet2.CreateRow(row + 2);
                sheet2.GetRow(row + 2).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 3);
                sheet2.GetRow(row + 3).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 4);
                sheet2.GetRow(row + 4).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 5);
                sheet2.GetRow(row + 5).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 6);
                sheet2.GetRow(row + 6).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 7);
                sheet2.GetRow(row + 7).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 8);
                sheet2.GetRow(row + 8).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 9);
                sheet2.GetRow(row + 9).Height = HeightDetail;
                Hrow = sheet2.CreateRow(row + 10);
                sheet2.GetRow(row + 10).Height = HeightHead;

                foreach (var result in modelFork)
                {
                    sheet2.GetRow(row - 1).CreateCell(cell).SetCellValue(longcount);
                    sheet2.GetRow(row - 1).GetCell(cell).CellStyle = styleContent5;
                    sheet2.GetRow(row).CreateCell(cell).SetCellValue(result.Route);
                    sheet2.GetRow(row).CreateCell(cell + 1).SetCellValue("");
                    sheet2.GetRow(row).CreateCell(cell + 2).SetCellValue("");
                    sheet2.AddMergedRegion(new CellRangeAddress(row, row, cell, cell + 2));
                    sheet2.GetRow(row).GetCell(cell).CellStyle = styleContent3;
                    sheet2.GetRow(row).GetCell(cell + 1).CellStyle = styleContent3;
                    sheet2.GetRow(row).GetCell(cell + 2).CellStyle = styleContent3;

                    sheet2.GetRow(row + 1).CreateCell(cell).SetCellValue("No");
                    sheet2.GetRow(row + 1).CreateCell(cell + 1).SetCellValue("T/S " + result.Physical_Station);
                    sheet2.GetRow(row + 1).CreateCell(cell + 2).SetCellValue("");
                    sheet2.AddMergedRegion(new CellRangeAddress(row + 1, row + 1, cell + 1, cell + 2));
                    sheet2.GetRow(row + 1).GetCell(cell).CellStyle = styleContent2;
                    sheet2.GetRow(row + 1).GetCell(cell + 1).CellStyle = styleContent3;
                    sheet2.GetRow(row + 1).GetCell(cell + 2).CellStyle = styleContent3;

                    for (sup = 2; sup < 10; sup++)
                    {
                        sheet2.GetRow(row + sup).CreateCell(cell).SetCellValue(sup - 1);
                        sheet2.GetRow(row + sup).CreateCell(cell + 1).SetCellValue("");
                        sheet2.GetRow(row + sup).CreateCell(cell + 2).SetCellValue("");
                        sheet2.GetRow(row + sup).GetCell(cell).CellStyle = styleContent2;
                        sheet2.GetRow(row + sup).GetCell(cell + 1).CellStyle = styleContent2;
                        sheet2.GetRow(row + sup).GetCell(cell + 2).CellStyle = styleContent2;
                    }

                    sup = 2;
                    detailFork = DbContext.Fetch<TruckSheet>("GetForkDetail", new object[] { result.RouteCode, result.Del_No, p_rep_a, p_rep_b, p_Dock });
                    foreach (var d in detailFork)
                    {
                        //sheet2.GetRow(row + sup).GetCell(cell).SetCellValue(d.SuppCD);
                        //sheet2.GetRow(row + sup).GetCell(cell).CellStyle = styleContent2;
                        sheet2.GetRow(row + sup).GetCell(cell + 1).SetCellValue(d.SuppCD);
                        sheet2.GetRow(row + sup).GetCell(cell + 1).CellStyle = styleContent2;
                        sheet2.GetRow(row + sup).GetCell(cell + 2).SetCellValue(d.Supp);
                        sheet2.GetRow(row + sup).GetCell(cell + 2).CellStyle = styleContent2;
                        sup++;
                    }
                    sup = 10;
                    sheet2.GetRow(row + sup).CreateCell(cell).SetCellValue("ARV");
                    sheet2.GetRow(row + sup).CreateCell(cell + 1).SetCellValue(result.Arrvdatetime);
                    sheet2.GetRow(row + sup).CreateCell(cell + 2).SetCellValue("Fri:" + result.ArrvFri);
                    //sheet2.AddMergedRegion(new CellRangeAddress(row + sup, row + sup, cell + 1, cell + 2));
                    sheet2.GetRow(row + sup).GetCell(cell).CellStyle = styleContent23;
                    sheet2.GetRow(row + sup).GetCell(cell + 1).CellStyle = styleContent34;
                    sheet2.GetRow(row + sup).GetCell(cell + 2).CellStyle = styleContent35;

                    cell = cell + 5;
                    c++;
                    longcount++;
                    if (c == 6)
                    {
                        c = 1;
                        countA++;
                        cell = 1;
                        sheet2.CreateRow(row + 11).RowStyle = styleContent4;
                        sheet2.GetRow(row + 11).Height = HeightDetail;

                        int e = 0;
                        for (int d = 1; d <= 5; d++)
                        {
                            sheet2.GetRow(row + 11).CreateCell(e).CellStyle = styleContent66;
                            e = (5 * d);
                        }
                        sheet2.GetRow(row + 11).CreateCell(24).CellStyle = styleContent6;

                        if (countA % 6 == 0) sheet2.SetRowBreak(row + 11);
                        //sheet2.GetRow(row + 10).CreateCell(25).CellStyle = styleContent66;

                        row = row + 13;

                        Hrow = sheet2.CreateRow(row - 1);
                        sheet2.GetRow(row - 1).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row);
                        sheet2.GetRow(row).Height = HeightHead;
                        Hrow = sheet2.CreateRow(row + 1);
                        sheet2.GetRow(row + 1).Height = HeightHead;
                        Hrow = sheet2.CreateRow(row + 2);
                        sheet2.GetRow(row + 2).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 3);
                        sheet2.GetRow(row + 3).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 4);
                        sheet2.GetRow(row + 4).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 5);
                        sheet2.GetRow(row + 5).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 6);
                        sheet2.GetRow(row + 6).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 7);
                        sheet2.GetRow(row + 7).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 8);
                        sheet2.GetRow(row + 8).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 9);
                        sheet2.GetRow(row + 9).Height = HeightDetail;
                        Hrow = sheet2.CreateRow(row + 10);
                        sheet2.GetRow(row + 10).Height = HeightHead;
                    }

                }

                cell = 1;
                sheet2.CreateRow(row + 11).RowStyle = styleContent4;
                sheet2.GetRow(row + 11).Height = HeightDetail;

                int f = 0;
                for (int d = 1; d <= 5; d++)
                {
                    sheet2.GetRow(row + 11).CreateCell(f).CellStyle = styleContent66;
                    f = (5 * d);
                }
                sheet2.GetRow(row + 11).CreateCell(24).CellStyle = styleContent6;
                #endregion

                #region Forklift B
                ISheet sheet3 = workbook.GetSheet("Forklift_B");
                sheet3.ForceFormulaRecalculation = true;

                modelFork = DbContext.Fetch<TruckSheet>("GetForkHeader", new object[] { p_Dock, p_rep_a, p_rep_b });
                row = 1;
                cell = 1;
                sup = 1;
                c = 1;
                longcount = 1;
                countA = 0;

                HeightHead = 430;
                HeightDetail = 215;

                Hrow = sheet3.CreateRow(row - 1);
                sheet3.GetRow(row - 1).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row);
                sheet3.GetRow(row).Height = HeightHead;
                Hrow = sheet3.CreateRow(row + 1);
                sheet3.GetRow(row + 1).Height = HeightHead;
                Hrow = sheet3.CreateRow(row + 2);
                sheet3.GetRow(row + 2).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 3);
                sheet3.GetRow(row + 3).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 4);
                sheet3.GetRow(row + 4).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 5);
                sheet3.GetRow(row + 5).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 6);
                sheet3.GetRow(row + 6).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 7);
                sheet3.GetRow(row + 7).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 8);
                sheet3.GetRow(row + 8).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 9);
                sheet3.GetRow(row + 9).Height = HeightDetail;
                Hrow = sheet3.CreateRow(row + 10);
                sheet3.GetRow(row + 10).Height = HeightHead;

                foreach (var result in modelFork)
                {
                    sheet3.GetRow(row - 1).CreateCell(cell).SetCellValue(longcount);
                    sheet3.GetRow(row - 1).GetCell(cell).CellStyle = styleContent5;
                    sheet3.GetRow(row).CreateCell(cell).SetCellValue(result.Route);
                    sheet3.GetRow(row).CreateCell(cell + 1).SetCellValue("");
                    sheet3.GetRow(row).CreateCell(cell + 2).SetCellValue("");
                    sheet3.AddMergedRegion(new CellRangeAddress(row, row, cell, cell + 2));
                    sheet3.GetRow(row).GetCell(cell).CellStyle = styleContent8;
                    sheet3.GetRow(row).GetCell(cell + 1).CellStyle = styleContent8;
                    sheet3.GetRow(row).GetCell(cell + 2).CellStyle = styleContent8;

                    sheet3.GetRow(row + 1).CreateCell(cell).SetCellValue("No");
                    sheet3.GetRow(row + 1).CreateCell(cell + 1).SetCellValue("T/S " + result.Physical_Station);
                    sheet3.GetRow(row + 1).CreateCell(cell + 2).SetCellValue("");
                    sheet3.AddMergedRegion(new CellRangeAddress(row + 1, row + 1, cell + 1, cell + 2));
                    sheet3.GetRow(row + 1).GetCell(cell).CellStyle = styleContent7;
                    sheet3.GetRow(row + 1).GetCell(cell + 1).CellStyle = styleContent8;
                    sheet3.GetRow(row + 1).GetCell(cell + 2).CellStyle = styleContent8;

                    for (sup = 2; sup < 10; sup++)
                    {
                        sheet3.GetRow(row + sup).CreateCell(cell).SetCellValue(sup - 1);
                        sheet3.GetRow(row + sup).CreateCell(cell + 1).SetCellValue("");
                        sheet3.GetRow(row + sup).CreateCell(cell + 2).SetCellValue("");
                        sheet3.GetRow(row + sup).GetCell(cell).CellStyle = styleContent7;
                        sheet3.GetRow(row + sup).GetCell(cell + 1).CellStyle = styleContent7;
                        sheet3.GetRow(row + sup).GetCell(cell + 2).CellStyle = styleContent7;
                    }

                    sup = 2;
                    detailFork = DbContext.Fetch<TruckSheet>("GetForkDetail", new object[] { result.RouteCode, result.Del_No, p_rep_a, p_rep_b, p_Dock });
                    foreach (var d in detailFork)
                    {
                        sheet3.GetRow(row + sup).GetCell(cell + 1).SetCellValue(d.SuppCD);
                        sheet3.GetRow(row + sup).GetCell(cell + 1).CellStyle = styleContent7;
                        sheet3.GetRow(row + sup).GetCell(cell + 2).SetCellValue(d.Supp);
                        sheet3.GetRow(row + sup).GetCell(cell + 2).CellStyle = styleContent7;
                        sup++;
                    }
                    sup = 10;
                    sheet3.GetRow(row + sup).CreateCell(cell).SetCellValue("ARV");
                    sheet3.GetRow(row + sup).CreateCell(cell + 1).SetCellValue(result.Arrvdatetime);
                    sheet3.GetRow(row + sup).CreateCell(cell + 2).SetCellValue("Fri:" + result.ArrvFri);
                    //sheet3.AddMergedRegion(new CellRangeAddress(row + sup, row + sup, cell + 1, cell + 2));
                    sheet3.GetRow(row + sup).GetCell(cell).CellStyle = styleContent77; //Ganti
                    sheet3.GetRow(row + sup).GetCell(cell + 1).CellStyle = styleContent81;
                    sheet3.GetRow(row + sup).GetCell(cell + 2).CellStyle = styleContent82;

                    cell = cell + 5;
                    c++;
                    longcount++;
                    if (c == 6)
                    {
                        c = 1;
                        countA++;
                        cell = 1;
                        sheet3.CreateRow(row + 11).RowStyle = styleContent4;
                        sheet3.GetRow(row + 11).Height = HeightDetail;

                        int e = 0;
                        for (int d = 1; d <= 5; d++)
                        {
                            sheet3.GetRow(row + 11).CreateCell(e).CellStyle = styleContent66;
                            e = (5 * d);
                        }
                        sheet3.GetRow(row + 11).CreateCell(24).CellStyle = styleContent6;
                        if (countA % 6 == 0) sheet3.SetRowBreak(row + 11);

                        row = row + 13;

                        Hrow = sheet3.CreateRow(row - 1);
                        sheet3.GetRow(row - 1).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row);
                        sheet3.GetRow(row).Height = HeightHead;
                        Hrow = sheet3.CreateRow(row + 1);
                        sheet3.GetRow(row + 1).Height = HeightHead;
                        Hrow = sheet3.CreateRow(row + 2);
                        sheet3.GetRow(row + 2).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 3);
                        sheet3.GetRow(row + 3).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 4);
                        sheet3.GetRow(row + 4).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 5);
                        sheet3.GetRow(row + 5).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 6);
                        sheet3.GetRow(row + 6).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 7);
                        sheet3.GetRow(row + 7).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 8);
                        sheet3.GetRow(row + 8).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 9);
                        sheet3.GetRow(row + 9).Height = HeightDetail;
                        Hrow = sheet3.CreateRow(row + 10);
                        sheet3.GetRow(row + 10).Height = HeightHead;
                    }
                }

                cell = 1;
                sheet3.CreateRow(row + 11).RowStyle = styleContent4;
                sheet3.GetRow(row + 11).Height = HeightDetail;

                int g = 0;
                for (int d = 1; d <= 5; d++)
                {
                    sheet3.GetRow(row + 11).CreateCell(g).CellStyle = styleContent66;
                    g = (5 * d);
                }
                sheet3.GetRow(row + 11).CreateCell(24).CellStyle = styleContent6;
                #endregion

                #region Visual
                ISheet sheet4 = workbook.GetSheet("Visual");
                sheet4.ForceFormulaRecalculation = true;


                DateTime MyDateFrom;
                MyDateFrom = new DateTime();
                MyDateFrom = DateTime.ParseExact(p_DateFrom, "MM/dd/yyyy",
                                                 System.Globalization.CultureInfo.InvariantCulture);

                DateTime MyDateTo;
                MyDateTo = new DateTime();
                MyDateTo = DateTime.ParseExact(p_DateTo, "MM/dd/yyyy",
                                                 System.Globalization.CultureInfo.InvariantCulture);


                sheet4.GetRow(0).GetCell(0).SetCellValue(MyDateFrom.ToString("yyyy-MM-dd") + " / " + MyDateTo.ToString("yyyy-MM-dd"));

                #endregion

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();
            }
            catch (Exception err)
            {
                string x = err.ToString();
                byte[] a = null;
                x = err.ToString();
                return a;
            }
        }

        private byte[] PrintPlane(string p_Dock, string p_DateFrom, string p_DateTo, string p_rep_a, string p_rep_b)
        {
            try
            {
                #region INITIALIZATION
                string filesTmp = "";
                filesTmp = HttpContext.Request.MapPath("~/Template/Plane.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                int row = 1;
                int cell = 1;
                int cell1 = 1;
                int max = 1;
                IRow Hrow;
                IDBContext db = DbContext;

                ISheet sheet = workbook.GetSheet("Sheet1");
                sheet.ForceFormulaRecalculation = true;

                List<Plane> modelHead = new List<Plane>();
                List<Plane> modelDock = new List<Plane>();
                List<Plane> modelSupplier = new List<Plane>();
                string Plane = "";
                int ord = 0;
                int seq = 1;
                int x = 0;
                #endregion

                #region Styling
                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 72;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font2 = workbook.CreateFont();
                Font2.FontHeightInPoints = 14;
                Font2.FontName = "Arial";
                Font2.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font3 = workbook.CreateFont();
                Font3.FontHeightInPoints = 8;
                Font3.FontName = "Arial";
                Font3.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 12;
                Font4.FontName = "Arial";
                Font4.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent1.Alignment = HorizontalAlignment.CENTER;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font1);

                ICellStyle styleContent11 = workbook.CreateCellStyle();
                styleContent11.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent11.Alignment = HorizontalAlignment.CENTER;
                styleContent11.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.SetFont(Font2);
                styleContent11.Rotation = 90;

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent2.Alignment = HorizontalAlignment.CENTER;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.DOUBLE;
                styleContent2.SetFont(Font2);

                ICellStyle styleContent21 = workbook.CreateCellStyle();
                styleContent21.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent21.Alignment = HorizontalAlignment.CENTER;
                styleContent21.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent21.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent21.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent21.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent21.SetFont(Font2);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent3.Alignment = HorizontalAlignment.CENTER;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.DOUBLE;
                styleContent3.SetFont(Font3);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.SetFont(Font4);

                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.BorderBottom = NPOI.SS.UserModel.BorderStyle.DASHED;
                #endregion

                //formating date for excel files
                DateTime MyDateFrom;
                MyDateFrom = new DateTime();
                MyDateFrom = DateTime.ParseExact(p_DateFrom, "MM/dd/yyyy",
                                                 System.Globalization.CultureInfo.InvariantCulture);

                DateTime MyDateTo;
                MyDateTo = new DateTime();
                MyDateTo = DateTime.ParseExact(p_DateTo, "MM/dd/yyyy",
                                                 System.Globalization.CultureInfo.InvariantCulture);


                /*string[] a = p_DateFrom.Split('/');
                if (a[0].Length == 2) a[0] = a[0].Substring(1,1);
                if (a[1].Length == 2) a[1] = a[1].Substring(1,1);

                p_DateFrom = a[0] + "/" + a[1] + "/" + a[2];

                string[] b = p_DateTo.Split('/');
                if (b[0].Length == 2) b[0] = b[0].Substring(1,1);
                if (b[1].Length == 2) b[1] = b[1].Substring(1,1);

                p_DateTo = b[0] + "/" + b[1] + "/" + b[2];*/

                modelHead = DbContext.Fetch<Plane>("GetPlaneHeader", new object[] { p_Dock, p_rep_a, p_rep_b });

                //var Header = modelHead.GroupBy(p => new {p.P_LANE }).Select(y => y.First());

                x = (int)Math.Ceiling(modelHead.Count / 6M);
                foreach (var result in modelHead)
                {
                    #region PAGE NUMBER and CREATE NEW ROW FOR HEADER
                    if ((seq - 1) % 6 == 0)
                    {
                        sheet.CreateRow(row - 1).CreateCell(1).SetCellValue("Page " + ((seq + 6) / 6).ToString() + " / " + x.ToString() + " Pages");
                    }

                    ord = 1;

                    for (int i = 0; i <= 5; i++)
                    {
                        Hrow = sheet.CreateRow(row + i);
                    }
                    #endregion

                    #region FETCH HEADER DATA
                    if (result.P_LANE.ToString().Length == 1) { Plane = "0" + result.P_LANE; }
                    else { Plane = result.P_LANE; }
                    sheet.GetRow(row).CreateCell(1).SetCellValue("P/L");
                    sheet.GetRow(row + 1).CreateCell(1).SetCellValue(Plane); //No yg dipinggir
                    sheet.GetRow(row).GetCell(1).CellStyle = styleContent21;
                    sheet.GetRow(row + 1).GetCell(1).CellStyle = styleContent1;
                    for (int i = 2; i <= 5; i++)
                    {
                        sheet.GetRow(row + i).CreateCell(1).SetCellValue("");
                        sheet.GetRow(row + i).GetCell(1).CellStyle = styleContent1;
                    }
                    sheet.AddMergedRegion(new CellRangeAddress(row + 1, row + 5, 1, 1));
                    sheet.GetRow(row).CreateCell(2).SetCellValue("PROGRESS LANE");
                    sheet.GetRow(row + 1).CreateCell(2).SetCellValue("RECEIVING DOCK");
                    sheet.GetRow(row + 2).CreateCell(2).SetCellValue("SUPPLIER CODE");
                    sheet.GetRow(row + 3).CreateCell(2).SetCellValue("SUPPLIER NAME");
                    sheet.GetRow(row + 4).CreateCell(2).SetCellValue("P/LANE IN");
                    sheet.GetRow(row + 5).CreateCell(2).SetCellValue("NO");
                    sheet.GetRow(row).CreateCell(3).SetCellValue(MyDateFrom.ToString("yyyy-MM-dd") + " / " + MyDateTo.ToString("yyyy-MM-dd")); //abc
                    sheet.GetRow(row).CreateCell(4).SetCellValue("");
                    sheet.GetRow(row).CreateCell(5).SetCellValue("");
                    sheet.GetRow(row).CreateCell(6).SetCellValue("");
                    sheet.GetRow(row).CreateCell(7).SetCellValue(Plane); //progress lane
                    //sheet.GetRow(row).CreateCell(8).SetCellValue("");

                    sheet.GetRow(row).GetCell(2).CellStyle = styleContent3;
                    sheet.GetRow(row + 1).GetCell(2).CellStyle = styleContent3;
                    sheet.GetRow(row + 2).GetCell(2).CellStyle = styleContent3;
                    sheet.GetRow(row + 3).GetCell(2).CellStyle = styleContent3;
                    sheet.GetRow(row + 4).GetCell(2).CellStyle = styleContent3;
                    sheet.GetRow(row + 5).GetCell(2).CellStyle = styleContent3;
                    //sheet.GetRow(row).GetCell(3).CellStyle = styleContent2;
                    #endregion

                    cell = 3; //cell untuk dock yg pling awal
                    cell1 = cell;
                    modelDock = DbContext.Fetch<Plane>("GetPlaneDock", new object[] { p_Dock, p_rep_a, p_rep_b, result.P_LANE });

                    //var Docks = modelHead.Where(item => (item.P_LANE == result.P_LANE));
                    //var Dock = Docks.ToList();
                    row = row + 1;
                    foreach (var result1 in modelDock)
                    {

                        modelSupplier = DbContext.Fetch<Plane>("GetPlaneSupplier", new object[] { p_rep_a, p_rep_b, result.P_LANE, result1.RCVDOCK });
                        //var Suppliers = Docks.Where(item => (item.RCVDOCK == result1.RCVDOCK) && (item.P_LANE == result1.P_LANE));
                        //var Supplier = Suppliers.ToList();

                        foreach (var result2 in modelSupplier)
                        {
                            #region FETCH DOCK AND SUPPLIER
                            sheet.GetRow(row).CreateCell(cell1).SetCellValue("");
                            sheet.GetRow(row).CreateCell(cell1 + 1).SetCellValue("");

                            sheet.GetRow(row + 1).CreateCell(cell1).SetCellValue(result2.SUPPID); //isi supplier code
                            sheet.GetRow(row + 2).CreateCell(cell1).SetCellValue(result2.SUP_ABBR); //isi supplier name
                            sheet.GetRow(row + 3).CreateCell(cell1).SetCellValue(result2.PLANE_IN); //isi supplier p lane
                            sheet.GetRow(row + 4).CreateCell(cell1).SetCellValue(ord); //isi no
                            ord++;

                            sheet.GetRow(row).CreateCell(cell).SetCellValue(result1.RCVDOCK);
                            sheet.GetRow(row).GetCell(cell1).CellStyle = styleContent2;
                            sheet.GetRow(row + 1).GetCell(cell1).CellStyle = styleContent3;
                            sheet.GetRow(row + 2).GetCell(cell1).CellStyle = styleContent3;
                            sheet.GetRow(row + 3).GetCell(cell1).CellStyle = styleContent3;
                            sheet.GetRow(row + 4).GetCell(cell1).CellStyle = styleContent3;
                            cell1 = cell1 + 1;
                            #endregion
                        }
                        sheet.AddMergedRegion(new CellRangeAddress(row, row, cell, cell1 - 1));
                        sheet.GetRow(row).GetCell(cell).CellStyle = styleContent2;
                        if (cell1 > max)
                        {
                            max = cell1;
                        }
                        cell = cell1;
                    }

                    #region CREATE NEW COLUMN IF SUPPLIER < 6
                    if (ord < 6)
                    {
                        int stcell = cell1;
                        while (cell1 < 8)
                        {
                            sheet.GetRow(row).CreateCell(cell1).SetCellValue("");
                            sheet.GetRow(row).CreateCell(cell1 + 1).SetCellValue("");

                            sheet.GetRow(row + 1).CreateCell(cell1).SetCellValue(""); //isi supplier code
                            sheet.GetRow(row + 2).CreateCell(cell1).SetCellValue(""); //isi supplier name
                            sheet.GetRow(row + 3).CreateCell(cell1).SetCellValue(""); //isi supplier p lane
                            sheet.GetRow(row + 4).CreateCell(cell1).SetCellValue(""); //isi no
                            ord++;

                            sheet.GetRow(row).CreateCell(cell).SetCellValue("");
                            sheet.GetRow(row).GetCell(cell1).CellStyle = styleContent2;
                            sheet.GetRow(row + 1).GetCell(cell1).CellStyle = styleContent3;
                            sheet.GetRow(row + 2).GetCell(cell1).CellStyle = styleContent3;
                            sheet.GetRow(row + 3).GetCell(cell1).CellStyle = styleContent3;
                            sheet.GetRow(row + 4).GetCell(cell1).CellStyle = styleContent3;
                            cell1 = cell1 + 1;
                        }
                        sheet.AddMergedRegion(new CellRangeAddress(row, row, stcell, cell1 - 1));
                    }
                    #endregion

                    sheet.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 3, 6));
                    if ((cell - 1) > 7) sheet.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 7, cell - 1));

                    sheet.GetRow(row - 1).GetCell(3).CellStyle = styleContent5;
                    sheet.GetRow(row - 1).GetCell(4).CellStyle = styleContent5;
                    sheet.GetRow(row - 1).GetCell(5).CellStyle = styleContent5;
                    sheet.GetRow(row - 1).GetCell(6).CellStyle = styleContent5;
                    sheet.GetRow(row - 1).GetCell(7).CellStyle = styleContent2;
                    if (cell > 8) sheet.GetRow(row - 1).CreateCell(8).CellStyle = styleContent2;

                    for (int m = 9; m <= cell - 1; m++)
                    {
                        sheet.GetRow(row - 1).CreateCell(m).CellStyle = styleContent2;
                    }

                    sheet.GetRow(row - 1).CreateCell(cell1).SetCellValue("PLANE " + Plane);
                    sheet.GetRow(row).CreateCell(cell1).SetCellValue("");
                    sheet.GetRow(row + 1).CreateCell(cell1).SetCellValue("");
                    sheet.GetRow(row + 2).CreateCell(cell1).SetCellValue("");
                    sheet.GetRow(row + 3).CreateCell(cell1).SetCellValue("");
                    sheet.GetRow(row + 4).CreateCell(cell1).SetCellValue("");

                    sheet.AddMergedRegion(new CellRangeAddress(row - 1, row + 4, cell1, cell1));

                    sheet.GetRow(row - 1).GetCell(cell1).CellStyle = styleContent11;
                    sheet.GetRow(row).GetCell(cell1).CellStyle = styleContent11;
                    sheet.GetRow(row + 1).GetCell(cell1).CellStyle = styleContent11;
                    sheet.GetRow(row + 2).GetCell(cell1).CellStyle = styleContent11;
                    sheet.GetRow(row + 3).GetCell(cell1).CellStyle = styleContent11;
                    sheet.GetRow(row + 4).GetCell(cell1).CellStyle = styleContent11;

                    row = row + 7;
                    sheet.CreateRow(row - 2).RowStyle = styleContent4;
                    if (seq % 6 == 0) sheet.SetRowBreak(row - 2);
                    sheet.GetRow(row - 2).Height = 200;
                    sheet.CreateRow(row - 1);
                    sheet.GetRow(row - 1).Height = 200;
                    //if (seq % 6 == 0) sheet.SetRowBreak(row);
                    seq++;
                }
                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();
            }
            catch (Exception err)
            {
                string x = err.ToString();
                byte[] a = null;
                x = err.ToString();
                return a;
            }
        }

        private byte[] PrintEmptyJobCard(string p_Dock, string p_DateFrom, string p_DateTo, string ProductionMonth)
        {

            try
            {
                string filesTmp = "";
                filesTmp = HttpContext.Request.MapPath("~/Template/EmptyJobCard.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;
                IDBContext db = DbContext;

                #region Row Data

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);
                #endregion

                int row = 1;
                string CONCTDATE = "";
                string RTEDATE = "";

                ISheet sheet = workbook.GetSheet("Data");
                sheet.ForceFormulaRecalculation = true;

                List<EmptyJobCard> modelTruck = new List<EmptyJobCard>();

                #region Fetch Data
                modelTruck = DbContext.Fetch<EmptyJobCard>("GetEmptyJobRowData", new object[] { p_Dock, p_DateFrom, p_DateTo }); // select header
                foreach (var result in modelTruck)
                {
                    Hrow = sheet.CreateRow(row);

                    CONCTDATE = result.CONCTODRTEDATE2.Substring(0, result.CONCTODRTEDATE2.IndexOf(" "));
                    RTEDATE = result.RTEDATE2.Substring(0, result.RTEDATE2.IndexOf(" "));
                    CONCTDATE = Convert.ToDateTime(CONCTDATE).ToString("dd-MMM-yyyy");
                    RTEDATE = Convert.ToDateTime(RTEDATE).ToString("dd-MMM-yyyy");

                    Hrow.CreateCell(0).SetCellValue(result.CONCTOCRTE);
                    Hrow.CreateCell(1).SetCellValue(result.CONCTOCRTETRP);
                    Hrow.CreateCell(2).SetCellValue(CONCTDATE);
                    Hrow.CreateCell(3).SetCellValue(result.RTEGRPCD);
                    Hrow.CreateCell(4).SetCellValue(result.RUNSEQ);
                    Hrow.CreateCell(5).SetCellValue(RTEDATE);
                    Hrow.CreateCell(6).SetCellValue(result.SUPPCD);
                    Hrow.CreateCell(7).SetCellValue(result.SUPPPLANTCD);
                    Hrow.CreateCell(8).SetCellValue(result.SUPPDOCKCD);
                    Hrow.CreateCell(9).SetCellValue(result.NUMPALLETE);
                    Hrow.CreateCell(10).SetCellValue(result.ORDDATE);
                    Hrow.CreateCell(11).SetCellValue(result.ORDSEQ);
                    Hrow.CreateCell(12).SetCellValue(result.RCVCOMPDOCKCD);

                    Hrow.GetCell(0).CellStyle = styleContent1;
                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent1;
                    Hrow.GetCell(3).CellStyle = styleContent1;
                    Hrow.GetCell(4).CellStyle = styleContent1;
                    Hrow.GetCell(5).CellStyle = styleContent1;
                    Hrow.GetCell(6).CellStyle = styleContent1;
                    Hrow.GetCell(7).CellStyle = styleContent1;
                    Hrow.GetCell(8).CellStyle = styleContent1;
                    Hrow.GetCell(9).CellStyle = styleContent1;
                    Hrow.GetCell(10).CellStyle = styleContent1;
                    Hrow.GetCell(11).CellStyle = styleContent1;
                    Hrow.GetCell(12).CellStyle = styleContent1;

                    row = row + 1;
                }
                #endregion

                #endregion

                #region Visual Empty Job Card

                #region Initialization
                ISheet sheet2 = workbook.GetSheet("Empty Job Card");
                sheet2.ForceFormulaRecalculation = true;
                sheet2.Autobreaks = false;
                int totalrow = 0;
                row = 1;
                int count = 0;
                int page = 1;
                List<EmptyJobCard> EJC = new List<EmptyJobCard>();
                string MonthNumb = DateTime.ParseExact(ProductionMonth, "MMMM yyyy", System.Globalization.CultureInfo.InvariantCulture).Month.ToString();
                if (MonthNumb.Length == 1)
                {
                    MonthNumb = "0" + MonthNumb;
                }
                int newrow = 0;
                int cell = 1;
                #endregion

                #region Styling Visual
                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 22;
                Font1.FontName = "Arial";
                Font1.Color = HSSFColor.WHITE.index;
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font2 = workbook.CreateFont();
                Font2.FontHeightInPoints = 16;
                Font2.Color = HSSFColor.WHITE.index;
                Font2.FontName = "Arial";

                IFont Font3 = workbook.CreateFont();
                Font3.FontHeightInPoints = 11;
                Font3.FontName = "Arial";

                //ROUTE
                ICellStyle styleContent22 = workbook.CreateCellStyle();
                styleContent22.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent22.Alignment = HorizontalAlignment.CENTER;
                styleContent22.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent22.TopBorderColor = HSSFColor.BLACK.index;
                styleContent22.LeftBorderColor = HSSFColor.BLACK.index;
                styleContent22.RightBorderColor = HSSFColor.WHITE.index;
                styleContent22.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent22.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent22.SetFont(Font1);

                //PHASE
                ICellStyle styleContent22a = workbook.CreateCellStyle();
                styleContent22a.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent22a.Alignment = HorizontalAlignment.CENTER;
                styleContent22a.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent22a.TopBorderColor = HSSFColor.BLACK.index;
                styleContent22a.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent22a.RightBorderColor = HSSFColor.BLACK.index;
                styleContent22a.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22a.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22a.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22a.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22a.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent22a.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent22a.SetFont(Font1);

                //TS
                ICellStyle styleContent22b = workbook.CreateCellStyle();
                styleContent22b.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent22b.Alignment = HorizontalAlignment.CENTER;
                styleContent22b.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent22b.TopBorderColor = HSSFColor.BLACK.index;
                styleContent22b.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent22b.RightBorderColor = HSSFColor.WHITE.index;
                styleContent22b.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22b.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22b.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22b.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent22b.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent22b.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent22b.SetFont(Font1);

                //SUPPLIER
                ICellStyle styleContent16a = workbook.CreateCellStyle();
                styleContent16a.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16a.Alignment = HorizontalAlignment.CENTER;
                styleContent16a.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent16a.TopBorderColor = HSSFColor.WHITE.index;
                styleContent16a.LeftBorderColor = HSSFColor.BLACK.index;
                styleContent16a.RightBorderColor = HSSFColor.WHITE.index;
                styleContent16a.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a.BorderBottom = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent16a.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16a.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16a.SetFont(Font2);

                //SUPPLIER and PLAN
                ICellStyle styleContent16a1 = workbook.CreateCellStyle();
                styleContent16a1.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16a1.Alignment = HorizontalAlignment.CENTER;
                styleContent16a1.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent16a1.TopBorderColor = HSSFColor.WHITE.index;
                styleContent16a1.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent16a1.RightBorderColor = HSSFColor.WHITE.index;
                styleContent16a1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a1.BorderBottom = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent16a1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a1.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16a1.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16a1.SetFont(Font2);

                //NAME
                ICellStyle styleContent16a3 = workbook.CreateCellStyle();
                styleContent16a3.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16a3.Alignment = HorizontalAlignment.CENTER;
                styleContent16a3.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent16a3.TopBorderColor = HSSFColor.WHITE.index;
                styleContent16a3.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent16a3.RightBorderColor = HSSFColor.WHITE.index;
                styleContent16a3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a3.BorderTop = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent16a3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a3.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16a3.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16a3.SetFont(Font2);

                //CODE
                ICellStyle styleContent16a4 = workbook.CreateCellStyle();
                styleContent16a4.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16a4.Alignment = HorizontalAlignment.CENTER;
                styleContent16a4.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent16a4.TopBorderColor = HSSFColor.WHITE.index;
                styleContent16a4.LeftBorderColor = HSSFColor.BLACK.index;
                styleContent16a4.RightBorderColor = HSSFColor.WHITE.index;
                styleContent16a4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a4.BorderTop = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent16a4.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a4.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16a4.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16a4.SetFont(Font2);

                //FINISH
                ICellStyle styleContent16a5 = workbook.CreateCellStyle();
                styleContent16a5.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16a5.Alignment = HorizontalAlignment.CENTER;
                styleContent16a5.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent16a5.TopBorderColor = HSSFColor.WHITE.index;
                styleContent16a5.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent16a5.RightBorderColor = HSSFColor.BLACK.index;
                styleContent16a5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a5.BorderBottom = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent16a5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a5.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16a5.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16a5.SetFont(Font2);

                //JUMLAH SKID and ACTUAL
                ICellStyle styleContent16a2 = workbook.CreateCellStyle();
                styleContent16a2.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16a2.Alignment = HorizontalAlignment.CENTER;
                styleContent16a2.BottomBorderColor = HSSFColor.WHITE.index;
                styleContent16a2.TopBorderColor = HSSFColor.WHITE.index;
                styleContent16a2.LeftBorderColor = HSSFColor.WHITE.index;
                styleContent16a2.RightBorderColor = HSSFColor.BLACK.index;
                styleContent16a2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16a2.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16a2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16a2.SetFont(Font2);

                //without border top
                ICellStyle styleContent16b = workbook.CreateCellStyle();
                styleContent16b.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16b.Alignment = HorizontalAlignment.CENTER;
                styleContent16b.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16b.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16b.BorderTop = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent16b.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16b.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16b.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16b.SetFont(Font2);

                ICellStyle styleContent16c = workbook.CreateCellStyle();
                styleContent16c.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent16c.Alignment = HorizontalAlignment.CENTER;
                styleContent16c.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16c.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16c.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16c.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent16c.FillForegroundColor = HSSFColor.BLACK.index;
                styleContent16c.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent16c.SetFont(Font2);

                ICellStyle styleContent11 = workbook.CreateCellStyle();
                styleContent11.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent11.Alignment = HorizontalAlignment.CENTER;
                styleContent11.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11.SetFont(Font3);

                //without top and bottom border
                ICellStyle styleContent11a = workbook.CreateCellStyle();
                styleContent11a.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent11a.Alignment = HorizontalAlignment.CENTER;
                styleContent11a.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11a.BorderBottom = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent11a.BorderTop = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent11a.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11a.SetFont(Font3);

                //without top border
                ICellStyle styleContent11b = workbook.CreateCellStyle();
                styleContent11b.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent11b.Alignment = HorizontalAlignment.CENTER;
                styleContent11b.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11b.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11b.BorderTop = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent11b.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11b.SetFont(Font3);

                //Only top border
                ICellStyle styleContent11c = workbook.CreateCellStyle();
                styleContent11c.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent11c.Alignment = HorizontalAlignment.CENTER;
                styleContent11c.BorderLeft = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent11c.BorderBottom = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent11c.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent11c.BorderRight = NPOI.SS.UserModel.BorderStyle.NONE;
                styleContent11c.SetFont(Font3);

                ICellStyle styleContent44 = workbook.CreateCellStyle();
                styleContent44.BorderTop = NPOI.SS.UserModel.BorderStyle.DASHED;

                ICellStyle styleContent45 = workbook.CreateCellStyle();
                styleContent45.BorderTop = NPOI.SS.UserModel.BorderStyle.DASHED;
                styleContent45.BorderRight = NPOI.SS.UserModel.BorderStyle.DASHED;
                #endregion

                EJC = DbContext.Fetch<EmptyJobCard>("GetEmptyJobCardHeader", new object[] { p_Dock, p_DateFrom, p_DateTo });
                var EJCHeader = EJC.GroupBy(x => new { x.SROUTE, x.SSTATION, x.SDOCK }).Select(y => y.First());

                Hrow = sheet2.CreateRow(row);

                #region Fetch Data
                if (EJCHeader.Count() > 0)
                {
                    for (int j = 0; j <= 22; j++)
                    {
                        Hrow = sheet2.CreateRow(row + j);
                    }
                    double x = (int)Math.Ceiling((EJCHeader.Count()) / 4M);
                    sheet2.CreateRow(row - 1).CreateCell(1).SetCellValue("Page 1/" + x.ToString());
                    foreach (var headerResult in EJCHeader)
                    {
                        //row = 0;
                        //set data header
                        var EJCDetails = EJC.Where(item => (item.SROUTE == headerResult.SROUTE) &&
                                                           (item.SSTATION == headerResult.SSTATION) &&
                                                           (item.SDOCK == headerResult.SDOCK));
                        var EJCDetail = EJCDetails.ToList();

                        #region Fetch Data Header
                        sheet2.GetRow(row).CreateCell(cell).SetCellValue(headerResult.SROUTE);
                        sheet2.GetRow(row).CreateCell(cell + 1).SetCellValue("");
                        sheet2.GetRow(row).CreateCell(cell + 2).SetCellValue("");
                        sheet2.GetRow(row).CreateCell(cell + 3).SetCellValue("P " + MonthNumb);
                        sheet2.AddMergedRegion(new CellRangeAddress(row, row, cell, cell + 2));
                        sheet2.GetRow(row).GetCell(cell).CellStyle = styleContent22;
                        sheet2.GetRow(row).GetCell(cell + 1).CellStyle = styleContent22;
                        sheet2.GetRow(row).GetCell(cell + 2).CellStyle = styleContent22;
                        sheet2.GetRow(row).GetCell(cell + 3).CellStyle = styleContent22a;
                        sheet2.GetRow(row + 1).CreateCell(cell).SetCellValue("Supplier");
                        sheet2.GetRow(row + 1).GetCell(cell).CellStyle = styleContent16a;
                        sheet2.GetRow(row + 2).CreateCell(cell).SetCellValue("Code");
                        sheet2.GetRow(row + 2).GetCell(cell).CellStyle = styleContent16a4;
                        sheet2.GetRow(row + 1).CreateCell(cell + 1).SetCellValue("Supplier");
                        sheet2.GetRow(row + 1).GetCell(cell + 1).CellStyle = styleContent16a1;
                        sheet2.GetRow(row + 2).CreateCell(cell + 1).SetCellValue("Name");
                        sheet2.GetRow(row + 2).GetCell(cell + 1).CellStyle = styleContent16a3;
                        sheet2.GetRow(row + 1).CreateCell(cell + 2).SetCellValue("Jumlah Skid");
                        sheet2.GetRow(row + 1).CreateCell(cell + 3).SetCellValue("");
                        sheet2.AddMergedRegion(new CellRangeAddress(row + 1, row + 1, cell + 2, cell + 3));
                        sheet2.GetRow(row + 1).GetCell(cell + 2).CellStyle = styleContent16a2;
                        sheet2.GetRow(row + 1).GetCell(cell + 3).CellStyle = styleContent16a2;
                        sheet2.GetRow(row + 2).CreateCell(cell + 2).SetCellValue("Plan");
                        sheet2.GetRow(row + 2).GetCell(cell + 2).CellStyle = styleContent16a1;
                        sheet2.GetRow(row + 2).CreateCell(cell + 3).SetCellValue("Actual");
                        sheet2.GetRow(row + 2).GetCell(cell + 3).CellStyle = styleContent16a2;
                        #endregion

                        newrow = 0;

                        #region Fetch Data Detail
                        foreach (var detailResult in EJCDetail)
                        {
                            if (newrow < 8)
                            {
                                sheet2.GetRow(row + 3).CreateCell(cell).SetCellValue(detailResult.SSUPPLIER);
                                sheet2.GetRow(row + 3).CreateCell(cell + 1).SetCellValue(detailResult.SABBR.Trim());
                                sheet2.GetRow(row + 3).CreateCell(cell + 2).SetCellValue(detailResult.SNUM.ToString());
                                sheet2.GetRow(row + 3).CreateCell(cell + 3).SetCellValue("");

                                sheet2.GetRow(row + 3).GetCell(cell).CellStyle = styleContent11;
                                sheet2.GetRow(row + 3).GetCell(cell + 1).CellStyle = styleContent11;
                                sheet2.GetRow(row + 3).GetCell(cell + 2).CellStyle = styleContent11;
                                sheet2.GetRow(row + 3).GetCell(cell + 3).CellStyle = styleContent11;
                                newrow++;
                                row++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        #endregion

                        totalrow = EJCDetail.Count();

                        #region Create Empty Cell
                        if (totalrow < 8)
                        {
                            for (int k = 0; k < 8 - totalrow; k++)
                            {
                                sheet2.GetRow(row + 3).CreateCell(cell).SetCellValue("");
                                sheet2.GetRow(row + 3).CreateCell(cell + 1).SetCellValue("");
                                sheet2.GetRow(row + 3).CreateCell(cell + 2).SetCellValue("");
                                sheet2.GetRow(row + 3).CreateCell(cell + 3).SetCellValue("");

                                sheet2.GetRow(row + 3).GetCell(cell).CellStyle = styleContent11;
                                sheet2.GetRow(row + 3).GetCell(cell + 1).CellStyle = styleContent11;
                                sheet2.GetRow(row + 3).GetCell(cell + 2).CellStyle = styleContent11;
                                sheet2.GetRow(row + 3).GetCell(cell + 3).CellStyle = styleContent11;

                                row++;
                            }
                        }
                        #endregion

                        #region Create Row For Time Data
                        row = row + 3;
                        sheet2.GetRow(row).CreateCell(cell).SetCellValue(headerResult.SDOCK);
                        sheet2.GetRow(row).GetCell(cell).CellStyle = styleContent22;
                        sheet2.GetRow(row).CreateCell(cell + 1).SetCellValue("T/S " + headerResult.SSTATION);
                        sheet2.GetRow(row).GetCell(cell + 1).CellStyle = styleContent22b;
                        sheet2.GetRow(row).CreateCell(cell + 2).SetCellValue("NIGHT");
                        sheet2.GetRow(row).GetCell(cell + 2).CellStyle = styleContent22a;
                        sheet2.GetRow(row).CreateCell(cell + 3).SetCellValue("");
                        sheet2.GetRow(row).GetCell(cell + 3).CellStyle = styleContent22a;
                        sheet2.AddMergedRegion(new CellRangeAddress(row, row, cell + 2, cell + 3));

                        sheet2.GetRow(row + 1).CreateCell(cell).SetCellValue("START");
                        sheet2.GetRow(row + 2).CreateCell(cell).SetCellValue("");
                        sheet2.GetRow(row + 1).CreateCell(cell + 1).SetCellValue("");
                        sheet2.GetRow(row + 2).CreateCell(cell + 1).SetCellValue("");
                        sheet2.AddMergedRegion(new CellRangeAddress(row + 1, row + 2, cell, cell + 1));
                        sheet2.GetRow(row + 1).GetCell(cell).CellStyle = styleContent16a;
                        sheet2.GetRow(row + 2).GetCell(cell).CellStyle = styleContent16a;
                        sheet2.GetRow(row + 1).GetCell(cell + 1).CellStyle = styleContent16a;
                        sheet2.GetRow(row + 2).GetCell(cell + 1).CellStyle = styleContent16a;
                        sheet2.GetRow(row + 1).CreateCell(cell + 2).SetCellValue("FINISH");
                        sheet2.GetRow(row + 2).CreateCell(cell + 2).SetCellValue("");
                        sheet2.GetRow(row + 1).CreateCell(cell + 3).SetCellValue("");
                        sheet2.GetRow(row + 2).CreateCell(cell + 3).SetCellValue("");
                        sheet2.AddMergedRegion(new CellRangeAddress(row + 1, row + 2, cell + 2, cell + 3));
                        sheet2.GetRow(row + 1).GetCell(cell + 2).CellStyle = styleContent16a5;
                        sheet2.GetRow(row + 2).GetCell(cell + 2).CellStyle = styleContent16a5;
                        sheet2.GetRow(row + 1).GetCell(cell + 3).CellStyle = styleContent16a5;
                        sheet2.GetRow(row + 2).GetCell(cell + 3).CellStyle = styleContent16a5;
                        row = row + 3;
                        for (int k = 0; k < 8; k++)
                        {
                            sheet2.GetRow(row).CreateCell(cell).SetCellValue("");
                            sheet2.GetRow(row).CreateCell(cell + 1).SetCellValue("");
                            sheet2.GetRow(row).CreateCell(cell + 2).SetCellValue("");
                            sheet2.GetRow(row).CreateCell(cell + 3).SetCellValue("");

                            sheet2.GetRow(row).GetCell(cell).CellStyle = styleContent11a;
                            sheet2.GetRow(row).GetCell(cell + 1).CellStyle = styleContent11a;
                            sheet2.GetRow(row).GetCell(cell + 2).CellStyle = styleContent11a;
                            sheet2.GetRow(row).GetCell(cell + 3).CellStyle = styleContent11a;
                            row++;
                        }

                        sheet2.AddMergedRegion(new CellRangeAddress(row - 8, row - 1, cell, cell + 1));
                        sheet2.AddMergedRegion(new CellRangeAddress(row - 8, row - 1, cell + 2, cell + 3));

                        //create bottom border for start and finish header
                        sheet2.GetRow(row - 1).GetCell(cell).CellStyle = styleContent11b;
                        sheet2.GetRow(row - 1).GetCell(cell + 1).CellStyle = styleContent11b;
                        sheet2.GetRow(row - 1).GetCell(cell + 2).CellStyle = styleContent11b;
                        sheet2.GetRow(row - 1).GetCell(cell + 3).CellStyle = styleContent11b;
                        #endregion

                        #region Create Break and Dashed Border
                        sheet2.CreateRow(row + 1).RowStyle = styleContent44;
                        sheet2.GetRow(row + 1).CreateCell(5).CellStyle = styleContent45;
                        sheet2.GetRow(row + 1).CreateCell(11).CellStyle = styleContent45;
                        count++;
                        if (count % 4 == 0)
                        {
                            page++;
                            sheet2.SetRowBreak(row);
                            if (count != EJCHeader.Count()) sheet2.GetRow(row + 1).CreateCell(1).SetCellValue("Page " + page.ToString() + "/" + x.ToString());
                        }
                        #endregion

                        #region ROW Checking
                        cell = cell + 6;
                        if (cell > 10)
                        {
                            cell = 1;
                            row = row + 2;
                            //if (EJCDetail.Count <= 8)
                            //{
                            for (int j = 0; j <= 22; j++)
                            {
                                Hrow = sheet2.CreateRow(row + j);
                            }
                            //}
                            /*else
                            {
                                for (int j = 0; j <= 22 + (EJCDetail.Count - 8); j++)
                                {
                                    Hrow = sheet2.CreateRow(row + j);
                                }
                            }*/
                        }
                        else
                        {
                            row = row - 22;
                        }
                        #endregion
                    }
                }
                #endregion

                #endregion

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();
            }
            catch (Exception err)
            {
                string x = err.ToString();
                byte[] a = null;
                x = err.ToString();
                return a;
            }

        }


        //new
        private byte[] PrintDeliveryControlList(string p_DateFrom, string p_DateTo)
        {
            try
            {


                DateTime datefrom = DateTime.ParseExact(p_DateFrom, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime dateto = DateTime.ParseExact(p_DateTo, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                string sdatefrom = datefrom.ToString("yyyy-MM-dd");
                string sdateto = dateto.ToString("yyyy-MM-dd");


                string filesTmp = "";
                filesTmp = HttpContext.Request.MapPath("~/Template/DCL.xls");


                string excelpath = "";
                excelpath = HttpContext.Request.MapPath("~/Template/DCL.xlsx");//Server.MapPath("~/UploadExcel/" + DateTime.UtcNow.Date.ToString() + ".xlsx"); //@"E:\UserData\Muhajir\Book4.xlsx";//                  

                FileInfo finame = new FileInfo(excelpath);
                if (System.IO.File.Exists(excelpath))
                {
                    System.IO.File.Delete(excelpath);
                }
                if (!System.IO.File.Exists(excelpath))
                {
                    ExcelPackage excel = new ExcelPackage(finame);
                    var sheetcreate = excel.Workbook.Worksheets.Add(DateTime.UtcNow.Date.ToString());

                    List<DeliveryControlList> DCLs = new List<DeliveryControlList>();

                    DCLs = DbContext.Fetch<DeliveryControlList>("GetDeliveryControlListReport", new object[] { p_DateFrom, p_DateTo }); // select header

                    var DCLHeader = DCLs.GroupBy(x => new { x.PickupDate }).Select(y => y.First());

                    //creating all sheet
                    double totalSheets = (double)DCLHeader.Count() / 7;
                    int totalSheet = (int)Math.Ceiling(totalSheets);
                    ISheet sheet;

                    var ns = excel.Workbook.Styles.CreateNamedStyle("styleContent1");
                    ns.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ns.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ns.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ns.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ns.Style.Font.Name = "Arial";
                    ns.Style.Font.Size = 10;


                    int row = 3; 
                    for (int a = 0; a < totalSheet; a++)
                    {                        
                        sheetcreate.Cells[1, 1].Value = "DELIVERY CONTROL LIST";
                        sheetcreate.Cells[2, 1].Value = "Period : " + sdatefrom + " / " + sdateto;
                        sheetcreate.Cells[4, 1].Value = "Delivery No           ";
                        sheetcreate.Cells[4, 2].Value = "Pickup Date    ";
                        sheetcreate.Cells[4, 3].Value = "Route Code";
                        sheetcreate.Cells[4, 4].Value = "Rate";
                        sheetcreate.Cells[4, 5].Value = "LP Code";
                        sheetcreate.Cells[4, 6].Value = "LP Name                                             ";
                        sheetcreate.Cells[4, 7].Value = "Logistic Point";
                        sheetcreate.Cells[4, 8].Value = "Supplier Name                                              ";
                        sheetcreate.Cells[4, 9].Value = "Arrival Plan                 ";
                        sheetcreate.Cells[4, 10].Value = "Departure Plan             ";
                        sheetcreate.Cells[4, 11].Value = "Order Plan       ";
                        sheetcreate.Cells[4, 12].Value = "Dock Code";
                        sheetcreate.Cells[4, 13].Value = "";

                        sheetcreate.Cells[row + 1, 1].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 2].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 3].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 4].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 5].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 6].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 7].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 8].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 9].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 10].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 11].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 12].StyleName = "styleContent1";
                        sheetcreate.Cells[row + 1, 13].StyleName = "styleContent1";
                        sheetcreate = excel.Workbook.Worksheets.Add(DateTime.UtcNow.Date.ToString());
                    }

                    int pointerSheet = 1;
                    var oldDate = "";
                    var pointerDate = 1;

                    //for set the sheet's name
                    string firstDate = "";
                    string firstLoop = "Y";
                    string lastDate = "";
                    row = 4;

                    var DCLDetail = DCLs.Where(item => (item.PickupDate == "")).ToList(); //declare DCLDetail

                    foreach (var DCLHead in DCLHeader)
                    {
                        //for set the sheet's name
                        if (firstLoop == "Y")
                        {
                            firstDate = DCLHead.PickupDate;
                            firstLoop = "N";
                        }
                        lastDate = DCLHead.PickupDate; //for set the sheet's name


                        if (pointerDate <= 7)
                        {
                            var DCLDetails = DCLs.Where(item => (item.PickupDate == DCLHead.PickupDate));
                            DCLDetail = DCLDetails.ToList();
                            pointerDate = pointerDate + 1;

                        }
                        else
                        {
                            firstDate = DCLHead.PickupDate;
                            pointerSheet = pointerSheet + 1;
                            var DCLDetails = DCLs.Where(item => (item.PickupDate == DCLHead.PickupDate));
                            DCLDetail = DCLDetails.ToList();
                            pointerDate = 1;
                            row = 4;
                        }

                        foreach (var result in DCLDetail)
                        {
                                sheetcreate = excel.Workbook.Worksheets[pointerSheet];                        
                                sheetcreate.Cells[row + 1, 1].Value = result.DELIVERY_NO;
                                sheetcreate.Cells[row + 1, 2].Value = result.PickupDate;
                                sheetcreate.Cells[row + 1, 3].Value = result.RouteCode;
                                sheetcreate.Cells[row + 1, 4].Value = result.Rate;
                                sheetcreate.Cells[row + 1, 5].Value = result.LPCode;
                                sheetcreate.Cells[row + 1, 6].Value = result.LPName;
                                sheetcreate.Cells[row + 1, 7].Value = result.LogisticPoint;
                                sheetcreate.Cells[row + 1, 8].Value = result.SupplierName;
                                sheetcreate.Cells[row + 1, 9].Value = result.ArrivalPlan;
                                sheetcreate.Cells[row + 1, 10].Value = result.DeparturePlan;
                                sheetcreate.Cells[row + 1, 11].Value = result.OrderNo;
                                sheetcreate.Cells[row + 1, 12].Value = result.DockCode;
                                sheetcreate.Cells[row + 1, 13].Value = "";

                                sheetcreate.Cells[row + 1, 1].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 2].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 3].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 4].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 5].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 6].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 7].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 8].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 9].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 10].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 11].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 12].StyleName = "styleContent1";
                                sheetcreate.Cells[row + 1, 13].StyleName = "styleContent1";

                                //sheetcreate.Cells[row + 1, 1].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 2].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 3].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 4].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 5].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 6].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 7].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 8].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 9].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 10].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 11].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 12].AutoFitColumns();
                                //sheetcreate.Cells[row + 1, 13].AutoFitColumns();
                                row = row + 1;
                        }
                        sheetcreate.Cells.AutoFitColumns();
                        excel.Workbook.Worksheets[pointerSheet].Name = "DCL_" + firstDate + " ~ " + lastDate;
                    }

                    using (var memoryStream = new MemoryStream())
                    {
                        excel.SaveAs(memoryStream);
                        return memoryStream.ToArray();
                    }
                }
                
                return null;
                

            }
            catch (Exception err)
            {
                string x = err.ToString();
                byte[] a = null;
                x = err.ToString();
                return a;
            }
        }



        private byte[] PrintChangeOver(string p_Dock, string p_rep_a, string p_rep_a_current)
        {
            try
            {
                string filesTmp = "";
                filesTmp = HttpContext.Request.MapPath("~/Template/ChangeOver.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;
                IDBContext db = DbContext;


                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 18;
                Font1.FontName = "Arial";
                Font1.Underline = (byte)NPOI.SS.UserModel.FontUnderlineType.SINGLE;

                IFont Font2 = workbook.CreateFont();
                Font2.FontHeightInPoints = 10;
                Font2.FontName = "Arial";
                Font2.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                IFont Font3 = workbook.CreateFont();
                Font3.FontHeightInPoints = 10;
                Font3.FontName = "Arial";
                Font3.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                Font3.Color = NPOI.HSSF.Util.HSSFColor.WHITE.index;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.Alignment = HorizontalAlignment.CENTER;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_GREEN.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.SetFont(Font2);

                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.Alignment = HorizontalAlignment.CENTER;
                styleContent4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent4.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.YELLOW.index;
                styleContent4.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent4.SetFont(Font2);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.SetFont(Font1);

                ICellStyle styleContent5 = workbook.CreateCellStyle();
                styleContent5.Alignment = HorizontalAlignment.CENTER;
                styleContent5.VerticalAlignment = VerticalAlignment.TOP;
                styleContent5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent5.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.PALE_BLUE.index;
                styleContent5.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent5.SetFont(Font2);


                ICellStyle styleContent6 = workbook.CreateCellStyle();
                styleContent6.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent6.Alignment = HorizontalAlignment.CENTER;
                styleContent6.WrapText = true;
                styleContent6.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent6.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
                styleContent6.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent6.SetFont(Font3);


                ICellStyle styleContent7 = workbook.CreateCellStyle();
                styleContent7.VerticalAlignment = VerticalAlignment.CENTER;
                styleContent7.Alignment = HorizontalAlignment.CENTER;
                styleContent7.WrapText = true;

                styleContent7.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent7.SetFont(Font4);
                #endregion

                List<ChangeOverList> rawdatas = new List<ChangeOverList>();
                List<ChangeOverList> currentData = new List<ChangeOverList>();
                List<ChangeOverList> nextData = new List<ChangeOverList>();
                //List<string> listForDistinctRunSeq = new List<string>();
                List<ChangeOverList> runseqLevelCurrentData = new List<ChangeOverList>();
                List<ChangeOverList> runseqLevelNextData = new List<ChangeOverList>();

                rawdatas = DbContext.Fetch<ChangeOverList>("GetChangeOverList", new object[] { p_Dock, p_rep_a, p_rep_a_current });

                //creating var for store list
                List<string> specCurrentData = new List<string>();
                List<string> specNextData = new List<string>();
                List<string> specUnionData = new List<string>();
                string combineSuppCd = "";
                string combinePlant = "";
                string combineDock = "";

                var docksCurrent = new List<compareDock>();
                var docksNext = new List<compareDock>();
                List<string> compareDock = new List<string>();

                List<string> suppCurrent = new List<string>();
                List<string> suppNext = new List<string>();

                string msgDockPlant = "";

                var distinctData = (from p in rawdatas
                                    select new { p.RTEGRPCD }).Distinct().OrderBy(p => p.RTEGRPCD);

                var runseqdata = (from p in rawdatas
                                  select new { p.RUNSEQ }).Distinct();

                #region collaps
                DateTime nextrtedate = Convert.ToDateTime(p_rep_a);
                DateTime currrtedate = Convert.ToDateTime(p_rep_a_current);

                ISheet sheet;
                sheet = workbook.GetSheet("Change Over Report");
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue("Change Over Report");

                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));
                Hrow.GetCell(0).CellStyle = styleContent3;


                Hrow = sheet.CreateRow(2);
                Hrow.CreateCell(3).SetCellValue("Current (" + currrtedate.ToString("dd.MM.yyyy") + ")");
                Hrow.CreateCell(4).SetCellValue("");
                Hrow.CreateCell(5).SetCellValue("");
                Hrow.CreateCell(6).SetCellValue("");
                Hrow.CreateCell(7).SetCellValue("");
                Hrow.CreateCell(8).SetCellValue("");
                Hrow.CreateCell(9).SetCellValue("");
                Hrow.CreateCell(10).SetCellValue("");

                sheet.AddMergedRegion(new CellRangeAddress(2, 2, 3, 10));

                Hrow.CreateCell(11).SetCellValue("Next (" + nextrtedate.ToString("dd.MM.yyyy") + ")");
                Hrow.CreateCell(12).SetCellValue("");
                Hrow.CreateCell(13).SetCellValue("");
                Hrow.CreateCell(14).SetCellValue("");
                Hrow.CreateCell(15).SetCellValue("");
                Hrow.CreateCell(16).SetCellValue("");
                Hrow.CreateCell(17).SetCellValue("");
                Hrow.CreateCell(18).SetCellValue("");

                sheet.AddMergedRegion(new CellRangeAddress(2, 2, 11, 18));

                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;

                Hrow.CreateCell(19).SetCellValue("Order No Change");
                Hrow.CreateCell(20).SetCellValue("Dock Change");
                Hrow.CreateCell(21).SetCellValue("Changing Point");

                Hrow = sheet.CreateRow(3);
                Hrow.CreateCell(1).SetCellValue("No    ");

                sheet.AutoSizeColumn(1);

                Hrow.CreateCell(1).SetCellValue("");
                sheet.AutoSizeColumn(0);

                Hrow.CreateCell(2).SetCellValue("Route         ");
                sheet.AutoSizeColumn(2);

                Hrow.CreateCell(3).SetCellValue("  Delivery Frequency  ");
                Hrow.CreateCell(11).SetCellValue("  Delivery Frequency  ");
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(11);

                Hrow.CreateCell(4).SetCellValue(" Run Seq ");
                Hrow.CreateCell(12).SetCellValue(" Run Seq ");
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(12);

                Hrow.CreateCell(5).SetCellValue("    Arrival Time    ");
                Hrow.CreateCell(13).SetCellValue("    Arrival Time    ");
                sheet.AutoSizeColumn(5);
                sheet.AutoSizeColumn(13);

                Hrow.CreateCell(6).SetCellValue("    Supplier Code    ");
                Hrow.CreateCell(14).SetCellValue("    Supplier Code    ");
                sheet.AutoSizeColumn(6);
                sheet.AutoSizeColumn(14);

                Hrow.CreateCell(7).SetCellValue("    Supplier Plant    ");
                Hrow.CreateCell(15).SetCellValue("    Supplier Plant    ");
                sheet.AutoSizeColumn(7);
                sheet.AutoSizeColumn(15);

                Hrow.CreateCell(8).SetCellValue("    Dock Code    ");
                Hrow.CreateCell(16).SetCellValue("    Dock Code    ");
                sheet.AutoSizeColumn(8);
                sheet.AutoSizeColumn(16);

                Hrow.CreateCell(9).SetCellValue("       End of Order       ");
                Hrow.CreateCell(17).SetCellValue("       End of Order       ");
                sheet.AutoSizeColumn(9);
                sheet.AutoSizeColumn(17);

                Hrow.CreateCell(10).SetCellValue("   LP   ");
                Hrow.CreateCell(18).SetCellValue("   LP   ");
                sheet.AutoSizeColumn(10);
                sheet.AutoSizeColumn(18);

                Hrow.CreateCell(19).SetCellValue("");
                Hrow.CreateCell(20).SetCellValue("");
                Hrow.CreateCell(21).SetCellValue("");

                sheet.AddMergedRegion(new CellRangeAddress(2, 3, 19, 19));
                sheet.AddMergedRegion(new CellRangeAddress(2, 3, 20, 20));
                sheet.AddMergedRegion(new CellRangeAddress(2, 3, 21, 21));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent4;
                Hrow.GetCell(4).CellStyle = styleContent4;
                Hrow.GetCell(5).CellStyle = styleContent4;
                Hrow.GetCell(6).CellStyle = styleContent4;
                Hrow.GetCell(7).CellStyle = styleContent4;
                Hrow.GetCell(8).CellStyle = styleContent4;
                Hrow.GetCell(9).CellStyle = styleContent4;
                Hrow.GetCell(10).CellStyle = styleContent4;
                Hrow.GetCell(11).CellStyle = styleContent5;
                Hrow.GetCell(12).CellStyle = styleContent5;
                Hrow.GetCell(13).CellStyle = styleContent5;
                Hrow.GetCell(14).CellStyle = styleContent5;
                Hrow.GetCell(15).CellStyle = styleContent5;
                Hrow.GetCell(16).CellStyle = styleContent5;
                Hrow.GetCell(17).CellStyle = styleContent5;
                Hrow.GetCell(18).CellStyle = styleContent5;
                Hrow.GetCell(19).CellStyle = styleContent6;
                Hrow.GetCell(20).CellStyle = styleContent6;
                Hrow.GetCell(21).CellStyle = styleContent6;

                Hrow = sheet.GetRow(2);
                Hrow.GetCell(19).CellStyle = styleContent6;
                Hrow.GetCell(20).CellStyle = styleContent6;
                Hrow.GetCell(21).CellStyle = styleContent6;

                int no = 1;
                //int totaldata = datas.Count();
                int startpointer = 0;
                int maxpointer = 0;
                int row = 4;
                string currFlag = "N";
                string nextFlag = "N";
                string msg = "";
                int changeFrequencyFlag = 0;

                Hrow = sheet.CreateRow(row);

                #endregion


                int countSpecificRouteCurrent = 0;
                int countSpecificRouteNext = 0;
                string endofordermsg = "";

                int rows = 4;
                string startnewroutecd = "Y";
                string cacheRunseqCurr = "";
                string cacheRunseqNext = "";

                string arrivalCurrent = "";
                string arrivalNext = "";

                int startMergeCell = 0;
                int endMergeCell = 0;

                //foreach level route cd
                foreach (var n in distinctData)
                {
                    if (rows != 4)
                    {
                        Hrow = sheet.CreateRow(rows);
                    }
                    startnewroutecd = "Y";
                    msg = "";
                    combineSuppCd = "";
                    combinePlant = "";
                    combineDock = "";
                    Hrow.CreateCell(1).SetCellValue(no);
                    Hrow.CreateCell(2).SetCellValue(n.RTEGRPCD);

                    Hrow.GetCell(1).CellStyle = styleContent1;
                    Hrow.GetCell(2).CellStyle = styleContent1;

                    currentData = (from p in rawdatas
                                   where p.RTEGRPCD == n.RTEGRPCD &&
                                              p.STATUS == "current"
                                   select p
                                       ).ToList();

                    nextData = (from p in rawdatas
                                where p.RTEGRPCD == n.RTEGRPCD &&
                                           p.STATUS == "next"
                                select p).ToList();

                    runseqdata = (from p in rawdatas
                                  where p.RTEGRPCD == n.RTEGRPCD
                                  select new { p.RUNSEQ }
                                      ).Distinct().OrderBy(p => p.RUNSEQ).ToList();

                    //check for route
                    countSpecificRouteCurrent = currentData.Count();
                    countSpecificRouteNext = nextData.Count();

                    if (countSpecificRouteCurrent == 0)
                    {
                        msg = "New Route";
                    }
                    else if (countSpecificRouteNext == 0)
                    {
                        msg = "Route Deletion";
                    }

                    int countFreqCurrent = 0, countFreqNext = 0;

                    //count frequency
                    if (msg == "")
                    {
                        countFreqCurrent = (from p in currentData
                                            select p.RUNSEQ).Distinct().Count();
                        countFreqNext = (from p in nextData
                                         select p.RUNSEQ).Distinct().Count();

                        msg = (countFreqCurrent != countFreqNext) ? "Change Delivery Freq" : "";

                        #region check suppcd
                        specCurrentData = (from p in currentData
                                           select p.SUPPCD).ToList();
                        specNextData = (from p in nextData
                                        select p.SUPPCD).ToList();

                        //currentData = currentData.
                        //currentData.ForEach(x => x.ENDOFORDER = x.ENDOFORDER + x.CHANGING_POINT);


                        specCurrentData = specCurrentData.GroupBy(x => x).Select(g => g.First()).ToList();
                        specNextData = specNextData.GroupBy(x => x).Select(g => g.First()).ToList();

                        specUnionData = specCurrentData.Union(specNextData).ToList();

                        //--reusing var suppcurrentdata
                        specCurrentData = (from curr in specCurrentData
                                           join next in specNextData
                                           on curr equals next
                                           select curr).ToList();

                        specUnionData = specUnionData.Where(uni => !specCurrentData.Contains(uni)).ToList();

                        if (specUnionData.Count() > 0)
                        {
                            combineSuppCd = String.Join(", ", specUnionData.ToArray());

                        }

                        if (combineSuppCd != "")
                        {
                            msg = (msg != "") ? msg + "," : msg;
                            msg = msg + " Change Supplier (" + combineSuppCd + ")";
                        }

                        #endregion

                        #region check plant
                        specCurrentData = (from p in currentData
                                           select p.SUPPPLANTCD).ToList();
                        specNextData = (from p in nextData
                                        select p.SUPPPLANTCD).ToList();

                        specCurrentData = specCurrentData.GroupBy(x => x).Select(g => g.First()).ToList();
                        specNextData = specNextData.GroupBy(x => x).Select(g => g.First()).ToList();

                        specUnionData = specCurrentData.Union(specNextData).ToList();

                        //--reusing var speccurrentdata
                        specCurrentData = (from curr in specCurrentData
                                           join next in specNextData
                                           on curr equals next
                                           select curr).ToList();

                        specUnionData = specUnionData.Where(uni => !specCurrentData.Contains(uni)).ToList();

                        if (specUnionData.Count() > 0)
                        {
                            combinePlant = String.Join(", ", specUnionData.ToArray());
                        }

                        if (combinePlant != "")
                        {
                            msg = (msg != "") ? msg + "," : msg;
                            msg = msg + " Change Plant (" + combinePlant + ")";
                        }

                        #endregion

                        #region check dock
                        specCurrentData = (from p in currentData
                                           select p.RCVCOMPDOCKCD).ToList();
                        specNextData = (from p in nextData
                                        select p.RCVCOMPDOCKCD).ToList();

                        specCurrentData = specCurrentData.GroupBy(x => x).Select(g => g.First()).ToList();
                        specNextData = specNextData.GroupBy(x => x).Select(g => g.First()).ToList();

                        specUnionData = specCurrentData.Union(specNextData).ToList();

                        //--reusing var speccurrentdata
                        specCurrentData = (from curr in specCurrentData
                                           join next in specNextData
                                           on curr equals next
                                           select curr).ToList();

                        specUnionData = specUnionData.Where(uni => !specCurrentData.Contains(uni)).ToList();

                        if (specUnionData.Count() > 0)
                        {
                            combineDock = String.Join(", ", specUnionData.ToArray());
                        }

                        if (combineDock != "")
                        {
                            msg = (msg != "") ? msg + "," : msg;
                            msg = msg + " Change Dock (" + combineDock + ")";
                        }
                        #endregion


                        #region comparedock
                        docksCurrent = (from p in currentData
                                        select new compareDock { keys = p.RTEGRPCD + p.SUPPCD + p.SUPPPLANTCD, RCVCOMPDOCKCD = p.RCVCOMPDOCKCD }).
                                        ToList();
                        docksNext = (from p in nextData
                                     select new compareDock { keys = p.RTEGRPCD + p.SUPPCD + p.SUPPPLANTCD, RCVCOMPDOCKCD = p.RCVCOMPDOCKCD }).
                                        ToList();
                        docksCurrent = docksCurrent.GroupBy(x => new { x.keys, x.RCVCOMPDOCKCD }).Select(g => new compareDock { keys = g.Key.keys, RCVCOMPDOCKCD = g.Key.RCVCOMPDOCKCD }).ToList();
                        docksNext = docksNext.GroupBy(x => new { x.keys, x.RCVCOMPDOCKCD }).Select(g => new compareDock { keys = g.Key.keys, RCVCOMPDOCKCD = g.Key.RCVCOMPDOCKCD }).ToList();

                        //reuse docksNext variable for storing result of join
                        compareDock = (from p in docksCurrent
                                       select p.keys + p.RCVCOMPDOCKCD
                                           ).ToList();

                        docksNext = docksNext.Where(x => !compareDock.Contains(x.keys + x.RCVCOMPDOCKCD)).ToList();

                        if (docksNext.Count() > 0)
                        {
                            msgDockPlant = "Several Dock Changed (" + String.Join(",", (from p in docksNext select p.RCVCOMPDOCKCD.Replace(" ", "")).ToArray()) + ")";
                        }

                        //check for rtegrpcd+suppcd+suppplantcd that not match between current and next
                        suppCurrent = (from p in currentData
                                       select p.RTEGRPCD + p.SUPPCD + p.SUPPPLANTCD
                                           ).ToList();
                        suppNext = (from p in nextData
                                    select p.RTEGRPCD + p.SUPPCD + p.SUPPPLANTCD
                                        ).ToList();

                        suppCurrent = suppCurrent.GroupBy(x => x).Select(g => g.First()).ToList();
                        suppNext = suppNext.GroupBy(x => x).Select(g => g.First()).ToList();

                        suppNext = suppNext.Where(x => !suppCurrent.Contains(x)).ToList();

                        if (suppNext.Count() > 0)
                        {
                            if (msgDockPlant != "") { msgDockPlant = msgDockPlant + ", "; }
                            msgDockPlant = msgDockPlant + "Supplier Plant Changed.";
                        }
                        //msg = msgDockPlant ;

                        //docksNext.Where(nos => !docksCurrent.Contains(nos.keys)).ToList();

                        #endregion
                    }



                    Hrow.CreateCell(3).SetCellValue((countFreqCurrent > 0) ? countFreqCurrent.ToString() : "");
                    Hrow.CreateCell(11).SetCellValue((countFreqNext > 0) ? countFreqNext.ToString() : "");

                    //foreach runseq level
                    foreach (var z in runseqdata)
                    {
                        cacheRunseqCurr = "";
                        cacheRunseqNext = "";
                        runseqLevelCurrentData = (from p in currentData
                                                  where p.RUNSEQ == z.RUNSEQ
                                                  select p).OrderBy(p => p.UNLOADSTRTIME).
                                       ThenBy(p => p.SUPPCD).ThenBy(p => p.SUPPPLANTCD).ThenBy(p => p.RCVCOMPDOCKCD).ThenBy(p => p.ENDOFORDER)
                                       .ToList();

                        runseqLevelNextData = (from p in nextData
                                               where p.RUNSEQ == z.RUNSEQ
                                               select p).OrderBy(p => p.UNLOADSTRTIME).
                                       ThenBy(p => p.SUPPCD).ThenBy(p => p.SUPPPLANTCD).ThenBy(p => p.RCVCOMPDOCKCD).ThenBy(p => p.ENDOFORDER)
                                       .ToList();


                        maxpointer = (runseqLevelCurrentData.Count() > runseqLevelNextData.Count()) ? runseqLevelCurrentData.Count() : runseqLevelNextData.Count();
                        startpointer = 0;

                        for (startpointer = 0; startpointer < maxpointer; startpointer++)
                        {
                            if (startnewroutecd == "Y")
                            {
                                startnewroutecd = "N";
                                startMergeCell = rows;//start merge row for changing point
                                endMergeCell = rows; //end merge row for changing point
                            }
                            else
                            {
                                rows++;
                                endMergeCell = rows; //end merge row for changing point
                                Hrow = sheet.CreateRow(rows);

                                //for styling purpose
                                Hrow.CreateCell(1).SetCellValue("");
                                Hrow.CreateCell(2).SetCellValue("");
                                Hrow.CreateCell(3).SetCellValue("");
                                Hrow.CreateCell(11).SetCellValue("");
                            }
                            currFlag = "Y";
                            nextFlag = "Y";
                            endofordermsg = "";

                            if (startpointer < runseqLevelCurrentData.Count())
                            {
                                if ((cacheRunseqCurr == "") //jika runseq bukan row initiate dan runseqcache <> runseq yang aktif
                                    || (cacheRunseqCurr != runseqLevelCurrentData[startpointer].RUNSEQ)
                                    )
                                {
                                    Hrow.CreateCell(4).SetCellValue(runseqLevelCurrentData[startpointer].RUNSEQ);
                                    cacheRunseqCurr = runseqLevelCurrentData[startpointer].RUNSEQ;
                                }
                                else
                                {
                                    Hrow.CreateCell(4).SetCellValue("");
                                }

                                if ((arrivalCurrent == "") //jika runseq bukan row initiate dan arrival <> unloadstrtime yang aktif
                                    || (arrivalCurrent != runseqLevelCurrentData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss"))
                                    )
                                {
                                    Hrow.CreateCell(5).SetCellValue(runseqLevelCurrentData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss"));
                                    arrivalCurrent = runseqLevelCurrentData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss");
                                }
                                else
                                {
                                    Hrow.CreateCell(5).SetCellValue("");
                                }

                                //Hrow.CreateCell(4).SetCellValue(runseqLevelCurrentData[startpointer].RUNSEQ);
                                //Hrow.CreateCell(5).SetCellValue(runseqLevelCurrentData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss"));
                                Hrow.CreateCell(6).SetCellValue(runseqLevelCurrentData[startpointer].SUPPCD);
                                Hrow.CreateCell(7).SetCellValue(runseqLevelCurrentData[startpointer].SUPPPLANTCD);
                                Hrow.CreateCell(8).SetCellValue(runseqLevelCurrentData[startpointer].RCVCOMPDOCKCD);
                                Hrow.CreateCell(9).SetCellValue(runseqLevelCurrentData[startpointer].ENDOFORDER);
                                Hrow.CreateCell(10).SetCellValue(runseqLevelCurrentData[startpointer].LOGPARTNERCD);
                                currFlag = "Y";
                            }
                            else
                            {
                                Hrow.CreateCell(4).SetCellValue("");
                                Hrow.CreateCell(5).SetCellValue("");
                                Hrow.CreateCell(6).SetCellValue("");
                                Hrow.CreateCell(7).SetCellValue("");
                                Hrow.CreateCell(8).SetCellValue("");
                                Hrow.CreateCell(9).SetCellValue("");
                                Hrow.CreateCell(10).SetCellValue("");
                                currFlag = "N";
                            }

                            if (startpointer < runseqLevelNextData.Count())
                            {
                                if ((cacheRunseqNext == "") //jika arrival bukan row initiate dan runseqcache <> runseq yang aktif
                                    || (cacheRunseqNext != runseqLevelNextData[startpointer].RUNSEQ)
                                    )
                                {
                                    Hrow.CreateCell(12).SetCellValue(runseqLevelNextData[startpointer].RUNSEQ);
                                    cacheRunseqNext = runseqLevelNextData[startpointer].RUNSEQ;
                                }
                                else
                                {
                                    Hrow.CreateCell(12).SetCellValue("");
                                }

                                if ((arrivalNext == "") //jika arrival bukan row initiate dan arrival <> unloadstrtime yang aktif
                                    || (arrivalNext != runseqLevelNextData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss"))
                                    )
                                {
                                    Hrow.CreateCell(13).SetCellValue(runseqLevelNextData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss"));
                                    arrivalNext = runseqLevelNextData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss");
                                }
                                else
                                {
                                    Hrow.CreateCell(13).SetCellValue("");
                                }


                                //Hrow.CreateCell(12).SetCellValue(runseqLevelNextData[startpointer].RUNSEQ);
                                //Hrow.CreateCell(13).SetCellValue(runseqLevelNextData[startpointer].UNLOADSTRTIME.ToString("HH:mm:ss"));
                                Hrow.CreateCell(14).SetCellValue(runseqLevelNextData[startpointer].SUPPCD);
                                Hrow.CreateCell(15).SetCellValue(runseqLevelNextData[startpointer].SUPPPLANTCD);
                                Hrow.CreateCell(16).SetCellValue(runseqLevelNextData[startpointer].RCVCOMPDOCKCD);
                                Hrow.CreateCell(17).SetCellValue(runseqLevelNextData[startpointer].ENDOFORDER);
                                Hrow.CreateCell(18).SetCellValue(runseqLevelNextData[startpointer].LOGPARTNERCD);
                                nextFlag = "Y";
                            }
                            else
                            {
                                Hrow.CreateCell(12).SetCellValue("");
                                Hrow.CreateCell(13).SetCellValue("");
                                Hrow.CreateCell(14).SetCellValue("");
                                Hrow.CreateCell(15).SetCellValue("");
                                Hrow.CreateCell(16).SetCellValue("");
                                Hrow.CreateCell(17).SetCellValue("");
                                Hrow.CreateCell(18).SetCellValue("");
                                nextFlag = "N";
                            }


                            endofordermsg = (currFlag == "Y" && nextFlag == "Y" && (runseqLevelCurrentData[startpointer].ORDSEQ != runseqLevelNextData[startpointer].ORDSEQ))
                                ? "Change Order No" : "";

                            Hrow.CreateCell(19).SetCellValue(endofordermsg);

                            if (msgDockPlant != "")
                            {
                                Hrow.CreateCell(20).SetCellValue(msgDockPlant);
                                msgDockPlant = "";
                            }
                            else
                            {
                                Hrow.CreateCell(20).SetCellValue("");
                            }

                            if (msg != "")
                            {
                                Hrow.CreateCell(21).SetCellValue(msg);
                                msg = "";
                            }
                            else
                            {
                                Hrow.CreateCell(21).SetCellValue("");
                            }




                            for (int r = 1; r < 21; r++)
                            {
                                Hrow.GetCell(r).CellStyle = styleContent1;
                            }

                            Hrow.GetCell(20).CellStyle = styleContent7;
                            Hrow.GetCell(21).CellStyle = styleContent7;
                        }




                    }

                    sheet.AddMergedRegion(new CellRangeAddress(startMergeCell, endMergeCell, 20, 20));
                    sheet.AddMergedRegion(new CellRangeAddress(startMergeCell, endMergeCell, 21, 21));
                    //sheet.AutoSizeColumn(20);

                    no++;
                    rows++;
                }

                #region old
                /*
                    maxpointer = currentData.Count() > nextData.Count() ? currentData.Count() : nextData.Count();

                    startpointer = 0;
                    changeFrequencyFlag = 0;
                    for (startpointer = 0; startpointer < maxpointer; startpointer++) {
                        
                        currFlag = "Y";
                        nextFlag = "Y";
                        msg = "";

                        if (startpointer < currentData.Count()) {
                            Hrow.CreateCell(4).SetCellValue(currentData[startpointer].RTEDATE);
                            Hrow.CreateCell(5).SetCellValue(currentData[startpointer].RCVCOMPDOCKCD);
                            Hrow.CreateCell(6).SetCellValue(currentData[startpointer].RTEGRPCD);
                            Hrow.CreateCell(7).SetCellValue(currentData[startpointer].ORDSEQ);
                            Hrow.CreateCell(8).SetCellValue(currentData[startpointer].ENDOFORDER);
                            Hrow.CreateCell(9).SetCellValue(currentData[startpointer].LOGPARTNERCD);
                            
                        }
                        else
                        {
                            Hrow.CreateCell(4).SetCellValue("");
                            Hrow.CreateCell(5).SetCellValue("");
                            Hrow.CreateCell(6).SetCellValue("");
                            Hrow.CreateCell(7).SetCellValue("");
                            Hrow.CreateCell(8).SetCellValue("");
                            Hrow.CreateCell(9).SetCellValue("");
                            currFlag = "N";
                        }

                        Hrow.GetCell(4).CellStyle = styleContent1;
                        Hrow.GetCell(5).CellStyle = styleContent1;
                        Hrow.GetCell(6).CellStyle = styleContent1;
                        Hrow.GetCell(7).CellStyle = styleContent1;
                        Hrow.GetCell(8).CellStyle = styleContent1;
                        Hrow.GetCell(9).CellStyle = styleContent1;


                        if (startpointer < nextData.Count())
                        {
                            Hrow.CreateCell(10).SetCellValue(nextData[startpointer].RTEDATE);
                            Hrow.CreateCell(11).SetCellValue(nextData[startpointer].RCVCOMPDOCKCD);
                            Hrow.CreateCell(12).SetCellValue(nextData[startpointer].RTEGRPCD);
                            Hrow.CreateCell(13).SetCellValue(nextData[startpointer].ORDSEQ);
                            Hrow.CreateCell(14).SetCellValue(nextData[startpointer].ENDOFORDER);
                            Hrow.CreateCell(15).SetCellValue(nextData[startpointer].LOGPARTNERCD);
                            
                        }
                        else
                        {
                            Hrow.CreateCell(10).SetCellValue("");
                            Hrow.CreateCell(11).SetCellValue("");
                            Hrow.CreateCell(12).SetCellValue("");
                            Hrow.CreateCell(13).SetCellValue("");
                            Hrow.CreateCell(14).SetCellValue("");
                            Hrow.CreateCell(15).SetCellValue("");
                            nextFlag = "N";
                        }

                        Hrow.GetCell(10).CellStyle = styleContent1;
                        Hrow.GetCell(11).CellStyle = styleContent1;
                        Hrow.GetCell(12).CellStyle = styleContent1;
                        Hrow.GetCell(13).CellStyle = styleContent1;
                        Hrow.GetCell(14).CellStyle = styleContent1;
                        Hrow.GetCell(15).CellStyle = styleContent1;

                        if (currentData.Count() == 0) {
                            msg = "New Supplier";
                        }
                        else if (nextData.Count() == 0)
                        {
                            msg = "Supplier Deletion";
                        }
                        else if (currentData.Count() != nextData.Count())
                        {
                            if(changeFrequencyFlag == 0){
                                msg = "Change Delivery Frequency";
                                changeFrequencyFlag = 1;
                            }
                            if ((currFlag == "Y" && nextFlag == "Y")) {
                                if (currentData[startpointer].RTEGRPCD != nextData[startpointer].RTEGRPCD) {
                                    msg = (msg != "") ? msg + ", " : msg + "";
                                    msg = msg + "Change Route Name";
                                }
                                if (currentData[startpointer].ENDOFORDER != nextData[startpointer].ENDOFORDER)
                                {
                                    msg = (msg != "") ? msg + ", " : msg + "";
                                    msg = msg + "Change End of Order";
                                }
                                if (currentData[startpointer].LOGPARTNERCD!= nextData[startpointer].LOGPARTNERCD)
                                {
                                    msg = (msg != "") ? msg + ", " : msg + "";
                                    msg = msg + "Change LP";
                                }
                            
                            }
                
                        }
                        //--
                        else if ((currFlag == "Y" && nextFlag == "Y"))
                        {
                            msg = "";

                            if (currentData[startpointer].RTEGRPCD != nextData[startpointer].RTEGRPCD)
                            {
                                msg = (msg != "") ? msg + ", " : msg + "";
                                msg = msg + "Change Route Name";
                            }
                            if (currentData[startpointer].ENDOFORDER != nextData[startpointer].ENDOFORDER)
                            {
                                msg = (msg != "") ? msg + ", " : msg + "";
                                msg = msg + "Change End of Order";
                            }
                            if (currentData[startpointer].LOGPARTNERCD != nextData[startpointer].LOGPARTNERCD)
                            {
                                msg = (msg != "") ? msg + ", " : msg + "";
                                msg = msg + "Change LP";
                            }
                        }
                        else
                        {
                            //Hrow.CreateCell(16).SetCellValue("");
                            msg = "";
                        }

                        Hrow.CreateCell(16).SetCellValue(msg);
                        Hrow.GetCell(16).CellStyle = styleContent1;

                        if (startpointer + 1 < maxpointer)
                        {
                            row++;
                            Hrow = sheet.CreateRow(row);

                            Hrow.CreateCell(1).SetCellValue("");
                            Hrow.CreateCell(2).SetCellValue("");
                            Hrow.CreateCell(3).SetCellValue("");

                            Hrow.GetCell(1).CellStyle = styleContent1;
                            Hrow.GetCell(2).CellStyle = styleContent1;
                            Hrow.GetCell(3).CellStyle = styleContent1;
                        }
                    }


                    no++;
                    row++;
                    Hrow = sheet.CreateRow(row);
                }


                    */
                #endregion


                //sheet.AutoSizeColumn(19);
                //sheet.AutoSizeColumn(20);
                //sheet.AutoSizeColumn(21);

                //sheet.AutoSizeColumn(3);
                //sheet.AutoSizeColumn(16);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                return ms.ToArray();
            }
            catch (Exception err)
            {
                string x = err.ToString();
                byte[] a = null;
                x = err.ToString();
                return a;
            }



        }



    }
}
