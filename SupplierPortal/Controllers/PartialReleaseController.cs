﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using Portal.Models.Globals;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using Portal.Models.PartInfo;
using DevExpress.Web.Mvc;
using System.Transactions;
using DevExpress.Web.ASPxUploadControl;
using System.Data.SqlClient;
using Portal.ExcelUpload;
using System.Web.UI.WebControls;
using System.Text;
using Portal.Models.Dock;
using Portal.Models.OrderPlan;
using Portal.Models.OrderPlanPartial;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.Plant;
using System.Threading.Tasks;

namespace Portal.Controllers
{
    public class PartialReleaseController : BaseController
    {
        public PartialReleaseController()
            : base("Partial Release")
        {
        }

        protected override void StartUp()
        {
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }



        protected override void Init()
        {

            IDBContext db = DbContext;

            OrderPlanModel modelOrderPlanInfo = new OrderPlanModel();
            OrderPlanPartialModel modelOrderPlanPartialInfo = new OrderPlanPartialModel();

            ViewData["GridPlanCd"] = Model.GetModel<User>().FilteringArea<PlantData>(
                GetAllPlant(),
                "PlantCode",
                "PartialRelease",
                "PartialReleasePlantGridLookup");

            ViewData["GridDockCd"] = Model.GetModel<User>().FilteringArea<DockData>(
                GetAllDock(),
                "DockCode",
                "PartialRelease",
                "PartialReleasePlantGridLookup");


            ViewData["GridPlanCdHold"] = Model.GetModel<User>().FilteringArea<PlantData>(
                GetAllPlant(),
                "PlantCode",
                "PartialRelease",
                "PartialReleasePlantHoldGridLookup");

            ViewData["GridDockCdHold"] = Model.GetModel<User>().FilteringArea<DockData>(
                GetAllDock(),
                "DockCode",
                "PartialRelease",
                "PartialReleasePlantHoldGridLookup");

            Model.AddModel(modelOrderPlanInfo);
            Model.AddModel(modelOrderPlanPartialInfo);
            //ViewData["OrderDate"] = DateTime.Now.ToString("dd.MM.yyyy");
        }



        [HttpGet]
        public ActionResult PartialHeader()
        {
            ViewData["GridPlanCd"] = Model.GetModel<User>().FilteringArea<PlantData>(
                GetAllPlant(),
                "PlantCode",
                "PartialRelease",
                "PartialReleasePlantGridLookup");

            ViewData["GridDockCd"] = Model.GetModel<User>().FilteringArea<DockData>(
                GetAllDock(),
                "DockCode",
                "PartialRelease",
                "PartialReleasePlantGridLookup");

            return PartialView("PartialHeader");

        }

        public ActionResult PartialHeaderHold()
        {
            ViewData["GridPlanCdHold"] = Model.GetModel<User>().FilteringArea<PlantData>(
                GetAllPlant(),
                "PlantCode",
                "PartialRelease",
                "PartialReleasePlantHoldGridLookup");

            ViewData["GridDockCdHold"] = Model.GetModel<User>().FilteringArea<DockData>(
                GetAllDock(),
                "DockCode",
                "PartialRelease",
                "PartialReleasePlantHoldGridLookup");

            return PartialView("PartialHeaderHold");

        }
        public ActionResult PartialDetail()
        {

            OrderPlanModel model = Model.GetModel<OrderPlanModel>();

            model.orderPlanDatas = getPartInfo(Request.Params["ORDER_NO"],
             Request.Params["orderDate"],
             Request.Params["PlantCd"],
             Request.Params["DockCd"]);


            Model.AddModel(model);
            return PartialView("PartialDetail", Model);
        }

        public ActionResult PartialRelease()
        {
            return PartialView("PartialRelease");
        }

        public ActionResult PartialDetail2()
        {

            OrderPlanPartialModel model2 = Model.GetModel<OrderPlanPartialModel>();

            model2.orderPlanPartialDatas = getPartInfoPartial(Request.Params["ORDER_NO"],
            Request.Params["orderDate"],
            Request.Params["PlantCd"],
            Request.Params["DockCd"]);

            Model.AddModel(model2);

            return PartialView("PartialDetail2", Model);
        }

        protected List<OrderPlanData> getPartInfo(string OrderPlanCdParam, string orderDate, string plantCd, string DockCd)
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<OrderPlanData> listTooltipDatas = new List<OrderPlanData>();

            listTooltipDatas = db.Query<OrderPlanData>("GetOrderPlanSearch",
                new object[] { OrderPlanCdParam,
                    orderDate,
                    plantCd,
                    DockCd
                }).ToList();

            db.Close();
            return listTooltipDatas;
        }

        protected List<OrderPlanPartialData> getPartInfoPartial(string OrderPlanPartialCdParam, string orderDate, string plantCdPartial, string DockCd)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<OrderPlanPartialData> listTooltipPartialDatas = new List<OrderPlanPartialData>();

            listTooltipPartialDatas = db.Query<OrderPlanPartialData>("GetOrderPlanSearchPartial",
                new object[] { OrderPlanPartialCdParam,
                    orderDate,
                    plantCdPartial,
                    DockCd
                }).ToList();

            db.Close();
            return listTooltipPartialDatas;
        }



        private List<OrderPlanData> GetAllOrderPlan()
        {
            List<OrderPlanData> PartIndoOrderPlanModel = new List<OrderPlanData>();
            IDBContext db = DbContext;
            PartIndoOrderPlanModel = db.Fetch<OrderPlanData>("GetAllOrderPlan", new object[] { });
            db.Close();

            return PartIndoOrderPlanModel;
        }

        private List<OrderPlanPartialData> GetAllOrderPlanPartial()
        {
            List<OrderPlanPartialData> PartIndoOrderPlanPartialModel = new List<OrderPlanPartialData>();
            IDBContext db = DbContext;
            PartIndoOrderPlanPartialModel = db.Fetch<OrderPlanPartialData>("GetAllOrderPlanPartial", new object[] { });
            db.Close();

            return PartIndoOrderPlanPartialModel;
        }


        public ContentResult OrderHold(string ORDER_NO, string RCV_PLANT_CODE, string DOCK_CODE, string PART_NO)
        {
            string resultMessage = "";

            string[] orderNo = ORDER_NO.Split(',');
            string[] rcvPlantCode = RCV_PLANT_CODE.Split(',');
            string[] dockCode = DOCK_CODE.Split(',');
            string[] partNo = PART_NO.Split(',');

            List<string> data = new List<string>();
            try
            {
                for (int i = 0; i < orderNo.Length - 1; i++)
                {

                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                    db.ExecuteScalar<string>("OrderHold",
                        new object[] {
                            orderNo[i],
                            rcvPlantCode[i],
                            dockCode[i],
                            partNo[i] });
                    db.Close();
                }

                resultMessage = "Order has been hold";
            }
            catch (Exception e)
            {
                resultMessage = e.ToString();
            }
            return Content(resultMessage);
        }

        public ContentResult OrderHoldAll(string ORDER_NO, string orderDate, string RCV_PLANT_CODE, string DOCK_CODE)
        {
            string resultMessage = "";


            try
            {

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                db.ExecuteScalar<string>("OrderHoldAll",
                    new object[] {
                        ORDER_NO,
                        RCV_PLANT_CODE,
                        DOCK_CODE,
                        orderDate });
                db.Close();


                resultMessage = "All Order has been hold";
            }
            catch (Exception e)
            {
                resultMessage = e.ToString();
            }
            return Content(resultMessage);
        }

        public ContentResult ReleaseOrder(string ORDER_NO, string RCV_PLANT_CODE, string DOCK_CODE, string PART_NO)
        {
            string resultMessage = "";

            string[] orderNo = ORDER_NO.Split(',');
            string[] rcvPlantCode = RCV_PLANT_CODE.Split(',');
            string[] dockCode = DOCK_CODE.Split(',');
            string[] partNo = PART_NO.Split(',');

            List<string> data = new List<string>();
            try
            {
                for (int i = 0; i < orderNo.Length - 1; i++)
                {
                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                    db.ExecuteScalar<string>("ReleaseOrder",
                        new object[] {
                            orderNo[i],
                            rcvPlantCode[i],
                            dockCode[i],
                            partNo[i] });
                    db.Close();
                }

                resultMessage = "Order has been Release";
            }
            catch (Exception e)
            {
                resultMessage = e.ToString();
            }
            return Content(resultMessage);
        }


        public ContentResult ReleaseOrderAll(string ORDER_NO, string orderDate, string RCV_PLANT_CODE, string DOCK_CODE)
        {
            string resultMessage = "";


            try
            {

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                db.ExecuteScalar<string>("ReleaseOrderAll",
                    new object[] {
                        ORDER_NO,
                        RCV_PLANT_CODE,
                        DOCK_CODE,
                        orderDate });
                db.Close();


                resultMessage = "All Order has been Release";
            }
            catch (Exception e)
            {
                resultMessage = e.ToString();
            }
            return Content(resultMessage);
        }
        public ActionResult PartialHeaderLookUp()
        {
            TempData["GridName"] = "PartialReleasePlantGridLookup";
            //List<PlantData> docks = GetAllPlant();
            List<PlantData> plants = Model.GetModel<User>().FilteringArea<PlantData>(
                GetAllPlant(),
                "PlantCode",
                "PartialRelease",
                "PartialReleasePlantGridLookup");

            ViewData["GridPlanCd"] = plants;
            return PartialView("GridLookup/PartialGrid", ViewData["GridPlanCd"]);
        }

        public ActionResult PartialHeaderHoldLookUp()
        {
            TempData["GridName"] = "PartialReleasePlantHoldGridLookup";
            //List<PlantData> docks = GetAllPlant();
            List<PlantData> plants = Model.GetModel<User>().FilteringArea<PlantData>(
                GetAllPlant(),
                "PlantCode",
                "PartialRelease",
                "PartialReleasePlantHoldGridLookup");

            ViewData["GridPlanCdHold"] = plants;
            return PartialView("GridLookup/PartialGrid", ViewData["GridPlanCdHold"]);
        }

        private List<PlantData> GetAllPlant()
        {
            List<PlantData> PartIndoDockModel = new List<PlantData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<PlantData>("GetAllPlant", new object[] { });
            db.Close();

            return PartIndoDockModel;

        }





        public ActionResult PartialHeaderDockLookUp()
        {
            TempData["GridName"] = "PartialReleaseDockGridLookup";
            //List<PlantData> docks = GetAllPlant();
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(
                GetAllDock(),
                "DockCode",
                "PartialRelease",
                "PartialReleasePlantGridLookup");

            ViewData["GridDockCd"] = docks;
            return PartialView("GridLookup/PartialGrid", ViewData["GridDockCd"]);
        }

        public ActionResult PartialHeaderDockHoldLookUp()
        {
            TempData["GridName"] = "PartialReleaseDockHoldGridLookup";
            //List<PlantData> docks = GetAllPlant();
            List<DockData> docks = Model.GetModel<User>().FilteringArea<DockData>(
                GetAllDock(),
                "DockCode",
                "PartialRelease",
                "PartialReleasePlantHoldGridLookup");

            ViewData["GridDockCdHold"] = docks;
            return PartialView("GridLookup/PartialGrid", ViewData["GridDockCdHold"]);
        }



        private List<DockData> GetAllDock()
        {
            List<DockData> PartIndoDockModel = new List<DockData>();
            IDBContext db = DbContext;
            PartIndoDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return PartIndoDockModel;

        }


        public void DownloadOrderRelease(string ORDER_NO, string orderDateRelease, string RCV_PLANT_CODE, string DOCK_CODE)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<OrderPlanData> orderReleaseExcel = new List<OrderPlanData>();

            orderReleaseExcel = db.Query<OrderPlanData>("DownloadOrderRelease",
                new object[] {
                    ORDER_NO,
                    orderDateRelease,
                    RCV_PLANT_CODE,
                    DOCK_CODE
                }).ToList();

            fileName = "OrderRelease_" + orderDateRelease + ".xls";

            data = exporter.Write(ConvertToDataTable(orderReleaseExcel), "Order Release");

            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        public void DownloadOrderHold(string ORDER_NO, string orderDateRelease, string RCV_PLANT_CODE, string DOCK_CODE)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            byte[] data;

            IExcelWriter exporter = ExcelWriter.GetInstance();
            string fileName = String.Empty;

            List<OrderPlanPartialData> orderHoldExcel = new List<OrderPlanPartialData>();

            orderHoldExcel = db.Query<OrderPlanPartialData>("DownloadOrderHold",
                new object[] {
                    ORDER_NO,
                    orderDateRelease,
                    RCV_PLANT_CODE,
                    DOCK_CODE
                }).ToList();

            fileName = "OrderHold_" + orderDateRelease + ".xls";

            data = exporter.Write(ConvertToDataTable(orderHoldExcel), "Hold Order");

            db.Close();
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(data.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(data);
            Response.End();
        }

        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}