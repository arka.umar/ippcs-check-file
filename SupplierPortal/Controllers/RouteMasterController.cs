﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Configuration;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.RouteMaster;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using System.Diagnostics;
using Toyota.Common.Web.Credential;
using Portal.Models.Globals;
using Portal.Models.DCLReport;
using Portal.Models.Master;

namespace Portal.Controllers
{
    public class RouteMasterController : BaseController
    {
       public RouteMaster modelExport;
       IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

       public RouteMasterController()
           : base("RouteMaster", "Route Master")
           
       {

       }

        
        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            RouteMasterModel mdl = new RouteMasterModel();
            
            ViewData["RouteGridLookup"] = GetAllRoute();

            //mdl.routemaster = getroutemaster(routecd
            //   , routename, routesource);
            Model.AddModel(mdl);

           
        }


        public ActionResult PartialHeader()
        {
            return PartialView("MasterIndexHeader");
        }

        public ActionResult PartialRouteDatas()
        {
            string RouteCD = String.IsNullOrEmpty(Request.Params["RouteValues"]) ? "" : Request.Params["RouteValues"];
            string RouteName = String.IsNullOrEmpty(Request.Params["RouteName"]) ? "" : Request.Params["RouteName"];
            string RouteSource = String.IsNullOrEmpty(Request.Params["RouteSource"]) ? "" : Request.Params["RouteSource"];
            
            RouteMasterModel mdl = Model.GetModel<RouteMasterModel>();

            //if (!String.IsNullOrEmpty(Request.Params["RouteValues"]))
            //{
            if (Request.Params["RouteValues"] == "x0x") RouteCD = "";
                mdl.RouteMaster = GetRouteMaster(RouteCD 
                    ,RouteName,RouteSource);
            //}

            Model.AddModel(mdl);

            return PartialView("MasterGridIndex", Model);
        }
        //buat synch
        public ContentResult Synchronize()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int isError = 0;
            string errorMessage = "";
            try
            {
                var QueryLog = db.SingleOrDefault<CheckResult>("CekQueeRoute", new object[] { });
                //db.Execute("CekQueeRoute", new object[] { });
                //return Content("Success");
                if (QueryLog.result == 1)
                {
                    isError = 1;
                    errorMessage = "Another process still runnning!";
                }
                else
                {
                    db.Execute("SynchOnDemanFrom", new object[] { "C", "IPPCS_SYN_ROUTE_MASTER" });
                    errorMessage = "Synchronize Route Master Data Success.";
                }
            }
                
            catch (Exception ex)
            {
                //return Content("Failed");
                isError = 1;
                errorMessage = " Synchronize Route Master Data Failed ";

            }
            return Content(((isError == 1) ? "Eror|" : "Success|") + errorMessage);
            db.Close();
            db.Dispose();
        }
        //"Master Route Synch"
        public ContentResult SynchronizeCheck()
        {
            string ResultCheck = "";
            string ResultMessage = "";

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                List<SyncCheck> QueryLog = db.Fetch<SyncCheck>("SynchOnDemanFrom", new object[] { "S", "IPPCS_SYN_ROUTE_MASTER" });
                for (int i = 0; i < QueryLog.Count; i++)
                {
                    //if (QueryLog[i].Job_Name == "IPPCS_SYN_ROUTE_MASTER")
                    if (QueryLog[i].Job_Name == "IPPCS_SYN_ROUTE_MASTER")
                    {
                        ResultCheck = QueryLog[i].Job_Status;
                        ResultMessage = QueryLog[i].Job_Message;
                    }
                }

                return Content(ResultCheck + "|" + ResultMessage);
            }
            catch (Exception ex)
            {
                return Content("Failed|Synchronize failed. " + ex.ToString());
            }
            db.Close();
            db.Dispose();
        }

        //batas synch
        public ContentResult DeleteMaster(string GridId)
        {
            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string route = "";
            string route_name = "";
          

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                //SPLIT ID WITH DELIMETER ";"
                char[] splitchar = { ';' };
                string[] ParamUpdate = GridId.Split(splitchar);
                int check = 0;
                //LOOP BY COUNT OF ARRAY  WAS SPLIT ";"
                for (int i = 0; i < ParamUpdate.Length; i++)
                {      
                   
                    if(ParamUpdate[i]!="") {
                        //SPLIT ID BY OBJECT WITH DELIMETER "_"
                        char[] SplitId = { '_' };
                        string[] ParamId = ParamUpdate[i].Split(SplitId);

                        route = ParamId[0];
                        route_name = ParamId[1];
            
                      

                        check = db.SingleOrDefault<int>("DeleteRouteMasterCheck", new object[] {route});}}
                    if (check != 0)
                        throw new Exception("Cannot delete data that have been used on transaction");
                else{
                        db.ExecuteScalar<string>("DeleteRouteMaster", new object[] { route, route_name});
                        //Result += "\n";
                    }
                    
              return Content("Succes To Deleted Data");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally { db.Close(); }
        }
        public ActionResult PartialHeaderRouteGridLookupPart()
        {

            TempData["GridName"] = "OIHRouteOption";
            ViewData["RouteGridLookup"] = GetAllRoute();

            return PartialView("GridLookup/PartialGrid", ViewData["RouteGridLookup"]);
        }

        //public ActionResult PartialHeaderRouteGridLookupPartname()
        //{

        //    TempData["GridName"] = "RouteName";
        //    ViewData["RouteGridLookupname"] = GetRouteName();

        //    return PartialView("GridLookup/PartialGrid", ViewData["RouteGridLookupname"]);
        //}


        

        protected List<RouteMaster> GetRouteMaster(string routeCode,string routeName,string SourceData)
        {
            List<RouteMaster> listOfRouteMaster = db.Fetch<RouteMaster>("GetRouteMaster", new object[] { routeCode,routeName,SourceData });
            return listOfRouteMaster;
            
        }
        public List<Route> GetAllRoute()
        {
            List<Route> listOfRoute = new List<Route>();
            try
            {
                listOfRoute = db.Fetch<Route>("GetAllRoute", new object[] { });
            }
            catch (Exception ex) { throw ex; }
            return listOfRoute;
        }
        //public List<Route> GetRouteName()
        //{
        //    List<Route> listOfRoute = new List<Route>();
        //    try
        //    {
        //        listOfRoute = db.Fetch<Route>("GetRouteName", new object[] { });
        //    }
        //    catch (Exception ex) { throw ex; }
        //    return listOfRoute;
        //}
        public ActionResult AddNewPart(string routeCode, string routeName, string SourceData,string status)
        {
            string ResultQuery = "Data Has Been Saved";
            
            try
            {
                if (status == "new")
                {
                    int check = 0;
                    check = db.SingleOrDefault<int>("RouteMasterAddCheck", new object[] 
                {
                   routeCode
                  
                });
                    if (check != 0)
                        throw new Exception("Data Already Exist");
                    else
                    {
                        db.Execute("InsertRouteMaster", new object[] 
                {
                   routeCode,
                   routeName,
                   SourceData,
                   AuthorizedUser.Username,
                   DateTime.Now
                });
                        //TempData["GridName"] = "PartGridView";
                    }
                }
                else if (status == "update")
                {                   
                        db.Execute("UpdateRouteMaster", new object[] 
                {
                   routeCode,
                   routeName,
                   SourceData,
                   AuthorizedUser.Username,
                   DateTime.Now
                });
                                    
                }
                
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            db.Close();
            return Content(ResultQuery);
        }

    }
}
