﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Data;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Data;
using System.Xml;
using System.Transactions;

using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Credential;

using Portal.Models.AnnualPlan;
using Portal.Models.Forecast;
using Portal.Models.Supplier;
using Portal.Models.SupplierFeedbackPart;
using Portal.Models.SupplierFeedbackSummary;
using Portal.ExcelUpload;

namespace Portal.Controllers
{
    public class AnnualPlanController : BaseController
    {
        private const string fileServerPath = "/Portal/DataExchange/";
        private FTPUpload ftpUpload = new FTPUpload();

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public AnnualPlanController()
            : base("Annual Plan")
        {
            PageLayout.UseMessageBoard = true;
        }

        #region Model properties
        List<SupplierName> _annualPlanSupplierModel = null;
        private List<SupplierName> _AnnualPlanSupplierModel
        {
            get
            {
                if (_annualPlanSupplierModel == null)
                    _annualPlanSupplierModel = new List<SupplierName>();

                return _annualPlanSupplierModel;
            }
            set
            {
                _annualPlanSupplierModel = value;
            }
        }

        AnnualPlanModel _annualPlanModel = null;
        private AnnualPlanModel _AnnualPlanModel
        {
            get
            {
                if (_annualPlanModel == null)
                    _annualPlanModel = new AnnualPlanModel();

                return _annualPlanModel;
            }
            set
            {
                _annualPlanModel = value;
            }
        }
        
        AnnualPlanDetailData _annualPlanDetailData = null;
        private AnnualPlanDetailData _AnnualPlanDetailData
        {
            get
            {
                if (_annualPlanDetailData == null)
                    _annualPlanDetailData = new AnnualPlanDetailData();

                return _annualPlanDetailData;
            }
            set
            {
                _annualPlanDetailData = value;
            }
        }

        AnnualPlanParameter _annualPlanParameterModel = null;
        private AnnualPlanParameter _AnnualPlanParameterModel
        {
            get
            {
                if (_annualPlanParameterModel == null)
                    _annualPlanParameterModel = new AnnualPlanParameter();

                return _annualPlanParameterModel;
            }
            set
            {
                _annualPlanParameterModel = value;
            }
        }

        AnnualPlanAPPSModel _annualPlanAPPSModel = null;
        private AnnualPlanAPPSModel _AnnualPlanAPPSModel
        {
            get
            {
                if (_annualPlanAPPSModel == null)
                    _annualPlanAPPSModel = new AnnualPlanAPPSModel();

                return _annualPlanAPPSModel;
            }
            set
            {
                _annualPlanAPPSModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;
        }

        #region Annual Plan Inquiry controller
        #region View controller
        public ActionResult PartialAnnualPlanInquiry()
        {
            #region Required model in partial page : for AnnualPlanInquiryHeaderPartial
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "AnnualPlan", "APIHSupplierIDLookup");
            if (suppliers.Count > 0)
            {
                _AnnualPlanSupplierModel = suppliers;
            }
            else
            {
                _AnnualPlanSupplierModel = GetAllSupplier();
            }

            Model.AddModel(_AnnualPlanSupplierModel);
            #endregion

            #region Required model in partial page : for AnnualPlanInquiryDetailPartial
            String supplierCode = String.Empty;

            if (suppliers.Count == 1)
                supplierCode = suppliers[0].SUPPLIER_CODE;

            _AnnualPlanModel.AnnualPlanHeaderList = GetAllAnnualPlanHeader(Request.Params["APYear"], Request.Params["Version"], String.IsNullOrEmpty(supplierCode) ? Request.Params["SupplierCode"] : supplierCode);
            Model.AddModel(_AnnualPlanModel);
            #endregion

            return PartialView("AnnualPlanInquiryPartial", Model);
        }

        public ActionResult PartialHeaderSupplierLookupGridAnnualPlanInquiry()
        {
            #region Required model in partial page : for AnnualPlanInquiryHeaderPartial APIHSupplierIDLookup
            TempData["GridName"] = "APIHSupplierIDLookup";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "AnnualPlan", "APIHSupplierIDLookup");
            if (suppliers.Count > 0)
                _AnnualPlanSupplierModel = suppliers;
            else
                _AnnualPlanSupplierModel = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", _AnnualPlanSupplierModel);
        }

        public ActionResult PartialDetailAnnualPlanInquiry()
        {
            // get current user login detail
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "AnnualPlan", "APIHSupplierIDLookup");
            String supplierCode = String.Empty;
            if (suppliers.Count == 1)
            {
                supplierCode = suppliers[0].SUPPLIER_CODE;
            }

            #region Required model in partial page : for AnnualPlanInquiryDetailPartial
            _AnnualPlanModel.AnnualPlanHeaderList = GetAllAnnualPlanHeader(Request.Params["APYear"], Request.Params["Version"], String.IsNullOrEmpty(supplierCode) ? Request.Params["SupplierCode"] : supplierCode);
            Model.AddModel(_AnnualPlanModel);
            #endregion

            return PartialView("AnnualPlanInquiryDetailPartial", Model);
        }
        #endregion

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> annualPlanSupplierModelList = new List<SupplierName>();
            IDBContext db = DbContext;
            try
            {
                annualPlanSupplierModelList = db.Fetch<SupplierName>("GetAllSupplierForecast", new object[] { "" });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return annualPlanSupplierModelList;
        }

        private List<AnnualPlanHeaderData> GetAllAnnualPlanHeader(string apYear, string version, string supplier)
        {
            List<AnnualPlanHeaderData> annualPlanHeaderData = new List<AnnualPlanHeaderData>();
            IDBContext db = DbContext;
            try
            {
                annualPlanHeaderData = db.Fetch<AnnualPlanHeaderData>("GetAllAnnualPlanHeader", new object[] { apYear, version, supplier });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return annualPlanHeaderData;
        }        
        #endregion

        #region Download Controller
        /// <summary>
        /// Convert IEnumberable into DataTable, for excel exporting purpose
        /// </summary>
        /// <param name="ien"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IEnumerable<object> ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                System.Reflection.PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (System.Reflection.PropertyInfo pi in pis)
                    {
                        if (pi.PropertyType == typeof(DateTime?))
                            dt.Columns.Add(pi.Name, typeof(DateTime));
                        else if (pi.PropertyType == typeof(Boolean?))
                            dt.Columns.Add(pi.Name, typeof(Boolean));
                        else
                            dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (System.Reflection.PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value == null ? DBNull.Value : value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DownloadAnnualPlanDetailData(String apYear, String version, String supplierCode)
        {
            string fileName = "AnnualPlanDetailData" + apYear.Trim() + version.Trim() + supplierCode.Trim() + ".xls";

            // Update download user and status
            IDBContext db = DbContext;
            IEnumerable<AnnualPlanDetailData> qry = db.Fetch<AnnualPlanDetailData>("GetAllAnnualPlanDetail", new object[] { apYear, version, supplierCode });
            db.Close();
           
            IExcelWriter exporter = ExcelWriter.GetInstance();

            byte[] hasil = exporter.Write(ConvertToDataTable(qry), "AnnualPlanDetailData");
            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            /*
             * @Lufty: numpang nambahin, kisanak
             */
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion

        #region Unlock Controller
        #endregion
        #endregion

        #region Annual Plan Inquiry Detail Data controller
        #region View controller
        public ActionResult PartialAnnualPlanInquiryDetailData()
        {
            #region Required model in partial page
            #region Model for AnnualPlanInquiryDetailDataHeaderPartial
            _AnnualPlanParameterModel.APYear = Request.Params["APYear"];
            _AnnualPlanParameterModel.Version = Request.Params["Version"];
            _AnnualPlanParameterModel.SupplierCode = Request.Params["SupplierCode"];
            Model.AddModel(_AnnualPlanParameterModel);
            #endregion

            #region Model for AnnualPlanInquiryDetailDataDetailPartial
            _AnnualPlanModel = GetAllAnnualPlanDetail(_AnnualPlanParameterModel.APYear, _AnnualPlanParameterModel.Version, _AnnualPlanParameterModel.SupplierCode);
            Model.AddModel(_AnnualPlanModel);
            #endregion
            #endregion

            return PartialView("AnnualPlanInquiryDetailDataPartial", Model);
        }

        public ActionResult PartialAnnualPlanUnlockImagePartial()
        {
            Model.AddModel(_AnnualPlanDetailData);

            return PartialView("AnnualPlanUnlockDetailConfirmationPartial", Model);
        }

        public ActionResult PartialDetailAnnualPlanInquiryDetailData()
        {
            #region Model for AnnualPlanInquiryDetailDataDetailPartial
            _AnnualPlanModel = GetAllAnnualPlanDetail(Request.Params["APYear"], Request.Params["Version"], Request.Params["SupplierCode"]);
            Model.AddModel(_AnnualPlanModel);
            #endregion

            return PartialView("AnnualPlanInquiryDetailDataDetailPartial", Model);
        }
        #endregion

        #region Database controller
        private AnnualPlanModel GetAllAnnualPlanDetail(String apYear, String version, String supplierCode)
        {
            AnnualPlanModel annualPlanModelList = new AnnualPlanModel();
            IDBContext db = DbContext;
            annualPlanModelList.AnnualPlanDetailList = db.Fetch<AnnualPlanDetailData>("GetAllAnnualPlanDetail", new object[] { apYear, version, supplierCode});
            db.Close();

            return annualPlanModelList;
        }
        #endregion
        #endregion
        
        #region Annual Plan APPS controller
        #region View controller
        public ActionResult PartialAnnualPlanAPPS()
        {
            #region Required model in partial page
            #region Model for AnnualPlanAPPSDetailPartial
            _AnnualPlanAPPSModel.AnnualPlanPullHeaderList = GetAllAnnualPlanPullHeader();
            Model.AddModel(_AnnualPlanAPPSModel);
            #endregion
            #endregion

            return PartialView("AnnualPlanAPPSPartial", Model);
        }

        public ActionResult PartialDetailAnnualPlanAPPS()
        {
            #region Model for AnnualPlanAPPSDetailPartial
            _AnnualPlanAPPSModel.AnnualPlanPullHeaderList = GetAllAnnualPlanPullHeader();
            Model.AddModel(_AnnualPlanAPPSModel);
            #endregion

            return PartialView("AnnualPlanAPPSDetailPartial", Model);
        }

        public ActionResult PartialDetailAnnualPlanAPPSDetailData()
        {
            #region Model for AnnualPlanAPPSDetailDataDetailPartial
            _AnnualPlanAPPSModel.AnnualPlanPullDetailList = GetAllAnnualPlanPullDetail(
                Request.Params["PlanningType"]
                ,Request.Params["APYear"]
                ,Request.Params["Version"]
                );
            Model.AddModel(_AnnualPlanAPPSModel);
            #endregion

            return PartialView("AnnualPlanAPPSDetailDataDetailPartial", Model);
        }

        public ActionResult PartialDetailAnnualPlanAPPSCrosstabDetailData()
        {
            #region Model for AnnualPlanAPPSCrosstabDetailDataDetailPartial
            _AnnualPlanAPPSModel.AnnualPlanCrosstabDetailList = GetAllAnnualPlanCrosstabDetail(
                Request.Params["PlanningType"]
                , Request.Params["APYear"]
                , Request.Params["Version"]
                );
            Model.AddModel(_AnnualPlanAPPSModel);
            #endregion

            return PartialView("AnnualPlanAPPSCrosstabDetailDataDetailPartial", Model);
        }
        #endregion

        #region Database controller
        public ContentResult ImportAPPS(String planning, String apYear, String version)
        {
            String resultMessage = "";
            resultMessage = "Import from APPS success.";

            if ((planning != "" && planning != null) || (apYear != "" && apYear != null) || (version != "" && version != null))
            {
                IDBContext db = DbContext;

                try
                {
                    String configName = "DB2_PCS_PROD";
                    String query = "";
                    DataTable dbResult = new DataTable();

                    System.Configuration.ConnectionStringSettings dbSettings = System.Configuration.ConfigurationManager.ConnectionStrings[configName];
                    String dbConnectionString = dbSettings.ConnectionString;
                    System.Data.Common.DbProviderFactory dbProvider = System.Data.Common.DbProviderFactories.GetFactory(dbSettings.ProviderName);
                    System.Data.Common.DbConnection dbConnection = dbProvider.CreateConnection();
                    System.Data.Common.DbDataAdapter dbAdapter = dbProvider.CreateDataAdapter();
                    System.Data.Common.DbCommand dbCommand = dbConnection.CreateCommand();

                    // 1. SELECT FROM APPDLIB.ROAPBRERES
                    using (dbConnection)
                    {
                        dbConnection.ConnectionString = dbConnectionString;
                        dbConnection.Open();

                        dbAdapter.SelectCommand = dbCommand;

                        string tableName = "APPDLIB.ROAPBRERES";
                        switch (version)
                        {
                            case "OAP":
                                tableName = "APPDLIB.ROAPBRERES";
                                break;
                            case "RAP":
                                tableName = "APPDLIB.RRAPBRERES";
                                break;
                            case "PRJ":
                                tableName = "APPDLIB.RPAPBRERES";
                                break;
                        }

                        query = "SELECT * FROM " + tableName +
                                " WHERE " +
                                    "CATEGCD = '" + planning + "' " +
                                    "AND VERSICD = '" + version + "' " +
                                    "AND APYEAR = '" + apYear + "'";

                        dbCommand.CommandText = query;
                        dbAdapter.Fill(dbResult);
                        dbConnection.Close();
                    }

                    // 2. INSERT INTO TB_T_PULL_FROM_APPS
                    #region Pulls From APPS - Bulk Copy
                    configName = "PCS";
                    dbSettings = System.Configuration.ConfigurationManager.ConnectionStrings[configName];
                    dbConnectionString = dbSettings.ConnectionString;

                    db.ExecuteScalar<string>("DeleteAnnualPlan_PullFromAPPS", new object[] { version, apYear, planning });

                    using (System.Data.SqlClient.SqlBulkCopy sqlBulkCopy = new System.Data.SqlClient.SqlBulkCopy(dbConnectionString, System.Data.SqlClient.SqlBulkCopyOptions.Default))
                    {
                        sqlBulkCopy.BatchSize = 10000;
                        sqlBulkCopy.DestinationTableName = "dbo.TB_T_PULL_FROM_APPS_D";

                        foreach (DataRow dbRow in dbResult.Rows)
                        {
                            dbRow[23] = AuthorizedUser.Username;
                            dbRow[24] = DateTime.Now;

                            dbRow.AcceptChanges();
                        }

                        sqlBulkCopy.ColumnMappings.Add(2, "AP_YEAR");
                        sqlBulkCopy.ColumnMappings.Add(3, "CATEGORY_CD");
                        sqlBulkCopy.ColumnMappings.Add(0, "VERSION_CD");
                        sqlBulkCopy.ColumnMappings.Add(1, "EDITION");
                        sqlBulkCopy.ColumnMappings.Add(4, "PLANT_CD");
                        sqlBulkCopy.ColumnMappings.Add(5, "CAR_FAMILY_CD");
                        sqlBulkCopy.ColumnMappings.Add(6, "PART_NO");
                        sqlBulkCopy.ColumnMappings.Add(7, "COLOR_SFX");
                        sqlBulkCopy.ColumnMappings.Add(8, "AP_OF");
                        sqlBulkCopy.ColumnMappings.Add(9, "PART_MATCHING_KEY");
                        sqlBulkCopy.ColumnMappings.Add(10, "SUPPLIER_CD");
                        sqlBulkCopy.ColumnMappings.Add(11, "SUPPLIER_PLANT_CD");
                        sqlBulkCopy.ColumnMappings.Add(12, "SUPPLIER_NAME");
                        sqlBulkCopy.ColumnMappings.Add(13, "PART_NAME");
                        sqlBulkCopy.ColumnMappings.Add(14, "KANBAN_SIZE");
                        sqlBulkCopy.ColumnMappings.Add(15, "PROD_MONTH");
                        sqlBulkCopy.ColumnMappings.Add(16, "VOLUME");
                        sqlBulkCopy.ColumnMappings.Add(17, "FINAL_VOLUME");
                        sqlBulkCopy.ColumnMappings.Add(18, "VENDOR_SHARE_GROUP_NO");
                        sqlBulkCopy.ColumnMappings.Add(19, "VENDOR_SHARE_TYPE");
                        sqlBulkCopy.ColumnMappings.Add(20, "VENDOR_SHARE_VALUE");
                        sqlBulkCopy.ColumnMappings.Add(23, "CREATED_BY");
                        sqlBulkCopy.ColumnMappings.Add(24, "CREATED_DT");

                        sqlBulkCopy.WriteToServer(dbResult);
                    }

                    db.ExecuteScalar<string>("InsertAnnualPlan_PullFromAPPS", new object[] { version, apYear, planning, AuthorizedUser.Username, DateTime.Now });
                    #endregion
                }
                catch (Exception exc)
                {
                    resultMessage = exc.Message;
                }
                finally
                {
                    db.Close();
                }
            }
            else
            {
                resultMessage = "Import from APPS failed, neither Planning, AP Year nor Version is choosed.";
            }

            return Content(resultMessage.ToString());
        }

        public ContentResult ApproveAPPS(String parameterList)
        {
            char[] rowSeparator = { ';' };
            char[] fieldSeparator = { '|' };
            string[] rowList = parameterList.Split(rowSeparator);
            string[] fieldList;

            string resultMessage = "Approve APPS Success!";

            if ((rowList != null) && (rowList.Length > 0))
            {
                List<IDictionary<string, string>> dataList = new List<IDictionary<string, string>>();
                IDictionary<string, string> dataMap;
                for (int i = 0; i < rowList.Length; i++)
                {
                    fieldList = rowList[i].Split(fieldSeparator);
                    dataMap = new Dictionary<string, string>();
                    dataMap.Add("PlanningType", fieldList[0]);
                    dataMap.Add("APYear", fieldList[1]);
                    dataMap.Add("Version", fieldList[2]);
                    dataList.Add(dataMap);
                }

                IDBContext db = DbContext;
                try
                {
                    foreach (IDictionary<string, string> map in dataList)
                    {
                        // 3. CROSTAB INTO TB_R_ANNUAL_CROSSTAB
                        db.ExecuteScalar<string>(
                            "InsertAnnualPlan_CrossTab"
                            ,new object[] { 
                                map["Version"]
                                ,map["APYear"]
                                ,map["PlanningType"]
                                ,AuthorizedUser.Username
                                ,DateTime.Now 
                            });

                        // 4. COMBINE INTO TB_R_ANNUAL_COMBINE
                        db.ExecuteScalar<string>(
                            "InsertAnnualPlan_Combine"
                            ,new object[] { 
                                 map["Version"]
                                ,map["APYear"]
                                ,map["PlanningType"]
                                ,AuthorizedUser.Username
                                ,DateTime.Now 
                            });
                    }
                }
                catch (Exception exc)
                {
                    resultMessage = exc.Message;
                }
                finally
                {
                    db.Close();
                }
            }

            return Content(resultMessage.ToString());
        }

        public ContentResult DeleteAPPS(String parameterList)
        {
            char[] rowSeparator = { ';' };
            char[] fieldSeparator = { '|' };
            string[] rowList = parameterList.Split(rowSeparator);
            string[] fieldList;

            string resultMessage = "Delete APPS Success!";

            if ((rowList != null) && (rowList.Length > 0))
            {
                List<IDictionary<string, string>> dataList = new List<IDictionary<string, string>>();
                IDictionary<string, string> dataMap;
                for (int i = 0; i < rowList.Length; i++)
                {
                    fieldList = rowList[i].Split(fieldSeparator);
                    dataMap = new Dictionary<string, string>();
                    dataMap.Add("PlanningType", fieldList[0]);
                    dataMap.Add("APYear", fieldList[1]);
                    dataMap.Add("Version", fieldList[2]);
                    dataList.Add(dataMap);
                }

                IDBContext db = DbContext;
                try
                {
                    foreach (IDictionary<string, string> map in dataList)
                    {
                        db.ExecuteScalar<string>(
                            "DeleteAnnualPlan_PullFromAPPS"
                            ,new object[] { 
                                map["Version"]
                                ,map["APYear"]
                                ,map["PlanningType"]
                            });
                    }
                }
                catch (Exception exc)
                {
                    resultMessage = exc.Message;
                }
                finally
                {
                    db.Close();
                }
            }

            return Content(resultMessage.ToString());
        }

        public void DownloadAPPS(String planningType, String apYear, String version, String downloadType)
        {
            string fileName = String.Empty;
            string tabName = String.Empty;

            // Update download user and status
            IDBContext db = DbContext;
            IEnumerable<AnnualPlanPullDetailData> qryRaw = null;
            IEnumerable<AnnualPlanCrosstabDetailData> qryCrosstab = null;

            fileName = "AnnualPlan" + downloadType + "DetailData" + planningType + apYear + version + ".xls";
            if (downloadType == "Raw")
            {
                qryRaw = db.Fetch<AnnualPlanPullDetailData>("GetAllAnnualPlanPullDetail", new object[] { planningType, apYear, version });
            } 
            else if (downloadType == "Crosstab")
            {
                qryCrosstab = db.Fetch<AnnualPlanCrosstabDetailData>("GetAllAnnualPlanCrosstabDetail", new object[] { planningType, apYear, version });
            }

            IExcelWriter exporter = ExcelWriter.GetInstance();

            db.Close();

            tabName = "AnnualPlan" + downloadType + "DetailData";
            byte[] hasil = null;
            if (downloadType == "Raw")
            {
                hasil = exporter.Write(ConvertToDataTable(qryRaw), tabName);
            }
            else if (downloadType == "Crosstab")
            {
                hasil = exporter.Write(ConvertToDataTable(qryCrosstab), tabName);
            }

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            /*
             * @Lufty: numpang nambahin, kisanak
             */
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            /* ------------------------------*/
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        private List<AnnualPlanPullHeaderData> GetAllAnnualPlanPullHeader()
        {
            List<AnnualPlanPullHeaderData> annualPlanPullHeaderData = new List<AnnualPlanPullHeaderData>();
            IDBContext db = DbContext;
            try
            {
                annualPlanPullHeaderData = db.Fetch<AnnualPlanPullHeaderData>("GetAllAnnualPlanPullHeader", new object[] { });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return annualPlanPullHeaderData;
        }

        private List<AnnualPlanPullDetailData> GetAllAnnualPlanPullDetail(String planningType, String apYear, String version)
        {
            List<AnnualPlanPullDetailData> annualPlanPullDetailData = new List<AnnualPlanPullDetailData>();
            IDBContext db = DbContext;
            try
            {
                annualPlanPullDetailData = db.Fetch<AnnualPlanPullDetailData>("GetAllAnnualPlanPullDetail", new object[] { planningType, apYear, version});
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return annualPlanPullDetailData;
        }

        private List<AnnualPlanCrosstabDetailData> GetAllAnnualPlanCrosstabDetail(String planningType, String apYear, String version)
        {
            List<AnnualPlanCrosstabDetailData> annualPlanCrosstabDetailData = new List<AnnualPlanCrosstabDetailData>();
            IDBContext db = DbContext;
            try
            {
                annualPlanCrosstabDetailData = db.Fetch<AnnualPlanCrosstabDetailData>("GetAllAnnualPlanCrosstabDetail", new object[] { planningType, apYear, version });
            }
            catch (Exception exc) { throw exc; }
            finally { db.Close(); }

            return annualPlanCrosstabDetailData;
        }
        #endregion
        #endregion

        #region Annual Plan Unclock
        public ContentResult UnlockAnnualPlan(String productionYearD, String versionD, String sourceD, String apofD, String suspedstatD)
        {
            String resultMessage = "";

            try
            {
                if ((productionYearD != "" && productionYearD != null) || (versionD != "" && versionD != null) || (sourceD != "" && sourceD != null) || (apofD != "" && apofD != null))
                {

                    IDBContext db = DbContext;
                    resultMessage += db.ExecuteScalar<string>("UnlockAnnualPlan", new object[] { productionYearD, versionD, sourceD, apofD, suspedstatD, AuthorizedUser.Username });
                    db.Close();

                    resultMessage = "Unlock Annual Plan success.";
                }
                else
                {
                    resultMessage = "Unlock Annual Plan failed, neither production year nor version no either supplier is filled.";
                }
            }
            catch (Exception alerterrorunlockAPCRUD)
            {
                resultMessage = "Terdapat error pada saat sistem coba mengeksekusi annual plan eventhandler seko controllere";
                resultMessage = alerterrorunlockAPCRUD.Message + alerterrorunlockAPCRUD.Source + alerterrorunlockAPCRUD.StackTrace + alerterrorunlockAPCRUD.TargetSite;
            }

            return Content(resultMessage.ToString());
        }
        #endregion
    }
}
