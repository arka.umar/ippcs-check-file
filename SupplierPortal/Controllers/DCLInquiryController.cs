﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.UI.WebControls;
using Toyota.Common.Web.MVC;
using Portal.Models.DCLInquiry;
using Portal.Models.Globals;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Telerik.Reporting.Processing;
using System.Data;
using System.Data.OleDb;
using Telerik.Reporting.XmlSerialization;
using DevExpress.Web.Mvc;
using System.Threading;
using Toyota.Common.Web.Util.Configuration;

using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;

using DevExpress.Web.ASPxUploadControl;

using Toyota.Common.Web.Credential;
using DevExpress.Data;
using DevExpress.Data.Filtering;

namespace Portal.Controllers
{
    public class DCLInquiryController : BaseController
    {
        //change
        string p_DeliveryType = "R";
        string p_LPCode = "";
        DateTime p_PickUpDateFrom = DateTime.Now.AddDays(-1);
        DateTime p_PickUpDateTo = DateTime.Now;
        string p_Route = "";
        string p_Rate = "";
        string p_DockCode = "";
        string p_DeliveryNo = "";
        string p_DeliveryStatus = "";
        string p_DeliveryStat2 = "";
        //change

        public DCLInquiryController()
            : base("Delivery Tracking")
        {
            PageLayout.UseMessageBoard = true;
        }

        List<LogisticPartner> _logisticPartnerModel = null;
        private List<LogisticPartner> _LogisticPartnerModel
        {
            get
            {
                if (_logisticPartnerModel == null)
                    _logisticPartnerModel = new List<LogisticPartner>();

                return _logisticPartnerModel;
            }
            set
            {
                _logisticPartnerModel = value;
            }
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        protected override void StartUp()
        {

        }

        protected override void Init()
        {
            PageLayout.UseSlidingLeftPane = true;
            PageLayout.UseSlidingBottomPane = false;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryDock = db.Query<Dock>("GetAllDock");

            ViewData["DockCode"] = QueryDock;

            //List<LogisticPartner> listLP = new List<LogisticPartner>();
            //var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            //listLP = QueryLog.ToList<LogisticPartner>();

            TempData["GridName"] = "grlLPHeader";
            List<LogisticPartner> listLP = Model.GetModel<User>().FilteringArea<LogisticPartner>(GetAllLogisticPartner(), "LPCode", "DCLInquiry", "grlLPHeader");

            if (listLP.Count > 0)
            {
                _LogisticPartnerModel = listLP;
            }
            else
            {
                _LogisticPartnerModel = GetAllLogisticPartner();
            }

            ViewData["LogisticPartnerData"] = _LogisticPartnerModel;
            db.Close();

            ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");

            
            //DCLInquiryModel mdl = new DCLInquiryModel();

            //mdl.DCLInquiryDbModel = DCLget("R", listLP.Count == 1 ? listLP[0].LPCode : "", DateTime.Now.AddDays(-1), DateTime.Now, "", "", "", "", "", "");
            //mdl.DCLInquiryAdditionals = new List<DCLInquiryDb>();//DCLget("A", listLP.Count == 1 ? listLP[0].LPCode : "", DateTime.Now.AddDays(-1), DateTime.Now, "", "", "", "", "", "");
            //Model.AddModel(mdl);

            DCLInquiryModel mdl = new DCLInquiryModel();

            if (mdl.GvModel == null)
            {
                mdl.GvModel = CreateGridViewModel();
                mdl.GvModelAdditional = CreateGridViewModel();

                List<DCLInquiryDb> DCLListAdd = new List<DCLInquiryDb>();

                decorateModelBindingCustom_init(mdl.GvModel);

                mdl.DCLInquiryDbModel = DCLListAdd;
                mdl.DCLInquiryAdditionals = DCLListAdd;
            }



            Model.AddModel(mdl);
        }

        protected List<DCLInquiryDb> DCLget(string DeliveryType, string LPCode, DateTime PickUpDateFrom, DateTime PickUpDateTo, string Route, string Rate, string DockCode, string DeliveryNo, string DeliveryStatus, string DeliveryStat2)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<DCLInquiryDb> DCLListAdd = new List<DCLInquiryDb>();
            //var QueryLog = db.Query<DCLInquiryDb>("SelectDCLInquiryHeader", new Object[] { DeliveryType, LPCode, PickUpDateFrom, PickUpDateTo, Route, Rate, DockCode, DeliveryNo, DeliveryStatus, DeliveryStat2 });

            //DCLListAdd = db.Query<DCLInquiryDb>("SelectDCLInquiryHeader", new Object[] { DeliveryType, LPCode, PickUpDateFrom, PickUpDateTo, Route, Rate, DockCode, DeliveryNo, DeliveryStatus, DeliveryStat2 }).ToList();

            //if (QueryLog.Any())
            //{
            //    int count = 0;
            //    foreach (var q in QueryLog)
            //    {
            //        DCLListAdd.Add(new DCLInquiryDb()
            //        {
            //            DELIVERY_NO = q.DELIVERY_NO,
            //            REVISE_NO = q.REVISE_NO,
            //            PICKUP_DT = q.PICKUP_DT,
            //            ROUTE_RATE = q.ROUTE_RATE,
            //            LP_CD = q.LP_CD,
            //            LOG_PARTNER_NAME = q.LOG_PARTNER_NAME,
            //            CONFIRM_STS = q.CONFIRM_STS != null ? q.CONFIRM_STS : string.Empty,
            //            DRIVER_NAME = q.DRIVER_NAME != null ? q.DRIVER_NAME : string.Empty,
            //            DELIVERY_STS = q.DELIVERY_STS,
            //            DEPARTURE_STATUS = q.DEPARTURE_STATUS,
            //            ARRIVAL_STATUS = Convert.ToString(q.ARRIVAL_STATUS),
            //            REQUISITIONER_BY = q.REQUISITIONER_BY,
            //            REQUISITIONER_DT = q.REQUISITIONER_DT,
            //            DOWNLOAD_BY = q.DOWNLOAD_BY,
            //            DOWNLOAD_DT = q.DOWNLOAD_DT,
            //            CREATED_BY = q.CREATED_BY,
            //            CREATED_DT = q.CREATED_DT,
            //            INVOICE_BY = q.INVOICE_BY,
            //            INVOICE_DT = q.INVOICE_DT,
            //            DELIVERY_REASON = q.DELIVERY_REASON,
            //            APPROVE_BY = q.APPROVE_BY,
            //            APPROVE_DT = q.APPROVE_DT,
            //            Notif = SetNoticeIconString(q.Notif),
            //            DriverManifest = SetDownloadIcon(count),
            //            VEHICLE_NO = q.VEHICLE_NO,
            //            MAP = "Show"
            //        });
            //        count++;
            //    }
            //}
            db.Close();
            return DCLListAdd;
        }

        private string SetDownloadIcon(int val)
        {
            string img = string.Empty;
            string imageBytes3 = "~/Content/Images/pdf_icon.png";
            img = imageBytes3;
            return img;
        }

        private string SetNoticeIconString(string val)
        {
            string img = string.Empty;
            string imageBytes1 = "~/Content/Images/notice_open_icon.png";
            string imageBytes2 = "~/Content/Images/notice_new_icon.png";
            string imageBytes3 = "~/Content/Images/notice_close_icon.png";
            string imageBytes4 = "~/Content/Images/notice_add_icon.png";

            if (Convert.ToInt16(val) % 4 == 0)
            {
                img = imageBytes4;
            }
            else if (Convert.ToInt16(val) % 4 == 1)
            {
                img = imageBytes2;
            }
            else if (Convert.ToInt16(val) % 4 == 2)
            {
                img = imageBytes1;
            }
            else if (Convert.ToInt16(val) % 4 == 3)
            {
                img = imageBytes3;
            }
            return img;
        }

        private string SetStatusString(int val)
        {
            string stat = string.Empty;
            string status1 = "Downloaded";
            string status2 = "Approved";
            string status3 = "Delivered";
            string status4 = "Invoiced";

            if (val % 4 == 0)
            {
                stat = status1;
            }
            else if (val % 4 == 1)
            {
                stat = status2;
            }
            else if (val % 4 == 2)
            {
                stat = status3;
            }
            else if (val % 4 == 3)
            {
                stat = status4;
            }

            return stat;
        }

        public ActionResult UpdateCallBackPanel()
        {
            ViewData["DeliveryNo"] = Request.Params["DeliveryNo"];
            return PartialView("PopUpCallBackPartial");
        }

        public ActionResult UpdateCallBackPanelAdditional()
        {
            ViewData["DeliveryNo"] = Request.Params["DeliveryNo"];
            return PartialView("PopUpCallBackAdditionalPartial");
        }

        private List<LogisticPartner> GetAllLogisticPartner()
        {
            List<LogisticPartner> logisticPartnerModel = new List<LogisticPartner>();
            IDBContext db = DbContext;
            try
            {
                logisticPartnerModel = db.Fetch<LogisticPartner>("GetAllLogisticPartner");
            }
            catch (Exception exc) { throw exc; }
            db.Close();

            return logisticPartnerModel;
        }

        //public List<LogisticPartner> GetAllLogisticPartners()
        //{
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
        //    var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
        //    listLogisticPartner = QueryLog.ToList<LogisticPartner>();
        //    db.Close();
        //    return listLogisticPartner;
        //}

        public ActionResult LPPartial()
        {
            TempData["GridName"] = "grlLPHeader";
            //ViewData["LogisticPartnerData"] = GetAllLogisticPartners();

            List<LogisticPartner> listLP = Model.GetModel<User>().FilteringArea<LogisticPartner>(GetAllLogisticPartner(), "LPCode", "DCLInquiry", "grlLPHeader");

            if (listLP.Count > 0)
            {
                _LogisticPartnerModel = listLP;
            }
            else
            {
                _LogisticPartnerModel = GetAllLogisticPartner();
            }

            ViewData["LogisticPartnerData"] = _LogisticPartnerModel;

            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult LPPartialRegular()
        {
            TempData["GridName"] = "grlLPRegular";
            //ViewData["LogisticPartnerData"] = GetAllLogisticPartners();

            List<LogisticPartner> listLP = Model.GetModel<User>().FilteringArea<LogisticPartner>(GetAllLogisticPartner(), "LPCode", "DCLInquiry", "grlLPRegular");

            if (listLP.Count > 0)
            {
                _LogisticPartnerModel = listLP;
            }
            else
            {
                _LogisticPartnerModel = GetAllLogisticPartner();
            }

            ViewData["LogisticPartnerData"] = _LogisticPartnerModel;

            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult LPPartialAdd()
        {
            TempData["GridName"] = "grlLPAdd";
            //ViewData["LogisticPartnerData"] = GetAllLogisticPartners();

            List<LogisticPartner> listLP = Model.GetModel<User>().FilteringArea<LogisticPartner>(GetAllLogisticPartner(), "LPCode", "DCLInquiry", "grlLPAdd");

            if (listLP.Count > 0)
            {
                _LogisticPartnerModel = listLP;
            }
            else
            {
                _LogisticPartnerModel = GetAllLogisticPartner();
            }

            ViewData["LogisticPartnerData"] = _LogisticPartnerModel;

            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult LPPartialEdit()
        {
            TempData["GridName"] = "grlLPRegular";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();

            ViewData["LogisticPartnerData"] = listLogisticPartner;
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["LogisticPartnerData"]);
        }

        public ActionResult LPPartialEditAdditional()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            List<LogisticPartner> listLogisticPartner = new List<LogisticPartner>();
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            listLogisticPartner = QueryLog.ToList<LogisticPartner>();

            ViewData["LogisticPartnerData"] = listLogisticPartner;
            db.Close();
            return PartialView("PartialLogisticPartnerGridPopupAdd", ViewData["LogisticPartnerData"]);
        }

        public ActionResult DockCodePartial()
        {
            TempData["GridName"] = "grlDock";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            var QueryDock = db.Query<Dock>("GetAllDock");
            ViewData["DockCode"] = QueryDock;
            db.Close();
            return PartialView("GridLookup/PartialGrid", ViewData["DockCode"]);
        }

        public ActionResult PartialDCLInquiry(string DeliveryNo)
        {
            ViewData["DeliveryNo"] = DeliveryNo;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            DCLInquiry DeliveryDetail = new DCLInquiry();
            DeliveryDetail = db.SingleOrDefault<DCLInquiry>("GetDCLDetail", new Object[] { DeliveryNo });
            if (DeliveryDetail == null)
            {
                DeliveryDetail = new DCLInquiry
                {
                    DeliverNo = "",
                    RevisionNo = 1,
                    PickUpDate = "",
                    RouteRate = "",
                    DriverName = "",
                    DriverManifest = "",
                    DownloadBy = "",
                    DownloadTime = DateTime.Now,
                    LogisticPartner = "",
                    ApprovalStatus = "",
                    DeliveryStatus = "",
                    ArrivalAt = "",
                    LPConfirmation = "",

                    Notice = "",
                    DeliveryReason = "",
                    ReviseBy = "",
                    ReviseDate = DateTime.Now,
                    Approver = "",
                    ApprovalDate = DateTime.Now,

                    Status = "",
                    DeliveryBy = "",
                    DeliveryDate = DateTime.Now,
                    InvoiceBy = "",
                    InvoiceDate = DateTime.Now,
                    Requisitioner = "",
                    RequisitionDate = DateTime.Now,
                    LPName = ""
                };
            }
            db.Close();
            return PartialView("PopupRegulerOrderHeader", DeliveryDetail);
        }

        public ActionResult PartialDCLAdditionalInquiry(string DeliveryNo)
        {
            ViewData["DeliveryNo"] = DeliveryNo;
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            DCLInquiry DeliveryDetail = new DCLInquiry();
            DeliveryDetail = db.SingleOrDefault<DCLInquiry>("GetDCLDetail", new Object[] { DeliveryNo });
            if (DeliveryDetail == null)
            {
                DeliveryDetail = new DCLInquiry
                {
                    DeliverNo = "",
                    RevisionNo = 0,
                    PickUpDate = "",
                    RouteRate = "",
                    DriverName = "",
                    DriverManifest = "",
                    DownloadBy = "",
                    DownloadTime = DateTime.Now,
                    LogisticPartner = "",
                    ApprovalStatus = "",
                    DeliveryStatus = "",
                    ArrivalAt = "",
                    LPConfirmation = "",

                    Notice = "",
                    DeliveryReason = "",
                    ReviseBy = "",
                    ReviseDate = DateTime.Now,
                    Approver = "",
                    ApprovalDate = DateTime.Now,

                    Status = "",
                    DeliveryBy = "",
                    DeliveryDate = DateTime.Now,
                    InvoiceBy = "",
                    InvoiceDate = DateTime.Now,
                    Requisitioner = "",
                    RequisitionDate = DateTime.Now,
                    LPName = ""
                };
            }
            db.Close();
            return PartialView("PopupAdditionalOrderHeader", DeliveryDetail);
        }

        public ActionResult PartialLogisticPartnerMaster()
        {
            List<LogisticPartner> listLP = new List<LogisticPartner>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<LogisticPartner>("GetAllLogisticPartner");
            db.Close();
            listLP = QueryLog.ToList<LogisticPartner>();

            ViewData["LogisticPartnerData"] = listLP;
            return PartialView("PartialLogisticPartnerGridPopupAdd", ViewData["LogisticPartnerData"]);
        }

        public ActionResult PartialSupplier(string DeliveryNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<DCLRouteDestination>("GetDCLRouteDestination", new object[] { DeliveryNo });
            db.Close();
            ViewData["RouteDestination"] = QueryLog.ToList<DCLRouteDestination>();
            return PartialView("PopUpRegulerDetailRoute", ViewData["RouteDestination"]);
        }

        public ActionResult PartialSupplierAdditional(string DeliveryNo)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<DCLRouteDestination>("GetDCLRouteDestination", new object[] { DeliveryNo });
            db.Close();
            ViewData["RouteDestination"] = QueryLog.ToList<DCLRouteDestination>();
            return PartialView("PopUpAdditionalDetailRoute", ViewData["RouteDestination"]);
        }

        public ActionResult PartialPickUpHeader(string DeliveryNo)
        {
            List<DCLPickUp> listPickUp = new List<DCLPickUp>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<DCLPickUp>("GetDCLManifestDetail", new object[] { DeliveryNo });
            db.Close();
            listPickUp = QueryLog.ToList<DCLPickUp>();

            ViewData["PickUp"] = listPickUp;
            ViewData["DeliveryNo"] = DeliveryNo;
            return PartialView("PopupRegulerOrderHeader2", ViewData["PickUp"]);
        }

        public ActionResult PartialPickUpHeaderAdditional(string DeliveryNo)
        {
            List<DCLPickUp> listPickUp = new List<DCLPickUp>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<DCLPickUp>("GetDCLManifestDetail", new object[] { DeliveryNo });
            db.Close();
            listPickUp = QueryLog.ToList<DCLPickUp>();
            ViewData["PickUp"] = listPickUp;
            ViewData["DeliveryNo"] = DeliveryNo;
            return PartialView("PopupAdditionalOrderHeader2", ViewData["PickUp"]);
        }

        public ActionResult PartialPickUp(string DeliveryNo)
        {
            List<DCLPickUp> listPickUp = new List<DCLPickUp>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<DCLPickUp>("GetDCLManifest", new object[] { DeliveryNo });
            db.Close();
            listPickUp = QueryLog.ToList<DCLPickUp>();
            ViewData["PickUp"] = listPickUp;
            return PartialView("PopUpRegulerPickUp", ViewData["PickUp"]);
        }

        public ActionResult PartialPickUpAdditonal(string DeliveryNo)
        {
            List<DCLPickUp> listPickUp = new List<DCLPickUp>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var QueryLog = db.Query<DCLPickUp>("GetDCLManifest", new object[] { DeliveryNo });
            db.Close();
            listPickUp = QueryLog.ToList<DCLPickUp>();

            ViewData["PickUp"] = listPickUp;
            ViewData["DeliveryNo"] = DeliveryNo;
            return PartialView("PopUpAdditionalPickUp", ViewData["PickUp"]);
        }

        public ActionResult PartialDCLInquiryRegional()
        {
            return PartialView("PartialDCLInquiryRegional", Model);
        }

        public ActionResult PartialDCLInquiryAdditional()
        {
            //DCLInquiryModel mdl = Model.GetModel<DCLInquiryModel>();
            //mdl.DCLInquiryAdditionals = DCLget("A", (Request.Params["LPCode"]),
            //    Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]),
            //    Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]),
            //    Request.Params["Route"], Request.Params["Rate"], Request.Params["DockCode"],
            //    Request.Params["DeliveryNo"],
            //    Request.Params["DeliveryStatus"] != null ? Request.Params["DeliveryStatus"] : string.Empty,
            //    Request.Params["DeliveryStat2"]);
            //Model.AddModel(mdl);

            //return PartialView("PartialDCLInquiryAdditional", Model);

            //List<DriverNameList> listDriver = GetAllDriverName();
            //ViewData["DriverNameListData"] = listDriver;

            //set param//
            p_DeliveryType = "A";
            p_LPCode = (Request.Params["LPCode"]);
            p_PickUpDateFrom = Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]);
            p_PickUpDateTo = Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]);
            p_Route = Request.Params["Route"];
            p_Rate = Request.Params["Rate"];
            p_DockCode = Request.Params["DockCode"];
            p_DeliveryNo = Request.Params["DeliveryNo"];
            p_DeliveryStatus = Request.Params["DeliveryStatus"] != null ? Request.Params["DeliveryStatus"] : string.Empty;
            p_DeliveryStat2 = Request.Params["DeliveryStat2"];

            DCLInquiryModel mdl = Model.GetModel<DCLInquiryModel>();

            mdl.GvModelAdditional = GridViewExtension.GetViewModel("grdDataAdditional");
            if (mdl.GvModelAdditional == null)
            {
                mdl.GvModelAdditional = CreateGridViewModel();
            }

            decorateModelBindingCustomAdditional(mdl.GvModelAdditional);
            Model.AddModel(mdl);

            return PartialView("PartialDCLInquiryAdditional", Model);
        }

        public ActionResult PartialDCLInquiryRegular()
        {
            //DCLInquiryModel mdl = Model.GetModel<DCLInquiryModel>();
            //mdl.DCLInquiryDbModel = DCLget("R", (Request.Params["LPCode"]),
            //    Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]),
            //    Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]),
            //    Request.Params["Route"], Request.Params["Rate"], Request.Params["DockCode"],
            //    Request.Params["DeliveryNo"],
            //    Request.Params["DeliveryStatus"] != null ? Request.Params["DeliveryStatus"] : string.Empty,
            //    Request.Params["DeliveryStat2"]);
            //Model.AddModel(mdl);
            //return PartialView("PartialDCLInquiryRegular", Model);

            //List<DriverNameList> listDriver = GetAllDriverName();
            //ViewData["DriverNameListData"] = listDriver;

            //set param//
            p_DeliveryType = "R";
            p_LPCode = (Request.Params["LPCode"]);
            p_PickUpDateFrom = Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]);
            p_PickUpDateTo = Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]);
            p_Route = Request.Params["Route"];
            p_Rate = Request.Params["Rate"];
            p_DockCode = Request.Params["DockCode"];
            p_DeliveryNo = Request.Params["DeliveryNo"];
            p_DeliveryStatus = Request.Params["DeliveryStatus"] != null ? Request.Params["DeliveryStatus"] : string.Empty;
            p_DeliveryStat2 = Request.Params["DeliveryStat2"];

            DCLInquiryModel mdl = Model.GetModel<DCLInquiryModel>();

            mdl.GvModel = GridViewExtension.GetViewModel("grdDataRegular");
            if (mdl.GvModel == null)
            {
                mdl.GvModel = CreateGridViewModel();
            }

            decorateModelBindingCustom(mdl.GvModel);
            Model.AddModel(mdl);

            return PartialView("PartialDCLInquiryRegular", Model);
        }

        //change//
        private GridViewModel CreateGridViewModel()
        {
            var viewModel = new GridViewModel();

            viewModel.Columns.Add("DELIVERY_NO");
            viewModel.Columns.Add("REVISE_NO");
            viewModel.Columns.Add("PICKUP_DT");
            viewModel.Columns.Add("ROUTE_RATE");
            viewModel.Columns.Add("LP_CD");
            viewModel.Columns.Add("LOG_PARTNER_NAME");
            viewModel.Columns.Add("CONFIRM_STS");
            viewModel.Columns.Add("DRIVER_NAME");
            viewModel.Columns.Add("DELIVERY_STS");
            viewModel.Columns.Add("DEPARTURE_STATUS");
            viewModel.Columns.Add("ARRIVAL_STATUS");
            viewModel.Columns.Add("REQUISITIONER_BY");
            viewModel.Columns.Add("REQUISITIONER_DT");
            viewModel.Columns.Add("DOWNLOAD_BY");
            viewModel.Columns.Add("DOWNLOAD_DT");
            viewModel.Columns.Add("CREATED_BY");
            viewModel.Columns.Add("CREATED_DT");
            viewModel.Columns.Add("INVOICE_BY");
            viewModel.Columns.Add("INVOICE_DT");
            viewModel.Columns.Add("DELIVERY_REASON");
            viewModel.Columns.Add("APPROVE_BY");
            viewModel.Columns.Add("APPROVE_DT");
            viewModel.Columns.Add("Notif");
            viewModel.Columns.Add("DriverManifest");
            viewModel.Columns.Add("VEHICLE_NO");
            //viewModel.Columns.Add("INPUT_CONTROL");
            viewModel.Columns.Add("MAP");

            viewModel.Pager.PageSize = 10;

            return viewModel;
        }

        private void setParameter()
        {
            p_LPCode = (Request.Params["LPCode"] == null ? "" : Request.Params["LPCode"]);
            p_PickUpDateFrom = Convert.ToDateTime(Request.Params["PickUpDateFrom"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateFrom"]);
            p_PickUpDateTo = Convert.ToDateTime(Request.Params["PickUpDateTo"] == "" ? "1900-01-01 00:00:00" : Request.Params["PickUpDateTo"]);
            p_Route = Request.Params["Route"] == null ? "" : Request.Params["Route"];
            p_Rate = Request.Params["Rate"] == null ? "" : Request.Params["Rate"];
            p_DockCode = Request.Params["DockCode"] == null ? "" : Request.Params["DockCode"];
            p_DeliveryNo = Request.Params["DeliveryNo"] == null ? "" : Request.Params["DeliveryNo"];
            p_DeliveryStatus = Request.Params["DeliveryStatus"] != null ? Request.Params["DeliveryStatus"] : string.Empty;
            p_DeliveryStat2 = Request.Params["DeliveryStat2"] == null ? "" : Request.Params["DeliveryStat2"];
        }

        private void decorateModelBindingCustomAdditional(GridViewModel gvmodelAdditional)
        {
            p_DeliveryType = "A";
            setParameter();

            gvmodelAdditional.ProcessCustomBinding(
                GetDataRowCountAdvanced,
                GetDataAdvanced
            );
        }


        //("R", listLP.Count == 1 ? listLP[0].LPCode : "", DateTime.Now.AddDays(-1), DateTime.Now, "", "", "", "", "", "");

        private void decorateModelBindingCustom_init(GridViewModel gvmodel)
        {
            p_DeliveryType = "R";

            p_LPCode = "";
            p_PickUpDateFrom = DateTime.Now.AddDays(-1);
            p_PickUpDateTo = DateTime.Now;
            p_Route = "";
            p_Rate = "";
            p_DockCode = "";
            p_DeliveryNo = "";
            p_DeliveryStatus = "";
            p_DeliveryStat2 = "";

            gvmodel.ProcessCustomBinding(
                GetDataRowCountAdvanced,
                GetDataAdvanced
            );
        }

        private void decorateModelBindingCustom(GridViewModel gvmodel)
        {
            p_DeliveryType = "R";
            setParameter();

            gvmodel.ProcessCustomBinding(
                GetDataRowCountAdvanced,
                GetDataAdvanced
            );
        }

        public void GetDataRowCountAdvanced(GridViewCustomBindingGetDataRowCountArgs e)
        {
            Dictionary<string, object> filterMap = retrieveFilterValue(e.State.FilterExpression);
            IDBContext db = DbContext;

            try
            {
                int count = db.SingleOrDefault<int>("SelectDCLInquiryHeaderCount", new object[] { 
                        p_DeliveryType, p_LPCode, p_PickUpDateFrom, p_PickUpDateTo, p_Route, p_Rate
                        , p_DockCode, p_DeliveryNo, p_DeliveryStatus, p_DeliveryStat2});

                e.DataRowCount = count;
            }
            finally
            {
                db.Close();
            }
        }

        public void GetDataAdvanced(GridViewCustomBindingGetDataArgs e)
        {
            Dictionary<string, object> filterMap = retrieveFilterValue(e.State.FilterExpression);
            List<DCLInquiryDb> dataRegularList = new List<DCLInquiryDb>();
            IDBContext db = DbContext;
            try
            {
                var dataRegular = db.Fetch<DCLInquiryDb>(
                     "SelectDCLInquiryHeader",
                     new object[] { 
                          p_DeliveryType, p_LPCode, p_PickUpDateFrom, p_PickUpDateTo, p_Route, p_Rate
                        , p_DockCode, p_DeliveryNo, p_DeliveryStatus, p_DeliveryStat2, 
                        e.StartDataRowIndex + 1, 
                        e.StartDataRowIndex + 1 + e.DataRowCount});

                dataRegularList = dataRegular.ToList();

                //if (dataRegular.Count() > 0)
                //{
                //    int count = 0;
                //    foreach (var q in dataRegular)
                //    {
                //        dataRegularList.Add(new DCLInquiryDb()
                //        {
                //            DELIVERY_NO = q.DELIVERY_NO,
                //            REVISE_NO = q.REVISE_NO,
                //            PICKUP_DT = q.PICKUP_DT,
                //            ROUTE_RATE = q.ROUTE_RATE,
                //            LP_CD = q.LP_CD,
                //            LOG_PARTNER_NAME = q.LOG_PARTNER_NAME,
                //            CONFIRM_STS = q.CONFIRM_STS != null ? q.CONFIRM_STS : string.Empty,
                //            DRIVER_NAME = q.DRIVER_NAME != null ? q.DRIVER_NAME : string.Empty,
                //            DELIVERY_STS = q.DELIVERY_STS,
                //            DEPARTURE_STATUS = q.DEPARTURE_STATUS,
                //            ARRIVAL_STATUS = Convert.ToString(q.ARRIVAL_STATUS),
                //            REQUISITIONER_BY = q.REQUISITIONER_BY,
                //            REQUISITIONER_DT = q.REQUISITIONER_DT,
                //            DOWNLOAD_BY = q.DOWNLOAD_BY,
                //            DOWNLOAD_DT = q.DOWNLOAD_DT,
                //            CREATED_BY = q.CREATED_BY,
                //            CREATED_DT = q.CREATED_DT,
                //            INVOICE_BY = q.INVOICE_BY,
                //            INVOICE_DT = q.INVOICE_DT,
                //            DELIVERY_REASON = q.DELIVERY_REASON,
                //            APPROVE_BY = q.APPROVE_BY,
                //            APPROVE_DT = q.APPROVE_DT,
                //            Notif = SetNoticeIconString(q.Notif),
                //            DriverManifest = SetDownloadIcon(count),
                //            VEHICLE_NO = q.VEHICLE_NO,
                //            MAP = "Show"
                //        });
                //        count++;
                //    }
                //}

                e.Data = dataRegularList;
            }
            finally
            {
                db.Close();
            }
        }

        public ActionResult GridCustomActionCore(GridViewModel gridViewModel)
        {

            DCLInquiryModel mdl = Model.GetModel<DCLInquiryModel>();

            mdl.GvModel = gridViewModel;

            Model.AddModel(mdl);

            decorateModelBindingCustom(gridViewModel);

            return PartialView("PartialDCLInquiryRegular", Model);
        }

        public ActionResult GridCustomActionCoreAdditional(GridViewModel gridViewModelAdditional)
        {

            DCLInquiryModel mdl = Model.GetModel<DCLInquiryModel>();

            mdl.GvModelAdditional = gridViewModelAdditional;

            Model.AddModel(mdl);

            decorateModelBindingCustomAdditional(gridViewModelAdditional);

            return PartialView("PartialDCLInquiryAdditional", Model);
        }

        public ActionResult PartialDCLInquiryAdditionalPagingAction(GridViewPagerState pager)
        {
            var viewModelAdditional = GridViewExtension.GetViewModel("grdDataAdditional");
            viewModelAdditional.Pager.Assign(pager);
            return GridCustomActionCoreAdditional(viewModelAdditional);
        }

        public ActionResult PartialDCLInquiryRegularPagingAction(GridViewPagerState pager)
        {
            var viewModel = GridViewExtension.GetViewModel("grdDataRegular");
            viewModel.Pager.Assign(pager);
            return GridCustomActionCore(viewModel);
        }

        protected Dictionary<string, object> retrieveFilterValue(string filterExpression)
        {
            OperandValue[] parameters;
            CriteriaOperator filterCriteria =
                DevExpress.Data.Filtering.Helpers.CriteriaParser.Parse(filterExpression, out parameters);

            Dictionary<string, object> filterMap = new Dictionary<string, object>();
            ParseCriteria(filterMap, filterCriteria);
            return filterMap;
        }

        protected void ParseCriteria(Dictionary<string, object> properties, CriteriaOperator criteria)
        {
            if (criteria is BinaryOperator)
            {
                AddPropertyName(properties,
                    (OperandProperty)((BinaryOperator)criteria).LeftOperand,
                    (OperandValue)((BinaryOperator)criteria).RightOperand);
            }
            else if (criteria is GroupOperator)
            {
                foreach (CriteriaOperator operand in ((GroupOperator)criteria).Operands)
                {
                    ParseCriteria(properties, operand);
                }
            }
        }

        protected void AddPropertyName(
            Dictionary<string, object> filterMap, OperandProperty leftProperty, OperandValue rightProperty)
        {
            filterMap.Add(leftProperty.PropertyName, rightProperty.Value);
        }
        //change//

        public ContentResult UpdateLPConfirmation(string DeliveryNo, string LPStat)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            int Result = db.Execute("UpdateLPConfirmation", new object[] { DeliveryNo, LPStat, UserID });
            db.Close();
            return Content(Result.ToString());
        }

        public ContentResult UpdateDCLInquiryHeader(string DeliveryNo, string LPCode, string UserLogin)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            string Result = db.ExecuteScalar<string>("UpdateDCLInquiry", new object[] { DeliveryNo, LPCode, AuthorizedUser.Username });
            db.Close();
            //int Result = db.Execute("UpdateDCLInquiry", new object[] { DeliveryNo, LPCode, AuthorizedUser.Username });
            return Content(Result.ToString());
        }

        public ContentResult UpdateDCLInquiryApproval(string DeliveryNos, string StatusDCL, string UserLogin)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            char[] splitchar = { ';' };
            string[] DeliveryNo = DeliveryNos.Split(splitchar);
            string Result = "";

            for (int i = 0; i < DeliveryNo.Length; i++)
            {
                Result += DeliveryNo[i] + ": ";
                Result += db.ExecuteScalar<string>("UpdateStatusDCLInquiry", new object[] { DeliveryNo[i], StatusDCL, AuthorizedUser.Username });
                Result += "\n";
            }
            db.Close();
            return Content(Result.ToString());
        }

        public ContentResult ManifestStatus(string DeliveryNo)
        {
            IDBContext dbManifest = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            string ResultManifest = dbManifest.ExecuteScalar<string>("GetDCLManifestStatus", new object[] { DeliveryNo });
            dbManifest.Close();
            return Content(ResultManifest.ToString());
        }

        private static string oldValueSupplier = string.Empty;
        private static string oldValueLogistic = string.Empty;
        private static string newValueLogistic = string.Empty; //add by  xsis.cece 8 feb 2021
        private string oldValueArrivalPlan = string.Empty;
        private string oldValueDeparturePlan = string.Empty;
        private string oldValueShippingDock = string.Empty;


        int i = 1;
        private void groupLogistic_ItemDataBound(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.TextBox textbox = sender as
                Telerik.Reporting.Processing.TextBox;


            string firstValue = (oldValueLogistic + oldValueSupplier).Replace(" ", string.Empty);
            string secondValue = (textbox.Text.Trim() + oldValueSupplier).Replace(" ", string.Empty);

            if (null != textbox)
            {
                if (0 == string.Compare(firstValue, secondValue, true))
                {
                    textbox.Value = string.Empty;
                }
                else
                {
                    oldValueLogistic = textbox.Text;
                }
            }
        }
        private void groupFieldDataTextBox_ItemDataBound(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.TextBox textbox = sender as
                Telerik.Reporting.Processing.TextBox;   
            //modify by xsis.cece 8 feb 2021
            string firstValue = (oldValueSupplier + newValueLogistic ).Replace(" ", string.Empty);
                newValueLogistic = oldValueLogistic;
            string secondValue = (textbox.Text.Trim() + newValueLogistic).Replace(" ", string.Empty);
            
            if (null != textbox)
            {
               if (!oldValueLogistic.Contains("807"))
               {
                    if (0 == string.Compare(firstValue, secondValue, true))
                    {
                        textbox.Value = string.Empty;
                        
                    }
                    else
                    {
                        oldValueSupplier = textbox.Text;
                    }
                }
            }
            i += 1;
        }
        //private void groupLogistic_ItemDataBound(object sender, System.EventArgs e)
        //{
        //    Telerik.Reporting.Processing.TextBox textbox = sender as
        //        Telerik.Reporting.Processing.TextBox;
        //  
        //
        //    string firstValue = (oldValueLogistic + oldValueSupplier).Replace(" ", string.Empty);
        //    string secondValue = (textbox.Text.Trim() + oldValueSupplier).Replace(" ", string.Empty);
        //
        //    if (null != textbox)
        //    {
        //        if (0 == string.Compare(firstValue, secondValue, true))
        //        {
        //            textbox.Value = string.Empty;
        //        }
        //        else
        //        {
        //            oldValueLogistic = textbox.Text;
        //        }
        //    }
        //}

        private void groupArrPlan_ItemDataBound(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.TextBox textbox = sender as
                Telerik.Reporting.Processing.TextBox;

            string firstValue = (oldValueLogistic + oldValueSupplier).Replace(" ", string.Empty);
            string secondValue = (textbox.Text.Trim() + oldValueSupplier).Replace(" ", string.Empty);

            if (null != textbox)
            {
                if (0 == string.Compare(firstValue, secondValue, true))
                {
                    textbox.Value = string.Empty;
                }
                else
                {
                    this.oldValueArrivalPlan = textbox.Text;
                }
            }
        }

        private void groupDepPlan_ItemDataBound(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.TextBox textbox = sender as
                Telerik.Reporting.Processing.TextBox;

            string firstValue = (oldValueLogistic + oldValueSupplier).Replace(" ", string.Empty);
            string secondValue = (textbox.Text.Trim() + oldValueSupplier).Replace(" ", string.Empty);

            if (null != textbox)
            {
                if (0 == string.Compare(firstValue, secondValue, true))
                {
                    textbox.Value = string.Empty;
                }
                else
                {
                    this.oldValueDeparturePlan = textbox.Text;
                }
            }
        }
        //Start// add column shiiping dock 20200302
        private void groupShippingDock_ItemDataBound(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.TextBox textbox = sender as
                Telerik.Reporting.Processing.TextBox;

            string firstValue = (oldValueLogistic + oldValueSupplier).Replace(" ", string.Empty);
            string secondValue = (textbox.Text.Trim() + oldValueSupplier).Replace(" ", string.Empty);

            if (null != textbox)
            {
                if (0 == string.Compare(firstValue, secondValue, true))
                {
                    textbox.Value = string.Empty;
                }
                else
                {
                    this.oldValueShippingDock = textbox.Text;
                }
            }
        }
        //end//
        public void ReportExports(string DeliveryNo, String DriverName, String VehicleNo) //Modif by xsis.cece request istd.zaenal (add Shipping dock) 24 Nov 2020
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            var Result = db.Execute("UpdateDriverDownloadDCL", new object[] { DriverName, AuthorizedUser.Username, AuthorizedUser.Username, DeliveryNo, VehicleNo });
            db.Close();
            Telerik.Reporting.Report rpt;
            Telerik.Reporting.Report SubReportDetail;
            Telerik.Reporting.Report SubHeader;

            TelerikReportLoader telerikReport = new TelerikReportLoader();

            rpt = telerikReport.Open("DCLTracking", DBContextNames.DB_PCS, "ReportGetDCLInquiry", new string[] { DeliveryNo });
            SubHeader = telerikReport.Open("DCLTrackingHeader", DBContextNames.DB_PCS, "ReportGetDCLInquiryHeader", new string[] { DeliveryNo });
            SubReportDetail = telerikReport.Open("DCLTrackingSubDetail", DBContextNames.DB_PCS, "ReportGetDCLInquirySubDetail", new string[] { DeliveryNo });

            rpt.Items["detail"].Items["txtLogisticPoint"].ItemDataBound += new EventHandler(groupLogistic_ItemDataBound);
            rpt.Items["detail"].Items["txtSupplierName"].ItemDataBound += new EventHandler(groupFieldDataTextBox_ItemDataBound);
           
           // rpt.Items["detail"].Items["txtShippingDock"].ItemDataBound += new EventHandler(groupShippingDock_ItemDataBound);//add column shiiping dock 20201124
            rpt.Items["detail"].Items["txtArrivalPlan"].ItemDataBound += new EventHandler(groupArrPlan_ItemDataBound);
            rpt.Items["detail"].Items["txtDeparturePlan"].ItemDataBound += new EventHandler(groupDepPlan_ItemDataBound);

            Telerik.Reporting.SubReport rptSub1 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport1", true)[0];
            rptSub1.ReportSource = SubReportDetail;

            Telerik.Reporting.SubReport rptSub2 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport2", true)[0];
            rptSub2.ReportSource = SubHeader;

            ReportProcessor reportProcessor = new ReportProcessor();

            rpt.ReportParameters.Add("DeliveryNo", Telerik.Reporting.ReportParameterType.String, DeliveryNo);

            RenderingResult result = reportProcessor.RenderReport("Pdf", rpt, null);

            string fileName = DeliveryNo.Trim() + "." + result.Extension;


            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;


            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
            rpt.Dispose();
            oldValueSupplier = string.Empty;
            oldValueLogistic = string.Empty;
            oldValueArrivalPlan = string.Empty;
            oldValueDeparturePlan = string.Empty;
        }

        public void DownloadExports(string PickupDateFrom, string PickupDateTo, string LPCode, string Route, string Rate,
            string DockCode, string DeliveryNo, string DeliveryStatus, string DeliveryAchievement, string DeliveryType)
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            //DateTime dtFrom = DateTime.ParseExact(PickupDateFrom, "dd.MM.yyyy", null);
            //DateTime dtTo = DateTime.ParseExact(PickupDateTo, "dd.MM.yyyy", null);

            string strFrom = DateTime.ParseExact(PickupDateFrom, "dd.MM.yyyy", null).ToString("yyyy-MM-dd");
            string strTo = DateTime.ParseExact(PickupDateTo, "dd.MM.yyyy", null).ToString("yyyy-MM-dd");

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;


            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(Server.MapPath("~") + @"\Report\DCLInquiry\Download_DCLInquiry.trdx", settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;
            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            sqlDataSource.SelectCommand = "SP_Rep_DCLExport";

            sqlDataSource.Parameters.Add("@DeliveryNo", System.Data.DbType.String, DeliveryNo);
            sqlDataSource.Parameters.Add("@LogPartner", System.Data.DbType.String, LPCode.Trim());
            sqlDataSource.Parameters.Add("@DockCode", System.Data.DbType.String, DockCode.Trim());
            sqlDataSource.Parameters.Add("@PickupFrom", System.Data.DbType.DateTime, strFrom);
            sqlDataSource.Parameters.Add("@PickupTo", System.Data.DbType.DateTime, strTo);
            sqlDataSource.Parameters.Add("@Route", System.Data.DbType.String, Route.Trim());
            sqlDataSource.Parameters.Add("@Rate", System.Data.DbType.String, Rate.Trim());
            sqlDataSource.Parameters.Add("@Status", System.Data.DbType.String, DeliveryStatus.Trim());
            sqlDataSource.Parameters.Add("@Achievement", System.Data.DbType.String, DeliveryAchievement.Trim());
            sqlDataSource.Parameters.Add("@DeliveryType", System.Data.DbType.String, DeliveryType);

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", rpt, null);
            string fileName = "DeliveryTracking" + "." + result.Extension;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
            rpt.Dispose();
        }

        #region Upload Driver Name List

        //#region UPLOAD CONFIRMATION - PROPERTIES
        //private string _ErrorMessage = "";

        //private String _uploadDirectory = "";
        //private String _UploadDirectory
        //{
        //    get
        //    {
        //        _uploadDirectory = Server.MapPath("~/Views/DCLInquiry/UploadTemp/");
        //        return _uploadDirectory;
        //    }
        //}

        //private String _filePath = "";
        //private String _FilePath
        //{
        //    get
        //    {
        //        return _filePath;
        //    }
        //    set
        //    {
        //        _filePath = value;
        //    }
        //}

        //private TableDestination _tableDestination = null;
        //private TableDestination _TableDestination
        //{
        //    get
        //    {
        //        if (_tableDestination == null)
        //            _tableDestination = new TableDestination();

        //        return _tableDestination;
        //    }
        //    set
        //    {
        //        _tableDestination = value;
        //    }
        //}

        //private long _processId = 0;
        //private long ProcessId
        //{
        //    get
        //    {
        //        if (_processId == 0)
        //        {
        //            IDBContext db = DbContext;

        //            try
        //            {
        //                _processId = db.Fetch<long>("GetLastProcessId", new object[] { })[0];
        //            }
        //            catch (Exception exc) { _processId = 0; }
        //            finally { db.Close(); }

        //            return _processId;
        //        }

        //        return _processId;
        //    }
        //}

        //private List<Portal.ExcelUpload.ErrorValidation> _errorValidationList = null;
        //private List<Portal.ExcelUpload.ErrorValidation> _ErrorValidationList
        //{
        //    get
        //    {
        //        if (_errorValidationList == null)
        //            _errorValidationList = new List<Portal.ExcelUpload.ErrorValidation>();

        //        return _errorValidationList;
        //    }
        //    set
        //    {
        //        _errorValidationList = value;
        //    }
        //}

        //private int errorCount = 0;
        //#endregion

        //[HttpPost]
        //public void UplDriverName_CallbackRouteValues()
        //{
        //    UploadControlExtension.GetUploadedFiles("UplDriverName", ValidationSettings, UplDriverName_FileUploadComplete);
        //}

        //protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        //{
        //    AllowedFileExtensions = new string[] { ".xls", ".xlsx" },
        //    MaxFileSize = 1000000000,
        //    MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
        //    ShowErrors = true
        //};

        //protected void UplDriverName_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        //{
        //    if (e.UploadedFile.IsValid)
        //    {
        //        _FilePath = _UploadDirectory + Convert.ToString(ProcessId) + e.UploadedFile.FileName;

        //        e.UploadedFile.SaveAs(_FilePath, true);
        //        e.CallbackData = _FilePath;
        //    }
        //}

        //[HttpPost]
        //public ActionResult HeaderDCLInquiry_Upload(string filePath)
        //{
        //    ProcessDriverUpload(filePath);

        //    String status = ((_ErrorMessage == "") ? "SUCCESS|Upload file success." : _ErrorMessage);

        //    return Json(
        //        new
        //        {
        //            Status = status
        //        });
        //}

        //private String GetColumnName()
        //{
        //    ArrayList columnNameArray = new ArrayList();

        //    IDBContext db = DbContext;
        //    try
        //    {
        //        // get temporary upload table column name list and create column name parameter
        //        List<ExcelReaderColumnName> columnNameList = db.Fetch<ExcelReaderColumnName>("GetColumnNames", new object[] { _TableDestination.tableFromTemp });
        //        foreach (ExcelReaderColumnName columnName in columnNameList)
        //        {
        //            columnNameArray.Add(columnName.ColumnName);
        //        }

        //        columnNameArray.RemoveAt(0); // remove id column name, cause it auto-generate column
        //    }
        //    catch (Exception exc)
        //    {
        //        throw new Exception("GetColumnName:" + exc.Message);
        //    }
        //    db.Close();

        //    return String.Join(",", columnNameArray.ToArray());
        //}

        private string formatingShortDate(string dateString)
        {
            string result = "";
            string[] extractString = new string[10];

            if (dateString != "")
            {
                extractString = dateString.Split('.');
                if (extractString.Length >= 3)
                    result = extractString[0] + "-" + extractString[1] + "-" + extractString[2];
            }

            return result;
        }

        //private DCLInquiryMainUploadDriver ProcessDriverUpload(string filePath)
        //{
        //    string created_by = AuthorizedUser.Username;
        //    DateTime created_dt = DateTime.Now;
        //    _ErrorMessage = "";

        //    DCLInquiryMainUploadDriver _UploadedDriver = new DCLInquiryMainUploadDriver();
        //    //IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            
        //    //---=== Table destination properties
        //    _TableDestination.filePath = filePath;
        //    _TableDestination.sheetName = "DriverInfo";
        //    _TableDestination.rowStart = "1";
        //    _TableDestination.columnStart = "1";
        //    _TableDestination.columnEnd = "7";

        //    LogProvider logProvider = new LogProvider("103", "3", AuthorizedUser);
        //    ILogSession sessionNTLMS = logProvider.CreateSession();
        //    OleDbConnection excelConn = null;
        //    IDBContext db = DbContext;

        //    try
        //    {
        //        //---=== Clear temporary upload table.
        //        DCLInquiryMainUploadDriver model = new DCLInquiryMainUploadDriver();
        //        model.MainList = model.MainList.ToList<DCLInquiryUploadDriver>();
        //        Model.AddModel(model);

        //        #region EXECUTE EXCEL AS TEMP_DB
        //        DataSet ds = new DataSet();
        //        OleDbCommand excelCommand = new OleDbCommand();
        //        OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
        //        string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";

        //        excelConn = new OleDbConnection(excelConnStr);
        //        excelConn.Open();
        //        DataSet dsExcel = new DataSet();
        //        DataTable dtPatterns = new DataTable();

        //        excelCommand = new OleDbCommand(@"select * from [" + _TableDestination.sheetName + "$]", excelConn);

        //        excelDataAdapter.SelectCommand = excelCommand;
        //        excelDataAdapter.Fill(dtPatterns);
        //        ds.Tables.Add(dtPatterns);

        //        string errorMessage = "";
        //        int lastLine = 0;

        //        string LPCode = Convert.ToString(ds.Tables[0].Rows[3][3]);
        //        string DeliveryMonth = Convert.ToString(ds.Tables[0].Rows[4][3]);

        //        for (int i = 7; i < dtPatterns.Rows.Count; i++)
        //        {

        //            errorMessage = "";

        //            if (Convert.ToString(ds.Tables[0].Rows[i][0]) == "")
        //                break;

        //            #region Validate Data

        //            //---=== Check Delivery No
        //            if (Convert.ToString(ds.Tables[0].Rows[i][1]) == "")
        //            {
        //                errorMessage = "Data " + (i + 1) + " : Delivery no can't be empty";
        //            }

        //            //---=== Check Pickup Date
        //            if (Convert.ToString(ds.Tables[0].Rows[i][2]) == "")
        //            {
        //                errorMessage = "Data " + (i + 2) + " : Pickup Date can't be empty";
        //            }
        //            else if (formatingShortDate(Convert.ToString(ds.Tables[0].Rows[i][2])) == "")
        //            {
        //                errorMessage = "Data " + (i + 2) + " : Pickup Date wrong date";
        //            }

        //            //---=== Check Route
        //            if (Convert.ToString(ds.Tables[0].Rows[i][3]) == "")
        //            {
        //                errorMessage = "Data " + (i + 3) + " : Route can't be empty";
        //            }

        //            //---=== Check Rate
        //            if (Convert.ToString(ds.Tables[0].Rows[i][4]) == "")
        //            {
        //                errorMessage = "Data " + (i + 4) + " : Rate can't be empty";
        //            }

        //            //---=== Check Vehicle No
        //            if (Convert.ToString(ds.Tables[0].Rows[i][5]) == "")
        //            {
        //                errorMessage = "Data " + (i + 5) + " : Vehicle no can't be empty";
        //            }

        //            //---=== Check Driver Name
        //            if (Convert.ToString(ds.Tables[0].Rows[i][6]) == "")
        //            {
        //                errorMessage = "Data " + (i + 6) + " : Driver name no can't be empty";
        //            }

        //            #endregion

        //            if (errorMessage != "")
        //            {
        //                errorCount = errorCount + 1;
        //                lastLine = i;
        //                if (_ErrorMessage == "")
        //                {
        //                    _ErrorMessage = "ERROR|" + errorMessage;
        //                }
        //            }

        //            #region Insert to Model Table
                    
        //            Model.GetModel<DCLInquiryMainUploadDriver>().MainList.Add(new DCLInquiryUploadDriver()
        //            {
        //                DeliveryNo = Convert.ToString(ds.Tables[0].Rows[i][1]).Trim().ToUpper(),
        //                PickupDate = formatingShortDate(Convert.ToString(ds.Tables[0].Rows[i][2])),
        //                Route = Convert.ToString(ds.Tables[0].Rows[i][3]).Trim().ToUpper(),
        //                Rate = Convert.ToString(ds.Tables[0].Rows[i][4]).Trim(),
        //                VehicleNo = Convert.ToString(ds.Tables[0].Rows[i][5]).Trim().ToUpper(),
        //                DriverName = Convert.ToString(ds.Tables[0].Rows[i][6]).Trim()
        //            });
        //            #endregion

        //        }

        //        #endregion

        //        #region Insert Into Table
        //        if (errorCount == 0)
        //        {
        //            foreach (var drv in model.MainList)
        //            {
        //                db.Execute("UpdateUploadDriverInfo", new object[] { 
        //                    drv.DeliveryNo, drv.PickupDate, drv.Route, drv.Rate, drv.VehicleNo, drv.DriverName, LPCode
        //                });    
        //            }
        //        }
        //        #endregion
        //    }
        //    catch (Exception exc)
        //    {
        //        _ErrorMessage = "ERROR|" + exc.ToString();
        //    }
        //    finally
        //    {
        //        excelConn.Close();

        //        if (System.IO.File.Exists(_TableDestination.filePath))
        //            System.IO.File.Delete(_TableDestination.filePath);
        //    }
        //    db.Close();
        //    return _UploadedDriver;
        //}

        public FileContentResult DCLInquiry_DownloadTemplate(string DeliveryNo, string DeliveryStatus, string DeliveryStatus2, string LP_CD, 
                                                         string PickupDateFrom, string PickupDateTo, string Route, string Rate)
        {
            //OleDbConnection excelConn = null;
            ////string filePath = _UploadDirectory + DateTime.Today.Ticks.ToString() + "_DCLDriverInfo.xls";
            //string filePath = _UploadDirectory + "DCLDriverInfo.xls";
            ////---=== Copy Template ===--//
            ////System.IO.File.Copy(_UploadDirectory + "DCLDriverInfo.xls", filePath);
            ////---=== End Copy Template ===--//
            //DCLInquiryModel mdl = Model.GetModel<DCLInquiryModel>();
            //mdl.DCLInquiryDbModel = DCLget("R", LP_CD,
            //    Convert.ToDateTime(PickupDateFrom == "" ? "1900-01-01 00:00:00" : PickupDateFrom),
            //    Convert.ToDateTime(PickupDateTo == "" ? "1900-01-01 00:00:00" : PickupDateTo),
            //    Route, Rate, Request.Params["DockCode"],
            //    DeliveryNo,
            //    DeliveryStatus != null ? DeliveryStatus : string.Empty,
            //    DeliveryStatus2);
            //Model.AddModel(mdl);

            ////---=== Open and modify excel ===---//
            
            ////---=== Table destination properties
            //_TableDestination.filePath = filePath;
            //_TableDestination.sheetName = "DriverInfo";
            //_TableDestination.rowStart = "1";
            //_TableDestination.columnStart = "1";
            //_TableDestination.columnEnd = "7";

            //try
            //{
            //    #region EXECUTE EXCEL AS TEMP_DB
            //    DataSet ds = new DataSet();
            //    OleDbCommand excelCommand = new OleDbCommand();
            //    OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
            //    string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=Yes;IMEX=1;\"";

            //    excelConn = new OleDbConnection(excelConnStr);
            //    excelConn.Open();
            //    DataSet dsExcel = new DataSet();
            //    DataTable dtPatterns = new DataTable();

            //    excelCommand = new OleDbCommand(@"select * from [" + _TableDestination.sheetName + "$]", excelConn);
                
            //    excelDataAdapter.SelectCommand = excelCommand;
            //    excelDataAdapter.Fill(dtPatterns);
            //    ds.Tables.Add(dtPatterns);

            //    //---=== Set LP Code ===---//
            //    ds.Tables[0].Rows[3][3] = LP_CD;
            //    //---=== End Set LP Code ===---//

            //    //int n = 1;
            //    //int nrow = 7;

            //    //foreach (var dcl in mdl.DCLInquiryDbModel)
            //    //{
            //    //    if (n > 8)
            //    //    {
            //    //        ds.Tables[0].NewRow();
            //    //    }

            //    //    ds.Tables[0].Rows[nrow][0] = n.ToString();
            //    //    ds.Tables[0].Rows[nrow][1] = dcl.DELIVERY_NO;
            //    //    ds.Tables[0].Rows[nrow][2] = dcl.PICKUP_DT.ToString("yyyy.MM.dd");
            //    //    ds.Tables[0].Rows[nrow][3] = dcl.ROUTE_RATE.Split('-')[0].Trim();
            //    //    ds.Tables[0].Rows[nrow][4] = dcl.ROUTE_RATE.Split('-')[1].Trim();
            //    //    ds.Tables[0].Rows[nrow][5] = dcl.VEHICLE_NO;
            //    //    ds.Tables[0].Rows[nrow][6] = dcl.DRIVER_NAME;

            //    //    n++;
            //    //    nrow++;
            //    //}
            //    #endregion

            //    //var recs = ds.Update(ds, "[" + _TableDestination.sheetName + "$]");
            //    //var recs = excelDataAdapter.Update(ds, "[" + _TableDestination.sheetName + "$]");



            //    using (OleDbDataAdapter da = new OleDbDataAdapter(@"select * from [Sheet1$]", excelConn))
            //    {
            //        using (da.InsertCommand = new OleDbCommand("INSERT INTO [Sheet1$] ( [D5] ) VALUES (?)",
            //            excelConn))
            //        {
            //    //        da.InsertCommand.Parameters.Add("D5", OleDbType.VarChar, 3, "[D5]");

            //    //        //List<int> testData = new List<int>();
            //    //        //for (int i = 1; i < 400000; i++)
            //    //        //{
            //    //        //    testData.Add(i);
            //    //        //}

            //    //        //DataSet dsTest = new DataSet();
            //    //        //dsTest.Tables.Add("[Sheet1$]");
            //    //        //dsTest.Tables[0].Columns.Add("[A1]");

            //            var recs = da.Update(ds, "[Sheet1$]");
            //        }
            //    }

            //}
            //catch (Exception ex)
            //{ }
            string filePath = Path.Combine(Server.MapPath("~/Views/DCLInquiry/UploadTemp/DCLDriverInfo.xls"));
            byte[] documentBytes = StreamFile(filePath);
            return File(documentBytes, "application/vnd.ms-excel", "DCLDriverInfo.xls");
            //---=== End Modify ===---//
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        #endregion

    }

    public class PopupModel
    {
        public string DeliveryNo { set; get; }
        public string DriverName { set; get; }
        public DateTime PickUpDate { set; get; }
    }

    public class LogisticPartnerTemp
    {
        public string Code { set; get; }
        public string Name { set; get; }
    }
}

