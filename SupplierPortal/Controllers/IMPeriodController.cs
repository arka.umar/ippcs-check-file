﻿using System;
using System.Data;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Reporting;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.MessageBoard;
using Portal.Models.IMPeriod;
using Portal.Models.Supplier;
using Portal.Models.Globals;

namespace Portal.Controllers
{
    public class IMPeriodController : BaseController
    {
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public IMPeriodController()
            : base("IM Period Maintenance Screen")
        {
        }
        protected override void StartUp()
        {
        }
        //
     
        //
        protected override void Init()
        {
            IMPeriod md2 = new IMPeriod();
            List<IMPeriod> lst = new List<IMPeriod>();

            IMPeriodModel model = new IMPeriodModel();

            Model.AddModel(model);
        }
//=================================================================
        //
        //private List<IMPeriod> ListModel
        //{
        //    get
        //    {
        //        if (_listmodel == null)
        //            _listmodel = new List<IMPeriod>();
        //        return _listmodel;
        //    }
        //    set
        //    {
        //        _listmodel = value;
        //    }
        //}
    
        //
        private List<IMPeriod> GetAllIMPeriod(string period, string year)
        {
            List<IMPeriod> IMPeriod = new List<IMPeriod>();
            IDBContext db = DbContext;

            IMPeriod = db.Fetch<IMPeriod>("GetAllIMPeriod", new object[] { period,year});
            
            db.Close();

            return IMPeriod;
        }
    

        public ActionResult GetIMPeriodGrid()
        {
           
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<IMPeriod> getdata = new List<IMPeriod>();
            getdata = db.Fetch<IMPeriod>("GetAllIMPeriod");
           // Model.AddModel(getdata);
            //di ubah
            IMPeriodModel model = Model.GetModel<IMPeriodModel>();
            model.IMPeriodd = GetAllIMPeriod ( Request.Params["period"],
                Request.Params["year"]);
            Model.AddModel(getdata);
            //IMPeriodModel model = Model.GetModel<IMPeriodModel>();
            //IMPeriodModel m = db .Fetch<IMPeriodModel>("",new object [] {} )
            //model.IMPeriodd = GetIMPeriodGrid(Request.Params["IMPERIOD"],

                                       //Request.Params["IMYEAR"],
                                       // Request.Params["DateSHIFT"],
                                       // Request.Params["TimeShift",
                                       // Request.Params["DateCorrection"],
                                       // Request.Params["TimeCorrection"],
                                      
                                      // Request.Params["RTEGRPCD"]);

           

            return PartialView("IMPeriodGrid", Model);

        }
        public void addnew(string IMPERIOD, string IMYEAR, string DateSHIFT, string TimeShift, string DateCorrection, string TimeCorrection)
        {
            
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            DateTime dateshift = Convert.ToDateTime(DateSHIFT + " " + TimeShift);
            DateTime dateCorrect = Convert.ToDateTime(DateCorrection + " " + TimeCorrection);
            
            int active_flag = 0;
           
                db.Execute("InsertIMPeriod", new object[] { 
                            IMPERIOD, IMYEAR, dateshift,dateCorrect,active_flag, AuthorizedUser.Username
                        });
            
            
        }
        

    }
}

  