﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Transactions;
using System.Data;

using Toyota.Common.Web.MVC;
using Portal.Models;

using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.Part;
using Portal.Models.Supplier;
using Portal.Models.SupplierInfo;

using Portal.Models;
using Portal.Models.Globals;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;
using System.Data.OleDb;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Log;
using DevExpress.Web.Mvc;
using System.Data.SqlClient;


namespace Portal.Controllers
{
    public class PartController : BaseController
    {
        public PartController()
            : base("Part Master Inquiry Screen")
        {
        }
        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

      

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            IDBContext db = DbContext;

            PartModel modelPartInfo = new PartModel(); 
           modelPartInfo.PartDatas = getAllSupplierPart("", "");

            Model.AddModel(modelPartInfo);

            ViewData["SupplierGridLookup"] = GetAllSupplier();
            ViewData["DockGridLookup"] = GetAllDock();
            ViewData["GridPart"] = GetAllPart("", "", "", "");
           
            //List<SupplierName> supplierModel = new List<SupplierName>();
            //supplierModel = db.Fetch<SupplierName>("GetSupplierCombobox", new object[] { });
            //ViewData["GridSupplier"] = supplierModel;
            //DbContext.Close();

        }
        //protected override void Init()
        //{
        //    PartModel model = new PartModel();
        //    for (int i = 0; i < 21; i++)
        //    {
        //        model.PartDatas.Add(new PartData()
        //            {
        //                Check = false,
        //                PartNo = "PC" + (i + 1),
        //                PartName = "Part" + (i + 1),
        //                UniqNo = "1456A" + (i + 1),
        //                SuppCode = "SC" + (i + 1),
        //                SuppName = "Supplier" + (i + 1),
        //                SuppPlant = "1",
        //                DockCode = "DC1",
        //                Packaging = " ",
        //                CarFamCode = "700A",
        //                ModelSeries = " ",
        //                PickTime = " ",
        //                Kanban = " ",
        //                SafeStock = " ",
        //                MaxStock = " ",
        //                SafeTime = " ",
        //                InterKanban = " ",
        //                SuppShip = " ",
        //                RecCompCode = "807B",
        //                RecPlantCode = "2",
        //                RecDockCode = "1H",
        //                InHouseRoute = " ",
        //                TimeFrom = DateTime.Now,
        //                TimeTo = DateTime.Now,
        //                LifeCycleCode = "0",
        //                TeamMember = "Supplier Packing",
        //                SeriesName = "376W",
        //                OrderMethod = "B",
        //                BcRefFlag = "0",
        //                KanbanNo = "1634",
        //                KanbanQty = 20,
        //                VendShareFlag = " ",
        //                VendShareValue = " ",
        //                PCLeadTime = "60",
        //                PCSafeTime = "60",
        //                LnLeadTime = "60",
        //                LnSafeTime = "60",
        //                SafePcs = "60",
        //                PckType = "Poly Box",
        //                PartsWeight = "3660",
        //                PackPerCont = "20"
        //            }
        //            );
        //    }
        //    Model.AddModel(model);
        //}

       public ActionResult PartialHeaderSupplierGridLookupPart()
        {
            #region Required model in partial page : for EOCHDockGridLookup
            TempData["GridName"] = "OIHSupplierOption";
            ViewData["SupplierGridLookup"] = GetAllSupplier();
            #endregion

            return PartialView("GridLookup/PartialGrid", ViewData["SupplierGridLookup"]);
        }

       public ActionResult PartialSupplierGridLookupPart()
       {
           #region Required model in partial page : for EOCHDockGridLookup
           TempData["GridName"] = "grlSuppCd";
           ViewData["SupplierGridLookup"] = GetAllSupplier();
           #endregion

           return PartialView("GridLookup/PartialGrid", ViewData["SupplierGridLookup"]);
       }

       private List<PartData> GetAllPart(string SupplierCode, string SupplierPlant, string DockCode, string KanbanNo)
       {
           List<PartData> partModelList = new List<PartData>();

           try
           {
               //partModelList = DbContext.Fetch<Part>("GetAllPartNoGrid", new object[] { SupplierCode, SupplierPlant, DockCode });

               partModelList = DbContext.Fetch<PartData>("GetSupplierPartCombobox", new object[] { SupplierCode, SupplierPlant, DockCode, KanbanNo });
           }
           catch (Exception exc) { throw exc; }
           finally { DbContext.Close(); }

           return partModelList;
       }

       private List<Supplier> GetAllSupplier()
       {

           IDBContext db = DbContext;

           List<Supplier> supplierModelList = new List<Supplier>();


           try
           {
               supplierModelList = DbContext.Fetch<Supplier>("GetAllSupplierGrid", new object[] { "" });
           }
           catch (Exception exc) { throw exc; }
           db.Close();

           return supplierModelList;
       }

       public ActionResult PartialHeaderDockGridLookupEmergencyOrderCreation()
       {
           #region Required model in partial page : for EOCHDockGridLookup
           TempData["GridName"] = "PartHDockGridLookup";
           ViewData["DockGridLookup"] = GetAllDock();
           #endregion

           return PartialView("GridLookup/PartialGrid", ViewData["DockGridLookup"]);
       }
       private List<Dock> GetAllDock()
       {
           List<Dock> dockModelList = new List<Dock>();

           try
           {
               dockModelList = DbContext.Fetch<Dock>("GetAllDock", new object[] { "" });
           }
           catch (Exception exc) { throw exc; }
           finally { DbContext.Close(); }

           return dockModelList;
       }

       protected List<PartData> getAllSupplierPart(string suppCdParam, string partNoParam)
       {
           IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
           List<PartData> listTooltipDatas = new List<PartData>();
           string vSupCode = "";
           string vSupPlantCd = "";
           if (suppCdParam != "")
           {
               string result = string.Empty;
               char[] splitStr = { '-' };
               string[] supps = suppCdParam.Split(splitStr);
               vSupCode = supps[0].Trim();
               vSupPlantCd = supps[1].Trim();
           }

            listTooltipDatas = db.Query<PartData>("GetAllSupplierPart", new object[] { suppCdParam, partNoParam }).ToList();
           db.Close();
          
           return listTooltipDatas;

       }
       public ActionResult PartialSupplierDatas()
       {
           string SuppCd = String.IsNullOrEmpty(Request.Params["SupplierCode1"]) ? "" : Request.Params["SupplierCode1"];
           string PartNo = String.IsNullOrEmpty(Request.Params["PartNo1"]) ? "" : Request.Params["PartNo1"];

           PartModel model = Model.GetModel<PartModel>();
           model.PartDatas = getAllSupplierPart(SuppCd, PartNo.Replace("-", ""));

           Model.AddModel(model);

           return PartialView("PartialPartData", Model);
       }

       public ActionResult PartialHeaderPartLookupGridPart()
       {
           IDBContext db = DbContext;

           TempData["GridName"] = "OIHPartOption";
           List<PartData> partModel = new List<PartData>();
           partModel = db.Fetch<PartData>("GetSupplierPartCombobox", new object[] { });
           ViewData["GridPart"] = partModel;

           db.Close();

           return PartialView("GridLookup/PartialGrid", ViewData["GridPart"]);
       }



       public ActionResult PartialHeader()
       {
           #region Required model in partial page : for SupplierInfoHeaderPartial
           ViewData["GridSupplier"] = GetAllSupplier();
         ViewData["GridPart"] = GetAllPart("", "", "", "");
           #endregion

           return PartialView("PartialPartHeader");
       }


       public ActionResult AddNewPart(string Supplier, string SupplierShipping, string RecCompCd, string RecPlantCd, string RecDockCd 
           , string InhouseRouting, string PartNo, string TimeFrom, string TimeTo, string PartName, string LifeCycleCd, string TeamMember 
           , string SeriesName, string OrderMethod, string BcRefFlag,string KanBanNo, int KanbanQty, string VendoShareFlag, string VendorShareValue
           , int PcLeadTime, int PcSafetyTime, int LineLeadTime,int LineSafetyTime, int SafetyPcs, string PackType, int PartWeight, int PackPerContainer 
           ,int AudioFlag,string User)
       {
           string ResultQuery = "Process succesfully";
           string[] Suppliers = Supplier.Split('-');
           string SupplierCode = Suppliers[0].Replace(" ", "");
           string SupplierPlant = Suppliers[1].Replace(" ", "");
           DateTime vTo = DateTime.ParseExact( TimeTo, "dd/MM/yyyy", null);
           DateTime vFrom = DateTime.ParseExact((TimeFrom), "dd/MM/yyyy", null);

           if (SupplierShipping == "") { SupplierShipping = "   "; }
           if (RecCompCd == "") { RecCompCd = "    "; }
           if (RecPlantCd == "") { RecPlantCd = " "; }
           if (RecDockCd == "") { RecDockCd = "  "; }
           if (InhouseRouting == "") { InhouseRouting = "      "; }
           User = AuthorizedUser.Username;
           IDBContext db = DbContext;
           try
           {
               db.Execute("InsertPart", new object[] 
                {
                   SupplierCode,
                   SupplierPlant,
                   SupplierShipping,
                   RecCompCd,
                   RecPlantCd,
                   RecDockCd,
                   InhouseRouting,
                   PartNo,
                  vFrom,
                   vTo,
                   PartName,
                   LifeCycleCd,
                   TeamMember,
                   SeriesName,
                   OrderMethod,
                   BcRefFlag,
                   KanBanNo,
                   KanbanQty,
                   VendoShareFlag,
                   VendorShareValue,
                   PcLeadTime,
                   PcSafetyTime,
                   LineLeadTime,
                   LineSafetyTime,
                   SafetyPcs,
                   PackType,
                   PartWeight,
                   PackPerContainer,
                   AudioFlag,
                   User,
                });
               TempData["GridName"] = "PartGridView";
               db.Close();
           }
           catch (Exception ex)
           {
               ResultQuery = "Error: " + ex.Message;
           }
           return Content(ResultQuery);
       }
         


    }
}
