﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.DetailForecast;

namespace Portal.Controllers

{
    public class DetailForecastController : BaseController
    {
        public DetailForecastController(): base("Detail Forecast Inquiry Screen")
        {

        }
        protected override void StartUp()
        {

        }
        protected override void Init()
        {
            DetailForecastModel mdl = new DetailForecastModel();
            for (int i = 1; i < 50; i++)
            {
                mdl.Details.Add(new DetailForecast()
                {
                    Part_No = "22450K0233-" + i,
                    Part_Name = "Part Name. " + 1,
                    RCV_Area = i + "M",
                    Kanban_Size = Convert.ToString(9 * i),
                    SFX = "B2",
                    N_QTY = "1020",
                    N_Plus_One_QTY = "1120",
                    N_Plus_Two_QTY = "1200",
                    N_Plus_Three_QTY = "1020",
                    Remark = " ",
                    Message = "Create new"
                });
                Model.AddModel(mdl);
            }
        }
        public ActionResult DetailForecastGrid()
        {
            return PartialView("DetailForecastDataGrid", Model);
        }
    }
}