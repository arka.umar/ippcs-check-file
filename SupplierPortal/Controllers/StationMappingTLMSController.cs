﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models;
using System.Data;
using Toyota.Common.Web.Messaging;
using Toyota.Common.Web.Display;
using Toyota.Common.Web.Menu;
using Portal.Models.PO;

namespace Portal.Controllers
{
    public class StationMappingTLMSController : BaseController
    {
        public StationMappingTLMSController()
            : base("Station Mapping TLMS")
        {
        
        }

        public ActionResult HeaderStationMappingTLMS()
        {
            return PartialView("HeaderStationMappingTLMS");
        }

        public ActionResult GridStationMappingTLMS()
        {
            return PartialView("GridStationMappingTLMS");
        }

        public ActionResult HeaderButtonStationMappingTLMS()
        {
            return PartialView("HeaderButtonStationMappingTLMS");
        }

        public ActionResult HaderLeftStationTLMS()
        {
            return PartialView("HaderLeftStationTLMS");
        }
        
        protected override void StartUp()
        {
        }

        protected override void Init()
        {
        }
    }
}
