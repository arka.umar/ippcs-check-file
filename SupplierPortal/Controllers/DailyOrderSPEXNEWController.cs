﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.FTP;
using Toyota.Common.Web.Credential;
using Portal.Models.DailyOrder;
using Portal.Models.Supplier;
using Portal.Models.Dock;

namespace Portal.Controllers
{
    public class DailyOrderSPEXNEWController : BaseController
    {
        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileContentResult)
            {
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            }
            else
            {
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);
            base.OnActionExecuted(filterContext);
        }

        private IDBContext DbContext
        {
            get
            {
                return DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            }
        }

        public DailyOrderSPEXNEWController()
             : base("SPEX Order Inquiry")
         {
             PageLayout.UseMessageBoard = true;
             CookieName = "fileDownload";
             CookiePath = "/";
         }

        #region Model properties
        List<SupplierName> _orderInquirySupplierModel = null;
        private List<SupplierName> _OrderInquirySupplierModel
        {
            get
            {
                if (_orderInquirySupplierModel == null)
                    _orderInquirySupplierModel = new List<SupplierName>();

                return _orderInquirySupplierModel;
            }
            set
            {
                _orderInquirySupplierModel = value;
            }
        }

        List<SupplierNameSPEX> _orderInquirySupplierSPEXModel = null;
        private List<SupplierNameSPEX> _OrderInquirySupplierSPEXModel
        {
            get
            {
                if (_orderInquirySupplierSPEXModel == null)
                    _orderInquirySupplierSPEXModel = new List<SupplierNameSPEX>();

                return _orderInquirySupplierSPEXModel;
            }
            set
            {
                _orderInquirySupplierSPEXModel = value;
            }
        }

        List<DockData> _orderInquiryDockModel = null;
        private List<DockData> _OrderInquiryDockModel
        {
            get
            {
                if (_orderInquiryDockModel == null)
                    _orderInquiryDockModel = new List<DockData>();

                return _orderInquiryDockModel;
            }
            set
            {
                _orderInquiryDockModel = value;
            }
        }
        #endregion

        protected override void StartUp()
        {
        }

        protected override void Init()
        {
            PageLayout.UseSlidingBottomPane = true;

            DailyOrderModel mdl = new DailyOrderModel();
            Model.AddModel(mdl);

            ViewData["DateFrom"] = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
            ViewData["DateTo"] = DateTime.Now.ToString("dd.MM.yyyy");
        }

        #region Order Inquiry controller
        #region View controller
        public ActionResult PartialHeaderOrderInquiry()
        {
            Session["paramManifestNo"] = String.IsNullOrEmpty(Request.Params["ManifestNos"]) ? Session["paramManifestNo"] : Request.Params["ManifestNos"];

            #region Required model in partial page : for OrderInquiryHeaderPartial
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSupplierOption");
            _OrderInquirySupplierModel = suppliers;

            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "DailyOrderSPEXNEW", "OIHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderSPEXNEW", "OIHDockOption");
            _OrderInquiryDockModel = DockCodes;

            Model.AddModel(_OrderInquirySupplierModel);
            Model.AddModel(_OrderInquirySupplierSPEXModel);
            Model.AddModel(_OrderInquiryDockModel);
            #endregion

            return PartialView("OrderInquiryHeaderPartial", Model);
        }

        public ActionResult PartialHeaderDockLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHDockOption GridLookup
            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderSPEXNEW", "OIHDockOption");
            TempData["GridName"] = "OIHDockOption";
            _OrderInquiryDockModel = DockCodes;
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquiryDockModel);
        }

        public ActionResult PartialHeaderSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSupplierOption");

            TempData["GridName"] = "OIHSupplierOption";
            _OrderInquirySupplierModel = suppliers;
            #endregion

            return PartialView("GridLookup/PartialGrid", _OrderInquirySupplierModel);
        }

        public ActionResult PartialHeaderSubSupplierLookupGridOrderInquiry()
        {
            #region Required model in partial page : for OIHSupplierOption GridLookup
            List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "DailyOrderSPEXNEW", "OIHSubSupplierOption");
            List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSubSupplierOption");
            subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE_PLANT, y.SUB_SUPPLIER_NAME })
                                .Select(x => new SupplierNameSPEX()
                                {
                                    SUB_SUPPLIER_CODE_PLANT = x.Key.SUB_SUPPLIER_CODE_PLANT,
                                    SUB_SUPPLIER_NAME = x.Key.SUB_SUPPLIER_NAME
                                }).ToList();

            TempData["GridName"] = "OIHSubSupplierOption";
            _orderInquirySupplierSPEXModel = distinctSubSuppCD;
            #endregion

            return PartialView("GridLookup/PartialGrid", _orderInquirySupplierSPEXModel);
        }

        public ActionResult PartialDetailOrderInquiryServicePartExport()
        {
            DailyOrderModel model = Model.GetModel<DailyOrderModel>();
            try
            {
                #region Required model in partial page : for OrderInquiryEmergencyOrderDetailPartial
                model.ServicePartExportSubSupp = GetAllServicePartExport(Request.Params["SupplierCode"],
                                                    Request.Params["DockCode"],
                                                    Request.Params["ManifestNo"],
                                                    Request.Params["DateFrom"],
                                                    Request.Params["DateTo"],
                                                    "3",
                                                    Session["paramManifestNo"] == null ? "" : Session["paramManifestNo"].ToString(),
                                                    Request.Params["PartName"],
                                                    Request.Params["PartStatus"],
                                                    Request.Params["SubSupplierCode"]);
                #endregion
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return PartialView("OrderInquiryServicePartExportDetailPartial", Model);
        }

        public ActionResult PopupHeaderDailyDataPartSubDetail()
        {
            string manifest = Request.Params["ManifestNo"];
            return PartialView("PartialHeaderDailyDataSubDetail", ViewData["ManifestNo"]);
        }

        public ActionResult PopupDailyDataPartSubDetail()
        {
            string manifestNo = Request.Params["ManifestNo"];
            string problemFlag = Request.Params["ProblemFlag"];
            ViewData["partDailyData"] = DailyDataPartOrderFillGrid(manifestNo, problemFlag);
            return PartialView("DailyDataSubDetail", ViewData["partDailyData"]);
        }
        #endregion

        #region Common controller
        private String GetGeneralProductionMonth(String productionMonth)
        {
            Dictionary<String, String> monthDictionary = new Dictionary<String, String>();
            monthDictionary.Add("01", "January");
            monthDictionary.Add("02", "February");
            monthDictionary.Add("03", "March");
            monthDictionary.Add("04", "April");
            monthDictionary.Add("05", "May");
            monthDictionary.Add("06", "June");
            monthDictionary.Add("07", "July");
            monthDictionary.Add("08", "August");
            monthDictionary.Add("09", "September");
            monthDictionary.Add("10", "October");
            monthDictionary.Add("11", "November");
            monthDictionary.Add("12", "December");

            String generalProductionMonth = "";

            if (!String.IsNullOrEmpty(productionMonth))
            {
                String year = productionMonth.Substring(0, 4);
                String month = productionMonth.Substring(productionMonth.Length - 2, 2);

                generalProductionMonth = monthDictionary[month] + " " + year;
            }

            return generalProductionMonth;
        }
        #endregion

        #region Database controller
        private List<SupplierName> GetAllSupplier()
        {
            List<SupplierName> orderInquirySupplierModel = new List<SupplierName>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierName>("GetAllSupplierSPEXNEW", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<SupplierNameSPEX> GetAllSubSupplier()
        {
            List<SupplierNameSPEX> orderInquirySupplierModel = new List<SupplierNameSPEX>();
            IDBContext db = DbContext;
            orderInquirySupplierModel = db.Fetch<SupplierNameSPEX>("GetAllSubSupplierSPEXNEW", new object[] { });
            db.Close();

            return orderInquirySupplierModel;
        }

        private List<DockData> GetAllDock()
        {
            List<DockData> orderInquiryDockModel = new List<DockData>();
            IDBContext db = DbContext;
            orderInquiryDockModel = db.Fetch<DockData>("GetAllDock", new object[] { });
            db.Close();

            return orderInquiryDockModel;
        }

        private List<NonDailyOrderSPEXSubSupp> GetAllServicePartExport(String supplierCode, String dockCode, String manifestNo, String dateFrom, String dateTo, string OrderType, string manifestNos, string PartName, string PartStatus, string SubSupplierCode)
        {
            IDBContext db = DbContext;
            string SupplierCodeLDAP = "";
            string SubSupplierCodeLDAP = "";
            string DockCodeLDAP = "";

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            //List<SupplierNameSPEX> subsuppliers = new List<SupplierNameSPEX>();
            //List<SupplierNameSPEX> subsuppliersparent = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_MAIN", "DailyOrderSPEXNEW", "OIHSubSupplierOption");
            //List<SupplierNameSPEX> subsupplierschild = Model.GetModel<User>().FilteringArea<SupplierNameSPEX>(GetAllSubSupplier(), "SUB_SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSubSupplierOption");
            //subsuppliers = subsuppliersparent.Concat(subsupplierschild).ToList();

            //List<SupplierNameSPEX> distinctSubSuppCD = subsuppliers.GroupBy(y => new { y.SUB_SUPPLIER_CODE })
            //                    .Select(x => new SupplierNameSPEX()
            //                    {
            //                        SUB_SUPPLIER_CODE = x.Key.SUB_SUPPLIER_CODE
            //                    }).ToList();

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderSPEXNEW", "OIHDockOption");

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            //foreach (SupplierNameSPEX s in distinctSubSuppCD)
            //{
            //    SubSupplierCodeLDAP += s.SUB_SUPPLIER_CODE + ",";
            //}

            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            List<NonDailyOrderSPEXSubSupp> dailyOrderModel = db.Fetch<NonDailyOrderSPEXSubSupp>("GetAllServicePartExportSubSuppNEW", new object[] {
                supplierCode == null ? "" : supplierCode,
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType,
                manifestNos,
                PartName == "-" ? "" : PartName,
                PartStatus == null ? "" : PartStatus,
                SubSupplierCode,
                SupplierCodeLDAP,
                DockCodeLDAP,
                SubSupplierCodeLDAP
            });
            db.Close();

            return dailyOrderModel;
        }

        protected List<PartDetailManifest> DailyDataPartOrderFillGrid(string manifestNo, string problemFlag)
        {
            List<PartDetailManifest> getPartDetailManifest = new List<PartDetailManifest>();
            IDBContext db = DbContext;
            getPartDetailManifest = db.Query<PartDetailManifest>("GetPartDetailManifestNEW", new object[] { manifestNo, problemFlag }).ToList<PartDetailManifest>();

            db.Close();
            return getPartDetailManifest;
        }

        private void UpdateDownloadFlagNonRegular(string ManifestNo)
        {
            IDBContext db = DbContext;

            db.Execute("UpdateDownloadFlagOrderNonRegularNEW", new Object[] { ManifestNo, AuthorizedUser.Username });
            db.Close();
        }
        #endregion
        #endregion

        #region DownloadPDF
        public FileContentResult DownloadPDF(string ManifestNo, string OrderType)
        {
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
            Stream output = new MemoryStream();
            byte[] documentBytes;
            string filePath = "";

            try
            {
                FTPUpload vFtp = new FTPUpload();
                filePath = vFtp.Setting.FtpPath("ServicePartExport");
                string msg = "";

                string serverPath = String.Format("ftp://{0}/{1}", vFtp.Setting.IP, filePath);
                documentBytes = vFtp.FtpDownloadByte(String.Format("{0}/{1}", serverPath, ManifestNo + ".zip"),
                                                       ref msg);

                UpdateDownloadFlagNonRegular(ManifestNo);

                return File(documentBytes, "application/zip", ManifestNo + ".zip");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Download Raw TXT
        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] ImageData = new byte[fs.Length];
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return ImageData;
        }

        public FileContentResult PrintDailyOrderTXTSPEX(string ManifestNo, string OrderDate, string SupplierCode, string OrderType)
        {
            ICompression compress = ZipCompress.GetInstance();
            Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();

            Stream output = new MemoryStream();
            string fileName = "Order_" + OrderDate + ".txt";
            string outputName = "";

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "dd.MM.yyyy hh:mm", null);

            if (OrderType == "3") OrderType = "C";
            else OrderType = "XXX";

            IDBContext db = DbContext;

            string filePath = Path.Combine(Server.MapPath("~/Views/DailyOrderSPEXNEW/DownloadTemp/"), Supplier[0] + Supplier[1] + ".txt");
            string filePathZip = Path.Combine(Server.MapPath("~/Views/DailyOrderSPEXNEW/DownloadTemp/"), Supplier[0] + Supplier[1] + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                try
                {
                    if (OrderType != "C")
                    {
                        throw new Exception("Wrong Data Selected, Please Refresh The Page Before Continue");
                    }

                    List<SPEXDailyOrderRAWData> rows = db.Fetch<SPEXDailyOrderRAWData>("GetTXTRawSPEX", new object[] { ManifestNo, OrdDate.ToString("yyyyMMdd"), Supplier[0], Supplier[1] });

                    string tab = "";
                    StringBuilder strHeader = new StringBuilder();
                    StringBuilder str = new StringBuilder();

                    int orderQty = 0;
                    string[] columns;
                    foreach (SPEXDailyOrderRAWData r in rows)
                    {
                        tab = "";
                        columns = r.DATA.Split('|');
                        for (int j = 0; j < columns.Length; j++)
                        {
                            str.Append(tab + (columns[j] == null ? "" : columns[j].ToString().TrimEnd()));

                            if (r.LVL > 1)
                            {
                                tab = "\t";
                            }
                            else
                            {
                                tab = "|";
                            }
                        }

                        str.AppendLine();
                    }
                    writer.Write(str.ToString());
                    writer.Dispose();

                    outputName = Supplier[0] + Supplier[1];
                    outputName = db.ExecuteScalar<string>("Get_SPEX_SubSuppbySuppNEW", new object[] { ManifestNo, OrdDate.ToString("yyyyMMdd"), Supplier[0], Supplier[1] });

                    byte[] documentBytes = StreamFile(filePath);
                    listCompress.Add(outputName + ".txt", documentBytes);

                    compress.Compress(listCompress, output);
                    documentBytes = new byte[output.Length];
                    output.Position = 0;
                    output.Read(documentBytes, 0, documentBytes.Length);

                    UpdateDownloadFlagNonRegular(ManifestNo);
                    return File(documentBytes, "application/zip", outputName + OrderType + OrdDate.ToString("yyyyMMddhhmm") + ".zip");
                }
                catch (Exception ex)
                {
                    writer.Dispose();
                    throw ex;
                }
            }
        }
        #endregion

        #region Download Pra
        public void DownloadList(String supplierCode, String dockCode, String manifestNo, String dateFrom, String dateTo, string OrderType, string manifestNos, string PartName, string PartStatus, string SubSupplierCode)
        {

            IDBContext db = DbContext;
            string SupplierCodeLDAP = "";
            string SubSupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            string fileName = "Order.xls";

            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderSPEXNEW", "OIHDockOption");

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = ""; 

            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            List<ReportPraSPEXDailyOrderNew> dailyOrderModel = db.Fetch<ReportPraSPEXDailyOrderNew>("ReportPraSPEXDailyOrder", new object[] {
                supplierCode == null ? "" : supplierCode,
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType  == null ? "" : OrderType,
                manifestNos == null ? "" : manifestNos,
                PartName == "-" ? "" : PartName,
                PartStatus == null ? "" : PartStatus,
                SubSupplierCode == null ? "" : SubSupplierCode, 
                SupplierCodeLDAP,
                DockCodeLDAP,
                SubSupplierCodeLDAP
            });
            hasil = exporter.Write(dailyOrderModel, "SPEX"); 
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            fileName = "Order_" + date + ".xls";

            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion


        #region Download Pra Detail
        public void DownloadListDetail(String supplierCode, String dockCode, String manifestNo, String dateFrom, String dateTo, string OrderType, string manifestNos, string PartName, string PartStatus, string SubSupplierCode)
        {

            IDBContext db = DbContext;
            string SupplierCodeLDAP = "";
            string SubSupplierCodeLDAP = "";
            string DockCodeLDAP = "";
            string fileName = "Order.xls";

            IExcelWriter exporter = new NPOIWriter();
            byte[] hasil;

            List<SupplierName> suppliers = Model.GetModel<User>().FilteringArea<SupplierName>(GetAllSupplier(), "SUPPLIER_CODE", "DailyOrderSPEXNEW", "OIHSupplierOption");
            List<string> suppCode = suppliers.Select(x => x.SUPPLIER_CODE).Distinct().ToList<string>();

            List<DockData> DockCodes = Model.GetModel<User>().FilteringArea<DockData>(GetAllDock(), "DockCode", "DailyOrderSPEXNEW", "OIHDockOption");

            if (suppCode.Count == 1) SupplierCodeLDAP = suppliers[0].SUPPLIER_CODE;
            else SupplierCodeLDAP = "";

            foreach (DockData d in DockCodes)
            {
                DockCodeLDAP += d.DockCode + ",";
            }

            List<ReportPraSPEXDailyOrderNewDetail> dailyOrderModelDetail = db.Fetch<ReportPraSPEXDailyOrderNewDetail>("ReportPraSPEXDailyOrderDetail", new object[] {
                supplierCode == null ? "" : supplierCode,
                dockCode == null ? "" : dockCode,
                manifestNo == null ? "" : manifestNo,
                (dateFrom == "" || dateFrom == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateFrom,"dd.MM.yyyy",null),
                (dateTo == "" || dateTo == null) ? DateTime.ParseExact("01.01.1900","dd.MM.yyyy",null) : DateTime.ParseExact(dateTo,"dd.MM.yyyy",null),
                OrderType  == null ? "" : OrderType,
                manifestNos == null ? "" : manifestNos,
                PartName == "-" ? "" : PartName,
                PartStatus == null ? "" : PartStatus,
                SubSupplierCode == null ? "" : SubSupplierCode, 
                SupplierCodeLDAP,
                DockCodeLDAP,
                SubSupplierCodeLDAP
            });
            hasil = exporter.Write(dailyOrderModelDetail, "SPEX");
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            fileName = "OrderDetail_" + date + ".xls";

            db.Close();

            Response.Clear();
            //Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }
        #endregion


        public String ReGeneratePDF(string ManifestNo)
        {
            IDBContext db = DbContext;
            string result = "";

            try
            {
                result = db.ExecuteScalar<String>("ReGeneratePDFSPEXNEW",
                        new object[] { ManifestNo });
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }
    }
}
