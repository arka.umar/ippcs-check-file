﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Portal.Models.PriceConfirmationApprovalByPC;
using Toyota.Common.Web.Database;
namespace Portal.Controllers
{
    public class PriceConfirmationApprovalByPCController : BaseController
    {
        public PriceConfirmationApprovalByPCController()
            //: base("Price Confirmation Approval By PC")
            : base("Price Confirmation Approval By PC Number")
        {

        }


        protected override void StartUp()
        {

        }

        protected override void Init()
        {
           

            PriceConfirmationApprovalByPCModel mdl = new PriceConfirmationApprovalByPCModel();

            List<PriceConfirmationApprovalByPCDetail> lst = new List<PriceConfirmationApprovalByPCDetail>();

            mdl.PriceConfirmationApprovalByPCs.Add(new PriceConfirmationApprovalByPC()
            {
                PCNo = "1",
                PCDate = DateTime.Now,
                Year = "2012"
            });

            //for (int i = 0; i < 100; i++)
            //{
            //    lst.Add(new PriceConfirmationApprovalByPCDetail()
            //    {
            //        ApprovalNo = i + 1,
            //        MaterialNo = "521590K27" + i,
            //        MaterialDescription = "Insulator" ,
            //        PackType = "0",
            //        UOM = "PC",
            //        CurrentPrice = "20,000",
            //        NewPrice= "30,000",
            //        Remarks = "OK",
            //        //penyatadatadisablenyecuy = "Nusa Toyotetsu Corp"
            //    });
            //}

            //mdl.PriceConfirmationApprovalByPCDetails = lst;
            //Model.AddModel(mdl);
        
        }

        public ActionResult Index(string PCNO)
        {

            PriceConfirmationApprovalByPCModel model = new PriceConfirmationApprovalByPCModel();
            model.PriceConfirmationApprovalByPCDetails = PriceConfirmationApprovalByDetailList(PCNO);
            Model.AddModel(model);

         
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            PriceConfirmationApprovalByPC ListPrice = dbContext.SingleOrDefault<PriceConfirmationApprovalByPC>("GetPriceConfirmationApprovalByPC", new object[] { PCNO });

            PriceConfirmationApprovalByPC listRow = new PriceConfirmationApprovalByPC();
            if (ListPrice != null)
            {
                listRow.PCDate = ListPrice.PCDate;
                listRow.PCNo = ListPrice.PCNo;
                listRow.Year = ListPrice.Year;
            };
            dbContext.Close();
            Model.AddModel(listRow);
            return View("PriceConfirmationApprovalByPCPartial", Model);
        }
        protected List<PriceConfirmationApprovalByPCDetail> PriceConfirmationApprovalByDetailList(string PCNO)
        {

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<PriceConfirmationApprovalByPCDetail> LogList = new List<PriceConfirmationApprovalByPCDetail>();
            var QueryLog = db.Query<PriceConfirmationApprovalByPCDetail>("GetPriceConfirmationApprovalByPC", new object[] { PCNO });

            int index = 0;
            if (QueryLog.Any())
            {
                //PriceConfirmationApprovalByPCModel mdl = new PriceConfirmationApprovalByPCModel();
                //mdl.PriceConfirmationApprovalByPCs.Add(new PriceConfirmationApprovalByPC()
                //        {
                //            PCNo = ,
                //            PCDate = DateTime.Now,
                //            Year = "2012"
                //        });

                foreach (var q in QueryLog)
                {
                    LogList.Add(new PriceConfirmationApprovalByPCDetail()
                    {
                       No = index,
                       ApprovalNo =q.ApprovalNo,
                       CurrentPrice=q.CurrentPrice,
                       MaterialDescription=q.MaterialDescription,
                       MaterialNo=q.MaterialNo,
                       NewPrice=q.NewPrice,
                       PackType=q.PackType,
                       Remarks=q.Remarks,
                       UOM=q.UOM
                    });
                    index++;
                }
            }
            db.Close();
            return LogList;
        }


        //public ActionResult ReloadGrid()
        //{
        //    //string Vers = Request.Params["Vers"];
        //    //string txtDate = Request.Params["txtDate"];
        //    //string AnnualLookup = Request.Params["AnnualLookup"];
        //    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
        //    PriceConfirmationApprovalByPCModel model = Model.GetModel<PriceConfirmationApprovalByPCModel>();
        //    model.PriceConfirmationApprovalByPCDetails = db.Fetch<PriceConfirmationApprovalByPCDetail>("GetPriceConfirmationApprovalByPC", new object[] { Vers, txtDate, AnnualLookup });
        //    return PartialView("GridConsolidatedMaster", Model);
        //}
        public ActionResult PriceConfirmationApprovalByPCPartial()
        {
            return PartialView("PriceConfirmationApprovalByPCPartial", Model);
        }

        public ActionResult PartialHeader()
        {
            return PartialView("PriceConfirmationHeader", Model);
        }

        public ActionResult PartialFooter()
        {
            return PartialView("PriceConfirmationFooter", Model);
        }

        public ActionResult ActionLinkToPcToBeReleased(string SupplierCode)
        {
            return RedirectToAction("Index", "PcToBeReleased");
        }
        
    }
}
