﻿using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Portal.Models.UserMaintenance;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Compression;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.MVC;

namespace Portal.Controllers.Maintenance
{
    public class UserMaintenanceController : BaseController
    {
        public UserMaintenanceController()
            : base("User Maintenance")
        {

        }

        protected override void StartUp()
        {
            
        }

        protected override void Init()
        {
            try
            {

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    
                UserMaintenanceModel um = new UserMaintenanceModel();
                Model.AddModel(um);

                var query = db.Query<CompanyData>("GetCompanyData");

                db.Close();

                ViewData["CompanyData"] = query;


            }
            catch (Exception e)
            { 
            
            
            }
          
        }


        public ActionResult PartialHeader()
        {
            #region Required model in partial page : for SupplierInfoHeaderPartial
            //ViewData["GridSupplier"] = GetAllSupplier();
            //ViewData["GridPart"] = GetAllPart("", "", "", "");
            #endregion

            return PartialView("PartialUserMaintenanceHeader");
        }

        public ActionResult PartialUserDatas()
        {
            UserMaintenanceModel um = new UserMaintenanceModel();
            Model.AddModel(um);

            return PartialView("PartialUserMaintenanceGrid", Model);
        }

        public ActionResult PartialAuthDatas()
        {
            UserMaintenanceModel um = new UserMaintenanceModel();
            Model.AddModel(um);

            return PartialView("PartialAuthMaintenanceGrid", Model);
        }

        public ActionResult PartialHeaderButton()
        {
            return PartialView("PartialUserMaintenanceButtonHeader");
        }

        public ActionResult PartialGridView()
        {
            try
            {
                string pUsername = Request.Params["p_username"];
                string pName = Request.Params["p_Name"];
                string pCompany = Request.Params["p_company"];

                string pClear = Request.Params["p_clear"];


                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                if (pClear == "N")//if pclear = N it means search button pressed, otherwise cancel button pressed
                {
                    var query = db.Query<UserMaintenanceData>("GetAllUserData", new object[] { pUsername, pName, pCompany });
                    um.UserMaintenanceDatas = query.ToList<UserMaintenanceData>();
                }
                db.Close();

                Model.AddModel(um);
           
            }
            catch (Exception e)
            {}

            return PartialView("PartialUserMaintenanceGrid", Model);

        }

        

        public ActionResult getCompanyData()
        {
            try
            {
                TempData["GridName"] = "grlCompany";

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<CompanyData> companyData = db.Query<CompanyData>("GetCompanyData").ToList();


               // var query = db.Query<CompanyData>("GetCompanyData");

                db.Close();

                ViewData["CompanyData"] = companyData;

            }
            catch (Exception e)
            { 
            
            }

            return PartialView("GridLookup/PartialGrid", ViewData["CompanyData"]);
        
        
        }


        public ActionResult getCompanyDataAdd()
        {
            try
            {
                string Company = Request.Params["gridCompanyName"];


                TempData["GridName"] = Company;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<CompanyData> companyData = db.Query<CompanyData>("GetCompanyData").ToList();


                // var query = db.Query<CompanyData>("GetCompanyData");

                db.Close();

                ViewData["CompanyDataAdd"] = companyData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["CompanyData"]);


        }


        public ActionResult getBranchDataAdd()
        {
            try
            {
                string branchName = Request.Params["gridBranchName"];

                TempData["GridName"] = branchName;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<BranchData> branchData = db.Query<BranchData>("GetBranchData").ToList();


                // var query = db.Query<CompanyData>("GetCompanyData");

                db.Close();

                ViewData["BranchDataAdd"] = branchData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["BranchDataAdd"]);
           // return PartialView("gridLookup", ViewData["BranchDataAdd"]);

        }


        public ActionResult getClassDataAdd()
        {
            try
            {
                string pBranch = Request.Params["p_branch"];

                string className = Request.Params["gridClassName"];

                TempData["GridName"] = className;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<ClassData> classData = db.Query<ClassData>("GetClassData", new object[] {pBranch}).ToList();


                // var query = db.Query<CompanyData>("GetCompanyData");

                db.Close();

                ViewData["ClassDataAdd"] = classData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["ClassDataAdd"]);


        }

        public ActionResult getDefaultAppAdd()
        {
            try
            {
                string defaultApp = Request.Params["gridDefaultAppname"];

                TempData["GridName"] = defaultApp;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                
                List<DefaultAppData> DefaultAppData = db.Query<DefaultAppData>("GetDefaultAppData").ToList();

                db.Close();

                ViewData["DefaultAppDataAdd"] = DefaultAppData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["DefaultAppDataAdd"]);
        }




        public ActionResult getLocationDataAdd()
        {
            try
            {
                string Location = Request.Params["gridLocationName"];

                TempData["GridName"] = Location;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                //UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<LocationData> LocationData = db.Query<LocationData>("GetLocationData").ToList();

                db.Close();

                ViewData["LocationDataAdd"] = LocationData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["LocationDataAdd"]);

        }

        public ActionResult getJobFunctionDataAdd()
        {
            try
            {

                string jobfunction = Request.Params["gridJobFunctionName"];

                TempData["GridName"] = jobfunction;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                //UserMaintenanceModel um = Model.GetModel<UserMaintenanceModel>();

                List<JobFunctionData> JobFunctionData = db.Query<JobFunctionData>("GetJobFunctionData").ToList();

                db.Close();

                ViewData["JobFunctionDataAdd"] = JobFunctionData;

            }
            catch (Exception e)
            {

            }

            return PartialView("GridLookup/PartialGrid", ViewData["JobFunctionDataAdd"]);


        }




        public ActionResult PartialPopupUserMaintenance()
        {
            try
            {
                string pBranch = Request.Params["p_branch"];

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                UserMaintenanceModel um = new UserMaintenanceModel();
                Model.AddModel(um);

                var query = db.Query<CompanyData>("GetCompanyData");
                var branch = db.Query<BranchData>("GetBranchData");
                var classs = db.Query<ClassData>("GetClassData", new object[] { pBranch });
                var location = db.Query<LocationData>("GetLocationData");
                var jobfunction = db.Query<JobFunctionData>("GetJobFunctionData");
                var defaultApp = db.Query<DefaultAppData>("GetDefaultAppData");
                

                
                db.Close();

                ViewData["CompanyDataAdd"] = query;
                ViewData["BranchDataAdd"] = branch;
                ViewData["ClassDataAdd"] = classs;
                ViewData["LocationDataAdd"] = location;
                ViewData["JobFunctionDataAdd"] = jobfunction;
                ViewData["DefaultAppDataAdd"] = defaultApp;
            }
            catch (Exception ex)
            { 
            
            }

            return PartialView("PopUpNew");
        
        }


        public ActionResult PartialPopupUserEditMaintenance()
        {
            try
            {
                string pBranch = Request.Params["p_branch"];

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                UserMaintenanceModel um = new UserMaintenanceModel();
                Model.AddModel(um);

                var query = db.Query<CompanyData>("GetCompanyData");
                var branch = db.Query<BranchData>("GetBranchData");
                var classs = db.Query<ClassData>("GetClassData", new object[] { pBranch });
                var location = db.Query<LocationData>("GetLocationData");
                var jobfunction = db.Query<JobFunctionData>("GetJobFunctionData");
                var defaultApp = db.Query<DefaultAppData>("GetDefaultAppData");



                db.Close();

                ViewData["CompanyDataAdd"] = query;
                ViewData["BranchDataAdd"] = branch;
                ViewData["ClassDataAdd"] = classs;
                ViewData["LocationDataAdd"] = location;
                ViewData["JobFunctionDataAdd"] = jobfunction;
                ViewData["DefaultAppDataAdd"] = defaultApp;
            }
            catch (Exception ex)
            {

            }

            return PartialView("PopUpEdit");

        }


        public ActionResult CheckUsername(string username)
        {
            string result = "none";
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try {                
                result = db.SingleOrDefault<string>("checkUserNameExists", new object[] { username });
            }
            catch (Exception e) {
                result = e.Message.ToString();
            }

            db.Close();

            return Content(result);

        }




        public ActionResult getSpecifiedUserData(string username)
        {
            string result = "";
            DateTime MyDateFrom;
            MyDateFrom = new DateTime();
            string varDate = "";


            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                List<UserMaintenanceData> specifiedData = db.Fetch<UserMaintenanceData>("getSpecifiedDataUser", new object[]{username});

                foreach (var data in specifiedData) 
                {
                    result = data.BRANCH_ID.ToString();
                    result += "|";
                    result += data.NO_REG == null ? "" : data.NO_REG.ToString();
                    result += "|";
                    result += data.USERNAME == null ? "" : data.USERNAME.ToString();
                    result += "|";
                    result += data.PASSWORD == null ? "" : data.PASSWORD.ToString();

                    result += "|";
                    varDate = data.PASSWORD_EXPIRATION_DATE.ToString("dd.MM.yyyy");
                    result += varDate;

                    result += "|";
                    varDate = data.USER_EXPIRATION_DATE.ToString("dd.MM.yyyy");
                    result += varDate;

                    result += "|";
                    result += data.ACTIVE_DIRECTORY_FLAG.ToString();
                    result += "|";
                    result += data.LOCK_FLAG.ToString();
                    result += "|";
                    result += data.ISACTIVE_FLAG.ToString();
                    result += "|";
                    result += data.NOTIFICATION_FLAG.ToString();
                    result += "|";
                    result += data.FIRST_NAME == null ? "" : data.FIRST_NAME.ToString();
                    result += "|";
                    result += data.LAST_NAME == null ? "" : data.LAST_NAME.ToString();
                    result += "|";
                    result += data.FULL_NAME == null ? "" : data.FULL_NAME.ToString();
                    result += "|";
                    result += data.TEL_NO == null ? "" : data.TEL_NO.ToString();
                    result += "|";
                    result += data.MOBILE_NO == null ? "" : data.MOBILE_NO.ToString();
                    result += "|";
                    result += data.CLASS_ID == null ? "" : data.CLASS_ID.ToString();
                    result += "|";
                    result += data.JOB_FUNCTION_ID.ToString();
                    result += "|";
                    result += data.CC_CODE == null ? "" : data.CC_CODE.ToString();
                    result += "|";
                    result += data.COMPANY_ID.ToString();
                    result += "|";
                    result += data.LOCATION_ID.ToString();
                    result += "|";
                    result += data.ISPRODUCTION.ToString();
                    result += "|";
                    result += data.MAX_LOGIN.ToString();
                    result += "|";
                    result += data.QUOTA.ToString();
                    result += "|";
                    result += data.DEFAULT_APP.ToString();
                    result += "|";
                    result += data.PASSWORD_MUST_BE_CHANGED.ToString();
                    result += "|";
                    result += data.EMAIL == null ? "" : data.EMAIL.ToString();
                    result += "|";
                    result += data.MULTIPLE_LOGIN.ToString();
                    result += "|";
                    result += data.DELETED_FLAG.ToString();

                }
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }

            db.Close();

            return Content(result);
        }


        public ActionResult AddNewUser(
            string branch, string noreg, string username, string password, string passwordexpirationdate, string userexpirationdate,
            string firstname, string lastname, string fullname, string telpno, string mobileno, string classid, string jobfunction,
            string cccode, string companyid, string locationid, string isproduction, string maxlogin, string quota, string defaultapp,
            string email, string multiplelogin, string passwordchange, string activedirectory, string locked, string isactive,
            string notification, string deletecheck
            )
        {

            string ResultQuery = "Process succesfully";
            string User = AuthorizedUser.Username;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("addUpdateUserProcess", new object[] 
                {
                    branch,  noreg,  username,  password,  passwordexpirationdate,  userexpirationdate,
                    firstname,  lastname,  fullname,  telpno,  mobileno,  classid,  jobfunction,
                    cccode,  companyid,  locationid,  isproduction,  maxlogin,  quota,  defaultapp,
                    email,  multiplelogin,  passwordchange,  activedirectory,  locked,  isactive,
                    notification,  deletecheck, User, "new"
                });
                db.Close();
            }
            catch (Exception ex)
            {
                ResultQuery = "Error: " + ex.Message;
            }
            return Content(ResultQuery);            



        }


        public ActionResult UpdateUser(
            string branch, string noreg, string username, string password, string passwordexpirationdate, string userexpirationdate,
            string firstname, string lastname, string fullname, string telpno, string mobileno, string classid, string jobfunction,
            string cccode, string companyid, string locationid, string isproduction, string maxlogin, string quota, string defaultapp,
            string email, string multiplelogin, string passwordchange, string activedirectory, string locked, string isactive,
            string notification, string deletecheck
            )
        {

            string ResultQuery = "Process succesfully";
            string User = AuthorizedUser.Username;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            try
            {
                db.Execute("addUpdateUserProcess", new object[] 
                {
                    branch,  noreg,  username,  password,  passwordexpirationdate,  userexpirationdate,
                    firstname,  lastname,  fullname,  telpno,  mobileno,  classid,  jobfunction,
                    cccode,  companyid,  locationid,  isproduction,  maxlogin,  quota,  defaultapp,
                    email,  multiplelogin,  passwordchange,  activedirectory,  locked,  isactive,
                    notification,  deletecheck, User, "edit"
                });
                db.Close();

                ResultQuery = "success|";

            }
            catch (Exception ex)
            {
                ResultQuery = "error|Error: " + ex.Message;
            }
            return Content(ResultQuery);

        }

        public ContentResult countData(string pUsername, string pName, string pCompany)
        {
            string resultmessage = "";
            
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                int result = db.SingleOrDefault<int>("countDataUserMaintenanceDownload", new object[] {pUsername, pName, pCompany});
                
                if(result > 0)
                {
                    resultmessage = "download";
                }
                else
                {
                    resultmessage = "Data not found.";
                }
            }
            catch (Exception e)
            { 
                resultmessage = e.Message.ToString();
            }

            return Content(resultmessage);
        
        }


        public ContentResult DeleteUser(string usernamelist)
        {
            string resultMessage = "";
            
            usernamelist = usernamelist.Substring(0, usernamelist.Length - 1);

            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                db.Execute("deleteUser", new object[] { usernamelist });
                resultMessage = "Delete Data Finish Successfully.";
                
            }
            catch (Exception e)
            {
                resultMessage = "Error Occurred";
            }

            return Content(resultMessage.ToString());



        }



        public void download(string pUsername, string pName, string pCompany)
        {
            try
            {
                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;

                DateTime today = DateTime.Today;

                string date = today.ToString("dd-MM-yyyy");


                string filename = "User_Maintenance_List_" + date + ".xls";
                string zipname = "User_Maintenance_List_" + date + ".zip";


                documentBytes = downloadProcess(pUsername, pName, pCompany);

                listCompress.Add(filename, documentBytes);

                compress.Compress(listCompress, output);
                documentBytes = new byte[output.Length];
                output.Position = 0;
                output.Read(documentBytes, 0, documentBytes.Length);

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/zip";
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", zipname));

                Response.BinaryWrite(documentBytes);
                Response.Flush();
                Response.End();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;

            }


        }








        public byte[] downloadProcess(string pUsername, string pName, string pCompany)
        {
            try
            {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();

                string title = "User Maintenance";
                title = title.ToUpper();

                //filesTmp = HttpContext.Request.MapPath("~/Template/User_maintenance_template.xls");

                //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook();

                IRow Hrow;

                int row = 3;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);


                #endregion

                List<UserMaintenanceData> datas = new List<UserMaintenanceData>();
                datas = db.Query<UserMaintenanceData>("GetAllUserData", new object[] { pUsername, pName, pCompany }).ToList();

                //datas = GetSpecifiedLog(processidd);

                /*DateTime MyDateFrom;
                string stringDateFrom;
                MyDateFrom = new DateTime();
                MyDateFrom = DateTime.ParseExact(p_dateFrom, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);

                stringDateFrom = MyDateFrom.ToString("dd.MM.yyyy");

                DateTime MyDateTo;
                string stringDateTo;
                MyDateTo = new DateTime();
                MyDateTo = DateTime.ParseExact(p_dateTo, "yyyy-MM-dd",
                                                 System.Globalization.CultureInfo.InvariantCulture);
                stringDateTo = MyDateTo.ToString("dd.MM.yyyy");*/


                ISheet sheet;

                sheet = workbook.CreateSheet("Sheet 1");
                Hrow = sheet.CreateRow(0);
                Hrow.CreateCell(0).SetCellValue(title);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                Hrow.GetCell(0).CellStyle = styleContent4;


                /*Hrow = sheet.CreateRow(1);
                Hrow.CreateCell(0).SetCellValue("Prod. Date : " + stringDateFrom + " - " + stringDateTo);
                sheet.AddMergedRegion(new CellRangeAddress(1, 1, 0, 3));*/

                Hrow = sheet.CreateRow(2);

                Hrow.CreateCell(0).SetCellValue("No. ");
                sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("Branch      ");
                sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("Username       ");
                sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Password           ");
                sheet.AutoSizeColumn(3);
                Hrow.CreateCell(4).SetCellValue("Password Expiration Date           ");
                sheet.AutoSizeColumn(4);
                Hrow.CreateCell(5).SetCellValue("User Expiration Date       ");
                sheet.AutoSizeColumn(5);
                Hrow.CreateCell(6).SetCellValue("Active Directory Flag      ");
                sheet.AutoSizeColumn(6);
                Hrow.CreateCell(7).SetCellValue("Lock Flag      ");
                sheet.AutoSizeColumn(7);
                Hrow.CreateCell(8).SetCellValue("Is Active Flag        ");
                sheet.AutoSizeColumn(8);
                Hrow.CreateCell(9).SetCellValue("Notification Flag      ");
                sheet.AutoSizeColumn(9);
                Hrow.CreateCell(10).SetCellValue("No Reg.        ");
                sheet.AutoSizeColumn(10);
                Hrow.CreateCell(11).SetCellValue("First Name            ");
                sheet.AutoSizeColumn(11);
                Hrow.CreateCell(12).SetCellValue("Last Name                   ");
                sheet.AutoSizeColumn(12);
                Hrow.CreateCell(13).SetCellValue("Full Name                   ");
                sheet.AutoSizeColumn(13);
                Hrow.CreateCell(14).SetCellValue("Mobile No           ");
                sheet.AutoSizeColumn(14);
                Hrow.CreateCell(15).SetCellValue("Class ID            ");
                sheet.AutoSizeColumn(15);
                Hrow.CreateCell(16).SetCellValue("Job Function ID           ");
                sheet.AutoSizeColumn(16);
                Hrow.CreateCell(17).SetCellValue("CC Code       ");
                sheet.AutoSizeColumn(17);
                Hrow.CreateCell(18).SetCellValue("Company ID         ");
                sheet.AutoSizeColumn(18);
                Hrow.CreateCell(19).SetCellValue("Company Name       ");
                sheet.AutoSizeColumn(19);
                Hrow.CreateCell(20).SetCellValue("Location ID      ");
                sheet.AutoSizeColumn(20);
                Hrow.CreateCell(21).SetCellValue("Is Production       ");
                sheet.AutoSizeColumn(21);
                Hrow.CreateCell(22).SetCellValue("Max Login        ");
                sheet.AutoSizeColumn(22);
                Hrow.CreateCell(23).SetCellValue("Quota       ");
                sheet.AutoSizeColumn(23);
                Hrow.CreateCell(24).SetCellValue("Delete Flag       ");
                sheet.AutoSizeColumn(24);
                Hrow.CreateCell(25).SetCellValue("Default App       ");
                sheet.AutoSizeColumn(25);
                Hrow.CreateCell(26).SetCellValue("ID       ");
                sheet.AutoSizeColumn(26);
                Hrow.CreateCell(27).SetCellValue("Created By                 ");
                sheet.AutoSizeColumn(27);
                Hrow.CreateCell(28).SetCellValue("Created Date                   ");
                sheet.AutoSizeColumn(28);
                Hrow.CreateCell(29).SetCellValue("Changed By                   ");
                sheet.AutoSizeColumn(29);
                Hrow.CreateCell(30).SetCellValue("Changed Date                  ");
                sheet.AutoSizeColumn(30);
                Hrow.CreateCell(31).SetCellValue("Email                        ");
                sheet.AutoSizeColumn(31);
                Hrow.CreateCell(32).SetCellValue("Last Logon                  ");
                sheet.AutoSizeColumn(32);
                Hrow.CreateCell(33).SetCellValue("Multiple Login       ");
                sheet.AutoSizeColumn(33);
                Hrow.CreateCell(34).SetCellValue("Password Must Be Changed      ");
                sheet.AutoSizeColumn(34);

                for(int cell = 0; cell < 35;cell++)
                {
                    Hrow.GetCell(cell).CellStyle = styleContent2;
                }

                int no = 1;

                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);
                    
                    
                    Hrow.CreateCell(0).SetCellValue(no);
                    Hrow.CreateCell(1).SetCellValue(data.BRANCH_ID);
                    Hrow.CreateCell(2).SetCellValue(data.USERNAME);
                    Hrow.CreateCell(3).SetCellValue(data.PASSWORD);
                    Hrow.CreateCell(4).SetCellValue((data.PASSWORD_EXPIRATION_DATE.ToString("dd.MM.yyyy") == "01.01.0001") ? "" : data.PASSWORD_EXPIRATION_DATE.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(5).SetCellValue((data.USER_EXPIRATION_DATE.ToString("dd.MM.yyyy") == "01.01.0001") ? "" : data.USER_EXPIRATION_DATE.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(6).SetCellValue(data.ACTIVE_DIRECTORY_FLAG);
                    Hrow.CreateCell(7).SetCellValue(data.LOCK_FLAG);
                    Hrow.CreateCell(8).SetCellValue(data.ISACTIVE_FLAG);
                    Hrow.CreateCell(9).SetCellValue(data.NOTIFICATION_FLAG);
                    Hrow.CreateCell(10).SetCellValue(data.NO_REG);
                    Hrow.CreateCell(11).SetCellValue(data.FIRST_NAME);
                    Hrow.CreateCell(12).SetCellValue(data.LAST_NAME);
                    Hrow.CreateCell(13).SetCellValue(data.FULL_NAME);
                    Hrow.CreateCell(14).SetCellValue(data.MOBILE_NO);
                    Hrow.CreateCell(15).SetCellValue(data.CLASS_ID);
                    Hrow.CreateCell(16).SetCellValue(data.JOB_FUNCTION_ID);
                    Hrow.CreateCell(17).SetCellValue(data.CC_CODE);
                    Hrow.CreateCell(18).SetCellValue(data.COMPANY_ID);
                    Hrow.CreateCell(19).SetCellValue(data.COMPANY_NAME);
                    Hrow.CreateCell(20).SetCellValue(data.LOCATION_ID);
                    Hrow.CreateCell(21).SetCellValue(data.ISPRODUCTION);
                    Hrow.CreateCell(22).SetCellValue(data.MAX_LOGIN);
                    Hrow.CreateCell(23).SetCellValue(data.QUOTA);
                    Hrow.CreateCell(24).SetCellValue(data.DELETED_FLAG);
                    Hrow.CreateCell(25).SetCellValue(data.DEFAULT_APP);
                    Hrow.CreateCell(26).SetCellValue(data.ID);
                    Hrow.CreateCell(27).SetCellValue(data.CREATED_BY);
                    Hrow.CreateCell(28).SetCellValue(data.CREATED_DATE.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(29).SetCellValue(data.CHANGED_BY);
                    Hrow.CreateCell(30).SetCellValue((data.CHANGED_DATE.ToString("dd.MM.yyyy") == "01.01.0001") ? "" : data.CHANGED_DATE.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(31).SetCellValue(data.EMAIL);
                    Hrow.CreateCell(32).SetCellValue((data.LAST_LOGON.ToString("dd.MM.yyyy") == "01.01.0001") ? "" : data.LAST_LOGON.ToString("dd.MM.yyyy"));
                    Hrow.CreateCell(33).SetCellValue(data.MULTIPLE_LOGIN);
                    Hrow.CreateCell(34).SetCellValue(data.PASSWORD_MUST_BE_CHANGED);

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent1;

                    for(int cel = 2; cel< 34; cel++)
                    {
                        Hrow.GetCell(cel).CellStyle = styleContent3;
                    }

                    Hrow.GetCell(34).CellStyle = styleContent1;

                    row++;
                    no++;
                }

                



                //for (int autosize = 0; autosize < 35; autosize++)
                //{
                 //   sheet.AutoSizeColumn(4);
                 //   sheet.AutoSizeColumn(autosize);
                   
                //}


                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                //ftmp.Close();
                return ms.ToArray();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();
                return a;
            }

        }





        [HttpPost]
        public void OnUploadCallbackRouteValues()
        {
            UploadControlExtension.GetUploadedFiles("UMUpload", ValidationSettings, FileUploadComplete);
        }

        protected readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xls" },
            MaxFileSize = 1000000000,
            MaxFileSizeErrorText = "The size of file uploaded larger than allowed",
            ShowErrors = true
        };

        protected void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                _FilePath = _UploadDirectory + e.UploadedFile.FileName;

                e.UploadedFile.SaveAs(_FilePath, true);
                e.CallbackData = _FilePath;
            }
        }

        private String _filePath = "";
        private String _FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }


        private String _uploadDirectory = "";
        private String _UploadDirectory
        {
            get
            {
                _uploadDirectory = Server.MapPath("~/Views/UserMaintenance/TempUpload/");
                return _uploadDirectory;
            }
        }


        #region ACTION
        [HttpPost]
        public ActionResult UserMaintenanceUpload(string filePath)
        {
            AllProcessUpload(filePath);

            String status = Convert.ToString(TempData["IsValid"]);

            return Json(
                new
                {
                    Status = status
                });
        }
        #endregion

        public FileContentResult DownloadTemplate()
        {
            string filePath = string.Empty;
            string fileName = string.Empty;
            byte[] documentBytes = null;

            fileName = "User_maintenance_template.xls";
            filePath = Path.Combine(Server.MapPath("~/Views/UserMaintenance/TempXls/" + fileName));
            documentBytes = StreamFile(filePath);

            return File(documentBytes, "application/vnd.ms-excel", fileName);
        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }




        ////
        [HttpGet]
        public void UploadInvalid(String process_id)
        {
            try
            {
                string filesTmp = "";

                ICompression compress = ZipCompress.GetInstance();
                Dictionary<string, byte[]> listCompress = new Dictionary<string, byte[]>();
                Stream output = new MemoryStream();
                byte[] documentBytes;

                filesTmp = HttpContext.Request.MapPath("~/Template/User_maintenance_template.xls");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp);

                IRow Hrow;

                int row = 1;

                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

                #region Styling Row Data
                IFont Font4 = workbook.CreateFont();
                Font4.FontHeightInPoints = 10;
                Font4.FontName = "Arial";

                IFont Font1 = workbook.CreateFont();
                Font1.FontHeightInPoints = 10;
                Font1.FontName = "Arial";
                Font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent1.SetFont(Font4);

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent2.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_ORANGE.index;
                styleContent2.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleContent2.Alignment = HorizontalAlignment.LEFT;
                styleContent2.SetFont(Font1);

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.TOP;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                styleContent3.SetFont(Font4);
                styleContent3.Alignment = HorizontalAlignment.LEFT;


                ICellStyle styleContent4 = workbook.CreateCellStyle();
                styleContent4.VerticalAlignment = VerticalAlignment.TOP;
                styleContent4.SetFont(Font1);

                #endregion

                List<UMErrorListDownload> datas = new List<UMErrorListDownload>();

                datas = db.Query<UMErrorListDownload>("GET_ERROR_LIST_UPLOAD_USER_MAINTENANCE", new object[] { process_id }).ToList(); ;

                ISheet sheet;

                sheet = workbook.GetSheetAt(0);
                Hrow = sheet.CreateRow(0);


                Hrow = sheet.CreateRow(0);

                Hrow.CreateCell(0).SetCellValue("Branch      ");
                sheet.AutoSizeColumn(0);
                Hrow.CreateCell(1).SetCellValue("Username       ");
                sheet.AutoSizeColumn(1);
                Hrow.CreateCell(2).SetCellValue("Password           ");
                sheet.AutoSizeColumn(2);
                Hrow.CreateCell(3).SetCellValue("Password Expiration Date           ");
                sheet.AutoSizeColumn(3);
                Hrow.CreateCell(4).SetCellValue("User Expiration Date       ");
                sheet.AutoSizeColumn(4);
                Hrow.CreateCell(5).SetCellValue("Active Directory Flag      ");
                sheet.AutoSizeColumn(5);
                Hrow.CreateCell(6).SetCellValue("Lock Flag      ");
                sheet.AutoSizeColumn(6);
                Hrow.CreateCell(7).SetCellValue("Is Active Flag        ");
                sheet.AutoSizeColumn(7);
                Hrow.CreateCell(8).SetCellValue("Notification Flag      ");
                sheet.AutoSizeColumn(8);
                Hrow.CreateCell(9).SetCellValue("No Reg.        ");
                sheet.AutoSizeColumn(9);
                Hrow.CreateCell(10).SetCellValue("First Name            ");
                sheet.AutoSizeColumn(10);
                Hrow.CreateCell(11).SetCellValue("Last Name                   ");
                sheet.AutoSizeColumn(11);
                Hrow.CreateCell(12).SetCellValue("Tel No");
                sheet.AutoSizeColumn(12);
                Hrow.CreateCell(13).SetCellValue("Mobile No           ");
                sheet.AutoSizeColumn(13);
                Hrow.CreateCell(14).SetCellValue("Class ID            ");
                sheet.AutoSizeColumn(14);
                Hrow.CreateCell(15).SetCellValue("Job Function ID           ");
                sheet.AutoSizeColumn(15);
                Hrow.CreateCell(16).SetCellValue("CC Code       ");
                sheet.AutoSizeColumn(16);
                Hrow.CreateCell(17).SetCellValue("Company ID         ");
                sheet.AutoSizeColumn(17);
                Hrow.CreateCell(18).SetCellValue("Location ID      ");
                sheet.AutoSizeColumn(18);
                Hrow.CreateCell(19).SetCellValue("Is Production       ");
                sheet.AutoSizeColumn(19);
                Hrow.CreateCell(20).SetCellValue("Max Login        ");
                sheet.AutoSizeColumn(20);
                Hrow.CreateCell(21).SetCellValue("Quota       ");
                sheet.AutoSizeColumn(21);
                Hrow.CreateCell(22).SetCellValue("Delete Flag       ");
                sheet.AutoSizeColumn(22);
                Hrow.CreateCell(23).SetCellValue("Default App       ");
                sheet.AutoSizeColumn(23);
                Hrow.CreateCell(24).SetCellValue("Email                        ");
                sheet.AutoSizeColumn(24);
                Hrow.CreateCell(25).SetCellValue("Multiple Login       ");
                sheet.AutoSizeColumn(25);
                Hrow.CreateCell(26).SetCellValue("Password Must Be Changed      ");
                sheet.AutoSizeColumn(26);
                Hrow.CreateCell(27).SetCellValue("Message      ");
                sheet.AutoSizeColumn(27);

                for (int cell = 0; cell < 28; cell++)
                {
                    Hrow.GetCell(cell).CellStyle = styleContent2;
                }

                
                foreach (var data in datas)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(data.BRANCH_ID);
                    Hrow.CreateCell(1).SetCellValue(data.USERNAME);
                    Hrow.CreateCell(2).SetCellValue(data.PASSWORD);
                    Hrow.CreateCell(3).SetCellValue(data.PASSWORD_EXPIRATION_DATE);
                    Hrow.CreateCell(4).SetCellValue(data.USER_EXPIRATION_DATE);
                    Hrow.CreateCell(5).SetCellValue(data.ACTIVE_DIRECTORY_FLAG);
                    Hrow.CreateCell(6).SetCellValue(data.LOCK_FLAG);
                    Hrow.CreateCell(7).SetCellValue(data.ISACTIVE_FLAG);
                    Hrow.CreateCell(8).SetCellValue(data.NOTIFICATION_FLAG);
                    Hrow.CreateCell(9).SetCellValue(data.NO_REG);
                    Hrow.CreateCell(10).SetCellValue(data.FIRST_NAME);
                    Hrow.CreateCell(11).SetCellValue(data.LAST_NAME);
                    Hrow.CreateCell(12).SetCellValue(data.TEL_NO);
                    Hrow.CreateCell(13).SetCellValue(data.MOBILE_NO);
                    Hrow.CreateCell(14).SetCellValue(data.CLASS_ID);
                    Hrow.CreateCell(15).SetCellValue(data.JOB_FUNCTION_ID);
                    Hrow.CreateCell(16).SetCellValue(data.CC_CODE);
                    Hrow.CreateCell(17).SetCellValue(data.COMPANY_ID);
                    Hrow.CreateCell(18).SetCellValue(data.LOCATION_ID);
                    Hrow.CreateCell(19).SetCellValue(data.ISPRODUCTION);
                    Hrow.CreateCell(20).SetCellValue(data.MAX_LOGIN);
                    Hrow.CreateCell(21).SetCellValue(data.QUOTA);
                    Hrow.CreateCell(22).SetCellValue(data.DELETED_FLAG);
                    Hrow.CreateCell(23).SetCellValue(data.DEFAULT_APP);
                    Hrow.CreateCell(24).SetCellValue(data.EMAIL);
                    Hrow.CreateCell(25).SetCellValue(data.MULTIPLE_LOGIN);
                    Hrow.CreateCell(26).SetCellValue(data.PASSWORD_MUST_BE_CHANGED);
                    Hrow.CreateCell(27).SetCellValue(data.MSGG);

                    Hrow.GetCell(0).CellStyle = styleContent3;
                    Hrow.GetCell(1).CellStyle = styleContent1;

                    for (int cel = 2; cel < 27; cel++)
                    {
                        Hrow.GetCell(cel).CellStyle = styleContent3;
                    }

                    Hrow.GetCell(27).CellStyle = styleContent1;

                    row++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();

                documentBytes = ms.ToArray();

                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;

                string FileNames = "userMaintenance_Invalid.xls";

                Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", FileNames));
                Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

                Response.BinaryWrite(documentBytes);
                Response.End();


                //return ms.ToArray();

            }
            catch (Exception e)
            {
                string x = e.ToString();
                byte[] a = null;
                x = e.ToString();

                //return a;
            }

        }



        //---
        public ActionResult MoveUserDataTemp(string processId)
        {
            string message = "";
            string user = AuthorizedUser.Username;
            try
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("moveUserDataTemp", new object[] { processId, user });
                message = "SUCCESS";
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }


            return Content(message);
        }

        //--




        


        private UserMaintenanceDataTemp AllProcessUpload(string filePath)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            UserMaintenanceDataTemp _UploadModel = new UserMaintenanceDataTemp();

            #region DECLARE XLS PROPERTIES

            string function = "61007";
            string module = "6";
            string location = "";
            string message = "";
            string processID = "";
            string username = AuthorizedUser.Username;
            string sts = "0";

            string tableFromTemp = "TB_T_USER_MAINTENANCE_UPLOAD_TEMP";
            //string tableToAccess = "TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP";

            string sheetName = "UserList";

            message = "Starting Upload User Maintenance";
            location = "UserMaintenanceUpload.init";

            processID = db.ExecuteScalar<string>(
                                "GenerateProcessId",
                                new object[] { 
                            message,     // what
                            username,     // user
                            location,     // where
                            "",     // pid OUTPUT
                            "MPCS00004INF",     // id
                            "",     // type
                            module,     // module
                            function,     // function
                            "0"      // sts
                        });


            OleDbConnection excelConn = null;

            try
            {

                message = "Clear TB_T_USER_MAINTENANCE_UPLOAD_TEMP";
                location = "UserMaintenanceUpload.process";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });


                // clear temporary upload table.
                db.ExecuteScalar<String>("ClearTempUpload", new object[] { tableFromTemp });
                //db.ExecuteScalar<String>("ClearTempUpload", new object[] { _TableDestination.tableToAccess });

                #region EXECUTE EXCEL AS TEMP_DB
                // read uploaded excel file,
                // get temporary upload table column name, 
                // get uploaded excel file column value and
                // insert uploaded excel file value into temporary upload table.
                DataSet ds = new DataSet();
                OleDbCommand excelCommand = new OleDbCommand();
                OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
                //Modified By FID.Reggy, change HDR into No, so excel OLE will read and cast all data into text
                string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties =\"Excel 8.0;HDR=No;IMEX=1;ImportMixedTypes=Text\"";
                //string excelConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + "; Extended Properties = Excel 8.0";

                excelConn = new OleDbConnection(excelConnStr);
                excelConn.Open();
                DataSet dsExcel = new DataSet();
                DataTable dtPatterns = new DataTable();

                string OleCommand = @"select " +
                                    processID + " as PROCESS_ID, " +
                                    @"IIf(IsNull(F1), Null, CStr(F1)) as BranchID," +
                                    @"IIf(IsNull(F2), Null, CStr(F2)) as Username," +
                                    @"IIf(IsNull(F3), Null, CStr(F3)) as Passwordd," +
                                    @"IIf(IsNull(F4), Null, CStr(F4)) as PasswordExpiredDate," +
                                    @"IIf(IsNull(F5), Null, CStr(F5)) as UserExpiredDate," +
                                    @"IIf(IsNull(F6), Null, CStr(F6)) as ADFlag," +
                                    @"IIf(IsNull(F7), Null, CStr(F7)) as LockFlag," +
                                    @"IIf(IsNull(F8), Null, CStr(F8)) as IsInactiveFlag," +
                                    @"IIf(IsNull(F9), Null, CStr(F9)) as NotificationFlag," +
                                    @"IIf(IsNull(F10), Null, CStr(F10)) as NoReg," +
                                    @"IIf(IsNull(F11), Null, CStr(F11)) as FirstName," +
                                    @"IIf(IsNull(F12), Null, CStr(F12)) as LastName," +
                                    @"IIf(IsNull(F13), Null, CStr(F13)) as TelpNo," +
                                    @"IIf(IsNull(F14), Null, CStr(F14)) as MobileNo," +
                                    @"IIf(IsNull(F15), Null, CStr(F15)) as ClassID," +
                                    @"IIf(IsNull(F16), Null, CStr(F16)) as JobFunctionID," +
                                    @"IIf(IsNull(F17), Null, CStr(F17)) as CCCode," +
                                    @"IIf(IsNull(F18), Null, CStr(F18)) as CompanyID," +
                                    @"IIf(IsNull(F19), Null, CStr(F19)) as LocationID," +
                                    @"IIf(IsNull(F20), Null, CStr(F20)) as IsProduction," +
                                    @"IIf(IsNull(F21), Null, CStr(F21)) as MaxLogin," +
                                    @"IIf(IsNull(F22), Null, CStr(F22)) as Quota," +
                                    @"IIf(IsNull(F23), Null, CStr(F23)) as DeleteFlag," +
                                    @"IIf(IsNull(F24), Null, CStr(F24)) as DefaultApp," +
                                    //@"IIf(IsNull(F25), Null, CStr(F25)) as Id," +
                                    @"IIf(IsNull(F11), Null, CStr(F11)) + '' + IIf(IsNull(F12), Null, CStr(F12)) as FullName, " +
                                    @"IIf(IsNull(F25), Null, CStr(F25)) as Email," +
                                    @"IIf(IsNull(F26), Null, CStr(F26)) as MultipleLogin," +
                                    @"IIf(IsNull(F27), Null, CStr(F27)) as PasswordMustBeChanged ";
                                    
                OleCommand = OleCommand + "from [" + sheetName + "$]";

                excelCommand = new OleDbCommand(OleCommand, excelConn);

                excelDataAdapter.SelectCommand = excelCommand;
                //add by alira.agi 2015-04-10
                try
                {
                    excelDataAdapter.Fill(dtPatterns);
                    ds.Tables.Add(dtPatterns);

                    //Added By FID.reggy
                    //Deleting first row (column alias row), impact for using HDR:No in ConnectionString
                    DataRow rowDel = ds.Tables[0].Rows[0];
                    ds.Tables[0].Rows.Remove(rowDel);
                }
                catch (Exception e)
                {
                    //throw new Exception("invalid excel template please download template excel");
                    //Changed By FID.Reggy
                    message = "Error : " + e.Message;
                    location = "UserMaintenanceUpload.finish";

                    processID = db.ExecuteScalar<string>(
                                        "GenerateProcessId",
                                        new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });



                    throw new Exception(e.Message);
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
                {
                    //try
                    //{
                    bulkCopy.DestinationTableName = tableFromTemp;
                    bulkCopy.ColumnMappings.Clear();

                    bulkCopy.ColumnMappings.Add("PROCESS_ID", "PROCESS_ID");
                    bulkCopy.ColumnMappings.Add("BranchID", "BRANCH_ID");
                    bulkCopy.ColumnMappings.Add("Username", "USERNAME");
                    bulkCopy.ColumnMappings.Add("Passwordd", "PASSWORD");
                    bulkCopy.ColumnMappings.Add("PasswordExpiredDate", "PASSWORD_EXPIRATION_DATE");
                    bulkCopy.ColumnMappings.Add("UserExpiredDate", "USER_EXPIRATION_DATE");
                    bulkCopy.ColumnMappings.Add("ADFlag", "ACTIVE_DIRECTORY_FLAG");
                    bulkCopy.ColumnMappings.Add("LockFlag", "LOCK_FLAG");
                    bulkCopy.ColumnMappings.Add("IsInactiveFlag", "ISACTIVE_FLAG");
                    bulkCopy.ColumnMappings.Add("NotificationFlag", "NOTIFICATION_FLAG");
                    bulkCopy.ColumnMappings.Add("NoReg", "NO_REG");
                    bulkCopy.ColumnMappings.Add("FirstName", "FIRST_NAME");
                    bulkCopy.ColumnMappings.Add("LastName", "LAST_NAME");
                    bulkCopy.ColumnMappings.Add("TelpNo", "TEL_NO");
                    bulkCopy.ColumnMappings.Add("MobileNo", "MOBILE_NO");
                    bulkCopy.ColumnMappings.Add("ClassID", "CLASS_ID");
                    bulkCopy.ColumnMappings.Add("JobFunctionID", "JOB_FUNCTION_ID");
                    bulkCopy.ColumnMappings.Add("CCCode", "CC_CODE");
                    bulkCopy.ColumnMappings.Add("CompanyID", "COMPANY_ID");
                    bulkCopy.ColumnMappings.Add("LocationID", "LOCATION_ID");
                    bulkCopy.ColumnMappings.Add("IsProduction", "ISPRODUCTION");
                    bulkCopy.ColumnMappings.Add("MaxLogin", "MAX_LOGIN");
                    bulkCopy.ColumnMappings.Add("Quota", "QUOTA");
                    bulkCopy.ColumnMappings.Add("DeleteFlag", "DELETED_FLAG");
                    bulkCopy.ColumnMappings.Add("DefaultApp", "DEFAULT_APP");
                    bulkCopy.ColumnMappings.Add("Email", "EMAIL");
                    bulkCopy.ColumnMappings.Add("MultipleLogin", "MULTIPLE_LOGIN");
                    bulkCopy.ColumnMappings.Add("PasswordMustBeChanged", "PASSWORD_MUST_BE_CHANGED");
                    bulkCopy.ColumnMappings.Add("FullName", "FULL_NAME");

                #endregion

                    try
                    {
                        message = "Insert Data to TB_T_USER_MAINTENANCE_UPLOAD_TEMP";
                        location = "UserMaintenanceUpload.process";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "0"      // sts
                                        });



                        // Write from the source to the destination.
                        // Check if the data set is not null and tables count > 0 etc
                        bulkCopy.WriteToServer(ds.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        message = "Error : " + ex.Message;
                        location = "UserMaintenanceUpload.finish";

                        processID = db.ExecuteScalar<string>(
                                            "GenerateProcessId",
                                            new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            "6"      // sts
                                        });
                        //modif by alira.agi 2015-04-10
                        //throw new Exception ("Column does not match");

                        //modified by fid.intan 2015-08-24
                        throw new Exception(ex.Message);
                    }
                }


                // validate data in temporary uploaded table.
                string validate = db.SingleOrDefault<string>("ValidateUserMaintenanceUpload", new object[] { processID });




                if (validate == "error")
                {
                    TempData["IsValid"] = "false;" + processID;
                    sts = "6";
                    message = "Upload finish with error.";
                }
                else if (validate == "No Data Found.")
                {
                    TempData["IsValid"] = "empty;" + processID;
                    sts = "6";
                    message = validate;
                }
                else
                {
                    TempData["IsValid"] = "true;" + processID;
                    sts = "1";
                    message = "Upload finish.";
                }


                location = "UserMaintenanceUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            sts      // sts
                                        });

            }
            catch (Exception exc)
            {

                TempData["IsValid"] = "false;" + processID + ";" + exc.Message;


                message = "Error : " + exc.Message;
                location = "UserMaintenanceUpload.finish";

                processID = db.ExecuteScalar<string>(
                                    "GenerateProcessId",
                                    new object[] { 
                                            message,     // what
                                            username,     // user
                                            location,     // where
                                            processID,     // pid OUTPUT
                                            "MPCS00004INF",     // id
                                            "",     // type
                                            module,     // module
                                            function,     // function
                                            6      // sts
                                        });

            }
            finally
            {


                excelConn.Close();

                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

            }
            return _UploadModel;
        }
            #endregion

    }
}
