﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Html;
using Toyota.Common.Web.Configuration;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Menu;
using Toyota.Common.Web.Credential;

namespace Portal.Controllers.Maintenance
{
    public class MenuMaintenanceController : MaintenanceController
    {
        public MenuMaintenanceController(): base("Menu Maintenance")
        {

        }
        
        protected override void InitComponents()
        {
            AddSearchParameter(new HtmlTextComponent("SearchScreenId", "Screen ID", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchCaption", "Caption", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchUrl", "URL", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchParentId", "Parent ID", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchhPosition", "Position", typeof(string)));

            SetDataGrid(new HtmlDataGridComponent("MenuTable", "Menu Table", typeof(ConfigurationItem)));
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.KeyFieldName = "Id";
            grid.Columns.Add(new HtmlTextComponent("Id", "Id", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("ScreenId", "Screen ID", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Caption", "Caption", typeof(string), Unit.Pixel(200)));
            grid.Columns.Add(new HtmlTextComponent("Url", "URL", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("ParentId", "Parent ID", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Position", "Position", typeof(string), Unit.Pixel(120)));
            grid.DataSource = MenuProvider.GetInstance().GetAll(Model.GetModel<User>());
        }
        
        protected override void OnUpdatingSearchResult(Dictionary<string, string> requestParams)
        {
            
        }

        protected override void PerformUpdate(Dictionary<string, string> requestParams)
        {
         
        }

        protected override void PerformDelete(Dictionary<string, string> requestParams)
        {
            
        }

        protected override void PerformBulkDelete(List<string[]> selectedKeys)
        {
            
        }

        protected override void PerformInsert(Dictionary<string, string> requestParams)
        {
            
        }

        protected override void OnUpdatingDetailSearchResult(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailUpdate(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailDelete(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformBulkDetailDelete(List<string[]> selectedKeys)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailInsert(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }
    }
}
