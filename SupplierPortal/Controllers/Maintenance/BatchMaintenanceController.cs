﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Html;
using Toyota.Common.Web.Batch;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Database;
using Portal.Models.Master;
using Toyota.Common.Web.Credential;

namespace Portal.Controllers.Maintenance
{
    public class BatchMaintenanceController : MaintenanceController
    {
        public BatchMaintenanceController():base("Batch Maintenance") { }

        protected override void InitComponents()
        {
            AddSearchParameter(new HtmlTextComponent("SearchBatchId", "Batch ID", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchBatchName", "Batch Name", typeof(string)));

            SetDataGrid(new HtmlDataGridComponent("batchTable", "Batch Processor Table", typeof(BatchProcessor)));
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.KeyFieldName = "Id";
            grid.Columns.Add(new HtmlTextComponent("Id", "ID", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Name", "Name", typeof(string), Unit.Pixel(120)));
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            grid.DataSource = db.Fetch<BatchProcessor>("BatchProcessor_SelectAll");

            HtmlOptionComponent priorityOptions = new HtmlOptionComponent("PriorityName", "Priority", typeof(string), Unit.Pixel(120));
            priorityOptions.ValueType = typeof(byte);
            priorityOptions.ValueField = "Level";
            priorityOptions.TextField = "Name";
            priorityOptions.Items = db.Fetch<Priority>("Master_Priority_SelectAll");
            grid.Columns.Add(priorityOptions);

            grid.Columns.Add(new HtmlTextComponent("Command", "Command", typeof(string), Unit.Pixel(120)));
            db.Close();
        }

        protected override void OnUpdatingSearchResult(Dictionary<string, string> requestParams)
        {
            string sId = null;
            string sName = null;
            if (requestParams.ContainsKey("SearchBatchId"))
            {
                sId = requestParams["SearchBatchId"];
            }
            if (requestParams.ContainsKey("SearchBatchName"))
            {
                sName = requestParams["SearchBatchName"];
            }

            List<BatchProcessor> resultList = new List<BatchProcessor>();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);

            if(string.IsNullOrEmpty(sId) && string.IsNullOrEmpty(sName)) 
            {
                resultList = db.Fetch<BatchProcessor>("BatchProcessor_SelectAll");
            }
            else if (!string.IsNullOrEmpty(sId) && string.IsNullOrEmpty(sName))
            {
                resultList = db.Fetch<BatchProcessor>("BatchProcessor_SelectById", new object[] {sId});
            }
            else if (string.IsNullOrEmpty(sId) && !string.IsNullOrEmpty(sName))
            {
                resultList = db.Fetch<BatchProcessor>("BatchProcessor_SelectByName", new object[] { sName });
            }
            else if (!string.IsNullOrEmpty(sId) && !string.IsNullOrEmpty(sName))
            {
                resultList = db.Fetch<BatchProcessor>("BatchProcessor_SelectByAllParam", new object[] { sId, sName });
            }
            db.Close();
           
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.DataSource = resultList;
        }

        protected override void PerformUpdate(Dictionary<string, string> requestParams)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<Priority> priorityList = db.Fetch<Priority>("Master_Priority_SelectAll");

            string Id = requestParams["Id"];
            string sLevel = null;
            byte? level = null;
            string name = null;
            string command = null;
            if (requestParams.ContainsKey("PriorityName"))
            {
                sLevel = requestParams["PriorityName"];
                foreach (Priority p in priorityList)
                {
                    if (p.Name.Equals(sLevel))
                    {
                        level = p.Level;
                        break;
                    }
                }
            }
            if (requestParams.ContainsKey("Name"))
            {
                name = requestParams["Name"];
            }
            if (requestParams.ContainsKey("Command"))
            {
                command = requestParams["Command"];
            }

            
            db.Execute("BatchProcessor_Update", new object[] {
                name, level, command, SessionState.GetAuthorizedUser().Username, DateTime.Now, Id
            });
            db.Close();
        }

        protected override void PerformDelete(Dictionary<string, string> requestParams)
        {
            string selectedKey = requestParams.ContainsKey("Id") ? requestParams["Id"] : null;
            if (!string.IsNullOrEmpty(selectedKey))
            {
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("BatchProcessor_Delete", new object[] { selectedKey });
                db.Close();
            }
        }

        protected override void PerformBulkDelete(List<string[]> selectedKeys)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            foreach (string[] keys in selectedKeys)
            {
                db.Execute("BatchProcessor_Delete", new object[] { keys[0].Trim() });
            }
            db.Close();
        }

        protected override void PerformInsert(Dictionary<string, string> requestParams)
        {
            string id = requestParams.ContainsKey("Id") ? requestParams["Id"] : null;
            string name = requestParams.ContainsKey("Name") ? requestParams["Name"] : null;
            string priorityName = requestParams.ContainsKey("PriorityName") ? requestParams["PriorityName"] : null;
            byte? priorityLevel = null;
            string command = requestParams.ContainsKey("Command") ? requestParams["Command"] : null;

            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            if (!string.IsNullOrEmpty(priorityName))
            {
                List<Priority> priorityList = db.Fetch<Priority>("Master_Priority_SelectAll");
                foreach (Priority p in priorityList)
                {
                    if (p.Name.Equals(priorityName))
                    {
                        priorityLevel = p.Level;
                        break;
                    }
                }
            }            

            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(command) || (priorityLevel == null))
            {
                return;
            }

            User user = SessionState.GetAuthorizedUser();            
            db.Execute("BatchProcessor_Insert", new object[] {               
                id, name, priorityLevel, command,
                user.Username, DateTime.Now                    
            });
            db.Close();
        }

        protected override void OnUpdatingDetailSearchResult(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailUpdate(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailDelete(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformBulkDetailDelete(List<string[]> selectedKeys)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailInsert(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }
    }
}
