﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Html;
using System.Web.Mvc;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Database;
using DevExpress.Web.Mvc;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Validation;
using Toyota.Common.Web.Messaging;
using Portal.Models.Sample;
using System.IO;
using System.Reflection;

namespace Portal.Controllers.Maintenance
{
    public class DummyMaintenanceController : MaintenanceController
    {
        private List<DummyModel> data;

        public DummyMaintenanceController()
            : base("Dummy Maintenance")
        {
            data = new List<DummyModel>();

            DummyModel dummyModel;
            FileStream imageStream = new FileStream(HttpRuntime.AppDomainAppPath + "\\Content\\Images\\Common\\Question-Mark.png", FileMode.Open);
            //Stream imageStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(HttpRuntime.AppDomainAppPath + "\\Content\\Images\\Common\\Question-Mark.png");
            MemoryStream memoryStream = new MemoryStream();
            imageStream.CopyTo(memoryStream);
            byte[] imageBytes = memoryStream.ToArray();
            memoryStream.Close();
            imageStream.Close();

            for (int i = 0; i < 30; i++)
            {
                dummyModel = new DummyModel()
                {
                    Name = "Dummy " + i                    
                };
                dummyModel.AttachmentPreview = imageBytes;
                data.Add(dummyModel);
            }
        }

        protected override void InitComponents()
        {            
            AddSearchParameter(new HtmlTextComponent("SearchKey", "Key", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchQualifier", "Qualifier", typeof(string)));

            SetDataGrid(new HtmlDataGridComponent("configTable", "Config Table", typeof(ConfigurationItem)));
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.KeyFieldName = "Key;Qualifier";            
            grid.Columns.Add(new HtmlTextComponent("Name", "Name", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlUploadComponent("AttachmentPreview", "Attachment", typeof(byte), Unit.Pixel(120)));
            grid.DataSource = data;
        }

        protected override void OnUpdatingSearchResult(Dictionary<string, string> requestParams)
        {
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.DataSource = data;
        }

        protected override void PerformUpdate(Dictionary<string, string> requestParams)
        {
            
        }

        protected override void PerformDelete(Dictionary<string, string> requestParams)
        {
            
        }

        protected override void PerformInsert(Dictionary<string, string> requestParams)
        {
            
        }

        protected override void PerformBulkDelete(List<string[]> selectedKeys)
        {
            
        }

        protected override void ValidateInput(InputValidation validation)
        {
            
        }

        protected override void OnUpdatingDetailSearchResult(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailUpdate(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailDelete(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformBulkDetailDelete(List<string[]> selectedKeys)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailInsert(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }
    }
}