﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Html;
using System.Web.Mvc;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Database;
using DevExpress.Web.Mvc;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Credential;

namespace Portal.Controllers.Maintenance
{
    public class ConfigurationMaintenanceController : MaintenanceController
    {
        public ConfigurationMaintenanceController()
            : base("Configuration Maintenance")
        {
        }

        protected override void InitComponents()
        {            
            AddSearchParameter(new HtmlTextComponent("SearchKey", "Key", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchQualifier", "Qualifier", typeof(string)));

            SetDataGrid(new HtmlDataGridComponent("configTable", "Config Table", typeof(ConfigurationItem)));
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.KeyFieldName = "Key;Qualifier";            
            grid.Columns.Add(new HtmlTextComponent("Key", "Key", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Qualifier", "Qualifier", typeof(string), Unit.Pixel(120)));
            grid.Columns.Add(new HtmlTextComponent("Value", "Value", typeof(string), Unit.Pixel(200)));
            grid.Columns.Add(new HtmlMemoComponent("TextValue", "Value (Text)", typeof(string), Unit.Pixel(200)) { Rows = 4 });
            grid.DataSource = ApplicationConfiguration.GetInstance().GetAll();
        }

        protected override void OnUpdatingSearchResult(Dictionary<string, string> requestParams)
        {
            string sKey = null;
            string sQualifier = null;
            if (requestParams.ContainsKey("SearchKey"))
            {
                sKey = requestParams["SearchKey"];
            }
            if (requestParams.ContainsKey("SearchQualifier"))
            {
                sQualifier = requestParams["SearchQualifier"];
            }

            IApplicationConfiguration appConfig = ApplicationConfiguration.GetInstance();
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            List<ConfigurationItem> result = new List<ConfigurationItem>();
            if (!string.IsNullOrEmpty(sKey) && !string.IsNullOrEmpty(sQualifier))
            {
                ConfigurationItem config = appConfig.Get(sKey.Trim(), sQualifier.Trim());
                result.Add(config);
            }
            else if (string.IsNullOrEmpty(sKey) && (string.IsNullOrEmpty(sQualifier)))
            {
                result = appConfig.GetAll();
            }
            else if (string.IsNullOrEmpty(sKey) && (!string.IsNullOrEmpty(sQualifier)))
            {
                result = appConfig.GetByQualifier(sQualifier);
            }
            else if (!string.IsNullOrEmpty(sKey) && (string.IsNullOrEmpty(sQualifier)))
            {
                result = appConfig.GetByKey(sKey);
            }

            if (result == null)
            {
                result = new List<ConfigurationItem>();
            }
            grid.DataSource = result;
        }

        protected override void PerformUpdate(Dictionary<string, string> requestParams)
        {
            string key = requestParams.ContainsKey("Key") ? requestParams["Key"] : null;
            string qualifier = requestParams.ContainsKey("Qualifier") ? requestParams["Qualifier"] : null;
            string value = requestParams.ContainsKey("Value") ? requestParams["Value"] : null;
            string textValue = requestParams.ContainsKey("TextValue") ? requestParams["TextValue"] : null;

            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(qualifier))
            {
                User user = SessionState.GetAuthorizedUser();
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("UpdateConfiguration", new object[] {               
                    value, 
                    textValue,
                    user.Username,
                    DateTime.Now,
                    key,
                    qualifier
                });
                db.Close();
            }
        }

        protected override void PerformDelete(Dictionary<string, string> requestParams)
        {
            string selectedKey = requestParams.ContainsKey("Key;Qualifier") ? requestParams["Key;Qualifier"] : null;
            if (!string.IsNullOrEmpty(selectedKey))
            {
                string[] keys = selectedKey.Split('|');
                if ((keys != null) && (keys.Length == 2))
                {
                    IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                    db.Execute("DeleteConfiguration", new object[] { keys[0], keys[1] });
                    db.Close();
                }
            }
        }

        protected override void PerformInsert(Dictionary<string, string> requestParams)
        {
            string key = requestParams.ContainsKey("Key") ? requestParams["Key"] : null;
            string qualifier = requestParams.ContainsKey("Qualifier") ? requestParams["Qualifier"] : null;
            string value = requestParams.ContainsKey("Value") ? requestParams["Value"] : null;
            string textValue = requestParams.ContainsKey("TextValue") ? requestParams["TextValue"] : null;

            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(qualifier))
            {
                return;
            }

            User user = SessionState.GetAuthorizedUser();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            db.Execute("InsertConfiguration", new object[] {               
                    key,
                    qualifier,
                    value, 
                    textValue,
                    user.Username,
                    DateTime.Now                    
                });
            db.Close();
        }

        protected override void PerformBulkDelete(List<string[]> selectedKeys)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);            
            foreach (string[] keys in selectedKeys)
            {
                db.Execute("DeleteConfiguration", new object[] { keys[0].Trim(), keys[1].Trim() });
            }
            db.Close();
        }

        protected override void OnUpdatingDetailSearchResult(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailUpdate(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailDelete(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformBulkDetailDelete(List<string[]> selectedKeys)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailInsert(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }
    }
}