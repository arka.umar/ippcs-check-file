﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Html;
using Portal.Models;
using System.Web.UI.WebControls;
using Toyota.Common.Web.Configuration;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Credential;


namespace Portal.Controllers
{
    public class NewsMaintenanceController : MaintenanceController
    {
        public NewsMaintenanceController(): base("News Maintenance")
        {
        }

        protected override void InitComponents()
        {
            AddSearchParameter(new HtmlTextComponent("SearchTitle", "Title", typeof(string)));
            AddSearchParameter(new HtmlTextComponent("SearchContent", "Content", typeof(string)));

            SetDataGrid(new HtmlDataGridComponent("News", "News Table", typeof(News)));
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.KeyFieldName = "Id";
            grid.Columns.Add(new HtmlTextComponent("Id", "Id", typeof(int), Unit.Pixel(250)));
            grid.Columns.Add(new HtmlTextComponent("Title", "Title", typeof(string), Unit.Pixel(250)));
            grid.Columns.Add(new HtmlMemoComponent("Content", "Content", typeof(string), Unit.Pixel(250)));
          
            grid.DataSource = GetNews();
            
        }

        protected override void OnUpdatingSearchResult(Dictionary<string, string> requestParams)
        {
            HtmlDataGridComponent grid = (HtmlDataGridComponent)GetDataGrid();
            grid.DataSource = GetNews();


        }

        protected override void PerformUpdate(Dictionary<string, string> requestParams)
        {
            string Id = requestParams.ContainsKey("Id") ? requestParams["Id"] : null;
            string Title = requestParams.ContainsKey("Title") ? requestParams["Title"] : null;
            string Content = requestParams.ContainsKey("Content") ? requestParams["Content"] : null;

            if (!string.IsNullOrEmpty(Title) && !string.IsNullOrEmpty(Content))
            {
                User user = SessionState.GetAuthorizedUser();
                IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
                db.Execute("UpdateNews", new object[] 
                {
                    Title, 
                    Content,
                    user.Username,
                    DateTime.Now,
                    Id
                });
                db.Close();
            }
        }

        protected override void PerformDelete(Dictionary<string, string> requestParams)
        {

          string selectedKey = requestParams.ContainsKey("Id") ? requestParams["Id"] : null;
          if (!string.IsNullOrEmpty(selectedKey))
          {
              IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
              db.Execute("DeleteNews", new object[] { selectedKey});
              db.Close();
          }
        }

        protected override void PerformBulkDelete(List<string[]> selectedKeys)
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            foreach (string[] keys in selectedKeys)
            {
                db.Execute("DeleteNews", new object[] {keys[0]});
            }
            db.Close();
        }

        protected override void PerformInsert(Dictionary<string, string> requestParams)
        {
            string Title = requestParams.ContainsKey("Title") ? requestParams["Title"] : null;
            string Content = requestParams.ContainsKey("Content") ? requestParams["Content"] : null;

            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(Content))
            {
                return;
            }

            User user = SessionState.GetAuthorizedUser();
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            db.Execute("InsertNews", new object[] {               
                    Title, 
                    Content,
                    user.Username,
                    DateTime.Now                    
                });
            db.Close();
        }

        protected List<News> GetNews()
        {
            IDBContext db = DatabaseManager.GetInstance().GetContext(DBContextNames.DB_PCS);
            List<News> newsList = new List<News>();
            var QueryNews = db.Query<News>("GetNews");
            if (QueryNews.Any())
            {
                foreach (var q in QueryNews)
                {
                    newsList.Add(new News()
                    {
                        Id = q.Id,
                        Content = q.Content,
                        Title = q.Title
                    });
                }
            }
            db.Close();
            return newsList;


     
        }

        protected override void OnUpdatingDetailSearchResult(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailUpdate(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailDelete(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }

        protected override void PerformBulkDetailDelete(List<string[]> selectedKeys)
        {
            throw new NotImplementedException();
        }

        protected override void PerformDetailInsert(Dictionary<string, string> requestParams)
        {
            throw new NotImplementedException();
        }
    }
}
