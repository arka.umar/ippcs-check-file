﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.ExcelUpload
{
    public class ErrorValidation
    {
        public string FunctionID { set; get; }
        public string ProcessID { set; get; }
        public string Field { set; get; }
        public string ErrorMessage { set; get; }
    }
}