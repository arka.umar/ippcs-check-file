﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Web.Excel;
using Toyota.Common.Web.Credential;

namespace Portal.ExcelUpload
{
    public class InsertTable
    {
        public static void Insert()
        {
            ArrayList tableDetail = new ArrayList();
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);

            TableDestination tableName = dbContext.SingleOrDefault<TableDestination>("GetUploadDestination", new object[] { "100" });
            var parameterData = dbContext.Query<ExcelReaderColumnName>("GetColumnNames", new object[] { tableName.tableFromTemp });
            string parameters = String.Empty;
            int SeqNumber = 1;
            int row = Convert.ToInt32(tableName.rowStart);


            //INSERT START PROCESS UPLOAD TO LOG Detail
            dbContext.Execute("InsertInformationUploadToLog", new object[] { 
                    tableName.processID,
                    SeqNumber,
                    "start",
                    "T",
                    "message start",
                    "Start Common Upload",
                    "niit.diko",
                    DateTime.Now,
                    "niit.diko ",
                    DateTime.Now
                });

            try
            {
                string client = ExcelReader.GetCell(tableName.filePath, tableName.sheetName, 5, 4);
                string PRType = ExcelReader.GetCell(tableName.filePath, tableName.sheetName, 6, 4);
                tableDetail = ExcelReader.ReaderExcel(tableName.filePath, tableName.sheetName, Convert.ToInt32(tableName.rowStart), Convert.ToInt32(tableName.columnStart), Convert.ToInt32(tableName.columnEnd));

                foreach (var par in parameterData)
                {
                    if (!String.IsNullOrEmpty(parameters))
                    {
                        parameters = parameters + "," + par.ColumnName;
                    }
                    else
                    {
                        parameters = par.ColumnName;
                    }
                }

                foreach (var value in tableDetail)
                {
                    string tempValue = "'" + row + "'" + "," + "'" + client + "'" + "," + "'" + PRType + "'" + value;

                    dbContext.Execute("InsertTempUpload", new object[] 
                    {
                        tableName.tableFromTemp,
                        parameters,
                        tempValue   
                    });
                    dbContext.Close();
                    row++;
                }
            }
            catch (Exception ex)
            {
                SeqNumber++;
                //INSERT FAIL UPLOAD TO LOG Detail
                dbContext.Execute("InsertInformationUploadToLog", new object[] { 
                    tableName.processID,
                    SeqNumber,
                    "Failed",
                    "T",
                    ex.Message,
                    "End Common Upload",
                    "niit.diko",
                    DateTime.Now,
                    "niit.diko",
                    DateTime.Now
                });
            }
            dbContext.Close();
            Validation(tableName.functionID, tableName.processID, tableName.tableFromTemp, tableName.tableToAccess, parameters);
        }

        public static void Validation(string functionID, string processID, string tableTemp, string tableDestination, string parameters)
        {
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            //seqNumber++;
            dbContext.Execute("ProcValidationExcel", new object[] { functionID, "100", new User().Username });
            //var error = dbContext.Query<ErrorValidation>("GetErrorMessageUpload");
            //dbContext.HardClose();

            //dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            //if (error == null)
            //{
            //    dbContext.ReplaceAndQuery("InsertTempToDestinationTableUpload", tableDestination, parameters, parameters, tableTemp);
            //    dbContext.HardClose();

            //    //INSERT SUCCESS TO LOG Detail
            //    //dbContext.Execute("InsertInformationUploadToLog", new object[] { 
            //    //    processID,
            //    //    seqNumber,
            //    //    "Succes",
            //    //    "T",
            //    //    "Success Message",
            //    //    "Common Upload",
            //    //    "niit.diko",
            //    //    DateTime.Now,
            //    //    "niit.diko ",
            //    //    DateTime.Now
            //    //});
            //}
            //else
            //{
            //   foreach (var listError in error)
            //    {
            //        //INSERT ERROR TO LOG Detail
            //        //dbContext.Close();
            //        dbContext.Execute("InsertInformationUploadToLog", new object[] {
            //            processID,
            //            seqNumber,
            //            "error",
            //            "T",
            //            listError.ErrorMessage,
            //            "Common Upload",
            //            "niit.diko",
            //            DateTime.Now,
            //            "niit.diko ",
            //            DateTime.Now
            //        });
            //        //dbContext.Close();
            //        seqNumber++;
            //    }
            //}

            dbContext.Close();
        }
    }
}