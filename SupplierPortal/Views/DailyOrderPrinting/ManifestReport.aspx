﻿<%--<%@ Import Namespace="Portal.Report.DailyOrder" %>--%>
 
<%@ Register assembly="Telerik.ReportViewer.WebForms, Version=6.1.12.820, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" namespace="Telerik.ReportViewer.WebForms" tagprefix="telerik" %>
 
<%@ Register assembly="Telerik.Reporting, Version=6.1.12.820, Culture=neutral, PublicKeyToken=a9d7983dfcc261be"
namespace="Telerik.Reporting" tagprefix="telerik" %>
<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>ManifestReport</title>
</head>
<body>
    <script runat="server">
        public override void VerifyRenderingInServerForm(Control control)
        {
            // to avoid the server form (<form runat="server"> requirement
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var instanceReportSource = new Telerik.Reporting.InstanceReportSource();
            instanceReportSource.ReportDocument = this.ViewBag.report;
            ReportViewer1.ReportSource = instanceReportSource;
        }
    </script>

    <form id="main" method="post" action="">
    <div style="visibility:collapse">
     <telerik:ReportViewer ID="ReportViewer1" Width="100%" Height="800px" runat="server">
     </telerik:ReportViewer>
     </div>

     <script type="text/javascript">
         //        <%=ReportViewer1.ClientID %>.PrintReport("Default");
         this.print(false);
    </script>
        
    </form>
</body>
</html>
