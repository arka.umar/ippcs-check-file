﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.MVC.Login;
using Toyota.Common.Web.Ioc;
using Portal.Provider;
using Toyota.Common.Web.Lookup;

namespace WebEntrance
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : MVCBaseApplication
    {
        protected override void StartUp()
        {
            Bootstrap.GetInstance().RegisterProvider<ILoginValidator>(typeof(LoginValidator), true);
            SystemState.GetInstance().EnableMenuAutoGeneration = false;
        }
    }
}