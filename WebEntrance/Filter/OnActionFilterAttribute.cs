﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Portal.Filter
{
    
    public class OnActionFilterAttribute : ActionFilterAttribute
    {
         
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (System.Web.HttpContext.Current.Session["HasLogin"] == null) return;
            if ((bool)System.Web.HttpContext.Current.Session["HasLogin"] == true)
            {
                if (string.Equals(System.Web.HttpContext.Current.Session["chkDefaultApplication"].ToString(), "false"))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Security", action = "Index" }));
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Security", action = "Index" }));
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                    System.Web.HttpContext.Current.Session["chkDefaultApplication"] = "false";
                }
                
            }
        }
          
    }
}