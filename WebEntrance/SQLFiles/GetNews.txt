﻿Select TOP 4
	 [NEWS_ID] as Id
	,[CREATED_DT] as Date
	,[NEWS_TITLE] as Title
	,[NEWS_BODY] as Content
	,[NEWS_PICTURE] as Picture
From TB_M_NEWS ORDER by CREATED_DT DESC