﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models
{
    //[Serializable]
    public class UserLoggedInfo
    {
        public string IPAddress { get; set; }
        public string Browser { get; set; }
        public string LoginTime { get; set; }
         
    }
}