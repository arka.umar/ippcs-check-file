﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models
{
    public class UserLogged
    {
        public List<UserLoggedInfo> Info { get; set; }
    }
}