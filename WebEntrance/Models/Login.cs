﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class Login
    {
        public string ID { set; get; }
        [Required(ErrorMessage = "Please enter your title")]
        //public string Title { set; get; }
        //[Required(ErrorMessage = "Please enter your content")]
        public string UserName { set; get; }
        [Required(ErrorMessage = "Please Enter Your User Name")]
        public string Password { set; get; }
        [Required(ErrorMessage = "Please Enter Your Password")]

        private DateTime createDate;
        public DateTime CreateDate
        {
            set
            {
                createDate = value;
            }
            get
            {
                return createDate;
            }
        }

        ////[Required(ErrorMessage = "Please enter submit the picture file")]
        //public string Picture { set; get; }

        //[Display(Name = "Local file")]
        ////public HttpPostedFileBase File { get; set; }

        //public bool IsFile { get; set; }

        //[Range(0, int.MaxValue)]
        //public int X { get; set; }

        //[Range(0, int.MaxValue)]
        //public int Y { get; set; }

        //[Range(1, int.MaxValue)]
        //public int Width { get; set; }

        //[Range(1, int.MaxValue)]
        //public int Height { get; set; }

        public Login()
        {
            createDate = DateTime.Now;
        }
    }
}