﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.News
{
    public class NewsModel
    {
        public List<News> NewsList { set; get; }

        public NewsModel()
        {
            NewsList = new List<News>();
        }
    }
}