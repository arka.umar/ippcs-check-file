﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models.News
{
    public class News
    {
        public string ID { set; get; }

        [Required(ErrorMessage = "Please enter your title")]
        public string Title { set; get; }

        [Required(ErrorMessage = "Please enter your content")]
        public string Content { set; get; }

        private DateTime createDate;
        
        public DateTime CreateDate
        {
            set
            {
                createDate = value;
            }
            get
            {
                return createDate;
            }
        }

        public string Picture { set; get; }

        [Display(Name = "Local file")]
        public HttpPostedFileBase File { get; set; }

        public bool IsFile { get; set; }

        [Range(0, int.MaxValue)]
        public int X { get; set; }

        [Range(0, int.MaxValue)]
        public int Y { get; set; }

        [Range(1, int.MaxValue)]
        public int Width { get; set; }

        [Range(1, int.MaxValue)]
        public int Height { get; set; }

        public News()
        {
            createDate = DateTime.Now;
        }
    }
}