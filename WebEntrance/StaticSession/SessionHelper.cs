﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.StaticSession
{
    public class SessionHelper
    {
        public static string getFlash(Controller Context, string Key, string DefaultValue = "") 
        {
            if (Context.Session[Key] != null)
            {
                string tmp = Context.Session[Key].ToString();
                Context.Session[Key] = null;
                return tmp;
            }

            return DefaultValue;
        }

        public static void setFlash(Controller Context, string Key, string Value)
        {
            Context.Session[Key] = Value;
        }
    }
}