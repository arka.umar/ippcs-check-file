﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Portal.Controllers;

namespace Portal.StaticSession
{
    /**
     *
     * This class responsible for maintaining Session on Web Entrance,
     * Which also responsible for Unlock or Logout or redirecting to another app/system.
     * 
     **/
    public class SessionManagement
    {
        public static string AuthorizationKey = "Auth_Cookie";
        public static string Authorization2Key = "Logged";
        public static string LoginFlagKey = "HasLogin";
        private static string username = "";
        private static string password = "";
        private static int login = 0;

        public static string GetEncryptedCredential(Controller Context)
        {
            if(IsLogged(Context))
                return Context.Session["Encrypted"].ToString();
            return "";
        }

        public static void Destroy(Controller Context)
        {
            Context.Session[LoginFlagKey] = false;
            HttpCookie c = new HttpCookie(AuthorizationKey);
            c.Expires = DateTime.Now.AddYears(-1);
            Context.Response.Cookies.Add(c);

            HttpCookie c2 = new HttpCookie(Authorization2Key);
            c2.Expires = DateTime.Now.AddYears(-1);
            Context.Response.Cookies.Add(c2);
        }

        public static bool IsSessionFound(Controller Context)
        { 
            if (Context.Session[LoginFlagKey] != null)
                return true;
            return false;
        }

        public static bool IsLocked(Controller Context)
        {
            if (IsLogged(Context) && Context.Request.Cookies[AuthorizationKey] == null)
                return true;
            
            return false;
        }

        public static bool IsLogged(Controller Context)
        {
            if(IsSessionFound(Context) && (bool)Context.Session[LoginFlagKey] == true)
                return true;

            return false;
        }

        public static void MarkAsUnlock(Controller Context)
        {           
            HttpCookie authCookie2 = new HttpCookie(Authorization2Key, "anom");
            authCookie2.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["CookieLifeSpan"]));
            Context.Response.Cookies.Add(authCookie2);

            if (Context.Request.Cookies[AuthorizationKey] == null)
            {
                HttpCookie authCookie = new HttpCookie(AuthorizationKey, "anom");
                authCookie.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["CookieLifeSpan"]));
                Context.Response.Cookies.Add(authCookie);
            }
            else
                Context.Response.Cookies.Get(AuthorizationKey).Expires = DateTime.Now.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["CookieLifeSpan"]));
        }

        public static void ExtendSession(Controller Context)
        {
            if (IsSessionFound(Context) && !IsLocked(Context))
            {
                Context.Response.Cookies.Get(AuthorizationKey).Expires = DateTime.Now.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["CookieLifeSpan"]));

                HttpCookie authCookie2 = new HttpCookie(Authorization2Key, "anom");
                authCookie2.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["CookieLifeSpan"]));
                Context.Response.Cookies.Add(authCookie2);

                Context.Session["CheckFlag"] = " ";
            }
        }

        public static void MarkAsLogged(Controller Context)
        {

        }

        public static void RedirectTo(ActionExecutingContext filterContext, string Controller, string Action = "Index")
        {
            if (!(filterContext.RouteData.Values["controller"].ToString().ToLower() == Controller.ToLower() &&
                filterContext.RouteData.Values["action"].ToString().ToLower() == Action.ToLower()))
            {
                try
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = Controller, action = Action }));
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                }
                catch // (HttpException ex) TODO: Implement a specific handler
                {
                }
            }
        }

        public static void RedirectToUrl(ActionExecutingContext filterContext, string Url)
        {
            filterContext.Result = new RedirectResult(Url);
            filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
        }

        public static void RedirectToDefaultPage(ActionExecutingContext filterContext)
        {
            RedirectTo(filterContext, "Welcome");
        }

        public static void RedirectToLoginPage(ActionExecutingContext filterContext)
        {
            RedirectTo(filterContext, "Login");
        }

        public static void RedirectToUnlockPage(ActionExecutingContext filterContext)
        {
            RedirectTo(filterContext, "Unlock");
        }

        public static void RedirectToSecurityPage(ActionExecutingContext filterContext)
        {
            RedirectTo(filterContext, "Security");
        }

        /**
         * 
         * This filter is used to provide redirecting to appropriate page based on the situation
         * on which a page has
         * 
         **/ 
        public static void Filter(Controller Context, ActionExecutingContext filterContext, string ReturnUrl="")
        {
            if (!IsSessionFound(Context))
            {
                SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
                //bool delUserLogged = sc.delUserLogged(username, password, username);
                sc.Close();
                RedirectToDefaultPage(filterContext);
            }
            else if (!IsLogged(Context))
            {
                RedirectToLoginPage(filterContext);
            }
            /* we disable first as requested by Ondi hohoho
            else if (IsLocked(Context))
            {
                if(Context.RouteData.Values["controller"].ToString().ToLower() != "unlock")
                    RedirectToUnlockPage(filterContext);
            }
            * */
            else
            {
                if (ReturnUrl.Trim() != "")
                {
                    RedirectToUrl(filterContext, ReturnUrl+"home?Logged=ada&btrck="+(string)Context.Session["Encrypted"]);
                }
                else
                    if (Context.RouteData.Values["controller"].ToString().ToLower() == "unlock")
                        RedirectToSecurityPage(filterContext);
            }

            ExtendSession(Context);
        }

        /**
         * 
         * We prepare for url which is used to redirecting from web entrance to another application/system.
         * 
         **/ 
        public static string PrepareUrl(Controller Context, string Url, string AdditionalParameter="")
        {
            if (Url != null)
            {
                //Prepare Additional Parameter
                if (AdditionalParameter.IndexOfAny(new char[] { '?', '+' }) >= 0)
                    AdditionalParameter = AdditionalParameter.Substring(1);
                
                // Prepare Url
                string tmpUrl = "btrck=" + GetEncryptedCredential(Context);
                string[] tmps = new string[] {tmpUrl, AdditionalParameter};
                foreach (string s in tmps)
                {
                    if (s!="" && !Url.Contains(s))
                    {
                        if (Url.IndexOf('?') > 0)
                            Url += "&";
                        else
                            Url += "?";

                        Url = Url + s;
                    }
                }
                
                return Url;
            }

            return Url;
        }

        public static void setUserPass(string user, string pass)
        {
            username = user;
            password = pass;
        }

        public static void setLogin()
        {
            login = 1;
        }

        public static int getLogin()
        {
            return login;
        }
    }
}