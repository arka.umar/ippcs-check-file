$(document).ready(function(){
	
	$('#pg').jphotogrid({
		baseCSS: {
			width: '230px',
			height: '140px',
			padding: '0px'
		},
		selectedCSS: {
			top: '50px',
			left: '100px',
			width: '520px',
			height: '380px',
			padding: '10px'
		}
	});
		
});