
function openPopupDialog(prospectElementID, overlayID, positionLeft) {
    if (arguments.length == 2) {
        $('#' + overlayID).fadeIn('fast', function () {
            $('#boxpopup').css('display', 'block');
            $('#boxpopup').animate({ 'left': '8%' }, 500);
            $('#' + prospectElementID).css('display', 'block');
            $('#' + prospectElementID).animate({ 'left': '8%' }, 500);
        });
    } else if (arguments.length == 3) {
        $('#' + overlayID).fadeIn('fast', function () {
            $('#boxpopup').css('display', 'block');
            $('#boxpopup').animate({ 'left': '8%' }, 500);
            $('#' + prospectElementID).css('display', 'block');
            $('#' + prospectElementID).animate({ 'left': positionLeft }, 500);
        });
    }
}

function closeOffersDialog(prospectElementID, popupID, overlayID) {
    $(function ($) {
        $(document).ready(function () {
            $('#' + prospectElementID).css('position', 'absolute');
            $('#' + prospectElementID).animate({ 'left': '-100%' }, 500, function () {
                $('#' + prospectElementID).css('position', 'fixed');
                $('#' + prospectElementID).css('left', '100%');
                $('#' + overlayID).fadeOut('fast');
            });
            $('#' + popupID).css('display', 'none');
        });
    });
}

function openOffersDialog() {
    $('#overlay').fadeIn('fast', function () {
        $('#boxpopup').css('display', 'block');
        $('#boxpopup').animate({ 'left': '8%' }, 500);
        $('#PopLegacy').css('display', 'block');
        $('#PopLegacy').animate({ 'left': '10%' }, 500);
        //
        $('#PopContactUs').css('display', 'block');
        $('#PopContactUs').animate({ 'left': '8%' }, 500);
        //
        $('#PopRegis').css('display', 'block');
        $('#PopRegis').animate({ 'left': '20%' }, 500);
        //
        $('#PopPortFolio').css('display', 'block');
        $('#PopPortFolio').animate({ 'left': '20%' }, 500);
    });
}