﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using System.Web.Routing;
using Portal.Filter;

namespace Portal.Controllers
{
    //[OnActionFilter]
    public class HomeController : FrontController
    {

        public HomeController()
        {
            LoginControllerName = "Welcome";
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //This session is to handle request from FrontEnd
            StaticSession.SessionHelper.setFlash(this, "FrontEndURL", StaticSession.SessionManagement.PrepareUrl(this, Request.QueryString["URL"]));

            if (Session["HasLogin"] == null) return;
            StaticSession.SessionManagement.Filter(this, filterContext,Request.QueryString["URL"] == null ? "" : Request.QueryString["URL"]);
        }
          
        protected override void StartUp()
        {
            //Session["HasLogin"] = false;
            Session["ErrorLogin"] = "";
        }
    }
}
