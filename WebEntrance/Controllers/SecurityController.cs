﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using System.Configuration;
using System.Reflection;
using System.Web.Routing;
using System.Text;
using Portal.Filter;
using Toyota.Common.Web.Credential;
using Newtonsoft.Json;

namespace Portal.Controllers
{

    public class SecurityController : BaseController
    {
        public SecurityController()
            : base("TMMIN Web Portal")
        {
            CheckSessionState = true;
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            //We disable check for logged user info due to openLDAP problem.
            /*
            string username = (string)Session["username"];
            string password = (string)Session["password"];
            TempData["username"] = username;
            TempData["password"] = password;
            if (string.IsNullOrEmpty(username))
            {
                TempData["LoginInvalid"] = 1;  
            }
            else
            {
                SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
                string UserLogged = sc.getUserLogged(username);
                sc.Close();
                bool notFound = false;
                if (!string.IsNullOrEmpty(UserLogged))
                {
                    List<UserAD> userADUserLogged = JsonConvert.DeserializeObject<List<UserAD>>(UserLogged);
                    foreach (UserAD uad in userADUserLogged)
                    {
                        if ((uad.properties["sessionid"][0] == HttpContext.Session.SessionID) &&
                          uad.properties["ipaddress"][0] == HttpContext.Request.UserHostAddress)
                        {
                            StaticSession.SessionManagement.setLogin();
                            notFound = true;
                            break;
                        }
                    }
                    if (!notFound)
                    {
                        TempData["LoginNotValid"] = "LoginNotValid();";
                        TempData["LoginInvalid"] = 1;
                        if (!filterContext.HttpContext.Request.IsAjaxRequest())
                        {
                            filterContext.Result = new RedirectToRouteResult(new
                                                                RouteValueDictionary(new { controller = "Login" }));
                            filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                        }
                        LogoutPC();
                    }
                }
                if (string.IsNullOrEmpty(UserLogged) && StaticSession.SessionManagement.getLogin() == 1)
                {

                    TempData["LoginNotValid"] = "LoginNotValid();";
                    TempData["LoginInvalid"] = 1;
                    if (!filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.Result = new RedirectToRouteResult(new
                                                            RouteValueDictionary(new { controller = "Login" }));
                        filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                    }
                    LogoutPC();
                }
            }
             * */
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (TempData["LoginInvalid"] != null)
            {
                if ((int)TempData["LoginInvalid"] == 1) StaticSession.SessionManagement.Filter(this, filterContext);
            }

            if (Session["username"] != null)
            {
                StaticSession.SessionManagement.setUserPass( (string)Session["username"],  (string)Session["password"]);
            }
        }

        protected override void StartUp()
        {
        }

        [Obsolete]
        protected override void Init()
        {

        }

        public PartialViewResult Security()
        {
            return PartialView("Security");
        }

        public ActionResult Logout()
        {
            string result = LogoutFunc();

            return RedirectToAction("Index", "Login");
        }

        public ActionResult redirectToDefApp()
        {
            return RedirectToAction("IPPCS", "Security");
        }

        public ActionResult DeleteSessionLandingURL()
        {
            if (Session["AuthorizedLandingUrl"] != null)
            {
                Session["AuthorizedLandingUrl"] = null;
                return Content("1") ;
            }
            return Content("0");
        }        

        public string LogoutPC()
        {
            string username = (string)Session["username"];
            string password = (string)Session["password"];
            if (username != null && password != null)
            {
                TempData["AuthorizedUser"] = null;
                StaticSession.SessionManagement.Destroy(this);
                Session.Clear();
            }
            return "/Login/Index";
        }

        public string LogoutFunc()
        {
            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            string username = (string)Session["username"];
            string password = (string)Session["password"];
            if (username != null && password != null)
            { 
                //bool delUserLogged = sc.delUserLogged(username, password, username);
                TempData["AuthorizedUser"] = null;
                StaticSession.SessionManagement.Destroy(this);
                Session.Clear();
            } 
            sc.Close(); 
            return "/Login/Index";
        }

        public string initDropList()
        {
            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            if (Session["username"] != null)
            { 
                string defaultAppName = sc.getDefaultApp(Session["username"].ToString(), Session["password"].ToString());
                if (!String.IsNullOrEmpty(defaultAppName) || String.Compare(defaultAppName, "NULL") != 1)
                    return defaultAppName;
                else
                    return "------------- Choose your default application -------------";
                sc.Close();
            } 
            return "";
        }

        public ActionResult UpdateDefaultApp(string userName, string defApplication)
        {
            if (defApplication.CompareTo("------------- Choose your default application -------------") == 1)
            {
                SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
                String username = Session["username"].ToString();
                String password = Session["password"].ToString();

                if (sc.UpdateDefaultApp(username, password, defApplication))
                {
                    sc.Close();
                    return RedirectToAction("Index", "Security");
                }
                else
                {
                    TempData["ErrorMessage"] = "Error while updating Data";

                    sc.Close();
                    return RedirectToAction("Index", "Security");
                }

                //sc.Dispose();
            }
            else
            {
                TempData["ErrorMessage"] = "Error while updating Data";
                return RedirectToAction("Index", "Security");
            }
        }



        public ActionResult GDE()
        {
            string link = ConfigurationManager.AppSettings["GDE"];
            return Redirect(link);
        }

        public ActionResult Elvis()
        {
            string link = ConfigurationManager.AppSettings["Elvis"];
            return Redirect(link);
        }

        //Remarked By   : Rahmat
        //Remarked Date : 2014-11-20
        //Desc          : submit ANYS, spot disubmit dengan bug id baru

        public ActionResult SplitMenu() {
        //    #region
        //    //Created By    : Yudha Ari
        //    //Created Dt    : 2013-12-20
        //    //Description   : Switch menu for get in to IPPCS or SPOT
        //    #endregion
            return RedirectToAction("index", "SplitMenu");
        }

        //public ActionResult IPPCS()
        //{
        //    string link = string.Format(ConfigurationManager.AppSettings["IPPCS"] + "home?btrck={0}", (string)Session["Encrypted"]);//, (string)Session["ParamPassword"], "IPPCS");
        //    return Redirect(link);
        //}

        public ActionResult TMS()
        {
            string link = string.Format(ConfigurationManager.AppSettings["TMS"] + "home?btrck={0}", (string)Session["Encrypted"]);
            return Redirect(link);
        }

        public ActionResult DownloadTools()
        {
            string link = string.Format(ConfigurationManager.AppSettings["DownloadTools"] + "home?btrck={0}", (string)Session["Encrypted"]);
            return Redirect(link);
        }
        //public ActionResult ANY()
        //{
        //    string link = string.Format(ConfigurationManager.AppSettings["ANYS"] + "Login/login?btrck={0}", (string)Session["Encrypted"]);
        //    return Redirect(link);
        //}
    }
}
