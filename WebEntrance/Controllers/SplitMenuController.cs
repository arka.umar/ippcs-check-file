﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using System.Configuration;
using System.Reflection;
using System.Web.Routing;
using System.Text;
using Portal.Filter;
using Toyota.Common.Web.Credential;
using Newtonsoft.Json;

#region
//Created By    : Yudha Ari
//Created Dt    : 2013-12-20
//Description   : Switch menu for get in to IPPCS or SPOT
#endregion

namespace Portal.Controllers
{
    public class SplitMenuController : BaseController
    {
        //
        // GET: /SplitMenu/
        public SplitMenuController()
            : base("TMMIN Web Portal")
        {
            CheckSessionState = true;
        }


        protected override void StartUp()
        {
        }

        [Obsolete]
        protected override void Init()
        {

        }

        //public override ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult IPPCS()
        {
            string link = string.Format(ConfigurationManager.AppSettings["IPPCS"] + "home?btrck={0}", (string)Session["Encrypted"]);//, (string)Session["ParamPassword"], "IPPCS");
            return Redirect(link);
        }

        public ActionResult ServicePart()
        {
            string link = string.Format(ConfigurationManager.AppSettings["SPOT"] + "home?btrck={0}", (string)Session["Encrypted"]);//, (string)Session["ParamPassword"], "IPPCS");
            return Redirect(link);
        }

    }
}
