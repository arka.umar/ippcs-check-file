﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;
using Portal.Filter;

namespace Portal.Controllers
{
    public class UnlockController : Controller
    {
        //
        // GET: /Unlock/
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            StaticSession.SessionManagement.Filter(this, filterContext,Request.Form["ReturnUrl"] == null ? "" : Request.Form["ReturnUrl"].ToString());
        }

        public ActionResult Index()
        {
            ViewBag.ReturnUrl = StaticSession.SessionHelper.getFlash(this, "FrontEndURL", null);
            return View();
        }

        [HttpPost]
        public ActionResult UnlockApp(string Username, string Password, string ReturnUrl = "")
        {
            if (!(Session["username"].Equals(Username) && Session["password"].Equals(Password)))
            {
                Session.Add("ErrorMessage", "Incorrect password.");
                return View("Index");
            }

            StaticSession.SessionManagement.MarkAsUnlock(this);

            if (ReturnUrl.Trim() == "")
            {
                return Redirect(Url.Action("Index", "Security"));
            }

            return Redirect(StaticSession.SessionManagement.PrepareUrl(this, ReturnUrl, "home?btrck="+ (string)Session["Encrypted"]));
        }
    }
}
