﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC.Login;
using Portal.Models;
using Toyota.Common.Web.Credential;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;
using System.Text;
using Toyota.Common.Web.Ioc;
using System.Configuration;
using Portal.SC;
using Portal.Filter;
using System.Web.Routing;
using System.Web.UI;

namespace Portal.Controllers
{
    [OnActionFilter]
    public class LoginController : BaseLoginController
    {
        
        public LoginController() {
            AuthorizedLandingControllerName = "Security";
            LogoutLandingControllerName = "Security";
        }
       

        protected override void StartUp()
        {
           
        }

        [Obsolete]
        protected override void Init()
        { 
        }


        public ActionResult ForgotPassword()
        {
            return View();
        }

        public ActionResult OnLoadGrid()
        {
            TempData["UserLoggedInfoLoginFirst"] = TempData["UserLoggedInfo"];
            return PartialView("UserLoggedPartial", TempData["UserLoggedInfo"]);
        }

        private string SerializeUserLogged(string Username, string Duration = "0")
        {
            UserAD ex = new UserAD(); 
            ex.properties.Add("ipaddress", new List<string>() { HttpContext.Request.UserHostAddress });
            ex.properties.Add("browser", new List<string>() { HttpContext.Request.Browser.Browser });
            ex.properties.Add("sessionid", new List<string>() { HttpContext.Session.SessionID });
            ex.properties.Add("sc", new List<string>() { ConfigurationManager.AppSettings["SC"] });
            ex.properties.Add("logintime", new List<string>() { DateTime.Now.ToString() });
            ex.properties.Add("duration", new List<string>() { Duration });
            ex.properties.Add("username", new List<string>() { Username });
            return JsonConvert.SerializeObject(ex);
        }

        public ActionResult MultipleLoginFirst(string Username, string Password)
        {
            /*
            UserAD ex = new UserAD();
            List<UserLoggedInfo> list = (List<UserLoggedInfo>)TempData["UserLoggedInfoLoginFirst"];
            UserLoggedInfo uli = list[0];
            TimeSpan duration = DateTime.Now - Convert.ToDateTime(uli.LoginTime); 
            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            bool ret = sc.chUserLogged(Username, Password, SerializeUserLogged(Username, Math.Round(duration.TotalMinutes).ToString()));
            sc.Close();
            **/
            SubmitLogin(Username, Password);
            return Content(""); 
        }

        public ActionResult MultipleLoginSecondEnding(string Username, string Password)
        {
            /*
            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            bool delUserLogged = sc.delUserLogged(Username, Password, Username); 
            bool createUserLogged = sc.crUserLogged(Username, Password, SerializeUserLogged(Username));
            sc.Close();
            **/
            SubmitLogin(Username, Password); 
            return Content("");
        }

        public ActionResult MultipleLoginSecond(string Username, string Password)
        { 
            /*
            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            bool createUserLogged = sc.crUserLogged(Username, Password, SerializeUserLogged(Username));
            sc.Close();
            **/
            SubmitLogin(Username, Password);
            return Content("");
        }

        public ActionResult OnValidating(string username, string password, string chkdefaultApp)
        {
            //This function has been modified to disable multiple login due to OpenLDAP problem.
            Session["chkDefaultApplication"] = chkdefaultApp;
            String user = Request["Username"];
            String pwd = Request["Password"];
            //SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            //string UserLogged = sc.getUserLogged(username);
            //List<UserAD> userADUserLogged = JsonConvert.DeserializeObject<List<UserAD>>(UserLogged);
            //if (string.IsNullOrEmpty(UserLogged))
            //{
            //    sc.crUserLogged(username, password, SerializeUserLogged(username)); 
            //    sc.Close();
                return Content("-1");
            //}
            /*
            else
            {
                sc.Close();
                ViewBag.User = userADUserLogged[0].properties["username"];
                List<UserLoggedInfo> listInfo = new List<UserLoggedInfo>();
                foreach (UserAD uad in userADUserLogged)
                { 
                    listInfo.Add(new UserLoggedInfo()
                    {
                        LoginTime = uad.properties["logintime"][0],
                        Browser = uad.properties["browser"][0],
                        IPAddress = uad.properties["ipaddress"][0]
                    });
                }
                TempData["UserLoggedInfo"] = listInfo; 
                if (userADUserLogged[0].properties["multiplelogin"][0] == "1")
                    return Content("1");
                else
                {
                    return Content("0");
                }
            }
             * */
        }

        protected override void PrepareLogin()
        {
            string chkDefaultApplication = Session["chkDefaultApplication"].ToString();
            String username = Request["Username"];
            String password = Request["Password"];
            Session["username"] = username;
            Session["password"] = password;
            if (!string.IsNullOrEmpty(chkDefaultApplication) && (chkDefaultApplication.Equals("true")))
            {
                SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
                string defaultAppName = sc.getDefaultApp(username, password);
                if (!string.IsNullOrEmpty(defaultAppName))
                {
                    string url = string.Format(ConfigurationManager.AppSettings[defaultAppName] + "home?btrck={0}", (string)Session["Encrypted"]);//, (string)Session["ParamPassword"], "IPPCS");
                    Session["AuthorizedLandingUrl"] = url;
                }
                Session["chkDefaultApplication"] = false;
                sc.Close();
            }
            Session.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["SessionLifeSpan"]);
            StaticSession.SessionManagement.MarkAsUnlock(this);
        }

        public ActionResult UserLoggedBindingPartial1()
        {
            return View();
        }

        public ActionResult UserLoggedBindingPartial0()
        {
            return View();
        }

        public ActionResult ForgotPasswordAction(string usernameOrpass)
        {
            SC.SecurityCenterClient s = new SC.SecurityCenterClient();

            string newPass = s.genNewPass("", "", "");

            s.Close();
            return View("Index");
        }

        public ActionResult ForgotUsername()
        {            
            return View();
        }

        public ActionResult ForgotUsernameAction(string usernameOrpass)
        {
            SC.SecurityCenterClient s = new SC.SecurityCenterClient();

            string newUsername = s.genNewUser("", "", "");

            s.Close();
            return View("Index");
        }

        public string CheckIfPasswordMustBeChanged(string username, string password)
        {
            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            bool passwordMustBeChanged = sc.IsPasswordMustBeChanged(username, password);
            sc.Close();

            return Convert.ToString(passwordMustBeChanged).ToLower();
        }

        public string PerformChangePassword(string username, string password, string newPassword) 
        {
            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            string feedback = sc.ChangePassword(username, password, newPassword);
            sc.Close();

            return feedback;
        }

        public ActionResult ChangePassword()
        {
            return PartialView("ChangePasswordPartial");
        }
    }
}
