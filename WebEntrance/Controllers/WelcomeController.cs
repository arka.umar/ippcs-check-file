﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.MVC;
using Toyota.Common.Web.Database;
using Portal.Models.News;
using System.Web.Routing;
using Portal.Filter;

namespace Portal.Controllers
{
    [OnActionFilter]
    public class WelcomeController : BaseController
    {
        public WelcomeController()
            : base("TMMIN Web Portal")
        {
            CheckSessionState = false;
        }

        protected override void StartUp()
        {
           
        }

        [Obsolete]
        protected override void Init()
        {
            NewsModel mdl = new NewsModel();

            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_WEB_PORTAL);
            IEnumerable<News> newslist = dbContext.Query<News>("GetNews");
            mdl.NewsList = newslist.ToList<News>();

            Model.AddModel(mdl);    
        }

        public PartialViewResult NewsDetail(string ID)
        {
            IDBContextManager dbManager = DatabaseManager.GetInstance();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_WEB_PORTAL);
            dbContext = dbManager.GetContext(DBContextNames.DB_WEB_PORTAL);
            News list = dbContext.SingleOrDefault<News>("GetDetailNews", new object[] { ID });

            return PartialView("NewsDetail", list);            
        }

        public PartialViewResult AboutUs()
        {
            return PartialView("AboutUs");
        }

        public PartialViewResult ToyotaWays()
        {
            return PartialView("ToyotaWays");
        }
    }
}
