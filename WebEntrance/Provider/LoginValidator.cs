﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.MVC.Login;
using Toyota.Common.Web.Credential;
using System.IO;
using System.Reflection;
using System.Text;
using System.Runtime.Remoting;
using Toyota.Common.Web.Util;
using Portal.SC;
using System.Configuration;
using Toyota.Common.Web.Util.Converter;
using Cryptography;
using Toyota.Common.Util.Text;

namespace Portal.Provider
{
    public class LoginValidator : ILoginValidator
    {
        public User Validate(HttpSessionStateBase session, HttpRequestBase request, IDictionary<string, object> parameters)
        {

            SC.SecurityCenterClient sc = new SC.SecurityCenterClient();
            Crypter encrypter = new Crypter(new ConfigKeySym());

            string username = null;
            string password =  null;
            if (parameters.ContainsKey("Username"))
            {
                username = (string)parameters["Username"];
            }
            if (parameters.ContainsKey("Password"))
            {
                password = (string)parameters["Password"];
            }

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                if (sc.Login(username, password))
                {
                    string userNameEnrypted;
                    string passwordEnrypted;

                    //We got the user in order to show the full name on Welcome in security context
                    Dictionary<string, List<string>> userTemp = JSON.ToObject<Dictionary<string, List<string>>>(sc.getUserInfo(username, password));
                    User TMMINuser = Toyota.Common.Web.Credential.User.Cast<User>(userTemp);

                    session["ParamUn"] = TMMINuser.FullName;
                    userNameEnrypted = encrypter.Encrypt(username);
                    passwordEnrypted = encrypter.Encrypt(password);
                    string codeWebEntrance = encrypter.Encrypt("SXdkcvXvFhtjdUhSD6NALuwY");
                    session["Encrypted"] = codeWebEntrance + "_" + userNameEnrypted + "_" + passwordEnrypted;// + "_" + Convert.ToString(ConfigurationManager.AppSettings["SessionLifeSpan"]);
                    session["ParamPassword"] = passwordEnrypted;
                    session["HasLogin"] = true;
                    session["ErrorLogin"] = "";
                    //session["ChkDefault"] = (string)parameters["ChkDefault"];

                    sc.Close();
                    return TMMINuser;
                    ////return new User() { Username = username };
                }
                else
                {
                    session["ErrorLogin"] = "Incorrect Username or Password";
                }

            }

            sc.Close();
            return null;
        }

        internal bool ReleaseHandleStrongNameKey()
        {
            bool close = false;
            IEnumerator<Dictionary<IntPtr, FileInfo>> list = VmcController.Services.DetectOpenFiles.GetOpenFilesEnumerator(System.Diagnostics.Process.GetCurrentProcess().Id);
            while (list.MoveNext())
            {
                var ret = list.Current.Values.Where(c => c.FullName.Contains("scenter.snk"));
                if (ret.Any() && ret != null)
                {
                    close = VmcController.Services.NativeMethods.CloseHandle(list.Current.Keys.ToList()[0]);
                    break;
                }
            }
            list.Dispose();
            return close;
        }

        private object Cast(Type keyType, List<string> tes)
        {
            ObjectHandle oList = (ObjectHandle)Activator.CreateInstance("mscorlib", "System.Collections.Generic.List`1[[" + keyType.FullName + ", " + keyType.AssemblyQualifiedName.Split(',')[1] + "]]");
            object listAuthorizationDetail = oList.Unwrap();

            for (int i = 0; i < tes.Count; i++)
            {
                string[] listDetails = tes[i].Split(',');
                object o = Activator.CreateInstance(Assembly.GetExecutingAssembly().FullName, keyType.FullName);
                object oDetail = ((ObjectHandle)o).Unwrap();
                Type detailType = ((ObjectHandle)o).Unwrap().GetType();

                foreach (string value in listDetails)
                {
                    string[] listValues = value.Split('=');
                    PropertyInfo piDetail = detailType.GetProperty(listValues[0], BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.IgnoreCase);
                    if (piDetail != null) piDetail.SetValue(oDetail, listValues[1], null);
                }
                listAuthorizationDetail.GetType().InvokeMember("Add", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.Public, null, listAuthorizationDetail, new object[] { oDetail });
            }
            return listAuthorizationDetail;
        }
    }
}