USE [IPPCS_QA]
GO
/****** Object:  StoredProcedure [dbo].[SP_PackingIndicatorMasterkUploadValidate]    Script Date: 12/10/2020 11:17:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		wot.agunga (agungpanduan.com)
-- Create date: 2020-10-06
-- Description:	validation when uploading master build out 
-- =============================================
CREATE PROCEDURE [dbo].[SP_PackingIndicatorMasterkUploadValidate]
	@process_id varchar(20)
	--@message varchar(2000) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @continue char(1) = 'Y';
	DECLARE @functionId CHAR(5) = '71016';
	DECLARE @InValidCOunt int= 0;
	DECLARE @message varchar(2000)

	CREATE TABLE #duplicate (
	        [ID] int identity(1,1),
			[DOCK_CD] [varchar](6) NULL,
			[VALID_DT_FR] [varchar](10) NULL,
			[VALID_DT_TO] [varchar](10) NULL,
			[PACKING_TYPE_CD] [varchar](1) NULL,
			[PART_COLOR_SFX] [varchar](2) NULL,
			[MAT_NO] [varchar](23) NULL,
			[PROD_PURPOSE_CD] [varchar](5) NULL,
			[SOURCE_TYPE] [varchar](1) NULL,
			[CREATED_BY] [varchar](20) NULL
		)

	IF(EXISTS (SELECT '' FROM [TB_T_PACKING_INDICATOR_TEMP] P WHERE P.PROCESS_ID = @process_id))
	BEGIN

		--== VALIDATE DATA ==--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, T.PROCESS_ID, T.ID, 'DOCK_CD, VALID_DT_FR, VALID_DT_TO, PACKING_TYPE_CD, PART_COLOR_SFX, MAT_NO PROD_PURPOSE_CD, and SOURCE_TYPE cant be null' from TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.DOCK_CD, '') = ''
		    AND ISNULL(T.VALID_DT_FR,'') =''
			AND ISNULL(T.VALID_DT_TO,'') =''
			AND ISNULL(T.PACKING_TYPE_CD, '') = '' 
			AND ISNULL(T.PART_COLOR_SFX, '') = ''
			AND ISNULL(T.MAT_NO, '') = ''
			AND ISNULL(T.PROD_PURPOSE_CD, '') = ''
			AND ISNULL(T.SOURCE_TYPE, '') = ''
			AND ISNULL(T.CREATED_BY, '') = ''
			AND T.PROCESS_ID = @process_id

		--validate DOCK_CD null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.DOCK_CD, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)
	
		--validate VALID_DT_FR null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.VALID_DT_FR, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)
		
		--validate VALID_DT_TO null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.VALID_DT_TO, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)

		--validate PACKING_TYPE_CD null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.PACKING_TYPE_CD, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)

		--validate PART_COLOR_SFX null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.PART_COLOR_SFX, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)

		--validate MAT_NO null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.MAT_NO, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)

		--validate PROD_PURPOSE_CD null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.PROD_PURPOSE_CD, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)
		
		--validate SOURCE_TYPE null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.SOURCE_TYPE, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)
		
		--validate CREATED_BY null value--
		INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
		SELECT @functionId, PROCESS_ID, ID, 'Column DOCK_CD cant be null' FROM TB_T_PACKING_INDICATOR_TEMP T
		WHERE ISNULL(T.CREATED_BY, '') = '' AND T.PROCESS_ID = @process_id
			AND NOT EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE X WHERE X.ID = T.ID AND X.PROCESS_ID = @process_id)

     	IF(EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE WHERE FUNCTION_ID = @functionId AND PROCESS_ID = @process_id))
		BEGIN
		    SET @message =(SELECT MSG FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE V WHERE V.PROCESS_ID = @process_id AND V.FUNCTION_ID = @functionId)
			SET @continue = 'N';
		END
	
		IF(@continue = 'Y')
		BEGIN
			--validate VALID_DT_FR cannot be greater than VALID_DT_TO
			SET @InValidCOunt = (select count(1) FROM TB_T_PACKING_INDICATOR_TEMP T
									WHERE (ISDATE(CONVERT(CHAR(10),CONVERT(DATETIME,LEFT(VALID_DT_FR,10),105),101))!=1  OR ISDATE(CONVERT(CHAR(10),CONVERT(DATETIME,LEFT(VALID_DT_TO,10),105),101))!=1)
									AND T.PROCESS_ID = @process_id)
			
			IF @InValidCOunt > 0 
			BEGIN
				INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
				SELECT @functionId, PROCESS_ID, ID, 'Invalid Data Type VALID_DT_FR' FROM TB_T_PACKING_INDICATOR_TEMP T
				WHERE ISDATE(VALID_DT_FR)!=1 
					AND T.PROCESS_ID = @process_id

				IF(EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE WHERE FUNCTION_ID = @functionId AND PROCESS_ID = @process_id))
				BEGIN
					SET @continue = 'N';
					SET @message ='Invalid Data Type VALID_DT_FR'
				END
			END
			ELSE
			BEGIN
				SET @InValidCOunt = (select count(1) from TB_T_PACKING_INDICATOR_TEMP 
										WHERE CONVERT(CHAR(10),CONVERT(DATETIME,LEFT(VALID_DT_FR,10),105),101) > CONVERT(CHAR(10),CONVERT(DATETIME,LEFT(VALID_DT_TO,10),105),101)
										AND PROCESS_ID = @process_id)
				IF @InValidCOunt > 0 
				BEGIN
					INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE
					SELECT @functionId, PROCESS_ID, ID, 'VALID_DT_FR cannot be greater than VALID_DT_TO' FROM TB_T_PACKING_INDICATOR_TEMP T
					WHERE CONVERT(CHAR(10),CONVERT(DATETIME,LEFT(VALID_DT_FR,10),105),101) > CONVERT(CHAR(10),CONVERT(DATETIME,LEFT(VALID_DT_TO,10),105),101)
						AND T.PROCESS_ID = @process_id

					IF(EXISTS (SELECT '' FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE WHERE FUNCTION_ID = @functionId AND PROCESS_ID = @process_id))
					BEGIN
						SET @continue = 'N';
						SET @message ='VALID_DT_FR cannot be greater than VALID_DT_TO'
					END
				END
			END
		END

		IF(@continue = 'Y')
		BEGIN
			INSERT INTO #duplicate
			SELECT 
				MAX(A.[DOCK_CD]),
				MAX(A.[VALID_DT_FR]),
				MAX(A.[VALID_DT_TO]),
				MAX(A.[PACKING_TYPE_CD]),
				MAX(A.[PART_COLOR_SFX]),
				A.[MAT_NO],
				MAX(A.[PROD_PURPOSE_CD]),
				MAX(A.[SOURCE_TYPE]),
				MAX(A.[CREATED_BY]) 
			FROM TB_T_PACKING_INDICATOR_TEMP A INNER JOIN TB_T_PACKING_INDICATOR_TEMP B
				ON A.MAT_NO = B.MAT_NO
				--AND A.DOCK_CD = B.DOCK_CD AND A.PROD_PURPOSE_CD= B.PROD_PURPOSE_CD 
				--AND A.SOURCE_TYPE= B.SOURCE_TYPE AND AND A.PART_COLOR_SFX= B.PART_COLOR_SFX
			--WHERE A.PROCESS_ID = @process_id
			GROUP BY A.MAT_NO
			HAVING COUNT(1) > 1

			IF(EXISTS(SELECT MAT_NO FROM #duplicate))
			BEGIN
			    PRINT 'insert duplicate'
				SET @continue = 'N';
				SET @message ='This data is duplicate, please delete this row'
				--concate duplicate ID
				INSERT INTO TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE(FUNCTION_ID, PROCESS_ID, MSG, ID)
				SELECT @functionId, @process_id, 'This data is duplicate, Please delete one of the MAT_NO: ' + B.MAT_NO,
				B.ID AS ID
				From #duplicate B
				--WHERE B.ID != (SELECT MAX(ID) FROM #duplicate A 
								--WHERE A.MAT_NO = B.MAT_NO)
			END
		END

		IF(NOT EXISTS(SELECT 1 FROM TB_T_PACKING_INDICATOR_UPLOAD_VALIDATE V WHERE V.PROCESS_ID = @process_id AND V.FUNCTION_ID = @functionId))
		BEGIN
		    PRINT 'insert'
			INSERT INTO [dbo].[TB_M_PACKING_INDICATOR]
			(	[DOCK_CD]
				,[VALID_DT_FR]
				,[PACKING_TYPE_CD]
				,[VALID_DT_TO]
				,[PART_COLOR_SFX]
				,[MAT_NO]
				,[PROD_PURPOSE_CD]
				,[SOURCE_TYPE]
				,[CREATED_BY]
				,[CREATED_DT]
			)			
			SELECT [DOCK_CD]
				  ,CONVERT(CHAR(10),CONVERT(DATETIME,LEFT([VALID_DT_FR],10),105),101)
				  ,[PACKING_TYPE_CD]
				  ,CONVERT(CHAR(10),CONVERT(DATETIME,LEFT([VALID_DT_TO],10),105),101)
				  ,[PART_COLOR_SFX]
				  ,[MAT_NO]
				  ,[PROD_PURPOSE_CD]
				  ,[SOURCE_TYPE]
				  ,[CREATED_BY]
				  ,GETDATE()
			FROM [IPPCS_QA].[dbo].[TB_T_PACKING_INDICATOR_TEMP] 
			WHERE PROCESS_ID = @process_id

			SET @message ='success'
		END

		SELECT @message
	END
	ELSE
	BEGIN
		SET @message ='No Data Found'
		SELECT @message
	END

	IF OBJECT_ID('tempdb..#duplicate') IS NOT NULL 
	DROP TABLE #duplicate

	
END	
--RETURN 0
SET ANSI_NULLS ON