USE [IPPCS_QA]
GO

/****** Object:  StoredProcedure [dbo].[SP_Buying_Price_Upload_Batch]    Script Date: 11/12/2020 09:17:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
---- =============================================
ALTER PROCEDURE [dbo].[SP_Buying_Price_Upload_Batch]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE
	@ro_v_userid	varchar(20) = 'Job_system',
	@Message		varchar(max),
	@MESSAGE_ID			varchar(12)
	--@created_by varchar(50) = 'job_Sp_Supplier_Master';


	DECLARE
	@Processid  bigint = (select dbo.fn_get_last_process_id()),
	@module varchar(8)='91022',
	@function varchar(10) = '91022',
	@location varchar(50) = 'SP_Buying_Price_Upload_Batch',
	@process_flag varchar(max) = 'start upload',
	@now date = getdate(),
	@MESSAGE_TYPE varchar(5) = 'INF',
	@update_flag int = 0,
	@apprval_sts varchar(1),
	@validFrom varchar(16),
	@rowNo int = 0,
	@Flag_price_status varchar(5),
	@CountTemp int,
	@CountRem int,
	@countOfExists int = 0,
	@countofUpdate int,
	@referTble varchar(12) = 'SOURCE LIST'
	,@INSERT INT = 0
	,@UPDATE INT = 0
	,@ERR_FLAG INT = 0
	,@INVALIDCOUNT INT = 0
	-- cursor variable
	declare
	@Mat_no			varchar(23),
	@supp_code		varchar(6),
	@Source_type	varchar(1),
	@prod_purpse	varchar(5),
	@packing_type	varchar(1),
	@par_color_sufix varchar(2),
	@valid_fr		 varchar(16),
	@price			numeric(16),
	@curr_cd		 varchar(3)
	,@SEQNO		 INT
	--select @Processid
	--return;

	EXEC [dbo].[spInsertLogHeader] @Processid,@function,@module,@now,@now,'5','SYSTEM'
	
    SET @MESSAGE_ID = 'MPCS00002INF'
	SET @Message = (select REPLACE(MESSAGE_TEXT,'{0}','job') from TB_M_MESSAGE where MESSAGE_ID = @MESSAGE_ID)
	
	EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

	DECLARE l_cursor CURSOR FAST_FORWARD FOR
	SELECT ROW_NUMBER ()OVER (ORDER BY A.MAT_NO DESC) SEQ_NO, * from(

	SELECT 
	 MAT_NO 
	,SUPP_CD
	,SOURCE_TYPE
	,PROD_PURPOSE_CD
	,PACKING_TYPE
	,PART_COLOR_SFX
	,VALID_DT_FR
	,PRICE_AMT
	,CURR_CD
	FROM TB_T_MAT_PRICE
	)A
	OPEN l_cursor
	FETCH NEXT FROM l_cursor INTO
	@SEQNO,
	 @Mat_no			
	,@supp_code		
	,@Source_type	
	,@prod_purpse	
	,@packing_type	
	,@par_color_sufix
	,@valid_fr		
	,@price			
	,@curr_cd

	while @@FETCH_STATUS = 0
	BEGIN
		-- Do something with your ID and string
		SET @update_flag = 0;
		SET @rowNo = @rowNo + 1

		if @Mat_no IS NULL OR LEN(@Mat_no)<1
		begin
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','MAT_NO');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		if @supp_code IS NULL OR LEN(@supp_code)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','SUPPLIER_CODE');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		if @Source_type IS NULL OR LEN(@Source_type)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','SOURCE_TYPE');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end
		
		
		if @prod_purpse IS NULL OR LEN(@prod_purpse)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','PROD PURPOSE');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		
		if @packing_type IS NULL OR LEN(@packing_type)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','SUPPLIER_CODE');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		if @par_color_sufix IS NULL OR LEN(@par_color_sufix)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','PART COLOR SFX');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		if @valid_fr IS NULL OR LEN(@valid_fr)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','VALID FROM');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		if @price IS NULL OR LEN(@price)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','PRICE');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		if @curr_cd IS NULL OR LEN(@curr_cd)<1
		begin
			--print 'On row {row}, {field name} should not be empty.'
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, {1} should not be empty','{0}',@rowNo),'{1}','CURR CODE');
			SET @MESSAGE_TYPE = 'ERR'

			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @ERR_FLAG = 1
		end

		-- duplicate validation
		--SET @countOfExists = 0;
		if exists(select 1 from TB_M_DRAFT_MATERIAL_PRICE WHERE 1=1
					AND MAT_NO = @Mat_no
					AND SUPP_CD = @supp_code
					AND SOURCE_TYPE = @Source_type
					AND PROD_PURPOSE_CD = @prod_purpse
					AND PACKING_TYPE = @packing_type
					AND PART_COLOR_SFX = @par_color_sufix
					AND VALID_DT_FR = @valid_fr
					AND DELETION_FLAG = 'N'
		)
		begin 
			--print 'On row {row1, row2, etc}, found duplication for the combination of {Material No, Supplier Code, and Valid From}'
			SET @update_flag = 1;

			declare 
			@varCombination varchar(max) = ('MAT_NO : '+@Mat_no+', SUPP_CODE = '+@supp_code+', SOURCE_TYPE = '+@Source_type)
			SET @MESSAGE = REPLACE(REPLACE('On row {0}, found duplication for the combination of {1}','{0}',@rowNo),'{1}',@varCombination);
			SET @MESSAGE_TYPE = 'ERR'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;
			
			
			SET @ERR_FLAG = 1
		end
		else
		begin
			SET @countOfExists = @countOfExists+1;
		end

		-- refernces validation
		if not exists( select 1 from TB_M_SOURCE_LIST where MAT_NO = @Mat_no)
		begin
			--print 'On row {row}, field {fieldname} = {value} is not found in {reference table.field}'
			SET @MESSAGE = REPLACE(REPLACE(REPLACE(REPLACE('On row {0}, field {1} = {2} is not found in {3}','{0}',@rowNo),'{1}','MAT_NO'),'{2}',@Mat_no),'{3}',@referTble);
			SET @MESSAGE_TYPE = 'ERR'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

			SET @update_flag =1;

			SET @ERR_FLAG = 1
		end

		if not exists( select 1 from TB_M_SOURCE_LIST where SUPP_CD = @supp_code)
		begin
			--print 'On row {row}, field {fieldname} = {value} is not found in {reference table.field}'
			SET @MESSAGE = REPLACE(REPLACE(REPLACE(REPLACE('On row {0}, field {1} = {2} is not found in {3}','{0}',@rowNo),'{1}','SUPP_CD'),'{2}',@supp_code),'{3}',@referTble);
			SET @MESSAGE_TYPE = 'ERR'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;
			SET @update_flag =1;

			SET @ERR_FLAG = 1
		end

		if not exists( select 1 from TB_M_SOURCE_LIST where SOURCE_TYPE = @Source_type)
		begin
			--print 'On row {row}, field {fieldname} = {value} is not found in {reference table.field}'
			SET @MESSAGE = REPLACE(REPLACE(REPLACE(REPLACE('On row {0}, field {1} = {2} is not found in {3}','{0}',@rowNo),'{1}','SOURCE_TYPE'),'{2}',@Source_type),'{3}',@referTble);
			SET @MESSAGE_TYPE = 'ERR'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;
			SET @update_flag =1;

			SET @ERR_FLAG = 1
		end

		if not exists( select 1 from TB_M_SOURCE_LIST where PROD_PURPOSE_CD = @prod_purpse)
		begin
			--print 'On row {row}, field {fieldname} = {value} is not found in {reference table.field}'
			SET @MESSAGE = REPLACE(REPLACE(REPLACE(REPLACE('On row {0}, field {1} = {2} is not found in {3}','{0}',@rowNo),'{1}','PROD_PURPOSE_CD'),'{2}',@prod_purpse),'{3}',@referTble);
			SET @MESSAGE_TYPE = 'ERR'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;
			SET @update_flag =1;

			SET @ERR_FLAG = 1
		end

		if not exists( select 1 from TB_M_CURRENCY where CURRENCY_CD = @curr_cd)
		begin
			--print 'On row {row}, field {fieldname} = {value} is not found in {reference table.field}'
			SET @MESSAGE = REPLACE(REPLACE(REPLACE(REPLACE('On row {0}, field {1} = {2} is not found in {3}','{0}',@rowNo),'{1}','CURRENCY_CD'),'{2}',@curr_cd),'{3}','TB_M_CURRENCY');
			
			SET @MESSAGE_TYPE = 'ERR'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;
			SET @update_flag =1;

			SET @ERR_FLAG = 1
		end

		-- CEHEK DELETION FLAG VALIDATION
		DECLARE 
		@detionFlag varchar(2);

		select @detionFlag = DELETION_FLAG from TB_M_DRAFT_MATERIAL_PRICE WHERE 1=1
				AND MAT_NO = @Mat_no
					AND SUPP_CD = @supp_code
					AND SOURCE_TYPE = @Source_type
					AND PROD_PURPOSE_CD = @prod_purpse
					AND PACKING_TYPE = @packing_type
					AND PART_COLOR_SFX = @par_color_sufix
					AND VALID_DT_FR = @valid_fr					
		
			--print 'On row {row}, field {fieldname} = {value} is not found in {reference table.field}'
		if @detionFlag = 'Y'
		begin	
			SET @varCombination = ('MAT_NO : '+@Mat_no+', SUPP_CODE = '+@supp_code+', SOURCE_TYPE = '+@Source_type)
			SET @MESSAGE = REPLACE(REPLACE('on row {0}, the material is deleted = {1}','{0}',@rowNo),'{1}',@varCombination);
			SET @MESSAGE_TYPE = 'ERR'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;
			SET @update_flag =1;

			SET @ERR_FLAG = 1
		end

		-- CHECK VALIDATION APPROVAL	
		SET @apprval_sts = (select RELEASE_STS from TB_M_DRAFT_MATERIAL_PRICE where 1=1
					AND MAT_NO = @Mat_no
					AND SUPP_CD = @supp_code
					AND SOURCE_TYPE = @Source_type
					AND PROD_PURPOSE_CD = @prod_purpse
					AND PACKING_TYPE = @packing_type
					AND PART_COLOR_SFX = @par_color_sufix
					AND VALID_DT_TO = '9999-12-31'
		)
		if @apprval_sts = 'N'
		begin
			SET @update_flag = 1;
			--print 'the latest price for material [var] still not approve`';
			SET @MESSAGE ='the latest price for material [var] still not approve';
			SET @MESSAGE_TYPE = 'INF'
			EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;
		end
		--if @apprval_sts = 'Y'
		--begin
			-- Time Control Validation
			SET @validFrom = (SELECT VALID_DT_FR FROM TB_M_DRAFT_MATERIAL_PRICE
			 where 1=1
					AND MAT_NO = @Mat_no
					AND SUPP_CD = @supp_code
					AND SOURCE_TYPE = @Source_type
					AND PROD_PURPOSE_CD = @prod_purpse
					AND PACKING_TYPE = @packing_type
					AND PART_COLOR_SFX = @par_color_sufix
					AND VALID_DT_TO = '9999-12-31' )

			if exists(
				select 1 from TB_M_DRAFT_MATERIAL_PRICE where 1=1
					AND MAT_NO = @Mat_no
					AND SUPP_CD = @supp_code
					AND SOURCE_TYPE = @Source_type
					AND PROD_PURPOSE_CD = @prod_purpse
					AND PACKING_TYPE = @packing_type
					AND PART_COLOR_SFX = @par_color_sufix
					AND VALID_DT_TO = '9999-12-31'
			)
			begin
				if @valid_fr <= @validFrom
				begin
					SET @update_flag = 1;
					--print 'On row {row}, Valid From must be greater than {Valid From of latest existing row}'
					SET @MESSAGE = REPLACE(REPLACE('On row {0}, Valid From must be greater than valid from {1}','{0}',@rowNo),'{1}',@validFrom);
					SET @MESSAGE_TYPE = 'ERR';
					EXEC dbo.SPInsertLogDetail @Processid
					,@MESSAGE_ID
					,@MESSAGE_TYPE
					,@MESSAGE
					,@LOCATION
					,'SYSTEM'
					,null;

					SET @ERR_FLAG = 1
				end
				else
				begin
					SET @Flag_price_status = 'F';
				end
			end
			else
			begin
				SET @Flag_price_status = 'T';

			end

		
		if @update_flag = 1
		begin
			SET @countofUpdate = @countofUpdate + 1;
			UPDATE TB_T_MAT_PRICE
				SET [STATUS] = 'NG'
				WHERE 1=1
				AND MAT_NO = @Mat_no
				AND SUPP_CD = @supp_code
				AND SOURCE_TYPE = @Source_type
				AND PROD_PURPOSE_CD = @prod_purpse
				AND PACKING_TYPE = @packing_type
				AND PART_COLOR_SFX = @par_color_sufix
				AND VALID_DT_FR = @valid_fr
			
			--print 'masuk update NG'
			 
		end

		if @Flag_price_status IS NULL OR LEN(@flag_price_status)<1
		begin 
			--print 'masuk update PRICE_STATUS'
			BEGIN TRANSACTION Insert_UPDATE;
				UPDATE TB_M_DRAFT_MATERIAL_PRICE
				SET PRICE_STATUS = @Flag_price_status
				WHERE 1=1
				AND MAT_NO			= @Mat_no
				AND SUPP_CD			= @supp_code
				AND SOURCE_TYPE		= @Source_type
				AND PROD_PURPOSE_CD = @prod_purpse
				AND PACKING_TYPE	= @packing_type
				AND PART_COLOR_SFX	= @par_color_sufix
				AND VALID_DT_TO		= '9999-12-31'
			COMMIT TRANSACTION Insert_UPDATE;
		end

		-- modify temporary table
		UPDATE TB_T_MAT_PRICE
		SET VALID_DT_TO = '9999-12-31'
		,DELETION_FLAG	= 'N'
		,APPROVE_STS	= 'N'
		,RETRO_STS		= 'N'
		--end

	IF @ERR_FLAG = 1
	BEGIN
		
		SET @INVALIDCOUNT = @INVALIDCOUNT+1;
		if @@TRANCOUNT > 0
		ROLLBACK TRANSACTION Insert_UPDATE;
	END
	ELSE
	BEGIN
		if not exists(
			SELECT * from TB_M_DRAFT_MATERIAL_PRICE  m	
			WHERE MAT_NO = @Mat_no
					AND SUPP_CD = @supp_code
					AND SOURCE_TYPE = @Source_type
					AND PROD_PURPOSE_CD = @prod_purpse
					AND PACKING_TYPE = @packing_type
					AND PART_COLOR_SFX = @par_color_sufix
			--AND t.[STATUS] != 'NG' OR t.[STATUS] IS NULL

			)
		begin
			--print 'insert'
			begin try
				SET IDENTITY_INSERT TB_M_DRAFT_MATERIAL_PRICE ON

				INSERT INTO TB_M_DRAFT_MATERIAL_PRICE (
					WARP_BUYER_CD,
					SUPP_CD,
					MAT_NO,
					PRICE_STATUS,
					RELEASE_STS,
					RELEASE_INPUT_DT,
					RELEASED_BY,
					SOURCE_TYPE,
					VALID_DT_FR,
					VALID_DT_TO,
					PC_NO,
					SOURCE_DATA,
					DRAFT_DF,
					WARP_REF_NO,
					CPP_FLAG,
					PROD_PURPOSE_CD,
					PART_COLOR_SFX,
					CURR_CD,
					PRICE_AMT,
					PACKING_TYPE,
					SENT_PIECE_PO_DT,
					DATA_SEQUENCE,
					BUSINESS_AREA,
					DELETION_FLAG,
					UOM,
					MM_DF,
					PRICE_TYPE,
					CREATED_BY,
					CREATED_DT,
					CHANGED_BY,
					CHANGED_DT,
					id
				)
				SELECT 
					''--t.[WARP_BUYER_CD]		
					,t.[SUPP_CD]			
					,t.[MAT_NO]			
					,t.[PRICE_STATUS]		
					,'' --t.[RELEASE_STS]		
					,'' --t.[RELEASE_INPUT_DT]	
					,'' --t.[RELEASED_BY]		
					,t.[SOURCE_TYPE]		
					,t.[VALID_DT_FR]		
					,t.[VALID_DT_TO]		
					,t.[PC_NO]				
					,'' --t.[SOURCE_DATA]		
					,'' --t.[DRAFT_DF]			
					,'' --t.[WARP_REF_NO]		
					,'' --t.[CPP_FLAG]			
					,t.[PROD_PURPOSE_CD]	
					,t.[PART_COLOR_SFX]	
					,t.[CURR_CD]			
					,t.[PRICE_AMT]			
					,t.[PACKING_TYPE]		
					,'' --t.[SENT_PIECE_PO_DT]	
					,'' --t.[DATA_SEQUENCE]		
					,'' --t.[BUSINESS_AREA]		
					,'N' --t.[DELETION_FLAG]		
					,'' --t.[UOM]				
					,'' --t.[MM_DF]				
					,'' --t.[PRICE_TYPE]		
					,'job'		
					,getdate()		
					,null	
					,null		
					,''		
				FROM TB_T_MAT_PRICE t
				WHERE t.[STATUS] != 'NG' OR t.[STATUS] IS NULL
				AND t.SUPP_CD = @supp_code
				AND t.SOURCE_TYPE = @Source_type
				AND t.PROD_PURPOSE_CD = @prod_purpse
				AND t.PACKING_TYPE = @packing_type
				AND t.PART_COLOR_SFX = @par_color_sufix 
				SET @INSERT += 1 
				SET IDENTITY_INSERT TB_M_DRAFT_MATERIAL_PRICE OFF
			end try
			begin catch		
				SET @MESSAGE = ERROR_MESSAGE();
				SET @MESSAGE_TYPE = 'ERR';

				EXEC dbo.SPInsertLogDetail 
				 @Processid
				,@MESSAGE_ID
				,@MESSAGE_TYPE
				,@MESSAGE
				,@LOCATION
				,'SYSTEM'
				,null;
			end catch
		end
		else
		begin
		--print 'update TB_M_DRAFT_MATERIAL_PRICE'
			SET IDENTITY_INSERT TB_M_DRAFT_MATERIAL_PRICE ON

			UPDATE m
			SET 
				m.VALID_DT_TO = DATEADD(DAY,-1,t.VALID_DT_FR),
				m.CHANGED_BY = 'job',
				m.CHANGED_DT = getdate()
			FROM TB_M_DRAFT_MATERIAL_PRICE m
			JOIN TB_T_MAT_PRICE t
			ON (m.MAT_NO = t.mat_no
			AND m.SUPP_CD = t.supp_cd
			AND m.SOURCE_TYPE = t.source_type
			AND m.PROD_PURPOSE_CD = t.prod_purpose_cd
			AND m.PACKING_TYPE = t.packing_type
			AND m.PART_COLOR_SFX = t.part_color_sfx	)	
			WHERE m.VALID_DT_TO = '9999-12-31' --AND t.[STATUS] != 'NG'

			INSERT INTO TB_M_DRAFT_MATERIAL_PRICE (
					WARP_BUYER_CD,
					SUPP_CD,
					MAT_NO,
					PRICE_STATUS,
					RELEASE_STS,
					RELEASE_INPUT_DT,
					RELEASED_BY,
					SOURCE_TYPE,
					VALID_DT_FR,
					VALID_DT_TO,
					PC_NO,
					SOURCE_DATA,
					DRAFT_DF,
					WARP_REF_NO,
					CPP_FLAG,
					PROD_PURPOSE_CD,
					PART_COLOR_SFX,
					CURR_CD,
					PRICE_AMT,
					PACKING_TYPE,
					SENT_PIECE_PO_DT,
					DATA_SEQUENCE,
					BUSINESS_AREA,
					DELETION_FLAG,
					UOM,
					MM_DF,
					PRICE_TYPE,
					CREATED_BY,
					CREATED_DT,
					CHANGED_BY,
					CHANGED_DT,
					id
				)
				SELECT 
					''--t.[WARP_BUYER_CD]		
					,t.[SUPP_CD]			
					,t.[MAT_NO]			
					,t.[PRICE_STATUS]		
					,'' --t.[RELEASE_STS]		
					,'' --t.[RELEASE_INPUT_DT]	
					,'' --t.[RELEASED_BY]		
					,t.[SOURCE_TYPE]		
					,t.[VALID_DT_FR]		
					,t.[VALID_DT_TO]		
					,t.[PC_NO]				
					,'' --t.[SOURCE_DATA]		
					,'' --t.[DRAFT_DF]			
					,'' --t.[WARP_REF_NO]		
					,'' --t.[CPP_FLAG]			
					,t.[PROD_PURPOSE_CD]	
					,t.[PART_COLOR_SFX]	
					,t.[CURR_CD]			
					,t.[PRICE_AMT]			
					,t.[PACKING_TYPE]		
					,'' --t.[SENT_PIECE_PO_DT]	
					,'' --t.[DATA_SEQUENCE]		
					,'' --t.[BUSINESS_AREA]		
					,'N' --t.[DELETION_FLAG]		
					,'' --t.[UOM]				
					,'' --t.[MM_DF]				
					,'' --t.[PRICE_TYPE]		
					,'job'		
					,getdate()		
					,null	
					,null		
					,''		
				FROM TB_T_MAT_PRICE t
				WHERE t.[STATUS] != 'NG' OR t.[STATUS] IS NULL
				AND t.SUPP_CD = @supp_code
				AND t.SOURCE_TYPE = @Source_type
				AND t.PROD_PURPOSE_CD = @prod_purpse
				AND t.PACKING_TYPE = @packing_type
				AND t.PART_COLOR_SFX = @par_color_sufix 

				SET IDENTITY_INSERT TB_M_DRAFT_MATERIAL_PRICE OFF

			SET @UPDATE += 1
		end
	END
		
		FETCH NEXT FROM l_cursor INTO
		@SEQNO		,
		 @Mat_no			
		,@supp_code		
		,@Source_type	
		,@prod_purpse	
		,@packing_type	
		,@par_color_sufix
		,@valid_fr		
		,@price			
		,@curr_cd	
	END

	close l_cursor;
	deallocate l_cursor;

	
	--INSERT INTO TB_M_DRAFT_MATERIAL_PRICE
	--SELECT * FROM TB_T_MAT_PRICE
	--WHERE [STATUS] = 'NG'
	--ORDER BY MAT_NO ,SUPPLIER_CODE,SOURCE_TYPE,PROD_PURPOSE,PACKING_TYPE ASC
	

	-- inssert log detil 
		SET @CountTemp = (select count(1) from TB_T_MAT_PRICE)
		SET @MESSAGE = REPLACE('Count the total of uploaded record to temporary table = {0}','{0}',@CountTemp);
		SET @MESSAGE_TYPE = 'INF'

		EXEC dbo.SPInsertLogDetail @Processid
		,@MESSAGE_ID
		,@MESSAGE_TYPE
		,@MESSAGE
		,@LOCATION
		,'SYSTEM'
		,null;

		--Count the total of remaining record after validation = {0}
		SET @CountRem = (select count(1) from TB_T_MAT_PRICE where [STATUS] = 'NG')
		SET @MESSAGE = REPLACE('Count the total of remaining record after validation = {0}','{0}',@CountRem);
		SET @MESSAGE_TYPE = 'INF'

		EXEC dbo.SPInsertLogDetail @Processid
		,@MESSAGE_ID
		,@MESSAGE_TYPE
		,@MESSAGE
		,@LOCATION
		,'SYSTEM'
		,null;

		--SET @MESSAGE = (REPLACE('Count the total of record that don''t have existing record on permanent table = {0}','{0}',@countOfExists));
		--SET @MESSAGE_TYPE = 'INF'
		--EXEC dbo.SPInsertLogDetail @Processid
		--,@MESSAGE_ID
		--,@MESSAGE_TYPE
		--,@MESSAGE
		--,@LOCATION
		--,'SYSTEM'
		--,null;

		--All records posted to table
		--SET @MESSAGE = (REPLACE('Count the total of record that don''t have existing record on permanent table = {0}','{0}',@countOfExists));
		SET @MESSAGE = (REPLACE(REPLACE('{0} inserted. {1} updated.','{0}',@INSERT), '{1}', @UPDATE));
		SET @MESSAGE_TYPE = 'INF'
		EXEC dbo.SPInsertLogDetail @Processid
		,@MESSAGE_ID
		,@MESSAGE_TYPE
		,@MESSAGE
		,@LOCATION
		,'SYSTEM'
		,null;

		--Invalid record = uploaded record - remaining record
		--SET @MESSAGE = (REPLACE('Count the total of record that don''t have existing record on permanent table = {0}','{0}',@countOfExists));
		SET @MESSAGE = (REPLACE(REPLACE('Found invalid records = {0} of total {1}','{0}',@INVALIDCOUNT), '{1}', @CountTemp));
		SET @MESSAGE_TYPE = 'INF'
		EXEC dbo.SPInsertLogDetail @Processid
		,@MESSAGE_ID
		,@MESSAGE_TYPE
		,@MESSAGE
		,@LOCATION
		,'SYSTEM'
		,null;

		--Inserted record = total record that don't have existing record on permanent table

		--REPLACE('Updated record = remaining record - inserted record = {0}','{0}',@countofUpdate)


		 DELETE  FROM TB_T_MAT_PRICE


		 SET @MESSAGE = 'Process is finished';
		 SET @MESSAGE_TYPE = 'INF'
		 EXEC dbo.SPInsertLogDetail @Processid
		,@MESSAGE_ID
		,@MESSAGE_TYPE
		,@MESSAGE
		,@LOCATION
		,'SYSTEM'
		,null;


END



GO

