CREATE TABLE [dbo].[TB_M_MATERIAL](
	[MAT_NO] [varchar](23) NOT NULL,
	[PART_FLAG] [varchar](1) NOT NULL,
	[ORI_MAT_NO] [varchar](23) NULL,
	[SPECIAL_STOCK_VALUATION_FLAG] [varchar](1) NOT NULL,
	[UNIT_OF_MEASURE_CD] [varchar](3) NOT NULL,
	[CAR_FAMILY_CD] [varchar](4) NULL,
	[MAT_GRP_CD] [varchar](9) NOT NULL,
	[MAT_TYPE_CD] [varchar](4) NOT NULL,
	[DELETION_FLAG] [varchar](1) NOT NULL,
	[RE_EXPORT_CD] [varchar](1) NULL,
	[MAT_DESC] [varchar](40) NOT NULL,
	[AUTO_GOODS_ISSUE_FLAG] [varchar](1) NOT NULL,
	[SELECTIVE_FLAG] [varchar](1) NOT NULL,
	[CASE_EXPLOSION_FLAG] [varchar](1) NOT NULL,
	[DELETION_DT] [datetime] NULL,
	[MAT_IS_LOCKED] [varchar](1) NULL,
	[PART_LIST_FLAG] [varchar](1) NULL,
	[PART_LIST_USAGE] [varchar](1) NULL,
	[CREATED_BY] [varchar](20) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[INDUSTRY_SECTOR] [varchar](1) NULL,
	[BATCH_MANAGEMENT] [varchar](1) NULL,
	[PRODUCT_TYPE] [varchar](10) NULL,
	[MRP_GROUP] [varchar](4) NULL,
	[MRP_TYPE] [varchar](2) NULL,
	[PROCUREMENT_TYPE] [varchar](1) NULL,
	[PROD_STOR_LOCATION] [varchar](4) NULL,
	[REPEAT_MANUFACTURING] [varchar](1) NULL,
	[REM_PROFILE] [varchar](4) NULL,
	[DO_NOT_COST] [varchar](1) NULL,
	[VARIANCE_KEY] [varchar](6) NULL,
	[COSTING_LOT_SIZE] [numeric](16, 3) NULL,
	[SPECIAL_PROCUREMENT_FOR_COSTING] [varchar](2) NULL,
	[PRICE_DETERMINATION] [varchar](1) NULL,
	[WITH_QUANTITY_STRUCTURE] [varchar](1) NULL,
	[MATERIAL_ORIGIN] [varchar](1) NULL,
	[ORIGIN_GROUP] [varchar](4) NULL,
 CONSTRAINT [PK_TB_M_MATERIAL] PRIMARY KEY CLUSTERED 
(
	[MAT_NO] ASC
)
) ON [PRIMARY]
GO


