@echo off

if "%1"=="RENAME" goto ren

rem di to z.txt

if NOT exisT z.txt (
echo doing nothing 
 goto ende
)
    
for /F "usebackq" %%A in ('z.txt') do set zxsize=%%~zA
if  %zxsize%==0 (
  ECHO already cleaned
  goto ende
)
type z.txt|sort|uniq > zx.txt 

"c:\Program Files\7-Zip\7zG.exe" a z.7z @zx.txt -sdel

del z.txt,zx.txt /q 

:ren
IF NOT EXIST dbo.*.sql (
echo nothing to rename 
goto ende
)
for %%f in (dbo.*.sql) do (
  call :reno %%f 
)
goto ende

:reno
set siji=%1
set loro=%siji:~4,200%
ren %siji% %loro%>nul
exit /b 1

:ende 
echo DONE