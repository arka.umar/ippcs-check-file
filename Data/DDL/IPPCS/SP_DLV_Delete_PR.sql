/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Delete PR item
*/

CREATE PROCEDURE [dbo].[SP_DLV_Delete_PR]

@GRID VARCHAR (MAX),
@CREATED_BY VARCHAR (20),
@pid AS BIGINT

AS
BEGIN

		-- =================================================================================================
		
		DECLARE @MODUL_ID INT = 3
		DECLARE @FUNCTION_ID VARCHAR (6) =  '31102'
		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_Delete_PR'
		DECLARE @step AS VARCHAR(50) = 'init'
		--DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)

		DECLARE @CHECK_MODULE INT;

		DECLARE @ERR_MSG VARCHAR (MAX);

		

		--CREATE LOG
		SET @steps = @na+'.'+@step;
		SET @log = 'Delete PR Process started.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
		


		SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID IN (@FUNCTION_ID,'31103') AND PROCESS_END_DT IS NULL );
		IF (@CHECK_MODULE=0) BEGIN

						INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
						VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());


						DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
						DECLARE @ID varchar(11),@ITEM varchar(MAX);
						DECLARE @PROD_MONTH VARCHAR(7),
										@LP_CD VARCHAR(3), 
										@ROUTE_CD VARCHAR(4), 
										@SOURCE VARCHAR (20), 
										@PR_QTY INT, 
										@STATUS VARCHAR (15);


						DECLARE BTMP_EXPLODE CURSOR
						FOR
								
						SELECT ItemNumber, Item FROM dbo.FN_DLV_ATD_EXPLODE(@GRID,';');	

						OPEN BTMP_EXPLODE;
						FETCH NEXT FROM BTMP_EXPLODE INTO
						@ID,@ITEM;

						WHILE @@FETCH_STATUS = 0
						BEGIN

								DELETE FROM @FIELDS;
								INSERT INTO @FIELDS
								SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ITEM,'|');

								SET @PROD_MONTH = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
								SET @LP_CD 			= (SELECT ITEM FROM @FIELDS WHERE ID = 2);
								SET @ROUTE_CD 	= (SELECT ITEM FROM @FIELDS WHERE ID = 3);
								SET @SOURCE 		= (SELECT ITEM FROM @FIELDS WHERE ID = 4);
								SET @PR_QTY 		= (SELECT ITEM FROM @FIELDS WHERE ID = 5);
								SET @STATUS 		= (SELECT ITEM FROM @FIELDS WHERE ID = 6);

								UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = @pid WHERE PROD_MONTH = @PROD_MONTH AND LP_CD = @LP_CD AND ROUTE_CD = @ROUTE_CD AND SOURCE = @SOURCE
								
								
						FETCH NEXT FROM BTMP_EXPLODE INTO
							@ID,@ITEM
						END;
						CLOSE BTMP_EXPLODE;
						DEALLOCATE BTMP_EXPLODE;

						
						
						--CREATE LOG
						SET @steps = @na+'.'+@step;
						SET @log = 'Checking Status PR.';
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

						DECLARE @CHECK_STATUS INT, @CHECK_DATA INT, @CHECK_STATUS_DATA VARCHAR (4) ;

						-- SET  @CHECK_STATUS = (SELECT COUNT (STATUS) FROM #TEMP WHERE STATUS <> 'PR Retrieved');
						SET  @CHECK_STATUS = (SELECT COUNT (*) FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid AND PR_STATUS_FLAG <> 'PR1');

						IF (@CHECK_STATUS = 0 ) BEGIN
									DECLARE BTMP_CUR_1 CURSOR
									FOR
									SELECT PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG
									-- FROM #TEMP;
									FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid;

									OPEN BTMP_CUR_1;
									FETCH NEXT FROM BTMP_CUR_1 INTO
									@PROD_MONTH, @LP_CD, @ROUTE_CD, @SOURCE, @PR_QTY, @STATUS;

									WHILE @@FETCH_STATUS = 0
									BEGIN

												SET @CHECK_DATA = (SELECT COUNT(*) FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH = @PROD_MONTH AND LP_CD=@LP_CD AND ROUTE_CD = @ROUTE_CD AND SOURCE=@SOURCE );

												IF (@CHECK_DATA = 0) BEGIN
														UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;
														UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
														RAISERROR ('Data is not exist',16,1);
														RETURN
												END
												ELSE BEGIN
														
														SET @CHECK_STATUS_DATA = (SELECT PR_STATUS_FLAG FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH = @PROD_MONTH AND LP_CD=@LP_CD AND ROUTE_CD = @ROUTE_CD AND SOURCE=@SOURCE );
														
														IF (@CHECK_STATUS_DATA = 'PR1') BEGIN
																DELETE FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH = @PROD_MONTH AND LP_CD=@LP_CD AND ROUTE_CD = @ROUTE_CD AND SOURCE=@SOURCE;	
														END
														ELSE BEGIN
																UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;
																UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
																RAISERROR('Only data with status Retrieved can be deleted',16,1);
																RETURN
														END

														

												END

									FETCH NEXT FROM BTMP_CUR_1 INTO
										@PROD_MONTH, @LP_CD, @ROUTE_CD, @SOURCE, @PR_QTY, @STATUS
									END;
									CLOSE BTMP_CUR_1;
									DEALLOCATE BTMP_CUR_1;

									--CREATE LOG
									SET @steps = @na+'.'+@step;
									SET @log = 'Process delete PR has been finished';
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
									

									UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;


									


						END
						ELSE BEGIN
								
								--CREATE LOG
								SET @steps = @na+'.'+@step;
								SET @log = 'Only data with status Retrieved can be deleted';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
												
								UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

								RAISERROR('Only data with status Retrieved can be deleted',16,1)
								RETURN
						END
						
						UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
			


		END
		ELSE BEGIN
				RAISERROR('Other process is still running by another user.',16,1)
				RETURN
		END


				
		
		--CREATE LOG
		SET @steps = @na+'.'+@step;
		SET @log = 'Process Delete PR has been finished.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

END

