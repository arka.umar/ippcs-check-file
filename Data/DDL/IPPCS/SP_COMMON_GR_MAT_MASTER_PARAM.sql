-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	05-Oct-2020
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_COMMON_GR_MAT_MASTER_PARAM] @USER_ID VARCHAR(20) = 'AGAN'
	,@PROCESS_ID BIGINT = '202009230021'
	,@r_v_mat_no VARCHAR(100)
	,@r_v_prod_purpose VARCHAR(100)
	,@r_v_source_type VARCHAR(100)
	,@r_v_plant_cd VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MODULE_ID VARCHAR(50) = '4'
		,@FUNCTION_ID VARCHAR(100) = '42005'
		,@IsError CHAR(1) = 'N'
		,@CURRDT DATE = CAST(GETDATE() AS DATE)
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_COMMON_GR_MAT_MASTER_PARAM'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@RES INT = 0;

	IF OBJECT_ID('tempdb..#csMatMasterParam') IS NOT NULL
	BEGIN
		DROP TABLE #csMatMasterParam
	END;

	CREATE TABLE #csMatMasterParam (
		unit_of_measure_cd VARCHAR(3)
		,special_procurement_type_cd VARCHAR(2)
		,mat_grp_cd VARCHAR(9)
		,mat_type_cd VARCHAR(4)
		,mat_desc VARCHAR(40)
		,costing_type VARCHAR(2)
		,issue_storage_location VARCHAR(6)
		,price_control_flag VARCHAR(1)
		,moving_average_price NUMERIC(15, 2)
		,standard_price NUMERIC(15, 2)
		,valuation_class_cd VARCHAR(4)
		,sloc_for_ex_proc VARCHAR(6)
		,STATUS INT
		)

	BEGIN TRY
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'GET MATERIAL MASTER PARAM'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		DECLARE @l_n_count INT
			,@l_v_msg VARCHAR(500)
			,@l_v_msg2 VARCHAR(500);

		-- Mandatory Check Input Parameter
		IF RTRIM(LTRIM(ISNULL(@r_v_mat_no, ''))) = ''
			OR RTRIM(LTRIM(ISNULL(@r_v_prod_purpose, ''))) = ''
			OR RTRIM(LTRIM(ISNULL(@r_v_source_type, ''))) = ''
			OR RTRIM(LTRIM(ISNULL(@r_v_plant_cd, ''))) = ''
		BEGIN
			IF RTRIM(LTRIM(ISNULL(@r_v_mat_no, ''))) = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Material Number,';
			END;

			IF RTRIM(LTRIM(ISNULL(@r_v_prod_purpose, ''))) = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Production Purposed cd,';
			END;

			IF RTRIM(LTRIM(ISNULL(@r_v_source_type, ''))) = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Source Type,';
			END;

			IF RTRIM(LTRIM(ISNULL(@r_v_plant_cd, ''))) = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Plant Code,';
			END;

			SET @IsError = 'Y';
			SET @l_v_msg = SUBSTRING(@l_v_msg, 1, LEN(@l_v_msg) - 1);

			EXEC dbo.CommonGetMessage 'MPCS00003ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,@l_v_msg

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00003ERR'
				,'ERR'
				,'MPCS00003ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ' : Mandatory Checking'
				,@USER_ID
				,GETDATE()

			INSERT INTO #csMatMasterParam
			SELECT NULL unit_of_measure_cd
				,NULL special_procurement_type
				,NULL mat_grp_cd
				,NULL mat_type_cd
				,NULL mat_desc
				,NULL costing_type
				,NULL issue_storage_location
				,NULL price_control_flag
				,NULL moving_average_price
				,NULL standard_price
				,NULL valuation_class_cd
				,NULL sloc_for_ex_proc
				,1 STATUS
		END;

		IF @IsError <> 'Y'
		BEGIN
			SELECT @l_n_count = COUNT(1)
			FROM tb_m_material mat
				,tb_r_material_valuation matval
			WHERE mat.mat_no = matval.mat_no
				AND mat.mat_no = RTRIM(LTRIM(@r_v_mat_no))
				AND matval.prod_purpose_cd = RTRIM(LTRIM(@r_v_prod_purpose))
				AND matval.source_type = RTRIM(LTRIM(@r_v_source_type))
				AND matval.plant_cd = RTRIM(LTRIM(@r_v_plant_cd));

			IF @l_n_count = 0
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Data doesnt exist for input parameter Material: ' + @r_v_mat_no + ', Production Purpose : ' + @r_v_prod_purpose + ', Source Type: ' + @r_v_source_type + ', Plant: ' + @r_v_plant_cd;

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Existense Checking'
					,@USER_ID
					,GETDATE()

				INSERT INTO #csMatMasterParam
				SELECT NULL unit_of_measure_cd
					,NULL special_procurement_type
					,NULL mat_grp_cd
					,NULL mat_type_cd
					,NULL mat_desc
					,NULL costing_type
					,NULL issue_storage_location
					,NULL price_control_flag
					,NULL moving_average_price
					,NULL standard_price
					,NULL valuation_class_cd
					,NULL sloc_for_ex_proc
					,1 STATUS
			END
			ELSE
			BEGIN
				INSERT INTO #csMatMasterParam
				SELECT mat.unit_of_measure_cd
					,matval.special_procurement_type_cd
					,mat.mat_grp_cd
					,mat.mat_type_cd
					,mat.mat_desc
					,matval.costing_type
					,matval.issue_storage_location
					,matval.price_control_flag
					,matval.moving_average_price
					,matval.standard_price
					,matval.valuation_class_cd
					,matval.sloc_for_ex_proc
					,0 STATUS
				FROM tb_m_material mat
					,tb_r_material_valuation matval
				WHERE mat.mat_no = matval.mat_no
					AND mat.mat_no = LTRIM(RTRIM(@r_v_mat_no))
					AND matval.prod_purpose_cd = LTRIM(RTRIM(@r_v_prod_purpose))
					AND matval.source_type = LTRIM(RTRIM(@r_v_source_type))
					AND matval.plant_cd = LTRIM(RTRIM(@r_v_plant_cd));
			END;
		END

		IF @IsError = 'N'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET MATERIAL MASTER PARAM'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET MATERIAL MASTER PARAM'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		SELECT *
		FROM #csMatMasterParam;
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		INSERT INTO #csMatMasterParam
		SELECT NULL unit_of_measure_cd
			,NULL special_procurement_type
			,NULL mat_grp_cd
			,NULL mat_type_cd
			,NULL mat_desc
			,NULL costing_type
			,NULL issue_storage_location
			,NULL price_control_flag
			,NULL moving_average_price
			,NULL standard_price
			,NULL valuation_class_cd
			,NULL sloc_for_ex_proc
			,2 STATUS

		SELECT *
		FROM #csMatMasterParam
	END CATCH
END
