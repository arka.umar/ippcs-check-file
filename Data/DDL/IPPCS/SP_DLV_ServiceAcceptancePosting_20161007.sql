/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Service acceptance posting
*/

CREATE PROCEDURE [dbo].[SP_DLV_ServiceAcceptancePosting_20161007]
	@pid as BIGINT,
	@MODULE_ID as varchar(1),
	@FUNCTION_ID as varchar(5), --'31302'
	@UserId as varchar(20)
AS
BEGIN
	
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_ServiceAcceptancePosting'
	DECLARE @step AS VARCHAR(50)
	DECLARE @isPostedError varchar(1)
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)
	--DECLARE @po_period AS VARCHAR(6) = (SELECT CURRENT_IM_YEAR + '' + CURRENT_IM_PERIOD FROM TB_M_IM_PERIOD)
	DECLARE @SP_NAME as varchar(MAX) = 'SP_DLV_ServiceAcceptancePosting'
	DECLARE @SERVICE_DOC_NO as VARCHAR(11)
	DECLARE @LAST_SERV_DOC as VARCHAR(11)
	DECLARE @Y as VARCHAR(2)
	DECLARE @M as VARCHAR(2)
	DECLARE @D as VARCHAR(2)
	DECLARE @CURR_Y as VARCHAR(2)
	DECLARE @CURR_M as VARCHAR(2)
	DECLARE @CURR_D as VARCHAR(2)
	DECLARE @DIGIT as VARCHAR(4)
	DECLARE @SEQS as VARCHAR(4)
	DECLARE @ADDS AS VARCHAR(11)
	DECLARE @lastSeq INT;
	DECLARE @postSts as varchar(2)
	DECLARE @DlvSts as varchar(15)			
	DECLARE @pn as varchar(12);
	DECLARE @a as INT
	DECLARE @QueueStat as varchar(3) = (SELECT PROCESS_STATUS FROM TB_R_DLV_QUEUE WHERE PROCESS_ID = @pid)
	DECLARE @SourceString as varchar(MAX) = ''
	DECLARE @isOK as INT = 0;
	DECLARE @isNG as INT = 0;
	DECLARE @isNomatch as INT = 0;
	DECLARE @isNotGood as int = 0;
	DECLARE @errData as int = 0;
	DECLARE @pr as int = 0;
	DECLARE @RAW_COUNT as INT = 0;
	DECLARE @j_status VARCHAR(10) = 'SUCCESS'
	
	DECLARE 
	@DELIVERY_NO as varchar(max), 
	@PROCESS_ID as bigint, 
	@ROUTE as varchar(max), 
	@RATE as varchar(2), 
	@TRANSACTION_CD as varchar(max), 
	@PICKUP_DT as date, 
	@LP_CD as varchar(max), 
	@POSTING_DT as datetime, 
	@OK_FLAG as varchar(max), 
	@CREATED_BY as varchar(max), 
	@CREATED_DT as datetime,
	@PO_NO as varchar(max)
	
	DECLARE @TB_T_LOG_D as TABLE(
		PROCESS_ID BIGINT, 
		SEQUENCE_NUMBER INT, 
		MESSAGE_ID VARCHAR(12), 
		MESSAGE_TYPE VARCHAR(3), 
		[MESSAGE] VARCHAR(MAX), 
		LOCATION VARCHAR(MAX), 
		CREATED_BY VARCHAR(20), 
		CREATED_DATE DATETIME
	)

	SET @log = 'Preparing SA Posting with Process ID ' + CONVERT(VARCHAR(MAX), @pid);
	INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Init', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

	SELECT 
		DELIVERY_NO, 
		PROCESS_ID, 
		[ROUTE], 
		RATE, 
		TRANSACTION_CD, 
		PICKUP_DT, 
		LP_CD, 
		POSTING_DT, 
		OK_FLAG, 
		CREATED_BY, 
		CREATED_DT, 
		PO_NO 
	INTO #CTL_H FROM(
		--SELECT  DISTINCT a.[DELIVERY_NO], @pid as PROCESS_ID, a.[ROUTE], a.[RATE], CASE WHEN a.[ROUTE] IS NULL THEN '' ELSE (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '31302' AND SYSTEM_CD = 'TRANS_CD_SA_POSTING') END as TRANSACTION_CD, a.[PICKUP_DT], a.LP_CD, GETDATE() as POSTING_DT, CASE WHEN b.[ROUTE_CD] IS NULL THEN 'NG' ELSE 'OK' END AS OK_FLAG, @UserId as CREATED_BY, GETDATE() as CREATED_DT, c.PO_NO
		--FROM TB_R_DELIVERY_CTL_H a
		--LEFT JOIN TB_R_DLV_PO_ITEM b ON b.ROUTE_CD = a.ROUTE AND SUBSTRING(b.PO_NO, 1,3) = a.LP_CD
		--LEFT JOIN TB_R_DLV_PO_H c ON c.PROD_MONTH = CAST(YEAR(a.PICKUP_DT) as varchar(4))+''+dbo.FN_VARMONTH(a.PICKUP_DT) AND SUBSTRING(c.PO_NO, 1,3) = a.LP_CD AND (c.PO_STATUS_FLAG = 'PO4' OR c.PO_STATUS_FLAG = 'PO6') 
		--LEFT JOIN TB_R_DLV_PO_D d ON d.PO_NO = b.PO_NO AND d.PO_ITEM_NO = b.PO_ITEM_NO
		--WHERE a.PROCESS_ID = @pid AND a.DELIVERY_STS IN ('Approved', 'Error Posting') AND c.PROD_MONTH = CAST(YEAR(GETDATE()) as varchar(4))+''+dbo.FN_VARMONTH(GETDATE())
		
		--modif by agung 03.06.2014
		SELECT 
			a.DELIVERY_NO, 
			@pid as PROCESS_ID, 
			a.[ROUTE], 
			a.RATE, 
			CASE 
				WHEN a.ROUTE IS NULL 
					THEN '' ELSE (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM 
									WHERE FUNCTION_ID = '31302' 
										  AND SYSTEM_CD = 'TRANS_CD_SA_POSTING') 
			END as TRANSACTION_CD,
			a.PICKUP_DT, 
			a.LP_CD, 
			GETDATE() as POSTING_DT, 
			CASE 
				WHEN b.ROUTE_CD IS NULL 
					THEN 'NG' ELSE 'OK' 
			END AS OK_FLAG, 
			@UserId as CREATED_BY, 
			GETDATE() as CREATED_DT, 
			b.PO_NO
		FROM TB_R_DELIVERY_CTL_H a
			LEFT JOIN TB_R_DLV_PO_ITEM b 
				ON b.ROUTE_CD = a.ROUTE 
				   AND a.LP_CD = SUBSTRING(b.PO_NO,1,3) 
				   AND (SELECT PROD_MONTH FROM TB_R_DLV_PO_H 
							WHERE PO_NO = b.PO_NO) = CAST(YEAR(a.PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(a.PICKUP_DT) 
				   --AND (SELECT PO_STATUS_FLAG FROM TB_R_DLV_PO_H 
							--WHERE PO_NO = b.PO_NO) = 'PO4'
		WHERE a.PROCESS_ID = @pid 
			  AND a.DELIVERY_STS IN ('Approved', 'Error Posting')
	)tbl;
	
	DECLARE @okf2 as varchar(max) = '' 
	DECLARE @count_rc as int = 0
	DECLARE @pin2 as int = 0
	DECLARE @last_po_item as int = 0
	DECLARE @pm as varchar(6) = ''
	DECLARE @FINAL_PRICE NUMERIC(15,2) = 0
	DECLARE @AMOUNT_PRICE NUMERIC(15,2) = 0
	DECLARE @NEW_CHECK_PO VARCHAR(MAX)
	DECLARE @OPQTY as int = 0
	DECLARE @SCHEMA_CD varchar(max)
	DECLARE @CONDITION_CATEGORY VARCHAR(1),
			@COMP_PRICE_CD VARCHAR(4),
			@CALCULATION_TYPE VARCHAR(1),
			@BASE_VALUE_TO VARCHAR(1),
			@BASE_VALUE_FROM VARCHAR(1),
			@PLUS_MINUS_FLAG VARCHAR(1), 
			@RTP_TAX_CD VARCHAR(2)
	DECLARE @ROUTE_PRICE INT,
			@BASE_PRICE BIGINT
	DECLARE @PRICE NUMERIC(15,2)
	DECLARE @COMP_PRICE NUMERIC(15,2)
	DECLARE @PERSEN2 NUMERIC(15,2)
	DECLARE @SET_PRICE NUMERIC(15,2)
	DECLARE @ERROR_SA VARCHAR(MAX)

	DECLARE CTLH_CURS CURSOR
	FOR
		SELECT 
			DELIVERY_NO, 
			PROCESS_ID, 
			[ROUTE], 
			RATE, 
			TRANSACTION_CD, 
			PICKUP_DT, 
			LP_CD, 
			POSTING_DT, 
			OK_FLAG, 
			CREATED_BY, 
			CREATED_DT, 
			PO_NO 
		FROM #CTL_H;
	OPEN CTLH_CURS;
	FETCH NEXT FROM CTLH_CURS 
		INTO @DELIVERY_NO, @PROCESS_ID, @ROUTE, @RATE, @TRANSACTION_CD, @PICKUP_DT, @LP_CD, @POSTING_DT, @OK_FLAG, @CREATED_BY, @CREATED_DT, @PO_NO;
	WHILE @@FETCH_STATUS = 0  
	BEGIN
		
		BEGIN TRY

			IF OBJECT_ID('tempdb..#TB_CHECK') IS NOT NULL 
				DROP TABLE #TB_CHECK

			SET @okf2 = ''

			SET @log = '[' + @DELIVERY_NO + '] Checking PO and Price Started'
			INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
				
			SET @OK_FLAG = (
				SELECT CASE WHEN b.ROUTE_CD IS NULL THEN 'NG' ELSE 'OK' END AS OK_FLAG
				FROM TB_R_DELIVERY_CTL_H a
					LEFT JOIN TB_R_DLV_PO_ITEM b 
						ON b.ROUTE_CD = a.ROUTE 
						   AND a.LP_CD = SUBSTRING(b.PO_NO,1,3) 
						   AND (SELECT PROD_MONTH FROM TB_R_DLV_PO_H 
									WHERE PO_NO = b.PO_NO) = CAST(YEAR(a.PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(a.PICKUP_DT) 
						   AND (SELECT PO_STATUS_FLAG FROM TB_R_DLV_PO_H 
									WHERE PO_NO = b.PO_NO) = 'PO4'
				WHERE a.PROCESS_ID = @pid 
					  AND a.DELIVERY_STS IN ('Approved', 'Error Posting') 
					  AND a.DELIVERY_NO = @DELIVERY_NO
			)
				
			IF (@OK_FLAG = 'NG') 
			BEGIN --JIKA STATUS HASIL SELECTION NG

				SET @log = '[' + @DELIVERY_NO + '] PO Item is Not Found. Start Checking PO.'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				--MENCARI PRICE DI ROUTE PRICE DENGAN ROUTE TERSEBUT
				SET @count_rc = (SELECT COUNT(ROUTE_CD) FROM TB_M_ROUTE_PRICE 
												WHERE ROUTE_CD = @ROUTE 
													  AND LP_CD = @LP_CD 
													  AND (GETDATE() BETWEEN VALID_FROM AND VALID_TO)) 
											
				IF(@count_rc > 0) 
				BEGIN --JIKA PRICE ADA

					SET @log = '[' + @DELIVERY_NO + '] Price Found in Master Route Price.'
					INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

					SET @last_po_item = (SELECT TOP 1 PO_ITEM_NO FROM TB_R_DLV_PO_ITEM 
														WHERE PO_NO = @PO_NO ORDER BY PO_ITEM_NO DESC)
					SET @pm = (SELECT CAST(YEAR(PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(PICKUP_DT) 
														FROM TB_R_DELIVERY_CTL_H WHERE DELIVERY_NO = @DELIVERY_NO)
					SET @FINAL_PRICE = 0
					SET @AMOUNT_PRICE = 0
					SET @pin2 = @last_po_item + 1
												
					--MODIF BY AGUNG -- MENAMBAHKAN PENGECEKAN PO STATUS
					SET @NEW_CHECK_PO = (SELECT DISTINCT PO_STATUS_FLAG 
											FROM TB_R_DLV_PO_H WHERE PO_NO = @PO_NO)

					IF (@NEW_CHECK_PO = 'PO4') 
					BEGIN
						SET @okf2 = 'OK'
						SET @isNotGood = 0
					END
					ELSE 
					BEGIN
						SET @okf2 = 'NG'
						SET @isNotGood = 1
						SET @log = '[' + @DELIVERY_NO + '] PO Number ' + @PO_NO + ' Is Not Released.';
						INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;			
					END
					--END MODIF
				
					DECLARE @r_count as int = (SELECT COUNT(*) FROM TB_R_DLV_PO_ITEM 
													WHERE ROUTE_CD = @ROUTE 
														  AND SUBSTRING(PO_NO, 1,3) = @LP_CD 
														  AND PO_NO = @PO_NO); --MENCARI ROUTE DI PO
												
					IF (@r_count = 0) 
					BEGIN --JIKA ROUTE TIDAK DITEMUKAN DI PO

						SET @log = '[' + @DELIVERY_NO + '] Route ' + @ROUTE + ' Not Found in PO Item with PO NO ' + @PO_NO + '. Start Checking PR.'
						INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

						SET @pr = (SELECT DISTINCT COUNT(b.ROUTE_CD) as cnt 
									FROM TB_R_DLV_PR_H a 
										LEFT JOIN TB_R_DLV_PR_D b ON b.PR_NO = a.PR_NO
									WHERE a.PR_STATUS_FLAG = 'PR5' 
										  AND PROD_MONTH = (SELECT CAST(YEAR(@PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(@PICKUP_DT))
										  AND a.LP_CD = @LP_CD
										  AND b.ROUTE_CD = @ROUTE)
										  --AND b.PO_NO = @PO_NO) -- MAKA AKAN MELAKUKAN PENCARIAN ROUTE KE PR
															
						IF(@pr > 0) 
						BEGIN --JIKA DI PR ROUTE TERSEBUT ADA

							SET @log = '[' + @DELIVERY_NO + '] Route Found in PR with Prod. Month ' + (SELECT CAST(YEAR(@PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(@PICKUP_DT)) 
									   + ', LP Code ' + @LP_CD + ', Route Code ' + @ROUTE + '.'
							INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

							SELECT 
								ROUTE_CD, 
								PO_NO, 
								PR_NO, 
								PR_QTY, 
								PR_ITEM_NO 
							INTO #TB_CHECK FROM(
							SELECT DISTINCT 
								b.ROUTE_CD, 
								b.PO_NO, 
								a.PR_NO, 
								b.PR_QTY, 
								b.PR_ITEM_NO 
							FROM TB_R_DLV_PR_H a 
								LEFT JOIN TB_R_DLV_PR_D b 
									ON b.PR_NO = a.PR_NO
							WHERE a.PR_STATUS_FLAG = 'PR5' 
								  AND PROD_MONTH = (SELECT CAST(YEAR(@PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(@PICKUP_DT))
								  AND a.LP_CD = @LP_CD
								  AND b.ROUTE_CD = @ROUTE
							)tbl;
																			
							IF (@PO_NO IS NULL)
							BEGIN

								SET @log = '[' + @DELIVERY_NO + '] Get PO No From PO Header with Prod. Month ' + (SELECT CAST(YEAR(@PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(@PICKUP_DT)) 
										   + ' and LP Code ' + @LP_CD + '.'
								INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

								SET @PO_NO = (SELECT PO_NO FROM TB_R_DLV_PO_H 
												WHERE PROD_MONTH = (SELECT CAST(YEAR(@PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(@PICKUP_DT))
													  AND LP_CD = @LP_CD
													  AND PO_STATUS_FLAG = 'PO4')
								SET @last_po_item = (SELECT TOP 1 PO_ITEM_NO FROM TB_R_DLV_PO_ITEM 
														WHERE PO_NO = @PO_NO 
														ORDER BY PO_ITEM_NO DESC)
								SET @pin2 = @last_po_item + 1
							END
																			
							SET @OPQTY = (SELECT PR_QTY FROM #TB_CHECK)

							IF (ISNULL((SELECT PO_QTY FROM TB_R_DLV_PO_ITEM 
											WHERE PO_NO = @PO_NO 
												  AND ROUTE_CD = @ROUTE),0) = 0) --CHECK PR QUANTITY
							BEGIN
								SET @log = '[' + @DELIVERY_NO + '] Create Po Item For Po No ' + @PO_NO + ' and Route ' + @ROUTE + '.'
								INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

								INSERT INTO TB_R_DLV_PO_ITEM (
									PO_NO, 
									PO_ITEM_NO, 
									ROUTE_CD, 
									PO_QTY,
									SA_QTY, 
									OPEN_QTY, 
									PR_NO, 
									PR_ITEM_NO, 
									CREATED_BY, 
									CREATED_DT)
								SELECT 
									@PO_NO as PO_NO, 
									@pin2 as PO_ITEM_NO,
									@ROUTE as ROUTE_CD,
									(SELECT PR_QTY FROM #TB_CHECK) as PO_QTY, 
									0 as SA_QTY, 
									(SELECT PR_QTY FROM #TB_CHECK) as OPEN_QTY, 
									(SELECT PR_NO FROM #TB_CHECK) as PR_NO, 
									(SELECT PR_ITEM_NO FROM #TB_CHECK) as PR_ITEM_NO,
									@UserId as CREATED_BY, 
									GETDATE() as CREATED_DT
							END
																			
							---MEMULAI CALCUTAION SCHEME SAMA DENGAN DI PO -====================================================================================================================================================================================================
							SET @SCHEMA_CD = (SELECT CALCULATION_SCHEMA_CD FROM TB_M_TRUCKING_SCHEME 
												WHERE LP_CD = @LP_CD 
													  AND VALID_TO >= GETDATE());
									
							SET @log = '[' + @DELIVERY_NO + '] Preparing Calculation Scheme with LP Code ' + @LP_CD + '.'
							INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
													
							DECLARE C_CALCULATE CURSOR FOR
								--SELECT CONDITION_CATEGORY,COMP_PRICE_CD,CALCULATION_TYPE,BASE_VALUE_TO,BASE_VALUE_FROM,PLUS_MINUS_FLAG FROM TB_M_CALC_SCHEME WHERE CALCULATION_SCHEMA_CD = @SCHEMA_CD ORDER BY SEQ_NO
								SELECT 
									C.CONDITION_CATEGORY, 
									C.COMP_PRICE_CD, 
									C.CALCULATION_TYPE, 
									C.BASE_VALUE_TO, 
									C.BASE_VALUE_FROM, 
									C.PLUS_MINUS_FLAG, 
									CASE 
										WHEN C.CONDITION_CATEGORY = 'H' 
										THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM 
												WHERE SYSTEM_CD = 'RTP_BASE_TAX_CD') 
										ELSE R.TAX_CD 
									END AS TAX_CD
								FROM TB_M_CALC_SCHEME C
									LEFT JOIN TB_M_DLV_COMP_PRICE_RATE R 
										ON R.COMP_PRICE_CD = C.COMP_PRICE_CD
								WHERE CALCULATION_SCHEMA_CD = @SCHEMA_CD 
								ORDER BY SEQ_NO
							OPEN C_CALCULATE
							FETCH NEXT FROM C_CALCULATE 
								INTO @CONDITION_CATEGORY, @COMP_PRICE_CD, @CALCULATION_TYPE, @BASE_VALUE_TO, @BASE_VALUE_FROM, @PLUS_MINUS_FLAG, @RTP_TAX_CD
							WHILE @@FETCH_STATUS = 0
							BEGIN
							
								SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Starting Calculation Scheme.'
								INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

								SET @ROUTE_PRICE = 0								
								--SET @ROUTE_PRICE = (SELECT PRICE FROM TB_M_ROUTE_PRICE WHERE LP_CD = @LP_CD AND ROUTE_CD = @ROUTE AND VALID_TO >= GETDATE())
								SET @ROUTE_PRICE = (SELECT PRICE FROM TB_M_ROUTE_PRICE 
														WHERE LP_CD = @LP_CD 
															   AND ROUTE_CD = @ROUTE 
															   AND (GETDATE() BETWEEN VALID_FROM AND VALID_TO))
																									
								--CHECK CONDITION CATEGORY
								IF(@CONDITION_CATEGORY = 'H')
								BEGIN
									SET @BASE_PRICE = @ROUTE_PRICE
									SET @PRICE = @BASE_PRICE;
								END
								ELSE 
								BEGIN

									SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Get Data From Master Component Price.'
									INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
								
									SET @COMP_PRICE = (SELECT COMP_PRICE FROM TB_M_DLV_COMP_PRICE_RATE WHERE COMP_PRICE_CD = @COMP_PRICE_CD);
															
									IF (@CALCULATION_TYPE = '3') 
									BEGIN
										SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Calculation Type is 3 With Base Value To : ' + @BASE_VALUE_TO + '.'
										INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

										IF (ISNULL(@BASE_VALUE_TO,'') = '') 
										BEGIN
											SET @PRICE = 0
											SET @PRICE = (@COMP_PRICE/100) * @BASE_PRICE
										END
										ELSE 
										BEGIN
											SET @PRICE = 0
											---------------------------------
										
											--IF (ISNULL(@BASE_VALUE_TO,'') = '') BEGIN
											--SET @PERSEN2 =(SELECT SUM(pr.COMP_PRICE/100) * SUM(@BASE_PRICE)
											--			   FROM TB_M_CALC_SCHEME cs 
											--			   JOIN TB_M_DLV_COMP_PRICE_RATE pr
											--			   ON cs.COMP_PRICE_CD=pr.COMP_PRICE_CD
											--			   WHERE --cs.SEQ_NO >=@BASE_VALUE_FROM AND cs.SEQ_NO<= @BASE_VALUE_TO AND 
											--			   cs.CALCULATION_SCHEMA_CD = @SCHEMA_CD);
											--	--SELECT @PERSEN2 as PERSEN2_FIRST
											--END
											--ELSE BEGIN
											SET @PERSEN2 =
												(SELECT SUM((CASE WHEN ISNULL(pr.COMP_PRICE,0) = 0 THEN 0 ELSE pr.COMP_PRICE END)/100) * SUM(@BASE_PRICE)
													 FROM TB_M_CALC_SCHEME cs 
														LEFT JOIN TB_M_DLV_COMP_PRICE_RATE pr
															ON cs.COMP_PRICE_CD=pr.COMP_PRICE_CD
													  WHERE cs.SEQ_NO >=@BASE_VALUE_FROM 
															AND cs.SEQ_NO<= @BASE_VALUE_TO 
															AND cs.CALCULATION_SCHEMA_CD = @SCHEMA_CD);
											--END
																															   
											SET @PRICE = @PERSEN2 + @BASE_PRICE;
																												
											--SET @PERSEN2 =(SELECT SUM(pr.COMP_PRICE/100) * SUM(@BASE_PRICE)
											--			   FROM TB_M_CALC_SCHEME cs 
											--			   JOIN TB_M_DLV_COMP_PRICE_RATE pr
											--			   ON cs.COMP_PRICE_CD=pr.COMP_PRICE_CD
											--			   WHERE cs.SEQ_NO >=@BASE_VALUE_FROM AND cs.SEQ_NO<= @BASE_VALUE_TO AND 
											--			   cs.CALCULATION_SCHEMA_CD = @SCHEMA_CD);
											--	--SELECT @PERSEN2 as PERSEN2_SECOND
											----END
																																   
											--SET @PRICE = @PERSEN2 + @BASE_PRICE;
											--SELECT @PRICE

											--KALIKAN DENGAN PERSENNYA DIA SENDIRI
											SET @PRICE = (@COMP_PRICE/100) * @PRICE;
											--PRINT 'PRICE PRECENTAGE: ' + CONVERT(VARCHAR(100),@PRICE);			
											----------------------
										END
																												
										--PRINT 'PLUS MINUS: ' + CONVERT(VARCHAR(100),@PLUS_MINUS_FLAG);
																												
										IF(@PLUS_MINUS_FLAG = '1') -- INI DARI UCUP
										BEGIN
											SET @FINAL_PRICE = @FINAL_PRICE + @PRICE
										END
										ELSE
										BEGIN
											SET @FINAL_PRICE = @FINAL_PRICE - @PRICE
										END
																												
										SET @AMOUNT_PRICE = @BASE_PRICE + @FINAL_PRICE															
										----INSERT INTO PO_ITEM ------
										--SELECT ROUND(@AMOUNT_PRICE,2) as PRICE, @FINAL_PRICE
														
										SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Update Price to PO Item With PO No ' + @PO_NO + '.'
										INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
																												
										UPDATE TB_R_DLV_PO_ITEM 
											SET PRICE = ROUND(@BASE_PRICE,2) 
										WHERE PO_NO = @PO_NO 
											  AND PO_ITEM_NO = @pin2
									END																			
									ELSE 
									BEGIN
										SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Calculation Type is ' + @CALCULATION_TYPE + ' Set Price = Base Price.'
										INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

										SET @PRICE = @BASE_PRICE;
									END																			
								END

								-- UPDATE INTO PR_D -------
								UPDATE TB_R_DLV_PR_D 
									SET PO_NO = @PO_NO,
										PO_FLAG ='Y',
										CHANGED_BY = @UserId, 
										CHANGED_DT = GETDATE() 
								WHERE PR_NO = (SELECT PR_NO FROM #TB_CHECK) 
									  AND PR_ITEM_NO = (SELECT PR_ITEM_NO FROM #TB_CHECK)
								-- INSERT INTO TABLE PO_D

								SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Update PO No and PO FLag in PR Detail with PR No ' + (SELECT PR_NO FROM #TB_CHECK) 
											+ ' And PR Item ' + (SELECT CONVERT(VARCHAR(MAX), PR_ITEM_NO) FROM #TB_CHECK) + ' .'
								INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

								SET @SET_PRICE = 0

								IF(@PLUS_MINUS_FLAG = '1')
								BEGIN
									SET @SET_PRICE = @PRICE
								END
								ELSE BEGIN
									SET @SET_PRICE = @PRICE * (-1)
								END
																										
								INSERT INTO TB_R_DLV_PO_D (
									PO_NO,
									PO_ITEM_NO,
									COMP_PRICE_CD,
									BASE_VALUE_FROM,
									BASE_VALUE_TO,
									CALCULATION_TYPE,
									PLUS_MINUS_FLAG,
									ACCRUAL_POSTING_FLAG,
									CONDITION_CATEGORY,
									PRICE,
									PR_NO,
									PR_ITEM_NO, 
									TAX_CD, 
									PROCESS_ID,
									CREATED_BY,
									CREATED_DT)
								SELECT 
									@PO_NO,
									@pin2,
									@COMP_PRICE_CD,
									@BASE_VALUE_FROM,
									@BASE_VALUE_TO,
									@CALCULATION_TYPE,
									@PLUS_MINUS_FLAG,
									'Y',
									@CONDITION_CATEGORY,
									@SET_PRICE,
									(SELECT PR_NO FROM #TB_CHECK), 
									(SELECT PR_ITEM_NO FROM #TB_CHECK), 
									@RTP_TAX_CD AS TAX_CD, 
									@pid,@UserId,GETDATE()
									--VALUES(@LAST_PO,@PO_ITEM,@COMP_PRICE_CD,@BASE_VALUE_FROM,@BASE_VALUE_TO,@CALCULATION_TYPE,@PLUS_MINUS_FLAG,'Y',@CONDITION_CATEGORY,@SET_PRICE,@PR_NO,@PR_ITEM,@pid,@USER,GETDATE())
								-- END INSERT

								SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Insert Data Into PO Detail.'
								INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

								SET @log = '[' + @DELIVERY_NO + ', ' + @COMP_PRICE_CD + '] Calculation Scheme Finished.'
								INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

								FETCH NEXT FROM C_CALCULATE 
									INTO @CONDITION_CATEGORY,@COMP_PRICE_CD,@CALCULATION_TYPE,@BASE_VALUE_TO,@BASE_VALUE_FROM,@PLUS_MINUS_FLAG,@RTP_TAX_CD
							END
							CLOSE C_CALCULATE
							DEALLOCATE C_CALCULATE
																						
							SET @FINAL_PRICE = 0
																						
							--END INSERT PO D ==================================================================================================================================================================================================================================
							UPDATE TB_R_DLV_PO_H 
								SET PO_AMOUNT = (SELECT SUM(PO_QTY * PRICE) 
													FROM TB_R_DLV_PO_ITEM 
													WHERE PO_NO = @PO_NO), 
								CHANGED_BY = @UserId, 
								CHANGED_DT = GETDATE() 
							WHERE PO_NO = @PO_NO
						
							SET @log = '[' + @DELIVERY_NO + '] Update Po Amount in PO Header.'
							INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

							DROP TABLE #TB_CHECK
							---AKHIR CALCUTAION SCHEME SAMA DENGAN DI PO -====================================================================================================================================================================================================
						END
						ELSE 
						BEGIN --JIKA ROUTE DI PR PUN TIDAK ADA
							SET @log = '[' + @DELIVERY_NO + '] Route ' + @ROUTE + ' Not Found in PR.'
							INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
						
							SET @okf2 = 'NG'
							SET @isNotGood = 1
						END
					END				
				END
				ELSE 
				BEGIN --JIKA PRICE TIDAK DITEMUKAN
					SET @log = '[' + @DELIVERY_NO + '] Price Not Found in Route Price Master.'
					INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;

					SET @okf2 = 'NG'
					SET @isNotGood = 1
				END
			END
			ELSE 
			BEGIN --JIKA HASIL SELECTION OK
				SET @log = '[' + @DELIVERY_NO + '] PO Item Found.'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				SET @okf2 = 'OK'
				SET @isNotGood = 0
			END
				
			DELETE FROM TB_T_DLV_SA_RAW WHERE DELIVERY_NO = @DELIVERY_NO 
		
			INSERT INTO TB_T_DLV_SA_RAW (
				DELIVERY_NO, 
				PROCESS_ID, 
				ROUTE_CD, 
				RATE, 
				TRANSACTION_CD, 
				PICKUP_DT, 
				LP_CD, 
				POSTING_DT, 
				OK_FLAG, 
				CREATED_BY, 
				CREATED_DT)
			SELECT 
				@DELIVERY_NO, 
				@PROCESS_ID, 
				@ROUTE, 
				@RATE, 
				@TRANSACTION_CD, 
				@PICKUP_DT, 
				@LP_CD, 
				@POSTING_DT, 
				@okf2, 
				@CREATED_BY,
				@CREATED_DT
		
			SET @log = '[' + @DELIVERY_NO + '] Cleaning Temporary SA RAW Table and Insert Data into SA RAW Table.'
			INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

		END TRY
		BEGIN CATCH
			
			SET @log = '[' + @DELIVERY_NO + '] Error : ' + ERROR_MESSAGE() + ' At Line ' + CONVERT(VARCHAR(MAX), ERROR_LINE()) + '.'
			INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
			
			UPDATE TB_R_DELIVERY_CTL_H
				SET DELIVERY_STS = 'Error Posting'
			WHERE DELIVERY_NO = @DELIVERY_NO 

			DELETE FROM TB_T_DLV_SA_RAW WHERE PROCESS_ID = @pid AND DELIVERY_NO = @DELIVERY_NO
			--UPDATE TB_T_DLV_SA_RAW
			--	SET OK_FLAG = 'NG'
			--WHERE PROCESS_ID = @pid
			--	  AND DELIVERY_NO = @DELIVERY_NO
			DELETE FROM TB_T_DLV_ERROR_POSTING WHERE DELIVERY_NO = @DELIVERY_NO;
			INSERT INTO TB_T_DLV_ERROR_POSTING (
					DELIVERY_NO, 
					PROCESS_ID, 
					ROUTE_PRICE_FLAG, 
					ROUTE_PRICE_DESC, 
					ROUTE_MASTER_FLAG, 
					LP_MASTER_FLAG, 
					SYSTEM_FLAG, 
					ROUTE_MASTER_DESC, 
					LP_MASTER_DESC, 
					SYSTEM_DESC, 
					CREATED_BY, 
					CREATED_DT)
				SELECT 
					@DELIVERY_NO, 
					@pid, 
					'0', 
					'', 
					'0', 
					'0', 
					'1', 
					'',
					'',
					'Query Error', 
					@UserId, 
					GETDATE()

			SET @log = '[' + @DELIVERY_NO + '] Updating Delivery Status to Error Posting.'
			INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;

			IF CURSOR_STATUS('global','C_CALCULATE') >= -1
            BEGIN
				CLOSE C_CALCULATE
				DEALLOCATE C_CALCULATE
            END

		END CATCH

	FETCH NEXT FROM CTLH_CURS 
		INTO @DELIVERY_NO, @PROCESS_ID, @ROUTE, @RATE, @TRANSACTION_CD, @PICKUP_DT, @LP_CD, @POSTING_DT, @OK_FLAG, @CREATED_BY, @CREATED_DT, @PO_NO;	
	END;
	CLOSE CTLH_CURS;
	DEALLOCATE CTLH_CURS;

	DROP TABLE #CTL_H;
		
	SET @log = 'Preparing SA Journal Posting.'
	INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;

	--INSERT INTO TB_R_DLV_SERVICE_DOC(SERVICE_DOC_NO, SERVICE_DOC_YEAR, DOC_DT, LP_CD, ROUTE_CD, TRANSACTION_QTY, TRANSACTION_CD, TRANSACTION_VALUE, TRANSACTION_CURR, SYSTEM_SOURCE, REFF_NO, PROCESS_ID, CREATED_BY, CREATED_DT)
	select 
		dlv_no, 
		p_no, 
		pmt, 
		l_cd, 
		r_cd, 
		tqty, 
		tcd, 
		tsign,
		tvalue, 
		tcurr, 
		sys_s, 
		rn, 
		crb, 
		crd, 
		ok 
	into #LoopRaw from(
		--SELECT DISTINCT a.DELIVERY_NO as dlv_no, b.PO_NO as p_no, b.PROD_MONTH as pmt, a.LP_CD as l_cd, a.ROUTE_CD as r_cd, '1' as tqty,(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '31302' AND SYSTEM_CD = 'TRANS_CD_SA_POSTING') as tcd, '+' as tsign, c.PRICE as tvalue, 'IDR' as tcurr, 'IPPCS_RTP' as sys_s, a.DELIVERY_NO as rn, @UserId as crb, GETDATE() as crd, a.OK_FLAG as ok FROM TB_T_DLV_SA_RAW a
		--LEFT JOIN TB_R_DLV_PO_H b ON b.PROD_MONTH = CAST(YEAR(a.PICKUP_DT) as varchar(4))+''+dbo.FN_VARMONTH(a.PICKUP_DT) AND b.LP_CD = a.LP_CD
		--LEFT JOIN TB_R_DLV_PO_ITEM c ON c.PO_NO = b.PO_NO AND c.ROUTE_CD = a.ROUTE_CD
		--WHERE a.PROCESS_ID = @pid AND b.RELEASED_FLAG = 'Y' AND b.PO_STATUS_FLAG ='PO4' 
		
		--MODIF BY AGUNG 03.07.2014	
		SELECT 
			a.DELIVERY_NO as dlv_no, 
			b.PO_NO as p_no, 
			(SELECT PROD_MONTH FROM TB_R_DLV_PO_H 
				WHERE PO_NO = b.PO_NO) as pmt, 
			a.LP_CD as l_cd, 
			a.ROUTE_CD as r_cd, 
			'1' as tqty, 
			(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM 
				WHERE FUNCTION_ID = '31302' 
					  AND SYSTEM_CD = 'TRANS_CD_SA_POSTING') as tcd, 
			'+' as tsign, 
			b.PRICE as tvalue, 
			'IDR' as tcurr, 
			'IPPCS_RTP' as sys_s, 
			a.DELIVERY_NO as rn, 
			@UserId as crb, 
			GETDATE() as crd, 
			a.OK_FLAG as ok 
		FROM TB_T_DLV_SA_RAW a
			LEFT JOIN TB_R_DLV_PO_ITEM b 
				ON b.ROUTE_CD = a.ROUTE_CD 
				   AND a.LP_CD = SUBSTRING(b.PO_NO,1,3) 
				   AND (SELECT PROD_MONTH FROM TB_R_DLV_PO_H 
						WHERE PO_NO = b.PO_NO) = CAST(YEAR(a.PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(a.PICKUP_DT) 
					AND (SELECT PO_STATUS_FLAG FROM TB_R_DLV_PO_H 
						WHERE PO_NO = b.PO_NO) = 'PO4'
		WHERE a.PROCESS_ID = @pid 
		--END MODIF
	)tbl;

	DECLARE @stats char(2) = 'OK'

	DECLARE @dlv_no as varchar(13),
			@p_no as varchar(20),
			@pmt as varchar(6),
			@l_cd as varchar(4),
			@r_cd as varchar(4),
			@tqty as int, 
			@tcd as varchar(3), 
			@tsign as varchar(1), 
			@tvalue varchar(max), 
			@tcurr varchar(3), 
			@sys_s as varchar(20),
			@rn as varchar(13), 
			@crb as varchar(20), 
			@crd as datetime,
			@ok as varchar(max)

	DECLARE 
		@dno as varchar(13),
		@dc_dt as date,
		@pno as varchar(11),
		@prd_mnt as varchar(16),
		@lcd as varchar(3),
		@rcd as varchar(4),
		@t_qty as int,
		@t_cd as varchar(3),
		@t_sign as varchar(1),
		@t_curr as varchar(3),
		@sys_source as varchar(10),
		@reff_no as varchar(13),
		@cr_by as varchar(20),
		@cr_dt as datetime,
		@okf as varchar(2),
		@rf as varchar(1),
		@pin as int,
		@cpc as varchar(4),
		@pmf as varchar(2),
		@apf as varchar(1),
		@cc as varchar(1),
		@gamt as decimal(15,2),
		@grir_tax_cd as varchar(2)
		
	DECLARE BTMP_CURS CURSOR FOR 
		SELECT 
			dlv_no, 
			p_no, 
			pmt, 
			l_cd, 
			r_cd, 
			tqty, 
			tcd, 
			tsign, 
			tvalue, 
			tcurr, 
			sys_s, 
			rn, 
			crb, 
			crd, 
			ok 
		FROM #LoopRaw;
	OPEN BTMP_CURS;
	FETCH NEXT FROM BTMP_CURS 
		INTO @dlv_no, @p_no, @pmt, @l_cd, @r_cd, @tqty, @tcd, @tsign, @tvalue, @tcurr, @sys_s, @rn, @crb, @crd, @ok;
	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
		BEGIN TRY

			IF OBJECT_ID('tempdb..#LoopRaw2') IS NOT NULL 
				DROP TABLE #LoopRaw2

			SET @log = '[' + @dlv_no + '] SA Posting Journal Started.'
			INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
						
			--CREATE SERVICE DOC NUMBER==================================================================================
			SET @LAST_SERV_DOC = (SELECT TOP 1 SERVICE_DOC_NO FROM TB_R_DLV_SERVICE_DOC ORDER BY SERVICE_DOC_NO DESC)
			SET @Y  = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2)
			SET @M  = dbo.FN_VARMONTH(GETDATE())
			SET @D = dbo.FN_VARDAY(getdate())
						
			BEGIN
				SET @SERVICE_DOC_NO = @MODULE_ID+''+@Y;
				IF(@LAST_SERV_DOC<>'')
				BEGIN
					SET @CURR_Y = (SELECT SUBSTRING(@LAST_SERV_DOC, 2,2))	
					SET @CURR_M = (SELECT SUBSTRING(@LAST_SERV_DOC, 4,2))
					SET @CURR_D = (SELECT SUBSTRING(@LAST_SERV_DOC, 6,2))
					SET @DIGIT ='000000'
					SET @SEQS = (SELECT SUBSTRING(@LAST_SERV_DOC, 8, LEN(@LAST_SERV_DOC)))
					SET @ADDS = (SELECT CAST(CAST(@SEQS as INT)+1 AS VARCHAR(11)))
					IF(SUBSTRING(@LAST_SERV_DOC,1,7) <> ('3'+@Y+''+@M+''+@D))
						SET @SERVICE_DOC_NO = @MODULE_ID+''+@Y+''+@M+''+@D+'000001';
					ELSE
						SET @SERVICE_DOC_NO = @MODULE_ID+''+@CURR_Y+''+@CURR_M+''+@CURR_D+''+(SELECT SUBSTRING(@DIGIT, 1, LEN(@DIGIT)-LEN(@ADDS))+@ADDS);
				END
				ELSE
				BEGIN
					SET @SERVICE_DOC_NO = @MODULE_ID+''+@Y+''+@M+''+@D+'000001';
				END

				SET @log = '[' + @dlv_no + '] Creating New Service Doc. No ' + @SERVICE_DOC_NO + '.'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
			END
						
						
			IF (@ok <> 'NG') 
			BEGIN
				INSERT INTO TB_R_DLV_SERVICE_DOC (
					PROCESS_ID, 
					SERVICE_DOC_NO, 
					SERVICE_DOC_YEAR, 
					DOC_DT, 
					PO_NO, 
					PROD_MONTH, 
					LP_CD, 
					ROUTE_CD, 
					TRANSACTION_QTY, 
					TRANSACTION_CD, 
					TRANSACTION_SIGN, 
					TRANSACTION_VALUE, 
					TRANSACTION_CURR, 
					SYSTEM_SOURCE, 
					REFF_NO, 
					CREATED_BY, 
					CREATED_DT)
				SELECT 
					@pid, 
					@SERVICE_DOC_NO, 
					CAST(YEAR(DATEADD(MM,-3, GETDATE())) as VARCHAR(4)), 
					GETDATE(), 
					@p_no,
					@pmt,
					@l_cd,
					@r_cd,
					@tqty,
					@tcd,
					@tsign,
					@tvalue,
					@tcurr,
					@sys_s,
					@rn,
					@crb,
					@crd

				SET @log = '[' + @dlv_no + '] Insert Data To Service Doc. With Service Doc. No. ' + @SERVICE_DOC_NO
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
			
				SET @log = '[' + @dlv_no + '] Preparing Insert Data Into GR IR.'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
						
				--MASUK KE TABEL GR IR===========================================================================
				select 
					dno,
					dc_dt,
					pno,
					prd_mnt,
					lcd,
					rcd,
					t_qty,
					t_cd,
					t_sign,
					t_curr,
					sys_source,
					reff_no,
					cr_by,
					cr_dt,
					okf,
					rf,
					pin,
					cpc, 
					pmf, 
					apf, 
					cc, 
					gamt, 
					grir_tax_cd 
				into #LoopRaw2 from(
					--SELECT a.DELIVERY_NO as dno, GETDATE() as dc_dt, b.PO_NO as pno, b.PROD_MONTH as prd_mnt, b.LP_CD as lcd,  a.ROUTE_CD as rcd, '1' as t_qty, (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '31302' AND SYSTEM_CD = 'TRANS_CD_SA_POSTING') as t_cd,'+' as t_sign, 'IDR' as t_curr, 'IPPCS_RTP' as sys_source, a.DELIVERY_NO as reff_no, @UserId as cr_by , GETDATE() as cr_dt, a.OK_FLAG as okf, b.RELEASED_FLAG as rf, d.PO_ITEM_NO as pin, d.COMP_PRICE_CD as cpc, d.PLUS_MINUS_FLAG as pmf, d.ACCRUAL_POSTING_FLAG as apf, d.CONDITION_CATEGORY as cc, d.PRICE as gamt FROM TB_T_DLV_SA_RAW a
					--LEFT JOIN TB_R_DLV_PO_H b ON b.PROD_MONTH = CAST(YEAR(a.PICKUP_DT) as varchar(4))+''+dbo.FN_VARMONTH(a.PICKUP_DT) AND b.LP_CD = a.LP_CD 
					--LEFT JOIN TB_R_DLV_PO_ITEM c ON c.PO_NO = b.PO_NO AND c.ROUTE_CD = a.ROUTE_CD
					--LEFT JOIN TB_R_DLV_PO_D d ON d.PO_NO = b.PO_NO AND d.PO_ITEM_NO = c.PO_ITEM_NO
					--WHERE a.PROCESS_ID = @pid AND c.PO_NO = @p_no AND b.PO_STATUS_FLAG ='PO4' AND b.RELEASED_FLAG = 'Y' AND a.ROUTE_CD = @r_cd AND a.DELIVERY_NO = @dlv_no
											
					--modif by agung 03.07.2014
											
					SELECT 
						a.DELIVERY_NO AS dno, 
						GETDATE() as dc_dt, 
						b.PO_NO as pno, 
						(SELECT PROD_MONTH FROM TB_R_DLV_PO_H 
							WHERE PO_NO = b.PO_NO) as prd_mnt,
						a.LP_CD as lcd, 
						a.ROUTE_CD as rcd, 
						'1' as t_qty, 
						(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM 
							WHERE FUNCTION_ID = '31302' 
								  AND SYSTEM_CD = 'TRANS_CD_SA_POSTING') as t_cd,
						'+' as t_sign, 
						'IDR' as t_curr, 
						'IPPCS_RTP' as sys_source, 
						a.DELIVERY_NO as reff_no, 
						'system' as cr_by, 
						GETDATE() as cr_dt, 
						a.OK_FLAG as okf, 
						(SELECT RELEASED_FLAG FROM TB_R_DLV_PO_H WHERE PO_NO = b.PO_NO) as rf, 
						d.PO_ITEM_NO as pin, 
						d.COMP_PRICE_CD as cpc, 
						d.PLUS_MINUS_FLAG as pmf, 
						d.ACCRUAL_POSTING_FLAG as apf, 
						d.CONDITION_CATEGORY as cc, 
						d.PRICE as gamt,
						d.TAX_CD as grir_tax_cd
					FROM TB_T_DLV_SA_RAW a
						LEFT JOIN TB_R_DLV_PO_ITEM b 
							ON b.ROUTE_CD = a.ROUTE_CD 
							   AND a.LP_CD = SUBSTRING(b.PO_NO,1,3) 
							   AND (SELECT PROD_MONTH FROM TB_R_DLV_PO_H 
										WHERE PO_NO = b.PO_NO) = CAST(YEAR(a.PICKUP_DT) as varchar(4)) + '' + dbo.FN_VARMONTH(a.PICKUP_DT) 
							   AND (SELECT PO_STATUS_FLAG FROM TB_R_DLV_PO_H WHERE PO_NO = b.PO_NO) = 'PO4'
						LEFT JOIN TB_R_DLV_PO_D d 
							ON d.PO_NO = b.PO_NO 
							   AND d.PO_ITEM_NO = b.PO_ITEM_NO
					WHERE a.PROCESS_ID = @pid 
						  AND b.PO_NO = @p_no 
						  AND a.ROUTE_CD = @r_cd 
						  AND a.DELIVERY_NO = @dlv_no				

				) tbl;
										
				DECLARE BTMP_CURS2 CURSOR 
				FOR 
				SELECT dno,dc_dt,pno,prd_mnt,lcd,rcd,t_qty,t_cd,t_sign,t_curr,sys_source,reff_no,cr_by,cr_dt,okf,rf,pin,cpc,pmf,apf,cc,gamt, grir_tax_cd FROM #LoopRaw2;
					OPEN BTMP_CURS2;
				FETCH NEXT FROM BTMP_CURS2 
					INTO @dno,@dc_dt,@pno,@prd_mnt,@lcd,@rcd,@t_qty,@t_cd,@t_sign,@t_curr,@sys_source,@reff_no,@cr_by,@cr_dt,@okf,@rf,@pin,@cpc,@pmf,@apf,@cc,@gamt,@grir_tax_cd
				WHILE @@FETCH_STATUS = 0  
				BEGIN  
				
					SET @log = '[' + @dno + '] Create GR Started.'
					INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008INF', 'INF', @MODULE_ID, @FUNCTION_ID;
				
					IF(@okf = 'NG')
					BEGIN
						SET @DlvSts = 'Error Posting'
						SET @isPostedError = 'Y'

						SET @log = '[' + @dno + '] Cannot Find Price For Route Code : ' + @rcd;
						INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS00008ERR', 'INF', @MODULE_ID, @FUNCTION_ID;
											
						UPDATE TB_R_DELIVERY_CTL_H SET DELIVERY_STS = 'Error Posting' WHERE DELIVERY_NO = @dno
					END
												
					SET @a = (SELECT TOP 1 ITEM_NO FROM TB_R_DLV_GR_IR a 
								WHERE PO_NO = @pno 
									  AND SERVICE_DOC_NO = @SERVICE_DOC_NO 
								ORDER BY ITEM_NO DESC)
				
					IF(@a IS NULL)
					BEGIN
						SET @a = 0
					END
				
					SET @a = @a + 1;
											
					INSERT INTO TB_R_DLV_GR_IR (
						PO_NO, 
						PO_ITEM_NO, 
						SERVICE_DOC_NO, 
						SERVICE_DOC_YEAR, 
						DOC_DT, 
						GR_IR_AMT, 
						LP_CD, 
						COMP_PRICE_CD, 
						ACCRUAL_POSTING_FLAG, 
						PLUS_MINUS_FLAG, 
						REFF_NO, 
						CONDITION_CATEGORY, 
						PO_DETAIL_PRICE, 
						ITEM_CURR, 
						ITEM_NO, 
						STATUS_CD, 
						TAX_CD, 
						CREATED_BY, 
						CREATED_DT)
					SELECT 
						@pno, 
						@pin, 
						@SERVICE_DOC_NO, 
						CAST(YEAR(DATEADD(MM,-3, GETDATE())) as VARCHAR(4)), 
						@dc_dt, 
						@gamt, 
						@lcd, 
						@cpc, 
						@apf, 
						@pmf, 
						@reff_no, 
						@cc, 
						@gamt, 
						@t_curr, 
						@a, 
						'1', 
						@grir_tax_cd, 
						@UserId, 
						GETDATE()
													
					SET @log = '[' + @dno + '] Successfuly insert Component Price Code : ' + @cpc + ' into GR IR';
					INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS00004INF', 'INF', @MODULE_ID, @FUNCTION_ID;
									
					FETCH NEXT FROM BTMP_CURS2 INTO @dno,@dc_dt,@pno,@prd_mnt,@lcd,@rcd,@t_qty,@t_cd,@t_sign,@t_curr,@sys_source,@reff_no,@cr_by,@cr_dt,@okf,@rf,@pin,@cpc,@pmf,@apf,@cc,@gamt,@grir_tax_cd
				END;
				CLOSE BTMP_CURS2;
				DEALLOCATE BTMP_CURS2;

				DROP TABLE #LoopRaw2;
			END
			ELSE 
			BEGIN
				SET @log = '[' + @dlv_no + '] Error Posting For Route : ' + @r_cd + ', Status is NG, Check Between Production Month And Pickup Date Or Route And Route Price Not Find';
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.Process', @pid, 'MPCS000008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
			
				UPDATE TB_R_DELIVERY_CTL_H SET DELIVERY_STS = 'Error Posting' WHERE DELIVERY_NO = @dlv_no
			END
						
			IF (@ok = 'NG')
			BEGIN
				SET @log = '[' + @dlv_no + '] Posting SA Has Been Finished With Error : Cannot Find Price.';
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.End', @pid, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;

				UPDATE TB_R_DELIVERY_CTL_H SET DELIVERY_STS = 'Error Posting' WHERE DELIVERY_NO = @dlv_no
				DELETE FROM TB_T_DLV_ERROR_POSTING WHERE DELIVERY_NO = @dlv_no;
			
				INSERT INTO TB_T_DLV_ERROR_POSTING (
					DELIVERY_NO, 
					PROCESS_ID, 
					ROUTE_PRICE_FLAG, 
					ROUTE_PRICE_DESC, 
					ROUTE_MASTER_FLAG, 
					LP_MASTER_FLAG, 
					SYSTEM_FLAG, 
					ROUTE_MASTER_DESC, 
					LP_MASTER_DESC, 
					SYSTEM_DESC, 
					CREATED_BY, 
					CREATED_DT)
				SELECT 
					@dlv_no, 
					@pid, 
					'1', 
					'Cannot find price', 
					'0', 
					'0', 
					'0', 
					'',
					'',
					'', 
					@UserId, 
					GETDATE()			
			END
			ELSE
			BEGIN
				UPDATE TB_R_DLV_PO_ITEM 
					SET OPEN_QTY = CASE WHEN [OPEN_QTY] IS NULL THEN 0 WHEN [OPEN_QTY] > 0 THEN [OPEN_QTY] - 1 ELSE [OPEN_QTY] END, 
						[SA_QTY] = [SA_QTY] + 1 
				WHERE [PO_NO] =  @p_no AND [ROUTE_CD] = @r_cd
							
				--SET @Stats = ''
				SET @j_status = ''

				--INSERT INTO @TB_T_LOG_D 
				EXEC dbo.SP_DLV_POSTING_GR @UserId, @pid, @dlv_no, 'TRANS_CD_SA_POSTING', @j_status OUTPUT

				IF(ISNULL(@j_status, '') = 'ERROR')
				BEGIN
					RAISERROR('Error When Posting Journal', 16, 1)
				END
							
				--DECLARE @R_SEQ_NO INT = (SELECT TOP 1 SEQUENCE_NUMBER FROM TB_R_LOG_D WHERE PROCESS_ID = @pid ORDER BY SEQUENCE_NUMBER DESC)

				--INSERT INTO TB_R_LOG_D(PROCESS_ID, SEQUENCE_NUMBER, MESSAGE_ID, MESSAGE_TYPE, [MESSAGE], LOCATION, CREATED_BY, CREATED_DATE) 
				--SELECT PROCESS_ID, @R_SEQ_NO + ROW_NUMBER() OVER (ORDER BY PROCESS_ID), MESSAGE_ID, MESSAGE_TYPE, [MESSAGE], LOCATION, CREATED_BY, CREATED_DATE FROM @TB_T_LOG_D

				UPDATE TB_R_DELIVERY_CTL_H SET DELIVERY_STS = 'Posted' WHERE DELIVERY_NO = @dlv_no

				SET @log = '[' + @dlv_no + '] Posting SA Has Been Finished Successfuly.';
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.End', @pid, 'MPCS00003INF', 'INF', @MODULE_ID, @FUNCTION_ID;
			END
		
			--SET @SourceString = @SourceString + @dlv_no + ','
			DELETE FROM TB_T_DLV_SA_RAW WHERE PROCESS_ID = @pid AND DELIVERY_NO = @dlv_no
		END TRY
		BEGIN CATCH
			---DELETE TABLE-TABLE TERKAIT----
			DELETE FROM TB_R_DLV_FI_JOURNAL 
				WHERE DOC_NO IN(SELECT DOC_NO FROM TB_R_DLV_FI_TRANS_H 
									WHERE SUBSTRING(REFF_NO, 1, CASE WHEN CHARINDEX('-', REFF_NO) - 1 <= 0 THEN 0 ELSE CHARINDEX('-', REFF_NO) - 1 END)  = @SERVICE_DOC_NO)

			DELETE FROM TB_R_DLV_FI_TRANS_D 
				WHERE DOC_NO IN (SELECT DOC_NO FROM TB_R_DLV_FI_TRANS_H 
									WHERE SUBSTRING(REFF_NO, 1, CASE WHEN CHARINDEX('-', REFF_NO) - 1 <= 0 THEN 0 ELSE CHARINDEX('-', REFF_NO) - 1 END) = @SERVICE_DOC_NO)

			DELETE FROM TB_R_DLV_FI_TRANS_H 
				WHERE SUBSTRING(REFF_NO, 1, CASE WHEN CHARINDEX('-', REFF_NO) - 1 <= 0 THEN 0 ELSE CHARINDEX('-', REFF_NO) - 1 END) = @SERVICE_DOC_NO
			
			DELETE FROM TB_R_DLV_GR_IR WHERE SERVICE_DOC_NO = @SERVICE_DOC_NO
			
			DELETE FROM TB_R_DLV_SERVICE_DOC WHERE SERVICE_DOC_NO = @SERVICE_DOC_NO

			--DELETE FROM TB_T_DLV_SA_RAW WHERE PROCESS_ID = @pid AND  DELIVERY_NO = @dlv_no
			--END DELETE---------------------
		
			SET @log = '[' + @dlv_no + '] Error Posting SA. Rollback Data With Service Doc. No. ' + @SERVICE_DOC_NO + ' .';
			INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.End', @pid, 'MPCS00003ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;

			UPDATE TB_R_DELIVERY_CTL_H 
				SET DELIVERY_STS = 'Error Posting' 
			WHERE DELIVERY_NO = @dlv_no

			--UPDATE TB_R_DELIVERY_CTL_H 
			--	SET PROCESS_ID = NULL 
			--WHERE PROCESS_ID = @pid

			DELETE FROM TB_T_DLV_ERROR_POSTING WHERE DELIVERY_NO = @dlv_no;
			INSERT INTO TB_T_DLV_ERROR_POSTING (
					DELIVERY_NO, 
					PROCESS_ID, 
					ROUTE_PRICE_FLAG, 
					ROUTE_PRICE_DESC, 
					ROUTE_MASTER_FLAG, 
					LP_MASTER_FLAG, 
					SYSTEM_FLAG, 
					ROUTE_MASTER_DESC, 
					LP_MASTER_DESC, 
					SYSTEM_DESC, 
					CREATED_BY, 
					CREATED_DT)
				SELECT 
					@dlv_no, 
					@pid, 
					'0', 
					'', 
					'0', 
					'0', 
					'1', 
					'',
					'',
					'Query Error', 
					@UserId, 
					GETDATE()

			SET @stats = 'NG'

			SET @log = '[' + @dlv_no + '] Error Posting SA : ' + ERROR_MESSAGE() + ' At Line ' + CONVERT(VARCHAR(MAX), ERROR_LINE()) + ' .';
			INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.End', @pid, 'MPCS00003ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;

			IF CURSOR_STATUS('global','BTMP_CURS2') >= -1
            BEGIN
				CLOSE BTMP_CURS2
				DEALLOCATE BTMP_CURS2
            END

		END CATCH
	FETCH NEXT FROM BTMP_CURS 
		INTO @dlv_no, @p_no, @pmt, @l_cd, @r_cd, @tqty, @tcd, @tsign, @tvalue, @tcurr, @sys_s, @rn, @crb, @crd, @ok;
	END;
	CLOSE BTMP_CURS;
	DEALLOCATE BTMP_CURS;

	SET @log = 'SA Journal Posting Process Has Been Finished'
		INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.End', @pid, 'MPCS00003INF', 'INF', @MODULE_ID, @FUNCTION_ID;

	DROP TABLE #LoopRaw;
	
	UPDATE TB_R_DLV_SERVICE_DOC SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
	UPDATE TB_R_DELIVERY_CTL_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
	
	IF(@stats = 'OK')
	BEGIN
		SET @log = 'SA Posting Process Has Been Finished'
		INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptancePosting.End', @pid, 'MPCS00003INF', 'INF', @MODULE_ID, @FUNCTION_ID;

		UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU3', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
	END
	ELSE
	BEGIN
		UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE(), CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
	END
END

