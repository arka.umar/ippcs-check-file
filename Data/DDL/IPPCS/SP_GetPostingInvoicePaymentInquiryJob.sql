CREATE PROCEDURE [dbo].[SP_GetPostingInvoicePaymentInquiryJob]
			@ri_v_supp_cd				 varchar(6),
			@ri_v_supp_inv_no			 varchar(12),	
            @ri_d_posting_dt			 datetime, -- default 01.01.1900
            @ri_d_inv_dt				 datetime, -- default 01.01.1900
            @ri_d_baseline_dt			 datetime, -- default 01.01.1900
            @ri_v_pay_term_cd			 varchar(4), 
            @ri_v_pay_method_cd			 varchar(2), 
            @ri_v_withholding_tax_cd	 varchar(2), 
            @ri_n_base_amt				 numeric(16,5), 
            @ri_v_partner_bank			 varchar(10),
            @ri_n_calculate_tax			 int, 
            @ri_n_amount				 numeric(16,5),
            @ri_v_invoice_note			 varchar(max),
            @ri_v_header_text			 varchar(100),
            @ri_v_assignment			 varchar(12), 
            @ri_v_tax_cd				 varchar(2), 
            @ri_d_tax_dt			     datetime, -- default 01.01.1900
            @ri_v_gl_account			 varchar(10),
            @ri_n_ppv_amount			 numeric(16,5), 
			@ri_v_user					 varchar(max), 
			@ro_v_err_mesg				 varchar(2000) OUTPUT

AS

DECLARE @@WithHoldingRate				 numeric(5,2),
		@@TaxRate						 numeric(5,2),
		@@InvoiceNo						 varchar(16),
		@@Sum_GL_Account				 numeric(16,5),
		@@Sum_GL_Account_Tax			 numeric(16,5),
		@@SUM_INV_AMT					 numeric(16,2),
		@@l_n_process_status             smallint = 0

SELECT @@WithHoldingRate=ISNULL(RATE,0) FROM TB_M_WITHHOLDING_TAX WHERE WITHHOLDING_TAX_CD=@ri_v_withholding_tax_cd
SELECT @@TaxRate=CASE WHEN @ri_n_calculate_tax=1 THEN ISNULL(RATE,0) ELSE 0 END FROM TB_M_TAX WHERE TAX_CD=@ri_v_tax_cd

IF EXISTS(
	SELECT '' 
	FROM TB_R_ICS_QUEUE 
	WHERE IPPCS_MODUL='5' 
	  AND IPPCS_FUNCTION='53002' 
	  AND @ri_v_supp_inv_no  =
		(CASE WHEN NULLIF(REMARK, '') IS NULL THEN ''
		     WHEN CHARINDEX(';', REMARK) > 1 THEN  
				  SUBSTRING(ISNULL(REMARK, ''), 1, CHARINDEX(';', ISNULL(REMARK, ''))-1
				)
			ELSE ''
			END)
	  AND PROCESS_STATUS IN(0,1)
	  )
BEGIN
    SET @ro_v_err_mesg = 'Please wait until Progress Status is Completed';
	SET @@l_n_process_status = 2;
	-- RAISERROR('Please wait until Progress Status is Completed',16,1);
	RETURN @@l_n_process_status;
END
ELSE
BEGIN
	Begin Try
		--BEGIN TRANSACTION IPPCSPostingInvoice

		IF NOT EXISTS(SELECT '' FROM TB_T_INV_GL_ACCOUNT WHERE SUPP_INVOICE_NO=@ri_v_supp_inv_no and SUPP_CD=@ri_v_supp_cd)
		BEGIN
			INSERT INTO TB_T_INV_GL_ACCOUNT(
				INV_NO,
				SUPP_INVOICE_NO,
				GL_ACCOUNT,
				INV_AMT,
				COST_CENTER,
				TAX_CD,
				PROCESS_ID,
				SUPP_CD --added fid.deny 2015-06-03 (supp_cd cant be null)
			)
			SELECT
				'',
				@ri_v_supp_inv_no,
				@ri_v_gl_account,
				CONVERT(NUMERIC(16,5),ISNULL(@ri_n_ppv_amount,0)),
				COST_CENTER,
				TAX_CD,
				'',
				@ri_v_supp_cd --added fid.deny 2015-06-03 (supp_cd cant be null)
			FROM TB_M_LIV_TRANS_MAPPING
			WHERE GL_ACCOUNT=@ri_v_gl_account
		END
		ELSE
		BEGIN
			UPDATE TB_T_INV_GL_ACCOUNT SET
				INV_AMT=CONVERT(NUMERIC(16,5),ISNULL(@ri_n_ppv_amount,0))
			WHERE SUPP_INVOICE_NO=@ri_v_supp_inv_no AND GL_ACCOUNT=@ri_v_gl_account
		END

		SELECT
			@@Sum_GL_Account=SUM((CASE WHEN @ri_n_calculate_tax=1 THEN ISNULL(tx.RATE,0) ELSE 0 END)/1.00/100*CONVERT(NUMERIC(16,5),ISNULL(t.INV_AMT,0)) + CONVERT(NUMERIC(16,5),ISNULL(t.INV_AMT,0))),
			@@Sum_GL_Account_Tax=SUM((CASE WHEN @ri_n_calculate_tax=1 THEN ISNULL(tx.RATE,0) ELSE 0 END)/1.00/100*CONVERT(NUMERIC(16,5),ISNULL(t.INV_AMT,0)))
		FROM TB_T_INV_GL_ACCOUNT t
		INNER JOIN TB_M_TAX tx ON t.TAX_CD=tx.TAX_CD
		WHERE SUPP_INVOICE_NO=@ri_v_supp_inv_no

		SELECT @@SUM_INV_AMT = SUM(ISNULL(AMT,0))
		  FROM TB_R_INV_UPLOAD 
		 WHERE SUPP_INV_NO=@ri_v_supp_inv_no 
		       AND SUPP_CD=@ri_v_supp_cd  
			   AND (CONVERT(VARCHAR(8),@ri_d_inv_dt,112)='19000101' 
			         OR (CONVERT(VARCHAR(8),@ri_d_inv_dt,112)<>'19000101' 
			   AND CONVERT(VARCHAR(8),INV_DT,112)=CONVERT(VARCHAR(8),@ri_d_inv_dt,112)));

		INSERT INTO TB_R_INV_UPLOAD_POSTING_LOG
		SELECT * from TB_R_INV_UPLOAD
		WHERE SUPP_INV_NO=@ri_v_supp_inv_no AND SUPP_CD=@ri_v_supp_cd

		INSERT INTO TB_T_INV_GL_ACCOUNT_POSTING_LOG
		SELECT * FROM TB_T_INV_GL_ACCOUNT
		WHERE SUPP_INVOICE_NO = @ri_v_supp_inv_no

		UPDATE TB_R_INV_UPLOAD SET
			--SUPP_CD=@0,
			POSTING_DT=CASE WHEN @ri_d_posting_dt <> '1900-01-01' THEN @ri_d_posting_dt END,
			--INV_DT=@3,
			BASELINE_DT=CASE WHEN @ri_d_baseline_dt <> '1900-01-01' THEN @ri_d_baseline_dt END,
			PAY_TERM_CD=@ri_v_pay_term_cd,
			PAY_METHOD_CD=@ri_v_pay_method_cd,
			WITHHOLDING_TAX_CD=@ri_v_withholding_tax_cd,
			WITHHOLDING_TAX_AMT=ISNULL(@@WithHoldingRate,0)/1.00/100*CONVERT(NUMERIC(16,5),ISNULL(@ri_n_base_amt,0)),
			BASE_AMT=@ri_n_base_amt,
			INV_AMT_TOTAL=ISNULL(TURN_OVER,0) + ISNULL(@@Sum_GL_Account,0) + (ISNULL(@@TaxRate,0)/1.00/100*CONVERT(NUMERIC(16,2),ISNULL(TURN_OVER,0))),
			--BANK_KEY=@9,
			--SUPP_BANK_COUNTRY=,
			--SUPP_ACCOUNT=,
			SUPP_BANK_TYPE=@ri_v_partner_bank,
			--AMT=@11,
			CALC_TAX_FLAG=CASE WHEN @ri_n_calculate_tax=1 THEN 'Y' ELSE 'N' END,
			INV_TEXT=@ri_v_invoice_note,
			--INV_TAX_NO=@13,
			--INV_TAX_AMOUNT=ISNULL(@@TaxRate,0)/1.00/100*CONVERT(NUMERIC(16,2),ISNULL(TURN_OVER,0)) + CONVERT(NUMERIC(16,2),ISNULL(@@Sum_GL_Account_Tax,0)),
			INV_TAX_AMOUNT=ISNULL(@@TaxRate,0)/1.00/100*CONVERT(NUMERIC(16,2),ISNULL(TURN_OVER,0)) + CONVERT(NUMERIC(16,5),ISNULL(@@Sum_GL_Account_Tax,0)),
			ASSIGNMENT=@ri_v_assignment,
			TAX_CD=@ri_v_tax_cd,
			--INV_TAX_DT=@16,
			--TURN_OVER=ISNULL(TURN_OVER,0) + CONVERT(NUMERIC(16,5),@18)
			CHANGED_BY=@ri_v_user,
			CHANGED_DT=GETDATE()
		WHERE SUPP_INV_NO=@ri_v_supp_inv_no AND SUPP_CD=@ri_v_supp_cd AND (CONVERT(VARCHAR(8),@ri_d_inv_dt,112)='19000101' OR (CONVERT(VARCHAR(8),@ri_d_inv_dt,112)<>'19000101' AND CONVERT(VARCHAR(8),INV_DT,112)=CONVERT(VARCHAR(8),@ri_d_inv_dt,112)))
	
		-- COMMIT TRANSACTION IPPCSPostingInvoice
	END TRY

	Begin Catch
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE()
			
		set @ro_v_err_mesg = 'ERROR: spGetPostInvoiceByJob : ' + @ErrorMessage + ', at line = ' +  cast (@ErrorLine as varchar)
	    SET @@l_n_process_status = 2;
		
		return @@l_n_process_status;
	End Catch		
END

