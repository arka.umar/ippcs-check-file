-- =============================================
-- Author:		Rahmat Setiawan (Arkamaya)
-- Create date: 02.10.2013
-- Description:	Execute email notification for update supplier feedback time
-- =============================================
CREATE PROCEDURE [dbo].[SP_LPOPConfirmation_Email_OLD]
AS
BEGIN
	SET NOCOUNT ON;
	
    --DECLARE @TimeExecute DATETIME;

	--SELECT @TimeExecute = CAST(SUBSTRING(CAST(GETDATE() AS VARCHAR), 0, 12) AS DATETIME) + CAST(SYSTEM_VALUE AS DATETIME) FROM TB_M_SYSTEM WHERE FUNCTION_ID = '11005' AND SYSTEM_CD = 'LPOP_TIME_EXECUTION'

	--IF (@TimeExecute <= GETDATE())
	--BEGIN
		--=== Jika time server melewati time yang di setting (lebih besar)
		INSERT INTO TB_R_BACKGROUND_TASK_REGISTRY	
		SELECT 
			PROCESS_ID,
			'SupplierFeedbackTentativeTaskRuntime',		--NAME
			'Background Task Supplier Feedback',		--DESCRIPTION
			'System',			--SUBMITTER
			'UpdateDataGrid',	--FUNCTION_NAME
			'{&quot;productionMonth&quot;:&quot;' + PRODUCTION_MONTH + '&quot;,&quot;timing&quot;:&quot;' + LPOP_TIMING + '&quot;,&quot;tID&quot;:&quot;' + PROCESS_ID + '&quot;}',--PARAMETER
			0,		--TYPE
			0,		--STATUS
			'C:\Background_Task\Tasks\SuppFeedbackTentativeTaskRuntime.exe',--COMMAND
			0,      --START_DATE
			0,		--END_DATE
			4,		--PERIODIC_TYPE
			null,	--INTERVAL
			'',		--EXECUTION_DAYS
			'',		--EXECUTION_MONTHS
			6840000
		FROM TB_R_LPOP_CONFIRMATION
		WHERE LAST_REMINDING_DATE >= GETDATE() AND PROCESS_ID IS NOT NULL
				--AND
			  --((LAST_EXECUTION_DATE IS NULL) OR	
			  --(CAST(SUBSTRING(CAST(LAST_EXECUTION_DATE AS VARCHAR), 0, 12) AS DATETIME) + CAST('00:00:00' AS DATETIME) < CAST(SUBSTRING(CAST(LAST_EXECUTION_DATE AS VARCHAR), 0, 12) AS DATETIME) + CAST('00:00:00' AS DATETIME)))
			  
		--UPDATE TB_R_LPOP_CONFIRMATION
		--	SET LAST_EXECUTION_DATE = GETDATE()
		--WHERE LAST_REMINDING_DATE >= GETDATE() AND PROCESS_ID IS NOT NULL
				--AND
			  --((LAST_EXECUTION_DATE IS NULL) OR	
			  --(CAST(SUBSTRING(CAST(LAST_EXECUTION_DATE AS VARCHAR), 0, 12) AS DATETIME) + CAST('00:00:00' AS DATETIME) < CAST(SUBSTRING(CAST(LAST_EXECUTION_DATE AS VARCHAR), 0, 12) AS DATETIME) + CAST('00:00:00' AS DATETIME)))
			
	--END
END

