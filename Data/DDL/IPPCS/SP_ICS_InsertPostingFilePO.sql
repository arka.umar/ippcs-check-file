-- =============================================
-- Author		: Rahmat Setiawan (Arkamaya)
-- Create date	: 17 September 2013
-- Description	: Taking Data From ICS Finished Create PO
-- changed by : FID.Iman & FID.Goldy & FID.Deny
-- changed dt : 21 September 2013, 21 Maret 2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_InsertPostingFilePO] 
	@processId BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY	
	--BEGIN TRANSACTION InsertData	

	--delete all datas on TB_T_PO_ICS_FINISH
	TRUNCATE TABLE TB_T_PO_ICS_FINISH
	
		DECLARE @sqlQuery AS VARCHAR(MAX),
				@rowIndex BIGINT,
				@L_V_PO_NO AS VARCHAR(30),
				@L_V_PO_ITEM_NO AS VARCHAR(5),
				@L_V_PROD_MONTH AS VARCHAR(8),
				@L_V_SOURCE_TYPE AS VARCHAR(1), 
				@L_V_PROD_PURPOSE_CD AS VARCHAR(5), 
				@L_V_PLANT_CD AS VARCHAR(4), 
				@L_V_SLOC_CD AS VARCHAR(6), 
				@L_V_SUPP_CD AS VARCHAR(5),
				@L_V_DOCK_CD AS VARCHAR(3),
				@L_V_PART_NO AS VARCHAR(12),
				@L_V_PROCESS_ID AS BIGINT,
				@L_V_PROCESS_ID_ICS AS INT,
				@L_V_ERR_CD AS VARCHAR(20),
				@LOG_ID_H AS BIGINT,
				@L_V_ERR_MESSAGE AS VARCHAR(MAX),
				@L_V_USER_ID AS VARCHAR(20);
				
		INSERT INTO TB_T_LOCK
			   ([FUNCTION_NO]
			   ,[LOCK_REFF]
			   ,[CREATED_BY]
			   ,[CREATED_DT])
		VALUES
			('11004'
			,@processId
			,'System'
			,GETDATE())
			
		
		SET @sqlQuery = 'INSERT INTO TB_T_PO_ICS_FINISH 
			SELECT * FROM OPENQUERY([IPPCS_TO_ICS] , 
			''SELECT rownum, b.* 
			  FROM 
				(SELECT DISTINCT 
					a.PO_NO, a.PROD_MONTH, a.DOCK_CD, a.SUPP_CD, 
					a.PO_CURR, a.PO_EXCHANGE_RATE, 0 AS TOTAL_AMOUNT, 
					NULL AS SUPPLIER_PLANT, NULL AS CREATED_BY, 
					a.PO_ITEM_NO, a.PR_NO, a.PR_ITEM, a.MAT_NO, 
					a.SOURCE_TYPE, a.PROD_PURPOSE_CD, a.PLANT_CD, a.SLOC_CD, 
					a.PART_COLOR_SFX, a.PACKING_TYPE, a.PO_QUANTITY_NEW, 
					a.PO_MAT_PRICE, i.DELIVERY_DT AS REQUIRED_DT, a.DOC_DT AS PO_DATE, 
					a.ERR_CD, a.ERR_MSG, a.QTY_N, a.OTHER_PROCESS_ID, 
					a.PROCESS_ID, i.MAT_DESC, i.UNIT_OF_MEASURE_CD UOM
				 FROM TB_R_FINISHED_CREATE_PO a 
				 LEFT JOIN TB_R_PO_ITEM i ON a.PO_NO = i.PO_NO AND a.PO_ITEM_NO = i.PO_ITEM_NO 					
				 ) b 
			'') ';
		--WHERE a.PROD_MONTH = 201505
		EXEC (@sqlQuery);
		
		--modded fid.deny 2015-05-2015
		DELETE FROM TB_T_PO_ICS_FINISH WHERE PO_NO IS NULL

		--==add data to tb_r_po_h
		
		DELETE
		FROM TB_T_PO_ICS_FINISH
				WHERE EXISTS (SELECT 'x' FROM TB_R_PO_D WHERE TB_R_PO_D.PO_NO+TB_R_PO_D.PO_ITEM = TB_T_PO_ICS_FINISH.PO_NO + TB_T_PO_ICS_FINISH.PO_ITEM)


		--- Get Process ID and User ID
		SELECT TOP 1 @L_V_PROCESS_ID = PROCESS_ID,
					 @L_V_PROCESS_ID_ICS = PROCESS_ID_ICS  
		FROM TB_T_PO_ICS_FINISH;
		

		SELECT @L_V_USER_ID = CREATED_BY
		FROM TB_R_ICS_QUEUE 
		WHERE PROCESS_ID = @L_V_PROCESS_ID;
		
		--uncomment after test
		--SET @L_V_USER_ID = 'ujang'
		--end of uncomment after test

	
		--SELECT COUNT(1) FROM TB_T_PO_ICS_FINISH
		--SELECT 'INSERT TB_R_PO_H'+' - '+CONVERT(VARCHAR(20), GETDATE(), 0);
		
		
		INSERT INTO TB_R_PO_H 
							(PO_NO, SUPPLIER_CD, PRODUCTION_MONTH, PO_DATE, PO_TYPE, 
							SUPPLIER_PLANT, EXCHANGE_RATE, PO_CURR, TOTAL_AMOUNT, STATUS_CD, 
							REQUIRED_DT, CREATED_BY, CREATED_DT)
		SELECT distinct F.PO_NO, F.SUPPLIER_CD, F.PRODUCTION_MONTH, F.PO_DATE, '1', 
						F.SUPPLIER_PLANT, F.EXCHANGE_RATE, F.CURRENCY_CD, 0 TOTAL_AMOUNT, 10, 
						F.REQUIRED_DT, @L_V_USER_ID, GETDATE()  
		FROM TB_T_PO_ICS_FINISH F 
		WHERE NOT EXISTS (SELECT 'x' FROM TB_R_PO_H H WHERE H.PO_NO = F.PO_NO)
			

		--SELECT 'INSERT TB_R_PO_D'+' - '+CONVERT(VARCHAR(20), GETDATE(), 0);

		--==add data to tb_r_po_d
		INSERT INTO TB_R_PO_D 
						(PO_NO, PO_ITEM, PR_NO, 
						PR_ITEM, MAT_NO, SOURCE_TYPE, 
						PROD_PURPOSE_CD, PLANT_CD, SLOC_CD, 
						PART_COLOR_SUFFIX, PACKING_TYPE, PO_QUANTITY_NEW, 
						PO_MAT_PRICE, REQUIRED_DT, CURRENCY_CD, 
						MAT_DESC, UOM, 
						AMOUNT, CREATED_BY, CREATED_DT )
		SELECT distinct x.PO_NO, x.PO_ITEM, x.PR_NO, 
						x.PR_ITEM, x.MAT_NO, x.SOURCE_TYPE, 
						x.PROD_PURPOSE_CD, x.PLANT_CD, x.SLOC_CD, 
						x.PART_COLOR_SUFFIX, x.PACKING_TYPE, x.PO_QUANTITY_NEW, 
						x.PO_MAT_PRICE, x.REQUIRED_DT, x.CURRENCY_CD, 
						X.MAT_DESC, X.UOM, 
						x.PO_MAT_PRICE * x.PO_QUANTITY_NEW,@L_V_USER_ID, GETDATE() 
		FROM TB_T_PO_ICS_FINISH x 
		--WHERE NOT EXISTS (SELECT 'x' FROM TB_R_PO_D D WHERE D.PO_NO+D.PO_ITEM = x.PO_NO + X.PO_ITEM)
		
		
		--SELECT 'UPDATE TB_R_LPOP_SUM'+' - '+CONVERT(VARCHAR(20), GETDATE(), 0);	
		--==update po_no, po_item for tb_r_lpop_sum
		UPDATE TB_R_LPOP_SUM
		SET TB_R_LPOP_SUM.PO_NO = F.PO_NO,
			TB_R_LPOP_SUM.PO_ITEM =  F.PO_ITEM
		FROM TB_R_LPOP_SUM
		INNER JOIN TB_T_PO_ICS_FINISH F
		ON TB_R_LPOP_SUM.PACK_MONTH = F.PRODUCTION_MONTH AND
		   TB_R_LPOP_SUM.SUPPLIER_CD = F.SUPPLIER_CD AND
		   TB_R_LPOP_SUM.PART_NO = F.MAT_NO+F.PART_COLOR_SUFFIX
		WHERE TB_R_LPOP_SUM.D_ID = 'FCST' AND
			  TB_R_LPOP_SUM.VERS = 'F'
			
		--SELECT 'UPDATE TB_R_PO_H'+' - '+CONVERT(VARCHAR(20), GETDATE(), 0);
		--==update total amount TB_R_PO_H
		UPDATE TB_R_PO_H
		SET TB_R_PO_H.TOTAL_AMOUNT = (SELECT SUM(AMOUNT)
										FROM TB_R_PO_D D
										WHERE 
											D.PO_NO = TB_R_PO_H.PO_NO
										)
		WHERE EXISTS (SELECT 'x' FROM TB_T_PO_ICS_FINISH F WHERE F.PO_NO = TB_R_PO_H.PO_NO)
		--end of modified
		--SELECT 'END '+' - '+CONVERT(VARCHAR(20), GETDATE(), 0);
		
		
		--- distinct po
		DECLARE cur_ics_po CURSOR FOR
		   SELECT DISTINCT PO_NO
		   FROM TB_T_PO_ICS_FINISH;
		   
		--- open cursor
		OPEN cur_ics_po
		FETCH NEXT FROM cur_ics_po INTO @L_V_PO_NO;
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC IPPCS_WORKFLOW.dbo.sp_RegisterWorklist @L_V_PO_NO, 1, @L_V_USER_ID, @L_V_PROCESS_ID, 1;
			-- next fetch
			FETCH NEXT FROM cur_ics_po INTO @L_V_PO_NO
		END
		-- close cursor
		CLOSE cur_ics_po
		DEALLOCATE cur_ics_po
			
		 --- Delete previous error in log trans
		SELECT  TOP 1 @L_V_PROD_MONTH = PRODUCTION_MONTH
		FROM TB_T_PO_ICS_FINISH;
		
		DECLARE @TEMP_LOG_TRANS TABLE
		(
		 LOG_ID_H BIGINT
		 );
		

		-- Insert into temp table
		INSERT INTO @TEMP_LOG_TRANS
		SELECT LOG_ID_H FROM TB_R_LOG_TRANSACTION_D 
		WHERE D_ID = 'FCST'
		AND PACK_MONTH = @L_V_PROD_MONTH;
		
		-- Delete log trans
		DELETE TB_R_LOG_TRANSACTION_D 
		WHERE EXISTS (SELECT 'x' FROM @TEMP_LOG_TRANS a WHERE TB_R_LOG_TRANSACTION_D.LOG_ID_H = a.LOG_ID_H)
			
		DELETE TB_R_LOG_TRANSACTION_H
		WHERE EXISTS (SELECT 'x' FROM @TEMP_LOG_TRANS b WHERE TB_R_LOG_TRANSACTION_H.LOG_ID_H = b.LOG_ID_H)
			
		--insert into TB_R_LOG_TRANSACTION_H
		IF EXISTS(SELECT 1 FROM TB_T_PO_ICS_FINISH V WHERE RTRIM(PO_NO) IS NULL AND V.ERR_CD IS NOT NULL)
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM TB_R_LOG_TRANSACTION_H WHERE PROCESS_ID = @L_V_PROCESS_ID)
			BEGIN
				INSERT INTO TB_R_LOG_TRANSACTION_H (PROCESS_ID, 
													PROCESS_ID_ICS, 
													MANIFEST_NO, 
													CREATED_BY, 
													CREATED_DATE)
				VALUES  (@L_V_PROCESS_ID, -- PROCESS_ID - bigint
						  @L_V_PROCESS_ID_ICS , -- PROCESS_ID_ICS - int
						 'PCS_PO' , -- MANIFEST_NO - varchar(25)
						 @L_V_USER_ID , -- CREATED_BY - varchar(20)
						 GETDATE())
			END;
		
		
			SELECT @LOG_ID_H=LOG_ID_H FROM TB_R_LOG_TRANSACTION_H WHERE PROCESS_ID = @L_V_PROCESS_ID
		
			--insert into tb_r_log_transaction_d
			INSERT INTO TB_R_LOG_TRANSACTION_D (LOG_ID_H, -- 1
														SEQ_NO,   --2
														SYSTEM_SOURCE,  --3 
														USR_ID,  --4
														PROCESS_DT, --5
														MANIFEST_NO,  --6
														PROD_PURPOSE_CD,  --7
														SOURCE_TYPE,  --8
														PACK_MONTH,  --9
														D_ID, -- 9a
														VERS,  --10
														PART_NO,  --11
														SUPP_CD,  --12
														DOCK_CD,  --13
														PLANT_CD,  --14
														SLOC_CD,  --15
														LOCATION,  --16
														ERR_CD,  --16a
														ERR_MESSAGE,  --16
														CREATED_BY,  --17
														CREATED_DT) --18
			SELECT DISTINCT 
							@LOG_ID_H, --1
							row_number() over (order by V.source_type), --2
							'IPPCS', --3
							@L_V_USER_ID, --4
							GETDATE(), --5
							'', --6
							V.PROD_PURPOSE_CD, --7
							V.SOURCE_TYPE, --8
							@L_V_PROD_MONTH, --9
							'FCST', --9a
							'F', --10
							V.MAT_NO+V.PART_COLOR_SUFFIX, --11
							V.SUPPLIER_CD, --12
							V.DOCK_CD, --13
							V.PLANT_CD, --14
							V.SLOC_CD, --15
							'IPPCS PO', --16
							V.ERR_CD, --16a
							V.ERR_MESSAGE, --17
							@L_V_USER_ID,
							GETDATE()
			FROM TB_T_PO_ICS_FINISH V
			WHERE RTRIM(PO_NO) IS NULL
			   AND V.ERR_CD IS NOT NULL;
		
			----- Insert Log Transaction ---- 
			DECLARE cur_log_trans CURSOR FOR
			   SELECT DISTINCT V.MAT_NO+V.PART_COLOR_SUFFIX,
							   V.ERR_CD,V.SUPPLIER_CD
			   FROM TB_T_PO_ICS_FINISH V
			   WHERE RTRIM(PO_NO) IS NULL
			   AND V.ERR_CD IS NOT NULL;
		
			--- open cursor
			OPEN cur_log_trans
			FETCH NEXT FROM cur_log_trans 
			INTO @L_V_PART_NO,@L_V_ERR_CD,@L_V_SUPP_CD;

			--loop
			WHILE @@FETCH_STATUS = 0
			BEGIN

					--Insert LPOP Abnormality
					EXEC SP_InsertLPOPConfirmationAbnormality @L_V_PROD_MONTH, 'FCST', 'F', 
															  @L_V_PART_NO, @L_V_SUPP_CD, @L_V_ERR_CD
				
				
						
				-- next fetch
				FETCH NEXT FROM cur_log_trans 
				INTO @L_V_PART_NO,@L_V_ERR_CD,@L_V_SUPP_CD;
				 
			END
		
		-- close cursor
		CLOSE cur_log_trans
		DEALLOCATE cur_log_trans
		
		END;

		-- final, if all supp already got PO then update
		IF NOT EXISTS (SELECT DISTINCT 1
					   FROM TB_T_PO_ICS_FINISH X
					   WHERE X.SUPPLIER_CD is not null 
					   AND X.PO_NO is null )
		   BEGIN
			   --- UPDATE CREATE PO
			   UPDATE TB_R_LPOP_CONFIRMATION SET 
					  PO_CREATED_BY = @L_V_USER_ID, 
					  PO_CREATED_DT = GETDATE(), 
					  CHANGED_BY = @L_V_USER_ID, 
					  CHANGED_DT = GETDATE() ,
					  PO_CREATION_STATUS = '2'
				WHERE PRODUCTION_MONTH = @L_V_PROD_MONTH  
				--modify by hesti only status 1 will be updated
				AND ISNULL(PO_CREATION_STATUS,'0') = '1' 
			   
		   END
		
		DELETE FROM [TB_T_LOCK]
		WHERE
			[FUNCTION_NO] = '11004'
			AND [LOCK_REFF] = @processId;

		---=== Store to TB_R_ICS_QUEUE modified by Rahmat 2013.10.31
		UPDATE TB_R_ICS_QUEUE
		SET
			PROCESS_STATUS = 2,
			CHANGED_BY = 'SYSTEM',
			CHANGED_DT = GETDATE()
		WHERE
			IPPCS_MODUL = '1'
			AND IPPCS_FUNCTION = '11004'
			AND PROCESS_ID = @processId
			AND PROCESS_STATUS = '1'
		
	
	

		
	--COMMIT TRANSACTION InsertData
	END TRY
	
	Begin Catch
		--Rollback Transaction InsertData;
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		return
	End Catch
    
END

