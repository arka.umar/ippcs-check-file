-- ==================================================================================
-- Author:		fid.iman
-- Create date: 16.10.2013
-- Description:	Procedure to register GR Posting automatically
--              - Select manifest no which flag = 3 or 5 (Approved or error posting)
--              - Register job under system
-- ==================================================================================
CREATE PROCEDURE [dbo].[SP_ICS_Scheduler_ErrorPostingGR_BU] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE
	  @l_v_function_no varchar(10),
	  @l_v_module_no varchar(5),
	  @l_n_process_id bigint,
	  @l_v_message_id varchar(12),
	  @l_v_message varchar(500),
	  @l_v_location varchar(100),
	  @l_v_manifest_no varchar(max),
	  @l_v_manifest_no_temp varchar(max);
	  
	--1. set param for log
	set @l_v_message = 'Start Process Error Posting GR';
	set @l_v_message_id = 'MPCS00002INF';
	set @l_v_location = 'Error Posting GR';
	set @l_v_module_no = '4';
	set @l_v_function_no = '42001';
	
	--2. create log    
	exec sp_PutLog @l_v_message,
	               'system',
	               @l_v_location,
	               @l_n_process_id OUTPUT,
	               @l_v_message_id,
	               'INF',
	               @l_v_module_no,
	               @l_v_function_no,
	               '0';
	               
	-- cursor
	DECLARE cur_manifest CURSOR FOR
	  SELECT  top 100 manifest_no
	  FROM TB_R_DAILY_ORDER_MANIFEST v
	  WHERE MANIFEST_RECEIVE_FLAG in ('5')
	  AND ARRIVAL_PLAN_DT > '2016-09-23'
	  --AND SUPPLIER_CD not in ('807B','807D','807J')
	  --AND CANCEL_FLAG = '0'
	  --AND ICS_FLAG = '0'
	  ORDER BY ARRIVAL_PLAN_DT asc

		
	-- open cursor  
	OPEN cur_manifest
	FETCH cur_manifest INTO @l_v_manifest_no_temp

	WHILE @@FETCH_STATUS = 0
	
	BEGIN
	  SET @l_v_manifest_no = @l_v_manifest_no_temp + ';' + ISNULL(@l_v_manifest_no,';');
	
	  UPDATE TB_R_DAILY_ORDER_MANIFEST SET MANIFEST_RECEIVE_FLAG = '3' WHERE MANIFEST_NO = @l_v_manifest_no_temp

	  FETCH NEXT FROM cur_manifest INTO @l_v_manifest_no_temp

	END;
	
    -- close cursor
	CLOSE cur_manifest;
	DEALLOCATE cur_manifest;
	
	if(LEN(@l_v_manifest_no) > 0)
	begin

		-- remove extra ';'
		SET @l_v_manifest_no = LEFT(@l_v_manifest_no,LEN(@l_v_manifest_no)-1)
	
		-- call sp generate posting file GR
		--3. create log before call other function
		set @l_v_message = 'Start Process Generate Posting File GR';
		set @l_v_message_id = 'MPCS00002INF';
		set @l_v_location = 'Auto Error Posting GR';
	
		exec sp_PutLog @l_v_message,
					   'system',
					   @l_v_location,
					   @l_n_process_id,
					   @l_v_message_id,
					   'INF',
					   @l_v_module_no,
					   @l_v_function_no,
					   '0';
		
		-- 4.sp call posting file gr
		exec SP_ICS_GeneratePostingFileGR 'ippcs.admin',
										  @l_v_manifest_no,
										  '',
										  '',
										  '',
										  @l_n_process_id;
									  
	
		set @l_v_message = 'Process Generate Posting File GR is finished';
		set @l_v_message_id = 'MPCS00003INF';
		set @l_v_location = 'Auto Error Posting GR';
	
		-- 5.create log finish GR Posting					  
		exec sp_PutLog @l_v_message,
					   'system',
					   @l_v_location,
					   @l_n_process_id,
					   @l_v_message_id,
					   'INF',
					   @l_v_module_no,
					   @l_v_function_no,
					   '0';
	

		set @l_v_message = 'Process Error Posting GR is finished';
		set @l_v_message_id = 'MPCS00003INF';
		set @l_v_location = 'Auto Error Posting GR';
	
		-- 6.create log finish process					  
		exec dbo.sp_PutLog @l_v_message,
						   'system',
						   @l_v_location,
						   @l_n_process_id,
						   @l_v_message_id,
						   'INF',
						   @l_v_module_no,
						   @l_v_function_no,
						   '0';
	end
	

END

