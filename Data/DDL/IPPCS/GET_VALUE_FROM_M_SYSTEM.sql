-- =============================================
-- Author:		niit.yudha
-- Create date: 3-Mei-2013
-- Description:	Get Name value from TB_R_SYSTEM
-- =============================================
CREATE FUNCTION [dbo].[GET_VALUE_FROM_M_SYSTEM]
    (
      -- Add the parameters for the function here
      @FUNCTION_ID VARCHAR(100) ,
      @SYSTEM_CD VARCHAR(1000)
    )
RETURNS VARCHAR(1000)
AS 
    BEGIN
        DECLARE @RET_VAL VARCHAR(1000)
        
        SELECT  @RET_VAL = SYSTEM_VALUE
        FROM    TB_M_SYSTEM
        WHERE   FUNCTION_ID = @FUNCTION_ID
                AND SYSTEM_CD LIKE @SYSTEM_CD
        RETURN(@RET_VAL)
    END

