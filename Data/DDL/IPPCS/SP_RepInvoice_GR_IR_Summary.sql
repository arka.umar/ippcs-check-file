-- ==============================================
-- Created By	: Fid.Joko
-- Created Date : 2013-11-21
-- Created For	: Report GR IR Summary
-- Modified By	: 
-- Modified Date: 
-- ==============================================

CREATE PROCEDURE [dbo].[SP_RepInvoice_GR_IR_Summary]
	@DOC_DT varchar(max) = NULL
AS
BEGIN

SELECT
	g.SUPPLIER_CD,
	s.SUPP_NAME,
	g.ITEM_CURR,
	Year1=case when Month(convert(datetime,@DOC_DT)) between 1 and 3 then Year(convert(datetime,@DOC_DT))-1 else Year(convert(datetime,@DOC_DT)) end,
	Year2=case when Month(convert(datetime,@DOC_DT)) between 1 and 3 then Year(convert(datetime,@DOC_DT)) else Year(convert(datetime,@DOC_DT))+1 end,
	APR=SUM(CASE WHEN MONTH(g.DOC_DT)=4 THEN GR_IR_AMT ELSE 0 END),
	MAY=SUM(CASE WHEN MONTH(g.DOC_DT)=5 THEN GR_IR_AMT ELSE 0 END),
	JUN=SUM(CASE WHEN MONTH(g.DOC_DT)=6 THEN GR_IR_AMT ELSE 0 END),
	JUL=SUM(CASE WHEN MONTH(g.DOC_DT)=7 THEN GR_IR_AMT ELSE 0 END),
	AUG=SUM(CASE WHEN MONTH(g.DOC_DT)=8 THEN GR_IR_AMT ELSE 0 END),
	SEP=SUM(CASE WHEN MONTH(g.DOC_DT)=9 THEN GR_IR_AMT ELSE 0 END),
	OCT=SUM(CASE WHEN MONTH(g.DOC_DT)=10 THEN GR_IR_AMT ELSE 0 END),
	NOV=SUM(CASE WHEN MONTH(g.DOC_DT)=11 THEN GR_IR_AMT ELSE 0 END),
	DECEMBER=SUM(CASE WHEN MONTH(g.DOC_DT)=12 THEN GR_IR_AMT ELSE 0 END),
	JAN=SUM(CASE WHEN MONTH(g.DOC_DT)=1 THEN GR_IR_AMT ELSE 0 END),
	FEB=SUM(CASE WHEN MONTH(g.DOC_DT)=2 THEN GR_IR_AMT ELSE 0 END),
	MAR=SUM(CASE WHEN MONTH(g.DOC_DT)=3 THEN GR_IR_AMT ELSE 0 END)
FROM TB_R_GR_IR g
INNER JOIN TB_M_SUPPLIER_ICS s on g.SUPPLIER_CD=s.SUPP_CD
WHERE g.STATUS_CD='1' AND g.CANCEL_FLAG='0' AND CONVERT(VARCHAR(8),g.DOC_DT,112) < @DOC_DT
GROUP BY g.SUPPLIER_CD,
	s.SUPP_NAME,
	g.ITEM_CURR
END

