-- ==============================================
-- Created By	: Fid.Muhajir
-- Created Date : 10-01-2019
-- Created For	: Sub Manifest Create Interface
-- ==============================================

CREATE PROCEDURE [dbo].[SP_Interface_File_Batch2]
AS
BEGIN
	DECLARE @log VARCHAR(2000)
			,@functionId VARCHAR(10) = '13501'
			,@module VARCHAR(10) = '135'
			,@p_PID BIGINT
			,@lock_PID BIGINT
			,@uid varchar(max) = 'system'
			,@error_flag CHAR(1) = 'N'
			,@p_count_lock INT
			,@p_gr_error INT
			,@DestinationPath varchar(1000)
			,@SuccessPath varchar(1000)
			,@ErrorPath varchar(1000)
			,@FileName varchar(max)
			,@SQL varchar(max)
			,@continue CHAR(1) = 'Y'
			,@p_ErrorStatus INT 
			,@supplier_name varchar(max)
			,@last_seq int
			,@PID varchar(max)
	DECLARE @px TABLE (PROCESS_ID BIGINT);

	
	IF OBJECT_ID('tempdb..#TB_T_BULK') IS NOT NULL
		drop TABLE #TB_T_BULK

	CREATE TABLE #TB_T_BULK (
			[MANIFEST_NO] VARCHAR (MAX) NULL,
			[ITEM_NO] VARCHAR(MAX) NULL,
			[PART_NO] VARCHAR(MAX) NULL,
			[QTY_DELIVERY] VARCHAR(MAX) NULL,
			[DELIVERY_DT] VARCHAR(MAX) NULL,
			[ROUTE] VARCHAR(MAX) NULL,
			[CASE_NO] VARCHAR(MAX) NULL,
			[SUPPLIER_REF_NO_1] VARCHAR(20) NULL,
			[SUPPLIER_REF_NO_2] VARCHAR(20) NULL,
			[KANBAN_ID] VARCHAR(MAX) NULL
    );
	
	DECLARE @gr_check TABLE (GR_NO VARCHAR(100))
	DECLARE @gr_error_po TABLE (GR_NO VARCHAR(100))
	DECLARE @item_gr_error VARCHAR(MAX)
	DECLARE @msg VARCHAR(MAX) = ''
	DECLARE @counter INT = 0

	
	SELECT TOP 1 @DestinationPath = SYSTEM_VALUE
				FROM TB_M_SYSTEM
				WHERE FUNCTION_ID = 'INT_SUB_MANIFEST'
					AND SYSTEM_CD = 'FTP'

	SELECT TOP 1 @SuccessPath = SYSTEM_VALUE
				FROM TB_M_SYSTEM
				WHERE FUNCTION_ID = 'INT_SUB_MANIFEST'
					AND SYSTEM_CD = 'FTP_SUCCESS'

	SELECT TOP 1 @ErrorPath = SYSTEM_VALUE
				FROM TB_M_SYSTEM
				WHERE FUNCTION_ID = 'INT_SUB_MANIFEST'
					AND SYSTEM_CD = 'FTP_ERROR'

	SELECT TOP 1 @FileName = SYSTEM_VALUE
				FROM TB_M_SYSTEM
				WHERE FUNCTION_ID = 'INT_SUB_MANIFEST'
					AND SYSTEM_CD = 'FILE_NAME'


	
		DECLARE @FILE_NAME VARCHAR(100),@PATH VARCHAR(100),@SUPPLIER_CD VARCHAR(100)
		SET @PATH = 'dir /B "'+@DestinationPath+'"'

		EXEC sp_configure 'show advanced options', 1  
		RECONFIGURE
		EXEC sp_configure 'xp_cmdshell', 1 
		RECONFIGURE 
		
		IF OBJECT_ID('tempdb..#tmp') IS NOT NULL
			drop TABLE #tmp

		IF OBJECT_ID('tempdb..#tmp') IS NOT NULL
			drop TABLE #TB_T_LIST_FILE		
		
		CREATE TABLE #tmp(INTERFACE_NAME VARCHAR(100));
		INSERT INTO #tmp
		EXEC xp_cmdshell @PATH;
	
		DELETE FROM #TMP WHERE LEFT(INTERFACE_NAME,13) <> 'SUB_MANIFEST_' 

		SELECT	 A.INTERFACE_NAME
				,B.SUPPLIER_CODE
				,B.SUPPLIER_PLANT_CD
				INTO #TB_T_LIST_FILE 
			FROM #TMP A JOIN TB_M_SUPPLIER B
				ON 
				LEFT(SUBSTRING(A.INTERFACE_NAME, 14, LEN(A.INTERFACE_NAME)), CHARINDEX('.', SUBSTRING(A.INTERFACE_NAME, 14, LEN(A.INTERFACE_NAME)) )-1)  = ISNULL(B.SUPPLIER_CODE,'') + ISNULL(B.SUPPLIER_PLANT_CD,'')				
		
		While (SELECT COUNT(*) FROM #TB_T_LIST_FILE WHERE INTERFACE_NAME IS NOT NULL) > 0
		Begin

		SET @log = 'Start Submit Sub Manifest';

		INSERT @px
		EXEC sp_PutLog @log
			,@uid
			,'SUBMIT SUB MANIFEST'
			,@lock_PID OUTPUT
			,'MSGS00001INF'
			,'I'
			,@module
			,@functionId
			,0;


		SELECT @p_count_lock = count(1)
		FROM tb_t_lock
		WHERE FUNCTION_NO = @functionId
			AND CREATED_BY = left(@uid, 4);


		SELECT TOP 1 @FILE_NAME = INTERFACE_NAME,@SUPPLIER_CD = SUPPLIER_CODE FROM #TB_T_LIST_FILE

		IF @p_count_lock > 0
		BEGIN
			SET @error_flag = 'Y';
			SET @log = 'Submit Sub Manifest Process is lock';

			INSERT @px
			EXEC sp_PutLog @log
				,@uid
				,'SUBMIT SUB MANIFEST'
				,@lock_PID OUTPUT
				,'MTOCSTD024I'
				,'I'
				,@module
				,@functionId
				,0;
		END
		ELSE
		BEGIN
			-- register locking
			PRINT @lock_PID;

			INSERT INTO TB_T_LOCK (
				FUNCTION_NO
				,LOCK_REFF
				,CREATED_BY
				,CREATED_DT
				)
			VALUES (
				@functionId
				,@lock_PID
				,left(@uid, 4)
				,getdate()
				)


			
			DELETE FROM [dbo].[TB_T_SPEX_SUB_MANIFEST]


			SET @SQL = 'BULK INSERT #TB_T_BULK
					  FROM ''' + @DestinationPath + '\' + @FILE_NAME + '''
					  WITH	( FIELDTERMINATOR = ''\t'',
							  ROWTERMINATOR =''\n''
							  --FORMATFILE=''D:\\home\\SPEX\\interface''
							)'

					PRINT 'query bulk = ' + @SQL
					PRINT 'destination = ' + @DestinationPath
					PRINT 'filename = ' + @FILE_NAME

			EXECUTE (@SQL);



			INSERT INTO [dbo].[TB_T_SPEX_SUB_MANIFEST]
			   ([DATA_ID]
			   ,[PROCESS_ID]
			   ,[MANIFEST_NO]
			   ,[PART_NO]
			   ,[ITEM_NO]
			   ,[QTY_DELIVERY]
			   ,[CASE_NO]
			   ,[KANBAN_ID]
			   ,[ETD]
			   ,[SUPPLIER_REF_1]
			   ,[SUPPLIER_REF_2]
			   ,[USER_ID])
			 SELECT
				   'D',
				   @lock_PID,
				   MANIFEST_NO,
				   PART_NO,
				   ITEM_NO,
				   QTY_DELIVERY,
				   CASE_NO,
				   KANBAN_ID,
				   DELIVERY_DT,
				   SUPPLIER_REF_NO_1,
				   SUPPLIER_REF_NO_2,
				   @SUPPLIER_CD			
			FROM #TB_T_BULK

			SET @PID = cast(@lock_PID as varchar)
			EXEC [dbo].[SP_SPEX_ValidateSMCUpload] @PID, @SUPPLIER_CD
		
			SELECT @last_seq = MAX(d.SEQUENCE_NUMBER) 
			FROM dbo.TB_R_LOG_D d WHERE process_id = @lock_PID;

		
			

			IF (
				EXISTS (
					SELECT *
					FROM TB_T_UPLOAD_VALIDATE V
					WHERE V.PROCESS_ID = @lock_PID
						AND V.FUNCTION_ID = '81001'
					)
				)
			BEGIN
				SET @p_ErrorStatus = 1


				INSERT INTO TB_R_LOG_D
				(	[PROCESS_ID],
					[SEQUENCE_NUMBER],
					[MESSAGE_ID],
					[MESSAGE_TYPE],
					[MESSAGE],
					[LOCATION],
					[CREATED_BY],
					[CREATED_DATE],
					[CHANGED_BY],
					[CHANGED_DATE]
					)
					SELECT PROCESS_ID,
						   (ROW_NUMBER() OVER (ORDER BY ID ASC ) + @last_seq),
						   'MSGS00004ERR',
						   'ERR',
						   MSG,
						   'Validation Sub Manifest',
						   @uid,
						   GETDATE(),
						   @uid,
						   GETDATE()
						   FROM TB_T_UPLOAD_VALIDATE WHERE PROCESS_ID = @PID

			END
			ELSE
			BEGIN
				SET @p_ErrorStatus = 0
			END


			IF(@p_ErrorStatus = 0)
			BEGIN
				print 'aa'
				EXEC SP_SPEX_SUB_MANIFEST_CREATION_BATCH @lock_PID , @SUPPLIER_CD , @SUPPLIER_CD
			END

		
			IF @p_ErrorStatus > 0
			BEGIN
				SET @error_flag = 'Y';
				SET @log = 'Submit Sub Manifest FILE '+(@FILE_NAME)+' Process Finished With Error';

				INSERT @px
				EXEC sp_PutLog @log
					,@uid
					,'SUBMIT SUB MANIFEST'
					,@lock_PID OUTPUT
					,'MTOCSTD024I'
					,'E'
					,@module
					,@functionId
					,@p_ErrorStatus;

				SET @ErrorPath = 'MOVE '+@DestinationPath+'\'+@FILE_NAME+' '+@ErrorPath;
				SELECT @ErrorPath
				EXEC master..xp_cmdshell @ErrorPath
			END
			ELSE
			BEGIN
				SET @error_flag = 'Y';
				SET @log = 'Submit Sub Manifest FILE '+(@FILE_NAME)+' Process Finished With Success';

							INSERT @px
							EXEC sp_PutLog @log
								,@uid
								,'SUBMIT SUB MANIFEST'
								,@lock_PID OUTPUT
								,'MTOCSTD024I'
								,'I'
								,@module
								,@functionId
								,@p_ErrorStatus;			
				SET @SuccessPath = 'MOVE '+@DestinationPath+'\'+@FILE_NAME+' '+@SuccessPath;
				select @SuccessPath
				--SET @SuccessPath = 'MOVE C:\Home\TMMIN\SPEX\INF_SUB\interface\sub_manifest_00034.txt C:\Home\TMMIN\SPEX\INF_SUB\Error'
				EXEC master..xp_cmdshell @SuccessPath
			END

			SELECT * FROM TB_R_LOG_D WHERE PROCESS_ID = @lock_PID
			


		END
		
		DELETE FROM #TB_T_LIST_FILE WHERE INTERFACE_NAME = @FILE_NAME
		-- release lock
		DELETE
			FROM TB_T_LOCK
				WHERE FUNCTION_NO = @functionId
					AND LOCK_REFF = @lock_PID
		
		End
END

