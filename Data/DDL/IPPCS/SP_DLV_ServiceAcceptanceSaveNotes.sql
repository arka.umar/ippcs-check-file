/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Service acceptance save note
*/

CREATE PROCEDURE [dbo].[SP_DLV_ServiceAcceptanceSaveNotes]
   @UserId varchar(50),
   @DELIVERY_NO VARCHAR(MAX),
   @SA_NOTES VARCHAR(MAX)
AS
BEGIN
	UPDATE TB_R_DELIVERY_CTL_H 
		SET SA_NOTES = @SA_NOTES, 
			DELIVERY_STS = 'Delivered',
			CHANGED_BY = @UserId,
			CHANGED_DT = GETDATE()
	WHERE DELIVERY_NO = @DELIVERY_NO
END

