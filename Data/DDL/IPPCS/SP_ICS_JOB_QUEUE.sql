-- =============================================
-- Author:		ALIRA)Deni
-- Create date: 02.03.2015
-- Description:	Send email for job queue
-- Updated:		IS)Cytta
-- Update Date  24.08.2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_JOB_QUEUE]
	--@Recipient VARCHAR(MAX)
	--,@Content VARCHAR(MAX)
AS
BEGIN

	DECLARE
	@profile varchar(max),
	@body varchar(max),
	@subject varchar(100),
	@MAIL_QUERY VARCHAR(MAX),
	@RecipientEmail VARCHAR(MAX),
	@RecipientCopyEmail VARCHAR(MAX),
	@51002 VARCHAR(MAX),
	@53002 VARCHAR(MAX),
	@41002 VARCHAR(MAX),
	@creationStartTime DATETIME,
	@postingStartTime DATETIME,
	@GRStartTime DATETIME
	
	SELECT @profile = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE';
		IF (@profile IS NULL) BEGIN
			SET @profile = 'NotificationAgent';
		END
	
	SET @RecipientEmail = 'app.helpdesk@toyota.co.id'
	SET @RecipientCopyEmail = 'aang.dyaksa@toyota.co.id; hesti.asrianingsih; cytta.rizka@toyota.co.id'
		
	SELECT @creationStartTime = MIN(CREATED_DT)
	FROM [IPPCS_QA].[dbo].[TB_R_ICS_QUEUE]
	WHERE IPPCS_FUNCTION = '51002' AND PROCESS_STATUS < 2
	
	SELECT @postingStartTime = MIN(CREATED_DT)
	FROM [IPPCS_QA].[dbo].[TB_R_ICS_QUEUE]
	WHERE IPPCS_FUNCTION = '53002' AND PROCESS_STATUS < 2
	
	SELECT @GRStartTime = MIN(CREATED_DT)
	FROM [IPPCS_QA].[dbo].[TB_R_ICS_QUEUE]
	WHERE IPPCS_FUNCTION = '42001' AND PROCESS_STATUS < 2
  
	SELECT  @subject=NOTIFICATION_SUBJECT,
			@body=NOTIFICATION_CONTENT
	FROM    TB_M_NOTIFICATION_CONTENT
	WHERE   FUNCTION_ID = '91002'
	
	PRINT @body

	SELECT @51002 = NOTIFICATION_CONTENT FROM TB_M_NOTIFICATION_CONTENT WHERE FUNCTION_ID = '51002'
	SELECT @53002 = NOTIFICATION_CONTENT FROM TB_M_NOTIFICATION_CONTENT WHERE FUNCTION_ID = '53002'
	SELECT @41002 = NOTIFICATION_CONTENT FROM TB_M_NOTIFICATION_CONTENT WHERE FUNCTION_ID = '41002'

	IF(ISNULL((SELECT DATEDIFF(hour, @creationStartTime, GETDATE())),0) > 4)
	BEGIN
		SET @body = REPLACE(@body, '@51002', @51002)
		SET @body = REPLACE(@body, '@creationStartTime', @creationStartTime)
	END
	ELSE
	BEGIN
		SET @body = REPLACE(@body, '@51002', @51002)
		SET @body = REPLACE(@body, '@creationStartTime', '-')
	END
	
	PRINT @body
	
	IF(ISNULL((SELECT DATEDIFF(hour, @postingStartTime, GETDATE())),0) > 4)
	BEGIN
		SET @body = REPLACE(@body, '@53002', @53002)
		SET @body = REPLACE(@body, '@postingStartTime', @postingStartTime)
	END
	ELSE
	BEGIN
		SET @body = REPLACE(@body, '@53002', @53002)
		SET @body = REPLACE(@body, '@postingStartTime', '-')
	END

	PRINT @body
	
	IF(ISNULL((SELECT DATEDIFF(hour, @GRStartTime, GETDATE())),0) > 4)
	BEGIN
		SET @body = REPLACE(@body, '@41002', @41002)
		SET @body = REPLACE(@body, '@GRStartTime', @GRStartTime)
	END
	ELSE
	BEGIN
		SET @body = REPLACE(@body, '@41002', @41002)
		SET @body = REPLACE(@body, '@GRStartTime', '-')
	END
		
	PRINT @body

	SET @body = ISNULL(@body, '')
	SET @subject = ISNULL(@subject, '')
	
	PRINT @body
	PRINT @subject
	
	IF (ISNULL((SELECT DATEDIFF(hour, @creationStartTime, GETDATE())),0) > 4 
		OR ISNULL((SELECT DATEDIFF(hour, @postingStartTime, GETDATE())),0) > 4 
		OR ISNULL((SELECT DATEDIFF(hour, @GRStartTime, GETDATE())),0) > 4)
	BEGIN
	IF (@RecipientCopyEmail IS NOT NULL)
	BEGIN
		SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
	
	SET @MAIL_QUERY = '	
						EXEC msdb.dbo.sp_send_dbmail
							@profile_name	= ''' + @profile + ''',
							@body 			= ''' + @body + ''',
							@body_format 	= ''HTML'',
							@recipients 	= ''' + @RecipientEmail + ''',
							' + @RecipientCopyEmail + '
							@subject 		= ''' + @subject + ''''
							
	EXEC (@MAIL_QUERY)
	END
	END
END

