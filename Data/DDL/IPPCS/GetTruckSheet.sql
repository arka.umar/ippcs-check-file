CREATE PROCEDURE [dbo].[GetTruckSheet](@DOCK VARCHAR(5), @DATEFROM varchar(15), @DATETO varchar(15) ,@C INT OUTPUT)
AS
BEGIN

DECLARE @ROUTE VARCHAR(10), @DEL_NO VARCHAR(10), @ALLSUPPLIER VARCHAR(MAX),
@ARRFRI VARCHAR(7), @DEPFRI VARCHAR(7), @ARR VARCHAR(7), @DEP VARCHAR(7), @D VARCHAR(10)
DECLARE @TEMP#1 TABLE(
[ROUTE] VARCHAR(10),
RUNSEQ VARCHAR(10),
DEL_NO VARCHAR(10),
DOOR_CD VARCHAR(10),
PHYSICAL_STATION INT,
ARRVDATETIME VARCHAR(10),
DPTDATETIME VARCHAR(10),
ARRVFRI VARCHAR(10),
DEPTFRI VARCHAR(10),
LOGPARTNERCD VARCHAR(10),
SUPPLIER VARCHAR(MAX),
TEMP VARCHAR(10),
TEMP2 VARCHAR(10),
DOCK VARCHAR(5)
)


INSERT INTO @TEMP#1
SELECT DISTINCT 
--A.RTEDATE,
A.RTEGRPCD AS [ROUTE],
A.RUNSEQ,
CASE WHEN SUBSTRING(A.RUNSEQ,1,1) = '0' THEN SUBSTRING(A.RUNSEQ,2,1) ELSE A.RUNSEQ END AS DEL_NO,
A.DOORCD,
B.PHYSICAL_STATION,
CONVERT(CHAR(5), A.ARRVDATETIME, 108) ARRVDATETIME,
CONVERT(CHAR(5), A.DPTDATETIME, 108) DPTDATETIME,
--CASE WHEN CONVERT(CHAR(5), C.ARRVDATETIME, 108)  <> CONVERT(CHAR(5), A.ARRVDATETIME, 108) THEN
      --CONVERT(CHAR(5), C.ARRVDATETIME, 108) ELSE '-' END [ARRFRI],
--CASE WHEN CONVERT(CHAR(5), C.DPTDATETIME, 108)  <> CONVERT(CHAR(5), A.DPTDATETIME, 108) THEN
      --CONVERT(CHAR(5), C.DPTDATETIME, 108) ELSE '-' END [DEPTFRI],
NULL AS ARRVFRI,
NULL AS DEPTFRI,
A.LOGPARTNERCD,
NULL AS SUPPLIER,
CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, A.ARRVDATETIME)), 108),
CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, A.DPTDATETIME)), 108),
A.DOCKCD
FROM
TB_R_DCL_TLMS_DRIVER_DATA A
INNER JOIN TB_M_DELIVERY_STATION_MAPPING B ON B.DOOR_CD_TLMS = A.DOORCD AND B.PHYSICAL_DOCK = A.DOCKCD
/*INNER JOIN TB_T_DCL_TLMS_DRIVER_DATA C
ON C.DOCKCD = A.DOCKCD 
   AND C.DOORCD <> ''
   AND DATENAME(dw,CONVERT(DATE, C.ARRVDATETIME)) = 'Friday' 
   AND DATENAME(dw,CONVERT(DATE, C.DPTDATETIME)) = 'Friday' 
   AND C.RTEGRPCD = A.RTEGRPCD 
   AND C.RUNSEQ = A.RUNSEQ*/
WHERE A.RTEDATE >= @DATEFROM 
AND A.RTEDATE <= @DATETO 
AND A.DOCKCD = @DOCK 
AND A.DOORCD <> ''
--AND A.LOGPTCD NOT LIKE '807%'
ORDER BY A.DOCKCD ASC, B.PHYSICAL_STATION ASC, 
 CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, A.ARRVDATETIME)), 108) ASC,
 CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, A.DPTDATETIME)), 108) ASC  

DECLARE CURR_1 CURSOR FOR
SELECT [ROUTE], RUNSEQ, DOCK, ARRVDATETIME, DPTDATETIME FROM @TEMP#1
OPEN CURR_1
FETCH NEXT FROM CURR_1 INTO @ROUTE, @DEL_NO, @D, @ARR, @DEP
WHILE @@FETCH_STATUS = 0
BEGIN
SET @ALLSUPPLIER = dbo.GETSUPPLIER(@ROUTE, @DEL_NO, @DATEFROM, @DATETO, @D)
SET @ARRFRI = ISNULL(dbo.GetFridayArrv(@D, @DATEFROM, @DATETO, @ROUTE, @DEL_NO, @ARR, @DEP),'-')
SET @DEPFRI = ISNULL(dbo.GetFridayDept(@D, @DATEFROM, @DATETO, @ROUTE, @DEL_NO, @ARR, @DEP),'-')
UPDATE @TEMP#1 SET 
SUPPLIER = @ALLSUPPLIER,
ARRVFRI = @ARRFRI,
DEPTFRI = @DEPFRI
WHERE [ROUTE] = @ROUTE AND RUNSEQ = @DEL_NO
FETCH NEXT FROM CURR_1 INTO @ROUTE, @DEL_NO, @D, @ARR, @DEP
END
CLOSE CURR_1
DEALLOCATE CURR_1

SELECT *
--CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, ARRVDATETIME)), 108),
--CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, DPTDATETIME)), 108)
 FROM @TEMP#1 ORDER BY DOCK ASC, PHYSICAL_STATION ASC, TEMP ASC, TEMP2 ASC
  --CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, ARRVDATETIME)), 108) ASC,
  --CONVERT(VARCHAR(5), DATEADD(minute, -15, DATEADD(hh, -7, DPTDATETIME)), 108) ASC

SELECT @C = @@ROWCOUNT
END

