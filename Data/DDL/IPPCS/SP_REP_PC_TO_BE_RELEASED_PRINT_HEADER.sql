-- =============================================
-- Author:		FID.Deny
-- Create date: 2015-07-23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_REP_PC_TO_BE_RELEASED_PRINT_HEADER]
		@p_Pcnos varchar(max) = null,
        @p_SuppCd varchar(max) =  null,
        @p_PcDateBegin varchar(max)= null,
        @p_PcDateEnd varchar(max)= null,
        @p_EfDateBegin varchar(max) = null,
        @p_EfDateEnd varchar(max) = null,
        @p_CurrPIC VARCHAR(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
	@Pcnos varchar(max) = @p_Pcnos,
	@SuppCd varchar(max) =  @p_SuppCd,
	@PcDateBegin DATETIME= CONVERT(DATETIME, NULLIF( @p_PcDateBegin,''), 104),
	@PcDateEnd DATETIME= CONVERT(DATETIME, NULLIF( @p_PcDateEnd,''), 104),
	@EfDateBegin DATETIME = CONVERT(DATETIME, NULLIF( @p_EfDateBegin, ''), 104),
	@EfDateEnd DATETIME =CONVERT(DATETIME, NULLIF( @p_EfDateEnd, ''), 104),
	@CurrPIC VARCHAR(50) = @p_CurrPIC



	SELECT
		a.[PC_NO] [PCNO], 
		a.[PC_DT] [PCDate], 
		SHDS.[ACTUAL_DT] [SHDate],
		DPHDS.[ACTUAL_DT] [DPHDate],
		DHDS.[ACTUAL_DT] [DHDate],
		a.[SUPPLIER_CD] SupplierCode,
		B.SUPP_NAME SupplierName
	FROM      TB_R_PC_H a
	LEFT JOIN TB_M_SUPPLIER_ICS b ON a.SUPPLIER_CD = b.SUPP_CD
	JOIN      [IPPCS_WORKFLOW].[dbo].TB_R_WORKLIST c ON c.FOLIO_NO = a.PC_NO
	JOIN      (
					SELECT FOLIO_NO FROM [IPPCS_WORKFLOW].[dbo].TB_R_DISTRIBUTION_STATUS
					GROUP BY FOLIO_NO
			  ) dsx ON dsx.FOLIO_NO = a.PC_NO



	LEFT JOIN [IPPCS_WORKFLOW]. [dbo]. [TB_R_DISTRIBUTION_STATUS] SHDS
		   ON a. [PC_NO] = SHDS. [FOLIO_NO]
		  AND SHDS.STATUS_CD = 31

	LEFT JOIN [IPPCS_WORKFLOW]. [dbo]. [TB_R_DISTRIBUTION_STATUS] DPHDS
		   ON a. [PC_NO] = DPHDS. [FOLIO_NO]
		  AND DPHDS.STATUS_CD = 32

	LEFT JOIN [IPPCS_WORKFLOW]. [dbo]. [TB_R_DISTRIBUTION_STATUS] DHDS
		   ON a. [PC_NO] = DHDS. [FOLIO_NO]
		  AND DHDS.STATUS_CD = 33

	LEFT JOIN [IPPCS_WORKFLOW].[dbo].TB_M_STATUS d ON d.STATUS_CD = c.STATUS_CD

	LEFT JOIN (
			SELECT x.rno, x.DESTINATION, x.FOLIO_CD,x.MODULE_CD
			FROM (
			SELECT d.DESTINATION, d.FOLIO_CD, d.MODULE_CD
				, ROW_NUMBER() over (PARTITION by d.FOLIO_CD, d.MODULE_CD
									 order by d.folio_cd, d.module_cd) as rno
			FROM [IPPCS_WORKFLOW]. [dbo]. [TB_R_WORKLIST_DESTINATION] d
			) x
			WHERE x.rNo = 1
		) AS wd ON WD.FOLIO_CD = A.PC_NO
	LEFT JOIN (
			SELECT xx.FOLIO_NO, xx.MODULE_CD, xx.PROCEED_BY
			FROM (
				SELECT dx.FOLIO_NO, dx.MODULE_CD, CASE WHEN DX.ACTUAL_DT IS NULL THEN NULL ELSE  dx.PROCEED_BY END AS PROCEED_BY,
				ROW_NUMBER() over (PARTITION BY dx.FOLIO_NO, dx.MODULE_CD
								   ORDER BY dx.STATUS_CD DESC) AS RNO
				FROM IPPCS_WORKFLOW.dbo.TB_R_DISTRIBUTION_STATUS dx
				WHERE ISNULL(dx.SKIP_FLAG, 0) = 0
				AND dx.MODULE_CD = 2
			) XX WHERE XX.RNO = 1
		) ri ON ri.FOLIO_NO = a.PC_NO

	LEFT JOIN dbo.TB_R_VW_EMPLOYEE ve    ON ve.USERNAME = wd.DESTINATION
	LEFT JOIN dbo.TB_R_VW_EMPLOYEE vx    ON vx.USERNAME = a.CREATED_BY
	LEFT JOIN dbo.TB_R_VW_EMPLOYEE vr    ON vr.USERNAME = ri.PROCEED_BY
	LEFT JOIN dbo.TB_R_VW_EMPLOYEE eSH   ON eSH.USERNAME = SHDS. [PROCEED_BY]
	LEFT JOIN dbo.TB_R_VW_EMPLOYEE eDPH  ON eDPH.USERNAME = DPHDS. [PROCEED_BY]
	LEFT JOIN dbo.TB_R_VW_EMPLOYEE eDH   ON eDH.USERNAME = DHDS. [PROCEED_BY]

	WHERE 1 = 1

	AND ((LEN(ISNULL(@Pcnos,'')) <1) OR
			PC_NO in (SELECT * FROM dbo.fnSplit(@Pcnos, ';'))  )

	AND ((LEN(ISNULL(@SuppCd, '')) < 1) OR
		SUPPLIER_CD IN (SELECT * FROM dbo.fnSplit(@SuppCd, ';'))  )

	AND ((@EfDateBegin IS NULL OR @EfDateEnd IS NULL)
		 OR (EFFECTIVE_DT BETWEEN @EfDateBegin
							  AND DATEADD(SECOND, 86399, @EfDateEnd)) )

	AND ((@PcDateBegin IS NULL OR @PcDateEnd IS NULL)
		 OR (PC_DT BETWEEN @PcDateBegin
					   AND DATEADD(SECOND, 86399, @PcDateEnd)) )

	AND (( NULLIF(@CurrPIC,'') IS NULL)
		 OR (ve.FULL_NAME LIKE '%' + ISNULL(@CurrPIC,'') + '%'))

	ORDER BY A.PC_DT, A.PC_NO;


END

