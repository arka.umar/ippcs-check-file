-- =============================================
-- Author:		niit.yudha
-- Create date: 24 jan 2013
-- Description:	memformat string dari 'BETA', 'SUKA', 'LISTRIK' menjadi (Nama = 'BETA') AND (Nama = 'SUKA') AND (Nama = 'LISTRIK')
-- Example:
-- dbo.ColumnParser @Values = 'BETA, SUKA, LISTRIK', -- varchar(100)
--    @Delimeter = ',', -- varchar(10)
--    @ColumnName = 'DockCode', -- varchar(100)
--    @Operator = '' -- varchar(10)

-- =============================================
CREATE PROCEDURE [dbo].[ColumnParser]
	-- Add the parameters for the stored procedure here
    @Values VARCHAR(100) ,
    @Delimeter VARCHAR(10) ,
    @ColumnName VARCHAR(100) ,
    @Operator VARCHAR(10)
AS 
    BEGIN
        DECLARE @i INT ,
            @Value VARCHAR(100) ,
            @Max INT ,
            @Result VARCHAR(800)
              
        DECLARE @xml XML ,
            @Trims VARCHAR(8000)
			
        DECLARE @Alphabetic AS VARCHAR(50) = '%[^A-Za-z0-9 ]%' ,
            @Temp VARCHAR(100)
        
        IF ( @Delimeter IS NULL
             OR @Delimeter = ''
           ) 
            SET @Temp = @Values
        BEGIN
            WHILE PATINDEX(@Alphabetic, @Temp) > 0 
                BEGIN
                    SELECT  @Delimeter = SUBSTRING(@Temp,
                                                   PATINDEX(@Alphabetic, @Temp),
                                                   1)
                    SET @Temp = STUFF(@Temp, PATINDEX(@Alphabetic, @Temp), 1,
                                      '')
                END
        END
					
        IF ( RIGHT(RTRIM(@Values), LEN(@Delimeter)) = @Delimeter ) 
            SET @Trims = LEFT(RTRIM(@Values),
                              LEN(RTRIM(@Values)) - LEN(@Delimeter))
        ELSE 
            SET @Trims = RTRIM(@Values)
					
        SET @xml = CAST(( '<X>' + REPLACE(@Trims, @Delimeter, '</X><X>')
                          + '</X>' ) AS XML)
        SELECT  C.value('.', 'varchar(10)') AS Column1
        INTO    #Splitter
        FROM    @xml.nodes('X') AS X ( C )
	
        SET @i = 1
        SET @Result = ''

        SELECT  ROW_NUMBER() OVER ( ORDER BY Column1 DESC ) AS SeqNbr ,
                Column1
        INTO    #TmpSplitter
        FROM    #Splitter

        SELECT  @Max = MAX(SeqNbr)
        FROM    #TmpSplitter

        WHILE ( @i < ( @Max + 1 ) ) 
            BEGIN
                SELECT  @Value = RTRIM(LTRIM(Column1))
                FROM    #TmpSplitter
                WHERE   SeqNbr = @i
                

                IF ( ISNUMERIC(@Value) = 0 ) 
                    SET @Value = '''' + @Value + ''''
                    
                IF ( @i > 1
                     AND ( @i < @Max )
                   )
                    OR ( @i = @Max
                         AND @i <> 1
                       ) 
                    SET @Result = '(' + @ColumnName + '=' + @Value + ')' + ' '
                        + ' ' + @Operator + ' ' + ' ' + @Result
                ELSE 
                    SET @Result = '(' + @ColumnName + '=' + @Value + ')' + ' '
                        + @Result
                        
                SET @i = @i + 1

            END
    
        SELECT  @Result
        
        DROP	TABLE #TmpSplitter
        DROP	TABLE #Splitter
    END

