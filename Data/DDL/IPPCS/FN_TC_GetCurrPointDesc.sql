CREATE FUNCTION [dbo].[FN_TC_GetCurrPointDesc] (
	@ri_v_curr_area_cd			varchar(8),
	@ri_v_curr_area_cd_alias	varchar(8),
	@ri_v_curr_loc_cd			varchar(8),
	@ri_v_curr_loc_cd_alias		varchar(8),
	@ri_v_delivery_no			varchar(13)
)
RETURNS varchar(64)
AS
BEGIN
	-- if in yard
	if @ri_v_curr_area_cd is not null and @ri_v_curr_area_cd <> ''
	begin
		return 'Yard ' + isnull(@ri_v_curr_loc_cd_alias, 'NULL') + ' - ' + isnull(@ri_v_curr_area_cd_alias, 'NULL')
	end

	-- if in station
	declare @l_v_dock_cd	varchar(3),
		@l_v_station		varchar(3)

	select top 1 @l_v_dock_cd = dcd_1.DOCK_CD,
		@l_v_station = dcd_1.STATION
	from TB_R_DELIVERY_CTL_D dcd_1
	where 1 = 1
		and dcd_1.ARRIVAL_ACTUAL_DT is not null
		and dcd_1.ARRIVAL_ACTUAL_DT <= getdate()
		and dcd_1.DEPARTURE_ACTUAL_DT is null
		and dcd_1.DELIVERY_NO = @ri_v_delivery_no
	order by dcd_1.ARRIVAL_PLAN_DT asc

	if @l_v_dock_cd is not null and @l_v_dock_cd <> ''
	begin
		return 'Dock ' + isnull(@l_v_dock_cd, 'NULL') + ' - ' + isnull(@l_v_station, 'NULL')
	end

	-- if already finish
	if not exists(
		select 1 
		from TB_R_DELIVERY_CTL_D dcd 
		where 1 = 1
			and dcd.DELIVERY_NO = @ri_v_delivery_no
			and dcd.DEPARTURE_ACTUAL_DT is null)
	begin
		return 'Finish'
	end

	-- if on the way
	return 'On The Way'
END

