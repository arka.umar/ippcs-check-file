-- =============================================
-- Author:		Fid.Joko
-- Create date: 2013-11-15
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_REP_PC_TO_BE_RELEASED_PRINT]
	@PCNo varchar(max) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		h.PC_NO,
		h.PC_DT, 
		h.EFFECTIVE_DT,
		s.SUPP_CD,
		s.SUPP_NAME SUPPLIER_NAME,  
		s.SUPP_ADDR STREET,
		s.SUPP_CITY CITY,
		s.SUPP_ATTENTION MAIN_CONTACT,
		s.SUPP_ATTENTION_POSITION MAIN_CONTACT_POSITION,
		s.SUPP_SEARCH_TERM,
		h.COVER_LETTER_BODY_CONTENT, 
		(Select S.SYSTEM_VALUE +', ' + S.SYSTEM_REMARK from TB_M_SYSTEM S where S.SYSTEM_CD='PC_CC_REPORT') CC_VALUE, 
		(Select F.SYSTEM_VALUE from TB_M_SYSTEM F where F.SYSTEM_CD='PC_SIGN_REPORT') SIGN_VALUE, 
		(Select G.SYSTEM_REMARK from TB_M_SYSTEM G where G.SYSTEM_CD='PC_SIGN_REPORT') SIGN_REMARK,
		ITEM_NO=row_number() over (order by h.PC_NO,d.MAT_NO),
		d.MAT_NO,
		d.MAT_DESC,
		SOURCE_TYPE=d.PACKING_TYPE,
		d.PRICE_STATUS,
		d.UOM,
		d.CURRENT_PRICE,
		d.NEW_PRICE,
		--CONVERT(DECIMAL(18,2),((d.NEW_PRICE - d.CURRENT_PRICE)/d.CURRENT_PRICE) * 100) AS PERCENTAGE,
		d.REMARKS,
		ISRELEASE=case when h.RELEASE_DT is not null then '1' else '0' end,
		dbo.fn_GetSystemValue('24008', 'PO_PC_FOOTER') AFOOT 
	from TB_R_PC_H h
	inner join TB_M_SUPPLIER_ICS s on h.SUPPLIER_CD=s.SUPP_CD
	LEFT JOIN TB_R_PC_D d on h.PC_NO=d.PC_NO
	where h.PC_NO=@PCNo

END

