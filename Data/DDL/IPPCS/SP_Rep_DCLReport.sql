-- ==============================================
-- Created By	: Arie
-- Created Date : 2013-02-22
-- Created For	: DCL Report Monty
-- ==============================================

CREATE PROCEDURE [dbo].[SP_Rep_DCLReport]
	@PickupDateFrom varchar (10),
	@PickupDateTo varchar (10),
	@LPCode varchar(100),
	@SupplierCode varchar(100)
AS
BEGIN


SELECT row_number() OVER (ORDER BY SUPPLIER_NAME DESC) AS SeqNbr
     , SUPPLIER_NAME
     , route_code
     , RATE
     , convert(TIME, ARRVDATETIME) AS ARRVDATETIME
     , convert(TIME, DPTDATETIME) AS DPTDATETIME
     , dock_code
     , lp_code
     , PICKUP_DT
     --, plant_code
     INTO 
  #temp
FROM
  (SELECT DISTINCT r.SUPPLIER_NAME
                 , r.ROUTE AS route_code
                 , r.RATE
                 , convert(TIME, r.ARRVDATETIME) AS ARRVDATETIME
                 , convert(TIME, r.DPTDATETIME) AS DPTDATETIME
                 , r.DOCK_CD AS dock_code
                 , r.LP_CD AS lp_code
                 , DATENAME(MONTH, r.PICKUP_DT) + ' ' + CAST(YEAR(r.PICKUP_DT)AS varchar) AS PICKUP_DT
                 --,r.SUPP_PLANT_CD AS plant_code
   FROM
     TB_R_DELIVERY_CTL_SUPPLIERDOCK r
   WHERE
	 --DATENAME(MONTH, r.PICKUP_DT) + ' ' + CAST(YEAR(r.PICKUP_DT)AS varchar) = @PickupDateFrom
	 r.PICKUP_DT >= CAST(@PickupDateFrom AS DATE) AND r.PICKUP_DT <= CAST(@PickupDateTo AS DATE)
     AND r.PICKUP_DT = @PickupDateFrom
     AND ((r.SUPPLIER_CD + '-' + r.SUPP_PLANT_CD = @SupplierCode
     AND isnull(@SupplierCode, '') <> ''
     OR (isnull(@SupplierCode, '') = '')))
     AND ((r.LP_CD = @LPCode
     AND isnull(@LPCode, '') <> ''
     OR (isnull(@LPCode, '') = '')))) AS a
ORDER BY
  SeqNbr ASC   
  
CREATE TABLE #temp2
  (
  SeqNbr INT,
  SupplierName Varchar(500),
  RouteCode VARCHAR(4),
  Rate VARCHAR(2),
  Arrival varchar(5),
  Departure varchar(5),
  DockCode VARCHAR(500),
  Lp_Code VARCHAR(3)
  ,PickUp VARCHAR(100)
  --,plant_code VARCHAR (10)
)
DECLARE @Max INT,
        @i INT,
        @RouteCode VARCHAR(4),
        @RateCode VARCHAR(2),
        @DockCode VARCHAR(2),
        @Supplier VARCHAR(250),
        @Arrival TIME,
        @Departure TIME


SELECT @Max = MAX(SeqNbr)  , @i = 1
FROM #temp  

WHILE (@i <= @Max)
  BEGIN
		SELECT DISTINCT @RouteCode = route_code
			 , @RateCode = RATE
			 , @DockCode = dock_code
			 , @Supplier = SUPPLIER_NAME
			 , @Arrival = CONVERT (TIME,ARRVDATETIME)
			 , @Departure = CONVERT (TIME,DPTDATETIME)
		FROM
		  #temp
		WHERE
		  SeqNbr = @i
			 
		IF NOT EXISTS (SELECT 1
					   FROM
						 #temp2
					   WHERE
						 RouteCode = @RouteCode
						 AND Rate = @RateCode
						 AND SupplierName = @Supplier
						 AND Arrival = @Arrival
						 AND Departure = @Departure)
			 BEGIN
					INSERT INTO #Temp2
					SELECT *
					FROM	#temp
					WHERE	SeqNbr = @i			
								 
			  END
			ELSE
			  BEGIN			
  					UPDATE #temp2
					SET DockCode = @DockCode + ', ' + (SELECT DockCode FROM #Temp2 WHERE RouteCode = @RouteCode AND Rate = @RateCode 
					AND SupplierName = @Supplier AND Arrival = @Arrival AND Departure = @Departure)
					FROM #temp2
					WHERE RouteCode = @RouteCode AND Rate = @RateCode AND SupplierName = @Supplier 
					AND Arrival = @Arrival AND Departure = @Departure
			  END
			  
		--SELECT @DockCode = '', @RouteCode = '', @RateCode = '', @Supplier = ''
        SET @i = @i + 1
  END
  
  
  SELECT * FROM #temp2
  
  
  DROP TABLE #temp
  DROP TABLE #temp2


END

