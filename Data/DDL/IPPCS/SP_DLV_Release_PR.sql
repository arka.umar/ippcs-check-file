/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : PR release
*/

CREATE PROCEDURE [dbo].[SP_DLV_Release_PR]

@PRNO Varchar (MAX),
@CREATED_BY VARCHAR (25)
--,@POSITION_ID INT

AS
BEGIN

		DECLARE @POSITION_ID INT, @CHECK_AUTH INT;
		
		DECLARE @CHECKSTS VARCHAR (3), @CHECK_STATUS_FLAG INT, @CHECK_APPROVAL VARCHAR(25), @CHECK_SH_APP VARCHAR (20), @CHECK_DPH_APP VARCHAR(20);
		DECLARE @STS VARCHAR (3), @APPROVED_BY VARCHAR (20), @APPROVED_DT VARCHAR (20),@POS INT, @LVL INT;

		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_Release_PR'
		DECLARE @step AS VARCHAR(50) = 'Init'
		DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)
		DECLARE @CHECK_MODULE INT
		DECLARE @MODUL_ID INT = 3
		DECLARE @FUNCTION_ID VARCHAR (6) =  '31105'
	
		DECLARE @STS_ERROR INT;
		--==Temporary==--
			--  == VARIABLE TABLE TEMPORARY UNTUK EXPLODING GRID ==
			DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
			DECLARE @MIN_ROW INT;
			DECLARE @ROW_DETAIL VARCHAR(MAX);
			DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
			DECLARE @MIN_FIELD INT;
			-- ================================================================
			--  == Variabel Untuk Menyimpan Data Ke TABLE DETAIL ==
			DECLARE @arr_1 VARCHAR(12);
			INSERT INTO @ROWS
			SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@PRNO,';');
			-- ================================================================
						

		CREATE TABLE #PR_H_TEMP(
				PR_NO varchar(11),
				PROD_MONTH varchar(6),
				LP_CD varchar(4),
				PR_STATUS_FLAG varchar(3),
				PR_NOTES varchar(255),
				SH_APPROVED_BY varchar(20),
				SH_APPROVED_DT datetime,
				DPH_APPROVED_BY varchar(20),
				DPH_APPROVED_DT datetime,
				DH_APPROVED_BY varchar(20),
				DH_APPROVED_DT datetime,
				REJECTED_BY varchar(20),
				REJECTED_POSITION varchar(3),
				REJECTED_DT datetime,
				REMARK varchar(255),
				RELEASED_FLAG varchar(1),
				CREATED_BY varchar(20),
				CREATED_DT datetime,
				CHANGED_BY varchar(20),
				CHANGED_DT datetime,
				PROCESS_ID bigint
		);

		CREATE TABLE #TB_T_PR_RETRIEVAL(
						PROD_MONTH varchar(6),
						LP_CD varchar(4),
						ROUTE_CD varchar(4),
						SOURCE varchar(20),
						PR_QTY int,
						PR_STATUS_FLAG varchar(3),
						PROCESS_ID bigint,
						CREATED_DT datetime,
						CREATED_BY varchar(50),
						CHANGED_DT datetime,
						CHANGED_BY varchar(50)
				);
		
		CREATE TABLE #TEMP(
					PR_NO VARCHAR(12)
		);
		
		--==CREATE LOG==--
		SET @steps = @na+'.'+@step;
		SET @log = 'PR Approval process started.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		--==END CREATE LOG==--

		SET @steps = @na+'.'+@step;
		SET @log = 'PR No : ' + ISNULL(@PRNO, '');
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		
		--Modif by agung 02.07.2014
		SET @steps = @na+'.Process';
		SET @log = 'Check Current Production Month';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		DECLARE @CHECK_PROD_MONTH_1 VARCHAR(12), @CHECK_PROD_MONTH_2 VARCHAR(12), @CHECK_PROD_MONTH_3 VARCHAR(12)
		SET @CHECK_PROD_MONTH_1 = (SELECT REPLACE(CONVERT(VARCHAR(7),GETDATE(),111),'/',''));
		SET @CHECK_PROD_MONTH_2 = (SELECT DISTINCT PROD_MONTH FROM TB_R_DLV_PR_H WHERE PR_NO IN (SELECT ITEM FROM @ROWS));
		SET @CHECK_PROD_MONTH_3 = (SELECT REPLACE(CONVERT(VARCHAR(7),DATEADD(mm, 1, GETDATE()),111),'/',''));
		
		IF ((@CHECK_PROD_MONTH_1 = @CHECK_PROD_MONTH_2) OR (@CHECK_PROD_MONTH_2 = @CHECK_PROD_MONTH_3)) BEGIN
								
								
								SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID IN (@FUNCTION_ID, '31103') AND PROCESS_END_DT IS NULL );
								IF (@CHECK_MODULE=0) BEGIN
								
										-- Insert QUEUE
										INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
										VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());

								
										--==CREATE LOG==--
										SET @steps = @na+'.Process';
										SET @log = 'Check authorization level';
										EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
										--==END CREATE LOG==--
										
										-- Update QUEUE
										UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;

										SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE JOB_FUNCTION_ID = 6 AND POSITION_ID IN (6,3,12) AND USERNAME = @CREATED_BY);
										
										IF (@CHECK_AUTH=1)BEGIN

												
												SET @POSITION_ID = (SELECT POSITION_LEVEL  FROM [dbo].[TB_R_VW_EMPLOYEE] WHERE USERNAME = @CREATED_BY);

												IF (@POSITION_ID =50) BEGIN
													SET @POS = 3;
													SET @APPROVED_BY = 'SH_APPROVED_BY';
													SET @APPROVED_DT = 'SH_APPROVED_DT';
													SET @STS = 'PR3';
												END
												ELSE IF (@POSITION_ID =40) BEGIN
													SET @POS = 4;
													SET @APPROVED_BY = 'DPH_APPROVED_BY';
													SET @APPROVED_DT = 'DPH_APPROVED_DT';
													SET @STS = 'PR4';
												END
												ELSE BEGIN 
													SET @POS = 5;
													SET @APPROVED_BY = 'DH_APPROVED_BY';
													SET @APPROVED_DT = 'DH_APPROVED_DT';
													SET @STS = 'PR5';
												END

												--==CREATE LOG==--
												SET @steps = @na+'.Process';
												SET @log = 'Parsing data from parameter';
												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
												--==END CREATE LOG==--
												

												
												--  == Ambil Nilai terkecil dari VARIABEL TABLE @ROW ==
												SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

												WHILE @MIN_ROW IS NOT NULL BEGIN
															-- 6.3.1 == Hapus VARIABLE TABLE @FIELDS jika ada ==
															DELETE FROM @FIELDS;
															
															SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
															INSERT INTO @FIELDS
															SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'|');
															-- ========================================================================================
															-- 6.3.3 == Ambil ITEM Berdasarkan ID untuk dimasukkan kedalam VARIABLE FIELD Detail ==
															SET @arr_1 = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
															
															INSERT INTO #TEMP VALUES (@arr_1)

															SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
																			-- =================================================================================
												END --END WHILE
												--==Temporary==--
												

												
												SET @CHECK_STATUS_FLAG = (SELECT COUNT(PR_STATUS_FLAG) FROM TB_R_DLV_PR_H 
																									JOIN #TEMP ON TB_R_DLV_PR_H.PR_NO=#TEMP.PR_NO
																									WHERE TB_R_DLV_PR_H.PR_STATUS_FLAG IN ('PR5','PR6'));
											
												

												IF (@CHECK_STATUS_FLAG = 0) BEGIN

															DECLARE @PRNO_T varchar(12);

															DECLARE BTMP_CUR2 CURSOR
															FOR

															SELECT PR_NO
															FROM #TEMP
															;

															OPEN BTMP_CUR2;
															FETCH NEXT FROM BTMP_CUR2 INTO
															@PRNO_T;

															WHILE @@FETCH_STATUS = 0
															BEGIN

																-- disini yg diambil per rownya
																--select @ROW1,@ROW2;
																			--==CREATE LOG==--
																			SET @steps = @na+'.Process';
																			SET @log = 'Checking PR_STATUS_FLAG  for PR Number '+@PRNO_T;
																			EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																			--==END CREATE LOG==--

																			SET @CHECKSTS = (SELECT PR_STATUS_FLAG FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T);
																			
																			IF (@CHECKSTS = 'PR2') BEGIN
																							SET @LVL = 2;
																					END
																					ELSE IF (@CHECKSTS = 'PR3') BEGIN
																						SET @LVL = 3;
																					END
																					ELSE IF (@CHECKSTS = 'PR4') BEGIN
																						SET @LVL = 4;
																					END
																					ELSE BEGIN
																						SET @LVL = 5;
																					END

																			IF (@CHECKSTS = 'PR5') OR (@CHECKSTS = 'PR6') BEGIN
																					

																					--== UPDATE QUEUE
																					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

																					--==CREATE LOG==--
																					SET @steps = @na+'.Process';
																					SET @log = 'Failed to approve PR Number '+@PRNO_T;
																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
																					--==END CREATE LOG==--
																					

																					--==CREATE LOG==--
																					SET @steps = @na+'.Finish';
																					SET @log = 'PR approval process has been finished.';
																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					--==END CREATE LOG==--
																					

																					RAISERROR('Failed to approve.',16,1)
																					RETURN
																				
																			END
																			ELSE BEGIN
																					IF (@POS > @LVL) BEGIN
																							
																							--INSERT TEMPORARY PR_H-----
																							INSERT INTO #PR_H_TEMP (PR_NO, PROD_MONTH, LP_CD, PR_STATUS_FLAG, PR_NOTES, SH_APPROVED_BY, SH_APPROVED_DT,
																																			DPH_APPROVED_BY, DPH_APPROVED_DT, DH_APPROVED_BY, DH_APPROVED_DT, REJECTED_BY, 
																																			REJECTED_POSITION, REJECTED_DT, REMARK, RELEASED_FLAG, CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT, PROCESS_ID)
																							SELECT PR_NO, PROD_MONTH, LP_CD, PR_STATUS_FLAG, PR_NOTES, SH_APPROVED_BY, SH_APPROVED_DT,
																																			DPH_APPROVED_BY, DPH_APPROVED_DT, DH_APPROVED_BY, DH_APPROVED_DT, REJECTED_BY, 
																																			REJECTED_POSITION, REJECTED_DT, REMARK, RELEASED_FLAG, CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT, PROCESS_ID

																							FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T;
																							----------------------------
																							

																							IF (@POS=3) BEGIN

																									--==CREATE LOG==--
																									SET @steps = @na+'.Process';
																									SET @log = 'Updating TB_R_DLV_PR_H for PR Number '+@PRNO_T;
																									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																									--==END CREATE LOG==--

																									UPDATE TB_R_DLV_PR_H
																									SET
																									PR_STATUS_FLAG = @STS,
																									SH_APPROVED_BY = @CREATED_BY,
																									SH_APPROVED_DT = GETDATE(),
																									CHANGED_BY = @CREATED_BY,
																									PROCESS_ID = @pid,
																									CHANGED_DT =  GETDATE()
																									WHERE 
																									PR_NO = @PRNO_T
																									
																										

																							END

																							ELSE IF (@POS = 4) BEGIN
																									
																									SET @CHECK_SH_APP = (SELECT SH_APPROVED_BY FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T);
																									
																									IF (@CHECK_SH_APP IS NULL) BEGIN
																			
														
																												--==CREATE LOG==--
																												SET @steps = @na+'.Process';
																												SET @log = 'Updating TB_R_DLV_PR_H for PR Number '+@PRNO_T;
																												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																												--==END CREATE LOG==--

																												UPDATE TB_R_DLV_PR_H
																												SET
																												PR_STATUS_FLAG = @STS,
																												SH_APPROVED_BY = @CREATED_BY,
																												SH_APPROVED_DT = GETDATE(),
																												DPH_APPROVED_BY = @CREATED_BY,
																												DPH_APPROVED_DT = GETDATE(),
																												CHANGED_BY = @CREATED_BY,
																												PROCESS_ID = @pid,
																												CHANGED_DT =  GETDATE()
																												WHERE 
																												PR_NO = @PRNO_T




																									END 
																									ELSE BEGIN

																												--==CREATE LOG==--
																												SET @steps = @na+'.Process';
																												SET @log = 'Updating TB_R_DLV_PR_H for PR Number '+@PRNO_T;
																												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																												--==END CREATE LOG==--

																												UPDATE TB_R_DLV_PR_H
																												SET
																												PR_STATUS_FLAG = @STS,
																												DPH_APPROVED_BY = @CREATED_BY,
																												DPH_APPROVED_DT = GETDATE(),
																												CHANGED_BY = @CREATED_BY,
																												PROCESS_ID = @pid,
																												CHANGED_DT =  GETDATE()
																												WHERE 
																												PR_NO = @PRNO_T
																									END


																							END
																							ELSE BEGIN
																									
																									SET @CHECK_SH_APP = (SELECT SH_APPROVED_BY FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T);
																									SET @CHECK_DPH_APP = (SELECT DPH_APPROVED_BY FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T);

																									IF (@CHECK_SH_APP IS NULL AND @CHECK_DPH_APP IS NULL)BEGIN

																												--==CREATE LOG==--
																												SET @steps = @na+'.Process';
																												SET @log = 'Updating TB_R_DLV_PR_H for PR Number '+@PRNO_T;
																												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																												--==END CREATE LOG==--

																												UPDATE TB_R_DLV_PR_H
																												SET
																												PR_STATUS_FLAG = @STS,
																												SH_APPROVED_BY = @CREATED_BY,
																												SH_APPROVED_DT = GETDATE(),
																												DPH_APPROVED_BY = @CREATED_BY,
																												DPH_APPROVED_DT = GETDATE(),
																												DH_APPROVED_BY = @CREATED_BY,
																												DH_APPROVED_DT = GETDATE(),
																												CHANGED_BY = @CREATED_BY,
																												CHANGED_DT =  GETDATE(),
																												PROCESS_ID = @pid,
																												RELEASED_FLAG = 'Y'
																												WHERE 
																												PR_NO = @PRNO_T

																									END
																									ELSE IF(@CHECK_DPH_APP IS NULL)BEGIN

																												--==CREATE LOG==--
																												SET @steps = @na+'.Process';
																												SET @log = 'Updating TB_R_DLV_PR_H for PR Number '+@PRNO_T;
																												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																												--==END CREATE LOG==--

																												UPDATE TB_R_DLV_PR_H
																												SET
																												PR_STATUS_FLAG = @STS,
																												DPH_APPROVED_BY = @CREATED_BY,
																												DPH_APPROVED_DT = GETDATE(),
																												DH_APPROVED_BY = @CREATED_BY,
																												DH_APPROVED_DT = GETDATE(),
																												CHANGED_BY = @CREATED_BY,
																												CHANGED_DT =  GETDATE(),
																												PROCESS_ID = @pid,
																												RELEASED_FLAG = 'Y'
																												WHERE 
																												PR_NO = @PRNO_T

																									END
																									ELSE BEGIN

																												--==CREATE LOG==--
																												SET @steps = @na+'.Process';
																												SET @log = 'Updating TB_R_DLV_PR_H for PR Number '+@PRNO_T;
																												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																												--==END CREATE LOG==--

																												UPDATE TB_R_DLV_PR_H
																												SET
																												PR_STATUS_FLAG = @STS,
																												DH_APPROVED_BY = @CREATED_BY,
																												DH_APPROVED_DT = GETDATE(),
																												CHANGED_BY = @CREATED_BY,
																												CHANGED_DT =  GETDATE(),
																												PROCESS_ID = @pid,
																												RELEASED_FLAG = 'Y'
																												WHERE 
																												PR_NO = @PRNO_T
																									END
																							END
																							
																							

																							DECLARE @PROD_MONTH varchar(12),@LP_CD varchar(4), @ROUTE_CD VARCHAR(4);

																								DECLARE BTMP_CUR CURSOR
																								FOR
																								SELECT  prh.PROD_MONTH AS PROD_MONTH, prh.LP_CD AS LP_CD, prd.ROUTE_CD AS ROUTE_CD 
																								FROM TB_R_DLV_PR_H prh JOIN TB_R_DLV_PR_D prd ON prh.PR_NO = prd.PR_NO
																								WHERE prh.PR_NO=@PRNO_T;

																								OPEN BTMP_CUR;
																								FETCH NEXT FROM BTMP_CUR INTO
																								@PROD_MONTH,@LP_CD, @ROUTE_CD;

																								WHILE @@FETCH_STATUS = 0
																								BEGIN

																											--==CREATE LOG==--
																											SET @steps = @na+'.Process';
																											SET @log = 'Updating TB_T_PR_RETRIEVAL with PROD_MONTH = '+@PROD_MONTH+', LP_CD ='+@LP_CD+' AND ROUTE_CD='+@ROUTE_CD;
																											EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																											--==END CREATE LOG==--

																											INSERT INTO #TB_T_PR_RETRIEVAL 
																											(PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, PROCESS_ID, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT)
																											SELECT PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, PROCESS_ID, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT 
																											FROM TB_T_PR_RETRIEVAL
																											WHERE PROD_MONTH =@PROD_MONTH AND LP_CD=@LP_CD AND ROUTE_CD=@ROUTE_CD;

																											UPDATE TB_T_PR_RETRIEVAL
																											SET PR_STATUS_FLAG = @STS, CHANGED_BY = @CREATED_BY, CHANGED_DT =  GETDATE(), PROCESS_ID=@pid
																											WHERE PROD_MONTH =@PROD_MONTH AND LP_CD=@LP_CD AND ROUTE_CD=@ROUTE_CD;


																									


																								FETCH NEXT FROM BTMP_CUR INTO
																									@PROD_MONTH,@LP_CD, @ROUTE_CD
																								END;
																								CLOSE BTMP_CUR;
																								DEALLOCATE BTMP_CUR;

																								--== UPDATE QUEUE
																								--UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
																								SET @STS_ERROR = 0;
																							
																					END

																					ELSE BEGIN

																							--disini
																							DELETE FROM TB_R_DLV_PR_H WHERE PROCESS_ID = @pid;
													
																							INSERT INTO TB_R_DLV_PR_H (PR_NO, PROD_MONTH, LP_CD, PR_STATUS_FLAG, PR_NOTES, SH_APPROVED_BY, SH_APPROVED_DT,
																																			DPH_APPROVED_BY, DPH_APPROVED_DT, DH_APPROVED_BY, DH_APPROVED_DT, REJECTED_BY, 
																																			REJECTED_POSITION, REJECTED_DT, REMARK, RELEASED_FLAG, CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT, PROCESS_ID)

																							SELECT PR_NO, PROD_MONTH, LP_CD, PR_STATUS_FLAG, PR_NOTES, SH_APPROVED_BY, SH_APPROVED_DT,
																										 DPH_APPROVED_BY, DPH_APPROVED_DT, DH_APPROVED_BY, DH_APPROVED_DT, REJECTED_BY, 
																										 REJECTED_POSITION, REJECTED_DT, REMARK, RELEASED_FLAG, CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT, PROCESS_ID
																							FROM #PR_H_TEMP;


																							DELETE FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid;

																							INSERT INTO TB_T_PR_RETRIEVAL 
																							(PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, PROCESS_ID, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT)
																							SELECT PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, PROCESS_ID, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT 
																							FROM #TB_T_PR_RETRIEVAL;


																							--== UPDATE QUEUE
																							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

																							--==CREATE LOG==--
																							SET @steps = @na+'.Process';
																							SET @log = 'Failed to approve PR Number '+@PRNO_T;
																							EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
																							--==END CREATE LOG==--
																															

																							--==CREATE LOG==--
																							SET @steps = @na+'.Finish';
																							SET @log = 'PR approval process has been finished.';
																							EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																							--==END CREATE LOG==--

																							SET @STS_ERROR = 1;

																							RAISERROR('PR already approved.',16,1)
																							RETURN
																					END
																			END


															FETCH NEXT FROM BTMP_CUR2 INTO
																@PRNO_T
															END;
															CLOSE BTMP_CUR2;
															DEALLOCATE BTMP_CUR2;
												
															IF (@STS_ERROR = 0) BEGIN
																		--CREATE LOG
																		SET @steps = @na+'.Proses';
																		SET @log = 'Starting sent e-mail.';

																		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																								--===========SEND E-MAIL
																								BEGIN TRY
																										DECLARE @RecipientEmail varchar(max),
																												@RecipientCopyEmail varchar(max),
																												@RecipientBlindCopyEmail varchar(max),
																												@ERR_FLAG varchar(50),
																												@body varchar(max),
																												@subjek varchar(100),
																												@ERROR_DETAIL varchar(max),
																												@profile varchar(max),
																												@MAIL_QUERY VARCHAR(MAX),
																												@ACTION VARCHAR(25),
																												@LINK VARCHAR(MAX)
																												
																										SET @profile = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE');
																										if (@profile IS NULL) BEGIN
																												SET @profile = 'NotificationAgent';
																										END
																										
																										IF (@POSITION_ID = 50) BEGIN
																											SET @ACTION = 'PR_APPROVAL_SH';
																											SET @LINK ='<a href="https://portal.toyota.co.id/ippcs-app/D31104"> https://portal.toyota.co.id </a>';
																										END
																										ELSE IF (@POSITION_ID = 40) BEGIN
																											SET @ACTION = 'PR_APPROVAL_DPH';
																											SET @LINK ='<a href="https://portal.toyota.co.id/ippcs-app/D31104"> https://portal.toyota.co.id </a>';
																										END
																										ELSE IF (@POSITION_ID = 30) BEGIN
																											SET @ACTION = 'PR_APPROVAL_RELEASE';
																											SET @LINK ='<a href="https://portal.toyota.co.id/ippcs-app/D31202"> https://portal.toyota.co.id </a>';
																										END
																											
																										SELECT @RecipientEmail=EMAIL_TO, @RecipientCopyEmail=EMAIL_CC, @RecipientBlindCopyEmail=EMAIL_BCC 
																										FROM TB_M_RTP_NOTIFICATION_EMAIL 
																										WHERE MODULE_ID = CAST(@MODUL_ID AS VARCHAR) 
																											AND FUNCTION_ID = @FUNCTION_ID AND ACTION = @ACTION
																										
																										SELECT  @subjek=NOTIFICATION_SUBJECT,
																												@body=NOTIFICATION_CONTENT
																										FROM    TB_M_NOTIFICATION_CONTENT
																										WHERE   FUNCTION_ID = @FUNCTION_ID
																												AND ROLE_ID = '31'
																												AND NOTIFICATION_METHOD = '1'
																										
																										SET @subjek = REPLACE(@subjek, '@ProdMonth', RIGHT(LEFT(@PRNO, 9), 6))
																										
																										IF ((@POSITION_ID = 50) OR (@POSITION_ID = 40)) BEGIN
																											SET @subjek = REPLACE(@subjek, '@subjectaction', 'Approval')
																											SET @body = REPLACE(@body, '@bodyaction', 'approved')
																										END ELSE BEGIN
																											SET @subjek = REPLACE(@subjek, '@subjectaction', 'Release')
																											SET @body = REPLACE(@body, '@bodyaction', 'released')
																										END
																										
																										SET @body = REPLACE(@body, '@approve', @CREATED_BY)
																										SET @body = REPLACE(@body, '@DateTime', (SELECT CAST(DAY(GETDATE()) AS VARCHAR) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR)))
																										SET @body = REPLACE(@body, '@prno', @PRNO)
																										SET @body = REPLACE(@body, '@link', @LINK)
																										
																										IF (@RecipientCopyEmail IS NOT NULL)
																											SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
																											
																										IF (@RecipientCopyEmail IS NOT NULL)
																											SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
																										
																										SET @body = ISNULL(@body, '')
																										SET @subjek = ISNULL(@subjek, '')
																										
																										SET @RecipientEmail = ISNULL(@RecipientEmail, '')
																										SET @RecipientCopyEmail = ISNULL(@RecipientCopyEmail, '')
																										SET @RecipientBlindCopyEmail = ISNULL(@RecipientBlindCopyEmail, '')
																										
																										SET @MAIL_QUERY = '	
																															EXEC msdb.dbo.sp_send_dbmail
																																@profile_name	= ''' + @profile + ''',
																																@body 			= ''' + @body + ''',
																																@body_format 	= ''HTML'',
																																@recipients 	= ''' + @RecipientEmail + ''',
																																' + @RecipientCopyEmail + '
																																' + @RecipientBlindCopyEmail + '
																																@subject 		= ''' + @subjek + ''''
																										
																										EXEC (@MAIL_QUERY)

																										SET @steps = @na+'.finished';
																										SET @log = 'Email notification has been sent';

																										EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																										

																										UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE()
																										WHERE PROCESS_ID = @pid;

																										--CREATE LOG
																										SET @steps = @na+'.Proses';
																										SET @log = 'send e-mail has been finished.';

																										EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;


																								--==FINISH==--

																								END TRY
																								BEGIN CATCH

																											--CREATE LOG
																											SET @steps = @na+'.finished';
																											SET @log = 'Email notification failed sent to all PIC.' + @MAIL_QUERY;
																											EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;

																											UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
																											WHERE PROCESS_ID = @pid;


																								END CATCH

						--===========END OF SEND E-MAIL
															END

												END
												ELSE BEGIN

														--== UPDATE QUEUE
														UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

														--==CREATE LOG==--
														SET @steps = @na+'.Process';
														SET @log = 'Failed to approve PR';
														EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
														--==END CREATE LOG==--
																						

														--==CREATE LOG==--
														SET @steps = @na+'.Finish';
														SET @log = 'PR approval process has been finished.';
														EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
														--==END CREATE LOG==--
														
														RAISERROR('Failed to approve.',16,1)
														RETURN

												END


												--==CREATE LOG==--
												SET @steps = @na+'.Finish';
												SET @log = 'PR Approval process has been finished.';
												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
												--==END CREATE LOG==--

												UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL;
												UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;

										END
										ELSE BEGIN

												--== UPDATE QUEUE
												UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
												--==CREATE LOG==--
												SET @steps = @na+'.Process';
												SET @log = 'Access denied!';
												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
												--==END CREATE LOG==--
												
												--==CREATE LOG==--
												SET @steps = @na+'.Finish';
												SET @log = 'PR Approval process has been finished.';
												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
												--==END CREATE LOG==--

												RAISERROR('You are not authorized to perform this action.',16,1)
												RETURN

										END
								END

								ELSE BEGIN
										
										RAISERROR('Other process is still running by another user.',16,1)
										RETURN
								END

								DROP TABLE #TEMP;
				END
				ELSE BEGIN
								--==CREATE LOG==--
								SET @steps = @na+'.Process';
								SET @log = 'PR cannot be approved with this production month!';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
								--==END CREATE LOG==--
								
								--==CREATE LOG==--
								SET @steps = @na+'.Finish';
								SET @log = 'PR Approval process has been finished.';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
								--==END CREATE LOG==--

								RAISERROR('PR Production Month Not In Current Month And Next Month',16,1)
								RETURN
				END

END

