-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	05-Oct-2020
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_COMMON_GR_SUP_ASSIGNMENT] @USER_ID VARCHAR(20) = 'AGAN'
	,@PROCESS_ID BIGINT = '202009230021'
	,@r_mat_no VARCHAR(100)
	,@r_color_sfx VARCHAR(100)
	,@r_source_type VARCHAR(100)
	,@r_prod_purpose_cd VARCHAR(100)
	,@r_ref_date DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MODULE_ID VARCHAR(50) = '4'
		,@FUNCTION_ID VARCHAR(100) = '42005'
		,@IsError CHAR(1) = 'N'
		,@CURRDT DATE = CAST(GETDATE() AS DATE)
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_COMMON_GR_SUP_ASSIGNMENT'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@RES INT = 0
		,@r_supp_cd VARCHAR(100);

	BEGIN TRY
		DECLARE @l_v_status VARCHAR(2) = 'OK'
			,@l_v_prod_purpose_cd VARCHAR(50)
			,@lv_supp_cd VARCHAR(50)
			,@ln_records INT = 0
			,@ln_status_supp_cd INT = 0;

		--* Mandatory Checking *--
		--check validation parameter
		IF ISNULL(@r_mat_no, '') = ''
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00003ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Material number'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00003ERR'
				,'ERR'
				,'MPCS00003ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ' : Mandatory Checking'
				,@USER_ID
				,GETDATE()
		END;

		IF ISNULL(@r_ref_date, '') = ''
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00003ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Ref date'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00003ERR'
				,'ERR'
				,'MPCS00003ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ' : Mandatory Checking'
				,@USER_ID
				,GETDATE()
		END;

		IF ISNULL(@r_prod_purpose_cd, '') = ''
		BEGIN
			SELECT @l_v_prod_purpose_cd = SYSTEM_VALUE
			FROM TB_M_SYSTEM
			WHERE FUNCTION_ID = 'COMMON_PO'
				AND SYSTEM_CD = 'PROD_PURPOSE_CD'

			IF ISNULL(@l_v_prod_purpose_cd, '') = ''
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Production purpose code is not available in system master';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Mandatory Checking'
					,@USER_ID
					,GETDATE()
			END
		END
		ELSE
		BEGIN
			SET @l_v_prod_purpose_cd = @r_prod_purpose_cd;
		END;

		--end check validation parameter
		IF @IsError <> 'Y'
		BEGIN
			SET @ln_records = 0;
			SET @ln_status_supp_cd = 0;

			DECLARE @Cn INT = 1
				,@CnAll INT = 0
				,@SupCdPar VARCHAR(100) = '';

			SELECT ROW_NUMBER() OVER (
					ORDER BY (
							SELECT 1
							)
					) NO
				,SUPP_CD
			INTO #tb_lv_supp_cd1
			FROM TB_M_SOURCE_LIST
			WHERE MAT_NO = @r_mat_no
				AND PART_COLOR_SFX = @r_color_sfx
				AND SOURCE_TYPE = @r_source_type
				AND PROD_PURPOSE_CD = @l_v_prod_purpose_cd
				AND CONVERT(VARCHAR, VALID_DT_FR, 112) <= CONVERT(VARCHAR, @r_ref_date, 112)
				AND ISNULL(CONVERT(VARCHAR, VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @r_ref_date, 112)

			SELECT @CnAll = COUNT(1)
			FROM #tb_lv_supp_cd1

			WHILE @Cn <= @CnAll
			BEGIN
				SET @ln_records = @ln_records + 1;

				SELECT @lv_supp_cd = SUPP_CD
				FROM #tb_lv_supp_cd1
				WHERE NO = @Cn

				IF @ln_records > 1
				BEGIN
					IF @SupCdPar = @lv_supp_cd
					BEGIN
						IF @ln_status_supp_cd = 1
						BEGIN
							SET @ln_status_supp_cd = 1;
						END
						ELSE
						BEGIN
							SET @ln_status_supp_cd = 0;
						END;
					END
					ELSE
					BEGIN
						SET @ln_status_supp_cd = 1;
					END;
				END;

				SET @SupCdPar = @lv_supp_cd;
				SET @Cn = @Cn + 1;
			END

			DROP TABLE #tb_lv_supp_cd1

			IF @ln_records = 1
			BEGIN
				SET @r_supp_cd = @lv_supp_cd;
			END
			ELSE
				IF @ln_records > 1
				BEGIN
					IF @ln_status_supp_cd = 0
					BEGIN
						--there is more than 1 supp_cd but they are same
						--SET @IsError = 'Y';
						SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' is found more than 1 record but assign to supplier (check source list)';

						EXEC dbo.CommonGetMessage 'MSPT00001ERR'
							,@MSG_TXT OUTPUT
							,@N_ERR OUTPUT
							,@PARAM1

						INSERT INTO TB_R_LOG_D (
							PROCESS_ID
							,SEQUENCE_NUMBER
							,MESSAGE_ID
							,MESSAGE_TYPE
							,[MESSAGE]
							,LOCATION
							,CREATED_BY
							,CREATED_DATE
							)
						SELECT @PROCESS_ID
							,(
								SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
								FROM dbo.TB_R_LOG_D
								WHERE PROCESS_ID = @PROCESS_ID
								)
							,'MSPT00001ERR'
							,'ERR'
							,'MSPT00001ERR : ' + @MSG_TXT
							,@LOG_LOCATION + ' : Mandatory Checking'
							,@USER_ID
							,GETDATE()

						SET @r_supp_cd = @lv_supp_cd;
					END
					ELSE
					BEGIN
						--there is more than 1 supp_cd but they are different
						SET @IsError = 'Y';
						SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' assign to more than 1 supplier (check source list)';

						EXEC dbo.CommonGetMessage 'MSPT00001ERR'
							,@MSG_TXT OUTPUT
							,@N_ERR OUTPUT
							,@PARAM1

						INSERT INTO TB_R_LOG_D (
							PROCESS_ID
							,SEQUENCE_NUMBER
							,MESSAGE_ID
							,MESSAGE_TYPE
							,[MESSAGE]
							,LOCATION
							,CREATED_BY
							,CREATED_DATE
							)
						SELECT @PROCESS_ID
							,(
								SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
								FROM dbo.TB_R_LOG_D
								WHERE PROCESS_ID = @PROCESS_ID
								)
							,'MSPT00001ERR'
							,'ERR'
							,'MSPT00001ERR : ' + @MSG_TXT
							,@LOG_LOCATION + ' : Mandatory Checking'
							,@USER_ID
							,GETDATE()

						SET @r_supp_cd = NULL;
					END
				END --first step
				ELSE
				BEGIN -- = 0 first step
					PRINT 'second step'

					SET @ln_records = 0;
					SET @ln_status_supp_cd = 0;

					SELECT ROW_NUMBER() OVER (
							ORDER BY (
									SELECT 1
									)
							) NO
						,SUPP_CD
					INTO #tb_lv_supp_cd2
					FROM TB_M_SOURCE_LIST
					WHERE MAT_NO = @r_mat_no
						AND PART_COLOR_SFX IN (
							SELECT SYSTEM_VALUE
							FROM TB_M_SYSTEM
							WHERE FUNCTION_ID = 'COMMON_PO'
								AND SYSTEM_CD = 'PART_COLOR_SFX_PRICE'
							)
						AND SOURCE_TYPE = @r_source_type
						AND PROD_PURPOSE_CD = @l_v_prod_purpose_cd
						AND CONVERT(VARCHAR, VALID_DT_FR, 112) <= CONVERT(VARCHAR, @r_ref_date, 112)
						AND ISNULL(CONVERT(VARCHAR, VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @r_ref_date, 112)

					SET @Cn = 1;
					SET @CnAll = 0;
					SET @SupCdPar = ''

					SELECT @CnAll = COUNT(1)
					FROM #tb_lv_supp_cd2

					WHILE @Cn <= @CnAll
					BEGIN
						SET @ln_records = @ln_records + 1;

						SELECT @lv_supp_cd = SUPP_CD
						FROM #tb_lv_supp_cd2
						WHERE NO = @Cn

						IF @ln_records > 1
						BEGIN
							IF @SupCdPar = @lv_supp_cd
							BEGIN
								IF @ln_status_supp_cd = 1
								BEGIN
									SET @ln_status_supp_cd = 1;
								END
								ELSE
								BEGIN
									SET @ln_status_supp_cd = 0;
								END;
							END
							ELSE
							BEGIN
								SET @ln_status_supp_cd = 1;
							END;
						END;

						SET @SupCdPar = @lv_supp_cd;
						SET @Cn = @Cn + 1;
					END

					DROP TABLE #tb_lv_supp_cd2

					IF @ln_records = 1 -- second step
					BEGIN
						SET @r_supp_cd = @lv_supp_cd;
					END
					ELSE
						IF @ln_records > 1
						BEGIN
							IF @ln_status_supp_cd = 0
							BEGIN
								--there is more than 1 supp_cd but they are same
								--SET @IsError = 'Y';
								SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' is found more than 1 record but assign to supplier (check source list)';

								EXEC dbo.CommonGetMessage 'MSPT00001ERR'
									,@MSG_TXT OUTPUT
									,@N_ERR OUTPUT
									,@PARAM1

								INSERT INTO TB_R_LOG_D (
									PROCESS_ID
									,SEQUENCE_NUMBER
									,MESSAGE_ID
									,MESSAGE_TYPE
									,[MESSAGE]
									,LOCATION
									,CREATED_BY
									,CREATED_DATE
									)
								SELECT @PROCESS_ID
									,(
										SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
										FROM dbo.TB_R_LOG_D
										WHERE PROCESS_ID = @PROCESS_ID
										)
									,'MSPT00001ERR'
									,'ERR'
									,'MSPT00001ERR : ' + @MSG_TXT
									,@LOG_LOCATION + ' : Mandatory Checking'
									,@USER_ID
									,GETDATE()

								SET @r_supp_cd = @lv_supp_cd;
							END
							ELSE
							BEGIN
								--there is more than 1 supp_cd but they are different
								SET @IsError = 'Y';
								SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' assign to more than 1 supplier (check source list)';

								EXEC dbo.CommonGetMessage 'MSPT00001ERR'
									,@MSG_TXT OUTPUT
									,@N_ERR OUTPUT
									,@PARAM1

								INSERT INTO TB_R_LOG_D (
									PROCESS_ID
									,SEQUENCE_NUMBER
									,MESSAGE_ID
									,MESSAGE_TYPE
									,[MESSAGE]
									,LOCATION
									,CREATED_BY
									,CREATED_DATE
									)
								SELECT @PROCESS_ID
									,(
										SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
										FROM dbo.TB_R_LOG_D
										WHERE PROCESS_ID = @PROCESS_ID
										)
									,'MSPT00001ERR'
									,'ERR'
									,'MSPT00001ERR : ' + @MSG_TXT
									,@LOG_LOCATION + ' : Mandatory Checking'
									,@USER_ID
									,GETDATE()

								SET @r_supp_cd = NULL;
							END
						END -- second step
						ELSE -- = 0 second step
						BEGIN
							PRINT 'third step'

							SET @ln_records = 0;
							SET @ln_status_supp_cd = 0;

							SELECT ROW_NUMBER() OVER (
									ORDER BY (
											SELECT 1
											)
									) NO
								,SUPP_CD
							INTO #tb_lv_supp_cd3
							FROM TB_M_SOURCE_LIST
							WHERE MAT_NO = @r_mat_no
								AND PART_COLOR_SFX = @r_color_sfx
								--AND    SOURCE_TYPE = @r_source_type
								AND PROD_PURPOSE_CD = @l_v_prod_purpose_cd
								AND CONVERT(VARCHAR, VALID_DT_FR, 112) <= CONVERT(VARCHAR, @r_ref_date, 112)
								AND ISNULL(CONVERT(VARCHAR, VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @r_ref_date, 112)

							SET @Cn = 1;
							SET @CnAll = 0;
							SET @SupCdPar = ''

							SELECT @CnAll = COUNT(1)
							FROM #tb_lv_supp_cd3

							WHILE @Cn <= @CnAll
							BEGIN
								SET @ln_records = @ln_records + 1;

								SELECT @lv_supp_cd = SUPP_CD
								FROM #tb_lv_supp_cd3
								WHERE NO = @Cn

								IF @ln_records > 1
								BEGIN
									IF @SupCdPar = @lv_supp_cd
									BEGIN
										IF @ln_status_supp_cd = 1
										BEGIN
											SET @ln_status_supp_cd = 1;
										END
										ELSE
										BEGIN
											SET @ln_status_supp_cd = 0;
										END;
									END
									ELSE
									BEGIN
										SET @ln_status_supp_cd = 1;
									END;
								END;

								SET @SupCdPar = @lv_supp_cd;
								SET @Cn = @Cn + 1;
							END;

							DROP TABLE #tb_lv_supp_cd3

							IF @ln_records = 1 -- third step
							BEGIN
								SET @r_supp_cd = @lv_supp_cd;
							END
							ELSE
								IF @ln_records > 1
								BEGIN
									IF @ln_status_supp_cd = 0
									BEGIN
										--there is more than 1 supp_cd but they are same
										--SET @IsError = 'Y';
										SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' is found more than 1 record but assign to supplier (check source list)';

										EXEC dbo.CommonGetMessage 'MSPT00001ERR'
											,@MSG_TXT OUTPUT
											,@N_ERR OUTPUT
											,@PARAM1

										INSERT INTO TB_R_LOG_D (
											PROCESS_ID
											,SEQUENCE_NUMBER
											,MESSAGE_ID
											,MESSAGE_TYPE
											,[MESSAGE]
											,LOCATION
											,CREATED_BY
											,CREATED_DATE
											)
										SELECT @PROCESS_ID
											,(
												SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
												FROM dbo.TB_R_LOG_D
												WHERE PROCESS_ID = @PROCESS_ID
												)
											,'MSPT00001ERR'
											,'ERR'
											,'MSPT00001ERR : ' + @MSG_TXT
											,@LOG_LOCATION + ' : Mandatory Checking'
											,@USER_ID
											,GETDATE()

										SET @r_supp_cd = @lv_supp_cd;
									END
									ELSE
									BEGIN
										--there is more than 1 supp_cd but they are different
										SET @IsError = 'Y';
										SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' assign to more than 1 supplier (check source list)';

										EXEC dbo.CommonGetMessage 'MSPT00001ERR'
											,@MSG_TXT OUTPUT
											,@N_ERR OUTPUT
											,@PARAM1

										INSERT INTO TB_R_LOG_D (
											PROCESS_ID
											,SEQUENCE_NUMBER
											,MESSAGE_ID
											,MESSAGE_TYPE
											,[MESSAGE]
											,LOCATION
											,CREATED_BY
											,CREATED_DATE
											)
										SELECT @PROCESS_ID
											,(
												SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
												FROM dbo.TB_R_LOG_D
												WHERE PROCESS_ID = @PROCESS_ID
												)
											,'MSPT00001ERR'
											,'ERR'
											,'MSPT00001ERR : ' + @MSG_TXT
											,@LOG_LOCATION + ' : Mandatory Checking'
											,@USER_ID
											,GETDATE()

										SET @r_supp_cd = NULL;
									END
								END -- third step
								ELSE -- = 0 third step
								BEGIN
									PRINT 'four step'

									SET @ln_records = 0;
									SET @ln_status_supp_cd = 0;

									SELECT ROW_NUMBER() OVER (
											ORDER BY (
													SELECT 1
													)
											) NO
										,SUPP_CD
									INTO #tb_lv_supp_cd4
									FROM TB_M_SOURCE_LIST
									WHERE MAT_NO = @r_mat_no
										--AND    PART_COLOR_SFX = @r_color_sfx
										--AND    SOURCE_TYPE = @r_source_type
										AND PROD_PURPOSE_CD = @l_v_prod_purpose_cd
										AND CONVERT(VARCHAR, VALID_DT_FR, 112) <= CONVERT(VARCHAR, @r_ref_date, 112)
										AND ISNULL(CONVERT(VARCHAR, VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @r_ref_date, 112)

									SET @Cn = 1;
									SET @CnAll = 0;
									SET @SupCdPar = ''

									SELECT @CnAll = COUNT(1)
									FROM #tb_lv_supp_cd4

									WHILE @Cn <= @CnAll
									BEGIN
										SET @ln_records = @ln_records + 1;

										SELECT @lv_supp_cd = SUPP_CD
										FROM #tb_lv_supp_cd4
										WHERE NO = @Cn

										IF @ln_records > 1
										BEGIN
											IF @SupCdPar = @lv_supp_cd
											BEGIN
												IF @ln_status_supp_cd = 1
												BEGIN
													SET @ln_status_supp_cd = 1;
												END
												ELSE
												BEGIN
													SET @ln_status_supp_cd = 0;
												END;
											END
											ELSE
											BEGIN
												SET @ln_status_supp_cd = 1;
											END;
										END;

										SET @SupCdPar = @lv_supp_cd;
										SET @Cn = @Cn + 1;
									END;

									DROP TABLE #tb_lv_supp_cd4

									IF @ln_records = 1 -- four step
									BEGIN
										SET @r_supp_cd = @lv_supp_cd;
									END
									ELSE
										IF @ln_records > 1
										BEGIN
											IF @ln_status_supp_cd = 0
											BEGIN
												--there is more than 1 supp_cd but they are same
												--SET @IsError = 'Y';
												SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' is found more than 1 record but assign to supplier (check source list)';

												EXEC dbo.CommonGetMessage 'MSPT00001ERR'
													,@MSG_TXT OUTPUT
													,@N_ERR OUTPUT
													,@PARAM1

												INSERT INTO TB_R_LOG_D (
													PROCESS_ID
													,SEQUENCE_NUMBER
													,MESSAGE_ID
													,MESSAGE_TYPE
													,[MESSAGE]
													,LOCATION
													,CREATED_BY
													,CREATED_DATE
													)
												SELECT @PROCESS_ID
													,(
														SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
														FROM dbo.TB_R_LOG_D
														WHERE PROCESS_ID = @PROCESS_ID
														)
													,'MSPT00001ERR'
													,'ERR'
													,'MSPT00001ERR : ' + @MSG_TXT
													,@LOG_LOCATION + ' : Mandatory Checking'
													,@USER_ID
													,GETDATE()

												SET @r_supp_cd = @lv_supp_cd;
											END
											ELSE
											BEGIN
												--there is more than 1 supp_cd but they are different
												SET @IsError = 'Y';
												SET @PARAM1 = 'material number ' + ISNULL(@r_mat_no, '') + ' and production purpose code ' + ISNULL(@l_v_prod_purpose_cd, '') + '  and source type ' + ISNULL(@r_source_type, '') + ' and reference date ' + CONVERT(VARCHAR, @r_ref_date) + ' assign to more than 1 supplier (check source list)';

												EXEC dbo.CommonGetMessage 'MSPT00001ERR'
													,@MSG_TXT OUTPUT
													,@N_ERR OUTPUT
													,@PARAM1

												INSERT INTO TB_R_LOG_D (
													PROCESS_ID
													,SEQUENCE_NUMBER
													,MESSAGE_ID
													,MESSAGE_TYPE
													,[MESSAGE]
													,LOCATION
													,CREATED_BY
													,CREATED_DATE
													)
												SELECT @PROCESS_ID
													,(
														SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
														FROM dbo.TB_R_LOG_D
														WHERE PROCESS_ID = @PROCESS_ID
														)
													,'MSPT00001ERR'
													,'ERR'
													,'MSPT00001ERR : ' + @MSG_TXT
													,@LOG_LOCATION + ' : Mandatory Checking'
													,@USER_ID
													,GETDATE()

												SET @r_supp_cd = NULL;
											END
										END -- four step
								END -- = 0 third step
						END -- = 0 second step
				END -- = 0 first step
		END --end of calculate

		SELECT @IsError l_n_status
			,@r_supp_cd l_v_supp_cd
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		SELECT 'Y' l_n_status
			,'' l_v_supp_cd
	END CATCH
END
