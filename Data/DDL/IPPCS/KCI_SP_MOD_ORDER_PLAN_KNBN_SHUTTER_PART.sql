CREATE PROCEDURE [dbo].[KCI_SP_MOD_ORDER_PLAN_KNBN_SHUTTER_PART] 
	-- Add the parameters for the stored procedure here
	@PO_RESULT_STATUS INT OUTPUT
	,@PO_RETURN_CODE varchar(8000) OUTPUT
AS
BEGIN TRY
	--set transaction isolation level repeatable read;
	--begin transaction process_tran
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--	SET DATEFORMAT DMY; -- DD/MM/YYYY
	-- ------------------------------------------------------
	-- Set tbe log to bulk before the main process
	--ALTER DATABASE NEWKBS SET RECOVERY BULK_LOGGED;
	-- table temporary logging
	DECLARE
		@V_M_RC int, -- return code process log
		@V_M_MSG_ID VARCHAR(12),
		@V_M_MSG VARCHAR(2000), -- error message process log
		@V_M_FUNCTION_ID VARCHAR(12),
		@V_M_MODULE_ID VARCHAR(12),
		@CURRENT_DATE DATETIME,
		@POSITION varchar(8000),
		@ITEM_NO INT
	/*
	DECLARE @TB_T_LOG TABLE
			(
				msg_seq int identity,
				msg_id varchar(12),
				message_type varchar(3),
				location varchar(100),
				message varchar(2000),
				process_sts varchar(1),
				date datetime,
				row_no decimal(5,0),
				flag_finish varchar(1)
			)
	*/
-- ------------------------------------------------------
	DECLARE 
		@intErrorCode INT,
		--@MAKE_CHANGES_FLAG INT,
		@FOUND_ERR INT,
		@NUM_OF_ERR INT

	DECLARE	
		@return_value int,
		@PROCESS_ID varchar(8000),
		@PI_USER_ID varchar(8000)

	DECLARE	
		@SUPP_CD char(4),
		@SUPP_PLANT char(1),
		@SHIPPING_DOCK char(3),
		@DOCK_CD char(2),
		@PRODUCTION_DT date,
		@ARRIVAL_SEQ smallint,
		@ROW_COUNT bigint,
		@COUNT_DATA bigint

	DECLARE 
		@PLANT_CD						varchar(1),
		@MANIFEST_LAST_SEQ_NO_TB_T		bigint,
		@MANIFEST_LAST_SEQ_NO_TB_R		bigint,
		@MANIFEST_LAST_SEQ_NO			bigint

	--SELECT	@PROCESS_ID = N'SEQ_PROCESS_ID'
	SELECT 'A'
    -- VARIABLE
	
	-- ==================
	SET @V_M_FUNCTION_ID = '1'
	SET @V_M_MODULE_ID = '1'
	SET @intErrorCode = 0
    SET @FOUND_ERR = 0
	SET @NUM_OF_ERR = 0
    SET @PO_RESULT_STATUS = 0
	SET @POSITION = NULL
	set	@PROCESS_ID = 'SEQ_PROCESS_ID'
	set @PI_USER_ID = 'KCI'
	
	-- ===================
	-- CHECK PROCESS ID
	-- ===================
	--PRINT '1. Check Process ID'
	
	EXEC @return_value = [KCI_DB].[dbo].[sp_GET_PROCESS_ID]
		@PROCESS_ID = @PROCESS_ID OUTPUT

	IF @PROCESS_ID IS NULL OR LEN(@PROCESS_ID) < 1
	BEGIN
		--PRINT '	Process Id cannot be empty'
		SET @PO_RESULT_STATUS = 1
		SET @PO_RETURN_CODE = 'Process Id cannot be empty'
		SET @FOUND_ERR = 1
		RETURN 1
	END
	-- =======================================================================================
		SELECT 'B'
	-- =========================
	-- LOGING START PROCESS --!
	-- =========================
	IF @FOUND_ERR = 0
	BEGIN
		-- WRITE LOG
		--PRINT '2. Logging Start Process'
		SET @POSITION = 'Starting Process'
		SET @V_M_MSG_ID = 'MSTD00336INF'
		SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
				   @V_M_MSG_ID
				  ,@V_M_FUNCTION_ID
				  ,'Scheduler Mod Order Plan Update Process'
				  ,null
				  ,null
				  ,null
				  ,null)
		
		SELECT @CURRENT_DATE = GETDATE()
		EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
					   @ri_v_user_id = @PI_USER_ID
					  ,@ri_n_process_id = @PROCESS_ID
					  ,@ri_v_fn_id = @V_M_FUNCTION_ID
					  ,@ri_v_module_id = @V_M_MODULE_ID
					  ,@ri_v_msg_id = @V_M_MSG_ID
					  ,@ri_v_message_type = 'INF'
					  ,@ri_v_location = @POSITION
					  ,@ri_v_message = @V_M_MSG
					  ,@ri_v_process_sts = '4' --,@ri_v_process_sts = '4' CREATE LOG H
					  ,@ri_d_date = @CURRENT_DATE 
					  ,@ri_n_row_no = 0 
					  ,@ri_v_flag_finish = 'N' 
					  ,@ro_v_err_msg = @V_M_MSG OUTPUT
		IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
		BEGIN
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
			SET @FOUND_ERR = 1
			--print @PO_RETURN_CODE
			RETURN @PO_RESULT_STATUS
		END
			SELECT 'C'
	END -- IF FOUND ERR
	-- =======================================================================================
	
			SELECT 'C1'
	-- =========================
	--  CHECK INPUT PARAMETER --!
	-- =========================
	IF @FOUND_ERR = 0
	BEGIN
		SET @POSITION = 'Check input parameter'
		
		--PRINT '3. Check Input Parameter'
		IF @PROCESS_ID IS NULL OR LEN(@PROCESS_ID) < 1
			BEGIN
			--
				-- WRITE LOG
				SET @PO_RESULT_STATUS = 1
				SET @FOUND_ERR = 1
				--SET @POSITION = 'Check input parameter'
				SET @V_M_MSG_ID = 'MSTD00338ERR'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'Process Id'
						  ,null
						  ,null
						  ,null
						  ,null)
				-- SET @V_M_MSG = 'Process Id should not be null'
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'ERR'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '1'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
			--
			END
			ELSE IF LEN(@PROCESS_ID) > 20
			BEGIN
				-- MSTD00119ERR
				-- WRITE LOG
				SET @PO_RESULT_STATUS = 1
				SET @FOUND_ERR = 1
				SET @POSITION = 'Check input parameter'
				SET @V_M_MSG_ID = 'MSTD00117ERR'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'Process Id'
						  ,'20'
						  ,null
						  ,null
						  ,null)
				-- SET @V_M_MSG = 'Process Id should not be null'
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'ERR'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '1'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
			END
			-- --------------------------------------------------------------
			ELSE IF @PI_USER_ID IS NULL OR LEN(@PI_USER_ID) < 1
			BEGIN
			--
				-- WRITE LOG
				SET @PO_RESULT_STATUS = 1
				SET @FOUND_ERR = 1
				SET @POSITION = 'Check input parameter'
				SET @V_M_MSG_ID = 'MSTD00338ERR'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'User Id'
						  ,null
						  ,null
						  ,null
						  ,null)
				-- SET @V_M_MSG = 'Process Id should not be null'
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'ERR'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '1'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
			--
			END
			ELSE IF LEN(@PI_USER_ID) > 20
			BEGIN
				-- WRITE LOG
				SET @PO_RESULT_STATUS = 1
				SET @FOUND_ERR = 1
				SET @POSITION = 'Check input parameter'
				SET @V_M_MSG_ID = 'MSTD00117ERR'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'User Id'
						  ,'20'
						  ,null
						  ,null
						  ,null)
				-- SET @V_M_MSG = 'Process Id should not be null'
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'ERR'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '1'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
			END

			-- P1FAILED:
			IF @FOUND_ERR <> 0 -- FAILED
			BEGIN
				--PRINT '	Check input parameter: failed'
				SET @FOUND_ERR = @FOUND_ERR
			END
				SELECT 'D'
	END -- IF FOUND ERR
	-- =======================================================================================
		
			SELECT 'D1'
	-- ########################################
	-- Get System Master of Last Release Date #
	-- ########################################
	SET @POSITION = 'Check system parameter'
	
	DECLARE @lg_LAST_ORDER_DATE DATE
	
	SELECT @lg_LAST_ORDER_DATE=SYSTEM_VALUE_DT
	FROM [KCI_DB].dbo.TB_M_SYSTEM
	WHERE SYSTEM_TYPE = 'RELEASE_PROCESS'
		AND SYSTEM_CD = 'LAST_ORDER_RELEASE_DATE'
	
	IF @lg_LAST_ORDER_DATE IS NULL
	BEGIN
		-- WRITE LOG
		SET @PO_RESULT_STATUS = 1
		SET @FOUND_ERR = 1
		--SET @POSITION = 'Check system parameter'
		SET @V_M_MSG_ID = 'MSTD00150ERR'
		SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
				   @V_M_MSG_ID
				  ,@V_M_FUNCTION_ID
				  ,'SYSTEM_VALUE_DT of System Type:RELEASE_PROCESS and System Code:LAST_ORDER_RELEASE_DATE'
				  ,null
				  ,null
				  ,null
				  ,null)
		-- SET @V_M_MSG = 'Process Id should not be null'
		SET @PO_RETURN_CODE = @V_M_MSG
		SELECT @CURRENT_DATE = GETDATE()
		EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
					   @ri_v_user_id = @PI_USER_ID
					  ,@ri_n_process_id = @PROCESS_ID
					  ,@ri_v_fn_id = @V_M_FUNCTION_ID
					  ,@ri_v_module_id = @V_M_MODULE_ID
					  ,@ri_v_msg_id = @V_M_MSG_ID
					  ,@ri_v_message_type = 'ERR'
					  ,@ri_v_location = @POSITION
					  ,@ri_v_message = @V_M_MSG
					  ,@ri_v_process_sts = '1'
					  ,@ri_d_date = @CURRENT_DATE 
					  ,@ri_n_row_no = 0 
					  ,@ri_v_flag_finish = 'N' 
					  ,@ro_v_err_msg = @V_M_MSG OUTPUT
		IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
		BEGIN
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
			SET @FOUND_ERR = 1
			--print @PO_RETURN_CODE
		END
	END
	SELECT 'E'
-- ====================================================================
	-- ################
	-- MAIN PROCESS [start]
	-- ################		
	-- 1. Truncate Temporary Table(s) [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '1. Truncate Temporary Table(s)'
			
			truncate table TB_T_ORDERPLAN_TMP_KCI_SUMM_0
			truncate table TB_T_ORDERPLAN_TMP_KCI_SUMM_0_SKIP
			truncate table TB_T_ORDER_MOD_PREP
			truncate table TB_T_DAILY_ORDER_MANIFEST_KCI
			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of Truncate Temporary Table(s) Done Successfully'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END			
	-- 1. Truncate Temporary Table(s) [end]
			
	-- 2. KCI E-Kanban Order Plan Summary [start]
	-- creating kci order plan summary by
	--				SUPPLIER_CODE,
	--				SUPPLIER_PLANT,
	--				SHIPPING_DOCK,
	--				DOCK_CODE,
	--				ORDER_NO	
	SELECT 'F'
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '2. KCI E-Kanban Order Plan Summary'
			
			INSERT INTO [TB_T_ORDERPLAN_TMP_KCI_SUMM_0]
					   ([SUPPLIER_CODE]
					   ,[SUPPLIER_PLANT]
					   ,[SHIPPING_DOCK]
					   ,[DOCK_CODE]
					   ,[ORDER_NO]
					   ,[PRODUCTION_DATE]
					   ,[ARRIVAL_SEQ]
					   ,[ARRIVAL_DATE]
					   ,[SHIPPING_DATE]
					   ,[COMPANY_CODE]
					   ,[RCV_PLANT_CODE]
					   ,[ORDER_TYPE]
					   ,[ORDER_RELEASE_DATE]
					   ,[MANIFEST_PRN_POINT_CODE]
					   ,[MANIFEST_PRN_DATE_PLANT]
					   ,[MANIFEST_PRN_DATE_LOCAL]
					   ,[MR_GRP_CODE]
					   ,[MR_ORDER_SEQ]
					   ,[MR_DATE]
					   ,[SR_GRP_CODE]
					   ,[SR_SEQ]
					   ,[SR_DATE]
					   ,[FRST_X_DOC_CODE]
					   ,[FST_X_PLANT_CODE]
					   ,[FST_X_SHIPPING_DOCK_CODE]
					   ,[FST_X_DOCK_ARRIVAL_DATE]
					   ,[FST_X_DOCK_DEPART_DATE]
					   ,[FST_X_DOCK_ROUTE_GRP_CODE]
					   ,[FST_X_DOCK_SEQ_NO]
					   ,[FST_ROUTE_DATE]
					   ,[SND_X_DOC_CODE]
					   ,[SND_X_PLANT_CODE]
					   ,[SND_X_SHIPPING_DOCK_CODE]
					   ,[SND_X_DOCK_ARRIVAL_DATE]
					   ,[SND_X_DOCK_DEPART_DATE]
					   ,[SND_X_DOCK_ROUTE_GRP_CODE]
					   ,[SND_X_DOCK_SEQ_NO]
					   ,[SND_ROUTE_DATE]
					   ,[TRD_X_DOC_CODE]
					   ,[TRD_X_PLANT_CODE]
					   ,[TRD_X_SHIPPING_DOCK_CODE]
					   ,[TRD_X_DOCK_ARRIVAL_DATE]
					   ,[TRD_X_DOCK_DEPART_DATE]
					   ,[TRD_X_DOCK_ROUTE_GRP_CODE]
					   ,[TRD_X_DOCK_SEQ_NO]
					   ,[TRD_ROUTE_DATE])
			select 
				t.[SUPPLIER_CODE]
				,t.[SUPPLIER_PLANT]
				,t.[SHIPPING_DOCK]
				,t.[DOCK_CODE]
				,t.[ORDER_NO]
				,convert(date, SUBSTRING(t.[ORDER_NO], 1, 8), 112) PRODUCTION_DATE
				,convert(smallint, SUBSTRING(t.[ORDER_NO], 9, 2)) ARRIVAL_SEQ
				,t.[ARRIVAL_DATE]
				,t.[SHIPPING_DATE]
				,t.[COMPANY_CODE]
				,t.[RCV_PLANT_CODE]
				,t.[ORDER_TYPE]
				,t.[ORDER_RELEASE_DATE]
				,t.[MANIFEST_PRN_POINT_CODE]
				,t.[MANIFEST_PRN_DATE_PLANT]
				,t.[MANIFEST_PRN_DATE_LOCAL]
				,t.[MR_GRP_CODE]
				,t.[MR_ORDER_SEQ]
				,t.[MR_DATE]
				,t.[SR_GRP_CODE]
				,t.[SR_SEQ]
				,t.[SR_DATE]
				,t.[FRST_X_DOC_CODE]
				,t.[FST_X_PLANT_CODE]
				,t.[FST_X_SHIPPING_DOCK_CODE]
				,t.[FST_X_DOCK_ARRIVAL_DATE]
				,t.[FST_X_DOCK_DEPART_DATE]
				,t.[FST_X_DOCK_ROUTE_GRP_CODE]
				,t.[FST_X_DOCK_SEQ_NO]
				,t.[FST_ROUTE_DATE]
				,t.[SND_X_DOC_CODE]
				,t.[SND_X_PLANT_CODE]
				,t.[SND_X_SHIPPING_DOCK_CODE]
				,t.[SND_X_DOCK_ARRIVAL_DATE]
				,t.[SND_X_DOCK_DEPART_DATE]
				,t.[SND_X_DOCK_ROUTE_GRP_CODE]
				,t.[SND_X_DOCK_SEQ_NO]
				,t.[SND_ROUTE_DATE]
				,t.[TRD_X_DOC_CODE]
				,t.[TRD_X_PLANT_CODE]
				,t.[TRD_X_SHIPPING_DOCK_CODE]
				,t.[TRD_X_DOCK_ARRIVAL_DATE]
				,t.[TRD_X_DOCK_DEPART_DATE]
				,t.[TRD_X_DOCK_ROUTE_GRP_CODE]
				,t.[TRD_X_DOCK_SEQ_NO]
				,t.[TRD_ROUTE_DATE]
			from (
				select *,
					ROW_NUMBER() over (
						partition by 
							SUPPLIER_CODE,
							SUPPLIER_PLANT,
							SHIPPING_DOCK,
							DOCK_CODE,
							ORDER_NO
						order by 
							SUPPLIER_CODE,
							SUPPLIER_PLANT,
							SHIPPING_DOCK,
							RCV_PLANT_CODE,
							ORDER_NO,
							PART_NO,
							KNBN_PRN_ADDRESS
					) as row_num 
				from TB_T_ORDERPLAN_TMP_KCI op_kci
				/*where op_kci.STATUS_FLAG = 0
					and op_kci.ORDER_RELEASE_DATE > CONVERT(VARCHAR(8), @lg_LAST_ORDER_DATE, 112) */
			) t
			where t.row_num = 1
		
			set @ROW_COUNT = @@ROWCOUNT
			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of KCI E-Kanban Order Plan Summary Done Successfully, Records [' + cast(@ROW_COUNT as varchar) + ']'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
			set @ROW_COUNT = 0
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END
	-- 2. KCI E-Kanban Order Plan Summary [end]
	
	-- 3. KCI Order Preparation [start]
	SELECT 'G'
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '3. KCI Order Preparation'
			
			INSERT INTO [dbo].[TB_T_ORDER_MOD_PREP]
					   ([PART_NO]
					   ,[ARRIVAL_SEQ]
					   ,[STACKING_LINE]
					   ,[PRODUCTION_DATE]
					   ,[ORDER_NO]
					   ,[LOT_MOD_NO]
					   ,[DOCK_CD]
					   ,[SUPP_CD]
					   ,[SUPP_PLANT]
					   ,[SHIPPING_DOCK]
					   ,[COMPANY_CD]
					   ,[RCV_PLANT_CODE]
					   ,[ARRIVAL_TIME]
					   ,[PART_READY_TIME]
					   ,[PART_PCK_QTY_BOX]
					   ,[ORDER_QTY]
					   ,[PIECES_PER_KANBAN]
					   ,[P_LANE_NO]
					   ,[SHUTTER_NO]
					   ,[STS_PROCESS]
					   ,[IMP_INFO_1]
					   ,[IMP_INFO_2]
					   ,[IMP_INFO_3]
					   ,[IMP_INFO_4]
					   ,[CREATED_BY]
					   ,[CREATED_DT]
					   ,[CHANGED_BY]
					   ,[CHANGED_DT]
					   ,[rowguid]
					   ,[PART_BAR_CODE]
					   ,[CASE_NO]
					   ,[MOD_DST_CD]
					   ,[BOX_NO]
					   ,[PLN_PCK_DT]
					   ,[CONT_SNO]
					   ,[PROCESS_TYPE]
					   ,[GROUP_INVOICE]
					   ,[SEPARATION_KEY_1]
					   ,[SEPARATION_KEY_2]
					   ,[SEPARATION_KEY_3])
			select 
				kci_ori.PART_NO,
				kci_ori.ARRIVAL_SEQ,
				kci_ori.STACKING_LINE,
				kci_ori.PRODUCTION_DATE,
				ekbn_sum.ORDER_NO,
				kci_ori.LOT_MOD_NO,
				kci_ori.DOCK_CD,
				kci_ori.SUPP_CD,
				kci_ori.SUPP_PLANT,
				kci_ori.SHIPPING_DOCK,
				ekbn_sum.COMPANY_CODE,
				ekbn_sum.RCV_PLANT_CODE,
				kci_ori.ARRIVAL_TIME,
				kci_ori.PART_READY_TIME,
				kci_ori.PART_PCK_QTY_BOX,
				kci_ori.ORDER_QTY,
				kci_ori.PIECES_PER_KANBAN,
				kci_ori.P_LANE_NO,
				kci_ori.SHUTTER_NO,
				kci_ori.STS_PROCESS,
				kci_ori.IMP_INFO_1,
				kci_ori.IMP_INFO_2,
				kci_ori.IMP_INFO_3,
				kci_ori.IMP_INFO_4,
				kci_ori.CREATED_BY,
				kci_ori.CREATED_DT,
				kci_ori.CHANGED_BY,
				kci_ori.CHANGED_DT,
				kci_ori.rowguid,
				kci_ori.PART_BAR_CODE,
				kci_ori.CASE_NO,
				kci_ori.MOD_DST_CD,
				kci_ori.BOX_NO,
				kci_ori.PLN_PCK_DT,
				kci_ori.CONT_SNO,
				kci_ori.PROCESS_TYPE,
				isnull(split.GROUP_INVOICE, '0') GROUP_INVOICE,
				case 
					when split.GROUP_INVOICE is null or split.GROUP_INVOICE = '0' then '0'
					when split.GROUP_INVOICE = 'B' then 
						isnull(kci_ori.LOT_MOD_NO, '') + '_' + 
						isnull(kci_ori.CASE_NO, '') + '_' + 
						isnull(kci_ori.MOD_DST_CD, '')
					else '1'
				end SEPARATION_KEY_1,
				'0' SEPARATION_KEY_2,
				'0' SEPARATION_KEY_3
			from [KCI_DB].[dbo].TB_R_ORDER_MOD kci_ori
				inner join TB_T_ORDERPLAN_TMP_KCI_SUMM_0 ekbn_sum 
					on kci_ori.SUPP_CD = ekbn_sum.SUPPLIER_CODE
					and kci_ori.SUPP_PLANT = ekbn_sum.SUPPLIER_PLANT
					and kci_ori.SHIPPING_DOCK = ekbn_sum.SHIPPING_DOCK
					and kci_ori.DOCK_CD = ekbn_sum.DOCK_CODE
					and kci_ori.PRODUCTION_DATE = ekbn_sum.PRODUCTION_DATE
					and kci_ori.ARRIVAL_SEQ = ekbn_sum.ARRIVAL_SEQ
				left join TB_M_SPLITTING_MANIFEST_GROUP split
					on split.PROCESS_TYPE = kci_ori.PROCESS_TYPE
			where 1 = 1
				-- comment this for testing purpose (if no data STS_PROCESS = N)
				--and kci_ori.STS_PROCESS = 'N'
				and (kci_ori.STS_PROCESS = 'N' or kci_ori.STS_PROCESS is null)
		
			set @ROW_COUNT = @@ROWCOUNT
			
			select @COUNT_DATA = COUNT(1)
			from [KCI_DB].[dbo].TB_R_ORDER_MOD kci_ori
				inner join TB_T_ORDERPLAN_TMP_KCI_SUMM_0 ekbn_sum 
					on kci_ori.SUPP_CD = ekbn_sum.SUPPLIER_CODE
					and kci_ori.SUPP_PLANT = ekbn_sum.SUPPLIER_PLANT
					and kci_ori.SHIPPING_DOCK = ekbn_sum.SHIPPING_DOCK
					and kci_ori.DOCK_CD = ekbn_sum.DOCK_CODE
					and kci_ori.PRODUCTION_DATE = ekbn_sum.PRODUCTION_DATE
					and kci_ori.ARRIVAL_SEQ = ekbn_sum.ARRIVAL_SEQ
				left join TB_M_SPLITTING_MANIFEST_GROUP split
					on split.PROCESS_TYPE = kci_ori.PROCESS_TYPE
			where 1 = 1
				and kci_ori.STS_PROCESS = 'Y'
			
			-- Logging warning total data not processed [start]
			if @COUNT_DATA > 0
			begin
				SET @V_M_MSG_ID = 'MSTD00001INF'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'Warning : [' + cast(@COUNT_DATA as varchar) + '] data KCI.TB_R_ORDER_MOD are not processed due to STS_PROCESS = ''Y'''
						  ,null
						  ,null
						  ,null
						  ,null)
						  
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'INF'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '0'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
			END
			-- Logging warning total data not processed [end]
			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of KCI Order Preparation Done Successfully, Records [' + cast(@ROW_COUNT as varchar) + ']'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
			set @ROW_COUNT = 0
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END
	-- 3. KCI Order Preparation [end]
		
	--added 2 march 2015--
	--by fid-lb223--
	-- 3.5. KCI ORDER PREPARATION II [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
		SET @POSITION = '3.5. KCI Order Preparation II'

		INSERT INTO [dbo].[TB_T_ORDER_MOD_PREP]
				([PART_NO],
				[ARRIVAL_SEQ],
				[STACKING_LINE],
				[PRODUCTION_DATE],
				[ORDER_NO],
				[LOT_MOD_NO],
				[DOCK_CD],
				[SUPP_CD],
				[SUPP_PLANT],
				[SHIPPING_DOCK],
				[COMPANY_CD],
				[RCV_PLANT_CODE],
				[ARRIVAL_TIME],
				[PART_READY_TIME],
				[PART_PCK_QTY_BOX],
				[ORDER_QTY],
				[PIECES_PER_KANBAN],
				[CREATED_BY],
				[CREATED_DT],
				[CHANGED_BY],
				[CHANGED_DT],
				[P_LANE_NO],
				[SHUTTER_NO],
				[STS_PROCESS],
				[IMP_INFO_1],
				[IMP_INFO_2],
				[IMP_INFO_3],
				[IMP_INFO_4],
				[PART_BAR_CODE],
				[CASE_NO],
				[MOD_DST_CD],
				[BOX_NO],
				[PLN_PCK_DT],
				[CONT_SNO],
				[PROCESS_TYPE],
				[GROUP_INVOICE],
				[SEPARATION_KEY_1],
				[SEPARATION_KEY_2],
				[SEPARATION_KEY_3],
				[EKBN_PRN_ADDRESS],
				[rowguid]
				)
			SELECT
				kci.PART_NO,
				convert(smallint, SUBSTRING(kci.ORDER_NO, 9, 2)),
				'EK1',
				convert(date, SUBSTRING(kci.ORDER_NO, 1, 8), 112),
				kci.ORDER_NO,
				'XXXXXX',
				kci.DOCK_CODE,
				kci.SUPPLIER_CODE,
				kci.SUPPLIER_PLANT,
				kci.SHIPPING_DOCK,
				kci.COMPANY_CODE,
				kci.RCV_PLANT_CODE,
				convert(datetime, 
						SUBSTRING(kci.ARRIVAL_DATE, 1, 4) + '-' +
						SUBSTRING(kci.ARRIVAL_DATE, 5, 2) + '-' +
						SUBSTRING(kci.ARRIVAL_DATE, 7, 2) + ' ' +
						SUBSTRING(kci.ARRIVAL_DATE, 9, 2) + ':' +
						SUBSTRING(kci.ARRIVAL_DATE, 11, 2) + ':00' , 120),
				convert(datetime, 
						SUBSTRING(kci.ARRIVAL_DATE, 1, 4) + '-' +
						SUBSTRING(kci.ARRIVAL_DATE, 5, 2) + '-' +
						SUBSTRING(kci.ARRIVAL_DATE, 7, 2) + ' ' +
						SUBSTRING(kci.ARRIVAL_DATE, 9, 2) + ':' +
						SUBSTRING(kci.ARRIVAL_DATE, 11, 2) + ':00' , 120), --part ready time
				0,
				0,
				kci.QTY_PER_CONTAINER,
				'EKBN',
				getdate(),
				'EKBN',
				getdate(), 
				null, null, null, null, null, null, null, null, null, 
				null, null, null, null, null, 0, 0, 0, 0,
				kci.KNBN_PRN_ADDRESS,
				
				(SELECT count(1)
				   FROM TB_T_ORDER_MOD_PREP) + row_number() over (order by 1/0)

			FROM TB_T_ORDERPLAN_TMP_KCI kci
			LEFT JOIN [KCI_DB].[dbo].TB_R_ORDER_MOD odr
			on kci.SUPPLIER_CODE = odr.SUPP_CD AND
			   kci.SUPPLIER_PLANT = odr.SUPP_PLANT AND
			   kci.SHIPPING_DOCK = odr.SHIPPING_DOCK AND
			   kci.DOCK_CODE = odr.DOCK_CD AND
			   kci.PART_NO = odr.PART_NO AND
			   convert(date, SUBSTRING(kci.ORDER_NO, 1, 8), 112) = odr.PRODUCTION_DATE AND
			   convert(smallint, SUBSTRING(kci.ORDER_NO, 9, 2)) = odr.ARRIVAL_SEQ
			WHERE odr.SUPP_CD is null AND
				  odr.SUPP_PLANT is null AND
				  odr.SHIPPING_DOCK is null AND
				  odr.DOCK_CD is null AND
				  odr.PART_NO is null	


			----
			set @ROW_COUNT = @@ROWCOUNT
			
			select @COUNT_DATA = COUNT(1)
			FROM TB_T_ORDERPLAN_TMP_KCI kci
			LEFT JOIN [KCI_DB].[dbo].TB_R_ORDER_MOD odr
			on kci.SUPPLIER_CODE = odr.SUPP_CD AND
			   kci.SUPPLIER_PLANT = odr.SUPP_PLANT AND
			   kci.SHIPPING_DOCK = odr.SHIPPING_DOCK AND
			   kci.DOCK_CODE = odr.DOCK_CD AND
			   kci.PART_NO = odr.PART_NO AND
			   convert(date, SUBSTRING(kci.ORDER_NO, 1, 8), 112) = odr.PRODUCTION_DATE AND
			   convert(smallint, SUBSTRING(kci.ORDER_NO, 9, 2)) = odr.ARRIVAL_SEQ
			WHERE odr.SUPP_CD is null AND
				  odr.SUPP_PLANT is null AND
				  odr.SHIPPING_DOCK is null AND
				  odr.DOCK_CD is null AND
				  odr.PART_NO is null
			---

			-- Logging warning total data not processed [start]
			if @COUNT_DATA > 0
			begin
				SET @V_M_MSG_ID = 'MSTD00001INF'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'Warning : [' + cast(@COUNT_DATA as varchar) + '] data EKBN is processed without KCI (QTY become 0 (zero))'
						  ,null
						  ,null
						  ,null
						  ,null)
						  
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'INF'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '0'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
			END
			-- Logging warning total data not processed [end]
			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of KCI Order Preparation Done Successfully, Records [' + cast(@ROW_COUNT as varchar) + ']'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
			set @ROW_COUNT = 0
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END
	-- 3.5. KCI ORDER PREPARATION II [end]


	-- 4. KCI Manifest Header Creation [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '4. KCI Manifest Header Creation'
						
			-- get plant code
			select TOP 1 @PLANT_CD = RCV_PLANT_CODE
			from TB_T_ORDERPLAN_TMP_KCI_SUMM_0
			
			-- get last manifest no from table R
			--UPDATE BY AGI
			--CAST(ISNULL(MAX(RIGHT(MANIFEST_NO, 7)), '0') AS BIGINT) TO CAST(ISNULL(MAX(RIGHT(MANIFEST_NO, 6)), '0') AS BIGINT)
			
			SELECT @MANIFEST_LAST_SEQ_NO_TB_R = CAST(ISNULL(MAX(RIGHT(MANIFEST_NO, 6)), '0') AS BIGINT)
			FROM TB_R_DAILY_ORDER_MANIFEST 
			WHERE MANIFEST_NO like @PLANT_CD + RIGHT(Convert(VARCHAR, YEAR(GETDATE())), 2) + '%';

			-- get last manifest no from table T
			SELECT @MANIFEST_LAST_SEQ_NO_TB_T = CAST(ISNULL(MAX(RIGHT(MANIFEST_NO, 6)), '0') AS BIGINT)
			FROM TB_T_DAILY_ORDER_MANIFEST 
			WHERE MANIFEST_NO like @PLANT_CD + RIGHT(Convert(VARCHAR, YEAR(GETDATE())), 2) + '%';

			

			IF @MANIFEST_LAST_SEQ_NO_TB_R > @MANIFEST_LAST_SEQ_NO_TB_T
			begin
				set @MANIFEST_LAST_SEQ_NO = @MANIFEST_LAST_SEQ_NO_TB_R
			end
			else
			begin
				set @MANIFEST_LAST_SEQ_NO = @MANIFEST_LAST_SEQ_NO_TB_T
			end
			
			-- format [PLANT_CD][CURR_YEAR_2_DIGIT][RUN_NO_7_DIGIT]
			-- example : 1150028846
			--@PLANT_CD + RIGHT(Convert(VARCHAR,YEAR(GETDATE())),2) + RIGHT(REPLICATE('0', 7) + CAST(@MANIFEST_LAST_SEQ_NO AS VARCHAR(7)), 7);
			
			INSERT INTO [dbo].[TB_T_DAILY_ORDER_MANIFEST_KCI]
					   ([MANIFEST_NO]
					   ,[ORDER_NO]
					   ,[SUPPLIER_CD]
					   ,[SUPPLIER_PLANT]
					   ,[ORDER_TYPE]
					   ,[ORDER_RELEASE_DT]
					   ,[COMPANY_CD]
					   ,[RCV_PLANT_CD]
					   ,[DOCK_CD]
					   ,[CAR_FAMILY_CD]
					   ,[RE_EXPORT_CD]
					   ,[TOTAL_ITEM]
					   ,[TOTAL_QTY]
					   ,[ERROR_QTY]
					   ,[ARRIVAL_PLAN_DT]
					   ,[ARRIVAL_ACTUAL_DT]
					   ,[DELAY_FLAG]
					   ,[MANIFEST_RECEIVE_FLAG]
					   ,[PROBLEM_FLAG]
					   ,[IS_ZIPED]
					   ,[PO_NO]
					   ,[GR_DOCUMENT_NO]
					   ,[GR_DOCUMENT_DT]
					   ,[GR_SLIP_DOCUMENT_NO]
					   ,[GR_SLIP_DT]
					   ,[SCAN_DT]
					   ,[APPROVED_DT]
					   ,[MAT_DOC_NO]
					   ,[MAT_DOC_YEAR]
					   ,[ICS_FLAG]
					   ,[ICS_DT]
					   ,[NOTICE_ID]
					   ,[SHIPPING_DOCK]
					   ,[SHIPPING_DT]
					   ,[CANCEL_FLAG]
					   ,[PRINT_FLAG]
					   ,[PRINT_DATE]
					   ,[PRINTED_BY]
					   ,[RECEIVE_FLAG]
					   ,[RECEIVE_DT]
					   ,[USAGE_FLAG]
					   ,[USAGE_DT]
					   ,[DELETION_FLAG]
					   ,[DELETION_DT]
					   ,[DROP_STATUS_FLAG]
					   ,[DROP_STATUS_DT]
					   ,[TOTAL_PAGE]
					   ,[FILENAME]
					   ,[CONVEYANCE_ROUTE]
					   ,[MANIFEST_PRINT_POINT_CD]
					   ,[MANIFEST_PRINT_DATE_PLANT]
					   ,[MANIFEST_PRINT_DATE_LOCAL]
					   ,[P_LANE_CD]
					   ,[P_LANE_SEQ]
					   ,[P_LANE_DT]
					   ,[SUB_ROUTE_CD]
					   ,[SUB_ROUTE_SEQ]
					   ,[SUB_ROUTE_DT]
					   ,[FST_X_DOC_CD]
					   ,[FST_X_PLANT_CD]
					   ,[FST_X_SHIPPING_DOCK_CD]
					   ,[FST_X_DOCK_ARRIVAL_DT]
					   ,[FST_X_DOCK_DEPART_DT]
					   ,[FST_X_DOCK_ROUTE_GRP_CD]
					   ,[FST_X_DOCK_SEQ_NO]
					   ,[FST_X_ROUTE_DT]
					   ,[SND_X_DOC_CD]
					   ,[SND_X_PLANT_CD]
					   ,[SND_X_SHIPPING_DOCK_CD]
					   ,[SND_X_DOCK_ARRIVAL_DT]
					   ,[SND_X_DOCK_DEPART_DT]
					   ,[SND_X_DOCK_ROUTE_GRP_CD]
					   ,[SND_X_DOCK_SEQ_NO]
					   ,[SND_X_ROUTE_DT]
					   ,[TRD_X_DOC_CD]
					   ,[TRD_X_PLANT_CD]
					   ,[TRD_X_SHIPPING_DOCK_CD]
					   ,[TRD_X_DOCK_ARRIVAL_DT]
					   ,[TRD_X_DOCK_DEPART_DT]
					   ,[TRD_X_DOCK_ROUTE_GRP_CD]
					   ,[TRD_X_DOCK_SEQ_NO]
					   ,[TRD_X_ROUTE_DT]
					   ,[PAT_DP_FLAG]
					   ,[PAT_PL_FLAG]
					   ,[CREATED_BY]
					   ,[CREATED_DT]
					   ,[CHANGED_BY]
					   ,[CHANGED_DT]
					   ,[INVOICE_NO]
					   ,[INVOICE_DT]
					   ,[INVOICE_BY]
					   ,[POSTING_DT]
					   ,[POSTING_BY]
					   ,[CANCEL_BY]
					   ,[CANCEL_DT]
					   ,[APPROVED_BY]
					   ,[FTP_FLAG]
					   ,[IN_PROGRESS]
					   ,[DOWNLOAD_FLAG]
					   ,[DOWNLOADED_BY]
					   ,[DOWNLOADED_DT]
					   ,[PROCESS_FLAG]
					   ,[GROUP_INVOICE]
					   ,[SEPARATION_KEY_1]
					   ,[SEPARATION_KEY_2]
					   ,[SEPARATION_KEY_3])
			select 			
				-- format [PLANT_CD][CURR_YEAR_2_DIGIT][RUN_NO_7_DIGIT]
				--@PLANT_CD + RIGHT(Convert(VARCHAR,YEAR(GETDATE())),2) + RIGHT(REPLICATE('0', 7) + CAST(@MANIFEST_LAST_SEQ_NO AS VARCHAR(7)), 7);
				
				
				kci_prep.RCV_PLANT_CODE +
				RIGHT(Convert(VARCHAR, YEAR(GETDATE())), 2) + 
				---commented by fid.deny March, 5 2015--
				---change <RIGHT(REPLICATE('0', 7) + CAST(> to <'P' + RIGHT(REPLICATE('0', 6) + CAST(>
				--RIGHT(REPLICATE('0', 7) + CAST( 
				'1' +
				RIGHT(REPLICATE('0', 6) + CAST( 
					ROW_NUMBER() over (
						partition by 
							kci_prep.RCV_PLANT_CODE
						order by 
							kci_prep.RCV_PLANT_CODE
					) + @MANIFEST_LAST_SEQ_NO
				--AS VARCHAR(7)), 7)as MANIFEST_NO,--[MANIFEST_NO] [varchar](16) NOT NULL, --commented March, 5 2015
				AS VARCHAR(6)), 6)as MANIFEST_NO,
				kci_prep.ORDER_NO,--[ORDER_NO] [varchar](12) NOT NULL,
				kci_prep.SUPP_CD,--[SUPPLIER_CD] [varchar](6) NOT NULL,
				kci_prep.SUPP_PLANT,--[SUPPLIER_PLANT] [char](1) NOT NULL,
				ekbn_sum.ORDER_TYPE,--[ORDER_TYPE] [char](1) NOT NULL,
				case
					when ekbn_sum.ORDER_RELEASE_DATE = '' then null
					else convert(datetime, ekbn_sum.ORDER_RELEASE_DATE, 112)
				end,--[ORDER_RELEASE_DT] [datetime] NULL,
				kci_prep.COMPANY_CD,--[COMPANY_CD] [varchar](4) NULL,
				kci_prep.RCV_PLANT_CODE,--[RCV_PLANT_CD] [char](1) NOT NULL,
				kci_prep.DOCK_CD,--[DOCK_CD] [varchar](2) NOT NULL,
				NULL CAR_FAMILY_CODE,--[CAR_FAMILY_CD] [varchar](4) NULL,
				NULL RE_EXPORT_CD,--[RE_EXPORT_CD] [char](1) NULL,
				kci_prep.COUNT_DATA,--[TOTAL_ITEM] [int] NOT NULL,
				kci_prep.SUM_ORDER_QTY,--[TOTAL_QTY] [int] NOT NULL,
				NULL,--[ERROR_QTY] [int] NULL,
				case
					when ekbn_sum.ARRIVAL_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.ARRIVAL_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.ARRIVAL_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.ARRIVAL_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.ARRIVAL_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.ARRIVAL_DATE, 11, 2) + ':00' , 120)
				end,--[ARRIVAL_PLAN_DT] [datetime] NULL,
				NULL,--[ARRIVAL_ACTUAL_DT] [datetime] NULL,
				NULL,--[DELAY_FLAG] [bit] NULL,
				'0',--[MANIFEST_RECEIVE_FLAG] [varchar](10) NULL,
				0,--[PROBLEM_FLAG] [bit] NULL,
				NULL,--[IS_ZIPED] [bit] NULL,
				'',--[PO_NO] [varchar](20) NULL,
				NULL,--[GR_DOCUMENT_NO] [varchar](20) NULL,
				NULL,--[GR_DOCUMENT_DT] [datetime] NULL,
				NULL,--[GR_SLIP_DOCUMENT_NO] [varchar](20) NULL,
				NULL,--[GR_SLIP_DT] [datetime] NULL,
				NULL,--[SCAN_DT] [datetime] NULL,
				NULL,--[APPROVED_DT] [datetime] NULL,
				NULL,--[MAT_DOC_NO] [varchar](10) NULL,
				NULL,--[MAT_DOC_YEAR] [varchar](4) NULL,
				0,--[ICS_FLAG] [bit] NULL,
				NULL,--[ICS_DT] [datetime] NULL,
				NULL,--[NOTICE_ID] [varchar](12) NULL,
				kci_prep.SHIPPING_DOCK,--[SHIPPING_DOCK] [char](3) NULL,
				case
					when ekbn_sum.SHIPPING_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.SHIPPING_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.SHIPPING_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.SHIPPING_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.SHIPPING_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.SHIPPING_DATE, 11, 2) + ':00' , 120)
				end,--[SHIPPING_DT] [datetime] NULL,
				0,--[CANCEL_FLAG] [bit] NULL,
				NULL,--[PRINT_FLAG] [bit] NULL,
				NULL,--[PRINT_DATE] [datetime] NULL,
				NULL,--[PRINTED_BY] [varchar](20) NULL,
				NULL,--[RECEIVE_FLAG] [char](1) NULL,
				NULL,--[RECEIVE_DT] [datetime] NULL,
				NULL,--[USAGE_FLAG] [char](1) NULL,
				NULL,--[USAGE_DT] [datetime] NULL,
				NULL,--[DELETION_FLAG] [char](1) NULL,
				NULL,--[DELETION_DT] [datetime] NULL,
				NULL,--[DROP_STATUS_FLAG] [char](10) NULL,
				NULL,--[DROP_STATUS_DT] [datetime] NULL,
				NULL,--[TOTAL_PAGE] [tinyint] NULL,
				NULL,--[FILENAME] [varchar](50) NULL,
				NULL,--[CONVEYANCE_ROUTE] [varchar](5) NULL,
				ekbn_sum.MANIFEST_PRN_POINT_CODE,--[MANIFEST_PRINT_POINT_CD] [varchar](35) NULL,
				case
					when ekbn_sum.MANIFEST_PRN_DATE_PLANT = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_PLANT, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_PLANT, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_PLANT, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_PLANT, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_PLANT, 11, 2) + ':00' , 120)
				end,--[MANIFEST_PRINT_DATE_PLANT] [datetime] NULL,	
				case
					when ekbn_sum.MANIFEST_PRN_DATE_LOCAL = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_LOCAL, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_LOCAL, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_LOCAL, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_LOCAL, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.MANIFEST_PRN_DATE_LOCAL, 11, 2) + ':00' , 120)
				end,--[MANIFEST_PRINT_DATE_LOCAL] [datetime] NULL,
				ekbn_sum.MR_GRP_CODE,--[P_LANE_CD] [varchar](10) NULL,
				ekbn_sum.MR_ORDER_SEQ,--[P_LANE_SEQ] [varchar](2) NULL,
				case
					when ekbn_sum.MR_DATE = '' then null
					else convert(datetime, substring(kci_prep.ORDER_NO, 1, 4) + ekbn_sum.MR_DATE, 112)
				end,--[P_LANE_DT] [datetime] NULL,
				ekbn_sum.SR_GRP_CODE,--[SUB_ROUTE_CD] [varchar](10) NULL,
				ekbn_sum.SR_SEQ,--[SUB_ROUTE_SEQ] [varchar](2) NULL,
				case
					when ekbn_sum.SR_DATE = '' then null
					else convert(datetime, substring(kci_prep.ORDER_NO, 1, 4) + ekbn_sum.SR_DATE, 112)
				end,--[SUB_ROUTE_DT] [datetime] NULL,
				ekbn_sum.FRST_X_DOC_CODE,--[FST_X_DOC_CD] [varchar](15) NULL,
				ekbn_sum.FST_X_PLANT_CODE,--[FST_X_PLANT_CD] [varchar](10) NULL,
				ekbn_sum.FST_X_SHIPPING_DOCK_CODE,--[FST_X_SHIPPING_DOCK_CD] [varchar](10) NULL,
				case
					when ekbn_sum.FST_X_DOCK_ARRIVAL_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.FST_X_DOCK_ARRIVAL_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_ARRIVAL_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_ARRIVAL_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_ARRIVAL_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_ARRIVAL_DATE, 11, 2) + ':00' , 120)
				end,--[FST_X_DOCK_ARRIVAL_DT] [datetime] NULL,
				case
					when ekbn_sum.FST_X_DOCK_DEPART_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.FST_X_DOCK_DEPART_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_DEPART_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_DEPART_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_DEPART_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.FST_X_DOCK_DEPART_DATE, 11, 2) + ':00' , 120)
				end,--[FST_X_DOCK_DEPART_DT] [datetime] NULL,
				ekbn_sum.FST_X_DOCK_ROUTE_GRP_CODE,--[FST_X_DOCK_ROUTE_GRP_CD] [varchar](10) NULL,
				ekbn_sum.FST_X_DOCK_SEQ_NO,--[FST_X_DOCK_SEQ_NO] [varchar](2) NULL,
				ekbn_sum.FST_ROUTE_DATE,--[FST_X_ROUTE_DT] [char](4) NULL,
				ekbn_sum.SND_X_DOC_CODE,--[SND_X_DOC_CD] [varchar](15) NULL,
				ekbn_sum.SND_X_PLANT_CODE,--[SND_X_PLANT_CD] [varchar](10) NULL,
				ekbn_sum.SND_X_SHIPPING_DOCK_CODE,--[SND_X_SHIPPING_DOCK_CD] [varchar](10) NULL,
				case
					when ekbn_sum.SND_X_DOCK_ARRIVAL_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.SND_X_DOCK_ARRIVAL_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_ARRIVAL_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_ARRIVAL_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_ARRIVAL_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_ARRIVAL_DATE, 11, 2) + ':00' , 120)
				end,--[SND_X_DOCK_ARRIVAL_DT] [datetime] NULL,
				case
					when ekbn_sum.SND_X_DOCK_DEPART_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.SND_X_DOCK_DEPART_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_DEPART_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_DEPART_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_DEPART_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.SND_X_DOCK_DEPART_DATE, 11, 2) + ':00' , 120)
				end,--[SND_X_DOCK_DEPART_DT] [datetime] NULL,
				ekbn_sum.SND_X_DOCK_ROUTE_GRP_CODE,--[SND_X_DOCK_ROUTE_GRP_CD] [varchar](10) NULL,
				ekbn_sum.SND_X_DOCK_SEQ_NO,--[SND_X_DOCK_SEQ_NO] [varchar](2) NULL,
				ekbn_sum.SND_ROUTE_DATE,--[SND_X_ROUTE_DT] [char](4) NULL,
				ekbn_sum.TRD_X_DOC_CODE,--[TRD_X_DOC_CD] [varchar](15) NULL,
				ekbn_sum.TRD_X_PLANT_CODE,--[TRD_X_PLANT_CD] [varchar](10) NULL,
				ekbn_sum.TRD_X_SHIPPING_DOCK_CODE,--[TRD_X_SHIPPING_DOCK_CD] [varchar](10) NULL,
				case
					when ekbn_sum.TRD_X_DOCK_ARRIVAL_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.TRD_X_DOCK_ARRIVAL_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_ARRIVAL_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_ARRIVAL_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_ARRIVAL_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_ARRIVAL_DATE, 11, 2) + ':00' , 120)
				end,--[TRD_X_DOCK_ARRIVAL_DT] [datetime] NULL,
				case
					when ekbn_sum.TRD_X_DOCK_DEPART_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_sum.TRD_X_DOCK_DEPART_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_DEPART_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_DEPART_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_DEPART_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_sum.TRD_X_DOCK_DEPART_DATE, 11, 2) + ':00' , 120)
				end,--[TRD_X_DOCK_DEPART_DT] [datetime] NULL,
				ekbn_sum.TRD_X_DOCK_ROUTE_GRP_CODE,--[TRD_X_DOCK_ROUTE_GRP_CD] [varchar](10) NULL,
				ekbn_sum.TRD_X_DOCK_SEQ_NO,--[TRD_X_DOCK_SEQ_NO] [varchar](2) NULL,
				ekbn_sum.TRD_ROUTE_DATE,--[TRD_X_ROUTE_DT] [char](4) NULL,
				'',--[PAT_DP_FLAG] [char](1) NULL,
				'',--[PAT_PL_FLAG] [char](1) NULL,
				'KCI1',--[CREATED_BY] [varchar](20) NOT NULL,
				getdate(),--[CREATED_DT] [datetime] NOT NULL,
				NULL,--[CHANGED_BY] [varchar](20) NULL,
				NULL,--[CHANGED_DT] [datetime] NULL,
				NULL,--[INVOICE_NO] [varchar](20) NULL,
				NULL,--[INVOICE_DT] [datetime] NULL,
				NULL,--[INVOICE_BY] [varchar](20) NULL,
				NULL,--[POSTING_DT] [datetime] NULL,
				NULL,--[POSTING_BY] [varchar](20) NULL,
				NULL,--[CANCEL_BY] [varchar](20) NULL,
				NULL,--[CANCEL_DT] [datetime] NULL,
				NULL,--[APPROVED_BY] [varchar](20) NULL,
				'0',--[FTP_FLAG] [char](1) NULL,
				NULL,--[IN_PROGRESS] [char](1) NULL,
				NULL,--[DOWNLOAD_FLAG] [varchar](1) NULL,
				NULL,--[DOWNLOADED_BY] [varchar](20) NULL,
				NULL,--[DOWNLOADED_DT] [datetime] NULL,
				'1',--[PROCESS_FLAG] [char](1) NULL,
				kci_prep.GROUP_INVOICE,--[GROUP_INVOICE] [varchar](1) NULL, --add
				kci_prep.SEPARATION_KEY_1,--[SEPARATION_KEY_1] [varchar](30) NULL, --add
				kci_prep.SEPARATION_KEY_2,--[SEPARATION_KEY_2] [varchar](30) NULL, --add
				kci_prep.SEPARATION_KEY_3--[SEPARATION_KEY_3] [varchar](30) NULL --add		
			from (
				select t.SUPP_CD,
					t.SUPP_PLANT,
					t.SHIPPING_DOCK,
					t.COMPANY_CD,
					t.RCV_PLANT_CODE,
					t.DOCK_CD,
					t.ORDER_NO,
					t.GROUP_INVOICE,
					t.SEPARATION_KEY_1,
					t.SEPARATION_KEY_2,
					t.SEPARATION_KEY_3,
					--edited 6 march 2015, fid.deny, only count if order_qty > 0
					--COUNT(1) COUNT_DATA,
					COUNT(CASE WHEN t.ORDER_QTY <> 0 THEN 1 ELSE NULL END) AS COUNT_DATA,
					SUM(t.ORDER_QTY) SUM_ORDER_QTY
				from TB_T_ORDER_MOD_PREP t
				group by t.SUPP_CD,
					t.SUPP_PLANT,
					t.SHIPPING_DOCK,
					t.COMPANY_CD,
					t.RCV_PLANT_CODE,
					t.DOCK_CD,
					t.ORDER_NO,
					t.GROUP_INVOICE,
					t.SEPARATION_KEY_1,
					t.SEPARATION_KEY_2,
					t.SEPARATION_KEY_3
			) kci_prep
				inner join TB_T_ORDERPLAN_TMP_KCI_SUMM_0 ekbn_sum 
					on kci_prep.SUPP_CD = ekbn_sum.SUPPLIER_CODE
					and kci_prep.SUPP_PLANT = ekbn_sum.SUPPLIER_PLANT
					and kci_prep.SHIPPING_DOCK = ekbn_sum.SHIPPING_DOCK
					and kci_prep.DOCK_CD = ekbn_sum.DOCK_CODE
					and kci_prep.ORDER_NO = ekbn_sum.ORDER_NO
			
			set @ROW_COUNT = @@ROWCOUNT
						
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of KCI Manifest Header Creation Done successfully, Records [' + cast(@ROW_COUNT as varchar) + ']'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
			set @ROW_COUNT = 0
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END
	-- 4. KCI Manifest Header Creation [end]
	
	-- 5. KCI Update Manifest Header Pattern Flag [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '5. KCI Update Manifest Header Pattern Flag'
			
			--------- PATTERN 3 ---------------
			UPDATE TB_T_DAILY_ORDER_MANIFEST_KCI--TB_R_DAILY_ORDER_MANIFEST
			SET 
				PAT_DP_FLAG = '1', 
				PAT_PL_FLAG = '1'
			WHERE  ISNULL(SND_X_DOC_CD,'') <> ''
				   AND   ISNULL(PAT_DP_FLAG,'') = ''
				   AND   ISNULL(PAT_PL_FLAG,'') = ''
				   AND   MANIFEST_RECEIVE_FLAG = 0
				   AND	 ORDER_TYPE='1'
				   --AND CREATED_DT=@TODAY AND RCV_PLANT_CD=@RcvPlantCode AND
				   --	SUPPLIER_CD=@SUPPLIER_CD_WEB AND
				   --	SUPPLIER_PLANT=@SUPPLIER_PLANT_WEB
				
			--------- PATTERN 1 ---------------
			UPDATE TB_T_DAILY_ORDER_MANIFEST_KCI--TB_R_DAILY_ORDER_MANIFEST 
			SET 
				PAT_DP_FLAG = '0', 
				PAT_PL_FLAG = '0'
			WHERE ISNULL(FST_X_DOC_CD,'') = ''
				   AND   ISNULL(PAT_DP_FLAG,'') = ''
				   AND   ISNULL(PAT_PL_FLAG,'') = ''
				   AND   MANIFEST_RECEIVE_FLAG = 0
				   AND   ORDER_TYPE='1'
				   --AND CREATED_DT=@TODAY AND RCV_PLANT_CD=@RcvPlantCode AND
				   --	SUPPLIER_CD=@SUPPLIER_CD_WEB AND
				   --	SUPPLIER_PLANT=@SUPPLIER_PLANT_WEB
			
			-- pattern 2 & 4 temp remark because need master util table
			--------- PATTERN 2 & 4 ---------------
			UPDATE TB_T_DAILY_ORDER_MANIFEST_KCI--TB_R_DAILY_ORDER_MANIFEST
			SET 
				TB_T_DAILY_ORDER_MANIFEST_KCI.PAT_DP_FLAG = 
					CASE PL.PAT_PL_FLAG 
						WHEN '0' THEN '1'
						WHEN '1' THEN '0'
					END, 
				TB_T_DAILY_ORDER_MANIFEST_KCI.PAT_PL_FLAG = PL.PAT_PL_FLAG
			FROM (
				SELECT  x.MANIFEST_NO,
						PAT_PL_FLAG=ISNULL(y.PAT_PL_FLAG,'0')
				FROM
					TB_T_DAILY_ORDER_MANIFEST_KCI X WITH(NOLOCK)
				LEFT OUTER JOIN TB_M_UTIL_PLANE Y WITH(NOLOCK)
				ON X.SUPPLIER_CD = Y.SUPPLIER_CD 
					AND X.SUPPLIER_PLANT = Y.SUPPLIER_PLANT 
					AND ISNULL(X.SHIPPING_DOCK,'*') = ISNULL(Y.SHIPPING_DOCK,'*') 
					AND X.RCV_PLANT_CD = Y.RCV_PLANT_CD 
					AND X.DOCK_CD = Y.DOCK_CD
			) PL 
			WHERE TB_T_DAILY_ORDER_MANIFEST_KCI.MANIFEST_NO = PL.MANIFEST_NO
			and   TB_T_DAILY_ORDER_MANIFEST_KCI.MANIFEST_RECEIVE_FLAG = 0
			and   ISNULL(TB_T_DAILY_ORDER_MANIFEST_KCI.FST_X_DOC_CD,'') <> ''
			AND	  ISNULL(TB_T_DAILY_ORDER_MANIFEST_KCI.SND_X_DOC_CD,'') = ''
			AND   ISNULL(TB_T_DAILY_ORDER_MANIFEST_KCI.PAT_DP_FLAG,'') = ''
			AND   ISNULL(TB_T_DAILY_ORDER_MANIFEST_KCI.PAT_PL_FLAG,'') = ''
			--AND TB_R_DAILY_ORDER_MANIFEST.CREATED_DT=@TODAY 
			AND TB_T_DAILY_ORDER_MANIFEST_KCI.ORDER_TYPE='1' 
			--AND TB_R_DAILY_ORDER_MANIFEST.RCV_PLANT_CD=@RcvPlantCode 
			--AND TB_R_DAILY_ORDER_MANIFEST.SUPPLIER_CD=@SUPPLIER_CD_WEB 
			--AND TB_R_DAILY_ORDER_MANIFEST.SUPPLIER_PLANT=@SUPPLIER_PLANT_WEB
			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of KCI Update Manifest Header Pattern Flag Done Successfully'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]		
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END			
	-- 5. KCI Update Manifest Header Pattern Flag [end]
	
	-- 6. KCI Manifest Header Copy [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '6. KCI Manifest Header Copy'
			
			INSERT INTO [dbo].[TB_T_DAILY_ORDER_MANIFEST]
					   ([MANIFEST_NO]
					   ,[ORDER_NO]
					   ,[SUPPLIER_CD]
					   ,[SUPPLIER_PLANT]
					   ,[ORDER_TYPE]
					   ,[ORDER_RELEASE_DT]
					   ,[COMPANY_CD]
					   ,[RCV_PLANT_CD]
					   ,[DOCK_CD]
					   ,[CAR_FAMILY_CD]
					   ,[RE_EXPORT_CD]
					   ,[TOTAL_ITEM]
					   ,[TOTAL_QTY]
					   ,[ERROR_QTY]
					   ,[ARRIVAL_PLAN_DT]
					   ,[ARRIVAL_ACTUAL_DT]
					   ,[DELAY_FLAG]
					   ,[MANIFEST_RECEIVE_FLAG]
					   ,[PROBLEM_FLAG]
					   ,[IS_ZIPED]
					   ,[PO_NO]
					   ,[GR_DOCUMENT_NO]
					   ,[GR_DOCUMENT_DT]
					   ,[GR_SLIP_DOCUMENT_NO]
					   ,[GR_SLIP_DT]
					   ,[SCAN_DT]
					   ,[APPROVED_DT]
					   ,[MAT_DOC_NO]
					   ,[MAT_DOC_YEAR]
					   ,[ICS_FLAG]
					   ,[ICS_DT]
					   ,[NOTICE_ID]
					   ,[SHIPPING_DOCK]
					   ,[SHIPPING_DT]
					   ,[CANCEL_FLAG]
					   ,[PRINT_FLAG]
					   ,[PRINT_DATE]
					   ,[PRINTED_BY]
					   ,[RECEIVE_FLAG]
					   ,[RECEIVE_DT]
					   ,[USAGE_FLAG]
					   ,[USAGE_DT]
					   ,[DELETION_FLAG]
					   ,[DELETION_DT]
					   ,[DROP_STATUS_FLAG]
					   ,[DROP_STATUS_DT]
					   ,[TOTAL_PAGE]
					   ,[FILENAME]
					   ,[CONVEYANCE_ROUTE]
					   ,[MANIFEST_PRINT_POINT_CD]
					   ,[MANIFEST_PRINT_DATE_PLANT]
					   ,[MANIFEST_PRINT_DATE_LOCAL]
					   ,[P_LANE_CD]
					   ,[P_LANE_SEQ]
					   ,[P_LANE_DT]
					   ,[SUB_ROUTE_CD]
					   ,[SUB_ROUTE_SEQ]
					   ,[SUB_ROUTE_DT]
					   ,[FST_X_DOC_CD]
					   ,[FST_X_PLANT_CD]
					   ,[FST_X_SHIPPING_DOCK_CD]
					   ,[FST_X_DOCK_ARRIVAL_DT]
					   ,[FST_X_DOCK_DEPART_DT]
					   ,[FST_X_DOCK_ROUTE_GRP_CD]
					   ,[FST_X_DOCK_SEQ_NO]
					   ,[FST_X_ROUTE_DT]
					   ,[SND_X_DOC_CD]
					   ,[SND_X_PLANT_CD]
					   ,[SND_X_SHIPPING_DOCK_CD]
					   ,[SND_X_DOCK_ARRIVAL_DT]
					   ,[SND_X_DOCK_DEPART_DT]
					   ,[SND_X_DOCK_ROUTE_GRP_CD]
					   ,[SND_X_DOCK_SEQ_NO]
					   ,[SND_X_ROUTE_DT]
					   ,[TRD_X_DOC_CD]
					   ,[TRD_X_PLANT_CD]
					   ,[TRD_X_SHIPPING_DOCK_CD]
					   ,[TRD_X_DOCK_ARRIVAL_DT]
					   ,[TRD_X_DOCK_DEPART_DT]
					   ,[TRD_X_DOCK_ROUTE_GRP_CD]
					   ,[TRD_X_DOCK_SEQ_NO]
					   ,[TRD_X_ROUTE_DT]
					   ,[PAT_DP_FLAG]
					   ,[PAT_PL_FLAG]
					   ,[CREATED_BY]
					   ,[CREATED_DT]
					   ,[CHANGED_BY]
					   ,[CHANGED_DT]
					   ,[INVOICE_NO]
					   ,[INVOICE_DT]
					   ,[INVOICE_BY]
					   ,[POSTING_DT]
					   ,[POSTING_BY]
					   ,[CANCEL_BY]
					   ,[CANCEL_DT]
					   ,[APPROVED_BY]
					   ,[FTP_FLAG]
					   ,[IN_PROGRESS]
					   ,[DOWNLOAD_FLAG]
					   ,[DOWNLOADED_BY]
					   ,[DOWNLOADED_DT]
					   ,[PROCESS_FLAG])
			SELECT [MANIFEST_NO]
				  ,[ORDER_NO]
				  ,[SUPPLIER_CD]
				  ,[SUPPLIER_PLANT]
				  ,[ORDER_TYPE]
				  ,[ORDER_RELEASE_DT]
				  ,[COMPANY_CD]
				  ,[RCV_PLANT_CD]
				  ,[DOCK_CD]
				  ,[CAR_FAMILY_CD]
				  ,[RE_EXPORT_CD]
				  ,[TOTAL_ITEM]
				  ,[TOTAL_QTY]
				  ,[ERROR_QTY]
				  ,[ARRIVAL_PLAN_DT]
				  ,[ARRIVAL_ACTUAL_DT]
				  ,[DELAY_FLAG]
				  ,[MANIFEST_RECEIVE_FLAG]
				  ,[PROBLEM_FLAG]
				  ,[IS_ZIPED]
				  ,[PO_NO]
				  ,[GR_DOCUMENT_NO]
				  ,[GR_DOCUMENT_DT]
				  ,[GR_SLIP_DOCUMENT_NO]
				  ,[GR_SLIP_DT]
				  ,[SCAN_DT]
				  ,[APPROVED_DT]
				  ,[MAT_DOC_NO]
				  ,[MAT_DOC_YEAR]
				  ,[ICS_FLAG]
				  ,[ICS_DT]
				  ,[NOTICE_ID]
				  ,[SHIPPING_DOCK]
				  ,[SHIPPING_DT]
				  ,[CANCEL_FLAG]
				  ,[PRINT_FLAG]
				  ,[PRINT_DATE]
				  ,[PRINTED_BY]
				  ,[RECEIVE_FLAG]
				  ,[RECEIVE_DT]
				  ,[USAGE_FLAG]
				  ,[USAGE_DT]
				  ,[DELETION_FLAG]
				  ,[DELETION_DT]
				  ,[DROP_STATUS_FLAG]
				  ,[DROP_STATUS_DT]
				  ,[TOTAL_PAGE]
				  ,[FILENAME]
				  ,[CONVEYANCE_ROUTE]
				  ,[MANIFEST_PRINT_POINT_CD]
				  ,[MANIFEST_PRINT_DATE_PLANT]
				  ,[MANIFEST_PRINT_DATE_LOCAL]
				  ,[P_LANE_CD]
				  ,[P_LANE_SEQ]
				  ,[P_LANE_DT]
				  ,[SUB_ROUTE_CD]
				  ,[SUB_ROUTE_SEQ]
				  ,[SUB_ROUTE_DT]
				  ,[FST_X_DOC_CD]
				  ,[FST_X_PLANT_CD]
				  ,[FST_X_SHIPPING_DOCK_CD]
				  ,[FST_X_DOCK_ARRIVAL_DT]
				  ,[FST_X_DOCK_DEPART_DT]
				  ,[FST_X_DOCK_ROUTE_GRP_CD]
				  ,[FST_X_DOCK_SEQ_NO]
				  ,[FST_X_ROUTE_DT]
				  ,[SND_X_DOC_CD]
				  ,[SND_X_PLANT_CD]
				  ,[SND_X_SHIPPING_DOCK_CD]
				  ,[SND_X_DOCK_ARRIVAL_DT]
				  ,[SND_X_DOCK_DEPART_DT]
				  ,[SND_X_DOCK_ROUTE_GRP_CD]
				  ,[SND_X_DOCK_SEQ_NO]
				  ,[SND_X_ROUTE_DT]
				  ,[TRD_X_DOC_CD]
				  ,[TRD_X_PLANT_CD]
				  ,[TRD_X_SHIPPING_DOCK_CD]
				  ,[TRD_X_DOCK_ARRIVAL_DT]
				  ,[TRD_X_DOCK_DEPART_DT]
				  ,[TRD_X_DOCK_ROUTE_GRP_CD]
				  ,[TRD_X_DOCK_SEQ_NO]
				  ,[TRD_X_ROUTE_DT]
				  ,[PAT_DP_FLAG]
				  ,[PAT_PL_FLAG]
				  ,[CREATED_BY]
				  ,[CREATED_DT]
				  ,[CHANGED_BY]
				  ,[CHANGED_DT]
				  ,[INVOICE_NO]
				  ,[INVOICE_DT]
				  ,[INVOICE_BY]
				  ,[POSTING_DT]
				  ,[POSTING_BY]
				  ,[CANCEL_BY]
				  ,[CANCEL_DT]
				  ,[APPROVED_BY]
				  ,[FTP_FLAG]
				  ,[IN_PROGRESS]
				  ,[DOWNLOAD_FLAG]
				  ,[DOWNLOADED_BY]
				  ,[DOWNLOADED_DT]
				  ,[PROCESS_FLAG]
				  /*
				  ,[GROUP_INVOICE]
				  ,[SEPARATION_KEY_1]
				  ,[SEPARATION_KEY_2]
				  ,[SEPARATION_KEY_3]
				  */
			FROM [dbo].[TB_T_DAILY_ORDER_MANIFEST_KCI]
			
			set @ROW_COUNT = @@ROWCOUNT
							
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of KCI Manifest Header Copy Done Successfully, Records [' + cast(@ROW_COUNT as varchar) + ']'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
			set @ROW_COUNT = 0
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END	
	-- 6. KCI Manifest Header Copy [end]
		
	-- 7. KCI Manifest Part Creation [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '7. KCI Manifest Part Creation'
			
			INSERT INTO [dbo].[TB_T_DAILY_ORDER_PART]
					   ([MANIFEST_NO]
					   ,[ITEM_NO]
					   ,[SUPPLIER_PLANT]
					   ,[RCV_PLANT_CD]
					   ,[DOCK_CD]
					   ,[PART_NO]
					   ,[KANBAN_PRINT_ADDRESS]
					   ,[KANBAN_NO]
					   ,[QTY_PER_CONTAINER]
					   ,[ORDER_QTY]
					   ,[ORDER_PLAN_QTY_LOT]
					   ,[RECEIVED_STATUS]
					   ,[BUILD_OUT_FLAG]
					   ,[BUILD_OUT_QTY]
					   ,[AICO_CEPT_FLAG]
					   ,[SUPPLIER_NAME]
					   ,[PART_NAME]
					   ,[ORDER_MODE]
					   ,[KANBAN_PRINT_POINT_CD]
					   ,[KANBAN_PRINT_DATE_PLANT]
					   ,[KANBAN_PRINT_DATE_LOCAL]
					   ,[CONVEYANCE_ROUTE]
					   ,[FERRY_PORT_ARRIVAL_DT]
					   ,[FERRY_PORT_DEPART_DT]
					   ,[EKBS_FLAG]
					   ,[EKBS_DT]
					   ,[PACKAGE_TYPE]
					   ,[RECEIVE_PLANT_NAME]
					   ,[SUPPLIER_INFO1]
					   ,[SUPPLIER_INFO2]
					   ,[SUPPLIER_INFO3]
					   ,[PRINT_CD]
					   ,[IMPORTIR_INFO]
					   ,[IMPORTIR_INFO2]
					   ,[IMPORTIR_INFO3]
					   ,[IMPORTIR_INFO4]
					   ,[CREATED_BY]
					   ,[CREATED_DT]
					   ,[CHANGED_BY]
					   ,[CHANGED_DT]
					   ,[PART_BARCODE]
					   ,[POSTING_STS]
					   ,[CASE_NO]
					   ,[MOD_DST_CD]
					   ,[BOX_NO]
					   ,[PLN_PCK_DT]
					   ,[CONT_SNO]
					   ,[PROCESS_TYPE]
					   ,[LOT_MOD_NO])
			select 
				kci_man.MANIFEST_NO--(<MANIFEST_NO, varchar(16),>
				,ROW_NUMBER() over(
					partition by kci_man.MANIFEST_NO 
					order by 
						kci_prep.SUPP_CD,
						kci_prep.SUPP_PLANT,
						kci_prep.SHIPPING_DOCK,
						kci_prep.COMPANY_CD,
						kci_prep.RCV_PLANT_CODE,
						kci_prep.DOCK_CD,
						kci_prep.ORDER_NO,
						kci_prep.PART_NO
				)--<ITEM_NO, int,>
				,kci_prep.SUPP_PLANT--<SUPPLIER_PLANT, char(1),>
				,kci_prep.RCV_PLANT_CODE--<RCV_PLANT_CD, char(1),>
				,kci_prep.DOCK_CD--<DOCK_CD, varchar(2),>
				,kci_prep.PART_NO--<PART_NO, varchar(12),>
				,
				--edited 2 maret 2015
				CASE WHEN kci_prep.CREATED_BY = 'EKBN'
					THEN kci_prep.EKBN_PRN_ADDRESS
				ELSE			
					isnull(spt.PART_TYPE, '') + isnull(kci_prep.SHUTTER_NO, '') + '-' + isnull(kci_prep.LOT_MOD_NO, '')--<KANBAN_PRINT_ADDRESS, varchar(10),>
				END
				--end of edited

				,ekbn_order.KNBN_NO--<KANBAN_NO, varchar(4),>
				,kci_prep.PIECES_PER_KANBAN--<QTY_PER_CONTAINER, int,>
				,kci_prep.ORDER_QTY--<ORDER_QTY, int,>
				,kci_prep.ORDER_QTY / kci_prep.PIECES_PER_KANBAN--<ORDER_PLAN_QTY_LOT, int,>
				,'0'--<RECEIVED_STATUS, char(1),>
				,ekbn_order.BUILD_OUT_FLAG--<BUILD_OUT_FLAG, char(1),>
				,ekbn_order.BUILD_OUT_QTY--<BUILD_OUT_QTY, varchar(7),>
				,ekbn_order.AICO_CEPT_FLAG--<AICO_CEPT_FLAG, char(1),>
				,ekbn_order.SUPPLIER_NAME--<SUPPLIER_NAME, varchar(40),>
				,ekbn_order.PARTS_NAME--<PART_NAME, varchar(40),>
				,ekbn_order.ORDER_MODE--<ORDER_MODE, char(1),>
				,ekbn_order.KNBN_PRN_POINT_CODE--<KANBAN_PRINT_POINT_CD, varchar(35),>
				,case
					when ekbn_order.KNBN_PRN_DATE_PLANT = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_PLANT, 1, 4) + '-' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_PLANT, 5, 2) + '-' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_PLANT, 7, 2) + ' ' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_PLANT, 9, 2) + ':' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_PLANT, 11, 2) + ':00' , 120)
				end--<KANBAN_PRINT_DATE_PLANT, datetime,>
				,case
					when ekbn_order.KNBN_PRN_DATE_LOCAL = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_LOCAL, 1, 4) + '-' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_LOCAL, 5, 2) + '-' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_LOCAL, 7, 2) + ' ' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_LOCAL, 9, 2) + ':' +
						SUBSTRING(ekbn_order.KNBN_PRN_DATE_LOCAL, 11, 2) + ':00' , 120)
				end--<KANBAN_PRINT_DATE_LOCAL, datetime,>
				,isnull(mu.CATEGORY, '')--<CONVEYANCE_ROUTE, varchar(5),>
				,case
					when ekbn_order.FERRY_PORT_ARRIVAL_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_order.FERRY_PORT_ARRIVAL_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_order.FERRY_PORT_ARRIVAL_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_order.FERRY_PORT_ARRIVAL_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_order.FERRY_PORT_ARRIVAL_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_order.FERRY_PORT_ARRIVAL_DATE, 11, 2) + ':00' , 120)
				end--<FERRY_PORT_ARRIVAL_DT, datetime,>
				,case
					when ekbn_order.FERRY_PORT_DEPART_DATE = '' then null
					else convert(datetime, 
						SUBSTRING(ekbn_order.FERRY_PORT_DEPART_DATE, 1, 4) + '-' +
						SUBSTRING(ekbn_order.FERRY_PORT_DEPART_DATE, 5, 2) + '-' +
						SUBSTRING(ekbn_order.FERRY_PORT_DEPART_DATE, 7, 2) + ' ' +
						SUBSTRING(ekbn_order.FERRY_PORT_DEPART_DATE, 9, 2) + ':' +
						SUBSTRING(ekbn_order.FERRY_PORT_DEPART_DATE, 11, 2) + ':00' , 120)
				end--<FERRY_PORT_DEPART_DT, datetime,>
				,NULL--<EKBS_FLAG, char(1),>
				,NULL--<EKBS_DT, char(1),>
				,part_info.PCK_TYPE--<PACKAGE_TYPE, varchar(18),>
				,plant.PLANT_NM--<RECEIVE_PLANT_NAME, varchar(30),>
				,supp_info.SP_INFO1--<SUPPLIER_INFO1, varchar(15),>
				,supp_info.SP_INFO2--<SUPPLIER_INFO2, varchar(15),>
				,supp_info.SP_INFO3--<SUPPLIER_INFO3, varchar(15),>
				,supp_info.PRINT_CD--<PRINT_CD, char(1),>
				,kci_prep.IMP_INFO_1--<IMPORTIR_INFO, varchar(10),>
				,kci_prep.IMP_INFO_2--<IMPORTIR_INFO2, varchar(10),>
				,kci_prep.IMP_INFO_3--<IMPORTIR_INFO3, varchar(10),>
				,kci_prep.IMP_INFO_4--<IMPORTIR_INFO4, varchar(10),>
				,'KCI-5'--<CREATED_BY, varchar(20),>
				,GETDATE()--<CREATED_DT, datetime,>
				,NULL--<CHANGED_BY, varchar(20),>
				,NULL--<CHANGED_DT, datetime,>
				,kci_prep.PART_BAR_CODE--<PART_BARCODE, varchar(18),>
				,NULL--<POSTING_STS, bit,>
				,kci_prep.CASE_NO--<CASE_NO, varchar(3),>
				,kci_prep.MOD_DST_CD--<MOD_DST_CD, varchar(4),>
				,kci_prep.BOX_NO--<BOX_NO, varchar(3),>
				,kci_prep.PLN_PCK_DT--<PLN_PCK_DT, date,>
				,kci_prep.CONT_SNO--<CONT_SNO, varchar(9),>
				,kci_prep.PROCESS_TYPE--<PROCESS_TYPE, varchar(50),>
				,kci_prep.LOT_MOD_NO--<LOT_MOD_NO, varchar(6),>)
			from TB_T_ORDER_MOD_PREP kci_prep
				inner join TB_T_DAILY_ORDER_MANIFEST_KCI kci_man
					on kci_man.SUPPLIER_CD = kci_prep.SUPP_CD
					and kci_man.SUPPLIER_PLANT = kci_prep.SUPP_PLANT
					and kci_man.SHIPPING_DOCK = kci_prep.SHIPPING_DOCK
					and kci_man.COMPANY_CD = kci_prep.COMPANY_CD
					and kci_man.RCV_PLANT_CD = kci_prep.RCV_PLANT_CODE
					and kci_man.DOCK_CD = kci_prep.DOCK_CD
					and kci_man.ORDER_NO = kci_prep.ORDER_NO
					and kci_man.GROUP_INVOICE = kci_prep.GROUP_INVOICE
					and kci_man.SEPARATION_KEY_1 = kci_prep.SEPARATION_KEY_1
					and kci_man.SEPARATION_KEY_2 = kci_prep.SEPARATION_KEY_2
					and kci_man.SEPARATION_KEY_3 = kci_prep.SEPARATION_KEY_3
				left join (
					/*get TB_T_ORDERPLAN_TMP_KCI with key 					
								t.SUPPLIER_CODE,
								t.SUPPLIER_PLANT,
								t.SHIPPING_DOCK,
								t.COMPANY_CODE,
								t.RCV_PLANT_CODE,
								t.DOCK_CODE,
								t.ORDER_NO,
								t.PART_NO
					*/
					select * from (
						select *,
							ROW_NUMBER() over (
							partition by 
								t.SUPPLIER_CODE,
								t.SUPPLIER_PLANT,
								t.SHIPPING_DOCK,
								t.COMPANY_CODE,
								t.RCV_PLANT_CODE,
								t.DOCK_CODE,
								t.ORDER_NO,
								t.PART_NO
							order by 
								t.KNBN_PRN_ADDRESS
								/*t.SUPPLIER_CODE,
								t.SUPPLIER_PLANT,
								t.SHIPPING_DOCK,
								t.COMPANY_CODE,
								t.RCV_PLANT_CODE,
								t.DOCK_CODE,
								t.ORDER_NO,
								t.PART_NO*/
							) row_num
						from TB_T_ORDERPLAN_TMP_KCI t
/*						where t.STATUS_FLAG = 0
							and t.ORDER_RELEASE_DATE > CONVERT(VARCHAR(8), @lg_LAST_ORDER_DATE, 112)
*/
					) t2 where t2.row_num = 1
				) ekbn_order
					on ekbn_order.SUPPLIER_CODE = kci_prep.SUPP_CD
					and ekbn_order.SUPPLIER_PLANT = kci_prep.SUPP_PLANT
					and ekbn_order.SHIPPING_DOCK = kci_prep.SHIPPING_DOCK
					and ekbn_order.COMPANY_CODE = kci_prep.COMPANY_CD
					and ekbn_order.RCV_PLANT_CODE = kci_prep.RCV_PLANT_CODE
					and ekbn_order.DOCK_CODE = kci_prep.DOCK_CD
					and ekbn_order.ORDER_NO = kci_prep.ORDER_NO
					and ekbn_order.PART_NO = kci_prep.PART_NO
				left join KCI_DB.dbo.TB_M_MODULE_UPDATE mu
					on mu.LOT_MOD_CD = substring(kci_prep.LOT_MOD_NO, 1, 2)
					and mu.PART_NO = kci_prep.PART_NO
					and mu.CASE_NO = kci_prep.CASE_NO
				left join TB_M_PART_INFO part_info
					on part_info.PART_NO = kci_prep.PART_NO
					and part_info.SUPPLIER_CD = kci_prep.SUPP_CD
					and part_info.SUPPLIER_PLANT = kci_prep.SUPP_PLANT
					and part_info.DOCK_CD = kci_prep.DOCK_CD
				left join TB_M_PLANT plant
					on plant.PLANT_CD = kci_prep.RCV_PLANT_CODE
				left join TB_M_SUPPLIER_INFO supp_info
					on supp_info.SUPPLIER_CD = kci_prep.SUPP_CD
					and supp_info.SUPPLIER_PLANT_CD = kci_prep.SUPP_PLANT
					and supp_info.PART_NO = kci_prep.PART_NO
					and supp_info.DOCK_CODE = kci_prep.DOCK_CD
				left join [KCI_DB].dbo.TB_M_SHUTTER_PART_TYPE spt	
					ON spt.PCK_LN_CD = LEFT(RTRIM(LTRIM(kci_prep.STACKING_LINE)), 2)
					and CAST(RIGHT(RTRIM(LTRIM(kci_prep.SHUTTER_NO)), 2) AS NUMERIC(2)) 
						between spt.SHUTTER_NO_FROM AND spt.SHUTTER_NO_TO
					and spt.VALID_FROM <= kci_prep.PRODUCTION_DATE
					and (spt.VALID_TO IS NULL or spt.VALID_TO >= kci_prep.PRODUCTION_DATE)
			
			set @ROW_COUNT = @@ROWCOUNT
			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of KCI Manifest Part Creation Done Successfully, Records [' + cast(@ROW_COUNT as varchar) + ']'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
			set @ROW_COUNT = 0
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END			
	-- 7. KCI Manifest Part Creation [end]
	
	-- 8. Update Table Order Mod at KCI [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '8. Update Table Order Mod at KCI'
			
			update kci_ori
			set STS_PROCESS = 'Y',
				CHANGED_DT = getdate(),
				CHANGED_BY = 'KCI'
			from TB_T_ORDER_MOD_PREP kci_prep
				inner join [KCI_DB].[dbo].TB_R_ORDER_MOD kci_ori
					on CONVERT(varchar(50), kci_ori.rowguid) = kci_prep.rowguid
					

					/*
					on kci_ori.DOCK_CD = kci_prep.DOCK_CD
					and kci_ori.PART_NO = kci_prep.PART_NO
					and kci_ori.ARRIVAL_SEQ = kci_prep.ARRIVAL_SEQ
					and kci_ori.STACKING_LINE = kci_prep.STACKING_LINE
					and kci_ori.PRODUCTION_DATE = kci_prep.PRODUCTION_DATE
					and kci_ori.LOT_MOD_NO = kci_prep.LOT_MOD_NO
					and kci_ori.SUPP_CD = kci_prep.SUPP_CD
					and kci_ori.SUPP_PLANT = kci_prep.SUPP_PLANT
					and kci_ori.rowguid = kci_prep.rowguid
					*/
			set @ROW_COUNT = @@ROWCOUNT
			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of Update Table Order Mod at KCI Done Successfully, Records [' + cast(@ROW_COUNT as varchar) + ']'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
			set @ROW_COUNT = 0
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END			
	-- 8. Update Table Order Mod at KCI [end]
			
	-- 9. E-Kanban Versus KCI Availability Warning [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '9. E-Kanban Versus KCI Availability Warning'
			
			DECLARE l_order_cursor CURSOR LOCAL FAST_FORWARD
			FOR
				select ekbn_sum.SUPPLIER_CODE --char(4)
					,ekbn_sum.SUPPLIER_PLANT --char(1)
					,ekbn_sum.SHIPPING_DOCK --char(3)
					,ekbn_sum.DOCK_CODE --char(2)
					,ekbn_sum.PRODUCTION_DATE --date
					,ekbn_sum.ARRIVAL_SEQ --smallint
				from TB_T_ORDERPLAN_TMP_KCI_SUMM_0 ekbn_sum
					left join TB_T_ORDER_MOD_PREP kci_prep
						on kci_prep.SUPP_CD = ekbn_sum.SUPPLIER_CODE
						and kci_prep.SUPP_PLANT = ekbn_sum.SUPPLIER_PLANT
						and kci_prep.SHIPPING_DOCK = ekbn_sum.SHIPPING_DOCK
						and kci_prep.DOCK_CD = ekbn_sum.DOCK_CODE
						and kci_prep.PRODUCTION_DATE = ekbn_sum.PRODUCTION_DATE
						and kci_prep.ARRIVAL_SEQ = ekbn_sum.ARRIVAL_SEQ
				where 1 = 1
					and kci_prep.SUPP_CD is null
					and kci_prep.SUPP_PLANT is null
					and kci_prep.SHIPPING_DOCK is null
					and kci_prep.DOCK_CD is null
					and kci_prep.PRODUCTION_DATE is null
					and kci_prep.ARRIVAL_SEQ is null					
			OPEN l_order_cursor
			FETCH NEXT FROM l_order_cursor INTO
				@SUPP_CD,
				@SUPP_PLANT,
				@SHIPPING_DOCK,
				@DOCK_CD,
				@PRODUCTION_DT,
				@ARRIVAL_SEQ
			WHILE @@FETCH_STATUS = 0
			BEGIN				
				-- insert into TB_T_ORDERPLAN_TMP_KCI_SUMM_0_SKIP
				INSERT INTO [dbo].[TB_T_ORDERPLAN_TMP_KCI_SUMM_0_SKIP]
						   ([PROCESS_ID]
						   ,[SUPPLIER_CODE]
						   ,[SUPPLIER_PLANT]
						   ,[SHIPPING_DOCK]
						   ,[DOCK_CODE]
						   ,[PRODUCTION_DATE]
						   ,[ARRIVAL_SEQ])
					 VALUES
						   (@PROCESS_ID--<PROCESS_ID, varchar(25),>
						   ,@SUPP_CD--<SUPPLIER_CODE, char(4),>
						   ,@SUPP_PLANT--<SUPPLIER_PLANT, char(1),>
						   ,@SHIPPING_DOCK--<SHIPPING_DOCK, char(3),>
						   ,@DOCK_CD--<DOCK_CODE, char(2),>
						   ,@PRODUCTION_DT--<PRODUCTION_DATE, date,>
						   ,@ARRIVAL_SEQ)--<ARRIVAL_SEQ, smallint,>)
				
				-- Logging warning [start]
				SET @V_M_MSG_ID = 'MSTD00001INF'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'Warning : EKanban Order Number not found at KCI, ' + 
						  'Supplier Cd [' + isnull(@SUPP_CD, 'NULL') + '], ' +
						  'Supplier Plant [' + isnull(@SUPP_PLANT, 'NULL') + '], ' +
						  'Shipping Dock [' + isnull(@SHIPPING_DOCK, 'NULL') + '], ' +
						  'Dock Cd [' + isnull(@DOCK_CD, 'NULL') + '], ' +
						  'Production Date [' + isnull(CONVERT(VARCHAR(8), @PRODUCTION_DT, 112), 'NULL') + '], ' +
						  'Arrival Seq [' + isnull(cast(@ARRIVAL_SEQ as varchar), 'NULL') + ']'
						  ,null
						  ,null
						  ,null
						  ,null)
						  
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'INF'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '0'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
				-- Logging warning [end]
				
				FETCH NEXT FROM l_order_cursor INTO
					@SUPP_CD,
					@SUPP_PLANT,
					@SHIPPING_DOCK,
					@DOCK_CD,
					@PRODUCTION_DT,
					@ARRIVAL_SEQ
			END -- LOOP
			CLOSE l_order_cursor
			DEALLOCATE l_order_cursor			
								
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of E-Kanban Versus KCI Availability Warning Done Successfully'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH
	END			
	-- 9. E-Kanban Versus KCI Availability Warning [end]
	
	-- ================================
	-- 10. Update System Master Data at KCI [start]
	-- ================================
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '10. Update System Master Data at KCI'

			DECLARE @lg_NEXT_ORDER_DATE DATE
			
			SELECT @lg_NEXT_ORDER_DATE=MAX(ORDER_RELEASE_DT)
			FROM [KCI_DB].dbo.TB_M_PART p with(nolock), TB_T_DAILY_ORDER_MANIFEST m2 with(nolock), TB_T_DAILY_ORDER_PART p2 with(nolock)
			WHERE p2.PART_NO = p.PART_NO
				AND m2.SUPPLIER_CD = p.SUPPLIER_CODE
				AND m2.DOCK_CD = p.DOCK_CODE
				AND m2.SUPPLIER_PLANT = p.SUPPLIER_PLANT
				AND m2.SHIPPING_DOCK = p.SHIPPING_DOCK
				AND m2.COMPANY_CD = p.COMPANY_CODE
				AND m2.RCV_PLANT_CD = p.RCV_PLANT_CODE
				AND m2.DOCK_CD = p.DOCK_CODE
				AND p.VALID_FROM <= CONVERT(DATE, LEFT(m2.ORDER_NO, 8), 12)
				AND (p.VALID_TO IS NULL OR p.VALID_TO >= CONVERT(DATE, LEFT(m2.ORDER_NO, 8), 12))
				AND m2.ORDER_RELEASE_DT > @lg_LAST_ORDER_DATE
				AND LEFT(m2.MANIFEST_NO, 1) != 'E'
				
			IF @lg_NEXT_ORDER_DATE IS NULL
			BEGIN
				-- WRITE LOG
				--DO NOT RETURN ERROR
				--SET @PO_RESULT_STATUS = 1
				--SET @FOUND_ERR = 1
				--SET @POSITION = 'Check system parameter'
				SET @V_M_MSG_ID = 'MSTD00150ERR'
				SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
						   @V_M_MSG_ID
						  ,@V_M_FUNCTION_ID
						  ,'for the next ORDER_RELEASE_DATE greater than ' + CONVERT(VARCHAR, @lg_LAST_ORDER_DATE, 112)
						  ,null
						  ,null
						  ,null
						  ,null)
				-- SET @V_M_MSG = 'Process Id should not be null'
				SET @PO_RETURN_CODE = @V_M_MSG
				SELECT @CURRENT_DATE = GETDATE()
				EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
							   @ri_v_user_id = @PI_USER_ID
							  ,@ri_n_process_id = @PROCESS_ID
							  ,@ri_v_fn_id = @V_M_FUNCTION_ID
							  ,@ri_v_module_id = @V_M_MODULE_ID
							  ,@ri_v_msg_id = @V_M_MSG_ID
							  ,@ri_v_message_type = 'INF'
							  ,@ri_v_location = @POSITION
							  ,@ri_v_message = @V_M_MSG
							  ,@ri_v_process_sts = '1'
							  ,@ri_d_date = @CURRENT_DATE 
							  ,@ri_n_row_no = 0 
							  ,@ri_v_flag_finish = 'N' 
							  ,@ro_v_err_msg = @V_M_MSG OUTPUT
				IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
				BEGIN
					SET @PO_RESULT_STATUS = 2
					SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
					SET @FOUND_ERR = 1
					--print @PO_RETURN_CODE
				END
			END
			ELSE
			BEGIN
				UPDATE [KCI_DB].dbo.TB_M_SYSTEM
				SET SYSTEM_VALUE_DT = @lg_NEXT_ORDER_DATE
				WHERE SYSTEM_TYPE = 'RELEASE_PROCESS'
					AND SYSTEM_CD = 'LAST_ORDER_RELEASE_DATE'
				/*	
				-- Additional Info for Warning Message [Start]
				INSERT INTO [KCI_DB].dbo.TB_R_ORDER_PENDING
				(
					ORDER_NO,
					SUPP_CD,
					SUPP_PLANT,
					SHIPPING_DOCK,
					DOCK_CD,
					PART_NO,
					LOT_MOD_NO,
					ORDER_PLAN_QTY,
					ORDER_PLAN_QTY_LOT,
					STS_PROCESS,
					CREATED_BY,
					CREATED_DT
				)
				SELECT DISTINCT m2.ORDER_NO
					  ,m.SUPP_CD
					  ,m.SUPP_PLANT
					  ,m.SHIPPING_DOCK
					  ,m.DOCK_CD
					  ,m.PART_NO
					  ,m.LOT_MOD_NO
					  ,m.ORDER_QTY AS ORDER_PLAN_QTY
					  ,CEILING(m.ORDER_QTY / m.PIECES_PER_KANBAN) AS ORDER_PLAN_QTY_LOT
					  ,'N' AS STS_PROCESS
					  ,'Release Batch' AS CREATED_BY
					  ,GETDATE() AS CREATED_DT
				  FROM [KCI_DB].dbo.TB_R_ORDER_MOD m with(nolock),
				  [KCI_DB].dbo.TB_M_PART p with(nolock),
				  TB_R_DAILY_ORDER_MANIFEST m2 with(nolock), TB_R_DAILY_ORDER_PART p2 with(nolock)
				WHERE m.PART_NO = p.PART_NO
					AND m.SUPP_CD = p.SUPPLIER_CODE
					AND m.DOCK_CD = p.DOCK_CODE
					AND m.SUPP_PLANT = p.SUPPLIER_PLANT
					AND m.SHIPPING_DOCK = p.SHIPPING_DOCK
					AND p.VALID_FROM <= m.PRODUCTION_DATE
					AND (p.VALID_TO IS NULL OR p.VALID_TO >= m.PRODUCTION_DATE)
					AND (m.STS_PROCESS IS NULL OR m.STS_PROCESS = 'N')
					AND m2.SUPPLIER_CD = p.SUPPLIER_CODE
					AND m2.SUPPLIER_PLANT = p.SUPPLIER_PLANT
					AND m2.SHIPPING_DOCK=p.SHIPPING_DOCK
					AND m2.COMPANY_CD = p.COMPANY_CODE
					AND m2.DOCK_CD=p.DOCK_CODE
					AND p2.PART_NO=p.PART_NO
					AND m2.ORDER_NO=(LEFT(CONVERT(VARCHAR, m.PRODUCTION_DATE, 112) +
						CASE WHEN LEN(CONVERT(VARCHAR, m.ARRIVAL_SEQ)) = 1 THEN
							'0'+ CONVERT(VARCHAR, m.ARRIVAL_SEQ)
						ELSE CONVERT(VARCHAR, m.ARRIVAL_SEQ) END, 12))
					AND m.LOT_MOD_NO NOT IN (SELECT SYSTEM_VALUE_TXT FROM 
						[KCI_DB].dbo.TB_M_SYSTEM with(nolock) WHERE
						SYSTEM_TYPE = 'RELEASE_PROCESS'
						AND SYSTEM_CD = 'DUMMY_LOT_MOD_NO')
					AND (SELECT COUNT(1) FROM [KCI_DB].dbo.TB_R_ORDER_PENDING d with(nolock)
								WHERE d.SUPP_CD=p.SUPPLIER_CODE
									and d.SUPP_PLANT = p.SUPPLIER_PLANT
									and d.SHIPPING_DOCK=p.SHIPPING_DOCK
									and d.ORDER_NO = m2.ORDER_NO
									and d.DOCK_CD = p.DOCK_CODE
									and d.PART_NO = p.PART_NO
									and d.LOT_MOD_NO = m.LOT_MOD_NO
						) <= 0
					-- additional condition [start]
					AND LEFT(m2.MANIFEST_NO, 1) != 'E'
					-- additional condition [end]
					ORDER BY m2.ORDER_NO
						,m.SUPP_CD
						,m.SUPP_PLANT
						,m.SHIPPING_DOCK
						,m.DOCK_CD
						,m.PART_NO
				-- Additional Info for Warning Message [End]
				*/
			END
		
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of Update System Master Data at KCI Done Successfully'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH			
	END
	-- 10. Update System Master Data at KCI [end]
	
	-- 11. Update Status Flag TB_T_ORDERPLAN_TMP_KCI [start]
	IF @FOUND_ERR = 0
	BEGIN
		BEGIN TRY
			SET @POSITION = '11. Update Status Flag TB_T_ORDERPLAN_TMP_KCI'
/*			
			update TB_T_ORDERPLAN_TMP_KCI
			set STATUS_FLAG = 1
			where STATUS_FLAG = 0
				and ORDER_RELEASE_DATE > CONVERT(VARCHAR(8), @lg_LAST_ORDER_DATE, 112)
*/			
			-- Logging step done successfully [start]
			SET @V_M_MSG_ID = 'MSTD00001INF'
			SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
					   @V_M_MSG_ID
					  ,@V_M_FUNCTION_ID
					  ,'Step of Update Status Flag TB_T_ORDERPLAN_TMP_KCI Done Successfully'
					  ,null
					  ,null
					  ,null
					  ,null)
					  
			SET @PO_RETURN_CODE = @V_M_MSG
			SELECT @CURRENT_DATE = GETDATE()
			EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
						   @ri_v_user_id = @PI_USER_ID
						  ,@ri_n_process_id = @PROCESS_ID
						  ,@ri_v_fn_id = @V_M_FUNCTION_ID
						  ,@ri_v_module_id = @V_M_MODULE_ID
						  ,@ri_v_msg_id = @V_M_MSG_ID
						  ,@ri_v_message_type = 'INF'
						  ,@ri_v_location = @POSITION
						  ,@ri_v_message = @V_M_MSG
						  ,@ri_v_process_sts = '0'
						  ,@ri_d_date = @CURRENT_DATE 
						  ,@ri_n_row_no = 0 
						  ,@ri_v_flag_finish = 'N' 
						  ,@ro_v_err_msg = @V_M_MSG OUTPUT
			
			IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
			BEGIN
				SET @PO_RESULT_STATUS = 2
				SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
				SET @FOUND_ERR = 1
				--print @PO_RETURN_CODE
			END
			-- Logging step done successfully [end]
		END TRY
		BEGIN CATCH
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = ERROR_MESSAGE()
			--PRINT @POSITION
			--PRINT @PO_RETURN_CODE
			SET @FOUND_ERR = 1
		END CATCH			
	END
	-- 11. Update Status Flag TB_T_ORDERPLAN_TMP_KCI [end]	
	-- MAIN PROCESS [end]
	
	-- Logging end process [start]
	--PRINT 'LOGING END PROCESS'
	IF @FOUND_ERR = 0 -- success
	BEGIN
		-- WRITE LOG
		--COMMIT;
		SET @POSITION = 'End Process'
		SET @V_M_MSG_ID = 'MSTD00171INF'
		SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
				   @V_M_MSG_ID
				  ,@V_M_FUNCTION_ID
				  ,'Mod Order Plan Process'
				  ,null
				  ,null
				  ,null
				  ,null)
		SET @PO_RETURN_CODE = @V_M_MSG
		SELECT @CURRENT_DATE = GETDATE()
		EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
					   @ri_v_user_id = @PI_USER_ID
					  ,@ri_n_process_id = @PROCESS_ID
					  ,@ri_v_fn_id = @V_M_FUNCTION_ID
					  ,@ri_v_module_id = @V_M_MODULE_ID
					  ,@ri_v_msg_id = @V_M_MSG_ID
					  ,@ri_v_message_type = 'INF'
					  ,@ri_v_location = @POSITION
					  ,@ri_v_message = @V_M_MSG
					  ,@ri_v_process_sts = '0'
					  ,@ri_d_date = @CURRENT_DATE 
					  ,@ri_n_row_no = 0 
					  ,@ri_v_flag_finish = 'Y' 
					  ,@ro_v_err_msg = @V_M_MSG OUTPUT
		
		IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
		BEGIN
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
			SET @FOUND_ERR = 1
			--print @PO_RETURN_CODE
		END
	END
	ELSE IF @FOUND_ERR = 3 -- warning
	BEGIN
		-- WRITE LOG
		--COMMIT;
		SET @POSITION = 'End Process'
		SET @V_M_MSG_ID = 'MSTD00173INF'
		SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
				   @V_M_MSG_ID
				  ,@V_M_FUNCTION_ID
				  ,'Order Plan Process'
				  ,null
				  ,null
				  ,null
				  ,null)
		SET @PO_RETURN_CODE = @V_M_MSG
		SELECT @CURRENT_DATE = GETDATE()
		EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
					   @ri_v_user_id = @PI_USER_ID
					  ,@ri_n_process_id = @PROCESS_ID
					  ,@ri_v_fn_id = @V_M_FUNCTION_ID
					  ,@ri_v_module_id = @V_M_MODULE_ID
					  ,@ri_v_msg_id = @V_M_MSG_ID
					  ,@ri_v_message_type = 'INF'
					  ,@ri_v_location = @POSITION
					  ,@ri_v_message = @V_M_MSG
					  ,@ri_v_process_sts = '3'
					  ,@ri_d_date = @CURRENT_DATE 
					  ,@ri_n_row_no = 0 
					  ,@ri_v_flag_finish = 'Y' 
					  ,@ro_v_err_msg = @V_M_MSG OUTPUT
		
		IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
		BEGIN
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
			SET @FOUND_ERR = 1
			--print @PO_RETURN_CODE
		END
	END
	ELSE -- error
	BEGIN
		-- WRITE LOG ERROR
		SET @V_M_MSG_ID = 'MSTD00172INF'
		SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
				   @V_M_MSG_ID
				  ,@V_M_FUNCTION_ID
				  ,isnull(@PO_RETURN_CODE, 'NULL')
				  ,null
				  ,null
				  ,null
				  ,null)
		--SET @PO_RETURN_CODE = @V_M_MSG
		SELECT @CURRENT_DATE = GETDATE()
		EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
					   @ri_v_user_id = @PI_USER_ID
					  ,@ri_n_process_id = @PROCESS_ID
					  ,@ri_v_fn_id = @V_M_FUNCTION_ID
					  ,@ri_v_module_id = @V_M_MODULE_ID
					  ,@ri_v_msg_id = @V_M_MSG_ID
					  ,@ri_v_message_type = 'ERR'
					  ,@ri_v_location = @POSITION
					  ,@ri_v_message = @V_M_MSG
					  ,@ri_v_process_sts = '1'
					  ,@ri_d_date = @CURRENT_DATE 
					  ,@ri_n_row_no = 0 
					  ,@ri_v_flag_finish = 'N' 
					  ,@ro_v_err_msg = @V_M_MSG OUTPUT
		
		IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
		BEGIN
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
			SET @FOUND_ERR = 1
			--print @PO_RETURN_CODE
		END
				
		--ROLLBACK;
		-- WRITE LOG END WITH ERROR
		SET @POSITION = 'End Process'
		SET @V_M_MSG_ID = 'MSTD00172INF'
		SELECT @V_M_MSG = [dbo].[fn_get_message_kci] (
				   @V_M_MSG_ID
				  ,@V_M_FUNCTION_ID
				  ,'Order Plan Process'
				  ,''
				  ,null
				  ,null
				  ,null)
		SET @PO_RETURN_CODE = @V_M_MSG
		SELECT @CURRENT_DATE = GETDATE()
		EXECUTE @V_M_RC = [KCI_DB].[dbo].[sp_create_log] 
					   @ri_v_user_id = @PI_USER_ID
					  ,@ri_n_process_id = @PROCESS_ID
					  ,@ri_v_fn_id = @V_M_FUNCTION_ID
					  ,@ri_v_module_id = @V_M_MODULE_ID
					  ,@ri_v_msg_id = @V_M_MSG_ID
					  ,@ri_v_message_type = 'INF'
					  ,@ri_v_location = @POSITION
					  ,@ri_v_message = @V_M_MSG
					  ,@ri_v_process_sts = '1'
					  ,@ri_d_date = @CURRENT_DATE 
					  ,@ri_n_row_no = 0 
					  ,@ri_v_flag_finish = 'Y' 
					  ,@ro_v_err_msg = @V_M_MSG OUTPUT
		
		IF @V_M_RC <> 0 OR @V_M_MSG IS NOT NULL
		BEGIN
			SET @PO_RESULT_STATUS = 2
			SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: '+@V_M_MSG
			SET @FOUND_ERR = 1
			--print @PO_RETURN_CODE
		END
	END
	-- Logging end process [end]
	
	-- =================================================
	IF @PO_RESULT_STATUS = 0 AND @FOUND_ERR = 0   SET @PO_RETURN_CODE = NULL
	--PRINT 'END PROCESS'
	
	-- Set the recovery process to full
	--ALTER DATABASE NEWKBS SET RECOVERY FULL;

	RETURN @PO_RESULT_STATUS
END TRY
BEGIN CATCH
	/*
	IF @@TRANCOUNT > 0
		rollback transaction process_tran
	*/
	SET @PO_RESULT_STATUS = 2
	SET @PO_RETURN_CODE = 'ERROR WRITE LOG. MSG: ' + ERROR_MESSAGE()
	SET @FOUND_ERR = 1
	
	RETURN @PO_RESULT_STATUS
END CATCH

