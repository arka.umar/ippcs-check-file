
/****** Object:  StoredProcedure [dbo].[SP_INVOICE_CREATION_MAIN_ADD_REMARKS]    Script Date: 30/04/2021 17:00:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2020-12-12
-- Description:	
-- 
-- =============================================
ALTER PROCEDURE [dbo].[SP_INVOICE_CREATION_MAIN_ADD_REMARKS] @T_PROCESS_ID BIGINT
	,@T_VALIDATION_TYPE VARCHAR(10)
	,@T_REMARKS VARCHAR(2000)
	,@T_SUPP_CD VARCHAR(10)
	,@T_ROW_NO INT
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO TB_S_INVOICE_UPLOAD
    (
        --inv_doc_item,       --1
        SUPP_CD, --2
        PO_NO, --3
        PO_ITEM_NO, --4
        ORI_MAT_NO, --5
        MAT_NO, --6
        PROD_PURPOSE_CD, --7
        SOURCE_TYPE, --8
        PACKING_TYPE, --9
        PART_COLOR_SFX, --10
        MAT_TEXT, --11
        MAT_DOC_NO, --12
        MAT_DOC_ITEM, --13
        COMP_PRICE_CD, --14
        SUPP_MANIFEST, --15
        DOC_DT, --16
        QUANTITY, --17
        ORDER_NO, --18
        DOCK_CD, --19
        SUPP_INV_NO, --20
        S_QUANTITY, --21
        PRICE_AMT, --22
        PAYMENT_CURR,
        CURR_CD, --23
        AMMOUNT, --24
        VALIDATION_TYPE, --28
        REMARKS --29
        )
    SELECT 
        --l_v_cur_vld.inv_doc_item,       --1
        L_V_CUR_VLD.SUPP_CD, --2
        L_V_CUR_VLD.PO_NO, --3
        L_V_CUR_VLD.PO_ITEM_NO, --4
        L_V_CUR_VLD.MAT_NO_REF, --5
        L_V_CUR_VLD.MAT_NO, --6
        L_V_CUR_VLD.PROD_PURPOSE_CD, --7
        L_V_CUR_VLD.SOURCE_TYPE_CD, --8
        L_V_CUR_VLD.PACKING_TYPE, --9
        L_V_CUR_VLD.PART_COLOR_SFX, --10
        L_V_CUR_VLD.MAT_TEXT, --11
        L_V_CUR_VLD.MAT_DOC_NO, --12
        L_V_CUR_VLD.MAT_DOC_ITEM, --13
        L_V_CUR_VLD.COMP_PRICE_CD, --14
        L_V_CUR_VLD.INV_REF_NO, --15
        L_V_CUR_VLD.DOC_DT, --16
        L_V_CUR_VLD.QTY, --17
        L_V_CUR_VLD.ORDER_NO, --18
        L_V_CUR_VLD.DOCK_CD, --19
        L_V_CUR_VLD.SUPP_INV_NO, --20
        L_V_CUR_VLD.S_QTY, --21
        L_V_CUR_VLD.PRICE_AMT, --22
        L_V_CUR_VLD.PAY_CURR,
        L_V_CUR_VLD.PAY_CURR, --23
        L_V_CUR_VLD.AMT, --24
        @T_VALIDATION_TYPE, --28 (default value)
        ISNULL(@T_REMARKS, '') --29 (default value) 20-04-07
	FROM TB_T_INV_UPLOAD L_V_CUR_VLD WITH (NOLOCK)
	WHERE PROCESS_ID = @T_PROCESS_ID
		AND SUPP_CD = @T_SUPP_CD
		AND ROW_NO = @T_ROW_NO;
END
