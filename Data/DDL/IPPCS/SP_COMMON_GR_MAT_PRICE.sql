-- =============================================
-- Author       :   FID.Ridwan
-- Create date  :   05-Oct-2020
-- Description  :   Preparation Goods Receipt to ICS
-- Update       :
-- =============================================
CREATE PROCEDURE [dbo].[SP_COMMON_GR_MAT_PRICE] @USER_ID VARCHAR(20) = 'AGAN'
    ,@PROCESS_ID BIGINT = '202009230021'
    ,@r_mat_no VARCHAR(100)
    ,@r_source_type VARCHAR(100)
    ,@r_prod_purpose_cd VARCHAR(100)
    ,@r_color_sfx VARCHAR(100)
    ,@r_packing_type VARCHAR(100)
    ,@r_supp_cd VARCHAR(100)
    ,@r_price_type VARCHAR(100)
    ,@r_ref_date DATE
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    DECLARE @MODULE_ID VARCHAR(50) = '4'
        ,@FUNCTION_ID VARCHAR(100) = '42005'
        ,@IsError CHAR(1) = 'N'
        ,@CURRDT DATE = CAST(GETDATE() AS DATE)
        ,@MSG_TXT AS VARCHAR(MAX)
        ,@MSG_TYPE AS VARCHAR(MAX)
        ,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_COMMON_GR_MAT_PRICE'
        ,@L_ERR AS INT = 0
        ,@N_ERR AS INT = 0
        ,@PARAM1 AS VARCHAR(MAX)
        ,@PARAM2 AS VARCHAR(MAX)
        ,@PARAM3 AS VARCHAR(MAX)
        ,@RES INT = 0;

    BEGIN TRY
        DECLARE @l_v_status VARCHAR(2) = 'OK'
            ,@l_v_prod_purpose_cd VARCHAR(50)
            ,@ln_mat_price NUMERIC(16, 5)
            ,@lv_curr_cd VARCHAR(3)
            ,@l_color_sfx VARCHAR(100)
            ,@r_mat_price NUMERIC(16, 5)
            ,@r_curr_cd VARCHAR(3);

        EXEC dbo.CommonGetMessage 'MPCS00002INF'
            ,@MSG_TXT OUTPUT
            ,@N_ERR OUTPUT
            ,'GET MATERIAL PRICE'

        INSERT INTO TB_R_LOG_D (
            PROCESS_ID
            ,SEQUENCE_NUMBER
            ,MESSAGE_ID
            ,MESSAGE_TYPE
            ,[MESSAGE]
            ,LOCATION
            ,CREATED_BY
            ,CREATED_DATE
            )
        SELECT @PROCESS_ID
            ,(
                SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                FROM dbo.TB_R_LOG_D
                WHERE PROCESS_ID = @PROCESS_ID
                )
            ,'MPCS00002INF'
            ,'INF'
            ,'MPCS00002INF : ' + @MSG_TXT
            ,@LOG_LOCATION
            ,@USER_ID
            ,GETDATE()

        --check validation parameter
        IF ISNULL(@r_mat_no, '') = ''
        BEGIN
            SET @IsError = 'Y';

            EXEC dbo.CommonGetMessage 'MPCS00003ERR'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,'Material number'

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MPCS00003ERR'
                ,'ERR'
                ,'MPCS00003ERR : ' + @MSG_TXT
                ,@LOG_LOCATION + ' : Mandatory Checking'
                ,@USER_ID
                ,GETDATE()

            SET @r_mat_price = 0;
            SET @r_curr_cd = '';
        END
        ELSE
            IF LEN(@r_mat_no) > 23
            BEGIN
                SET @IsError = 'Y';
                SET @PARAM1 = 'Invalid Length of Material number,  the Length should be between 1 and 23 characters.';

                EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                    ,@MSG_TXT OUTPUT
                    ,@N_ERR OUTPUT
                    ,@PARAM1

                INSERT INTO TB_R_LOG_D (
                    PROCESS_ID
                    ,SEQUENCE_NUMBER
                    ,MESSAGE_ID
                    ,MESSAGE_TYPE
                    ,[MESSAGE]
                    ,LOCATION
                    ,CREATED_BY
                    ,CREATED_DATE
                    )
                SELECT @PROCESS_ID
                    ,(
                        SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                        FROM dbo.TB_R_LOG_D
                        WHERE PROCESS_ID = @PROCESS_ID
                        )
                    ,'MSPT00001ERR'
                    ,'ERR'
                    ,'MSPT00001ERR : ' + @MSG_TXT
                    ,@LOG_LOCATION
                    ,@USER_ID
                    ,GETDATE()

                SET @r_mat_price = 0;
                SET @r_curr_cd = '';
            END;

        IF ISNULL(@r_source_type, '') = ''
        BEGIN
            SET @IsError = 'Y';

            EXEC dbo.CommonGetMessage 'MPCS00003ERR'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,'Source type'

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MPCS00003ERR'
                ,'ERR'
                ,'MPCS00003ERR : ' + @MSG_TXT
                ,@LOG_LOCATION + ' : Mandatory Checking'
                ,@USER_ID
                ,GETDATE()

            SET @r_mat_price = 0;
            SET @r_curr_cd = '';
        END
        ELSE
            IF LEN(@r_source_type) > 1
            BEGIN
                SET @IsError = 'Y';
                SET @PARAM1 = 'Invalid Length of Source type,  the Length should be between 1 and 1 characters.';

                EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                    ,@MSG_TXT OUTPUT
                    ,@N_ERR OUTPUT
                    ,@PARAM1

                INSERT INTO TB_R_LOG_D (
                    PROCESS_ID
                    ,SEQUENCE_NUMBER
                    ,MESSAGE_ID
                    ,MESSAGE_TYPE
                    ,[MESSAGE]
                    ,LOCATION
                    ,CREATED_BY
                    ,CREATED_DATE
                    )
                SELECT @PROCESS_ID
                    ,(
                        SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                        FROM dbo.TB_R_LOG_D
                        WHERE PROCESS_ID = @PROCESS_ID
                        )
                    ,'MSPT00001ERR'
                    ,'ERR'
                    ,'MSPT00001ERR : ' + @MSG_TXT
                    ,@LOG_LOCATION
                    ,@USER_ID
                    ,GETDATE()

                SET @r_mat_price = 0;
                SET @r_curr_cd = '';
            END;

        IF ISNULL(@r_ref_date, '') = ''
        BEGIN
            SET @IsError = 'Y';

            EXEC dbo.CommonGetMessage 'MPCS00003ERR'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,'Ref date'

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MPCS00003ERR'
                ,'ERR'
                ,'MPCS00003ERR : ' + @MSG_TXT
                ,@LOG_LOCATION + ' : Mandatory Checking'
                ,@USER_ID
                ,GETDATE()

            SET @r_mat_price = 0;
            SET @r_curr_cd = '';
        END;

        IF ISNULL(@r_price_type, '') = ''
        BEGIN
            SET @IsError = 'Y';

            EXEC dbo.CommonGetMessage 'MPCS00003ERR'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,'Price type'

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MPCS00003ERR'
                ,'ERR'
                ,'MPCS00003ERR : ' + @MSG_TXT
                ,@LOG_LOCATION + ' : Mandatory Checking'
                ,@USER_ID
                ,GETDATE()

            SET @r_mat_price = 0;
            SET @r_curr_cd = '';
        END
        ELSE
            IF upper(@r_price_type) = 'S'
            BEGIN
                SET @IsError = 'N';
            END
            ELSE
                IF upper(@r_price_type) = 'T'
                BEGIN
                    IF ISNULL(@r_packing_type, '') = ''
                    BEGIN
                        SET @IsError = 'Y';

                        EXEC dbo.CommonGetMessage 'MPCS00003ERR'
                            ,@MSG_TXT OUTPUT
                            ,@N_ERR OUTPUT
                            ,'Packing type'

                        INSERT INTO TB_R_LOG_D (
                            PROCESS_ID
                            ,SEQUENCE_NUMBER
                            ,MESSAGE_ID
                            ,MESSAGE_TYPE
                            ,[MESSAGE]
                            ,LOCATION
                            ,CREATED_BY
                            ,CREATED_DATE
                            )
                        SELECT @PROCESS_ID
                            ,(
                                SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                                FROM dbo.TB_R_LOG_D
                                WHERE PROCESS_ID = @PROCESS_ID
                                )
                            ,'MPCS00003ERR'
                            ,'ERR'
                            ,'MPCS00003ERR : ' + @MSG_TXT
                            ,@LOG_LOCATION + ' : Mandatory Checking'
                            ,@USER_ID
                            ,GETDATE()

                        SET @r_mat_price = 0;
                        SET @r_curr_cd = '';
                    END --@r_packing_type
                    ELSE
                        IF LEN(@r_packing_type) > 1
                        BEGIN
                            SET @IsError = 'Y';
                            SET @PARAM1 = 'Invalid Length of Source type,  the Length should be between 1 and 1 characters.';

                            EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                                ,@MSG_TXT OUTPUT
                                ,@N_ERR OUTPUT
                                ,@PARAM1

                            INSERT INTO TB_R_LOG_D (
                                PROCESS_ID
                                ,SEQUENCE_NUMBER
                                ,MESSAGE_ID
                                ,MESSAGE_TYPE
                                ,[MESSAGE]
                                ,LOCATION
                                ,CREATED_BY
                                ,CREATED_DATE
                                )
                            SELECT @PROCESS_ID
                                ,(
                                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                                    FROM dbo.TB_R_LOG_D
                                    WHERE PROCESS_ID = @PROCESS_ID
                                    )
                                ,'MSPT00001ERR'
                                ,'ERR'
                                ,'MSPT00001ERR : ' + @MSG_TXT
                                ,@LOG_LOCATION
                                ,@USER_ID
                                ,GETDATE()

                            SET @r_mat_price = 0;
                            SET @r_curr_cd = '';
                        END;--@r_packing_type

                    IF ISNULL(@r_supp_cd, '') = ''
                    BEGIN
                        SET @IsError = 'Y';

                        EXEC dbo.CommonGetMessage 'MPCS00003ERR'
                            ,@MSG_TXT OUTPUT
                            ,@N_ERR OUTPUT
                            ,'Supplier code'

                        INSERT INTO TB_R_LOG_D (
                            PROCESS_ID
                            ,SEQUENCE_NUMBER
                            ,MESSAGE_ID
                            ,MESSAGE_TYPE
                            ,[MESSAGE]
                            ,LOCATION
                            ,CREATED_BY
                            ,CREATED_DATE
                            )
                        SELECT @PROCESS_ID
                            ,(
                                SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                                FROM dbo.TB_R_LOG_D
                                WHERE PROCESS_ID = @PROCESS_ID
                                )
                            ,'MPCS00003ERR'
                            ,'ERR'
                            ,'MPCS00003ERR : ' + @MSG_TXT
                            ,@LOG_LOCATION + ' : Mandatory Checking'
                            ,@USER_ID
                            ,GETDATE()

                        SET @r_mat_price = 0;
                        SET @r_curr_cd = '';
                    END --@r_supp_cd
                    ELSE
                        IF LEN(@r_supp_cd) > 6
                        BEGIN
                            SET @IsError = 'Y';
                            SET @PARAM1 = 'Invalid Length of Supplier code,  the Length should be between 1 and 6 characters.';

                            EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                                ,@MSG_TXT OUTPUT
                                ,@N_ERR OUTPUT
                                ,@PARAM1

                            INSERT INTO TB_R_LOG_D (
                                PROCESS_ID
                                ,SEQUENCE_NUMBER
                                ,MESSAGE_ID
                                ,MESSAGE_TYPE
                                ,[MESSAGE]
                                ,LOCATION
                                ,CREATED_BY
                                ,CREATED_DATE
                                )
                            SELECT @PROCESS_ID
                                ,(
                                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                                    FROM dbo.TB_R_LOG_D
                                    WHERE PROCESS_ID = @PROCESS_ID
                                    )
                                ,'MSPT00001ERR'
                                ,'ERR'
                                ,'MSPT00001ERR : ' + @MSG_TXT
                                ,@LOG_LOCATION
                                ,@USER_ID
                                ,GETDATE()

                            SET @r_mat_price = 0;
                            SET @r_curr_cd = '';
                        END --@r_supp_cd;
                END --upper(@r_price_type) = 'T'
                ELSE
                BEGIN
                    SET @IsError = 'Y';
                    SET @PARAM1 = 'Price Type Must be T or S';

                    EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                        ,@MSG_TXT OUTPUT
                        ,@N_ERR OUTPUT
                        ,@PARAM1

                    INSERT INTO TB_R_LOG_D (
                        PROCESS_ID
                        ,SEQUENCE_NUMBER
                        ,MESSAGE_ID
                        ,MESSAGE_TYPE
                        ,[MESSAGE]
                        ,LOCATION
                        ,CREATED_BY
                        ,CREATED_DATE
                        )
                    SELECT @PROCESS_ID
                        ,(
                            SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                            FROM dbo.TB_R_LOG_D
                            WHERE PROCESS_ID = @PROCESS_ID
                            )
                        ,'MSPT00001ERR'
                        ,'ERR'
                        ,'MSPT00001ERR : ' + @MSG_TXT
                        ,@LOG_LOCATION
                        ,@USER_ID
                        ,GETDATE()

                    SET @r_mat_price = 0;
                    SET @r_curr_cd = '';
                END;

        IF LEN(@r_prod_purpose_cd) > 5
        BEGIN
            SET @IsError = 'Y';
            SET @PARAM1 = 'Invalid Length of Prod Purpose,  the Length should be between 1 and 5 characters.';

            EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,@PARAM1

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MSPT00001ERR'
                ,'ERR'
                ,'MSPT00001ERR : ' + @MSG_TXT
                ,@LOG_LOCATION
                ,@USER_ID
                ,GETDATE()

            SET @r_mat_price = 0;
            SET @r_curr_cd = '';
        END;

        IF ISNULL(@r_prod_purpose_cd, '') = ''
        BEGIN
            SELECT @l_v_prod_purpose_cd = SYSTEM_VALUE
            FROM TB_M_SYSTEM
            WHERE FUNCTION_ID = 'COMMON_PO'
                AND SYSTEM_CD = 'PROD_PURPOSE_CD'

            IF ISNULL(@l_v_prod_purpose_cd, '') = ''
            BEGIN
                SET @IsError = 'Y';
                SET @PARAM1 = 'Production purpose is not available in system master';

                EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                    ,@MSG_TXT OUTPUT
                    ,@N_ERR OUTPUT
                    ,@PARAM1

                INSERT INTO TB_R_LOG_D (
                    PROCESS_ID
                    ,SEQUENCE_NUMBER
                    ,MESSAGE_ID
                    ,MESSAGE_TYPE
                    ,[MESSAGE]
                    ,LOCATION
                    ,CREATED_BY
                    ,CREATED_DATE
                    )
                SELECT @PROCESS_ID
                    ,(
                        SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                        FROM dbo.TB_R_LOG_D
                        WHERE PROCESS_ID = @PROCESS_ID
                        )
                    ,'MSPT00001ERR'
                    ,'ERR'
                    ,'MSPT00001ERR : ' + @MSG_TXT
                    ,@LOG_LOCATION
                    ,@USER_ID
                    ,GETDATE()

                SET @r_mat_price = 0;
                SET @r_curr_cd = '';
            END;
            ELSE
            BEGIN
                SET @l_v_prod_purpose_cd = @r_prod_purpose_cd;
            END
        END ELSE
        BEGIN
            SET @l_v_prod_purpose_cd = @r_prod_purpose_cd;
        END

        SELECT @l_color_sfx = SYSTEM_VALUE
        FROM TB_M_SYSTEM
        WHERE FUNCTION_ID = 'COMMON_PO'
            AND SYSTEM_CD = 'PART_COLOR_SFX_PRICE'

        IF ISNULL(@l_color_sfx, '') = ''
        BEGIN
            SET @IsError = 'Y';
            SET @PARAM1 = 'Color Suffix is not available in system master';

            EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,@PARAM1

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MSPT00001ERR'
                ,'ERR'
                ,'MSPT00001ERR : ' + @MSG_TXT
                ,@LOG_LOCATION
                ,@USER_ID
                ,GETDATE()

            SET @r_mat_price = 0;
            SET @r_curr_cd = '';
        END;

        --end check validation parameter
        IF @IsError = 'N'
        BEGIN
            --start main process
            -- check price type
            IF upper(@r_price_type) = 'S'
            BEGIN
                --begin 1 of r_price_type = 'S'
                SELECT @ln_mat_price = S.STANDARD_PRICE
                    ,@lv_curr_cd = S.CURR_CD
                FROM TB_M_STANDARD_PRICE S
                WHERE S.MAT_NO = @r_mat_no
                    AND S.SOURCE_TYPE = @r_source_type
                    AND S.PROD_PURPOSE_CD = @l_v_prod_purpose_cd
                    AND CONVERT(VARCHAR, S.VALID_DT_FR, 112) <= CONVERT(VARCHAR, @r_ref_date, 112)
                    AND ISNULL(CONVERT(VARCHAR, S.VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @r_ref_date, 112);

                IF ISNULL(@ln_mat_price, '0') = '0'
                BEGIN
                    SET @IsError = 'Y';
                    SET @PARAM1 = 'Price not found for Material Number: ' + ISNULL(@r_mat_no, '') + ', Source Type: ' + ISNULL(@r_source_type,'') + ', Prod Purpose: ' + ISNULL(@l_v_prod_purpose_cd,'') + ', Reff Date: ' + CONVERT(VARCHAR, ISNULL(@r_ref_date, ''), 112);

                    EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                        ,@MSG_TXT OUTPUT
                        ,@N_ERR OUTPUT
                        ,@PARAM1

                    INSERT INTO TB_R_LOG_D (
                        PROCESS_ID
                        ,SEQUENCE_NUMBER
                        ,MESSAGE_ID
                        ,MESSAGE_TYPE
                        ,[MESSAGE]
                        ,LOCATION
                        ,CREATED_BY
                        ,CREATED_DATE
                        )
                    SELECT @PROCESS_ID
                        ,(
                            SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                            FROM dbo.TB_R_LOG_D
                            WHERE PROCESS_ID = @PROCESS_ID
                            )
                        ,'MPCS00006ERR'
                        ,'ERR'
                        ,'MPCS00006ERR : ' + @MSG_TXT
                        ,@LOG_LOCATION
                        ,@USER_ID
                        ,GETDATE()

                    SET @r_mat_price = 0;
                    SET @r_curr_cd = '';
                END
                ELSE
                BEGIN
                    SET @r_mat_price = @ln_mat_price;
                    SET @r_curr_cd = @lv_curr_cd;
                END
            END --end of begin 1, of r_price_type = 'S'
            ELSE
                IF upper(@r_price_type) = 'T'
                BEGIN --start of begin 2 of r_price_type = 'T'
                    --begin 2 of r_price_type = 'T'
                    --get price on date with param.color_suffix
                    --if param.color_suffix is null, replace with system.color_suffix
                    SELECT @ln_mat_price = M.PRICE_AMT
                        ,@lv_curr_cd = M.CURR_CD
                    FROM TB_M_MATERIAL_PRICE M
                    WHERE M.MAT_NO = @r_mat_no
                        AND M.SOURCE_TYPE = @r_source_type
                        AND M.PROD_PURPOSE_CD = @l_v_prod_purpose_cd
                        AND M.PART_COLOR_SFX = ISNULL(@r_color_sfx, @l_color_sfx)
                        AND M.Packing_Type = @r_packing_type
                        AND M.SUPP_CD = @r_supp_cd
                        AND CONVERT(VARCHAR, M.VALID_DT_FR, 112) <= CONVERT(VARCHAR, @r_ref_date, 112)
                        AND ISNULL(CONVERT(VARCHAR, M.VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @r_ref_date, 112)
                        AND M.APPROVE_STS = 'Y';

                    IF ISNULL(@ln_mat_price, '0') <> '0'
                    BEGIN
                        SET @r_mat_price = @ln_mat_price;
                        SET @r_curr_cd = @lv_curr_cd;
                    END
                    ELSE
                    BEGIN --2
                        --get price on date with system.color_suffix
                        SELECT @ln_mat_price = M.PRICE_AMT
                            ,@lv_curr_cd = M.CURR_CD
                        FROM TB_M_MATERIAL_PRICE M
                        WHERE M.MAT_NO = @r_mat_no
                            AND M.SOURCE_TYPE = @r_source_type
                            AND M.PROD_PURPOSE_CD = @l_v_prod_purpose_cd
                            AND M.PART_COLOR_SFX = @l_color_sfx
                            AND M.Packing_Type = @r_packing_type
                            AND M.SUPP_CD = @r_supp_cd
                            AND CONVERT(VARCHAR, M.VALID_DT_FR, 112) <= CONVERT(VARCHAR, @r_ref_date, 112)
                            AND ISNULL(CONVERT(VARCHAR, M.VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @r_ref_date, 112)
                            AND M.APPROVE_STS = 'Y';

                        IF ISNULL(@ln_mat_price, '0') <> '0'
                        BEGIN
                            SET @r_mat_price = @ln_mat_price;
                            SET @r_curr_cd = @lv_curr_cd;
                        END
                        ELSE
                        BEGIN --3
                            --get latest approved price with param.color_suffix
                            --if param.color_suffix is null, replace with system.color_suffix
                            SELECT @ln_mat_price = PRICE_AMT
                                ,@lv_curr_cd = CURR_CD
                            FROM (
                                SELECT TOP 1 M.PRICE_AMT
                                    ,M.CURR_CD
                                FROM TB_M_MATERIAL_PRICE M
                                WHERE M.MAT_NO = @r_mat_no
                                    AND M.SOURCE_TYPE = @r_source_type
                                    AND M.PROD_PURPOSE_CD = @l_v_prod_purpose_cd
                                    AND M.PART_COLOR_SFX = ISNULL(@r_color_sfx, @l_color_sfx)
                                    AND M.Packing_Type = @r_packing_type
                                    AND M.SUPP_CD = @r_supp_cd
                                    AND M.APPROVE_STS = 'Y'
                                ORDER BY m.valid_dt_fr DESC
                                ) TB;

                            IF ISNULL(@ln_mat_price, '0') <> '0'
                            BEGIN
                                SET @r_mat_price = @ln_mat_price;
                                SET @r_curr_cd = @lv_curr_cd;
                            END
                            ELSE
                            BEGIN --4
                                --get latest approved price with param.color_suffix
                                SELECT @ln_mat_price = PRICE_AMT
                                    ,@lv_curr_cd = CURR_CD
                                FROM (
                                    SELECT TOP 1 M.PRICE_AMT
                                        ,M.CURR_CD
                                    FROM TB_M_MATERIAL_PRICE M
                                    WHERE M.MAT_NO = @r_mat_no
                                        AND M.SOURCE_TYPE = @r_source_type
                                        AND M.PROD_PURPOSE_CD = @l_v_prod_purpose_cd
                                        AND M.PART_COLOR_SFX = @l_color_sfx
                                        AND M.Packing_Type = @r_packing_type
                                        AND M.SUPP_CD = @r_supp_cd
                                        AND M.APPROVE_STS = 'Y'
                                    ORDER BY m.valid_dt_fr DESC
                                    ) TB;

                                IF ISNULL(@ln_mat_price, '0') <> '0'
                                BEGIN
                                    SET @r_mat_price = @ln_mat_price;
                                    SET @r_curr_cd = @lv_curr_cd;
                                END
                                ELSE
                                BEGIN
                                    SET @IsError = 'Y';
                                    SET @PARAM1 = 'Price not found for Material Number: ' + ISNULL(@r_mat_no, '') + ', Source Type: ' + ISNULL(@r_source_type, '') + ', Prod Purpose: ' + ISNULL(@l_v_prod_purpose_cd, '') + ', Reff Date: ' + CONVERT(VARCHAR, @r_ref_date, 112) + ', Color Suffix: ' + ISNULL(@r_color_sfx, @l_color_sfx) + ', Supplier Code: ' + @r_supp_cd;

                                    EXEC dbo.CommonGetMessage 'MSPT00001ERR'
                                        ,@MSG_TXT OUTPUT
                                        ,@N_ERR OUTPUT
                                        ,@PARAM1

                                    INSERT INTO TB_R_LOG_D (
                                        PROCESS_ID
                                        ,SEQUENCE_NUMBER
                                        ,MESSAGE_ID
                                        ,MESSAGE_TYPE
                                        ,[MESSAGE]
                                        ,LOCATION
                                        ,CREATED_BY
                                        ,CREATED_DATE
                                        )
                                    SELECT @PROCESS_ID
                                        ,(
                                            SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                                            FROM dbo.TB_R_LOG_D
                                            WHERE PROCESS_ID = @PROCESS_ID
                                            )
                                        ,'MPCS00006ERR'
                                        ,'ERR'
                                        ,'MPCS00006ERR : ' + @MSG_TXT
                                        ,@LOG_LOCATION
                                        ,@USER_ID
                                        ,GETDATE()

                                    SET @r_mat_price = 0;
                                    SET @r_curr_cd = '';
                                END
                            END --4
                        END --3
                    END --2
                END;--end of begin 2 of r_price_type = 'T'
        END;--end if of l_v_status 'OK'

        IF @IsError = 'N'
        BEGIN
            EXEC dbo.CommonGetMessage 'MSPT00005INF'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,'GET MATERIAL PRICE'

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MSPT00005INF'
                ,'INF'
                ,'MSPT00005INF : ' + @MSG_TXT
                ,@LOG_LOCATION
                ,@USER_ID
                ,GETDATE()
        END
        ELSE
        BEGIN
            EXEC dbo.CommonGetMessage 'MSPT00006INF'
                ,@MSG_TXT OUTPUT
                ,@N_ERR OUTPUT
                ,'GET MATERIAL PRICE'

            INSERT INTO TB_R_LOG_D (
                PROCESS_ID
                ,SEQUENCE_NUMBER
                ,MESSAGE_ID
                ,MESSAGE_TYPE
                ,[MESSAGE]
                ,LOCATION
                ,CREATED_BY
                ,CREATED_DATE
                )
            SELECT @PROCESS_ID
                ,(
                    SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                    FROM dbo.TB_R_LOG_D
                    WHERE PROCESS_ID = @PROCESS_ID
                    )
                ,'MSPT00006INF'
                ,'INF'
                ,'MSPT00006INF : ' + @MSG_TXT
                ,@LOG_LOCATION
                ,@USER_ID
                ,GETDATE()
        END

        SELECT @IsError l_n_status
            ,@r_mat_price l_n_mat_price
            ,@lv_curr_cd l_v_curr_cd;
    END TRY

    BEGIN CATCH
        SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

        EXEC dbo.CommonGetMessage 'MPCS00999ERR'
            ,@MSG_TXT OUTPUT
            ,@N_ERR OUTPUT
            ,@PARAM1

        INSERT INTO TB_R_LOG_D (
            PROCESS_ID
            ,SEQUENCE_NUMBER
            ,MESSAGE_ID
            ,MESSAGE_TYPE
            ,[MESSAGE]
            ,LOCATION
            ,CREATED_BY
            ,CREATED_DATE
            )
        SELECT @PROCESS_ID
            ,(
                SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
                FROM dbo.TB_R_LOG_D
                WHERE PROCESS_ID = @PROCESS_ID
                )
            ,'MPCS00999ERR'
            ,'ERR'
            ,'MPCS00999ERR : ' + @MSG_TXT
            ,@LOG_LOCATION
            ,@USER_ID
            ,GETDATE()

        SELECT 'Y' l_n_status
            ,'0' l_n_mat_price
            ,'' l_v_curr_cd;
    END CATCH
END
