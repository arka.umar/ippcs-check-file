-- =============================================
-- Author:		fid.deny
-- Create date: 24 - 11 - 2015
-- Description:	GET list of error when uploading supplier email
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_ERROR_LIST_FOR_SUPPLIER_EMAIL]
@process_id varchar(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT T.*, V.MSG AS MSG
	INTO #temp
		FROM TB_T_SUPPLIER_EMAIL_UPLOAD_TEMP T LEFT JOIN TB_T_SUPPLIER_EMAIL_UPLOAD_TEMP_VALIDATE V
	ON 
		T.ID = V.ID AND T.PROCESS_ID = V.PROCESS_ID
	WHERE T.PROCESS_ID = @process_id
	ORDER BY T.ID ASC

	SELECT
		DISTINCT t2.ID, t2.SUPPLIER_CODE, t2.AREA_CODE, t2.NAME, t2.EMAIL, t2.POSITION,
		SUBSTRING((
					SELECT ';' + t1.MSG AS [text()]
					FROM #temp t1
					WHERE t1.ID = t2.ID
					FOR XML PATH('')), 2, 1000) MSGG
	FROM #temp t2
	ORDER BY t2.ID ASC

	IF OBJECT_ID('tempdb..#temp') IS NOT NULL 
	DROP TABLE #temp
END

