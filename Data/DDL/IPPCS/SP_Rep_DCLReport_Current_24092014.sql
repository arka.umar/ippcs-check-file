-- ==============================================
-- Created By	: Arie
-- Created Date : 2013-02-22
-- Modified By : Rahmat
-- Modified Date : 24-06-2014
-- Created For	: DCL Report Monthly
-- ==============================================

CREATE PROCEDURE [dbo].[SP_Rep_DCLReport_Current_24092014]
	@ProductionYear varchar (4),
	@ProductionMonth varchar (2),
	@LPCode varchar(100),
	@SupplierCode varchar(100)
AS
BEGIN

	DECLARE @SEQROW INT = 0

	SELECT row_number() OVER (ORDER BY SUPPLIER_NAME, ARRVDATETIME ASC, DPTDATETIME DESC) AS SeqNbr
			, SUPPLIER_NAME
			, route_code
			, RATE
			, convert(TIME, ARRVDATETIME) AS ARRVDATETIME
			, convert(TIME, DPTDATETIME) AS DPTDATETIME
			, ARRVDATE
			, dock_code
			, lp_code
			, PICKUP_DT
			, Supplier
			, SupplierPlant
     INTO #temp
	FROM
	  (
		SELECT 
		s.SUPPLIER_NAME, 
		sm.RTEGRPCD AS route_code, 
		sm.RUNSEQ AS RATE, 
		convert(TIME, sm.ARRVDATETIME) AS ARRVDATETIME, 
		convert(TIME, sm.DPTDATETIME) AS DPTDATETIME, 
		convert(DATE, sm.ARRVDATETIME) AS ARRVDATE,
		CASE WHEN sm.LOGPTCD LIKE '807%' THEN ISNULL((SELECT DISTINCT PHYSICAL_DOCK FROM TB_M_DELIVERY_STATION_MAPPING WHERE DOCK_CD = sm.DOCKCD), sm.DOCKCD) ELSE '' END AS dock_code, 
		sm.LOGPARTNERCD AS lp_code, 
		DATENAME(MONTH, @ProductionYear+'-'+@ProductionMonth+'-01') + ' ' + @ProductionYear AS PICKUP_DT,
		sm.LOGPTCD AS Supplier,
		sm.PLANTCD AS SupplierPlant
	   FROM
		 TB_T_SUPPLIER_MEETING sm
		 INNER JOIN TB_M_SUPPLIER s ON s.SUPPLIER_CODE = sm.LOGPTCD AND s.SUPPLIER_PLANT_CD = sm.PLANTCD
	   WHERE
		 sm.PLANNINGTERMYEARFROM = @ProductionYear AND sm.PLANNINGTERMCDFROM = 'P'+@ProductionMonth
		 AND ((sm.LOGPTCD + '-' + sm.PLANTCD = @SupplierCode
		 AND isnull(@SupplierCode, '') <> ''
		 OR (isnull(@SupplierCode, '') = '')))
		 AND ((sm.LOGPARTNERCD = @LPCode
		 AND isnull(@LPCode, '') <> ''
		 OR (isnull(@LPCode, '') = '')))
		 AND INBOUTBFLAG = 0
	  ) AS a
	ORDER BY
	  SeqNbr ASC
	
	CREATE TABLE #temp2
	 (SeqNbr INT,
	  SupplierName Varchar(500),
	  RouteCode VARCHAR(4),
	  Rate VARCHAR(2),
	  Arrival varchar(5),
	  Departure varchar(5),
	  ArrivalDate varchar(10),
	  DockCode VARCHAR(500),
	  Lp_Code VARCHAR(3),
	  PickUp VARCHAR(100),
	  PickupMethod CHAR(2),
	  Supplier Varchar(20),
	  SupplierPlant Varchar(20)) 
	  
	CREATE TABLE #temp3
	 (SeqNbr INT,
	  DockCode VARCHAR(20)) 
	
	--INSERT INTO #temp2
	--SELECT row_number() OVER (ORDER BY SUPPLIER_NAME) AS SeqNbr, SUPPLIER_NAME, route_code , RATE, ARRVDATETIME,  DPTDATETIME, dock_code, 
	--	   lp_code, PICKUP_DT, PickupMethod, Supplier, SupplierPlant FROM (
	--	SELECT SUPPLIER_NAME, route_code , RATE, MIN(ARRVDATETIME) AS ARRVDATETIME, MAX(DPTDATETIME) AS DPTDATETIME, '' AS dock_code, lp_code, PICKUP_DT,
	--		CASE WHEN lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 1) THEN 'DR' ELSE 'LP' END AS PickupMethod, Supplier, SupplierPlant
	--	FROM #temp
	--	GROUP BY SUPPLIER_NAME, route_code , RATE, lp_code, PICKUP_DT, Supplier, SupplierPlant
	--) tbl
	
	INSERT INTO #temp2
	SELECT row_number() OVER (ORDER BY SUPPLIER_NAME) AS SeqNbr, SUPPLIER_NAME, route_code , RATE, ARRVDATETIME,  DPTDATETIME, ARRVDATE, dock_code, 
		   lp_code, PICKUP_DT, PickupMethod, Supplier, SupplierPlant FROM (
		SELECT t.SUPPLIER_NAME, t.route_code , t.RATE, MIN(convert(TIME, sm.ARRVDATETIME)) AS ARRVDATETIME, MAX(convert(TIME, sm.DPTDATETIME)) AS DPTDATETIME, MIN(convert(DATE, sm.ARRVDATETIME)) AS ARRVDATE, '' AS dock_code, t.lp_code, t.PICKUP_DT,
			CASE 
				WHEN t.lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 1) THEN 'DR'
				WHEN t.lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 2) THEN 'LP' 
				ELSE 'DR' 
			END AS PickupMethod, t.Supplier, t.SupplierPlant
		FROM #temp t
			INNER JOIN TB_T_SUPPLIER_MEETING sm ON sm.PLANNINGTERMYEARFROM = @ProductionYear AND sm.PLANNINGTERMCDFROM = 'P' + @ProductionMonth
				AND sm.RTEGRPCD = t.route_code AND sm.RUNSEQ = t.RATE AND sm.LOGPTCD LIKE '807%'
		GROUP BY t.SUPPLIER_NAME, t.route_code , t.RATE, t.lp_code, t.PICKUP_DT, t.Supplier, t.SupplierPlant, sm.ARRVDATETIME
	) tbl WHERE PickupMethod = 'DR'
	
	SET @SEQROW = (SELECT COUNT(1) FROM #temp2)
	
	INSERT INTO #temp2
	SELECT row_number() OVER (ORDER BY SUPPLIER_NAME) + @SEQROW AS SeqNbr, SUPPLIER_NAME, route_code , RATE, ARRVDATETIME,  DPTDATETIME, ARRVDATE, dock_code, 
		   lp_code, PICKUP_DT, PickupMethod, Supplier, SupplierPlant FROM (
		SELECT SUPPLIER_NAME, route_code , RATE, MIN(ARRVDATETIME) AS ARRVDATETIME, MAX(DPTDATETIME) AS DPTDATETIME, MIN(ARRVDATE) AS ARRVDATE, '' AS dock_code, lp_code, PICKUP_DT,
			CASE 
				WHEN lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 1) THEN 'DR' 
				WHEN lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 2) THEN 'LP'
				ELSE 'DR' 
			END AS PickupMethod, Supplier, SupplierPlant
		FROM #temp
		GROUP BY SUPPLIER_NAME, route_code , RATE, lp_code, PICKUP_DT, Supplier, SupplierPlant, ARRVDATE
	) tbl 
	WHERE PickupMethod = 'LP'
	
	DECLARE @Max INT,
			@i INT,
			@RouteCode VARCHAR(4),
			@RateCode VARCHAR(2),
			@DockCode VARCHAR(500),
			@Supplier VARCHAR(250),
			@DockCodeX VARCHAR(500),
			@CIndex INT,
			@Max3 INT,
			@i3 INT
	
	IF ((SELECT ISNULL(@SupplierCode, '')) <> '')
	BEGIN
	
		SELECT @Max = MAX(SeqNbr), @i = 1
		FROM #temp2
		
		WHILE (@i <= @Max)
		BEGIN
			
			SELECT @RouteCode = RouteCode, @RateCode = Rate, @DockCode = DockCode, @Supplier = Supplier + '-' + SupplierPlant
			FROM #temp2 WHERE SeqNbr = @i
			
			DELETE FROM #temp3
			
			INSERT INTO #temp3
			SELECT row_number() OVER (ORDER BY RTEGRPCD, RUNSEQ) AS SeqNbr, RCVCOMPDOCKCD
			FROM TB_T_SUPPLIER_MEETING_ORDER
			WHERE RTEGRPCD = @RouteCode AND RUNSEQ = @RateCode AND SUPPCD + '-' + SUPPPLANTCD = @Supplier
			
			SELECT @Max3 = MAX(SeqNbr), @i3 = 1
			FROM #temp3
			
			WHILE (@i3 <= @Max3)
			BEGIN
				SELECT @DockCodeX = DockCode FROM #temp2 WHERE SeqNbr = @i
				SELECT @DockCode = DockCode FROM #temp3 WHERE SeqNbr = @i3
				
				IF (@DockCodeX <> '')
				BEGIN
					SELECT @CIndex = CHARINDEX(@DockCode, @DockCodeX);
					
					IF (@CIndex = 0)
					BEGIN
						UPDATE #temp2 SET DockCode = DockCode + ', ' + @DockCode
							WHERE SeqNbr = @i
					END
				END ELSE BEGIN
					UPDATE #temp2 SET DockCode = @DockCode
						WHERE SeqNbr = @i
				END
				
				SET @i3 = @i3 + 1
				
			END
			
			SET @i = @i + 1
			
		END
			
	END ELSE BEGIN
	
		SELECT @Max = MAX(SeqNbr), @i = 1
		FROM #temp
			 
		WHILE (@i <= @Max)
		BEGIN
			SELECT @RouteCode = route_code, @RateCode = RATE, @DockCode = dock_code, @Supplier = SUPPLIER_NAME
			FROM #temp WHERE SeqNbr = @i
			
			SELECT @DockCodeX = DockCode FROM #temp2 WHERE RouteCode = @RouteCode AND Rate = @RateCode AND SupplierName = @Supplier
			
			IF (@DockCodeX <> '')
			BEGIN
				SELECT @CIndex = CHARINDEX(@DockCode, @DockCodeX);
				
				IF (@CIndex = 0)
				BEGIN
					UPDATE #temp2 SET DockCode = DockCode + ', ' + @DockCode
						WHERE RouteCode = @RouteCode AND Rate = @RateCode AND SupplierName = @Supplier
				END
			END ELSE BEGIN
				UPDATE #temp2 SET DockCode = @DockCode
					WHERE RouteCode = @RouteCode AND Rate = @RateCode AND SupplierName = @Supplier
			END
			
			SET @i = @i + 1
			
		END		 
		
	END
		 
	SELECT * FROM #temp2
END

