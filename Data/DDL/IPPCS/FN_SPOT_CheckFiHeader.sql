CREATE FUNCTION [dbo].[FN_SPOT_CheckFiHeader] (
    @p_DOC_NO AS VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	DECLARE
    	@d_OUT_LI AS INT,
        @d_LI AS INT;
        
  	EXEC @d_OUT_LI= dbo.SP_SPOT_ICS_CheckFIHeader @p_DOC_NO, @d_LI OUTPUT
    
    RETURN @d_OUT_LI;
END

