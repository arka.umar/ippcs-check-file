--exec MonthlyReport '2013-03-01','5022','2'

CREATE PROCEDURE [dbo].[SP_MonthlyReport] 
(      
     @PARAM_PICKUP_DT date,  
     @PARAM_SUPPLIER_CD  varchar(10), 
     @PARAM_SUPP_PLANT_CD varchar(10)
)  

AS

BEGIN



	SET NOCOUNT ON;
	
 --declare untuk cursor select table 
   --declare
   --@DELIVERY_NO varchar(13),
   --@SUPPLIER_CD varchar(10), 
   --@SUPPLIER_NAME varchar(40),
   --@DOCK_CD varchar(3),
   --@ROUTE varchar(4),
   --@RATE varchar(2),
   --@ARRVDATETIME datetime ,
   --@DPTDATETIME datetime,
   --@LP_CD varchar(4) ,
   --@CREATED_BY varchar(20),
   --@CREATED_DT datetime,
   --@CHANGED_BY varchar(20),
   --@CHANGED_DT datetime,
   --@SUPP_PLANT_CD  varchar(10),
   --@PICKUP_DT date;
   
   
   --DECLARE supplierdock_cursor CURSOR FOR
   
	select distinct 
	DELIVERY_NO,
	SUPPLIER_CD,
	SUPPLIER_NAME,
	DOCK_CD,
	ROUTE,
	RATE,
	ARRVDATETIME,
	DPTDATETIME,
	LP_CD,
	CREATED_BY,
	CREATED_DT,
	CHANGED_BY,
	CHANGED_DT,
	SUPP_PLANT_CD,
	PICKUP_DT
	from  TB_R_DELIVERY_CTL_SUPPLIERDOCK  with(nolock)
	where 
	 PICKUP_DT = @PARAM_PICKUP_DT  
     and SUPPLIER_CD = @PARAM_SUPPLIER_CD 
     and SUPP_PLANT_CD = @PARAM_SUPP_PLANT_CD
	order by route,rate asc


	--OPEN delivery_cursor
END

