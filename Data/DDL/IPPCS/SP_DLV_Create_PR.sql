/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Create PR
*/

CREATE PROCEDURE [dbo].[SP_DLV_Create_PR]

@PR_PO_MONTH VARCHAR (7),
@NOTES VARCHAR (MAX),
@CREATED_BY VARCHAR (25),
@pid AS BIGINT

AS
BEGIN


		DECLARE @MODUL_ID INT = 3, @FUNCTION_ID VARCHAR (6) =  '31103';

		DECLARE @err_message nvarchar(255);

		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_Create_PR'
		DECLARE @step AS VARCHAR(50) = 'Init'
		--DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)
		
		DECLARE  @CURRENT_NO VARCHAR (12),@PREFIX VARCHAR (12),@SEQ VARCHAR(2);


		

		DECLARE @STS_ERROR INT;


		
		DECLARE @PROD_MONTH VARCHAR(7),@LP_CD VARCHAR(3), @ROUTE_CD VARCHAR(4), @SOURCE VARCHAR (20), @PR_QTY INT;
		DECLARE @PRNO AS VARCHAR (MAX);
		DECLARE @CHECKS INT, @CHECK_PR_NO INT, @CHECK_PR_H INT;
		
		

		-- CREATE LOG
		SET @steps = @na+'.'+@step;
		SET @log = 'PR Creation process starting';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
		

		BEGIN TRY
				INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
				VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());
		END TRY
		BEGIN CATCH
				UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;
		END CATCH

		


				--==START==--

				DECLARE @PROD_MONTH_T varchar(6),@LP_CD_T varchar(4);

				DECLARE BTMP_CUR2 CURSOR
				FOR
				SELECT DISTINCT PROD_MONTH, LP_CD FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid;

				OPEN BTMP_CUR2;
				FETCH NEXT FROM BTMP_CUR2 INTO
				@PROD_MONTH_T,@LP_CD_T;

				
				SET @PRNO = '';

				WHILE @@FETCH_STATUS = 0
				BEGIN
							
							--==CREATE LOG==--
							SET @steps = @na+'.Process';
							SET @log = 'Checking PR_STATUS_FLAG with PROD_MONTH='+@PROD_MONTH_T+' AND LP_CD='+@LP_CD_T;
							EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
							--==END CREATE LOG==--

							--CHECK TABLE DLV_PR_H
							SET @CHECK_PR_H = (SELECT COUNT (*) FROM TB_R_DLV_PR_H WHERE PROD_MONTH =@PROD_MONTH_T AND LP_CD = @LP_CD_T
													 --AND PR_STATUS_FLAG IN ('PR2','PR3','PR4','PR5'));
													 AND PR_STATUS_FLAG NOT IN ('PR6'));

							--CHECK TABLE RETRIEVAL
							IF (@CHECK_PR_H > 0) BEGIN
																		
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
									WHERE PROCESS_ID = @pid;
									
									--==CREATE LOG==--
									SET @steps = @na+'.Process';
									SET @log = 'PR already created.';
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
									--==END CREATE LOG==--

									SET @err_message = 'PR already created, only PR with status retrieved and rejected can be created!';
									RAISERROR(@err_message,16,1)
									RETURN
									
							END

							ELSE BEGIN
											
									--==CREATE LOG==--
									SET @steps = @na+'.Process';
									SET @log = 'Checking PR_NO with PROD_MONTH='+@PROD_MONTH_T+' AND LP_CD='+@LP_CD_T;
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
									--==END CREATE LOG==--

									--==CREATE LOG==--
									SET @steps = @na+'.Process';
									SET @log = 'Generating PR_NO ';
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
									--==END CREATE LOG==--

									SET @CHECK_PR_NO = (SELECT COUNT (*) FROM TB_R_DLV_PR_H WHERE LP_CD = @LP_CD_T AND PROD_MONTH = @PROD_MONTH_T);

									IF (@CHECK_PR_NO <> 0) BEGIN
														
													SET @SEQ = (SELECT CONVERT (VARCHAR (11),SUBSTRING (MAX(PR_NO), 11, 2)+1) FROM TB_R_DLV_PR_H WHERE LP_CD = @LP_CD_T);
													SET @PREFIX = (SELECT CONVERT (VARCHAR (11),SUBSTRING(MAX(PR_NO), 0, 10)) FROM TB_R_DLV_PR_H WHERE LP_CD = @LP_CD_T);
																									
													IF ( (SELECT LEN(@SEQ)) = 1) BEGIN
																SET @SEQ = ('0'+@SEQ);
													END 
													ELSE BEGIN
																SET @SEQ = (@SEQ);
													END				
													
													SET @CURRENT_NO = @PREFIX+@SEQ;
																						
										END	
										ELSE BEGIN
												SET @CURRENT_NO =  RTRIM(@LP_CD_T)+@PROD_MONTH_T+'01';
										END
											
											BEGIN TRY
													--CREATE LOG
													SET @steps = @na+'.Process';
													SET @log = 'PR '+@CURRENT_NO+' has been processed.';

													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;

													--INSERT INTO TB_R_DLV_PR_
													INSERT INTO TB_R_DLV_PR_H (PR_NO, PROD_MONTH, LP_CD, PR_STATUS_FLAG, PR_NOTES, CREATED_BY, CREATED_DT,PROCESS_ID)
													--VALUES (@CURRENT_NO, @PR_PO_MONTH,@LP_CD, 'PR2',@NOTES, @CREATED_BY, GETDATE());
													SELECT DISTINCT @CURRENT_NO, PROD_MONTH, LP_CD, 'PR2',@NOTES, @CREATED_BY, GETDATE(), @pid
													FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH = @PROD_MONTH_T AND LP_CD = @LP_CD_T
													GROUP BY PROD_MONTH,LP_CD;

													--INSERT INTO TB_R_DLV_PR_D
													INSERT INTO TB_R_DLV_PR_D (PR_NO, PR_ITEM_NO, ROUTE_CD, PR_QTY,PO_FLAG, CREATED_BY, CREATED_DT)
													SELECT @CURRENT_NO,ROW_NUMBER() OVER(ORDER BY [LP_CD] ASC), ROUTE_CD, SUM (PR_QTY) AS PR_QTY, 1, @CREATED_BY, GETDATE() FROM TB_T_PR_RETRIEVAL
													--SELECT @CURRENT_NO, ROUTE_CD, SUM (PR_QTY) AS PR_QTY, 1, @CREATED_BY, GETDATE() FROM #RETRIEVAL_TEMP
													WHERE LP_CD=@LP_CD_T AND PROD_MONTH =@PROD_MONTH_T
													GROUP BY LP_CD, ROUTE_CD, PROD_MONTH;

													--==CREATE LOG UPDATE TABLE RETRIEVAL==--
													SET @steps = @na+'.Process';
													SET @log = 'Updating data TB_T_PR_RETRIEVAL with'+
																			'PROD_MONTH = '+@PROD_MONTH_T+', LP_CD ='+@LP_CD+', ROUTE_CD='+@ROUTE_CD+' AND SOURCE='+@SOURCE;
													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
													
													UPDATE TB_T_PR_RETRIEVAL SET PR_STATUS_FLAG = 'PR2', CHANGED_BY=@CREATED_BY, CHANGED_DT=GETDATE() 
													WHERE PROCESS_ID = @pid
													

													--CREATE LOG
													SET @steps = @na+'.Process';
													SET @log = 'PR '+@CURRENT_NO+' has been created.';
													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
													SET @PRNO +=@CURRENT_NO+',';

													SET @STS_ERROR = 0;

											END TRY
											BEGIN CATCH

													--CREATE LOG
													SET @steps = @na+'.Process';
													SET @log = 'Error when create PR '+@CURRENT_NO+'.';
													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
				
													UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
													WHERE PROCESS_ID = @pid;

													DELETE FROM TB_R_DLV_PR_D WHERE PR_NO IN (SELECT PR_NO FROM TB_R_DLV_PR_H WHERE PROCESS_ID = @pid);
													DELETE FROM TB_R_DLV_PR_H WHERE PROCESS_ID = @pid;
													

													SET @STS_ERROR = 1;

											END CATCH
											

							END

				FETCH NEXT FROM BTMP_CUR2 INTO
				@PROD_MONTH_T,@LP_CD_T
				END;
				CLOSE BTMP_CUR2;
				DEALLOCATE BTMP_CUR2;

				UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;

				

				--CREATE LOG
				SET @steps = @na+'.Process';
				SET @log = 'PR creation has been finished.';
			
				EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
						

				UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE()
				WHERE PROCESS_ID = @pid;



				IF (@STS_ERROR = 0) BEGIN
						--CREATE LOG
						SET @steps = @na+'.Process';
						SET @log = 'Starting sent e-mail.';

						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;

						--==FINISH==--
						
						UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;

						BEGIN TRY


								--===========SEND E-MAIL
								DECLARE @RecipientEmail varchar(max),
										@RecipientCopyEmail varchar(max),
										@RecipientBlindCopyEmail varchar(max),
										@ERR_FLAG varchar(50),
										@body varchar(max),
										@subjek varchar(100),
										@ERROR_DETAIL varchar(max),
										@profile varchar(max),
										@MAIL_QUERY VARCHAR(MAX)
										
								SET @profile = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE');
								if (@profile IS NULL) BEGIN
										SET @profile = 'NotificationAgent';
								END
								
								SELECT @RecipientEmail=EMAIL_TO, @RecipientCopyEmail=EMAIL_CC, @RecipientBlindCopyEmail=EMAIL_BCC 
								FROM TB_M_RTP_NOTIFICATION_EMAIL 
								WHERE MODULE_ID = CAST(@MODUL_ID AS VARCHAR) 
									AND FUNCTION_ID = @FUNCTION_ID AND ACTION = 'PR_CREATION'
								
								SELECT  @subjek=NOTIFICATION_SUBJECT,
										@body=NOTIFICATION_CONTENT
								FROM    TB_M_NOTIFICATION_CONTENT
								WHERE   FUNCTION_ID = @FUNCTION_ID
										AND ROLE_ID = '31'
										AND NOTIFICATION_METHOD = '1'
								
								SET @subjek = REPLACE(@subjek, '@ProdMonth', @PR_PO_MONTH)
								SET @body = REPLACE(@body, '@DateTime', (SELECT CAST(DAY(GETDATE()) AS VARCHAR) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR)))
								SET @body = REPLACE(@body, '@prno', @PRNO)
								
								--SET @subjek = 'Delivery Purchase Requestion process ' + @PROD_MONTH
								--SET @body = 'Dear User,' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) +
								--			'New purchase requistion has been created at ' + CAST (GETDATE() AS VARCHAR(50)) + ' for list of PR number below :'+
								--			'<br />'+
								--			@PRNO +
								--			'.<br />Please login IPPCS - PR approval screen to sign approval of PR number above <br />' +
								--			'at <a href="https://portal.toyota.co.id"> https://portal.toyota.co.id </a> <br>'+
								--			'Thank you.'+
								--			'<br /><br />'+
								--			'<b>Best Regards,</b>'+
								--			'<br /><b>IPPCS Admin </b>';
								
								IF (@RecipientCopyEmail IS NOT NULL)
									SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
									
								IF (@RecipientBlindCopyEmail IS NOT NULL)
									SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
								
								SET @body = ISNULL(@body, '')
								SET @subjek = ISNULL(@subjek, '')
								
								SET @RecipientEmail = ISNULL(@RecipientEmail, '')
								SET @RecipientCopyEmail = ISNULL(@RecipientCopyEmail, '')
								SET @RecipientBlindCopyEmail = ISNULL(@RecipientBlindCopyEmail, '')
								
								SET @MAIL_QUERY = '	
													EXEC msdb.dbo.sp_send_dbmail
														@profile_name	= ''' + @profile + ''',
														@body 			= ''' + @body + ''',
														@body_format 	= ''HTML'',
														@recipients 	= ''' + @RecipientEmail + ''',
														' + @RecipientCopyEmail + '
														' + @RecipientBlindCopyEmail + '
														@subject 		= ''' + @subjek + ''''
								
								EXEC (@MAIL_QUERY)
								
								SET @steps	= @na + '.Process';
								SET @log	= 'Email notification has been sent';

								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
								--===========END OF SEND E-MAIL

								UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE()
								WHERE PROCESS_ID = @pid;
								--SET @steps = @na+'.finished';
								--SET @log = 'Email notification has been sent to all PIC.';
								--INSERT @T
								--EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
							--COMMIT TRANSACTION

							--CREATE LOG
						SET @steps = @na+'.Finish';
						SET @log = 'send e-mail has been finished.';

						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;


						--==FINISH==--

						END TRY
						BEGIN CATCH

								--CREATE LOG
								SET @steps = @na+'.Finish';
								SET @log = 'Email notification failed sent to all PIC.';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;

								UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
								WHERE PROCESS_ID = @pid;


						END CATCH

				END

		

END

