CREATE PROCEDURE [dbo].[SP_GetGoodReceipt] 
	@ArrivalDateFrom datetime,
	@ArrivalDateTo datetime,
	@DockCode varchar(3),
	@SupplierCode varchar(6),
	@RcvPlantCode char(1),
	@Route char(4),
	@Rate char(2),
	@ManifestNo varchar(16),
	@Status char(1),
	@OrderNo varchar(12)
AS
BEGIN
	Declare @sql nvarchar(max)

	CREATE TABLE #tmpSplitStringDock
	( 
		column1 VARCHAR(100)
	)

	CREATE TABLE #tmpSplitStringSupplier
	( 
		column1 VARCHAR(100)
	)

	CREATE TABLE #tmpSplitStringStatus
	( 
		column1 VARCHAR(100)
	)

	INSERT  INTO #tmpSplitStringDock(column1)
	EXEC dbo.SplitString @DockCode, ''

	INSERT  INTO #tmpSplitStringSupplier(column1)
	EXEC dbo.SplitString @SupplierCode, ''

	INSERT  INTO #tmpSplitStringStatus(column1)
	EXEC dbo.SplitString @Status, ''

	set @sql = '
	select 
		m.MANIFEST_NO,
		m.ORDER_TYPE,
		m.SUPPLIER_CD,
		s.SUPPLIER_NAME,
		m.RCV_PLANT_CD,
		m.DOCK_CD,
		m.TOTAL_ITEM,
		m.TOTAL_QTY,
		(case when m.ARRIVAL_ACTUAL_DT is NULL then ''Not Yet Scan''
			when m.POSTING_DT is not null then ''Already Posted''
			when m.ERROR_QTY is NOT null then ''NG''
			--when ARRIVAL_ACTUAL_DT is NULL then ''Error To ICS''
			when m.ARRIVAL_ACTUAL_DT is NOT Null AND m.POSTING_DT is null then ''Ready To Post''
		end) Status,
		case when isnull(p.ORDER_QTY,0) > (isnull(p.RECEIVED_QTY,0) + isnull(p.SHORTAGE_QTY,0) + isnull(p.MISSPART_QTY,0) + isnull(p.DAMAGE_QTY,0)) then ''O'' else ''X'' end IssueDelay,
		case when (isnull(p.RECEIVED_QTY,0) + isnull(p.SHORTAGE_QTY,0) + isnull(p.MISSPART_QTY,0) + isnull(p.DAMAGE_QTY,0)) > 0 then ''O'' else ''X'' end IssueProblem,
		m.NOTIFY_ID,
		m.ORDER_NO,
		m.PO_NO,
		RouteRate = m.ROUTE + '' - '' + m.RATE,
		ARRIVAL_PLAN_DT = m.ARRIVAL_PLAN_DT,
		ARRIVAL_ACTUAL_DT = m.ARRIVAL_ACTUAL_DT,
		ScanDate = m.SCAN_DT,
		ApprovedDate = m.APPROVED_DT,
		m.MAT_DOC_NO,
		m.POSTING_DT,
		m.INVOICE_NO,
		m.INVOICE_DT
	from TB_R_DAILY_ORDER_MANIFEST m
	left join (
		select
			MANIFEST_NO,
			ORDER_QTY=sum(isnull(ORDER_QTY,0)),
			RECEIVED_QTY=sum(isnull(RECEIVED_QTY,0)),
			SHORTAGE_QTY=sum(isnull(SHORTAGE_QTY,0)),
			MISSPART_QTY=sum(isnull(MISSPART_QTY,0)),
			DAMAGE_QTY=sum(isnull(DAMAGE_QTY,0))
		from TB_R_DAILY_ORDER_PART 
		Group By MANIFEST_NO
	)p on m.MANIFEST_NO=p.MANIFEST_NO
	left join TB_M_SUPPLIER s on m.SUPPLIER_CD=s.SUPPLIER_CODE
	Where (convert(varchar(8),m.ARRIVAL_PLAN_DT,112) between ''' + convert(varchar(8),@ArrivalDateFrom,112) + ''' AND ''' + convert(varchar(8),@ArrivalDateTo,112) + ''') AND
		((m.RCV_PLANT_CD=''' + @RcvPlantCode + ''' AND isnull(''' + @RcvPlantCode + ''','''') <> '''') or (isnull(''' + @RcvPlantCode + ''','''') = '''')) and
		((m.ROUTE=''' + @Route + ''' AND isnull(''' + @Route + ''','''') <> '''') or (isnull(''' + @Route + ''','''') = '''')) and
		((m.RATE=''' + @Rate + ''' AND isnull(''' + @Rate + ''','''') <> '''') or (isnull(''' + @Rate + ''','''') = '''')) and
		((m.MANIFEST_NO=''' + @ManifestNo + ''' AND isnull(''' + @ManifestNo + ''','''') <> '''') or (isnull(''' + @ManifestNo + ''','''') = '''')) and
		((m.ORDER_NO=''' + @OrderNo + ''' AND isnull(''' + @OrderNo + ''','''') <> '''') or (isnull(''' + @OrderNo + ''','''') = ''''))
	'

	IF ( @DockCode <> '' ) 
		BEGIN
			SET @sql = @sql + ' AND (m.DOCK_CD IN (SELECT column1 FROM #tmpSplitStringDock))'
		END

	IF ( @SupplierCode <> '' ) 
		BEGIN
			SET @sql = @sql + ' AND (m.SUPPLIER_CD IN (SELECT column1 FROM #tmpSplitStringSupplier))'
		END
		
	IF ( @Status <> '' ) 
		BEGIN
			SET @sql = @sql + ' AND (
					(case when m.ARRIVAL_ACTUAL_DT is NULL then ''Not Yet Scan''
						when m.POSTING_DT is not null then ''Already Posted''
						when m.ERROR_QTY is NOT null then ''NG''
						--when ARRIVAL_ACTUAL_DT is NULL then ''Error To ICS''
						when m.ARRIVAL_ACTUAL_DT is NOT Null AND m.POSTING_DT is null then ''Ready To Post''
					end) IN (SELECT column1 FROM #tmpSplitStringStatus))'
		END

	execute (@sql)
        
END

