/****** Object:  StoredProcedure [dbo].[SP_CED_ValidateEmergencyOrderCreationSubmit]    Script Date: 9/28/2022 6:03:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2018-02-12
-- Description:	validation when submit emergency order
-- =============================================
ALTER PROC [dbo].[SP_CED_ValidateEmergencyOrderCreationSubmit] (
	@functionID VARCHAR(30)
	,@SessionID VARCHAR(30)
	,@userID VARCHAR(30)
	)
AS
BEGIN
	DECLARE @continue CHAR(1) = 'Y';
	DECLARE @items VARCHAR(MAX)

	SELECT @items = SYSTEM_VALUE
	FROM TB_M_SYSTEM
	WHERE FUNCTION_ID = 'DOCK_PACKING'
		AND SYSTEM_CD = 'EMERGENCY_ORDER'

	DELETE X
	FROM TB_T_CED_ERROR_UPLOAD X
	WHERE X.FUNCTION_ID = @functionID
		AND X.PROCESS_ID = @SessionID
		AND X.UserID = @userID

	IF (
			EXISTS (
				SELECT ''
				FROM TB_T_CED_EMERGENCY_ORDER_PART P
				WHERE P.SessionID = @SessionID
					AND P.UserID = @userID
				)
			)
	BEGIN
		--== VALIDATE DATA ==--
		INSERT INTO TB_T_CED_ERROR_UPLOAD (
			FUNCTION_ID
			,PROCESS_ID
			,FIELD
			,ERRORS_MESSAGE
			,UserID
			)
		SELECT --TOP 1  --** remove by FID.Andri (2022-09-28) -> handling error for every record
       @functionId
			,T.SessionID
			,'DOCK CODE'
			,'Data in Row [' + Convert(VARCHAR, t.ID) + ']' + ', Dock Code ' + ISNULL(T.DOCK_CD, '') + ' are not exist in allowed Emergency Order Dock Code. Allowed Dock Code are ' + ISNULL(@items, '')
			,@userID
		FROM TB_T_CED_EMERGENCY_ORDER_PART T
		WHERE ISNULL(T.DOCK_CD, '') <> ''
			AND T.SessionID = @SessionID
			AND T.UserID = @userID
			AND ISNULL(T.DOCK_CD, '') NOT IN (
				SELECT LTRIM(RTRIM(items))
				FROM dbo.FN_SPLIT(@items, ',')
				)

		--AND NOT EXISTS (
		--	SELECT ''
		--	FROM TB_T_ERROR_UPLOAD X
		--	WHERE X.FUNCTION_ID = @functionID
		--		AND X.PROCESS_ID = @SessionID
		--		AND X.UserID = @userID
		--	)

		INSERT INTO TB_T_CED_ERROR_UPLOAD (
			FUNCTION_ID
			,PROCESS_ID
			,FIELD
			,ERRORS_MESSAGE
			,UserID
			)
		SELECT --TOP 1 --** remove by FID.Andri (2022-09-28) -> handling error for every record
      @functionId
			,T.SessionID
			,'PART BARCODE'
			,'Data in Row [' + Convert(VARCHAR, t.ID) + ']' + ', Part Barcode ' + ISNULL(T.PART_BARCODE, '') + ' not found in Daily Order Part'
			,@userID
		FROM TB_T_CED_EMERGENCY_ORDER_PART T
		WHERE ISNULL(T.PART_BARCODE, '') <> ''
			AND T.SessionID = @SessionID
			AND T.UserID = @userID
			AND NOT EXISTS (
				SELECT DISTINCT B.PART_BARCODE
				FROM TB_R_DAILY_ORDER_MANIFEST A JOIN 
					TB_R_DAILY_ORDER_PART B ON A.MANIFEST_NO = B.MANIFEST_NO 
					WHERE T.PART_NO = B.PART_NO
					AND T.SUPPLIER_CD = A.SUPPLIER_CD
					AND T.SUPPLIER_PLANT = A.SUPPLIER_PLANT
					AND T.DOCK_CD = A.DOCK_CD
					AND T.PART_BARCODE = B.PART_BARCODE
				)


		IF (
				EXISTS (
					SELECT 1
					FROM TB_T_CED_ERROR_UPLOAD V
					WHERE V.PROCESS_ID = @SessionID
						AND V.FUNCTION_ID = @functionId
						AND V.UserID = @userID
					)
				)
		BEGIN
			SELECT 'false;' + CONVERT(VARCHAR, @SessionID) + ';' + CONVERT(VARCHAR, @functionID)
		END
		ELSE
		BEGIN
			SELECT 'true;'
		END
	END
	ELSE
	BEGIN
		SELECT 'Exception;No Data Found.';
	END
END
