
/****** Object:  StoredProcedure [dbo].[SP_INVOICE_SENDING_TO_ICS_FOR_FI_JOURNAL]    Script Date: 30/04/2021 17:08:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	19-Sept-2020
-- Description	:	Generate Invoice for FI Journal new ICS
-- Update	: 
-- =============================================
ALTER PROCEDURE [dbo].[SP_INVOICE_SENDING_TO_ICS_FOR_FI_JOURNAL] @USER_ID VARCHAR(20)
	,@PROCESS_ID BIGINT 
	,@SupplierInvoiceNo VARCHAR(MAX)
	,@SupplierCode VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IsError CHAR(1) = 'N'
	DECLARE @CURRDT DATE = CAST(GETDATE() AS DATE)
	DECLARE @FUNCTION_ID AS VARCHAR(MAX) = '53002'
		,@MODULE_ID AS VARCHAR(MAX) = '5'
		,@FUNTION_NM AS VARCHAR(MAX) = 'SP Invoice Sending To ICS for FI Journal'
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP Invoice Sending To ICS for FI Journal'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@query VARCHAR(MAX)
		,@ICSSrvrName NVARCHAR(256)
		,@ICSDBName NVARCHAR(256)

	DECLARE @R_SEQ_NO INT
	DECLARE @T TABLE (PID BIGINT);
	DECLARE @TB_T_LOG_D AS TABLE (
		PROCESS_ID BIGINT
		,SEQUENCE_NUMBER INT
		,MESSAGE_ID VARCHAR(12)
		,MESSAGE_TYPE VARCHAR(3)
		,[MESSAGE] VARCHAR(MAX)
		,LOCATION VARCHAR(MAX)
		,CREATED_BY VARCHAR(20)
		,CREATED_DATE DATETIME
		)
	DECLARE @RetStep1 INT = 0;
	DECLARE @RetStep2 INT = 0;
	DECLARE @RetStep3 INT = 0;

	SET NOCOUNT ON

	BEGIN TRY
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Invoice Sending To ICS for FI Journal'

		SET @LOG_LOCATION = 'Invoice Sending Init';

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()
			
		--* Get System Master *--
		SELECT @ICSSrvrName = SYSTEM_VALUE
		FROM [TB_M_SYSTEM]
		WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
			AND SYSTEM_CD = 'LINKED_SERVER';

		SELECT @ICSDBName = SYSTEM_VALUE
		FROM [TB_M_SYSTEM]
		WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
			AND SYSTEM_CD = 'DB_NAME';
			
		IF OBJECT_ID('tempdb..#TB_TEMP_MATERIAL_ICS_1') IS NOT NULL
		BEGIN
			DROP TABLE #TB_TEMP_MATERIAL_ICS_1
		END

		CREATE TABLE #TB_TEMP_MATERIAL_ICS_1 (
			[MAT_NO] [varchar](50) NULL
			,[MAT_DESC] [varchar](100) NULL
			)

		SET @query = ' INSERT INTO #TB_TEMP_MATERIAL_ICS_1
			SELECT * FROM OPENQUERY (' + @ICSSrvrName + ' , ''SELECT DISTINCT MAT_NO, MAT_DESC
			FROM ' + @ICSDBName + 'TB_M_MATERIAL
			WHERE ISNULL(DELETION_FLAG, ''''N'''') = ''''N'''''')';

		EXEC (@query);

		--* Invoice Validation *--
		EXEC @RetStep1 = [dbo].[SP_INVOICE_VALIDATION] @USER_ID
			,@PROCESS_ID
			,@SupplierInvoiceNo
			,@SupplierCode

		IF @RetStep1 <> 1
		BEGIN
			CREATE TABLE #cur_inv_header (
				ROWNO INT
				,EXCHANGE_RATE NUMERIC(13, 2)
				,INV_AMT NUMERIC(16, 2)
				,CONVERT_FLAG VARCHAR(1)
				,PAYMENT_CURR VARCHAR(3)
				,BALANCE NUMERIC(16, 2)
				,LIV_BASED VARCHAR(1)
				,PARTNER_BANK_KEY VARCHAR(10)
				,SUPP_CD VARCHAR(6)
				)

			INSERT INTO #cur_inv_header
			SELECT ROW_NUMBER() OVER (
					ORDER BY (
							SELECT 1
							)
					) NO
				,ISNULL(EXCHANGE_RATE, 0) EXCHANGE_RATE
				,ISNULL(INV_AMT, 0) INV_AMT
				,CONVERT_FLAG
				,PAYMENT_CURR
				,ISNULL(BALANCE, 0) BALANCE
				,LIV_BASED
				,PARTNER_BANK_KEY
				,SUPP_CD
			FROM TB_T_INVOICE_H
			WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
				AND PROCESS_ID = @PROCESS_ID;

			DECLARE @CnAllH INT;
			DECLARE @LoopH INT = 1;
			DECLARE @CnAllD INT;
			DECLARE @LoopD INT = 1;
			DECLARE @ItemNo INT = 1;
			DECLARE @EXCHANGE_RATE NUMERIC(13, 2) = 0;
			DECLARE @INV_AMT NUMERIC(16, 2);
			DECLARE @CONVERT_FLAG VARCHAR(1);
			DECLARE @PAYMENT_CURR VARCHAR(3);
			DECLARE @BALANCE NUMERIC(16, 2);
			DECLARE @LIV_BASED VARCHAR(1);
			DECLARE @PARTNER_BANK_KEY VARCHAR(10);
			DECLARE @SUPP_CD VARCHAR(6);
			DECLARE @INV_NO VARCHAR(10);
			DECLARE @AMOUNT NUMERIC(16, 2);
			DECLARE @ROWNO INT;
			DECLARE @l_v_inv_doc_item VARCHAR(5);
			DECLARE @l_n_amt_local_curr NUMERIC(16, 2);

			SELECT @CnAllH = COUNT(1)
			FROM #cur_inv_header

			WHILE @LoopH <= @CnAllH
			BEGIN
				SELECT @EXCHANGE_RATE = EXCHANGE_RATE
					,@INV_AMT = INV_AMT
					,@CONVERT_FLAG = CONVERT_FLAG
					,@PAYMENT_CURR = PAYMENT_CURR
					,@BALANCE = BALANCE
					,@LIV_BASED = LIV_BASED
					,@PARTNER_BANK_KEY = PARTNER_BANK_KEY
					,@SUPP_CD = SUPP_CD
				FROM #cur_inv_header
				WHERE ROWNO = @LoopH

				IF (@BALANCE <> 0)
				BEGIN
					/*TODO : Write Log MICS0200AERR*/
					SET @PARAM1 = 'Invoice amount is no balance';

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()

					BREAK;
				END
				ELSE
				BEGIN
					IF (ISNULL(@CONVERT_FLAG, '') = '')
					BEGIN
						UPDATE D
						SET D.INV_DOC_ITEM = T.INV_DOC_ITEM
							,D.INV_AMT_LOCAL_CURR = INV_AMT * @EXCHANGE_RATE
						FROM TB_T_INVOICE_D D
						JOIN (
							SELECT RIGHT('00000' + CONVERT(VARCHAR(5), (
											ROW_NUMBER() OVER (
												ORDER BY (
														SELECT 1
														)
												)
											)), 5) INV_DOC_ITEM
								,SUPP_INVOICE_NO
								,ROWNO
							FROM TB_T_INVOICE_D A
							WHERE A.PROCESS_ID = @PROCESS_ID
								AND A.SUPP_INVOICE_NO = @SupplierInvoiceNo
							) T ON D.SUPP_INVOICE_NO = T.SUPP_INVOICE_NO
								AND D.ROWNO = T.ROWNO
						WHERE D.PROCESS_ID = @PROCESS_ID
							AND D.SUPP_INVOICE_NO = @SupplierInvoiceNo;
						
						--* Invoice Update *--
						EXEC @RetStep2 = [dbo].[SP_INVOICE_UPDATE_INV_DOC_NO] @USER_ID
							,@PROCESS_ID
							,@SupplierInvoiceNo
							,@SupplierCode

						IF @RetStep2 <> 0
						BEGIN
							BREAK;
						END
						ELSE
						BEGIN
							--* Invoice Insert Process *--
							EXEC @RetStep3 = [dbo].[SP_INVOICE_SAVE_PROCESS_PCS] @USER_ID
								,@PROCESS_ID
								,@SupplierInvoiceNo
								,@SupplierCode
								,@LIV_BASED;

							IF @RetStep3 <> 0
							BEGIN
								BREAK;
							END
							ELSE
							BEGIN
								--* Invoice Sending *--
								SELECT @INV_NO = INV_NO
								FROM TB_T_INVOICE_H
								WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
									AND PROCESS_ID = @PROCESS_ID;

								EXEC [dbo].[SP_INVOICE_SEND_FI_JOURNAL] @USER_ID
									,@PROCESS_ID
									,@INV_NO
							END
						END
					END
				END

				SET @LoopH = @LoopH + 1;
			END

			DROP TABLE #cur_inv_header

			SELECT @INV_NO = INV_NO
			FROM TB_T_INVOICE_H
			WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
				AND PROCESS_ID = @PROCESS_ID;

			-- delete tb t
			DELETE FROM TB_T_INVOICE_H WHERE PROCESS_ID = @PROCESS_ID AND SUPP_INVOICE_NO = @SupplierInvoiceNo
			DELETE FROM TB_T_INVOICE_D WHERE PROCESS_ID = @PROCESS_ID AND SUPP_INVOICE_NO = @SupplierInvoiceNo
			DELETE FROM TB_T_INVOICE_GL_ACCOUNT WHERE PROCESS_ID = @PROCESS_ID AND SUPP_INVOICE_NO = @SupplierInvoiceNo
			DELETE FROM TB_T_INV_WITHOLDING_TAX WHERE PROCESS_ID = @PROCESS_ID AND SUPP_INVOICE_NO = @SupplierInvoiceNo
			DELETE FROM TB_T_INV_TAX WHERE PROCESS_ID = @PROCESS_ID AND SUPP_INVOICE_NO = @SupplierInvoiceNo
			
			-- Release Lock and Update TB_R_ICS_QUEUE
			UPDATE TB_R_ICS_QUEUE
			SET
				PROCESS_STATUS = 2,
				ICS_STATUS = 'ICS FINISHED', 
				CHANGED_BY = 'SYSTEM', 
				CHANGED_DT=GETDATE()
			WHERE
				IPPCS_MODUL = '5'
				AND IPPCS_FUNCTION = @FUNCTION_ID
				AND PROCESS_ID = @PROCESS_ID
				AND PROCESS_STATUS = '1'

			BEGIN TRY
				DECLARE @Email VARCHAR(1000)
					,@fullName VARCHAR(200)
					,@body VARCHAR(MAX)

				SELECT @Email = m.EMAIL
					,@fullName = m.FULL_NAME
				FROM TB_R_INV_H h
				INNER JOIN dbo.TB_R_VW_EMPLOYEE m ON h.POSTING_BY = m.USERNAME
				WHERE INV_NO = @INV_NO

				SET @body = 'Dear Mr./Mrs. ' + ISNULL(@fullName, '') + ',' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) + 'Please be informed that your Invoice with Supplier Invoice No ' + CONVERT(VARCHAR, @SupplierInvoiceNo) + ' is posted with Invoice Document No ' + CONVERT(VARCHAR, @INV_NO) + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) + 'This email is autogenerated by system, please do not reply' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) + 'Thank you for your attention '

				IF @Email <> ''
				BEGIN
					EXEC msdb.dbo.sp_send_dbmail @profile_name = 'IPPCS Notification'
						,@body = @body
						,@recipients = @Email
						,
						--@copy_recipients = 'supplierportal6@toyota.co.id;vandy.cahyadi@toyota.co.id',
						--@recipients = 'supplierportal8@toyota.co.id;vandy.cahyadi@toyota.co.id',
						@subject = '[IPPCS] Post Invoice Notification';
				END

			END TRY

			BEGIN CATCH
				SET @IsError = 'Y';

				SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

				EXEC dbo.CommonGetMessage 'MPCS00999ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MPCS00999ERR'
					,'ERR'
					,'MPCS00999ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()
			END CATCH

		END
		ELSE BEGIN
			-- ICS PROCESS ERROR
			UPDATE TB_R_INV_UPLOAD
			SET IN_PROGRESS = '3'
				,CHANGED_BY = 'AGENT'
				,CHANGED_DT = GETDATE()
			WHERE SUPP_INV_NO = @SupplierInvoiceNo
				AND SUPP_CD = @SupplierCode

			UPDATE gr
			SET gr.STATUS_CD = '-5'
				,gr.CHANGED_BY = 'AGENT'
				,gr.CHANGED_DT = GETDATE()
			FROM TB_R_INV_UPLOAD tu
			INNER JOIN TB_R_GR_IR gr ON gr.PO_NO = tu.PO_NO
				AND gr.PO_ITEM_NO = tu.PO_ITEM_NO
				AND gr.MAT_DOC_NO = tu.MAT_DOC_NO
				AND gr.ITEM_NO = tu.MAT_DOC_ITEM
				AND gr.COMP_PRICE_CD = tu.COMP_PRICE_CD
			WHERE tu.SUPP_INV_NO = @SupplierInvoiceNo
				AND tu.SUPP_CD = @SupplierCode

			UPDATE TB_R_ICS_QUEUE
			SET PROCESS_STATUS = 2
				,ICS_STATUS = 'PROCESS ERROR AT ICS'
				,CHANGED_BY = 'SYSTEM'
				,CHANGED_DT = GETDATE()
			WHERE PROCESS_ID = @PROCESS_ID

		END

		IF (@RetStep1 <> 0)
		BEGIN
			EXEC dbo.CommonGetMessage 'MPCS00001INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Sending To ICS for FI Journal is finished with error'

			SET @LOG_LOCATION = 'Invoice Sending Finish';

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00001INF'
				,'INF'
				,'MPCS00001INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			UPDATE TB_R_LOG_H
			SET PROCESS_STATUS = 4
			WHERE PROCESS_ID = @PROCESS_ID
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MPCS00001INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Sending To ICS for FI Journal is finished successfully'

			SET @LOG_LOCATION = 'Invoice Sending Finish';

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00001INF'
				,'INF'
				,'MPCS00001INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			UPDATE TB_R_LOG_H
			SET PROCESS_STATUS = 1
			WHERE PROCESS_ID = @PROCESS_ID
		END
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		RETURN 1;
			/*DECLARE @QueryLinkedServer VARCHAR(MAX)
		SET @QueryLinkedServer = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Invoice_h WHERE SUPP_INVOICE_NO=''' + LTRIM(@SupplierInvoiceNo) + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR,@L_N_PROCESS_ID) + ''''
		SET @QueryLinkedServer = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@QueryLinkedServer, '''', '''''') + ''')'
		EXEC (@QueryLinkedServer)
		
		SET @QueryLinkedServer = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Invoice_d WHERE SUPP_INVOICE_NO=''' + LTRIM(@SupplierInvoiceNo) + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR,@L_N_PROCESS_ID) + ''''
		SET @QueryLinkedServer = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@QueryLinkedServer, '''', '''''') + ''')'
		EXEC (@QueryLinkedServer)
		
		SET @QueryLinkedServer = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Inv_Gl_Account WHERE SUPP_INVOICE_NO=''' + LTRIM(@SupplierInvoiceNo) + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR,@L_N_PROCESS_ID) + ''''
		SET @QueryLinkedServer = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@QueryLinkedServer, '''', '''''') + ''')'
		EXEC (@QueryLinkedServer)
		
		SET @QueryLinkedServer = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Inv_Witholding_Tax WHERE SUPP_INVOICE_NO=''' + LTRIM(@SupplierInvoiceNo) + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR,@L_N_PROCESS_ID) + ''''
		SET @QueryLinkedServer = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@QueryLinkedServer, '''', '''''') + ''')'
		EXEC (@QueryLinkedServer)
		
		SET @QueryLinkedServer = 'SELECT OTHER_PROCESS_ID FROM TB_R_PCS_INTERFACE_STATUS WHERE OTHER_PROCESS_ID=''' + CONVERT(VARCHAR,@L_N_PROCESS_ID) + ''''
		SET @QueryLinkedServer = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@QueryLinkedServer, '''', '''''') + ''')'
		EXEC (@QueryLinkedServer)*/
			--DELETE FROM [TB_R_ICS_QUEUE] WHERE [PROCESS_ID]=@L_N_PROCESS_ID
	END CATCH
END
