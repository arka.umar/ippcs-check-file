CREATE FUNCTION [dbo].[FN_TC_GetAvailableAreaCd] (
	@ri_v_loc_cd varchar(8)
)
RETURNS varchar(8)
AS
BEGIN
	declare @l_v_area_cd varchar(8)

	SELECT top 1 
		@l_v_area_cd = ta.AREA_CD
	FROM TB_M_TC_TRUCK_AREA ta
	WHERE 1 = 1
		and ta.LOC_CD = @ri_v_loc_cd
		and ta.FILLED_FLAG = 0
		and ta.ACTIVE_FLAG = 1
	ORDER BY
		ta.AREA_SEQ ASC

	return @l_v_area_cd
END

