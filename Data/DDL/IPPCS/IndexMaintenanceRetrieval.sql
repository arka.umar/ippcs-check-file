CREATE PROCEDURE [dbo].[IndexMaintenanceRetrieval]
	@database_name NVARCHAR(100)
AS 
BEGIN
DECLARE @T_TABLE TABLE (
			table_seq int,
			table_name varchar(100)
		)
DECLARE @i INT = 1,
		@max_seq INT = 0,
		@table_name NVARCHAR(100)

INSERT INTO @T_TABLE 
SELECT 
	ROW_NUMBER() OVER(ORDER BY name ASC),
	name 
FROM sys.tables where name NOT LIKE '%_TABLE_%'

IF(EXISTS (SELECT '' FROM TB_M_TABLE_STATUS WHERE ISNULL(is_locked, '') = 'Y'))
BEGIN
	RETURN
END
ELSE
BEGIN
	TRUNCATE TABLE TB_M_TABLE_STATUS
END

SELECT @max_seq = MAX(table_seq) FROM @T_TABLE

WHILE (@i <= @max_seq)
BEGIN
	SELECT @table_name = table_name FROM @T_TABLE WHERE table_seq = @i
	EXEC IndexMaintenancePhysicalValidation @database_name, @table_name
	SET @i = @i + 1
END
END

