-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetPartType]
AS
BEGIN
	SELECT [PART_NO]
		,[DOCK_CD]
		,ISNULL(CONVERT(VARCHAR,[QTY_KANBAN]), '0') [QTY_KANBAN]
		,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
	FROM [dbo].[TB_T_CED_PART_TYPE]
END

