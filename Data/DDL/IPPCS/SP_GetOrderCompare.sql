-- =============================================
-- Author:		FID.Deny
-- Create date: 2015-09-10
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetOrderCompare] 
	@dateFrom varchar(15),
	@dateTo varchar(15),
	@type varchar(15)
AS
BEGIN
	SET NOCOUNT ON;

		declare @dateFromm varchar(15) = @dateFrom
		declare @dateToo varchar(15) = @dateTo
		declare @typee varchar(15) = @type


		SELECT k.PRODUCTION_DATE, k.ARRIVAL_SEQ, i.DOCK_CD, i.SUPPLIER_CD + '-' + i.SUPPLIER_PLANT AS SUPPLIER, i.PART_NO, i.IPPCS_ORDER_QTY, k.ORDER_QTY_KCI				
		FROM (                                					
				select a.ORDER_NO, 			
					a.DOCK_CD, 		
					a.SUPPLIER_CD,		
					a.SUPPLIER_PLANT,		
					b.PART_NO, 				
					SUM(b.ORDER_QTY) IPPCS_ORDER_QTY                             		
				from IPPCS_PROD.dbo.TB_R_DAILY_ORDER_MANIFEST a, 			
					 IPPCS_PROD.dbo.TB_R_DAILY_ORDER_PART b                                		
				where 
					--CAST(LEFT(a.ORDER_NO, 8) AS DATE) BETWEEN @dateFromm AND @dateToo	
					CASE WHEN a.ORDER_TYPE NOT IN ('3') THEN CAST(LEFT(a.ORDER_NO, 8) AS DATE) ELSE CAST('1993-01-01' as DATE) END BETWEEN @dateFromm AND @dateToo
				AND a.MANIFEST_NO = b.MANIFEST_NO                                			
				GROUP BY a.ORDER_NO, a.DOCK_CD, a.SUPPLIER_CD,a.SUPPLIER_PLANT, b.PART_NO, a.PROCESS_FLAG,a.CREATED_DT			
			 ) i                                				
		INNER JOIN (					
				SELECT PRODUCTION_DATE, ARRIVAL_SEQ, SUPP_CD, 			
					SUPP_PLANT, 		
					PART_NO, 		
					DOCK_CD, 		
					LEFT(CONVERT(VARCHAR, PRODUCTION_DATE, 112) +                                		
						CASE WHEN LEN(CONVERT(VARCHAR, ARRIVAL_SEQ)) = 1 THEN    	
							'0'+ CONVERT(VARCHAR, ARRIVAL_SEQ)
							ELSE CONVERT(VARCHAR, ARRIVAL_SEQ) END, 12) ORDER_NO, 
					SUM(ORDER_QTY) ORDER_QTY_KCI		
				FROM KCI_DB.dbo.TB_R_ORDER_MOD                                 			
				WHERE PRODUCTION_DATE BETWEEN @dateFromm AND @dateToo                                			
				GROUP BY SUPP_CD, SUPP_PLANT, PART_NO, DOCK_CD, PRODUCTION_DATE, ARRIVAL_SEQ			
			) k                                 				
		ON k.SUPP_CD = i.SUPPLIER_CD 					
		AND k.SUPP_PLANT = i.SUPPLIER_PLANT                                					
		AND k.PART_NO = i.PART_NO 					
		AND k.DOCK_CD = i.DOCK_CD 					
		AND k.ORDER_NO = i.ORDER_NO
		

		AND ((k.ORDER_QTY_KCI <> i.IPPCS_ORDER_QTY AND @typee = 'difference') OR
			 (k.ORDER_QTY_KCI = i.IPPCS_ORDER_QTY AND @typee = 'match') OR
			 (@typee = 'all')
			)

		ORDER BY K.PRODUCTION_DATE, K.ARRIVAL_SEQ
		



END

