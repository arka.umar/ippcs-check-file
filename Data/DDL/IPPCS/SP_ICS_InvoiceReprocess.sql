CREATE PROCEDURE [dbo].[SP_ICS_InvoiceReprocess] @SUPPLIER_INV VARCHAR(20), @SUPPLIER_CD VARCHAR(10)
AS
BEGIN
	DECLARE @@STATUS_CD VARCHAR(10) = '',
			@@CREATED_DT VARCHAR(20) = NULL,
			@@SUPP_INV_NO VARCHAR(20) = @SUPPLIER_INV,
			@@SUPP_CD VARCHAR(10) = @SUPPLIER_CD,
			@@uid VARCHAR(20) = 'System',
			@@loc VARCHAR(20) = 'Re-Process Invoice',
			@@processID BIGINT,
			@@function VARCHAR(10) = '53006',
			@@module VARCHAR(10) = '5',
			@@InvExist INT,
			@@oldProcessID BIGINT,
			@@ICSQuery VARCHAR(MAX),
			@@logMessage VARCHAR(MAX),
			@@Hours INT,
			@@TEXT VARCHAR(MAX),
			@@ROW INT,
			@@CERTIFICATE_ID VARCHAR(30),
			@@BASELINE_DT DATETIME,
			@@SUBMIT_BY VARCHAR(30),
			@@SUBMIT_DT DATETIME,
			@@REMARKS VARCHAR(MAX)

	DECLARE @@TB_T_LOG_D as TABLE(
			PROCESS_ID BIGINT, 
			SEQUENCE_NUMBER INT, 
			MESSAGE_ID VARCHAR(12), 
			MESSAGE_TYPE VARCHAR(3), 
			[MESSAGE] VARCHAR(MAX), 
			LOCATION VARCHAR(MAX), 
			CREATED_BY VARCHAR(20), 
			CREATED_DATE DATETIME
		)
	DECLARE @@T TABLE (PROCESS_ID BIGINT)

	--SELECT 
	--	@@processID = PROCESS_ID
	--FROM TB_R_INV_UPLOAD
	--WHERE SUPP_INV_NO = @@SUPP_INV_NO
	--	  AND SUPP_CD = @@SUPP_CD

	SELECT TOP 1 
		@@CREATED_DT = CREATED_DT 
	FROM TB_R_INV_UPLOAD 
	WHERE SUPP_INV_NO = @@SUPP_INV_NO 
		  AND SUPP_CD = @@SUPP_CD;

	SELECT @@Hours = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'MAX_PROCESS_TIME' AND FUNCTION_ID = '53006' --TB_M_SYSTEM
	
	IF((DATEDIFF(HOUR, @@CREATED_DT, GETDATE()) < @@Hours) AND (@@CREATED_DT IS NOT NULL))
	BEGIN
		SELECT 'Invoice Can be Re-Processed ' + CONVERT(VARCHAR(15), @@Hours) + ' Hours From Creation Time'
		RETURN;
	END
	ELSE IF (@@CREATED_DT IS NULL)
	BEGIN
		SELECT 'Created Date is Null, Invoice Cannot Be Cancelled'
		RETURN;
	END
	ELSE
	BEGIN
		INSERT INTO @@T
		EXEC sp_PutLog 
			'Rollback Invoice Started'
			, @@uid
			, @@loc
			, @@processID OUTPUT
			, 'MPCS00003INF'
			, 'INF' --MSG_TYPE
			, @@module --MODULE_ID
			, @@function
			, 0 --STATUS

		BEGIN TRY
			SELECT TOP 1
				@@STATUS_CD = MIN(STATUS_CD)
			FROM TB_R_INV_UPLOAD 
			WHERE SUPP_INV_NO = @@SUPP_INV_NO
				  AND SUPP_CD = @@SUPP_CD
			GROUP BY SUPP_INV_NO, SUPP_CD 
			--HAVING MIN(STATUS_CD) = 1

			--IF(@@STATUS_CD <> '1')
			IF(@@STATUS_CD NOT IN ('1', '-5'))
			BEGIN
				SELECT 'Only Invoice with Outstanding GR or Error Posting status can be ReProcessed'
				RETURN;
			END

			SET @@logMessage = 'Reprocess Invoice with Status ' + CASE @@STATUS_CD WHEN '1' THEN 'Outstanding GR' ELSE 'Error Posting' END
			INSERT INTO @@T
			EXEC sp_PutLog 
				@@logMessage
				, @@uid
				, @@loc
				, @@processID OUTPUT
				, 'MPCS00003INF'
				, 'INF' --MSG_TYPE
				, @@module --MODULE_ID
				, @@function
				, 0 --STATUS

			SELECT TOP 1
				@@CERTIFICATE_ID = CERTIFICATE_ID,
				@@BASELINE_DT = BASELINE_DT,
				@@SUBMIT_BY = SUBMIT_BY,
				@@SUBMIT_DT = SUBMIT_DT 
			FROM TB_R_INV_UPLOAD
			WHERE SUPP_INV_NO = @@SUPP_INV_NO
					AND SUPP_CD = @@SUPP_CD

			SET @@logMessage = 'Checking Invoice Data in TB_T_INV_UPLOAD for SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
			INSERT INTO @@T
			EXEC sp_PutLog 
					@@logMessage
				, @@uid
				, @@loc
				, @@processID
				, 'MPCS00003INF'
				, 'INF' --MSG_TYPE
				, @@module --MODULE_ID
				, @@function
				, 0 --STATUS

			SELECT 
				@@InvExist = COUNT(1),
				@@oldProcessID = PROCESS_ID
			FROM TB_T_INV_UPLOAD
			WHERE SUPP_INV_NO = @@SUPP_INV_NO
				  AND SUPP_CD = @@SUPP_CD
			GROUP BY PROCESS_ID

			IF(ISNULL(@@InvExist, 0) <= 0)
			BEGIN
				SET @@logMessage = 'Re-Insert Data Into TB_T_INV_UPLOAD for SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
						@@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS
			
				BEGIN TRY
					INSERT INTO TB_T_INV_UPLOAD (
							PROCESS_ID
							,ROW_NO
							,SUPP_CD
							,PO_NO
							,PO_ITEM_NO
							,MAT_NO_REF
							,MAT_NO
							,PROD_PURPOSE_CD
							,SOURCE_TYPE_CD
							,PACKING_TYPE
							,PART_COLOR_SFX
							,MAT_TEXT
							,MAT_DOC_NO
							,MAT_DOC_ITEM
							,COMP_PRICE_CD
							,INV_REF_NO
							,DOC_DT
							,QTY
							,ORDER_NO
							,DOCK_CD
							,SUPP_INV_NO
							,S_QTY
							,PRICE_AMT
							,PAY_CURR
							,AMT
							,VALIDATION_TYPE
							,INV_DT
							,INV_TAX_NO
							,INV_TAX_DT
							,TURN_OVER
							,INV_TAX_AMT
							,TOTAL_MANIFEST
							,RETRO_DOC_NO
							,STAMP_FLAG
							,INV_TOTAL_AMT
							,REMARKS
							,PLANT_CD
							,SLOC_CD
							,UOM_CD
							,IN_PROGRESS
							,CREATED_DT
							,CREATED_BY
							)
					SELECT 
						@@processID
						,ROW_NO
						,SUPP_CD
						,PO_NO
						,PO_ITEM_NO
						,MAT_NO_REF
						,MAT_NO
						,PROD_PURPOSE_CD
						,SOURCE_TYPE_CD
						,PACKING_TYPE
						,PART_COLOR_SFX
						,MAT_TEXT
						,MAT_DOC_NO
						,MAT_DOC_ITEM
						,COMP_PRICE_CD
						,INV_REF_NO
						,DOC_DT
						,QTY
						,ORDER_NO
						,DOCK_CD
						,SUPP_INV_NO
						,S_QTY
						,PRICE_AMT
						,PAY_CURR
						,AMT
						,VALIDATION_TYPE
						,INV_DT
						,INV_TAX_NO
						,INV_TAX_DT
						,TURN_OVER
						,INV_TAX_AMOUNT
						,TOTAL_MANIFEST
						,RETRO_DOC_NO
						,STAMP_FLAG
						,INV_AMT_TOTAL
						,NULL
						,PLANT_CD
						,SLOC_CD
						,UOM_CD
						,IN_PROGRESS
						,CREATED_DT
						,CREATED_BY
					FROM TB_R_INV_UPLOAD
					WHERE SUPP_INV_NO = @@SUPP_INV_NO
						  AND SUPP_CD = @@SUPP_CD
					--WHERE SUPP_INV_NO + ';' + SUPP_CD + ';' + cast(STATUS_CD AS VARCHAR(10)) IN (
					--		SELECT SUPP_INV_NO + ';' + SUPP_CD + ';' + cast(STATUS_CD AS VARCHAR(10))
					--		FROM (
					--			SELECT DISTINCT SUPP_INV_NO
					--				,SUPP_CD
					--				,STATUS_CD
					--			FROM TB_R_INV_UPLOAD
					--			WHERE SUPP_INV_NO = @@SUPP_INV_NO
					--			) a
					--		)

					SELECT @@ROW = @@ROWCOUNT
				END TRY
				BEGIN CATCH
					SET @@logMessage = 'Error While Re-Insert Data Into TB_T_INV_UPLOAD : ' + ERROR_MESSAGE()
					RAISERROR(@@logMessage, 16, 1)
				END CATCH
			END
			ELSE
			BEGIN
				SET @@logMessage = 'Invoice Data Still Exists in TB_T_INV_UPLOAD for SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS

				UPDATE TB_T_INV_UPLOAD 
					SET PROCESS_ID = @@processID
				WHERE SUPP_INV_NO = @@SUPP_INV_NO
				  AND SUPP_CD = @@SUPP_CD

				SELECT @@ROW = @@ROWCOUNT

				SET @@logMessage = 'Update Existing Data in TB_T_INV_UPLOAD with Process ID ' + 
									   CONVERT(VARCHAR(30), ISNULL(@@oldProcessID, 0)) + 
									   ' to Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@processID, 0))
					INSERT INTO @@T
					EXEC sp_PutLog 
						  @@logMessage
						, @@uid
						, @@loc
						, @@processID
						, 'MPCS00003INF'
						, 'INF' --MSG_TYPE
						, @@module --MODULE_ID
						, @@function
						, 0 --STATUS
			END

			SET @@logMessage = 'Checking Invoice Data in TB_T_INV_UPLOAD_FILE with Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@oldProcessID, 0))
			INSERT INTO @@T
			EXEC sp_PutLog 
				@@logMessage
				, @@uid
				, @@loc
				, @@processID
				, 'MPCS00003INF'
				, 'INF' --MSG_TYPE
				, @@module --MODULE_ID
				, @@function
				, 0 --STATUS

			SELECT 
				@@InvExist = COUNT(1) 
			FROM TB_T_INV_UPLOAD_FILE
			WHERE PROCESS_ID = ISNULL(@@oldProcessID, 0)

			IF(ISNULL(@@ROW, 0) > 0)
			BEGIN
				IF(ISNULL(@@InvExist, 0) <= 0)
				BEGIN
					SET @@logMessage = 'Re-Insert Into TB_T_INV_UPLOAD_FILE with Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@processID, 0))
					INSERT INTO @@T
					EXEC sp_PutLog 
						  @@logMessage
						, @@uid
						, @@loc
						, @@processID
						, 'MPCS00003INF'
						, 'INF' --MSG_TYPE
						, @@module --MODULE_ID
						, @@function
						, 0 --STATUS

					BEGIN TRY
						INSERT INTO TB_T_INV_UPLOAD_FILE
							(
								PROCESS_ID,
								ICS_PROCESS_ID,
								SUPP_CD,
								USERNAME,
								[FILENAME],
								FILESIZE,
								BODY,
								CREATED_BY,
								CREATED_DT,
								CHANGED_BY,
								CHANGED_DT
							)	
						SELECT DISTINCT 
							@@processID, 
							NULL, 
							SUPP_CD, 
							CREATED_BY, 
							CONVERT(VARCHAR(12),PROCESS_ID)+'.csv', 
							0, 
							NULL, 
							CREATED_BY, 
							GETDATE(), 
							NULL,
							NULL
						FROM TB_T_INV_UPLOAD
						WHERE PROCESS_ID = @@processID

						SELECT @@ROW = @@ROWCOUNT
					END TRY
					BEGIN CATCH
						SET @@logMessage = 'Error While Re-Insert Data Into TB_T_INV_UPLOAD_FILE : ' + ERROR_MESSAGE()
						RAISERROR(@@logMessage, 16, 1)
					END CATCH
				END
				ELSE
				BEGIN
					SET @@logMessage = 'Invoice Data Still Exists in TB_T_INV_UPLOAD_FILE with Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@oldProcessID, 0))
					INSERT INTO @@T
					EXEC sp_PutLog 
						  @@logMessage
						, @@uid
						, @@loc
						, @@processID
						, 'MPCS00003INF'
						, 'INF' --MSG_TYPE
						, @@module --MODULE_ID
						, @@function
						, 0 --STATUS

					UPDATE TB_T_INV_UPLOAD_FILE
						SET PROCESS_ID = @@processID
					WHERE PROCESS_ID = @@oldProcessID

					SELECT @@ROW = @@ROWCOUNT

					SET @@logMessage = 'Update Existing Data in TB_T_INV_UPLOAD_FILE with Process ID ' + 
									   CONVERT(VARCHAR(30), ISNULL(@@oldProcessID, 0)) + 
									   ' to Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@processID, 0))
					INSERT INTO @@T
					EXEC sp_PutLog 
						  @@logMessage
						, @@uid
						, @@loc
						, @@processID
						, 'MPCS00003INF'
						, 'INF' --MSG_TYPE
						, @@module --MODULE_ID
						, @@function
						, 0 --STATUS
				END
			END
			ELSE
			BEGIN
				RAISERROR('No Rows Inserted/Updated in TB_T_INV_UPLOAD', 16, 1)
			END
			
			IF(ISNULL(@@ROW, 0) <= 0)
			BEGIN
				RAISERROR('No Rows Inserted/Updated in TB_T_INV_UPLOAD_FILE', 16, 1)
			END

			--Rollback Process
			--DELETE
			BEGIN TRY
				SET @@logMessage = 'Delete Data From ICS.TB_T_INVOICE_UPLOAD with SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS
			
				SET @@ICSQuery = 'DELETE
						OPENQUERY([IPPCS_TO_ICS], ''SELECT * FROM TB_T_INVOICE_UPLOAD WHERE SUPP_INV_NO = ''''' + @@SUPP_INV_NO + ''''' AND SUPP_CD = ''''' + @@SUPP_CD + ''''''')'
				EXEC (@@ICSQuery)
			END TRY
			BEGIN CATCH
				SET @@logMessage = 'Error While Delete ICS.TB_T_INVOICE_UPLOAD : ' + ERROR_MESSAGE()
				RAISERROR(@@logMessage, 16, 1)
			END CATCH
		
			BEGIN TRY
				SET @@logMessage = 'Delete Data From ICS.TB_R_INVOICE_UPLOAD with SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS
			
				SET @@ICSQuery = 'DELETE
						OPENQUERY([IPPCS_TO_ICS], ''SELECT * FROM TB_R_INVOICE_UPLOAD WHERE SUPP_INV_NO = ''''' + @@SUPP_INV_NO + ''''' AND SUPP_CD = ''''' + @@SUPP_CD + ''''''')'
				EXEC (@@ICSQuery)
			END TRY
			BEGIN CATCH
				SET @@logMessage = 'Error While Delete ICS.TB_R_INVOICE_UPLOAD : ' + ERROR_MESSAGE()
				RAISERROR(@@logMessage, 16, 1)
			END CATCH

			BEGIN TRY
				SET @@logMessage = 'Delete Data From ICS.TB_T_INVOICE_H with SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS
			
				SET @@ICSQuery = 'DELETE
						OPENQUERY([IPPCS_TO_ICS], ''SELECT * FROM TB_T_INVOICE_H WHERE SUPP_INVOICE_NO = ''''' + @@SUPP_INV_NO + ''''' AND SUPP_CD = ''''' + @@SUPP_CD + ''''''')'
				EXEC (@@ICSQuery)
			END TRY
			BEGIN CATCH
				SET @@logMessage = 'Error While Delete ICS.TB_T_INVOICE_H : ' + ERROR_MESSAGE()
				RAISERROR(@@logMessage, 16, 1)
			END CATCH

			BEGIN TRY
				SET @@logMessage = 'Delete Data From ICS.TB_R_INVOICE_H with SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS
			
				SET @@ICSQuery = 'DELETE 
						OPENQUERY([IPPCS_TO_ICS], ''SELECT * FROM TB_R_INVOICE_H WHERE SUPP_INV_NO = ''''' + @@SUPP_INV_NO + ''''' AND SUPP_CD = ''''' + @@SUPP_CD + ''''''')'
				EXEC (@@ICSQuery)
			END TRY
			BEGIN CATCH
				SET @@logMessage = 'Error While Delete ICS.TB_R_INVOICE_H : ' + ERROR_MESSAGE()
				RAISERROR(@@logMessage, 16, 1)
			END CATCH

			BEGIN TRY
				SET @@logMessage = 'Delete Data From IPPCS.TB_R_INV_UPLOAD with SUPP_INV_NO ' + @@SUPP_INV_NO  + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS
			
				DELETE FROM TB_R_INV_UPLOAD WHERE SUPP_INV_NO = @@SUPP_INV_NO AND SUPP_CD = @@SUPP_CD
			END TRY
			BEGIN CATCH
				SET @@logMessage = 'Error While Delete IPPCS.TB_R_INV_UPLOAD : ' + ERROR_MESSAGE()
				RAISERROR(@@logMessage, 16, 1)
			END CATCH

			BEGIN TRY
				SET @@logMessage = 'Update GR IR Status for SUPP_INV_NO ' + @@SUPP_INV_NO + ' And SUPP_CD ' + @@SUPP_CD
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS

				UPDATE TB_R_GR_IR 
					SET STATUS_CD = '1'
				WHERE INV_REF_NO IN (SELECT INV_REF_NO 
									 FROM TB_T_INV_UPLOAD 
									 WHERE SUPP_INV_NO = @@SUPP_INV_NO
										   AND SUPP_CD = @@SUPP_CD)

				SELECT @@TEXT = COALESCE(@@TEXT + ',', '') + CAST(INV_REF_NO AS VARCHAR)
				FROM TB_R_INV_UPLOAD 
				WHERE SUPP_INV_NO = @@SUPP_INV_NO
					AND SUPP_CD = @@SUPP_CD

				SET @@logMessage = 'Reprocess Data for SUPP_INV_NO ' + @@SUPP_INV_NO + ' with Manifest No. ' + ISNULL(@@TEXT, '')
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS

			END TRY
			BEGIN CATCH
				SET @@logMessage = 'Error While Update Status in GR IR for SUPP_INV_NO ' + @@SUPP_INV_NO
				RAISERROR(@@logMessage, 16, 1)
			END CATCH

			BEGIN TRY
				SET @@logMessage = 'Update Data From IPPCS.TB_R_ICS_QUEUE with Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@oldProcessID, 0))
				INSERT INTO @@T
				EXEC sp_PutLog 
					  @@logMessage
					, @@uid
					, @@loc
					, @@processID
					, 'MPCS00003INF'
					, 'INF' --MSG_TYPE
					, @@module --MODULE_ID
					, @@function
					, 0 --STATUS

				--DELETE FROM TB_R_ICS_QUEUE WHERE PROCESS_ID = @@oldProcessID
				UPDATE TB_R_ICS_QUEUE
					SET PROCESS_STATUS = 5,
						ICS_STATUS = 'PROCESS FINISH WITH ERROR'
				WHERE PROCESS_ID = @@oldProcessID
			END TRY
			BEGIN CATCH
				SET @@logMessage = 'Error While Delete IPPCS.TB_R_ICS_QUEUE : ' + ERROR_MESSAGE()
				RAISERROR(@@logMessage, 16, 1)
			END CATCH

			SET @@REMARKS = @@SUPP_CD + ';' + 
							@@SUPP_INV_NO + ';' + 
							@@CERTIFICATE_ID + ';' +
							CONVERT(VARCHAR(30), @@BASELINE_DT, 21) + ';' + 
							@@SUBMIT_BY + ';' + 
							CONVERT(VARCHAR(30), @@SUBMIT_DT, 21) + ';' +
							@@STATUS_CD

			--Exec Creation Job
			INSERT INTO @@T
			EXEC sp_ICS_InvoiceCreationJobStart @@processID, @@REMARKS

			SELECT 'Success Perform Re-Process Invoice with Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@processID, 0))

		END TRY
		BEGIN CATCH
			SET @@logMessage = 'Error while perform Re-Process Invoice : ' + ERROR_MESSAGE()
			INSERT INTO @@T
			EXEC sp_PutLog 
					@@logMessage
				, @@uid
				, @@loc
				, @@processID
				, 'MPCS00003ERR'
				, 'ERR' --MSG_TYPE
				, @@module --MODULE_ID
				, @@function
				, 1 --STATUS
	
			SELECT 'Error While Perform Re-Process Invoice, Please see Log Monitoring with Process ID ' + CONVERT(VARCHAR(30), ISNULL(@@processID, 0))
		END CATCH
	END
END

