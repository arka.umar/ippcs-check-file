CREATE PROCEDURE [dbo].[KeihenMail]
	@format varchar(20) = 'APPROVAL',
	@pack_month varchar(8),
	@userid varchar(20),
	@process_id bigint,
	@end_datetime datetime = NULL,
	@sendmailstatus VARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @p_userid VARCHAR(20) = @userid
	DECLARE @p_packmonth varchar(6) = @pack_month
	DECLARE @p_error_msg VARCHAR(max) = ''
	DECLARE @APPROVED_DT datetime
	DECLARE @APPROVED_BY varchar(20) = ''
	DECLARE @SUPPLIER TABLE (
		ROW_NO INT,
		SUPP_CD VARCHAR(10),
		SUPP_PLANT VARCHAR(5),
		SUPP_NAME VARCHAR(max),
		SUPP_EMAIL VARCHAR(max)
	)

	DECLARE @MAIL_PROFILE VARCHAR(50) = (SELECT TOP 1 SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE')
	DECLARE @recipientCode VARCHAR(4) = 'cc01'
	DECLARE @PCDCode VARCHAR(4) = '9999'
	DECLARE @PCDRole VARCHAR(2) = '99'
	DECLARE @SuppRole VARCHAR(2) = '35'
	DECLARE @ApprovalCode VARCHAR(6) = '99997'
	DECLARE @ReminderCode VARCHAR(6) = '99996'
	DECLARE @NotificationMethod VARCHAR(2) = '1'

	DECLARE @ccEmail VARCHAR(MAX) = ''
	DECLARE @toEmail VARCHAR(MAX) = ''
	DECLARE @supp_plant VARCHAR(5) = ''
	DECLARE @supp_code VARCHAR(10) = ''
	DECLARE @mailSubjectSupp VARCHAR(MAX) = ''
	DECLARE @mailSubjectPCD VARCHAR(MAX) = ''
	DECLARE @mailBodySupp VARCHAR(MAX) = ''
	DECLARE @mailBodyPCD VARCHAR(MAX) = ''
	DECLARE @p_prodmonth_char VARCHAR(20) = REPLACE(CONVERT(VARCHAR(20), CONVERT(DATE, @p_packmonth + '01'), 13), '01 ', '')
	DECLARE @p_prodmonth_date DATE = CONVERT(DATE, @p_packmonth + '01')

	DECLARE @log varchar(max) = ''
	DECLARE @T TABLE (pid bigint)
	SET @sendmailstatus = 'OK'

	BEGIN TRY
		SET @log = 'Initial Send Mail Reminder';
		INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id OUTPUT, 'MPCS00008INF', 'INF', '9', '99997', 0;

		INSERT INTO @SUPPLIER (
			ROW_NO,
			SUPP_CD,
			SUPP_PLANT,
			SUPP_EMAIL,
			SUPP_NAME
		)
		SELECT 
			ROW_NUMBER() OVER(ORDER BY SUPPLIER_CD),
			SUPPLIER_CD,
			S_PLANT_CD,
			EMAIL,
			NAME
		FROM (
			SELECT DISTINCT
				s.SUPPLIER_CD,
				s.S_PLANT_CD,
				STUFF(
				(select distinct ';' + C.EMAIL
					from TB_M_SUPPLIER_EMAIL C
					where C.SUPPLIER_CD = s.SUPPLIER_CD
				for xml path('')), 1, 1, '') as EMAIL,
				b.NAME
			FROM TB_R_KEIHEN_SUMMARY s
				INNER JOIN TB_M_SUPPLIER_EMAIL b 
				ON b.SUPPLIER_CD = s.SUPPLIER_CD
			WHERE s.PACK_MONTH = @p_packmonth 
					AND s.N_1_ORIGINAL <> s.N_1_NEW
			UNION ALL
			SELECT DISTINCT
				b.SUPPLIER_CD,
				'0',
				STUFF(
				(select distinct ';' + C.EMAIL
					from TB_M_SUPPLIER_EMAIL C
					where C.SUPPLIER_CD = b.SUPPLIER_CD
				for xml path('')), 1, 1, '') as EMAIL,
				'PCD'
			FROM TB_M_SUPPLIER_EMAIL b
			WHERE b.SUPPLIER_CD = @PCDCode
		)TBL1
			
		--SELECT * FROM @SUPPLIER

		SELECT TOP 1
			@ccEmail = EMAIL
		FROM (
			SELECT DISTINCT STUFF(
					(select distinct ';' + C.EMAIL_ADDRESS
						from TB_T_EMAIL_RECIPIENT C
						where C.RECIPIENT_CD = E1.RECIPIENT_CD
					for xml path('')), 1, 1, '') as EMAIL
			FROM TB_T_EMAIL_RECIPIENT E1
				WHERE E1.RECIPIENT_CD = @recipientCode
		) TBL1

		SET @log = 'CC Email : ' + ISNULL(@ccEmail, '');
		INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008INF', 'INF', '9', '99997', 0;

		IF(@format = 'APPROVAL')
		BEGIN
			SELECT TOP 1  
				@mailSubjectSupp = NOTIFICATION_SUBJECT,
				@mailBodySupp = REPLACE(NOTIFICATION_CONTENT, '@pack_month', REPLACE(CONVERT(VARCHAR(20), @p_prodmonth_date , 13), '01 ', ''))
			FROM    TB_M_NOTIFICATION_CONTENT
			WHERE   FUNCTION_ID = @ApprovalCode
					AND ROLE_ID = @SuppRole
					AND NOTIFICATION_METHOD = @NotificationMethod

			SELECT TOP 1
				@mailSubjectPCD = NOTIFICATION_SUBJECT,
				@mailBodyPCD = REPLACE(NOTIFICATION_CONTENT, '@pack_month', REPLACE(CONVERT(VARCHAR(20), @p_prodmonth_date, 13), '01 ', ''))
			FROM TB_M_NOTIFICATION_CONTENT
			WHERE FUNCTION_ID = @ApprovalCode
					AND ROLE_ID = @PCDRole 
					AND NOTIFICATION_METHOD = @NotificationMethod
		END
		ELSE
		BEGIN
			SELECT TOP 1  
				@mailSubjectSupp = NOTIFICATION_SUBJECT,
				@mailBodySupp = REPLACE(NOTIFICATION_CONTENT, '@pack_month', REPLACE(CONVERT(VARCHAR(20), @p_prodmonth_date, 13), '01 ', ''))
			FROM    TB_M_NOTIFICATION_CONTENT
			WHERE   FUNCTION_ID = @ReminderCode
					AND ROLE_ID = @SuppRole
					AND NOTIFICATION_METHOD = @NotificationMethod
			SET @mailSubjectSupp = REPLACE(@mailSubjectSupp, '@FullMonth', DATENAME(MONTH, @p_prodmonth_date) + ' ' + DATENAME(YEAR, @p_prodmonth_date))
			SET @mailBodySupp = REPLACE(@mailBodySupp, '@end_datetime', CONVERT(VARCHAR(50), ISNULL(@END_DATETIME, 'Undefined'), 13))

			SELECT TOP 1
				@mailSubjectPCD = NOTIFICATION_SUBJECT,
				@mailBodyPCD = REPLACE(NOTIFICATION_CONTENT, '@pack_month', REPLACE(CONVERT(VARCHAR(20), @p_prodmonth_date, 13), '01 ', ''))
			FROM TB_M_NOTIFICATION_CONTENT
			WHERE FUNCTION_ID = @ReminderCode 
					AND ROLE_ID = @PCDRole
					AND NOTIFICATION_METHOD = @NotificationMethod
			SET @mailSubjectPCD = REPLACE(@mailSubjectPCD, '@FullMonth', DATENAME(MONTH, @p_prodmonth_date) + ' ' + DATENAME(YEAR, @p_prodmonth_date))
			SET @mailBodyPCD = REPLACE(@mailBodyPCD, '@end_datetime', CONVERT(VARCHAR(50), ISNULL(@END_DATETIME, 'Undefined'), 13))
		END

		DECLARE @index INT = 1
		DECLARE @max INT = 0
		DECLARE @Body VARCHAR(MAX) = ''

		SELECT @max = ROW_NO FROM @SUPPLIER
		WHILE(@index <= @max)
		BEGIN
			SET @Body = ''
			SET @Body = @mailBodySupp

			SELECT TOP 1
				@supp_code = SUPP_CD,
				@supp_plant = SUPP_PLANT,
				@toEmail = SUPP_EMAIL
			FROM @SUPPLIER
			WHERE ROW_NO = @index

			IF(EXISTS(SELECT 'x' FROM TB_R_KEIHEN_FEEDBACK 
					  WHERE SUPPLIER_CD = @supp_code 
						    AND S_PLANT_CD = @supp_plant 
							AND ISNULL(FEEDBACK_BY, '') = '' 
							AND ISNULL(FEEDBACK_DT, '') = ''))
			BEGIN
				BEGIN TRY
					IF(ISNULL(@toEmail, '')!='')
					BEGIN
						--SELECT @supp_code, @PCDCode
						IF(@supp_code <> @PCDCode)
						BEGIN
							SET @Body = REPLACE(@Body, '@suppcode', ISNULL(@supp_code, ''))
							SET @Body = REPLACE(@Body, '@suppplant', ISNULL(@supp_plant, ''))

							EXEC msdb.dbo.sp_send_dbmail
								@profile_name = @MAIL_PROFILE,
								@body = @Body,
								@body_format = 'HTML',
								@recipients = @toEmail,
								@copy_recipients = @ccEmail,
								@subject = @mailSubjectSupp

							SET @log = 'Mail Send to Supplier ' + ISNULL(@supp_code, '') + ' Supplier Plant ' + ISNULL(@supp_plant, '') ;
							INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008INF', 'INF', '9', '99997', 0;

							SET @log = 'Email : ' + ISNULL(@toEmail, '');
							INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008INF', 'INF', '9', '99997', 0;
						END
					END
					ELSE
					BEGIN
						SET @log = 'Mail Not Send to Supplier ' + ISNULL(@supp_code, '') + ' Supplier Plant ' + ISNULL(@supp_plant, '') + '. Email is Empty' ;
						INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008INF', 'INF', '9', '99997', 0;

						SET @sendmailstatus = 'NG'
					END
				END TRY
				BEGIN CATCH
					SET @log = 'Mail Not Send to Supplier ' + ISNULL(@supp_code, '') + ' Supplier Plant ' + ISNULL(@supp_plant, '') + '. ' + ERROR_MESSAGE() ;
					INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008INF', 'INF', '9', '99997', 0;

					SET @sendmailstatus = 'NG'
				END CATCH
			END
			ELSE
			BEGIN
				EXEC msdb.dbo.sp_send_dbmail
					@profile_name = @MAIL_PROFILE,
					@body = @mailBodyPCD,
					@body_format = 'HTML',
					@recipients = @toEmail,
					@copy_recipients = @ccEmail,
					@subject = @mailSubjectPCD
								
				SET @log = 'Mail Send to PCD : ' + ISNULL(@toEmail, '') ;
				INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008INF', 'INF', '9', '99997', 0;
			END
			SET @index = @index + 1
		END

		IF(@sendmailstatus = 'NG')
		BEGIN
			IF(@format = 'APPROVAL')
				SET @log = '(WARNING) Approval Success But Some Email Cannot be Send. Please Check Log with Process ID ' + ISNULL(CONVERT(VARCHAR(MAX), @process_id), '')
			ELSE
				SET @log = '(WARNING) Some Email Cannot be Send' + ISNULL(CONVERT(VARCHAR(MAX), @process_id), '')

			INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008ERR', 'ERR', '9', '99997', 0;
			
			SET @sendmailstatus = @log
		END
		ELSE
		BEGIN
			SET @log = 'Success Send Email Remainder'
			INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008INF', 'INF', '9', '99997', 0;
			
			SET @sendmailstatus = 'SUCCESS'
		END
	END TRY
	BEGIN CATCH
		SET @log = ERROR_MESSAGE()
		INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Reminder', @process_id, 'MPCS00008ERR', 'ERR', '9', '99997', 0;
		
		IF(@format = 'APPROVAL')
			SET @sendmailstatus = '(WARNING) Approval Success But Some Email Cannot be Send : ' + ISNULL(@log, '')
		ELSE
			SET @sendmailstatus = '(WARNING) Some Email Cannot be Send : ' + ISNULL(@log, '')
	END CATCH
END

