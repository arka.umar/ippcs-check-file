/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Create PO
*/

CREATE PROCEDURE [dbo].[SP_DLV_CREATE_PO]
@PR_NO varchar(12), 
@USER varchar(30), 
@PROD_MONTH varchar(11),
@pid BIGINT
AS
BEGIN
	--==== DECLARE MODULE AND FUNCTION
	DECLARE @MODUL_ID INT, @FUNCTION_ID VARCHAR (6); 
	SET @MODUL_ID = 3;
    SET @FUNCTION_ID = '31202';
	--END ---------------------------
	
	EXEC dbo.sp_PutLog 'PO Creation process started', @USER, 'SP_DLV_Create_PO.start', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;			
	-- ======= UPDATE DLV_QUEUE ==============================
	UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid
	-- ======= END UPDATE DLV QUEUE ==========================
	
	--====GENERATE PO NUMBER-------
	DECLARE @LAST_PO VARCHAR(MAX),@LP_CD VARCHAR(MAX),@SCHEMA_CD VARCHAR(MAX)
	
	SET @LP_CD = (SELECT SUBSTRING(PR_NO,1,3) AS LP_CD FROM TB_R_DLV_PR_H WHERE PR_NO = @PR_NO);
	SET @SCHEMA_CD = (SELECT CALCULATION_SCHEMA_CD FROM TB_M_TRUCKING_SCHEME WHERE LP_CD = @LP_CD AND VALID_TO >= GETDATE());
	
	SET @LAST_PO = dbo.FN_GetPONumber(@LP_CD,@PROD_MONTH)
	IF @LAST_PO = '-1' BEGIN
		RAISERROR('Logistic Partner Code not found',16,1)
	END
	ELSE BEGIN
	
	
		--CREATE LOG--------------------------
		DECLARE @log3 AS VARCHAR(MAX)
		SET @log3 = 'PO '+@LAST_PO+' has been Created.';
		EXEC dbo.sp_PutLog @log3, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
		--END LOG ============================
		
		DECLARE @log1001 AS VARCHAR(MAX)
		SET @log1001 = 'Insert '+@LAST_PO+' to PO Header';
		EXEC dbo.sp_PutLog @log1001, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
		
		--INSERT PO HEADER ==================================================================================
		INSERT INTO TB_R_DLV_PO_H(PO_NO,PROD_MONTH,LP_CD
		,PO_STATUS_FLAG
		,PO_AMOUNT
		,CALCULATION_SCHEME_CD
		,DOC_TYPE
		,PO_EXCHANGE_RATE
		,PO_CURR
		,PAYMENT_METHOD_CD
		,PAYMENT_TERM_CD
		,CREATED_BY
		,CREATED_DT
		,PROCESS_ID
		)
		VALUES (
		@LAST_PO
		,@PROD_MONTH
		,@LP_CD
		,'PO1'
		,NULL
		,@SCHEMA_CD
		,(SELECT DOC_TYPE FROM TB_M_FI_POSTING_RULE_H WHERE TRANS_TYPE_CD = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DEL_TRANS_TYPE' AND FUNCTION_ID = '31405'))
		,'1'
		,'IDR'
		,(SELECT PAYMENT_METHOD_CD FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = @LP_CD)
		,(SELECT PAYMENT_TERM_CD FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = @LP_CD)
		,@USER
		,GETDATE()
		,@pid
		)
		--END INSERT PO HEADER ==================================================================================
		
		
		
		--LOG ROUTE WITH NO PRICE
		DECLARE @PRICE_PR_NO VARCHAR(MAX),@PRICE_ROUTE_CD VARCHAR(MAX),@PRICE_PRICE NUMERIC(15,2)
		
		DECLARE BTMP_CUR_PRICE CURSOR FOR
		SELECT	HEADER_PR.PR_NO,DETAIL_PR.ROUTE_CD,(CASE WHEN ISNULL(PRICE.PRICE,0)=0 THEN 0 ELSE PRICE.PRICE END) AS PRICE
		FROM 
		TB_R_DLV_PR_H AS HEADER_PR LEFT JOIN TB_R_DLV_PR_D AS DETAIL_PR ON HEADER_PR.PR_NO = DETAIL_PR.PR_NO
		LEFT JOIN TB_M_ROUTE_PRICE AS PRICE ON PRICE.ROUTE_CD = DETAIL_PR.ROUTE_CD AND HEADER_PR.LP_CD = PRICE.LP_CD AND (GETDATE() BETWEEN PRICE.VALID_FROM AND PRICE.VALID_TO)
        WHERE (HEADER_PR.PR_NO = @PR_NO) 

		OPEN BTMP_CUR_PRICE;
		FETCH NEXT FROM BTMP_CUR_PRICE INTO @PRICE_PR_NO,@PRICE_ROUTE_CD,@PRICE_PRICE;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (ISNULL(@PRICE_PRICE,0)=0) BEGIN
			DECLARE @logprice AS VARCHAR(MAX)
			SET @logprice = 'Route ' + @PRICE_ROUTE_CD +' does not have a price';
			EXEC dbo.sp_PutLog @logprice, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
			END 
						
		FETCH NEXT FROM BTMP_CUR_PRICE INTO @PRICE_PR_NO,@PRICE_ROUTE_CD,@PRICE_PRICE;
		END;
		CLOSE BTMP_CUR_PRICE;
		DEALLOCATE BTMP_CUR_PRICE;
		--END LOG ROUTE WITH NO PRICE
						
		
		--BEGIN CALCULATION SCHEME ==========================================================================
				DECLARE @PR_ID VARCHAR(12),@ROUTE_CD VARCHAR(10),@PR_ITEM VARCHAR(10),@PR_QTY INT,@PO_ITEM INT
				DECLARE @FINAL_PRICE NUMERIC(15,2) = 0;
				DECLARE @AMOUNT_PRICE NUMERIC(15,2) = 0;
				SET @PO_ITEM = 0
				
				DECLARE C_PR CURSOR FOR
					SELECT DETAIL.PR_NO,DETAIL.ROUTE_CD,DETAIL.PR_ITEM_NO,DETAIL.PR_QTY FROM TB_R_DLV_PR_H HEADER 
					INNER JOIN TB_R_DLV_PR_D DETAIL ON DETAIL.PR_NO = HEADER.PR_NO
					INNER JOIN TB_M_ROUTE_PRICE PRICE ON PRICE.LP_CD = HEADER.LP_CD
					WHERE HEADER.PR_NO = @PR_NO AND DETAIL.ROUTE_CD = PRICE.ROUTE_CD
					AND (GETDATE() BETWEEN PRICE.VALID_FROM AND PRICE.VALID_TO)
					ORDER BY DETAIL.PR_ITEM_NO
				OPEN C_PR
				
				FETCH NEXT FROM C_PR INTO @PR_ID,@ROUTE_CD,@PR_ITEM,@PR_QTY
				WHILE @@FETCH_STATUS = 0
				BEGIN
				
				SET @PO_ITEM = @PO_ITEM + 1
				
						--INSERT PO_ITEM====================================================================================================
						DECLARE @log1002 AS VARCHAR(MAX)
						SET @log1002 = 'Insert PO Item ' + CAST(@PO_ITEM AS VARCHAR(10)) +' to Table PO Item with Route ' + @ROUTE_CD;
						EXEC dbo.sp_PutLog @log1002, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
		
						INSERT INTO TB_R_DLV_PO_ITEM(PO_NO,PO_ITEM_NO,ROUTE_CD,PO_QTY,SA_QTY,OPEN_QTY,PR_NO,PR_ITEM_NO,CREATED_BY,CREATED_DT)
					    VALUES(@LAST_PO,@PO_ITEM,@ROUTE_CD,@PR_QTY,0,@PR_QTY,@PR_NO,@PR_ITEM,@USER,GETDATE());
						--END INSERT PO DETAIL =============================================================================================
						
						DECLARE @CONDITION_CATEGORY VARCHAR(1),@COMP_PRICE_CD VARCHAR(4),@CALCULATION_TYPE VARCHAR(1),@BASE_VALUE_TO VARCHAR(1),
								@BASE_VALUE_FROM VARCHAR(1),@PLUS_MINUS_FLAG VARCHAR(1), @RTP_TAX_CD VARCHAR(2)
						
						DECLARE C_CALCULATE CURSOR FOR
							--SELECT CONDITION_CATEGORY,COMP_PRICE_CD,CALCULATION_TYPE,BASE_VALUE_TO,BASE_VALUE_FROM,PLUS_MINUS_FLAG FROM TB_M_CALC_SCHEME WHERE CALCULATION_SCHEMA_CD = @SCHEMA_CD ORDER BY SEQ_NO
							SELECT C.CONDITION_CATEGORY, C.COMP_PRICE_CD, C.CALCULATION_TYPE, C.BASE_VALUE_TO, C.BASE_VALUE_FROM, C.PLUS_MINUS_FLAG, 
								CASE WHEN C.CONDITION_CATEGORY = 'H' THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'RTP_BASE_TAX_CD') ELSE R.TAX_CD END AS TAX_CD
							FROM TB_M_CALC_SCHEME C
								LEFT JOIN TB_M_DLV_COMP_PRICE_RATE R ON R.COMP_PRICE_CD = C.COMP_PRICE_CD
							WHERE CALCULATION_SCHEMA_CD = @SCHEMA_CD ORDER BY SEQ_NO
						OPEN C_CALCULATE
						FETCH NEXT FROM C_CALCULATE INTO @CONDITION_CATEGORY,@COMP_PRICE_CD,@CALCULATION_TYPE,@BASE_VALUE_TO,@BASE_VALUE_FROM,@PLUS_MINUS_FLAG,@RTP_TAX_CD
						WHILE @@FETCH_STATUS = 0
						BEGIN
						
								DECLARE @ROUTE_PRICE INT,@BASE_PRICE BIGINT
								DECLARE @PRICE NUMERIC(15,2)
								SET @ROUTE_PRICE = 0
								SELECT @ROUTE_PRICE = PRICE FROM TB_M_ROUTE_PRICE WHERE LP_CD = @LP_CD AND ROUTE_CD = @ROUTE_CD AND (GETDATE() BETWEEN VALID_FROM AND VALID_TO)
								
								--CHECK CONDITION CATEGORY
									IF(@CONDITION_CATEGORY = 'H')BEGIN
										SET @BASE_PRICE = @ROUTE_PRICE
										SET @PRICE = @BASE_PRICE;
									END
									ELSE BEGIN
									-- GET DATA FROM TB_M_DLV_COMP_PRICE_RATE
										DECLARE @COMP_PRICE NUMERIC(15,2)
										SET @COMP_PRICE = (SELECT COMP_PRICE FROM TB_M_DLV_COMP_PRICE_RATE WHERE COMP_PRICE_CD = @COMP_PRICE_CD);
										--PRINT 'COMP PRICE : ' + CONVERT(VARCHAR(100),@COMP_PRICE);
										--PRINT 'CALC TYPE : ' + CONVERT(VARCHAR(100),@CALCULATION_TYPE);
										
										IF (@CALCULATION_TYPE = '3') BEGIN
											
											
											IF (ISNULL(@BASE_VALUE_TO,'') = '') BEGIN
												SET @PRICE = 0
												SET @PRICE = (@COMP_PRICE/100) * @BASE_PRICE;
												--PRINT 'PRICE PRECENTAGE: ' + CONVERT(VARCHAR(100),@PRICE);
											END
											ELSE BEGIN
												SET @PRICE = 0
											---------------------------------
												DECLARE @PERSEN2 NUMERIC(15,2)
												--IF (ISNULL(@BASE_VALUE_TO,'') = '') BEGIN
												--SET @PERSEN2 =(SELECT SUM(pr.COMP_PRICE/100) * SUM(@BASE_PRICE)
												--			   FROM TB_M_CALC_SCHEME cs 
												--			   JOIN TB_M_DLV_COMP_PRICE_RATE pr
												--			   ON cs.COMP_PRICE_CD=pr.COMP_PRICE_CD
												--			   WHERE --cs.SEQ_NO >=@BASE_VALUE_FROM AND cs.SEQ_NO<= @BASE_VALUE_TO AND 
												--			   cs.CALCULATION_SCHEMA_CD = @SCHEMA_CD);
												--END
												--ELSE BEGIN
												--SET @PERSEN2 =(SELECT SUM((CASE WHEN ISNULL(pr.COMP_PRICE,0) = 0 THEN 0 ELSE pr.COMP_PRICE END)/100) * SUM(@BASE_PRICE)
												--			   FROM TB_M_CALC_SCHEME cs 
												--			   LEFT JOIN TB_M_DLV_COMP_PRICE_RATE pr
												--			   ON cs.COMP_PRICE_CD=pr.COMP_PRICE_CD
												--			   WHERE cs.SEQ_NO >=@BASE_VALUE_FROM AND cs.SEQ_NO<= @BASE_VALUE_TO AND 
												--			   cs.CALCULATION_SCHEMA_CD = @SCHEMA_CD);
												--END
												
												SET @PERSEN2 = (SELECT SUM((pod.PRICE * (CASE WHEN cs.PLUS_MINUS_FLAG = '1' THEN 1 ELSE -1 END)))
																FROM TB_R_DLV_PO_D pod
																	INNER JOIN TB_M_CALC_SCHEME cs ON cs.SEQ_NO >= @BASE_VALUE_FROM AND cs.SEQ_NO <= @BASE_VALUE_TO AND cs.CALCULATION_SCHEMA_CD = @SCHEMA_CD 
																		AND cs.COMP_PRICE_CD = pod.COMP_PRICE_CD
																WHERE pod.PO_NO = @LAST_PO AND pod.PO_ITEM_NO IN (
																	SELECT PO_ITEM_NO FROM TB_R_DLV_PO_ITEM WHERE PO_NO = @LAST_PO AND ROUTE_CD = @ROUTE_CD
																));
															   
												--SET @PRICE = @PERSEN2 + @BASE_PRICE;
												
												--KALIKAN DENGAN PERSENNYA DIA SENDIRI
												--SET @PRICE = (@COMP_PRICE/100) * @PRICE;
												SET @PRICE = (@COMP_PRICE/100) * @PERSEN2;
												--PRINT 'PRICE PRECENTAGE: ' + CONVERT(VARCHAR(100),@PRICE);
												
											----------------------
											END
											
											--PRINT 'PLUS MINUS: ' + CONVERT(VARCHAR(100),@PLUS_MINUS_FLAG);
											
											IF(@PLUS_MINUS_FLAG = '1') -- INI DARI UCUP
											BEGIN
												SET @FINAL_PRICE = @FINAL_PRICE + @PRICE
											END
											ELSE
											BEGIN
												SET @FINAL_PRICE = @FINAL_PRICE - @PRICE
											END
											
											SET @AMOUNT_PRICE = @BASE_PRICE + @FINAL_PRICE
											
											
											----INSERT INTO PO_ITEM ------
											--UPDATE TB_R_DLV_PO_ITEM SET PRICE = ROUND(@AMOUNT_PRICE,2) WHERE PO_NO = @LAST_PO AND PO_ITEM_NO = @PO_ITEM
											UPDATE TB_R_DLV_PO_ITEM SET PRICE = ROUND(@BASE_PRICE,2) WHERE PO_NO = @LAST_PO AND PO_ITEM_NO = @PO_ITEM
											
										END
											
										ELSE BEGIN
										SET @PRICE = @BASE_PRICE;
										END
											
									END
									
									
									
				
									-- UPDATE INTO PR_D -------
									UPDATE TB_R_DLV_PR_D SET PO_NO = @LAST_PO,PO_FLAG ='Y',CHANGED_BY = @USER, CHANGED_DT = GETDATE() WHERE PR_NO = @PR_NO AND PR_ITEM_NO = @PR_ITEM
												
												
									-- INSERT INTO TABLE PO_D
									DECLARE @SET_PRICE NUMERIC(15,2)
									IF(@PLUS_MINUS_FLAG = '1')
									BEGIN
										SET @SET_PRICE = @PRICE
									END
									ELSE BEGIN
										SET @SET_PRICE = @PRICE * (-1)
									END
												
									
									--INSERT PO D=======================================================================================================
									DECLARE @log1003 AS VARCHAR(MAX)
									SET @log1003 = 'Insert ' + @COMP_PRICE_CD + ' to PO Detail';
									EXEC dbo.sp_PutLog @log1003, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
					
					
									INSERT INTO TB_R_DLV_PO_D(PO_NO,PO_ITEM_NO,COMP_PRICE_CD,BASE_VALUE_FROM,BASE_VALUE_TO,CALCULATION_TYPE,PLUS_MINUS_FLAG,ACCRUAL_POSTING_FLAG,CONDITION_CATEGORY,PRICE,PR_NO,PR_ITEM_NO,TAX_CD,PROCESS_ID,CREATED_BY,CREATED_DT)
									VALUES(@LAST_PO,@PO_ITEM,@COMP_PRICE_CD,@BASE_VALUE_FROM,@BASE_VALUE_TO,@CALCULATION_TYPE,@PLUS_MINUS_FLAG,'Y',@CONDITION_CATEGORY,@SET_PRICE,@PR_NO,@PR_ITEM,
										@RTP_TAX_CD,@pid,@USER,GETDATE())
									-- END INSERT
									
			
								
						FETCH NEXT FROM C_CALCULATE INTO @CONDITION_CATEGORY,@COMP_PRICE_CD,@CALCULATION_TYPE,@BASE_VALUE_TO,@BASE_VALUE_FROM,@PLUS_MINUS_FLAG, @RTP_TAX_CD
						END
						CLOSE C_CALCULATE
						DEALLOCATE C_CALCULATE
						
						--INI BUAT MENGOSONGKAN LAGI PRICE AKHIR
						SET @FINAL_PRICE = 0
						
						--END INSERT PO D ====================================================================================================
				
				FETCH NEXT FROM C_PR INTO @PR_ID,@ROUTE_CD,@PR_ITEM,@PR_QTY
				END
				CLOSE C_PR
				DEALLOCATE C_PR
				
		--END CALCULATION SCHEME ==========================================================================				
		
		--UPDATE PO AMOUNT ================================================================================
		DECLARE @PO_CREATE_FINAL VARCHAR(12), @PO_AMOUNT NUMERIC(15,2)
		
		SET @PO_CREATE_FINAL = (SELECT DISTINCT PO_NO FROM TB_R_DLV_PO_ITEM WHERE PR_NO = @PR_NO AND PO_NO = @LAST_PO);
		SET @PO_AMOUNT = (SELECT SUM(PO_QTY * PRICE) FROM TB_R_DLV_PO_ITEM WHERE PO_NO =@LAST_PO);
		UPDATE TB_R_DLV_PO_H SET PO_AMOUNT = @PO_AMOUNT WHERE PO_NO = @LAST_PO
		
		--CHECK DATA PO DETAIL, PO ITEM, PO HEADER
		EXEC dbo.sp_PutLog 'Checking PO Data Has Been Created Or Not In Table Header, Item, And Detail.', @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
		DECLARE @PO_HEADER_CHECK INT,@PO_ITEM_CHECK INT,@PO_DETAIL_CHECK INT
		SET @PO_HEADER_CHECK = (SELECT COUNT(*) FROM TB_R_DLV_PO_H WHERE PO_NO = @LAST_PO);
		SET @PO_ITEM_CHECK = (SELECT COUNT(*) FROM TB_R_DLV_PO_ITEM WHERE PO_NO = @LAST_PO);
		SET @PO_DETAIL_CHECK = (SELECT COUNT(*) FROM TB_R_DLV_PO_D WHERE PO_NO = @LAST_PO);
		
		IF (@PO_HEADER_CHECK = 0 OR @PO_ITEM_CHECK = 0 OR @PO_DETAIL_CHECK =0) BEGIN
		RAISERROR('PO Data Not Created, Header,Item, And Detail Null.',16,1)
		END
		ELSE BEGIN
							
							--LOG
							DECLARE @log1004 AS VARCHAR(MAX)
							SET @log1004 = 'Create PO '+@LAST_PO+' has been finished';
							EXEC dbo.sp_PutLog @log1004, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
							--END LOG
										
							
							--SEND MAIL ========================================================================================
							EXEC dbo.sp_PutLog 'Prepare To Send Mail', @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
								
							------SEND EMAIL TO PIC =============================================================================
							BEGIN TRY
								--===========SEND E-MAIL
								DECLARE @RecipientEmail varchar(max),
										@RecipientCopyEmail varchar(max),
										@RecipientBlindCopyEmail varchar(max),
										@ERR_FLAG varchar(50),
										@body varchar(max),
										@subjek varchar(100),
										@ERROR_DETAIL varchar(max),
										@profile varchar(max),
										@MAIL_QUERY VARCHAR(MAX)
								
								SET @profile = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE');
								if (@profile IS NULL) BEGIN
										SET @profile = 'NotificationAgent';
								END
								
								SELECT @RecipientEmail=EMAIL_TO, @RecipientCopyEmail=EMAIL_CC, @RecipientBlindCopyEmail=EMAIL_BCC 
								FROM TB_M_RTP_NOTIFICATION_EMAIL 
								WHERE MODULE_ID = CAST(@MODUL_ID AS VARCHAR) 
									AND FUNCTION_ID = @FUNCTION_ID AND ACTION = 'PO_CREATION'
								
								SELECT  @subjek=NOTIFICATION_SUBJECT,
										@body=NOTIFICATION_CONTENT
								FROM    TB_M_NOTIFICATION_CONTENT
								WHERE   FUNCTION_ID = @FUNCTION_ID
										AND ROLE_ID = '31'
										AND NOTIFICATION_METHOD = '1'
								
								SET @subjek = REPLACE(@subjek, '@ProdMonth', @PROD_MONTH)
								SET @body = REPLACE(@body, '@DateTime', (SELECT CAST(DAY(GETDATE()) AS VARCHAR) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR)))
								SET @body = REPLACE(@body, '@pono', @LAST_PO)
							
								IF (@RecipientCopyEmail IS NOT NULL)
									SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
									
								IF (@RecipientCopyEmail IS NOT NULL)
									SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
								
								SET @MAIL_QUERY = '	
													EXEC msdb.dbo.sp_send_dbmail
														@profile_name	= ''' + @profile + ''',
														@body 			= ''' + @body + ''',
														@body_format 	= ''HTML'',
														@recipients 	= ''' + @RecipientEmail + ''',
														' + @RecipientCopyEmail + '
														' + @RecipientBlindCopyEmail + '
														@subject 		= ''' + @subjek + ''''
								EXEC (@MAIL_QUERY)
								
							EXEC dbo.sp_PutLog 'Email notification has been sent', @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;

							END TRY			
							BEGIN CATCH
							DECLARE @logemail2 VARCHAR(MAX)
							SET @logemail2 = 'Email notification failed sent to all PIC';
							EXEC dbo.sp_PutLog @logemail2, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
							
							END CATCH
										
							------END EMAIL =====================================================================================
		END
	END
						
						
				
	
	RETURN
END

