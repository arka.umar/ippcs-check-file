-- =============================================
-- Author:		Fid.Joko
-- Create date: 2013-12-27
-- Description:	get Data for Purchase order CPO type report 
-- =============================================
CREATE PROCEDURE [dbo].[SP_REP_PO_CPO]
	@PoNo varchar(max) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	declare 
		@fid VARCHAR(6) = '24006', 
		@@mo INT = 1,
		@@classSH INT, 
		@@classDPH INT,
		@@classDH INT,
		@@statusSH INT, 
		@@statusDPH INT,
		@@statusDH INT;
		

SELECT TOP 1 
  @@classSH = CASE WHEN ISNUMERIC(SYSTEM_VALUE) = 1 
                   THEN CONVERT(INT, SYSTEM_VALUE) 
                   ELSE 0 
              END 
FROM TB_M_SYSTEM sx
WHERE sx.SYSTEM_CD = 'PO_APPROVER_SH'
  AND sx.FUNCTION_ID = 'ORDER';

SELECT TOP 1 
  @@classDPH = CASE WHEN ISNUMERIC(SYSTEM_VALUE) = 1 
                   THEN CONVERT(INT, SYSTEM_VALUE) 
                   ELSE 0 
              END 
FROM TB_M_SYSTEM sx
WHERE sx.SYSTEM_CD = 'PO_APPROVER_DPH'
  AND sx.FUNCTION_ID = 'ORDER';

SELECT TOP 1 
  @@classDH = CASE WHEN ISNUMERIC(SYSTEM_VALUE) = 1 
                   THEN CONVERT(INT, SYSTEM_VALUE) 
                   ELSE 0 
              END 
FROM TB_M_SYSTEM sx
WHERE sx.SYSTEM_CD = 'PO_APPROVER_DH'
  AND sx.FUNCTION_ID = 'ORDER';

SELECT TOP 1 @@statusSH = STATUS_cD 
FROM [IPPCS_WORKFLOW].dbo.TB_R_STATUS
WHERE [ACTION] = 1
AND [MODULE_CD] = @@mo
AND [CLASS] = @@classSH;

SELECT TOP 1 @@statusDPH = STATUS_CD 
FROM [IPPCS_WORKFLOW].dbo.TB_R_STATUS
WHERE [ACTION] = 1
AND [MODULE_CD] = @@mo
AND [CLASS] = @@classDPH;

SELECT TOP 1 @@statusDH = STATUS_CD 
FROM [IPPCS_WORKFLOW].dbo.TB_R_STATUS
WHERE [ACTION] = 1
AND [MODULE_CD] = @@mo
AND [CLASS] = @@classDH;

	
SELECT 
	h.PO_NO,
	h.PO_DATE,
	h.PO_DATE DATEPROD, 
	i.SUPP_CD,
	i.SUPP_NAME,
	i.SUPP_ADDR,
	i.SUPP_CITY,
	i.POSTAL_CD,
	i.SUPP_ATTENTION,
	i.PHONE_NO,
	i.FAX_NO,
	h.RELEASE_DT, 
	d.PO_ITEM,
	d.MAT_NO,
	d.MAT_DESC, 
	d.PART_COLOR_SUFFIX,
	d.PACKING_TYPE,
	d.PO_QUANTITY_NEW,
	case when d.REQUIRED_DT is null then '1900-01-01' else d.REQUIRED_DT end AS REQUIRED_DT, 
	d.PO_MAT_PRICE,
	d.AMOUNT,
	NULLIF(LTRIM(RTRIM(d.NOTE)), '') AS NOTE,
	-- CASE WHEN NULLIF(LTRIM(RTRIM(d.NOTE)), '') IS NULL THEN 'FALSE' ELSE 'TRUE' END AS HAS_NOTE,
	d.DELIVERY_NAME,
	d.DELIVERY_ADDRESS DELIVERY_ADDR, 
	d.DELIVERY_CITY, 
	d.DELIVERY_POSTAL_CD DELIVERY_POSTAL, 
	
	dbo.fn_GetSystemValue(@fid, 'PO_FORM_COMPANY') AS [CNAME], 
	dbo.fn_GetSystemValue(@fid, 'PO_FORM_COMPANY_ADDRESS') + CHAR(13) + CHAR(10) 
		+  dbo.fn_GetSystemValue(@fid, 'PO_FORM_CITY') + ' ' + 
		+  dbo.fn_GetSystemValue(@fid, 'PO_FORM_POSTAL_CODE') + ' - ' + 
		+  dbo.fn_GetSystemValue(@fid, 'PO_FORM_COUNTRY') 
	AS [CADDR], 
	dbo.fn_GetSystemValue(@fid, 'PO_FORM_PHONE') AS [CPHONE], 
	dbo.fn_GetSystemValue(@fid, 'PO_FORM_FAX') AS [CFAX], 
	dbo.fn_GetSystemValue(@fid, 'PO_NPWP') AS [CNPWP],
	dbo.fn_GetSystemValue('24008', 'PO_PC_FOOTER') AS [AFOOT], 
	dbo.fn_GetSystemValue(@fid, 'PO_CONTACT_PERSON') AS [CONTACT_PERSON] 
	, ISNULL(vdh.FIRST_NAME,'') + ISNULL(' ' + UPPER(SUBSTRING(vdh.LAST_NAME, 1, 1) + '.'),'')  as DHName
	, ISNULL(vdph.FIRST_NAME,'') + ISNULL(' ' + UPPER(SUBSTRING(vdph.LAST_NAME, 1, 1) + '.'),'')  as  DpHName 
	, ISNULL(vsh.FIRST_NAME,'') + ISNULL(' ' + UPPER(SUBSTRING(vsh.LAST_NAME, 1, 1) + '.'),'')  as SHName
	, case when h.RELEASE_DT is not null then '1' else '0' end as [ISRELEASE]
	, CASE WHEN VDH.FULL_NAME IS NOT NULL THEN 'SIGNED' ELSE '' END AS dhSigned 
	, CASE WHEN VDpH.FULL_NAME IS NOT NULL THEN 'SIGNED' ELSE '' END AS dphSigned 
	, CASE WHEN vsh.FULL_NAME IS NOT NULL THEN 'SIGNED' ELSE '' END AS shSigned 
FROM TB_R_PO_H h
LEFT JOIN TB_R_PO_D d ON h.PO_NO=d.PO_NO

LEFT JOIN TB_M_SUPPLIER_ICS i on h.SUPPLIER_CD=i.SUPP_CD

LEFT JOIN (SELECT * FROM [IPPCS_WORKFLOW].[dbo].[TB_R_DISTRIBUTION_STATUS] 
	where STATUS_CD = @@statusSH) 
   AS SHDS 
   ON h.[PO_NO] = SHDS.[FOLIO_NO] 
        
  
LEFT JOIN (SELECT * FROM [IPPCS_WORKFLOW].[dbo].[TB_R_DISTRIBUTION_STATUS] 
			WHERE STATUS_CD = @@statusDPH)
	   AS DPHDS 
	   ON h.[PO_NO] = DPHDS.[FOLIO_NO] 
       
LEFT JOIN (SELECT * FROM [IPPCS_WORKFLOW].[dbo].[TB_R_DISTRIBUTION_STATUS] 
			WHERE STATUS_CD = @@statusDH) 
	   AS DHDS 
	   ON h.[PO_NO] = DHDS.[FOLIO_NO] 
	   
LEFT JOIN dbo.TB_R_VW_EMPLOYEE vsh ON vsh.USERNAME = shds.PROCEED_BY 
LEFT JOIN dbo.TB_R_VW_EMPLOYEE vdph ON vdph.USERNAME = dphds.PROCEED_BY
LEFT JOIN dbo.TB_R_VW_EMPLOYEE vdh ON vdh.USERNAME = dhds.PROCEED_BY

WHERE h.PO_NO=@PoNo

END

