/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Invoice save without posting
*/

CREATE PROCEDURE [dbo].[SP_DLV_INV_SaveInvoice_OLD]
   @PO_NO varchar(max),
   @LP_INV_NO varchar(max),
   @INV_NO as varchar(max),
   @INV_DT as varchar(max),
   @INV_TAX_CD as varchar(max),
   @INV_TAX_DT as varchar(max),
   @STATUS_CD as varchar(2),
   @UserId as varchar(20),
   @DLV_NO as varchar(max),
	 --@BASELINE_DT As VARCHAR (MAX),
	 @LP_INV_NO_GRID AS VARCHAR (MAX) --added 29042014


AS
BEGIN
	DECLARE @MODULE_ID as varchar(1) = '3'
	DECLARE @FUNCTION_ID as varchar(5) = '31401'
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_INV_SaveInvoice'
	DECLARE @step AS VARCHAR(50) = 'init'
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @errorMessage as varchar(max)
	DECLARE @x XML;
	SET @x = '<r>' + REPLACE((SELECT @DLV_NO FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	DECLARE @count2 as int = (SELECT COUNT(*) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND INV_TEXT IS NOT NULL AND CONDITION_CATEGORY = 'H' AND STATUS_CD NOT IN('-3'))
	DECLARE @count  as int = (SELECT COUNT(*) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND INV_TEXT IS NULL AND CONDITION_CATEGORY = 'H' AND STATUS_CD NOT IN('-3'))
	DECLARE @LP_CD as varchar(3);


	CREATE TABLE #tempSplitDelivery
	( 
		ID int identity,
		name VARCHAR(MAX)
	)

	INSERT INTO #tempSplitDelivery (name)
	EXEC dbo.SplitString @DLV_NO, ','
	
	DECLARE @iData int, @cntData int
	
	IF (@LP_INV_NO_GRID = '') AND (@count>0) BEGIN
		SET @log = 'Data already exist. By '+@na;
		INSERT @T
		EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00005ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
		SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Data already exist' as [MESSAGE]
	END
	ELSE IF(@count2>0) BEGIN
		--UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
		SET @log = 'Data already exist. By '+@na;
		INSERT @T
		EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00005ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
		SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Data already exist' as [MESSAGE]
	END
	ELSE BEGIN
			IF(@count>0) BEGIN

					SET @pid = (SELECT TOP 1 PROCESS_ID FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO ORDER BY SEQ_NO DESC)
					SET @LP_CD = (SELECT TOP 1 LP_CD FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO ORDER BY SEQ_NO DESC)
					
					SET @log = 'Saving invoice By '+@na;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
					
					BEGIN TRY 
							UPDATE TB_R_DLV_INV_UPLOAD 
							SET 
							INV_NO = NULL, 
							INV_DT = @INV_DT, 
							INV_TAX_DT = @INV_TAX_DT, 
							BASELINE_DT = @INV_TAX_DT,
							--BASELINE_DT = @BASELINE_DT,
							STATUS_CD = 2,
							IN_PROGRESS = NULL,
							--CERTIFICATE_ID = @LP_CD+''+@LP_INV_NO+''+replace(convert(VARCHAR(20),getdate(),102),'.','')
							CERTIFICATE_ID = dbo.FN_CERTID(@LP_CD),
							PAYMENT_METHOD_CD = (SELECT PAYMENT_METHOD_CD FROM TB_R_DLV_PO_H WHERE PO_NO = @PO_NO),
							PAYMENT_TERM_CD = (SELECT PAYMENT_TERM_CD FROM TB_R_DLV_PO_H WHERE PO_NO = @PO_NO)
							WHERE PO_NO = @PO_NO AND LP_INV_NO = @LP_INV_NO
							
							UPDATE TB_R_DLV_INV_UPLOAD SET INV_TEXT = @INV_TAX_CD WHERE PO_NO = @PO_NO AND LP_INV_NO = @LP_INV_NO AND CONDITION_CATEGORY = 'H'
							 
							UPDATE TB_R_DLV_GR_IR SET STATUS_CD = 2 WHERE REFF_NO IN (SELECT DISTINCT DELIVERY_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO) AND SERVICE_DOC_NO IN (SELECT DISTINCT SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO)
							
							SET @log = 'Saving invoice success By '+@na;
							INSERT @T
							EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
							SELECT 'success' as [STATUS], 'INF' as [TYPE], 'Saving invoice success' as [MESSAGE]
					
					END TRY
					BEGIN CATCH
							SET @errorMessage = ERROR_MESSAGE();
							SET @log = 'Saving invoice failed : '+@errorMessage+'. By '+@na;
							INSERT @T
							EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
							SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Saving invoice failed' as [MESSAGE]
					END CATCH
			END
			ELSE BEGIN
					SET @log = 'Saving invoice By '+@na;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
					
					INSERT INTO TB_R_DLV_QUEUE(PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, PROCESS_END_DT, CREATED_BY, CREATED_DT)
					SELECT @pid as PROCESS_ID, @MODULE_ID, @FUNCTION_ID, 'QU1', GETDATE(), NULL, @UserId, GETDATE()
					
					
					
					BEGIN TRY
							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU2' WHERE PROCESS_ID = @pid

							
--===29042014===--
							DECLARE @CHECK_LP_INV INT; --FROM SELECTION GRID
							DECLARE @CHECK_REFF INT, @CHECK_TUPLOAD INT;
							DECLARE @LP_NAME varchar(MAX);
							DECLARE @SERVICE_DOC_NO VARCHAR(MAX), @ROUTE_CD VARCHAR (5), @COMP_PRICE_CD VARCHAR (6), @AMOUNT VARCHAR (MAX), @TRANSACTION_CURR VARCHAR (4);
							
							DECLARE @DELIVERY_NO varchar(MAX);
							SET @iData = 1;
							SET @cntData = (SELECT COUNT ('') FROM #tempSplitDelivery);

							WHILE @iData <= @cntData BEGIN 


										SET @DELIVERY_NO=(SELECT name FROM #tempSplitDelivery WHERE ID=@iData);

										SET @SERVICE_DOC_NO = (SELECT DISTINCT SERVICE_DOC_NO FROM TB_R_DLV_GR_IR WHERE REFF_NO = @DELIVERY_NO AND PO_NO = @PO_NO AND CANCEL_STS IS NULL)
										SET @ROUTE_CD = (SELECT DISTINCT ROUTE_CD FROM TB_R_DLV_SERVICE_DOC WHERE REFF_NO = @DELIVERY_NO AND PO_NO = @PO_NO)
										SET @LP_NAME = (SELECT DISTINCT LOG_PARTNER_NAME FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = @LP_CD)
										SET @COMP_PRICE_CD = (SELECT COMP_PRICE_CD FROM TB_R_DLV_GR_IR WHERE REFF_NO = @DELIVERY_NO AND PO_NO = @PO_NO AND CANCEL_STS IS NULL AND CONDITION_CATEGORY = 'H');
										SET @AMOUNT = (SELECT GR_IR_AMT FROM TB_R_DLV_GR_IR WHERE REFF_NO = @DELIVERY_NO AND PO_NO = @PO_NO AND CANCEL_STS IS NULL  AND CONDITION_CATEGORY = 'H');
										SET @TRANSACTION_CURR = (SELECT ITEM_CURR FROM TB_R_DLV_GR_IR WHERE REFF_NO = @DELIVERY_NO AND PO_NO = @PO_NO AND CANCEL_STS IS NULL  AND CONDITION_CATEGORY = 'H');

										
										
										SET @CHECK_REFF = (SELECT COUNT (*) FROM TB_R_DLV_SERVICE_DOC WHERE PROCESS_ID IS NULL AND REFF_NO = @DELIVERY_NO )
			
										IF (@CHECK_REFF > 0) BEGIN 
													
												SET @CHECK_LP_INV = (SELECT COUNT (*) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND PO_NO = @PO_NO AND STATUS_CD NOT IN ('1', '-1', '-3'));					
												
												IF (@CHECK_LP_INV = 0 ) BEGIN
														
														INSERT INTO TB_T_DLV_INV_UPLOAD(SERVICE_DOC_NO, LP_NAME, LP_CD, PO_NO, ROUTE_CD, DELIVERY_NO, COMP_PRICE_CD, DOC_DT, QTY, LP_INV_NO, PRICE_AMT, PAYMENT_CURR, PROCESS_ID, REMARKS)
														SELECT @SERVICE_DOC_NO, @LP_NAME, @LP_CD, @PO_NO, @ROUTE_CD, @DELIVERY_NO, @COMP_PRICE_CD, GETDATE(), 1, @LP_INV_NO, @AMOUNT, @TRANSACTION_CURR, @pid, 'Correct'
														UPDATE TB_R_DLV_SERVICE_DOC SET PROCESS_ID = @PID WHERE REFF_NO = @DELIVERY_NO;
												END
												ELSE BEGIN
														
														-- COMPARE GR_IR
														INSERT INTO TB_T_DLV_INV_UPLOAD(SERVICE_DOC_NO, LP_NAME, LP_CD, PO_NO, ROUTE_CD, DELIVERY_NO, COMP_PRICE_CD, DOC_DT, QTY, LP_INV_NO, PRICE_AMT, PAYMENT_CURR, PROCESS_ID, REMARKS)
														SELECT @SERVICE_DOC_NO, @LP_NAME, @LP_CD, @PO_NO,  @ROUTE_CD, @DELIVERY_NO, @COMP_PRICE_CD, GETDATE(), 1, @LP_INV_NO, @AMOUNT, @TRANSACTION_CURR, @pid, 'Incorrect'

												END
												SET @iData = @iData + 1


										END
										ELSE BEGIN
												--SET @log = 'Saving invoice failed : '+@errorMessage+'. By '+@na;
												--INSERT @T
												--EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
												SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Error, Service document already process' as [MESSAGE];
												UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
												RETURN
												SET @iData = @cntData + 1
										END 
										
										
							END --===END OF WHILE


							SET @CHECK_TUPLOAD = (SELECT COUNT(*) FROM TB_T_DLV_INV_UPLOAD WHERE REMARKS = 'Incorrect' AND PROCESS_ID = @pid);

							IF ( @CHECK_TUPLOAD = 0 ) BEGIN
							
					
									UPDATE TB_R_DLV_GR_IR SET STATUS_CD = 2 WHERE REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND CANCEL_STS IS NULL

									INSERT INTO TB_R_DLV_INV_UPLOAD(PO_NO, PO_ITEM_NO, DELIVERY_NO, COMP_PRICE_CD, SEQ_NO, LP_INV_NO, ROUTE_CD, LP_CD, SERVICE_DOC_NO, DOC_DT, QTY, S_QTY, PRICE_AMT, PAYMENT_CURR, REMARKS, INV_AMT, INV_AMT_LOCAL_CURR, CONDITION_CATEGORY, CURR_CD, INV_TAX_CD, INV_TAX_AMT, UOM, IN_PROGRESS, STATUS_CD, CERTIFICATE_ID, CREATED_BY, CREATED_DT, INV_DT, INV_TAX_DT, INV_TEXT, PAYMENT_METHOD_CD, PAYMENT_TERM_CD, BASELINE_DT, PARTNER_BANK_KEY, PROCESS_ID)
									SELECT 
									a.PO_NO, 
									a.PO_ITEM_NO, 
									a.REFF_NO as DELIVERY_NO, 
									a.COMP_PRICE_CD, 
									ISNULL((SELECT MAX(si.SEQ_NO)+1 AS SEQ_NO FROM TB_R_DLV_INV_UPLOAD si WHERE si.DELIVERY_NO = a.REFF_NO AND si.COMP_PRICE_CD = a.COMP_PRICE_CD), '1'),
									@LP_INV_NO as LP_INV_NO,
									b.ROUTE_CD,
									a.LP_CD,
									a.SERVICE_DOC_NO,
									GETDATE() as DOC_DT,
									1 as QTY,
									1 as S_QTY,
									PO_DETAIL_PRICE as PRICE_AMT,
									'IDR' as PAYMENT_CURR,
									'Correct' as REMARKS,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN a.PO_DETAIL_PRICE ELSE NULL END as INV_AMT,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN a.PO_DETAIL_PRICE ELSE NULL END as INV_AMT_LOCAL_CURR,
									a.CONDITION_CATEGORY as CONDITION_CATEGORY,
									'IDR' as CURR_CD,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE TB_M_SYSTEM.SYSTEM_CD = 'DEFAULT_TAX_CODE') ELSE NULL END as INV_TAX_CD,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN (0.1 * a.GR_IR_AMT) ELSE NULL END as INV_TAX_AMT,
									'RET' as UOM,
									NULL as IN_PROGRESS,
									2 as STATUS_CD,
									--CERTIFICATE_ID = a.LP_CD+''+@LP_INV_NO+''+replace(convert(VARCHAR(20),getdate(),102),'.',''),
									CERTIFICATE_ID = dbo.FN_CERTID(a.LP_CD),
									@UserId as CREATED_BY,
									GETDATE() as CREATED_DT,
									@INV_DT, 
									@INV_TAX_DT,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN @INV_TAX_CD ELSE NULL END as INV_TAX_CD,
									d.PAYMENT_METHOD_CD as PAYMENT_METHODO_CD,
									d.PAYMENT_TERM_CD as PAYMENT_TERM_CD,
									@INV_TAX_DT,
									--@BASELINE_DT,
									1 as PARTNER_BANK_KEY,
									@pid
									FROM TB_R_DLV_GR_IR a
									LEFT JOIN TB_R_DLV_SERVICE_DOC b ON b.SERVICE_DOC_NO = a.SERVICE_DOC_NO
									LEFT JOIN TB_R_DLV_PO_ITEM c ON c.PO_NO = b.PO_NO AND c.ROUTE_CD = b.ROUTE_CD
									--LEFT JOIN TB_M_LOGISTIC_PARTNER d ON d.LOG_PARTNER_CD = a.LP_CD
									LEFT JOIN TB_R_DLV_PO_H d ON d.PO_NO = a.PO_NO
									WHERE a.REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND CANCEL_STS IS NULL
									

									UPDATE TB_R_DLV_GR_IR SET LIV_FLAG = 1 WHERE REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND CANCEL_STS IS NULL
									AND CANCEL_STS IS NULL



									SET @log = 'Saving invoice success By '+@na;
									INSERT @T
									EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU3', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
									SELECT 'success' as [STATUS], 'INF' as [TYPE], 'Saving invoice success' as [MESSAGE]
							

							END
							ELSE BEGIN 
									SET @log = 'Some data are incorrect, terminating process. by '+@na;
									INSERT @T
									EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODULE_ID, @FUNCTION_ID;
									
									
									SET @log = 'LIV verification failed By '+@na;
									INSERT @T
									EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODULE_ID, @FUNCTION_ID;
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
									SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'LIV verification failed' as [MESSAGE]


							END
							
							DELETE FROM TB_T_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid
							UPDATE TB_R_DLV_SERVICE_DOC SET  PROCESS_ID = NULL WHERE PROCESS_ID = @pid
							UPDATE TB_R_DLV_INV_UPLOAD SET  PROCESS_ID = NULL WHERE PROCESS_ID = @pid


					END TRY
					BEGIN CATCH
							SET @errorMessage = ERROR_MESSAGE();
							SET @log = 'Saving invoice failed : '+@errorMessage+'. By '+@na;
							INSERT @T
							EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
							SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Saving invoice failed' as [MESSAGE]

					END CATCH

				--WHERE a.CONDITION_CATEGORY = 'H' AND a.REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND CANCEL_STS IS NULL
			END
	END
END

