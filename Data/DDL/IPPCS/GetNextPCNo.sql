ALTER PROCEDURE GetNextPCNo @PCDate DATETIME, @run int=0, @NextPCNo varchar(21) output  
as 
BEGIN
  declare @nm varchar(20) = 'GetNextPCNo', @fn varchar(5) = '25124', @syscd varchar(20), @pcnoseq int = 0, @romanov varchar(4) ='', @seqnew int =0
  , @mon int = month(getdate())
  , @yy int = year(getdate())  
  
  set @syscd = 'PC_NO_SEQ_' + CONVERT(VARCHAR(4), @yy) + '-' + RIGHT('0'+CONVERT(VARCHAR(2), @mon),2)
  
  declare @numeral table (num int, roman varchar(4)); 
  insert into @numeral(num, roman) values (1, 'I'), (2, 'II'),(3, 'III'), (4, 'IV'), (5, 'V'), (6, 'VI'), (7, 'VII'),  (8, 'VIII'), (9, 'IX'),  (10, 'X'),  (11, 'XI'),  (12, 'XII'); 
  SET @pcnoseq= (SELECT TOP 1 CONVERT(INT, SYSTEM_VALUE) + 1 pcnoseq 
  FROM TB_M_SYSTEM m
  WHERE FUNCTION_ID = '25124' AND SYSTEM_CD = @syscd); 
  IF @pcnoseq is null
  BEGIN
    set @seqnew = 1; 
    set @pcnoseq = 1; 
  END; 
  if @run = 1 
  begin
    if @seqnew =1 
    begin
      insert into tb_m_system(function_id, system_cd, system_value, system_remark, created_by, created_dt) 
      values (@fn, @syscd, convert(varchar(15), @pcnoseq), 'seq of pc_no', @nm, getdate()); 
    end
    else 
    begin
      update tb_m_system set 
		system_value = convert(varchar(15), @pcnoseq) 
	  , changed_by = @nm
	  , changed_dt = getdate() 
	  where function_id = @fn and system_cd = @syscd;    
    end 
  end;
  select top 1 @NextPCNo = rtrim(lTRIM(SYSTEM_VALUE))
  from tb_m_system where function_id = @fn and system_cd= 'PC_NO_FORMAT';
  
  IF @NextPCNo IS NULL 
    SET @NextPCNo = 'PuD/E/PC/';

  select @NextPcNo = a.PCHead + a.seq + '/' + a.roman + '/' + a.yy
  from (  
     SELECT case when x.ci > 0 then left(x.x, x.ci) else x.x end  PCHead, 
      right('0000' +convert(varchar(4), @pcnoseq) ,4) seq,
      n.roman, 
      right('0000' + convert(varchar(4),  @yy),2) yy
    from (select @NextPcNo x, (charindex('{', @nextpcno)-1) ci ) x   
    join (select top 1 roman from  @numeral n where n.num = @mon) n on 1=1 
    ) a;
   
END 