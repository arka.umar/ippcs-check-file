-- =============================================
-- Author:		FID.Deny
-- Create date: 2015-09-10
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetOrderCompareSummary] 
	@dateFrom varchar(15),
	@dateTo varchar(15)
AS
BEGIN
	SET NOCOUNT ON;
	

	SELECT 

		(SELECT COUNT(1)				
		FROM (                                					
				select a.ORDER_NO, 			
					a.DOCK_CD, 		
					a.SUPPLIER_CD,		
					a.SUPPLIER_PLANT,		
					b.PART_NO, 				
					SUM(b.ORDER_QTY) IPPCS_ORDER_QTY                             		
				from IPPCS_QA.dbo.TB_R_DAILY_ORDER_MANIFEST a, 			
					 IPPCS_QA.dbo.TB_R_DAILY_ORDER_PART b                                		
				where 
					CAST(LEFT(a.ORDER_NO, 8) AS DATE) BETWEEN @dateFrom AND @dateTo	
				AND a.MANIFEST_NO = b.MANIFEST_NO                                			
				GROUP BY a.ORDER_NO, a.DOCK_CD, a.SUPPLIER_CD,a.SUPPLIER_PLANT, b.PART_NO, a.PROCESS_FLAG,a.CREATED_DT			
			 ) i                                				
		INNER JOIN (					
				SELECT PRODUCTION_DATE, ARRIVAL_SEQ, SUPP_CD, 			
					SUPP_PLANT, 		
					PART_NO, 		
					DOCK_CD, 		
					LEFT(CONVERT(VARCHAR, PRODUCTION_DATE, 112) +                                		
						CASE WHEN LEN(CONVERT(VARCHAR, ARRIVAL_SEQ)) = 1 THEN    	
							'0'+ CONVERT(VARCHAR, ARRIVAL_SEQ)
							ELSE CONVERT(VARCHAR, ARRIVAL_SEQ) END, 12) ORDER_NO, 
					SUM(ORDER_QTY) ORDER_QTY_KCI		
				FROM KCI_DB.dbo.TB_R_ORDER_MOD                                 			
				WHERE PRODUCTION_DATE BETWEEN @dateFrom AND @dateTo                                			
				GROUP BY SUPP_CD, SUPP_PLANT, PART_NO, DOCK_CD, PRODUCTION_DATE, ARRIVAL_SEQ			
			) k                                 				
		ON k.SUPP_CD = i.SUPPLIER_CD 					
		AND k.SUPP_PLANT = i.SUPPLIER_PLANT                                					
		AND k.PART_NO = i.PART_NO 					
		AND k.DOCK_CD = i.DOCK_CD 					
		AND k.ORDER_NO = i.ORDER_NO
		) AS countAll,



		(SELECT COUNT(1)				
		FROM (                                					
				select a.ORDER_NO, 			
					a.DOCK_CD, 		
					a.SUPPLIER_CD,		
					a.SUPPLIER_PLANT,		
					b.PART_NO, 				
					SUM(b.ORDER_QTY) IPPCS_ORDER_QTY                             		
				from IPPCS_QA.dbo.TB_R_DAILY_ORDER_MANIFEST a, 			
					 IPPCS_QA.dbo.TB_R_DAILY_ORDER_PART b                                		
				where 
					CAST(LEFT(a.ORDER_NO, 8) AS DATE) BETWEEN @dateFrom AND @dateTo	
				AND a.MANIFEST_NO = b.MANIFEST_NO                                			
				GROUP BY a.ORDER_NO, a.DOCK_CD, a.SUPPLIER_CD,a.SUPPLIER_PLANT, b.PART_NO, a.PROCESS_FLAG,a.CREATED_DT			
			 ) i                                				
		INNER JOIN (					
				SELECT PRODUCTION_DATE, ARRIVAL_SEQ, SUPP_CD, 			
					SUPP_PLANT, 		
					PART_NO, 		
					DOCK_CD, 		
					LEFT(CONVERT(VARCHAR, PRODUCTION_DATE, 112) +                                		
						CASE WHEN LEN(CONVERT(VARCHAR, ARRIVAL_SEQ)) = 1 THEN    	
							'0'+ CONVERT(VARCHAR, ARRIVAL_SEQ)
							ELSE CONVERT(VARCHAR, ARRIVAL_SEQ) END, 12) ORDER_NO, 
					SUM(ORDER_QTY) ORDER_QTY_KCI		
				FROM KCI_DB.dbo.TB_R_ORDER_MOD                                 			
				WHERE PRODUCTION_DATE BETWEEN @dateFrom AND @dateTo                                			
				GROUP BY SUPP_CD, SUPP_PLANT, PART_NO, DOCK_CD, PRODUCTION_DATE, ARRIVAL_SEQ			
			) k                                 				
		ON k.SUPP_CD = i.SUPPLIER_CD 					
		AND k.SUPP_PLANT = i.SUPPLIER_PLANT                                					
		AND k.PART_NO = i.PART_NO 					
		AND k.DOCK_CD = i.DOCK_CD 					
		AND k.ORDER_NO = i.ORDER_NO
		AND k.ORDER_QTY_KCI = i.IPPCS_ORDER_QTY
		) AS countMatch,


				(SELECT COUNT(1)				
		FROM (                                					
				select a.ORDER_NO, 			
					a.DOCK_CD, 		
					a.SUPPLIER_CD,		
					a.SUPPLIER_PLANT,		
					b.PART_NO, 				
					SUM(b.ORDER_QTY) IPPCS_ORDER_QTY                             		
				from IPPCS_QA.dbo.TB_R_DAILY_ORDER_MANIFEST a, 			
					 IPPCS_QA.dbo.TB_R_DAILY_ORDER_PART b                                		
				where 
					CAST(LEFT(a.ORDER_NO, 8) AS DATE) BETWEEN @dateFrom AND @dateTo	
				AND a.MANIFEST_NO = b.MANIFEST_NO                                			
				GROUP BY a.ORDER_NO, a.DOCK_CD, a.SUPPLIER_CD,a.SUPPLIER_PLANT, b.PART_NO, a.PROCESS_FLAG,a.CREATED_DT			
			 ) i                                				
		INNER JOIN (					
				SELECT PRODUCTION_DATE, ARRIVAL_SEQ, SUPP_CD, 			
					SUPP_PLANT, 		
					PART_NO, 		
					DOCK_CD, 		
					LEFT(CONVERT(VARCHAR, PRODUCTION_DATE, 112) +                                		
						CASE WHEN LEN(CONVERT(VARCHAR, ARRIVAL_SEQ)) = 1 THEN    	
							'0'+ CONVERT(VARCHAR, ARRIVAL_SEQ)
							ELSE CONVERT(VARCHAR, ARRIVAL_SEQ) END, 12) ORDER_NO, 
					SUM(ORDER_QTY) ORDER_QTY_KCI		
				FROM KCI_DB.dbo.TB_R_ORDER_MOD                                 			
				WHERE PRODUCTION_DATE BETWEEN @dateFrom AND @dateTo                                			
				GROUP BY SUPP_CD, SUPP_PLANT, PART_NO, DOCK_CD, PRODUCTION_DATE, ARRIVAL_SEQ			
			) k                                 				
		ON k.SUPP_CD = i.SUPPLIER_CD 					
		AND k.SUPP_PLANT = i.SUPPLIER_PLANT                                					
		AND k.PART_NO = i.PART_NO 					
		AND k.DOCK_CD = i.DOCK_CD 					
		AND k.ORDER_NO = i.ORDER_NO
		AND k.ORDER_QTY_KCI <> i.IPPCS_ORDER_QTY
		) AS countDiff

		



END

