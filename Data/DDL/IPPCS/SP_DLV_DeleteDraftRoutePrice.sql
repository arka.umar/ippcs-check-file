/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Delete draft route price
*/

CREATE PROCEDURE [dbo].[SP_DLV_DeleteDraftRoutePrice]

@GRID VARCHAR (MAX),
@CREATED_BY VARCHAR (MAX)

AS
BEGIN

		CREATE TABLE #TEMP(
			CHILD_ROUTE VARCHAR(4),
			PARENT_ROUTE VARCHAR (4),
			LP_CD VARCHAR(4),
			VALID_FROM DATE,
			VALID_TO DATE

		);
		

		DECLARE @CHECK_AUTH INT;
		
		DECLARE  @CHECK_OLDER INT, @CHECK_PRICE INT, @CHECK_APPROVED INT, @CHECK_VALID INT;

		SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE JOB_FUNCTION_ID = 6 AND USERNAME = @CREATED_BY);
		
		IF (@CHECK_AUTH = 1) BEGIN
		
		
				-- 2. == VARIABLE TABLE TEMPORARY UNTUK EXPLODING GRID ==
				DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
				DECLARE @MIN_ROW INT;
				DECLARE @ROW_DETAIL VARCHAR(MAX);
				DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
				DECLARE @MIN_FIELD INT;
				-- ================================================================

				-- 3. == Variabel Untuk Menyimpan Data Ke TABLE DETAIL ==
				DECLARE @CHILD_ROUTE_T VARCHAR(4),@PARENT_ROUTE_T VARCHAR (4), @LP_CD_T VARCHAR(3), @VALID_FROM_T DATE, @VALID_TO_T DATE;
				
				-- =================================================================
				INSERT INTO @ROWS
				SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@GRID,';');
				-- ================================================================
				SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);
				-- ========================================================
				WHILE @MIN_ROW IS NOT NULL BEGIN
							-- 6.3.1 == Hapus VARIABLE TABLE @FIELDS jika ada ==
							DELETE FROM @FIELDS;
							
							SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
							INSERT INTO @FIELDS
							SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'|');
							-- ========================================================================================

							-- 6.3.3 == Ambil ITEM Berdasarkan ID untuk dimasukkan kedalam VARIABLE FIELD Detail ==
							SET @CHILD_ROUTE_T = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
							SET @PARENT_ROUTE_T = (SELECT ITEM FROM @FIELDS WHERE ID = 2);
							SET @LP_CD_T = (SELECT ITEM FROM @FIELDS WHERE ID = 3);
							SET @VALID_FROM_T = (SELECT ITEM FROM @FIELDS WHERE ID = 4);
							SET @VALID_TO_T = (SELECT ITEM FROM @FIELDS WHERE ID = 5);
							
							-- ====================================================================================

							
							INSERT INTO #TEMP VALUES(@CHILD_ROUTE_T, @PARENT_ROUTE_T, @LP_CD_T, @VALID_FROM_T, @VALID_TO_T);


							-- 6.3.5 == Ambil ID dari VARIABLE TABLE @ROWS yg IDnya lebih besar dari @MIN_ROW ==
							SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
							-- =================================================================================

				END --END WHILE
				
				


				SET @CHECK_VALID = (SELECT COUNT (*) FROM #TEMP WHERE VALID_TO = '9999-12-31' );

				
				IF (@CHECK_VALID > 0) BEGIN
				

						DECLARE @CHILD_ROUTE VARCHAR(4),@PARENT_ROUTE VARCHAR (4), @LP_CD VARCHAR(3), @VALID_FROM DATE, @VALID_TO DATE;
						
						DECLARE BTMP_CUR CURSOR
						FOR
						SELECT CHILD_ROUTE, PARENT_ROUTE, LP_CD, VALID_FROM, VALID_TO
						FROM #TEMP;

						BEGIN TRANSACTION DELETETRANS
						BEGIN TRY
							OPEN BTMP_CUR;
							FETCH NEXT FROM BTMP_CUR INTO
							@CHILD_ROUTE, @PARENT_ROUTE, @LP_CD, @VALID_FROM, @VALID_TO

							WHILE @@FETCH_STATUS = 0
							BEGIN

								
										SET @CHECK_OLDER = ( SELECT COUNT (*) FROM TB_M_DLV_DRAFT_ROUTE_PRICE WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND ROUTE_CD_PARENT = @PARENT_ROUTE 
																				AND LP_CD=@LP_CD AND VALID_FROM = CAST (@VALID_FROM AS DATE) AND VALID_TO = CAST (@VALID_TO AS DATE) );

										SET @CHECK_PRICE = ( SELECT COUNT (*) FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @PARENT_ROUTE );

										SET @CHECK_APPROVED = (SELECT COUNT (*) FROM TB_M_DLV_DRAFT_ROUTE_PRICE WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND ROUTE_CD_PARENT = @PARENT_ROUTE 
																				AND LP_CD=@LP_CD AND VALID_FROM = CAST (@VALID_FROM AS DATE) AND VALID_TO = CAST (@VALID_TO AS DATE)
																				AND APPROVED_BY IS NOT NULL AND APPROVED_DT IS NOT NULL );
										
										

										--IF (@CHECK_PRICE = 0) BEGIN

												IF (@CHECK_APPROVED = 0) BEGIN

															IF (@CHECK_OLDER > 0) BEGIN
																	
																		UPDATE TB_M_DLV_DRAFT_ROUTE_PRICE 
																		--SET VALID_TO = DATEADD(DAY, -1, @VALID_FROM)
																		SET VALID_TO = '9999-12-31'
																		WHERE ROUTE_CD_CHILD = @CHILD_ROUTE 
																		AND PROPOSED_DT = (SELECT MAX(PROPOSED_DT) FROM TB_M_DLV_DRAFT_ROUTE_PRICE 
																										WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND ROUTE_CD_PARENT != @PARENT_ROUTE) 
																		--AND ROUTE_CD_PARENT = @PARENT_ROUTE AND LP_CD=@LP_CD;

																		DELETE FROM TB_M_DLV_DRAFT_ROUTE_PRICE WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND ROUTE_CD_PARENT = @PARENT_ROUTE AND LP_CD=@LP_CD 
																		AND VALID_FROM = CAST (@VALID_FROM AS DATE) AND VALID_TO = CAST (@VALID_TO AS DATE);

															END
															ELSE BEGIN
																	
																		DELETE FROM TB_M_DLV_DRAFT_ROUTE_PRICE WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND ROUTE_CD_PARENT = @PARENT_ROUTE AND LP_CD=@LP_CD 
																		AND VALID_FROM = CAST (@VALID_FROM AS DATE) AND VALID_TO = CAST (@VALID_TO AS DATE);

															END


												END
												ELSE BEGIN
															RAISERROR('Cannot delete master draft route price, because it already approved.',16,1)
															RETURN
												END

							FETCH NEXT FROM BTMP_CUR INTO
								@CHILD_ROUTE, @PARENT_ROUTE, @LP_CD, @VALID_FROM, @VALID_TO
							END;
							CLOSE BTMP_CUR;
							DEALLOCATE BTMP_CUR;
							
							COMMIT TRANSACTION DELETETRANS
							
						END TRY
						BEGIN CATCH
							ROLLBACK TRANSACTION DELETETRANS
						END CATCH

						DROP TABLE #TEMP;
		


				END
				ELSE BEGIN
						RAISERROR('Cannot delete data.',16,1)
						RETURN
				END

		END
		ELSE BEGIN
				
				RAISERROR('You are not authorized to perform this action.',16,1)
				RETURN
		END

END

