CREATE PROCEDURE [dbo].[GetLPOPCompletenessPriceSheet]
AS
BEGIN
DECLARE @PROD_MONTH VARCHAR(6)

DECLARE @cols AS NVARCHAR(MAX),
		@query  AS NVARCHAR(MAX),
		@max_week INT;

DECLARE @WEEK TABLE (
	[WEEK] INT
)

SELECT @PROD_MONTH = MAX(PACK_MONTH) FROM TB_R_LPOP WHERE VERS = 'F'
INSERT INTO @WEEK
SELECT DISTINCT [WEEK] FROM TB_R_LPOP_COMPLETENESS_PRICE WHERE PACK_MONTH = @PROD_MONTH ORDER BY [WEEK] ASC

SET @cols = STUFF((SELECT distinct ',' + QUOTENAME('WEEK_' + CONVERT(VARCHAR(10), c.[WEEK])) 
            FROM @WEEK c
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

SELECT @max_week = MAX([WEEK]) FROM TB_R_LPOP_COMPLETENESS_PRICE
WHERE PACK_MONTH = ISNULL(@PROD_MONTH, '')

set @query = 'SELECT 
					SUPPLIER_CD,
					SUPPLIER_NAME,
					PART_NO,
					PART_NAME,
					PART_COLOR_SFX,
					DOCK_CD,
					N_VOLUME,
					N_1_VOLUME,
					N_2_VOLUME,
					N_3_VOLUME, 
					' + @cols + ' 
			FROM 
            (
                SELECT 
                    ''WEEK_'' + CONVERT(VARCHAR(10), [WEEK]) [WEEK],
					SUPPLIER_CD,
					SUPPLIER_NAME,
					PART_NO,
					PART_NAME,
					PART_COLOR_SFX,
					DOCK_CD,
					CONVERT(VARCHAR(MAX), N_VOLUME) N_VOLUME,
					CONVERT(VARCHAR(MAX), N_1_VOLUME) N_1_VOLUME,
					CONVERT(VARCHAR(MAX), N_2_VOLUME) N_2_VOLUME,
					CONVERT(VARCHAR(MAX), N_3_VOLUME) N_3_VOLUME,
					ISNULL(PACKING_TYPE, '''') + ''|'' +
					ISNULL(PC_NO, '''') + ''|'' +
					ISNULL(PC_RELEASE_STATUS, '''') + ''|'' + 
					ISNULL(CONVERT(VARCHAR(20), PC_VALID_DT_FR, 104), '''') + ''|'' + 
					ISNULL(SOURCE_SUPP_CD, '''') + ''|'' + 
					ISNULL(CONVERT(VARCHAR(20), SOURCE_VALID_DT_FROM, 104), '''') +
					CASE WHEN [WEEK] = ' + CONVERT(VARCHAR(10), @max_week) + 
					' THEN ''|'' + ISNULL(OVER_STATUS, '''') ELSE '''' END as value
                FROM TB_R_LPOP_COMPLETENESS_PRICE
				WHERE PACK_MONTH = ''' + ISNULL(@PROD_MONTH, '') + '''
           ) x
            PIVOT 
            (
                MAX(value)
                FOR [WEEK] IN (' + @cols + ')
            ) p '

EXECUTE(@query)

--SELECT * FROM TB_R_LPOP_COMPLETENESS_PRICE WHERE PACK_MONTH = @PROD_MONTH
END

