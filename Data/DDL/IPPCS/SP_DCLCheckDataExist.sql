-- =============================================
-- Author:fid.deny
-- alter date: 2015-03-10
-- Description:do checking after DCL sync, if table is empty, don't continue with dcl creation.
-- =============================================
CREATE PROCEDURE [dbo].[SP_DCLCheckDataExist] 
AS
BEGIN
DECLARE @@status varchar(10),
    @@items varchar(10)

SET NOCOUNT ON;

SELECT @@items = COUNT(1) FROM TB_T_DCL_TLMS_DRIVER_DATA;

IF(CAST(@@items AS INT) > 0)
BEGIN
SET @@status = 'exist'
END
ELSE
BEGIN
SET @@status = 'empty'
END

select @@status

END

