-- =============================================
-- Author:		fid.deny
-- Create date: 2015-11-11
-- Description:	Reminder email for LPOP Confirmation
-- =============================================
CREATE PROCEDURE [dbo].[SP_LPOP_Reminder_Confirmation] 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @prodMonth VARCHAR(6),
			@lpopTiming VARCHAR(9);

	DECLARE @longGAPday int;
	DECLARE @contentID VARCHAR(2)
	DECLARE @position int;
	DECLARE @formattedProdMonth varchar(20);
	
	DECLARE @startRemindingDate DATE;
	DECLARE @endRemindingDate DATE;
	DECLARE @tentativeDate DATE;
	DECLARE @compareGetDate DATE;
	DECLARE @BEGIN_FEEDBACK DATE;
	DECLARE @END_FEEDBACK DATE;
	DECLARE @FORECAST_VOL VARCHAR(30);
	DECLARE @timingName VARCHAR(10);

	DECLARE @realGetDate DATE = CAST(GETDATE() AS DATE);
	DECLARE @minID int = 0, @maxID int = 0;
	DECLARE @M_SUBJECT_FIX VARCHAR(MAX) = ''; --contains master template
	DECLARE @M_CONTENT_FIX VARCHAR(MAX) = ''; --contains master template

	DECLARE @F_SUBJECT_FIX VARCHAR(MAX) = ''; --contains masked template
	DECLARE @F_CONTENT_FIX VARCHAR(MAX) = ''; --contains masked template

	DECLARE @PCD_SUBJECT VARCHAR(MAX) = '';
	DECLARE @PCD_CONTENT VARCHAR(MAX) = '';

	SELECT TOP 1 
		@startRemindingDate = C.FEEDBACK_DUE_DATE, 
		@endRemindingDate = C.LAST_REMINDING_DATE, 
		@prodMonth = C.PRODUCTION_MONTH,
		@lpopTiming = C.LPOP_TIMING
	FROM TB_R_LPOP_CONFIRMATION C
	ORDER BY C.APPROVE_DT DESC

	IF(@lpopTiming = 'T')
	BEGIN
		SET @longGAPday = 3;
		SET @contentID = '34';
		SET @timingName = 'Tentative';
	END
	ELSE
	BEGIN
		SET @longGAPday = 5;
		SET @contentID = '31';
		SET @timingName = 'Firm';
	END

	SET @compareGetDate = DATEADD(DAY, 1, CAST(@startRemindingDate AS DATE)); --start compare date, approvedate + 1
	SET @tentativeDate = DATEADD(DAY, @longGAPday, @startRemindingDate); --end compare date, approvedate + @logGAPDay according to @lpopTiming
	
	--==if getdate() >= @comparegetdate, then confirmation already late for 1 day.
	IF(@realGetDate BETWEEN @compareGetDate AND @endRemindingDate)
	BEGIN

		DECLARE @suppcd VARCHAR(100), @plant varchar(100), @addresses VARCHAR(MAX) = ' ';

		SET @position = DATEDIFF(DAY,@compareGetDate, @realGetDate) + 1;--get GAP between compareGetDate and real get date now, + 1 because compareGetDate can equal to real get date
		
		--IF lpop timing is F and datediff is 4, change datediff to 3.
		--because recipient is still untill manager (3), 4 is general manager
		IF(@lpopTiming = 'F' AND @position > 4)
		BEGIN
			SET @position = 4;
		END
		ELSE IF (@lpopTiming = 'T' AND @position > 3)
		BEGIN
			SET @position = 3;
		END
		

		SET @BEGIN_FEEDBACK = CAST(LEFT(@prodMonth, 4) + '-' + RIGHT(@prodMonth, 2) + '-01' AS DATE)
		SET @END_FEEDBACK = DATEADD(mm, 3, @BEGIN_FEEDBACK)
		SET @FORECAST_VOL = (SELECT DATENAME(MONTH, @BEGIN_FEEDBACK) + ' ' + CAST(YEAR(@BEGIN_FEEDBACK) AS VARCHAR) + ' - ' + 
									DATENAME(MONTH, @END_FEEDBACK) + ' ' + CAST(YEAR(@END_FEEDBACK) AS VARCHAR))
		
		--formating prodmonth
		SELECT @formattedProdMonth = DATENAME(MONTH, CAST(@prodMonth + '01' AS DATE)) + ' ' + LEFT(@prodMonth, 4)

		--get template email for supplier
		SELECT @M_SUBJECT_FIX =  NOTIFICATION_SUBJECT, @M_CONTENT_FIX = NOTIFICATION_CONTENT
			FROM TB_M_NOTIFICATION_CONTENT
		WHERE FUNCTION_ID = '11005' AND ROLE_ID = @contentID AND NOTIFICATION_METHOD = '1'


		--get template email for PCD
		SELECT @PCD_SUBJECT =  NOTIFICATION_SUBJECT, @PCD_CONTENT = NOTIFICATION_CONTENT
			FROM TB_M_NOTIFICATION_CONTENT
		WHERE FUNCTION_ID = '11005' AND ROLE_ID = '99' AND NOTIFICATION_METHOD = '1'


		--get the data
		SELECT
			ROW_NUMBER() OVER(ORDER BY a.SUPPLIER_CD) AS NUM, a.SUPPLIER_CD AS SUPPLIER_CD, a.SUPPLIER_PLANT
		INTO #datas
		FROM TB_R_MONTHLY_FORECAST a 
		INNER JOIN dbo.TB_R_LPOP_CONFIRMATION b ON a.PRODUCTION_MONTH = b.PRODUCTION_MONTH
		WHERE 
			CASE @lpopTiming 
				WHEN 'T' THEN T_FEEDBACK_DT
				ELSE F_FEEDBACK_DT
			END IS NULL AND		
			a.PRODUCTION_MONTH = @prodMonth AND b.LPOP_TIMING = @lpopTiming
			AND EXISTS (
              SELECT ''
              FROM tb_r_supplier_feedback f
              WHERE f.PRODUCTION_MONTH = a.PRODUCTION_MONTH
                     AND f.SUPPLIER_CD = a.SUPPLIER_CD
                     AND f.FORECAST_TYPE = @lpopTiming
              )

		GROUP BY a.SUPPLIER_CD, a.SUPPLIER_PLANT

		

		SELECT @maxID = MAX(NUM) FROM #datas

		--insert data for PCD to #datas
		INSERT INTO #datas VALUES(@maxID + 1, '9999', '0');
		
		SELECT @minID = MIN(NUM), @maxID = MAX(NUM) FROM #datas
		--SELECT MIN(NUM), MAX(NUM) FROM #datas

		WHILE (@minID <= @maxID)
		BEGIN
			SELECT
				@suppcd = s.SUPPLIER_CD,
				@plant = s.SUPPLIER_PLANT
			FROM #datas s
			WHERE
				s.NUM = @minID
			
			--
			IF(@suppcd != '9999')
			BEGIN
				--set subject and object mail
				SET @F_SUBJECT_FIX = REPLACE(@M_SUBJECT_FIX, '@production_month', @formattedProdMonth);
				SET @F_CONTENT_FIX = REPLACE(@M_CONTENT_FIX, '@suppCode', @suppcd);
				SET @F_CONTENT_FIX = REPLACE(@F_CONTENT_FIX, '@production_month', @formattedProdMonth);
				SET @F_CONTENT_FIX = REPLACE(@F_CONTENT_FIX, '@date', CONVERT(VARCHAR(11),@endRemindingDate,106));
				SET @F_CONTENT_FIX = REPLACE(@F_CONTENT_FIX, '@plantcd', @plant);
				SET @F_CONTENT_FIX = REPLACE(@F_CONTENT_FIX, '@forecastvol', @FORECAST_VOL)
			END
			ELSE
			BEGIN
				--set subject and object mail
				SET @F_SUBJECT_FIX = REPLACE(@PCD_SUBJECT, '@production_month', @formattedProdMonth);
				SET @F_SUBJECT_FIX = REPLACE(@F_SUBJECT_FIX, '@TIMING', @timingName);
				SET @F_CONTENT_FIX = REPLACE(@PCD_CONTENT, '@production_month', @formattedProdMonth);
				SET @F_CONTENT_FIX = REPLACE(@F_CONTENT_FIX, '@date', CONVERT(VARCHAR(11),@endRemindingDate,106));
				SET @F_CONTENT_FIX = REPLACE(@F_CONTENT_FIX, '@forecastvol', @FORECAST_VOL)

			END
			--get addreses

			SET @addresses = (SELECT DISTINCT SUBSTRING(
											(
												SELECT ';'+ E2.EMAIL AS [text()]
												FROM 
													TB_M_SUPPLIER_EMAIL E2
												WHERE E1.SUPPLIER_CD = E2.SUPPLIER_CD
												AND E2.POSITION_CD <= @position
												FOR XML PATH ('')				
											), 2, 1000
			
										  ) EMAILADD
			FROM 
				TB_M_SUPPLIER_EMAIL E1
			WHERE 
				E1.SUPPLIER_CD = @suppcd AND
				E1.POSITION_CD <= @position AND
				E1.AREA_CD = 1)
				
			
			--send the email
			
			IF(ISNULL(@addresses, '')!='')
			BEGIN
			
				EXEC msdb.dbo.sp_send_dbmail
					@profile_name = 'IPPCS NOTIFICATION',
					@body = @F_CONTENT_FIX,
					@body_format = 'HTML',
					@recipients = @addresses,
					@subject = @F_SUBJECT_FIX;
			
				END
	
			SET @minID = @minID + 1; --set pointer to next
		END
		

	END






END

