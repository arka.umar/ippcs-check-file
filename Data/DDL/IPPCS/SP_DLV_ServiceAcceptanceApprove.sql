/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Service acceptance approve
*/

CREATE PROCEDURE [dbo].[SP_DLV_ServiceAcceptanceApprove]
   @UserId varchar(50),
   @SourceString VARCHAR(MAX),
   @DLVT varchar(max)='',
   @LP VARCHAR(MAX)='',
   @RC varchar(max)='',
   @ST varchar(max)='',
   @DTFROM varchar(max)='',
   @DTTO varchar(max)='',
   @PNO varchar(max)='',
   @ischeckall varchar(max)=''
AS
BEGIN

	DECLARE @x XML;
	SET @x = '<r>' + REPLACE((SELECT REPLACE(@SourceString, ''',''', ',') FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	DECLARE @a XML;
	SET @a = '<r>' + REPLACE((SELECT REPLACE(@DLVT, ''',''', ',') FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	DECLARE @b XML;
	SET @b = '<r>' + REPLACE((SELECT REPLACE(@LP, ''',''', ',') FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	DECLARE @c XML;
	SET @c = '<r>' + REPLACE((SELECT REPLACE(@RC, ''',''', ',') FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	DECLARE @d XML;
	SET @d = '<r>' + REPLACE((SELECT REPLACE(@ST, ''',''', ',') FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	DECLARE @CHK_STS as INT;
	DECLARE @CHK_PO as INT;
	--DECLARE @count as INT = (SELECT COUNT(DISTINCT DELIVERY_NO)FROM TB_R_DELIVERY_CTL_H a LEFT JOIN TB_R_DLV_PO_ITEM b ON b.ROUTE_CD = a.ROUTE AND SUBSTRING(b.PO_NO, 1,3) = a.LP_CD JOIN TB_R_DLV_PO_H c ON c.PROD_MONTH = CAST(YEAR(a.PICKUP_DT) as varchar(4))+''+dbo.FN_VARMONTH(a.PICKUP_DT) WHERE a.[DELIVERY_NO] IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol)))
	DECLARE @count as INT
	DECLARE @MODULE_ID as varchar(1) = '3'
	DECLARE @FUNCTION_ID as varchar(5) = '31302'
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_ServiceAcceptanceApprove'
	DECLARE @step AS VARCHAR(50) = 'init'
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)
	DECLARE @T AS TABLE (PID BIGINT)
	--DECLARE @PROCESS_STATUS as varchar(3) = (SELECT TOP 1 PROCESS_STATUS FROM TB_R_DLV_QUEUE WHERE MODULE_ID = @MODULE_ID AND FUNCTION_ID = @FUNCTION_ID ORDER BY PROCESS_ID DESC);
	DECLARE @errData as int = 0;
	Declare @@sql varchar(max)
	DECLARE @isOK as INT = 0;
	DECLARE @isNG as INT = 0;
	DECLARE @isNomatch as INT = 0;
	DECLARE @isNotGood as int = 0;
	DECLARE @pr as int = 0;
	DECLARE @COUNT_STATUS INT = (SELECT 
									COUNT(PROCESS_STATUS) 
								 FROM TB_R_DLV_QUEUE 
								 WHERE MODULE_ID = @MODULE_ID 
									   AND FUNCTION_ID = @FUNCTION_ID
									   AND PROCESS_STATUS NOT IN ('QU3', 'QU4'));

	SET @log = 'Approve Service Acceptance Process Started By '+@na;
	INSERT @T
	EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		
	SET @log = 'Registering into TB_R_DLV_QUEUE, pid : '+CAST(@pid as varchar(max))+' by '+@na;
	INSERT @T
	EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.Queueing', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODULE_ID, @FUNCTION_ID;

	INSERT INTO TB_R_DLV_QUEUE(PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, PROCESS_END_DT, CREATED_BY, CREATED_DT)
	SELECT @pid as PROCESS_ID, @MODULE_ID, @FUNCTION_ID, 'QU1', GETDATE(), NULL, @UserId, GETDATE()

	--IF(@PROCESS_STATUS = 'QU4' OR @PROCESS_STATUS = 'QU3' OR @PROCESS_STATUS IS NULL)
	IF(ISNULL(@COUNT_STATUS, 0) <= 0)
	BEGIN
			
		IF(@ischeckall='true')
			BEGIN
				--BEGIN TRAN
				SET @log = 'Updating CTL_H By '+@na+@SourceString;
				INSERT @T
				EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.checkall', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODULE_ID, @FUNCTION_ID;
			
				SET @@sql = 'UPDATE a SET a.DELIVERY_STS = ''Approved'', a.PROCESS_ID = '''+CAST(@pid as varchar(max))+''', a.APPROVE_BY = ''' + @UserId + ''', a.APPROVE_DT = GETDATE(), a.CHANGED_BY = ''' + @UserId + ''', a.CHANGED_DT = GETDATE() FROM TB_R_DELIVERY_CTL_H a
				LEFT JOIN TB_R_DLV_PO_H e ON e.PROD_MONTH = CAST(YEAR(a.PICKUP_DT) as VARCHAR(4))+dbo.FN_VARMONTH(a.PICKUP_DT) AND SUBSTRING(e.PO_NO, 1,3) = a.LP_CD AND (e.PO_STATUS_FLAG = ''PO4'' OR e.PO_STATUS_FLAG=''PO6'') WHERE 1=1 ';
			  
			  if(@SourceString <> '')
				set @@sql = @@sql + 'and a.[DELIVERY_NO] LIKE '''+ cast(@SourceString as varchar(max)) +'%'' '

			  if(@DLVT <> '')
				set @@sql = @@sql + 'and LEFT(a.[DELIVERY_NO],1) IN ('+ cast(@DLVT as varchar(max)) +') '
				
			  if(@LP <> '')
				set @@sql = @@sql + 'and a.[LP_CD] IN ('+ cast (@LP as varchar(max)) +') '

			  if(@RC <> '')
				set @@sql = @@sql + 'and a.[ROUTE] IN ('+ cast (@RC as varchar(max)) + ') '

			  if(@ST <> '')
				set @@sql = @@sql + 'and a.[DELIVERY_STS] IN ('+ cast (@ST as varchar(max)) + ') '

			  if(@PNO <> '')
				set @@sql = @@sql + 'and e.[PO_NO] LIKE '''+ cast (@PNO as varchar(max)) + ''' '
			
			  if(@DTFROM <> '' AND @DTTO <>'')
				set @@sql = @@sql + 'and (a.[PICKUP_DT] BETWEEN '+@DTFROM+' AND '+@DTTO+' )'
				
			  execute(@@sql);
			  
			END
			ELSE
			BEGIN
				SET @CHK_STS = (SELECT COUNT(DELIVERY_STS) FROM TB_R_DELIVERY_CTL_H WHERE DELIVERY_NO IN(SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol)) AND DELIVERY_STS NOT IN ('Delivered', 'Error Posting', 'Canceled'))
			
				IF(@CHK_STS>0)
				BEGIN
					SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Service acceptance cannot be approved, only if delivery status is delivered, Error Posting and canceled.' as [MESSAGE]
					SET @errData = 1;
					SET @log = 'Service acceptance cannot be approved, only if delivery status is delivered, Error Posting and canceled. by '+@na;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.End', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE(), CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
					UPDATE TB_R_DELIVERY_CTL_H SET PROCESS_ID = NULL WHERE DELIVERY_NO IN(SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol))
				END
			
				
				IF(@errData = 0)
				BEGIN
					SET @log = 'Updating CTL_H By '+@na+@SourceString;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.processing', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODULE_ID, @FUNCTION_ID;
					UPDATE a SET a.DELIVERY_STS = 'Approved', a.PROCESS_ID = NULL, a.APPROVE_BY = @UserId, a.APPROVE_DT = GETDATE(), a.CHANGED_BY = @UserId, a.CHANGED_DT = GETDATE() 
					FROM TB_R_DELIVERY_CTL_H a WHERE a.[DELIVERY_NO] IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol))
				END
			END
			
			IF(@errData = 0)
			BEGIN
				--SET @log = 'Registering background task By '+@na;
				--INSERT @T
				--EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.processing', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
				
				
				--insert into tb_r_background_task_registry
				--values (
				--	@pid,--ID
				--	'DeliverySAPostingTaskRuntime',--NAME
				--	'Background Task Service Acceptance Posting ',--DESCRIPTION
				--	'System',	--SUBMITTER
				--	'Service Acceptance Posting',	--FUNCTION_NAME
				--	'{&quot;ProcessID&quot;:&quot;' + CAST(@pid as varchar)  + '&quot;&quot;Username&quot;:&quot;' + CAST(@UserId as varchar)  + '&quot;}',--PARAMETER
				--		0,	--TYPE
				--		0,	--STATUS
				--	'C:\Background_Task\Tasks\DeliverySAPostingTaskRuntime.exe',--COMMAND
				--	0,       --START_DATE
				--	0,		--END_DATE
					
				--	4,		--PERIODIC_TYPE
				--	null,		--INTERVAL
				--	'',		--EXECUTION_DAYS
				--	'',	--EXECUTION_MONTHS
				--	6840000 
				--)
				
				--SET @log = 'Registering background task completed By '+@na;
				--INSERT @T
				--EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.processing', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
				
				
				SET @log = 'Service Acceptance Approval has been processed. By '+@na;
				INSERT @T
				EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.End', @pid OUTPUT, 'MPCS00003INF', 'INF', @MODULE_ID, @FUNCTION_ID;
				UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU3', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
				SELECT 'success' as [STATUS], 'INF' as [TYPE], 'Approval finished' as [MESSAGE]
				UPDATE TB_R_DELIVERY_CTL_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
			END
			
	END
	ELSE
	BEGIN
		SET @log = 'Another process has already running. by '+@na;
		INSERT @T
		EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceApprove.End', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
		UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
		SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Another process has already running.' as [MESSAGE]	
		UPDATE TB_R_DELIVERY_CTL_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
	END
	
	
	
	
END

