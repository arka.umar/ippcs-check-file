CREATE PROCEDURE [dbo].[LPOP_POErrorCreationMonthlySummary] 
	@PROD_MONTH VARCHAR(6)
AS
BEGIN
DECLARE @SQL NVARCHAR(MAX) = ''

IF OBJECT_ID('tempdb..#dps') IS NOT NULL 
		DROP TABLE #dps

TRUNCATE TABLE TB_R_LPOP_PO_ERROR
SET @SQL = 'INSERT INTO TB_R_LPOP_PO_ERROR
				(
					MAT_NO,
					SOURCE_TYPE,
					PROD_PURPOSE_CD,
					PLANT_CD,
					SLOC_CD,
					SUPP_CD,
					ERR_CD,
					ERR_MSG,
					ERR_DT,
					PROD_MONTH,
					CREATED_BY,
					CREATED_DT
				)
			SELECT 
				MAT_NO,
				SOURCE_TYPE,
				PROD_PURPOSE_CD,
				PLANT_CD,
				SLOC_CD,
				SUPP_CD,
				ERR_CD,
				ERR_MSG,
				ERR_DT as ERR_DT,
				PROD_MONTH,
				''LPOPPOJob'',
				GETDATE()
			FROM OPENQUERY([IPPCS_TO_ICS], ''
										select distinct
											h.mat_no,
											h.source_type,
											h.prod_purpose_cd,
											h.plant_cd,
											h.sloc_cd,
											h.supp_cd,
											d.err_cd,
											d.err_msg,
											d.err_dt,
											h.prod_month 
										from pricu001.tb_r_err_posting_h h
											join pricu001.tb_r_err_posting_d d
												on d.err_id = h.err_id
										where h.system_source = ''''IPPCS'''' 
											  and h.transaction_type is null 
											  and h.changed_by <> ''''SYSTEM'''' 
											  and h.prod_month = ''''' + @PROD_MONTH + '''''
											  '')
					'

EXEC sp_executesql @SQL
END

