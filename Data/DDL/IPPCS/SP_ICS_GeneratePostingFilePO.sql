-- =============================================
-- Author		: Rahmat Setiawan (Arkamaya)
-- Create date	: 17 September 2013
-- Description	: Query data for transfer to ICS table TB_T_LOCAL_ORDER
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_GeneratePostingFilePO]
	@ProductionMonth Varchar(6),
	@Version Char(1),
	@processId BIGINT,
	@UserID VARCHAR(20)

AS
BEGIN
	DECLARE @dockServicePart VARCHAR(20) = '',
			@prodPurposeServicePart VARCHAR(20) = '',
			@generalProdPurpose VARCHAR(20) = ''

	SELECT @dockServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DOCK_SERVICE_PART'
	SELECT @prodPurposeServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'PROD_PURPOSE_GR_SERVICE_PART'
	SELECT @generalProdPurpose = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '11004' AND SYSTEM_CD = 'PO_PROD_PURPOSE_CD'

	DECLARE @dockServicePartTable AS TABLE (DOCK VARCHAR(2))
	INSERT INTO @dockServicePartTable (DOCK)
    EXEC dbo.SplitString @dockServicePart, ','

	--Add supplier Stamping Steel Project
	--20191104 by hesti
	DECLARE @SupplierSPRINTS VARCHAR(100) = ''
	SELECT @SupplierSPRINTS = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'PO_SUPPLIER_SPRINTS'	
	DECLARE @SupplierSPRINTSTable AS TABLE (SUPPLIER_CD VARCHAR(4))
	INSERT INTO @SupplierSPRINTSTable (SUPPLIER_CD)
        EXEC dbo.SplitString @SupplierSPRINTS, ','

	--Customize for Stamping Steel Project
	DECLARE @dockStampingSteel VARCHAR (20)= '', @sourceTypeStampingSteel VARCHAR (20)= ''
	SELECT @dockStampingSteel = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DOCK_STAMPING_STEEL'
	SELECT @sourceTypeStampingSteel = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'SOURCE_TYPE_PO_STAMPING_STEEL'
	DECLARE @dockStampingSteelTable AS TABLE (DOCK VARCHAR(2))
	INSERT INTO @dockStampingSteelTable (DOCK)
	EXEC dbo.SplitString @dockStampingSteel, ',' 
	--End Customize for Stamping Steel Project

	--Customize for Stamping Sheet Project
	DECLARE @dockStampingCutt VARCHAR (20)= '', @sourceTypeStampingCutt VARCHAR (20)= ''
	SELECT @dockStampingCutt = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DOCK_STAMPING_CUTT'
	SELECT @sourceTypeStampingCutt = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'SOURCE_TYPE_PO_STAMPING_CUTT'
	DECLARE @dockStampingCuttTable AS TABLE (DOCK VARCHAR(2))
	INSERT INTO @dockStampingCuttTable (DOCK)
	EXEC dbo.SplitString @dockStampingCutt, ',' 
	--End Customize for Stamping Sheet Project

	IF EXISTS(SELECT 1 FROM TB_R_LPOP_CONFIRMATION WHERE PRODUCTION_MONTH = @ProductionMonth)
	BEGIN
			--DELETE OPENQUERY ([IPPCS_TO_ICS], 'SELECT * FROM QAICU001.TB_T_LOCAL_ORDER ');
			--DELETE OPENQUERY ([IPPCS_TO_ICS], 'SELECT * FROM QAICU001.TB_R_FINISHED_CREATE_PO ')
			PRINT '#1' 
			INSERT OPENQUERY (
				[IPPCS_TO_ICS]
				,'SELECT DOCK_CD, 
				SUPP_CD, 
				MAT_NO, 
				PROD_PURPOSE_CD, 
				SOURCE_TYPE, 
				MAT_DESC, 
				PROD_MONTH, 
				PART_COLOR_SFX, 
				QTY_N, 
				QTY_N1, 
				QTY_N2, 
				QTY_N3, 
				OTHER_PROCESS_ID 
				FROM TB_T_LOCAL_ORDER'
				)
			SELECT DOCK_CD dockCode --1
				,SUPPLIER_CD suppCode --2
				,matNo = SUBSTRING(PART_NO, 1, 10) --3
				--,prodPurposeCode = (
				--	SELECT SYSTEM_VALUE
				--	FROM TB_M_SYSTEM
				--	WHERE FUNCTION_ID = '11004'
				--		AND SYSTEM_CD = 'PO_PROD_PURPOSE_CD'
				--	),										--4

				,CASE WHEN (SELECT 1 WHERE DOCK_CD IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
					THEN @prodPurposeServicePart
					ELSE
						@generalProdPurpose
					END
					  AS prodPurposeCode

				--,sourceType = (
				--	SELECT SYSTEM_VALUE
				--	FROM TB_M_SYSTEM
				--	WHERE FUNCTION_ID = '11004'
				--		AND SYSTEM_CD = 'PO_SOURCE_TYPE'
				--	)
														
				,CASE WHEN (SELECT 1 WHERE DOCK_CD IN (SELECT DOCK FROM @dockStampingSteelTable)) = 1 
					THEN @sourceTypeStampingSteel
					WHEN (SELECT 1 WHERE DOCK_CD IN (SELECT DOCK FROM @dockStampingCuttTable)) = 1 
					THEN @sourceTypeStampingCutt
					ELSE
					(SELECT SYSTEM_VALUE
					FROM TB_M_SYSTEM
					WHERE FUNCTION_ID = '11004'
						AND SYSTEM_CD = 'PO_SOURCE_TYPE'
					) 
					END AS sourceType
														    --5
				,SUBSTRING(PART_NAME,1,30) matDesc			--6 
				,PACK_MONTH prodMonth						--7
				,partColorSfx = SUBSTRING(PART_NO, 11, 2)	--8
				,N_VOLUME qtyN								--9
				,N_1_VOLUME qtyN1							--10
				,N_2_VOLUME qtyN2							--11
				,N_3_VOLUME qtyN3							--12
				,@processId									--13
			FROM TB_R_LPOP_SUM
			WHERE PACK_MONTH = @ProductionMonth AND VERS = @Version AND N_VOLUME <> 0
			--modify by hesti, 20191002, tambahan ID agar tdk menyatu dg sprints
			--Add filter supplier sprints 20191104
			AND SUPPLIER_CD NOT IN (SELECT SUPPLIER_CD FROM @SupplierSPRINTSTable) 
			
			PRINT '#2' 
			INSERT OPENQUERY (IPPCS_TO_ICS, 
							'SELECT  
								OTHER_PROCESS_ID,
								STATUS,
								REFERENCE,
								CREATED_BY,
								CREATED_DT
						     FROM TB_R_PCS_INTERFACE_STATUS')
            VALUES (@processId, 0, 'Posting Create PO',@UserID, GETDATE());
			PRINT '#3' 
			INSERT INTO [dbo].[TB_R_ICS_QUEUE]
					   ([IPPCS_MODUL]
					   ,[IPPCS_FUNCTION]
					   ,[PROCESS_ID]
					   ,[PROCESS_STATUS]
					   ,[REMARK]
					   ,[CREATED_BY]
					   ,[CREATED_DT])
            VALUES
			   ('1'
			   ,'11004'
			   ,@processId
			   ,0
			   ,@ProductionMonth
			   ,@UserID
			   ,GETDATE());
			 PRINT '#4' 
	END
END

