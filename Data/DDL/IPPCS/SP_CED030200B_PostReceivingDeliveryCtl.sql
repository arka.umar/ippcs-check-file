-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-05-02
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030200B_PostReceivingDeliveryCtl] @DELIVERY_NO VARCHAR(max) = ''
	,@DOCK_CD VARCHAR(max) = ''
	,@DELIVERY_STS VARCHAR(max) = ''
	,@ARRIVAL_GATE_DT VARCHAR(max) = ''
	,@ARRIVAL_ACTUAL_DT VARCHAR(max) = ''
	,@ARRIVAL_STATUS VARCHAR(max) = ''
	,@ARRIVAL_GAP VARCHAR(max) = ''
	,@DEPARTURE_ACTUAL_DT VARCHAR(max) = ''
	,@DEPARTURE_GAP VARCHAR(max) = ''
	,@DEPARTURE_STATUS VARCHAR(max) = ''
	,@REMAINING VARCHAR(max) = ''
	,@CREATED_BY VARCHAR(max) = ''
	,@PROCESS_ID VARCHAR(max) = ''
AS
BEGIN
	-- FID.Ridwan : 2019-11-25 apply log monitoring
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @MODULE_ID AS VARCHAR(1) = '2'
	DECLARE @FUNCTION_ID AS VARCHAR(5) = '23003'
	DECLARE @R_SEQ_NO INT
	DECLARE @TB_T_LOG_D AS TABLE (
		PROCESS_ID BIGINT
		,SEQUENCE_NUMBER INT
		,MESSAGE_ID VARCHAR(12)
		,MESSAGE_TYPE VARCHAR(3)
		,[MESSAGE] VARCHAR(MAX)
		,LOCATION VARCHAR(MAX)
		,CREATED_BY VARCHAR(20)
		,CREATED_DATE DATETIME
		)

	DECLARE @MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
	DECLARE @PROCESS_ID_IPPCS AS TABLE (PROCESS_ID_IPPCS BIGINT)

	CREATE TABLE #TB_List_Message (
		Result VARCHAR(MAX)
		,Message VARCHAR(MAX)
		)
		
	SET @log = 'Starting [Send Receiving data (Delivery CTL)]';

	INSERT @T
	EXEC dbo.sp_PutLog @log
		,'SYSTEM'
		,'SendReceivingDataDeliveryCTL.init'
		,@pid OUTPUT
		,'MPCS00002INF'
		,'INF'
		,@MODULE_ID
		,@FUNCTION_ID;

	BEGIN TRY
		SET @log = 'Iteration : Check PID yang masih aktif';

		INSERT INTO @TB_T_LOG_D
		SELECT @pid
			,NULL
			,'MPCS00002INF'
			,'INF'
			,@log
			,'SendReceivingDataDeliveryCTL'
			,'SYSTEM'
			,GETDATE()

		-- Check PID yang masih aktif
		INSERT INTO @PROCESS_ID_IPPCS
		SELECT TOP 1 PROCESS_ID
		FROM TB_T_CED_SEND_DELIVERY_DCL
		WHERE ISNULL(FINISH_FLAG, 'N') <> 'Y'
		ORDER BY PROCESS_ID

		IF EXISTS (
				SELECT ''
				FROM @PROCESS_ID_IPPCS
				)
		BEGIN
			SET @PARAM1 = (
					SELECT *
					FROM @PROCESS_ID_IPPCS
					)

			INSERT INTO #TB_List_Message (
				Result
				,Message
				)
			SELECT 'Failed'
				,'Send Data Receiving with Process ID ' + @PARAM1 + ' not finished yet in IPPCS';
				
			SET @log = 'Iteration : Send Data Receiving with Process ID ' + @PARAM1 + ' not finished yet in IPPCS';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingDataDeliveryCTL'
				,'SYSTEM'
				,GETDATE()

		END
		ELSE
		BEGIN
			SET @log = 'Iteration : Get list from TVEST (Delivey CTL)';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingDataDeliveryCTL'
				,'SYSTEM'
				,GETDATE()

			SELECT *
			INTO #TB_List_DeliveryNo
			FROM dbo.fnSplitString(@DELIVERY_NO, '|')

			SELECT *
			INTO #TB_List_DockCd
			FROM dbo.fnSplitString(@DOCK_CD, '|')

			SELECT *
			INTO #TB_List_DeliverySts
			FROM dbo.fnSplitString(@DELIVERY_STS, '|')

			SELECT *
			INTO #TB_List_ArrivalGateDate
			FROM dbo.fnSplitString(@ARRIVAL_GATE_DT, '|')

			SELECT *
			INTO #TB_List_ArrivalActualDate
			FROM dbo.fnSplitString(@ARRIVAL_ACTUAL_DT, '|')

			SELECT *
			INTO #TB_List_ArrivalSts
			FROM dbo.fnSplitString(@ARRIVAL_STATUS, '|')

			SELECT *
			INTO #TB_List_ArrivalGap
			FROM dbo.fnSplitString(@ARRIVAL_GAP, '|')

			SELECT *
			INTO #TB_List_DepartureActualDate
			FROM dbo.fnSplitString(@DEPARTURE_ACTUAL_DT, '|')

			SELECT *
			INTO #TB_List_DepartureSts
			FROM dbo.fnSplitString(@DEPARTURE_STATUS, '|')

			SELECT *
			INTO #TB_List_DepartureGap
			FROM dbo.fnSplitString(@DEPARTURE_GAP, '|')

			SELECT *
			INTO #TB_List_Remaining
			FROM dbo.fnSplitString(@REMAINING, '|')

			SELECT *
			INTO #TB_List_CreatedBy
			FROM dbo.fnSplitString(@CREATED_BY, '|')

			SELECT *
			INTO #TB_List_ProcessId
			FROM dbo.fnSplitString(@PROCESS_ID, '|')

			SELECT A.zeroBasedOccurance + 1 AS ROWNO
				,A.s AS DeliveryNo
				,B.s AS DockCd
				,C.s AS DeliverySts
				,D.s AS ArrivalGateDate
				,E.s AS ArrivalActualDate
				,F.s AS ArrivalSts
				,G.s AS ArrivalGap
				,H.s AS DepartureActualDate
				,I.s AS DepartureSts
				,J.s AS DepartureGap
				,K.s AS Remaining
				,L.s AS CreatedBy
				,M.s AS ProcessId
			INTO #tb_t_list_final
			FROM #TB_List_DeliveryNo A
			LEFT JOIN #TB_List_DockCd B ON A.zeroBasedOccurance = B.zeroBasedOccurance
			LEFT JOIN #TB_List_DeliverySts C ON A.zeroBasedOccurance = C.zeroBasedOccurance
			LEFT JOIN #TB_List_ArrivalGateDate D ON A.zeroBasedOccurance = D.zeroBasedOccurance
			LEFT JOIN #TB_List_ArrivalActualDate E ON A.zeroBasedOccurance = E.zeroBasedOccurance
			LEFT JOIN #TB_List_ArrivalSts F ON A.zeroBasedOccurance = F.zeroBasedOccurance
			LEFT JOIN #TB_List_ArrivalGap G ON A.zeroBasedOccurance = G.zeroBasedOccurance
			LEFT JOIN #TB_List_DepartureActualDate H ON A.zeroBasedOccurance = H.zeroBasedOccurance
			LEFT JOIN #TB_List_DepartureSts I ON A.zeroBasedOccurance = I.zeroBasedOccurance
			LEFT JOIN #TB_List_DepartureGap J ON A.zeroBasedOccurance = J.zeroBasedOccurance
			LEFT JOIN #TB_List_Remaining K ON A.zeroBasedOccurance = K.zeroBasedOccurance
			LEFT JOIN #TB_List_CreatedBy L ON A.zeroBasedOccurance = L.zeroBasedOccurance
			LEFT JOIN #TB_List_ProcessId M ON A.zeroBasedOccurance = M.zeroBasedOccurance
			
			SET @log = 'Iteration : Delete IPPCS.TB_T_CED_SEND_DELIVERY_DCL';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingDataDeliveryCTL'
				,'SYSTEM'
				,GETDATE()

			DELETE
			FROM TB_T_CED_SEND_DELIVERY_DCL
			
			SET @log = 'Iteration : Insert IPPCS.TB_T_CED_SEND_DELIVERY_DCL';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingDataDeliveryCTL'
				,'SYSTEM'
				,GETDATE()

			INSERT INTO TB_T_CED_SEND_DELIVERY_DCL (
				DELIVERY_NO
				,DOCK_CD
				,DELIVERY_STS
				,ARRIVAL_GATE_DT
				,ARRIVAL_ACTUAL_DT
				,ARRIVAL_STATUS
				,ARRIVAL_GAP
				,DEPARTURE_ACTUAL_DT
				,DEPARTURE_GAP
				,DEPARTURE_STATUS
				,REMAINING
				,CREATED_BY
				,CREATED_DT
				,PROCESS_ID
				,FINISH_FLAG
				,FINISH_DATE
				)
			SELECT DeliveryNo
				,DockCd
				,DeliverySts
				,CONVERT(DATETIME, ArrivalGateDate) ArrivalGateDate
				,CONVERT(DATETIME, ArrivalActualDate) ArrivalActualDate
				,ArrivalSts
				,CONVERT(TIME, ArrivalGap) ArrivalGap
				,CONVERT(DATETIME, DepartureActualDate) DepartureActualDate
				,CONVERT(TIME, DepartureGap) DepartureGap
				,DepartureSts
				,CONVERT(TIME, Remaining) Remaining
				,CreatedBy
				,GETDATE() CreatedDt
				,ProcessId
				,'N'
				,NULL
			FROM #tb_t_list_final
			
			SET @log = 'Iteration : Update IPPCS.TB_R_DELIVERY_CTL_D';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingDataDeliveryCTL'
				,'SYSTEM'
				,GETDATE()

			-- Update IPPCS.TB_R_DELIVERY_CTL_D
			UPDATE D
			SET D.ARRIVAL_GATE_DT = T.ARRIVAL_GATE_DT
				,D.ARRIVAL_ACTUAL_DT = T.ARRIVAL_ACTUAL_DT
				,D.ARRIVAL_STATUS = T.ARRIVAL_STATUS
				,D.ARRIVAL_GAP = T.ARRIVAL_GAP
				,D.DEPARTURE_ACTUAL_DT = T.DEPARTURE_ACTUAL_DT
				,D.DEPARTURE_STATUS = T.DEPARTURE_STATUS
				,D.DEPARTURE_GAP = T.DEPARTURE_GAP
				,D.REMAINING = T.REMAINING
				,D.CHANGED_BY = T.CREATED_BY
				,D.CHANGED_DT = GETDATE()
			FROM TB_R_DELIVERY_CTL_D D
			JOIN (
				SELECT DISTINCT DELIVERY_NO
					,DOCK_CD
					,ARRIVAL_GATE_DT
					,ARRIVAL_ACTUAL_DT
					,ARRIVAL_STATUS
					,ARRIVAL_GAP
					,DEPARTURE_ACTUAL_DT
					,DEPARTURE_STATUS
					,DEPARTURE_GAP
					,REMAINING
					,CREATED_BY
				FROM TB_T_CED_SEND_DELIVERY_DCL
				) T ON D.DELIVERY_NO = T.DELIVERY_NO
				AND D.DOCK_CD = T.DOCK_CD
			
			SET @log = 'Iteration : Update IPPCS.TB_R_DELIVERY_CTL_H';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingDataDeliveryCTL'
				,'SYSTEM'
				,GETDATE()

			-- FID.Ridwan 2019-10-15 : change logic delivery status
			SELECT DISTINCT A.DELIVERY_NO, 'PARTIAL' AS TYPE
			INTO #TB_T_LIST_DELIVERY_NO
			FROM TB_T_CED_SEND_DELIVERY_DCL A 
			JOIN TB_R_DELIVERY_CTL_D B ON A.DELIVERY_NO = B.DELIVERY_NO
			WHERE ISNULL(B.DEPARTURE_ACTUAL_DT,'') = ''
	
			-- Updata IPPCS.TB_R_DELIVERY_CTL_H
			UPDATE H
			SET --H.DELIVERY_STS = T.DELIVERY_STS
				H.DELIVERY_STS = CASE WHEN CC.TYPE = 'PARTIAL' THEN 'Arrived' ELSE 'Delivered' END
				,H.CHANGED_BY = T.CREATED_BY
				,H.CHANGED_DT = GETDATE()
			FROM TB_R_DELIVERY_CTL_H H
			JOIN (
				SELECT DISTINCT DELIVERY_NO
					,DELIVERY_STS
					,CREATED_BY
				FROM TB_T_CED_SEND_DELIVERY_DCL
				) T ON H.DELIVERY_NO = T.DELIVERY_NO
			LEFT JOIN #TB_T_LIST_DELIVERY_NO CC ON H.DELIVERY_NO = CC.DELIVERY_NO
			
			SET @log = 'Iteration : Update Finish Flag IPPCS.TB_T_CED_SEND_DELIVERY_DCL';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingDataDeliveryCTL'
				,'SYSTEM'
				,GETDATE()

			--Update Finish Flag
			UPDATE TB_T_CED_SEND_DELIVERY_DCL
			SET FINISH_FLAG = 'Y'
				,FINISH_DATE = GETDATE()

			INSERT INTO #TB_List_Message (
				Result
				,Message
				)
			SELECT 'Success' Result
				,'Delivery Ctl updated successfully' Message			
		END
		
		SET @log = 'Finish [Send Receiving data (Delivery CTL)]';

		INSERT INTO @TB_T_LOG_D
		SELECT @pid
			,NULL
			,'MPCS00002INF'
			,'INF'
			,@log
			,'SendReceivingDataDeliveryCTL'
			,'SYSTEM'
			,GETDATE()

				
		SELECT TOP 1 @R_SEQ_NO = SEQUENCE_NUMBER
		FROM TB_R_LOG_D
		WHERE PROCESS_ID = @pid
		ORDER BY SEQUENCE_NUMBER DESC

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT PROCESS_ID
			,@R_SEQ_NO + ROW_NUMBER() OVER (
				ORDER BY PROCESS_ID
				)
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
		FROM @TB_T_LOG_D

		UPDATE TB_R_LOG_H
		SET PROCESS_STATUS = 1
		WHERE PROCESS_ID = @pid

		SELECT *
		FROM #TB_List_Message
	END TRY
	BEGIN CATCH
		SET @log = 'Finish With Error [Send Receiving data (Delivery CTL)]';

		INSERT INTO @TB_T_LOG_D
		SELECT @pid
			,NULL
			,'MPCS00002INF'
			,'INF'
			,@log
			,'SendReceivingData'
			,'SYSTEM'
			,GETDATE()

		SELECT TOP 1 @R_SEQ_NO = SEQUENCE_NUMBER
		FROM TB_R_LOG_D
		WHERE PROCESS_ID = @pid
		ORDER BY SEQUENCE_NUMBER DESC

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT PROCESS_ID
			,@R_SEQ_NO + ROW_NUMBER() OVER (
				ORDER BY PROCESS_ID
				)
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
		FROM @TB_T_LOG_D

		UPDATE TB_R_LOG_H
		SET PROCESS_STATUS = 6
		WHERE PROCESS_ID = @pid

		DECLARE @ErrorMessage NVARCHAR(4000);

		SET @ErrorMessage = ERROR_MESSAGE();

		INSERT INTO #TB_List_Message (
				Result
				,Message
				)
			SELECT 'Failed'
				,'Exception : ' + @ErrorMessage;

				
		SELECT *
		FROM #TB_List_Message
	END CATCH

	IF OBJECT_ID('tempdb..#TB_List_DeliveryNo') IS NOT NULL
		DROP TABLE #TB_List_DeliveryNo

	IF OBJECT_ID('tempdb..#TB_List_DockCd') IS NOT NULL
		DROP TABLE #TB_List_DockCd

	IF OBJECT_ID('tempdb..#TB_List_DeliverySts') IS NOT NULL
		DROP TABLE #TB_List_DeliverySts

	IF OBJECT_ID('tempdb..#TB_List_ArrivalGateDate') IS NOT NULL
		DROP TABLE #TB_List_ArrivalGateDate

	IF OBJECT_ID('tempdb..#TB_List_ArrivalActualDate') IS NOT NULL
		DROP TABLE #TB_List_ArrivalActualDate

	IF OBJECT_ID('tempdb..#TB_List_ArrivalSts') IS NOT NULL
		DROP TABLE #TB_List_ArrivalSts

	IF OBJECT_ID('tempdb..#TB_List_ArrivalGap') IS NOT NULL
		DROP TABLE #TB_List_ArrivalGap

	IF OBJECT_ID('tempdb..#TB_List_DepartureActualDate') IS NOT NULL
		DROP TABLE #TB_List_DepartureActualDate

	IF OBJECT_ID('tempdb..#TB_List_DepartureSts') IS NOT NULL
		DROP TABLE #TB_List_DepartureSts

	IF OBJECT_ID('tempdb..#TB_List_DepartureGap') IS NOT NULL
		DROP TABLE #TB_List_DepartureGap

	IF OBJECT_ID('tempdb..#TB_List_Remaining') IS NOT NULL
		DROP TABLE #TB_List_Remaining

	IF OBJECT_ID('tempdb..#TB_List_CreatedBy') IS NOT NULL
		DROP TABLE #TB_List_CreatedBy

	IF OBJECT_ID('tempdb..#TB_List_ProcessId') IS NOT NULL
		DROP TABLE #TB_List_ProcessId

	IF OBJECT_ID('tempdb..#tb_t_list_final') IS NOT NULL
		DROP TABLE #tb_t_list_final

	IF OBJECT_ID('tempdb..#TB_List_Message') IS NOT NULL
		DROP TABLE #TB_List_Message
END

