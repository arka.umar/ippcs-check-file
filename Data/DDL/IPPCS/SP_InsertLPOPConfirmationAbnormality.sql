CREATE PROCEDURE [dbo].[SP_InsertLPOPConfirmationAbnormality]    
	@PackMonth char(6),
	@DID char(4),
	@Vers char(1),
	@PartNo varchar(12),
	@SuppCd varchar(5),
	@ErrCd varchar(max)
AS  
BEGIN  
 DECLARE  
	@SuppNm AS VARCHAR(40),
	@PartNm AS VARCHAR(40),
	@NVolume AS VARCHAR(8),
	@N1Volume AS VARCHAR(8),
	@N2Volume AS VARCHAR(8),
	@N3Volume AS VARCHAR(8),
	
	@area AS VARCHAR(MAX),  
	@group AS VARCHAR(MAX),  
	@remark AS VARCHAR(MAX),  

	@sql AS NVARCHAR(MAX)        
    
    print(@ErrCd)
     
	SELECT  
		@area = AREA  
		,@group = [GROUP]  
		,@remark = REMARK	
	FROM TB_M_ERRORMAPPING E   
	WHERE MESSAGE_ID = @ErrCd  
	
	SELECT
		@SuppNm = SUPPLIER_NAME
		,@PartNm = PART_NAME
		,@NVolume = N_VOLUME
		,@N1Volume = N_1_VOLUME
		,@N2Volume = N_2_VOLUME
		,@N3Volume = N_3_VOLUME
	FROM TB_R_LPOP_SUM
	WHERE
		PACK_MONTH = @PackMonth
		AND D_ID = @DID
		AND VERS = @Vers
		AND PART_NO = @PartNo
		AND SUPPLIER_CD = @SuppCd
    
	SET @sql = '
		IF NOT EXISTS(
			SELECT *
			FROM TB_R_LPOP_ABNORMALITY
			WHERE 
				((PACK_MONTH=''' + @PackMonth + ''' AND ISNULL('''+ @PackMonth +''', '''') <> '''') or (ISNULL('''+ @PackMonth +''', '''') = ''''))     
				AND ((D_ID=''' + @DID + ''' AND ISNULL('''+ @DID +''', '''') <> '''') or (ISNULL('''+ @DID +''', '''') = ''''))           
				AND ((VERS=''' + @Vers + ''' AND ISNULL('''+ @Vers +''', '''') <> '''') or (ISNULL('''+ @Vers +''', '''') = ''''))           
				AND ((PART_NO=''' + @PartNo + ''' AND ISNULL('''+ @PartNo +''', '''') <> '''') or (ISNULL('''+ @PartNo +''', '''') = ''''))           
				AND ((SUPPLIER_CD=''' + @SuppCd + ''' AND ISNULL('''+ @SuppCd +''', '''') <> '''') or (ISNULL('''+ @SuppCd +''', '''') = ''''))           		
		)
		BEGIN
			INSERT INTO TB_R_LPOP_ABNORMALITY (
				[PACK_MONTH]
				,[D_ID]
				,[VERS]
				,[SUPPLIER_CD]
				,[SUPPLIER_NAME]
				,[PART_NO]
				,[PART_NAME]
				,[N_VOLUME]
				,[N_1_VOLUME]
				,[N_2_VOLUME]
				,[N_3_VOLUME]
				,' + REPLACE(UPPER(RTRIM(@area)), ' ', '_') + '_' + REPLACE(UPPER(RTRIM(@group)), ' ', '_') + '
			)
			VALUES (
				''' + @PackMonth + '''
				,''' + @DID + '''
				,''' + @Vers + '''
				,''' + @SuppCd + '''
				,''' + @SuppNm + '''
				,''' + @PartNo + '''
				,''' + @PartNm + '''
				,''' + @NVolume + '''
				,''' + @N1Volume + '''
				,''' + @N2Volume + '''
				,''' + @N3Volume + '''
				,''' + @remark + '''
			)
		END
		ELSE
		BEGIN
			UPDATE TB_R_LPOP_ABNORMALITY
			SET
				' + REPLACE(UPPER(RTRIM(@area)), ' ', '_') + '_' + REPLACE(UPPER(RTRIM(@group)), ' ', '_') + ' = ''' + @remark + '''
			WHERE 
				((PACK_MONTH=''' + @PackMonth + ''' AND ISNULL('''+ @PackMonth +''', '''') <> '''') or (ISNULL('''+ @PackMonth +''', '''') = ''''))     
				AND ((D_ID=''' + @DID + ''' AND ISNULL('''+ @DID +''', '''') <> '''') or (ISNULL('''+ @DID +''', '''') = ''''))           
				AND ((VERS=''' + @Vers + ''' AND ISNULL('''+ @Vers +''', '''') <> '''') or (ISNULL('''+ @Vers +''', '''') = ''''))           
				AND ((PART_NO=''' + @PartNo + ''' AND ISNULL('''+ @PartNo +''', '''') <> '''') or (ISNULL('''+ @PartNo +''', '''') = ''''))           
				AND ((SUPPLIER_CD=''' + @SuppCd + ''' AND ISNULL('''+ @SuppCd +''', '''') <> '''') or (ISNULL('''+ @SuppCd +''', '''') = ''''))           				
		END'
  
	EXEC (@sql);  
    
       
END

