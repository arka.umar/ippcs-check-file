
/****** Object:  StoredProcedure [dbo].[SP_GetPostInvoiceByJob]    Script Date: 30/04/2021 17:08:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_GetPostInvoiceByJob]
			@ro_v_err_mesg_output  varchar(max) OUTPUT

AS 
DECLARE @l_v_supp_cd             varchar(6),
        @l_v_supp_inv_no		 varchar(12),
		@l_d_posting_dt			 datetime,
		@l_d_baseline_dt		 datetime,
		@l_d_inv_dt				 datetime,
		@l_v_pay_method_cd		 varchar(2),
		@l_v_pay_term_cd		 varchar(4),
		@l_v_supp_bank_type		 varchar(4),
		@l_n_inv_amt_total		 numeric(16,2),
		@l_v_inv_tax_no			 varchar(24),
		@l_v_inv_text			 varchar(50),
		@l_v_assignment			 varchar(12),
		@l_v_tax_cd				 varchar(2),
		@l_d_inv_tax_dt			 datetime,
		@l_v_withholding_tax_cd  varchar(2),
		@l_n_base_amt            numeric(16,5),
		@l_v_gl_account          varchar(10),
		@l_dc_inv_amt            decimal(16,5),
		@l_v_cost_center         varchar(40),
		@l_v_tax_cd_2            varchar(2),
		@l_n_inv_amt_local_cur   numeric(16,2)		

DECLARE @l_n_process_status      int,
		@l_v_partner_bank        varchar(max) = 'test',
		@l_n_calculate_tax       int = 1,
		@l_n_amount              numeric(10) = 0,
		@l_v_invoice_note        varchar(max) = 'invoice testing data',
		@l_v_header_text         varchar(max) = 'invoice header text',
		@l_n_ppv_amount          numeric(10) = 0,
		@l_v_user                varchar(15) = 'fdtemp3',
		@l_n_process_status_2    int,
		@l_v_messages            varchar(max),
		@l_v_location		     varchar(max),
		@l_v_module              varchar(10),
		@l_v_function            varchar(10),
		@l_big_processId		 bigint

BEGIN TRY
    DECLARE l_cursor CURSOR LOCAL FAST_FORWARD
	FOR
	SELECT tu.SUPP_CD, --modif by alira.agi 2015-04-08 replace max(tu.SUPP_CD) to tu.SUPP_CD
		   tu.SUPP_INV_NO,
		   MAX(tu.POSTING_DT) POSTING_DT,
		   MAX(tu.BASELINE_DT) BASELINE_DT,
		   MAX(tu.INV_DT) INV_DT,
		   MAX(sic.PAYMENT_METHOD_CD) PAY_METHOD_CD,
		   MAX(sic.PAYMENT_TERM_CD) PAY_TERM_CD,
		   MAX(ISNULL(case when tu.SUPP_BANK_TYPE='' 
						   then tb.SUPP_BANK_TYPE 
						   else tu.SUPP_BANK_TYPE 
							end,tb.SUPP_BANK_TYPE)) SUPP_BANK_TYPE,
		   MAX(tu.INV_AMT_TOTAL) INV_AMT_TOTAL,
		   MAX(tu.INV_TAX_NO) INV_TAX_NO,
		   MAX(tu.INV_TEXT) INV_TEXT,
		   MAX(tu.ASSIGNMENT) ASSIGNMENT,
		   MAX(tu.TAX_CD) TAX_CD,
		   MAX(tu.INV_TAX_DT) INV_TAX_DT,
		   MAX(tu.WITHHOLDING_TAX_CD) WITHHOLDING_TAX_CD,
		   MAX(tu.BASE_AMT) BASE_AMT,
		   MAX(ti.GL_ACCOUNT) GL_ACCOUNT,
		   MAX(ti.INV_AMT) INV_AMT,
		   MAX(ti.COST_CENTER) COST_CENTER,
		   MAX(ti.TAX_CD) TAX_CD,
		   (SELECT SUM(tu2.INV_AMT_LOCAL_CUR) 
		      FROM TB_R_INV_UPLOAD tu2 
			 WHERE tu.SUPP_INV_NO = tu2.SUPP_INV_NO) INV_AMT_LOCAL_CUR
	  FROM TB_R_INV_UPLOAD tu
		   INNER JOIN TB_M_SUPPLIER_ICS sic ON tu.SUPP_CD = sic.SUPP_CD
		   LEFT JOIN TB_T_INV_GL_ACCOUNT ti ON tu.SUPP_INV_NO = ti.SUPP_INVOICE_NO 
					 AND tu.SUPP_CD=ti.SUPP_CD --modif by alira.agi 2015-04-08 add join supp_code
					 AND ti.GL_ACCOUNT NOT IN(SELECT SYSTEM_VALUE 
												FROM TB_M_SYSTEM 
											   WHERE FUNCTION_ID='51004' 
													 AND SYSTEM_CD='STAMP_GL_ACCOUNT')
		   LEFT JOIN TB_M_SUPPLIER_BANK tb ON tu.SUPP_CD=tb.SUPP_CD AND tu.PAY_CURR=tb.ACCOUNT_CURR
	 WHERE tu.AUTO_POSTING = 'Y' 
			AND STATUS_CD in ('3','-5')
	      -- AND tu.SUPP_INV_NO=@0 
		 ---  AND CONVERT(VARCHAR(8),tu.[INV_DT],112)=CONVERT(VARCHAR(8),@2,112) 
		   --AND tu.SUPP_CD='5304'
		   --AND tu.SUPP_INV_NO = 'WBAB92373Y'
	GROUP BY tu.SUPP_INV_NO,tu.SUPP_CD, SUBMIT_DT
	ORDER BY SUBMIT_DT
	
	OPEN l_cursor
	FETCH NEXT FROM l_cursor INTO
		@l_v_supp_cd, @l_v_supp_inv_no, @l_d_posting_dt, @l_d_baseline_dt, 
		@l_d_inv_dt, @l_v_pay_method_cd, @l_v_pay_term_cd, @l_v_supp_bank_type, 
		@l_n_inv_amt_total, @l_v_inv_tax_no, @l_v_inv_text, @l_v_assignment, 
		@l_v_tax_cd, @l_d_inv_tax_dt, @l_v_withholding_tax_cd, @l_n_base_amt, 
		@l_v_gl_account, @l_dc_inv_amt, @l_v_cost_center, @l_v_tax_cd_2,@l_n_inv_amt_local_cur

	
	BEGIN
		WHILE @@FETCH_STATUS = 0
		BEGIN
		    
			 SET @l_v_module =  '5';
             SET @l_v_function = '53002';
			 SET @l_v_messages = '';
			 SET @l_v_location = '';
			 SET @l_big_processId = null;
			 SET @l_d_posting_dt = getdate();

			 
			SELECT  @l_v_invoice_note = 'Komponen ' + cast(qry.a as varchar)+' '+ cast(qry.b as varchar) from (
			SELECT TOP 1 CAST(DATENAME(MONTH,DOC_DT) AS VARCHAR) AS a,YEAR(DOC_DT)AS b, COUNT(1) AS c FROM TB_R_INV_UPLOAD
			WHERE SUPP_INV_NO = @l_v_supp_inv_no and SUPP_CD = @l_v_supp_cd
			GROUP BY DOC_DT) qry
			ORDER BY qry.c
			
			IF ISNULL(@l_v_tax_cd, '') = ''
			BEGIN
				SET @l_v_tax_cd = 'V1'
			END

			 EXEC	@l_n_process_status_2 = [dbo].[SP_GetPostingInvoicePaymentInquiryJob]
					@ri_v_supp_cd = @l_v_supp_cd,
					@ri_v_supp_inv_no = @l_v_supp_inv_no,
					@ri_d_posting_dt = @l_d_posting_dt,
					@ri_d_inv_dt = @l_d_inv_dt,
					@ri_d_baseline_dt = @l_d_baseline_dt,
					@ri_v_pay_term_cd = @l_v_pay_term_cd,
					@ri_v_pay_method_cd = @l_v_pay_method_cd,
					@ri_v_withholding_tax_cd = @l_v_withholding_tax_cd, -- FID.Ridwan: 2021-01-13 NULL,
					@ri_n_base_amt = @l_n_base_amt, -- FID.Ridwan: 2021-01-13  NULL,
					@ri_v_partner_bank = 1,
					@ri_n_calculate_tax = 1,
					@ri_n_amount = @l_n_inv_amt_local_cur,
					@ri_v_invoice_note = @l_v_invoice_note, --'KOMPONEN',
					@ri_v_header_text = @l_v_inv_tax_no,
					@ri_v_assignment = NULL,
					@ri_v_tax_cd = @l_v_tax_cd,
					@ri_d_tax_dt = @l_d_inv_tax_dt,
					@ri_v_gl_account = NULL,
					@ri_n_ppv_amount = NULL,
					@ri_v_user = @l_v_user,
					@ro_v_err_mesg = @ro_v_err_mesg_output OUTPUT

             IF (@l_n_process_status_2 > 0)
			  BEGIN
			    SET @l_n_process_status = @l_n_process_status_2;

			    SET @l_v_location = 'update invoice upload';

				EXEC  [dbo].[sp_PutLog] 
						@what = @ro_v_err_mesg_output, 
						@user = @l_v_user, 
						@where = @l_v_location, 
						@pid = @l_big_processId OUTPUT, 
						@id = 'MPCS00004INF', 
						@type = 'INF',
						@module = @l_v_module, 
						@func = @l_v_function, 
						@sts = '0'
			  END 
			 ELSE
			  BEGIN
				
				SET @l_v_messages = 'Start Process Invoice Payment Posting from IPPCS';
			    SET @l_v_location = 'Invoice Inquiry';
				
				EXEC [dbo].[sp_PutLog] 
									  @what = @l_v_messages, 
									  @user = @l_v_user, 
									  @where = @l_v_location, 
									  @pid = @l_big_processId OUTPUT, 
									  @id = 'MPCS00004INF', 
									  @type = '',
									  @module = @l_v_module, 
									  @func = @l_v_function, 
									  @sts = '0'

				SET @l_v_messages = 'Start Process Generate Data Posting';
                SET @l_v_location = 'Generate Posting File';

				EXEC [dbo].[sp_PutLog] 
						@what = @l_v_messages, 
						@user = @l_v_user, 
						@where = @l_v_location, 
						@pid = @l_big_processId OUTPUT, 
						@id = 'MPCS00004INF', 
						@type = '',
						@module = @l_v_module, 
						@func = @l_v_function, 
						@sts = '0'

				SET @l_v_messages = 'Start Process Generate Data Posting';
                SET @l_v_location = 'Generate Posting File';

				EXEC [dbo].[sp_PutLog] 
						@what = @l_v_messages, 
						@user = @l_v_user, 
						@where = @l_v_location, 
						@pid = @l_big_processId OUTPUT, 
						@id = 'MPCS00002INF', 
						@type = '',
						@module = @l_v_module, 
						@func = @l_v_function, 
						@sts = '0'

                 -- INSERT OR UPDATE ICS
				 	INSERT INTO [dbo].[TB_R_ICS_QUEUE]
							([IPPCS_MODUL]
							,[IPPCS_FUNCTION]
							,[PROCESS_ID]
							,[PROCESS_STATUS]
							,[REMARK]
							,[CREATED_BY]
							,[CREATED_DT])
						VALUES
							('5'
							,'53002'
							,@l_big_processId
							,0
							,@l_v_supp_inv_no + ';' + @l_v_supp_cd
							,@l_v_user
							,GETDATE());
				
					 UPDATE TB_R_INV_UPLOAD SET
							POSTING_BY=@l_v_user,
							IN_PROGRESS='1',
							CHANGED_BY=@l_v_user,
							CHANGED_DT=GETDATE()
					  WHERE SUPP_INV_NO=@l_v_supp_inv_no 
						    AND SUPP_CD=@l_v_supp_cd --AND (CONVERT(VARCHAR(8),@4,112)='19000101' OR (CONVERT(VARCHAR(8),@4,112)<>'19000101' AND CONVERT(VARCHAR(8),INV_DT,112)=CONVERT(VARCHAR(8),@4,112)))
		

				SET @l_v_messages = 'Finish Process Generate Data Posting';

				EXEC [dbo].[sp_PutLog] 
						@what = @l_v_messages, 
						@user = @l_v_user, 
						@where = @l_v_location, 
						@pid = @l_big_processId, 
						@id = 'MPCS00003INF', 
						@type = '',
						@module = @l_v_module, 
						@func = @l_v_function, 
						@sts = '0'
			  END

		FETCH NEXT FROM l_cursor INTO
		      @l_v_supp_cd, @l_v_supp_inv_no, @l_d_posting_dt, @l_d_baseline_dt, 
			  @l_d_inv_dt, @l_v_pay_method_cd, @l_v_pay_term_cd, @l_v_supp_bank_type, 
			  @l_n_inv_amt_total, @l_v_inv_tax_no, @l_v_inv_text, @l_v_assignment, 
			  @l_v_tax_cd, @l_d_inv_tax_dt, @l_v_withholding_tax_cd, @l_n_base_amt, 
			  @l_v_gl_account, @l_dc_inv_amt, @l_v_cost_center, @l_v_tax_cd_2,@l_n_inv_amt_local_cur
		END -- LOOP
	END
 END TRY
 BEGIN CATCH	
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	
	SELECT @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE()

    set @l_n_process_status = 2
	set @ro_v_err_mesg_output = 'ERROR: spGetPostInvoiceByJob : ' + @ErrorMessage + ', at line = ' +  cast (@ErrorLine as varchar)

	EXEC [dbo].[sp_PutLog] 
			@what = @ro_v_err_mesg_output, 
			@user = @l_v_user, 
			@where = @l_v_location, 
			@pid = @l_big_processId OUTPUT, 
			@id = '', 
			@type = '',
			@module = @l_v_module, 
			@func = @l_v_function, 
			@sts = '0'
	
	PRINT 'sp_post_invoice : ' + @ErrorMessage + ', at line = ' +  cast (@ErrorLine as varchar)
    rollback transaction process_tran;
			
      return @l_n_process_status;
END CATCH

