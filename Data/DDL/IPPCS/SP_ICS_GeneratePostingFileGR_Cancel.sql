-- =============================================
-- Author:		niit.yudha
-- Create date: 3-Mei-2013
-- Description:	Generate Goods Receipt ICS
-- Changes List: 
-- 2020.09.14 FID.Ridwan 
--			  change db link from ics (oracle) to new ics (sql server)
--			  add logging
-- =============================================
ALTER PROCEDURE [dbo].[SP_ICS_GeneratePostingFileGR_Cancel]
---DECLARE
	-- Add the parameters for the stored procedure here 
    @UserID VARCHAR(20) ,
    @ManifestNo TEXT ,
    @SupplierCode VARCHAR(MAX) ,
    @RvcPlantCode VARCHAR(MAX) ,
    @DockCode VARCHAR(MAX),
    @L_N_PROCESS_ID BIGINT
AS 

BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
    SET NOCOUNT ON;
        
    DECLARE @PROC_ID VARCHAR(12) ,
        @PROCESS_ID BIGINT ,
        @LAST_PROCESS_ID VARCHAR(12) ,
        @TODAY VARCHAR(8) ,
        @NOW DATETIME ,
        @CURR_NO INT ,
        @CURR_PROCESS_ID VARCHAR(12) ,
        @SEQ_NO_LOG BIGINT ,
        @LOCATION VARCHAR(100) ,
        @PROC_STAT TINYINT ,
        @L_N_COUNT_ERR INT,
        @L_N_COUNT_MANIFEST INT,
        @ERR_FLAG BIT   ,
		@query VARCHAR(MAX),
		@ICSSrvrName NVARCHAR(256),
		@ICSDBName NVARCHAR(256)
	 
    SET @LOCATION = 'SP ICS Cancel GR';
    SET @SEQ_NO_LOG = 1;
    SET @ERR_FLAG = 0; 
    SET @PROC_STAT = 0;


																	
    SET NOCOUNT ON	
         					
    DECLARE @TP_SUPPLIER_CODE VARCHAR(50) ,
        @FLTR_DATETIME_VALUE AS VARCHAR(50) ,
        @FLTR_LOGICAL_VALUE AS VARCHAR(50) ,
        @FLTR_FLAG_VALUE AS VARCHAR(50) ,
        @IS_VALID_FILTER CHAR(1) ,
        @TYPE_CODE INT ,
        @RECEIVE_NO VARCHAR(10) ,
        @SOURCE_TYPE_GR CHAR(1) ,
        @SOURCE_TYPE_TP CHAR(1) ,
        @SYS_SOURCE CHAR(20) ,
        @PROD_PURPOSE_CD CHAR(5) ,
        @URI_UOM CHAR(5) ,
        @SEND_FLAG VARCHAR(1)		
		
	DECLARE @FUNCTION_ID AS VARCHAR(MAX) = '42002'
		,@MODULE_ID AS VARCHAR(MAX) = '4'
		,@FUNTION_NM AS VARCHAR(MAX) = 'SP_ICS_GeneratePostingFileGR_Cancel'
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_ICS_GeneratePostingFileGR_Cancel'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)

	EXEC dbo.CommonGetMessage 'MPCS00002INF'
		,@MSG_TXT OUTPUT
		,@N_ERR OUTPUT
		,'Cancel GR'

	INSERT INTO TB_R_LOG_D (
		PROCESS_ID
		,SEQUENCE_NUMBER
		,MESSAGE_ID
		,MESSAGE_TYPE
		,[MESSAGE]
		,LOCATION
		,CREATED_BY
		,CREATED_DATE
		)
	SELECT @L_N_PROCESS_ID
		,(
			SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
			FROM dbo.TB_R_LOG_D
			WHERE PROCESS_ID = @L_N_PROCESS_ID
			)
		,'MPCS00002INF'
		,'INF'
		,'MPCS00002INF : ' + @MSG_TXT
		,@LOG_LOCATION
		,@UserID
		,GETDATE()

		
	EXEC dbo.CommonGetMessage 'MPCS00002INF'
		,@MSG_TXT OUTPUT
		,@N_ERR OUTPUT
		,'System Master Checking'

	INSERT INTO TB_R_LOG_D (
		PROCESS_ID
		,SEQUENCE_NUMBER
		,MESSAGE_ID
		,MESSAGE_TYPE
		,[MESSAGE]
		,LOCATION
		,CREATED_BY
		,CREATED_DATE
		)
	SELECT @L_N_PROCESS_ID
		,(
			SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
			FROM dbo.TB_R_LOG_D
			WHERE PROCESS_ID = @L_N_PROCESS_ID
			)
		,'MPCS00002INF'
		,'INF'
		,'MPCS00002INF : ' + @MSG_TXT
		,@LOG_LOCATION
		,@UserID
		,GETDATE()


	SELECT @ICSSrvrName = SYSTEM_VALUE FROM [TB_M_SYSTEM] 
	WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
		AND SYSTEM_CD = 'LINKED_SERVER';
		
	SELECT @ICSDBName = SYSTEM_VALUE FROM [TB_M_SYSTEM] 
	WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
		AND SYSTEM_CD = 'DB_NAME';
		
		
	 DECLARE @dockServicePart VARCHAR(20) = '',
                     @prodPurposeServicePart VARCHAR(20) = ''

                     SELECT @dockServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DOCK_SERVICE_PART'
                     SELECT @prodPurposeServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'PROD_PURPOSE_GR_SERVICE_PART'
                
                DECLARE @dockServicePartTable AS TABLE (DOCK VARCHAR(2))
                
                INSERT INTO @dockServicePartTable (DOCK)
    EXEC dbo.SplitString @dockServicePart, ','  												
																	
    SELECT  @TYPE_CODE = 42001															
																
		--GET VALUE FROM SYS TABLE																
    SELECT  @TP_SUPPLIER_CODE = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                            'TP_SPLR_CODE')
            + '%'																
    SELECT  @RECEIVE_NO = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                      'RECEIVE_NO_GR')																
    SELECT  @SOURCE_TYPE_GR = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                          'SOURCE_TYPE_GR')																

    SELECT  @SOURCE_TYPE_TP = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                          'SOURCE_TYPE_TP')

    SELECT  @SYS_SOURCE = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                      'SYSTEM_SOURCE_GR')																
    SELECT  @PROD_PURPOSE_CD = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                           'PROD_PURPOSE_CD_GR')																
    SELECT  @URI_UOM = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                   'ORI_UNIT_MEASU_CD_GR')																
    SELECT  @SEND_FLAG = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'SEND_FLAG')	
	                    															
		--GET FILTER PARAMETERS																
    SELECT  @FLTR_DATETIME_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'POST_TIME_FLTR')))																
    SELECT  @FLTR_LOGICAL_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'POST_LGCL_FLTR')))																
    SELECT  @FLTR_FLAG_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'POST_FLG_FLTR')))																
		-- END OF GET FILTER PARAMETERS																
			
	EXEC dbo.CommonGetMessage 'MPCS00002INF'
		,@MSG_TXT OUTPUT
		,@N_ERR OUTPUT
		,'Filter Validity'

	INSERT INTO TB_R_LOG_D (
		PROCESS_ID
		,SEQUENCE_NUMBER
		,MESSAGE_ID
		,MESSAGE_TYPE
		,[MESSAGE]
		,LOCATION
		,CREATED_BY
		,CREATED_DATE
		)
	SELECT @L_N_PROCESS_ID
		,(
			SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
			FROM dbo.TB_R_LOG_D
			WHERE PROCESS_ID = @L_N_PROCESS_ID
			)
		,'MPCS00002INF'
		,'INF'
		,'MPCS00002INF : ' + @MSG_TXT
		,@LOG_LOCATION
		,@UserID
		,GETDATE()
														
		-- CHECK FILTER VALIDITY																
    IF ( @FLTR_FLAG_VALUE = 'Y' ) 
        BEGIN																
            IF ( @FLTR_LOGICAL_VALUE IN ( '=', '>', '<', '>=', '<=' ) )
                AND ( LEN(@FLTR_DATETIME_VALUE) = 14 ) -- AND ( ISDATE(CHAR14TODATE(@FLTR_DATETIME_VALUE)) = 1 )
                BEGIN
                    SET @IS_VALID_FILTER = 'Y'															
                END 
            ELSE 
                BEGIN
                    SET @IS_VALID_FILTER = 'N'															
                END
        END																
    ELSE 
        BEGIN																
            IF ( @FLTR_FLAG_VALUE = 'N' ) 
                SET @IS_VALID_FILTER = 'Y'
            ELSE 
                SET @IS_VALID_FILTER = 'N'															
        END																
			-- END OF CHECK FILTER VALIDITY																
		
		
		
    
    CREATE TABLE #TempManifestNo
        (
          ManifestNo VARCHAR(100)
        )
    CREATE TABLE #tempSupplierCode
        (
          SupplierCode VARCHAR(100)
        )    
            
    CREATE TABLE #tempRvcPlantCode
        (
          RvcPlantCode VARCHAR(100)
        )
    CREATE TABLE #tempDockCode ( DockCode VARCHAR(100) )   
        
    IF ( SELECT CHARINDEX(';', @ManifestNo)
       ) > 0 
        BEGIN 
            INSERT  INTO #TempManifestNo
                    ( ManifestNo )
                    EXEC dbo.SplitString @ManifestNo, ';'
 	
        END		
    ELSE 
        BEGIN
            INSERT  INTO #TempManifestNo
                    ( ManifestNo )
                    SELECT  @ManifestNo
        END
            
            
    IF ( SELECT CHARINDEX(';', @SupplierCode)
       ) > 0 
        BEGIN 
            INSERT  INTO #tempSupplierCode
                    ( SupplierCode 
                        
                    )
                    EXEC dbo.SplitString @SupplierCode, ';'
 	
        END								
    ELSE 
        BEGIN 
            INSERT  INTO #tempSupplierCode
                    ( SupplierCode )
                    SELECT  @SupplierCode
 	
        END	  
            
               
    IF ( SELECT CHARINDEX(';', @RvcPlantCode)
       ) > 0 
        BEGIN 
            INSERT  INTO #tempRvcPlantCode
                    ( RvcPlantCode 
                        
                    )
                    EXEC dbo.SplitString @RvcPlantCode, ';' 
        END								
    ELSE 
        BEGIN 
            INSERT  INTO #tempRvcPlantCode
                    ( RvcPlantCode )
                    SELECT  @RvcPlantCode
 	
        END	 
            
              
    IF ( SELECT CHARINDEX(';', @DockCode)
       ) > 0 
        BEGIN
  
            INSERT  INTO #tempDockCode
                    ( DockCode )
                    EXEC dbo.SplitString @DockCode, ';' 
        END
    ELSE 
        BEGIN 
            INSERT  INTO #tempDockCode
                    ( DockCode )
                    SELECT  @DockCode 
        END     		
            
            							

	-- Added by imans, 07-08-2013
	---> EXECUTE SP UPDATE SHIPPING DOCK STAMPING & CASTING
	EXEC  [dbo].[SP_UPDATE_SHIPPING_DOCK] @MANIFEST = @ManifestNo
	---
	
		 					
    IF ( @IS_VALID_FILTER = 'Y' ) 
        BEGIN		
		
	EXEC dbo.CommonGetMessage 'MPCS00002INF'
		,@MSG_TXT OUTPUT
		,@N_ERR OUTPUT
		,'Get Data from Daily Order'

	INSERT INTO TB_R_LOG_D (
		PROCESS_ID
		,SEQUENCE_NUMBER
		,MESSAGE_ID
		,MESSAGE_TYPE
		,[MESSAGE]
		,LOCATION
		,CREATED_BY
		,CREATED_DATE
		)
	SELECT @L_N_PROCESS_ID
		,(
			SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
			FROM dbo.TB_R_LOG_D
			WHERE PROCESS_ID = @L_N_PROCESS_ID
			)
		,'MPCS00002INF'
		,'INF'
		,'MPCS00002INF : ' + @MSG_TXT
		,@LOG_LOCATION
		,@UserID
		,GETDATE()
																		
																			
            CREATE TABLE #ICS_POSTING
                (
                  REC_ID INT IDENTITY(1, 1) ,
                  MANIFEST_NO VARCHAR(25) ,
                  SUPPLIER_CODE VARCHAR(4) ,
                  SUPPLIER_PLANT CHAR(1) ,
                  SHIPPING_DOCK VARCHAR(3) ,
                  COMPANY_CODE VARCHAR(4) ,
                  RCV_PLANT_CODE CHAR(1) ,
                  DOCK_CODE VARCHAR(2) ,
                  ORDER_NO VARCHAR(12) ,
                  CAR_FAMILY_CODE VARCHAR(4) ,
                  RE_EXPORT_CODE VARCHAR(1) ,
                  PART_NO VARCHAR(12) ,
                  KNBN_PRN_ADDRESS VARCHAR(10) ,
                  ORDER_TYPE CHAR(1) ,
                  MOV_TYPE CHAR(3) ,
                  DROP_STATUS_DATE DATETIME ,
                  RECEIVED_QTY INT NULL ,
                  DN_COMPLETE_FLAG CHAR(1) ,
                  DATA_TYPE CHAR(2),
                  ORDER_RELEASE_DT DATETIME
                )															
																	
				/* ===========================================	 		
					CREATE DATA LIST FROM ORDER		 		
				   ===========================================*/															
             
            declare @tst int
            
              --- Remark Cancel : by Imans		 
			   --CANCEL ORDER														
            INSERT  INTO #ICS_POSTING
                    ( MANIFEST_NO ,
                      SUPPLIER_CODE ,
                      SUPPLIER_PLANT ,
                      SHIPPING_DOCK ,
                      COMPANY_CODE ,
                      RCV_PLANT_CODE ,
                      DOCK_CODE ,
                      ORDER_NO ,
                      CAR_FAMILY_CODE ,
                      RE_EXPORT_CODE ,
                      PART_NO ,
                      KNBN_PRN_ADDRESS ,
                      ORDER_TYPE ,
                      MOV_TYPE ,
                      DROP_STATUS_DATE ,
                      RECEIVED_QTY ,
                      DN_COMPLETE_FLAG ,
                      DATA_TYPE,
                      ORDER_RELEASE_DT													
			            
                    )
                    SELECT  x.MANIFEST_NO ,
                            SUPPLIER_CD ,
                            x.SUPPLIER_PLANT ,
                            SHIPPING_DOCK ,
                            COMPANY_CD ,
                            x.RCV_PLANT_CD ,
                            x.DOCK_CD ,
                            ORDER_NO ,
                            CAR_FAMILY_CD ,
                            RE_EXPORT_CD ,
                            PART_NO ,
                            '' AS KNBN_PRN_ADDRESS, -- was : y.KANBAN_PRINT_ADDRESS , not send to ics
                            ORDER_TYPE ,
                            CASE LEFT(x.SUPPLIER_CD, 3)
                              WHEN '807' THEN '302'
                              ELSE '102'
                            END AS MOVE_TYPE ,
                            getdate() AS DROP_STATUS_DATE ,
                            SUM(y.ORDER_QTY) AS RECEIVED_QTY ,
                            DBO.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                        'DN_COMPLETE_FLG_GR') AS DN_COMPLETE_FLAG ,
                            'CL' AS DATA_TYPE,
                            x.ARRIVAL_PLAN_DT as ORDER_RELEASE_DT
                    FROM    dbo.TB_R_DAILY_ORDER_MANIFEST x
                            INNER JOIN dbo.TB_R_DAILY_ORDER_PART y ON x.MANIFEST_NO = y.MANIFEST_NO
                                                             -- AND x.CANCEL_FLAG = 1
                    WHERE   x.PROBLEM_FLAG = '0'
                            --AND x.CANCEL_FLAG = '1'
                            AND 
                            (
								x.MANIFEST_RECEIVE_FLAG IN ('4')
								OR (x.MANIFEST_RECEIVE_FLAG IN ('5') AND ICS_FLAG = '1')
							)
                            AND x.MANIFEST_NO IN ( SELECT   *
                                                   FROM     #TempManifestNo )
							AND y.ORDER_QTY > 0
                    GROUP BY x.MANIFEST_NO ,
                            SUPPLIER_CD ,
                            x.SUPPLIER_PLANT ,
                            SHIPPING_DOCK ,
                            COMPANY_CD ,
                            x.RCV_PLANT_CD ,
                            x.DOCK_CD ,
                            ORDER_NO ,
                            CAR_FAMILY_CD ,
                            RE_EXPORT_CD ,
                            PART_NO ,
                            -- was : y.KANBAN_PRINT_ADDRESS ,
                            ORDER_TYPE,
                            x.ARRIVAL_PLAN_DT
                     
            CREATE TABLE #ICS_FILE
                (
                  REC_ID BIGINT IDENTITY(1, 1) , --1
                  SYS_SOURCE CHAR(20) ,  --2
                  [USER_ID] CHAR(20) ,  --3
                  MOV_TYPE CHAR(4) ,  --4
                  PROCESS_DATE DATETIME ,  --5
                  POSTING_DATE DATETIME ,  --6
                  REF_NO CHAR(16) ,  --7
                  KANBAN_ORDER_NO CHAR(16) ,  --8
                  PROD_PURPOSE_CD CHAR(5) ,  --9
                  SOURCE_TYPE CHAR(1) ,  --10
                  ORI_MAT_NO CHAR(30) ,  --11
                  POSTING_QUANTITY CHAR(10) ,  --12
                  ORI_UNIT_OF_MEASURE_CD CHAR(5) ,  --13
                  ORI_SUPP_CD CHAR(6) ,  --14
                  RECEIVING_PLANT_CD CHAR(4) ,  --15
                  RECEIVING_AREA_CD CHAR(6) ,  --16
                  INT_KANBAN_FLAG CHAR(1) ,  --17
                  DN_COMPLETE_FLAG CHAR(1) ,  --18
                  MAT_COMPLETE_FLAG CHAR(1) ,  --19
                  RECEIVE_NO CHAR(10) ,  --20
                  SEND_FLAG CHAR(1) ,  --21
                  PROCESS_ID BIGINT,  --22
                  ORDER_RELEASE_DT DATETIME
                )					
                
            insert into #ICS_FILE           
            SELECT  @SYS_SOURCE AS systemSource ,
                    @UserID AS userID ,
                    A.MOV_TYPE AS movementType ,
                    GETDATE() AS processDate,
                    CONVERT(DATE,CONVERT(VARCHAR(20),A.DROP_STATUS_DATE,112))  AS postingDate ,
                    A.MANIFEST_NO AS refNo ,
                    A.ORDER_NO AS kanbanOrderNo ,
                    --@PROD_PURPOSE_CD AS prodPurpose ,
					CASE WHEN (SELECT 1 WHERE A.DOCK_CODE IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
                                  THEN @prodPurposeServicePart
                                  ELSE
                                         @PROD_PURPOSE_CD
                                  END
                                    AS prodPurpose,
                    CASE LEFT(A.SUPPLIER_CODE, 3)
                      WHEN '807' THEN @SOURCE_TYPE_TP
                      ELSE  @SOURCE_TYPE_GR
                    END AS sourceType , 
                    A.PART_NO AS oriMatNo ,
                    RECEIVED_QTY AS postingQty ,
                    @URI_UOM AS oriUnitOfMeasure ,
                    CASE LEFT(A.SUPPLIER_CODE, 3)
                      WHEN '807' THEN A.SHIPPING_DOCK
                      ELSE A.SUPPLIER_CODE
                    END AS oriSuppCode ,
                    A.RCV_PLANT_CODE AS receivingPlantCode ,
                    A.DOCK_CODE AS receivingPlantArea ,
                    (
                    CASE 
                      WHEN (select COUNT(1) from TB_M_PART_INFO x
							   where x.SUPPLIER_CD = A.SUPPLIER_CODE
							   and x.SUPPLIER_PLANT = A.SUPPLIER_PLANT  
							   and x.DOCK_CD = A.DOCK_CODE
							   and x.PART_NO = A.PART_NO
							   and A.DROP_STATUS_DATE between x.start_dt and x.end_dt 
                            ) > 0 then 'Y'
                       ELSE 'N'
                     END
                     ) AS intKanbanFlag ,
                    A.DN_COMPLETE_FLAG AS dnCompleteFlag ,
                    NULL AS matCompleteFlag ,
                    @RECEIVE_NO AS receiveNo ,
                    @SEND_FLAG AS sendFlag ,   --- Y/N 
                    @PROCESS_ID AS processID ,
                    A.ORDER_RELEASE_DT AS orderReleaseDate 
            FROM    #ICS_POSTING A

			-- 1. UPDATE FLAG
			UPDATE #ICS_FILE 
			SET 
				INT_KANBAN_FLAG = 'N'
			-- USED IN OCTOBER	,ORI_SUPP_CD = CAST(LTRIM(B.SUPPLIER_CVRT) AS CHAR(6))
			FROM 
				#ICS_FILE A, TB_M_SUPPLIER_CVRT B
			WHERE 
				LTRIM(RTRIM(A.RECEIVING_PLANT_CD)) = LTRIM(RTRIM(B.RECEIVING_PLANT)) AND 
				LTRIM(RTRIM(A.RECEIVING_AREA_CD)) = LTRIM(RTRIM(B.RECEIVING_AREA)) 
                			
				 
			/*															
			=================================================															
				END OF CREATE DATA LIST FROM ORDER														
			=================================================															
			*/	
			 --select * from openquery([IPPCS_TO_ICS] , 'select * from TB_T_GOOD_RECEIVE_KBS')
			  
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert into ICS.TB_T_REFF_CANCELLATION'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @L_N_PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @L_N_PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@UserID
				,GETDATE()
				
			SET @query = ' 
			INSERT OPENQUERY (' + @ICSSrvrName + ' , ''SELECT PROCESS_ID,
			REFF_NO,
			POSTING_DT,
			SYSTEM_SOURCE,
			TRANSACTION_TYPE,
			STATUS,
			CREATED_BY,
			CREATED_DT,
			CHANGED_BY,
			CHANGED_DT 
			FROM ' + @ICSDBName + 'TB_T_REFF_CANCELLATION'')  
			SELECT DISTINCT ' + CONVERT(VARCHAR, @L_N_PROCESS_ID) + ' PROCESS_ID,
				REF_NO REFF_NO,
				POSTING_DATE POSTING_DT,
				''IPPCS'' SYSTEM_SOURCE,
				''GR'' TRANSACTION_TYPE,
				0 STATUS,
				[USER_ID] CREATED_BY,
				GETDATE() CREATED_DT,
				[USER_ID] CHANGED_BY,
				GETDATE() CHANGED_DT 
			FROM #ICS_FILE';
			EXEC(@query);
			   
			/* Remark FID.Ridwan : 2020-09-14 ==> change db link 
			INSERT OPENQUERY (IPPCS_TO_ICS, 
			'SELECT  
			PROCESS_ID, 
			SYSTEM_SOURCE, 
			USR_ID, 
			POSTING_DT,
			PROCESS_DT,
			REF_NO,
			MOVEMENT_TYPE,
			KANBAN_ORDER_NO, 
			PROD_PURPOSE_CD, 
			SOURCE_TYPE, 
			ORI_MAT_NO, 
			UNIT_OF_MEASURE_CD, 
			RECEIVING_PLANT_CD, 
			RECEIVING_AREA_CD, 
			INT_KANBAN_FLAG, 
			ORI_SUPP_CD, 
			DN_COMPLETE_FLAG, 
			CREATED_BY, 
			CREATED_DT, 
			POSTING_QUANTITY,  
 			RECEVING_PLANT_CD, 
			ORI_UNIT_OF_MEASURE_CD, 
			ARRIVAL_PLAN_DT, 
			OTHER_PROCESS_ID  
			FROM 
			TB_T_GOOD_RECEIVE_KBS')
			select 0 PROCESS_ID, --1
			       RTRIM(SYS_SOURCE),  --2
			       [USER_ID], --3
			       POSTING_DATE,
                   PROCESS_DATE,
                   RTRIM(REF_NO), --6
			       RTRIM(MOV_TYPE), --7
			       RTRIM(KANBAN_ORDER_NO),  --8
			       RTRIM(PROD_PURPOSE_CD),  --9
			       RTRIM(SOURCE_TYPE),  --10
			       RTRIM(ORI_MAT_NO),  --11
			       RTRIM(ORI_UNIT_OF_MEASURE_CD) ,  --12
			       RTRIM(RECEIVING_PLANT_CD),  --13
			       RTRIM(RECEIVING_AREA_CD),  --14
			       INT_KANBAN_FLAG,  --15
			       RTRIM(ORI_SUPP_CD),  --16
			       DN_COMPLETE_FLAG,  --17
			       [USER_ID], --18
			       GETDATE(),
			       POSTING_QUANTITY, --20
			       RTRIM(RECEIVING_PLANT_CD),  --21
			       RTRIM(ORI_UNIT_OF_MEASURE_CD),  --22
			       ORDER_RELEASE_DT,  --23
			       @L_N_PROCESS_ID --24
		    from #ICS_FILE*/
		    																								
        END

    -- CHECK ERR
    SELECT @L_N_COUNT_ERR = COUNT(1) FROM
    TB_R_DAILY_ORDER_MANIFEST
    where MANIFEST_NO in ( SELECT * FROM #TempManifestNo )
    and (ICS_FLAG <> 1
    or DROP_STATUS_FLAG <> 1
    or PROBLEM_FLAG <> 0
    or MANIFEST_RECEIVE_FLAG not in ('4','5'));;
    
    -- CHECK NUMBER OF MANIFEST
    SELECT @L_N_COUNT_MANIFEST = COUNT(1) 
    FROM
    #TempManifestNo;
    
    --COMPARE MANIFEST

	/*Remark FID.Ridwan : 2020-09-14 ==> change db link
    IF @L_N_COUNT_ERR <> @L_N_COUNT_MANIFEST
        BEGIN
            INSERT INTO [dbo].[TB_R_ICS_QUEUE]
					   ([IPPCS_MODUL]
					   ,[IPPCS_FUNCTION]
					   ,[PROCESS_ID]
					   ,[PROCESS_STATUS]
					   ,[CREATED_BY]
					   ,[CREATED_DT])
            VALUES
			   ('4'
			   ,'42002'
			   ,@L_N_PROCESS_ID
			   ,0
			   ,@UserID
			   ,GETDATE())

            INSERT OPENQUERY (IPPCS_TO_ICS, 
							'SELECT  
								OTHER_PROCESS_ID,
								STATUS,
								REFERENCE,
								CREATED_BY,
								CREATED_DT
						     FROM TB_R_PCS_INTERFACE_STATUS')
            VALUES (@L_N_PROCESS_ID, 0, 'Cancel Goods Receipt',@UserID,GETDATE()); 	
        END;*/


		--Modif by Fid.Reggy for Temporary
		--BEGIN
  --          SELECT * FROM
		--	TB_R_DAILY_ORDER_MANIFEST
		--	where MANIFEST_NO in ( SELECT * FROM #TempManifestNo )
		--	and (ISNULL(ICS_FLAG,1) <> 1
		--	or ISNULL(CANCEL_FLAG,1) <> 0
		--	or ISNULL(DROP_STATUS_FLAG,0) <> 1
		--	or ISNULL(PROBLEM_FLAG,1) <> 0
		--	or MANIFEST_RECEIVE_FLAG not in ('2','3','5'));    
  --      END;

		BEGIN
            SELECT * FROM
			TB_R_DAILY_ORDER_MANIFEST
			where MANIFEST_NO in ( SELECT * FROM #TempManifestNo )
			and (ISNULL(ICS_FLAG,1) <> 1
			or ISNULL(CANCEL_FLAG,1) <> 0
			or ISNULL(DROP_STATUS_FLAG,0) <> 1
			or ISNULL(PROBLEM_FLAG,1) <> 0)
			and MANIFEST_RECEIVE_FLAG not in ('2','3','5');    
        END;
           
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Cancel GR'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @L_N_PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @L_N_PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@UserID
				,GETDATE()
				                 
    IF OBJECT_ID('tempdb..#TempManifestNo') IS NOT NULL 
        DROP TABLE #TempManifestNo
    IF OBJECT_ID('tempdb..#tempDockCode') IS NOT NULL 
        DROP TABLE #tempDockCode
    IF OBJECT_ID('tempdb..#tempRvcPlantCode') IS NOT NULL 
        DROP TABLE #tempRvcPlantCode
    IF OBJECT_ID('tempdb..#tempDockCode') IS NOT NULL 
        DROP TABLE #tempDockCode
    IF OBJECT_ID('tempdb..#tempSupplierCode') IS NOT NULL 
        DROP TABLE #tempSupplierCode
    IF OBJECT_ID('tempdb..#ICS_FILE') IS NOT NULL 
        DROP TABLE #ICS_FILE
    IF OBJECT_ID('tempdb..#ICS_POSTING') IS NOT NULL 
        DROP TABLE #ICS_POSTING



END


