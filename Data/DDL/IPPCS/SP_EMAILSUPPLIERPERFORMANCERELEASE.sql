-- =============================================
-- Author:		fid.deny
-- Create date: 18 Mei 2017
-- Description:	send email to specified supplier when supplier performance status released
-- =============================================
CREATE PROCEDURE [dbo].[SP_EMAILSUPPLIERPERFORMANCERELEASE]
	@SUPP_CD VARCHAR(4),
	@PROD_MONTH VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @email VARCHAR(300) = '',
		    @name VARCHAR(300) = '',
			@subject VARCHAR(300) = '',
			@body VARCHAR(MAX) = ''

	SELECT 
		NAME, EMAIL 
	INTO #TEMP 
	FROM TB_M_SUPPLIER_CONTACT_PERSON P 
		WHERE AREA_CD = (SELECT AREA_CD FROM TB_M_SUPPLIER_AREA WHERE AREA_NAME = 'MARKETING')
		AND P.SUPPLIER_CD = @SUPP_CD

	SELECT @email = STUFF((SELECT ';' + EMAIL from #TEMP FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'');
	SELECT @name = STUFF((SELECT ', ' + NAME from #TEMP FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'');

	SET @subject = '[IPPCS] Supplier Performance Report Release for Prod Month '+@PROD_MONTH;

	SET @body = (SELECT 'Dear '+@name+' </br></br> Please do informed that Supplier Performance Report for Prod Month '+@PROD_MONTH+' has been released. </br></br>
				<a href="https://portal.toyota.co.id">Go To Supplier Performance Status</a> </br></br>
				Best Regards, </br> IPPCS ADMIN')


	if(isnull(@email, '') != '')
	BEGIN
		EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'IPPCS NOTIFICATION',
				@recipients = @email,
				@subject = @subject, 
				@body = @body,
				@body_format = 'HTML';
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT 1 FROM TB_R_SUPPLIER_TMMIN_COMMENT WHERE PROD_MONTH = CONVERT(DATETIME, @PROD_MONTH  + '01', 112) AND SUPPLIER_CD = @SUPP_CD)
		BEGIN
			IF NOT EXISTS (SELECT 1 FROM TB_R_SUPPLIER_TMMIN_COMMENT WHERE PROD_MONTH = CONVERT(DATETIME, @PROD_MONTH  + '01', 112) AND SUPPLIER_CD = @SUPP_CD AND COMMENT LIKE '%Please maintain your email%')
			BEGIN
				UPDATE TB_R_SUPPLIER_TMMIN_COMMENT
					SET COMMENT = COMMENT + ' (Please maintain your email)'
				WHERE 
					PROD_MONTH = CONVERT(DATETIME, @PROD_MONTH  + '01', 112) AND SUPPLIER_CD = @SUPP_CD
			END
		END
		ELSE
		BEGIN
			INSERT INTO TB_R_SUPPLIER_TMMIN_COMMENT VALUES(CONVERT(DATETIME, @PROD_MONTH  + '01', 112), @SUPP_CD, 'Please maintain your email.', 'system', getdate(), null, null)
		END



	
		SELECT @body = 'Dear TMMIN PUD, <br> Please contact '+@SUPP_CD+' to maintain the email.<br><br> Regards. <br> IPPCS Admin.'

		--send email
		EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'IPPCS NOTIFICATION',
				@recipients = 'ujang@toyota.co.id;bayu.kurniadi@toyota.co.id',
				@subject = '[IPPCS] Supplier Performance Status Release', 
				@body = @body,
				@body_format = 'HTML';

	END



	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL 
	DROP TABLE #TEMP

END

