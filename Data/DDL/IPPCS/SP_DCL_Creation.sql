-- =============================================
-- Author: ISTD.Aang
-- Create date: 2015-03-25
-- Desc: Delete Data Delivery Synchronization DCL
-- =============================================
CREATE PROCEDURE [dbo].[SP_DCL_Creation] 
AS 
BEGIN
	DECLARE 
	@datefrom varchar(20),
	@dateto varchar(20), 
	@dateparam varchar(40), 
	@CountDeliveryNo INT
		
	set @dateparam = (select SYSTEM_VALUE from TB_M_SYSTEM WHERE SYSTEM_CD = 'Sync DCL' AND FUNCTION_ID = 'DCL_SYNC') 
	set @datefrom = LEFT(@dateparam, 10) --datafrom
	set @dateto = RIGHT(@dateparam, 10) --dateto
	
	/*LOCKING PROCESS JOB*/
	update TB_M_SYSTEM 
	set SYSTEM_VALUE = 'Y', 
		CHANGED_BY = 'DCL.Job', 
		CHANGED_DT = GETDATE()
	where SYSTEM_CD = 'LOCK_PROCESS_DCL_CREATION'	

	/*Count delivery no that have delivery status <> initial or downloaded, 
	if there was null so can continue process dclcreation */
	
	set @CountDeliveryNo = (SELECT COUNT(1) FROM TB_R_DELIVERY_CTL_H 
								WHERE PICKUP_DT >= @datefrom 
								AND PICKUP_DT <= @dateto
								AND DELIVERY_STS not in ('Initial', 'Downloaded')
								AND DELIVERY_NO LIKE 'R%'
							)
	--select @CountDeliveryNo
	
	/* Only continue dclcreation (sp_dclreceiving) if there is no data with 
	delivery status = Arrived, Posted, or Paid etc */
	
	IF (@CountDeliveryNo = 0)
	BEGIN
		/*backup delivery no*/
		INSERT INTO TB_H_DELIVERY_CTL_H_DELETED
		SELECT DELIVERY_NO, PICKUP_DT, [ROUTE], LP_CD, GETDATE()
			FROM TB_R_DELIVERY_CTL_H
			WHERE PICKUP_DT >= @datefrom 
					and PICKUP_DT <= @dateto 
					and DELIVERY_NO LIKE 'R%'
					
		/*Delete data delivery control detail, route & Supplier Dock*/
		DELETE from TB_R_DELIVERY_CTL_D 
			where DELIVERY_NO in 
			(select DELIVERY_NO 
				from TB_R_DELIVERY_CTL_H 
					where PICKUP_DT >= @datefrom 
					and PICKUP_DT <= @dateto 
					and DELIVERY_NO LIKE 'R%');
					
		DELETE from TB_R_DELIVERY_CTL_ROUTE 
			where DELIVERY_NO in 
			(select DELIVERY_NO 
				from TB_R_DELIVERY_CTL_H 
					where PICKUP_DT >= @datefrom 
					and PICKUP_DT <= @dateto 
					and DELIVERY_NO LIKE 'R%');

		DELETE from TB_R_DELIVERY_CTL_SUPPLIERDOCK 
			where DELIVERY_NO in 
			(select DELIVERY_NO 
				from TB_R_DELIVERY_CTL_H 
					where PICKUP_DT >= @datefrom
					and PICKUP_DT <= @dateto 
					and DELIVERY_NO LIKE 'R%');

		--ADDED fid.deny 2015-05-11
		DELETE from TB_R_DELIVERY_CTL_GATE 
			where DELIVERY_NO in 
			(select DELIVERY_NO 
				from TB_R_DELIVERY_CTL_H 
					where PICKUP_DT >= @datefrom
					and PICKUP_DT <= @dateto 
					and DELIVERY_NO LIKE 'R%');
		--end of ADDED fid.deny

		/*Delete data delivery control Header*/
		DELETE from TB_R_DELIVERY_CTL_H 
			where PICKUP_DT >= @datefrom 
				and PICKUP_DT <= @dateto 
				and DELIVERY_NO LIKE 'R%';
				
	/*begin try catch for dcl creation, exec sp_dclreceiving */
		begin try	
			exec sp_dclreceiving
			exec SP_DCL_Creation_Email 'success'
			--Send email succes/done
		end try
		
		begin catch
			exec SP_DCL_Creation_Email 'error'
			--raise error, send email 
		end catch
	END --end if
	
	/*ERROR condition, there was any delivery no that have delivery_sts <> initial or downloaded*/
	ELSE IF @CountDeliveryNo > 0 
		--Raise error, delivery status
	BEGIN
		 Select @CountDeliveryNo as Delivery_no
		 exec SP_DCL_Creation_Email 'statusFail'
	END --end else
	
	/*UNLOCKING PROCESS JOB*/
	update TB_M_SYSTEM 
	set SYSTEM_VALUE = 'N', 
		CHANGED_BY = 'DCL.Job', 
		CHANGED_DT = GETDATE()
	where SYSTEM_CD = 'LOCK_PROCESS_DCL_CREATION'	
END

