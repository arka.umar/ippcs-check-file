CREATE PROCEDURE [dbo].[IntaksMasterCompletenessCheck]
AS
BEGIN
	DECLARE @CREATED_DT DATETIME = GETDATE()
	DECLARE @MONTH VARCHAR(2) --= RIGHT('00' + CONVERT(VARCHAR(2), MONTH(@CREATED_DT) + 1), 2)
	DECLARE @YEAR VARCHAR(4) --= CONVERT(VARCHAR(4), YEAR(@CREATED_DT))
	DECLARE @VERS CHAR(1) = 'F'
	DECLARE @CREATED_BY VARCHAR(20) = 'LPOP Agent'
	DECLARE @SQL NVARCHAR(MAX) = ''
	DECLARE @i_PROD_MONTH VARCHAR(6) = ''

	SELECT @i_PROD_MONTH = MAX(PACK_MONTH) FROM TB_R_LPOP
	SET @MONTH = RIGHT(@i_PROD_MONTH, 2)
	SET @YEAR = LEFT(@i_PROD_MONTH, 4)

	DELETE FROM TB_R_LPOP_NOTIFICATION WHERE PROD_MONTH = @YEAR + @MONTH

	DECLARE @PROD_MONTH char(6),
			@PART_NO varchar(12),
			@DOCK_CD char(2),
			@PROD_PURPOSE_CD char(1),
			@PLANT_CD char(4),
			@ORDER_TYPE char(1),
			@SUPPLIER_CD varchar(6),
			@PART_LIST_EXISTS char(2),
			@SOURCE_LIST_EXISTS char(2),
			@MATERIAL_MASTER_EXISTS char(2),
			@MATERIAL_PRICE_EXISTS char(2)

	INSERT INTO TB_R_LPOP_NOTIFICATION
		(
			[NO],
			PROD_MONTH,
			PART_NO,
			DOCK_CD,
			PROD_PURPOSE_CD,
			PLANT_CD,
			ORDER_TYPE,
			SUPPLIER_CD,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			CREATED_BY,
			CREATED_DT
		)
	SELECT ROW_NUMBER() OVER(ORDER BY PART_NO) as [NO], TBL1.*
	FROM (

		SELECT
			'000000' as prod_month,
			substring(part_no, 1, 10) as part_no,
			'X' as prod_purpose,
			m.DOCK_CD,
			m.RCV_PLANT_CD, 
			'' as ad,
			SUPPLIER_CD, 
			sum(p.order_qty) as n_vol_1,
			0 as n_vol_2,
			0 as n_vol_3,
			'INTAKS' as username,
			getdate() as dat
		FROM TB_R_dAILY_ORDER_MANIFEST m
		inner join tb_r_daily_order_part p
		on m.manifest_no = p.manifest_no
		WHERE m.CREATED_DT > '2016-03-22'
		AND MANIFEST_RECEIVE_FLAG IN ('5')
		AND SUPPLIER_CD IN ('5022','0003','5011','5007','5024','5044')
		GROUP BY m.RCV_PLANT_CD, SUPPLIER_CD, M.DOCK_CD,part_no
		HAVING sum(p.order_qty) > 0
		--ORDER BY m.RCV_PLANT_CD, SUPPLIER_CD, M.SUPPLIER_PLANT,part_no


		--SELECT
		--	PACK_MONTH,
		--	SUBSTRING(PART_NO, 1, 10) as PART_NO,
		--	DOCK_CD,
		--	'D' as PROD_PURPOSE_CD,
		--	ISNULL(NULLIF(LEFT(CONVERT(VARCHAR(4), ISNULL(LTRIM(RTRIM(R_PLANT_CD)), '')) + '0000', 4), '0000'), '') as PLANT_CD,
		--	ORDER_TYPE,
		--	SUPPLIER_CD,
		--	SUM(ISNULL(CONVERT(INT, N_1_VOLUME), '')) as N_1_VOLUME,
		--	SUM(ISNULL(CONVERT(INT, N_2_VOLUME), '')) as N_2_VOLUME,
		--	SUM(ISNULL(CONVERT(INT, N_3_VOLUME), '')) as N_3_VOLUME,
		--	@CREATED_BY as CREATED_BY,
		--	@CREATED_DT as CREATED_DT
		--FROM TB_R_LPOP 
		--WHERE PACK_MONTH >= @YEAR + @MONTH 
		--	  AND VERS = @VERS 
		--	  AND (N_VOLUME <= 0 AND (N_1_VOLUME > 0 OR N_2_VOLUME > 0 OR N_3_VOLUME > 0))
		--GROUP BY
		--	PACK_MONTH,
		--	SUBSTRING(PART_NO, 1, 10),
		--	DOCK_CD,
		--	R_PLANT_CD,
		--	ORDER_TYPE,
		--	SUPPLIER_CD
	) TBL1

	DECLARE @MAX_NO BIGINT = (SELECT ISNULL(MAX([NO]), 0) FROM TB_R_LPOP_NOTIFICATION) --WHERE PROD_MONTH = @YEAR + @MONTH)
	DECLARE @MIN_NO BIGINT = (SELECT ISNULL(MIN([NO]), 0) FROM TB_R_LPOP_NOTIFICATION) --WHERE PROD_MONTH = @YEAR + @MONTH)
	DECLARE @i BIGINT = 0

	SET @i = @MIN_NO
	WHILE(@i <= @MAX_NO)
	BEGIN

		SET @PART_LIST_EXISTS = ''
		SET @SOURCE_LIST_EXISTS = ''
		SET @MATERIAL_MASTER_EXISTS = ''
		SET @MATERIAL_PRICE_EXISTS = ''

		--IF(EXISTS(SELECT 'x' FROM TB_R_LPOP_NOTIFICATION WHERE [NO] = @i AND PROD_MONTH = @YEAR + @MONTH))
		--BEGIN
			SELECT 
				@PROD_MONTH = PROD_MONTH,
				@PART_NO = PART_NO,
				@DOCK_CD = DOCK_CD,
				@PROD_PURPOSE_CD = PROD_PURPOSE_CD,
				@PLANT_CD = PLANT_CD,
				@ORDER_TYPE = ORDER_TYPE,
				@SUPPLIER_CD = SUPPLIER_CD
			FROM TB_R_LPOP_NOTIFICATION
			WHERE [NO] = @i
				  --AND PROD_MONTH = @YEAR + @MONTH

			SET @SQL = 'select @PART_LIST_EXISTS_n = case when d > 0 then ''OK'' ELSE ''NG'' END from openquery([IPPCS_TO_ICS], 
						''select count(1) as d from tb_m_part_list
							where mat_no = ''''' + @PART_NO + ''''' 
								  and prod_purpose_cd = ''''' + @PROD_PURPOSE_CD + '''''
								  and source_type = ''''' + @ORDER_TYPE + '''''
								  and plant_cd = ''''' + @PLANT_CD + '''''
								  and dock_cd = ''''' + @DOCK_CD + '''''
								  and rownum = 1
							'')'
			EXEC sp_executesql @SQL, N'@PART_LIST_EXISTS_n char(2) OUTPUT', @PART_LIST_EXISTS_n = @PART_LIST_EXISTS OUTPUT

			SET @SQL = 'select @SOURCE_LIST_EXISTS_n = case when d > 0 then ''OK'' ELSE ''NG'' END from openquery([IPPCS_TO_ICS], 
						'' select count(1) as d from tb_m_source_list
							where mat_no = ''''' + @PART_NO + ''''' 
								  and prod_purpose_cd = ''''' + @PROD_PURPOSE_CD + '''''
								  and source_type = ''''' + @ORDER_TYPE + '''''
								  and supp_cd = ''''' + @SUPPLIER_CD + '''''
								  and to_char(valid_dt_fr, ''''YYYYMM'''') <= ''''' + @YEAR + @MONTH + '''''
								  and to_char(valid_dt_to, ''''YYYYMM'''') >= ''''' + @YEAR + @MONTH + '''''
								  and rownum = 1
						'')'
			EXEC sp_executesql @SQL, N'@SOURCE_LIST_EXISTS_n char(2) OUTPUT', @SOURCE_LIST_EXISTS_n = @SOURCE_LIST_EXISTS OUTPUT

			SET @SQL = 'select @MATERIAL_MASTER_EXISTS_n = case when d > 0 then ''OK'' ELSE ''NG'' END from openquery([IPPCS_TO_ICS], 
						'' select count(1) as d from tb_m_material
							where mat_no = ''''' + @PART_NO + ''''' 
								  and deletion_flag = ''''N''''
								  and rownum = 1
						'')'
			EXEC sp_executesql @SQL, N'@MATERIAL_MASTER_EXISTS_n char(2) OUTPUT', @MATERIAL_MASTER_EXISTS_n = @MATERIAL_MASTER_EXISTS OUTPUT

			SET @SQL = 'select @MATERIAL_PRICE_EXISTS_n = case when d > 0 then ''OK'' ELSE ''NG'' END from openquery([IPPCS_TO_ICS], 
						'' select count(1) as d from tb_m_material_price
							where mat_no = ''''' + @PART_NO + ''''' 
								  and prod_purpose_cd = ''''' + @PROD_PURPOSE_CD + '''''
								  and source_type = ''''' + @ORDER_TYPE + '''''
								  and supp_cd = ''''' + @SUPPLIER_CD + '''''
								  and to_char(valid_dt_fr, ''''YYYYMM'''') <= ''''' + @YEAR + @MONTH + '''''
								  and to_char(valid_dt_to, ''''YYYYMM'''') >= ''''' + @YEAR + @MONTH + '''''
								  and rownum = 1
						'')'
			EXEC sp_executesql @SQL, N'@MATERIAL_PRICE_EXISTS_n char(2) OUTPUT', @MATERIAL_PRICE_EXISTS_n = @MATERIAL_PRICE_EXISTS OUTPUT

			UPDATE lpop
				SET PART_LIST_EXISTS = @PART_LIST_EXISTS,
					SOURCE_LIST_EXISTS = @SOURCE_LIST_EXISTS,
					MATERIAL_MASTER_EXISTS = @MATERIAL_MASTER_EXISTS,
					MATERIAL_PRICE_EXISTS = @MATERIAL_PRICE_EXISTS
			FROM TB_R_LPOP_NOTIFICATION lpop
			WHERE [NO] = @i
				  --AND PROD_MONTH = @YEAR + @MONTH
		--END

		SET @i = @i + 1
	END
END

