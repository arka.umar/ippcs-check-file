-- =============================================
-- Author:		fid.Joko
-- Create date: 23.09.2013
-- Description:	Insert Invoice Payment ICS
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_InsertPostingInvoice_backup] 
	-- Add the parameters for the stored procedure here
	@processId BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @sqlQuery AS VARCHAR(MAX),
		@function VARCHAR(10),
		@INVOICE_NO VARCHAR(16),
		@na VARCHAR(50), 
		@SupplierInvoiceNo VARCHAR(12),
		@SupplierCode VARCHAR(6),
		@FlagSuccess BIT,
		@stepId INT, 
		@ErrorMessage VARCHAR(MAX);
		
	SET @stepId = 1;
	CREATE TABLE #ICS_FINISH(INVOICE_NO VARCHAR(16), SupplierInvoiceNo VARCHAR(12), SupplierCode VARCHAR(6))
	
	SET @function = '53002';
	SET @na = 'SP_ICS_InsertPostingInvoice';
	EXEC sp_PutLog 
		'Get data posting is started'
		, 'System'
		, @na
		, @processId
		, 'MPCS00002INF'
		, ''
		, '5'
		, @function
		, 0
	
	Begin Try
	INSERT INTO TB_T_LOCK
           ([FUNCTION_NO]
           ,[LOCK_REFF]
           ,[CREATED_BY]
           ,[CREATED_DT])
    VALUES
		(@function
		,@processId
		,'System'
		,GETDATE())
		
	BEGIN TRANSACTION IPPCSSP_ICS_InsertPostingInvoice
	-- ============================================
	-- PHASE 1 : GET INVOICE_NO ICS PROCESS
	-- ============================================
	SET @stepId = 2;
	SET @sqlQuery = 'INSERT INTO #ICS_FINISH SELECT inv_no, SUPP_INVOICE_NO, SUPP_CD FROM OPENQUERY([IPPCS_TO_ICS] , ''select * from tb_t_invoice_h where other_process_id = ' + CONVERT(VARCHAR, @processID) + ''')';
	EXEC (@sqlQuery);
	
	SELECT @INVOICE_NO=INVOICE_NO,
		@SupplierInvoiceNo=SupplierInvoiceNo,
		@SupplierCode=SupplierCode
	FROM #ICS_FINISH
	
	EXEC sp_PutLog 
		'Get Invoice_No From Finished ICS Process'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	SET @stepId = 3;
	
	--INSERT INTO TB_T_LOCK
 --          ([FUNCTION_NO]
 --          ,[LOCK_REFF]
 --          ,[CREATED_BY]
 --          ,[CREATED_DT])
 --   VALUES
	--	(@function
	--	,@processId
	--	,'System'
	--	,GETDATE())
	
	-- ==============================================
	-- PHASE 2 : INSERT INTO Invoice Table
	-- ==============================================
	EXEC sp_PutLog 
		'Insert TB_R_INV_H'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 4;
	SET @sqlQuery = 'INSERT INTO TB_R_INV_H (
						INV_NO,INV_DT,SUPP_INV_NO,SUPPLIER_CD,CURRENCY_CD,EXCHANGE_RATE,INV_AMT,INV_LOCAL_AMT,INV_TAX_NO,INV_TEXT,
						PAYMENT_TERM_CD,PAYMENT_METHOD_CD,SUPP_BANK_TYPE,BALANCE,INV_DESC,WITHHOLDING_TAX_CD,BASE_AMT,WITHHOLDING_TAX_AMT,ASSIGNMENT,PAY_PLAN_DT,
						STATUS_CD,CALC_TAX_FLAG,REVERSE_FLAG,CREATED_BY,CREATED_DT,CHANGED_BY,IN_PROGRESS)
					SELECT 
						INV_NO,INV_DT,SUPP_INV_NO,SUPP_CD,PAYMENT_CURR,EXCHANGE_RATE,INV_AMT,INV_AMT_LOCAL_CURR,HEADER_TEXT,INV_TEXT,
						PAYMENT_TERM_CD,PAYMENT_METHOD_CD,PARTNER_BANK_KEY,BALANCE,INV_DESC,WITHOLDING_TAX_CD,WITHHOLDING_TAX_BASE_AMT,WITHHOLDING_TAX_AMT,ASSIGNMENT,NEXT_BASELINE_DT,
						4,CALC_TAX_FLAG,REVERSE_FLAG,CREATED_BY,CREATED_DT,CHANGED_BY,2
					FROM OPENQUERY([IPPCS_TO_ICS] , 
					''select h.*,wt.WITHOLDING_TAX_CD,h.BASELINE_DT+1 as NEXT_BASELINE_DT,wt.WITHHOLDING_TAX_BASE_AMT,wt.WITHHOLDING_TAX_AMT from TB_R_INVOICE_H h
					  left join TB_R_INVOICE_WITHOLDING_TAX wt ON h.INV_NO=wt.INV_NO
					  where h.INV_NO = ' + CONVERT(VARCHAR, @INVOICE_NO) + ''')';
	EXEC (@sqlQuery);
	
	EXEC sp_PutLog 
		'Insert TB_R_INV_D'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 5;
	SET @sqlQuery = 'INSERT INTO TB_R_INV_D (
						INV_NO,INV_DOC_ITEM,PO_NO,PO_ITEM_NO,COMP_PRICE_CD,MAT_DOC_NO,MAT_DOC_ITEM,MAT_NO_REF,INV_REF_NO,MAT_NO,
						PROD_PURPOSE_CD,SOURCE_TYPE_CD,PLANT_CD,SLOC_CD,PACKING_TYPE,PART_COLOR_SFX,DOC_DT,QTY,PRICE_AMT,
						SUPP_MANIFEST,INV_AMT,INV_LOCAL_AMT,CONDITION_CATEGORY,RETRO_DOC_NO,TAX_CD,UOM_CD,MAT_DESC,
						CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT)
					SELECT 
						INV_NO,INV_DOC_ITEM,PO_NO,PO_ITEM_NO,COMP_PRICE_CD,MAT_DOC_NO,MAT_DOC_ITEM,ORI_MAT_NO,REFF_NO,MAT_NO,
						PROD_PURPOSE_CD,SOURCE_TYPE,ISNULL(PLANT_CD,''''),ISNULL(SLOC_CD,''''),PACKING_TYPE_CD,PART_COLOR_SFX,DOC_DT,QUANTITY,PRICE_AMT,
						SUPP_MANIFEST,INV_AMT,INV_AMT_LOCAL_CURR,CONDITION_CATEGORY,ISNULL(RETRO_DOC_NO,''''),TAX_CD,UNIT_OF_MEASURE_CD,MAT_DESC,
						CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT
					FROM OPENQUERY([IPPCS_TO_ICS] , 
					''select d.*,m.MAT_DESC from TB_R_INVOICE_D d
					left join TB_M_MATERIAL m ON d.MAT_NO=m.MAT_NO
					where d.INV_NO = ' + CONVERT(VARCHAR, @INVOICE_NO) + ''')';
	EXEC (@sqlQuery);
	
	EXEC sp_PutLog 
		'Insert TB_R_INV_TAX'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 6;
	SET @sqlQuery = 'INSERT INTO TB_R_INV_TAX(
						INVOICE_TAX_NO,INVOICE_NO,TAX_CD,CURRENCY_CD,TAX_AMOUNT,LOCAL_TAX_AMOUNT,INVOICE_TAX_DT,
						CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT)
					SELECT 
						HEADER_TEXT,INV_NO,TAX_CD,CURR_CD,TAX_AMT,LOCAL_TAX_AMT,TAX_DT,
						CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT
					FROM OPENQUERY([IPPCS_TO_ICS] , 
					''select it.*,h.HEADER_TEXT,h.TAX_DT from TB_R_INVOICE_TAX it
					  inner join TB_R_INVOICE_H h on it.INV_NO=h.INV_NO
					where it.INV_NO = ' + CONVERT(VARCHAR, @INVOICE_NO) + ''')';
	EXEC (@sqlQuery);
	
	EXEC sp_PutLog 
		'Insert TB_R_INV_GL_ACCOUNT'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	SET @stepId = 7;
	SET @sqlQuery = 'INSERT INTO TB_R_INV_GL_ACCOUNT (
						INV_NO,GL_ACCOUNT,INV_AMT,COST_CENTER,TAX_CD,
						CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT)
					SELECT 
						INV_NO,GL_ACCOUNT,INV_AMT,COST_CENTER_CD,TAX_CD,
						CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT
					FROM OPENQUERY([IPPCS_TO_ICS] , ''select * from TB_R_INVOICE_GL_ACCOUNT where INV_NO = ' + CONVERT(VARCHAR, @INVOICE_NO) + ''')';
	EXEC (@sqlQuery);
	
	EXEC sp_PutLog 
		'Update TB_R_INV_H'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
		
	SET @stepId = 8;
	update h set
		h.SUBMIT_DT=tu.SUBMIT_DT,
		h.SUBMIT_BY=tu.SUBMIT_BY,
		h.POSTING_DT=tu.POSTING_DT,
		h.POSTING_BY=tu.POSTING_BY,
		h.TURN_OVER=tu.TURN_OVER,
		h.TOTAL_MANIFEST=tu.TOTAL_MANIFEST,
		h.STAMP_FLAG=tu.STAMP_FLAG,
		h.CERTIFICATE_ID=tu.CERTIFICATE_ID,
		h.CREATED_DT=tu.CREATED_DT,
		h.CREATED_BY=tu.CREATED_BY,
		h.CHANGED_BY=tu.POSTING_BY,
		h.CHANGED_DT=GETDATE()
	FROM TB_R_INV_H h
	INNER JOIN TB_R_INV_UPLOAD tu ON h.SUPP_INV_NO=tu.SUPP_INV_NO AND h.SUPPLIER_CD=tu.SUPP_CD
	WHERE h.INV_NO=@INVOICE_NO
	
	SET @stepId = 9;
	UPDATE gr SET
		gr.STATUS_CD='4',
		gr.CHANGED_BY='AGENT',
		gr.CHANGED_DT=GETDATE()
	FROM TB_R_INV_UPLOAD tu
	INNER JOIN TB_R_GR_IR gr ON gr.PO_NO=tu.PO_NO AND 
								gr.PO_ITEM_NO=tu.PO_ITEM_NO AND
								gr.MAT_DOC_NO=tu.MAT_DOC_NO AND
								gr.ITEM_NO=tu.MAT_DOC_ITEM AND
								gr.COMP_PRICE_CD=tu.COMP_PRICE_CD
	WHERE tu.SUPP_INV_NO=@SupplierInvoiceNo AND tu.SUPP_CD=@SupplierCode

	EXEC sp_PutLog 
		'Insert TB_T_INV_GL_ACCOUNT_LOG'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 10;
	INSERT INTO TB_T_INV_GL_ACCOUNT_LOG
	SELECT * from TB_T_INV_GL_ACCOUNT
	WHERE SUPP_INVOICE_NO=@SupplierInvoiceNo
	
	EXEC sp_PutLog 
		'Insert TB_R_INV_UPLOAD_LOG'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 11;
	INSERT INTO TB_R_INV_UPLOAD_LOG
	SELECT * from TB_R_INV_UPLOAD
	WHERE SUPP_INV_NO=@SupplierInvoiceNo AND SUPP_CD=@SupplierCode
	
	EXEC sp_PutLog 
		'Delete TB_T_INV_GL_ACCOUNT'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 12;
	delete from TB_T_INV_GL_ACCOUNT
	WHERE SUPP_INVOICE_NO=@SupplierInvoiceNo
	      	
	EXEC sp_PutLog 
		'Delete TB_R_INV_UPLOAD'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 13;
	delete from TB_R_INV_UPLOAD
	WHERE SUPP_INV_NO=@SupplierInvoiceNo AND SUPP_CD=@SupplierCode
	
	SET @stepId = 14;
	UPDATE TB_R_DAILY_ORDER_MANIFEST SET
		MANIFEST_RECEIVE_FLAG=6,
		CHANGED_BY='AGENT',
		CHANGED_DT=GETDATE()
	WHERE MANIFEST_NO IN(
		select manifest_no
		from 
		(
			select manifest_no,part_no 
			from tb_r_daily_order_part 
			group by manifest_no,part_no 
		)p
		left join(
			select inv_no,inv_ref_no,part_no=(mat_no+part_color_sfx) 
			from TB_R_INV_D
			where INV_NO=@INVOICE_NO
		)Qry on p.manifest_no=Qry.inv_ref_no and p.part_no=Qry.part_no
		where p.manifest_no in(select INV_REF_NO from TB_R_INV_D where INV_NO=@INVOICE_NO)
		group by manifest_no
		having SUM(case when Qry.inv_no is null then 1 else 0 end)=0
	)
	
	EXEC sp_PutLog 
		'Release Lock and Update TB_R_ICS_QUEUE'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 15;
	DELETE FROM [TB_T_LOCK]
	WHERE
		[FUNCTION_NO] = @function
        AND [LOCK_REFF] = @processId;

	SET @stepId = 16;
	UPDATE TB_R_ICS_QUEUE
	SET
		PROCESS_STATUS = 2,
		ICS_STATUS = 'ICS FINISHED', 
		CHANGED_BY = 'SYSTEM', 
		CHANGED_DT=GETDATE()
	WHERE
		IPPCS_MODUL = '5'
		AND IPPCS_FUNCTION = @function
		AND PROCESS_ID = @processId
		AND PROCESS_STATUS = '1'

	SET @FlagSuccess = 1
	COMMIT TRANSACTION IPPCSSP_ICS_InsertPostingInvoice;	
	End Try
	Begin Catch
		select @ErrorMessage= ISNULL(ERROR_MESSAGE(),'') 
			+ ' @line: ' + ISNULL(CONVERT(VARCHAR, ERROR_LINE()), '') 
			+ ' @step:' + CONVERT(varchar, @stepId);
		
		Rollback Transaction IPPCSSP_ICS_InsertPostingInvoice;
		SET @ErrorMessage = @ErrorMessage + ' [Release Lock and Update TB_R_ICS_QUEUE]';
		
		EXEC sp_PutLog 
		@ErrorMessage
		, 'System'
		, @na
		, @processId
		, 'MPCS00008ERR'
		, ''
		, '5'
		, @function
		, 0
	
	End Catch
	
	Begin Try
	BEGIN TRANSACTION IPPCSSP_ICS_PostingInvoice
	
	DECLARE @Email varchar(1000),
		@fullName VARCHAR(200),
		@body VARCHAR(MAX)
	SELECT @Email=m.EMAIL, @fullName =  m.FULL_NAME
	FROM TB_R_INV_H h
	inner join dbo.TB_R_VW_EMPLOYEE m ON h.POSTING_BY=m.USERNAME
	WHERE INV_NO=@INVOICE_NO
	
	SET @body = 'Dear Mr./Mrs. ' + ISNULL(@fullName, '') + ',' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) +
				'Please be informed that your Invoice with Supplier Invoice No ' + CONVERT(VARCHAR,@SupplierInvoiceNo) + ' is posted with Invoice Document No ' + CONVERT(VARCHAR,@INVOICE_NO) +  CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) +
				'This email is autogenerated by system, please do not reply' +  CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) +
				'Thank you for your attention '
			
	IF @Email <> ''
	BEGIN	
		SET @stepId = 16;
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = 'IPPCS Notification',
			@body = @body,
			@recipients = @Email,
			--@copy_recipients = 'supplierportal6@toyota.co.id;vandy.cahyadi@toyota.co.id',
			--@recipients = 'supplierportal8@toyota.co.id;vandy.cahyadi@toyota.co.id',
			@subject = '[IPPCS] Post Invoice Notification' ;
	END
	
	/* Remarked by imans, 26-10-2013
	-- Move above, Error on mail should not related with sending commit
	COMMIT TRANSACTION IPPCSSP_ICS_InsertPostingInvoice		
	End Try
	Begin Catch
		select ERROR_MESSAGE();
		Rollback Transaction IPPCSSP_ICS_InsertPostingInvoice;
	End Catch
	*/
	
	COMMIT TRANSACTION IPPCSSP_ICS_PostingInvoice		
	End Try
	Begin Catch
		SELECT @ErrorMessage= ISNULL(ERROR_MESSAGE(),'') 
			+ ' @line: ' + ISNULL(CONVERT(VARCHAR, ERROR_LINE()), '') 
			+ ' @step:' + CONVERT(varchar, @stepId);
		
		SET @ErrorMessage = @ErrorMessage + ' [Release Lock and Update TB_R_ICS_QUEUE]';
		
		EXEC sp_PutLog 
		@ErrorMessage
		, 'System'
		, @na
		, @processId
		, 'MPCS00008ERR'
		, ''
		, '5'
		, @function
		, 0
	
	End Catch
	
	IF @FlagSuccess = 1
	BEGIN
		SET @stepId = 17;
		SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Invoice_h WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
		SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
		EXEC (@sqlQuery)
		
		SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Invoice_d WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
		SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
		EXEC (@sqlQuery)
		
		SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Inv_Gl_Account WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
		SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
		EXEC (@sqlQuery)
		
		SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Inv_Witholding_Tax WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
		SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
		EXEC (@sqlQuery)
	END
	
	EXEC sp_PutLog 
		'Get data posting is finished'
		, 'System'
		, @na
		, @processId
		, 'MPCS00003INF'
		, ''
		, '5'
		, @function
		, 0
	
END

