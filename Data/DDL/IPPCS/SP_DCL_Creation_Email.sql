-- =============================================
-- Author:		fid.deny
-- Create date: 2015-03-26
-- Description:	Email notification DCL CREATION
-- =============================================
CREATE PROCEDURE [dbo].[SP_DCL_Creation_Email]
	@type varchar(20) 
AS
BEGIN
	
	
	DECLARE @emailheader varchar(max),
			@emailcontent varchar(max),
			@text varchar(max) 
			--@subject_status varchar
	
	DECLARE @email varchar(MAX)
	SELECT @email = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_TLMS' AND SYSTEM_CD = 'DCL';

	if(@type = 'success')
	BEGIN
		SET @emailcontent = '<b>DCL CREATION SUCCESS.</b>'
		--SET @subject_status = 'DCL Creation - Finished Successfully'
		
	END
	else if (@type = 'error')
	BEGIN
		SET @emailcontent = '<b>DCL CREATION ERROR.</b>'
		--SET @subject_status = 'DCL Creation - Finished with Error'
	END
	else 
	BEGIN
		SET @emailcontent = '<b>DCL CREATION FAILED.</b><br>'
		--SET @subject_status = 'DCL Creation - Finished with Error'
	END


	BEGIN TRY
		SET @text = 'TLMS Synchronize Notification </br>'
		SET @text = @text + ' ' + @emailcontent
		
		SET @text = @text + '</br></br></br>Regards.</br>'
		SET @text = @text + 'IPPCS Admin'


		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'IPPCS NOTIFICATION',
		@recipients = @email,
		@subject = 'DCL Creation Finished',
		@body = @text,
		@body_format = 'HTML';

	END TRY
	
	--BEGIN CATCH
	--	print ERROR_MESSAGE()
	--END CATCH
	Begin Catch
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		return
	End Catch










END

