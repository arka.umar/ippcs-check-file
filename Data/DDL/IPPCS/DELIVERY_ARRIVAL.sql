CREATE VIEW [dbo].[DELIVERY_ARRIVAL]
AS
SELECT     TOP (100) PERCENT X.GATE_CODE_IN, X.DELIVERY_NO, X.SCAN_DT, X.ROUTE, X.RATE, X.PICKUP_DT, X.LP_CD, X.STATUS, X.TIMING, X.GET_DATE
FROM         (SELECT     G.GATE_CODE_IN, G.DELIVERY_NO, G.PLAN_IN_DT AS SCAN_DT, H.ROUTE, H.RATE, H.PICKUP_DT, H.LP_CD, CASE WHEN ACTUAL_IN_DT IS NULL AND 
                                              PLAN_IN_DT <= GETDATE() THEN '2' WHEN ACTUAL_IN_DT IS NULL AND PLAN_IN_DT >= GETDATE() THEN '5' WHEN STATUS_IN IN ('1', '3', '99') 
                                              THEN '4' ELSE '0' END AS STATUS, 'PLAN' AS TIMING, GETDATE() AS GET_DATE
                       FROM          dbo.TB_R_DELIVERY_CTL_GATE AS G INNER JOIN
                                              dbo.TB_R_DELIVERY_CTL_H AS H ON G.DELIVERY_NO = H.DELIVERY_NO
                       WHERE      (H.PICKUP_DT = CONVERT(VARCHAR(10), GETDATE(), 126))
                       UNION ALL
                       SELECT     G.GATE_CODE_IN, G.DELIVERY_NO, ISNULL(G.ACTUAL_IN_DT, CASE WHEN G.PLAN_IN_DT < GETDATE() THEN GETDATE() ELSE G.PLAN_IN_DT END) 
                                             AS SCAN_DT, H.ROUTE, H.RATE, H.PICKUP_DT, H.LP_CD, G.STATUS_IN AS STATUS, 'ACTUAL' AS TIMING, GETDATE() AS GET_DATE
                       FROM         dbo.TB_R_DELIVERY_CTL_GATE AS G INNER JOIN
                                             dbo.TB_R_DELIVERY_CTL_H AS H ON G.DELIVERY_NO = H.DELIVERY_NO
                       WHERE     (H.PICKUP_DT = CONVERT(VARCHAR(10), GETDATE(), 126))) AS X INNER JOIN
                      dbo.TB_M_LOGISTIC_PARTNER AS P ON X.LP_CD = P.LOG_PARTNER_CD
WHERE     (P.ACTIVE_FLAG = '2')
ORDER BY X.DELIVERY_NO, X.TIMING DESC

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[16] 4[18] 2[33] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "X"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 204
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "P"
            Begin Extent = 
               Top = 6
               Left = 242
               Bottom = 125
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2295
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DELIVERY_ARRIVAL'
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DELIVERY_ARRIVAL'
