CREATE PROCEDURE [dbo].[IndexMaintenancePhysicalValidation] 
	@i_db_name NVARCHAR(100) = NULL,
	@i_object_name NVARCHAR(100) = NULL,
	@i_schema_name NVARCHAR(100) = NULL,
	@i_index INT = NULL,
	@i_partition_no INT = NULL,
	@i_mode VARCHAR(30) = 'DETAILED',
	@i_data_pages INT = 1
AS
BEGIN
	DECLARE @db_name NVARCHAR(100) = @i_db_name,
			@object_name NVARCHAR(100) = @i_object_name,
			@schema_name NVARCHAR(100) = @i_schema_name,
			@index_id INT = @i_index,
			@partition_no INT = @i_partition_no,
			@mode varchar(30) = @i_mode,
			@just_data_pages INT = @i_data_pages

	IF OBJECT_ID('tempdb..#dps') IS NOT NULL 
			DROP TABLE #dps

	IF OBJECT_ID('tempdb..#data') IS NOT NULL 
			DROP TABLE #data

	DECLARE @db_id SMALLINT,
			@object_id INT

	SET @schema_name = CASE WHEN ISNULL(@schema_name, '') = '' THEN 'dbo' ELSE @schema_name END
	SET @object_name = CASE WHEN ISNULL(@object_name, '') = '' THEN '' ELSE @db_name + '.' + @schema_name + '.' + @object_name END

	SET @db_id = DB_ID(@db_name)
	SET @object_id = OBJECT_ID(@object_name)
			 
	SELECT
	* 
	INTO #dps
	FROM sys.dm_db_index_physical_stats	(
						@db_id, 
						@object_id, 
						@index_id, 
						@partition_no, 
						@mode)
	WHERE (index_level = 0 AND @just_data_pages = 1) OR @just_data_pages <> 1

	INSERT INTO TB_M_TABLE_STATUS
		(
			table_name,
			index_name,
			[schema_name],
			index_type,
			fill_factor,
			records,
			pages,
			ideal_est_pages,
			table_size,
			avg_fragmentation,
			user_seeks,
			user_scans,
			user_lookups,
			user_updates,
			last_user_lookup, 
			last_user_scan,
			last_user_seek,
			last_user_update,
			max_record_size,
			min_record_size,
			avg_record_size,
			avg_record_per_page,
			ghost_records,
			versioned_ghost_records,
			partition_no,
			result,
			allocation_unit,
			index_level,
			index_depth,
			avg_used_space_per_table,
			avg_free_space_per_table,
			query_result,
			date_retrieved
		)
	SELECT 
		a.*,
		(avg_rec_per_page * avg_rec_size) as avg_space_per_page,
		(8192 - 96) - (avg_rec_per_page * avg_rec_size) as avg_free_space_per_page,
		CASE 
			WHEN result = 'REBUILD' OR result = 'REORGANIZE' 
				THEN 'ALTER INDEX [' + a.index_nm + '] ON [' + schema_nm + '].[' + table_name + '] ' + result + partition_no
			ELSE '' END as Query,
		GETDATE()
	FROM (
			SELECT 
				t.name as table_name,
				ix.name as index_nm,
				s.name as schema_nm,
				case ix.index_id
					when 0 THEN 'Heap'
					when 1 THEN 'Clustered'
					else 'Non-Clustered'
				end as [index],
				ix.fill_factor,
				record_count as records,
				ps.page_count as pages,
				CASE avg_record_size_in_bytes 
					WHEN 0 THEN 0 ELSE CEILING(record_count / FLOOR((8192 - 96) / avg_record_size_in_bytes)) END as ideal_est_page,
				CONVERT(DECIMAL(18, 6), (ps.page_count * 8192)) as size,
				ps.avg_fragmentation_in_percent as avg_fragment,
				b.user_seeks, 
				b.user_scans, 
				b.user_lookups, 
				b.user_updates,
				b.last_user_lookup, 
				b.last_user_scan,
				b.last_user_seek,
				b.last_user_update,
				ps.min_record_size_in_bytes as min_rec_size,
				ps.max_record_size_in_bytes as max_rec_size,
				ps.avg_record_size_in_bytes as avg_rec_size,
				CASE avg_record_size_in_bytes WHEN 0 THEN 0 ELSE CONVERT(BIGINT, FLOOR((8192 - 96) / avg_record_size_in_bytes)) END avg_rec_per_page,
				ghost_record_count as ghost,
				version_ghost_record_count as vgr,
				CASE WHEN pc.partition_count > 1 THEN ' PARTITION = ' + cast(ps.partition_number as nvarchar(max)) ELSE '' END as partition_no,
				CASE 
					WHEN (ps.avg_fragmentation_in_percent >= 20 
							AND ps.avg_fragmentation_in_percent <= 40 
							AND page_count > 1000
							AND ix.index_id > 0)
							THEN 'REORGANIZE' 
					WHEN (ps.avg_fragmentation_in_percent > 40
							AND page_count > 1000
							AND ix.index_id > 0)
							THEN 'REBUILD'
					WHEN ((ps.avg_fragmentation_in_percent >= 20 
							AND ps.avg_fragmentation_in_percent <= 40 
							AND page_count > 1000
							AND ix.index_id = 0)
							OR (ps.avg_fragmentation_in_percent > 40
							AND page_count > 1000
							AND ix.index_id = 0))
							THEN 'CREATE INDEX'
					ELSE '' 
				END as result,
				ps.alloc_unit_type_desc,
				ps.index_level,
				ps.index_depth
			--INTO #data
			FROM sys.indexes AS ix 
				INNER JOIN sys.tables t
					ON t.object_id = ix.object_id
				INNER JOIN sys.schemas s
					ON t.schema_id = s.schema_id
				INNER JOIN #dps ps
					ON t.object_id = ps.object_id 
						AND ix.index_id = ps.index_id
				INNER JOIN (SELECT 
								object_id, 
								index_id, 
								COUNT(DISTINCT partition_number) AS partition_count
							FROM sys.partitions
							GROUP BY object_id, index_id) pc
					ON t.object_id = pc.object_id 
						AND ix.index_id = pc.index_id
				LEFT JOIN sys.dm_db_index_usage_stats b 
					ON b.object_id =  ix.object_id
						AND b.index_id = ix.index_id
		) a
	ORDER BY a.size DESC, a.avg_fragment DESC
END

