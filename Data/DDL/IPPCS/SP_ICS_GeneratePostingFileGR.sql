-- =============================================
-- Author:           niit.yudha
-- Create date: 3-Mei-2013
-- Description:      Generate Goods Receipt ICS
-- Update	: FID.Ridwan : 10-Sep-2020
--			  => Take out GR logic move to SP_ICS_PostingGRtoICS
-- =============================================
ALTER PROCEDURE [dbo].[SP_ICS_GeneratePostingFileGR]
---DECLARE
       -- Add the parameters for the stored procedure here 
    @UserID VARCHAR(20) ,
    @ManifestNo VARCHAR(MAX) ,
    @SupplierCode VARCHAR(MAX) ,
    @RvcPlantCode VARCHAR(MAX) ,
    @DockCode VARCHAR(MAX),
    @L_N_PROCESS_ID BIGINT
AS 

BEGIN
              -- SET NOCOUNT ON added to prevent extra result sets from
              -- interfering with SELECT statements.
    SET NOCOUNT ON;
        
    DECLARE @PROC_ID VARCHAR(12) ,
        @PROCESS_ID BIGINT ,
        @LAST_PROCESS_ID VARCHAR(12) ,
        @TODAY VARCHAR(8) ,
        @NOW DATETIME ,
        @CURR_NO INT ,
        @CURR_PROCESS_ID VARCHAR(12) ,
        @SEQ_NO_LOG BIGINT ,
        @LOCATION VARCHAR(100) ,
        @PROC_STAT TINYINT ,
        @L_N_COUNT_MANIFEST INT ,
        @L_N_COUNT_ERR INT ,
        @ERR_FLAG BIT  
        
    SET @LOCATION = 'SP ICS GeneratePosting GR';
    SET @SEQ_NO_LOG = 1;
    SET @ERR_FLAG = 0; 
    SET @PROC_STAT = 0;


                                                                                                                     
    SET NOCOUNT ON   
                                         
    DECLARE @TP_SUPPLIER_CODE VARCHAR(50) ,
        @FLTR_DATETIME_VALUE AS VARCHAR(50) ,
        @FLTR_LOGICAL_VALUE AS VARCHAR(50) ,
        @FLTR_FLAG_VALUE AS VARCHAR(50) ,
        @IS_VALID_FILTER CHAR(1) ,
        @TYPE_CODE INT ,
        @RECEIVE_NO VARCHAR(10) ,
        @SOURCE_TYPE_GR CHAR(1) ,
        @SOURCE_TYPE_TP CHAR(1) ,
        @SYS_SOURCE CHAR(20) ,
        @PROD_PURPOSE_CD CHAR(5) ,
        @URI_UOM CHAR(5) ,
        @SEND_FLAG VARCHAR(1)            
              
              
              
       DECLARE @dockServicePart VARCHAR(20) = '',
                     @prodPurposeServicePart VARCHAR(20) = ''

                     SELECT @dockServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DOCK_SERVICE_PART'
                     SELECT @prodPurposeServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'PROD_PURPOSE_GR_SERVICE_PART'
                
                DECLARE @dockServicePartTable AS TABLE (DOCK VARCHAR(2))
                
                INSERT INTO @dockServicePartTable (DOCK)
    EXEC dbo.SplitString @dockServicePart, ','        
    

                
    SELECT  @TYPE_CODE = 42001                                                                                                    
                                                                                                              
              --GET VALUE FROM SYS TABLE                                                                                                        
    SELECT  @TP_SUPPLIER_CODE = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                            'TP_SPLR_CODE')
            + '%'                                                                                                          
    SELECT  @RECEIVE_NO = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                      'RECEIVE_NO_GR')                                                                                                            
    SELECT  @SOURCE_TYPE_GR = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                          'SOURCE_TYPE_GR')                                                                                                       

    SELECT  @SOURCE_TYPE_TP = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                          'SOURCE_TYPE_TP')

    SELECT  @SYS_SOURCE = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                      'SYSTEM_SOURCE_GR')                                                                                                         
    SELECT  @PROD_PURPOSE_CD = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                           'PROD_PURPOSE_CD_GR')                                                                                                         
    SELECT  @URI_UOM = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                   'ORI_UNIT_MEASU_CD_GR')                                                                                                        
    SELECT  @SEND_FLAG = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'SEND_FLAG')     
                                                                                                                           
              --GET FILTER PARAMETERS                                                                                                           
    SELECT  @FLTR_DATETIME_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'POST_TIME_FLTR')))                                                                                                        
    SELECT  @FLTR_LOGICAL_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'POST_LGCL_FLTR')))                                                                                                        
    SELECT  @FLTR_FLAG_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'POST_FLG_FLTR')))                                                                                                         
              -- END OF GET FILTER PARAMETERS                                                                                                          
                                                                                                                     
              -- CHECK FILTER VALIDITY                                                                                                          
    IF ( @FLTR_FLAG_VALUE = 'Y' ) 
        BEGIN                                                                                                        
            IF ( @FLTR_LOGICAL_VALUE IN ( '=', '>', '<', '>=', '<=' ) )
                AND ( LEN(@FLTR_DATETIME_VALUE) = 14 ) -- AND ( ISDATE(CHAR14TODATE(@FLTR_DATETIME_VALUE)) = 1 )
                BEGIN
                    SET @IS_VALID_FILTER = 'Y'                                                                                                  
                END 
            ELSE 
                BEGIN
                    SET @IS_VALID_FILTER = 'N'                                                                                                  
                END
        END                                                                                                          
    ELSE 
        BEGIN                                                                                                        
            IF ( @FLTR_FLAG_VALUE = 'N' ) 
                SET @IS_VALID_FILTER = 'Y'
            ELSE 
                SET @IS_VALID_FILTER = 'N'                                                                                                      
        END                                                                                                          
                     -- END OF CHECK FILTER VALIDITY                                                                                                          
              
              
              
    
    CREATE TABLE #TempManifestNo
        (
          ManifestNo VARCHAR(100)
        )
    CREATE TABLE #tempSupplierCode
        (
          SupplierCode VARCHAR(100)
        )    
            
    CREATE TABLE #tempRvcPlantCode
        (
          RvcPlantCode VARCHAR(100)
        )
    CREATE TABLE #tempDockCode ( DockCode VARCHAR(100) )   
        
    IF ( SELECT CHARINDEX(';', @ManifestNo)
       ) > 0 
        BEGIN 
            INSERT  INTO #TempManifestNo
                    ( ManifestNo )
                    EXEC dbo.SplitString @ManifestNo, ';'
      
        END          
    ELSE 
        BEGIN
            INSERT  INTO #TempManifestNo
                    ( ManifestNo )
                    SELECT  @ManifestNo
        END
            
            
    IF ( SELECT CHARINDEX(';', @SupplierCode)
       ) > 0 
        BEGIN 
            INSERT  INTO #tempSupplierCode
                    ( SupplierCode 
                        
                    )
                    EXEC dbo.SplitString @SupplierCode, ';'
      
        END                                                   
    ELSE 
        BEGIN 
            INSERT  INTO #tempSupplierCode
                    ( SupplierCode )
                    SELECT  @SupplierCode
      
        END     
            
               
    IF ( SELECT CHARINDEX(';', @RvcPlantCode)
       ) > 0 
        BEGIN 
            INSERT  INTO #tempRvcPlantCode
                    ( RvcPlantCode 
                        
                    )
                    EXEC dbo.SplitString @RvcPlantCode, ';' 
        END                                                   
    ELSE 
        BEGIN 
            INSERT  INTO #tempRvcPlantCode
                    ( RvcPlantCode )
                    SELECT  @RvcPlantCode
      
        END   
            
              
    IF ( SELECT CHARINDEX(';', @DockCode)
       ) > 0 
        BEGIN
  
            INSERT  INTO #tempDockCode
                    ( DockCode )
                    EXEC dbo.SplitString @DockCode, ';' 
        END
    ELSE 
        BEGIN 
            INSERT  INTO #tempDockCode
                    ( DockCode )
                    SELECT  @DockCode 
        END                
            
                                                       

       -- Added by imans, 07-08-2013
       ---> EXECUTE SP UPDATE SHIPPING DOCK STAMPING & CASTING
       EXEC  [dbo].[SP_UPDATE_SHIPPING_DOCK] @MANIFEST = @ManifestNo
       ---
       
                                               
    IF ( @IS_VALID_FILTER = 'Y' ) 
        BEGIN                                                                                                        
                                                                                                                     
            CREATE TABLE #INVOICENO_GR ( INVOICENO VARCHAR(25) )                                                                                                     
            CREATE TABLE #INVOICENO_TP
                (
                  INVOICENO VARCHAR(25) ,
                  PART_NO VARCHAR(12)
                )                                                                                                    
            CREATE TABLE #ICS_POSTING
                (
                  REC_ID INT IDENTITY(1, 1) ,
                  MANIFEST_NO VARCHAR(25) ,
                  SUPPLIER_CODE VARCHAR(4) ,
                  SUPPLIER_PLANT CHAR(1) ,
                  SHIPPING_DOCK VARCHAR(3) ,
                  COMPANY_CODE VARCHAR(4) ,
                  RCV_PLANT_CODE CHAR(1) ,
                  DOCK_CODE VARCHAR(2) ,
                  ORDER_NO VARCHAR(12) ,
                  CAR_FAMILY_CODE VARCHAR(4) ,
                  RE_EXPORT_CODE VARCHAR(1) ,
                  PART_NO VARCHAR(12) ,
                  KNBN_PRN_ADDRESS VARCHAR(10) ,
                  ORDER_TYPE CHAR(1) ,
                  MOV_TYPE CHAR(3) ,
                  DROP_STATUS_DATE DATETIME ,
                  RECEIVED_QTY INT NULL ,
                  DN_COMPLETE_FLAG CHAR(1) ,
                  DATA_TYPE CHAR(2),
                  ORDER_RELEASE_DT DATETIME
                )                                                                                                    
                                                                                                                     
                           /* ===========================================               
                                  CREATE DATA LIST FROM ORDER                    
                              ===========================================*/                                                                                                       
            BEGIN                         
			
				/* Remark FID.Ridwan : 2020-09-10                                                                    
                                         --GR                                                                                     
                INSERT  INTO #INVOICENO_GR
                        SELECT  A.MANIFEST_NO
                        FROM    ( SELECT DISTINCT
                                            ( c.MANIFEST_NO ) ,
                                            MAX(b.ORDER_QTY) AS ORDER_QTY ,
                                            c.RCV_PLANT_CD ,
                                            c.DOCK_CD ,
                                            c.ICS_FLAG ,
                                            c.DROP_STATUS_FLAG ,
                                            c.DROP_STATUS_DT ,
                                            c.fst_x_plant_cd AS FST_X_PLANT_CODE ,
                                            c.fst_x_shipping_dock_cd AS FST_X_SHIPPING_DOCK_CODE ,
                                            c.SUPPLIER_CD AS SUPPLIER_CODE
                                  FROM      TB_R_DAILY_ORDER_MANIFEST c
                                            INNER JOIN ( SELECT  DISTINCT
                                                              ( MANIFEST_NO ) ,
                                                              MAX(ORDER_QTY) AS ORDER_QTY
                                                         FROM TB_R_DAILY_ORDER_PART
                                                         WHERE
                                                              MANIFEST_NO IN (
                                                              SELECT
                                                              *
                                                              FROM
                                                              #TempManifestNo )
                                                         GROUP BY ( MANIFEST_NO )
                                                       ) AS b ON c.MANIFEST_NO = b.MANIFEST_NO
                                          --
                                  WHERE     c.PROBLEM_FLAG = '0'
                                            AND c.CANCEL_FLAG = '0'
                                            AND c.MANIFEST_RECEIVE_FLAG in ('2','3','5')
                                            AND c.MANIFEST_NO IN ( SELECT
                                                              *
                                                              FROM
                                                              #TempManifestNo )
                                            AND c.SUPPLIER_CD NOT LIKE @TP_SUPPLIER_CODE
                                          --            
                                  GROUP BY  c.MANIFEST_NO ,
                                            c.RCV_PLANT_CD ,
                                            c.DOCK_CD ,
                                            c.ICS_FLAG ,
                                            c.DROP_STATUS_FLAG ,
                                            c.DROP_STATUS_DT ,
                                            c.fst_x_plant_cd ,
                                            c.fst_x_shipping_dock_cd ,
                                            c.SUPPLIER_CD
                                           
                                ) AS A
                        WHERE   A.ICS_FLAG LIKE '0'
                                AND A.ORDER_QTY > 0
                        GROUP BY A.MANIFEST_NO
                        HAVING  MIN(A.DROP_STATUS_FLAG) = 1   
						*/
                                                                    
                                                                                                                                                              
                INSERT  INTO #INVOICENO_TP
                        ( INVOICENO ,
                          PART_NO
                                
                        )
                        SELECT  A.MANIFEST_NO ,
                                A.PART_NO
                        FROM    ( SELECT DISTINCT
                                            ( c.MANIFEST_NO ) ,
                                            MAX(b.ORDER_QTY) AS ORDER_QTY ,
                                            c.RCV_PLANT_CD ,
                                            c.DOCK_CD ,
                                            c.ICS_FLAG ,
                                            c.DROP_STATUS_FLAG ,
                                            c.DROP_STATUS_DT ,
                                            c.fst_x_plant_cd AS FST_X_PLANT_CODE ,
                                            c.fst_x_shipping_dock_cd AS FST_X_SHIPPING_DOCK_CODE ,
                                            c.SUPPLIER_CD AS SUPPLIER_CODE ,
                                            b.PART_NO
                                  FROM      TB_R_DAILY_ORDER_MANIFEST c
                                            INNER JOIN ( SELECT  DISTINCT
                                                              ( MANIFEST_NO ) ,
                                                              MAX(ORDER_QTY) AS ORDER_QTY ,
                                                              PART_NO
                                                         FROM TB_R_DAILY_ORDER_PART
                                                         WHERE
                                                              MANIFEST_NO IN (
                                                              SELECT
                                                              *
                                                              FROM
                                                              #TempManifestNo )
                                                         GROUP BY ( MANIFEST_NO ) ,
                                                              PART_NO
                                                       ) AS b ON c.MANIFEST_NO = b.MANIFEST_NO
                                          --
                                  WHERE     c.PROBLEM_FLAG = '0'
                                            AND c.CANCEL_FLAG = '0'
                                            AND c.MANIFEST_RECEIVE_FLAG in ('2','3','5')
                                            AND c.MANIFEST_NO IN ( SELECT
                                                              *
                                                              FROM
                                                              #TempManifestNo )
                                            AND c.SUPPLIER_CD LIKE '%'+@TP_SUPPLIER_CODE+'%'
                                          --
                                  GROUP BY  c.MANIFEST_NO ,
                                            c.RCV_PLANT_CD ,
                                            c.DOCK_CD ,
                                            c.ICS_FLAG ,
                                            c.DROP_STATUS_FLAG ,
                                            c.DROP_STATUS_DT ,
                                            c.fst_x_plant_cd ,
                                            c.fst_x_shipping_dock_cd ,
                                            c.SUPPLIER_CD ,
                                            b.PART_NO
                                ) AS A
                        WHERE   A.ICS_FLAG LIKE '0'
                                AND A.ORDER_QTY > 0
                        GROUP BY A.MANIFEST_NO ,
                                A.PART_NO
                        HAVING  MIN(A.DROP_STATUS_FLAG) = 1                                                                                    
            END                                                                                               
            
             
            declare @tst int
            
            select @tst = count(1)
            from  #INVOICENO_TP
                                  
            print('1 '+@tst)    
			
			/* Remark FID.Ridwan : 2020-09-10                                           
                           --GR                                                                                            
            INSERT  INTO #ICS_POSTING
                    ( MANIFEST_NO ,
                      SUPPLIER_CODE ,
                      SUPPLIER_PLANT ,
                      SHIPPING_DOCK ,
                      COMPANY_CODE ,
                      RCV_PLANT_CODE ,
                      DOCK_CODE ,
                      ORDER_NO ,
                      CAR_FAMILY_CODE ,
                      RE_EXPORT_CODE ,
                      PART_NO ,
                      KNBN_PRN_ADDRESS ,
                      ORDER_TYPE ,
                      MOV_TYPE ,
                      DROP_STATUS_DATE ,
                      RECEIVED_QTY ,
                      DN_COMPLETE_FLAG ,
                      DATA_TYPE,
                      ORDER_RELEASE_DT                                                                                     
                                 
                    )
                    SELECT  A.MANIFEST_NO ,
                            A.SUPPLIER_CODE ,
                            A.SUPPLIER_PLANT ,
                            A.SHIPPING_DOCK ,
                            A.COMPANY_CD ,
                            A.RCV_PLANT_CODE ,
                            A.DOCK_CODE ,
                            A.ORDER_NO ,
                            A.CAR_FAMILY_CD ,
                            A.RE_EXPORT_CD ,
                            A.PART_NO ,
                            '' AS KNBN_PRN_ADDRESS, -- was : A.KNBN_PRN_ADDRESS , not send to ics
                            A.ORDER_TYPE ,
                            A.MOVE_TYPE ,
                            A.DROP_STATUS_DT ,
                            SUM(A.ORDER_QTY) ORDER_QTY ,
                            A.DN_COMPLETE_FLAG ,
                            A.DATA_TYPE,
                            A.ORDER_RELEASE_DT
                    FROM    ( SELECT DISTINCT
                                        ( c.MANIFEST_NO ) ,
                                        c.SUPPLIER_CD AS SUPPLIER_CODE ,
                                        c.SUPPLIER_PLANT ,
                                        c.SHIPPING_DOCK ,
                                        c.RCV_PLANT_CD AS RCV_PLANT_CODE ,
                                        c.DOCK_CD AS DOCK_CODE ,
                                        c.ICS_FLAG ,
                                        C.COMPANY_CD ,
                                        c.CAR_FAMILY_CD ,
                                        c.RE_EXPORT_CD ,
                                        SUM(b.ORDER_QTY) ORDER_QTY ,
                                        c.ORDER_NO ,
                                        c.DROP_STATUS_FLAG ,
                                        c.DROP_STATUS_DT ,
                                        b.PART_NO ,
                                        '' AS KNBN_PRN_ADDRESS, --was : b.KANBAN_PRINT_ADDRESS AS KNBN_PRN_ADDRESS ,
                                        c.ORDER_TYPE ,
                                        /*CASE LEFT(c.SUPPLIER_CD, 3)
                                          WHEN '807' THEN '301'
                                          ELSE '101'
                                        END AS MOVE_TYPE ,*/
                                                                     CASE LEFT(c.SUPPLIER_CD, 3)
                                                                           WHEN '807' THEN 
                                                                                  CASE WHEN (SELECT 1 WHERE c.DOCK_CD IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
                                                                                         THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'MOVEMENT_TYPE_SERVICE_PART')
                                                                                  ELSE
                                                                                         '301'
                                                                                  END
                                                                           ELSE '101'
                                                                     END AS MOVE_TYPE, 

                                        dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'DN_COMPLETE_FLG_GR') AS DN_COMPLETE_FLAG ,
                                        'GR' AS DATA_TYPE,
                                        /*
                                        Modified by imans, 14-11-2013
                                        -- Requested by pa Agung, Vandy : 
                                           email Different Posting Date from Scan Date --- CONFIRMED
                                        -- was : c.ORDER_RELEASE_DT
                                        */
                                        c.ARRIVAL_PLAN_DT AS ORDER_RELEASE_DT
                                                    
                              FROM      TB_R_DAILY_ORDER_MANIFEST c
                                        INNER JOIN ( SELECT MANIFEST_NO ,
                                                            SUM(ORDER_QTY) ORDER_QTY ,
                                                            RCV_PLANT_CD ,
                                                            PART_NO ,
                                                            KANBAN_PRINT_ADDRESS
                                                     FROM   TB_R_DAILY_ORDER_PART
                                                     WHERE  ORDER_QTY > 0
                                                     AND    MANIFEST_NO IN ( SELECT   INVOICENO
                                                                             FROM     #INVOICENO_GR )
                                                     GROUP BY ( MANIFEST_NO ) ,
                                                            PART_NO ,
                                                            RCV_PLANT_CD ,
                                                            PART_NO ,
                                                            KANBAN_PRINT_ADDRESS
                                                   ) AS b ON c.MANIFEST_NO = b.MANIFEST_NO
                              WHERE     c.PROBLEM_FLAG = '0'
                                        AND c.CANCEL_FLAG = '0'
                                        AND c.MANIFEST_RECEIVE_FLAG in ('2','3','5')
                                        AND c.MANIFEST_NO IN ( SELECT   INVOICENO
                                                               FROM     #INVOICENO_GR )
                             GROUP BY  c.MANIFEST_NO ,
                                        SUPPLIER_CD ,
                                        SUPPLIER_PLANT ,
                                        c.SHIPPING_DOCK ,
                                        C.COMPANY_CD ,
                                        c.CAR_FAMILY_CD ,
                                        c.RE_EXPORT_CD ,
                                        c.RCV_PLANT_CD ,
                                        DOCK_CD ,
                                        ORDER_NO ,
                                        c.DROP_STATUS_FLAG ,
                                        c.DROP_STATUS_DT ,
                                        PART_NO ,
                                        -- was : b.KANBAN_PRINT_ADDRESS ,
                                        ORDER_TYPE ,
                                        c.ICS_FLAG ,
                                        c.ARRIVAL_PLAN_DT
                            ) AS A
                    WHERE   A.MANIFEST_NO IN ( SELECT   INVOICENO
                                               FROM     #INVOICENO_GR )
                    GROUP BY A.MANIFEST_NO ,
                            A.SUPPLIER_CODE ,
                            A.SUPPLIER_PLANT ,
                            A.SHIPPING_DOCK ,
                            A.COMPANY_CD ,
                            A.RCV_PLANT_CODE ,
                            A.DOCK_CODE ,
                            A.ORDER_NO ,
                            A.CAR_FAMILY_CD ,
                            A.RE_EXPORT_CD ,
                            A.PART_NO ,
                            -- was : A.KNBN_PRN_ADDRESS ,
                            A.ORDER_TYPE ,
                            A.MOVE_TYPE ,
                            A.DROP_STATUS_DT ,
                            A.DN_COMPLETE_FLAG ,
                            A.DATA_TYPE,
                            A.ORDER_RELEASE_DT
                    HAVING  MIN(A.DROP_STATUS_FLAG) = 1              
                                                                                  
                                                                                                                     
            UPDATE  #ICS_POSTING
            SET     DROP_STATUS_DATE = X.DROP_STATUS_DATE
            FROM    #ICS_POSTING ,
                    ( SELECT    MANIFEST_NO ,
                                MAX(DROP_STATUS_DATE) AS DROP_STATUS_DATE
                      FROM      #ICS_POSTING
                      GROUP BY  MANIFEST_NO
                    ) X
            WHERE   #ICS_POSTING.MANIFEST_NO = X.MANIFEST_NO      
			*/                                                                              
                     
                           --TP                                                                                            
            INSERT  INTO #ICS_POSTING
                    ( MANIFEST_NO ,
                      SUPPLIER_CODE ,
                      SUPPLIER_PLANT ,
                      SHIPPING_DOCK ,
                      COMPANY_CODE ,
                      RCV_PLANT_CODE ,
                      DOCK_CODE ,
                      ORDER_NO ,
                      CAR_FAMILY_CODE ,
                      RE_EXPORT_CODE ,
                      PART_NO ,
                      KNBN_PRN_ADDRESS ,
                      ORDER_TYPE ,
                      MOV_TYPE ,
                      DROP_STATUS_DATE ,
                      RECEIVED_QTY ,
                      DN_COMPLETE_FLAG ,
                      DATA_TYPE ,
                      ORDER_RELEASE_DT                                                                               
                                 
                    )
                    SELECT  A.MANIFEST_NO ,
                            A.SUPPLIER_CODE ,
                            A.SUPPLIER_PLANT ,
                            A.SHIPPING_DOCK ,
                            A.COMPANY_CD ,
                            A.RCV_PLANT_CODE ,
                            A.DOCK_CODE ,
                            A.ORDER_NO ,
                            A.CAR_FAMILY_CD ,
                            A.RE_EXPORT_CD ,
                            A.PART_NO ,
                            '' AS KNBN_PRN_ADDRESS, -- was : A.KNBN_PRN_ADDRESS , not parse to ics
                            A.ORDER_TYPE ,
                            A.MOVE_TYPE ,
                            A.DROP_STATUS_DATE ,
                            A.RECEIVED_QTY ,
                            A.DN_COMPLETE_FLAG ,
                            A.DATA_TYPE,
                            A.ORDER_RELEASE_DT
                    FROM    ( SELECT    A.MANIFEST_NO ,
                                        A.SUPPLIER_CODE ,
                                        A.SUPPLIER_PLANT ,
                                        A.SHIPPING_DOCK ,
                                        A.COMPANY_CD ,
                                        A.RCV_PLANT_CODE ,
                                        A.DOCK_CODE ,
                                        A.ORDER_NO ,
                                        A.CAR_FAMILY_CD ,
                                        A.RE_EXPORT_CD ,
                                        A.PART_NO ,
                                        '' AS KNBN_PRN_ADDRESS, -- was : A.KNBN_PRN_ADDRESS , not send to ics
                                        A.ORDER_TYPE ,
                                        /*CASE LEFT(A.SUPPLIER_CODE, 3)
                                          WHEN '807' THEN '301'
                                          ELSE '101'
                                        END AS MOVE_TYPE ,*/

                                                                     CASE LEFT(A.SUPPLIER_CODE, 3)
                                                                           WHEN '807' THEN 
                                                                                   CASE WHEN (SELECT 1 WHERE A.DOCK_CODE IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
                                                                                         THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'MOVEMENT_TYPE_SERVICE_PART')
                                                                                  ELSE
                                                                                         '301'
                                                                                  END
                                                                           ELSE '101'
                                                                     END AS MOVE_TYPE,


                                        MAX(A.DROP_STATUS_DT) AS DROP_STATUS_DATE ,
                                        SUM(A.ORDER_QTY) AS RECEIVED_QTY ,
                                        DBO.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'DN_UNCOMPLETE_FLG_GR') AS DN_COMPLETE_FLAG ,
                                        'TP' AS DATA_TYPE,
                                        A.ORDER_RELEASE_DT
                              FROM      ( SELECT DISTINCT
                                                    ( c.MANIFEST_NO ) ,
                                                    c.SUPPLIER_CD AS SUPPLIER_CODE ,
                                                    c.SUPPLIER_PLANT ,
                                                    c.SHIPPING_DOCK ,
                                                    c.RCV_PLANT_CD AS RCV_PLANT_CODE ,
                                                    c.DOCK_CD AS DOCK_CODE ,
                                                    c.ICS_FLAG ,
                                                    SUM(b.ORDER_QTY) ORDER_QTY ,
                                                    c.ORDER_NO ,
                                                    b.PART_NO ,
                                                    c.DROP_STATUS_FLAG ,
                                                    c.DROP_STATUS_DT ,
                                                    b.KANBAN_PRINT_ADDRESS AS KNBN_PRN_ADDRESS ,
                                                    c.ORDER_TYPE ,
                                                    C.COMPANY_CD ,
                                                    C.CAR_FAMILY_CD ,
                                                    C.RE_EXPORT_CD ,
                                                    /*CASE LEFT(c.SUPPLIER_CD, 3)
                                                      WHEN '807' THEN '301'
                                                      ELSE '101'
                                                    END AS MOVE_TYPE ,*/
                                                                                         CASE LEFT(c.SUPPLIER_CD, 3)
                                                                                         WHEN '807' THEN 
                                                                                                CASE WHEN (SELECT 1 WHERE c.DOCK_CD IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
                                                                                                       THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'MOVEMENT_TYPE_SERVICE_PART')
                                                                                                ELSE
                                                                                                       '301'
                                                                                                END
                                                                                         ELSE '101'
                                                                                         END AS MOVE_TYPE,
                                                    dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'DN_UNCOMPLETE_FLG_GR') AS DN_COMPLETE_FLAG ,
                                                    'TP' AS DATA_TYPE,
                                                    /*
                                                    Modified by imans, 14-11-2013
                                                    -- Requested by pa Agung, Vandy : 
                                                       email Different Posting Date from Scan Date --- CONFIRMED
                                                    -- was : c.ORDER_RELEASE_DT
                                                    */
                                                    c.ARRIVAL_PLAN_DT AS ORDER_RELEASE_DT
                                                    
                                          FROM      TB_R_DAILY_ORDER_MANIFEST c
                                                    INNER JOIN ( SELECT
                                                             MANIFEST_NO ,
                                                              SUM(ORDER_QTY) ORDER_QTY ,
                                                              RCV_PLANT_CD ,
                                                              PART_NO ,
                                                              KANBAN_PRINT_ADDRESS
                                                                --ADD BY GOLDY
                                                              , POSTING_STS
                                                                 --END ADD BY GOLDY
                                                              FROM
                                                              TB_R_DAILY_ORDER_PART
                                                              WHERE
                                                            
                                                              ISNULL(POSTING_STS,'3') <> 0 AND
                                                           
                                                              ORDER_QTY > 0
                                                              AND MANIFEST_NO IN (
                                                                                                                                  SELECT  LTRIM(RTRIM(INVOICENO))
                                                                                                                                  FROM    #INVOICENO_TP)
                                                              GROUP BY ( MANIFEST_NO ) ,
                                                              PART_NO ,
                                                              RCV_PLANT_CD ,
                                                              PART_NO ,
                                                              KANBAN_PRINT_ADDRESS,
                                                              POSTING_STS
                                                              ) AS b ON c.MANIFEST_NO = b.MANIFEST_NO
                                          WHERE     c.PROBLEM_FLAG = '0'
                                                    AND c.CANCEL_FLAG = '0'
                                                    AND c.MANIFEST_RECEIVE_FLAG in ('2','3','5')
                                                    --ADD BY GOLDY
                                                     --ADD BY GOLDY
                                                    --OR (c.manifest_receive_flag='5'  and  ( b.POSTING_STS = 1 or b.POSTING_STS = null) )
                                                    --END ADD BY GOLDY
                                                    --END ADD BY GOLDY
                                                    AND c.MANIFEST_NO IN (  SELECT  LTRIM(RTRIM(INVOICENO))
                                                                                                                                  FROM    #INVOICENO_TP)
                                          GROUP BY  c.MANIFEST_NO ,
                                                    SUPPLIER_CD ,
                                                    SUPPLIER_PLANT ,
                                                    c.SHIPPING_DOCK ,
                                                    c.RCV_PLANT_CD ,
                                                    DOCK_CD ,
                                                    ORDER_NO ,
                                                    PART_NO ,
                                                    c.DROP_STATUS_FLAG ,
                                                    c.DROP_STATUS_DT ,
                                                    b.KANBAN_PRINT_ADDRESS ,
                                                    ORDER_TYPE ,
                                                    C.COMPANY_CD ,
                                                    C.CAR_FAMILY_CD ,
                                                    C.RE_EXPORT_CD ,
                                                    c.ICS_FLAG,
                                                    c.ARRIVAL_PLAN_DT
                                        ) AS A
                              WHERE     A.MANIFEST_NO IN (
                                        SELECT  LTRIM(RTRIM(INVOICENO))
                                        FROM    #INVOICENO_TP --WHERE PART_NO NOT LIKE '-'                                                                           
                                  )
                              GROUP BY  A.MANIFEST_NO ,
                                        A.SUPPLIER_CODE ,
                                        A.SUPPLIER_PLANT ,
                                        A.SHIPPING_DOCK ,
                                        A.COMPANY_CD ,
                                        A.RCV_PLANT_CODE ,
                                        A.DOCK_CODE ,
                                        A.ORDER_NO ,
                                        A.CAR_FAMILY_CD ,
                                        A.RE_EXPORT_CD ,
                                        A.PART_NO ,
                                        -- was : A.KNBN_PRN_ADDRESS ,
                                        A.ORDER_TYPE,
                                        A.ORDER_RELEASE_DT
                            ) A 
                     /*  --- Remark Cancel : by Imans         
                        --CANCEL ORDER                                                                                            
            INSERT  INTO #ICS_POSTING
                    ( MANIFEST_NO ,
                      SUPPLIER_CODE ,
                      SUPPLIER_PLANT ,
                      SHIPPING_DOCK ,
                      COMPANY_CODE ,
                      RCV_PLANT_CODE ,
                      DOCK_CODE ,
                      ORDER_NO ,
                      CAR_FAMILY_CODE ,
                      RE_EXPORT_CODE ,
                      PART_NO ,
                      KNBN_PRN_ADDRESS ,
                      ORDER_TYPE ,
                      MOV_TYPE ,
                      DROP_STATUS_DATE ,
                      RECEIVED_QTY ,
                      DN_COMPLETE_FLAG ,
                      DATA_TYPE,
                      ORDER_RELEASE_DT                                                                                     
                                 
                    )
                    SELECT  x.MANIFEST_NO ,
                            SUPPLIER_CD ,
                            x.SUPPLIER_PLANT ,
                            SHIPPING_DOCK ,
                            COMPANY_CD ,
                            x.RCV_PLANT_CD ,
                            x.DOCK_CD ,
                            ORDER_NO ,
                            CAR_FAMILY_CD ,
                            RE_EXPORT_CD ,
                            PART_NO ,
                            '' AS KNBN_PRN_ADDRESS, -- was : y.KANBAN_PRINT_ADDRESS , not send to ics
                            ORDER_TYPE ,
                            CASE LEFT(x.SUPPLIER_CD, 3)
                              WHEN '807' THEN '302'
                              ELSE '102'
                            END AS MOVE_TYPE ,
                            getdate() AS DROP_STATUS_DATE ,
                            SUM(y.ORDER_QTY) AS RECEIVED_QTY ,
                            DBO.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                        'DN_COMPLETE_FLG_GR') AS DN_COMPLETE_FLAG ,
                            'CL' AS DATA_TYPE,
                            x.ORDER_RELEASE_DT
                    FROM    dbo.TB_R_DAILY_ORDER_MANIFEST x
                            INNER JOIN dbo.TB_R_DAILY_ORDER_PART y ON x.MANIFEST_NO = y.MANIFEST_NO
                                                             -- AND x.CANCEL_FLAG = 1
                    WHERE   x.PROBLEM_FLAG = '0'
                            --AND x.CANCEL_FLAG = '1'
                            AND 
                            (
                                                       x.MANIFEST_RECEIVE_FLAG IN ('4')
                                                       OR (x.MANIFEST_RECEIVE_FLAG IN ('5') AND ICS_FLAG = '1')
                                                )
                            AND x.MANIFEST_NO IN ( SELECT   *
                                                   FROM     #TempManifestNo )
                    GROUP BY x.MANIFEST_NO ,
                            SUPPLIER_CD ,
                            x.SUPPLIER_PLANT ,
                            SHIPPING_DOCK ,
                            COMPANY_CD ,
                            x.RCV_PLANT_CD ,
                            x.DOCK_CD ,
                            ORDER_NO ,
                            CAR_FAMILY_CD ,
                            RE_EXPORT_CD ,
                            PART_NO ,
                            -- was : y.KANBAN_PRINT_ADDRESS ,
                            ORDER_TYPE,
                            x.ORDER_RELEASE_DT
            */                                  
                                                                                                                     
                                                                                                                     
                           --UPDATE DN_COMPLETE_FLAG                                                                                            
            UPDATE  #ICS_POSTING
            SET     DN_COMPLETE_FLAG = DBO.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE,
                                                              'DN_UNCOMPLETE_FLG_GR')
            WHERE   DATA_TYPE LIKE 'TP'
                    AND MANIFEST_NO IN ( SELECT INVOICENO
                                         FROM   #INVOICENO_TP
                                         WHERE  PART_NO NOT LIKE '-' )                                                                                        
                           
              
              
            --EXEC dbo.spInsertLogDetail @PROCESS_ID, 'MPCS00003INF', 'INF',
            --    'Process Posting GR TP data to ICS is finished', @LOCATION,
            --    'SYSTEM';
                     
               -- MODIFY END_DATE ON LOG HEADER
            --UPDATE  dbo.TB_R_LOG_H
            --SET     END_DATE = SYSDATETIME() ,
            --        PROCESS_STATUS = @PROC_STAT
            --WHERE   PROCESS_ID = @PROCESS_ID;
              
                           
            CREATE TABLE #ICS_FILE
                (
                  REC_ID BIGINT IDENTITY(1, 1) , --1
                  SYS_SOURCE CHAR(20) ,  --2
                  [USER_ID] CHAR(20) ,  --3
                  MOV_TYPE CHAR(4) ,  --4
                  PROCESS_DATE DATETIME ,  --5
                  POSTING_DATE DATETIME ,  --6
                  REF_NO CHAR(16) ,  --7
                  KANBAN_ORDER_NO CHAR(16) ,  --8
                  PROD_PURPOSE_CD CHAR(5) ,  --9
                  SOURCE_TYPE CHAR(1) ,  --10
                  ORI_MAT_NO CHAR(30) ,  --11
                  POSTING_QUANTITY CHAR(10) ,  --12
                  ORI_UNIT_OF_MEASURE_CD CHAR(5) ,  --13
                  ORI_SUPP_CD CHAR(6) ,  --14
                  RECEIVING_PLANT_CD CHAR(4) ,  --15
                  RECEIVING_AREA_CD CHAR(6) ,  --16
                  INT_KANBAN_FLAG CHAR(1) ,  --17
                  DN_COMPLETE_FLAG CHAR(1) ,  --18
                  MAT_COMPLETE_FLAG CHAR(1) ,  --19
                  RECEIVE_NO CHAR(10) ,  --20
                  SEND_FLAG CHAR(1) ,  --21
                  PROCESS_ID BIGINT,  --22
                  ARRIVAL_PLAN_DT DATETIME
                )                               
                

                     

            insert into #ICS_FILE           
            SELECT  @SYS_SOURCE AS systemSource ,
                    @UserID AS userID ,
                    A.MOV_TYPE AS movementType ,
                    GETDATE() AS processDate,
                    CONVERT(DATE,CONVERT(VARCHAR(20),A.DROP_STATUS_DATE,112))  AS postingDate ,
                    A.MANIFEST_NO AS refNo ,
                    A.ORDER_NO AS kanbanOrderNo ,



                    --@PROD_PURPOSE_CD AS prodPurpose ,
                                  /*CASE LEFT(A.SUPPLIER_CODE, 3)
                      WHEN '807' THEN @PROD_PURPOSE_CD
                      ELSE
                                         CASE (A.DOCK_CODE)  
                                                WHEN @dockServicePart THEN @prodPurposeServicePart
                                                ELSE
                                                       @PROD_PURPOSE_CD
                                                END
                                    END AS prodPurpose,*/
                    
                                  /*CASE LEFT(A.SUPPLIER_CODE, 3)
                                         WHEN '807' THEN @PROD_PURPOSE_CD
                                  ELSE
                                         CASE WHEN (SELECT 1 WHERE A.DOCK_CODE IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
                                  THEN @prodPurposeServicePart
                                  ELSE
                                         @PROD_PURPOSE_CD
                                  END
                                  END AS prodPurpose, */
                                  
                                  CASE WHEN (SELECT 1 WHERE A.DOCK_CODE IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
                                  THEN @prodPurposeServicePart
                                  ELSE
                                         @PROD_PURPOSE_CD
                                  END
                                    AS prodPurpose,
                                           
                                  
                                  
                                  CASE LEFT(A.SUPPLIER_CODE, 3)
                      WHEN '807' THEN @SOURCE_TYPE_TP
                      ELSE  @SOURCE_TYPE_GR
                    END AS sourceType , 
                    A.PART_NO AS oriMatNo ,
                    RECEIVED_QTY AS postingQty ,
                    @URI_UOM AS oriUnitOfMeasure ,
                    CASE LEFT(A.SUPPLIER_CODE, 3)
                      WHEN '807' THEN A.SHIPPING_DOCK
                      ELSE A.SUPPLIER_CODE
                    END AS oriSuppCode ,
                    A.RCV_PLANT_CODE AS receivingPlantCode ,
                    A.DOCK_CODE AS receivingPlantArea ,
                    (
                    CASE 
                      WHEN (select COUNT(1) from TB_M_PART_INFO x
                                                   where x.SUPPLIER_CD = A.SUPPLIER_CODE
                                                   and x.SUPPLIER_PLANT = A.SUPPLIER_PLANT  
                                                   and x.DOCK_CD = A.DOCK_CODE
                                                   and x.PART_NO = A.PART_NO
                                                   and A.DROP_STATUS_DATE between x.start_dt and x.end_dt 
                            ) > 0 then 'Y'
                       ELSE 'N'
                     END
                     ) AS intKanbanFlag ,
                    A.DN_COMPLETE_FLAG AS dnCompleteFlag ,
                    NULL AS matCompleteFlag ,
                    @RECEIVE_NO AS receiveNo ,
                    @SEND_FLAG AS sendFlag ,   --- Y/N 
                    @PROCESS_ID AS processID ,
                    A.ORDER_RELEASE_DT AS orderReleaseDate 
            FROM    #ICS_POSTING A


              
                     -- 1. UPDATE FLAG
                     UPDATE #ICS_FILE 
                     SET 
                           INT_KANBAN_FLAG = 'N'
                     -- USED IN OCTOBER   ,ORI_SUPP_CD = CAST(LTRIM(B.SUPPLIER_CVRT) AS CHAR(6))
                     FROM 
                           #ICS_FILE A, TB_M_SUPPLIER_CVRT B
                     WHERE 
                           LTRIM(RTRIM(A.RECEIVING_PLANT_CD)) = LTRIM(RTRIM(B.RECEIVING_PLANT)) AND 
                           LTRIM(RTRIM(A.RECEIVING_AREA_CD)) = LTRIM(RTRIM(B.RECEIVING_AREA)) 
                                  
                            
                     /*                                                                                                    
                     =================================================                                                                                                     
                           END OF CREATE DATA LIST FROM ORDER                                                                                          
                     =================================================                                                                                                     
                     */     
                     --select * from openquery([IPPCS_TO_ICS] , 'select * from TB_T_GOOD_RECEIVE_KBS')
                         
                     INSERT OPENQUERY (IPPCS_TO_ICS, 
                     'SELECT  
                     PROCESS_ID, 
                     SYSTEM_SOURCE, 
                     USR_ID, 
                     POSTING_DT,
                     PROCESS_DT,
                     REF_NO,
                     MOVEMENT_TYPE,
                     KANBAN_ORDER_NO, 
                     PROD_PURPOSE_CD, 
                     SOURCE_TYPE, 
                     ORI_MAT_NO, 
                     UNIT_OF_MEASURE_CD, 
                     RECEIVING_PLANT_CD, 
                     RECEIVING_AREA_CD, 
                     INT_KANBAN_FLAG, 
                     ORI_SUPP_CD, 
                     DN_COMPLETE_FLAG, 
                     CREATED_BY, 
                     CREATED_DT, 
                     POSTING_QUANTITY,  
                     RECEVING_PLANT_CD, 
                     ORI_UNIT_OF_MEASURE_CD, 
                     ARRIVAL_PLAN_DT, 
                     OTHER_PROCESS_ID  
                     FROM 
                     TB_T_GOOD_RECEIVE_KBS')
                     select 0 PROCESS_ID, --1
                            RTRIM(SYS_SOURCE),  --2
                            [USER_ID], --3
                            POSTING_DATE,
                   PROCESS_DATE,
                   RTRIM(REF_NO), --6
                            RTRIM(MOV_TYPE), --7
                            RTRIM(KANBAN_ORDER_NO),  --8
                            RTRIM(PROD_PURPOSE_CD),  --9
                            RTRIM(SOURCE_TYPE),  --10
                            RTRIM(ORI_MAT_NO),  --11
                            RTRIM(ORI_UNIT_OF_MEASURE_CD) ,  --12
                            RTRIM(RECEIVING_PLANT_CD),  --13
                            RTRIM(RECEIVING_AREA_CD),  --14
                            INT_KANBAN_FLAG,  --15
                            RTRIM(ORI_SUPP_CD),  --16
                            DN_COMPLETE_FLAG,  --17
                            [USER_ID], --18
                            GETDATE(),
                            POSTING_QUANTITY, --20
                            RTRIM(RECEIVING_PLANT_CD),  --21
                            RTRIM(ORI_UNIT_OF_MEASURE_CD),  --22
                            ARRIVAL_PLAN_DT,  --23
                            @L_N_PROCESS_ID --24
                  from #ICS_FILE
                                                                                                                                                                                                                                         
        END

    -- CHECK ERR
       /*
    SELECT @L_N_COUNT_ERR = COUNT(1) FROM
    TB_R_DAILY_ORDER_MANIFEST
    where MANIFEST_NO in ( SELECT * FROM #TempManifestNo )
    and (ISNULL(ICS_FLAG,1) <> 0
    or ISNULL(CANCEL_FLAG,1) <> 0
    or ISNULL(DROP_STATUS_FLAG,0) <> 1
    or ISNULL(PROBLEM_FLAG,1) <> 0
    or MANIFEST_RECEIVE_FLAG not in ('0','1','2','3','5'));*/
    
       SELECT @L_N_COUNT_ERR = COUNT(1) FROM
    TB_R_DAILY_ORDER_MANIFEST
    where MANIFEST_NO in ( SELECT * FROM #TempManifestNo )
    and (ISNULL(ICS_FLAG,1) <> 0
    or ISNULL(CANCEL_FLAG,1) <> 0
    or ISNULL(DROP_STATUS_FLAG,0) <> 1
    or ISNULL(PROBLEM_FLAG,1) <> 0
    or MANIFEST_RECEIVE_FLAG not in ('2','3','5'));



    -- CHECK NUMBER OF MANIFEST
    SELECT @L_N_COUNT_MANIFEST = COUNT(1) 
    FROM
    #TempManifestNo;
    
    --COMPARE MANIFEST
    IF @L_N_COUNT_ERR <> @L_N_COUNT_MANIFEST

        BEGIN
            INSERT INTO [dbo].[TB_R_ICS_QUEUE]
                                     ([IPPCS_MODUL]
                                     ,[IPPCS_FUNCTION]
                                     ,[PROCESS_ID]
                                     ,[PROCESS_STATUS]
                                     ,[CREATED_BY]
                                     ,[CREATED_DT])
            VALUES
                        ('4'
                        ,'42001'
                        ,@L_N_PROCESS_ID
                        ,0
                        ,@UserID
                        ,GETDATE());
                     
                     INSERT OPENQUERY (IPPCS_TO_ICS, 
                                                'SELECT  
                                                       OTHER_PROCESS_ID,
                                                       STATUS,
                                                       REFERENCE,
                                                       CREATED_BY,
                                                       CREATED_DT
                                              FROM TB_R_PCS_INTERFACE_STATUS')
            VALUES (@L_N_PROCESS_ID, 0, 'Posting Goods Receipt',@UserID,GETDATE());                                             
        
        END;
        
              BEGIN
                     SELECT c.* FROM
                     TB_R_DAILY_ORDER_MANIFEST c,
                     TB_R_DAILY_ORDER_PART d
                     where c.MANIFEST_NO in ( SELECT * FROM #TempManifestNo )
                     and c.MANIFEST_NO = d.MANIFEST_NO
                     and (ISNULL(c.ICS_FLAG,1) <> 0
                     or ISNULL(c.CANCEL_FLAG,1) <> 0
                     or ISNULL(c.DROP_STATUS_FLAG,0) <> 1
                     or ISNULL(c.PROBLEM_FLAG,1) <> 0
                     or c.MANIFEST_RECEIVE_FLAG  not in ('0','1','2','3','5')
                     -- added for TP
                     or ISNULL(d.POSTING_STS,'3') = 0)
                     ;    
              END;
                            
    IF OBJECT_ID('tempdb..#TempManifestNo') IS NOT NULL 
        DROP TABLE #TempManifestNo
    IF OBJECT_ID('tempdb..#tempDockCode') IS NOT NULL 
        DROP TABLE #tempDockCode
    IF OBJECT_ID('tempdb..#tempRvcPlantCode') IS NOT NULL 
        DROP TABLE #tempRvcPlantCode
    IF OBJECT_ID('tempdb..#tempDockCode') IS NOT NULL 
        DROP TABLE #tempDockCode
   IF OBJECT_ID('tempdb..#tempSupplierCode') IS NOT NULL 
        DROP TABLE #tempSupplierCode
    IF OBJECT_ID('tempdb..#ICS_FILE') IS NOT NULL 
        DROP TABLE #ICS_FILE
    IF OBJECT_ID('tempdb..#ICS_POSTING') IS NOT NULL 
        DROP TABLE #ICS_POSTING
    IF OBJECT_ID('tempdb..#INVOICENO_GR') IS NOT NULL 
        DROP TABLE #INVOICENO_GR
    IF OBJECT_ID('tempdb..#INVOICENO_TP') IS NOT NULL 
        DROP TABLE #INVOICENO_TP



END

