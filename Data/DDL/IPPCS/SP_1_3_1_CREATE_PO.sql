
CREATE procedure [dbo].[sp_1_3_1_CREATE_PO]
  @Prod_Month varchar(6) = NULL -- YYYYMM
, @process_id BIGINT = 0 OUTPUT
, @username VARCHAR(20) = NULL
  AS
BEGIN TRY
-- translated from pkg_01_procurement_01_16.sp_pcs_po_local_create_main
-- for ippcs procurement 2020-09-16
-- depends on:
--   type : POHeader PODetail POItem CompPrice
--    PutLog
--    Lock_Process
--    sp_1_3_1_order_data_val
--    sp_1_3_1_po_local_create
--       sp_1_3_1_create_po_detail
--       sp_1_3_1_create_po_item
--       sp_1_3_1_create_po_header
--       sp_1_3_1_po_local_post
-- param:
--  @prod_month : required, production month to process
--  @process_id : optional, only required for local order, when not present, get data from tb_t_po_process
--  @username   : required for approval worklist creation
-----

--History -> FID.Ridwan: 2021-03-23 --> add logic if source data from sprint or doc type in system master po manual sprints then
-- -> PO always create new po no based on doc type and other process id
-- -> if exists same supp cd but differet other process id then create more than one po no, because sprint based on other process id not supp cd

DECLARE
  @fn VARCHAR(6) = '10301'
, @na VARCHAR(30) = 'sp_1_3_1_CREATE_PO'
, @nu VARCHAR(10) = '?'
, @msgid VARCHAR(12)
, @loc VARCHAR(800) = ''
, @by varchar(20)
, @emsg varchar(max)
, @MAX_LOOP INT =  65535
, @errCount int = 0
, @row_count_err int = 0
, @row_count_current int = 0
, @lastStatus TINYINT = 0
, @lock_stat bigint = 0
, @pid BIGINT = 0
, @stat_get_comp_price int
, @po_no_by_supplier varchar(10)
, @ret_get_delivery_dt date
, @po_data_rowcount int = 0
, @prod_date DATETIME = NULL
, @now DATETIME
, @inf varchar(12) = 'MPCS00001INF'
, @count_po_no INT = 0
, @po_no varchar(2000)= null
, @logID bigint = NULL
, @DOC_TYPE VARCHAR(10) = NULL
, @po_reguler_doc_type VARCHAR(2) = NULL
, @lock_key varchar(20) = 'INT_PO_LOCAL_CREATE'
, @lock_key_supp varchar(20) = null
, @lock_key_material varchar(20) = null
;
    SET NOCOUNT ON;

if NULLIF(@username,'') is not null
BEGIN
    IF (select object_id('tempdb..#TB_T_PO_H')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_H;
    end;
    IF (select object_id('tempdb..#TB_T_PO_D')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_D;
    end;
    IF (select object_id('tempdb..#TB_T_PO_I')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_I;
    end;
END;
IF (select object_id('tempdb..#TB_T_PO_H')) IS  NULL
    CREATE TABLE #TB_T_PO_H
    (
      PO_NO                       varchar(10)
    , SUPP_CD                     varchar(6)
    , DOC_DT                      date
    , PO_NOTE                     varchar(100)
    , INV_WO_GR_FLAG              varchar(1)
    , DELETION_FLAG               varchar(1)
    , CALCULATION_SCHEMA_CD       varchar(4)
    , DOC_TYPE                    varchar(2)
    , PROD_MONTH                  numeric(6)
    , PO_EXCHANGE_RATE            numeric(13,2)
    , PO_CURR                     varchar(3)
    , PAYMENT_METHOD_CD           varchar(2)
    , PAYMENT_TERM_CD             varchar(4)
    , REFF_INV_NO                 varchar(20)
    , REFF_INV_DT                 date
    , REFF_DOC                    varchar(20)
    , REFF_DOC_DT                 date
    , RECORD_ID                   INT
    , ID                          INT IDENTITY (1,1)
    );
IF (select object_id('tempdb..#TB_T_PO_I')) IS  NULL
    CREATE TABLE #TB_T_PO_I (
      PO_NO                       varchar(10)
    , PO_ITEM_NO                  varchar(5)
    , PR_NO                       varchar(8)
    , PR_ITEM                     varchar(5)
    , MAT_NO                      varchar(23)
    , SOURCE_TYPE                 varchar(1)
    , PLANT_CD                    varchar(4)
    , PROD_PURPOSE_CD             varchar(5)
    , SLOC_CD                     varchar(6)
    , PART_COLOR_SFX              varchar(2)
    , UNLIMITED_FLAG              varchar(1)
    , TOLERANCE_PERCENTAGE        numeric(5,2)
    , PO_GR_QUANTITY              numeric(13,3)
    , CONTAINER_NO                varchar(10)
    , NOTE                        varchar(100)
    , SPECIAL_PROCUREMENT_TYPE_CD varchar(2)
    , MAT_DESC                    varchar(40)
    , PO_QUANTITY_ORIGINAL        numeric(13,3)
    , PO_QUANTITY_NEW             numeric(13,3)
    , PO_QUANTITY_OPEN            numeric(13,3)
    , UNIT_OF_MEASURE_CD          varchar(3)
    , PO_MAT_PRICE                numeric(16,5)
    , DELIVERY_DT                 date
    , DELETION_FLAG               varchar(1)
    , TAX_CD                      varchar(2)
    , PACKING_TYPE                varchar(1)
    , SUBCONTRACT_FLAG            varchar(1)
    , RECORD_ID                   INT
    , ID                          INT IDENTITY(1,1)
    )
IF (select object_id('tempdb..#TB_T_PO_D')) IS  NULL
    CREATE TABLE #TB_T_PO_D (
      PO_NO                       varchar(10)
    , PO_ITEM_NO                  varchar(5)
    , COMP_PRICE_CD               varchar(4)
    , COMP_PRICE_RATE             numeric(16,5)
    , INVOICE_FLAG                varchar(1)
    , PO_EXCHANGE_RATE            numeric(13,2)
    , SEQ_NO                      numeric(3)
    , BASE_VALUE_F                numeric(3)
    , BASE_VALUE_T                numeric(3)
    , CALCULATION_TYPE            varchar(1)
    , PLUS_MINUS_FLAG             varchar(1)
    , SUPP_CD                     varchar(6)
    , PO_CURR                     varchar(3)
    , PO_DETAIL_PRICE             numeric(16,5)
    , PO_DETAIL_AMT               numeric(16,5)
    , INVENTORY_FLAG              varchar(1)
    , ACCRUAL_POSTING_FLAG        varchar(1)
    , CONDITION_CATEGORY          varchar(1)
    , RECORD_ID                   INT
    , ID                          INT IDENTITY(1,1)
    )

    SELECT
       @prod_date = CASE WHEN ISDATE(@Prod_Month+'01') = 1 THEN  CONVERT(DATETIME, @Prod_Month+'01', 112) ELSE NULL END,
       @now = getdate(),
       @by = COALESCE(@username, @na),
       @msgid = 'MPCS00001INF',
       @logID= NULLIF(@process_id,0),
       @emsg = 'CREATE_PO (' + COALESCE(@PROD_MONTH,@nu)  + ', '+ COALESCE(CONVERT(VARCHAR, @PROCESS_ID), @nu) + ') STARTED';

    if NULLIF(@PROCESS_ID,0) is not null
    AND NOT EXISTS(SELECT TOP 1 PROCESS_ID FROM TB_R_LOG_H WHERE PROCESS_ID = @PROCESS_ID)
    BEGIN
        SET @emsg = @emsg + '. invalid process_id, start a new log';
        set @logID = NULL;
    END

    exec dbo.PutLog @what = @emsg, @user= @by, @where= @na, @pid= @PROCESS_ID output, @id= @msgid, @func = @fn;

    IF NULLIF(@prod_month,'') IS NULL
    BEGIN
        SELECT @errCount = @errCount+ 1;
        exec dbo.PutLog @what = NULL, @user = @by, @where = 'prod month check', @pid = @PROCESS_ID OUTPUT, @id =  'MPCS3058BERR', @v0 = 'prod_month';
    END

    if len(@prod_month) != 6 or  isdate(@prod_month + '01')=0
    begin
        select @errCount = @errCount +1;
        exec dbo.PutLog @what = @emsg, @user = @by, @where = 'prod month format', @pid = @PROCESS_ID OUTPUT, @id = 'MPCS3089BERR', @v0 = 'prod_month', @v1 = 'YYYYMM';
    end

    SELECT TOP 1 @po_no = SYSTEM_VALUE
    FROM TB_M_SYSTEM WHERE FUNCTION_ID = '300' and SYSTEM_CD = 'PO_NO_Z1';

    select top 1 @PO_REGULER_DOC_TYPE= convert(varchar(2), system_value)
    from tb_m_system where function_id = '300' and system_cd = 'PO_LOCAL_REGULER_DOC_TYPE'; -- S ?

    if @po_no IS NULL
    BEGIN
        SELECT @errCount = @errCount+ 1;

        exec dbo.PutLog @what = NULL, @user = @by, @where = 'PO_NO system master', @pid = @PROCESS_ID OUTPUT, @id = 'MPCS3062BERR', @v0 = 'PO_NO';
    END;

    if @po_no IS NOT NULL
    BEGIN
        IF ISNUMERIC(@po_no) = 0 or len(@po_no) > 10
        begin
            select @errCount = @errCount+ 1;
            exec dbo.PutLog @what = NULL, @user = @by, @where = 'PO NO format', @pid = @PROCESS_ID OUTPUT, @id = 'MPCS3089BERR', @V0 = 'PO_NO', @V1= 'numeric 10 digit ';
        end;
    END;

    SELECT top 1 @doc_type = system_cd
    from tb_m_system where function_id = '102' and system_value ='LOCAL'; --Z3

    IF @doc_type is null
    BEGIN
       select @errCount = @errCount+ 1
       exec dbo.PutLog @what = @emsg, @user = @by, @where = 'tb_m_system(''102'',@DOC_TYPE,''LOCAL'')', @pid = @PROCESS_ID OUTPUT, @id = 'MPCS3062BERR', @v0 = 'Document Type';
    END;

    IF @prod_date IS NULL
        set @errCount = @errCount + 1;

    if @errCount > 0
    begin
        select @msgid = 'MPCS3060BINF'
        , @emsg = 'Process Exit with Error'
        , @lastStatus =2;
        RAISERROR (@emsg, 16, 1, null);
    end;

      --     attempt lock process
    set @pid = @process_id;
    EXEC @lock_stat = dbo.Lock_Process @fn, @lock_key,  @pid OUTPUT, @username;
    if @lock_stat != 0
    begin
        SELECT @emsg = dbo.fn_MessageFormat('MPCS3080BERR', @lock_key, null, null, null, null);

       raiserror (@emsg, 16, 3, null);
    end;

    set @emsg = 'lock '+@lock_key;
    exec dbo.PutLog @emsg, @by, 'lock', @process_id output, @inf;

    set @emsg = '';
    declare @order_data_ret int = 0;
    -- call data validation fn_order_data_val_proc
    EXEC @order_data_ret = dbo.sp_1_3_1_order_data_val @prod_month, @process_id, @by, @emsg OUTPUT, @logID;

    -- if data validation failed
    IF @order_data_ret != 0
    BEGIN
      select @msgid = 'MPCS3063BABO'
        , @emsg = 'Order data validation was not success, process aborted'
        , @lastStatus =2;
        RAISERROR (@emsg, 16, 2, null);
    END

    -- if data validation success



    -- continue process
    declare @count_po_data int = 0;

    select  @count_po_data =count(1)
    FROM TB_T_LOCAL_VAL_H
    WHERE VAL_STS ='OK'
    AND QTY_N != 0
    AND PROD_MONTH = @prod_month
    -- group by   SUPP_CD,MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PART_COLOR_SFX,
    -- MAT_DESC, PLANT_CD, SLOC_CD, CALCULATION_SCHEMA_CD, PAYMENT_METHOD_CD,
    -- PAYMENT_TERM_CD, SPECIAL_PROCUREMENT_TYPE_CD, UNIT_OF_MEASURE_CD,
    -- PACKING_TYPE, MAT_PRICE, PO_CURR, VAL_STS, PROD_MONTH, PO_NO, PO_ITEM_NO;

    set @emsg = 'process ' + convert(varchar(15), @count_po_data) + ' po data';
    exec dbo.PutLog @emsg, @by, 'PO', @process_id output, @inf;

    DECLARE @ix_po_data int = 0
     , @iloop int = 0;

    set @emsg = 'po data by  ' + @prod_month + ' has ' + convert(varchar(15), @count_po_data) + ' rows OK';
    exec dbo.PutLog @emsg, @by, @where='get_po_data', @pid=@process_id output, @id=@inf;

    declare
      @last_supp_cd                   varchar(6) = null
    , @supp_cd_next                   varchar(6) = null
    , @supp_cd_log                    varchar(6) = null
    , @err_get_Comp_price             int

    , @ret_po_no_by_supp              varchar(10)
    , @ret_stat_get_po_by_mat         int

    , @ret_get_po_no_by_mat           varchar(10)
    , @ret_get_po_item_by_mat         varchar(5)
    , @ret_get_po_qty_new             numeric(13,3)
    , @ret_get_po_open_qty            numeric(13,3)
    , @ret_get_mat_price              numeric(16,5)
    , @ret_delivery_dt                date
    , @supp_cd                        varchar(6)

    , @po_SUPP_CD                     varchar(6)
    , @po_MAT_NO                      varchar(23)
    , @po_PROD_PURPOSE_CD             varchar(5)
    , @po_SOURCE_TYPE                 varchar(1)
    , @po_PART_COLOR_SFX              varchar(2)
    , @po_MAT_DESC                    varchar(40)
    , @po_QTY_N                       numeric(13, 3)
    , @po_QTY_N1                      numeric(13, 3)
    , @po_QTY_N2                      numeric(13, 3)
    , @po_QTY_N3                      numeric(13, 3)
    , @po_PLANT_CD                    varchar(4)
    , @po_SLOC_CD                     varchar(6)
    , @po_CALCULATION_SCHEMA_CD       varchar(4)
    , @po_PAYMENT_METHOD_CD           varchar(2)
    , @po_PAYMENT_TERM_CD             varchar(4)
    , @po_SPECIAL_PROCUREMENT_TYPE_CD varchar(2)
    , @po_UNIT_OF_MEASURE_CD          varchar(3)
    , @po_PACKING_TYPE                varchar(1)
    , @po_MAT_PRICE                   numeric(16, 5)
    , @po_PO_CURR                     varchar(3)
    , @po_VAL_STS                     varchar(3)
    , @po_PROD_MONTH                  numeric(6, 0)
    , @po_PO_NO                       varchar(10)
    , @po_PO_ITEM_NO                  varchar(5)
    , @po_doc_type                    varchar(2)
    , @po_record_id                   int
    --* FID.Ridwan: 20210323 -> PO Manual SPRINTS
    , @last_other_process                    varchar(15) = null
    , @po_OTHER_PROCESS_ID                      varchar(15)
    , @last_record_id INT = 0
    , @curr_PO_NO                       varchar(10)

    declare @llowed_doc_type_spr table(doc_type varchar(10));
    insert @llowed_doc_type_spr(doc_type)
        select convert(varchar(10), items) doc_type
        from dbo.fn_split((
            select TOP 1 system_value
            from tb_m_system where FUNCTION_ID = '10301' and SYSTEM_CD = 'DOC_TYPE_SPR'
        ), ',') x;
    --* end FID.Ridwan

    set @ix_po_data = 0;

	--FID.Ridwan: 20210521 --> tune query take out looping when insert to tb r po
	--* Get list from local val h where sts = OK
	IF OBJECT_ID('tempdb..#TB_T_LOCAL_VAL_H_SOURCE') IS NOT NULL
	BEGIN
		DROP TABLE #TB_T_LOCAL_VAL_H_SOURCE
	END

	IF OBJECT_ID('tempdb..#TB_T_LOCAL_VAL_H_OK') IS NOT NULL
	BEGIN
		DROP TABLE #TB_T_LOCAL_VAL_H_OK
	END

	CREATE TABLE #TB_T_LOCAL_VAL_H_OK
	(
		MATERIAL_ROW_NO INT,
		SUPPLIER_ROW_NO INT,
		SUPP_CD varchar(6),
		MAT_NO varchar(23),
		PROD_PURPOSE_CD varchar(5),
		SOURCE_TYPE varchar(1),
		PART_COLOR_SFX varchar(2),
		MAT_DESC varchar(40),
		QTY_N numeric(13,3),
		QTY_N1 numeric(13,3),
		QTY_N2 numeric(13,3),
		QTY_N3 numeric(13,3),
		PLANT_CD varchar(4),
		SLOC_CD varchar(6),
		CALCULATION_SCHEMA_CD varchar(4),
		PAYMENT_METHOD_CD varchar(2),
		PAYMENT_TERM_CD varchar(4),
		SPECIAL_PROCUREMENT_TYPE_CD varchar(2),
		UNIT_OF_MEASURE_CD varchar(3),
		PACKING_TYPE varchar(1),
		MAT_PRICE numeric(16,5),
		PO_CURR varchar(3),
		VAL_STS varchar(3),
		PROD_MONTH numeric(6),
		PO_NO varchar(10),
		PO_ITEM_NO varchar(5),
		PR_NO varchar(8),
		PR_ITEM_NO varchar(5),
		doc_type varchar(2),
		OTHER_PROCESS_ID varchar(15),
		DOC_TYPE_FLAG varchar(1),
		cal_scheme_flag varchar(1),
		PREV_PO_NO varchar(10),
		PREV_PO_ITEM_NO varchar(5),
		TYPE CHAR(1)
	)
	CREATE NONCLUSTERED INDEX [IDX_PO_NO] ON #TB_T_LOCAL_VAL_H_OK
	(
		po_no ASC, po_item_no ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]	
	CREATE NONCLUSTERED INDEX [IDX_MATERIAL] ON #TB_T_LOCAL_VAL_H_OK
	(
		SUPP_CD,mat_no,SOURCE_TYPE,PROD_PURPOSE_CD,packing_type,part_color_sfx,plant_cd,sloc_cd ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	SELECT g.rowno
			,RECORD_ID
			,t.SUPP_CD
			,MAT_NO
			,PROD_PURPOSE_CD
			,SOURCE_TYPE
			,PART_COLOR_SFX
			,MAT_DESC
			,QTY_N
			,QTY_N1
			,QTY_N2
			,QTY_N3
			,PLANT_CD
			,SLOC_CD
			,CALCULATION_SCHEMA_CD
			,PAYMENT_METHOD_CD
			,PAYMENT_TERM_CD
			,SPECIAL_PROCUREMENT_TYPE_CD
			,UNIT_OF_MEASURE_CD
			,PACKING_TYPE
			,MAT_PRICE
			,PO_CURR
			,VAL_STS
			,PROD_MONTH
			,PO_NO
			,PO_ITEM_NO
			,PR_NO
			,PR_ITEM_NO
			,COALESCE(t.DOC_TYPE, @doc_type) doc_type
			,OTHER_PROCESS_ID
			,CASE WHEN COALESCE(t.DOC_TYPE, @doc_type) not in (select doc_type from @llowed_doc_type_spr) THEN 'L' ELSE 'S' END DOC_TYPE_FLAG
			,CASE WHEN ISNULL(CALCULATION_SCHEMA_CD, '') in (select DISTINCT CALCULATION_SCHEMA_CD from TB_M_COMP_PRICE) THEN '1' ELSE '0' END cal_scheme_flag
	INTO #TB_T_LOCAL_VAL_H_SOURCE
	FROM TB_T_LOCAL_VAL_H t
	join (select SUPP_CD, row_number() over(order by supp_cd) rowno FROM (select distinct supp_cd from TB_T_LOCAL_VAL_H t
		WHERE t.VAL_STS ='OK'
		   AND t.QTY_N != 0
		   AND t.PROD_MONTH = @prod_month) tb) g on t.SUPP_CD = g.SUPP_CD
    WHERE t.VAL_STS ='OK'
       AND t.QTY_N != 0
       AND t.PROD_MONTH = @prod_month
       order by record_id

	IF EXISTS(SELECT TOP 1 1 FROM #TB_T_LOCAL_VAL_H_SOURCE WHERE cal_scheme_flag = '0')
	BEGIN
		SET @msgid = 'MPCS11000ERR';

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT D.PROCESS_ID
			,MAX(D.SEQUENCE_NUMBER) + M.SEQ_NO AS SEQUENCE_NUMBER
			,M.MESSAGE_ID
			,M.MESSAGE_TYPE
			,REPLACE(M.MESSAGE_TEXT, '{0}', 'Calculation Schema Code: ' + M.CALCULATION_SCHEMA_CD + ' not found in master') AS [MESSAGE]
			,@loc AS [LOCATION]
			,@username AS CREATED_BY
			,CURRENT_TIMESTAMP AS CREATED_DATE
		FROM TB_R_LOG_D D
			,(
				SELECT ROW_NUMBER() OVER (
						ORDER BY A.CALCULATION_SCHEMA_CD
						) AS SEQ_NO
					,A.CALCULATION_SCHEMA_CD
					,MSG.MESSAGE_ID
					,MSG.MESSAGE_TYPE
					,MSG.MESSAGE_TEXT
				FROM #TB_T_LOCAL_VAL_H_SOURCE a
					,dbo.TB_M_MESSAGE MSG
				WHERE cal_scheme_flag = '0'
					AND MSG.MESSAGE_ID = @msgid
				) M
		WHERE D.PROCESS_ID = @process_id
		GROUP BY D.PROCESS_ID
			,M.CALCULATION_SCHEMA_CD
			,M.SEQ_NO
			,M.MESSAGE_ID
			,M.MESSAGE_TYPE
			,M.MESSAGE_TEXT
	END

	-- (IPPCS, SPRINTS, CPS)
	IF EXISTS(SELECT TOP 1 1 FROM #TB_T_LOCAL_VAL_H_SOURCE WHERE DOC_TYPE_FLAG = 'L' AND cal_scheme_flag = '1')
	BEGIN
		-- get list previous PO NO from header with same prod month
		IF OBJECT_ID('tempdb..#TB_T_PREV_PO_H') IS NOT NULL
		BEGIN
			DROP TABLE #TB_T_PREV_PO_H
		END

		SELECT PO_NO,
			SUPPLIER_CD,
			DOC_TYPE
		INTO #TB_T_PREV_PO_H
		FROM (
			SELECT ROW_NUMBER() OVER(PARTITION BY SUPPLIER_CD,DOC_TYPE ORDER BY PO_NO DESC) ROWNO,
				PO_NO,
				SUPPLIER_CD,
				DOC_TYPE
			FROM TB_R_PO_H H
			WHERE COALESCE(DELETION_FLAG, 0) = 0
			AND production_month = @PROD_MONTH) TB
		WHERE TB.ROWNO = 1

		-- get list previous PO ITEM from item based on list header
		IF OBJECT_ID('tempdb..#TB_T_PREV_PO_ITEM') IS NOT NULL
		BEGIN
			DROP TABLE #TB_T_PREV_PO_ITEM
		END

		SELECT I.PO_NO, 
			I.PO_ITEM_NO, 
			H.DOC_TYPE,
			H.SUPPLIER_CD,
			I.MAT_NO,
			I.SOURCE_TYPE,
			I.PROD_PURPOSE_CD,
			I.PACKING_TYPE,
			I.PART_COLOR_SFX,
			I.PLANT_CD,
			I.SLOC_CD
		INTO #TB_T_PREV_PO_ITEM
		FROM TB_R_PO_ITEM i 
		JOIN #TB_T_PREV_PO_H h ON h.PO_NO = i.PO_NO
		WHERE i.deletion_flag = 'N'

		-- summ qty from list val h, handle if same supp cd and material
		INSERT INTO #TB_T_LOCAL_VAL_H_OK
		SELECT ROW_NUMBER() OVER(PARTITION BY TB.SUPP_CD ORDER BY TB.SUPP_CD ASC) MAT_ROW_NO, 
			TB.* ,
			POH.PO_NO AS PREV_PO_NO,
			POI.PO_ITEM_NO AS PREV_PO_ITEM_NO,
			null TYPE
		FROM (
		SELECT max(rowno) rownosupp 
			,SUPP_CD
			,MAT_NO
			,PROD_PURPOSE_CD
			,SOURCE_TYPE
			,PART_COLOR_SFX
			,MAX(MAT_DESC) MAT_DESC
			,SUM(QTY_N) QTY_N
			,SUM(QTY_N1) QTY_N1
			,SUM(QTY_N2) QTY_N2
			,SUM(QTY_N3) QTY_N3
			,PLANT_CD
			,SLOC_CD
			,MAX(CALCULATION_SCHEMA_CD) CALCULATION_SCHEMA_CD
			,MAX(PAYMENT_METHOD_CD) PAYMENT_METHOD_CD
			,MAX(PAYMENT_TERM_CD) PAYMENT_TERM_CD
			,MAX(SPECIAL_PROCUREMENT_TYPE_CD) SPECIAL_PROCUREMENT_TYPE_CD
			,MAX(UNIT_OF_MEASURE_CD) UNIT_OF_MEASURE_CD
			,PACKING_TYPE
			,MAX(MAT_PRICE) MAT_PRICE
			,MAX(PO_CURR) PO_CURR
			,MAX(VAL_STS) VAL_STS
			,MAX(PROD_MONTH) PROD_MONTH
			,MAX(PO_NO) PO_NO
			,MAX(PO_ITEM_NO) PO_ITEM_NO
			,MAX(PR_NO) PR_NO
			,MAX(PR_ITEM_NO) PR_ITEM_NO
			,doc_type
			,MAX(OTHER_PROCESS_ID) OTHER_PROCESS_ID
			,MAX(DOC_TYPE_FLAG) DOC_TYPE_FLAG
			,MAX(cal_scheme_flag) cal_scheme_flag
		FROM #TB_T_LOCAL_VAL_H_SOURCE
		WHERE DOC_TYPE_FLAG = 'L' AND cal_scheme_flag = '1'
		group by SUPP_CD,
			mat_no,
			PROD_PURPOSE_CD,
			SOURCE_TYPE,
			PART_COLOR_SFX,
			PLANT_CD,
			SLOC_CD,
			PACKING_TYPE,
			doc_type) TB 
		LEFT JOIN #TB_T_PREV_PO_H POH ON TB.SUPP_CD = POH.SUPPLIER_CD
			AND TB.doc_type = POH.DOC_TYPE
		LEFT JOIN #TB_T_PREV_PO_ITEM POI ON TB.DOC_TYPE = POI.DOC_TYPE
			AND TB.SUPP_CD = POI.SUPPLIER_CD
			AND TB.MAT_NO = POI.MAT_NO
			AND TB.SOURCE_TYPE = POI.SOURCE_TYPE
			AND TB.PROD_PURPOSE_CD = POI.PROD_PURPOSE_CD
			AND TB.PACKING_TYPE = POI.PACKING_TYPE
			AND TB.PART_COLOR_SFX = POI.PART_COLOR_SFX
			AND TB.PLANT_CD = POI.PLANT_CD
			AND TB.SLOC_CD = POI.SLOC_CD

		-- summary po type with status = B -> create po h item d, C -> create po item, d, D -> update po item, d
		UPDATE A
		SET A.TYPE = 'D'
		FROM #TB_T_LOCAL_VAL_H_OK A
		WHERE A.PREV_PO_NO IS NOT NULL
			AND A.PREV_PO_ITEM_NO IS NOT NULL

		UPDATE A
		SET A.TYPE = 'C'
		FROM #TB_T_LOCAL_VAL_H_OK A
		WHERE A.PREV_PO_NO IS NOT NULL
			AND A.PREV_PO_ITEM_NO IS NULL

		UPDATE A
		SET A.TYPE = 'B'
		FROM #TB_T_LOCAL_VAL_H_OK A
		WHERE A.PREV_PO_NO IS NULL
			AND A.PREV_PO_ITEM_NO IS NULL

		IF EXISTS(SELECT TOP 1 1 FROM #TB_T_LOCAL_VAL_H_OK WHERE TYPE = 'D')
		BEGIN 
			SELECT @emsg= 'CREATE PO "D" '
                ,  @loc = 'po_local_create D' ;
			exec PutLog @what = @emsg, @user=@by, @where = @loc, @pid = @process_id output, @id = @INF;

			EXEC dbo.sp_1_3_1_po_local_create 'D'
                , @process_id
                , @username
                , @prod_month;
		END

		IF EXISTS(SELECT TOP 1 1 FROM #TB_T_LOCAL_VAL_H_OK WHERE TYPE = 'C')
		BEGIN 
			SELECT @emsg= 'CREATE PO "C" '
                ,  @loc = 'po_local_create C' ;
			exec PutLog @what = @emsg, @user=@by, @where = @loc, @pid = @process_id output, @id = @INF;

			EXEC dbo.sp_1_3_1_po_local_create 'C'
                , @process_id
                , @username
                , @prod_month;
		END

		IF EXISTS(SELECT TOP 1 1 FROM #TB_T_LOCAL_VAL_H_OK WHERE TYPE = 'B')
		BEGIN 
			SELECT @emsg= 'CREATE PO "B" '
                ,  @loc = 'po_local_create B' ;
			exec PutLog @what = @emsg, @user=@by, @where = @loc, @pid = @process_id output, @id = @INF;

			EXEC dbo.sp_1_3_1_po_local_create 'B'
                , @process_id
                , @username
                , @prod_month;
		END
	END

	-- SPRINT MANUAL
	IF EXISTS(SELECT TOP 1 1 FROM #TB_T_LOCAL_VAL_H_SOURCE WHERE DOC_TYPE_FLAG = 'S' AND cal_scheme_flag = '1')
	BEGIN
		INSERT INTO #TB_T_LOCAL_VAL_H_OK
		SELECT ROW_NUMBER() OVER(PARTITION BY TB.SUPP_CD ORDER BY TB.SUPP_CD ASC) MAT_ROW_NO, 
			TB.rowno rownosupp 
			,TB.SUPP_CD
			,TB.MAT_NO
			,TB.PROD_PURPOSE_CD
			,TB.SOURCE_TYPE
			,TB.PART_COLOR_SFX
			,TB.MAT_DESC
			,TB.QTY_N
			,TB.QTY_N1
			,TB.QTY_N2
			,TB.QTY_N3
			,TB.PLANT_CD
			,TB.SLOC_CD
			,TB.CALCULATION_SCHEMA_CD
			,TB.PAYMENT_METHOD_CD
			,TB.PAYMENT_TERM_CD
			,TB.SPECIAL_PROCUREMENT_TYPE_CD
			,TB.UNIT_OF_MEASURE_CD
			,TB.PACKING_TYPE
			,TB.MAT_PRICE
			,TB.PO_CURR
			,TB.VAL_STS
			,TB.PROD_MONTH
			,TB.PO_NO
			,TB.PO_ITEM_NO
			,TB.PR_NO
			,TB.PR_ITEM_NO
			,TB.doc_type
			,TB.OTHER_PROCESS_ID
			,TB.DOC_TYPE_FLAG
			,TB.cal_scheme_flag ,
			NULL AS PREV_PO_NO,
			NULL AS PREV_PO_ITEM_NO,
			'B' TYPE
		FROM #TB_T_LOCAL_VAL_H_SOURCE TB
		WHERE TB.DOC_TYPE_FLAG = 'S' AND TB.cal_scheme_flag = '1'

		SELECT @emsg= 'CREATE PO "B" '
                ,  @loc = 'po_local_create B' ;
        exec PutLog @what = @emsg, @user=@by, @where = @loc, @pid = @process_id output, @id = @INF;

		EXEC dbo.sp_1_3_1_po_local_create 'B'
                , @process_id
                , @username
                , @prod_month;
	END
	--End FID.Ridwan: 20210521

    set @pid = null;
    EXEC @lock_stat = dbo.Lock_Process @fn, @lock_key,  @pid OUTPUT, @username;

    declare @recoRd_id_starten int = null, @record_id_enden int = null, @record_id_enden_spr int = null;
    SELECT @record_id_starten = COALESCE((
    select top 1 CASE WHEN ISNUMERIC([MESSAGE]) = 1 THEN CONVERT(INT, [MESSAGE]) ELSE 1 END
    from tb_r_log_d
    where process_id = @process_id
    and [LOCATION] = '+TB_T_LOCAL_ORDER'
    ), 0) + 1;

    SELECT @record_id_enden = CASE WHEN ISNUMERIC([MESSAGE]) = 1 THEN CONVERT(INT, [MESSAGE]) ELSE null END
    from tb_r_log_d
    where process_id = @process_id
    and [LOCATION] = '+TB_T_PO_PROCESS';

    --* FID.Ridwan: 20210309 -> PO Manual SPRINTS
    SELECT @record_id_enden_spr = CASE WHEN ISNUMERIC([MESSAGE]) = 1 THEN CONVERT(INT, [MESSAGE]) ELSE null END
    from tb_r_log_d
    where process_id = @process_id
    and [LOCATION] = '+TB_T_PO_SPRINTS';

    if @record_id_enden is not null
    begin
        update t
        set process_status = 9
            , po_no = x.PO_NO
            , PO_ITEM_NO = x.PO_ITEM_NO
            , changed_by = @username
            , changed_dt = GETDATE()
        from tb_t_po_process t
        join (
            select distinct
                o.prod_month, o.dock_cd, o.supp_cd, o.mat_no, o.source_type, o.prod_purpose_cd, o.part_color_sfx,
                o.mat_desc, o.po_no, o.po_item_no, o.sloc_cd, o.plant_cd, MAX(o.qty_n) QTY_N
             from tb_t_local_val_h o
             where record_id between @record_id_starten and @record_id_enden
             and (o.po_no is not null or o.po_item_no is not null)
             group by o.prod_month, o.dock_cd, o.supp_cd, o.mat_no, o.source_type, o.prod_purpose_cd, o.part_color_sfx,
                o.mat_desc, o.po_no, o.po_item_no, o.sloc_cd, o.plant_cd
        ) x on x.prod_month= t.prod_month
        --and (t.DOCK_CODE is null or  x.dock_cd = t.DOCK_CODE)
        and x.supp_cd = t.supp_cd
        and x.mat_no = t.mat_no
        and x.prod_purpose_cd = t.prod_purpose_cd
        and x.source_type = t.source_type
        and x.part_color_sfx = t.part_color_sfx
        and x.sloc_cd = t.sloc_cd
        and x.plant_cd = t.plant_Cd
        where t.PO_NO is null or t.PO_ITEM_NO is null
    end;

    --* FID.Ridwan: 20210309 -> PO Manual SPRINTS
    if @record_id_enden_spr is not null
    begin
        update t
        set process_status = 9
            , po_no = x.PO_NO
            , PO_ITEM_NO = x.PO_ITEM_NO
            , changed_by = @username
            , changed_dt = GETDATE()
        from TB_T_PO_SPRINTS t
        join (
            select distinct
                o.prod_month, o.dock_cd, o.supp_cd, o.mat_no, o.source_type, o.prod_purpose_cd, o.part_color_sfx,
                o.mat_desc, o.po_no, o.po_item_no, o.sloc_cd, o.plant_cd, MAX(o.qty_n) QTY_N, o.OTHER_PROCESS_ID,
                o.PR_NO, o.PR_ITEM_NO
             from tb_t_local_val_h o
             where record_id between @record_id_starten and @record_id_enden_spr
             and (o.po_no is not null or o.po_item_no is not null)
             group by o.prod_month, o.dock_cd, o.supp_cd, o.mat_no, o.source_type, o.prod_purpose_cd, o.part_color_sfx,
                o.mat_desc, o.po_no, o.po_item_no, o.sloc_cd, o.plant_cd, o.OTHER_PROCESS_ID, o.PR_NO, o.PR_ITEM_NO
        ) x on x.prod_month= t.prod_month
        --and (t.DOCK_CODE is null or  x.dock_cd = t.DOCK_CODE)
        and x.supp_cd = t.supp_cd
        and x.mat_no = t.mat_no
        and x.prod_purpose_cd = t.prod_purpose_cd
        and x.source_type = t.source_type
        and x.part_color_sfx = t.part_color_sfx
        and x.sloc_cd = t.sloc_cd
        and x.plant_cd = t.plant_Cd
        and x.OTHER_PROCESS_ID = t.OTHER_PROCESS_ID
        and x.PR_NO = t.PR_NO
        and x.PR_ITEM_NO = t.PR_ITEM_NO
        where t.PO_NO is null or t.PO_ITEM_NO is null
    end;

    MERGE INTO TB_T_PO_RESULT t
    USING ( select process_id= @process_id
            , SEQ =COALESCE(
                (SELECT MAX(SEQ_NO)
                FROM TB_T_PO_RESULT
                WHERE PROCESS_ID = @process_id)
                , 0) + 1
    ) x on x.PROCESS_ID = T.process_id AND x.SEQ = t.SEQ_NO
    WHEN NOT MATCHED THEN
    INSERT (PROCESS_ID, SEQ_NO, ERR_CD, ERR_TEXT, CREATED_BY, CREATED_DT)
    VALUES (x.process_id, x.seq, 'MPCS00001INF', 'CREATE PO ' + @prod_month + ' DONE', @USERNAME, GETDATE());

    declare @worklist table (PO_NO VARCHAR(10), ID INT IDENTITY(1,1))

    insert INTO @worklist (PO_NO)
    select h.po_no
    from tb_t_local_val_h h
    where h.po_no is not null
    and h.PROD_MONTH = @prod_month
    and h.VAL_STS = 'OK'
    group by h.po_no;

    DECLARE @@i INT =0, @@j INT =0, @@pono VARCHAR(10);
    select  @@j = MAX(ID) FROM @worklist w;
    while @@i <= @@j
    begin
        set @@pono = NULL;
        select top 1 @@pono = PO_NO FROM @worklist where ID = @@i;

        update h
        set TOTAL_AMOUNT = (SELECT SUM(AMOUNT) FROM TB_R_PO_D WHERE PO_NO = @@pono)
        FROM TB_R_PO_H h
        WHERE h.PO_NO = @@pono
        and h.PRODUCTION_MONTH = @prod_month ;

        IF @@pono IS NOT NULL and nullif(@USERNAME ,'') is not null AND (SELECT count(1) fROM IPPCS_WORKFLOW.dbo.TB_R_WORKLIST where FOLIO_NO =@@pono) = 0
        BEGIN
            EXEC IPPCS_WORKFLOW.dbo.sp_RegisterWorklist @@pono, 1, @username, @process_id, 1;
        END;
        set @@i = @@i + 1;
    end;
    if NULLIF(@username,'') is NOT null
    begin
    IF (select object_id('tempdb..#TB_T_PO_H')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_H;
    end;
    IF (select object_id('tempdb..#TB_T_PO_D')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_d;
    end;
    IF (select object_id('tempdb..#TB_T_PO_I')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_I;
    end;
    end
    exec dbo.PutLog @what=NULL, @user= @by, @where= @na, @pid= @PROCESS_ID output, @id='MPCS01000INF', @sts = 6, @v0 = @na;
    return 0;
END TRY
BEGIN CATCH
    DECLARE @errState INT =1;
    SET @errState = ERROR_STATE();
    select @emsg = case when @emsg is not null then @emsg + '. '  else '' end
         + coalesce(ERROR_MESSAGE(), @nu)
         + ' @'+ coalesce(convert(varchar(100), ERROR_PROCEDURE()), @nu)
         + ':' + coalesce(CONVERT(VARCHAR(15), ERROR_LINE()), @nu)
         + ' #' + coalesce(convert(varchar(15), ERROR_NUMBER()),@nu)
         + ' $' + coalesce(convert(varchar(15), error_severity()), @nu)
         + ' ' + coalesce(convert(varchar(15), ERROR_STATE()) , @nu )
         ;
    print @emsg;
    exec dbo.PutLog @what= @emsg, @user=@by, @where=@na, @pid= @PROCESS_ID OUTPUT,@id = 'MPCS11000ERR', @func=@fn, @sts=@lastStatus;

    MERGE INTO TB_T_PO_RESULT t
    USING (
        select process_id= @process_id
            , SEQ =COALESCE(
                (SELECT MAX(SEQ_NO)
                   FROM TB_T_PO_RESULT
                  WHERE PROCESS_ID = @process_id)
                , 0) + 1
    ) x on x.PROCESS_ID = T.process_id AND x.SEQ = t.SEQ_NO
    WHEN NOT MATCHED THEN
    INSERT (PROCESS_ID, SEQ_NO, ERR_CD, ERR_TEXT, CREATED_BY, CREATED_DT)
    VALUES (x.process_id, X.SEQ, 'MPCS11000ERR', 'CREATE PO ' + @prod_month + ' ERROR ' + @emsg, @USERNAME, GETDATE());

    declare @pidn bigint = null;
    EXEC @lock_stat = dbo.Lock_Process @fn, @lock_key,  @pidn OUTPUT, @username;
    if @lock_key_supp is not null
    begin
      set @pidn = null;
      EXEC @lock_stat = dbo.Lock_Process @fn, @lock_key_supp,  @pidn OUTPUT, @username;
    end;
    /*if @lock_key_material is not null
    begin
      set @pidn = null;
      EXEC @lock_stat = dbo.Lock_Process @fn, @lock_key_material,  @pidn OUTPUT, @username;
    end
    */
    IF @errState >= 2
        SELECT @errCount = 1;
    IF NULLIF(@username,'') IS NOT NULL
    BEGIN
    IF (select object_id('tempdb..#TB_T_PO_H')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_H;
    end;
    IF (select object_id('tempdb..#TB_T_PO_D')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_D;
    end;
    IF (select object_id('tempdb..#TB_T_PO_I')) IS NOT NULL
    begin
        DROP TABLE #TB_T_PO_I;
    end;
    END
    return 1;
END CATCH
