-- =============================================
-- Author:		fid.deny
-- Create date: 23-03-2016
-- Description:	Get error list when upload manster build out
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_ERROR_UPLOAD_BO]-- '201511030019'
	@process_id varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT T.ID, T.PART_NO, T.SUPPLIER_CD, T.SUPPLIER_PLANT, T.DOCK_CD, T.KANBAN_NO, T.QTY_PER_BOX, V.MSG 
	INTO #temp
		FROM TB_T_PART_BO T LEFT JOIN TB_T_UPLOAD_VALIDATE V
	ON
		T.ID = V.ID AND T.PROCESS_ID = V.PROCESS_ID
		WHERE T.PROCESS_ID = @process_id
		ORDER BY T.ID ASC

	SELECT DISTINCT ST2.ID, ST2.PART_NO, ST2.SUPPLIER_CD, ST2.SUPPLIER_PLANT, ST2.DOCK_CD AS DOCK_CODE, ST2.KANBAN_NO, ST2.QTY_PER_BOX,
    SUBSTRING(
        (
            Select '; '+ST1.MSG  AS [text()]
            From #temp ST1
            Where ST1.ID = ST2.ID AND
			ISNULL(ST1.PART_NO,'') = ISNULL(ST2.PART_NO,'') AND
			ISNULL(ST1.SUPPLIER_CD, '') = ISNULL(ST2.SUPPLIER_CD, '') AND
			ISNULL(ST1.SUPPLIER_PLANT, '') = ISNULL(ST2.SUPPLIER_PLANT, '') AND
			ISNULL(ST1.DOCK_CD, '') = ISNULL(ST2.DOCK_CD, '') AND
			ISNULL(ST1.KANBAN_NO, '') = ISNULL(ST2.KANBAN_NO, '') AND
			ISNULL(ST1.QTY_PER_BOX, '') = ISNULL(ST2.QTY_PER_BOX, '')
            For XML PATH ('')
        ), 2, 1000) [MESSAGE]
	From #temp ST2
	ORDER BY ST2.ID ASC

	IF OBJECT_ID('tempdb..#temp') IS NOT NULL 
		DROP TABLE #temp

END

