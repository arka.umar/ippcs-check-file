CREATE PROCEDURE [dbo].[SP_CapacitySendMailAll]
	@To varchar(1000),
	@CC varchar(1000),
	@Subject varchar(200),
	@MailMessage varchar(max)
AS
BEGIN
	EXEC msdb.dbo.sp_send_dbmail
	@profile_name = 'NotificationAgent',
	@recipients = @To,
	@copy_recipients = @CC , 
	@subject = @Subject, 
	@body = @MailMessage,
	@body_format = 'HTML',
	@from_address = 'ippcs-admin@toyota.co.id';
END

