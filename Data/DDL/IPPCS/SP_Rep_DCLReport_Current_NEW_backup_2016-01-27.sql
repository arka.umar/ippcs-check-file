CREATE PROCEDURE [dbo].[SP_Rep_DCLReport_Current_NEW_backup_2016-01-27]
	--created by istd.aang
	--due to : incident cant get data route direct 

	@Date1a varchar(15),
	@Date1b varchar(15),
	@Date2a varchar(15),
	@Date2b varchar(15),
	@LPCode varchar(100),
	@SupplierCode varchar(100)
AS
BEGIN

	DECLARE @SEQROW INT = 0

SELECT row_number() OVER (ORDER BY SUPPLIER_NAME, ARRVDATETIME ASC, DPTDATETIME DESC) AS SeqNbr
			, SUPPLIER_NAME
			, route_code
			, RATE
			--, RTEDATE
			, ARRVDATETIME AS ARRVDATETIME
			, DPTDATETIME AS DPTDATETIME
			--, ARRVDATE
			, dock_code
			, lp_code
			, PICKUP_DT
			, Supplier
			, SupplierPlant
			, dateorder --add by fid.deny 2015-05-05 for ordering result by datetime
     INTO #temp
	FROM
	  (
			SELECT 
				s.SUPPLIER_NAME, 
				sm.RTEGRPCD AS route_code, 
				sm.RUNSEQ AS RATE, 
				--CONVERT(VARCHAR(20),sm.RTEDATE,111) AS RTEDATE,
				sm.ARRVDATETIME AS ARRVDATETIME, 
				sm.DPTDATETIME AS DPTDATETIME, 
				--convert(DATE, sm.ARRVDATETIME) AS ARRVDATE,
				CASE WHEN sm.LOGPTCD LIKE '807%' THEN ISNULL((SELECT DISTINCT PHYSICAL_DOCK FROM TB_M_DELIVERY_STATION_MAPPING WHERE DOCK_CD = sm.DOCKCD AND USED_FLAG = 1), sm.DOCKCD) ELSE '' END AS dock_code, 
				sm.LOGPARTNERCD AS lp_code, 
				--DATENAME(MONTH, @ProductionYear + '-'+ @ProductionMonth + '-01') + ' ' + @ProductionYear AS PICKUP_DT,
				null AS PICKUP_DT,
				sm.LOGPTCD AS Supplier,
				sm.PLANTCD AS SupplierPlant,
				sm.ARRVDATETIME AS dateorder --add by fid.deny 2015-05-05 for ordering result by datetime
			FROM
				TB_R_SUPP_MEETING_DRIVER_DATA sm
				INNER JOIN TB_M_SUPPLIER s ON s.SUPPLIER_CODE = sm.LOGPTCD AND s.SUPPLIER_PLANT_CD = sm.PLANTCD
			WHERE
				((sm.LOGPTCD + '-' + sm.PLANTCD = @SupplierCode
				AND isnull(@SupplierCode, '') <> ''
				OR (isnull(@SupplierCode, '') = '')))
				AND ((sm.LOGPARTNERCD = @LPCode
				AND isnull(@LPCode, '') <> ''
				OR (isnull(@LPCode, '') = '')))

				AND

				(sm.RTEDATE = @Date1a
				
				OR ((RTEDATE = @Date1b) AND (ISNULL(@Date1b,'') <> ''))
				OR ((RTEDATE = @Date2a) AND (ISNULL(@Date2a,'') <> ''))
				OR ((RTEDATE = @Date2b) AND (ISNULL(@Date2b,'') <> '')))

				--added fid.deny 2015-05-04 (filtering GT and YD)
				AND sm.DOCKCD NOT IN ('GT', 'YD')
				
				AND '' = CASE WHEN ISNULL(@SupplierCode, '') <> '' THEN
							RTRIM(LTRIM(sm.DOORCD)) --compare with DOORCD if @suppliercode is not null (it means report by supplier)
						 ELSE
							'' --compare with '' (will return true) if @suppliercode is null (it means report by LP)
						 END
				AND '' <> CASE WHEN ISNULL(@SupplierCode, '') <> '' THEN
							RTRIM(LTRIM(sm.LOGPARTNERCD)) --compare with DOORCD if @suppliercode is not null (it means report by supplier)
						 ELSE
							'a' --compare with 'a' (will return true) if @suppliercode is null (it means report by LP)
						 END
				--end of added fid.deny


				--YEAR(sm.RTEDATE) = @ProductionYear AND MONTH(sm.RTEDATE) = @ProductionMonth
				--AND INBOUTBFLAG <> 0
		) AS a
	ORDER BY
	  SeqNbr ASC

	CREATE TABLE #temp2
	 (SeqNbr INT,
	  SupplierName Varchar(500),
	  RouteCode VARCHAR(4),
	  Rate VARCHAR(2),
	  --RTEDATE VARCHAR(20),
	  Arrival varchar(5),
	  Departure varchar(5),
	  --ArrivalDate varchar(10),
	  DockCode VARCHAR(500),
	  Lp_Code VARCHAR(3),
	  PickUp VARCHAR(100),
	  PickupMethod CHAR(2),
	  Supplier Varchar(20),
	  SupplierPlant Varchar(20),
	  DateOrder datetime)  --add by fid.deny 2015-05-05 for ordering result by datetime

CREATE TABLE #temp3
	 (SeqNbr INT,
	  DockCode VARCHAR(20)) 

	INSERT INTO #temp2
	SELECT row_number() OVER (ORDER BY SUPPLIER_NAME) AS SeqNbr, SUPPLIER_NAME, route_code , RATE, ARRVDATETIME,  DPTDATETIME, dock_code, 
		   lp_code, PICKUP_DT, PickupMethod, Supplier, SupplierPlant, DateOrder /*add by fid.deny 2015-05-05 for ordering result by datetime*/ FROM (
		SELECT t.SUPPLIER_NAME, t.route_code , t.RATE, CONVERT(TIME,MIN(sm.ARRVDATETIME)) AS ARRVDATETIME,  CONVERT(TIME, MAX(sm.DPTDATETIME)) AS DPTDATETIME, '' AS dock_code, t.lp_code, t.PICKUP_DT,
			CASE 
				WHEN t.lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 1) THEN 'DR'
				WHEN t.lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 2) THEN 'LP' 
				ELSE 'DR' 
			END AS PickupMethod, t.Supplier, t.SupplierPlant, MIN(t.dateorder) as DateOrder /*add by fid.deny 2015-05-05 for ordering result by datetime*/
		FROM #temp t
			INNER JOIN TB_R_SUPP_MEETING_DRIVER_DATA sm ON 
			sm.RTEGRPCD = t.route_code AND sm.RUNSEQ = t.RATE AND sm.LOGPTCD LIKE '807%' AND
			(sm.RTEDATE = @Date1a
				
				OR ((RTEDATE = @Date1b) AND (ISNULL(@Date1b,'') <> ''))
				OR ((RTEDATE = @Date2a) AND (ISNULL(@Date2a,'') <> ''))
				OR ((RTEDATE = @Date2b) AND (ISNULL(@Date2b,'') <> '')))
			WHERE sm.DOCKCD not in ('YD','GT')

				--added fid.deny 2015-05-04
				AND '' = CASE WHEN ISNULL(@SupplierCode, '') <> '' THEN
							RTRIM(LTRIM(sm.DOORCD)) --compare with DOORCD if @suppliercode is not null (it means report by supplier)
						 ELSE
							'' --compare with '' (will return true) if @suppliercode is null (it means report by LP)
						 END
				AND '' <> CASE WHEN ISNULL(@SupplierCode, '') <> '' THEN
							RTRIM(LTRIM(sm.LOGPARTNERCD)) --compare with DOORCD if @suppliercode is not null (it means report by supplier)
						 ELSE
							'a' --compare with 'a' (will return true) if @suppliercode is null (it means report by LP)
						 END
				--end of added fid.deny



		GROUP BY t.SUPPLIER_NAME, t.route_code , t.RATE, t.lp_code, t.PICKUP_DT, t.Supplier, t.SupplierPlant
	) tbl WHERE PickupMethod = 'DR'

	SET @SEQROW = (SELECT COUNT(1) FROM #temp2)

	INSERT INTO #temp2
	SELECT row_number() OVER (ORDER BY SUPPLIER_NAME) + @SEQROW AS SeqNbr, SUPPLIER_NAME, route_code , RATE, ARRVDATETIME,  DPTDATETIME, dock_code, 
		   lp_code, PICKUP_DT, PickupMethod, Supplier, SupplierPlant, DateOrder /*add by fid.deny 2015-05-05 for ordering result by datetime*/ FROM (
		SELECT SUPPLIER_NAME, route_code , RATE, CONVERT(TIME, MIN(ARRVDATETIME)) AS ARRVDATETIME, CONVERT(TIME, MAX(DPTDATETIME)) AS DPTDATETIME, '' AS dock_code, lp_code, PICKUP_DT,
			CASE 
				WHEN lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 1) THEN 'DR' 
				WHEN lp_code IN (SELECT LOG_PARTNER_CD FROM TB_M_LOGISTIC_PARTNER WHERE ACTIVE_FLAG = 2) THEN 'LP'
				ELSE 'DR' 
			END AS PickupMethod, Supplier, SupplierPlant, MIN(dateorder) as DateOrder /*add by fid.deny 2015-05-05 for ordering result by datetime*/
		FROM #temp
		GROUP BY SUPPLIER_NAME, route_code , RATE, lp_code, PICKUP_DT, Supplier, SupplierPlant
	) tbl 
	WHERE PickupMethod = 'LP'

	DECLARE @Max INT,
			@i INT,
			@RouteCode VARCHAR(4),
			@RateCode VARCHAR(2),
			@DockCode VARCHAR(500),
			@Supplier VARCHAR(250),
			@DockCodeX VARCHAR(500),
			@CIndex INT,
			@Max3 INT,
			@i3 INT

	IF ((SELECT ISNULL(@SupplierCode, '')) <> '')
	BEGIN
	
		SELECT @Max = MAX(SeqNbr), @i = 1
		FROM #temp2
		
		WHILE (@i <= @Max)
		BEGIN
			
			SELECT @RouteCode = RouteCode, @RateCode = Rate, @DockCode = DockCode, @Supplier = Supplier + '-' + SupplierPlant
			FROM #temp2 WHERE SeqNbr = @i
			
			DELETE FROM #temp3
			
			INSERT INTO #temp3
			SELECT row_number() OVER (ORDER BY RTEGRPCD, RUNSEQ) AS SeqNbr, RCVCOMPDOCKCD
			FROM TB_R_SUPP_MEETING_ORDER_ASSIGNMENT
			WHERE RTEGRPCD = @RouteCode AND RUNSEQ = @RateCode AND SUPPCD + '-' + SUPPPLANTCD = @Supplier
			
			SELECT @Max3 = MAX(SeqNbr), @i3 = 1
			FROM #temp3
			
			WHILE (@i3 <= @Max3)
			BEGIN
				SELECT @DockCodeX = DockCode FROM #temp2 WHERE SeqNbr = @i
				SELECT @DockCode = DockCode FROM #temp3 WHERE SeqNbr = @i3
				
				IF (@DockCodeX <> '')
				BEGIN
					SELECT @CIndex = CHARINDEX(@DockCode, @DockCodeX);
					
					IF (@CIndex = 0)
					BEGIN
						UPDATE #temp2 SET DockCode = DockCode + ', ' + @DockCode
							WHERE SeqNbr = @i
					END
				END ELSE BEGIN
					UPDATE #temp2 SET DockCode = @DockCode
						WHERE SeqNbr = @i
				END
				
				SET @i3 = @i3 + 1
				
			END
			
			SET @i = @i + 1
			
		END
			
	END ELSE BEGIN
	
		SELECT @Max = MAX(SeqNbr), @i = 1
		FROM #temp
			 
		WHILE (@i <= @Max)
		BEGIN
			SELECT @RouteCode = route_code, @RateCode = RATE, @DockCode = dock_code, @Supplier = SUPPLIER_NAME
			FROM #temp WHERE SeqNbr = @i
			
			SELECT @DockCodeX = DockCode FROM #temp2 WHERE RouteCode = @RouteCode AND Rate = @RateCode AND SupplierName = @Supplier
			
			IF (@DockCodeX <> '')
			BEGIN
				SELECT @CIndex = CHARINDEX(@DockCode, @DockCodeX);
				
				IF (@CIndex = 0)
				BEGIN
					UPDATE #temp2 SET DockCode = DockCode + ', ' + @DockCode
						WHERE RouteCode = @RouteCode AND Rate = @RateCode AND SupplierName = @Supplier
				END
			END ELSE BEGIN
				UPDATE #temp2 SET DockCode = @DockCode
					WHERE RouteCode = @RouteCode AND Rate = @RateCode AND SupplierName = @Supplier
			END
			
			SET @i = @i + 1
			
		END		 
		
	END

--SELECT * FROM #temp2 ORDER BY RouteCode ASC, Rate Asc, Arrival ASC

SELECT * FROM #temp2 ORDER BY RouteCode ASC, Rate Asc, DateOrder ASC
END

