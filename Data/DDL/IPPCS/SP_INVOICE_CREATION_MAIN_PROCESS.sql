
/****** Object:  StoredProcedure [dbo].[SP_INVOICE_CREATION_MAIN_PROCESS]    Script Date: 30/04/2021 17:02:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	11-Des-2020
-- Description	:	Invoice Creation for IPPCS
-- Update	: 
-- =============================================
ALTER PROCEDURE [dbo].[SP_INVOICE_CREATION_MAIN_PROCESS] @USER_ID VARCHAR(20) = 'agan'
	,@RI_V_PROCESSID BIGINT = '202009070019'
	,@RI_V_SUPP_CD VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IsError CHAR(1) = 'N'
	DECLARE @E_CHECK_ERROR CHAR(1) = 'N';
	DECLARE @CURRDT DATE = CAST(GETDATE() AS DATE)
	DECLARE @FUNCTION_ID AS VARCHAR(MAX) = '51002'
		,@MODULE_ID AS VARCHAR(MAX) = '5'
		,@FUNTION_NM AS VARCHAR(MAX) = 'SP Invoice Creation Main Process'
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_INVOICE_CREATION_MAIN_PROCESS'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@query VARCHAR(MAX)
		,@ICSSrvrName NVARCHAR(256)
		,@ICSDBName NVARCHAR(256)
		,@L_V_STATUS_SUPP VARCHAR(10) = 'N'
	DECLARE @RetStep1 INT = 0;
	DECLARE @RetStep2 INT = 0;
	DECLARE @RetStep3 INT = 0;

	SET NOCOUNT ON

	BEGIN TRY
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Invoice Verification Data Upload Process'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @RI_V_PROCESSID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D WITH (NOLOCK)
				WHERE PROCESS_ID = @RI_V_PROCESSID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION + ': Invoice Creation Init'
			,@USER_ID
			,GETDATE()

		/* ------ Checking Multiple Supplier Code ------ */
		/* checking status error */
		SELECT @L_V_STATUS_SUPP = CASE 
				WHEN SUPP_CHECK.INVALID_SUPP > 0
					THEN 'N'
				ELSE 'Y'
				END
		FROM (
			SELECT COUNT(1) INVALID_SUPP
			FROM TB_T_INV_UPLOAD U WITH (NOLOCK)
			WHERE U.PROCESS_ID = @RI_V_PROCESSID
				AND ISNULL(SUPP_CD, '') <> @RI_V_SUPP_CD
			) SUPP_CHECK;

		-- check data consistency
		IF @L_V_STATUS_SUPP = 'Y'
		BEGIN
			PRINT 'check data consistency';
				-- dbms_output.put_line('good');
				-- delete data in temporary
				/* removed deletion because input from tb_t_invoice_upload */
		END
		ELSE
		BEGIN
			-- write log and stop process
			SET @IsError = 'Y';
			SET @E_CHECK_ERROR = 'N';
			SET @PARAM1 = 'There is Any Supplier Code from CSV is not same with Input Parameter';

			EXEC dbo.CommonGetMessage 'MSPT00001ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,@PARAM1

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @RI_V_PROCESSID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D WITH (NOLOCK)
					WHERE PROCESS_ID = @RI_V_PROCESSID
					)
				,'MSPT00001ERR'
				,'ERR'
				,'MSPT00001ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ': Supplier Code Compare'
				,@USER_ID
				,GETDATE()
		END

		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Checking duplication data'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @RI_V_PROCESSID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D WITH (NOLOCK)
				WHERE PROCESS_ID = @RI_V_PROCESSID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION + ': Checking duplication data'
			,@USER_ID
			,GETDATE()

		IF EXISTS (
				SELECT TOP 1 1
				FROM TB_T_INV_UPLOAD WITH (NOLOCK)
				WHERE 1 = 1
					AND PROCESS_ID = @RI_V_PROCESSID
				GROUP BY MAT_DOC_NO
					,MAT_DOC_ITEM
					,COMP_PRICE_CD
					,PROCESS_ID
				HAVING COUNT(1) > 1
				)
		BEGIN
			SET @IsError = 'Y'

			INSERT dbo.TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT D.PROCESS_ID
				,MAX(D.SEQUENCE_NUMBER) + M.SEQ_NO AS SEQ_NO
				,M.MESSAGE_ID
				,M.MESSAGE_TYPE
				,REPLACE(M.MESSAGE_ID + ' : ' + M.MESSAGE_TEXT, '{0}', 'Material Document Number, Material Document Item, and Component Price in line ' + CONVERT(VARCHAR, M.ROW_NUM)) AS [MESSAGE]
				,@LOG_LOCATION AS LOC
				,@USER_ID
				,CURRENT_TIMESTAMP AS ERR_DT
			FROM dbo.TB_R_LOG_D D WITH (NOLOCK)
				,(
					SELECT ROW_NUMBER() OVER (
							ORDER BY T.ROW_NUM
							) AS SEQ_NO
						,T.ROW_NUM
						,MSG.MESSAGE_ID
						,MSG.MESSAGE_TYPE
						,MSG.MESSAGE_TEXT
					FROM (
						SELECT count(*) AS TOTAL
							,a.MAT_DOC_NO
							,a.MAT_DOC_ITEM
							,a.COMP_PRICE_CD
							,MAX(a.ROW_NO) AS ROW_NUM
						FROM dbo.TB_T_INV_UPLOAD a
						WHERE 1 = 1
							AND PROCESS_ID = @RI_V_PROCESSID
						GROUP BY a.MAT_DOC_NO
							,a.MAT_DOC_ITEM
							,a.COMP_PRICE_CD
							,a.PROCESS_ID
						) T
						,dbo.TB_M_MESSAGE MSG
					WHERE T.TOTAL > 1
						AND MSG.MESSAGE_ID = 'MPCS00018ERR'
					) M
			WHERE D.PROCESS_ID = @RI_V_PROCESSID
			GROUP BY D.PROCESS_ID
				,M.ROW_NUM
				,M.SEQ_NO
				,M.MESSAGE_ID
				,M.MESSAGE_TYPE
				,M.MESSAGE_TEXT

			UPDATE A
			SET REMARKS = SUBSTRING(ISNULL(A.REMARKS, '') + '; ' + 'Material Document Number, Material Document Item, and Component Price', 1, 2000)
			FROM TB_T_INV_UPLOAD A
			JOIN (
				SELECT MAT_DOC_NO
					,MAT_DOC_ITEM
					,COMP_PRICE_CD
					,PROCESS_ID
				FROM TB_T_INV_UPLOAD
				WHERE 1 = 1
					AND PROCESS_ID = @RI_V_PROCESSID
				GROUP BY MAT_DOC_NO
					,MAT_DOC_ITEM
					,COMP_PRICE_CD
					,PROCESS_ID
				HAVING COUNT(1) > 1
				) B ON A.PROCESS_ID = B.PROCESS_ID
				AND A.MAT_DOC_NO = B.MAT_DOC_NO
				AND A.MAT_DOC_ITEM = B.MAT_DOC_ITEM
				AND A.COMP_PRICE_CD = B.COMP_PRICE_CD
		END

		/* call function for validation process and set of remark and post data to permanent */
		DECLARE @RESULT VARCHAR(1);

		BEGIN TRY
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Validation and verification data'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @RI_V_PROCESSID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D WITH (NOLOCK)
					WHERE PROCESS_ID = @RI_V_PROCESSID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION + ': Verification data'
				,@USER_ID
				,GETDATE()

			EXEC SP_INVOICE_CREATION_MAIN_VERIFICATION @USER_ID
				,@RI_V_PROCESSID
				,@RI_V_SUPP_CD
				,@RESULT OUTPUT;

		END TRY

		BEGIN CATCH
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Verification Data Process'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @RI_V_PROCESSID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D WITH (NOLOCK)
					WHERE PROCESS_ID = @RI_V_PROCESSID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END CATCH

		PRINT @RESULT

		--* summary *--
		IF @IsError = 'Y' OR @RESULT = 'Y'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Verification Data Process'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @RI_V_PROCESSID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D WITH (NOLOCK)
					WHERE PROCESS_ID = @RI_V_PROCESSID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END ELSE
		BEGIN
			IF EXISTS (
					SELECT TOP 1 1
					FROM TB_T_INVOICE_UPLOAD tx
					WHERE tx.SUPP_CD = @RI_V_SUPP_CD
						AND tx.PROCESS_ID = @RI_V_PROCESSID
						AND LOWER(tx.remarks) = 'correct'
					)
			BEGIN
				UPDATE A
				SET A.LIV_SEND_FLAG = 'Y'
					,A.LIV_SEND_SUPP = 'Y'
				FROM TB_R_GR_IR A
				JOIN TB_T_INVOICE_UPLOAD B ON A.PO_NO = B.PO_NO
					AND A.PO_ITEM_NO = B.PO_ITEM_NO
					AND A.MAT_DOC_NO = B.MAT_DOC_NO
					AND A.COMP_PRICE_CD = B.COMP_PRICE_CD
					AND A.ITEM_NO = B.MAT_DOC_ITEM
					AND A.SUPPLIER_CD = @RI_V_SUPP_CD;
			END

			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Verification Data Process'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @RI_V_PROCESSID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D WITH (NOLOCK)
					WHERE PROCESS_ID = @RI_V_PROCESSID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @RI_V_PROCESSID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D WITH (NOLOCK)
				WHERE PROCESS_ID = @RI_V_PROCESSID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()
	END CATCH
END
