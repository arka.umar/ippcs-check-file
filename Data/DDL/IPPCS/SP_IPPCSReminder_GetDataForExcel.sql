-- =============================================
-- Author:		<Author,,Name>
-- Create date: 2017-05-22
-- Description:	get detail part for excel attachment IPPCS NOTIFICATION
-- =============================================
CREATE PROCEDURE [dbo].[SP_IPPCSReminder_GetDataForExcel]
	@DOCK_CD AS VARCHAR(4)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SUPPLIER_CD, p.SUPPLIER_NAME, m.MANIFEST_NO, 
	m.DOCK_CD, 
	p.PART_NO, CAST(pp.SHORTAGE_QTY AS VARCHAR) AS SHORTAGE_QTY, CAST(pp.DAMAGE_QTY AS VARCHAR) AS DAMAGE_QTY, CAST(pp.MISSPART_QTY AS VARCHAR) MISSPART_QTY FROM TB_R_DAILY_ORDER_MANIFEST M
	INNER JOIN TB_R_DAILY_ORDER_PART P
	ON M.MANIFEST_NO = P.MANIFEST_NO
	INNER JOIN TB_R_PROBLEM_PART PP
	ON M.MANIFEST_NO = PP.MANIFEST_NO
	where pp.SOLVED_FLAG = 0
	and m.dock_cd = @DOCK_CD


END

