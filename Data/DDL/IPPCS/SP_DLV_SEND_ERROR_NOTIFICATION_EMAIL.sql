-- =============================================
-- Author:		FID.Reggy
-- Create date: 2015.08.26
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_DLV_SEND_ERROR_NOTIFICATION_EMAIL]
	
AS
BEGIN
	
	IF OBJECT_ID('tempdb..#TB_T_GR_ERROR') IS NOT NULL 
		DROP TABLE #TB_T_GR_ERROR
	IF OBJECT_ID('tempdb..#TB_T_IR_ERROR') IS NOT NULL 
		DROP TABLE #TB_T_IR_ERROR


	SELECT * INTO #TB_T_GR_ERROR 
	FROM (
		SELECT
			ROW_NUMBER() OVER(ORDER BY CONVERT(DATE, POSTING_DT) DESC) AS [NO],
			DOC_NO,
			P.LOG_PARTNER_NAME,
			HEADER_TEXT AS DELIVERY_NO,
			ERROR_POSTING,
			CONVERT(DATE, POSTING_DT) as POSTING_DT
		FROM TB_T_DLV_SAP_PARAM_H H
			LEFT JOIN TB_M_LOGISTIC_PARTNER P
			ON H.SUPP_CD = P.VENDOR_SAP_ID
		WHERE ERROR_POSTING IS NOT NULL
			  AND DOC_NO LIKE 'CS%'
	)TBL1
	ORDER BY CONVERT(DATE, POSTING_DT) DESC

	SELECT * INTO #TB_T_IR_ERROR 
	FROM (
		SELECT 
			ROW_NUMBER() OVER(ORDER BY DOC_NO) AS [NO],
			DOC_NO,
			P.LOG_PARTNER_NAME,
			REF_NO AS INVOICE_NO,
			ERROR_POSTING,
			CONVERT(DATE, POSTING_DT) as POSTING_DT
		FROM TB_T_DLV_SAP_PARAM_H H
			LEFT JOIN TB_M_LOGISTIC_PARTNER P
			ON H.SUPP_CD = P.VENDOR_SAP_ID
		WHERE ERROR_POSTING IS NOT NULL
			  AND DOC_NO NOT LIKE 'CS%'
			  AND DOC_NO NOT LIKE 'TWX%'
	)TBL2
	ORDER BY INVOICE_NO DESC

	--SELECT * FROM #TB_T_GR_ERROR
	--SELECT * FROM #TB_T_IR_ERROR

	DECLARE @MAIL_PROFILE VARCHAR(100),
			@MAIL_TO VARCHAR(MAX),
			@MAIL_CC VARCHAR(MAX),
			@MAIL_SUBJECT VARCHAR(MAX),
			@MAIL_CONTENT VARCHAR(MAX),
			@FUNCTION_ID VARCHAR(10) = '31407',
			@ROLE_ID VARCHAR(5) = '31'

	SELECT 
		@MAIL_TO = EMAIL_TO,
		@MAIL_CC = EMAIL_CC
	FROM TB_M_RTP_NOTIFICATION_EMAIL
	WHERE FUNCTION_ID = @FUNCTION_ID

	SELECT
		@MAIL_SUBJECT = NOTIFICATION_SUBJECT,
		@MAIL_CONTENT = NOTIFICATION_CONTENT
	FROM TB_M_NOTIFICATION_CONTENT
	WHERE FUNCTION_ID = @FUNCTION_ID
		  AND ROLE_ID = @ROLE_ID

	DECLARE @LP_NAME VARCHAR(MAX),
			@DELIVERY_NO VARCHAR(25),
			@INVOICE_NO VARCHAR(16),
			@INV_POSTING_DT VARCHAR(20),
			@GR_POSTING_DT VARCHAR(20),
			@ERROR_POSTING VARCHAR(MAX),
			@GR_TABLE VARCHAR(MAX) = '<table border="1" style="border-collapse:collapse;">
										<tr style="text-align:center;">
											<th style="padding:5px"><b>LP Code</b></th>
											<th style="padding:5px"><b>Delivery No</b></th>
											<th style="padding:5px"><b>Posting Date</b></th>
											<th style="padding:5px"><b>Detail Error</b></th>
										</tr>',
			@IR_TABLE VARCHAR(MAX) = '<table border="1" style="border-collapse:collapse;">
										<tr style="text-align:center;">
											<th style="padding:5px"><b>LP Code</b></th>
											<th style="padding:5px"><b>Invoice No</b></th>
											<th style="padding:5px"><b>Posting Date</b></th>
											<th style="padding:5px"><b>Detail Error</b></th>
										</tr>',
			@GR_ERROR_DATA_COUNT INT,
			@IR_ERROR_DATA_COUNT INT,
			@COUNTER INT = 1,
			@APPEND_GRID VARCHAR(MAX) = ''
		
	SELECT @GR_ERROR_DATA_COUNT = COUNT(1) FROM #TB_T_GR_ERROR
	
	IF(@GR_ERROR_DATA_COUNT > 0)
	BEGIN
		WHILE (@COUNTER <= @GR_ERROR_DATA_COUNT)
		BEGIN	
			SELECT 
				@LP_NAME = LOG_PARTNER_NAME,
				@DELIVERY_NO = DELIVERY_NO,
				@ERROR_POSTING = ERROR_POSTING,
				@GR_POSTING_DT = CONVERT(VARCHAR, POSTING_DT)
			FROM #TB_T_GR_ERROR 
			WHERE [NO] = @COUNTER
	
			SET @GR_TABLE = @GR_TABLE + '<tr>
											<td style="padding:5px">' + ISNULL(@LP_NAME, 'Not Found') + '</td>
											<td style="padding:5px">' + ISNULL(@DELIVERY_NO, 'Not Found') + '</td>
											<td style="padding:5px;text-align:center">' + ISNULL(@GR_POSTING_DT, 'Not Found') + '</td>
											<td style="padding:5px">' + ISNULL(@ERROR_POSTING, 'Error') + '</td>
										</tr>'
			SET @COUNTER = @COUNTER + 1
		END

		SET @GR_TABLE = @GR_TABLE + '</table>'

		SET @APPEND_GRID = @APPEND_GRID + '<p>GR Data : </p>' + @GR_TABLE + '<br/><br/>'
	END
	SET @COUNTER = 1

	SELECT @IR_ERROR_DATA_COUNT = COUNT(1) FROM #TB_T_IR_ERROR
	IF(@IR_ERROR_DATA_COUNT > 0)
	BEGIN
		WHILE (@COUNTER <= @IR_ERROR_DATA_COUNT)
		BEGIN	
			SELECT 
				@LP_NAME = LOG_PARTNER_NAME,
				@INVOICE_NO = INVOICE_NO,
				@ERROR_POSTING = ERROR_POSTING,
				@INV_POSTING_DT = CONVERT(VARCHAR, POSTING_DT)
			FROM #TB_T_IR_ERROR 
			WHERE [NO] = @COUNTER
	
			SET @IR_TABLE = @IR_TABLE + '<tr>
											<td style="padding:5px">' + ISNULL(@LP_NAME, 'Not Found') + '</td>
											<td style="padding:5px">' + ISNULL(@INVOICE_NO, 'Not Found') + '</td>
											<td style="padding:5px;text-align:center">' + ISNULL(@INV_POSTING_DT, 'Not Found') + '</td>
											<td style="padding:5px">' + ISNULL(@ERROR_POSTING, 'Error') + '</td>
										</tr>'
			SET @COUNTER = @COUNTER + 1
		END

		SET @IR_TABLE = @IR_TABLE + '</table>'
		SET @APPEND_GRID = '<p>IR Data : </p>' + @IR_TABLE + '<br/><br/>' + @APPEND_GRID
	END

	SET @MAIL_CONTENT = REPLACE(@MAIL_CONTENT, '@GRID', CASE WHEN @APPEND_GRID <> '' THEN @APPEND_GRID ELSE 'No Data' END)

	SELECT @MAIL_PROFILE = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE'

	EXEC msdb.dbo.sp_send_dbmail
					@from_address = '"IPPCS Admin" <ippcs-admin@toyota.co.id>',
					@profile_name = @MAIL_PROFILE,
					@recipients = @MAIL_TO,
					@copy_recipients = @MAIL_CC,
					@subject = @MAIL_SUBJECT, 
					@body = @MAIL_CONTENT,
					@body_format = 'HTML'; 
END

