-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2020-09-30
-- Description:	This function to check value of option, NULL, Out of range or TRUE (1 - 6)
-- =============================================
CREATE FUNCTION [dbo].[FN_INVOICE_CHECK_OPTION] (@ri_n_option INT)
RETURNS VARCHAR(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(10)
		,@ro_v_indicator CHAR(1) = ''
		,@Ret CHAR(1) = '1'

	-- Add the T-SQL statements to compute the return value here
	-- check option, is NULL, Out of Range or true
	IF (@ri_n_option IS NULL)
	BEGIN
		SET @ro_v_indicator = 'A';
		SET @Ret = '0';
	END
	ELSE
		IF (@ri_n_option < 1)
			OR (@ri_n_option > 6)
		BEGIN
			SET @ro_v_indicator = 'B';
			SET @Ret = '0';
		END
		ELSE
		BEGIN
			SET @ro_v_indicator = 'X';
			SET @Ret = '1';
		END

	SET @Result = @ro_v_indicator + @Ret;

	-- Return the result of the function
	RETURN @Result
END
