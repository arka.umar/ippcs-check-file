
/****** Object:  StoredProcedure [dbo].[SP_JPO_PrepGRtoICS]    Script Date: 30/04/2021 17:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	28-Sep-2020
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
ALTER PROCEDURE [dbo].[SP_JPO_PrepGRtoICS] @USER_ID VARCHAR(20) = 'AGAN'
	,@PROCESS_ID BIGINT = '202009230021'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MODULE_ID VARCHAR(50) = '4'
		,@FUNCTION_ID VARCHAR(100) = '42005'
		,@IsError CHAR(1) = 'N'
		,@CURRDT DATE = CAST(GETDATE() AS DATE)
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'JPO Preparation GR to ICS'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@RES INT = 0;

	BEGIN TRY
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'JPO Preparation GR to ICS'

		SET @LOG_LOCATION = 'JPO Preparation Init';

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		--* get and check system parameter *--
		DECLARE @g_v_sm_man_gr_lvl VARCHAR(100);
		DECLARE @g_v_sm_ics_system_source VARCHAR(100);
		DECLARE @g_v_sm_default_pack_type VARCHAR(100);
		DECLARE @g_v_sm_po_unlimited_flag VARCHAR(100);
		DECLARE @g_v_sm_mvt_gr VARCHAR(100);

		SELECT @g_v_sm_man_gr_lvl = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'JPO_TO_ICS'
			AND SYSTEM_CD = 'MAN_GR_LVL'

		SELECT @g_v_sm_ics_system_source = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'JPO_TO_ICS'
			AND SYSTEM_CD = 'SYSTEM_SOURCE'

		SELECT @g_v_sm_default_pack_type = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'JPO_TO_ICS'
			AND SYSTEM_CD = 'DEFAULT_PACK_TYPE'

		SELECT @g_v_sm_po_unlimited_flag = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'JPO_TO_ICS'
			AND SYSTEM_CD = 'PO_UNLIMITED_LOCAL_INT_FLAG'

		SELECT @g_v_sm_mvt_gr = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'JPO_TO_ICS'
			AND SYSTEM_CD = 'MVT_GR'

		IF (ISNULL(@g_v_sm_man_gr_lvl, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(MAN_GR_LVL)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@g_v_sm_ics_system_source, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(SYSTEM_SOURCE)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@g_v_sm_default_pack_type, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(DEFAULT_PACK_TYPE)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@g_v_sm_po_unlimited_flag, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(PO_UNLIMITED_LOCAL_INT_FLAG)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@g_v_sm_mvt_gr, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(MVT_GR)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF @IsError = 'N'
		BEGIN
			--* process data from temporary good receive junbiki *--
			CREATE TABLE #lRec_junbiki_loop (
				ROWNO INT
				,DOC_DT DATE
				,QUANTITY NUMERIC(13, 3)
				,PLANT_CD VARCHAR(4)
				,SLOC_CD VARCHAR(6)
				,SOURCE_TYPE VARCHAR(1)
				,PROD_PURPOSE_CD VARCHAR(5)
				,MAT_NO VARCHAR(23)
				,MAT_DESC VARCHAR(40)
				,ITEM_NO BIGINT
				,UNIT_OF_MEASURE_CD VARCHAR(3)
				,RAW_MAT_FLAG VARCHAR(3)
				,REMARK_1 VARCHAR(255)
				,REMARK_2 VARCHAR(255)
				,SUPP_CD VARCHAR(6)
				,DOCK_CD VARCHAR(6)
				,DELIVERY_NOTE VARCHAR(16)
				,CREATED_BY VARCHAR(20)
				,CREATED_DT DATETIME
				)

			CREATE TABLE #TB_T_GOOD_RECEIVE_TEMP (
				PROCESS_KEY VARCHAR(63)
				,PROCESS_ID BIGINT
				,SYSTEM_SOURCE VARCHAR(20)
				,USR_ID VARCHAR(20)
				,SEQ_NO NUMERIC(12)
				,POSTING_DT DATETIME
				,DOC_DT DATETIME
				,PROCESS_DT DATETIME
				,REF_NO VARCHAR(16)
				,MOVEMENT_TYPE VARCHAR(4)
				,MAT_DOC_DESC VARCHAR(255)
				,PROD_PURPOSE_CD VARCHAR(5)
				,SOURCE_TYPE VARCHAR(1)
				,MAT_NO VARCHAR(23)
				,ORI_MAT_NO VARCHAR(23)
				,MAT_DESC VARCHAR(40)
				,POSTING_QUANTITY INT
				,PART_COLOR_SFX VARCHAR(2)
				,PACKING_TYPE VARCHAR(2)
				,UNIT_OF_MEASURE_CD VARCHAR(6)
				,ORI_UNIT_OF_MEASURE_CD VARCHAR(6)
				,BATCH_NO VARCHAR(10)
				,PLANT_CD VARCHAR(4)
				,SLOC_CD VARCHAR(6)
				,DOCK_CD VARCHAR(6)
				,MAT_CURR VARCHAR(3)
				,ACTUAL_EXCHANGE_RATE NUMERIC(7, 2)
				,SUPP_CD VARCHAR(6)
				,ORI_SUPP_CD VARCHAR(6)
				,GR_LEVEL VARCHAR(6)
				,RECEIVE_NO VARCHAR(10)
				,MAT_COMPLETE_FLAG VARCHAR(1)
				,RAW_MATERIAL_FLAG VARCHAR(1)
				,DN_COMPLETE_FLAG VARCHAR(1)
				,OTHER_PROCESS_ID BIGINT
				,COMPLETE_FLAG VARCHAR(1)
				,CREATED_BY VARCHAR(20)
				,CREATED_DT DATETIME
				,IS_COMPLETE INT
				,MAT_FLAG INT
				,PO_FLAG INT
				)

			DECLARE @DOC_DT DATE
				,@QUANTITY NUMERIC(13, 3)
				,@PLANT_CD VARCHAR(4)
				,@SLOC_CD VARCHAR(6)
				,@SOURCE_TYPE VARCHAR(1)
				,@PROD_PURPOSE_CD VARCHAR(5)
				,@MAT_NO VARCHAR(23)
				,@MAT_DESC VARCHAR(40)
				,@ITEM_NO BIGINT
				,@UNIT_OF_MEASURE_CD VARCHAR(3)
				,@RAW_MAT_FLAG VARCHAR(3)
				,@REMARK_1 VARCHAR(255)
				,@REMARK_2 VARCHAR(255)
				,@SUPP_CD VARCHAR(6)
				,@DOCK_CD VARCHAR(6)
				,@DELIVERY_NOTE VARCHAR(16)
				,@CREATED_BY VARCHAR(20)
				,@CREATED_DT DATETIME
				,@PART_COLOR_SFX VARCHAR(10) = ''
				,@RO_PACKING_TYPE VARCHAR(100)
				,@CN INT = 1
				,@CN_ALL INT = 0
				,@RES_MSG VARCHAR(1000)
				,@RES_FLAG VARCHAR(2)
				,@RES_MAT_FLAG INT
				,@RESULT VARCHAR(1000);

			INSERT INTO #lRec_junbiki_loop
			SELECT ROW_NUMBER() OVER (
					ORDER BY (
							SELECT 1
							)
					) NO
				,A.DOC_DT
				,A.QUANTITY
				,A.PLANT_CD
				,A.SLOC_CD
				,A.SOURCE_TYPE
				,A.PROD_PURPOSE_CD
				,A.MAT_NO
				,A.MAT_DESC
				,A.ROW_NO
				,A.UNIT_OF_MEASURE_CD
				,A.RAW_MAT_FLAG
				,A.REMARK_1
				,A.REMARK_2
				,A.SUPP_CD
				,A.DOCK_CD
				,A.DELIVERY_NOTE
				,A.CREATED_BY
				,A.CREATED_DT
			FROM TB_T_GR_JUNBIKI A
			WHERE (A.NEW_PROCESS_ID = @PROCESS_ID OR A.PROCESS_ID = @PROCESS_ID);

			SELECT @CN_ALL = COUNT(1)
			FROM #lRec_junbiki_loop

			WHILE @CN <= @CN_ALL
			BEGIN
				SELECT @DOC_DT = A.DOC_DT
					,@QUANTITY = A.QUANTITY
					,@PLANT_CD = A.PLANT_CD
					,@SLOC_CD = A.SLOC_CD
					,@SOURCE_TYPE = A.SOURCE_TYPE
					,@PROD_PURPOSE_CD = A.PROD_PURPOSE_CD
					,@MAT_NO = A.MAT_NO
					,@MAT_DESC = A.MAT_DESC
					,@ITEM_NO = A.ITEM_NO
					,@UNIT_OF_MEASURE_CD = A.UNIT_OF_MEASURE_CD
					,@RAW_MAT_FLAG = A.RAW_MAT_FLAG
					,@REMARK_1 = A.REMARK_1
					,@REMARK_2 = A.REMARK_2
					,@SUPP_CD = A.SUPP_CD
					,@DOCK_CD = A.DOCK_CD
					,@CREATED_BY = A.CREATED_BY
					,@CREATED_DT = A.CREATED_DT
					,@DELIVERY_NOTE = A.DELIVERY_NOTE
				FROM #lRec_junbiki_loop A
				WHERE ROWNO = @CN;

				SET @RES_MAT_FLAG = 1;

				--* material number conversion *--
				--SELECT @RESULT = [dbo].[FN_JPO_MATERIALNO_CONV](@MAT_NO);
				EXEC SP_JPO_MATERIALNO_CONV @MAT_NO, @RESULT OUTPUT;

				SELECT @RES_MSG = TB.s
				FROM (
					SELECT ROW_NUMBER() OVER (
							ORDER BY (
									SELECT 1
									)
							) NO
						,*
					FROM dbo.fnSplitString(@RESULT, '|')
					) TB
				WHERE NO = 4

				SELECT @RES_FLAG = TB.s
				FROM (
					SELECT ROW_NUMBER() OVER (
							ORDER BY (
									SELECT 1
									)
							) NO
						,*
					FROM dbo.fnSplitString(@RESULT, '|')
					) TB
				WHERE NO = 5

				IF (ISNULL(@RES_FLAG, '0') = '0')
				BEGIN
					SELECT @MAT_NO = TB.s
					FROM (
						SELECT ROW_NUMBER() OVER (
								ORDER BY (
										SELECT 1
										)
								) NO
							,*
						FROM dbo.fnSplitString(@RESULT, '|')
						) TB
					WHERE NO = 1

					SELECT @PART_COLOR_SFX = TB.s
					FROM (
						SELECT ROW_NUMBER() OVER (
								ORDER BY (
										SELECT 1
										)
								) NO
							,*
						FROM dbo.fnSplitString(@RESULT, '|')
						) TB
					WHERE NO = 2
				END
				ELSE
					IF (ISNULL(@RES_FLAG, '0') = '1')
					BEGIN
						SET @PART_COLOR_SFX = '';

						SET @RES_MAT_FLAG = 0;
					END
					ELSE
						IF (ISNULL(@RES_FLAG, '0') = '2')
						BEGIN
							SET @IsError = 'Y';

							SET @RES_MAT_FLAG = 0;

						END

				--* set default packing type *--
				SELECT @RO_PACKING_TYPE = PACKING_TYPE_CD
				FROM TB_M_PACKING_INDICATOR
				WHERE DOCK_CD = @DOCK_CD
					AND MAT_NO = @MAT_NO
					AND PROD_PURPOSE_CD = @PROD_PURPOSE_CD
					AND SOURCE_TYPE = @SOURCE_TYPE
					AND PART_COLOR_SFX = (
						CASE 
							WHEN ISNULL(@PART_COLOR_SFX, '') = ''
								THEN 'X'
							ELSE @PART_COLOR_SFX
							END
						)
					AND @DOC_DT BETWEEN VALID_DT_FR
						AND VALID_DT_TO;

				IF ISNULL(@RO_PACKING_TYPE, '') = ''
				BEGIN
					SET @RO_PACKING_TYPE = @g_v_sm_default_pack_type;
				END

				-- store data into temporary good receive data
				INSERT INTO #TB_T_GOOD_RECEIVE_TEMP (
					PROCESS_KEY
					,PROCESS_ID
					,SYSTEM_SOURCE
					,USR_ID
					,SEQ_NO
					,POSTING_DT
					,DOC_DT
					,PROCESS_DT
					,REF_NO
					,MOVEMENT_TYPE
					,MAT_DOC_DESC
					,PROD_PURPOSE_CD
					,SOURCE_TYPE
					,MAT_NO
					,ORI_MAT_NO
					,MAT_DESC
					,POSTING_QUANTITY
					,PART_COLOR_SFX
					,PACKING_TYPE
					,UNIT_OF_MEASURE_CD
					,ORI_UNIT_OF_MEASURE_CD
					,BATCH_NO
					,PLANT_CD
					,SLOC_CD
					,DOCK_CD
					,MAT_CURR
					,ACTUAL_EXCHANGE_RATE
					,SUPP_CD
					,ORI_SUPP_CD
					,GR_LEVEL
					,RECEIVE_NO
					,MAT_COMPLETE_FLAG
					,RAW_MATERIAL_FLAG
					,DN_COMPLETE_FLAG
					,OTHER_PROCESS_ID
					,COMPLETE_FLAG
					,CREATED_BY
					,CREATED_DT
					,IS_COMPLETE
					,MAT_FLAG
					,PO_FLAG
					)
				--Sys sourece + Ref No + Mat No + Prod Purpose + Source Type
				SELECT @g_v_sm_ics_system_source + RTRIM(@DELIVERY_NOTE) + LEFT(RTRIM(@MAT_NO), 10) + RTRIM(@PROD_PURPOSE_CD) + RTRIM(@SOURCE_TYPE) + CONVERT(VARCHAR, @ITEM_NO) PROCESS_KEY
					,@PROCESS_ID PROCESS_ID
					,@g_v_sm_ics_system_source SYSTEM_SOURCE
					,@USER_ID USR_ID
					,@CN SEQ_NO
					,GETDATE() POSTING_DT
					,@DOC_DT DOC_DT
					,GETDATE() PROCESS_DT
					,RTRIM(@DELIVERY_NOTE) REF_NO
					,RTRIM(@g_v_sm_mvt_gr) MOVEMENT_TYPE
					,'Good Receive JPO' MAT_DOC_DESC
					,RTRIM(@PROD_PURPOSE_CD) PROD_PURPOSE_CD
					,RTRIM(@SOURCE_TYPE) SOURCE_TYPE
					,LEFT(RTRIM(@MAT_NO), 10) MAT_NO
					,RTRIM(@MAT_NO) ORI_MAT_NO
					,RTRIM(@MAT_DESC) MAT_DESC
					,@QUANTITY POSTING_QUANTITY
					,RTRIM(@PART_COLOR_SFX) PART_COLOR_SFX
					,RTRIM(@RO_PACKING_TYPE) PACKING_TYPE
					,RTRIM(@UNIT_OF_MEASURE_CD) UNIT_OF_MEASURE_CD
					,RTRIM(@UNIT_OF_MEASURE_CD) ORI_UNIT_OF_MEASURE_CD
					,'dummy' BATCH_NO
					,@PLANT_CD PLANT_CD
					,@SLOC_CD SLOC_CD
					,@DOCK_CD DOCK_CD
					,'IDR' MAT_CURR
					,'1' ACTUAL_EXCHANGE_RATE
					,@SUPP_CD SUPP_CD
					,@SUPP_CD ORI_SUPP_CD
					,@g_v_sm_man_gr_lvl GR_LEVEL
					,'dummy' RECEIVE_NO
					,'Y' MAT_COMPLETE_FLAG
					,@RAW_MAT_FLAG RAW_MATERIAL_FLAG
					,'Y' DN_COMPLETE_FLAG
					,@PROCESS_ID OTHER_PROCESS_ID
					,'Y' COMPLETE_FLAG
					,@CREATED_BY CREATED_BY
					,@CREATED_DT CREATED_DT
					,@RES_MAT_FLAG IS_COMPLETE
					,@RES_MAT_FLAG
					,1 PO_FLAG

				SET @CN = @CN + 1;
			END

			DECLARE @DOC_TYPE VARCHAR(2);

			SELECT @DOC_TYPE = SYSTEM_VALUE
			FROM TB_M_SYSTEM
			WHERE FUNCTION_ID = 'COMMON_PO'
				AND SYSTEM_CD = 'PO_LOCAL_REGULER_DOC_TYPE'

			--* Validation *--
			--IF (@IsError = 'N')
			--BEGIN
				DECLARE @Param VARCHAR(1000);

				IF EXISTS (
						SELECT TOP 1 1
						FROM #TB_T_GOOD_RECEIVE_TEMP A
						LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE --POH.PO_NO = POD.PO_NO
							AND POH.SUPPLIER_CD = RTRIM(A.ORI_SUPP_CD)
							AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.DOC_DT, 112)
						WHERE ISNULL(POH.PO_NO, '') = ''
						)
				BEGIN
					SET @Iserror = 'Y';
					
					UPDATE A
					SET A.IS_COMPLETE = 0,
						A.PO_FLAG = 0
					FROM #TB_T_GOOD_RECEIVE_TEMP A
					LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE --POH.PO_NO = POD.PO_NO
						AND POH.SUPPLIER_CD = RTRIM(A.ORI_SUPP_CD)
						AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.DOC_DT, 112)
					WHERE ISNULL(POH.PO_NO, '') = ''
				END
			--END

			IF OBJECT_ID('tempdb..#TB_T_GOOD_RECEIVE_CAL') IS NOT NULL
			BEGIN
				DROP TABLE #TB_T_GOOD_RECEIVE_CAL
			END;

			CREATE TABLE #TB_T_GOOD_RECEIVE_CAL (
				ROWNO INT
				,PROCESS_KEY VARCHAR(63)
				,PROCESS_ID BIGINT
				,SYSTEM_SOURCE VARCHAR(20)
				,USR_ID VARCHAR(20)
				,SEQ_NO NUMERIC(12)
				,POSTING_DT DATETIME
				,DOC_DT DATETIME
				,PROCESS_DT DATETIME
				,REF_NO VARCHAR(16)
				,MOVEMENT_TYPE VARCHAR(4)
				,MAT_DOC_DESC VARCHAR(255)
				,PROD_PURPOSE_CD VARCHAR(5)
				,SOURCE_TYPE VARCHAR(1)
				,MAT_NO VARCHAR(23)
				,ORI_MAT_NO VARCHAR(23)
				,MAT_DESC VARCHAR(40)
				,POSTING_QUANTITY INT
				,PART_COLOR_SFX VARCHAR(2)
				,PACKING_TYPE VARCHAR(2)
				,UNIT_OF_MEASURE_CD VARCHAR(6)
				,ORI_UNIT_OF_MEASURE_CD VARCHAR(6)
				,BATCH_NO VARCHAR(20)
				,PLANT_CD VARCHAR(4)
				,SLOC_CD VARCHAR(6)
				,DOCK_CD VARCHAR(6)
				,MAT_CURR VARCHAR(3)
				,ACTUAL_EXCHANGE_RATE NUMERIC(7, 2)
				,SUPP_CD VARCHAR(6)
				,ORI_SUPP_CD VARCHAR(6)
				,PO_NO VARCHAR(10)
				,PO_DOC_TYPE VARCHAR(4)
				,PO_DOC_DT DATETIME
				,PO_ITEM_NO VARCHAR(5)
				,PO_MAT_PRICE NUMERIC(16, 5)
				,PO_CURR VARCHAR(3)
				,PO_EXCHANGE_RATE NUMERIC(13, 2)
				,GR_ORI_AMOUNT NUMERIC(16, 5)
				,GR_LOCAL_AMOUNT NUMERIC(16, 5)
				,PO_UNLIMITED_FLAG VARCHAR(1)
				,PO_TOLERANCE_PERCENTAGE NUMERIC(5, 2)
				,PO_TAX_CD VARCHAR(2)
				,PO_INV_WO_GR_FLAG VARCHAR(1)
				,GR_LEVEL VARCHAR(1)
				,RECEIVE_NO VARCHAR(20)
				,MAT_COMPLETE_FLAG VARCHAR(1)
				,RAW_MATERIAL_FLAG VARCHAR(1)
				,DN_COMPLETE_FLAG VARCHAR(1)
				,OTHER_PROCESS_ID BIGINT
				,COMPLETE_FLAG VARCHAR(1)
				,CREATED_BY VARCHAR(20)
				,CREATED_DT DATETIME
				,INSERT_FLAG INT
				,IS_COMPLETE INT
				,MAT_FLAG INT
				,PO_FLAG INT
				)

			--IF (@IsError = 'N')
			IF EXISTS(SELECT TOP 1 1 FROM #TB_T_GOOD_RECEIVE_TEMP)
			BEGIN
				INSERT INTO #TB_T_GOOD_RECEIVE_CAL (
					ROWNO
					,PROCESS_KEY
					,PROCESS_ID
					,SYSTEM_SOURCE
					,USR_ID
					,SEQ_NO
					,POSTING_DT
					,DOC_DT
					,PROCESS_DT
					,REF_NO
					,MOVEMENT_TYPE
					,MAT_DOC_DESC
					,PROD_PURPOSE_CD
					,SOURCE_TYPE
					,MAT_NO
					,ORI_MAT_NO
					,MAT_DESC
					,POSTING_QUANTITY
					,PART_COLOR_SFX
					,PACKING_TYPE
					,UNIT_OF_MEASURE_CD
					,ORI_UNIT_OF_MEASURE_CD
					,BATCH_NO
					,PLANT_CD
					,SLOC_CD
					,DOCK_CD
					,MAT_CURR
					,ACTUAL_EXCHANGE_RATE
					,SUPP_CD
					,ORI_SUPP_CD
					,PO_NO
					,PO_DOC_TYPE
					,PO_DOC_DT
					,PO_ITEM_NO
					,PO_MAT_PRICE
					,PO_CURR
					,PO_EXCHANGE_RATE
					,GR_ORI_AMOUNT
					,GR_LOCAL_AMOUNT
					,PO_UNLIMITED_FLAG
					,PO_TOLERANCE_PERCENTAGE
					,PO_TAX_CD
					,PO_INV_WO_GR_FLAG
					,GR_LEVEL
					,RECEIVE_NO
					,MAT_COMPLETE_FLAG
					,RAW_MATERIAL_FLAG
					,DN_COMPLETE_FLAG
					,OTHER_PROCESS_ID
					,COMPLETE_FLAG
					,CREATED_BY
					,CREATED_DT
					,INSERT_FLAG
					,IS_COMPLETE
					,MAT_FLAG
					,PO_FLAG
					)
				SELECT ROW_NUMBER() OVER (
						ORDER BY (
								SELECT 1
								)
						) NO
					,A.PROCESS_KEY
					,A.PROCESS_ID
					,A.SYSTEM_SOURCE
					,A.USR_ID
					,A.SEQ_NO
					,A.POSTING_DT
					,A.DOC_DT
					,A.PROCESS_DT
					,A.REF_NO
					,A.MOVEMENT_TYPE
					,A.MAT_DOC_DESC
					,A.PROD_PURPOSE_CD
					,A.SOURCE_TYPE
					,A.MAT_NO
					,A.ORI_MAT_NO
					,A.MAT_DESC
					,A.POSTING_QUANTITY
					,A.PART_COLOR_SFX
					,A.PACKING_TYPE
					,A.UNIT_OF_MEASURE_CD
					,A.ORI_UNIT_OF_MEASURE_CD
					,A.BATCH_NO
					,A.PLANT_CD
					,A.SLOC_CD
					,A.DOCK_CD
					,A.MAT_CURR
					,A.ACTUAL_EXCHANGE_RATE
					,A.SUPP_CD
					,A.ORI_SUPP_CD
					,POH.PO_NO
					,POH.PO_TYPE PO_DOC_TYPE
					,POH.PO_DATE PO_DOC_DT
					,POD.PO_ITEM_NO PO_ITEM_NO
					,POD.PO_MAT_PRICE
					,POH.PO_CURR
					,POH.EXCHANGE_RATE PO_EXCHANGE_RATE
					,POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE GR_ORI_AMOUNT
					,POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE * POH.EXCHANGE_RATE GR_LOCAL_AMOUNT
					,'N' PO_UNLIMITED_FLAG
					,'0.00' PO_TOLERANCE_PERCENTAGE
					,'' PO_TAX_CD
					,'N' PO_INV_WO_GR_FLAG
					,A.GR_LEVEL
					,A.RECEIVE_NO
					,A.MAT_COMPLETE_FLAG
					,A.RAW_MATERIAL_FLAG
					,A.DN_COMPLETE_FLAG
					,A.OTHER_PROCESS_ID
					,A.COMPLETE_FLAG
					,A.CREATED_BY
					,A.CREATED_DT
					,0 INSERT_FLAG
					,A.IS_COMPLETE
					,A.MAT_FLAG
					,A.PO_FLAG
				FROM #TB_T_GOOD_RECEIVE_TEMP A
				LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE --POH.PO_NO = POD.PO_NO
					AND POH.SUPPLIER_CD = RTRIM(ORI_SUPP_CD)
					AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.DOC_DT, 112)
				LEFT JOIN TB_R_PO_ITEM POD ON POH.PO_NO = POD.PO_NO
					AND POD.MAT_NO = LEFT(RTRIM(ORI_MAT_NO), 10)
					AND POD.PROD_PURPOSE_CD = RTRIM(A.PROD_PURPOSE_CD)
					AND POD.SOURCE_TYPE = RTRIM(A.SOURCE_TYPE)
					AND POD.PART_COLOR_SFX = RTRIM(A.PART_COLOR_SFX)
					AND POD.PLANT_CD = A.PLANT_CD
					AND POD.SLOC_CD = A.SLOC_CD
					AND POD.PACKING_TYPE = A.PACKING_TYPE

				DECLARE @PROCESS_ID_PAR BIGINT
					,@PO_NO VARCHAR(10)
					,@PO_ITEM_NO VARCHAR(5)
					,@PACKING_TYPE VARCHAR(2)
					,@POSTING_QUANTITY INT
					,@IS_COMPLETE INT
					,@MAT_FLAG INT
					,@PO_FLAG INT
					,@REF_NO VARCHAR(100)
					,@msgiD VARCHAR(100)

				SET @CN = 1
				SET @CN_ALL = 0

				SELECT @CN_ALL = COUNT(1)
				FROM #TB_T_GOOD_RECEIVE_CAL

				WHILE @CN <= @CN_ALL
				BEGIN
					SELECT @PROCESS_ID_PAR = PROCESS_ID
						,@CREATED_BY = CREATED_BY
						,@PO_NO = PO_NO
						,@PO_ITEM_NO = PO_ITEM_NO
						,@MAT_NO = MAT_NO
						,@SOURCE_TYPE = SOURCE_TYPE
						,@PROD_PURPOSE_CD = PROD_PURPOSE_CD
						,@PACKING_TYPE = PACKING_TYPE
						,@PLANT_CD = PLANT_CD
						,@SLOC_CD = SLOC_CD
						,@DOC_DT = DOC_DT
						,@POSTING_QUANTITY = POSTING_QUANTITY
						,@PART_COLOR_SFX = PART_COLOR_SFX
						,@IS_COMPLETE = IS_COMPLETE
						,@MAT_FLAG = MAT_FLAG
						,@PO_FLAG = PO_FLAG
						,@REF_NO = REF_NO
					FROM #TB_T_GOOD_RECEIVE_CAL
					WHERE ROWNO = @CN

					IF @IS_COMPLETE = 0
					BEGIN
						IF @MAT_FLAG = 0
						BEGIN
							SET @Param = 'Manifest No: ' + @REF_NO + ' and Material No: ' + @MAT_NO + ' not exist in ICS Material Table, process GR is not complete yet';

							EXEC dbo.CommonGetMessage 'MPCS00001INF'
								,@MSG_TXT OUTPUT
								,@N_ERR OUTPUT
								,@Param

							INSERT INTO TB_R_LOG_D (
								PROCESS_ID
								,SEQUENCE_NUMBER
								,MESSAGE_ID
								,MESSAGE_TYPE
								,[MESSAGE]
								,LOCATION
								,CREATED_BY
								,CREATED_DATE
								)
							SELECT @PROCESS_ID
								,(
									SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
									FROM dbo.TB_R_LOG_D
									WHERE PROCESS_ID = @PROCESS_ID
									)
								,'MPCS00001INF'
								,'INF'
								,'MPCS00001INF : ' + @MSG_TXT
								,@LOG_LOCATION
								,@USER_ID
								,GETDATE()
						END 
						
						IF @PO_FLAG = 0
						BEGIN
							SET @Param = 'Manifest No: ' + @REF_NO + ' and Material No: ' + @MAT_NO + ' doesn''t have PO No, process GR is not complete yet';

							EXEC dbo.CommonGetMessage 'MPCS00001INF'
								,@MSG_TXT OUTPUT
								,@N_ERR OUTPUT
								,@Param

							INSERT INTO TB_R_LOG_D (
								PROCESS_ID
								,SEQUENCE_NUMBER
								,MESSAGE_ID
								,MESSAGE_TYPE
								,[MESSAGE]
								,LOCATION
								,CREATED_BY
								,CREATED_DATE
								)
							SELECT @PROCESS_ID
								,(
									SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
									FROM dbo.TB_R_LOG_D
									WHERE PROCESS_ID = @PROCESS_ID
									)
								,'MPCS00001INF'
								,'INF'
								,'MPCS00001INF : ' + @MSG_TXT
								,@LOG_LOCATION
								,@USER_ID
								,GETDATE()
						END
					END ELSE
					BEGIN
						DELETE
						FROM TB_S_RESULT_PO
						WHERE PROCESS_ID = @PROCESS_ID

						SET @Param = 'Processing PO for Manifest No: ' + @REF_NO + ' and Material No: ' + @MAT_NO;

						EXEC dbo.CommonGetMessage 'MPCS00002INF'
							,@MSG_TXT OUTPUT
							,@N_ERR OUTPUT
							,@Param

						INSERT INTO TB_R_LOG_D (
							PROCESS_ID
							,SEQUENCE_NUMBER
							,MESSAGE_ID
							,MESSAGE_TYPE
							,[MESSAGE]
							,LOCATION
							,CREATED_BY
							,CREATED_DATE
							)
						SELECT @PROCESS_ID
							,(
								SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
								FROM dbo.TB_R_LOG_D
								WHERE PROCESS_ID = @PROCESS_ID
								)
							,'MPCS00002INF'
							,'INF'
							,'MPCS00002INF : ' + @MSG_TXT
							,@LOG_LOCATION
							,@USER_ID
							,GETDATE()

						EXEC [dbo].[SP_COMMON_GR_UPDATE_PO] @CREATED_BY
							,@PROCESS_ID_PAR
							,@REF_NO
							,@PO_NO
							,@MAT_NO
							,@SOURCE_TYPE
							,@PROD_PURPOSE_CD
							,@PACKING_TYPE
							,@PLANT_CD
							,@SLOC_CD
							,@DOC_DT
							,@POSTING_QUANTITY
							,@PART_COLOR_SFX
							,''

						IF EXISTS (
								SELECT 1
								FROM TB_S_RESULT_PO
								WHERE Result = 'Y'
									AND PROCESS_ID = @PROCESS_ID
								)
						BEGIN
							SET @Iserror = 'Y';
							SET @msgiD = 'MPCS00123ERR';

							UPDATE A
							SET A.IS_COMPLETE = 0
							FROM #TB_T_GOOD_RECEIVE_CAL A
							WHERE ROWNO = @CN

							--BREAK;
						END
						ELSE
						BEGIN
							SET @msgiD = 'MPCS00122INF';

							DECLARE @PO VARCHAR(20)
								,@POITM VARCHAR(10);

							SELECT @PO = PO_NO
								,@POITM = PO_ITEM_NO
							FROM TB_S_RESULT_PO
							WHERE PROCESS_ID = @PROCESS_ID

							UPDATE A
							SET A.INSERT_FLAG = 1
								,A.PO_NO = ISNULL(@PO, @PO_NO)
								,A.PO_ITEM_NO = ISNULL(@POITM, @PO_ITEM_NO)
							FROM #TB_T_GOOD_RECEIVE_CAL A
							WHERE ROWNO = @CN
						END
						
						EXEC dbo.CommonGetMessage @msgiD
							,@MSG_TXT OUTPUT
							,@N_ERR OUTPUT
							,@Param

						INSERT INTO TB_R_LOG_D (
							PROCESS_ID
							,SEQUENCE_NUMBER
							,MESSAGE_ID
							,MESSAGE_TYPE
							,[MESSAGE]
							,LOCATION
							,CREATED_BY
							,CREATED_DATE
							)
						SELECT @PROCESS_ID
							,(
								SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
								FROM dbo.TB_R_LOG_D
								WHERE PROCESS_ID = @PROCESS_ID
								)
							,@msgiD
							,'INF'
							,@msgiD + ' : ' + @MSG_TXT
							,@LOG_LOCATION
							,@USER_ID
							,GETDATE()
					END

					SET @CN = @CN + 1;
				END

				DELETE
				FROM TB_S_RESULT_PO
				WHERE PROCESS_ID = @PROCESS_ID
			END

			--IF @IsError <> 'Y'
			IF EXISTS(SELECT TOP 1 1 FROM #TB_T_GOOD_RECEIVE_TEMP)
			BEGIN
				--FID.Ridwan: 2020-12-04 add logic comp price cd and rate get from tb r po d
				-- with logic if comp price cd more than 1 record then pivot it.
			
				IF OBJECT_ID('tempdb..#TB_T_PO_D') IS NOT NULL
				BEGIN
					DROP TABLE #TB_T_PO_D
				END;

				CREATE TABLE #TB_T_PO_D (
					PO_NO VARCHAR(50)
					,PO_ITEM VARCHAR(10)
					,COMP_CD_RATE VARCHAR(MAX)
					)

				INSERT INTO #TB_T_PO_D
				SELECT DISTINCT U2.PO_NO
					,U2.PO_ITEM_NO
					,substring((
							SELECT '|' + U1.COMP_PRICE_CD + ';' + CONVERT(VARCHAR, U1.COMP_PRICE_RATE) AS [text()]
							FROM TB_R_PO_D U1
							WHERE u1.PO_NO = u2.PO_NO
								AND u1.PO_ITEM = u2.PO_ITEM_NO
							ORDER BY U1.PO_ITEM
							FOR XML PATH('')
							), 2, 1000)
				FROM #TB_T_GOOD_RECEIVE_CAL U2
				WHERE U2.INSERT_FLAG = 1

				MERGE TB_T_GOOD_RECEIVE AS [TARGET]
				USING (
					SELECT A.PROCESS_KEY
						,A.PROCESS_ID
						,A.SYSTEM_SOURCE
						,A.USR_ID
						,A.SEQ_NO
						,A.POSTING_DT
						,A.DOC_DT
						,A.PROCESS_DT
						,A.REF_NO
						,A.MOVEMENT_TYPE
						,A.MAT_DOC_DESC
						,A.PROD_PURPOSE_CD
						,A.SOURCE_TYPE
						,A.MAT_NO
						,A.ORI_MAT_NO
						,A.MAT_DESC
						,A.POSTING_QUANTITY
						,A.PART_COLOR_SFX
						,A.PACKING_TYPE
						,A.UNIT_OF_MEASURE_CD
						,A.ORI_UNIT_OF_MEASURE_CD
						,A.BATCH_NO
						,A.PLANT_CD
						,A.SLOC_CD
						,A.DOCK_CD
						,A.MAT_CURR
						,A.ACTUAL_EXCHANGE_RATE
						,A.SUPP_CD
						,A.ORI_SUPP_CD
						,POH.PO_NO
						,POH.PO_TYPE PO_DOC_TYPE
						,POH.PO_DATE PO_DOC_DT
						,POD.PO_ITEM_NO PO_ITEM_NO
						,POD.PO_MAT_PRICE
						,POH.PO_CURR
						,POH.EXCHANGE_RATE PO_EXCHANGE_RATE
						,POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE GR_ORI_AMOUNT
						,POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE * POH.EXCHANGE_RATE GR_LOCAL_AMOUNT
						,'N' PO_UNLIMITED_FLAG
						,'0.00' PO_TOLERANCE_PERCENTAGE
						,'' PO_TAX_CD
						,'N' PO_INV_WO_GR_FLAG
						,A.GR_LEVEL
						,A.RECEIVE_NO
						,A.MAT_COMPLETE_FLAG
						,A.RAW_MATERIAL_FLAG
						,A.DN_COMPLETE_FLAG
						,A.OTHER_PROCESS_ID
						,(
							CASE 
								WHEN A.IS_COMPLETE = 0
									THEN 'N'
								ELSE A.COMPLETE_FLAG
								END
							) COMPLETE_FLAG
						,A.CREATED_BY
						,A.CREATED_DT
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 1, 1) COMP_PRICE_CD_1
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 1, 2) COMP_PRICE_AMOUNT_1
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 2, 1) COMP_PRICE_CD_2
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 2, 2) COMP_PRICE_AMOUNT_2
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 3, 1) COMP_PRICE_CD_3
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 3, 2) COMP_PRICE_AMOUNT_3
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 4, 1) COMP_PRICE_CD_4
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 4, 2) COMP_PRICE_AMOUNT_4
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 5, 1) COMP_PRICE_CD_5
						,dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 5, 2) COMP_PRICE_AMOUNT_5
					FROM #TB_T_GOOD_RECEIVE_CAL A
					LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE --POH.PO_NO = POD.PO_NO
						AND POH.SUPPLIER_CD = RTRIM(ORI_SUPP_CD)
						AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.DOC_DT, 112)
					LEFT JOIN TB_R_PO_ITEM POD ON POH.PO_NO = POD.PO_NO
						AND POD.MAT_NO = LEFT(RTRIM(ORI_MAT_NO), 10)
						AND POD.PROD_PURPOSE_CD = RTRIM(A.PROD_PURPOSE_CD)
						AND POD.SOURCE_TYPE = RTRIM(A.SOURCE_TYPE)
						AND POD.PART_COLOR_SFX = RTRIM(A.PART_COLOR_SFX)
						AND POD.PLANT_CD = A.PLANT_CD
						AND POD.SLOC_CD = A.SLOC_CD
						AND POD.PACKING_TYPE = A.PACKING_TYPE
					LEFT JOIN #TB_T_PO_D B ON POD.PO_NO = B.PO_NO
						AND POD.PO_ITEM_NO = B.PO_ITEM
					) AS [SOURCE]
					ON [SOURCE].PROCESS_KEY = [TARGET].PROCESS_KEY
				WHEN MATCHED
					THEN
						UPDATE
						SET [TARGET].PO_NO = [SOURCE].PO_NO
							,[TARGET].PO_DOC_TYPE = [SOURCE].PO_DOC_TYPE
							,[TARGET].PO_DOC_DT = [SOURCE].PO_DOC_DT
							,[TARGET].PO_ITEM_NO = [SOURCE].PO_ITEM_NO
							,[TARGET].PO_MAT_PRICE = [SOURCE].PO_MAT_PRICE
							,[TARGET].PO_CURR = [SOURCE].PO_CURR
							,[TARGET].PO_EXCHANGE_RATE = [SOURCE].PO_EXCHANGE_RATE
							,[TARGET].GR_ORI_AMOUNT = [SOURCE].GR_ORI_AMOUNT
							,[TARGET].GR_LOCAL_AMOUNT = [SOURCE].GR_LOCAL_AMOUNT
							,[TARGET].COMP_PRICE_CD_1 = [SOURCE].COMP_PRICE_CD_1
							,[TARGET].COMP_PRICE_AMOUNT_1 = [SOURCE].COMP_PRICE_AMOUNT_1
							,[TARGET].COMP_PRICE_CD_2 = [SOURCE].COMP_PRICE_CD_2
							,[TARGET].COMP_PRICE_AMOUNT_2 = [SOURCE].COMP_PRICE_AMOUNT_2
							,[TARGET].COMP_PRICE_CD_3 = [SOURCE].COMP_PRICE_CD_3
							,[TARGET].COMP_PRICE_AMOUNT_3 = [SOURCE].COMP_PRICE_AMOUNT_3
							,[TARGET].COMP_PRICE_CD_4 = [SOURCE].COMP_PRICE_CD_4
							,[TARGET].COMP_PRICE_AMOUNT_4 = [SOURCE].COMP_PRICE_AMOUNT_4
							,[TARGET].COMP_PRICE_CD_5 = [SOURCE].COMP_PRICE_CD_5
							,[TARGET].COMP_PRICE_AMOUNT_5 = [SOURCE].COMP_PRICE_AMOUNT_5
							,[TARGET].COMPLETE_FLAG = [SOURCE].COMPLETE_FLAG
							,[TARGET].SEND_FLAG = NULL
							,[TARGET].PROCESS_ID = [SOURCE].PROCESS_ID
							,[TARGET].CHANGED_BY = [SOURCE].CREATED_BY
							,[TARGET].CHANGED_DT = [SOURCE].CREATED_DT
				WHEN NOT MATCHED
					THEN
						INSERT (
							PROCESS_KEY
							,PROCESS_ID
							,SYSTEM_SOURCE
							,USR_ID
							,SEQ_NO
							,POSTING_DT
							,DOC_DT
							,PROCESS_DT
							,REF_NO
							,MOVEMENT_TYPE
							,MAT_DOC_DESC
							,PROD_PURPOSE_CD
							,SOURCE_TYPE
							,MAT_NO
							,ORI_MAT_NO
							,MAT_DESC
							,POSTING_QUANTITY
							,PART_COLOR_SFX
							,PACKING_TYPE
							,UNIT_OF_MEASURE_CD
							,ORI_UNIT_OF_MEASURE_CD
							,BATCH_NO
							,PLANT_CD
							,SLOC_CD
							,DOCK_CD
							,MAT_CURR
							,ACTUAL_EXCHANGE_RATE
							,SUPP_CD
							,ORI_SUPP_CD
							,PO_NO
							,PO_DOC_TYPE
							,PO_DOC_DT
							,PO_ITEM_NO
							,PO_MAT_PRICE
							,PO_CURR
							,PO_EXCHANGE_RATE
							,GR_ORI_AMOUNT
							,GR_LOCAL_AMOUNT
							,PO_UNLIMITED_FLAG
							,PO_TOLERANCE_PERCENTAGE
							,PO_TAX_CD
							,PO_INV_WO_GR_FLAG
							,GR_LEVEL
							,RECEIVE_NO
							,MAT_COMPLETE_FLAG
							,RAW_MATERIAL_FLAG
							,DN_COMPLETE_FLAG
							,OTHER_PROCESS_ID
							,COMPLETE_FLAG
							,CREATED_BY
							,CREATED_DT
							,COMP_PRICE_CD_1
							,COMP_PRICE_AMOUNT_1
							,COMP_PRICE_CD_2
							,COMP_PRICE_AMOUNT_2
							,COMP_PRICE_CD_3
							,COMP_PRICE_AMOUNT_3
							,COMP_PRICE_CD_4
							,COMP_PRICE_AMOUNT_4
							,COMP_PRICE_CD_5
							,COMP_PRICE_AMOUNT_5
							)
						VALUES (
							[SOURCE].PROCESS_KEY
							,[SOURCE].PROCESS_ID
							,[SOURCE].SYSTEM_SOURCE
							,[SOURCE].USR_ID
							,[SOURCE].SEQ_NO
							,[SOURCE].POSTING_DT
							,[SOURCE].DOC_DT
							,[SOURCE].PROCESS_DT
							,[SOURCE].REF_NO
							,[SOURCE].MOVEMENT_TYPE
							,[SOURCE].MAT_DOC_DESC
							,[SOURCE].PROD_PURPOSE_CD
							,[SOURCE].SOURCE_TYPE
							,[SOURCE].MAT_NO
							,[SOURCE].ORI_MAT_NO
							,[SOURCE].MAT_DESC
							,[SOURCE].POSTING_QUANTITY
							,[SOURCE].PART_COLOR_SFX
							,[SOURCE].PACKING_TYPE
							,[SOURCE].UNIT_OF_MEASURE_CD
							,[SOURCE].ORI_UNIT_OF_MEASURE_CD
							,[SOURCE].BATCH_NO
							,[SOURCE].PLANT_CD
							,[SOURCE].SLOC_CD
							,[SOURCE].DOCK_CD
							,[SOURCE].MAT_CURR
							,[SOURCE].ACTUAL_EXCHANGE_RATE
							,[SOURCE].SUPP_CD
							,[SOURCE].ORI_SUPP_CD
							,[SOURCE].PO_NO
							,[SOURCE].PO_DOC_TYPE
							,[SOURCE].PO_DOC_DT
							,[SOURCE].PO_ITEM_NO
							,[SOURCE].PO_MAT_PRICE
							,[SOURCE].PO_CURR
							,[SOURCE].PO_EXCHANGE_RATE
							,[SOURCE].GR_ORI_AMOUNT
							,[SOURCE].GR_LOCAL_AMOUNT
							,[SOURCE].PO_UNLIMITED_FLAG
							,[SOURCE].PO_TOLERANCE_PERCENTAGE
							,[SOURCE].PO_TAX_CD
							,[SOURCE].PO_INV_WO_GR_FLAG
							,[SOURCE].GR_LEVEL
							,[SOURCE].RECEIVE_NO
							,[SOURCE].MAT_COMPLETE_FLAG
							,[SOURCE].RAW_MATERIAL_FLAG
							,[SOURCE].DN_COMPLETE_FLAG
							,[SOURCE].OTHER_PROCESS_ID
							,[SOURCE].COMPLETE_FLAG
							,[SOURCE].CREATED_BY
							,[SOURCE].CREATED_DT
							,[SOURCE].COMP_PRICE_CD_1
							,[SOURCE].COMP_PRICE_AMOUNT_1
							,[SOURCE].COMP_PRICE_CD_2
							,[SOURCE].COMP_PRICE_AMOUNT_2
							,[SOURCE].COMP_PRICE_CD_3
							,[SOURCE].COMP_PRICE_AMOUNT_3
							,[SOURCE].COMP_PRICE_CD_4
							,[SOURCE].COMP_PRICE_AMOUNT_4
							,[SOURCE].COMP_PRICE_CD_5
							,[SOURCE].COMP_PRICE_AMOUNT_5
							);

				IF EXISTS(SELECT TOP 1 1 FROM #TB_T_GOOD_RECEIVE_CAL WHERE INSERT_FLAG = 1)
				BEGIN
					DELETE A
					FROM TB_T_GR_JUNBIKI A
					JOIN #TB_T_GOOD_RECEIVE_CAL B ON A.DELIVERY_NOTE = B.REF_NO
						AND A.SUPP_CD = B.SUPP_CD 
						AND A.DOCK_CD = B.DOCK_CD 
						AND A.PLANT_CD = B.PLANT_CD
						AND A.MAT_NO = B.MAT_NO 
						AND A.SLOC_CD = B.SLOC_CD
					WHERE INSERT_FLAG = 1
				END
			END

			DROP TABLE #lRec_junbiki_loop

			DROP TABLE #TB_T_GOOD_RECEIVE_TEMP
		END

		IF @IsError = 'N'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'JPO Preparation GR to ICS'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			UPDATE TB_R_LOG_H
			SET PROCESS_STATUS = 1
			WHERE PROCESS_ID = @PROCESS_ID

			SET @RES = 0;
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'JPO Preparation GR to ICS'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			UPDATE TB_R_LOG_H
			SET PROCESS_STATUS = 4
			WHERE PROCESS_ID = @PROCESS_ID

			SET @RES = 0;
		END
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		UPDATE TB_R_LOG_H
		SET PROCESS_STATUS = 4
		WHERE PROCESS_ID = @PROCESS_ID

		SET @RES = 1;
	END CATCH
END

SELECT @RES AS RESULT
