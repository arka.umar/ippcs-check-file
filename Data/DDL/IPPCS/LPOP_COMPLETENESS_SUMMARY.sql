CREATE PROCEDURE [dbo].[LPOP_COMPLETENESS_SUMMARY]
AS 
BEGIN
	--History -> FID.Ridwan: 2021-05-27 --> Change dblink with list below
	/*
		from old ICS to IPPCS
		- tb_m_packing_indicator
		- tb_m_material_price
		- tb_m_source_list
		- tb_m_dock_cd

		from old ICS to IFAST
		- tb_r_sloc_data_material
		- tb_m_material
		- tb_r_material_valuation
	*/

	BEGIN TRY
		DECLARE @PROD_MONTH VARCHAR(10)
		DECLARE @PROD_DATE VARCHAR(20)
		DECLARE @SQL NVARCHAR(MAX) = ''
		DECLARE @d DATETIME = GETDATE()
		DECLARE @WEEK INT = 0
		,@ICSSrvrName NVARCHAR(256)
		,@ICSDBName NVARCHAR(256)

		IF OBJECT_ID('tempdb..#lpop_price') IS NOT NULL DROP TABLE #lpop_price
		IF OBJECT_ID('tempdb..#lpop_sloc') IS NOT NULL DROP TABLE #lpop_sloc
		IF OBJECT_ID('tempdb..#lpop_material') IS NOT NULL DROP TABLE #lpop_material
		IF OBJECT_ID('tempdb..#packing_indicator') IS NOT NULL DROP TABLE #packing_indicator
		IF OBJECT_ID('tempdb..#material_price') IS NOT NULL DROP TABLE #material_price
		IF OBJECT_ID('tempdb..#source_list') IS NOT NULL DROP TABLE #source_list
		IF OBJECT_ID('tempdb..#dock_code') IS NOT NULL DROP TABLE #dock_code
		IF OBJECT_ID('tempdb..#sloc_data') IS NOT NULL DROP TABLE #sloc_data
		IF OBJECT_ID('tempdb..#material') IS NOT NULL DROP TABLE #material
		IF OBJECT_ID('tempdb..#material_valuation') IS NOT NULL DROP TABLE #material_valuation

		DECLARE @PROCESS_ID BIGINT -- = CONVERT(BIGINT, REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(30), GETDATE(), 21), '-', ''), ':', ''),' ', ''), '.', ''))
		DECLARE @T AS TABLE (PID BIGINT)
		DECLARE @log VARCHAR(MAX) = '',
				@na VARCHAR(100) = 'LPOP.Completeness',
				@MODULE_ID VARCHAR(4) = '888',
				@FUNCTION_ID VARCHAR(6) = '888999',
				@COUNT INT = 0

		SET @log = 'Completeness Check for Price Started';
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

		SELECT @ICSSrvrName = SYSTEM_VALUE
		FROM [TB_M_SYSTEM]
		WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
			AND SYSTEM_CD = 'LINKED_SERVER';

		SELECT @ICSDBName = SYSTEM_VALUE
		FROM [TB_M_SYSTEM]
		WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
			AND SYSTEM_CD = 'DB_NAME';

		CREATE TABLE #lpop_price (
			SUPPLIER_CD varchar(6),
			SUPPLIER_NAME varchar(max),
			PART_NO varchar(20),
			PART_COLOR_SFX varchar(2),
			PART_NAME varchar(max),
			DOCK_CD varchar(4),
			PROD_PURPOSE_CD varchar(2),
			ORDER_TYPE varchar(2),
			N_VOLUME int,
			N_1_VOLUME int,
			N_2_VOLUME int,
			N_3_VOLUME int,
			PACKING_TYPE varchar(2),
			PC_NO VARCHAR(21),
			PC_RELEASE_STATUS VARCHAR(1),
			PC_VALID_DT_FR DATETIME,
			SOURCE_SUPP_CD VARCHAR(6),
			SOURCE_VALID_DT_FROM DATETIME
		)
		--CREATE INDEX lpop_packing_index ON #lpop_price (PART_NO, DOCK_CD, PART_COLOR_SFX, PROD_PURPOSE_CD, ORDER_TYPE)

		CREATE TABLE #lpop_sloc (
			SUPPLIER_CD varchar(6),
			SUPPLIER_NAME varchar(max),
			PART_NO varchar(20),
			PART_COLOR_SFX varchar(2),
			PART_NAME varchar(max),
			DOCK_CD varchar(4),
			PROD_PURPOSE_CD varchar(2),
			ORDER_TYPE varchar(2),
			N_VOLUME int,
			N_1_VOLUME int,
			N_2_VOLUME int,
			N_3_VOLUME int,
			PLANT_CD VARCHAR(10),
			SLOC_CD VARCHAR(10),
			SLOC_DATA_MATERIAL VARCHAR(25)
		)

		CREATE TABLE #lpop_material (
			SUPPLIER_CD varchar(6),
			SUPPLIER_NAME varchar(max),
			PART_NO varchar(20),
			PART_COLOR_SFX varchar(2),
			PART_NAME varchar(max),
			DOCK_CD varchar(4),
			PROD_PURPOSE_CD varchar(2),
			ORDER_TYPE varchar(2),
			N_VOLUME int,
			N_1_VOLUME int,
			N_2_VOLUME int,
			N_3_VOLUME int,
			PLANT_CD VARCHAR(10),
			MATERIAL VARCHAR(25),
			MATERIAL_VALUATION VARCHAR(25)
		)

		CREATE TABLE #packing_indicator (
			DOCK_CD VARCHAR(4),
			VALID_DT_FR DATETIME,
			VALID_DT_TO DATETIME,
			MAT_NO VARCHAR(20),
			PACKING_TYPE_CD VARCHAR(2),
			PART_COLOR_SFX VARCHAR(2),
			PROD_PURPOSE_CD VARCHAR(2),
			SOURCE_TYPE VARCHAR(2)
		)
		--CREATE INDEX packing_indicator_primary ON #packing_indicator (MAT_NO, DOCK_CD, PART_COLOR_SFX, PROD_PURPOSE_CD, SOURCE_TYPE)

		CREATE TABLE #material_price (
			SUPP_CD varchar(6), 
			MAT_NO varchar(23), 
			APPROVE_STS varchar(1), 
			PC_NO varchar(21), 
			VALID_DT_FR date, 
			VALID_DT_TO date,  
			PACKING_TYPE varchar(1), 
			SOURCE_TYPE varchar(1), 
			PART_COLOR_SFX varchar(2), 
			PROD_PURPOSE_CD varchar(5)
		)

		CREATE TABLE #source_list (
			SUPP_CD varchar(6), 
			MAT_NO varchar(23),
			VALID_DT_FR date, 
			VALID_DT_TO date, 
			SOURCE_TYPE varchar(1), 
			--PART_COLOR_SFX varchar(2), 
			PROD_PURPOSE_CD varchar(5)
		)

		CREATE TABLE #dock_code (
			DOCK_CD varchar(6), 
			PLANT_CD varchar(6), 
			SLOC_CD varchar(6), 
			VALID_DT_FR datetime, 
			VALID_DT_TO datetime
		)

		CREATE TABLE #sloc_data (
			PROD_PURPOSE_CD varchar(3), 
			MAT_NO varchar(25), 
			SOURCE_TYPE varchar(3), 
			PLANT_CD varchar(6), 
			SLOC_CD varchar(6)
		)

		CREATE TABLE #material (
			MAT_NO varchar(25), 
			MAT_DESC varchar(max)
		)

		CREATE TABLE #material_valuation (
			PROD_PURPOSE_CD varchar(3), 
			MAT_NO varchar(25), 
			SOURCE_TYPE varchar(3), 
			PLANT_CD varchar(6)
		)

		SET @log = 'Preparing in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms';
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SELECT @PROD_MONTH = MAX(PACK_MONTH) FROM TB_R_LPOP WHERE VERS = 'F'
		SET @PROD_DATE = CONVERT(VARCHAR(20), CONVERT(DATE, @PROD_MONTH + '01'), 105)

		SET @d = GETDATE()
		--PART_COLOR_SFX DEFAULT IF NULL = '00'
		--PACKING_TYPE DEFAULT IF NULL = '0'

		INSERT INTO #packing_indicator(
			DOCK_CD, 
			VALID_DT_FR, 
			VALID_DT_TO, 
			MAT_NO, 
			PACKING_TYPE_CD, 
			PART_COLOR_SFX, 
			PROD_PURPOSE_CD, 
			SOURCE_TYPE	
		)
		SELECT DOCK_CD
			,VALID_DT_FR
			,VALID_DT_TO
			,MAT_NO
			,PACKING_TYPE_CD
			,PART_COLOR_SFX
			,PROD_PURPOSE_CD
			,SOURCE_TYPE
		FROM tb_m_packing_indicator
		WHERE VALID_DT_FR <= @PROD_DATE
			AND VALID_DT_TO >= @PROD_DATE

		SET @COUNT = @@ROWCOUNT
		SET @log = 'Packing Indicator ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		--PART_COLOR_SUFFIX = X OR =PART_COLOR_SUFFIX

		INSERT INTO #material_price (
			SUPP_CD, 
			MAT_NO, 
			APPROVE_STS, 
			PC_NO, 
			VALID_DT_FR, 
			VALID_DT_TO, 
			PACKING_TYPE, 
			SOURCE_TYPE, 
			PART_COLOR_SFX, 
			PROD_PURPOSE_CD
		)
		SELECT SUPP_CD
			,MAT_NO
			,APPROVE_STS
			,PC_NO
			,VALID_DT_FR
			,VALID_DT_TO
			,PACKING_TYPE
			,SOURCE_TYPE
			,PART_COLOR_SFX
			,PROD_PURPOSE_CD
		FROM tb_m_material_price
		WHERE VALID_DT_FR <= @PROD_DATE
			AND VALID_DT_TO >= @PROD_DATE

		SET @COUNT = @@ROWCOUNT
		SET @log = 'Material Price ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE() 
		INSERT INTO #source_list (
			SUPP_CD, 
			MAT_NO, 
			VALID_DT_FR, 
			VALID_DT_TO, 
			SOURCE_TYPE, 
			--PART_COLOR_SFX, 
			PROD_PURPOSE_CD
		)
		SELECT DISTINCT SUPP_CD
			,MAT_NO
			,VALID_DT_FR
			,VALID_DT_TO
			,SOURCE_TYPE
			,PROD_PURPOSE_CD
		FROM tb_m_source_list
		WHERE VALID_DT_FR <= @PROD_DATE
			AND VALID_DT_TO >= @PROD_DATE

		SET @COUNT = @@ROWCOUNT
		SET @log = 'Source List ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()

		INSERT INTO #dock_code (
			DOCK_CD, 
			PLANT_CD, 
			SLOC_CD, 
			VALID_DT_FR, 
			VALID_DT_TO
		)
		SELECT DOCK_CD
			,PLANT_CD
			,SLOC_CD
			,VALID_DT_FR
			,VALID_DT_TO
		FROM tb_m_dock_cd
		WHERE VALID_DT_FR <= @PROD_DATE
			AND VALID_DT_TO >= @PROD_DATE

		SET @COUNT = @@ROWCOUNT
		SET @log = 'Dock Code ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		SET @SQL = 'SELECT PROD_PURPOSE_CD, MAT_NO, SOURCE_TYPE, PLANT_CD, SLOC_CD FROM OPENQUERY(' + @ICSSrvrName + ',
					''select DISTINCT PROD_PURPOSE_CD, MAT_NO, SOURCE_TYPE, PLANT_CD, SLOC_CD from ' + @ICSDBName + 'tb_r_sloc_data_material where DELETION_FLAG = ''''N''''
					'')'
		INSERT INTO #sloc_data (
			PROD_PURPOSE_CD, 
			MAT_NO, 
			SOURCE_TYPE, 
			PLANT_CD, 
			SLOC_CD
		)
		EXEC sp_executesql @SQL
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Sloc Data Material ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		SET @SQL = 'SELECT MAT_NO, MAT_DESC FROM OPENQUERY(' + @ICSSrvrName + ',
					''select distinct MAT_NO, MAT_DESC from ' + @ICSDBName + 'tb_m_material
					  where DELETION_FLAG = ''''N''''
					'')'
		INSERT INTO #material (
			MAT_NO, 
			MAT_DESC
		)
		EXEC sp_executesql @SQL
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Material ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		SET @SQL = 'SELECT PROD_PURPOSE_CD, MAT_NO, SOURCE_TYPE, PLANT_CD FROM OPENQUERY(' + @ICSSrvrName + ',
					''select distinct PROD_PURPOSE_CD, MAT_NO, SOURCE_TYPE, PLANT_CD from ' + @ICSDBName + 'tb_r_material_valuation 
					  where DELETION_FLAG = ''''N''''
					'')'
		INSERT INTO #material_valuation (
			PROD_PURPOSE_CD, 
			MAT_NO, 
			SOURCE_TYPE, 
			PLANT_CD
		)
		EXEC sp_executesql @SQL
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Material Valuation ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		DECLARE @dockServicePart VARCHAR(20) = '',
				@prodPurposeServicePart VARCHAR(20) = '',
				@generalProdPurpose VARCHAR(20) = ''

		SELECT @dockServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DOCK_SERVICE_PART'
		SELECT @prodPurposeServicePart = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'PROD_PURPOSE_GR_SERVICE_PART'
		SELECT @generalProdPurpose = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '11004' AND SYSTEM_CD = 'PO_PROD_PURPOSE_CD'

		DECLARE @dockServicePartTable AS TABLE (DOCK VARCHAR(2))
		INSERT INTO @dockServicePartTable (DOCK)
		EXEC dbo.SplitString @dockServicePart, ','

		SET @d = GETDATE()
		INSERT INTO #lpop_price (
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_COLOR_SFX,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME 
		)
		SELECT
			lpop.SUPPLIER_CD,
			MAX(LTRIM(RTRIM(REPLACE(lpop.SUPPLIER_NAME, '  ', ' ')))) SUPPLIER_NAME,
			SUBSTRING(lpop.PART_NO, 1, 10) PART_NO,
			SUBSTRING(lpop.PART_NO, 11, 12) PART_COLOR_SFX,
			MAX(LTRIM(RTRIM(REPLACE(lpop.PART_NAME, '  ', ' ')))) PART_NAME,
			lpop.DOCK_CD,
			CASE WHEN (SELECT 1 WHERE lpop.DOCK_CD IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
			THEN @prodPurposeServicePart
			ELSE
				@generalProdPurpose
			END
				as PROD_PURPOSE_CD,
			ISNULL(NULLIF(lpop.ORDER_TYPE, ''), '1') ORDER_TYPE,
			SUM(ISNULL(CONVERT(INT, lpop.N_VOLUME), '')) as N_VOLUME,
			SUM(ISNULL(CONVERT(INT, lpop.N_1_VOLUME), '')) as N_1_VOLUME,
			SUM(ISNULL(CONVERT(INT, lpop.N_2_VOLUME), '')) as N_2_VOLUME,
			SUM(ISNULL(CONVERT(INT, lpop.N_3_VOLUME), '')) as N_3_VOLUME
		FROM TB_R_LPOP lpop
		WHERE lpop.PACK_MONTH = @PROD_MONTH
				AND lpop.VERS = 'F'
				AND lpop.SRC IN ('1', '3')
				AND lpop.SUPPLIER_CD NOT LIKE '807%'
		GROUP BY lpop.SUPPLIER_CD, 
					SUBSTRING(lpop.PART_NO, 1, 10), 
					SUBSTRING(lpop.PART_NO, 11, 12),
					lpop.DOCK_CD,
					ISNULL(NULLIF(lpop.ORDER_TYPE, ''), '1')
		SET @COUNT = @@ROWCOUNT
		SET @log = 'LPOP Price Data ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		INSERT INTO #lpop_sloc (
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME 
		)
		SELECT
			lpop.SUPPLIER_CD,
			MAX(LTRIM(RTRIM(REPLACE(lpop.SUPPLIER_NAME, '  ', ' ')))) SUPPLIER_NAME,
			SUBSTRING(lpop.PART_NO, 1, 10) PART_NO,
			MAX(LTRIM(RTRIM(REPLACE(lpop.PART_NAME, '  ', ' ')))) PART_NAME,
			lpop.DOCK_CD,
			CASE WHEN (SELECT 1 WHERE lpop.DOCK_CD IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
			THEN @prodPurposeServicePart
			ELSE
				@generalProdPurpose
			END
				as PROD_PURPOSE_CD,
			ISNULL(NULLIF(lpop.ORDER_TYPE, ''), '1') ORDER_TYPE,
			SUM(ISNULL(CONVERT(INT, lpop.N_VOLUME), '')) as N_VOLUME,
			SUM(ISNULL(CONVERT(INT, lpop.N_1_VOLUME), '')) as N_1_VOLUME,
			SUM(ISNULL(CONVERT(INT, lpop.N_2_VOLUME), '')) as N_2_VOLUME,
			SUM(ISNULL(CONVERT(INT, lpop.N_3_VOLUME), '')) as N_3_VOLUME
		FROM TB_R_LPOP lpop
		WHERE lpop.PACK_MONTH = @PROD_MONTH
				AND lpop.VERS = 'F'
				AND lpop.SRC IN ('1', '3')
				AND lpop.SUPPLIER_CD NOT LIKE '807%'
		GROUP BY lpop.SUPPLIER_CD, 
					SUBSTRING(lpop.PART_NO, 1, 10),
					lpop.DOCK_CD,
					ISNULL(NULLIF(lpop.ORDER_TYPE, ''), '1')
		SET @COUNT = @@ROWCOUNT
		SET @log = 'LPOP Sloc Data ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET lpop.PACKING_TYPE = ISNULL(p.PACKING_TYPE_CD, 0)
		FROM #lpop_price lpop
			LEFT JOIN #packing_indicator p
				ON p.DOCK_CD = lpop.DOCK_CD
				   AND p.MAT_NO = lpop.PART_NO
				   AND p.PART_COLOR_SFX = lpop.PART_COLOR_SFX 
				   AND p.PROD_PURPOSE_CD = lpop.PROD_PURPOSE_CD 
				   AND p.SOURCE_TYPE = lpop.ORDER_TYPE
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_price left join #packing_indicator ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET lpop.PC_NO = m.PC_NO, 
				lpop.PC_RELEASE_STATUS = m.APPROVE_STS, 
				lpop.PC_VALID_DT_FR = m.VALID_DT_FR
		FROM #lpop_price lpop
			LEFT JOIN #material_price m
				ON m.MAT_NO = lpop.PART_NO
				   AND m.SUPP_CD = lpop.SUPPLIER_CD
				   AND m.PACKING_TYPE = lpop.PACKING_TYPE 
				   AND m.SOURCE_TYPE = lpop.ORDER_TYPE 
				   AND m.PROD_PURPOSE_CD = lpop.PROD_PURPOSE_CD
				   AND m.PART_COLOR_SFX = lpop.PART_COLOR_SFX
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_price left join #Material_Price Exact Color SFX ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET lpop.PC_NO = m.PC_NO, 
				lpop.PC_RELEASE_STATUS = m.APPROVE_STS, 
				lpop.PC_VALID_DT_FR = m.VALID_DT_FR
		FROM #lpop_price lpop
			LEFT JOIN #material_price m
				ON m.MAT_NO = lpop.PART_NO
				   AND m.SUPP_CD = lpop.SUPPLIER_CD
				   AND m.PACKING_TYPE = lpop.PACKING_TYPE 
				   AND m.SOURCE_TYPE = lpop.ORDER_TYPE 
				   AND m.PROD_PURPOSE_CD = lpop.PROD_PURPOSE_CD
				   AND m.PART_COLOR_SFX = 'X'
		WHERE ISNULL(lpop.PC_NO, '') = ''
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_price left join #Material_Price ''X'' Color SFX ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET SOURCE_SUPP_CD = s.SUPP_CD,
				SOURCE_VALID_DT_FROM = s.VALID_DT_FR
		FROM #lpop_price lpop
			LEFT JOIN #source_list s
				ON s.MAT_NO = lpop.PART_NO
				   --AND s.SUPP_CD = lpop.SUPPLIER_CD
				   AND s.SOURCE_TYPE = lpop.ORDER_TYPE 
				   AND s.PROD_PURPOSE_CD = lpop.PROD_PURPOSE_CD
				   --AND s.PART_COLOR_SFX = lpop.PART_COLOR_SFX
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_price left join #Source_List ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		--SET @d = GETDATE()
		--UPDATE lpop
		--	SET SOURCE_SUPP_CD = s.SUPP_CD,
		--		SOURCE_VALID_DT_FROM = s.VALID_DT_FR
		--FROM #lpop_price lpop
		--	LEFT JOIN #source_list s
		--		ON s.MAT_NO = lpop.PART_NO
		--		   --AND s.SUPP_CD = lpop.SUPPLIER_CD
		--		   AND s.SOURCE_TYPE = lpop.ORDER_TYPE 
		--		   AND s.PROD_PURPOSE_CD = lpop.PROD_PURPOSE_CD
		--		   --AND s.PART_COLOR_SFX = 'X'
		--WHERE ISNULL(SOURCE_SUPP_CD, '') = ''
		--SET @COUNT = @@ROWCOUNT
		--SET @log = 'Select #lpop_price left join #Source_List ''X'' Color SFX ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		--INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		----PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET lpop.PLANT_CD = d.PLANT_CD, 
				lpop.SLOC_CD = d.SLOC_CD
		FROM #lpop_sloc lpop
			LEFT JOIN #dock_code d
				ON d.DOCK_CD = lpop.DOCK_CD
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_sloc left join #dock_code ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET lpop.SLOC_DATA_MATERIAL = d.MAT_NO 
		FROM #lpop_sloc lpop
			LEFT JOIN #sloc_data d
				ON d.MAT_NO = lpop.PART_NO
				   AND d.PLANT_CD = lpop.PLANT_CD
				   AND d.SLOC_CD = lpop.SLOC_CD
				   AND d.PROD_PURPOSE_CD = lpop.PROD_PURPOSE_CD
				   AND d.SOURCE_TYPE = lpop.ORDER_TYPE
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_sloc left join #sloc_data ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		INSERT INTO #lpop_material (
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_NAME,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			PLANT_CD
		)
		SELECT 
			SUPPLIER_CD,
			MAX(LTRIM(RTRIM(REPLACE(SUPPLIER_NAME, '  ', ' ')))) SUPPLIER_NAME,
			PART_NO,
			MAX(LTRIM(RTRIM(REPLACE(PART_NAME, '  ', ' ')))) PART_NAME,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			SUM(N_VOLUME),
			SUM(N_1_VOLUME),
			SUM(N_2_VOLUME),
			SUM(N_3_VOLUME),
			PLANT_CD
		FROM #lpop_sloc
		GROUP BY SUPPLIER_CD,
				 PART_NO,
				 PROD_PURPOSE_CD,
				 ORDER_TYPE,
				 PLANT_CD
		SET @COUNT = @@ROWCOUNT
		SET @log = 'LPOP Material Data ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET lpop.MATERIAL = m.MAT_NO
		FROM #lpop_material lpop
			LEFT JOIN #material m
				ON m.MAT_NO = lpop.PART_NO
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_material left join #material ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		UPDATE lpop
			SET lpop.MATERIAL_VALUATION = v.MAT_NO
		FROM #lpop_material lpop
			LEFT JOIN #material_valuation v
				ON v.MAT_NO = lpop.PART_NO
				   AND v.PLANT_CD = lpop.PLANT_CD
				   AND v.PROD_PURPOSE_CD = lpop.PROD_PURPOSE_CD
				   AND v.SOURCE_TYPE = lpop.ORDER_TYPE
		SET @COUNT = @@ROWCOUNT
		SET @log = 'Select #lpop_material left join #material_valuation ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SELECT @WEEK = ISNULL(MAX([WEEK]), 0) FROM TB_R_LPOP_COMPLETENESS_PRICE WHERE PACK_MONTH = @PROD_MONTH
		SET @WEEK = @WEEK + 1

		SET @d = GETDATE()
		INSERT INTO TB_R_LPOP_COMPLETENESS_PRICE (
			PROCESS_ID,
			PACK_MONTH,
			[WEEK],
			CREATED_DT,
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_COLOR_SFX,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			PACKING_TYPE,
			PC_NO,
			PC_RELEASE_STATUS,
			PC_VALID_DT_FR,
			SOURCE_SUPP_CD,
			SOURCE_VALID_DT_FROM,
			OVER_STATUS
		)
		SELECT 
			@PROCESS_ID,
			@PROD_MONTH,
			@WEEK,
			GETDATE(),
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_COLOR_SFX,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			PACKING_TYPE,
			PC_NO,
			PC_RELEASE_STATUS,
			PC_VALID_DT_FR,
			SOURCE_SUPP_CD,
			SOURCE_VALID_DT_FROM,
			CASE WHEN (ISNULL(PACKING_TYPE, '') = '' 
					   OR ISNULL(PC_NO, '') = ''
					   OR ISNULL(PC_RELEASE_STATUS, '') = ''
					   OR ISNULL(PC_VALID_DT_FR, '') = ''
					   OR ISNULL(SOURCE_SUPP_CD, '') = ''
					   OR ISNULL(SOURCE_VALID_DT_FROM, '') = ''
					   OR (ISNULL(SOURCE_SUPP_CD, '') <> '' AND ISNULL(SOURCE_SUPP_CD, '') <> SUPPLIER_CD))
			THEN 'NG' ELSE 'OK' END as OVER_STATUS
		FROM #lpop_price
		SET @COUNT = @@ROWCOUNT
		SET @log = 'INSERT INTO TB_R_LPOP_COMPLETENESS_PRICE ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		INSERT INTO TB_R_LPOP_COMPLETENESS_SLOC (
			PROCESS_ID,
			PACK_MONTH,
			[WEEK],
			CREATED_DT,
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_COLOR_SFX,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			PLANT_CD,
			SLOC_CD,
			SLOC_DATA_MATERIAL,
			OVER_STATUS
		)
		SELECT 
			@PROCESS_ID,
			@PROD_MONTH,
			@WEEK,
			GETDATE(),
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_COLOR_SFX,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			PLANT_CD,
			SLOC_CD,
			SLOC_DATA_MATERIAL,
			CASE WHEN (ISNULL(PLANT_CD, '') = '' 
					   OR ISNULL(SLOC_CD, '') = ''
					   OR ISNULL(SLOC_DATA_MATERIAL, '') = '')
			THEN 'NG' ELSE 'OK' END as OVER_STATUS
		FROM #lpop_sloc
		SET @COUNT = @@ROWCOUNT
		SET @log = 'INSERT INTO TB_R_LPOP_COMPLETENESS_SLOC ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		SET @d = GETDATE()
		INSERT INTO TB_R_LPOP_COMPLETENESS_MATERIAL(
			PROCESS_ID,
			PACK_MONTH,
			[WEEK],
			CREATED_DT,
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_COLOR_SFX,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			PLANT_CD,
			MATERIAL,
			MATERIAL_VALUATION,
			OVER_STATUS
		)
		SELECT 
			@PROCESS_ID,
			@PROD_MONTH,
			@WEEK,
			GETDATE(),
			SUPPLIER_CD,
			SUPPLIER_NAME,
			PART_NO,
			PART_COLOR_SFX,
			PART_NAME,
			DOCK_CD,
			PROD_PURPOSE_CD,
			ORDER_TYPE,
			N_VOLUME,
			N_1_VOLUME,
			N_2_VOLUME,
			N_3_VOLUME,
			PLANT_CD,
			MATERIAL,
			MATERIAL_VALUATION,
			CASE WHEN (ISNULL(PLANT_CD, '') = '' 
					   OR ISNULL(MATERIAL, '') = ''
					   OR ISNULL(MATERIAL_VALUATION, '') = '')
			THEN 'NG' ELSE 'OK' END as OVER_STATUS
		FROM #lpop_material
		SET @COUNT = @@ROWCOUNT
		SET @log = 'INSERT INTO TB_R_LPOP_COMPLETENESS_MATERIAL ' + CONVERT(VARCHAR(MAX), ISNULL(@COUNT, '')) + ' row(s) in ' + CONVERT(VARCHAR(MAX), DATEDIFF(millisecond, @d, GETDATE())) + ' ms'
		INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		--PRINT @log

		--SELECT COUNT(1) FROM TB_R_LPOP_COMPLETENESS_PRICE
		--SELECT COUNT(1) FROM TB_R_LPOP_COMPLETENESS_SLOC
		--SELECT COUNT(1) FROM TB_R_LPOP_COMPLETENESS_MATERIAL
		--SELECT * FROM TB_R_LOG_D where PROCESS_ID = @PROCESS_ID
		--SELECT 'SUCCESS' as MSG, @PROCESS_ID PROCESS_ID
END TRY
BEGIN CATCH
	DELETE FROM TB_R_LPOP_COMPLETENESS_PRICE WHERE PROCESS_ID = @PROCESS_ID
	DELETE FROM TB_R_LPOP_COMPLETENESS_SLOC WHERE PROCESS_ID = @PROCESS_ID
	DELETE FROM TB_R_LPOP_COMPLETENESS_MATERIAL WHERE PROCESS_ID = @PROCESS_ID

	SET @log = ERROR_MESSAGE()
	INSERT @T EXEC dbo.sp_PutLog @log, 'LPOP', @na, @PROCESS_ID OUTPUT, 'MPCS00002ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
END CATCH
END

