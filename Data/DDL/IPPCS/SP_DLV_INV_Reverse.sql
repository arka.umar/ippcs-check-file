CREATE PROCEDURE [dbo].[SP_DLV_INV_Reverse]
@CREATED_BY VARCHAR (25),
@pid BIGINT

AS
BEGIN --modif by agung 25.07.2014
		DECLARE @MODUL_ID INT = 3, @FUNCTION_ID VARCHAR (6) =  '31405';
		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_INV_Reverse'
		DECLARE @step AS VARCHAR(50) = 'Init'
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)
		
		DECLARE @sts_err AS INT


		DECLARE @LP_INV_NO VARCHAR(30), @INV_DT DATE, @LP_CD VARCHAR(4)
		
		-- CREATE LOG
		SET @steps = @na+'.'+@step;
		SET @log = 'Invoice Reserve process starting';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,0;
		
		DECLARE BTMP_CUR_1_REV CURSOR
		FOR
		SELECT LP_INV_NO, INV_DT, LP_CD FROM TB_R_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid
		OPEN BTMP_CUR_1_REV;
		FETCH NEXT FROM BTMP_CUR_1_REV INTO @LP_INV_NO,@INV_DT, @LP_CD;

		WHILE @@FETCH_STATUS = 0
		BEGIN
					BEGIN TRY
						-- UPDATE TB_R_DLV_INV_UPLOAD -----------------------------------------------------------------------------------------
						SET @steps = @na+'.Process';
						SET @log = 'Update table TB_R_DLV_INV_UPLOAD with invoice number '+@LP_INV_NO;
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
						
						UPDATE 
							TB_R_DLV_INV_UPLOAD 
						SET 
							STATUS_CD = '-4',
							REVERSE_FLAG = 1 
						WHERE 
							LP_INV_NO = @LP_INV_NO
							AND
							SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO)
						-----------------------------------------------------------------------------------------------------------------------
						
						-- UPDATE TB_R_DLV_GR_IR
						SET @steps = @na+'.Process';
						SET @log = 'Update table TB_R_DLV_GR_IR with invoice number '+@LP_INV_NO;
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

						UPDATE TB_R_DLV_GR_IR SET STATUS_CD ='-4' 
						WHERE SERVICE_DOC_NO IN (
													SELECT 
														SERVICE_DOC_NO 
													FROM TB_R_DLV_INV_UPLOAD 
													WHERE 
														LP_INV_NO = @LP_INV_NO
														AND 
														SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO)
												)
						------------------------------------------------------------------------------------------------------------------------
						
						-- DELETE FROM TB_R_DLV_INV_D & TB_R_DLV_INV_H -------------------------------------------------------------------------
						--SET @steps = @na+'.Process';
						--SET @log = 'Delete from TB_R_DLV_INV_D with invoice number '+@LP_INV_NO;
						--EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

						--DELETE FROM TB_R_DLV_INV_D WHERE INV_NO IN (SELECT INV_NO FROM TB_R_DLV_INV_H WHERE LP_INV_NO = @LP_INV_NO)
						
						--SET @steps = @na+'.Process';
						--SET @log = 'Delete from TB_R_DLV_INV_H with invoice number '+@LP_INV_NO;
						--EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

						--DELETE FROM TB_R_DLV_INV_H WHERE LP_INV_NO = @LP_INV_NO
						
						------------------------------------------------------------------------------------------------------------------------
						
						-- DELETE FROM TB_R_DLV_FI_TRANS_D & TB_R_DLV_FI_TRANS_H ---------------------------------------------------------------
						--SET @steps = @na+'.Process';
						--SET @log = 'Delete data from table TB_R_DLV_FI_TRANS_D With REFF_NO '+@LP_INV_NO;
						--EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

						--DELETE FROM TB_R_DLV_FI_TRANS_D WHERE DOC_NO IN (SELECT DOC_NO FROM TB_R_DLV_FI_TRANS_H WHERE REFF_NO=@LP_INV_NO)
						
						--SET @steps = @na+'.Process';
						--SET @log = 'Delete data from table TB_R_DLV_FI_TRANS_H With REFF_NO '+@LP_INV_NO;
						--EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
						
						--DELETE FROM TB_R_DLV_FI_TRANS_H WHERE REFF_NO =@LP_INV_NO

						------------------------------------------------------------------------------------------------------------------------
						
						-- DELETE FROM TB_R_DLV_FI_JOURNAL -------------------------------------------------------------------------------------
						--SET @steps = @na+'.Process';
						--SET @log = 'Delete data from table TB_R_DLV_FI_JOURNAL';
						--EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

						--DELETE FROM TB_R_DLV_FI_JOURNAL WHERE  DOC_NO IN (
						--		SELECT DOC_NO FROM TB_R_DLV_FI_TRANS_H WHERE REFF_NO = @LP_INV_NO	
						--)
						------------------------------------------------------------------------------------------------------------------------
						
						-- POSTING INVOICE REVERSED -------------------------------------------------------------------------------------
						SET @steps = @na+'.Process';
						SET @log = 'EXEC invoice reversed';
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;
						
						EXEC SP_DLV_POSTING_INVOICE @CREATED_BY, @pid, 'Y'
						
						----------------------------------------------------------------------------------------------------------------------
						
						SET @sts_err = 0;
					END TRY
					BEGIN CATCH
						DECLARE @ERROR_MESSAGE VARCHAR(MAX)
						SET @ERROR_MESSAGE = ERROR_MESSAGE()
						SET @sts_err = 1;
					END CATCH
					
					IF (@sts_err <> 0) BEGIN
							-- CREATE LOG
							SET @steps = @na+'.Process';
							SET @log = 'Error when reverse data with LP_INV_NO '+@LP_INV_NO;
							EXEC dbo.sp_PutLog @ERROR_MESSAGE, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
							EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
							
							UPDATE 
								TB_R_DLV_INV_UPLOAD 
							SET 
								STATUS_CD = '4',
								REVERSE_FLAG = 0
							WHERE 
								LP_INV_NO = @LP_INV_NO
								AND
								SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO)

							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
							RAISERROR ('Error when updating data',16,1)
							RETURN
					END
					ELSE BEGIN
							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
					END
					
		FETCH NEXT FROM BTMP_CUR_1_REV INTO @LP_INV_NO,@INV_DT, @LP_CD
		END;
		CLOSE BTMP_CUR_1_REV;
		DEALLOCATE BTMP_CUR_1_REV;

		UPDATE TB_R_DLV_INV_UPLOAD SET PROCESS_ID = NULL  WHERE PROCESS_ID = @pid;
		-- CREATE LOG
		SET @steps = @na+'.Finish';
		SET @log = 'Invoice Reverse process finished';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
END

