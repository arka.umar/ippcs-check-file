CREATE FUNCTION [dbo].[ConcateCarCode] (@CAR_CD1 VARCHAR(400), @CAR_CD2 VARCHAR(400))
RETURNS VARCHAR(400)
AS
BEGIN



RETURN STUFF((SELECT DISTINCT ',' + item
            FROM (
				SELECT item FROM [dbo].fnSplit(LTRIM(RTRIM(ISNULL(@CAR_CD1, ''))),',')
				UNION ALL
				SELECT item FROM [dbo].fnSplit(LTRIM(RTRIM(ISNULL(@CAR_CD2, ''))),',')
			) TBL1
            FOR XML PATH('')) ,1,1,'')

END

