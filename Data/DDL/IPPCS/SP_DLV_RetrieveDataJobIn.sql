/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Register job PR retrieve data
*/

CREATE PROCEDURE [dbo].[SP_DLV_RetrieveDataJobIn] 

@PROD_MONTH VARCHAR (7),
@PROD_MONTH_LAST VARCHAR (6),
@CREATED_BY VARCHAR (25)

AS
BEGIN 

	DECLARE @process_status AS INT = 0
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_RetrieveDataJobIn'
	DECLARE @step AS VARCHAR(50) = 'init'
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)

	DECLARE @MODUL_ID VARCHAR(1) = '3';
	DECLARE @FUNCTION_ID VARCHAR(10) = '31102';

	DECLARE @err_message nvarchar(255);

	DECLARE @CHECK_MODULE INT, @CHECK_STS_PR INT, @COUNT_PR INT;

	DECLARE @CURR_DATE VARCHAR(6), @NEXT_MONTH VARCHAR (6);

	SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID IN (@FUNCTION_ID, '31103') AND PROCESS_END_DT IS NULL );
	

	IF (@CHECK_MODULE=0) BEGIN

			SET @CURR_DATE = (SELECT REPLACE(CONVERT (VARCHAR (7), GETDATE(),111),'/',''));
			
			SET @NEXT_MONTH = ( SELECT REPLACE(CONVERT (VARCHAR (7), DATEADD(mm, 1, GETDATE()),111),'/','' ) );

			IF (@CURR_DATE = @PROD_MONTH OR @PROD_MONTH = @NEXT_MONTH) BEGIN

						SET @CHECK_STS_PR = (SELECT COUNT(*) FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH=@PROD_MONTH AND PR_STATUS_FLAG = 'PR5');
						SET @COUNT_PR = (SELECT COUNT(*) FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH = @PROD_MONTH);

						IF ( @COUNT_PR <> 0 AND @CHECK_STS_PR = @COUNT_PR ) BEGIN
								RAISERROR('PR for selected month already released, can not retrieve data.',16,1)
								RETURN
						END
						ELSE BEGIN

									--CREATE LOG
									SET @steps = @na+'.'+@step;
									SET @log = 'EXEC SP_DLV_RetrieveDataJobIn';
									
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
									
									INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
									VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());

									--SELECT 'registry'
										INSERT INTO TB_R_BACKGROUND_TASK_REGISTRY
										VALUES (
										@pid,--ID
										'DeliveryRetrieveDataTaskRuntime',--NAME
										'Background Task Process Retrieve Data ',--DESCRIPTION
										'System',	--SUBMITTER
										'PR Creation Screen',	--FUNCTION_NAME
										'{&quot;productionMonth&quot;:&quot;'+@PROD_MONTH+'&quot;,'+
											'&quot;LastproductionMonth&quot;:&quot;'+@PROD_MONTH_LAST+'&quot;,'+
											'&quot;Username&quot;:&quot;'+@CREATED_BY+'&quot;,'+
											'&quot;ProcessID&quot;:&quot;' +CAST(@pid as varchar)+ '&quot;}',--PARAMETER
											0,	--TYPE
											0,	--STATUS
										--'D:\ATD\IPPCS App\IPPCS\BackgroundManager\DeliveryRetrieveDataTaskRuntime\bin\Debug\DeliveryRetrieveDataTaskRuntime.exe',--COMMAND
										'C:\Background_Task\Tasks\DeliveryRetrieveDataTaskRuntime.exe',--COMMAND
										0,       --START_DATE
										0,		--END_DATE
										
										4,		--PERIODIC_TYPE
										null,		--INTERVAL
										'',		--EXECUTION_DAYS
										'',	--EXECUTION_MONTHS
										6840000 
										)

									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2'
									--, PROCESS_END_DT=GETDATE()
									WHERE PROCESS_ID = @pid;

						END

				END
				ELSE BEGIN

						RAISERROR('Only able to retrieve data for current and next month.',16,1)
						RETURN

				END
	END
	ELSE BEGIN
			SET @err_message = 'Other process is still running by another user.';
			--SET @err_message = 'Same process still runnning with process ID <b>'+ CONVERT (varchar (12), @pid) +'</b>, please  wait until it finish.';
			RAISERROR(@err_message,16,1)
			RETURN
	END
			
	
END;

