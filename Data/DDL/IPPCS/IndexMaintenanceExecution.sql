CREATE PROCEDURE [dbo].[IndexMaintenanceExecution]
AS
BEGIN
DECLARE @T_REINDEX TABLE (
			process_seq INT,
			table_name VARCHAR(100),
			index_name VARCHAR(100),
			index_type VARCHAR(30),
			allocation_unit VARCHAR(30),
			index_level tinyint,
			result VARCHAR(30),
			query VARCHAR(MAX)
		)
DECLARE @i int = 1
DECLARE @max_seq int = 0
DECLARE @REBUILD int = 0,
		@REORGANIZE int = 0,
		@EXCEPTION int = 0,
		@FINAL_STATUS VARCHAR(15) = 'SUCCESS'

DECLARE @table_name VARCHAR(100),
		@index_name VARCHAR(100),
		@index_type VARCHAR(30),
		@allocation_unit VARCHAR(30),
		@index_level tinyint,
		@err_message VARCHAR(MAX),
		@result VARCHAR(30),
		@query NVARCHAR(MAX) = ''

INSERT INTO @T_REINDEX
	(
		process_seq,
		table_name,
		index_name,
		index_type,
		allocation_unit,
		index_level,
		result,
		query
	)
SELECT 
	ROW_NUMBER() OVER(ORDER BY table_name asc),
	table_name,
	index_name,
	index_type,
	allocation_unit,
	index_level,
	result,
	query_result
FROM TB_M_TABLE_STATUS 
WHERE index_type <> 'Heap' 
	  AND allocation_unit = 'IN_ROW_DATA' 
	  AND result IN ('REBUILD', 'REORGANIZE')

SELECT @max_seq = MAX(process_seq) FROM @T_REINDEX

WHILE(@i <= @max_seq)
BEGIN
	SELECT 
		@table_name = table_name,
		@index_name = index_name,
		@index_type = index_type,
		@allocation_unit = allocation_unit,
		@index_level = index_level,
		@result = result,
		@query = query
	FROM @T_REINDEX 
	WHERE process_seq = @i

	BEGIN TRY
		UPDATE TB_M_TABLE_STATUS 
			SET is_locked = 'Y',
				locked_date = GETDATE()
		WHERE table_name = @table_name
			  AND index_name = @index_name
			  AND index_type = @index_type
			  AND allocation_unit = @allocation_unit
			  AND index_level = @index_level

		PRINT @query
		exec sp_executesql @query
	
		UPDATE TB_M_TABLE_STATUS 
			SET is_locked = 'N',
				locked_date = NULL,
				query_result_exec_date = GETDATE(),
				query_result_exec_status = 'SUCCESS'
		WHERE table_name = @table_name
			  AND index_name = @index_name
			  AND index_type = @index_type
			  AND allocation_unit = @allocation_unit
			  AND index_level = @index_level

		IF(@result = 'REBUILD')
			SET @REBUILD = @REBUILD + 1

		IF(@result = 'REORGANIZE')
			SET @REORGANIZE = @REORGANIZE + 1
	END TRY
	BEGIN CATCH
		SET @FINAL_STATUS = 'ERROR'
		SET @err_message = ERROR_MESSAGE()
		UPDATE TB_M_TABLE_STATUS 
			SET is_locked = 'N',
				locked_date = NULL,
				query_result_exec_date = GETDATE(),
				query_result_exec_status = 'ERROR',
				query_result_exec_msg = @err_message
		WHERE table_name = @table_name
			  AND index_name = @index_name
			  AND index_type = @index_type
			  AND allocation_unit = @allocation_unit
			  AND index_level = @index_level

		SET @EXCEPTION = @EXCEPTION + 1
	END CATCH

	SET @i = @i + 1
END

	DECLARE @MAIL_PROFILE VARCHAR(50) = (SELECT TOP 1 SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE')
	DECLARE @MAIL_TO VARCHAR(MAX) = 'cytta.rizka@toyota.co.id;istd.app13@toyota.co.id'
	DECLARE @MAIL_CC VARCHAR(MAX) = 'fid.reggy@toyota.co.id;fid.deny@toyota.co.id'
	DECLARE @MAIL_CONTENT VARCHAR(MAX) = ''
	DECLARE @MAIL_SUBJECT VARCHAR(255) = 'Database ' + DB_NAME() + ' Index Maintenance Summary'

	SET @MAIL_CONTENT = '<div style="font-size:10pt"><p>Database Archieve ' + DB_NAME() + ' summary : </p>
						<br/>
						<table style="border:2px solid white">
							<tr>
								<td>Table Processed</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), (@i - 1)) + '</td>
							</tr>
							<tr>
								<td>Success Rebuild</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @REBUILD) + '</td>
							</tr>
							<tr>
								<td>Success Reorganize</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @REORGANIZE) + '</td>
							</tr>
							<tr>
								<td><strong>Exception</strong></td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @EXCEPTION) + '</td>
							</tr>
						</table>
						<br/>
						<p>With OVERALL STATUS : <span style="color:' + CASE @FINAL_STATUS WHEN 'ERROR' THEN 'red' ELSE 'black' END + 
						'"><strong>' + @FINAL_STATUS + '</strong></span></p>
						<p>Please See Detail in <strong>TB_M_TABLE_STATUS</strong></p>
						'
						
	EXEC msdb.dbo.sp_send_dbmail
					@from_address = '"Index Maintenance" <index-maintenance-master@toyota.co.id>',
					@profile_name = @MAIL_PROFILE,
					@recipients = @MAIL_TO,
					@copy_recipients = @MAIL_CC,
					@subject = @MAIL_SUBJECT, 
					@body = @MAIL_CONTENT,
					@body_format = 'HTML';
END

