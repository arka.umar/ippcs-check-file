-- =============================================
-- Author:		FID.Muhajir
-- Create date: 2019-01-24
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_INTERFACE_SEND_SUB_MANIFEST_CHECK]
	 @SubManifestNo VARCHAR(max),
	 @L_ERR INT OUTPUT
AS
BEGIN
	BEGIN TRY

		IF EXISTS(SELECT 1 FROM TB_R_DAILY_ORDER_MANIFEST WHERE ISNULL(SEND_FLAG,'N') = 'N' AND MANIFEST_NO = @SubManifestNo)
		BEGIN
			SET @L_ERR = 0 ;
		END
		ELSE
		BEGIN
			SET @L_ERR = 1 ;
		END

	

	END TRY
	BEGIN CATCH
		SET @L_ERR = 1;
	END CATCH

	SELECT @L_ERR;
	--SELECT @MSG_TYPE + '|' + @MSG_TXT AS TEXT
END

