-- =============================================
-- Author:		fid.deny
-- Create date: 16 dec 2015
-- Description:	Send email when LPOP Release (triggered by button on screen LPOP Consolidation)
-- =============================================
CREATE PROCEDURE [dbo].[SP_LPOP_RELEASE_NOTIFICATION]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE
		@functionID VARCHAR(5) = '11005',
		@contentID VARCHAR(2) = '35',
		@contentIDPCD VARCHAR(2) = '99',
		@recipientCode VARCHAR(4) = 'cc01',
		--@latestGroupID VARCHAR(10),
		@notificationSubject VARCHAR(MAX) = '',
		@notificationContent VARCHAR(MAX) = '',
		@notificationContentVar VARCHAR(MAX) = '',
		@PCD_CONTENT VARCHAR(MAX) = '',
		@ccEmail VARCHAR(MAX) = '',
		@email VARCHAR(MAX) = '',
		@EOF INT = 0, 
		@pointer INT = 0,
		@param VARCHAR(6) = '';

	DECLARE @supplierCode VARCHAR(100) = '', 
			@supplierPlant VARCHAR(3) = '';

	--get PROD_MONTH as parameter 
	SELECT @param = SYSTEM_VALUE FROM TB_M_SYSTEM
	WHERE FUNCTION_ID = '11003' AND SYSTEM_CD = 'PROD_MONTH'

	DECLARE	@paramDate DATE = CAST(@param + '01' AS DATE);
	DECLARE @paramDateN1 DATE = DATEADD(MONTH, 1, @paramDate),
			@paramDateN3 DATE = DATEADD(MONTH, 3, @paramDate);
	DECLARE	@monthYear VARCHAR(8) = RIGHT(CONVERT(varchar,@paramDate,106), 8),
			@monthYearConcat VARCHAR(19) = RIGHT(CONVERT(varchar,@paramDateN1,106), 8) + ' - ' + RIGHT(CONVERT(varchar,@paramDateN3,106), 8);

	
	--get supplier and plant
	SELECT ROW_NUMBER() OVER (
		ORDER BY NTABLE.supplierCode
		) AS ID
	,*
	INTO #suppPlant
	FROM (
		SELECT a.supplier_cd AS 'supplierCode'
			,a.SUPPLIER_PLANT AS 'supplierplant'
		FROM TB_R_MONTHLY_FORECAST a
		INNER JOIN TB_M_SUPPLIER_EMAIL b ON a.SUPPLIER_CD = b.SUPPLIER_CD
		WHERE PRODUCTION_MONTH = @param
		GROUP BY a.SUPPLIER_CD
			,a.SUPPLIER_PLANT
		) NTABLE

	--insert supplier_cd for PCD
	INSERT INTO #suppPlant VALUES((SELECT ISNULL(MAX(ID), 0) + 1 FROM #suppPlant), '9999', '0');

	--get last group id
	--SELECT @latestGroupID = MAX(GROUP_ID) + 1 FROM TB_R_NOTIFICATION

	--get subject and content of email
	SELECT  @notificationSubject = NOTIFICATION_SUBJECT,
        @notificationContent = NOTIFICATION_CONTENT
	FROM    TB_M_NOTIFICATION_CONTENT
	WHERE   FUNCTION_ID = @functionID
			AND ROLE_ID = @contentID
			AND NOTIFICATION_METHOD = '1'

	--get subject and content of email for PCD
	SELECT	
			@PCD_CONTENT = NOTIFICATION_CONTENT
	FROM TB_M_NOTIFICATION_CONTENT
	WHERE FUNCTION_ID = @functionID 
		  AND ROLE_ID = @contentIDPCD 
		  AND NOTIFICATION_METHOD = '1'

	--get email for cc
	SELECT DISTINCT @ccEmail = SUBSTRING((
			SELECT ';' + E2.EMAIL_ADDRESS AS [text()]
			FROM TB_T_EMAIL_RECIPIENT E2
			WHERE E2.RECIPIENT_CD = @recipientCode
			FOR XML PATH('')
			), 2, 1000)
	FROM TB_T_EMAIL_RECIPIENT E1
	WHERE E1.RECIPIENT_CD = @recipientCode
	
	--replacing some parameter in template
	SET @PCD_CONTENT = REPLACE(@PCD_CONTENT, '@production_month', @monthYear);
	SET @PCD_CONTENT = REPLACE(@PCD_CONTENT, '@forecastvol', @monthYearConcat)
	SET @PCD_CONTENT = REPLACE(@PCD_CONTENT, ' <b>Deadline</b> for supplier to give feedback is till @date. ', '');

	SET @notificationContent = REPLACE(@notificationContent, '@production_month', @monthYear);
	SET @notificationContent = REPLACE(@notificationContent, '@forecastvol', @monthYearConcat);
	
	SELECT @pointer = MIN(ID), @EOF = MAX(ID) FROM #suppPlant
	
	WHILE (@pointer <= @EOF)
	BEGIN
		SELECT
			@supplierCode = supplierCode,
			@supplierPlant = supplierplant
		FROM #suppPlant	
		WHERE ID = @pointer

		
		IF(@supplierCode != '9999')
		BEGIN
			SET @notificationContentVar = '';
			SET @notificationContentVar = REPLACE(@notificationContent, '@suppCode', @supplierCode);
			SET @notificationContentVar = REPLACE(@notificationContentVar, '@plantcd', @supplierPlant);
		END

		SET @email = (SELECT DISTINCT SUBSTRING(
											(
												SELECT ';'+ E2.EMAIL AS [text()]
												FROM 
													TB_M_SUPPLIER_EMAIL E2
												WHERE E1.SUPPLIER_CD = E2.SUPPLIER_CD
												FOR XML PATH ('')				
											), 2, 1000
			
										  ) EMAILADD
			FROM 
				TB_M_SUPPLIER_EMAIL E1
			WHERE 
				E1.SUPPLIER_CD = @supplierCode AND
				E1.AREA_CD = 1)


		--send email 
		
		IF(ISNULL(@email, '')!='')
		BEGIN
			
			IF(@supplierCode != '9999')
			BEGIN
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'IPPCS NOTIFICATION',
				@body = @notificationContentVar,
				@body_format = 'HTML',
				@recipients = @email,
				@copy_recipients = @ccEmail,
				@subject = @notificationSubject;
			END
			ELSE
			BEGIN
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'IPPCS NOTIFICATION',
				@body = @PCD_CONTENT,
				@body_format = 'HTML',
				@recipients = @email,
				@copy_recipients = @ccEmail,
				@subject = @notificationSubject;
			END
			
		END
		
	
		SET @pointer = @pointer + 1; --set pointer to next

	END

	

	IF OBJECT_ID('tempdb..#suppPlant') IS NOT NULL 
		DROP TABLE #suppPlant
END

