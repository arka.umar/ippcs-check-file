-- =============================================
-- Author:		<niit.suwito>
-- Create date: <07-10-2013>
-- Description:	<download report counter receiving,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Rep_Counter_Receiving]
        @CerfId varchar (50),
        @SupCD varchar (50),
        @SupName varchar (100),
        @InvNo varchar (50),
        @Curr varchar (10),
        @InvAmount bigint,
        @InvTaxNo varchar (50),
        @InvTaxAmount bigint,
		@SubmitDate datetime,
		@SubmitBy varchar(50),
        @Status varchar (20)
AS
BEGIN
	Declare @sql nvarchar(max)
	
	SET @sql = 'select
				HR.CERTIFICATE_ID CertificateID,
				HR.SUPP_CD SupplierCode,
				Sup.SUPP_NAME SupplierName,
				HR.SUPP_INV_NO InvoiceNo,
				parsename(convert(varchar(50),CAST(HR.INV_AMT_TOTAL as money),3),2) InvoiceAmount,
				convert(varchar(max),HR.INV_TAX_NO) InvoiceTaxNo,
				parsename(convert(varchar(50),CAST(HR.INV_TAX_AMT as money),3),2) InvoiceTaxAmount,
				HR.CREATED_DT ReceiveDate,
				HR.SUBMIT_DT SubmitDate,
				HR.SUBMIT_BY SubmitBy,
				HR.CURR_CD Currency,
				HR.STATUS_CD Status
				from TB_H_INV_RECEIVING HR
				inner join TB_M_SUPPLIER_ICS Sup
				on Sup.SUPP_CD = HR.SUPP_CD
				where 1 =1'
		    
		    if(@CerfId<>'0') 
			set @sql = @sql + 'and HR.CERTIFICATE_ID = '''+@CerfId+''''
			
			if(@SupCD <> '')
			set @sql = @sql + 'and HR.SUPP_CD = '''+@SupCD+''''
			
			if(@SupName<>'0') 
			set @sql = @sql + 'and Sup.SUPP_NAME = '''+@SupName+''''
			 
			if(@InvNo<>'0') 
			set @sql = @sql + 'and HR.SUPP_INV_NO = '''+@InvNo+''''
			
			if(@InvTaxNo<>'0') 
			set @sql = @sql + 'and HR.INV_TAX_NO = '''+@InvTaxNo+''''
			
			if(@Curr <> '')
			set @sql = @sql + 'and HR.CURR_CD = '''+@Curr+''''
			
			if(@InvAmount <> 0)
			set @sql = @sql + 'and HR.INV_AMT_TOTAL = '+@InvAmount
			
			if(@InvTaxAmount <> 0 )
			set @sql = @sql + 'and HR.INV_TAX_AMT = '+@InvTaxAmount
			
			--if(@SubmitDate <> '0')
			--set @sql = @sql + 'and HR.SUBMIT_DT = '''+@SubmitDate+''''
			
			if(@SubmitBy <>'0') 
			set @sql = @sql + 'and HR.SUBMIT_BY = '''+@SubmitBy+''''
			
			if(@Status='Accepted')			
			set @sql = @sql + 'and HR.STATUS_CD = 3'
			
			if(@Status='Rejected')			
			set @sql = @sql + 'and HR.STATUS_CD = -2'
			
			set @sql = @sql + ' order by HR.CREATED_DT desc'
			
	  EXECUTE (@sql) 
END

