-- =============================================================================
-- Author: FID.Goldy
-- Create date: 30 October 2013
-- Description:	GR IR Synchronization with ICS
-- Changes List: 
-- 2014.01.20 wot.daniel 
--            tidy log message, in step 6 : status updated into + 4 all done sync 
-- 2014.01.30 wot.daniel	
--            exchange all COUNT query by @@ROWCOUNT usage, 
--			  change condition in step 2 group code 3 condition, 
--            fix sync table read ,
--            on Demand feature add filter supplier code and REFF_NO 
--            add lock to prevent multiple consecutive execution 
-- 2014.04.24 wot.daniel	
--            use ROWID in TB_T_GR_IR reflect ROWID in TB_R_PCS_GR_IR_SYNC 
--            for faster update in step 6, updated in step 2 
-- 2014.04.25 wot.daniel
--            added group_cd = 4 for 2014.04.01 batch supplier 
-- 2019.12.18 wot.ayu
--            change condition in step 4 untuk LIV_SEND_SUPP and LIV_SEND_FLAG
-- 2020.09.11 FID.Ridwan 
--			  change db link from ics (oracle) to new ics (sql server)
--			  get source type from system master 
-- =============================================================================
CREATE PROCEDURE [dbo].[SP_GRIR_SYNC_FROM_ICS_BATCH]
    @suppCd VARCHAR(6) = '',   -- SUPP_CD filter in step 2
    @reffNos VARCHAR(MAX) = '' -- GR_IR REFF_NO separated by semicolon (;) used in step 2 

AS
BEGIN TRY
    DECLARE 
        @pid BIGINT = 0,
        @uid VARCHAR(20) = 'system',

        @na1 VARCHAR(50)= 'SP_GRIR_SYNC_FROM_ICS_BATCH : STEP 1 (DELETE TB_T_GR_IR IPPCS)', 
        @na2 VARCHAR(50)= 'SP_GRIR_SYNC_FROM_ICS_BATCH : STEP 2 (GET DATA GRIR FROM ICS)',
        @na3 VARCHAR(50)= 'SP_GRIR_SYNC_FROM_ICS_BATCH : STEP 3 (DELETE GR IR WHERE STATUS 2 IPPCS)',
        @na4 VARCHAR(50)= 'SP_GRIR_SYNC_FROM_ICS_BATCH : STEP 4 (UPDATE TB_R_GR_IR IPPCS)',
        @na5 VARCHAR(50)= 'SP_GRIR_SYNC_FROM_ICS_BATCH : STEP 5 (INSERT TB_R_GR_IR IPPCS)',
        @na6 VARCHAR(50)= 'SP_GRIR_SYNC_FROM_ICS_BATCH : STEP 6 (UPDATE GR IR SYNC  ICS)',
        @na7 VARCHAR(50)= 'SP_GRIR_SYNC_FROM_ICS_BATCH : ERROR OCCURED',

        @log VARCHAR(2000),

        @module VARCHAR(10) = '5',
        @function VARCHAR(10) = '50001',
		@n INT = 0      ,   
		@query VARCHAR(MAX),
		@ICSSrvrName NVARCHAR(256),
		@ICSDBName NVARCHAR(256),
		@SourceType NVARCHAR(256)
		;
		    
	SELECT @ICSSrvrName = SYSTEM_VALUE FROM [TB_M_SYSTEM] 
	WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
		AND SYSTEM_CD = 'LINKED_SERVER';
		
	SELECT @ICSDBName = SYSTEM_VALUE FROM [TB_M_SYSTEM] 
	WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
		AND SYSTEM_CD = 'DB_NAME';
		
	SELECT @SourceType = SYSTEM_VALUE FROM [TB_M_SYSTEM] 
	WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
		AND SYSTEM_CD = 'SYSTEM_SOURCE';

    DECLARE @px TABLE ( PROCESS_ID BIGINT); 	
    SET NOCOUNT ON;

    SET @log = 'START SP_GRIR_SYNC_FROM_ICS_BATCH'; 
    IF (nullif(@suppCd,'') is not null or nullif(@reffnos, '') is not null)
	BEGIN
		SET @log = @log + ' ' + ISNULL(@suppCd, '''''') + ', ' + ISNULL(@reffNos, ''''''); 
	END;
    INSERT @px 
    EXEC sp_PutLog @log, @uid, 'SP_GRIR_SYNC_FROM_ICS_BATCH', @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;

    PRINT @pid;
    
    --ADDED PRINT PARAMETER--
	PRINT 'SUPP CD'
    PRINT @suppCd
	PRINT 'REFF NO'
    PRINT @reffNos
    
    IF (EXISTS (
		SELECT TOP 1 LOCK_REFF FROM TB_T_LOCK
		WHERE FUNCTION_NO = @function 
		)) 
    BEGIN
		SET @log = 'Previous sync process ' 
			+ ISNULL( 
				(SELECT TOP 1 CONVERT(VARCHAR, LOCK_REFF) 
				FROM TB_T_LOCK 
				WHERE FUNCTION_NO = @function), '') 
			+ ' in progress';
		RAISERROR(@log, 15, 1);

    END;
    
    INSERT TB_T_LOCK (FUNCTION_NO, LOCK_REFF, CREATED_BY, CREATED_DT) 
    VALUES (@function, CONVERT(VARCHAR, @pid), @uid, GETDATE());
	    
-- =============================================
-- Section: DELETE TB_T_GR_IR
-- Description:	STEP 1 OF SYNCHRONIZATION (DELETE TB_T_GR_IR at IPPCS)
-- =============================================
    DELETE FROM TB_T_GR_IR;
    DBCC CHECKIDENT(TB_T_GR_IR, RESEED, 1); 
	    
    SET @log = isnull(convert(varchar, @@ROWCOUNT ),'0') + ' records sucessfully deleted from TB_T_GR_IR.';
    INSERT @px 
    EXEC sp_PutLog @log, @uid, @na1, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;
    
-- =============================================
-- Section: STEP 2 
-- Description:	GET DATA GRIR FROM ICS
-- =============================================
    
    SET @query = '
		SELECT 
			g.PO_NO, 
			g.PO_ITEM_NO, 
			g.MAT_DOC_NO, 
			g.MAT_DOC_YEAR, 
			g.ITEM_NO, 
			g.COMP_PRICE_CD,
			g.DOC_DT,
			g.SPECIAL_STOCK_TYPE,
			g.GR_IR_AMT,
			g.SUPP_CD,
			g.INVENTORY_FLAG,
			g.ACCRUAL_POSTING_FLAG,
			g.REFF_NO,
			g.CONDITION_CATEGORY,
			g.PO_DETAIL_PRICE,
			g.INV_NO,
			g.INV_DOC_ITEM,
			g.LIV_SEND_FLAG,
			g.LIV_SPECIAL_TYPE,
			g.LIV_SPECIAL_MAT_NO,
			g.INV_WO_GR_FLAG,
			g.DOCK_CD,
			g.KANBAN_ORDER_NO,
			g.DN_COMPLETE_FLAG,
			g.MAT_NO,
			g.SOURCE_TYPE,
			g.PROD_PURPOSE_CD,
			g.MAT_DESC,
			g.PLANT_CD,
			g.SLOC_CD,
			g.UOM_CD,
			g.CANCEL_STS,
			g.PART_COLOR_SFX,
			g.ORI_MAT_NO,
			g.BATCH_NO,
			g.ITEM_CURR,
			g.REFF_CANCEL_MAT_DOC_NO,
			g.REFF_CANCEL_MAT_DOC_YEAR,
			g.REFF_CANCEL_MAT_DOC_ITEM,
			g.MOVEMENT_QUANTITY,
			g.PACKING_TYPE,
			g.TAX_CD,
			g.LIV_SEND_SUPP,
			g.STATUS_CD,
			0 STATUS_ICS,
			--g.STATUS_ICS,
			g.CREATED_BY,
			g.CREATED_DT,
			g.CHANGED_BY,
			g.CHANGED_DT,
			g.IF_METHOD,
			g.IF_DT
			--g.ROWID, 
			--g.RNO 
		FROM 
		OPENQUERY(' + @ICSSrvrName + ' , ''
		SELECT 
			g.PO_NO, 
			g.PO_ITEM_NO, 
			g.MAT_DOC_NO, 
			g.MAT_DOC_YEAR, 
			g.ITEM_NO, 
			g.COMP_PRICE_CD,
			g.DOC_DT,
			g.SPECIAL_STOCK_TYPE,
			g.GR_IR_AMT,
			g.SUPP_CD,
			g.INVENTORY_FLAG,
			g.ACCRUAL_POSTING_FLAG,
			g.REFF_NO,
			g.CONDITION_CATEGORY,
			g.PO_DETAIL_PRICE,
			g.INV_NO,
			g.INV_DOC_ITEM,
			g.LIV_SEND_FLAG,
			g.LIV_SPECIAL_TYPE,
			g.LIV_SPECIAL_MAT_NO,
			g.INV_WO_GR_FLAG,
			g.DOCK_CD,
			g.KANBAN_ORDER_NO,
			g.DN_COMPLETE_FLAG,
			g.MAT_NO,
			g.SOURCE_TYPE,
			g.PROD_PURPOSE_CD,
			g.MAT_DESC,
			g.PLANT_CD,
			g.SLOC_CD,
			g.UNIT_OF_MEASURE_CD UOM_CD,
			g.CANCEL_STS,
			g.PART_COLOR_SFX,
			g.ORI_MAT_NO,
			g.BATCH_NO,
			g.ITEM_CURR,
			g.REFF_CANCEL_MAT_DOC_NO,
			g.REFF_CANCEL_MAT_DOC_YEAR,
			g.REFF_CANCEL_MAT_DOC_ITEM,
			g.MOVEMENT_QUANTITY,
			g.PACKING_TYPE,
			g.TAX_CD,
			g.LIV_SEND_SUPP,
			''''1'''' STATUS_CD,
			--S.STATUS STATUS_ICS,
			g.CREATED_BY,
			g.CREATED_DT,
			g.CHANGED_BY,
			g.CHANGED_DT,
			''''SSIS'''' IF_METHOD,
			GETDATE() IF_DT
			--S.ROWID, 
			--ROWNUM AS RNO
		FROM ' + @ICSDBName + 'TB_R_GR_IR g 
		--JOIN ' + @ICSDBName + 'TB_R_PCS_GR_IR_SYNC s 
		--ON 
		--	g.PO_NO = s.PO_NO and
		--	g.PO_ITEM_NO = s.PO_ITEM_NO and
		--	g.MAT_DOC_YEAR = s.MAT_DOC_YEAR and
		--	g.MAT_DOC_NO = s.MAT_DOC_NO and
		--	g.ITEM_NO = s.ITEM_NO and
		--	g.COMP_PRICE_CD = s.COMP_PRICE_CD
		WHERE 1 = 1
			-- FID.Ridwan => get source type from sysmaster
			AND g.SOURCE_TYPE in (' + @SourceType + ')
			--and s.STATUS in (0, 1 ,2)
			and CONVERT(VARCHAR, g.DOC_DT, 112) >= ''''20131031''''
			and g.INV_NO is NULL
			AND g.LIV_SEND_FLAG = ''''N''''
		'') AS g
		LEFT JOIN TB_T_SUPPLIER_EXCLUDE e ON E.SUPPLIER_CD = g.SUPP_CD 
		WHERE 
			  --(  (e.GROUP_CD = ''1'')
			  --OR (e.GROUP_CD = ''2'' AND DATEDIFF(DAY, CONVERT(DATETIME, ''20131201'', 112), g.DOC_DT) >= 0)
		   --   OR (e.GROUP_CD = ''3'' AND DATEDIFF(DAY, CONVERT(DATETIME, ''20140201'', 112), g.DOC_DT) >= 0)
		   --   OR (e.GROUP_CD = ''4'' AND DATEDIFF(DAY, CONVERT(DATETIME, ''20140401'', 112), g.DOC_DT) >= 0)
		   --   )
		--AND 
		(g.REFF_NO not in (SELECT MANIFEST_NO from TB_T_GR_IR_EXCLUDE))
		AND (NULLIF (''' + @suppCd + ''', '''') IS NULL 
			 OR g.SUPP_CD = ''' + @suppCd + ''')
		AND (NULLIF (''' + @reffNos + ''', '''') IS NULL
			OR g.REFF_NO IN (SELECT item as REFF_NO FROM dbo.fnSplit(''' + @reffNos + ''', '';'') ))'
		;

		INSERT INTO TB_T_GR_IR
        (
          PO_NO
          , PO_ITEM_NO
          , MAT_DOC_NO
          , MAT_DOC_YEAR
          , ITEM_NO
          , COMP_PRICE_CD
          , DOC_DT
          , SPECIAL_STOCK_TYPE
          , GR_IR_AMT
          , SUPPLIER_CD
          , INVENTORY_FLAG
          , ACTUAL_POSTING_FLAG
          , INV_REF_NO
          , CONDITION_CATEGORY
          , PO_DETAIL_PRICE
          , INV_NO
          , INV_ITEM_NO
          , LIV_SEND_FLAG
          , LIV_SPECIAL_TYPE
          , LIV_SPECIAL_MAT_NO
          , INV_WO_GR_FLAG
          , DOCK_CD
          , KANBAN_ORDER_NO
          , DN_COMPLETE_FLAG
          , ICS_MAT_NO
          , SOURCE_TYPE_CD
          , PROD_PURPOSE_CD
          , MAT_DESC
          , PLANT_CD
          , SLOC_CD
          , UOM_CD
          , CANCEL_FLAG
          , PART_COLOR_SFX
          , PART_NO
          , BATCH_NO
          , ITEM_CURR
          , REFF_CANCEL_MAT_DOC_NO
          , REFF_CANCEL_MAT_DOC_YEAR
          , REFF_CANCEL_MAT_DOC_ITEM
          , MOVEMENT_QTY
          , PACKING_TYPE
          , TAX_CD
          , LIV_SEND_SUPP
          , STATUS_CD
          , STATUS_ICS
          , CREATED_BY
          , CREATED_DT
          , CHANGED_BY
          , CHANGED_DT
          , IF_METHOD
          , IF_DT
          --, ROWID
          --, RNO
        )
	EXEC(@query);

    --SELECT @n = @@ROWCOUNT;
	SELECT @n = COUNT(1) FROM TB_T_GR_IR
    IF @n < 1
    BEGIN
		SET @log = 'no new GR IR';
		RAISERROR(@log, 15, 2); 
    END;

    SET @log = isnull(convert(varchar, @n),'0') + ' records sucessfully inserted into TB_T_GR_IR.';
    INSERT @px
    EXEC sp_PutLog @log, @uid, @na2, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;


-- =============================================
-- Section: STEP 3 
-- Description:	DELETE GR IR WHERE STATUS 2 IPPCS
-- =============================================	
    
    DELETE TB_R_GR_IR 
      FROM TB_R_GR_IR AS t1  
      JOIN (
			SELECT PO_NO, PO_ITEM_NO, MAT_DOC_NO, MAT_DOC_YEAR, ITEM_NO, COMP_PRICE_CD 
			FROM TB_T_GR_IR 
			WHERE STATUS_ICS = '2'
			GROUP BY PO_NO, PO_ITEM_NO, MAT_DOC_NO, MAT_DOC_YEAR, ITEM_NO, COMP_PRICE_CD 
		) t2 
        ON 
            t1.PO_NO = t2.PO_NO and 
            t1.PO_ITEM_NO = t2.PO_ITEM_NO and 
            t1.MAT_DOC_NO = t2.MAT_DOC_NO and 
            t1.MAT_DOC_YEAR = t2.MAT_DOC_YEAR and 
            t1.ITEM_NO = t2.ITEM_NO and 
            t1.COMP_PRICE_CD = t2.COMP_PRICE_CD 
      
    
    SET @log = isnull(convert(varchar,@@ROWCOUNT),'0') + ' records sucessfully deleted from TB_R_GR_IR.';
    INSERT @px
    EXEC sp_PutLog @log, @uid, @na3, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;
    
-- =============================================
-- Section: STEP 4 
-- Description:	UPDATE GR IR IPPCS
-- =============================================	
        
    update R
        set 
        R.SUPPLIER_CD = T.SUPPLIER_CD,
        R.PART_NO = T.PART_NO,
        R.ICS_MAT_NO = T.ICS_MAT_NO,
        R.PROD_PURPOSE_CD = T.PROD_PURPOSE_CD,
        R.SOURCE_TYPE_CD = T.SOURCE_TYPE_CD,
        R.PACKING_TYPE = T.PACKING_TYPE,
        R.PART_COLOR_SFX = T.PART_COLOR_SFX,
        R.MAT_DESC = T.MAT_DESC,
        R.INV_REF_NO = T.INV_REF_NO,
        R.DOC_DT = T.DOC_DT,
        R.MOVEMENT_QTY = T.MOVEMENT_QTY,
        R.KANBAN_ORDER_NO = T.KANBAN_ORDER_NO,
        R.DOCK_CD = T.DOCK_CD,
        R.INV_NO = T.INV_NO,
        R.INV_ITEM_NO = T.INV_ITEM_NO,
        R.PO_DETAIL_PRICE = T.PO_DETAIL_PRICE,
        R.ITEM_CURR = T.ITEM_CURR,
        R.GR_IR_AMT = T.GR_IR_AMT,
        R.INV_WO_GR_FLAG = T.INV_WO_GR_FLAG,
		--R.LIV_SEND_FLAG = T.LIV_SEND_FLAG,
        --R.LIV_SEND_FLAG = CASE WHEN (R.STATUS_CD = 0 or R.STATUS_CD = 1) THEN 'N' ELSE T.LIV_SEND_FLAG END, -- modification ayu 20191211
		R.LIV_SEND_FLAG = 'N',
        R.CANCEL_FLAG = T.CANCEL_FLAG,
        R.SPECIAL_STOCK_TYPE = T.SPECIAL_STOCK_TYPE,
        R.PLANT_CD = T.PLANT_CD,
        R.SLOC_CD = T.SLOC_CD,
        R.UOM_CD = T.UOM_CD,
        R.BATCH_NO = T.BATCH_NO,
        R.TAX_CD = T.TAX_CD,
        R.REFF_CANCEL_MAT_DOC_NO = T.REFF_CANCEL_MAT_DOC_NO,
        R.REFF_CANCEL_MAT_DOC_YEAR = T.REFF_CANCEL_MAT_DOC_YEAR,
        R.REFF_CANCEL_MAT_DOC_ITEM = T.REFF_CANCEL_MAT_DOC_ITEM,
        R.LIV_SPECIAL_TYPE = T.LIV_SPECIAL_TYPE,
        R.LIV_SPECIAL_MAT_NO = T.LIV_SPECIAL_MAT_NO,       
	   --  R.LIV_SEND_SUPP = T.LIV_SEND_SUPP,
	    R.LIV_SEND_SUPP = CASE WHEN (R.STATUS_CD = 0 or R.STATUS_CD = 1)  THEN 'N' ELSE T.LIV_SEND_SUPP END, -- modification ayu 20191211
        R.CONDITION_CATEGORY = T.CONDITION_CATEGORY,
        R.INVENTORY_FLAG = T.INVENTORY_FLAG,
        R.ACTUAL_POSTING_FLAG = T.ACTUAL_POSTING_FLAG,
        R.DN_COMPLETE_FLAG = T.DN_COMPLETE_FLAG,
        R.CHANGED_BY = T.CHANGED_BY,
        R.CHANGED_DT = T.CHANGED_DT,
		
         --R.STATUS_CD = T.STATUS_CD,
		  R.STATUS_CD = CASE WHEN ISNULL(R.STATUS_CD , 0) IN (1,-1,-2,-3,-4) THEN T.STATUS_CD ELSE R.STATUS_CD END, 

        R.IF_METHOD = T.IF_METHOD ,
         R.IF_DT = T.IF_DT 
    FROM 
    (
			SELECT tt.*, ROW_NUMBER() OVER (
				PARTITION BY tt.PO_NO, tt.PO_ITEM_NO, tt.MAT_DOC_NO, tt.MAT_DOC_YEAR, tt.ITEM_NO, tt.COMP_PRICE_CD
				ORDER BY tt.RNO desc 
				) RX 
			FROM dbo.TB_T_GR_IR tt
			WHERE (STATUS_ICS = 1 or STATUS_ICS = 0)
    ) T
    JOIN dbo.TB_R_GR_IR R 
      ON T.PO_NO =  R.PO_NO 
     AND T.PO_ITEM_NO = R.PO_ITEM_NO 
     AND T.MAT_DOC_NO = R.MAT_DOC_NO 
     AND T.MAT_DOC_YEAR = R.MAT_DOC_YEAR 
     AND T.ITEM_NO =  R.ITEM_NO 
     AND T.COMP_PRICE_CD = R.COMP_PRICE_CD
	WHERE t.RX = 1; 
	    
    SET @log = isnull(convert(varchar,@@ROWCOUNT),'0') + ' records sucessfully updated to TB_R_GR_IR.';
    INSERT @px
    EXEC sp_PutLog @log, @uid, @na4, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;

-- =============================================
-- Section: STEP 5 
-- Description:	INSERT GR IR IPPCS
-- =============================================	

    INSERT INTO TB_R_GR_IR 
        (
        PO_NO,
        PO_ITEM_NO,
        MAT_DOC_NO,
        MAT_DOC_YEAR,
        ITEM_NO,
        DOC_DT,
        SPECIAL_STOCK_TYPE,
        GR_IR_AMT,
        SUPPLIER_CD,
        COMP_PRICE_CD,
        INVENTORY_FLAG,
        ACTUAL_POSTING_FLAG,
        INV_REF_NO,
        CONDITION_CATEGORY,
        PO_DETAIL_PRICE,
        INV_NO,
        INV_ITEM_NO,
        LIV_SEND_FLAG,
        LIV_SPECIAL_TYPE,
        LIV_SPECIAL_MAT_NO,
        INV_WO_GR_FLAG,
        DOCK_CD,
        KANBAN_ORDER_NO,
        DN_COMPLETE_FLAG,
        ICS_MAT_NO,
        SOURCE_TYPE_CD,
        PROD_PURPOSE_CD,
        MAT_DESC,
        PLANT_CD,
        SLOC_CD,
        UOM_CD,
        CANCEL_FLAG,
        PART_COLOR_SFX,
        PART_NO,
        BATCH_NO,
        ITEM_CURR,
        REFF_CANCEL_MAT_DOC_NO,
        REFF_CANCEL_MAT_DOC_YEAR,
        REFF_CANCEL_MAT_DOC_ITEM,
        MOVEMENT_QTY,
        PACKING_TYPE,
        TAX_CD,
        LIV_SEND_SUPP,
        STATUS_CD,
        IF_METHOD,
        IF_DT,
        CREATED_BY,
        CREATED_DT,
        CHANGED_BY,
        CHANGED_DT
        )
    SELECT   
        T.PO_NO ,--PO_NO,
        T.PO_ITEM_NO, --PO_ITEM_NO,
        T.MAT_DOC_NO,--MAT_DOC_NO,
        T.MAT_DOC_YEAR, --MAT_DOC_YEAR,
        T.ITEM_NO, --ITEM_NO,
        T.DOC_DT, --DOC_DT,
        T.SPECIAL_STOCK_TYPE, --SPECIAL_STOCK_TYPE,
        T.GR_IR_AMT,--GR_IR_AMT,
        T.SUPPLIER_CD,--SUPPLIER_CD,
        T.COMP_PRICE_CD,--COMP_PRICE_CD,
        T.INVENTORY_FLAG,--INVENTORY_FLAG,
        T.ACTUAL_POSTING_FLAG,--ACTUAL_POSTING_FLAG,
        T.INV_REF_NO,--INV_REF_NO,
        T.CONDITION_CATEGORY, --CONDITION_CATEGORY,
        T.PO_DETAIL_PRICE,--PO_DETAIL_PRICE,
        T.INV_NO,--INV_NO,
        T.INV_ITEM_NO,--INV_ITEM_NO,
        --T.LIV_SEND_FLAG,--LIV_SEND_FLAG,
		'N' LIV_SEND_FLAG,
        T.LIV_SPECIAL_TYPE,--LIV_SPECIAL_TYPE,
        T.LIV_SPECIAL_MAT_NO,--LIV_SPECIAL_MAT_NO,
        T.INV_WO_GR_FLAG,--INV_WO_GR_FLAG,
        T.DOCK_CD,--DOCK_CD,
        T.KANBAN_ORDER_NO,--KANBAN_ORDER_NO,
        T.DN_COMPLETE_FLAG,--DN_COMPLETE_FLAG,
        T.ICS_MAT_NO,--ICS_MAT_NO,
        T.SOURCE_TYPE_CD,--SOURCE_TYPE_CD,
        T.PROD_PURPOSE_CD,--PROD_PURPOSE_CD,
        T.MAT_DESC,--MAT_DESC,
        T.PLANT_CD,--PLANT_CD,
        T.SLOC_CD,--SLOC_CD,
        T.UOM_CD,--UOM_CD,
        T.CANCEL_FLAG,--CANCEL_FLAG,
        T.PART_COLOR_SFX,--PART_COLOR_SFX,
        T.PART_NO,--PART_NO,
        T.BATCH_NO,--BATCH_NO,
        T.ITEM_CURR,--ITEM_CURR,
        T.REFF_CANCEL_MAT_DOC_NO,--REFF_CANCEL_MAT_DOC_NO,
        T.REFF_CANCEL_MAT_DOC_YEAR,--REFF_CANCEL_MAT_DOC_YEAR,
        T.REFF_CANCEL_MAT_DOC_ITEM,--REFF_CANCEL_MAT_DOC_ITEM,
        T.MOVEMENT_QTY,--MOVEMENT_QTY,
        T.PACKING_TYPE,--PACKING_TYPE,
        T.TAX_CD,--TAX_CD,
        T.LIV_SEND_SUPP,--LIV_SEND_SUPP,
        T.STATUS_CD,--STATUS_CD,
        T.IF_METHOD,--IF_METHOD,
        T.IF_DT,--IF_DT,
        T.CREATED_BY,--CREATED_BY,
        T.CREATED_DT,--CREATED_DT,
        T.CHANGED_BY,--CHANGED_BY,
        T.CHANGED_DT--CHANGED_DT	
    FROM     (
			SELECT tt.*, ROW_NUMBER() OVER (
				PARTITION BY tt.PO_NO, tt.PO_ITEM_NO, tt.MAT_DOC_NO, tt.MAT_DOC_YEAR, tt.ITEM_NO, tt.COMP_PRICE_CD
				ORDER BY tt.RNO desc 
				) RX 
			FROM dbo.TB_T_GR_IR tt
			WHERE STATUS_ICS = '0'
		    ) T
	-- prevent primary key violation
    LEFT JOIN TB_R_GR_IR g 
           ON g.PO_NO = t.PO_NO 
          AND g.PO_ITEM_NO = t.PO_ITEM_NO 
          AND g.MAT_DOC_NO=t.MAT_DOC_NO 
		  AND g.MAT_DOC_YEAR = t.MAT_DOC_YEAR 
		  AND g.ITEM_NO = t.ITEM_NO 
		  AND g.COMP_PRICE_CD = t.COMP_PRICE_CD 
    where t.RX = 1
    and g.PO_NO is null; -- ensuring no insert to existing entry 
    
    SET @log = isnull(convert(varchar,@@ROWCOUNT),'0') + ' records sucessfully inserted to TB_R_GR_IR.';
    INSERT @px
    EXEC sp_PutLog @log, @uid, @na5, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;

-- =============================================
-- Section: STEP 6 
-- Description:	UPDATE DOM (IPPCS), tb_r_spot_gr_h (SPOT)
-- =============================================
    
	set @log = 'updating Daily Order Manifer (IPPCS), Spot GR Header (SPOT)';
	INSERT @px
	EXEC sp_PutLog @log, @uid, @na6, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;

	--* ICS *--
	SET @query = 'UPDATE t 
	SET t.LIV_SEND_FLAG = ''Y''
	from
	OPENQUERY(' + @ICSSrvrName + ', ''select * from ' + @ICSDBName + 'TB_R_GR_IR g WHERE 1 = 1
				-- FID.Ridwan => get source type from sysmaster
				AND g.SOURCE_TYPE in (' + @SourceType + ')
				and CONVERT(VARCHAR, g.DOC_DT, 112) >= ''''20131031''''
				and g.INV_NO is NULL
				AND g.LIV_SEND_FLAG = ''''N'''' '') t

				JOIN TB_T_GR_IR g 
				ON g.PO_NO = t.PO_NO 
					AND g.PO_ITEM_NO = t.PO_ITEM_NO 
					AND g.MAT_DOC_NO=t.MAT_DOC_NO 
					AND g.MAT_DOC_YEAR = t.MAT_DOC_YEAR 
					AND g.ITEM_NO = t.ITEM_NO 
					AND g.COMP_PRICE_CD = t.COMP_PRICE_CD ';

	EXEC (@query);

	--* IPPCS *--
	UPDATE A
	SET A.MAT_DOC_NO = B.MAT_DOC_NO,
		A.MAT_DOC_YEAR = B.MAT_DOC_YEAR,
		A.ICS_FLAG = 1,
		A.ICS_DT = GETDATE(),
		A.MANIFEST_RECEIVE_FLAG = '4',
		A.POSTING_BY = B.CREATED_BY,
		A.POSTING_DT = B.CREATED_DT,
		A.IN_PROGRESS = 2,
		A.CHANGED_BY = B.CREATED_BY,
		A.CHANGED_DT = GETDATE(),
		A.PO_NO = B.PO_NO
	FROM TB_R_DAILY_ORDER_MANIFEST A
	JOIN TB_T_GR_IR B ON A.MANIFEST_NO = B.INV_REF_NO

	-- Update TB_R_DAILY_ORDER_PART, posting_sts = 0 when posting is succes
	UPDATE A
	SET A.POSTING_STS = 0
	FROM TB_R_DAILY_ORDER_PART A
	JOIN TB_T_GR_IR B ON A.MANIFEST_NO = B.INV_REF_NO

	--* SPOT *--
	UPDATE A
	SET A.GR_STATUS = '3'
		,A.CHANGED_BY = 'SYSTEM'
		,A.CHANGED_DT = getdate()
	FROM TB_R_SPOT_GR_H A
	JOIN TB_T_GR_IR B ON A.GR_NO = B.INV_REF_NO

	UPDATE A
	SET A.SUBMIT_STATUS = '3'
		,A.GR_DOC_NO = B.MAT_DOC_NO
		,A.CHANGED_BY = 'SYSTEM'
		,A.CHANGED_DT = getdate()
	FROM TB_R_SPOT_GR_D A
	JOIN TB_T_GR_IR B ON A.GR_NO = B.INV_REF_NO
	
	UPDATE A
	SET A.IN_PROGRESS = '3'
		,A.SUBMIT_GI = 'N'
		,A.CHANGED_BY = 'SYSTEM'
		,A.CHANGED_DT = getdate()
	FROM TB_R_SPOT_RCV_D A
	JOIN TB_T_GR_IR B ON A.RCV_NO = B.INV_REF_NO
		--AND A.PART_NO = B.ICS_MAT_NO
	PRINT ''
    /* remark FID.Ridwan -> table tb_r_pcs_gr_ir_sync doesnt exist in new ics
	declare @ADDI INT = 100,  
		 @i int = 0, @j INT =0, @NMAX INT =0, @nx INT = 0, 
		 @TROWS INT = 0, 
		 @sql varchar(max), @v varchar(max), @rowids varchar(max), 
		 @lastLog DATETIME, @lastExec DATETIME
		 ;
    set @n = 0;
    SET @lastLog = GETDATE();
    
    SELECT @TROWS = COUNT(1) FROM TB_T_GR_IR;
	SET @NMAX = ROUND((@TROWS / @ADDI), 0) + 1;
	
	set @log = 'updating ' + CONVERT(varchar, @trows) + ' rows ics SYNC, in ' 
			+ CONVERT(varchar, @nmax) + ' steps max';
	INSERT @px
	EXEC sp_PutLog @log, @uid, @na6, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;		
	
	 
	while (@i <= @TROWS) AND (@n <= @NMAX)
	BEGIN 
		SET @j = @i + @ADDI;
		
		-- joins multi rows of rowid into one long string 
		select @v = (STUFF(
			(
			select ''', ''' + x.ROWID
			from TB_T_GR_IR x 
			WHERE ID BETWEEN (@i+1) and @j
			for XML path('')
			)
			,1,1,'') + '''');
		
		set @rowids = SUBSTRING(@v, 2, len(@v) -1) ;		

		set @sql = '
		BEGIN 
			update ' + @ICSDBName + 'tb_r_pcs_gr_ir_sync 			
			set status = status + 4, changed_by = ''sp_grir_sync'', changed_dt = sysdate
			where rowid in (
				' + @rowids + '
			)
			and ((status = 0) or (status = 1) or (status = 2)); 
		END;
		';
		
		IF ((SELECT COUNT(1) FROM TB_T_GR_IR WHERE ID BETWEEN @i +1 and @J) > 0) 
		BEGIN 
			SET @n = @n + 1; 
			
			SET @lastExec = GETDATE();
			
			-- next diganti
			exec(@sql) at [IPPCS_TO_ICS];
			-- when exec too fast, increase capacity
			IF ((DATEDIFF(SECOND, @lastExec, GETDATE()) < 10) AND (@ADDI BETWEEN 100 AND 999)) 
			BEGIN
				declare @newAddi INT =0; 
				declare @msDiff INT = 0;
				SET @msDiff = DATEDIFF(MILLISECOND, @lastExec, GETDATE());
				IF @msDiff > 0 AND ((@msDiff / @ADDI) >= 10)
					SET @newAddi = ROUND( 10000 / (@msDiff / @ADDI), 0);
				IF (@newAddi BETWEEN 100 AND 999)
				BEGIN
					 SET @ADDI = @newAddi
					 SET @NMAX = ROUND((@TROWS / @ADDI), 0) + 1;
					 IF @NMAX < @n 
						set @NMAX = @n;
						
					set @log = 'updating by bulk ' + CONVERT(varchar, @addi) + ' rows ics SYNC, in ' 
						+ CONVERT(varchar, @nmax) + ' steps max';
					INSERT @px
					EXEC sp_PutLog @log, @uid, @na6, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;	
				END;
			END;
			
			IF (DATEDIFF(SECOND, @lastLog, GETDATE()) >= 60) -- a minute wait 
			BEGIN
				SET @log = '#' +  isnull(convert(varchar,@n),'0') + 
					+ ' update to IPPCS_TO_ICS..PRICU001.TB_R_PCS_GR_IR_SYNC.';
			
				INSERT @px
				EXEC sp_PutLog @log, @uid, @na6, @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;		
				SET @lastLog = GETDATE();
			END;
		END
		ELSE
		BEGIN 
			BREAK; 
		END;
		
		SET @i = @j;
    END; */

    SET @log = 'END SP_GRIR_SYNC_FROM_ICS_BATCH' ;
    INSERT @px 
    EXEC sp_PutLog @log, @uid, 'SP_GRIR_SYNC_FROM_ICS_BATCH', @pid OUTPUT, 'MPCS00010INF', 'INF', @module, @function, 0;
    
    DELETE TB_T_LOCK
    WHERE LOCK_REFF = CONVERT(VARCHAR, @pid)
    AND FUNCTION_NO = @function;
    
END TRY

BEGIN CATCH 
	--* IPPCS *--
	UPDATE A
	SET A.MANIFEST_RECEIVE_FLAG = '5', -- Set Error posting
		ICS_FLAG=0,
		IN_PROGRESS = 3
	FROM TB_R_DAILY_ORDER_MANIFEST A
	JOIN TB_T_GR_IR B ON A.MANIFEST_NO = B.INV_REF_NO

	UPDATE A
	SET A.POSTING_STS = 1
	FROM TB_R_DAILY_ORDER_PART A
	JOIN TB_T_GR_IR B ON A.MANIFEST_NO = B.INV_REF_NO

	--* SPOT *--
	UPDATE A
	SET A.GR_STATUS = '1'
	FROM TB_R_SPOT_GR_H A
	JOIN TB_T_GR_IR B ON A.GR_NO = B.INV_REF_NO

	UPDATE A
	SET A.SUBMIT_STATUS = '1'
		,A.CHANGED_BY = 'SYSTEM'
		,A.CHANGED_DT = getdate()
	FROM TB_R_SPOT_GR_D A
	JOIN TB_T_GR_IR B ON A.GR_NO = B.INV_REF_NO

	UPDATE A
	SET A.IN_PROGRESS = '1'
		,A.CHANGED_BY = 'SYSTEM'
		,A.CHANGED_DT = getdate()
	FROM TB_R_SPOT_RCV_D A
	JOIN TB_T_GR_IR B ON A.RCV_NO = B.INV_REF_NO
		--AND A.PART_NO = B.ICS_MAT_NO

    DECLARE @emsg VARCHAR(MAX);
    
    SET @emsg = ISNULL(ERROR_MESSAGE(), '') + 
           ' @line:' + ISNULL(CONVERT(VARCHAR, ERROR_LINE()), '') + 
           ' err#:' + ISNULL(CONVERT(VARCHAR, ERROR_NUMBER()), '');
    
    DELETE TB_T_LOCK
    WHERE LOCK_REFF = CONVERT(VARCHAR, @pid)
    AND FUNCTION_NO = @function;

    INSERT @px 
    EXEC sp_PutLog @emsg, @uid, @na7, 
                   @pid OUTPUT, 'MPCS00010ERR', 'ERR', @module, @function, 4;
END CATCH;
