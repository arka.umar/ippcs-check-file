-- =============================================
-- Author:		FID.Deny
-- Create date: 2015-07-24
-- Description:	Get All PO Approval (for download)
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPOApprovalDownload] 
	@datefrom varchar(50) = null, 
    @dateto varchar(50) = null, 
    @potype varchar(10) = null,
    @status varchar(100) = null,
    @isAll bit = null,
	@picFullName VARCHAR(100) = null, 
	@poNos VARCHAR(MAX) = null,
	@suppCodes VARCHAR(MAX) = null
AS
BEGIN
		DECLARE
			@mo INT = 1, 
			@classSH INT, 
			@classDPH INT,
			@classDH INT,
			@statusSH INT, 
			@statusDPH INT,
			@statusDH INT, 
			@Pos INT,
			@PENDING_LEVEL INT, 	
			@Delimiter NVARCHAR(40),
			@NextString varchar(100);
    

		SET @Delimiter = ';'	
		SET @PENDING_LEVEL = 0;
		IF (NULLIF(@status,'') IS NOT NULL)
		BEGIN
			SET @status = @status + @Delimiter;
			SET @Pos = charindex(@Delimiter,@status);
			while (@Pos <> 0)
			begin
				set @NextString = SUBSTRING(@status,1,@Pos-1);
        
				if(@NextString='Pending SH')
					SET @PENDING_LEVEL = @PENDING_LEVEL+ 1;
            
				if(@NextString='Pending DpH')
					SET @PENDING_LEVEL = @PENDING_LEVEL + 2;
            
				if(@NextString='Pending DH')
					SET @PENDING_LEVEL = @PENDING_LEVEL + 4;
        
				set @status = substring(@status, @pos+1,len(@status))
				set @pos = charindex(@Delimiter, @status)		
			end;
		END; 

		SELECT TOP 1 
		  @classSH = CASE WHEN ISNUMERIC(SYSTEM_VALUE) = 1 
						   THEN CONVERT(INT, SYSTEM_VALUE) 
						   ELSE 0 
					  END 
		FROM TB_M_SYSTEM sx
		WHERE sx.SYSTEM_CD = 'PO_APPROVER_SH'
		  AND sx.FUNCTION_ID = 'ORDER';

		SELECT TOP 1 
		  @classDPH = CASE WHEN ISNUMERIC(SYSTEM_VALUE) = 1 
						   THEN CONVERT(INT, SYSTEM_VALUE) 
						   ELSE 0 
					  END 
		FROM TB_M_SYSTEM sx
		WHERE sx.SYSTEM_CD = 'PO_APPROVER_DPH'
		  AND sx.FUNCTION_ID = 'ORDER';

		SELECT TOP 1 
		  @classDH = CASE WHEN ISNUMERIC(SYSTEM_VALUE) = 1 
						   THEN CONVERT(INT, SYSTEM_VALUE) 
						   ELSE 0 
					  END 
		FROM TB_M_SYSTEM sx
		WHERE sx.SYSTEM_CD = 'PO_APPROVER_DH'
		  AND sx.FUNCTION_ID = 'ORDER';

		SELECT TOP 1 @statusSH = STATUS_cD 
		FROM [IPPCS_WORKFLOW].dbo.TB_R_STATUS
		WHERE [ACTION] = 1
		AND [MODULE_CD] = @mo
		AND [CLASS] = @classSH;

		SELECT TOP 1 @statusDPH = STATUS_CD 
		FROM [IPPCS_WORKFLOW].dbo.TB_R_STATUS
		WHERE [ACTION] = 1
		AND [MODULE_CD] = @mo
		AND [CLASS] = @classDPH;

		SELECT TOP 1 @statusDH = STATUS_CD 
		FROM [IPPCS_WORKFLOW].dbo.TB_R_STATUS
		WHERE [ACTION] = 1
		AND [MODULE_CD] = @mo
		AND [CLASS] = @classDH;

		SELECT 
			  PO.[PO_NO]								   AS [PO_NO]
			, PO.[PO_DATE]								   AS [PO_DATE]
			, MS.SUPP_NAME   							   AS [SUPPLIER_NAME]
			, PO.[TOTAL_AMOUNT]							   AS [TOTAL_AMOUNT] 
			, PO.REQUIRED_DT                               AS [REQUIRED_DT]

		FROM [dbo].[TB_R_PO_H] PO

		LEFT JOIN TB_M_SUPPLIER_ICS MS ON PO.SUPPLIER_CD = ms.SUPP_CD
        
		LEFT JOIN (SELECT * FROM [IPPCS_WORKFLOW].[dbo].[TB_R_DISTRIBUTION_STATUS] 
				where STATUS_CD = @statusSH) 
			   AS SHDS 
			   ON PO.[PO_NO] = SHDS.[FOLIO_NO] 
            
      
		LEFT JOIN (SELECT * FROM [IPPCS_WORKFLOW].[dbo].[TB_R_DISTRIBUTION_STATUS] 
					WHERE STATUS_CD = @statusDPH)
			   AS DPHDS 
			   ON PO.[PO_NO] = DPHDS.[FOLIO_NO] 
       
		LEFT JOIN (SELECT * FROM [IPPCS_WORKFLOW].[dbo].[TB_R_DISTRIBUTION_STATUS] 
					WHERE STATUS_CD = @statusDH) 
			   AS DHDS 
			   ON PO.[PO_NO] = DHDS.[FOLIO_NO] 
      
		LEFT JOIN 
			(
				SELECT DISTINCT 
					B.FOLIO_CD
					, B.MODULE_CD
					, SUBSTRING((SELECT ',' + A.DESTINATION AS [text()]
						FROM  [IPPCS_WORKFLOW].[dbo].[TB_R_WORKLIST_DESTINATION] A
						WHERE A.FOLIO_CD = B.FOLIO_CD AND A.MODULE_CD = B.MODULE_CD
						ORDER BY A.DESTINATION
						For XML PATH ('')),2, 1000) AS [CurrPIC]
				FROM [IPPCS_WORKFLOW].[dbo].[TB_R_WORKLIST_DESTINATION] B
			) 
			AS PIC 
			ON PO.PO_NO = PIC.FOLIO_CD

		LEFT JOIN dbo.TB_R_VW_EMPLOYEE v ON v.USERNAME = PIC.CurrPIC 
		LEFT JOIN dbo.TB_R_VW_EMPLOYEE vsh ON vsh.USERNAME = shds.PROCEED_BY 
		LEFT JOIN dbo.TB_R_VW_EMPLOYEE vdph ON vdph.USERNAME = dphds.PROCEED_BY
		LEFT JOIN dbo.TB_R_VW_EMPLOYEE vdh ON vdh.USERNAME = dhds.PROCEED_BY

		WHERE 
			  (NULLIF(@dateFrom, '') IS NULL OR PO_DATE >= CONVERT(datetime, @dateFrom, 120))
		AND   (NULLIF(@dateTo, '') IS NULL  OR PO_DATE <= CONVERT(DATETIME, @dateTo, 120))
		 AND   (NULLIF(@poType, '') IS NULL  OR @poType = 'ALL' OR @potype = PO_TYPE)
		AND   ISNULL(po.DELETION_FLAG, 0) = 0 
		AND   (@IsAll = 1
				OR (NULLIF(@status, '') IS NULL)
				OR ((@PENDING_LEVEL > 0)
					AND 
						(
							( ((@PENDING_LEVEL & 1) > 0) AND SHDS.[ACTUAL_DT] IS NULL)
						AND ( ((@PENDING_LEVEL & 2) > 0) AND DPHDS.[ACTUAL_DT] IS NULL)
						AND ( ((@PENDING_LEVEL & 4) > 0) AND DHDS.[ACTUAL_DT] IS NULL)
						)
					)
			   ) 

		AND ( (NULLIF(@picFullName,'') IS NULL)
			OR 
				(v.FULL_NAME LIKE '%' + ISNULL(@picFullName, '') + '%')
			) 


		AND ( (NULLIF(@poNos, '') IS NULL	)
			  OR 
				po.PO_NO in (SELECT item FROM dbo.fnSplit(@poNos, ';'))
			  )	
	
		AND ( (NULLIF(@suppCodes , '') IS NULL	)
			  OR 
				po.SUPPLIER_CD in (SELECT item FROM dbo.fnSplit(@suppCodes, ';'))
			  )
		ORDER BY PO_DATE
END

