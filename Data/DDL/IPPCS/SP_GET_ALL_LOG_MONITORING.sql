
ALTER PROCEDURE [dbo].[SP_GET_ALL_LOG_MONITORING] @@processID VARCHAR(50) = NULL
	,@@strdateFrom VARCHAR(50) = NULL
	,@@strdateto VARCHAR(50) = NULL
	,@@functionID VARCHAR(50) = NULL
	,@@processStatus VARCHAR(50) = NULL
	,@@userID VARCHAR(50) = NULL
AS
BEGIN
	--declare @log varchar(max), @PID BIGINT= 0
	--set @log=COALESCE(@@PROCESSID ,'') + ', ' + COALESCE(@@strdatefrom,'') + ', ' + coalesce(@@strdateto, '') + ', ' + coalesce(@@functionid, '') + ', ' + coalesce(@@processstatus, '');
	--exec PutLog @log, 'me', 'sp_get_all_log_monitoring', @PID OUTPUT, 'MPCS00001INF';
	DECLARE @PROCESS_STATUSes TABLE (PROCESS_STATUS INT);

	INSERT @PROCESS_STATUSes (PROCESS_STATUS)
	SELECT DISTINCT CASE 
			WHEN isnumeric(items) = 1
				THEN convert(INT, items)
			ELSE NULL
			END
	FROM [dbo].fn_split(@@processStatus, ',') x;

	SELECT a.START_DATE AS ProcessDate
		,a.END_DATE AS enddate
		,a.PROCESS_ID AS ProcessID
		,a.MODULE_ID
		,a.FUNCTION_ID AS FunctionID
		,b.FUNCTION_NAME AS FunctionNM
		,a.CREATED_BY AS UserID
		,t.process_status AS ProcessStatus
	FROM TB_R_LOG_H a
	JOIN (
		SELECT _FROM = CASE 
				WHEN NULLIF(@@strDateFrom, '') IS NOT NULL
					AND ISDATE(@@strDateFrom) = 1
					THEN convert(DATETIME, @@strdateFrom)
				ELSE NULL
				END
			,_TO = CASE 
				WHEN nullif(@@strDateTo, '') IS NOT NULL
					AND ISDATE(@@strDateTo) = 1
					THEN dateadd(second, 86399, convert(DATETIME, @@strdateto))
				ELSE NULL
				END
			,_PID = CASE 
				WHEN nullif(@@processid, '') IS NOT NULL
					AND isnumeric(@@processid) = 1
					THEN convert(BIGINT, @@processid)
				ELSE NULL
				END
		) d ON 1 = 1
	LEFT JOIN TB_M_FUNCTION b ON a.FUNCTION_ID = b.FUNCTION_ID
	LEFT JOIN (
		VALUES (
			0
			,'OK '
			,'OK'
			)
			,(
			1
			,'ERR'
			,'Error'
			)
			,(
			2
			,'WRN'
			,'Success with Warning'
			)
			,(
			3
			,'SUS'
			,'Suspend'
			)
			,(
			4
			,'ABO'
			,'Abort'
			)
			,(
			5
			,'ON '
			,'On Progress'
			)
			,(
			6
			,'SUE'
			,'Done'
			)
			,(
			7
			,'HOL'
			,'On Hold'
			)
		) t(id, ABR, process_status) ON t.id = a.PROCESS_STATUS
	LEFT JOIN @PROCESS_STATUSes ps ON ps.PROCESS_STATUS = a.PROCESS_STATUS
	WHERE (
			d._pid IS NULL
			OR a.Process_ID = @@processID
			)
		AND (
			d._FROM IS NULL
			OR a.START_DATE >= d._FROM
			)
		AND (
			d._TO IS NULL
			OR a.END_DATE <= d._TO
			)
		AND (
			NULLIF(@@functionId, '') IS NULL
			OR (a.Function_ID = @@functionID)
			)
		AND (
			NULLIF(@@processStatus, '') IS NULL
			OR ps.PROCESS_STATUS IS NOT NULL
			)
		AND a.CREATED_BY LIKE '%' + @@userID + '%'
	ORDER BY a.start_date DESC
END
