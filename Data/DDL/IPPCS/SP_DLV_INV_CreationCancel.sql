/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Cancel invoice with status created
*/

CREATE PROCEDURE [dbo].[SP_DLV_INV_CreationCancel]
   @DELIVERY_NO2 VARCHAR(MAX),
   @UserId VARCHAR(20)
AS
BEGIN
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @process_status AS INT = 0
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_INV_CreationCancel'
	DECLARE @step AS VARCHAR(50)
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)
	DECLARE @MODULE_ID as varchar(1) = '3'
	DECLARE @FUNCTION_ID as varchar(5) = '31401'
	DECLARE @cnt as int;
	DECLARE @cnt2 as int;
	SET @log = 'Preparing to cancel invoice creation';
	SET @step = 'InitializeCancelProcess';
	SET @steps = @na+'.'+@step;
	INSERT @T
	EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
	
	INSERT INTO TB_R_DLV_QUEUE(PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, PROCESS_END_DT, CREATED_BY, CREATED_DT)
	SELECT @pid as PROCESS_ID, @MODULE_ID, @FUNCTION_ID, 'QU2', GETDATE(), NULL, @UserId, GETDATE()
	
	
	--BEGIN TRY
		--BEGIN TRAN
		DECLARE @v XML;
		SET @log = 'Deleting data form TB_R_DLV_INV_UPLOAD';
		SET @step = 'CancelProcess';
		SET @steps = @na+'.'+@step;
		INSERT @T
		EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
		
		SET @v = '<r>' + REPLACE((SELECT @DELIVERY_NO2 FOR XML PATH('')), ',', '</r><r>') + '</r>';
		SET @cnt = (SELECT  count(y.XmlCol.value('(text())[1]', 'VARCHAR(MAX)')) AS Value FROM    @v.nodes('/r') y(XmlCol))
		
		SET @cnt2 = (SELECT COUNT(LP_INV_NO) FROM TB_R_DLV_INV_UPLOAD WHERE CONDITION_CATEGORY = 'H' AND DELIVERY_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(MAX)') AS Value FROM    @v.nodes('/r') y(XmlCol)) AND STATUS_CD = '1')
		
		DELETE FROM TB_R_DLV_INV_UPLOAD WHERE DELIVERY_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(MAX)') AS Value FROM    @v.nodes('/r') y(XmlCol))
		
		SET @log = 'Rollback status code in GR_IR';
		SET @step = 'CancelProcess';
		SET @steps = @na+'.'+@step;
		INSERT @T
		EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
		
		UPDATE TB_R_DLV_GR_IR SET LIV_FLAG = NULL, STATUS_CD = 1 WHERE REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(MAX)') AS Value FROM    @v.nodes('/r') y(XmlCol))
		--COMMIT TRAN

		
		
		IF(@cnt <> @cnt2)
		BEGIN
			SET @log = 'Cancelation finish with error, some data are not in TB_R_DLV_INV_UPLOAD';
			SET @step = 'CancelProcessSuccess';
			SET @steps = @na+'.'+@step;
			INSERT @T
			EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00002ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;
			UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
			SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Cancelation finish with error.' as [MESSAGE]	
			
		END
		ELSE
		BEGIN
			SET @log = 'Cancel invoice creation success';
			SET @step = 'CancelProcessSuccess';
			SET @steps = @na+'.'+@step;
			INSERT @T
			EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
			UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU3', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
			SELECT 'success' as [STATUS], 'INF' as [TYPE], 'Cancel invoice creation success.' as [MESSAGE]	
		END
		
		
		
		
	--END TRY
	--BEGIN CATCH
	--	ROLLBACK TRAN
	--	SET @log = 'Cancel invoice creation failed';
	--	SET @step = 'CancelProcessFailed';
	--	SET @steps = @na+'.'+@step;
	--	INSERT @T
	--	EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
	--END CATCH
END

