/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : PO release
*/

CREATE PROCEDURE [dbo].[SP_DLV_Release_PO]
@PO_NO VARCHAR(MAX),@CREATED_BY VARCHAR(50)
AS
BEGIN

		DECLARE @POSITION_ID INT, @CHECK_AUTH INT;
		
		DECLARE @CHECKSTS VARCHAR (3), @CHECK_APPROVAL VARCHAR(25), @CHECK_SH_APP VARCHAR (20), @CHECK_DPH_APP VARCHAR(20);
		DECLARE @STS VARCHAR (3), @APPROVED_BY VARCHAR (20), @APPROVED_DT VARCHAR (20),@POS INT, @LVL INT;


		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_Release_PO'
		DECLARE @step AS VARCHAR(50) = 'Init'
		DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)

		DECLARE @MODUL_ID INT = 3
		DECLARE @FUNCTION_ID VARCHAR (6) =  '31203'

		--EXPLODE
		DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
		DECLARE @MIN_ROW INT;

		DECLARE @ROW_DETAIL VARCHAR(MAX);
		DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
		DECLARE @MIN_FIELD INT;

		DECLARE @PONO_T VARCHAR(12);

		DECLARE @POSITION VARCHAR(MAX) = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '31203' AND SYSTEM_CD = 'PO_APPROVAL_POSITION')

		INSERT INTO @ROWS
		SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@PO_NO,';');
		--EXPLODE

		
		--==CREATE LOG==--
		SET @steps = @na+'.'+@step;
		SET @log = 'PO Approval process started.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		--==END CREATE LOG==--
		
		
		--Modif 02.07.2014 - AGUNG
		SET @steps = @na+'.Process';
		SET @log = 'Check Current Production Month';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		DECLARE @CHECK_PROD_MONTH_1 VARCHAR(12), @CHECK_PROD_MONTH_2 VARCHAR(12), @CHECK_PROD_MONTH_3 VARCHAR(12)
		SET @CHECK_PROD_MONTH_1 = (SELECT REPLACE(CONVERT(VARCHAR(7),GETDATE(),111),'/',''));
		SET @CHECK_PROD_MONTH_2 = (SELECT DISTINCT PROD_MONTH FROM TB_R_DLV_PO_H WHERE PO_NO IN (SELECT ITEM FROM @ROWS));
		SET @CHECK_PROD_MONTH_3 = (SELECT REPLACE(CONVERT(VARCHAR(7),DATEADD(mm, 1, GETDATE()),111),'/',''));
		
		IF ((@CHECK_PROD_MONTH_1 = @CHECK_PROD_MONTH_2) OR (@CHECK_PROD_MONTH_2 = @CHECK_PROD_MONTH_3)) BEGIN
						
						--==CREATE LOG==--
						SET @steps = @na+'.Process';
						SET @log = 'Check authorization level';
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
						--==END CREATE LOG==--
								
						SET @CHECK_AUTH = (SELECT COUNT(1) FROM TB_R_VW_EMPLOYEE WHERE DIVISION_ID = 19 AND USERNAME = @CREATED_BY AND POSITION_ID IN (select s from fnSplitString(@POSITION,',')));
						
						IF (@CHECK_AUTH=1)BEGIN

								
								SET @POSITION_ID = (SELECT POSITION_LEVEL  FROM [dbo].[TB_R_VW_EMPLOYEE] WHERE USERNAME = @CREATED_BY);

								IF (@POSITION_ID = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '31203' AND SYSTEM_CD = 'PO_APPROVAL_LVL3')) 
								BEGIN
									SET @POS = 3;
									SET @APPROVED_BY = 'SH_APPROVED_BY';
									SET @APPROVED_DT = 'SH_APPROVED_DT';
									SET @STS = 'PO2';
								END
								ELSE IF (@POSITION_ID =(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '31203' AND SYSTEM_CD = 'PO_APPROVAL_LVL2')) 
								BEGIN
									SET @POS = 4;
									SET @APPROVED_BY = 'DPH_APPROVED_BY';
									SET @APPROVED_DT = 'DPH_APPROVED_DT';
									SET @STS = 'PO3';
								END
								ELSE IF (@POSITION_ID =(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '31203' AND SYSTEM_CD = 'PO_APPROVAL_LVL1')) 
								BEGIN 
									SET @POS = 5;
									SET @APPROVED_BY = 'DH_APPROVED_BY';
									SET @APPROVED_DT = 'DH_APPROVED_DT';
									SET @STS = 'PO4';
								END
								ELSE
								BEGIN
									RAISERROR('You are not authorized to perform this action.',16,1)
								END

								--==CREATE LOG==--
								SET @steps = @na+'.Process';
								SET @log = 'Parsing data from parameter';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
								--==END CREATE LOG==--

								SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

										WHILE @MIN_ROW IS NOT NULL BEGIN

												DELETE FROM @FIELDS;


												SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
												INSERT INTO @FIELDS
												SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'');
												

												SET @PONO_T = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
													-- ========================================================================================
												

												--==CREATE LOG==--
												SET @steps = @na+'.Process';
												SET @log = 'Checking PO_STATUS_FLAG  for PO Number '+@PONO_T;
												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
												--==END CREATE LOG==--

												SET @CHECKSTS = (SELECT PO_STATUS_FLAG FROM TB_R_DLV_PO_H WHERE PO_NO = @PONO_T);
												
												IF (@CHECKSTS = 'PO1') BEGIN
																SET @LVL = 2;
														END
														ELSE IF (@CHECKSTS = 'PO2') BEGIN
															SET @LVL = 3;
														END
														ELSE IF (@CHECKSTS = 'PO3') BEGIN
															SET @LVL = 4;
														END
														ELSE BEGIN
															SET @LVL = 5;
														END

												IF (@CHECKSTS = 'PO4') OR (@CHECKSTS = 'PO5') OR (@CHECKSTS = 'PO6') BEGIN
														
														--==CREATE LOG==--
														SET @steps = @na+'.Process';
														SET @log = 'Failed to approve PO Number '+@PONO_T;
														EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
														--==END CREATE LOG==--
														

														--==CREATE LOG==--
														SET @steps = @na+'.Finish';
														SET @log = 'PO approval process has been finished.';
														EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
														--==END CREATE LOG==--
									
														RAISERROR('Failed to approve.',16,1)
														RETURN
													
												END
												ELSE BEGIN
														IF ( @POS > @LVL) BEGIN
																
																IF (@POS=3) BEGIN

																		--==CREATE LOG==--
																		SET @steps = @na+'.Process';
																		SET @log = 'Updating TB_R_DLV_PO_H for PO Number '+@PONO_T;
																		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																		--==END CREATE LOG==--

																		--UPDATE PO HEADER =====================================
																		UPDATE TB_R_DLV_PO_H
																		SET
																		PO_STATUS_FLAG = @STS,
																		SH_APPROVED_BY = @CREATED_BY,
																		SH_APPROVED_DT = GETDATE(),
																		CHANGED_BY = @CREATED_BY,
																		CHANGED_DT =  GETDATE()
																		WHERE 
																		PO_NO = @PONO_T
																		--END HEADER ===========================================
																		
																		--UPDATE PO DETAIL ====================================
																		UPDATE TB_R_DLV_PO_D
																		SET
																		CHANGED_BY = @CREATED_BY,
																		CHANGED_DT =  GETDATE()
																		WHERE
																		PO_NO = @PONO_T
																		--END DETAIL ===========================================

																END
																ELSE IF (@POS = 4) BEGIN
																		
																		SET @CHECK_SH_APP = (SELECT SH_APPROVED_BY FROM TB_R_DLV_PO_H WHERE PO_NO = @PONO_T);
																		
																		IF (@CHECK_SH_APP IS NULL) BEGIN
												
							
																					--==CREATE LOG==--
																					SET @steps = @na+'.Process';
																					SET @log = 'Updating TB_R_DLV_PO_H for PO Number '+@PONO_T;
																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					--==END CREATE LOG==--
																					
																					--UPDATE PO HEADER =====================================
																					UPDATE TB_R_DLV_PO_H
																					SET
																					PO_STATUS_FLAG = @STS,
																					SH_APPROVED_BY = @CREATED_BY,
																					SH_APPROVED_DT = GETDATE(),
																					DPH_APPROVED_BY = @CREATED_BY,
																					DPH_APPROVED_DT = GETDATE(),
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE()
																					WHERE 
																					PO_NO = @PONO_T
																					--END HEADER ===========================================
																					
																					--UPDATE PO DETAIL ====================================
																					UPDATE TB_R_DLV_PO_D
																					SET
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE()
																					WHERE
																					PO_NO = @PONO_T
																					--END DETAIL ===========================================

																		END 
																		ELSE BEGIN

																					--==CREATE LOG==--
																					SET @steps = @na+'.Process';
																					SET @log = 'Updating TB_R_DLV_PO_H for PO Number '+@PONO_T;
																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					--==END CREATE LOG==--

																					--UPDATE PO HEADER =====================================
																					UPDATE TB_R_DLV_PO_H
																					SET
																					PO_STATUS_FLAG = @STS,
																					DPH_APPROVED_BY = @CREATED_BY,
																					DPH_APPROVED_DT = GETDATE(),
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE()
																					WHERE 
																					PO_NO = @PONO_T
																					--END HEADER ===========================================
																					
																					--UPDATE PO DETAIL ====================================
																					UPDATE TB_R_DLV_PO_D
																					SET
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE()
																					WHERE
																					PO_NO = @PONO_T
																					--END DETAIL ===========================================
																		END


																END
																ELSE BEGIN
																		
																		SET @CHECK_SH_APP = (SELECT SH_APPROVED_BY FROM TB_R_DLV_PO_H WHERE PO_NO = @PONO_T);
																		SET @CHECK_DPH_APP = (SELECT DPH_APPROVED_BY FROM TB_R_DLV_PO_H WHERE PO_NO = @PONO_T);

																		IF (@CHECK_SH_APP IS NULL AND @CHECK_DPH_APP IS NULL)BEGIN

																					--==CREATE LOG==--
																					SET @steps = @na+'.Process';
																					SET @log = 'Updating TB_R_DLV_PO_H for PO Number '+@PONO_T;
																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					--==END CREATE LOG==--

																					UPDATE TB_R_DLV_PO_H
																					SET
																					PO_STATUS_FLAG = @STS,
																					SH_APPROVED_BY = @CREATED_BY,
																					SH_APPROVED_DT = GETDATE(),
																					DPH_APPROVED_BY = @CREATED_BY,
																					DPH_APPROVED_DT = GETDATE(),
																					DH_APPROVED_BY = @CREATED_BY,
																					DH_APPROVED_DT = GETDATE(),
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE(),
																					RELEASED_FLAG = 'Y'
																					WHERE 
																					PO_NO = @PONO_T
																					
																					UPDATE TB_R_DLV_PO_D
																					SET
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE()
																					WHERE
																					PO_NO = @PONO_T

																		END
																		ELSE IF(@CHECK_DPH_APP IS NULL)BEGIN

																					--==CREATE LOG==--
																					SET @steps = @na+'.Process';
																					SET @log = 'Updating TB_R_DLV_PO_H for PO Number '+@PONO_T;
																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					--==END CREATE LOG==--

																					UPDATE TB_R_DLV_PO_H
																					SET
																					PO_STATUS_FLAG = @STS,
																					DPH_APPROVED_BY = @CREATED_BY,
																					DPH_APPROVED_DT = GETDATE(),
																					DH_APPROVED_BY = @CREATED_BY,
																					DH_APPROVED_DT = GETDATE(),
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE(),
																					RELEASED_FLAG = 'Y'
																					WHERE 
																					PO_NO = @PONO_T

																		END
																		ELSE BEGIN

																					--==CREATE LOG==--
																					SET @steps = @na+'.Process';
																					SET @log = 'Updating TB_R_DLV_PO_H for PO Number '+@PONO_T;
																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					--==END CREATE LOG==--

																					UPDATE TB_R_DLV_PO_H
																					SET
																					PO_STATUS_FLAG = @STS,
																					DH_APPROVED_BY = @CREATED_BY,
																					DH_APPROVED_DT = GETDATE(),
																					CHANGED_BY = @CREATED_BY,
																					CHANGED_DT =  GETDATE(),
																					RELEASED_FLAG = 'Y'
																					WHERE 
																					PO_NO = @PONO_T
																		END
																END
																
																--===========SEND E-MAIL
																SET @steps = @na+'.Process';
																SET @log = 'Starting sent e-mail.';

																EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																
																BEGIN TRY
																	DECLARE @RecipientEmail varchar(max),
																			@RecipientCopyEmail varchar(max),
																			@RecipientBlindCopyEmail varchar(max),
																			@ERR_FLAG varchar(50),
																			@body varchar(max),
																			@subjek varchar(100),
																			@ERROR_DETAIL varchar(max),
																			@profile varchar(max),
																			@MAIL_QUERY VARCHAR(MAX),
																			@ACTION VARCHAR(25)
																			
																			SET @profile = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE');
																			IF (@profile IS NULL) BEGIN
																				SET @profile = 'NotificationAgent';
																			END
																			
																			IF (@POSITION_ID = 50)
																				SET @ACTION = 'PO_APPROVAL_SH'
																			ELSE IF (@POSITION_ID = 40)
																				SET @ACTION = 'PO_APPROVAL_DPH'
																			ELSE IF (@POSITION_ID = 30)
																				SET @ACTION = 'PO_APPROVAL_RELEASE'
																										
																			SELECT @RecipientEmail=EMAIL_TO, @RecipientCopyEmail=EMAIL_CC, @RecipientBlindCopyEmail=EMAIL_BCC 
																			FROM TB_M_RTP_NOTIFICATION_EMAIL 
																			WHERE MODULE_ID = CAST(@MODUL_ID AS VARCHAR) 
																				AND FUNCTION_ID = @FUNCTION_ID AND ACTION = @ACTION
																			
																			SELECT  @subjek=NOTIFICATION_SUBJECT,
																					@body=NOTIFICATION_CONTENT
																			FROM    TB_M_NOTIFICATION_CONTENT
																			WHERE   FUNCTION_ID = @FUNCTION_ID
																					AND ROLE_ID = '31'
																					AND NOTIFICATION_METHOD = '1'
																			
																			SET @subjek = REPLACE(@subjek, '@ProdMonth', RIGHT(LEFT(@PONO_T, 9), 6))
																			
																			
																			IF ((@POSITION_ID = 50) OR (@POSITION_ID = 40)) BEGIN
																				SET @subjek = REPLACE(@subjek, '@subjectaction', 'Approval')
																				SET @body = REPLACE(@body, '@bodyaction', 'approved')
																			END ELSE BEGIN
																				SET @subjek = REPLACE(@subjek, '@subjectaction', 'Release')
																				SET @body = REPLACE(@body, '@bodyaction', 'released')
																			END
																			
																			SET @body = REPLACE(@body, '@approve', @CREATED_BY)
																			SET @body = REPLACE(@body, '@DateTime', (SELECT CAST(DAY(GETDATE()) AS VARCHAR) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR)))
																			SET @body = REPLACE(@body, '@pono', @PONO_T)
																			
																			IF (@RecipientCopyEmail IS NOT NULL)
																				SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
																				
																			IF (@RecipientCopyEmail IS NOT NULL)
																				SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
																			
																			SET @body = ISNULL(@body, '')
																			SET @subjek = ISNULL(@subjek, '')
																			
																			SET @RecipientEmail = ISNULL(@RecipientEmail, '')
																			SET @RecipientCopyEmail = ISNULL(@RecipientCopyEmail, '')
																			SET @RecipientBlindCopyEmail = ISNULL(@RecipientBlindCopyEmail, '')
																			
																			SET @MAIL_QUERY = '	
																								EXEC msdb.dbo.sp_send_dbmail
																									@profile_name	= ''' + @profile + ''',
																									@body 			= ''' + @body + ''',
																									@body_format 	= ''HTML'',
																									@recipients 	= ''' + @RecipientEmail + ''',
																									' + @RecipientCopyEmail + '
																									' + @RecipientBlindCopyEmail + '
																									@subject 		= ''' + @subjek + ''''
																									
																			EXEC (@MAIL_QUERY)
																			

																					SET @steps = @na+'.Process';
																					SET @log = 'Email has been sent';

																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					
																					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE()
																					WHERE PROCESS_ID = @pid;

																					--CREATE LOG
																					SET @steps = @na+'.Process';
																					SET @log = 'send e-mail has been finished.';

																					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																					--==FINISH==--

																END TRY
																BEGIN CATCH

																						DECLARE @ERROR VARCHAR(MAX)
																						SET @ERROR = ERROR_MESSAGE();
																						--CREATE LOG
																						SET @steps = @na+'.Process';
																						SET @log = 'Email notification failed sent to all PIC.';
																						EXEC dbo.sp_PutLog @ERROR, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
																						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;

																						UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
																						WHERE PROCESS_ID = @pid;


																END CATCH

														--===========END OF SEND E-MAIL
																
														END

														ELSE BEGIN
																RAISERROR('PR already approved.',16,1)
																RETURN
														END
												END


								SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
												-- =================================================================================

								END --END WHILE

								--==CREATE LOG==--
								SET @steps = @na+'.Finish';
								SET @log = 'PO Approval process has been finished.';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
								--==END CREATE LOG==--


						END
						ELSE BEGIN
								--==CREATE LOG==--
								SET @steps = @na+'.Process';
								SET @log = 'Access denied!';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
								--==END CREATE LOG==--
								
								--==CREATE LOG==--
								SET @steps = @na+'.Finish';
								SET @log = 'PO Approval process has been finished.';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
								--==END CREATE LOG==--

								RAISERROR('You are not authorized to perform this action.',16,1)
								RETURN

						END
			END
			ELSE BEGIN
						--==CREATE LOG==--
								SET @steps = @na+'.Process';
								SET @log = 'PO cannot be approved with this production month!';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
						--==END CREATE LOG==--
								
								--==CREATE LOG==--
								SET @steps = @na+'.Finish';
								SET @log = 'PO Approval process has been finished.';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
								--==END CREATE LOG==--

								RAISERROR('PO Production Month Not In Current Month And Next Month',16,1)
								RETURN
			END

END

