CREATE PROCEDURE [dbo].[SP_Rep_DCLExport]
	@PickupFrom varchar (10),
	@PickupTo varchar (10),
	@LogPartner varchar (100),
	@Route varchar(4),
	@Rate varchar(2),
	@DockCode varchar(100),
	@DeliveryNo varchar(20),
	@Status char(1),
	@Achievement varchar(12),
	@DeliveryType char (1)
AS
BEGIN
	Declare @Query nvarchar(max)

	--SET @PickupFrom = LTRIM(RTRIM(@PickupFrom))
	--SET @PickupTo = LTRIM(RTRIM(@PickupTo))
	--SET @LogPartner = LTRIM(RTRIM(@LogPartner))
	--SET @Route = LTRIM(RTRIM(@Route))
	--SET @Rate = LTRIM(RTRIM(@Rate))
	--SET @DockCode = LTRIM(RTRIM(@DockCode))
	--SET @DeliveryNo = LTRIM(RTRIM(@DeliveryNo))
	--SET @Status = LTRIM(RTRIM(@Status))
	--SET @Achievement = LTRIM(RTRIM(@Achievement))
	--SET @DeliveryType = LTRIM(RTRIM(@DeliveryType))

  CREATE TABLE #tmpSplitString
	( 
		column1 VARCHAR(100)
	)

  INSERT INTO #tmpSplitString (column1)
  EXEC dbo.SplitString @LogPartner, '' 

  INSERT INTO #tmpSplitString (column1)
  EXEC dbo.SplitString @DockCode, '' 
  
  SET @Query = '
SELECT
	(ROW_NUMBER() OVER (ORDER BY PICKUP_DT desc, A.ROUTE + '' - '' + A.RATE ASC)) AS NO,
	A.DELIVERY_NO,
	A.REVISE_NO, 
	A.PICKUP_DT, 
	A.ROUTE + '' - '' + A.RATE as ROUTE_RATE,
    A.LP_CD,
    B.LOG_PARTNER_NAME, 
    A.DRIVER_NAME, 
    A.DELIVERY_STS,
    ISNULL(X.ARRIVAL_STATUS, '''') ARRIVAL_STATUS,
    ISNULL(X2.DEPARTURE_STATUS, '''') DEPARTURE_STATUS,
    A.CONFIRM_STS,
    A.REQUISITIONER_BY,
    A.REQUISITIONER_DT,
    A.DOWNLOAD_BY, 
    A.DOWNLOAD_DT, 
    A.CREATED_BY, 
    A.CREATED_DT, 
    A.CHANGED_BY, 
    A.CHANGED_DT, 
    A.INVOICE_BY, 
    A.INVOICE_DT,
    A.DELIVERY_REASON,
	A.APPROVE_BY,
	A.APPROVE_DT,
	CASE 
		WHEN CONVERT(INT, A.NOTIFY_STS) % 4 = 0 THEN ''~/Content/Images/notice_add_icon.png''
		WHEN CONVERT(INT, A.NOTIFY_STS) % 4 = 1 THEN ''~/Content/Images/notice_new_icon.png''
		WHEN CONVERT(INT, A.NOTIFY_STS) % 4 = 2 THEN ''~/Content/Images/notice_open_icon.png''
		WHEN CONVERT(INT, A.NOTIFY_STS) % 4 = 3 THEN ''~/Content/Images/notice_close_icon.png''
	END as Notif
FROM [dbo].[TB_R_DELIVERY_CTL_H] A  
LEFT JOIN (
	SELECT 
		CASE min(ARRIVAL_STATUS)
		  WHEN 1 THEN ''NG''
		  WHEN 2 THEN ''NG''
		  WHEN 3 THEN ''OK''
		  ELSE ''''
          END AS ARRIVAL_STATUS, 
		DELIVERY_NO 
	FROM TB_R_DELIVERY_CTL_D
	WHERE DEPARTURE_STATUS != 0
	GROUP BY DELIVERY_NO
) X ON X.DELIVERY_NO = A.DELIVERY_NO
LEFT JOIN (
	SELECT 
		CASE min(DEPARTURE_STATUS)
		  WHEN 1 THEN ''NG''
		  WHEN 2 THEN ''NG''
		  WHEN 3 THEN ''OK''
		  ELSE ''''
          END AS DEPARTURE_STATUS, 
		DELIVERY_NO 
	FROM TB_R_DELIVERY_CTL_D
	WHERE DEPARTURE_STATUS != 0
	GROUP BY DELIVERY_NO
) X2 ON X2.DELIVERY_NO = A.DELIVERY_NO
LEFT JOIN [dbo].[TB_M_LOGISTIC_PARTNER] B ON a.LP_CD=b.LOG_PARTNER_CD          
WHERE A.PUSH_TO_WEB_FLAG=''1'' 
	  AND A.DELIVERY_TYPE = ''' + @DeliveryType + '''
	  AND (A.PICKUP_DT between ''' + @PickupFrom + ''' AND ''' + @PickupTo + ''' )
      AND ((A.ROUTE like ''%' + @Route + '%'' AND isnull(''' + @Route + ''','''') <> '''') or (isnull(''' + @Route + ''' ,'''') = ''''))
	  AND ((A.RATE like ''%' + @Rate + '%'' AND isnull(''' + @Rate + ''' ,'''') <> '''') or (isnull(''' + @Rate + ''','''') = ''''))
	  AND ((A.DELIVERY_NO like ''%' + @DeliveryNo + '%'' AND isnull(''' + @DeliveryNo + ''','''') <> '''') or (isnull(''' + @DeliveryNo + ''','''') = ''''))
	  AND ((A.DELIVERY_STS=''' + @Status + ''' AND isnull(''' + @Status + ''','''') <> '''') or (isnull(''' + @Status + ''','''') = ''''))
	  AND (((EXISTS (SELECT ''x'' FROM #tmpSplitString WHERE column1 = A.LP_CD)) AND ISNULL(''' + @LogPartner + ''', '''') <> '''') or (isnull(''' + @LogPartner + ''','''') = ''''))
	  AND (((EXISTS (SELECT ''x'' FROM TB_R_DELIVERY_CTL_D D JOIN #tmpSplitString T ON T.column1 = D.DOCK_CD AND D.DELIVERY_NO = A.DELIVERY_NO)) AND ISNULL(''' + @DockCode + ''', '''') <> '''') or (isnull(''' + @DockCode + ''','''') = ''''))
	  AND ((((X.ARRIVAL_STATUS = ''OK'' AND ''' + @Achievement + ''' = ''Arrival OK'')
			  OR (X.ARRIVAL_STATUS = ''NG'' AND ''' + @Achievement + ''' = ''Arrival NG'')
			  OR (X2.DEPARTURE_STATUS = ''OK'' AND ''' + @Achievement + ''' = ''Departure OK'')
			  OR (X2.DEPARTURE_STATUS = ''NG'' AND ''' + @Achievement + ''' = ''Departure NG'')) 
		   AND isnull(''' + @Achievement + ''','''') <> '''') or (isnull(''' + @Achievement + ''','''') = ''''))
	  '

SET @Query = @Query + ' ORDER BY DELIVERY_NO, PICKUP_DT desc, ROUTE_RATE ASC '

  EXECUTE (@Query) 
  DROP TABLE #tmpSplitString   
END

