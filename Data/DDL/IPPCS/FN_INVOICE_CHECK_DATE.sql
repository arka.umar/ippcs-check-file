-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2020-09-30
-- Description:	This function to check format of date or month-year
-- =============================================
CREATE FUNCTION [dbo].[FN_INVOICE_CHECK_DATE] (
	@ri_v_date VARCHAR(15)
	,@ri_v_date_format VARCHAR(15)
	)
RETURNS INT
AS
BEGIN
	DECLARE @Result INT = 1;

	IF LEN(@ri_v_date) = LEN(@ri_v_date_format)
	BEGIN
		SET @Result = 1;
	END
	ELSE
	BEGIN
		SET @Result = 0;
	END

	RETURN @Result;
END
