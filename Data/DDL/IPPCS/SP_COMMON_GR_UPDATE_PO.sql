
/****** Object:  StoredProcedure [dbo].[SP_COMMON_GR_UPDATE_PO]    Script Date: 30/04/2021 17:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	05-Oct-2020
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
ALTER PROCEDURE [dbo].[SP_COMMON_GR_UPDATE_PO] @USER_ID VARCHAR(20) = 'AGAN'
	,@PROCESS_ID BIGINT = '202009230021'
	,@ri_v_reff_no VARCHAR(100)
	,@ri_v_po_no VARCHAR(100)
	,@ri_v_mat_no VARCHAR(100)
	,@ri_v_source_type VARCHAR(100)
	,@ri_v_prod_purpose_cd VARCHAR(100)
	,@ri_v_packing_type VARCHAR(100)
	,@ri_v_plant_cd VARCHAR(100)
	,@ri_v_sloc_cd VARCHAR(100)
	,@ri_d_delivery_dt DATE
	,@ri_n_Quantity INT
	,@ri_v_part_color_sfx VARCHAR(100)
	,@ri_v_flag VARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MODULE_ID VARCHAR(50) = '4'
		,@FUNCTION_ID VARCHAR(100) = '42005'
		,@IsError CHAR(1) = 'N'
		,@CURRDT DATE = CAST(GETDATE() AS DATE)
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_COMMON_GR_UPDATE_PO'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@RES INT = 1
		,@FLAG_MATERIAL CHAR(1) = 'N';

	BEGIN TRY
		DECLARE @l_n_return INT
			,@l_n_status INT
			,@l_v_calc_schema_cd VARCHAR(100)
			,@l_v_payment_method_cd VARCHAR(100)
			,@l_v_payment_term_cd VARCHAR(100)
			,@l_v_curr_cd VARCHAR(100)
			,@l_v_po_item_no VARCHAR(100) = ''
			,@l_n_qty NUMERIC(13, 3)
			,@l_n_po_open_qty NUMERIC(13, 3)
			,@l_n_price NUMERIC(16, 5)
			,@l_v_supp_cd VARCHAR(100)
			,@l_v_supp_name VARCHAR(100)
			,@l_v_po_no VARCHAR(100) = ''
			,@l_n_mat_price NUMERIC(16, 5)
			,@l_n_row INT = 0
			,@l_n_length INT = 0
			,@l_b_check CHAR(1)
			,@l_n_null INT = 0
			,@l_v_message VARCHAR(2000)
			,@l_v_val VARCHAR(500)
			,@l_v_val_tmp VARCHAR(500)
			,@l_d_sysdate DATE
			,@l_v_msg_out1 VARCHAR(500)
			,@ri_v_flagsts VARCHAR(20);
		DECLARE @MAT_DESC VARCHAR(100) = ''
			,@UOM VARCHAR(100) = ''
			,@SPECIAL_PROC_TYPE VARCHAR(100) = ''
			,@SUPP_CD VARCHAR(100)
			,@MAT_PRICE NUMERIC(16, 5)
			,@CALC_SCHEMA_CD VARCHAR(100)
			,@PAYMENT_METHOD_CD VARCHAR(100)
			,@PAYMENT_TERM_CD VARCHAR(100)
			,@CURR_CD VARCHAR(100)
			,@PO_NO VARCHAR(100) = @ri_v_po_no
			,@MAT_NO VARCHAR(100) = @ri_v_mat_no
			,@SOURCE_TYPE VARCHAR(100) = @ri_v_source_type
			,@PROD_PURPOSE_CD VARCHAR(100) = @ri_v_prod_purpose_cd
			,@PACKING_TYPE VARCHAR(100) = @ri_v_packing_type
			,@PLANT_CD VARCHAR(100) = @ri_v_plant_cd
			,@SLOC_CD VARCHAR(100) = @ri_v_sloc_cd
			,@PROD_MONTH VARCHAR(100) = CONVERT(VARCHAR(6), @ri_d_delivery_dt, 112)
			,@QUANTITY INT = @ri_n_Quantity
			,@PART_COLOR_SUFFIX VARCHAR(100) = @ri_v_part_color_sfx;

		SET @PARAM1 = 'Calculation for PO NO : ' + @ri_v_po_no + ', Material No: ' + @ri_v_mat_no;

		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		IF ISNULL(@ri_v_flag, '') = ''
		BEGIN
			SET @ri_v_flagsts = 'IM';
		END
		ELSE
		BEGIN
			SET @ri_v_flagsts = 'PROC';
		END

		-- Get Syadate from Server
		SELECT @l_d_sysdate = GETDATE();

		--* Mandatory Checking *--
		-- Check Null Value from Input Parameters
		IF ISNULL(@ri_v_mat_no, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Material No';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Material No';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END
		END

		IF ISNULL(@ri_v_source_type, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Source type';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Source type';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_v_prod_purpose_cd, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Prod.Purpose Code';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Prod.Purpose Code';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_v_packing_type, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Packing Type';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Packing Type';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_v_plant_cd, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Plant Code';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Plant Code';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_v_sloc_cd, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'SLoc Code';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', SLoc Code';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_d_delivery_dt, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Delivery Date';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Delivery Date';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_n_Quantity, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Quantity';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Quantity';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_v_part_color_sfx, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Part Color Suffix';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Part Color Suffix';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF @l_n_null > 0
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00003ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,@l_v_val

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00003ERR'
				,'ERR'
				,'MPCS00003ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ' : Mandatory Checking'
				,@USER_ID
				,GETDATE()
		END;

		--* Length Checking *--
		IF ISNULL(@ri_v_po_no, '') <> ''
		BEGIN
			-- Get Length PO_NO from table TB_R_PO_H
			IF LEN(@ri_v_po_no) > 30
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of PO Number,  the Length should be between 1 and 30 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		-- Get Length MAT_NO from table TB_M_MATERIAL
		IF ISNULL(@ri_v_mat_no, '') <> ''
		BEGIN
			IF LEN(@ri_v_mat_no) > 23
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Material Number,  the Length should be between 1 and 23 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		-- Get Length SOURCE_TYPE from table TB_R_PO_ITEM
		IF ISNULL(@ri_v_source_type, '') <> ''
		BEGIN
			IF LEN(@ri_v_source_type) > 1
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Source Type,  the Length should be between 1 and 1 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		-- Get Length PROD_PURPOSE_CD from table TB_R_PO_ITEM
		IF ISNULL(@ri_v_prod_purpose_cd, '') <> ''
		BEGIN
			IF LEN(@ri_v_prod_purpose_cd) > 5
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Prod Purpose Code,  the Length should be between 1 and 5 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		-- Get Length PACKING_TYPE from table TB_R_PO_ITEM
		IF ISNULL(@ri_v_packing_type, '') <> ''
		BEGIN
			IF LEN(@ri_v_packing_type) > 1
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Packing Type,  the Length should be between 1 and 1 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		-- Get Length PLANT_CD from table TB_R_PO_ITEM
		IF ISNULL(@ri_v_plant_cd, '') <> ''
		BEGIN
			IF LEN(@ri_v_plant_cd) > 4
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Plant Code,  the Length should be between 1 and 4 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		-- Get Length SLOC_CD from table TB_R_PO_ITEM
		IF ISNULL(@ri_v_sloc_cd, '') <> ''
		BEGIN
			IF LEN(@ri_v_sloc_cd) > 20
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Sloc Code,  the Length should be between 1 and 20 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		-- Get Length PART_COLOR_SUFFIX from table TB_R_PO_ITEM
		IF ISNULL(@ri_v_part_color_sfx, '') <> ''
		BEGIN
			IF LEN(@ri_v_part_color_sfx) > 2
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Part Color Suffix,  the Length should be between 1 and 2 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Length Checking'
					,@USER_ID
					,GETDATE()
			END;
		END;

		IF @IsError <> 'Y'
		BEGIN
			-- Get Material Description, Unit Of Measure, and Special Procurement Type,
			IF OBJECT_ID('tempdb..#csMatMasterResult') IS NOT NULL
			BEGIN
				DROP TABLE #csMatMasterResult
			END;

			CREATE TABLE #csMatMasterResult (
				unit_of_measure_cd VARCHAR(3)
				,special_procurement_type_cd VARCHAR(2)
				,mat_grp_cd VARCHAR(9)
				,mat_type_cd VARCHAR(4)
				,mat_desc VARCHAR(40)
				,costing_type VARCHAR(2)
				,issue_storage_location VARCHAR(6)
				,price_control_flag VARCHAR(1)
				,moving_average_price NUMERIC(15, 2)
				,standard_price NUMERIC(15, 2)
				,valuation_class_cd VARCHAR(4)
				,sloc_for_ex_proc VARCHAR(6)
				,STATUS INT
				)

			INSERT INTO #csMatMasterResult
			EXEC [dbo].[SP_COMMON_GR_MAT_MASTER_PARAM] @USER_ID
				,@PROCESS_ID
				,@ri_v_mat_no
				,@ri_v_prod_purpose_cd
				,@ri_v_source_type
				,@ri_v_plant_cd

			IF EXISTS (
					SELECT 1
					FROM #csMatMasterResult
					WHERE STATUS = 1
					)
			BEGIN
				SET @IsError = 'Y';

				--register to manifest detail
				EXEC [dbo].[SP_COMMON_GR_MANIFEST_LOG] @USER_ID
					,@ri_v_reff_no
					,@ri_v_mat_no
					,2
					,'Material master not found'
			END
			ELSE
			BEGIN
				SELECT @MAT_DESC = mat_desc
					,@UOM = unit_of_measure_cd
					,@SPECIAL_PROC_TYPE = special_procurement_type_cd
				FROM #csMatMasterResult;
			END
		END

		IF @IsError <> 'Y'
		BEGIN
			IF ISNULL(@ri_v_po_no, '') = ''
			BEGIN
				IF OBJECT_ID('tempdb..#SupplierAssignmentResult') IS NOT NULL
				BEGIN
					DROP TABLE #SupplierAssignmentResult
				END;

				CREATE TABLE #SupplierAssignmentResult (
					l_n_status CHAR(1)
					,l_v_supp_cd VARCHAR(10)
					)

				-- by calling common function `C.1.01.02. Supplier Assignment
				INSERT INTO #SupplierAssignmentResult
				EXEC [dbo].[SP_COMMON_GR_SUP_ASSIGNMENT] @USER_ID
					,@PROCESS_ID
					,@ri_v_mat_no
					,@ri_v_part_color_sfx
					,@ri_v_source_type
					,@ri_v_prod_purpose_cd
					,@ri_d_delivery_dt

				IF EXISTS (
						SELECT 1
						FROM #SupplierAssignmentResult
						WHERE l_n_status = 'N'
							AND ISNULL(l_v_supp_cd, '') <> ''
						)
				BEGIN
					SELECT @SUPP_CD = l_v_supp_cd
					FROM #SupplierAssignmentResult
				END
				ELSE
				BEGIN
					SET @IsError = 'Y';
					SET @PARAM1 = 'Material Supplier doesnt exist on Source List Master table';

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()
				END
			END
			ELSE
			BEGIN
				SELECT @SUPP_CD = SUPPLIER_CD
				FROM TB_R_PO_H
				WHERE PO_NO = @ri_v_po_no
					AND ISNULL(DELETION_FLAG, '0') = '0';
			END
		END

		IF @IsError <> 'Y'
		BEGIN
			-- by calling common function C.1.2.1. Get Material Price
			IF OBJECT_ID('tempdb..#MaterialPriceResult') IS NOT NULL
			BEGIN
				DROP TABLE #MaterialPriceResult
			END;

			CREATE TABLE #MaterialPriceResult (
				l_n_status CHAR(1)
				,l_n_mat_price NUMERIC(16, 5)
				,l_v_curr_cd VARCHAR(10)
				)

			INSERT INTO #MaterialPriceResult
			EXEC [dbo].[SP_COMMON_GR_MAT_PRICE] @USER_ID
				,@PROCESS_ID
				,@ri_v_mat_no
				,@ri_v_source_type
				,@ri_v_prod_purpose_cd
				,@ri_v_part_color_sfx
				,@ri_v_packing_type
				,@SUPP_CD
				,'T'
				,@ri_d_delivery_dt

			IF EXISTS (
					SELECT 1
					FROM #MaterialPriceResult
					WHERE l_n_status = 'N'
						AND ISNULL(l_n_mat_price, '0') <> '0'
					)
			BEGIN
				SELECT @MAT_PRICE = l_n_mat_price
				FROM #MaterialPriceResult
			END
			ELSE
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Price data not found in master table.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()

				--register to manifest detail
				EXEC [dbo].[SP_COMMON_GR_MANIFEST_LOG] @USER_ID
					,@ri_v_reff_no
					,@ri_v_mat_no
					,4
					,@PARAM1
			END
		END

		IF @IsError <> 'Y'
		BEGIN
			-- by calling common function `C.1.02.02. Get Purchasing Parameters`
			IF OBJECT_ID('tempdb..#PurchasingParametersResult') IS NOT NULL
			BEGIN
				DROP TABLE #PurchasingParametersResult
			END;

			CREATE TABLE #PurchasingParametersResult (
				l_n_status CHAR(1)
				,ro_v_supp_name VARCHAR(100)
				,ro_v_curr VARCHAR(100)
				,ro_v_payment_term VARCHAR(100)
				,ro_v_payment_method VARCHAR(100)
				,ro_v_calculation_schema_cd VARCHAR(100)
				)

			INSERT INTO #PurchasingParametersResult
			EXEC [dbo].[SP_COMMON_GR_GET_PURCHASING_PARAM] @USER_ID
				,@PROCESS_ID
				,@SUPP_CD
				,@ri_d_delivery_dt

			IF EXISTS (
					SELECT 1
					FROM #PurchasingParametersResult
					WHERE l_n_status = 'N'
						AND ISNULL(ro_v_supp_name, '') <> ''
					)
			BEGIN
				SELECT @CALC_SCHEMA_CD = ro_v_calculation_schema_cd
					,@CURR_CD = ro_v_curr
					,@PAYMENT_TERM_CD = ro_v_payment_term
					,@PAYMENT_METHOD_CD = ro_v_payment_method
				FROM #PurchasingParametersResult
			END
			ELSE
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Supplier detail data for Supplier ' + @SUPP_CD + ' and Ref Date ' + CONVERT(VARCHAR, @ri_d_delivery_dt, 112) + ', doesnt exist on Supplier Master table';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()

				--register to manifest detail
				EXEC [dbo].[SP_COMMON_GR_MANIFEST_LOG] @USER_ID
					,@ri_v_reff_no
					,@ri_v_mat_no
					,6
					,'Supplier detail data not found in master table.'
			END
		END

		IF OBJECT_ID('tempdb..#TB_T_COMP_PRICE') IS NOT NULL
		BEGIN
			DROP TABLE #TB_T_COMP_PRICE
		END;

		CREATE TABLE #TB_T_COMP_PRICE (
			CALCULATION_SCHEMA_CD VARCHAR(4)
			,SEQ_NO NUMERIC(3)
			,COMP_PRICE_CD VARCHAR(4)
			,COMP_PRICE_TEXT VARCHAR(40)
			,CALCULATION_TYPE VARCHAR(1)
			,PLUS_MINUS_FLAG VARCHAR(1)
			,INVENTORY_FLAG VARCHAR(1)
			,CONDITION_CATEGORY VARCHAR(1)
			,BASE_VALUE_F NUMERIC(5)
			,BASE_VALUE_T NUMERIC(5)
			,CONDITION_RULE VARCHAR(1)
			,ACCRUAL_FLAG_TYPE VARCHAR(1)
			,COMP_PRICE_RATE NUMERIC(16)
			,TAX_CD VARCHAR(4)
			,SUPP_CD_DELIVERY VARCHAR(40)
			,PRICE NUMERIC(16, 2)
			)

		IF @IsError <> 'Y'
		BEGIN
			-- Get Component Price
			INSERT INTO #TB_T_COMP_PRICE
			SELECT CALCULATION_SCHEMA_CD
				,SEQ_NO
				,COMP_PRICE_CD
				,COMP_PRICE_TEXT
				,CALCULATION_TYPE
				,PLUS_MINUS_FLAG
				,INVENTORY_FLAG
				,CONDITION_CATEGORY
				,BASE_VALUE_F
				,BASE_VALUE_T
				,CONDITION_RULE
				,ACCRUAL_FLAG_TYPE
				,0 COMP_PRICE_RATE
				,NULL TAX_CD
				,NULL SUPP_CD_DELIVERY
				,0 PRICE
			FROM TB_M_COMP_PRICE
			WHERE CALCULATION_SCHEMA_CD = @CALC_SCHEMA_CD
		END

		IF @IsError <> 'Y'
		BEGIN
			--- Check PO existence (by Supplier)
			-------------------------------------------------------------------------------------------------
			--- PO LOCAL CREATION
			--- Read PO Data Source (variable)
			IF ISNULL(@PO_NO, '') = ''
			BEGIN
				IF OBJECT_ID('tempdb..#POBySupplierResult') IS NOT NULL
				BEGIN
					DROP TABLE #POBySupplierResult
				END;

				CREATE TABLE #POBySupplierResult (
					PO_NO VARCHAR(100)
					,SUPP_CD VARCHAR(100)
					,PROD_MONTH VARCHAR(100)
					,STATUS INT
					)

				INSERT INTO #POBySupplierResult
				EXEC [dbo].[SP_COMMON_GR_GET_PO_BY_SUPPLIER] @USER_ID
					,@PROCESS_ID
					,@SUPP_CD
					,@ri_d_delivery_dt
					,''

				IF EXISTS (
						SELECT 1
						FROM #POBySupplierResult
						WHERE STATUS = 0
						)
				BEGIN
					SELECT @PO_NO = PO_NO
					FROM #POBySupplierResult

					SET @RES = 0;
					SET @FLAG_MATERIAL = 'Y';
				END
				ELSE
				BEGIN
					SET @IsError = 'Y';
					SET @PO_NO = NULL;
				END;
			END
		END

		IF @IsError <> 'Y'
		BEGIN
			IF (ISNULL(@PO_NO, '') <> '')
				OR @RES = 0
			BEGIN
				IF OBJECT_ID('tempdb..#POByMaterialResult') IS NOT NULL
				BEGIN
					DROP TABLE #POByMaterialResult
				END;

				CREATE TABLE #POByMaterialResult (
					PO_NO VARCHAR(1000)
					,PO_ITEM_NO VARCHAR(1000)
					,po_quantity NUMERIC(13, 3)
					,po_quantity_open NUMERIC(13, 3)
					,po_mat_price NUMERIC(16, 5)
					,delivery_dt DATE
					,STATUS INT
					)

				INSERT INTO #POByMaterialResult
				EXEC [dbo].[SP_COMMON_GR_PO_BY_MATERIAL] @USER_ID
					,@PROCESS_ID
					,@ri_v_mat_no
					,@SUPP_CD
					,@ri_v_source_type
					,@PROD_PURPOSE_CD
					,@ri_v_packing_type
					,@ri_v_part_color_sfx
					,@ri_v_plant_cd
					,@ri_v_sloc_cd
					,@ri_d_delivery_dt
					,''

				IF EXISTS (
						SELECT 1
						FROM #POByMaterialResult
						WHERE STATUS = 0
						)
				BEGIN
					SELECT @l_v_po_no = PO_NO
						,@l_v_po_item_no = PO_ITEM_NO
						,@l_n_qty = po_quantity
						,@l_n_po_open_qty = PO_QUANTITY_OPEN
						,@l_n_price = PO_MAT_PRICE
						,@l_n_status = STATUS
					FROM #POByMaterialResult
				END
				--ELSE
				--BEGIN
				--	SET @IsError = 'Y';
				--END;

				DECLARE @tb_s_po_h tb_s_po_h
				DECLARE @tb_s_comp_price tb_s_comp_price

				--IF @IsError <> 'Y'
				--BEGIN
					CREATE TABLE #isResult (Result VARCHAR(10))

					CREATE TABLE #isResultNew (
						Result VARCHAR(10)
						,PO_NO VARCHAR(20)
						,PO_ITEM_NO VARCHAR(20)
						)

					IF (
							ISNULL(@l_v_po_no, '') <> ''
							AND ISNULL(@l_v_po_item_no, '') <> ''
							)
						OR (@l_n_status = 0)
					BEGIN
						INSERT INTO @tb_s_po_h
						SELECT @PO_NO
							,@MAT_NO
							,@SOURCE_TYPE
							,@PROD_PURPOSE_CD
							,@PACKING_TYPE
							,@PLANT_CD
							,@SLOC_CD
							,@PROD_MONTH
							,@QUANTITY
							,@PART_COLOR_SUFFIX
							,@MAT_DESC
							,@UOM
							,@SPECIAL_PROC_TYPE
							,@SUPP_CD
							,@MAT_PRICE
							,@CALC_SCHEMA_CD
							,@PAYMENT_METHOD_CD
							,@PAYMENT_TERM_CD
							,@CURR_CD

						INSERT INTO @tb_s_comp_price
						SELECT CALCULATION_SCHEMA_CD
							,SEQ_NO
							,COMP_PRICE_CD
							,COMP_PRICE_TEXT
							,CALCULATION_TYPE
							,PLUS_MINUS_FLAG
							,INVENTORY_FLAG
							,CONDITION_CATEGORY
							,BASE_VALUE_F
							,BASE_VALUE_T
							,CONDITION_RULE
							,ACCRUAL_FLAG_TYPE
							,COMP_PRICE_RATE
							,TAX_CD
							,SUPP_CD_DELIVERY
							,PRICE
						FROM #TB_T_COMP_PRICE

						-- Update_PO Item PO Detail;
						INSERT INTO #isResult
						EXEC [dbo].[SP_COMMON_GR_UPDATE_PO_ITEM_DETAIL] @tb_s_po_h
							,@tb_s_comp_price
							,@USER_ID
							,@PROCESS_ID
							,@l_v_po_no
							,@l_v_po_item_no
							,@ri_d_delivery_dt
							,@ri_v_flagsts

						IF (
								SELECT COUNT(1)
								FROM #isResult
								WHERE Result = 'Y'
								) = 1
						BEGIN
							SET @IsError = 'Y';
							SET @PARAM1 = 'Unknown exception error found!';

							EXEC dbo.CommonGetMessage 'MSPT00001ERR'
								,@MSG_TXT OUTPUT
								,@N_ERR OUTPUT
								,@PARAM1

							INSERT INTO TB_R_LOG_D (
								PROCESS_ID
								,SEQUENCE_NUMBER
								,MESSAGE_ID
								,MESSAGE_TYPE
								,[MESSAGE]
								,LOCATION
								,CREATED_BY
								,CREATED_DATE
								)
							SELECT @PROCESS_ID
								,(
									SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
									FROM dbo.TB_R_LOG_D
									WHERE PROCESS_ID = @PROCESS_ID
									)
								,'MSPT00001ERR'
								,'ERR'
								,'MSPT00001ERR : ' + @MSG_TXT
								,@LOG_LOCATION
								,@USER_ID
								,GETDATE()

							SET @l_v_po_no = '';
							SET @l_v_po_item_no = '';
						END
						ELSE
						BEGIN
							SET @l_v_po_no = @l_v_po_no;
							SET @l_v_po_item_no = @l_v_po_item_no;
						END
					END
					ELSE
					BEGIN
						INSERT INTO @tb_s_po_h
						SELECT @PO_NO
							,@MAT_NO
							,@SOURCE_TYPE
							,@PROD_PURPOSE_CD
							,@PACKING_TYPE
							,@PLANT_CD
							,@SLOC_CD
							,@PROD_MONTH
							,@QUANTITY
							,@PART_COLOR_SUFFIX
							,@MAT_DESC
							,@UOM
							,@SPECIAL_PROC_TYPE
							,@SUPP_CD
							,@MAT_PRICE
							,@CALC_SCHEMA_CD
							,@PAYMENT_METHOD_CD
							,@PAYMENT_TERM_CD
							,@CURR_CD

						INSERT INTO @tb_s_comp_price
						SELECT CALCULATION_SCHEMA_CD
							,SEQ_NO
							,COMP_PRICE_CD
							,COMP_PRICE_TEXT
							,CALCULATION_TYPE
							,PLUS_MINUS_FLAG
							,INVENTORY_FLAG
							,CONDITION_CATEGORY
							,BASE_VALUE_F
							,BASE_VALUE_T
							,CONDITION_RULE
							,ACCRUAL_FLAG_TYPE
							,COMP_PRICE_RATE
							,TAX_CD
							,SUPP_CD_DELIVERY
							,PRICE
						FROM #TB_T_COMP_PRICE

						-- Update_PO Item PO Detail;
						INSERT INTO #isResultNew
						EXEC [dbo].[SP_COMMON_GR_CREATE_NEW_PO_ITEM_DETAIL] @tb_s_po_h
							,@tb_s_comp_price
							,@USER_ID
							,@PROCESS_ID
							,@ri_v_po_no
							,@l_v_po_item_no
							,@ri_d_delivery_dt
							,@ri_v_flagsts

						IF (
								SELECT COUNT(1)
								FROM #isResultNew
								WHERE Result = 'Y'
								) = 1
						BEGIN
							SET @IsError = 'Y';
							SET @PARAM1 = 'Unknown exception error found!';

							EXEC dbo.CommonGetMessage 'MSPT00001ERR'
								,@MSG_TXT OUTPUT
								,@N_ERR OUTPUT
								,@PARAM1

							INSERT INTO TB_R_LOG_D (
								PROCESS_ID
								,SEQUENCE_NUMBER
								,MESSAGE_ID
								,MESSAGE_TYPE
								,[MESSAGE]
								,LOCATION
								,CREATED_BY
								,CREATED_DATE
								)
							SELECT @PROCESS_ID
								,(
									SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
									FROM dbo.TB_R_LOG_D
									WHERE PROCESS_ID = @PROCESS_ID
									)
								,'MSPT00001ERR'
								,'ERR'
								,'MSPT00001ERR : ' + @MSG_TXT
								,@LOG_LOCATION
								,@USER_ID
								,GETDATE()

							SET @l_v_po_no = '';
							SET @l_v_po_item_no = '';
						END
						ELSE
						BEGIN
							SELECT @l_v_po_no = PO_NO
								,@l_v_po_item_no = PO_ITEM_NO
							FROM #isResultNew
						END
					END

					DROP TABLE #isResult

					DROP TABLE #isResultNew
				--END
			END
			ELSE
			BEGIN
				PRINT 'need implement?'
					-- (PO for the Supplier doesn't exist)
					-- Create New PO Data
					--   create_new_po_data(l_v_po_no, l_v_po_item_no, l_v_msg_out1); -- (for the Supplier)           see Create_New_PO for details..
					--   IF l_v_msg_out1 IS NULL
					--   THEN
					--      IF (l_v_po_no IS NULL)
					--         OR (l_v_po_item_no IS NULL)
					--      THEN
					--         l_n_return   := 1;
					--         ro_v_message := PKG_00_COMMON_00.fn_get_message('MICS0438BERR',
					--                                                         NULL,
					--                                                         NULL,
					--                                                         NULL,
					--                                                         NULL,
					--                                                         NULL);
					--         ri_v_po_no   := l_v_po_no;
					--         ro_po_item   := l_v_po_item_no;
					--         l_b_check := TRUE;
					--         RAISE END_PROCESS;
					--      ELSE
					--         l_n_return   := 0;
					--         ro_v_message := NULL;
					--         ri_v_po_no   := l_v_po_no;
					--         ro_po_item   := l_v_po_item_no;
					--         RAISE END_PROCESS;
					--      END IF;
					--   ELSE
					--      ro_v_message := l_v_msg_out1;
					--      RETURN 1;
					--   END ;
			END;
		END

		INSERT INTO TB_S_RESULT_PO
		SELECT @PROCESS_ID
			,@IsError Result
			,@l_v_po_no PO_NO
			,@l_v_po_item_no PO_ITEM_NO
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		--register to manifest detail
		EXEC [dbo].[SP_COMMON_GR_MANIFEST_LOG] @USER_ID
			,@ri_v_reff_no
			,@ri_v_mat_no
			,7
			,@PARAM1

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		INSERT INTO TB_S_RESULT_PO
		SELECT @PROCESS_ID
			,'Y' Result
			,'' PO_NO
			,'' PO_ITEM_NO
	END CATCH
END
