/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Insert price
*/

CREATE PROCEDURE [dbo].[SP_DLV_INSERT_PRICE]
@ROUTE_CD varchar(4),
@LP_CD varchar(3),
@PRICE varchar(50),
@INPUT_VALID_FROM DATETIME,
@USERNAME varchar(20),
@PRICE_TYPE char(1)
AS
BEGIN
		--CHECK USER ====================================================================================================
		DECLARE @CHECK_AUTH INT;
		SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE JOB_FUNCTION_ID = 11 AND USERNAME = @USERNAME);
		IF (@CHECK_AUTH=1)BEGIN
		
				--CHECK MONTH
				IF(@INPUT_VALID_FROM >= (SELECT DATEADD(MONTH, -1, CAST(GETDATE() AS DATE)))) BEGIN
					IF(@INPUT_VALID_FROM >= (SELECT DATEADD(MONTH, +1, CAST(GETDATE() AS DATE)))) BEGIN
					SELECT 6 -- NOT GRETER THAN THIS MONTH
					END
					ELSE BEGIN
							BEGIN TRAN
							BEGIN TRY
								DECLARE @VALID_TO_EDIT DATETIME,@VALID_FROM DATETIME
								SET @VALID_TO_EDIT =  (SELECT CONVERT (VARCHAR(25),DATEADD(day,-1, @INPUT_VALID_FROM),101))
								
								IF EXISTS (SELECT 1 FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @ROUTE_CD AND VALID_FROM = @INPUT_VALID_FROM AND VALID_TO = '9999-12-31')
								BEGIN
									--"PK Code is not exist" 
									SELECT 0
								END
								ELSE BEGIN
									IF EXISTS (SELECT 1 FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @ROUTE_CD AND VALID_FROM = @INPUT_VALID_FROM)
									BEGIN
									SELECT 4
									END
									ELSE BEGIN
										
										IF EXISTS (SELECT 1 FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @ROUTE_CD AND VALID_TO = '9999-12-31')
										BEGIN
												--INSERT TB_H_ROUTE_PRICE
														INSERT INTO TB_H_ROUTE_PRICE ([PRICE_TYPE],[ROUTE_CD],[LP_CD],[PRICE],[VALID_FROM],[VALID_TO],[CREATED_BY],[CREATED_DT],[CHANGED_BY],[CHANGED_DT],[OPERATION],[OPERATION_BY],[OPERATION_DT])
														(SELECT [PRICE_TYPE],[ROUTE_CD],[LP_CD],[PRICE],[VALID_FROM],[VALID_TO],[CREATED_BY],[CREATED_DT],[CHANGED_BY],[CHANGED_DT],'Edit',@USERNAME,GETDATE() 
														FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @ROUTE_CD AND VALID_TO = '9999-12-31')
												--UPDATE TB M ROUTE
												
												UPDATE TB_M_ROUTE_PRICE SET VALID_TO = @VALID_TO_EDIT,
												CHANGED_BY = @USERNAME, CHANGED_DT = GETDATE()
												WHERE ROUTE_CD = @ROUTE_CD
												AND VALID_TO = '9999-12-31'
											
												INSERT INTO TB_M_ROUTE_PRICE ([ROUTE_CD]
																		,[LP_CD]
																		,[PRICE]
																		,[VALID_FROM]
																		,[VALID_TO]
																		,[PRICE_TYPE]
																		,[CREATED_BY]
																		,[CREATED_DT])
												VALUES (@ROUTE_CD,@LP_CD,@PRICE,@INPUT_VALID_FROM,
												'9999-12-31',@PRICE_TYPE,@USERNAME,GETDATE())
												
												--CHECK PARENT ROUTE
												--CHECK AND EDIT  ROUTE PRICE PARENT
												--CURSOR
												
												DECLARE @ROUTE_CD_CHILD VARCHAR(50),@CHILD_LP_CD VARCHAR(50),@VALID_FROM_CHILD VARCHAR(50);
												DECLARE C_DRAFT_ROUTE CURSOR FOR
												SELECT ROUTE_CD_CHILD,LP_CD,VALID_FROM FROM TB_M_DLV_DRAFT_ROUTE_PRICE 
												WHERE ROUTE_CD_PARENT = @ROUTE_CD AND VALID_TO = '9999-12-31' AND APPROVED_BY IS NOT NULL
												OPEN C_DRAFT_ROUTE
												
												FETCH NEXT FROM C_DRAFT_ROUTE INTO @ROUTE_CD_CHILD,@CHILD_LP_CD,@VALID_FROM_CHILD
												WHILE @@FETCH_STATUS = 0
												BEGIN
												

												--INSERT TB_H_ROUTE_PRICE
														INSERT INTO TB_H_ROUTE_PRICE ([PRICE_TYPE],[ROUTE_CD],[LP_CD],[PRICE],[VALID_FROM],[VALID_TO],[CREATED_BY],[CREATED_DT],[CHANGED_BY],[CHANGED_DT],[OPERATION],[OPERATION_BY],[OPERATION_DT])
														(SELECT [PRICE_TYPE],[ROUTE_CD],[LP_CD],[PRICE],[VALID_FROM],[VALID_TO],[CREATED_BY],[CREATED_DT],[CHANGED_BY],[CHANGED_DT],'Edit',@USERNAME,GETDATE() 
														FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @ROUTE_CD_CHILD AND LP_CD = @CHILD_LP_CD AND VALID_TO = '9999-12-31')
												--UPDATE TB M ROUTE
												
												 
												IF(@VALID_FROM_CHILD > @INPUT_VALID_FROM)BEGIN
												
												UPDATE TB_M_ROUTE_PRICE SET PRICE = @PRICE,CHANGED_BY = @USERNAME, CHANGED_DT = GETDATE()
												WHERE ROUTE_CD = @ROUTE_CD_CHILD AND LP_CD = @CHILD_LP_CD AND VALID_TO = '9999-12-31'
												
												END
												ELSE BEGIN
												DECLARE @VALID_TO_EDIT2 DATETIME
												SET @VALID_TO_EDIT2 =  (SELECT CONVERT (VARCHAR(25),DATEADD(day,-1, @INPUT_VALID_FROM),101))
												
												UPDATE TB_M_ROUTE_PRICE SET VALID_TO = @VALID_TO_EDIT2,
												CHANGED_BY = @USERNAME, CHANGED_DT = GETDATE()
												WHERE ROUTE_CD = @ROUTE_CD_CHILD --AND LP_CD = @CHILD_LP_CD 
												AND VALID_TO = '9999-12-31'
												
												INSERT INTO TB_M_ROUTE_PRICE ([ROUTE_CD]
																		,[LP_CD]
																		,[PRICE]
																		,[VALID_FROM]
																		,[VALID_TO]
																		,[PRICE_TYPE]
																		,[CREATED_BY]
																		,[CREATED_DT])
												VALUES (@ROUTE_CD_CHILD,@CHILD_LP_CD,@PRICE,@INPUT_VALID_FROM,
												'9999-12-31',@PRICE_TYPE,@USERNAME,GETDATE())
												
												
												END
													
													
												FETCH NEXT FROM C_DRAFT_ROUTE INTO @ROUTE_CD_CHILD,@CHILD_LP_CD,@VALID_FROM_CHILD
												END
												CLOSE C_DRAFT_ROUTE
												DEALLOCATE C_DRAFT_ROUTE
												
		

												--CURSOR
												
										END 
										ELSE BEGIN
												INSERT INTO TB_M_ROUTE_PRICE ([ROUTE_CD]
																		,[LP_CD]
																		,[PRICE]
																		,[VALID_FROM]
																		,[VALID_TO]
																		,[PRICE_TYPE]
																		,[CREATED_BY]
																		,[CREATED_DT])
												VALUES (@ROUTE_CD,@LP_CD,@PRICE,@INPUT_VALID_FROM,
												'9999-12-31',@PRICE_TYPE,@USERNAME,GETDATE())
										END
										
										
										
									SELECT 1
									END
								END
							COMMIT
							
							END TRY
							BEGIN CATCH
							--ROLLBACK
							SELECT 4
							SELECT ERROR_MESSAGE()
							END CATCH
							
					END
					
				END
				ELSE BEGIN
				SELECT 5 --MONTH NOT VALID
				END
				
		END
		ELSE BEGIN
		SELECT 3 --NOT VALID AUTHOR
		END
		--CHECK USER ====================================================================================================

END

