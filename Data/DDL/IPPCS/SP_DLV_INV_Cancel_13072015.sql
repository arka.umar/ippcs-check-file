/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Cancel invoice document
*/

CREATE PROCEDURE [dbo].[SP_DLV_INV_Cancel_13072015]
@CREATED_BY VARCHAR (25),
@pid BIGINT
AS
BEGIN --modif by agung 25.07.2014
	--PARAMETER GLOBAL
	DECLARE @MODUL_ID INT = 3, @FUNCTION_ID VARCHAR (6) =  '31405';
	DECLARE @process_status AS INT = 0
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_INV_Cancel'
	DECLARE @step AS VARCHAR(50) = 'Init'
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)

	DECLARE @sts_err AS INT
	
	--CREATE TABLE TEMP	
	CREATE TABLE #DLV_GR_IR(
			PO_NO varchar(11) ,
			PO_ITEM_NO int ,
			SERVICE_DOC_NO varchar(11) ,
			ITEM_NO int ,
			STATUS_CD varchar(2)
	)
	--END CREATE
	
								
	
	--CHECK USER TMMIN ATAU LP
	DECLARE @LP_COUNT as int = (SELECT COUNT(*) FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = (SELECT LEFT(@CREATED_BY,3)))
	IF(@LP_COUNT>0) BEGIN --INI LP USER
		
					--MULAI CANCEL INVOICE DENGAN LP ---------------------------------------------------------------------------------------------
					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;
					-- CREATE LOG
					SET @steps = @na+'.'+@step;
					SET @log = 'Invoice Cancel process starting with LP '+@CREATED_BY;
					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,0;
					
					DECLARE @LP_INV_NO VARCHAR(30), @INV_DT DATE, @LP_CD VARCHAR(4),@SERVICE_DOC VARCHAR(MAX)

					DECLARE BTMP_CUR_1 CURSOR FOR
					SELECT LP_INV_NO, INV_DT, LP_CD, SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid --SELECTION QUERY
					OPEN BTMP_CUR_1;
					FETCH NEXT FROM BTMP_CUR_1 INTO @LP_INV_NO,@INV_DT, @LP_CD, @SERVICE_DOC;

					WHILE @@FETCH_STATUS = 0
					BEGIN
								
								--INSERT TABLE GR_IR_TEMPORARY
								DELETE FROM #DLV_GR_IR
								INSERT INTO #DLV_GR_IR
								SELECT PO_NO, PO_ITEM_NO, SERVICE_DOC_NO, ITEM_NO, STATUS_CD FROM TB_R_DLV_GR_IR 
								WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO);
								--END INSERT TABLE GR_IR_TEMPORARY
								
								BEGIN TRY

									 --CREATE LOG
									SET @steps = @na+'.Process';
									SET @log = 'Update table TB_R_DLV_GR_IR with invoice number '+@LP_INV_NO;
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

									UPDATE TB_R_DLV_GR_IR
										SET STATUS_CD ='1',
											LIV_FLAG = 0 
									WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO)


									-- CREATE LOG
									SET @steps = @na+'.Process';
									SET @log = 'update status to cancel for TB_R_DLV_INV_UPLOAD with invoice number '+@LP_INV_NO;
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

									--DELETE FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO
									UPDATE TB_R_DLV_INV_UPLOAD 
										SET STATUS_CD = '-3'
									WHERE LP_INV_NO = @LP_INV_NO AND PROCESS_ID = @pid

									SET @sts_err = 0

								END TRY
								BEGIN CATCH
									DECLARE @ERROR_MESSAGE VARCHAR(MAX)
							        SET @ERROR_MESSAGE = ERROR_MESSAGE()
									SET @sts_err = 1;
								END CATCH
								
								IF (@sts_err <> 0 ) BEGIN --CHEK JIKA GAGAL
										-- CREATE LOG
										SET @steps = @na+'.Process';
										SET @log = 'Error when updating data';
										EXEC dbo.sp_PutLog @ERROR_MESSAGE, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
										EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;

													
										UPDATE TB_R_DLV_GR_IR
										SET STATUS_CD = (SELECT TOP 1 STATUS_CD FROM #DLV_GR_IR WHERE SERVICE_DOC_NO = @SERVICE_DOC)
										WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM #DLV_GR_IR WHERE SERVICE_DOC_NO = @SERVICE_DOC)


										UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

										RAISERROR ('Error when updating data',16,1)
								RETURN
								END 
								ELSE BEGIN --JIKA BERHASIL DI CANCEL
										UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
								END

					FETCH NEXT FROM BTMP_CUR_1 INTO @LP_INV_NO,@INV_DT, @LP_CD, @SERVICE_DOC
					END;
					CLOSE BTMP_CUR_1;
					DEALLOCATE BTMP_CUR_1;
					
					UPDATE TB_R_DLV_INV_UPLOAD SET PROCESS_ID = NULL  WHERE PROCESS_ID = @pid;

					-- CREATE LOG
					SET @steps = @na+'.Finish';
					SET @log = 'Invoice Cancel process finished';
					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
						
					DROP TABLE #DLV_GR_IR;
					
					--SELESAI CANCEL INVOICE DENGAN LP----------------------------------------------------------------------------------------------------
	END
	ELSE BEGIN --INI TMMIN USER
					
					--MULAI CANCEL INVOICE DENGAN USER TMMIN ---------------------------------------------------------------------------------------------
					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;
					-- CREATE LOG
					SET @steps = @na+'.'+@step;
					SET @log = 'Invoice Cancel process starting with TMMIN User '+@CREATED_BY;
					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,0;
					
					DECLARE @LP_INV_NO_2 VARCHAR(30), @INV_DT_2 DATE, @LP_CD_2 VARCHAR(4),@SERVICE_DOC_2 VARCHAR(MAX)

					DECLARE BTMP_CUR_1 CURSOR FOR
					SELECT LP_INV_NO, INV_DT, LP_CD, SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid --SELECTION QUERY
					OPEN BTMP_CUR_1;
					FETCH NEXT FROM BTMP_CUR_1 INTO @LP_INV_NO_2,@INV_DT_2, @LP_CD_2, @SERVICE_DOC_2;

					WHILE @@FETCH_STATUS = 0
					BEGIN
								
								--INSERT TABLE GR_IR_TEMPORARY
								DELETE FROM #DLV_GR_IR
								INSERT INTO #DLV_GR_IR
								SELECT PO_NO, PO_ITEM_NO, SERVICE_DOC_NO, ITEM_NO, STATUS_CD FROM TB_R_DLV_GR_IR 
								WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO_2);
								--END INSERT TABLE GR_IR_TEMPORARY
								
								BEGIN TRY

									 --CREATE LOG
									SET @steps = @na+'.Process';
									SET @log = 'Update table TB_R_DLV_GR_IR with invoice number '+@LP_INV_NO_2;
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

									UPDATE TB_R_DLV_GR_IR
										SET STATUS_CD ='1',
											LIV_FLAG = 0
									WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO_2)


									-- CREATE LOG
									SET @steps = @na+'.Process';
									SET @log = 'update status to cancel for TB_R_DLV_INV_UPLOAD with invoice number '+@LP_INV_NO_2;
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID;

									--DELETE FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO_2
									UPDATE TB_R_DLV_INV_UPLOAD 
										SET STATUS_CD = '-3'
									WHERE LP_INV_NO = @LP_INV_NO_2 AND PROCESS_ID = @pid

									SET @sts_err = 0

								END TRY
								BEGIN CATCH
									DECLARE @ERROR_MESSAGE_2 VARCHAR(MAX)
							        SET @ERROR_MESSAGE_2 = ERROR_MESSAGE()
									SET @sts_err = 1;
								END CATCH
								
								IF (@sts_err <> 0 ) BEGIN --CHEK JIKA GAGAL
										-- CREATE LOG
										SET @steps = @na+'.Process';
										SET @log = 'Error when updating data';
										EXEC dbo.sp_PutLog @ERROR_MESSAGE_2, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
										EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;

													
										UPDATE TB_R_DLV_GR_IR
										SET STATUS_CD = (SELECT TOP 1 STATUS_CD FROM #DLV_GR_IR WHERE SERVICE_DOC_NO = @SERVICE_DOC_2)
										WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM #DLV_GR_IR WHERE SERVICE_DOC_NO = @SERVICE_DOC_2)


										UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

										RAISERROR ('Error when updating data',16,1)
								RETURN
								END 
								ELSE BEGIN
										UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
								END

					FETCH NEXT FROM BTMP_CUR_1 INTO @LP_INV_NO,@INV_DT, @LP_CD, @SERVICE_DOC
					END;
					CLOSE BTMP_CUR_1;
					DEALLOCATE BTMP_CUR_1;
					
					UPDATE TB_R_DLV_INV_UPLOAD SET PROCESS_ID = NULL  WHERE PROCESS_ID = @pid;

					-- CREATE LOG
					SET @steps = @na+'.Finish';
					SET @log = 'Invoice Cancel process finished';
					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
						
					DROP TABLE #DLV_GR_IR;
					
					--SELESAI CANCEL INVOICE DENGAN USER TMMIN----------------------------------------------------------------------------------------------------
	END		
		
END

