/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Validate PO before create
*/

CREATE PROCEDURE [dbo].[SP_DLV_CHECK_PO] 
@USER varchar(30), 
@PROD_MONTH varchar(11),
@pid BIGINT
AS
BEGIN
	--DEKLARASI MODUL DAN FUNCTION
	DECLARE @MODUL_ID INT, @FUNCTION_ID VARCHAR (6); 
	SET @MODUL_ID = 3;
    SET @FUNCTION_ID = '31202';
	--END ---------------------------
	
	--LOOPING BERDASARKAN PO NO
	--CREATE TABLE #TEMP(
	--		PR_NO VARCHAR(MAX),
	--		PR_STATUS VARCHAR(MAX)
	--	);
	--INSERT INTO #TEMP
	--SELECT PR.PR_NO,SY.SYSTEM_VALUE 
	--FROM TB_R_DLV_PR_H PR 
	--	LEFT JOIN TB_M_SYSTEM SY ON SY.SYSTEM_CD = PR.PR_STATUS_FLAG 
	--WHERE PR.PR_STATUS_FLAG = 'PR5' AND PROCESS_ID = @pid
	CREATE TABLE #TEMP(
			PR_NO VARCHAR(MAX),
			PR_STATUS VARCHAR(MAX),
			PO_NO VARCHAR(MAX)
		);
	INSERT INTO #TEMP
	SELECT PRD.PR_NO, SY.SYSTEM_VALUE, PRD.PO_NO
	FROM TB_R_DLV_PR_D PRD 
		INNER JOIN TB_R_DLV_PR_H PRH ON PRH.PR_NO = PRD.PR_NO AND PRH.PR_STATUS_FLAG = 'PR5'
		LEFT JOIN TB_M_SYSTEM SY ON SY.SYSTEM_CD = PRH.PR_STATUS_FLAG
	WHERE PRD.PR_ITEM_NO = (SELECT TOP 1 PRDS.PR_ITEM_NO FROM TB_R_DLV_PR_D PRDS 
								WHERE PRDS.PR_NO = PRD.PR_NO ORDER BY PO_NO DESC) 
		AND PROCESS_ID = @pid

		----==Temporary==--
		----  == VARIABLE TABLE TEMPORARY UNTUK EXPLODING GRID ==
		--DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
		--DECLARE @MIN_ROW INT;
		--DECLARE @ROW_DETAIL VARCHAR(MAX);
		--DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
		--DECLARE @MIN_FIELD INT;
		---- ================================================================
		----  == Variabel Untuk Menyimpan Data Ke TABLE DETAIL ==
		--DECLARE @arr_1 VARCHAR(MAX),@arr_2 VARCHAR(MAX)
			
		--INSERT INTO @ROWS
		--SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@PR_NO,';');
		---- ================================================================
		----  == Ambil Nilai terkecil dari VARIABEL TABLE @ROW ==
		--SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

		--WHILE @MIN_ROW IS NOT NULL BEGIN
		--			-- 6.3.1 == Hapus VARIABLE TABLE @FIELDS jika ada ==
		--			DELETE FROM @FIELDS;
					
		--			SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
		--			INSERT INTO @FIELDS
		--			SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'|');
		--			-- ========================================================================================
		--			-- 6.3.3 == Ambil ITEM Berdasarkan ID untuk dimasukkan kedalam VARIABLE FIELD Detail ==
		--			SET @arr_1 = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
		--			SET @arr_2 = (SELECT ITEM FROM @FIELDS WHERE ID = 2);
		--			INSERT INTO #TEMP VALUES (@arr_1,@arr_2)

		--			SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
		--							-- =================================================================================
		--END --END WHILE
		--==Temporary==--
		
		--MODIF BY AGUNG 11.07.2014
		--CHECKING AUTH USER
		--Modified by FID.Reggy as Requested from Yanes H Sui
		DECLARE @DIVISION VARCHAR(MAX) = ''
		SELECT @DIVISION = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'RTP_PO' AND SYSTEM_CD = 'DIVISION_ID'

		DECLARE @T_DIV TABLE (DIV_ID VARCHAR(20))

		INSERT INTO @T_DIV
		SELECT s FROM [dbo].[fnSplitString](@DIVISION, ';')

		DECLARE @CHECK_AUTH INT;
		--SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE DIVISION_ID = 19 AND USERNAME = @USER);
		SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE DIVISION_ID IN (SELECT DIV_ID FROM @T_DIV) AND USERNAME = @USER);
		IF (@CHECK_AUTH > 0)BEGIN
		
		
						EXEC dbo.sp_PutLog 'Checking Production Month', @USER, 'SP_DLV_Create_PO.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
						DECLARE @CHECK_PROD_MONTH VARCHAR(12) --CHECK PRODUCTION MONTH
						DECLARE @NEXT_MONTH VARCHAR(12) -- CHECK NEXT MONTH
						
						SET @CHECK_PROD_MONTH = (SELECT REPLACE(CONVERT(VARCHAR(7),GETDATE(),111),'/',''));
						SET @NEXT_MONTH = ( SELECT REPLACE(CONVERT (VARCHAR (7), DATEADD(mm, 1, GETDATE()),111),'/','' ) );
						
						IF (@CHECK_PROD_MONTH = @PROD_MONTH OR @NEXT_MONTH = @PROD_MONTH) BEGIN
						
									--CHECK LOGISTIC TRUCKING SCHEME
									EXEC dbo.sp_PutLog 'Checking Trucking Scheme', @USER, 'SP_DLV_Create_PO.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
													
									DECLARE @CHECK_TEMP VARCHAR(10),@CHECK_SCHEME VARCHAR(10)
									SET @CHECK_TEMP = (SELECT COUNT(PR_NO) FROM #TEMP);
									SET @CHECK_SCHEME =(SELECT COUNT(PRH.PR_NO) FROM TB_R_DLV_PR_H PRH JOIN TB_M_TRUCKING_SCHEME S ON PRH.LP_CD = S.LP_CD WHERE PRH.PR_NO IN(SELECT PR_NO FROM #TEMP));
									IF (@CHECK_TEMP = @CHECK_SCHEME) BEGIN
										
										DECLARE @CHECK_PRICE VARCHAR(10),@CHECK_TEMP_PRICE VARCHAR(10)
										--CREATE TABLE TEMPORARY AND CHECKING ROUTE PRICE
										EXEC dbo.sp_PutLog 'Checking Price', @USER, 'SP_DLV_Create_PO.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
										CREATE TABLE #TEMP2( LP_CD VARCHAR(MAX));
										INSERT INTO #TEMP2 (LP_CD) SELECT MAX(PRH.LP_CD) FROM TB_R_DLV_PR_H PRH JOIN TB_M_ROUTE_PRICE PRICE ON PRICE.LP_CD = PRH.LP_CD
										WHERE PRH.PR_NO IN(SELECT PR_NO FROM #TEMP) GROUP BY PRH.LP_CD
										
										SET @CHECK_PRICE = (SELECT COUNT(LP_CD) FROM #TEMP2); 
										SET @CHECK_TEMP_PRICE = (SELECT COUNT(PR_NO) FROM #TEMP);
										
										--IF (@CHECK_PRICE = @CHECK_TEMP_PRICE) BEGIN
											
											DECLARE @CHK_STATUS VARCHAR(MAX) -- CHECK STATUS
											SET @CHK_STATUS = (SELECT COUNT(PR_STATUS) FROM #TEMP WHERE PR_STATUS ='PR Released' AND PO_NO IS NULL);
											IF (@CHK_STATUS = @CHECK_TEMP) BEGIN
											
												
												
														DECLARE @PR_NUMBER VARCHAR(MAX),@PR_STATUS VARCHAR(MAX) --CURSOR WHILE #TEMP
														DECLARE BTMP_CUR_1 CURSOR FOR
														SELECT PR_NO,PR_STATUS FROM #TEMP;

														OPEN BTMP_CUR_1;
														FETCH NEXT FROM BTMP_CUR_1 INTO
														@PR_NUMBER,@PR_STATUS;

														WHILE @@FETCH_STATUS = 0
														BEGIN
														
																				BEGIN TRY
																						--================CREATE PO============================
																						EXEC SP_DLV_CREATE_PO @PR_NUMBER,@USER,@PROD_MONTH,@pid --||||||||||||
																						--================CREATE PO============================
																						UPDATE TB_R_DLV_PO_D SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
																						UPDATE TB_R_DLV_PO_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
																						UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
																						--UPDATE QUEUE
																						UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS ='QU3',PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
																						--END UPDATE QUEUE
																						EXEC dbo.sp_PutLog 'PO creation has been finished', @USER, 'SP_DLV_Create_PO.finish', @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID;
																						
																						
																				END TRY
																				BEGIN CATCH
																						DECLARE @ERROR VARCHAR(MAX)
																						SET @ERROR = ERROR_MESSAGE();
																						RAISERROR('Several PO successfully created,but several PR failed to create PO.',16,1)
																						-- ======= UPDATE DLV_QUEUE DENGAN KONDISI ERORR ==============================
																						UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT= GETDATE() WHERE PROCESS_ID = @pid
																						-- ======= END UPDATE DLV QUEUE ===============================================
																						EXEC dbo.sp_PutLog @ERROR, @USER, 'SP_DLV_Create_PO.process', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
																						EXEC dbo.sp_PutLog 'Several PO successfully created,but several PR failed to create PO.', @USER, 'SP_DLV_Create_PO.finish', @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID;
																								
																						-- UNTUK SESUAI PROCESS ID
																						DELETE FROM TB_R_DLV_PO_D WHERE PROCESS_ID = @pid
																						DELETE FROM TB_R_DLV_PO_ITEM WHERE PO_NO = (SELECT TOP 1 PO_NO FROM TB_R_DLV_PO_H WHERE PROCESS_ID = @pid)
																						DELETE FROM TB_R_DLV_PO_H WHERE PROCESS_ID = @pid
																						UPDATE TB_R_DLV_PR_D SET PO_NO='',PO_FLAG = '1' WHERE PO_NO = (SELECT TOP 1 PO_NO FROM TB_R_DLV_PO_H WHERE PROCESS_ID = @pid)
																						UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
																						
																						
																				END CATCH
														
														FETCH NEXT FROM BTMP_CUR_1 INTO @PR_NUMBER,@PR_STATUS
														END;
														CLOSE BTMP_CUR_1;
														DEALLOCATE BTMP_CUR_1;
														DROP TABLE #TEMP;
												
											END
												ELSE BEGIN
												RAISERROR('Create PO failed with current PR, PR not in released status or PO was created.',16,1)
												-- ======= UPDATE DLV_QUEUE DENGAN KONDISI ERORR ==============================
												UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT= GETDATE() WHERE PROCESS_ID = @pid
												UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
												-- ======= END UPDATE DLV QUEUE ===============================================
												EXEC dbo.sp_PutLog 'Create PO failed with current PR, PR not in released status or PO was created', @USER, 'SP_DLV_Create_PO.finish', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
														
											END
												
										--END
										--ELSE BEGIN
										--	-- PRICE NOT FOUND
										--	RAISERROR('Price Not Found',16,1)
										--	-- ======= UPDATE DLV_QUEUE DENGAN KONDISI ERORR ==============================
										--	UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT= GETDATE() WHERE PROCESS_ID = @pid
										--	-- ======= END UPDATE DLV QUEUE ===============================================
										--	EXEC dbo.sp_PutLog 'Price Not Found', @USER, 'SP_DLV_Create_PO.finish', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
											
										--END
										
										
									END
									ELSE BEGIN
										RAISERROR('Logistic Partner Code not found in Trucking Scheme',16,1)
												-- ======= UPDATE DLV_QUEUE DENGAN KONDISI ERORR ==============================
												UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT= GETDATE() WHERE PROCESS_ID = @pid
												UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
												-- ======= END UPDATE DLV QUEUE ===============================================
										EXEC dbo.sp_PutLog 'Logistic Partner Code not found in Trucking Scheme', @USER, 'SP_DLV_Create_PO.finish', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
										
									END
						END
						ELSE BEGIN
									RAISERROR('PO cannot create with this production month',16,1)
											-- ======= UPDATE DLV_QUEUE DENGAN KONDISI ERORR ==============================
											UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT= GETDATE() WHERE PROCESS_ID = @pid
											UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
											-- ======= END UPDATE DLV QUEUE ===============================================
									EXEC dbo.sp_PutLog 'PO cannot create with this production month', @USER, 'SP_DLV_Create_PO.finish', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;
									
						END
		END
		ELSE BEGIN
			RAISERROR('You are not authorized to perform this action.',16,1)
					-- ======= UPDATE DLV_QUEUE DENGAN KONDISI ERORR ==============================
					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT= GETDATE() WHERE PROCESS_ID = @pid
					UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
					-- ======= END UPDATE DLV QUEUE ===============================================
			EXEC dbo.sp_PutLog 'You are not authorized to perform this action.', @USER, 'SP_DLV_Create_PO.finish', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID;		
		END
		
		
END

