-- =============================================
-- Author:		fid.deny
-- Create date: 13 April 2017
-- Description:	job for auto approve goods receipt (replacing sh approve button at goodreceiptinquiry)
-- =============================================
ALTER PROCEDURE [dbo].[SP_AUTOAPPROVEGR_IPPCS] 
AS
BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#TEMPMANIFEST') IS NOT NULL 
	DROP TABLE #TEMPMANIFEST
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL 
	DROP TABLE #TEMP
	IF OBJECT_ID('tempdb..#pid') IS NOT NULL 
	DROP TABLE #pid

    DECLARE  @totalRecord INT = 0,
		@startFrom INT = 1,
		@startTo INT = 0,
		@limit INT = 0,
		@pid VARCHAR(20),
		@manifestQueue varchar(max),
		@logManifest VARCHAR(MAX)
	    

	CREATE TABLE #pid (pid VARCHAR(20))

	--[get row per process]
	SELECT @startTo = SYSTEM_VALUE, @limit = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'MAX_GR_PERPROCESS' AND FUNCTION_ID = '42001'
	
	--[fetch data to process]
	SELECT 
		ROW_NUMBER() OVER ( ORDER BY M.RECEIVE_DT ASC) AS NUM, MANIFEST_NO, DOCK_CD, SUPPLIER_CD, SUPPLIER_PLANT, ORDER_NO INTO #TEMP
	FROM
		TB_R_DAILY_ORDER_MANIFEST M
	WHERE 
		M.MANIFEST_RECEIVE_FLAG IN ('2') AND
		((ISNULL(M.CANCEL_FLAG, 0) = 0) OR (ISNULL(M.CANCEL_FLAG, '') = '')) AND
		((ISNULL(M.DELETION_FLAG, 0) = 0) OR (ISNULL(M.DELETION_FLAG, '') = '')) AND 
		((ISNULL(PROBLEM_FLAG, 0) = 0) OR (ISNULL(PROBLEM_FLAG, '') = '')) AND 
		((ISNULL(ICS_FLAG, 0) = 0) OR (ISNULL(ICS_FLAG, '') = '')) AND
		M.TOTAL_QTY > 0 AND M.ORDER_RELEASE_DT > '2017-03-24'
		--for testing only
		--AND MANIFEST_NO IN ('1151104240', '1151104248', '1151104306','1151104348','1151104375', '1151105018','1151105107')

	--[count total record]
	SELECT @totalRecord = COUNT(1) FROM #TEMP
	
	IF(@totalRecord > 0)
	BEGIN
		WHILE (@startFrom <= @totalRecord)
		BEGIN
			--update tb_r_daily_order_manifest
			UPDATE OM
			SET 
				OM.MANIFEST_RECEIVE_FLAG = '3',
				OM.APPROVED_BY = CASE WHEN APPROVED_BY IS NULL
								THEN 'ippcs.admin'
								ELSE APPROVED_BY
								END,
				OM.APPROVED_DT = CASE WHEN APPROVED_DT IS NULL
								THEN GETDATE()
								ELSE APPROVED_DT
								END,
				OM.IN_PROGRESS = '1',
				OM.CHANGED_BY = 'ippcs.admin',
				OM.CHANGED_DT = GETDATE()
			FROM TB_R_DAILY_ORDER_MANIFEST OM
			INNER JOIN #TEMP TMN
			ON OM.MANIFEST_NO = TMN.MANIFEST_NO
			WHERE TMN.NUM BETWEEN @startFrom AND @startTo

			--create PID
			INSERT INTO #pid 
			EXEC sp_PutLog 'Start Process Goods Receipt Posting from IPPCS', 'system.autoapprove', 'Goods Receipt Inquiry',  
						   '',--pid
							'MPCS00004INF', '', '4', '42001', '0'

			SELECT TOP 1 @pid = pid FROM #pid

			INSERT INTO #pid 
			EXEC sp_PutLog 'Start Process Generate Data Posting', 'system.autoapprove', 'Goods Receipt Inquiry', 
							@pid,--pid
							'MPCS00004INF', '', '4', '42001', '0'

			--[[generatepostingfilegr]]
			select @manifestQueue = stuff((SELECT ';' +  manifest_no from #TEMP 
			WHERE NUM BETWEEN @startFrom AND @startTo
			FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
			
			--logging manifest
			SELECT @logManifest = 'Start to process manifest : '+ @manifestQueue;

			INSERT INTO #pid 
			EXEC sp_PutLog @logManifest, 'system.autoapprove', 'Goods Receipt Inquiry', 
							@pid,--pid
							'MPCS00004INF', '', '4', '42001', '0'

			--[execute SP for processing manifest]
			exec SP_ICS_GeneratePostingFileGR 'ippcs.admin', @manifestQueue,'', '', '', @pid;

			BEGIN TRY
				--* FID.Ridwan : 2020-09-10 --> new logic GR to ICS *--
				exec [SP_ICS_PostingGRtoICS] 'ippcs.admin', @manifestQueue,'', '', '', @pid;
			END TRY
			BEGIN CATCH
				DECLARE @PARAM VARCHAR(MAX) = CONVERT(VARCHAR(1000), ERROR_MESSAGE());
				INSERT INTO #pid 
				EXEC sp_PutLog @PARAM, 'system.autoapprove', 'Goods Receipt Inquiry', 
							@pid,--pid
							'MPCS00004INF', '', '4', '42001', '0'
			END CATCH

			INSERT INTO #pid 
			EXEC sp_PutLog 'Process Generate Posting File GR is finished', 'system.autoapprove', 'Goods Receipt Inquiry', 
							@pid,--pid
							'MPCS00004INF', '', '4', '42001', '0'

			INSERT INTO #pid 
			EXEC sp_PutLog 'Process Posting GR is finished', 'system.autoapprove', 'Goods Receipt Inquiry', 
							@pid,--pid
							'MPCS00004INF', '', '4', '42001', '0'

			DELETE FROM #pid

			SELECT @startFrom = @startFrom + @limit, @startTo = @startTo + @limit
		END
	END


	IF OBJECT_ID('tempdb..#TEMPMANIFEST') IS NOT NULL 
		DROP TABLE #TEMPMANIFEST
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL 
		DROP TABLE #TEMP
	IF OBJECT_ID('tempdb..#pid') IS NOT NULL 
		DROP TABLE #pid

END

