CREATE FUNCTION [dbo].[FN_VARMONTH]
(
	-- Add the parameters for the function here
	@DATE DATETIME
)
RETURNS VARCHAR(10)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @RETURN VARCHAR(MAX);

	-- Add the T-SQL statements to compute the return value here

	if(LEN(MONTH(@DATE))>1)begin
		SET @RETURN = CONVERT(CHAR(2),MONTH(@DATE));
	end else begin
		SET @RETURN = '0'+CONVERT(varchar(2),MONTH(@DATE));
	end

	-- Return the result of the function
	RETURN @RETURN;

END

