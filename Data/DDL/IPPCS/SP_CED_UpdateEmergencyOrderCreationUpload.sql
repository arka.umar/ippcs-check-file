/****** Object:  StoredProcedure [dbo].[SP_CED_UpdateEmergencyOrderCreationUpload]    Script Date: 9/28/2022 10:40:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[SP_CED_UpdateEmergencyOrderCreationUpload]
(
	@functionID VARCHAR(30),
	@prosesID VARCHAR(30),
	@username VARCHAR(MAX),
	@SessionID VARCHAR(50)
)
AS
SET NOCOUNT ON

IF object_id('tempdb..#TempValidate') IS NOT NULL DROP TABLE #TempValidate

DECLARE 
		@validateRowCount INT,
		@validateRowIndex INT,		
		
		@sql NVARCHAR(MAX),				
		@action VARCHAR(MAX),
		
		@partNo VARCHAR(MAX),
		@supplierCode VARCHAR(10),
		@supplierPlant VARCHAR(5),
		@dockCode VARCHAR(2),
		@orderLotSz VARCHAR(MAX),
		@KanbanNO varchar(MAX),
		
		@KANBAN_PRINT_ADDRESS VARCHAR(max),
		@IMPORTIR_INFO VARCHAR(max),
		@PART_BARCODE VARCHAR(max),
		@PROGRESS_LANE_NO VARCHAR(max),
		@CONVEYANCE_NO VARCHAR(max),
		@PROD_DATE VARCHAR(100) -- 2019-02-12 FID.Ridwan : Change Request 
				
DECLARE @ErrorMessage NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;

/* INSERT temporary validated data table INTO #TempValidate */
SELECT 
	CONVERT(INT, ID) AS SEQ_NO,
	* 
	INTO #TempValidate
FROM TB_T_CED_EMERGENCY_ORDER_VALIDATE
WHERE [FUNCTION_ID] = @functionID AND [PROCESS_ID] = @prosesID 
ORDER BY ID ASC

SELECT  @validateRowIndex = MIN(ID)
FROM    #TempValidate
WHERE [FUNCTION_ID] = @functionID AND [PROCESS_ID] = @prosesID

SELECT  @validateRowCount = MAX(ID)
FROM    #TempValidate
WHERE [FUNCTION_ID] = @functionID AND [PROCESS_ID] = @prosesID

--** Start : Add by FID.Andri (2022-09-28) -> Get Dock of EO
IF OBJECT_ID('tempdb..#tmpSplitStringDock') IS NOT NULL
  DROP TABLE #tmpSplitStringDock

CREATE TABLE #tmpSplitStringDock
( 
	dock_cd VARCHAR(MAX)
)

DECLARE @SYSTEM_VALUE VARCHAR(1000)
SELECT @SYSTEM_VALUE=[SYSTEM_VALUE] FROM [dbo].[TB_M_SYSTEM] WHERE [FUNCTION_ID]='DOCK_PACKING' AND [SYSTEM_CD]='EMERGENCY_ORDER'

INSERT  INTO #tmpSplitStringDock(dock_cd)
EXEC dbo.SplitString @SYSTEM_VALUE, ''
--** End : Add

WHILE @validateRowIndex <= @validateRowCount
    BEGIN
		SELECT  @action = ISNULL([ACTION], ''),
				@supplierCode = ISNULL([SUPPLIER_CD], ''),
				@supplierPlant = ISNULL([SUPPLIER_PLANT], ''),
				@dockCode = UPPER(ISNULL([DOCK_CD], '')),
				@partNo = ISNULL([PART_NO], ''),
				@orderLotSz = ISNULL([ORDER_LOT_SZ], 0),
				@KanbanNo = ISNULL([KANBAN_NO], ''),
				@KANBAN_PRINT_ADDRESS =  ISNULL([KANBAN_PRINT_ADDRESS], ''),
				@IMPORTIR_INFO =  ISNULL([IMPORTIR_INFO], ''),
				@PART_BARCODE =  ISNULL([PART_BARCODE], ''),
				@PROGRESS_LANE_NO =  ISNULL([PROGRESS_LANE_NO], ''),
				@CONVEYANCE_NO =  ISNULL([CONVEYANCE_NO], ''),
				@PROD_DATE = ISNULL([PROD_DATE],'01.01.1900') -- 2019-02-12 FID.Ridwan : Change Request 
		FROM #TempValidate
		WHERE SEQ_NO = @validateRowIndex
		
		SET @sql = 		
        --** Start : Change by FID.Andri (2022-09-28) -> Dock Checking to EO System Master
			--'IF ''' + @dockCode + ''' = ''1N'' ' +    
        /*
		    'IF ''' + @dockCode + ''' = ''1H'' or ''' + @dockCode + ''' = ''1J'' or ''' + @dockCode + ''' = ''1K'' or ' +
				'''' + @dockCode + ''' = ''1L'' or ''' + @dockCode + ''' = ''1M'' or ''' + @dockCode + ''' = ''1N'' or ' +
				'''' + @dockCode + ''' = ''1Q'' or ''' + @dockCode + ''' = ''1U'' or ''' + @dockCode + ''' = ''1V'' or ' +
				'''' + @dockCode + ''' = ''1W'' or ''' + @dockCode + ''' = ''1X'' or ''' + @dockCode + ''' = ''1Y'' or ' +
				'''' + @dockCode + ''' = ''1Z'' or ''' + @dockCode + ''' = ''H1'' or ''' + @dockCode + ''' = ''N1'' or ' +
				'''' + @dockCode + ''' = ''7Z'' ' +
        */
		    'IF ''' + @dockCode + ''' in (select * from #tmpSplitStringDock) ' +
        --** End : Change
				'BEGIN ' +	
				
				'IF NOT EXISTS (
					SELECT *
					FROM TB_T_CED_EMERGENCY_ORDER_PART
					WHERE ' +
					   'SessionID=''' + @SessionID + ''' AND ' +
					   'PART_NO = ''' + @partNo + ''' AND ' +
					   'SUPPLIER_CD = ''' + @supplierCode + ''' AND ' +
					   'SUPPLIER_PLANT = ''' + @supplierPlant + ''' AND ' +
					   'DOCK_CD = ''' + @dockCode + ''' AND ' +
					   'PART_BARCODE = ''' + @PART_BARCODE + '''' +						   
				') ' +
			'BEGIN ' +
				'INSERT INTO TB_T_CED_EMERGENCY_ORDER_PART (' +						   
				'SessionID,' +
				'UserID,' +
				'SUPPLIER_CD,' +
				'SUPPLIER_PLANT,' +
				'DOCK_CD,' +
				'PART_NO,' +
				'PART_NAME,' +
				'KANBAN_NO,' +
				'QTY_PER_CONTAINER,' +
				'ORDER_QTY,' +
				'PIECES_QTY, ' +				
				'KANBAN_PRINT_ADDRESS,' +
				'IMPORTIR_INFO,' +
				'PART_BARCODE,' +
				'PROGRESS_LANE_NO,' +				
				'CONVEYANCE_NO,' +
				'PROD_DATE,' + -- 2019-02-12 FID.Ridwan : Change Request 
				'FUNCTION_ID,' +
				'ID,' +
				'SUBMIT_TYPE) ' + 
				'SELECT' +													   
				'''' + @SessionID + ''', ' +
				'''' + @username + ''', ' +
				'''' + @supplierCode + ''', ' +
				'''' + @supplierPlant + ''', ' +
				'''' + @dockCode + ''', ' +
				'UPPER(PART_NO),' +
				'PART_NAME, ' +
				'KANBAN_NO, ' +
				'KANBAN_QTY, ' +
				@orderLotSz + ', ' +
				'KANBAN_QTY*' +
				'''' + @orderLotSz + ''', ' +
				'''' + @KANBAN_PRINT_ADDRESS + ''', ' +
				'''' + @IMPORTIR_INFO + ''', ' +
				'''' + @PART_BARCODE + ''', ' +
				'''' + @PROGRESS_LANE_NO + ''', ' +
				'''' + @CONVEYANCE_NO + ''', ' +
				'''' + CONVERT(VARCHAR,CONVERT(DATE, @PROD_DATE, 103),23) + ''', ' + -- 2019-02-12 FID.Ridwan : Change Request 
				'''' + @functionID + ''', ' +
				'''' + CONVERT(VARCHAR,@validateRowIndex) + ''', ' +
				'1 ' +
				' FROM TB_M_PART_INFO ' +
			 ' WHERE  
			 (
			 ((PART_NO =''' + @partNo + ''') AND ISNULL(''' + @partNo +''','''') <> '''' AND ISNULL(''' + @KanbanNO +''','''') = '''') OR
			 ((KANBAN_NO = ''' + @KanbanNO + ''') AND ISNULL(''' + @partNo +''','''') = '''' AND ISNULL(''' + @KanbanNO + ''','''') <> '''') OR
			 ((PART_NO = ''' + @partNo + ''' AND KANBAN_NO =''' + @KanbanNO + ''') AND ISNULL(''' + @partNo +''','''') <> '''' AND ISNULL(''' + @KanbanNO + ''','''') <> '''')) 
			 AND SUPPLIER_CD=''' + REPLACE(@supplierCode,' ','') + 
			 ''' AND SUPPLIER_PLANT=''' + REPLACE(@supplierPlant,' ','') + 
			 ''' AND DOCK_CD=''' + @dockCode + ''' ' +					 
		   'END ' +											
					
		'END ' +
				
	'ELSE ' +	
				'BEGIN ' +				
				
				'IF NOT EXISTS (
					SELECT *
					FROM TB_T_CED_EMERGENCY_ORDER_PART
					WHERE ' +
					   'SessionID=''' + @SessionID + ''' AND ' +
					   'PART_NO = ''' + @partNo + ''' AND ' +
					   'SUPPLIER_CD = ''' + @supplierCode + ''' AND ' +
					   'SUPPLIER_PLANT = ''' + @supplierPlant + ''' AND ' +
					   'DOCK_CD = ''' + @dockCode + '''' +					  					   
				') ' +
			'BEGIN ' +
				'INSERT INTO TB_T_CED_EMERGENCY_ORDER_PART (' +						   
				'SessionID,' +
				'UserID,' +
				'SUPPLIER_CD,' +
				'SUPPLIER_PLANT,' +
				'DOCK_CD,' +
				'PART_NO,' +
				'PART_NAME,' +
				'KANBAN_NO,' +
				'QTY_PER_CONTAINER,' +
				'ORDER_QTY,' +
				'PIECES_QTY, ' +
				'PART_BARCODE, ' +
				'PROD_DATE, ' + -- 2019-02-12 FID.Ridwan : Change Request 
				'FUNCTION_ID, ' +
				'ID,' +
				'SUBMIT_TYPE) ' + 
				'SELECT' +													   
				'''' + @SessionID + ''', ' +
				'''' + @username + ''', ' +
				'''' + @supplierCode + ''', ' +
				'''' + @supplierPlant + ''', ' +
				'''' + @dockCode + ''', ' +
				'UPPER(PART_NO),' +
				'PART_NAME, ' +
				'KANBAN_NO, ' +
				'KANBAN_QTY, ' +
				@orderLotSz + ', ' +
				'KANBAN_QTY*' +
				'''' + @orderLotSz + ''',' +
				CONVERT(VARCHAR(18), @validateRowIndex) + ', ' +
				'''' + CONVERT(VARCHAR,CONVERT(DATE, @PROD_DATE, 103),23) + ''', ' + -- 2019-02-12 FID.Ridwan : Change Request
				'''' + @functionID + ''', ' +
				'''' + CONVERT(VARCHAR(18), @validateRowIndex) + ''', ' +
				'1 ' +
				' FROM TB_M_PART_INFO ' +
			 ' WHERE  
			 (
			 ((PART_NO =''' + @partNo + ''') AND ISNULL(''' + @partNo +''','''') <> '''' AND ISNULL(''' + @KanbanNO +''','''') = '''') OR
			 ((KANBAN_NO = ''' + @KanbanNO + ''') AND ISNULL(''' + @partNo +''','''') = '''' AND ISNULL(''' + @KanbanNO + ''','''') <> '''') OR
			 ((PART_NO = ''' + @partNo + ''' AND KANBAN_NO =''' + @KanbanNO + ''') AND ISNULL(''' + @partNo +''','''') <> '''' AND ISNULL(''' + @KanbanNO + ''','''') <> '''')) 
			 AND SUPPLIER_CD=''' + REPLACE(@supplierCode,' ','') + 
			 ''' AND SUPPLIER_PLANT=''' + REPLACE(@supplierPlant,' ','') + 
			 ''' AND DOCK_CD=''' + @dockCode + ''' ' +					 
		   'END ' +		
				
				
				'END'	   
		   
		   
BEGIN TRY
	PRINT @sql
	EXEC (@sql)
END TRY
BEGIN CATCH
		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		return
END CATCH		
								
		SET @validateRowIndex = @validateRowIndex + 1
	END
