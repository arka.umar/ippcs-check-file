/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Register to queue
*/

CREATE PROCEDURE [dbo].[SP_DLV_RegQueue]
	@UserId varchar(20)
AS
BEGIN
	DECLARE @MODULE_ID as varchar(1) = '3'
	DECLARE @FUNCTION_ID as varchar(5) = '31401'
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_Insert_TB_T_DLV_INV_UPLOAD'
	DECLARE @step AS VARCHAR(50)
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @T AS TABLE (PID BIGINT)
	
	SET @log = 'Upload invoice creation started By '+@na;
	SET @step = @na+'.init'
	INSERT @T
	EXEC dbo.sp_PutLog @log, @UserId, @step, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
	
	INSERT INTO TB_R_DLV_QUEUE(PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, PROCESS_END_DT, CREATED_BY, CREATED_DT)
	SELECT @pid as PROCESS_ID, @MODULE_ID, @FUNCTION_ID, 'QU1', GETDATE(), NULL, @UserId, GETDATE()
	
	SELECT @pid as PROCESS_ID
END

