-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetPhysicalDockMapping]
AS
BEGIN
	SELECT [PHYSICAL_LOGPTCD]
		  ,[PHYSICAL_PLANTCD]
		  ,[PHYSICAL_DOCKCD]
		  ,[LOGICAL_LOGPTCD]
		  ,[LOGICAL_PLANTCD]
		  ,[LOGICAL_DOCKCD]
		  ,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
	FROM [dbo].[TB_T_CED_PHYSICAL_DOCK_MAPPING]
END

