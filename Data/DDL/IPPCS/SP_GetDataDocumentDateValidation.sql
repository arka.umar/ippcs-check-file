CREATE PROCEDURE [dbo].[SP_GetDataDocumentDateValidation]
            @@DOCUMENTDATE     varchar(max) 

AS

DECLARE @@TOTAL               int,
		@@GR_DATE             varchar(10),
		@@GR_DATE_NEXT		  varchar(10),
		@@DOCUMENT_DATE_STR   varchar(10),
		@@PROCESS             varchar(max)

		
	CREATE TABLE #tmpDocumentDateTable
	( 
		column1 VARCHAR(8000)
	)

	INSERT  INTO #tmpDocumentDateTable(column1)
	EXEC dbo.SplitString @@DOCUMENTDATE, ';'
	
	DECLARE l_cursor CURSOR LOCAL FAST_FORWARD
	FOR
		SELECT column1
			FROM #tmpDocumentDateTable
			WHERE 1=1
		 	
	OPEN l_cursor FETCH NEXT FROM l_cursor INTO
		@@DOCUMENT_DATE_STR

	BEGIN
		WHILE @@FETCH_STATUS = 0			
		BEGIN
		       IF (@@DOCUMENT_DATE_STR IS NOT NULL AND @@DOCUMENT_DATE_STR !='')
			      BEGIN
					     SELECT @@GR_DATE_NEXT = GR_DATE
						   FROM TB_M_INV_VALIDATION 
						  WHERE REPLACE(SUBSTRING(GR_DATE, 1, 2), ',', '') <= DAY(CAST(@@DOCUMENT_DATE_STR AS DATE)) 
								AND REPLACE(SUBSTRING(GR_DATE, 3, 3), ',', '') >= DAY(CAST(@@DOCUMENT_DATE_STR AS DATE)) 
						
						  IF (@@GR_DATE IS NULL)
						     BEGIN 
							    SET @@GR_DATE = @@GR_DATE_NEXT
								SET @@TOTAL = 1;
							 END 		
					      ELSE
						     BEGIN
							   IF @@GR_DATE IS NOT NULL AND @@GR_DATE_NEXT IS NOT NULL 
							          AND @@GR_DATE = @@GR_DATE_NEXT
							      BEGIN
							        SET @@GR_DATE = @@GR_DATE_NEXT
									SET @@TOTAL = 1;
                                  END  
                               ELSE
							      BEGIN
									SET @@TOTAL = @@TOTAL+1;
								  END 
							 END			                  
				  END		   
			    ELSE
				  BEGIN
				     
				     SET @@PROCESS = 'Document Date tidak boleh kosong atau null'

					-- SELECT @@PROCESS
					 BREAK				
					 
				  END 	  	 
				  	
			FETCH NEXT FROM l_cursor INTO
				@@DOCUMENT_DATE_STR
		END
	END
	
	IF @@PROCESS IS NULL	
	   BEGIN
		IF @@TOTAL > 1
		   BEGIN
			-- SET @@PROCESS = 'Please Selected Data with Date Range 1-15 Or 16-31'
			   SET @@PROCESS = 'Invoice Will Be Paid Based on Last GR & Invoice Submission Date'
		   END
		ELSE
		   BEGIN
			  SET @@PROCESS = 'OK'
		   END
       END
	   
  SELECT @@PROCESS

DROP TABLE #tmpDocumentDateTable

