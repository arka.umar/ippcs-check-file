CREATE FUNCTION [dbo].[FN_TC_DateToString] (
	@ri_d_date datetime
)
RETURNS varchar(12)
AS
BEGIN
	-- format DD-MON HH:MI, exam : 26-Jun 23:00
	return replace(convert(varchar(6), @ri_d_date, 106), ' ', '-') + ' ' + convert(varchar(5), @ri_d_date, 114)
END

