-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2020-09-30
-- Description:	This function for fill parameter output status and message
-- =============================================
CREATE FUNCTION [dbo].[FN_INVOICE_FILL_STATUS_MESSAGE] (
	@ri_n_option INT
	,@ri_v_indicator VARCHAR(50)
	,@ri_v_mon_year VARCHAR(50)
	,@ri_v_per_fiscal VARCHAR(50)
	,@ri_v_date VARCHAR(50)
	)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @Result VARCHAR(100) = ''
		,@r_v_msg VARCHAR(100)
		,@r_v_msg2 VARCHAR(100)
		,@ro_v_status VARCHAR(100)
		,@ro_v_Message VARCHAR(100)
		,@ro_res VARCHAR(1);

	IF @ri_v_indicator = '0'
	BEGIN
		SET @ro_v_status = 'E';

		IF @ri_n_option = 3
			OR @ri_n_option = 4
		BEGIN
			SET @r_v_msg = 'Period Fiscal  ' + @ri_v_per_fiscal;
		END
		ELSE
			IF @ri_n_option = 1
				OR @ri_n_option = 2
			BEGIN
				SET @r_v_msg = 'Month Year  ' + @ri_v_mon_year;
			END
			ELSE
			BEGIN
				SET @r_v_msg = 'Date  ' + @ri_v_date;
			END

		SET @ro_v_Message = 'No Data Found for ' + @r_v_msg;
		SET @ro_res = '0';
	END
	ELSE
		IF @ri_v_indicator = '1'
		BEGIN
			SET @ro_v_status = 'S';
			SET @ro_v_Message = 'Convertion has been done succesfully !';
			SET @ro_res = '1';
		END
		ELSE
			IF @ri_v_indicator = 'A'
			BEGIN
				SET @ro_v_status = 'E';
				SET @r_v_msg = 'Option ';
				SET @ro_v_Message = @r_v_msg + ' Should not be empty';
				SET @ro_res = '0';
			END
			ELSE
				IF @ri_v_indicator = 'B'
				BEGIN
					SET @ro_v_status = 'E';
					SET @r_v_msg = CONVERT(VARCHAR, @ri_n_option);
					SET @ro_v_Message = 'option ' + @r_v_msg + ' is not in the range of 1 ~6';
					SET @ro_res = '0';
				END
				ELSE
					IF @ri_v_indicator = 'C'
					BEGIN
						SET @ro_v_status = 'E';
						SET @r_v_msg = 'month_year ';
						SET @ro_v_Message = @r_v_msg + ' Should not be empty';
						SET @ro_res = '0';
					END
					ELSE
						IF @ri_v_indicator = 'D'
						BEGIN
							SET @ro_v_status = 'E';
							SET @r_v_msg = 'yyyymm';
							SET @r_v_msg2 = 'Month.year = ' + @ri_v_mon_year;
							SET @ro_v_Message = 'Invalid date format for ' + @r_v_msg + ' The format is ' + @r_v_msg2;
							SET @ro_res = '0';
						END
						ELSE
							IF @ri_v_indicator = 'E'
							BEGIN
								SET @ro_v_status = 'E';
								SET @r_v_msg = 'mm.yyyy';
								SET @r_v_msg2 = 'Month.year = ' + @ri_v_mon_year;
								SET @ro_v_Message = 'Invalid date format for ' + @r_v_msg + ' The format is ' + @r_v_msg2;
								SET @ro_res = '0';
							END
							ELSE
								IF @ri_v_indicator = 'F'
								BEGIN
									SET @ro_v_status = 'E';
									SET @r_v_msg = 'Period_Fiscal ';
									SET @ro_v_Message = @r_v_msg + ' Should not be empty';
									SET @ro_res = '0';
								END
								ELSE
									IF @ri_v_indicator = 'G'
									BEGIN
										SET @ro_v_status = 'E';
										SET @r_v_msg = 'ffffpp';
										SET @r_v_msg2 = 'Period Fiscal Year = ' + @ri_v_per_fiscal;
										SET @ro_v_Message = 'Invalid date format for ' + @r_v_msg + ' The format is ' + @r_v_msg2;
										SET @ro_res = '0';
									END
									ELSE
										IF @ri_v_indicator = 'H'
										BEGIN
											SET @ro_v_status = 'E';
											SET @r_v_msg = 'pp.ffff';
											SET @r_v_msg2 = 'Period Fiscal Year = ' + @ri_v_per_fiscal;
											SET @ro_v_Message = 'Invalid date format for ' + @r_v_msg + ' The format is ' + @r_v_msg2;
											SET @ro_res = '0';
										END
										ELSE
											IF @ri_v_indicator = 'I'
											BEGIN
												SET @ro_v_status = 'E';
												SET @r_v_msg = 'Date ';
												SET @ro_v_Message = @r_v_msg + ' Should not be empty';
												SET @ro_res = '0';
											END
											ELSE
												IF @ri_v_indicator = 'J'
												BEGIN
													SET @ro_v_status = 'E';
													SET @r_v_msg = 'dd.mm.yyyy';
													SET @ro_v_Message = @r_v_msg + ' Should not be empty';
													SET @ro_res = '0';
												END

	SET @Result = @ro_res + @ro_v_Message;

	RETURN @Result;
END
