CREATE FUNCTION [dbo].[FN_TC_GetLocTimeOutPlan] (
	@ri_d_dock_time_in_plan datetime,
	@ri_v_from_loc_cd varchar(8),
	@ri_v_to_dock_cd varchar(3)
)
RETURNS datetime
AS
BEGIN
	declare @l_n_lead_time bigint = 0,
		@l_n_buffer bigint = 0

	select top 1 @l_n_lead_time = lt.LEAD_TIME_ID
	from TB_M_TC_LEAD_TIME lt 
	where 1 = 1
		and lt.FROM_LOC_CD = @ri_v_from_loc_cd
		and lt.TO_DOCK_CD = @ri_v_to_dock_cd

	select @l_n_buffer = cast(s.SYSTEM_VALUE as bigint)
	from TB_M_SYSTEM s
	where 1 = 1
		and s.FUNCTION_ID = '35006'
		and s.SYSTEM_CD = 'BUFFER_TIME_TO_DOCK'

	return dateadd(second, @l_n_lead_time + @l_n_buffer, @ri_d_dock_time_in_plan);
END

