CREATE PROCEDURE [dbo].[ReviseKeihenData]
	@process_id varchar(MAX),
	@userid varchar(20)
AS
BEGIN
	DECLARE @p_userid VARCHAR(20) = @userid
	DECLARE @p_createddt DATETIME = GETDATE()
	DECLARE @p_process_id bigint = @process_id
	DECLARE @p_packmonth varchar(6) = ''
	DECLARE @uploaded_packmonth varchar(6) = ''
	DECLARE @p_error_msg VARCHAR(max) = ''

	IF OBJECT_ID('tempdb..#keihen') IS NOT NULL 
			DROP TABLE #keihen

	BEGIN TRY
		BEGIN TRANSACTION ReviseKeihen

		IF(EXISTS(SELECT 'x' FROM TB_T_KEIHEN_UPLOAD_REVISION WHERE PROCESS_ID = @p_process_id))
		BEGIN
			IF(NOT EXISTS(SELECT 'x' FROM TB_T_KEIHEN_UPLOAD_REVISION WHERE PROCESS_ID = @p_process_id AND [ACTION] IN ('1', '2', '3')))
			BEGIN
				SET @p_error_msg = '(WARNING) No Data With Action Insert, Update, or Delete. All data Ignored'
				RAISERROR(@p_error_msg, 16, 1)
			END

			UPDATE KS
				SET KS.CAR_CD = R.CAR_CD,
					KS.N_1_NEW = R.NEW_VOLUME_N_1,
					KS.FLUCTUATION = CASE WHEN R.ORIGINAL_VOLUME_N_1 > 0 THEN
										CONVERT(DECIMAL(30, 2), (CONVERT(DECIMAL(30, 2), (
											CONVERT(INT, R.NEW_VOLUME_N_1) - CONVERT(INT, R.ORIGINAL_VOLUME_N_1)
										)) / 
										CONVERT(DECIMAL(30, 2), R.ORIGINAL_VOLUME_N_1)) * 100)
									 ELSE NULL END,
					KS.CHANGED_BY = @userid,
					KS.CHANGED_DT = GETDATE(),
					KS.REVISION_BY = @userid,
					KS.REVISION_DT = GETDATE(),
					KS.REVISION_PROCESS_ID = @p_process_id
			FROM TB_R_KEIHEN_SUMMARY KS
				JOIN TB_T_KEIHEN_UPLOAD_REVISION R
					ON R.PACK_MONTH = KS.PACK_MONTH
					   AND R.PART_NO = KS.PART_NO
					   AND R.DOCK_CD = KS.DOCK_CD
					   AND R.SUPPLIER_CD = KS.SUPPLIER_CD
					   AND R.S_PLANT_CD = KS.S_PLANT_CD
			WHERE R.PROCESS_ID = @p_process_id
				  AND R.[ACTION] = '2'
				  AND R.DO = 'UPDATE'

			INSERT INTO TB_R_KEIHEN_SUMMARY
				(
					PACK_MONTH,
					PART_NO,
					DOCK_CD,
					SUPPLIER_CD,
					S_PLANT_CD,
					CAR_CD,
					N_1_ORIGINAL,
					N_1_NEW,
					FLUCTUATION,
					CREATED_BY,
					CREATED_DT,
					REVISION_BY,
					REVISION_DT,
					REVISION_PROCESS_ID,
					PART_NAME
				)
			SELECT
				PACK_MONTH,
				PART_NO,
				DOCK_CD,
				SUPPLIER_CD,
				S_PLANT_CD,
				CAR_CD,
				ORIGINAL_VOLUME_N_1,
				NEW_VOLUME_N_1,
				CASE WHEN ORIGINAL_VOLUME_N_1 > 0 THEN
						CONVERT(DECIMAL(30, 2), (CONVERT(DECIMAL(30, 2), (
							CONVERT(INT, R.NEW_VOLUME_N_1) - CONVERT(INT, R.ORIGINAL_VOLUME_N_1)
						)) / 
						CONVERT(DECIMAL(30, 2), ORIGINAL_VOLUME_N_1)) * 100)
					ELSE 0 END as FLUCTUATION,
				@userid,
				GETDATE(),
				@userid,
				GETDATE(),
				@p_process_id,
				PART_NAME
			FROM TB_T_KEIHEN_UPLOAD_REVISION R
			WHERE PROCESS_ID = @p_process_id
				  AND R.[ACTION] = '1'
				  AND R.[DO] = 'INSERT'
				  
			UPDATE KS
				SET KS.REVISION_BY = 'DELETE',
					KS.REVISION_PROCESS_ID = @p_process_id
			FROM TB_R_KEIHEN_SUMMARY KS
				JOIN TB_T_KEIHEN_UPLOAD_REVISION R
					ON R.PACK_MONTH = KS.PACK_MONTH
					   AND R.PART_NO = KS.PART_NO
					   AND R.DOCK_CD = KS.DOCK_CD
					   AND R.SUPPLIER_CD = KS.SUPPLIER_CD
					   AND R.S_PLANT_CD = KS.S_PLANT_CD
			WHERE R.PROCESS_ID = @p_process_id
				  AND R.[ACTION] = '3'

			DELETE FROM TB_R_KEIHEN_SUMMARY 
			WHERE REVISION_BY = 'DELETE' 
				  AND REVISION_PROCESS_ID = @p_process_id
		END
		ELSE
		BEGIN
			SET @p_error_msg = 'Keihen Revision Data Does Not Exists'
			RAISERROR(@p_error_msg, 16, 1)
		END

		DELETE FROM TB_T_KEIHEN_UPLOAD_REVISION where PROCESS_ID = @p_process_id

		COMMIT TRANSACTION ReviseKeihen
	END TRY
	BEGIN CATCH
		IF(@@TRANCOUNT > 0)
			ROLLBACK TRANSACTION ReviseKeihen

		SET @p_error_msg = ERROR_MESSAGE()
		RAISERROR(@p_error_msg, 16, 1)
	END CATCH
END

