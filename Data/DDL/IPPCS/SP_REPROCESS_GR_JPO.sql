
/****** Object:  StoredProcedure [dbo].[SP_REPROCESS_GR_JPO]    Script Date: 30/04/2021 17:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	26-Feb-2021
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
ALTER PROCEDURE [dbo].[SP_REPROCESS_GR_JPO]
AS
BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
		DROP TABLE #TEMP

	IF OBJECT_ID('tempdb..#pid') IS NOT NULL
		DROP TABLE #pid

	DECLARE @totalRecord INT = 0
		,@startFrom INT = 1
		,@startTo INT = 0
		,@limit INT = 0
		,@pid VARCHAR(20)
		,@manifestQueue VARCHAR(max)
		,@logManifest VARCHAR(MAX)
		,@query VARCHAR(MAX)
		,@JPOSrvrName NVARCHAR(256)
		,@JPODBName NVARCHAR(256)

	SELECT @JPOSrvrName = SYSTEM_VALUE
	FROM [TB_M_SYSTEM]
	WHERE FUNCTION_ID = 'IPPCS_TO_JPO'
		AND SYSTEM_CD = 'LINKED_SERVER';

	SELECT @JPODBName = SYSTEM_VALUE
	FROM [TB_M_SYSTEM]
	WHERE FUNCTION_ID = 'IPPCS_TO_JPO'
		AND SYSTEM_CD = 'DB_NAME';

	CREATE TABLE #pid (pid VARCHAR(20))

	--[get row per process]
	SELECT @startTo = SYSTEM_VALUE
		,@limit = SYSTEM_VALUE
	FROM TB_M_SYSTEM
	WHERE SYSTEM_CD = 'MAX_GR_PERPROCESS'
		AND FUNCTION_ID = '42001'

	--[fetch data to process]
	SELECT ROW_NUMBER() OVER (
			ORDER BY A.REF_NO ASC
			) AS NUM
		,A.REF_NO MANIFEST_NO
	INTO #TEMP
	FROM (
		SELECT DISTINCT A.REF_NO
		FROM TB_T_GOOD_RECEIVE A
		JOIN TB_T_GR_JUNBIKI B ON A.REF_NO = B.DELIVERY_NOTE
		WHERE A.SYSTEM_SOURCE = 'JPO'
			AND A.COMPLETE_FLAG = 'N'
		) A
	

	--[count total record]
	SELECT @totalRecord = COUNT(1)
	FROM #TEMP

	IF (@totalRecord > 0)
	BEGIN
		WHILE (@startFrom <= @totalRecord)
		BEGIN
			--create PID
			INSERT INTO #pid
			EXEC sp_PutLog 'Start Re-Process Goods Receipt for JPO'
				,'jpo.autoprocess'
				,'Goods Receipt JPO'
				,''
				,'MPCS00004INF'
				,''
				,'4'
				,'42005'
				,'0'

			SELECT TOP 1 @pid = pid
			FROM #pid

			INSERT INTO #pid
			EXEC sp_PutLog 'Start Process Generate Data Posting'
				,'jpo.autoprocess'
				,'Goods Receipt JPO'
				,@pid
				,'MPCS00004INF'
				,''
				,'4'
				,'42005'
				,'0'

			--[[generatepostingfilegr]]
			SELECT @manifestQueue = stuff((
						SELECT DISTINCT ';' + manifest_no
						FROM #TEMP
						WHERE NUM BETWEEN @startFrom
								AND @startTo
						FOR XML PATH('')
							,TYPE
						).value('.', 'NVARCHAR(MAX)'), 1, 1, '')

			--logging manifest
			SELECT @logManifest = 'Start to process manifest : ' + @manifestQueue;

			UPDATE B
			SET B.NEW_PROCESS_ID = @pid, 
				B.NEW_ROW_NO = A.NUM
			FROM #TEMP A 
			JOIN TB_T_GR_JUNBIKI B ON A.MANIFEST_NO = B.DELIVERY_NOTE
			WHERE NUM BETWEEN @startFrom
					AND @startTo


			INSERT INTO #pid
			EXEC sp_PutLog @logManifest
				,'jpo.autoprocess'
				,'Goods Receipt JPO'
				,@pid
				,--pid
				'MPCS00004INF'
				,''
				,'4'
				,'42005'
				,'0'

			--[execute SP for processing manifest]
			BEGIN TRY
				EXEC [SP_JPO_PrepGRtoICS] 'jpo.autoprocess'
					,@pid;
					
				-- Update send flag to null when refno exists in not complete flag
				INSERT INTO #pid
				EXEC sp_PutLog 'Start update po no to JPO db'
					,'jpo.autoprocess'
					,'Goods Receipt JPO'
					,@pid
					,--pid
					'MPCS00004INF'
					,''
					,'4'
					,'42005'
					,'0'

				UPDATE A
				SET A.SEND_FLAG = NULL, 
					A.CHANGED_BY = 'jpo.autoprocess',
					A.CHANGED_DT = GETDATE()
				FROM TB_T_GOOD_RECEIVE A
				JOIN #TEMP B ON A.REF_NO = B.MANIFEST_NO
				WHERE A.SYSTEM_SOURCE = 'JPO'
					AND NUM BETWEEN @startFrom AND @startTo

				-- Update status from JPO if po no update 
				SET @query = ' 
					INSERT OPENQUERY (' + @JPOSrvrName + ' , ''SELECT MANIFEST_NO
						,SYSTEM_SOURCE
						,SEQ_NO
						,PO_NO
						,MAT_DOC_NO
						,MAT_DOC_YEAR
						,ERR_CD
						,ERR_MESSAGE
						,PROCESS_STS
						,CREATED_BY
						,CREATED_DT 
					FROM ' + @JPODBName + 'TB_T_GR_OTHER_SYSTEM'')  
					SELECT A.MANIFEST_NO
						,''IPPCS'' SYSTEM_SOURCE
						,1 SEQ_NO
						,B.PO_NO
						,NULL MAT_DOC_NO
						,NULL MAT_DOC_YEAR
						,''-'' ERR_CD
						,''Update PO Successfully'' ERR_MESSAGE
						,0 PROCESS_STS
						,''jpo.autoprocess'' CREATED_BY
						,''' + CONVERT(VARCHAR, GETDATE()) + ''' CREATED_DT 
					FROM #TEMP A
					JOIN (SELECT DISTINCT REF_NO, PO_NO FROM TB_T_GOOD_RECEIVE WHERE SYSTEM_SOURCE = ''JPO'') B ON A.MANIFEST_NO = B.REF_NO
					WHERE MANIFEST_NO NOT IN(
						SELECT DISTINCT A.REF_NO
						FROM TB_T_GOOD_RECEIVE A
						JOIN TB_T_GR_JUNBIKI B ON A.REF_NO = B.DELIVERY_NOTE
						WHERE A.SYSTEM_SOURCE = ''JPO''
							AND A.COMPLETE_FLAG = ''N'')
					AND NUM BETWEEN ''' + CONVERT(VARCHAR,@startFrom)  + ''' AND ''' + CONVERT(VARCHAR,@startTo) + '''';

				EXEC (@query);

				SET @QUERY = 'EXEC [' + @JPOSrvrName + '].' + @JPODBName + '[SP_UPDATE_GR_STATUS_FROM_OTHER_SYSTEM] ''IPPCS'''

				EXEC (@QUERY);

			END TRY

			BEGIN CATCH
				DECLARE @PARAM VARCHAR(MAX) = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

				INSERT INTO #pid
				EXEC sp_PutLog @PARAM
					,'jpo.autoprocess'
					,'Goods Receipt JPO'
					,@pid
					,--pid
					'MPCS00004INF'
					,''
					,'4'
					,'42005'
					,'0'
			END CATCH

			INSERT INTO #pid
			EXEC sp_PutLog 'Process Generate Posting File GR is finished'
				,'jpo.autoprocess'
				,'Goods Receipt JPO'
				,@pid
				,--pid
				'MPCS00004INF'
				,''
				,'4'
				,'42005'
				,'0'

			INSERT INTO #pid
			EXEC sp_PutLog 'Process Posting GR is finished'
				,'jpo.autoprocess'
				,'Goods Receipt JPO'
				,@pid
				,'MPCS00004INF'
				,''
				,'4'
				,'42005'
				,'0'

			DELETE
			FROM #pid

			SELECT @startFrom = @startFrom + @limit
				,@startTo = @startTo + @limit
		END
	END

	--Temporary for testing 
	SELECT @pid

	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
		DROP TABLE #TEMP

	IF OBJECT_ID('tempdb..#pid') IS NOT NULL
		DROP TABLE #pid
END
