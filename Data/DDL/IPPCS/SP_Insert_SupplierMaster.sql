CREATE PROCEDURE [dbo].[SP_Insert_SupplierMaster] 
AS
BEGIN

-- ====================================================
-- Author:		FID.Iman
-- Create date: 2013.05.03
-- Description:	To move data from TB_T_SUPPLIER_MASTER
--				to TB_M_SUPPLIER
--              if exist then keep, else insert
-- Ver:  1.0    created.
-- ====================================================

	SET NOCOUNT ON;
    
    DECLARE
		@L_V_SUPPLIER_CD		varchar(4),
		@L_V_SUPPLIER_PLANT		varchar(1),
		@L_V_SUPPLIER_NM		varchar(40)
		--@L_V_SUPPLIER_ABR		varchar(3)	

    
   ---- was : Delete TB_M_SUPPLIER_PART;
   -- cursor for loop 
   DECLARE cur_supplier CURSOR FOR
	SELECT  a.SUPPLIER_CD,  -- 1
			a.SUPPLIER_PLANT,  --2
			a.SUPPLIER_NAME  --3
			--substring(a.SUPPLIER_NAME,1,3) SUPPLIER_ABR  --4
	FROM TB_T_SUPPLIER_MASTER a
    GROUP BY a.SUPPLIER_CD,
			a.SUPPLIER_PLANT,
			a.SUPPLIER_NAME
			
	
	-- open cursor 
	OPEN cur_supplier
	FETCH cur_supplier INTO 
		@L_V_SUPPLIER_CD,
		@L_V_SUPPLIER_PLANT,
		@L_V_SUPPLIER_NM
		--@L_V_SUPPLIER_ABR	
	WHILE @@FETCH_STATUS = 0
	
	BEGIN
		--Check if Part_m empty (master not exist) then insert
		IF EXISTS (SELECT 1 FROM TB_M_SUPPLIER s
		           WHERE s.SUPPLIER_CODE = @L_V_SUPPLIER_CD
		           AND s.SUPPLIER_PLANT_CD = @L_V_SUPPLIER_PLANT )
			 
		   BEGIN
		       -- Update Master table
				UPDATE TB_M_SUPPLIER
				   SET SUPPLIER_NAME = @L_V_SUPPLIER_NM,
				       --SUPPLIER_ABBREVIATION = @L_V_SUPPLIER_ABR,
				       CHANGED_BY = 'SYSTEM',
				       CHANGED_DT = GETDATE()
				 WHERE SUPPLIER_CODE = @L_V_SUPPLIER_CD
		           AND SUPPLIER_PLANT_CD = @L_V_SUPPLIER_PLANT 
		    END 
		   
		-- if master exist
		ELSE
		    BEGIN
		       INSERT TB_M_SUPPLIER 
		              (SUPPLIER_CODE, --1
		               SUPPLIER_PLANT_CD, --2
		               SUPPLIER_NAME, --3
		               --SUPPLIER_ABBREVIATION, --4
		               --JUNBIKI_FLAG, --5
		               CREATED_BY, --6
		               CREATED_DT) --7
		               
		        VALUES (@L_V_SUPPLIER_CD,  --1
						@L_V_SUPPLIER_PLANT,  --2
						@L_V_SUPPLIER_NM,  --3
						--@L_V_SUPPLIER_ABR,  --4
						-- '',  --5
						'SYSTEM', --6
						GETDATE() ) --7
		    END

        -- next fetch
		FETCH cur_supplier INTO 
			@L_V_SUPPLIER_CD,
			@L_V_SUPPLIER_PLANT,
			@L_V_SUPPLIER_NM
			--@L_V_SUPPLIER_ABR
	END

	CLOSE cur_supplier
	DEALLOCATE cur_supplier
    
     -- delete temp table;
     DELETE TB_T_SUPPLIER_MASTER ;

	--delete lock that created in ascii
	DELETE FROM TB_T_LOCK WHERE FUNCTION_NO = 'GBF4S40N'

          
END


