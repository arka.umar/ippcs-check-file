CREATE PROCEDURE [dbo].[SP_DCL_SUPPORTING_TOOLS_DATA_CREATION]
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE
		@rtedate DATETIME;
		/* , @runseq VARCHAR(10),
		@rtegrpcd VARCHAR(10); */

	--SET IDENTITY_INSERT dbo.TB_T_SUPP_MEETING_DRIVER_DATA ON

	BEGIN TRY
		BEGIN TRANSACTION INSERT_TB_R

		--------------------
		--DRIVER DATA PART--
		--------------------

		--get cursor for TB_T_DCL_TLMS_DRIVER_DATA
		DECLARE driver_data_cursor  CURSOR FOR
			SELECT 
				DISTINCT
				D.RTEDATE 
				/* 
				,D.RUNSEQ
				,D.RTEGRPCD
				*/
			FROM dbo.TB_T_DCL_TLMS_DRIVER_DATA D
 			ORDER BY D.RTEDATE ASC

		OPEN driver_data_cursor

		FETCH NEXT FROM  driver_data_cursor INTO @rtedate /* ,@runseq, @rtegrpcd */
												

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF EXISTS (SELECT '' FROM dbo.TB_R_DCL_TLMS_DRIVER_DATA	RD 
						WHERE RD.RTEDATE = @rtedate) 
						/*
						AND RD.RUNSEQ = @runseq 
						AND RD.RTEGRPCD = @rtegrpcd
						*/
						
			BEGIN
				DELETE FROM dbo.TB_R_DCL_TLMS_DRIVER_DATA 
						WHERE RTEDATE = @rtedate
						/*
						AND RUNSEQ = @runseq 
						AND RTEGRPCD = @rtegrpcd
						*/
			END

			--insert data
			INSERT INTO dbo.TB_R_DCL_TLMS_DRIVER_DATA(
				APPLTERMFROM,
				APPLTERMTO,
				RTEGRPCD,
				RTEDATE,
				RUNSEQ,
				LOGPTCD,
				DOCKCD,
				PLANTCD,
				INBOUTBFLAG,
				DRIVERACCSEQ,
				LOGPARTNERCD,
				ARRVDATETIME,
				DPTDATETIME,
				TRAVELDISTKM,
				DOORCD
				)
				SELECT 
					D.APPLTERMFROM,
					D.APPLTERMTO,
					D.RTEGRPCD,
					D.RTEDATE,
					D.RUNSEQ,
					D.LOGPTCD,
					D.DOCKCD,
					D.PLANTCD,
					D.INBOUTBFLAG,
					D.DRIVERACCSEQ,
					D.LOGPARTNERCD,
					D.ARRVDATETIME,
					D.DPTDATETIME,
					D.TRAVELDISTKM,
					D.DOORCD
				FROM TB_T_DCL_TLMS_DRIVER_DATA D
				WHERE
					D.RTEDATE = @rtedate 
					/* AND D.RUNSEQ = @runseq AND D.RTEGRPCD = @rtegrpcd */
				
			FETCH NEXT FROM driver_data_cursor INTO @rtedate /*, @runseq, @rtegrpcd */
													
		END

		CLOSE driver_data_cursor
		DEALLOCATE driver_data_cursor

		--------------------------
		---ORDER ASSIGNMENT PART--
		--------------------------

		DECLARE order_assignment_cursor CURSOR FOR
		SELECT 
			DISTINCT
			OA.RTEDATE
			/*
			,OA.RUNSEQ
			,OA.RTEGRPCD
			*/
		 FROM dbo.TB_T_DCL_TLMS_ORDER_ASSIGNMENT OA
		 ORDER BY OA.RTEDATE

		OPEN order_assignment_cursor
		
		FETCH NEXT FROM order_assignment_cursor INTO @rtedate /*, @runseq, @rtegrpcd */

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF EXISTS (SELECT '' FROM dbo.TB_R_DCL_TLMS_ORDER_ASSIGNMENT RMO 
						WHERE RMO.RTEDATE = @rtedate) 
						/* AND RMO.RUNSEQ = @runseq AND RMO.RTEGRPCD = @rtegrpcd */
						
			BEGIN
				DELETE FROM dbo.TB_R_DCL_TLMS_ORDER_ASSIGNMENT 
					WHERE RTEDATE = @rtedate 
					/* AND RUNSEQ = @runseq AND RTEGRPCD = @rtegrpcd */
				
			END

			--insert data
			INSERT INTO dbo.TB_R_DCL_TLMS_ORDER_ASSIGNMENT(
				CONCTOCRTE,
				CONCTOCRTETRP,
				CONCTODRTEDATE,
				RTEGRPCD,
				RUNSEQ,
				RTEDATE,
				SUPPCD,
				SUPPPLANTCD,
				SUPPDOCKCD,
				NUMPALLETE,
				ORDDATE,
				ORDSEQ,
				RCVCOMPDOCKCD,
				DHNDSTRTIME2,
				DHNDENDTIME2,
				SUPPNAME,
				PARTEMPKBTYPECD,
				UNLOADSTRDATE,
				UNLOADSTRTIME,
				UNLOADENDDATE,
				UNLOADENDTIME,
				PLANE
			)
			SELECT
				O.CONCTOCRTE,
				O.CONCTOCRTETRP,
				O.CONCTODRTEDATE,
				O.RTEGRPCD,
				O.RUNSEQ,
				O.RTEDATE,
				O.SUPPCD,
				O.SUPPPLANTCD,
				O.SUPPDOCKCD,
				O.NUMPALLETE,
				O.ORDDATE,
				O.ORDSEQ,
				O.RCVCOMPDOCKCD,
				O.DHNDSTRTIME2,
				O.DHNDENDTIME2,
				O.SUPPNAME,
				O.PARTEMPKBTYPECD,
				O.UNLOADSTRDATE,
				O.UNLOADSTRTIME,
				O.UNLOADENDDATE,
				O.UNLOADENDTIME,
				O.PLANE
			FROM TB_T_DCL_TLMS_ORDER_ASSIGNMENT O
			WHERE
				O.RTEDATE = @rtedate 
				/* AND O.RUNSEQ = @runseq AND O.RTEGRPCD = @rtegrpcd */

			FETCH NEXT FROM order_assignment_cursor INTO @rtedate /*, @runseq, @rtegrpcd */

		END

		CLOSE order_assignment_cursor
		DEALLOCATE order_assignment_cursor

		COMMIT TRANSACTION INSERT_TB_R
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION INSERT_TB_R

		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		return

	END CATCH

	--SET IDENTITY_INSERT dbo.TB_R_SUPP_MEETING_DRIVER_DATA OFF

END


/****** Object:  StoredProcedure [dbo].[SP_dclToSuppMeeting]    Script Date: 04/28/2015 19:50:15 ******/

