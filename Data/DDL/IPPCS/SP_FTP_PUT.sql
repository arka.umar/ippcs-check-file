CREATE PROCEDURE [dbo].[SP_FTP_PUT]
	@ro_v_err_mesg				varchar(2000) output,
	@ri_v_id					varchar(128),
	@ri_v_ftp_server			varchar(128),
	@ri_v_ftp_port				smallint,
	@ri_v_ftp_user				varchar(128),
	@ri_v_ftp_password			varchar(128),
	@ri_v_ftp_path				varchar(128),
	@ri_v_ftp_filename			varchar(128),
	@ri_v_src_path				varchar(128),
	@rio_v_src_filename			varchar(128) output,
	@ri_v_work_dir				varchar(128)
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--	SET DTFORMAT DMY; -- DD/MM/YYYY
	-- ------------------------------------------------------
	-- Set tbe log to bulk before the main process
	--ALTER DATABASE NKBS_DB SET RECOVERY BULK_LOGGED;
	
	-- common variable
	declare
		@l_n_return_value		smallint = 0,
		@l_v_command			varchar(2000),
		@l_v_work_filename		varchar(128) = 'ftpCmd.txt',
		@l_v_work_file_result	varchar(128) = 'ftpResult.txt',
		@l_v_query				varchar(2000)

	declare @l_t_cmdshell_mesg	table (mesg varchar(255))
	
	-- input parameter validation [start]
	--@ri_v_id						varchar(128),

	--@ri_v_ftp_server				varchar(128),
	if @ri_v_ftp_server is null or len(@ri_v_ftp_server) < 1
	begin
		set @ro_v_err_mesg = 'parameter FTP Server should not be empty'

		return 1
	end

	--@ri_v_ftp_port				smallint,

	--@ri_v_ftp_user				varchar(128),

	--@ri_v_ftp_password			varchar(128),

	--@ri_v_ftp_path				varchar(128),
	if @ri_v_ftp_path is null or len(@ri_v_ftp_path) < 1
	begin
		set @ro_v_err_mesg = 'parameter FTP Path should not be empty'

		return 1
	end

	--@ri_v_ftp_filename			varchar(128),
	if @ri_v_ftp_filename is null or len(@ri_v_ftp_filename) < 1
	begin
		set @ro_v_err_mesg = 'parameter FTP Filename should not be empty'

		return 1
	end

	--@ri_v_src_path				varchar(128),
	if @ri_v_src_path is null or len(@ri_v_src_path) < 1
	begin
		set @ro_v_err_mesg = 'parameter Source Path should not be empty'

		return 1
	end

	--@rio_v_src_filename			varchar(128),
	if @rio_v_src_filename is null or len(@rio_v_src_filename) < 1
	begin
		set @ro_v_err_mesg = 'parameter Source Filename should not be empty'

		return 1
	end

	--@ri_v_work_dir				varchar(128)
	if @ri_v_work_dir is null or len(@ri_v_work_dir) < 1
	begin
		set @ro_v_err_mesg = 'parameter Working Directory should not be empty'

		return 1
	end
	-- input parameter validation [end]

	--set @rio_v_src_filename = isnull(@ri_v_id + '_', '') + @rio_v_src_filename
	set @l_v_work_filename = isnull(@ri_v_id + '_', '') + @l_v_work_filename
	set @l_v_work_file_result = isnull(@ri_v_id + '_', '') + @l_v_work_file_result

	-- business validation [start]
	-- validate @ri_v_src_path path existence
	set	@l_v_command = 'dir ' + @ri_v_src_path
	--print @l_v_command
	insert into @l_t_cmdshell_mesg 
	exec @l_n_return_value = master..xp_cmdshell @l_v_command

	if @l_n_return_value = 1
	begin
		set @ro_v_err_mesg = 'parameter Source Path does not exist'
		--set @ro_v_err_mesg = 'parameter Source Path does not exist, error mesg : '
		--select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

		return 1
	end

	delete from @l_t_cmdshell_mesg

	-- validate @ri_v_work_dir path existence
	set	@l_v_command = 'dir ' + @ri_v_work_dir
	--print @l_v_command
	insert into @l_t_cmdshell_mesg 
	exec @l_n_return_value = master..xp_cmdshell @l_v_command

	if @l_n_return_value = 1
	begin
		set @ro_v_err_mesg = 'parameter Working Directory does not exist'
		--set @ro_v_err_mesg = 'parameter Working Directory does not exist, error mesg : '
		--select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

		return 1
	end

	delete from @l_t_cmdshell_mesg
	-- business validation [end]

	-- business process [start]
	-- deal with special characters for echo commands
	set @ri_v_ftp_server = replace(replace(replace(@ri_v_ftp_server, '|', '^|'),'<','^<'),'>','^>')
	set @ri_v_ftp_user = replace(replace(replace(@ri_v_ftp_user, '|', '^|'),'<','^<'),'>','^>')
	set @ri_v_ftp_password = replace(replace(replace(@ri_v_ftp_password, '|', '^|'),'<','^<'),'>','^>')
	set @ri_v_ftp_path = replace(replace(replace(@ri_v_ftp_path, '|', '^|'),'<','^<'),'>','^>')

	-- construct command ftp : open [server] [port]
	set	@l_v_command = 'echo ' + 'open ' + @ri_v_ftp_server + isnull(' ' + cast(@ri_v_ftp_port as varchar), '')
			+ ' > ' + @ri_v_work_dir + @l_v_work_filename
	--print @l_v_command
	insert into @l_t_cmdshell_mesg 
	exec @l_n_return_value = master..xp_cmdshell @l_v_command
	
	if @l_n_return_value = 1
	begin
		set @ro_v_err_mesg = 'unable to exec command : ' + @l_v_command + ', error mesg : '
		select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

		return 1
	end

	delete from @l_t_cmdshell_mesg

	-- construct command ftp : [user]
	if @ri_v_ftp_user is not null and len(@ri_v_ftp_user) > 0
	begin
		set	@l_v_command = 'echo ' + @ri_v_ftp_user
				+ '>> ' + @ri_v_work_dir + @l_v_work_filename
		--print @l_v_command
		insert into @l_t_cmdshell_mesg 
		exec @l_n_return_value = master..xp_cmdshell @l_v_command
	
		if @l_n_return_value = 1
		begin
			set @ro_v_err_mesg = 'unable to exec command : ' + @l_v_command + ', error mesg : '
			select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

			return 1
		end

		delete from @l_t_cmdshell_mesg
	end
		
	
	-- construct command ftp : [pass]
	if @ri_v_ftp_password is not null and len(@ri_v_ftp_password) > 0
	begin
		set	@l_v_command = 'echo ' + @ri_v_ftp_password
				+ '>> ' + @ri_v_work_dir + @l_v_work_filename
		--print @l_v_command
		insert into @l_t_cmdshell_mesg 
		exec @l_n_return_value = master..xp_cmdshell @l_v_command
	
		if @l_n_return_value = 1
		begin
			set @ro_v_err_mesg = 'unable to exec command : ' + @l_v_command + ', error mesg : '
			select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

			return 1
		end

		delete from @l_t_cmdshell_mesg
	end


	-- construct command ftp : put [src_path][src_filename] [ftp_path][ftp_filename]
	set	@l_v_command = 'echo ' + 'put ' + @ri_v_src_path + @rio_v_src_filename + ' ' + @ri_v_ftp_path + @ri_v_ftp_filename
			+ ' >> ' + @ri_v_work_dir + @l_v_work_filename
	--print @l_v_command
	insert into @l_t_cmdshell_mesg 
	exec @l_n_return_value = master..xp_cmdshell @l_v_command
	
	if @l_n_return_value = 1
	begin
		set @ro_v_err_mesg = 'unable to exec command : ' + @l_v_command + ', error mesg : '
		select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

		return 1
	end

	delete from @l_t_cmdshell_mesg

	-- construct command ftp : quit
	set	@l_v_command = 'echo ' + 'quit'
			+ ' >> ' + @ri_v_work_dir + @l_v_work_filename
	--print @l_v_command
	insert into @l_t_cmdshell_mesg 
	exec @l_n_return_value = master..xp_cmdshell @l_v_command
	
	if @l_n_return_value = 1
	begin
		set @ro_v_err_mesg = 'unable to exec command : ' + @l_v_command + ', error mesg : '
		select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

		return 1
	end

	delete from @l_t_cmdshell_mesg

	-- execute ftp
	set @l_v_command = 'ftp -v -s:' + @ri_v_work_dir + @l_v_work_filename 
			+ ' > ' + @ri_v_work_dir + @l_v_work_file_result
	--print @l_v_command
	insert into @l_t_cmdshell_mesg 
	exec @l_n_return_value = master..xp_cmdshell @l_v_command
	
	if @l_n_return_value = 1
	begin
		set @ro_v_err_mesg = 'unable to exec command : ' + @l_v_command + ', error mesg : '
		select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

		return 1
	end

	delete from @l_t_cmdshell_mesg

	-- check ftp result
	set @l_v_query = '
		if OBJECT_ID(''TempDB..#l_t_ftp_result'') is not null
		begin
			drop table #l_t_ftp_result
		end

		create table #l_t_ftp_result (mesg varchar(2000)) 

		bulk insert #l_t_ftp_result
		from ''' + @ri_v_work_dir + @l_v_work_file_result + '''

		select top 1 mesg
		from #l_t_ftp_result
		where 1 = 1
			and (mesg like ''5%''
				or mesg like ''%Not connected%'')
	'
	
	--print @l_v_query

	insert into @l_t_cmdshell_mesg 
	exec(@l_v_query)

	if exists(select 1 from @l_t_cmdshell_mesg where mesg <> 'quit')
	begin
		set @ro_v_err_mesg = 'ftp result error : '
		select top 1 @ro_v_err_mesg = @ro_v_err_mesg + mesg from @l_t_cmdshell_mesg

		return 1
	end

	delete from @l_t_cmdshell_mesg
	-- business process [end]
		
	return 0
END TRY
BEGIN CATCH	
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	
	select @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE()
	
	print 'SP_FTP_PUT, catch section : ' + @ErrorMessage + ', at line = ' +  cast (@ErrorLine as varchar)
		  
	set @ro_v_err_mesg = @ErrorMessage + ', at line = ' +  cast (@ErrorLine as varchar)
			
	return 2
END CATCH

