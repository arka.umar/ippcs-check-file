
-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	05-Oct-2020
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_COMMON_GR_PO_BY_MATERIAL] @USER_ID VARCHAR(20) = 'AGAN'
	,@PROCESS_ID BIGINT = '202009230021'
	,@r_v_mat_no VARCHAR(100)
	,@r_v_supp_cd VARCHAR(100)
	,@r_v_source_type VARCHAR(100)
	,@r_v_prod_purpose VARCHAR(100)
	,@r_v_packing_type VARCHAR(100)
	,@r_v_part_color_sfx VARCHAR(100)
	,@r_v_plant_cd VARCHAR(100)
	,@r_v_sloc_cd VARCHAR(100)
	,@r_v_delivery_dt DATE
	,@r_v_document_type VARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MODULE_ID VARCHAR(50) = '4'
		,@FUNCTION_ID VARCHAR(100) = '42005'
		,@IsError CHAR(1) = 'N'
		,@CURRDT DATE = CAST(GETDATE() AS DATE)
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_COMMON_GR_PO_BY_MATERIAL'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@RES INT = 1;

	IF OBJECT_ID('tempdb..#csGetPo') IS NOT NULL
	BEGIN
		DROP TABLE #csGetPo
	END;

	CREATE TABLE #csGetPo (
		PO_NO VARCHAR(1000)
		,PO_ITEM_NO VARCHAR(1000)
		,po_quantity NUMERIC(13, 3)
		,po_quantity_open NUMERIC(13, 3)
		,po_mat_price NUMERIC(16, 5)
		,delivery_dt DATE
		,STATUS INT
		)

	BEGIN TRY
		DECLARE @l_v_prod_month VARCHAR(10)
			,@l_n_count INT = 0
			,@l_n_count_check2 INT = 0
			,@r_v_msg VARCHAR(1000) = ''
			,@l_v_prod_purpose VARCHAR(100)
			,@l_v_part_color_sfx VARCHAR(100)
			,@l_n_status_system INT
			,@l_v_msg VARCHAR(200) = ''
			,@l_v_msg2 VARCHAR(300) = ''
			,@vDocType VARCHAR(100)
			,@l_v_doctype VARCHAR(100);

		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'GET PO By Material'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		-- Mandatory Check & get from system master
		IF ISNULL(RTRIM(@r_v_mat_no), '') = ''
			OR ISNULL(RTRIM(@r_v_supp_cd), '') = ''
			OR ISNULL(RTRIM(@r_v_source_type), '') = ''
			OR ISNULL(RTRIM(@r_v_packing_type), '') = ''
			OR ISNULL(RTRIM(@r_v_plant_cd), '') = ''
			OR ISNULL(RTRIM(@r_v_sloc_cd), '') = ''
			OR ISNULL(RTRIM(@r_v_delivery_dt), '') = ''
		BEGIN
			IF ISNULL(RTRIM(@r_v_mat_no), '') = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Material Number,';
			END;

			IF ISNULL(RTRIM(@r_v_supp_cd), '') = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Supplier Code,';
			END;

			IF ISNULL(RTRIM(@r_v_source_type), '') = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Source Type,';
			END;

			IF ISNULL(RTRIM(@r_v_packing_type), '') = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Packing Type,';
			END;

			IF ISNULL(RTRIM(@r_v_plant_cd), '') = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Plant Code,';
			END;

			IF ISNULL(RTRIM(@r_v_sloc_cd), '') = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Sloc Code,';
			END;

			IF ISNULL(RTRIM(@r_v_delivery_dt), '') = ''
			BEGIN
				SET @l_v_msg = @l_v_msg + 'Delivery Date,';
			END;

			SET @l_v_msg = SUBSTRING(@l_v_msg, 1, LEN(@l_v_msg) - 1);
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00003ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,@l_v_msg

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00003ERR'
				,'ERR'
				,'MPCS00003ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ' : Mandatory Checking'
				,@USER_ID
				,GETDATE()

			INSERT INTO #csGetPo
			SELECT '' po_no
				,'' po_item_no
				,'0' po_quantity
				,'0' po_quantity_open
				,'0' po_mat_price
				,NULL delivery_dt
				,1 STATUS
		END;

		IF ISNULL(RTRIM(@r_v_prod_purpose), '') = ''
		BEGIN
			SELECT @l_v_prod_purpose = SYSTEM_VALUE
			FROM TB_M_SYSTEM
			WHERE FUNCTION_ID = 'COMMON_PO'
				AND SYSTEM_CD = 'PROD_PURPOSE_CD'

			IF ISNULL(@l_v_prod_purpose, '') = ''
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'PROD_PURPOSE_CD is not available in system master';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@l_v_msg

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Mandatory Checking'
					,@USER_ID
					,GETDATE()

				INSERT INTO #csGetPo
				SELECT '' po_no
					,'' po_item_no
					,'0' po_quantity
					,'0' po_quantity_open
					,'0' po_mat_price
					,NULL delivery_dt
					,1 STATUS
			END;
		END;

		IF ISNULL(RTRIM(@r_v_part_color_sfx), '') = ''
		BEGIN
			SELECT @l_v_part_color_sfx = SYSTEM_VALUE
			FROM TB_M_SYSTEM
			WHERE FUNCTION_ID = 'JPO_TO_ICS'
				AND SYSTEM_CD = 'PART_COLOR_SFX'

			IF ISNULL(@l_v_part_color_sfx, '') = ''
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'PART_COLOR_SFX is not available in system master';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@l_v_msg

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION + ' : Mandatory Checking'
					,@USER_ID
					,GETDATE()

				INSERT INTO #csGetPo
				SELECT '' po_no
					,'' po_item_no
					,'0' po_quantity
					,'0' po_quantity_open
					,'0' po_mat_price
					,NULL delivery_dt
					,1 STATUS
			END;
		END

		SET @l_v_prod_month = CONVERT(VARCHAR(6), @r_v_delivery_dt, 112);

		/* Check Document Type on Input Parameter */
		SELECT @l_v_doctype = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'COMMON_PO'
			AND SYSTEM_CD = 'PO_LOCAL_REGULER_DOC_TYPE';

		IF (ISNULL(@l_v_doctype, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(PO_LOCAL_REGULER_DOC_TYPE)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO #csGetPo
			SELECT '' po_no
				,'' po_item_no
				,'0' po_quantity
				,'0' po_quantity_open
				,'0' po_mat_price
				,NULL delivery_dt
				,1 STATUS
		END

		IF @IsError <> 'Y'
		BEGIN
			CREATE TABLE #lcur_exist_stat1 (
				PO_NO VARCHAR(10)
				,PO_ITEM_NO VARCHAR(5)
				)

			IF ISNULL(@r_v_document_type, '') = ''
			BEGIN
				SET @vDocType = @l_v_doctype;
			END
			ELSE
			BEGIN
				SET @vDocType = @r_v_document_type;
			END;
			
			INSERT INTO #lcur_exist_stat1
			SELECT i.Po_No
				,i.Po_Item_No
			FROM tb_r_po_item i
				,tb_r_po_h h
			WHERE h.po_no = RTRIM(i.po_no)
				AND h.SUPPLIER_CD = RTRIM(@r_v_supp_cd)
				AND h.PRODUCTION_MONTH = RTRIM(@l_v_prod_month)
				AND h.doc_type = @vDocType
				AND (
					ISNULL(h.deletion_flag, '0') = '1'
					OR ISNULL(i.deletion_flag, 'N') = 'Y'
					)
				AND i.mat_no = RTRIM(@r_v_mat_no)
				AND i.source_type = RTRIM(@r_v_source_type)
				AND i.prod_purpose_cd = ISNULL(RTRIM(@r_v_prod_purpose), RTRIM(@l_v_prod_purpose))
				AND i.packing_type = RTRIM(@r_v_packing_type)
				AND i.part_color_sfx = ISNULL(RTRIM(@r_v_part_color_sfx), RTRIM(@l_v_part_color_sfx))
				AND i.plant_cd = RTRIM(@r_v_plant_cd)
				AND i.sloc_cd = RTRIM(@r_v_sloc_cd);

			SELECT @l_n_count = COUNT('X')
			FROM tb_r_po_item i
				,tb_r_po_h h
			WHERE h.po_no = i.po_no
				AND ISNULL(i.deletion_flag, 'N') = 'N'
				AND h.SUPPLIER_CD = RTRIM(@r_v_supp_cd)
				AND h.PRODUCTION_MONTH = RTRIM(@l_v_prod_month)
				AND h.doc_type = RTRIM(@vDocType)
				AND ISNULL(h.deletion_flag, '0') = '0'
				AND i.mat_no = RTRIM(@r_v_mat_no)
				AND i.source_type = RTRIM(@r_v_source_type)
				AND i.prod_purpose_cd = ISNULL(RTRIM(@r_v_prod_purpose), RTRIM(@l_v_prod_purpose))
				AND i.packing_type = RTRIM(@r_v_packing_type)
				AND i.part_color_sfx = ISNULL(RTRIM(@r_v_part_color_sfx), RTRIM(@l_v_part_color_sfx))
				AND i.plant_cd = RTRIM(@r_v_plant_cd)
				AND i.sloc_cd = RTRIM(@r_v_sloc_cd);

			-- count value for check whether the data selected is NULL
			IF @l_n_count = 0
			BEGIN
				/* if 0 then check with deletion flag = not N */
				SELECT @l_n_count_check2 = COUNT('X')
				FROM #lcur_exist_stat1

				IF @l_n_count_check2 >= 1
				BEGIN
					SELECT DISTINCT @r_v_msg = '{' + substring((
								SELECT '{' + U1.PO_NO + '}-{' + + U1.PO_ITEM_NO + '},' AS [text()]
								FROM #lcur_exist_stat1 U1
								ORDER BY U1.PO_NO
								FOR XML PATH('')
								), 2, 1000)
					FROM #lcur_exist_stat1 U2

					SET @r_v_msg = substring(substring(@r_v_msg, 1, len(@r_v_msg) - 1), 1, 500);
					SET @IsError = 'Y';
					SET @PARAM1 = 'The following PO No have been found but are currently mark as deleted: ' + @r_v_msg;

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()

					INSERT INTO #csGetPo
					SELECT '' po_no
						,'' po_item_no
						,'0' po_quantity
						,'0' po_quantity_open
						,'0' po_mat_price
						,NULL delivery_dt
						,1 STATUS
				END
				ELSE
				BEGIN
					SET @IsError = 'Y';
					SET @PARAM1 = 'PO Data doesnt exist for the passing input parameters';

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()

					INSERT INTO #csGetPo
					SELECT '' po_no
						,'' po_item_no
						,'0' po_quantity
						,'0' po_quantity_open
						,'0' po_mat_price
						,NULL delivery_dt
						,1 STATUS
				END
			END
			ELSE
			BEGIN
				INSERT INTO #csGetPo
				SELECT h.po_no
					,i.po_item_no
					,i.po_quantity_new
					,i.po_quantity_open
					,i.po_mat_price
					,i.delivery_dt
					,0 STATUS
				FROM tb_r_po_item i
					,tb_r_po_h h
				WHERE h.po_no = i.po_no
					AND ISNULL(i.deletion_flag, 'N') = 'N'
					AND h.SUPPLIER_CD = RTRIM(@r_v_supp_cd)
					AND h.PRODUCTION_MONTH = RTRIM(@l_v_prod_month)
					AND h.doc_type = RTRIM(@vDocType)
					AND ISNULL(h.deletion_flag, '0') = '0'
					AND i.mat_no = RTRIM(@r_v_mat_no)
					AND i.source_type = RTRIM(@r_v_source_type)
					AND i.prod_purpose_cd = ISNULL(RTRIM(@r_v_prod_purpose), RTRIM(@l_v_prod_purpose))
					AND i.packing_type = RTRIM(@r_v_packing_type)
					AND i.part_color_sfx = ISNULL(RTRIM(@r_v_part_color_sfx), RTRIM(@l_v_part_color_sfx))
					AND i.plant_cd = RTRIM(@r_v_plant_cd)
					AND i.sloc_cd = RTRIM(@r_v_sloc_cd)
				ORDER BY h.po_no DESC
					,i.po_item_no ASC;
			END
		END

		IF @IsError = 'N'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET PO By Material'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET PO By Material'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		SELECT *
		FROM #csGetPo
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		INSERT INTO #csGetPo
		SELECT '' po_no
			,'' po_item_no
			,'0' po_quantity
			,'0' po_quantity_open
			,'0' po_mat_price
			,NULL delivery_dt
			,2 STATUS

		SELECT *
		FROM #csGetPo
	END CATCH
END
