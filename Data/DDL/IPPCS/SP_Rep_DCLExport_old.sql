CREATE PROCEDURE [dbo].[SP_Rep_DCLExport_old]
	@PickupFrom varchar (10),
	@PickupTo varchar (10),
	@LogPartner varchar (100),
	@Route char(4),
	@Rate char(2),
	@DockCode varchar(100),
	@DeliveryNo varchar(20),
	@Status char(1),
	@Achievement varchar(12),
	@DeliveryType char (1)
AS
BEGIN
	Declare @Query nvarchar(max)

  CREATE TABLE #tmpSplitString
	( 
		column1 VARCHAR(100)
	)

  INSERT INTO #tmpSplitString (column1)
  EXEC dbo.SplitString @LogPartner, '' 

  INSERT INTO #tmpSplitString (column1)
  EXEC dbo.SplitString @DockCode, '' 
  
  SET @Query = '
    Select DELIVERY_NO, REVISE_NO, PICKUP_DT, ROUTE_RATE,
         LP_CD, LOG_PARTNER_NAME, DRIVER_NAME, 
   DELIVERY_STS, ARRIVAL_STATUS, DEPARTURE_STATUS,
   CONFIRM_STS, REQUISITIONER_BY,
   REQUISITIONER_DT, DOWNLOAD_BY, 
         DOWNLOAD_DT, CREATED_BY, CREATED_DT, 
         CHANGED_BY, CHANGED_DT, INVOICE_BY, INVOICE_DT, 
         DELIVERY_REASON, APPROVE_BY, APPROVE_DT, NOTIF
  FROM (
  Select A.DELIVERY_NO,
  A.REVISE_NO, 
  A.PICKUP_DT, 
  A.ROUTE + '' - '' + A.RATE as ROUTE_RATE,
      A.LP_CD,
      B.LOG_PARTNER_NAME, 
      A.DRIVER_NAME, 
      A.DELIVERY_STS,
       
  	CASE
           WHEN (SELECT min(x.ARRIVAL_STATUS) FROM TB_R_DELIVERY_CTL_D AS x
                 WHERE x.DELIVERY_NO = A.DELIVERY_NO AND ARRIVAL_STATUS != 0) = 1 THEN ''NG''
           WHEN (SELECT min(x.ARRIVAL_STATUS) FROM TB_R_DELIVERY_CTL_D AS x
                 WHERE x.DELIVERY_NO = A.DELIVERY_NO AND ARRIVAL_STATUS != 0) = 2 THEN ''NG''
           WHEN (SELECT min(x.ARRIVAL_STATUS) FROM TB_R_DELIVERY_CTL_D AS x
                 WHERE x.DELIVERY_NO = A.DELIVERY_NO AND ARRIVAL_STATUS != 0) = 3 THEN ''OK''
           ELSE ''''
  		 END AS ARRIVAL_STATUS,
  
      CASE 
  		  WHEN (select min(x.DEPARTURE_STATUS) from TB_R_DELIVERY_CTL_D AS x
                  where x.DELIVERY_NO = A.DELIVERY_NO AND x.DEPARTURE_STATUS != 0) = 1 THEN ''NG''
  		  WHEN (select min(x.DEPARTURE_STATUS) from TB_R_DELIVERY_CTL_D AS x
                  where x.DELIVERY_NO = A.DELIVERY_NO AND x.DEPARTURE_STATUS != 0) = 2 THEN ''NG''
  		  WHEN (select min(x.DEPARTURE_STATUS) from TB_R_DELIVERY_CTL_D AS x
  				where x.DELIVERY_NO = A.DELIVERY_NO AND x.DEPARTURE_STATUS != 0) = 3 THEN ''OK''
  		  ELSE ''''
            END AS DEPARTURE_STATUS,
      A.CONFIRM_STS,
      A.REQUISITIONER_BY,
      A.REQUISITIONER_DT,
      A.DOWNLOAD_BY, 
      A.DOWNLOAD_DT, 
      A.CREATED_BY, 
      A.CREATED_DT, 
      A.CHANGED_BY, 
      A.CHANGED_DT, 
      A.INVOICE_BY, 
      A.INVOICE_DT,
      A.DELIVERY_REASON,
  A.APPROVE_BY,
  A.APPROVE_DT,
  A.NOTIFY_STS Notif
  from [dbo].[TB_R_DELIVERY_CTL_H] A  
  left join [dbo].[TB_M_LOGISTIC_PARTNER] B on a.LP_CD=b.LOG_PARTNER_CD          
  where A.PUSH_TO_WEB_FLAG=''1'' and 
  A.DELIVERY_TYPE = ''' + @DeliveryType + ''' AND
  (A.PICKUP_DT between ''' + @PickupFrom + ''' AND ''' + @PickupTo + ''' ) and
  ((A.ROUTE like ''%' + @Route + '%'' AND isnull(''' + @Route + ''','''') <> '''') or (isnull(''' + @Route + ''' ,'''') = '''')) and
  ((A.RATE like ''%' + @Rate + '%'' AND isnull(''' + @Rate + ''' ,'''') <> '''') or (isnull(''' + @Rate + ''','''') = '''')) and
  ((A.DELIVERY_NO like ''%' + @DeliveryNo + '%'' AND isnull(''' + @DeliveryNo + ''','''') <> '''') or (isnull(''' + @DeliveryNo + ''','''') = '''')) and
  ((A.DELIVERY_STS=''' + @Status + ''' AND isnull(''' + @Status + ''','''') <> '''') or (isnull(''' + @Status + ''','''') = ''''))'

    	
    IF ( @LogPartner <> '' ) 
      BEGIN SET @Query = @Query + ' AND (A.LP_CD IN (SELECT column1 FROM #tmpSplitString))'
      END
    
    IF ( @DockCode <> '' ) 
        BEGIN SET @Query = @Query + ' AND EXISTS (SELECT 1 from tb_r_delivery_ctl_D AS D 
                where D.DELIVERY_NO = A.delivery_no 
                and D.DOCK_CD IN (SELECT column1 FROM #tmpSplitString))'
        END SET @Query = @Query + ') XX WHERE 1 = 1 '
       
    IF ( @Achievement = 'Arrival OK')
    BEGIN SET @Query = @Query + ' AND (ARRIVAL_STATUS = ''OK'') '
    END
    IF ( @Achievement = 'Departure OK')
    BEGIN SET @Query = @Query + ' AND (DEPARTURE_STATUS = ''OK'') '
    END
    IF ( @Achievement = 'Arrival NG')
    BEGIN SET @Query = @Query + ' AND (ARRIVAL_STATUS = ''NG'') '
    END
    IF ( @Achievement = 'Departure NG')
    BEGIN SET @Query = @Query + ' AND (DEPARTURE_STATUS = ''NG'') '
    END SET @Query = @Query + ' ORDER BY PICKUP_DT desc, DELIVERY_NO ' 
  
  EXECUTE (@Query) 
  DROP TABLE #tmpSplitString   
END

