
/****** Object:  StoredProcedure [dbo].[SP_Buying_Price_Upload_Batch]    Script Date: 04/05/2021 10:29:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_Buying_Price_Upload_Batch]
    @process_id BIGINT = NULL
  , @username varchar(20) ='job.BPUB'
AS BEGIN
SET NOCOUNT ON;
DECLARE
      @Message           varchar(max)
    , @midINF            VARCHAR(20) = 'MPCS00001INF'
    , @fn                varchar(10) = '71022'
	, @location          varchar(50) = 'SP_Buying_Price_Upload_Batch'
    , @process_flag      varchar(max) = 'start upload'
    , @update_flag       int = 0
    , @apprval_sts       varchar(1)
    , @draftValidFr      DATETIME
    , @Flag_price_status varchar(5)
    , @CountTemp         int = 0
    , @CountRem          int = 0
    , @countOfExists     int = 0
    , @countofUpdate     int = 0
    , @rowInserted       INT = 0
    , @rowUpdated        INT = 0
    , @ERR_FLAG          INT = 0
    , @INVALIDCOUNT      INT = 0
-- cursor variable
    , @Mat_no            varchar(23)
    , @supp_code         varchar(6)
    , @Source_type       varchar(1)
    , @prod_purpse       varchar(5)
    , @packing_type      varchar(1)
    , @par_color_sufix   varchar(2)
    , @valid_fr          datetime
    , @price             numeric(15,4)
    , @curr_cd           varchar(3)
    , @SEQNO             INT
	, @rowNo             int = 0
    exec dbo.PutLog 'Buying Price Upload Batch', @username, @location, @process_id output, 'MPCS00002INF', @fn, 5;

	update t set [STATUS] = 'NG', ERR_MSG = COALESCE(ERR_MSG, '') + 'Duplicate with ROW_NO : ' + CONVERT(VARCHAR, D.MIN_ROW_NO) 
	FROM TB_T_MAT_PRICE T 
	JOIN 
	(
		SELECT MAT_NO, SUPP_CD, SOURCE_TYPE, PROD_PURPOSE_CD, PACKING_TYPE, PART_COLOR_SFX, VALID_DT_FR, VALID_DT_TO, CURR_CD, PRICE_AMT, PRICE_STATUS, MIN(ROW_NO) MIN_ROW_NO 
		FROM TB_T_MAT_PRICE 
		WHERE PROCESS_ID = @process_id 
		GROUP BY MAT_NO, SUPP_CD, SOURCE_TYPE, PROD_PURPOSE_CD, PACKING_TYPE, PART_COLOR_SFX, VALID_DT_FR, VALID_DT_TO, CURR_CD, PRICE_AMT, PRICE_STATUS
		HAVING COUNT(1) > 1
	)  D ON D.MAT_NO = T.Mat_no
    AND D.SUPP_CD = T.supp_cD
    AND D.SOURCE_TYPE = T.Source_type
    AND D.PROD_PURPOSE_CD = T.PROD_PURPOSE_CD
    AND D.PACKING_TYPE = T.packing_type
    AND D.PART_COLOR_SFX = T.PART_COLOR_SFX
    AND D.VALID_DT_FR = T.VALID_DT_FR
    AND D.VALID_DT_TO = T.VALID_DT_TO
    AND D.CURR_CD = T.CURR_CD
    AND D.PRICE_AMT = T.PRICE_AMT
    AND D.PRICE_STATUS = T.PRICE_STATUS
	WHERE T.ROW_NO != D.MIN_ROW_NO
	AND T.PROCESS_ID = @process_id;


	UPDATE t SET [STATUS] = 'NG'
	 , t.ERR_MSG = coalesce(ERR_MSG,'') + ' Duplicate data found in Draft Material. '		
	FROM TB_T_MAT_PRICE t 
	where t.PROCESS_ID = @process_id
	and exists (
		select top 1 1 from TB_M_DRAFT_MATERIAL_PRICE d 
		where d.mat_no = t.MAT_NO and d.SUPP_CD = t.SUPP_CD 
		and d.SOURCE_TYPE = t.SOURCE_TYPE
		and  d.PROD_PURPOSE_CD= t.PROD_PURPOSE_CD  and d.PACKING_TYPE = t.PACKING_TYPE
		and d.VALID_DT_FR = t.VALID_DT_FR
	);
	SET @ERR_FLAG = @ERR_FLAG + @@ROWCOUNT;

	UPDATE t SET [STATUS] = 'NG'
	, t.err_msg = coalesce(t.err_msg, '') + ' Source List Not found. '
	from TB_T_MAT_PRICE t 
	where t.PROCESS_ID = @process_id
	and not exists (
		select top  1 1 from TB_M_SOURCE_LIST s 
		where s.MAT_NO = t.mat_no 
		and s.SOURCE_TYPE = t.SOURCE_TYPE
		and s.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
		and s.PART_COLOR_SFX = t.PART_COLOR_SFX	
		and s.SUPP_CD = t.SUPP_CD	
	); 

	update t SET [STATUS] = 'NG'
	, t.err_msg = coalesce(t.err_msg, '') + ' Currency  not found.'
	from tb_t_mat_price t 
	left join TB_M_CURRENCY c on c.CURRENCY_CD = t.CURR_CD 
	where t.PROCESS_ID = @process_id
	and c.CURRENCY_CD is null;
	
	update t SET [STATUS] = 'NG'
	   , ERR_MSG = COALESCE(ERR_MSG, '') 
	   + 'Draft material is deleted. '
    from TB_M_DRAFT_MATERIAL_PRICE d
	join TB_T_MAT_PRICE t 
    on d.MAT_NO = t.MAT_NO
    AND d.SUPP_CD = t.SUPP_CD
    AND d.SOURCE_TYPE = t.SOURCE_TYPE
    AND d.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
    AND d.PACKING_TYPE = t.PACKING_TYPE
    AND d.PART_COLOR_SFX = t.PART_COLOR_SFX
    AND d.VALID_DT_FR = t.VALID_DT_FR
	where d.DELETION_FLAG ='Y'
	AND T.PROCESS_ID = @process_id

   update t SET [STATUS]='NG'
	  , ERR_MSG = COALESCE(ERR_MSG, '') 
	  + ' Latest price for material var still not approved '
    from TB_T_MAT_PRICE t 
	where T.PROCESS_ID = @process_id
	AND EXISTS(
	SELECT TOP 1 1 FROM TB_M_DRAFT_MATERIAL_PRICE D 
	WHERE d.MAT_NO = t.MAT_NO
    AND d.SUPP_CD = t.SUPP_CD
    AND d.SOURCE_TYPE = t.SOURCE_TYPE
    AND d.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
    AND d.PACKING_TYPE = t.PACKING_TYPE
    AND d.PART_COLOR_SFX = t.PART_COLOR_SFX
    AND d.VALID_DT_TO = '9999-12-31'
	AND d.DELETION_FLAG ='N'
	AND D.RELEASE_STS = 'N'
	);
	    
	-- Mat Price Checking
   update t SET [STATUS]='NG'
	  , ERR_MSG = COALESCE(ERR_MSG, '') 
	  + ' Cannot process with backdate or current month due to data already exist '
    from TB_T_MAT_PRICE t 
	where T.PROCESS_ID = @process_id
		and VALID_DT_FR < GETDATE()
		AND EXISTS(
			SELECT TOP 1 1 FROM TB_M_MATERIAL_PRICE D 
			WHERE d.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
			AND d.MAT_NO = t.MAT_NO
			AND d.SOURCE_TYPE = t.SOURCE_TYPE
			AND d.PACKING_TYPE = t.PACKING_TYPE
			AND d.PART_COLOR_SFX = t.PART_COLOR_SFX
			AND d.SUPP_CD = t.SUPP_CD
			AND d.VALID_DT_TO = '9999-12-31'
			AND d.DELETION_FLAG ='N'
			);
	     
   update t SET [STATUS]='NG'
	  , ERR_MSG = COALESCE(ERR_MSG, '') 
	  + ' Cannot process with backdate or current month due to data already exist '
    from TB_T_MAT_PRICE t 
	where T.PROCESS_ID = @process_id
		and VALID_DT_FR < ISNULL((
			SELECT TOP 1 VALID_DT_FR FROM TB_M_MATERIAL_PRICE D 
			WHERE d.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
			AND d.MAT_NO = t.MAT_NO
			AND d.SOURCE_TYPE = t.SOURCE_TYPE
			AND d.PACKING_TYPE = t.PACKING_TYPE
			AND d.PART_COLOR_SFX = t.PART_COLOR_SFX
			AND d.SUPP_CD = t.SUPP_CD
			AND d.VALID_DT_TO = '9999-12-31'
			AND d.DELETION_FLAG ='N'
			), '1900-01-01');
	    
	-- Draft Mat Price Checking
   update t SET [STATUS]='NG'
	  , ERR_MSG = COALESCE(ERR_MSG, '') 
	  + ' Cannot process with backdate or current month due to data already exist '
    from TB_T_MAT_PRICE t 
	where T.PROCESS_ID = @process_id
		and VALID_DT_FR < GETDATE()
		AND EXISTS(
			SELECT TOP 1 1 FROM TB_M_DRAFT_MATERIAL_PRICE D 
			WHERE d.MAT_NO = t.MAT_NO
			AND d.SUPP_CD = t.SUPP_CD
			AND d.SOURCE_TYPE = t.SOURCE_TYPE
			AND d.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
			AND d.PACKING_TYPE = t.PACKING_TYPE
			AND d.PART_COLOR_SFX = t.PART_COLOR_SFX
			AND d.VALID_DT_TO = '9999-12-31'
			AND d.DELETION_FLAG ='N'
			);
	     
   update t SET [STATUS]='NG'
	  , ERR_MSG = COALESCE(ERR_MSG, '') 
	  + ' Cannot process with backdate or current month due to data already exist '
    from TB_T_MAT_PRICE t 
	where T.PROCESS_ID = @process_id
		and VALID_DT_FR < ISNULL((
			SELECT TOP 1 VALID_DT_FR FROM TB_M_DRAFT_MATERIAL_PRICE D 
			WHERE d.MAT_NO = t.MAT_NO
			AND d.SUPP_CD = t.SUPP_CD
			AND d.SOURCE_TYPE = t.SOURCE_TYPE
			AND d.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
			AND d.PACKING_TYPE = t.PACKING_TYPE
			AND d.PART_COLOR_SFX = t.PART_COLOR_SFX
			AND d.VALID_DT_TO = '9999-12-31'
			AND d.DELETION_FLAG ='N'
			), '1900-01-01');
	    
    -- modify temporary table
    UPDATE TB_T_MAT_PRICE
    SET VALID_DT_TO    = '9999-12-31'
        , DELETION_FLAG  = 'N'
        , APPROVE_STS    = 'N'
        , RETRO_STS      = 'N'
        , PRICE_AMT      = CASE WHEN CURR_CD = 'IDR' THEN FLOOR(PRICE_AMT) ELSE PRICE_AMT END
        , CHANGED_BY     = @username
        , CHANGED_DT     = GETDATE()
    WHERE (@PROCESS_ID IS NULL OR PROCESS_ID = @process_id);        

    DECLARE l_cursor CURSOR FAST_FORWARD FOR
    SELECT ROW_NUMBER ()OVER (ORDER BY MAT_NO DESC) SEQ_NO
    , MAT_NO
    , SUPP_CD
    , SOURCE_TYPE
    , PROD_PURPOSE_CD
    , PACKING_TYPE
    , PART_COLOR_SFX
    , VALID_DT_FR
    , PRICE_AMT
    , CURR_CD
	, ROW_NO 
    FROM TB_T_MAT_PRICE
    WHERE (@process_id IS NULL OR PROCESS_ID = @process_id)
	AND COALESCE([STATUS], '  ') != 'NG';
	    
	OPEN l_cursor
    FETCH NEXT FROM l_cursor INTO
     @SEQNO
    ,@Mat_no
    ,@supp_code
    ,@Source_type
    ,@prod_purpse
    ,@packing_type
    ,@par_color_sufix
    ,@valid_fr
    ,@price
    ,@curr_cD
	,@ROWNO

    while @@FETCH_STATUS = 0
    BEGIN
        -- Do something with your ID and string
        SET @update_flag = 0;   
		SET @Flag_price_status = NULL;

        -- Time Control Validation
        SET @draftValidFr = (
            SELECT VALID_DT_FR
             FROM TB_M_DRAFT_MATERIAL_PRICE
            where MAT_NO = @Mat_no
              AND SUPP_CD = @supp_code
              AND SOURCE_TYPE = @Source_type
              AND PROD_PURPOSE_CD = @prod_purpse
              AND PACKING_TYPE = @packing_type
              AND PART_COLOR_SFX = @par_color_sufix
              AND VALID_DT_TO = '9999-12-31')

        if exists(
            select 1
             from TB_M_DRAFT_MATERIAL_PRICE
             where MAT_NO = @Mat_no
               AND SUPP_CD = @supp_code
               AND SOURCE_TYPE = @Source_type
               AND PROD_PURPOSE_CD = @prod_purpse
               AND PACKING_TYPE = @packing_type
               AND PART_COLOR_SFX = @par_color_sufix
               AND VALID_DT_TO = '9999-12-31'
        )
        begin
            if @valid_fr <= @draftValidFr
            begin
                SET @update_flag = 1;

				SET @countOfExists= @countOfExists + 1;
               
                SET @ERR_FLAG = 1
            end
            else
            begin
                SET @Flag_price_status = 'F';
            end
        end
        else
        begin
            SET @Flag_price_status = 'T';
        end

		/* FID.Ridwan - 20210429 --> split (valid from / to) for tb m draft mat price is processed by pc released 
        if @update_flag = 1
        begin
            SET @countofUpdate = @countofUpdate + 1;
            UPDATE TB_T_MAT_PRICE
               SET STATUS = 'NG', ERR_MSG = ' Valid dt from must be greater than existing ' + convert(varchar(12), @draftValidFr, 121)
                 , CHANGED_BY = @username
                 , CHANGED_DT = GETDATE()
              WHERE MAT_NO = @Mat_no
                AND SUPP_CD = @supp_code
                AND SOURCE_TYPE = @Source_type
                AND PROD_PURPOSE_CD = @prod_purpse
                AND PACKING_TYPE = @packing_type
                AND PART_COLOR_SFX = @par_color_sufix
                AND VALID_DT_FR = @valid_fr
                AND (@process_id IS NULL OR PROCESS_ID = @process_id)
        end
		*/
        if nullif(@Flag_price_status,'') is null 
        begin
            UPDATE TB_M_DRAFT_MATERIAL_PRICE
            SET PRICE_STATUS = @Flag_price_status
              , CHANGED_BY = @username
              , CHANGED_DT = GETDATE()
            WHERE MAT_NO          = @Mat_no
             AND SUPP_CD         = @supp_code
             AND SOURCE_TYPE     = @Source_type
             AND PROD_PURPOSE_CD = @prod_purpse
             AND PACKING_TYPE    = @packing_type
             AND PART_COLOR_SFX  = @par_color_sufix
             AND VALID_DT_TO     = '9999-12-31'
        end

        --end

        IF @ERR_FLAG != 0
        BEGIN
            SET @INVALIDCOUNT = @INVALIDCOUNT+1;
        END
        ELSE
        BEGIN
          begin try
            if not exists(
                SELECT 1
                  from TB_M_DRAFT_MATERIAL_PRICE  m
                  WHERE MAT_NO = @Mat_no
                    AND SUPP_CD = @supp_code
                    AND SOURCE_TYPE = @Source_type
                    AND PROD_PURPOSE_CD = @prod_purpse
                    AND PACKING_TYPE = @packing_type
                    AND PART_COLOR_SFX = @par_color_sufix
                )
            begin
                INSERT INTO TB_M_DRAFT_MATERIAL_PRICE
                ( WARP_BUYER_CD
                , SUPP_CD
                , MAT_NO
                , PRICE_STATUS
                , RELEASE_STS
                , RELEASE_INPUT_DT
                , RELEASED_BY
                , SOURCE_TYPE
                , VALID_DT_FR
                , VALID_DT_TO
                , PC_NO
                , SOURCE_DATA
                , DRAFT_DF
                , WARP_REF_NO
                , CPP_FLAG
                , PROD_PURPOSE_CD
                , PART_COLOR_SFX
                , CURR_CD
                , PRICE_AMT
                , PACKING_TYPE
                , SENT_PIECE_PO_DT
                , DATA_SEQUENCE
                , BUSINESS_AREA
                , DELETION_FLAG
                , UOM
                , MM_DF
                , PRICE_TYPE
                , CREATED_BY
                , CREATED_DT
                , CHANGED_BY
                , CHANGED_DT)
                SELECT
                  WARP_BUYER_CD    = ''
                , t.SUPP_CD
                , t.MAT_NO
                , t.PRICE_STATUS
                , RELEASE_STS      = 'N'
                , RELEASE_INPUT_DT = NULL
                , RELEASED_BY      = ''
                , t.SOURCE_TYPE
                , t.VALID_DT_FR
                , t.VALID_DT_TO
                , t.PC_NO
                , SOURCE_DATA      = 'M'
                , DRAFT_DF         = ''
                , WARP_REF_NO      = ''
                , CPP_FLAG         = ''
                , t.PROD_PURPOSE_CD
                , t.PART_COLOR_SFX
                , t.CURR_CD
                , t.PRICE_AMT
                , t.PACKING_TYPE
                , SENT_PIECE_PO_DT = NULL
                , DATA_SEQUENCE    = ''
                , BUSINESS_AREA    = ''
                , DELETION_FLAG    = 'N'
                , UOM              = ''
                , MM_DF            = ''
                , PRICE_TYPE       = ''
                , CREATED_BY         = @username
                , CREATED_DT         = getdate()
                , CHANGED_BY         = null
                , null
                FROM TB_T_MAT_PRICE t
                WHERE(@PROCESS_ID IS NULL OR t.PROCESS_ID = @process_id)
				and coalesce(t.[STATUS], '  ') != 'NG' 
                AND t.SUPP_CD = @supp_code
				AND t.MAT_NO = @Mat_no
                AND t.SOURCE_TYPE = @Source_type
                AND t.PROD_PURPOSE_CD = @prod_purpse
                AND t.PACKING_TYPE = @packing_type
                AND t.PART_COLOR_SFX = @par_color_sufix
				
                SET @rowInserted += 1
            end
            else
            begin
                /* FID.Ridwan - 20210429 --> split (valid from / to) for tb m draft mat price is processed by pc released 
				UPDATE m
                SET
                    m.VALID_DT_TO = DATEADD(DAY,-1,t.VALID_DT_FR),
                    m.CHANGED_BY = @username,
                    m.CHANGED_DT = getdate()
                FROM TB_M_DRAFT_MATERIAL_PRICE m
                JOIN TB_T_MAT_PRICE t
                ON (m.MAT_NO = t.mat_no
                AND m.SUPP_CD = t.supp_cd
                AND m.SOURCE_TYPE = t.source_type
                AND m.PROD_PURPOSE_CD = t.prod_purpose_cd
                AND m.PACKING_TYPE = t.packing_type
                AND m.PART_COLOR_SFX = t.part_color_sfx )
                WHERE m.VALID_DT_TO = '9999-12-31' 
				and (@process_id IS NULL OR t.PROCESS_ID = @process_id) 
				AND coalesce(t.[STATUS],'  ') != 'NG'*/

                INSERT INTO TB_M_DRAFT_MATERIAL_PRICE
                ( WARP_BUYER_CD
                , SUPP_CD
                , MAT_NO
                , PRICE_STATUS
                , RELEASE_STS
                , RELEASE_INPUT_DT
                , RELEASED_BY
                , SOURCE_TYPE
                , VALID_DT_FR
                , VALID_DT_TO
                , PC_NO
                , SOURCE_DATA
                , DRAFT_DF
                , WARP_REF_NO
                , CPP_FLAG
                , PROD_PURPOSE_CD
                , PART_COLOR_SFX
                , CURR_CD
                , PRICE_AMT
                , PACKING_TYPE
                , SENT_PIECE_PO_DT
                , DATA_SEQUENCE
                , BUSINESS_AREA
                , DELETION_FLAG
                , UOM
                , MM_DF
                , PRICE_TYPE
                , CREATED_BY
                , CREATED_DT
                , CHANGED_BY
                , CHANGED_DT)
                SELECT
                  WARP_BUYER_CD    = ''
                , t.SUPP_CD
                , t.MAT_NO
                , t.PRICE_STATUS
                , RELEASE_STS      = 'N'
                , RELEASE_INPUT_DT = NULL
                , RELEASED_BY      = ''
                , t.SOURCE_TYPE
                , t.VALID_DT_FR
                , t.VALID_DT_TO
                , t.PC_NO
                , SOURCE_DATA      = 'M'
                , DRAFT_DF         = ''
                , WARP_REF_NO      = ''
                , CPP_FLAG         = ''
                , t.PROD_PURPOSE_CD
                , t.PART_COLOR_SFX
                , t.CURR_CD
                , t.PRICE_AMT
                , t.PACKING_TYPE
                , SENT_PIECE_PO_DT = NULL
                , DATA_SEQUENCE    = ''
                , BUSINESS_AREA    = ''
                , DELETION_FLAG    = 'N'
                , UOM              = ''
                , MM_DF            = ''
                , PRICE_TYPE       = ''
                , CREATED_BY       =  @username
                , CREATED_DT       = getdate()
                , CHANGED_BY       = null
                , CHANGED_DT       = null
                FROM TB_T_MAT_PRICE t
                WHERE t.STATUS != 'NG' OR t.STATUS IS NULL
                AND t.SUPP_CD = @supp_code
				AND t.MAT_NO = @Mat_no
                AND t.SOURCE_TYPE = @Source_type
                AND t.PROD_PURPOSE_CD = @prod_purpse
                AND t.PACKING_TYPE = @packing_type
                AND t.PART_COLOR_SFX = @par_color_sufix
				AND (@process_id IS NULL OR T.PROCESS_ID = @process_id)
				AND (coalesce(t.[STATUS],'  ') != 'NG')

                SET @rowUpdated += 1
            end
          end try
          begin catch
              SET @MESSAGE = ERROR_MESSAGE() + '@'+ COALESCE(ERROR_PROCEDURE(),' ') +':' + coalesce(convert(varchar, ERROR_LINE()),'') ;

              exec dbo.PutLog @message, @username, @location, @process_id output, 'MPCS00999ERR', @fn, 2;
          end catch
        END -- err_flag = 1

        FETCH NEXT FROM l_cursor INTO
         @SEQNO
        ,@Mat_no
        ,@supp_code
        ,@Source_type
        ,@prod_purpse
        ,@packing_type
        ,@par_color_sufix
        ,@valid_fr
        ,@price
        ,@curr_cd
		,@ROWNO 
    END -- while @@fetch_status =0

    close l_cursor;
    deallocate l_cursor;

    declare @v0 varchar(20), @v1 varchar(20), @v2 varchar(20), @v3 varchar(20), @v4 varchar(20); 

    SET @CountTemp = coalesce((select count(1) from TB_T_MAT_PRICE where @process_id is null or PROCESS_ID = @process_id ),0);
    SET @V0= CONVERT(VARCHAR(20), @COUNTTEMP);
    exec dbo.PutLog 'Total of uploaded record to temporary table: {0}', @username, 'total row count', @process_id output, @midINF, @fn, 2, @V0;

	SELECT @INVALIDCOUNT=COUNT(1) FROM TB_T_MAT_PRICE WHERE (@process_id is null or PROCESS_ID = @process_id) and COALESCE([STATUS],'  ') = 'NG';
    select @CountRem =count(1) from TB_T_MAT_PRICE where (@process_id is null or PROCESS_ID = @process_id) and COALESCE([STATUS],'  ') != 'NG';
    SET @V0 = CONVERT(VARCHAR(20), @CountRem);
	SET @V1 = CONVERT(VARCHAR(20), @INVALIDCOUNT);
    SET @V2 = CONVERT(VARCHAR(20), @countOfExists);
	SET @V3 = CONVERT(VARCHAR(20), @rowInserted);
    SET @V4 = CONVERT(VARCHAR(20), @rowUpdated);
	exec dbo.PutLog 'Total valid : {0} invalid: {1} existing: {2} inserted: {3} updated: {4}', @username, 'row count', @process_id output, @midINF, @fn, 2, @V0, @v1, @v2, @v3, @v4
    
	select @process_flag=
		case when (@countTemp = 0)
		then 'empty'
		when (@countTemp > 0 and (@rowInserted + @rowUpdated) > 0) and @INVALIDCOUNT = 0 
		then 'success'
		else 'error'
		end; 
	set @Message = 'Process done returning ' + @process_flag; 
    exec dbo.PutLog @Message, @username, 'Batch Upload Buying Price DONE', @process_id output, @midINF, @fn, 2;
	select @process_flag ret 
END

