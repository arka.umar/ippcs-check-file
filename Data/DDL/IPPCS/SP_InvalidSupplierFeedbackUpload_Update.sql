CREATE PROC [dbo].[SP_InvalidSupplierFeedbackUpload_Update]
(
	@functionID VARCHAR(30),
	@processID VARCHAR(30)
)
AS
SET NOCOUNT ON

IF object_id('tempdb..#TempErrorUpload') IS NOT NULL DROP TABLE #TempErrorUpload

DECLARE @rowIndex AS INT,
		@rowCount AS INT,
		
		@id AS VARCHAR(max),
		@errorMessage AS VARCHAR(MAX)
		
SET @rowIndex = 1;

SELECT  ROW_NUMBER() OVER ( ORDER BY ID ASC ) AS SEQ_NO , *
	INTO #TempErrorUpload
FROM dbo.TB_T_ERROR_UPLOAD 
WHERE FUNCTION_ID = @functionID AND PROCESS_ID = @processID

SELECT @rowCount = COUNT(*)
FROM #TempErrorUpload 

WHILE @rowIndex <= @rowCount
BEGIN
	SELECT @errorMessage = ERRORS_MESSAGE
	FROM #TempErrorUpload WHERE SEQ_NO = @rowIndex;
	
	SET @id = SUBSTRING(@errorMessage, CHARINDEX('[', @errorMessage) + 1,CHARINDEX(']', @errorMessage) - CHARINDEX('[', @errorMessage) - 1);
	
	UPDATE dbo.TB_T_SUPPLIER_FEEDBACK_UPLOAD
	SET ERRORS_MESSAGE_CONCAT = ISNULL(ERRORS_MESSAGE_CONCAT, '') + @errorMessage + '; '
	WHERE 
	FUNCTION_ID = @functionID AND
	PROCESS_ID = @processID AND 
	ID = @id
		
	SET @rowIndex = @rowIndex + 1;
END

DROP TABLE #TempErrorUpload

SELECT [ID]
      ,[FUNCTION_ID]
      ,[PROCESS_ID]
      ,[D_ID]
      ,[PACK_MONTH]
      ,[PART_NO]
      ,[PART_NAME]
      ,[VERSION]
      ,[SUPPLIER_CD]
      ,[S_PLANT_CD]
      ,[DOCK_CD]
      ,[N_VOLUME]
      ,[N_1_VOLUME]
      ,[N_2_VOLUME]
      ,[N_3_VOLUME]
      ,[N_FA]
      ,[N_1_FA]
      ,[N_2_FA]
      ,[N_3_FA]
      ,[N_MONTH] AS [N_REPLY]
      ,[N_1_MONTH] AS [N_1_REPLY]
      ,[N_2_MONTH] AS [N_2_REPLY]
      ,[N_3_MONTH] AS [N_3_REPLY]
      ,[ERRORS_MESSAGE_CONCAT]
  FROM [TB_T_SUPPLIER_FEEDBACK_UPLOAD]

