-- =============================================
-- Author:		fid.deny
-- Create date: 02-11-2015
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_ERROR_LIST]-- '201511030019'
	@process_id varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT T.ID, T.USERNAME AS USERNAME, T.ROLE_ID AS ROLE_ID, V.MSG AS MSG 
	INTO #temp
		FROM TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP T LEFT JOIN TB_T_AUTHORIZATION_MAPPING_UPLOAD_TEMP_VALIDATE V
	ON
		T.ID = V.ID AND T.PROCESS_ID = V.PROCESS_ID
		WHERE T.PROCESS_ID = @process_id
		ORDER BY T.ID ASC

	SELECT DISTINCT ST2.ID, ST2.USERNAME, ST2.ROLE_ID, 
    SUBSTRING(
        (
            Select '; '+ST1.MSG  AS [text()]
            From #temp ST1
            Where ST1.ID = ST2.ID AND
			ISNULL(ST1.USERNAME,'') = ISNULL(ST2.USERNAME,'') AND
			ISNULL(ST1.ROLE_ID, '') = ISNULL(ST2.ROLE_ID, '')
            For XML PATH ('')
        ), 2, 1000) MSG
	From #temp ST2
	ORDER BY ST2.ID ASC

	IF OBJECT_ID('tempdb..#temp') IS NOT NULL 
		DROP TABLE #temp

END

