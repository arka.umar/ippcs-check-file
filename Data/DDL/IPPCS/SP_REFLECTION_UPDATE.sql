-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_REFLECTION_UPDATE] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @x XML;
	DECLARE @SourceString as varchar(max) = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'DOCK_QR' AND SYSTEM_CD = 'DQ')
	SET @x = '<r>' + REPLACE((SELECT @SourceString FOR XML PATH('')), ',', '</r><r>') + '</r>';
	--SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol);


	update TB_R_RECEIVING_D
	set RECEIVING_FLG='1'
	where MANIFEST_NO in (select distinct a.manifest_no from TB_R_DAILY_ORDER_MANIFEST a
	inner join TB_R_RECEIVING_D b on a.MANIFEST_NO=b.MANIFEST_NO and a.ORDER_NO=b.ORDER_NO
	where a.DOCK_CD IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol)) and a.MANIFEST_RECEIVE_FLAG in('2','3','4','5') and a.RECEIVE_FLAG='1' and a.Problem_Flag='0')

	/*update TB_R_RECEIVING_D
	set RECEIVING_FLG='1'
	where MANIFEST_NO in (select distinct a.manifest_no from TB_R_DAILY_ORDER_MANIFEST a
	inner join TB_R_RECEIVING_D b on a.MANIFEST_NO=b.MANIFEST_NO and a.ORDER_NO=b.ORDER_NO
	where a.DOCK_CD='53' and a.MANIFEST_RECEIVE_FLAG in('2','3','4','5') and a.RECEIVE_FLAG='1' and a.Problem_Flag='0')
	*/
END

