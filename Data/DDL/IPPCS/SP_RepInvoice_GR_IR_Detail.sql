-- ==============================================
-- Created By	: Fid.Joko
-- Created Date : 2013-11-21
-- Created For	: Report GR IR Detail
-- Modified By	: 
-- Modified Date: 
-- ==============================================

CREATE PROCEDURE [dbo].[SP_RepInvoice_GR_IR_Detail]
	@DOC_DT varchar(max) = NULL
AS
BEGIN

SELECT 
	SUPPLIER_CD,
	PERIOD=
	case 
		when MONTH(DOC_DT) in ('1','2','3')
		then MONTH(DOC_DT)+9
	else 
		MONTH(DOC_DT)-3
	END,
	DOC_DT,
	MAT_DOC_NO,
	INV_REF_NO,
	DOCK_CD,
	MAT_NO=ICS_MAT_NO,
	PART_COLOR_SFX,
	MOVEMENT_QTY,
	ITEM_CURR,
	PO_DETAIL_PRICE,
	GR_IR_AMT
FROM TB_R_GR_IR
WHERE STATUS_CD='1' AND CANCEL_FLAG='0' AND CONVERT(VARCHAR(8),DOC_DT,112) < @DOC_DT

END

