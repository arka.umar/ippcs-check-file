/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Register job for PR creation
*/

CREATE PROCEDURE [dbo].[SP_DLV_CreatePRJobIn] 
@PR_PO_MONTH VARCHAR (7),
@NOTES VARCHAR (MAX),
@CREATED_BY VARCHAR (25),
@DETAIL VARCHAR (MAX),
@pid AS BIGINT

AS
BEGIN 

	DECLARE @process_status AS INT = 0
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_CreatePRJobIn'
	DECLARE @step AS VARCHAR(50) = 'init'
	--DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)

	DECLARE @MODUL_ID VARCHAR(1) = '3';
	DECLARE @FUNCTION_ID VARCHAR(10) = '31103';

	DECLARE @CHECK_MODULE INT;
	DECLARE @CHECK_STATUS INT, @COUNT_ROW INT;
	
	DECLARE @CURR_DATE VARCHAR(6), @PRODUCTION_MONTH VARCHAR(6), @NEXT_MONTH VARCHAR (6);

	SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID IN (@FUNCTION_ID,'31102') AND PROCESS_END_DT IS NULL );
	

	IF (@CHECK_MODULE=0) BEGIN	
				
			
				--CREATE LOG
				SET @steps = @na+'.'+@step;
				SET @log = 'EXEC SP_DLV_CreatePRJobIn';				
				EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

				INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
				VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());

				UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;


				DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
				
				DECLARE @PROD_MONTH VARCHAR (10),
						@LP_CD VARCHAR (5),
						@ROUTE_CD VARCHAR (5),
						@SOURCE VARCHAR (20),
						@QTY INT,
						@STATUS VARCHAR(30);

				DECLARE @ID varchar(11),@ITEM varchar(MAX);
				
				DECLARE BTMP_EXPLODE CURSOR
				FOR					
				SELECT ItemNumber, Item FROM dbo.FN_DLV_ATD_EXPLODE(@DETAIL,';');				

				OPEN BTMP_EXPLODE;
				FETCH NEXT FROM BTMP_EXPLODE INTO
				@ID,@ITEM;

				WHILE @@FETCH_STATUS = 0
				BEGIN

									DELETE FROM @FIELDS;
									INSERT INTO @FIELDS
									SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ITEM,'|');

									SET @PROD_MONTH	= (SELECT ITEM FROM @FIELDS WHERE ID =1);
									SET @LP_CD			= (SELECT ITEM FROM @FIELDS WHERE ID =2);
									SET @ROUTE_CD		= (SELECT ITEM FROM @FIELDS WHERE ID =3);
									SET @SOURCE			= (SELECT ITEM FROM @FIELDS WHERE ID =4);
									SET @QTY				= (SELECT ITEM FROM @FIELDS WHERE ID =5);
									SET @STATUS			= (SELECT ITEM FROM @FIELDS WHERE ID =6);
									
									UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = @pid WHERE PROD_MONTH = @PROD_MONTH AND LP_CD = @LP_CD AND ROUTE_CD = @ROUTE_CD AND SOURCE = @SOURCE
									
																
				FETCH NEXT FROM BTMP_EXPLODE INTO
				@ID,@ITEM
				END;
				CLOSE BTMP_EXPLODE;
				DEALLOCATE BTMP_EXPLODE;
							
				
											
				--==Temporary==--			

				SET @CHECK_STATUS = (SELECT COUNT(*) FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid AND PR_STATUS_FLAG IN ('PR1','PR6'));
				SET @COUNT_ROW = (SELECT COUNT(*) FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid);

				
				SET @CURR_DATE = (SELECT REPLACE(CONVERT (VARCHAR (7), GETDATE(),111),'/',''));
				SET @PRODUCTION_MONTH = (SELECT DISTINCT PROD_MONTH FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid);
				SET @NEXT_MONTH = ( SELECT REPLACE(CONVERT (VARCHAR (7), DATEADD(mm, 1, GETDATE()),111),'/','' ) );
				--SET @PRODUCTION_MONTH = @CURR_DATE;

				IF (@CURR_DATE = @PROD_MONTH OR @PROD_MONTH = @NEXT_MONTH) BEGIN

							IF (@CHECK_STATUS = @COUNT_ROW) BEGIN

											--SELECT 'registry'
												INSERT INTO TB_R_BACKGROUND_TASK_REGISTRY
												VALUES (
												@pid,--ID
												'DeliveryCreatePRTaskRuntime',--NAME
												'Background Task Process Create PR ',--DESCRIPTION
												'System',	--SUBMITTER
												'PR Creation Process',	--FUNCTION_NAME
												'{&quot;productionMonth&quot;:&quot;'+@PR_PO_MONTH+'&quot;,'+
													'&quot;Username&quot;:&quot;'+@CREATED_BY+'&quot;,'+
													'&quot;Notes&quot;:&quot;'+@NOTES+'&quot;,'+
													--'&quot;ProcessID&quot;:&quot;'+CAST(@pid as varchar)+'&quot;,'+
													'&quot;ProcessID&quot;:&quot;'+CAST(@pid as varchar)+'&quot;}',
													-- '&quot;Grid&quot;:&quot;'+@DETAIL+'&quot;}',--PARAMETER
													0,	--TYPE
													0,	--STATUS
												--'D:\ATD\IPPCS App\IPPCS\BackgroundManager\DeliveryCreatePRTaskRuntime\bin\Debug\DeliveryCreatePRTaskRuntime.exe',--COMMAND
												'C:\Background_Task\Tasks\DeliveryCreatePRTaskRuntime.exe',--COMMAND
												0,       --START_DATE
												0,		--END_DATE
												
												4,		--PERIODIC_TYPE
												null,		--INTERVAL
												'',		--EXECUTION_DAYS
												'',	--EXECUTION_MONTHS
												6840000 
												)

											

								END
								ELSE BEGIN
										
										UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4'
										,PROCESS_END_DT=GETDATE()
										WHERE PROCESS_ID = @pid;

										UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;

										RAISERROR('PR already created, only PR with status retrieved and rejected can be created!',16,1)
										RETURN
								END
					END
					ELSE BEGIN

							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4'
							,PROCESS_END_DT=GETDATE()
							WHERE PROCESS_ID = @pid;

							UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;

							RAISERROR('Only able to create PR for current and next month.',16,1)
							RETURN

					END


	END
	ELSE BEGIN

				RAISERROR('Other process is still running by another user.',16,1)
				RETURN
	END
	
	
END;

