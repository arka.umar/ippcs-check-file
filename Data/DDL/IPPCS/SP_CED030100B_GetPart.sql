-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetPart]
AS
BEGIN
	SELECT [PART_NO]
		,ISNULL([PART_NAME], '') [PART_NAME]
		,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
	FROM [dbo].[TB_T_CED_PART]
END

