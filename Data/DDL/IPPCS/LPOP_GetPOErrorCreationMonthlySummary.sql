CREATE PROCEDURE [dbo].[LPOP_GetPOErrorCreationMonthlySummary]
AS
BEGIN
SELECT
	PROD_MONTH,
	MAT_NO,
	SOURCE_TYPE,
	PROD_PURPOSE_CD,
	PLANT_CD,
	SLOC_CD,
	SUPP_CD,
	ERR_CD,
	ERR_MSG,
	CONVERT(VARCHAR(10), MAX(ERR_DT), 104) + ' ' + CONVERT(VARCHAR(10), MAX(ERR_DT), 114) as ERR_DT
FROM TB_R_LPOP_PO_ERROR
GROUP BY
	PROD_MONTH,
	MAT_NO,
	SOURCE_TYPE,
	PROD_PURPOSE_CD,
	PLANT_CD,
	SLOC_CD,
	SUPP_CD,
	ERR_CD,
	ERR_MSG
END

