-- =============================================
-- Author		: Rahmat Setiawan (Arkamaya)
-- Create date	: 17 September 2013
-- Description	: Taking Data From ICS Finished Create PO
-- changed by   : FID.Iman & FID.Goldy
-- changed dt   : 21 September 2013
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_InsertPostingFilePO_Goldy20150625]
	@processId BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
		@sqlQuery AS VARCHAR(MAX),
		@rowIndex BIGINT,
		@L_V_PO_NO AS VARCHAR(30),
		@L_V_PO_ITEM_NO AS VARCHAR(5),
		@L_V_PROD_MONTH AS VARCHAR(8),
		@L_V_SOURCE_TYPE AS VARCHAR(1), 
		@L_V_PROD_PURPOSE_CD AS VARCHAR(5), 
		@L_V_PLANT_CD AS VARCHAR(4), 
		@L_V_SLOC_CD AS VARCHAR(6), 
		@L_V_SUPP_CD AS VARCHAR(5),
		@L_V_DOCK_CD AS VARCHAR(3),
		@L_V_PART_NO AS VARCHAR(12),
		@L_V_PROCESS_ID AS BIGINT,
		@L_V_PROCESS_ID_ICS AS INT,
		@L_V_ERR_CD AS VARCHAR(20),
		@LOG_ID_H AS BIGINT,
		@L_V_ERR_MESSAGE AS VARCHAR(MAX),
		@L_V_USER_ID AS VARCHAR(20);
	        
    CREATE TABLE #ICS_FINISH
	(
		ROW_ID BIGINT,
		PO_NO VARCHAR(30),
		PRODUCTION_MONTH VARCHAR(6),
		DOCK_CD VARCHAR(3),
		SUPPLIER_CD VARCHAR(6),
		CURRENCY_CD VARCHAR(3),
		EXCHANGE_RATE NUMERIC,
		TOTAL_AMOUNT NUMERIC,
		SUPPLIER_PLANT CHAR(4),
		CREATED_BY VARCHAR(20),
		PO_ITEM VARCHAR(5),
        PR_NO VARCHAR(8),
        PR_ITEM VARCHAR(5),
        MAT_NO VARCHAR(23),
        SOURCE_TYPE VARCHAR(1),
        PROD_PURPOSE_CD VARCHAR(5),
        PLANT_CD VARCHAR(4),
        SLOC_CD nchar(10),
        PART_COLOR_SUFFIX VARCHAR(2),
        PACKING_TYPE VARCHAR(1),
        --PO_QTY_ORIGINAL INT,
        PO_QUANTITY_NEW INT,
        PO_MAT_PRICE NUMERIC,
        REQUIRED_DT DATETIME, 
		PO_DATE DATETIME,
		ERR_CD VARCHAR(20),
		ERR_MESSAGE VARCHAR(MAX),
		QTY_N NUMERIC,
		PROCESS_ID BIGINT,
		PROCESS_ID_ICS BIGINT,
		MAT_DESC VARCHAR(40), 
		UOM VARCHAR(3)
	)	
	
	INSERT INTO TB_T_LOCK
           ([FUNCTION_NO]
           ,[LOCK_REFF]
           ,[CREATED_BY]
           ,[CREATED_DT])
    VALUES
		('11004'
		,@processId
		,'System'
		,GETDATE())
		
	
    SET @sqlQuery = 'INSERT INTO #ICS_FINISH 
		SELECT * FROM OPENQUERY([IPPCS_TO_ICS] , 
		''SELECT rownum, b.* 
		  FROM 
		    (SELECT DISTINCT 
		        a.PO_NO, a.PROD_MONTH, a.DOCK_CD, a.SUPP_CD, 
		        a.PO_CURR, a.PO_EXCHANGE_RATE, 0 AS TOTAL_AMOUNT, 
		        NULL AS SUPPLIER_PLANT, NULL AS CREATED_BY, 
	            a.PO_ITEM_NO, a.PR_NO, a.PR_ITEM, a.MAT_NO, 
	            a.SOURCE_TYPE, a.PROD_PURPOSE_CD, a.PLANT_CD, a.SLOC_CD, 
	            a.PART_COLOR_SFX, a.PACKING_TYPE, a.PO_QUANTITY_NEW, 
		        a.PO_MAT_PRICE, i.DELIVERY_DT AS REQUIRED_DT, a.DOC_DT AS PO_DATE, 
		        a.ERR_CD, a.ERR_MSG, a.QTY_N, a.OTHER_PROCESS_ID, 
		        a.PROCESS_ID, i.MAT_DESC, i.UNIT_OF_MEASURE_CD UOM 
		     FROM TB_R_FINISHED_CREATE_PO a 
		     LEFT JOIN TB_R_PO_ITEM i ON a.PO_NO = i.PO_NO AND a.PO_ITEM_NO = i.PO_ITEM_NO 
		
		     ) b 
		'') ';
	
	EXEC (@sqlQuery);
	
	
	--- Get Process ID and User ID
	SELECT TOP 1 @L_V_PROCESS_ID = PROCESS_ID,
	             @L_V_PROCESS_ID_ICS = PROCESS_ID_ICS  
	FROM #ICS_FINISH;
	
	SELECT @L_V_USER_ID = CREATED_BY
	FROM TB_R_ICS_QUEUE 
	WHERE PROCESS_ID = @L_V_PROCESS_ID;

	--- distinct po
	DECLARE cur_ics_po CURSOR FOR
	   SELECT DISTINCT PO_NO
	   FROM #ICS_FINISH
	   WHERE PO_NO IS NOT NULL;
	   
	--- open cursor
	OPEN cur_ics_po
	FETCH NEXT FROM cur_ics_po INTO @L_V_PO_NO;
	    
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- INSERT INTO PO_H
		IF NOT EXISTS(SELECT 1 FROM TB_R_PO_H WHERE PO_NO IN (SELECT PO_NO FROM #ICS_FINISH WHERE PO_NO = @L_V_PO_NO ))
		BEGIN
			INSERT INTO TB_R_PO_H 
							(PO_NO, SUPPLIER_CD, PRODUCTION_MONTH, PO_DATE, PO_TYPE, 
			                SUPPLIER_PLANT, EXCHANGE_RATE, PO_CURR, TOTAL_AMOUNT, STATUS_CD, 
			                REQUIRED_DT, CREATED_BY, CREATED_DT)
			SELECT distinct PO_NO, SUPPLIER_CD, PRODUCTION_MONTH, PO_DATE, '1', 
			                SUPPLIER_PLANT, EXCHANGE_RATE, CURRENCY_CD, 0 TOTAL_AMOUNT, 10, 
			                REQUIRED_DT, @L_V_USER_ID, GETDATE() 
			FROM #ICS_FINISH 
			WHERE PO_NO = @L_V_PO_NO;
		END
		
		-- INSERT INTO PO_D
		INSERT INTO TB_R_PO_D 
						(PO_NO, PO_ITEM, PR_NO, 
						PR_ITEM, MAT_NO, SOURCE_TYPE, 
						PROD_PURPOSE_CD, PLANT_CD, SLOC_CD, 
						PART_COLOR_SUFFIX, PACKING_TYPE, PO_QUANTITY_NEW, 
						PO_MAT_PRICE, REQUIRED_DT, CURRENCY_CD, 
						MAT_DESC, UOM, 
						AMOUNT, CREATED_BY, CREATED_DT )
		SELECT distinct x.PO_NO, x.PO_ITEM, x.PR_NO, 
						x.PR_ITEM, x.MAT_NO, x.SOURCE_TYPE, 
						x.PROD_PURPOSE_CD, x.PLANT_CD, x.SLOC_CD, 
						x.PART_COLOR_SUFFIX, x.PACKING_TYPE, x.PO_QUANTITY_NEW, 
						x.PO_MAT_PRICE, x.REQUIRED_DT, x.CURRENCY_CD, 
						X.MAT_DESC, X.UOM, 
						x.PO_MAT_PRICE * x.PO_QUANTITY_NEW,@L_V_USER_ID, GETDATE() 
		FROM #ICS_FINISH x 
		WHERE PO_NO = @L_V_PO_NO AND x.PO_NO+x.PO_ITEM NOT IN (
			SELECT PO_NO+PO_ITEM FROM TB_R_PO_D
		);
		
		SELECT @L_V_PO_ITEM_NO = x.PO_ITEM, @L_V_PROD_MONTH = x.PRODUCTION_MONTH, @L_V_SUPP_CD = x.SUPPLIER_CD, 
			@L_V_PART_NO = x.MAT_NO + x.PART_COLOR_SUFFIX
		FROM #ICS_FINISH x WHERE PO_NO = @L_V_PO_NO;
		
		--- update po_no, po_item to tb_r_lpop_sum
		DECLARE cur_ics_po_detail CURSOR FOR
	    SELECT PO_NO, PO_ITEM, PRODUCTION_MONTH, MAT_NO + PART_COLOR_SUFFIX, SUPPLIER_CD
		   FROM #ICS_FINISH
		   WHERE PO_NO = @L_V_PO_NO;
		
		OPEN cur_ics_po_detail
		FETCH NEXT FROM cur_ics_po_detail INTO @L_V_PO_NO, @L_V_PO_ITEM_NO, @L_V_PROD_MONTH, @L_V_PART_NO, @L_V_SUPP_CD;
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE TB_R_LPOP_SUM
				SET PO_NO = @L_V_PO_NO,
					PO_ITEM = @L_V_PO_ITEM_NO
			WHERE D_ID = 'FCST' AND VERS = 'F' AND PACK_MONTH = @L_V_PROD_MONTH AND SUPPLIER_CD = @L_V_SUPP_CD
				AND PART_NO = @L_V_PART_NO
				
			FETCH NEXT FROM cur_ics_po_detail INTO @L_V_PO_NO, @L_V_PO_ITEM_NO, @L_V_PROD_MONTH, @L_V_PART_NO, @L_V_SUPP_CD
		END
		
		CLOSE cur_ics_po_detail
		DEALLOCATE cur_ics_po_detail
		--- end update
		
		---- update total amount
		UPDATE TB_R_PO_H
		SET TOTAL_AMOUNT = (SELECT SUM(AMOUNT) 
		                    FROM TB_R_PO_D
		                    WHERE PO_NO = @L_V_PO_NO) 
		WHERE PO_NO = @L_V_PO_NO
		
		EXEC IPPCS_WORKFLOW.dbo.sp_RegisterWorklist @L_V_PO_NO, 1, @L_V_USER_ID, @L_V_PROCESS_ID, 1;
		
        -- next fetch
		FETCH NEXT FROM cur_ics_po INTO @L_V_PO_NO
	END
	
	-- close cursor
	CLOSE cur_ics_po
	DEALLOCATE cur_ics_po
		
	 --- Delete previous error in log trans
	SELECT  TOP 1 @L_V_PROD_MONTH = PRODUCTION_MONTH
	FROM #ICS_FINISH;
	
	CREATE TABLE #TEMP_LOG_TRANS
	(
	 LOG_ID_H BIGINT
	 );
	
    -- Insert into temp table
    INSERT INTO #TEMP_LOG_TRANS
	SELECT LOG_ID_H FROM TB_R_LOG_TRANSACTION_D 
	WHERE D_ID = 'FCST'
	AND PACK_MONTH = @L_V_PROD_MONTH;
	
	-- Delete log trans
	DELETE TB_R_LOG_TRANSACTION_D 
	WHERE LOG_ID_H IN (SELECT LOG_ID_H FROM #TEMP_LOG_TRANS);
		
	DELETE TB_R_LOG_TRANSACTION_H
	WHERE LOG_ID_H IN (SELECT LOG_ID_H FROM #TEMP_LOG_TRANS);
	
	
    ----- Insert Log Transaction ---- 
	DECLARE cur_log_trans CURSOR FOR
	   SELECT DISTINCT V.SOURCE_TYPE, V.PROD_PURPOSE_CD, V.PLANT_CD, V.SLOC_CD, 
	                   V.DOCK_CD, V.MAT_NO+V.PART_COLOR_SUFFIX,
	                   V.ERR_CD,V.ERR_MESSAGE,V.SUPPLIER_CD
	   FROM #ICS_FINISH V
	   WHERE RTRIM(PO_NO) IS NULL
	   AND V.ERR_CD IS NOT NULL;
    
    
    --- open cursor
	OPEN cur_log_trans
	FETCH NEXT FROM cur_log_trans 
	INTO @L_V_SOURCE_TYPE, @L_V_PROD_PURPOSE_CD, @L_V_PLANT_CD, @L_V_SLOC_CD, @L_V_DOCK_CD,
	     @L_V_PART_NO,@L_V_ERR_CD,@L_V_ERR_MESSAGE,@L_V_SUPP_CD;
   SET @rowIndex = 1;
    --loop
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- index
      
         
		IF NOT EXISTS(SELECT 1 FROM TB_R_LOG_TRANSACTION_H WHERE PROCESS_ID = @L_V_PROCESS_ID)
			-- insert log header
			BEGIN
				INSERT INTO TB_R_LOG_TRANSACTION_H (PROCESS_ID, 
				                                    PROCESS_ID_ICS, 
				                                    MANIFEST_NO, 
				                                    CREATED_BY, 
				                                    CREATED_DATE)
				VALUES  (@L_V_PROCESS_ID, -- PROCESS_ID - bigint
						  @L_V_PROCESS_ID_ICS , -- PROCESS_ID_ICS - int
						 'PCS_PO' , -- MANIFEST_NO - varchar(25)
						 @L_V_USER_ID , -- CREATED_BY - varchar(20)
						 GETDATE())
	        END;
	        
	        -- insert log detail
	        SELECT @LOG_ID_H=LOG_ID_H FROM TB_R_LOG_TRANSACTION_H WHERE PROCESS_ID = @L_V_PROCESS_ID
			
			--print(@LOG_ID_H);
				
			INSERT INTO TB_R_LOG_TRANSACTION_D 
				(LOG_ID_H, -- 1
				SEQ_NO,   --2
				SYSTEM_SOURCE,  --3 
				USR_ID,  --4
				PROCESS_DT, --5
				MANIFEST_NO,  --6
				PROD_PURPOSE_CD,  --7
				SOURCE_TYPE,  --8
				PACK_MONTH,  --9
				D_ID, -- 9a
				VERS,  --10
				PART_NO,  --11
				SUPP_CD,  --12
				DOCK_CD,  --13
				PLANT_CD,  --14
				SLOC_CD,  --15
				LOCATION,  --16
				ERR_CD,  --16a
				ERR_MESSAGE,  --16
				CREATED_BY,  --17
				CREATED_DT) --18
			VALUES( 
				@LOG_ID_H , --1
				@rowIndex , --2
				'IPPCS' , -- 3
				@L_V_USER_ID , -- 4
				GETDATE() , -- 5
				'' , -- 6
				@L_V_PROD_PURPOSE_CD , -- 7
				@L_V_SOURCE_TYPE , -- 8
				@L_V_PROD_MONTH ,  -- 9
				'FCST', -- 9a
				'F', -- 10
				@L_V_PART_NO , -- 11
				@L_V_SUPP_CD , -- 12
				@L_V_DOCK_CD , -- 13
				@L_V_PLANT_CD , -- 14
				@L_V_SLOC_CD , -- 15
				'IPPCS PO' , -- 16
				@L_V_ERR_CD , --16a
				@L_V_ERR_MESSAGE , -- 17
				@L_V_USER_ID , -- 18
				GETDATE() );
			
	        --Insert LPOP Abnormality
			EXEC SP_InsertLPOPConfirmationAbnormality @L_V_PROD_MONTH, 'FCST', 'F', 
			                                          @L_V_PART_NO, @L_V_SUPP_CD, @L_V_ERR_CD
			
			
			        
        -- next fetch
		FETCH NEXT FROM cur_log_trans 
		INTO @L_V_SOURCE_TYPE, @L_V_PROD_PURPOSE_CD, @L_V_PLANT_CD, @L_V_SLOC_CD, @L_V_DOCK_CD,
			 @L_V_PART_NO,@L_V_ERR_CD,@L_V_ERR_MESSAGE,@L_V_SUPP_CD;
			 
			 SET @rowIndex = @rowIndex + 1;	
	END
	
	-- close cursor
	CLOSE cur_log_trans
	DEALLOCATE cur_log_trans
	
	
    -- final, if all supp already got PO then update
    IF NOT EXISTS (SELECT DISTINCT 1
                   FROM #ICS_FINISH X
                   WHERE X.SUPPLIER_CD is not null 
                   AND X.PO_NO is null )
       BEGIN
	       --- UPDATE CREATE PO
		   UPDATE TB_R_LPOP_CONFIRMATION SET 
		          PO_CREATED_BY = @L_V_USER_ID, 
				  PO_CREATED_DT = GETDATE(), 
				  CHANGED_BY = @L_V_USER_ID, 
				  CHANGED_DT = GETDATE() ,
				  PO_CREATION_STATUS = '2'
			WHERE PRODUCTION_MONTH = @L_V_PROD_MONTH     
	       
       END
    
	DELETE FROM [TB_T_LOCK]
	WHERE
		[FUNCTION_NO] = '11004'
        AND [LOCK_REFF] = @processId;

	---=== Store to TB_R_ICS_QUEUE modified by Rahmat 2013.10.31
	UPDATE TB_R_ICS_QUEUE
	SET
		PROCESS_STATUS = 2,
		CHANGED_BY = 'SYSTEM',
		CHANGED_DT = GETDATE()
	WHERE
		IPPCS_MODUL = '1'
		AND IPPCS_FUNCTION = '11004'
		AND PROCESS_ID = @processId
		AND PROCESS_STATUS = '1'    
END

