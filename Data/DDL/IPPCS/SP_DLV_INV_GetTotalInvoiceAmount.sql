/*
      Preparation Date : 2014-09-24
      Preparation By   : Rahmat
      Description      : Get total amount invoice creation

	  Modified By : FID.Reggy
	  Modified Dt : 01.09.2015
*/

CREATE PROCEDURE [dbo].[SP_DLV_INV_GetTotalInvoiceAmount]
   @PO_NO varchar(max),
   @SourceString varchar(max),
   @LP_INV_NO varchar(max)
AS
BEGIN

      --DECLARE @PO_NO as varchar(max) = 'ALS20140761';
      --DECLARE @SourceString as varchar(max) = 'R201407071691,R201407071878,R201407071739,R201407071834,R201407071536';
      --DECLARE @LP_INV_NO as varchar(max) = '';

      DECLARE @x XML;
      SET @x = '<r>' + REPLACE((SELECT @SourceString FOR XML PATH('')), ',', '</r><r>') + '</r>';
      DECLARE @TOTAL as numeric(15,2) = 0
      DECLARE @COUNT as numeric(15,2) = 0
      
      DECLARE @INV_DT as VARCHAR(10);
	  DECLARE @INV_TAX_CD as varchar(max);
	  DECLARE @INV_TAX_DT as VARCHAR(10);
      DECLARE @D_NO as varchar(max)='';
      
      
      --SET @INV_DT = (SELECT TOP 1 INV_DT FROM TB_R_DLV_INV_UPLOAD  WHERE PO_NO = @PO_NO)
      SET @INV_TAX_CD = (SELECT TOP 1 INV_TEXT 
						 FROM TB_R_DLV_INV_UPLOAD 
						 WHERE PO_NO = @PO_NO 
							   AND LP_INV_NO = @LP_INV_NO 
						 ORDER BY SEQ_NO DESC)
      --SET @INV_TAX_DT = (SELECT TOP 1 INV_TAX_DT FROM TB_R_DLV_INV_UPLOAD  WHERE PO_NO = @PO_NO)

      SET @INV_DT = (SELECT TOP 1 (CASE WHEN INV_DT IS NUll 
										THEN CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + 
											 RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + 
											 RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2) 
										ELSE CAST(YEAR(INV_DT) AS VARCHAR) + '-' + 
											 RIGHT('0'+CAST(MONTH(INV_DT) AS VARCHAR), 2) + '-' + 
											 RIGHT('0'+CAST(DAY(INV_DT) AS VARCHAR), 2) END ) 
					 FROM TB_R_DLV_INV_UPLOAD 
					 WHERE PO_NO = @PO_NO 
						   AND LP_INV_NO = @LP_INV_NO 
					 ORDER BY SEQ_NO DESC)

      SET @INV_TAX_DT = (SELECT TOP 1 (CASE WHEN INV_TAX_DT IS NUll 
											THEN CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + 
												 RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + 
												 RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2) 
											ELSE CAST(YEAR(INV_TAX_DT) AS VARCHAR) + '-' + 
												 RIGHT('0'+CAST(MONTH(INV_TAX_DT) AS VARCHAR), 2) + '-' + 
												 RIGHT('0'+CAST(DAY(INV_TAX_DT) AS VARCHAR), 2) END ) 
						  FROM TB_R_DLV_INV_UPLOAD  
						  WHERE PO_NO = @PO_NO 
								AND LP_INV_NO = @LP_INV_NO 
						  ORDER BY SEQ_NO DESC)
      
      IF(@LP_INV_NO <> '')
      BEGIN
            SELECT DISTINCT DLV_NO 
			INTO #test 
			FROM(
                  SELECT a.REFF_NO as DLV_NO
                  FROM TB_R_DLV_GR_IR a
					LEFT JOIN TB_R_DLV_INV_UPLOAD c 
						ON c.DELIVERY_NO = a.REFF_NO 
						   AND c.LP_INV_NO = @LP_INV_NO
                  WHERE a.CONDITION_CATEGORY = 'H' 
						AND a.REFF_NO IN (SELECT y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value 
										  FROM @x.nodes('/r') y(XmlCol)) 
						AND a.CANCEL_STS IS NULL
            )tbl;

            DECLARE @DLV_NO varchar(13);
            DECLARE TEST_CUR CURSOR FOR 
				SELECT dlv_no FROM #test;
            OPEN TEST_CUR;
            FETCH NEXT FROM TEST_CUR INTO @DLV_NO;
				WHILE @@FETCH_STATUS = 0  
				BEGIN  
					SET @D_NO = @D_NO+''+@DLV_NO+',';
					FETCH NEXT FROM TEST_CUR INTO @DLV_NO
				END;
            CLOSE TEST_CUR;
            DEALLOCATE TEST_CUR;
            drop table #test;
                  
            SELECT 
				COUNT(a.REFF_NO) as DLV_COUNT, 
				SUM(c.INV_AMT) as TOTAL,
				SUM(c.INV_AMT) + SUM(c.INV_TAX_AMT) as TAX_AMT,
				SUM(c.INV_TAX_AMT)as TAX,
				c.LP_INV_NO,
				--@INV_DT as INV_DT,
				ISNULL(@INV_DT, CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + 
					RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + 
					RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2)) AS INV_DT,
				@INV_TAX_CD as INV_TEXT,
				ISNULL(@INV_TAX_DT, CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + 
					RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + 
					RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2)) as INV_TAX_DT,
				--SUBSTRING(@D_NO, 1, LEN(@D_NO)-1) as DLV_NO
				@SourceString as DLV_NO
            FROM TB_R_DLV_GR_IR a
                  INNER JOIN TB_R_DLV_INV_UPLOAD c 
						ON c.DELIVERY_NO = a.REFF_NO 
						AND c.LP_INV_NO = @LP_INV_NO
                        AND c.comp_price_cd = a.comp_price_cd 
						AND c.status_cd = a.status_cd
            WHERE a.CONDITION_CATEGORY = 'H' 
				  AND a.CANCEL_STS IS NULL 
				  AND a.REFF_NO IN (SELECT y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value 
									FROM @x.nodes('/r') y(XmlCol))
            GROUP BY c.LP_INV_NO

      END
      ELSE
      BEGIN
            SELECT DISTINCT DLV_NO 
			INTO #test2 
			FROM(
                --MODIF BY AGUNG 21-07-2014
                SELECT a.REFF_NO as DLV_NO
                FROM TB_R_DLV_GR_IR a 
					LEFT JOIN TB_R_DLV_INV_UPLOAD b 
						ON b.PO_NO = a.PO_NO 
						   AND b.DELIVERY_NO = a.REFF_NO 
						   AND ((b.LP_INV_NO IS NULL) 
								 OR (b.LP_INV_NO = ''))
                WHERE a.PO_NO = @PO_NO 
					  AND a.REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value 
										FROM @x.nodes('/r') y(XmlCol))
					  AND a.CONDITION_CATEGORY = 'H' 
					  AND a.CANCEL_STS IS NULL
            )tbl;

            DECLARE @DLV_NO2 varchar(13);
            DECLARE TEST_CUR CURSOR FOR 
				SELECT dlv_no FROM #test2;
            OPEN TEST_CUR;
            FETCH NEXT FROM TEST_CUR INTO @DLV_NO2;
				WHILE @@FETCH_STATUS = 0  
				BEGIN  
					SET @D_NO = @D_NO+''+@DLV_NO2+',';
					FETCH NEXT FROM TEST_CUR INTO @DLV_NO2
				END;
            CLOSE TEST_CUR;
            DEALLOCATE TEST_CUR;
			drop table #test2;

            --MODIF BY AGUNG 21-07-2014
            
            --SELECT 
            --COUNT(a.REFF_NO) as DLV_COUNT,
            --(SELECT SUM(x.PO_DETAIL_PRICE) FROM TB_R_DLV_GR_IR x LEFT JOIN TB_R_DLV_INV_UPLOAD z ON z.PO_NO = x.PO_NO AND z.DELIVERY_NO = x.REFF_NO AND ((z.LP_INV_NO IS NULL) OR (z.LP_INV_NO = '')) WHERE REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND x.CONDITION_CATEGORY='H') as TOTAL,
            --(SELECT SUM(x.PO_DETAIL_PRICE) FROM TB_R_DLV_GR_IR x LEFT JOIN TB_R_DLV_INV_UPLOAD z ON z.PO_NO = x.PO_NO AND z.DELIVERY_NO = x.REFF_NO AND ((z.LP_INV_NO IS NULL) OR (z.LP_INV_NO = '')) WHERE REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND x.CONDITION_CATEGORY='H') +
            --(SELECT SUM(x.PO_DETAIL_PRICE) FROM TB_R_DLV_GR_IR x LEFT JOIN TB_R_DLV_INV_UPLOAD z ON z.PO_NO = x.PO_NO AND z.DELIVERY_NO = x.REFF_NO AND ((z.LP_INV_NO IS NULL) OR (z.LP_INV_NO = '')) WHERE REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND x.CONDITION_CATEGORY='D' AND x.COMP_PRICE_CD = 'ZVAT')
            --as TAX_AMT,
            --(SELECT SUM(x.PO_DETAIL_PRICE) FROM TB_R_DLV_GR_IR x LEFT JOIN TB_R_DLV_INV_UPLOAD z ON z.PO_NO = x.PO_NO AND z.DELIVERY_NO = x.REFF_NO AND ((z.LP_INV_NO IS NULL) OR (z.LP_INV_NO = '')) WHERE REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND x.CONDITION_CATEGORY='D' AND x.COMP_PRICE_CD = 'ZVAT') as TAX,
            --'' as LP_INV_NO,
            --ISNULL(@INV_DT, CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2)) AS INV_DT,
            --@INV_TAX_CD as INV_TEXT,
            --ISNULL(@INV_TAX_DT, CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2)) as INV_TAX_DT,
            --SUBSTRING(@D_NO, 1, LEN(@D_NO)-1) as DLV_NO
            --FROM TB_R_DLV_GR_IR a 
            --WHERE a.REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol))
            --          AND a.STATUS_CD IN ('1', '-1') AND a.CONDITION_CATEGORY = 'H'
            --          AND a.REFF_NO NOT IN (
            --                SELECT DELIVERY_NO FROM TB_R_DLV_INV_UPLOAD WHERE CONDITION_CATEGORY = 'H' AND STATUS_CD IN ('1', '-1')
            --          )
            --GROUP BY a.LP_CD
            
            SELECT 
                COUNT(a.REFF_NO) as DLV_COUNT,
                SUM(PO_DETAIL_PRICE) AS TOTAL,
                SUM(PO_DETAIL_PRICE) + (SUM(PO_DETAIL_PRICE) * 0.1) as TAX_AMT,
                (SUM(PO_DETAIL_PRICE) * 0.1) as TAX,
                '' as LP_INV_NO,
                ISNULL(@INV_DT, CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + 
					RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + 
					RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2)) AS INV_DT,
                @INV_TAX_CD as INV_TEXT,
                ISNULL(@INV_TAX_DT, CAST(YEAR(GETDATE()) AS VARCHAR) + '-' + 
					RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR), 2) + '-' + 
					RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR), 2)) as INV_TAX_DT,
                --SUBSTRING(@D_NO, 1, LEN(@D_NO)-1) as DLV_NO
				@SourceString as DLV_NO --Modified by FID.Reggy
            FROM TB_R_DLV_GR_IR a 
            WHERE CONDITION_CATEGORY = 'H' 
				  AND STATUS_CD IN ('1', '-1') 
				  AND (REFF_NO NOT IN ( SELECT DELIVERY_NO 
										FROM TB_R_DLV_INV_UPLOAD 
										WHERE CONDITION_CATEGORY = 'H' 
											  AND STATUS_CD IN ('1', '-1')
									  )
                        AND a.REFF_NO IN (SELECT y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value 
										  FROM @x.nodes('/r') y(XmlCol))
                        AND a.CANCEL_STS IS NULL
					  )
                  --WHERE a.REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol))
                  --    AND a.STATUS_CD IN ('1', '-1') AND a.CONDITION_CATEGORY = 'H'
                  --    AND a.REFF_NO NOT IN (
                  --          SELECT DELIVERY_NO FROM TB_R_DLV_INV_UPLOAD WHERE CONDITION_CATEGORY = 'H' AND STATUS_CD IN ('1', '-1')
                  --    )
      END
END

