-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2020-09-30
-- Description:	This function to Convert Month_year or date to Period_Fiscal
-- =============================================
CREATE PROCEDURE [dbo].[SP_INVOICE_CONVERT_MONTH_TO_FISCAL] @PROCESS_ID BIGINT
	,@ri_n_option INT
	,@ri_v_month CHAR(10)
	,@ri_v_year CHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Result VARCHAR(20)
		,@l_v_fiscal VARCHAR(4)
		,@l_v_period VARCHAR(2)
		,@l_n_jml INT
		,@l_n_counter INT = 0
		,@l_v_temp VARCHAR(2)
		,@l_v_temp_year VARCHAR(4)
		,@RET INT = 1
		,@ro_v_per_fiscal VARCHAR(10) = ''

	CREATE TABLE #LIST_PERIOD (
		ROWNO INT
		,FISCAL_YEAR VARCHAR(4)
		,PERIOD VARCHAR(2)
		)

	INSERT INTO #LIST_PERIOD
	SELECT ROW_NUMBER() OVER (
			ORDER BY (
					SELECT 1
					)
			) NO
		,fiscal_year
		,period
	FROM TB_M_IM_PERIOD
	WHERE year = @ri_v_year
		AND month = @ri_v_month
	ORDER BY fiscal_year;

	SELECT @l_n_jml = count(1)
	FROM tb_m_im_period
	WHERE year = @ri_v_year
		AND month = @ri_v_month;

	IF @l_n_jml = 1
	BEGIN
		-- get data (fiscal_year, period) from table tb_m_im_period
		SELECT @l_v_fiscal = fiscal_year
			,@l_v_period = period
		FROM TB_M_IM_PERIOD
		WHERE year = @ri_v_year
			AND month = @ri_v_month;
	END
	ELSE
		IF @l_n_jml > 1
		BEGIN
			DECLARE @CN_ALL INT
				,@CN INT = 1;

			SELECT @CN_ALL = COUNT(1)
			FROM #LIST_PERIOD

			WHILE @CN <= @CN_ALL
			BEGIN
				SELECT @l_v_fiscal = FISCAL_YEAR
					,@l_v_period = PERIOD
				FROM #LIST_PERIOD
				WHERE ROWNO = @CN;

				IF CAST(@l_v_period AS INT) <= 12
				BEGIN
					SET @l_v_temp = @l_v_period;
					SET @l_v_temp_year = @l_v_fiscal;
					SET @l_n_counter = @l_n_counter + 1;
				END

				SET @CN = @CN + 1;
			END

			IF @l_n_counter = 1
			BEGIN
				SET @l_v_fiscal = @l_v_temp_year;
				SET @l_v_period = @l_v_temp;
				SET @RET = 1;
			END
			ELSE
			BEGIN
				SET @RET = 0;
			END
		END
		ELSE
		BEGIN
			SET @RET = 0;
		END

	IF @RET = 1
	BEGIN
		-- fill field period_fiscal depend on option
		IF @ri_n_option = 1
			OR @ri_n_option = 5
		BEGIN
			SET @ro_v_per_fiscal = @l_v_fiscal + '' + @l_v_period;
		END
		ELSE
			IF @ri_n_option = 2
				OR @ri_n_option = 6
			BEGIN
				SET @ro_v_per_fiscal = @l_v_period + '.' + @l_v_fiscal;
			END;
	END

	INSERT INTO TB_T_RET_FISCAL
	SELECT @PROCESS_ID
		,@ro_v_per_fiscal

	DROP TABLE #LIST_PERIOD
END
