CREATE FUNCTION [dbo].[GETSUPPLIER](@RTEGRPCD VARCHAR(5), @RUNSEQ VARCHAR(3), @DATEFROM VARCHAR(12), @DATETO VARCHAR(12), @D VARCHAR(5))

RETURNS VARCHAR(MAX)
AS
BEGIN
DECLARE @TEXT VARCHAR(MAX)

SELECT @TEXT = COALESCE(@TEXT + ',', '') 
+ CAST(SUPP AS VARCHAR) 
FROM (SELECT DISTINCT CAST(C.SUPPLIER_ABBREVIATION AS VARCHAR) SUPP
--@TEXT = COALESCE(@TEXT + ',', '') + CAST(B.LOGPTCD AS varchar) 
FROM TB_R_DCL_TLMS_DRIVER_DATA B

INNER JOIN TB_R_DCL_TLMS_ORDER_ASSIGNMENT O
ON B.RTEDATE = O.RTEDATE 
AND B.RTEGRPCD = O.RTEGRPCD
AND B.RUNSEQ = O.RUNSEQ
AND B.LOGPTCD = O.SUPPCD
AND B.PLANTCD = O.SUPPPLANTCD

LEFT JOIN TB_M_PHYSICAL_DOCK_MAPPING P
ON O.RCVCOMPDOCKCD = P.LOGICAL_DOCKCD

LEFT JOIN TB_M_SUPPLIER C 
ON C.SUPPLIER_CODE = RIGHT(REPLICATE('0',4) 
+ CAST(B.LOGPTCD AS VARCHAR(4)),4) 
AND C.SUPPLIER_PLANT_CD = B.PLANTCD

 WHERE 
(B.DOORCD = '' OR LEN(B.DOCKCD) = 3 OR B.DOCKCD = '' )
AND (O.RCVCOMPDOCKCD IN (@D) OR P.PHYSICAL_DOCKCD IN (@D))
AND B.RTEGRPCD = @RTEGRPCD 
AND B.RUNSEQ = @RUNSEQ
--AND B.RTEDATE >= @DATEFROM AND B.RTEDATE <= @DATETO 
AND B.RTEDATE IN(@DATEFROM,@DATETO) -- changed fid.deny 2015-04-11
--AND B.LOGPTCD NOT LIKE '807%'
) TBL2

RETURN @TEXT
END

