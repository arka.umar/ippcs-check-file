
/****** Object:  StoredProcedure [dbo].[SP_INVOICE_SAVE_PROCESS_PCS]    Script Date: 30/04/2021 17:08:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	19-Sept-2020
-- Description	:	Invoice Update
-- Update	: 
-- =============================================
ALTER PROCEDURE [dbo].[SP_INVOICE_SAVE_PROCESS_PCS] @USER_ID VARCHAR(20)
	,@PROCESS_ID BIGINT
	,@SupplierInvoiceNo VARCHAR(MAX)
	,@SupplierCode VARCHAR(MAX)
	,@LivBase VARCHAR(MAX)
AS
BEGIN
	DECLARE @IsError CHAR(1) = 'N'
	DECLARE @CURRDT DATE = CAST(GETDATE() AS DATE)
	DECLARE @FUNCTION_ID AS VARCHAR(MAX) = '53002'
		,@MODULE_ID AS VARCHAR(MAX) = '5'
		,@FUNTION_NM AS VARCHAR(MAX) = 'SP Invoice Save Process'
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP Invoice Save Process'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@query VARCHAR(MAX)
		,@ICSSrvrName NVARCHAR(256)
		,@ICSDBName NVARCHAR(256)

	SET NOCOUNT ON

	BEGIN TRY
		DECLARE @l_v_inv_no BIGINT;

		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Invoice Save Process'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		--* Step 1. System Master Checking *--
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'System Master Checking'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		DECLARE @l_v_valuation_class VARCHAR(100);
		DECLARE @l_v_price_var VARCHAR(100);
		DECLARE @l_v_local_curr_CD VARCHAR(100);
		DECLARE @lv_liv_gl_acct VARCHAR(100);
		-- for rollback data
		DECLARE @InvNo VARCHAR(100);
		SELECT DISTINCT @InvNo = INV_NO 
		FROM TB_T_INVOICE_H
		WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
			AND PROCESS_ID = @PROCESS_ID


		SELECT @l_v_valuation_class = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'LIV_VALUATION_CLASS_CD'

		SELECT @l_v_price_var = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'COND_TYPE_PRICE_VARIANT'

		SELECT @l_v_local_curr_CD = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'LOCAL_CURR'

		SELECT @lv_liv_gl_acct = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'LIV_GL_ACCOUNT'

		IF (ISNULL(@l_v_valuation_class, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(LIV_VALUATION_CLASS_CD)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@l_v_price_var, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(COND_TYPE_PRICE_VARIANT)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@l_v_local_curr_CD, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(LOCAL_CURR)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@lv_liv_gl_acct, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(LIV_GL_ACCOUNT)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF @IsError = 'N'
		BEGIN
			SELECT @ICSSrvrName = SYSTEM_VALUE
			FROM [TB_M_SYSTEM]
			WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
				AND SYSTEM_CD = 'LINKED_SERVER';

			SELECT @ICSDBName = SYSTEM_VALUE
			FROM [TB_M_SYSTEM]
			WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
				AND SYSTEM_CD = 'DB_NAME';
			
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Get Material from IFAST'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			IF OBJECT_ID('tempdb..#TB_TEMP_MATERIAL_ICS_2') IS NOT NULL
			BEGIN
				DROP TABLE #TB_TEMP_MATERIAL_ICS_2
			END

			CREATE TABLE #TB_TEMP_MATERIAL_ICS_2 (
				[MAT_NO] [varchar](50) NULL
				,[MAT_DESC] [varchar](100) NULL
				)

			SET @query = ' INSERT INTO #TB_TEMP_MATERIAL_ICS_2
				SELECT * FROM OPENQUERY (' + @ICSSrvrName + ' , ''SELECT DISTINCT MAT_NO, MAT_DESC
				FROM ' + @ICSDBName + 'TB_M_MATERIAL
				WHERE ISNULL(DELETION_FLAG, ''''N'''') = ''''N'''''')';

			EXEC (@query);

			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert invoice header'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			--* Step 2. Insert Invoice Header *--
			INSERT INTO TB_R_INV_H (
				[INV_NO]
				,[INV_DT]
				,[SUPP_INV_NO]
				,[SUPPLIER_CD]
				,[CURRENCY_CD]
				,[EXCHANGE_RATE]
				,[INV_AMT]
				,[INV_LOCAL_AMT]
				,[INV_TAX_NO]
				,[INV_TEXT]
				,[PAYMENT_TERM_CD]
				,[PAYMENT_METHOD_CD]
				,[SUPP_BANK_TYPE]
				,[BALANCE]
				,[INV_DESC]
				,[WITHHOLDING_TAX_CD]
				,[BASE_AMT]
				,[WITHHOLDING_TAX_AMT]
				,[ASSIGNMENT]
				,[PAY_PLAN_DT]
				,[STATUS_CD]
				,[CALC_TAX_FLAG]
				,[REVERSE_FLAG]
				,[CREATED_BY]
				,[CREATED_DT]
				,[CHANGED_BY]
				,[CHANGED_DT]
				,[IN_PROGRESS]
				)
			SELECT DISTINCT H.INV_NO
				,H.INV_DT
				,H.SUPP_INVOICE_NO AS SUPP_INV_NO
				,H.SUPP_CD AS SUPPLIER_CD
				,H.PAYMENT_CURR AS CURRENCY_CD
				,H.EXCHANGE_RATE
				,H.INV_AMT
				,H.INV_AMT_LOCAL_CURR AS INV_LOCAL_AMT
				,H.HEADER_TEXT AS INV_TAX_NO
				,H.INV_TEXT
				,H.PAYMENT_TERM_CD
				,H.PAYMENT_METHOD_CD
				,H.PARTNER_BANK_KEY AS SUPP_BANK_TYPE
				,H.BALANCE
				,H.INV_DESCRIPTION AS INV_DESC
				,WT.WITHOLDING_TAX_CD
				,WT.WITHOLDING_TAX_BASE_AMT AS BASE_AMT
				,WT.WITHOLDING_TAX_AMT
				,H.ASSIGNMENT
				,H.BASELINE_DT AS PAY_PLAN_DT
				,4 AS STATUS_CD
				,H.CALC_TAX_FLAG
				,H.REVERSE_FLAG
				,'System' AS CREATED_BY
				,GETDATE() AS CREATED_DT
				,NULL AS CHANGED_BY
				,NULL AS CHANGED_DT
				,2 AS IN_PROGRESS
			FROM TB_T_INVOICE_H H
			LEFT JOIN TB_T_INV_WITHOLDING_TAX WT ON H.INV_NO = WT.INV_NO
			WHERE H.PROCESS_ID = @PROCESS_ID
				AND H.SUPP_INVOICE_NO = @SupplierInvoiceNo

			--* Step 3. Insert Invoice tax *--
			DECLARE @lv_calc_tax CHAR(1);

			SELECT @lv_calc_tax = calc_tax_flag
			FROM TB_T_INVOICE_H
			WHERE PROCESS_ID = @PROCESS_ID
				AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

			IF @lv_calc_tax = 'Y'
			BEGIN
				
				EXEC dbo.CommonGetMessage 'MPCS00002INF'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,'Insert invoice tax'

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MPCS00002INF'
					,'INF'
					,'MPCS00002INF : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()

				INSERT INTO TB_R_INV_TAX (
					INVOICE_TAX_NO
					,INVOICE_NO
					,TAX_CD
					,CURRENCY_CD
					,TAX_AMOUNT
					,LOCAL_TAX_AMOUNT
					,INVOICE_TAX_DT
					,CREATED_BY
					,CREATED_DT
					,CHANGED_BY
					,CHANGED_DT
					)
				SELECT DISTINCT H.HEADER_TEXT
					,IT.INV_NO
					,IT.TAX_CD
					,H.PAYMENT_CURR AS CURR_CD
					,IT.TAX_AMT
					,ROUND(IT.TAX_AMT * H.EXCHANGE_RATE, 0) LOCAL_TAX_AMT
					,H.TAX_DT
					,'System' AS CREATED_BY
					,GETDATE() AS CREATED_DT
					,NULL AS CHANGED_BY
					,NULL AS CHANGED_DT
				FROM TB_T_INV_TAX IT
				INNER JOIN TB_T_INVOICE_H H ON IT.INV_NO = H.INV_NO
				WHERE IT.PROCESS_ID = @PROCESS_ID
					AND IT.SUPP_INVOICE_NO = @SupplierInvoiceNo
			END

			--* Step 4. Insert Invoice Detail *--
			IF OBJECT_ID('tempdb..#TB_T_INV_D_TEMP') IS NOT NULL
			BEGIN
				DROP TABLE #TB_T_INV_D_TEMP
			END

			CREATE TABLE #TB_T_INV_D_TEMP (
				[INV_NO] [varchar](10) NOT NULL
				,[INV_DOC_ITEM] [varchar](5) NOT NULL
				,[PO_NO] [varchar](10) NULL
				,[PO_ITEM_NO] [varchar](5) NULL
				,[COMP_PRICE_CD] [varchar](4) NULL
				,[MAT_DOC_NO] [varchar](10) NULL
				,[MAT_DOC_ITEM] [numeric](4, 0) NULL
				,[MAT_NO_REF] [varchar](23) NULL
				,[INV_REF_NO] [varchar](16) NULL
				,[MAT_NO] [varchar](23) NOT NULL
				,[PROD_PURPOSE_CD] [varchar](5) NOT NULL
				,[SOURCE_TYPE_CD] [varchar](1) NOT NULL
				,[PLANT_CD] [varchar](4) NOT NULL
				,[SLOC_CD] [varchar](6) NOT NULL
				,[PACKING_TYPE] [varchar](1) NULL
				,[PART_COLOR_SFX] [varchar](2) NOT NULL
				,[DOC_DT] [date] NULL
				,[QTY] [numeric](13, 3) NULL
				,[PRICE_AMT] [numeric](16, 5) NULL
				,
				--[SUPP_MANIFEST] [varchar](12) NULL,--remarks by alira.agi 2015-06-24
				[SUPP_MANIFEST] [varchar](16) NULL
				,[INV_AMT] [numeric](16, 5) NULL
				,[INV_LOCAL_AMT] [numeric](16, 5) NULL
				,[CONDITION_CATEGORY] [varchar](1) NULL
				,[RETRO_DOC_NO] [varchar](10) NULL
				,[TAX_CD] [varchar](2) NOT NULL
				,[UOM_CD] [varchar](3) NULL
				,[MAT_DESC] [varchar](40) NULL
				,[CREATED_BY] [varchar](20) NULL
				,[CREATED_DT] [datetime] NULL
				,[CHANGED_BY] [varchar](20) NULL
				,[CHANGED_DT] [datetime] NULL
				)

			INSERT INTO #TB_T_INV_D_TEMP
			SELECT DISTINCT D.INV_NO
				,D.INV_DOC_ITEM
				,D.PO_NO
				,D.PO_ITEM_NO
				,D.COMP_PRICE_CD
				,D.MAT_DOC_NO
				,D.MAT_DOC_ITEM
				,D.ORI_MAT_NO AS MAT_NO_REF
				,D.REFF_NO AS INV_REF_NO
				,D.MAT_NO
				,D.PROD_PURPOSE_CD
				,D.SOURCE_TYPE AS SOURCE_TYPE_CD
				,ISNULL(D.PLANT_CD, '') AS PLANT_CD
				,ISNULL(D.SLOC_CD, '') AS SLOC_CD
				,NULL AS PACKING_TYPE
				,D.PART_COLOR_SFX
				,D.DOC_DT
				,D.QUANTITY AS QTY
				,D.PRICE_AMT
				,D.REFF_NO AS SUPP_MANIFEST
				,D.INV_AMT
				,ROUND(D.INV_AMT_LOCAL_CURR, 0) AS INV_LOCAL_AMT
				,D.CONDITION_CATEGORY
				,NULL AS RETRO_DOC_NO
				,D.TAX_CD
				,D.UNIT_OF_MEASURE_CD AS UOM_CD
				,M.MAT_DESC
				,'System' AS CREATED_BY
				,GETDATE() AS CREATED_DT
				,NULL AS CHANGED_BY
				,NULL AS CHANGED_DT
			FROM TB_T_INVOICE_D D
			LEFT JOIN #TB_TEMP_MATERIAL_ICS_2 M ON D.MAT_NO = M.MAT_NO
			WHERE D.PROCESS_ID = @PROCESS_ID
				AND D.SUPP_INVOICE_NO = @SupplierInvoiceNo

			UPDATE #TB_T_INV_D_TEMP
			SET PACKING_TYPE = C.PACKING_TYPE
				,RETRO_DOC_NO = C.RETRO_DOC_NO
				,CHANGED_BY = 'System'
				,CHANGED_DT = GETDATE()
			FROM #TB_T_INV_D_TEMP A
			JOIN TB_R_INV_UPLOAD C ON C.SUPP_INV_NO = @SupplierInvoiceNo
				AND C.SUPP_CD = @SupplierCode
				AND C.MAT_NO = A.MAT_NO

			
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert invoice detail'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO TB_R_INV_D (
				[INV_NO]
				,[INV_DOC_ITEM]
				,[PO_NO]
				,[PO_ITEM_NO]
				,[COMP_PRICE_CD]
				,[MAT_DOC_NO]
				,[MAT_DOC_ITEM]
				,[MAT_NO_REF]
				,[INV_REF_NO]
				,[MAT_NO]
				,[PROD_PURPOSE_CD]
				,[SOURCE_TYPE_CD]
				,[PLANT_CD]
				,[SLOC_CD]
				,[PACKING_TYPE]
				,[PART_COLOR_SFX]
				,[DOC_DT]
				,[QTY]
				,[PRICE_AMT]
				,[SUPP_MANIFEST]
				,[INV_AMT]
				,[INV_LOCAL_AMT]
				,[CONDITION_CATEGORY]
				,[RETRO_DOC_NO]
				,[TAX_CD]
				,[UOM_CD]
				,[MAT_DESC]
				,[CREATED_BY]
				,[CREATED_DT]
				,[CHANGED_BY]
				,[CHANGED_DT]
				)
			SELECT DISTINCT [INV_NO]
				,[INV_DOC_ITEM]
				,[PO_NO]
				,[PO_ITEM_NO]
				,[COMP_PRICE_CD]
				,[MAT_DOC_NO]
				,[MAT_DOC_ITEM]
				,[MAT_NO_REF]
				,[INV_REF_NO]
				,[MAT_NO]
				,[PROD_PURPOSE_CD]
				,[SOURCE_TYPE_CD]
				,[PLANT_CD]
				,[SLOC_CD]
				,[PACKING_TYPE]
				,[PART_COLOR_SFX]
				,[DOC_DT]
				,[QTY]
				,[PRICE_AMT]
				,[SUPP_MANIFEST]
				,[INV_AMT]
				,[INV_LOCAL_AMT]
				,[CONDITION_CATEGORY]
				,[RETRO_DOC_NO]
				,[TAX_CD]
				,[UOM_CD]
				,[MAT_DESC]
				,[CREATED_BY]
				,[CREATED_DT]
				,[CHANGED_BY]
				,[CHANGED_DT]
			FROM #TB_T_INV_D_TEMP

			
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert invoice witholding tax'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			--* Step 5. Insert Invoice Withholding Tax *--
			INSERT INTO TB_R_INVOICE_WITHOLDING_TAX (
				INV_NO
				,WITHOLDING_TAX_CD
				,WITHHOLDING_TAX_BASE_AMT
				,WITHHOLDING_TAX_AMT
				,CREATED_BY
				,CREATED_DT
				,CHANGED_BY
				,CHANGED_DT
				) (
				SELECT DISTINCT INV_NO
				,WITHOLDING_TAX_CD
				,WITHOLDING_TAX_BASE_AMT
				,WITHOLDING_TAX_AMT
				,@USER_ID
				,GETDATE()
				,@USER_ID
				,GETDATE()
				FROM TB_T_INV_WITHOLDING_TAX WHERE PROCESS_ID = @PROCESS_ID
				AND SUPP_INVOICE_NO = @SupplierInvoiceNo
				);

			
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert invoice gl account'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			--* Step 6. Insert Invoice GL Account *--
			INSERT INTO TB_R_INV_GL_ACCOUNT (
				INV_NO
				,GL_ACCOUNT
				,INV_AMT
				,COST_CENTER
				,TAX_CD
				,CREATED_BY
				,CREATED_DT
				,CHANGED_BY
				,CHANGED_DT
				)
			SELECT DISTINCT INV_NO
				,GL_ACCOUNT
				,INV_AMT
				,COST_CENTER AS COST_CENTER_CD
				,TAX_CD
				,'System' AS CREATED_BY
				,GETDATE() AS CREATED_DT
				,NULL AS CHANGED_BY
				,NULL AS CHANGED_DT
			FROM TB_T_INVOICE_GL_ACCOUNT
			WHERE PROCESS_ID = @PROCESS_ID
				AND SUPP_INVOICE_NO = @SupplierInvoiceNo

			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Update & delete invoice upload'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			--* FID.Ridwan: 2021-01-09 *--
			-- Update TB_R_INV_H
			UPDATE h
			SET h.SUBMIT_DT = tu.SUBMIT_DT
				,h.SUBMIT_BY = tu.SUBMIT_BY
				,h.POSTING_DT = tu.POSTING_DT
				,h.POSTING_BY = tu.POSTING_BY
				,h.TURN_OVER = tu.TURN_OVER
				,h.TOTAL_MANIFEST = tu.TOTAL_MANIFEST
				,h.STAMP_FLAG = tu.STAMP_FLAG
				,h.CERTIFICATE_ID = tu.CERTIFICATE_ID
				,h.CREATED_DT = tu.CREATED_DT
				,h.CREATED_BY = tu.CREATED_BY
				,h.CHANGED_BY = tu.POSTING_BY
				,h.CHANGED_DT = GETDATE()
				,h.AUTO_POSTING = tu.AUTO_POSTING
				,h.TRANSACTION_TYPE = tu.TRANSACTION_TYPE
			FROM TB_R_INV_H h
			INNER JOIN TB_R_INV_UPLOAD tu ON h.SUPP_INV_NO = tu.SUPP_INV_NO
				AND h.SUPPLIER_CD = tu.SUPP_CD
			WHERE tu.SUPP_INV_NO = @SupplierInvoiceNo
				AND tu.SUPP_CD = @SupplierCode

			-- Update status inv upload
			UPDATE gr
			SET gr.STATUS_CD = '4'
				,gr.CHANGED_BY = 'AGENT'
				,gr.CHANGED_DT = GETDATE()
			FROM TB_R_INV_UPLOAD tu
			INNER JOIN TB_R_GR_IR gr ON gr.PO_NO = tu.PO_NO
				AND gr.PO_ITEM_NO = tu.PO_ITEM_NO
				AND gr.MAT_DOC_NO = tu.MAT_DOC_NO
				AND gr.ITEM_NO = tu.MAT_DOC_ITEM
				AND gr.COMP_PRICE_CD = tu.COMP_PRICE_CD
			WHERE tu.SUPP_INV_NO = @SupplierInvoiceNo
				AND tu.SUPP_CD = @SupplierCode

			-- Insert TB_T_INV_GL_ACCOUNT_LOG
			INSERT INTO TB_T_INV_GL_ACCOUNT_LOG
			SELECT * from TB_T_INV_GL_ACCOUNT
			WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo

			-- Insert TB_R_INV_UPLOAD_LOG
			INSERT INTO TB_R_INV_UPLOAD_LOG
			SELECT * from TB_R_INV_UPLOAD
			WHERE SUPP_INV_NO = @SupplierInvoiceNo 
				AND SUPP_CD = @SupplierCode

			-- Delete TB_T_INV_GL_ACCOUNT
			delete from TB_T_INV_GL_ACCOUNT
			WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo

			-- Delete TB_R_INV_UPLOAD
			delete from TB_R_INV_UPLOAD
			WHERE SUPP_INV_NO = @SupplierInvoiceNo 
				AND SUPP_CD = @SupplierCode

			
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Update Daily order manifest'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			-- update dom
			UPDATE TB_R_DAILY_ORDER_MANIFEST
			SET MANIFEST_RECEIVE_FLAG = 6
				,CHANGED_BY = 'AGENT'
				,CHANGED_DT = GETDATE()
			WHERE MANIFEST_NO IN (
					SELECT manifest_no
					FROM (
						SELECT manifest_no
							,part_no
						FROM tb_r_daily_order_part
						GROUP BY manifest_no
							,part_no
						) p
					LEFT JOIN (
						SELECT inv_no
							,inv_ref_no
							,part_no = (mat_no + part_color_sfx)
						FROM TB_R_INV_D
						WHERE INV_NO = @InvNo
						) Qry ON p.manifest_no = Qry.inv_ref_no
						AND p.part_no = Qry.part_no
					WHERE p.manifest_no IN (
							SELECT INV_REF_NO
							FROM TB_R_INV_D
							WHERE INV_NO = @InvNo
							)
					GROUP BY manifest_no
					HAVING SUM(CASE 
								WHEN Qry.inv_no IS NULL
									THEN 1
								ELSE 0
								END) = 0
					)


			--* Step 7. Update GRIR Table *--
			-- Check LIV Based
			-- 1 = GR/IR, 2 = PO
			
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Update GR IR'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			IF OBJECT_ID('tempdb..#cur_inv_det') IS NOT NULL
			BEGIN
				DROP TABLE #cur_inv_det
			END

			CREATE TABLE #cur_inv_det (
				ROWNO INT
				,INV_NO VARCHAR(10)
				,INV_DOC_ITEM VARCHAR(5)
				,MAT_DOC_NO VARCHAR(10)
				,MAT_DOC_ITEM NUMERIC(4, 0)
				,COMP_PRICE_CD VARCHAR(4)
				,MAT_NO VARCHAR(23)
				,QUANTITY NUMERIC(13, 3)
				,INV_AMT NUMERIC(16, 5)
				,INV_AMT_LOCAL_CURR NUMERIC(16, 5)
				,PO_NO VARCHAR(10)
				,TAX_CD VARCHAR(2)
				,UNIT_OF_MEASURE_CD VARCHAR(3)
				)

			DECLARE @CnAllD INT;
			DECLARE @LoopD INT = 1;
			DECLARE @ROWNO INT;
			DECLARE @INV_NO VARCHAR(10);
			DECLARE @INV_DOC_ITEM VARCHAR(5);
			DECLARE @MAT_DOC_NO VARCHAR(10);
			DECLARE @MAT_DOC_ITEM NUMERIC(4, 0);
			DECLARE @COMP_PRICE_CD VARCHAR(4);
			DECLARE @MAT_NO VARCHAR(23);
			DECLARE @QUANTITY NUMERIC(13, 3);
			DECLARE @INV_AMT NUMERIC(16, 5);
			DECLARE @INV_AMT_LOCAL_CURR NUMERIC(16, 5);
			DECLARE @PO_NO VARCHAR(10);
			DECLARE @TAX_CD VARCHAR(2);
			DECLARE @UNIT_OF_MEASURE_CD VARCHAR(3);

			INSERT INTO #cur_inv_det
			SELECT DISTINCT ROWNO
				,INV_NO
				,INV_DOC_ITEM
				,MAT_DOC_NO
				,MAT_DOC_ITEM
				,COMP_PRICE_CD
				,MAT_NO
				,QUANTITY
				,INV_AMT
				,INV_AMT_LOCAL_CURR
				,PO_NO
				,TAX_CD
				,UNIT_OF_MEASURE_CD
			FROM TB_T_INVOICE_D
			WHERE PROCESS_ID = @PROCESS_ID
				AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

			IF @LivBase = '1'
			BEGIN
				UPDATE A
				SET INV_NO = B.INV_NO
					,INV_ITEM_NO = B.INV_DOC_ITEM
					,LIV_SEND_FLAG = 'Y'
					,CHANGED_BY = @USER_ID
					,CHANGED_DT = GETDATE()
				FROM TB_R_GR_IR A 
				JOIN #cur_inv_det  B ON A.Mat_Doc_No = B.MAT_DOC_NO
					AND A.ITEM_NO = B.MAT_DOC_ITEM;

				/* remark because no need to update to ifast and issue performance 
				SET @query = 'update a
				SET a.INV_NO = b.INV_NO, a.LIV_SEND_FLAG = ''Y'', a.INV_DOC_ITEM = b.INV_DOC_ITEM
				from ' + @ICSSrvrName + '.' + @ICSDBName + 'TB_R_GR_IR a
				join #cur_inv_det b on A.Mat_Doc_No = B.MAT_DOC_NO
					AND A.ITEM_NO = B.MAT_DOC_ITEM';

				EXEC (@query);*/

			END

			/*ELSE IF @LivBase = '2'
		BEGIN
			SET @LoopD = 1;

			SELECT @CnAllD = COUNT(1)
			FROM #cur_inv_det

			WHILE @LoopD <= @CnAllD
			BEGIN
				SELECT @ROWNO = ROWNO
					,@INV_NO = INV_NO
					,@INV_DOC_ITEM = INV_DOC_ITEM
					,@MAT_DOC_NO = MAT_DOC_NO
					,@MAT_DOC_ITEM = MAT_DOC_ITEM
					,@COMP_PRICE_CD = COMP_PRICE_CD
				FROM #cur_inv_det
				WHERE ROWNO = @LoopD

				-- update DETAIL TABLE
	            UPDATE A
	            SET    INVOICE_FLAG = 'Y',
	                   CHANGED_BY   = ri_v_user_id,
	                   CHANGED_DT   = l_d_changed_dt
				FROM TB_R_PO_D A
	            WHERE  PO_NO = @PO_NO
	            AND    PO_ITEM = @PO_ITEM_NO
	            AND    COMP_PRICE_CD = @COMP_PRICE_CD;
	          
	            -- update HEADER
	            UPDATE A
	            SET    CHANGED_BY = @USER_ID,
	                   CHANGED_DT = GETDATE()
				FROM TB_R_PO_H A
	            WHERE  PO_NO = @PO_NO;
	          
	            -- update ITEM TABLE
	            UPDATE A
	            SET    CHANGED_BY = @USER_ID,
	                   CHANGED_DT = GETDATE()
				FROM TB_R_PO_ITEM A
	            WHERE  PO_NO = @PO_NO
	            AND    PO_ITEM_NO = @PO_ITEM_NO;

				SET @LoopD = @LoopD + 1;
			END
		END*/
			--* Step 8. Journal Header Posting *--

			
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert FI Trans Header'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO TB_R_FI_TRANSACTION_H (
				DOC_NO
				,DOC_YEAR
				,DOC_DT
				,POSTING_DT
				,DOC_HEADER_TXT
				,SUPP_CD
				,PAYMENT_TERM_CD
				,PAYMENT_METHOD_CD
				,PARTNER_BANK_KEY
				,REFF_NO
				,CREATED_BY
				,CREATED_DT
				,CHANGED_BY
				,CHANGED_DT
				) (
				SELECT DISTINCT INV_NO
				,CONVERT(VARCHAR(4), INV_DT, 112)
				,BASELINE_DT
				,INV_POSTING_DT
				,HEADER_TEXT + '|' + ISNULL(ASSIGNMENT,'')
				,SUPP_CD
				,PAYMENT_TERM_CD
				,PAYMENT_METHOD_CD
				,PARTNER_BANK_KEY
				,SUPP_INVOICE_NO
				,@USER_ID
				,GETDATE()
				,@USER_ID
				,GETDATE()
				FROM TB_T_INVOICE_H WHERE PROCESS_ID = @PROCESS_ID
				AND SUPP_INVOICE_NO = @SupplierInvoiceNo
				);

			
			--* Step 9. Journal Detail Posting Based on Invoice Detail *--
			DECLARE @LastIdx INT = 0;
			DECLARE @TempIdx INT = 0;

			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert FI Trans Detail Based on Invoice Detail'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO TB_R_FI_TRANSACTION_D (
					DOC_NO
					,DOC_YEAR
					,DOC_ITEM
					,TRANS_TYPE_CD
					,CONDITION_TYPE
					,MAT_NO
					,DOC_QTY
					,DOC_AMT
					,DOC_CURR_CD
					,LOCAL_AMT
					,LOCAL_CURR_CD
					,EXCHANGE_RATE
					,PO_NO
					,TAX_CD
					,VALUATION_CLASS_CD
					,UNIT_OF_MEASURE_CD
					,CREATED_BY
					,CREATED_DT
					,CHANGED_BY
					,CHANGED_DT
					,ITEM_TEXT
					,WBS_NO
					)
			SELECT A.INV_NO
				,CONVERT(VARCHAR(4), B.inv_dt, 112)
				,RIGHT('00000' + CONVERT(VARCHAR(5), (ROW_NUMBER() OVER(ORDER BY (SELECT 1)))), 5)
				,'201'
				,A.COMP_PRICE_CD
				,A.MAT_NO
				,A.QUANTITY
				,A.INV_AMT
				,B.payment_curr
				,ROUND(A.INV_AMT_LOCAL_CURR, 0)
				,@l_v_local_curr_CD
				,B.exchange_rate
				,A.PO_NO
				,A.TAX_CD
				,@l_v_valuation_class
				,A.UNIT_OF_MEASURE_CD
				,@USER_ID
				,GETDATE()
				,@USER_ID
				,GETDATE()
				,B.INV_DESCRIPTION
				,CONVERT(VARCHAR, B.INV_DT, 112) + '|' + CONVERT(VARCHAR, B.TAX_DT, 112)
			FROM TB_T_INVOICE_D A
			JOIN TB_t_invoice_h B ON A.SUPP_INVOICE_NO = B.SUPP_INVOICE_NO
				AND A.PROCESS_ID = B.PROCESS_ID
			WHERE A.process_id = @PROCESS_ID
				AND B.supp_invoice_no = @SupplierInvoiceNo;

			--* Step 10. Journal Detail Posting Based on invoice GL account *--
			SELECT @TempIdx = COUNT(1) 
			FROM TB_T_INVOICE_D 
			WHERE process_id = @PROCESS_ID
				AND supp_invoice_no = @SupplierInvoiceNo;

			SET @LastIdx = @LastIdx + @TempIdx;

			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert FI Trans Detail Based on invoice GL account'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO TB_R_FI_TRANSACTION_D (
					DOC_NO
					,DOC_YEAR
					,DOC_ITEM
					,TRANS_TYPE_CD
					,CONDITION_TYPE
					,GL_ACCOUNT
					,DOC_AMT
					,DOC_CURR_CD
					,LOCAL_AMT
					,LOCAL_CURR_CD
					,EXCHANGE_RATE
					,TAX_CD
					,COST_CENTER_CD
					,VALUATION_CLASS_CD
					,CREATED_BY
					,CREATED_DT
					,CHANGED_BY
					,CHANGED_DT
					,ITEM_TEXT
					,WBS_NO
					)
			SELECT IG.INV_NO
				,CONVERT(VARCHAR(4), IH.inv_dt, 112)
				,RIGHT('00000' + CONVERT(VARCHAR(5), (ROW_NUMBER() OVER(ORDER BY (SELECT 1))) + @LastIdx), 5)
				,'201'
				,@lv_liv_gl_acct
				,IG.GL_ACCOUNT
				,IG.INV_AMT
				,IH.PAYMENT_CURR
				,ROUND(IG.INV_AMT * IH.EXCHANGE_RATE, 0)
				,@l_v_local_curr_CD
				,IH.exchange_rate
				,IG.TAX_CD
				,IG.COST_CENTER
				,@l_v_valuation_class
				,@USER_ID
				,GETDATE()
				,@USER_ID
				,GETDATE()
				,IH.INV_DESCRIPTION
				,CONVERT(VARCHAR, IH.INV_DT, 112) + '|' + CONVERT(VARCHAR, IH.TAX_DT, 112)
			FROM TB_T_INVOICE_GL_ACCOUNT IG 
			JOIN TB_t_invoice_h IH ON IG.SUPP_INVOICE_NO = IH.SUPP_INVOICE_NO
				AND IG.PROCESS_ID = IH.PROCESS_ID
			WHERE IG.process_id = @PROCESS_ID
				AND IG.supp_invoice_no = @SupplierInvoiceNo;

			--* Step 11. Journal Detail Posting Based on invoice withholding tax *--
			SELECT @TempIdx = COUNT(1) 
			FROM TB_T_INVOICE_GL_ACCOUNT 
			WHERE process_id = @PROCESS_ID
				AND supp_invoice_no = @SupplierInvoiceNo;

			SET @LastIdx = @LastIdx + @TempIdx;

			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert FI Trans Detail Based on invoice withholding tax'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO TB_R_FI_TRANSACTION_D (
					DOC_NO
					,DOC_YEAR
					,DOC_ITEM
					,TRANS_TYPE_CD
					,CONDITION_TYPE
					,DOC_AMT
					,DOC_CURR_CD
					,LOCAL_AMT
					,LOCAL_CURR_CD
					,EXCHANGE_RATE
					,TAX_CD
					,VALUATION_CLASS_CD
					,CREATED_BY
					,CREATED_DT
					,CHANGED_BY
					,CHANGED_DT
					,ITEM_TEXT
					,WBS_NO
					)
			SELECT IW.INV_NO
				,CONVERT(VARCHAR(4), IH.INV_DT, 112)
				,RIGHT('00000' + CONVERT(VARCHAR(5), (ROW_NUMBER() OVER(ORDER BY (SELECT 1))) + @LastIdx), 5)
				,'201'
				,'WH' + IW.WITHOLDING_TAX_CD
				,IW.WITHOLDING_TAX_AMT
				,IH.PAYMENT_CURR
				,ROUND(IW.WITHOLDING_TAX_AMT * IH.EXCHANGE_RATE, 0)
				,@l_v_local_curr_CD
				,IH.EXCHANGE_RATE
				,IW.WITHOLDING_TAX_CD
				,@l_v_valuation_class
				,@USER_ID
				,GETDATE()
				,@USER_ID
				,GETDATE()
				,IH.INV_DESCRIPTION
				,CONVERT(VARCHAR, IH.INV_DT, 112) + '|' + CONVERT(VARCHAR, IH.TAX_DT, 112)
			FROM TB_T_INV_WITHOLDING_TAX IW 
			JOIN TB_t_invoice_h IH ON IW.SUPP_INVOICE_NO = IH.SUPP_INVOICE_NO
				AND IW.PROCESS_ID = IH.PROCESS_ID
			WHERE IW.process_id = @PROCESS_ID
				AND IW.supp_invoice_no = @SupplierInvoiceNo;


			--* Step 12. Journal Detail Posting Based on invoice tax
			SELECT @TempIdx = COUNT(1) 
			FROM TB_T_INV_WITHOLDING_TAX 
			WHERE process_id = @PROCESS_ID
				AND supp_invoice_no = @SupplierInvoiceNo;

			SET @LastIdx = @LastIdx + @TempIdx;

			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Insert FI Trans Detail Based on invoice tax'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO TB_R_FI_TRANSACTION_D (
					DOC_NO
					,DOC_YEAR
					,DOC_ITEM
					,TRANS_TYPE_CD
					,CONDITION_TYPE
					,DOC_AMT
					,DOC_CURR_CD
					,LOCAL_AMT
					,LOCAL_CURR_CD
					,EXCHANGE_RATE
					,TAX_CD
					,VALUATION_CLASS_CD
					,CREATED_BY
					,CREATED_DT
					,CHANGED_BY
					,CHANGED_DT
					,ITEM_TEXT
					,WBS_NO
					)
			SELECT  IT.INV_NO
				,CONVERT(VARCHAR(4), IH.inv_dt, 112)
				,RIGHT('00000' + CONVERT(VARCHAR(5), (ROW_NUMBER() OVER(ORDER BY (SELECT 1))) + @LastIdx), 5)
				,'201'
				,IT.ACCOUNT_KEY
				,IT.TAX_AMT
				,IH.PAYMENT_CURR
				,IT.LOCAL_TAX_AMT
				,@l_v_local_curr_CD
				,IH.exchange_rate
				,IT.TAX_CD
				,@l_v_valuation_class
				,@USER_ID
				,GETDATE()
				,@USER_ID
				,GETDATE()
				,IH.INV_DESCRIPTION
				,CONVERT(VARCHAR, IH.INV_DT, 112) + '|' + CONVERT(VARCHAR, IH.TAX_DT, 112)
			FROM TB_T_INV_TAX IT 
			JOIN TB_t_invoice_h IH ON IT.SUPP_INVOICE_NO = IH.SUPP_INVOICE_NO
				AND IT.PROCESS_ID = IH.PROCESS_ID
			WHERE IH.process_id = @PROCESS_ID
				AND IH.supp_invoice_no = @SupplierInvoiceNo;

		END

		IF @IsError = 'N'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Save Process'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			RETURN 0;
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Save Process'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			RETURN 1;
		END
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		-- ROLLBACK DATA
		--DELETE fROM TB_R_FI_TRANSACTION_D WHERE DOC_NO = @InvNo
		--DELETE fROM TB_R_FI_TRANSACTION_H WHERE DOC_NO = @InvNo
		--DELETE fROM TB_R_INV_GL_ACCOUNT WHERE INV_NO = @InvNo
		--DELETE fROM TB_R_INVOICE_WITHOLDING_TAX WHERE INV_NO = @InvNo
		--DELETE fROM TB_R_INV_D WHERE INV_NO = @InvNo
		--DELETE fROM TB_R_INV_TAX WHERE INVOICE_NO = @InvNo
		--DELETE fROM TB_R_INV_H WHERE INV_NO = @InvNo

		RETURN 1;
	END CATCH
END
