-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetSupplierMaster]
AS
BEGIN
	SELECT [SUPPLIER_CD] SUPPLIER_CODE
      ,[SUPPLIER_PLANT_CD]
      ,ISNULL([SUPPLIER_ABBREVIATION],'') [SUPPLIER_ABBREVIATION]
      ,ISNULL([SUPPLIER_NAME],'') [SUPPLIER_NAME]
      ,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
  FROM [dbo].[TB_T_CED_SUPPLIER]
END

