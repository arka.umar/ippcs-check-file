CREATE PROCEDURE [dbo].[SP_DLV_INV_SaveInvoice_13072015]
   @PO_NO varchar(max),
   @LP_INV_NO varchar(max),
   @INV_NO as varchar(max),
   @INV_DT as varchar(max),
   @INV_TAX_CD as varchar(max),
   @INV_TAX_DT as varchar(max),
   @STATUS_CD as varchar(2),
   @UserId as varchar(20),
   @DLV_NO as varchar(max),
	 @LP_INV_NO_GRID AS VARCHAR (MAX) --added 29042014

AS
BEGIN
	--SET NOCOUNT ON;

	DECLARE @MODULE_ID as varchar(1) = '3'
	DECLARE @FUNCTION_ID as varchar(5) = '31401'
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_INV_SaveInvoice'
	DECLARE @step AS VARCHAR(50) = 'init'
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @errorMessage as varchar(max)
	
	DECLARE @count2 as int = (SELECT COUNT(1) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND INV_TEXT IS NOT NULL AND CONDITION_CATEGORY = 'H' AND STATUS_CD <> -3)
	DECLARE @count  as int = (SELECT COUNT(1) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND INV_TEXT IS NULL AND CONDITION_CATEGORY = 'H' AND STATUS_CD <> -3)
	DECLARE @LP_CD as varchar(3);

	DECLARE @tempSplitDelivery AS TABLE (
		ID int NOT NULL identity(1,1),
		name varchar(14)
	)
	
	INSERT INTO @tempSplitDelivery (name)
	EXEC dbo.SplitString @DLV_NO, ','
	
	--added fid.deny 2015-07-03
	DECLARE @tempStatusCode AS TABLE (
		statuscd int
	)
	--reference STATUS_CD NOT IN ('1', '-1', '-3')
	INSERT INTO @tempStatusCode(statuscd) VALUES(-5)
	INSERT INTO @tempStatusCode(statuscd) VALUES(-4)
	INSERT INTO @tempStatusCode(statuscd) VALUES(-2)
	INSERT INTO @tempStatusCode(statuscd) VALUES(2)
	INSERT INTO @tempStatusCode(statuscd) VALUES(3)
	INSERT INTO @tempStatusCode(statuscd) VALUES(4)
	INSERT INTO @tempStatusCode(statuscd) VALUES(5)
	
	--end of added
	
	DECLARE @iData int, @cntData int
	
	IF (@LP_INV_NO_GRID = '') AND (@count>0) BEGIN
		SET @log = 'Data already exist. By '+@na;
		INSERT @T
		EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00005ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
		SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Data already exist' as [MESSAGE]
	END
	ELSE IF(@count2>0) BEGIN
		SET @log = 'Data already exist. By '+@na;
		INSERT @T
		EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00005ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
		SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Data already exist' as [MESSAGE]
	END
	ELSE BEGIN
			IF(@count>0) BEGIN

					SET @pid = (SELECT TOP 1 PROCESS_ID FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO ORDER BY SEQ_NO DESC)
					SET @LP_CD = (SELECT TOP 1 LP_CD FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO ORDER BY SEQ_NO DESC)
					
					SET @log = 'Saving invoice By '+@na;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
					
					--added fid.deny 2015-07-07
					DECLARE @paymentMethod varchar(20)
					SET @paymentMethod = (SELECT PAYMENT_METHOD_CD FROM TB_R_DLV_PO_H WHERE PO_NO = @PO_NO);

					DECLARE @paymentTerm varchar(20)
					SET @paymentTerm = (SELECT PAYMENT_TERM_CD FROM TB_R_DLV_PO_H WHERE PO_NO = @PO_NO)

				
					DECLARE @LLPCDUpdate varchar(100)
					SET @LLPCDUpdate = dbo.FN_CERTID(@LP_CD)
					--end of added fid.deny 2015-07-07

					BEGIN TRY 
							UPDATE TB_R_DLV_INV_UPLOAD 
							SET 
							INV_NO = NULL, 
							INV_DT = @INV_DT, 
							INV_TAX_DT = @INV_TAX_DT, 
							BASELINE_DT = @INV_TAX_DT,
							STATUS_CD = 2,
							IN_PROGRESS = NULL,
							CERTIFICATE_ID = @LLPCDUpdate,
							PAYMENT_METHOD_CD = @paymentMethod,
							PAYMENT_TERM_CD = @paymentTerm
							WHERE PO_NO = @PO_NO AND LP_INV_NO = @LP_INV_NO 
							--add by alira.agi 2015-
							and STATUS_CD <> -3
							
							UPDATE TB_R_DLV_INV_UPLOAD SET INV_TEXT = @INV_TAX_CD WHERE PO_NO = @PO_NO AND LP_INV_NO = @LP_INV_NO AND CONDITION_CATEGORY = 'H'
							--add by alira.agi 2015-
							and STATUS_CD <> -3

							UPDATE TB_R_DLV_GR_IR
							SET STATUS_CD = 2 
							WHERE 
								EXISTS (SELECT '1' FROM TB_R_DLV_INV_UPLOAD D WHERE D.LP_INV_NO = @LP_INV_NO AND D.DELIVERY_NO = REFF_NO)
								AND EXISTS (SELECT '1' FROM TB_R_DLV_INV_UPLOAD IU WHERE IU.LP_INV_NO = @LP_INV_NO AND SERVICE_DOC_NO = IU.SERVICE_DOC_NO)
								AND STATUS_CD <> -3
							

							SET @log = 'Saving invoice success By '+@na;
							INSERT @T
							EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
							SELECT 'success' as [STATUS], 'INF' as [TYPE], 'Saving invoice success' as [MESSAGE]
					
							-- 20150519 / EFaktur / alira.dita / begin
							-- commented EXEC dbo.sp_PutLog 'Updating E-Faktur: begin', @UserId, 'SP_DLV_INV_SaveInvoice.Process', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

							UPDATE TMMIN_E_FAKTUR_UAT.dbo.TB_R_VAT_IN_H SET USED = 'Y', CHANGED_BY = @UserId, CHANGED_DT = GETDATE()
							WHERE SAP_TAX_INVOICE_NO = @INV_TAX_CD AND 
								SUPPLIER_CODE IN (SELECT DISTINCT LP_CD FROM TB_R_DLV_INV_UPLOAD DID
													WHERE EXISTS (SELECT 'x' FROM @tempSplitDelivery ts WHERE ts.name = DID.DELIVERY_NO) )


							EXEC dbo.sp_PutLog 'Updating E-Faktur: end', @UserId, 'SP_DLV_INV_SaveInvoice.Process', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
							EXEC dbo.sp_PutLog 'Saving Invoice finish', @UserId, 'SP_DLV_INV_SaveInvoice.Finish', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
							-- 20150519 / EFaktur / alira.dita / end

					END TRY
					BEGIN CATCH
							SET @errorMessage = ERROR_MESSAGE();
							SET @log = 'Saving invoice failed : '+@errorMessage+'. By '+@na;
							INSERT @T
							EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
							SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Saving invoice failed' as [MESSAGE]
					END CATCH
			END
			ELSE BEGIN
					SET @log = 'Saving invoice By '+@na;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
					select '3'
					INSERT INTO TB_R_DLV_QUEUE(PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, PROCESS_END_DT, CREATED_BY, CREATED_DT)
					SELECT @pid as PROCESS_ID, @MODULE_ID, @FUNCTION_ID, 'QU1', GETDATE(), NULL, @UserId, GETDATE()
					
					
					
					BEGIN TRY
							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU2' WHERE PROCESS_ID = @pid

							
--===29042014===--
							DECLARE @CHECK_LP_INV INT; --FROM SELECTION GRID
							DECLARE @CHECK_REFF INT, @CHECK_TUPLOAD INT;
							DECLARE @LP_NAME varchar(MAX);
							DECLARE @SERVICE_DOC_NO VARCHAR(MAX), @ROUTE_CD VARCHAR (5), @COMP_PRICE_CD VARCHAR (6), @AMOUNT VARCHAR (MAX), @TRANSACTION_CURR VARCHAR (4);
							
							DECLARE @DELIVERY_NO varchar(15);
							
							
							SET @CHECK_REFF = (SELECT COUNT (1) FROM TB_R_DLV_SERVICE_DOC WHERE PROCESS_ID IS not NULL AND REFF_NO = @DELIVERY_NO )
							if @CHECK_REFF>0
							begin 
									SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Error, Service document already process' as [MESSAGE];
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
									RETURN
							end
							else
							begin
								--TB_R_DLV_SERVICE_DOC update process id if data insert correct
								update a
								set a.PROCESS_ID = @PID
								from 
								TB_R_DLV_SERVICE_DOC a
								join @tempSplitDelivery b
								on a.REFF_NO=b.name 
								where --modified fid.deny 2015-07-02 (using exists)
								EXISTS (SELECT 'x' 
											FROM TB_R_DLV_INV_UPLOAD z 
										WHERE 
											LP_INV_NO = @LP_INV_NO AND 
											PO_NO = @PO_NO AND 
											EXISTS (SELECT 'x' FROM @tempStatusCode t WHERE t.statuscd = Z.STATUS_CD) AND 
											z.DELIVERY_NO = a.REFF_NO)

								--insert data TB_T_DLV_INV_UPLOAD
								INSERT INTO TB_T_DLV_INV_UPLOAD (SERVICE_DOC_NO, LP_NAME, LP_CD, PO_NO, ROUTE_CD, DELIVERY_NO, COMP_PRICE_CD, DOC_DT, QTY, LP_INV_NO, PRICE_AMT, PAYMENT_CURR, PROCESS_ID, REMARKS)
								select b.SERVICE_DOC_NO ,d.LOG_PARTNER_NAME LP_NAME,d.LOG_PARTNER_CD LP_CD,@PO_NO PO_NO, c.ROUTE_CD,
								a.name DLV_NO,e.COMP_PRICE_CD,getdate() DOC_DT,1 qty,@LP_INV_NO LP_INV_NO,e.GR_IR_AMT as AMOUNT,
								e.ITEM_CURR as TRANSACTION_CURR,@pid pid,
								--modified fid.deny 2015-07-02 (using exists)
								case when (SELECT COUNT (1) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND PO_NO = @PO_NO AND 
									EXISTS (SELECT 'x' FROM @tempStatusCode tc WHERE tc.statuscd = STATUS_CD)
								) = 0
								then 'Correct'
								else 
								'Incorrect'
								end
								
								from @tempSplitDelivery a
								join TB_R_DLV_GR_IR b on a.name=b.REFF_NO and b.PO_NO=@PO_NO AND CANCEL_STS IS NULL 
								join TB_R_DLV_SERVICE_DOC c on c.REFF_NO=a.name and c.PO_NO=@PO_NO
								join TB_M_LOGISTIC_PARTNER d on d.LOG_PARTNER_CD=b.LP_CD
								join TB_R_DLV_GR_IR e on e.REFF_NO=a.name and e.PO_NO=@PO_NO AND e.CANCEL_STS IS NULL  AND e.CONDITION_CATEGORY = 'H'
								group by b.SERVICE_DOC_NO,c.ROUTE_CD,d.LOG_PARTNER_NAME,e.COMP_PRICE_CD,e.GR_IR_AMT,e.ITEM_CURR ,a.name,d.LOG_PARTNER_CD

							end
							--end bulk insert

							SET @CHECK_TUPLOAD = (SELECT COUNT(1) FROM TB_T_DLV_INV_UPLOAD WHERE REMARKS = 'Incorrect' AND PROCESS_ID = @pid);

							IF ( @CHECK_TUPLOAD = 0 ) BEGIN
							
									UPDATE TB_R_DLV_GR_IR 
										SET STATUS_CD = 2 
									WHERE 
										EXISTS (SELECT 'x' FROM @tempSplitDelivery rn WHERE rn.name = REFF_NO)	--modified fid.deny 2015-07-02 (using exists)
										AND CANCEL_STS IS NULL 
									
									--added fid.deny 2015-07-07
									DECLARE @datenow date = GETDATE()
									DECLARE @defaultTaxCode varchar(20)
									SET @defaultTaxCode = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE TB_M_SYSTEM.SYSTEM_CD = 'DEFAULT_TAX_CODE')

									DECLARE @LPCD varchar(10)
									DECLARE @LLPCD varchar(100)
									SET @LPCD = (select TOP 1 LP_CD from TB_R_DLV_GR_IR a
													WHERE 
													EXISTS (SELECT 'x' FROM @tempSplitDelivery crn WHERE crn.name = a.REFF_NO)	--modified fid.deny 2015-07-02 (using exists)
													AND a.CANCEL_STS IS NULL 
													AND STATUS_CD <> -3)
									SET @LLPCD = dbo.FN_CERTID(@LPCD)
									--end of added


									INSERT INTO TB_R_DLV_INV_UPLOAD(PO_NO, PO_ITEM_NO, DELIVERY_NO, COMP_PRICE_CD, SEQ_NO, LP_INV_NO, ROUTE_CD, LP_CD, SERVICE_DOC_NO, DOC_DT, QTY, S_QTY, PRICE_AMT, PAYMENT_CURR, REMARKS, INV_AMT, INV_AMT_LOCAL_CURR, CONDITION_CATEGORY, CURR_CD, INV_TAX_CD, INV_TAX_AMT, UOM, IN_PROGRESS, STATUS_CD, CERTIFICATE_ID, CREATED_BY, CREATED_DT, INV_DT, INV_TAX_DT, INV_TEXT, PAYMENT_METHOD_CD, PAYMENT_TERM_CD, BASELINE_DT, PARTNER_BANK_KEY, PROCESS_ID)
									SELECT 
									a.PO_NO, 
									a.PO_ITEM_NO, 
									a.REFF_NO as DELIVERY_NO, 
									a.COMP_PRICE_CD, 
									ISNULL((SELECT MAX(si.SEQ_NO)+1 AS SEQ_NO FROM TB_R_DLV_INV_UPLOAD si WHERE si.DELIVERY_NO = a.REFF_NO AND si.COMP_PRICE_CD = a.COMP_PRICE_CD), '1'),
									@LP_INV_NO as LP_INV_NO,
									b.ROUTE_CD,
									a.LP_CD,
									a.SERVICE_DOC_NO,
									@datenow as DOC_DT,
									1 as QTY,
									1 as S_QTY,
									PO_DETAIL_PRICE as PRICE_AMT,
									'IDR' as PAYMENT_CURR,
									'Correct' as REMARKS,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN a.PO_DETAIL_PRICE ELSE NULL END as INV_AMT,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN a.PO_DETAIL_PRICE ELSE NULL END as INV_AMT_LOCAL_CURR,
									a.CONDITION_CATEGORY as CONDITION_CATEGORY,
									'IDR' as CURR_CD,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN @defaultTaxCode ELSE NULL END as INV_TAX_CD,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN (0.1 * a.GR_IR_AMT) ELSE NULL END as INV_TAX_AMT,
									'RET' as UOM,
									NULL as IN_PROGRESS,
									2 as STATUS_CD,
									@LLPCD as CERTIFICATE_ID,
									@UserId as CREATED_BY,
									@datenow as CREATED_DT,
									@INV_DT, 
									@INV_TAX_DT,
									CASE WHEN a.CONDITION_CATEGORY = 'H' THEN @INV_TAX_CD ELSE NULL END as INV_TAX_CD,
									d.PAYMENT_METHOD_CD as PAYMENT_METHODO_CD,
									d.PAYMENT_TERM_CD as PAYMENT_TERM_CD,
									@INV_TAX_DT,
									--@BASELINE_DT,
									1 as PARTNER_BANK_KEY,
									@pid
									FROM TB_R_DLV_GR_IR a
									LEFT JOIN TB_R_DLV_SERVICE_DOC b ON b.SERVICE_DOC_NO = a.SERVICE_DOC_NO
									LEFT JOIN TB_R_DLV_PO_ITEM c ON c.PO_NO = b.PO_NO AND c.ROUTE_CD = b.ROUTE_CD
									--LEFT JOIN TB_M_LOGISTIC_PARTNER d ON d.LOG_PARTNER_CD = a.LP_CD
									LEFT JOIN TB_R_DLV_PO_H d ON d.PO_NO = a.PO_NO
									
									WHERE 
										EXISTS (SELECT 'x' FROM @tempSplitDelivery crn WHERE crn.name = a.REFF_NO)	--modified fid.deny 2015-07-02 (using exists)
										AND a.CANCEL_STS IS NULL 
										AND STATUS_CD <> -3
									
									---=== NEW FIXING 2014-10-22 BY RAHMAT --modified fid.deny 2015-07-02 (using exists)
									UPDATE TB_R_DELIVERY_CTL_H
										SET DELIVERY_STS = 'Invoiced'
									WHERE 
										EXISTS (SELECT 'x' FROM @tempSplitDelivery crrn WHERE crrn.name = DELIVERY_NO)
									
									UPDATE TB_R_DLV_GR_IR --modified fid.deny 2015-07-02 (using exists)
										SET LIV_FLAG = 1 
									WHERE 
										EXISTS (SELECT 'x' FROM @tempSplitDelivery crnn WHERE crnn.name = REFF_NO)	
										AND CANCEL_STS IS NULL
										AND STATUS_CD <> -3
									
									SET @log = 'Saving invoice success By '+@na;
									INSERT @T
									EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU3', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
									SELECT 'success' as [STATUS], 'INF' as [TYPE], 'Saving invoice success' as [MESSAGE]
							
									-- 20150519 / EFaktur / alira.dita / begin
									EXEC dbo.sp_PutLog 'Updating E-Faktur: begin', @UserId, 'SP_DLV_INV_SaveInvoice.Process', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

									 UPDATE TMMIN_E_FAKTUR_UAT.dbo.TB_R_VAT_IN_H SET USED = 'Y', CHANGED_BY = @UserId, CHANGED_DT = GETDATE()
									 WHERE SAP_TAX_INVOICE_NO = @INV_TAX_CD AND 
										SUPPLIER_CODE IN (SELECT DISTINCT LP_CD FROM TB_R_DLV_INV_UPLOAD DIU
									 						WHERE EXISTS (SELECT 'x' FROM @tempSplitDelivery tsd WHERE tsd.name = DIU.DELIVERY_NO))

									EXEC dbo.sp_PutLog 'Updating E-Faktur: end', @UserId, 'SP_DLV_INV_SaveInvoice.Process', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
									EXEC dbo.sp_PutLog 'Saving Invoice finish', @UserId, 'SP_DLV_INV_SaveInvoice.Finish', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
									-- 20150519 / EFaktur / alira.dita / end

							END
							ELSE BEGIN 
									SET @log = 'Some data are incorrect, terminating process. by '+@na;
									INSERT @T
									EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODULE_ID, @FUNCTION_ID;
									
									
									SET @log = 'LIV verification failed By '+@na;
									INSERT @T
									EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODULE_ID, @FUNCTION_ID;
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
									SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'LIV verification failed' as [MESSAGE]


							END
							
							DELETE FROM TB_T_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid
							UPDATE TB_R_DLV_SERVICE_DOC SET  PROCESS_ID = NULL WHERE PROCESS_ID = @pid
							UPDATE TB_R_DLV_INV_UPLOAD SET  PROCESS_ID = NULL WHERE PROCESS_ID = @pid
					END TRY
					BEGIN CATCH
							SET @errorMessage = ERROR_MESSAGE();
							SET @log = 'Saving invoice failed : '+@errorMessage+'. By '+@na;
							INSERT @T
							EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_INV_SaveInvoice.init', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE() WHERE PROCESS_ID = @pid
							SELECT 'failed' as [STATUS], 'WRN' as [TYPE], 'Saving invoice failed' as [MESSAGE]

					END CATCH
			END
	END
END

