-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2020-09-30
-- Description:	This function perform to convert data from month-year or date to period-fiscal and from period-fiscal to month-year
-- * This function differentiate by option that other function should be fill in, from 1 - 6.
-- * 1. Convert month-year in format yyyymm into period-fiscal in format fffpp
-- * 2. Convert month-year in format mm.yyyy into period-fiscal in format pp.ffff
-- * 3. Convert period-fiscal in format ffffpp into month-year in format yyyymm
-- * 4. Convert period-fiscal in format pp.ffff into month-year in format mm.yyyy
-- * 5. Convert date in format dd.mm.yyyy into period-fiscal in format ffffpp
-- * 6. Convert date in format dd.mm.yyyy into period-fiscal in format pp.ffff
-- =============================================
CREATE PROCEDURE [dbo].[SP_INVOICE_CONVERT_MONTH_PERIOD] @PROCESS_ID BIGINT
	,@ri_n_option INT
	,@ri_v_mon_year CHAR(10)
	,@ri_v_per_fiscal CHAR(10)
	,@ri_v_date CHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_v_month     VARCHAR(2),
    @l_v_year      VARCHAR(4),
    @l_v_period    VARCHAR(2),
    @l_v_fiscal    VARCHAR(4),
    @l_v_indicator varchar(2);

	DECLARE @Res_Indicator VARCHAR(10);
	DECLARE @Res_Fiscal VARCHAR(15);
	DECLARE @Result VARCHAR(100);

	SELECT @Res_Indicator = [dbo].[FN_INVOICE_CHECK_OPTION](@ri_n_option)

	-- check Option (is blank, out of range or true) call function f_check_option
	IF (RIGHT(@Res_Indicator, 1) = 1)
	BEGIN
		IF @ri_n_option = 1
		BEGIN
			IF ISNULL(@ri_v_mon_year, '') = ''
			BEGIN
				SET @l_v_indicator = 'C';
			END
            -- Check format ri_v_mon_year call function for check date
			ELSE IF (SELECT [dbo].[FN_INVOICE_CHECK_DATE] (
				@ri_v_mon_year
				,'yyyymm'
				)) = 1
			BEGIN
				SET @l_v_month = SUBSTRING(@ri_v_mon_year, 5, 2);
				SET @l_v_year  = SUBSTRING(@ri_v_mon_year, 1, 4);

				--CALL Function Convertion From ri_v_mon_year to ri_v_per_fiscal
				EXEC SP_INVOICE_CONVERT_MONTH_TO_FISCAL @PROCESS_ID, @ri_n_option,@l_v_month,@l_v_year
				SELECT DISTINCT @Res_Fiscal = FISCAL FROM TB_T_RET_FISCAL WHERE PROCESS_ID = @PROCESS_ID

				IF ISNULL(@Res_Fiscal, '') <> ''
				BEGIN
					SET @l_v_indicator = '1';
				END
				ELSE
				BEGIN
					SET @l_v_indicator = '0';
				END
			END ELSE
			BEGIN
				SET @l_v_indicator = 'D';
			END

		END ELSE IF @ri_n_option = 2
		BEGIN
			IF ISNULL(@ri_v_mon_year, '') = ''
			BEGIN
				SET @l_v_indicator = 'C';
			END
			--CALL Function Convertion from ri_v_mon_year to ri_v_per_fiscal
			IF SUBSTRING(@ri_v_mon_year, 3, 1) = '.' 
			BEGIN
              IF (SELECT [dbo].[FN_INVOICE_CHECK_DATE] (
				@ri_v_mon_year
				,'mm.yyyy'
				)) = 1 
				BEGIN
					SET @l_v_month = SUBSTRING(@ri_v_mon_year, 1, 2);
					SET @l_v_year  = SUBSTRING(@ri_v_mon_year, 4, 4);

					--CALL Function Convertion from ri_v_mon_year to ri_v_per_fiscal
					EXEC SP_INVOICE_CONVERT_MONTH_TO_FISCAL @PROCESS_ID, @ri_n_option,@l_v_month,@l_v_year
					SELECT DISTINCT @Res_Fiscal = FISCAL FROM TB_T_RET_FISCAL WHERE PROCESS_ID = @PROCESS_ID

					IF ISNULL(@Res_Fiscal, '') <> ''
					BEGIN
					  SET @l_v_indicator = '1';
					END ELSE
					BEGIN
					  SET @l_v_indicator = '0';
					END
				END
              ELSE
			  BEGIN
                SET @l_v_indicator = 'E';
              END;
			END
            ELSE
			BEGIN
              SET @l_v_indicator = 'E';
            END;
		END
	END

	SELECT @Result = dbo.FN_INVOICE_FILL_STATUS_MESSAGE (
		@ri_n_option
		,@l_v_indicator
		,@ri_v_mon_year
		,@ri_v_per_fiscal
		,@ri_v_date
	)
	
	SELECT @Result
END
