CREATE PROCEDURE [dbo].[DataArchieving]
	@check VARCHAR(1) = NULL
AS
BEGIN
	DECLARE @TABLE_NAME VARCHAR(100),
			@REFERENCE_TABLE VARCHAR(100),
			@PROCESS_STATUS CHAR(2),
			@PROCESS_CONDITION VARCHAR(MAX),
			@IS_TRUNCATE CHAR(1),
			@IS_DELETE CHAR(1),
			@IS_BACKUP CHAR(1),
			@ARCHIEVE_RANGE INT,
			@SERVER_NAME VARCHAR(100),
			@DATABASE_NAME VARCHAR(100),
			@SCHEMA_NAME VARCHAR(100),
			@BACKUP_TABLE_NAME VARCHAR(100),
			@DEST_TABLE_EXISTS CHAR(1),
			@DEST_DATABASE_EXISTS CHAR(1),
			@ROW_PROCESSED BIGINT,
			@PROCESS_SEQ INT = 0,
			@SKIP char(1) = 'N',
			@QUERY NVARCHAR(MAX) = ''

	DECLARE @PROCESS_ID BIGINT = CONVERT(BIGINT, REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(30), GETDATE(), 21), '-', ''), ':', ''),' ', ''), '.', '')),
			@EXEC_TIME BIGINT = 0,
			@ERR_MESSAGE VARCHAR(MAX),
			@ERR_ACTION VARCHAR(15) = 'INITIALIZE',
			@DATENOW DATE = CONVERT(DATE, GETDATE())

	DECLARE @FINAL_STATUS VARCHAR(15) = 'SUCCESS',
			@SUC_BACKUP INT = 0,
			@SUC_DELETED INT = 0,
			@SUC_TRUNCATED INT = 0,
			@ERR_BACKUP INT = 0,
			@ERR_DELETED INT = 0,
			@ERR_TRUNCATED INT = 0,
			@EXCEPTION INT = 0,
			@SKIPPED INT = 0,
			@TABLES INT = 0,
			@IS_ZERO VARCHAR(1) = 'N'

	DECLARE @MAX_PROCESS_SEQ INT = (SELECT MAX(process_seq) FROM TB_M_TABLE_ARCHIVING_CONFIG)
	DECLARE @MIN_PROCESS_SEQ INT = (SELECT MIN(process_seq) FROM TB_M_TABLE_ARCHIVING_CONFIG)
	DECLARE @BREAK tinyint = 0
	DECLARE @st DATETIME

	SET @PROCESS_SEQ = @MIN_PROCESS_SEQ
	WHILE (@PROCESS_SEQ <= @MAX_PROCESS_SEQ) AND (@BREAK = 0)
	BEGIN
		SET @SKIP = 'N'
		SET @DEST_TABLE_EXISTS = 'N'
		SET @DEST_DATABASE_EXISTS = 'N'
		SET @QUERY = ''
		SET @st = GETDATE()
		SET @ROW_PROCESSED = 0
		SET @EXEC_TIME = 0
		SET @IS_ZERO = 'N'
	
		IF (EXISTS (SELECT 'x' FROM TB_M_TABLE_ARCHIVING_CONFIG 
					WHERE process_seq = @PROCESS_SEQ 
							AND ISNULL(NULLIF(now_archieving, ''), 'NOT_YET') = 'NOT YET'))
		BEGIN
				SELECT TOP 1 
					@TABLE_NAME = table_name, 
					@PROCESS_STATUS = last_process_status,
					@PROCESS_CONDITION = ISNULL(process_condition, ''),
					@IS_TRUNCATE = ISNULL(is_truncate, ''),
					@IS_DELETE = ISNULL(is_delete, ''),
					@IS_BACKUP = ISNULL(is_backup, ''),
					@ARCHIEVE_RANGE = ISNULL(archive_range, 0),
					@SERVER_NAME = ISNULL(backup_server, ''),
					@DATABASE_NAME = ISNULL(backup_database, ''),
					@SCHEMA_NAME = ISNULL(backup_schema, ''),
					@BACKUP_TABLE_NAME = ISNULL(backup_table, '')
				FROM TB_M_TABLE_ARCHIVING_CONFIG 
				WHERE process_seq = @PROCESS_SEQ

				SET @PROCESS_CONDITION = REPLACE(@PROCESS_CONDITION, '[archive_range]', CONVERT(VARCHAR(100), @ARCHIEVE_RANGE))
				SET @PROCESS_CONDITION = REPLACE(@PROCESS_CONDITION, '[now_date]', '''' + CONVERT(VARCHAR(15), @DATENOW) + '''')

			BEGIN TRY
				IF(NOT EXISTS (SELECT 'x'
							FROM TB_M_TABLE_ARCHIVING_CONFIG CG
							WHERE EXISTS (SELECT reference_table 
												 FROM TB_M_TABLE_ARCHIVING_CONFIG 
												 WHERE table_name = @TABLE_NAME
													   AND ISNULL(reference_table, '') = CG.table_name)
								  AND ISNULL(NULLIF(now_archieving,''), 'NOT YET') <> 'DONE'))
				BEGIN
					UPDATE TB_M_TABLE_ARCHIVING_CONFIG
						SET now_archieving = 'ON PROGRESS',
							last_process_id = @PROCESS_ID
					WHERE table_name = @TABLE_NAME

					IF(@IS_BACKUP = 'Y')
					BEGIN
						SET @ERR_ACTION = 'BACKUP'
						
						SET @QUERY = 'SELECT @ROW_PROCESSED = COUNT(1) FROM ' + @TABLE_NAME + ' WHERE 1=1' + 
									 CASE ISNULL(@PROCESS_CONDITION, '') WHEN '' THEN '' ELSE ' AND ' + ISNULL(@PROCESS_CONDITION, '') END
						
						IF(@check IS NULL)
						BEGIN
							EXEC sp_executesql @QUERY, N'@ROW_PROCESSED BIGINT OUTPUT', @ROW_PROCESSED OUT
						END
						ELSE
						BEGIN
							SET @ROW_PROCESSED = 1
						END

						IF((@ROW_PROCESSED = 0) AND (@IS_TRUNCATE <> 'Y'))
						BEGIN
							SET @IS_ZERO = 'Y'
							SET @ERR_MESSAGE = 'No Data Found'
							RAISERROR(@ERR_MESSAGE, 16, 1)
						END
						
						SELECT @SERVER_NAME = CASE WHEN @SERVER_NAME = '' THEN name ELSE @SERVER_NAME END FROM sys.servers WHERE server_id = 0

						IF(NOT EXISTS (SELECT 'x' FROM sys.servers WHERE name = @SERVER_NAME))
						BEGIN
							SET @ERR_ACTION = 'EXCEPTION'
							SET @ERR_MESSAGE = 'Server ' + @SERVER_NAME + ' Does Not Exists or Not Registered as Linked Server'
							RAISERROR(@ERR_MESSAGE, 16, 1)
						END
						ELSE
						BEGIN
							SET @QUERY = 'SELECT @DEST_DATABASE_EXISTS = ''Y'' FROM [' + @SERVER_NAME + '].[master].sys.databases WHERE name = ''' + @DATABASE_NAME + ''''
							EXEC sp_executesql @QUERY, N'@DEST_DATABASE_EXISTS CHAR(1) OUTPUT', @DEST_DATABASE_EXISTS OUT;
						END

						IF(@DEST_DATABASE_EXISTS = 'Y')
						BEGIN
							SET @QUERY = 'SELECT @SCHEMA_NAME = TABLE_SCHEMA, @DEST_TABLE_EXISTS = ''Y'' FROM [' + @SERVER_NAME + '].[' + @DATABASE_NAME + 
										 '].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''' + @BACKUP_TABLE_NAME + ''''
							EXEC sp_executesql @QUERY, N'@DEST_TABLE_EXISTS CHAR(1) OUTPUT, @SCHEMA_NAME VARCHAR(100) OUTPUT', @DEST_TABLE_EXISTS OUT, @SCHEMA_NAME OUT;
						END
						ELSE
						BEGIN
							SET @ERR_ACTION = 'EXCEPTION'
							SET @ERR_MESSAGE = 'Database ' + @DATABASE_NAME + ' Does Not Exists in Server ' + @SERVER_NAME
							RAISERROR(@ERR_MESSAGE, 16, 1)
						END

						IF(@DEST_TABLE_EXISTS = 'Y')
						BEGIN
							SET @SCHEMA_NAME = CASE WHEN ISNULL(@SCHEMA_NAME, '') = '' THEN 'dbo' ELSE @SCHEMA_NAME END
							
							IF(EXISTS (SELECT 'x' FROM sys.servers WHERE name = @SERVER_NAME AND server_id = 0))
							BEGIN
								SET @QUERY = 'INSERT INTO [' + @DATABASE_NAME + '].[' + @SCHEMA_NAME + '].[' + @BACKUP_TABLE_NAME + ']'
							END
							ELSE
							BEGIN
								SET @QUERY = 'INSERT OPENQUERY([' + @SERVER_NAME + '], ''select * from [' + @DATABASE_NAME + '].[' + @SCHEMA_NAME + '].[' + @BACKUP_TABLE_NAME + ']'')'
							END

							SET @QUERY = @QUERY + ' SELECT * FROM ' + @TABLE_NAME + ' WHERE 1=1 ' + 
										 CASE ISNULL(@PROCESS_CONDITION, '') WHEN '' THEN '' ELSE ' AND ' + ISNULL(@PROCESS_CONDITION, '') END + '; SELECT @ROW_PROCESSED = @@ROWCOUNT'
						END
						ELSE
						BEGIN
							SET @SCHEMA_NAME = CASE WHEN ISNULL(@SCHEMA_NAME, '') = '' THEN 'dbo' ELSE @SCHEMA_NAME END

							IF(EXISTS (SELECT 'x' FROM sys.servers WHERE name = @SERVER_NAME AND server_id = 0))
							BEGIN
								SET @QUERY = 'SELECT * INTO [' + @DATABASE_NAME + '].[' + @SCHEMA_NAME + '].[' + @BACKUP_TABLE_NAME + '] FROM ' + 
											 @TABLE_NAME + ' WHERE 1=1 ' + CASE ISNULL(@PROCESS_CONDITION, '') WHEN '' THEN '' ELSE ' AND ' + ISNULL(@PROCESS_CONDITION, '') END + '; SELECT @ROW_PROCESSED = @@ROWCOUNT'
							END
							ELSE
							BEGIN
								SET @ERR_ACTION = 'EXCEPTION'
								SET @ERR_MESSAGE = 'Table ' + @BACKUP_TABLE_NAME + ' Does No Exists in [' + @SERVER_NAME + '].[' + @DATABASE_NAME + ']. Cannot Create Table on Linked Server, Please Create Table Manually.'
								RAISERROR(@ERR_MESSAGE, 16, 1)
							END
						END

						SET @st = GETDATE()
						INSERT INTO TB_H_TABLE_ARCHIEVE_PROGRESS
							(
								process_id,
								table_name,
								[action],
								start_time,
								query
							)
						SELECT
							@PROCESS_ID,
							@TABLE_NAME,
							@ERR_ACTION,
							GETDATE(),
							@QUERY

						IF(@check IS NULL)
						BEGIN
							SET @ROW_PROCESSED = 0
							EXEC sp_executesql @QUERY, N'@ROW_PROCESSED BIGINT OUTPUT', @ROW_PROCESSED OUT
						END

						SET @EXEC_TIME = CONVERT(BIGINT, RTRIM(CAST(DATEDIFF(MILLISECOND, @st, GETDATE()) AS VARCHAR(MAX))))
						UPDATE TB_H_TABLE_ARCHIEVE_PROGRESS
							SET rows_processed = @ROW_PROCESSED,
								execution_time = @EXEC_TIME,
								end_time = GETDATE(),
								[status] = 'SUCCESS'
						WHERE process_id = @PROCESS_ID
							  AND table_name = @TABLE_NAME
							  AND [action] = @ERR_ACTION
					END	

					IF((@IS_DELETE = 'Y') AND ((@IS_BACKUP = 'Y' AND @ROW_PROCESSED > 0) OR (@IS_BACKUP = 'N')))
					BEGIN
						SET @ERR_ACTION = 'DELETE'
						SET @ROW_PROCESSED = 0

						SET @QUERY = 'DELETE FROM ' + @TABLE_NAME + ' WHERE 1=1 ' + 
									 CASE ISNULL(@PROCESS_CONDITION, '') WHEN '' THEN '' ELSE ' AND ' + ISNULL(@PROCESS_CONDITION, '') END + '; SELECT @ROW_PROCESSED = @@ROWCOUNT'

						SET @st = GETDATE()
						INSERT INTO TB_H_TABLE_ARCHIEVE_PROGRESS
							(
								process_id,
								table_name,
								[action],
								start_time,
								query
							)
						SELECT
							@PROCESS_ID,
							@TABLE_NAME,
							@ERR_ACTION,
							GETDATE(),
							@QUERY

						IF(@check IS NULL)
						BEGIN
							EXEC sp_executesql @QUERY, N'@ROW_PROCESSED BIGINT OUTPUT', @ROW_PROCESSED OUT
						END

						SET @EXEC_TIME = CONVERT(BIGINT, RTRIM(CAST(DATEDIFF(MILLISECOND, @st, GETDATE()) AS VARCHAR(MAX))))
						UPDATE TB_H_TABLE_ARCHIEVE_PROGRESS
							SET rows_processed = @ROW_PROCESSED,
								execution_time = @EXEC_TIME,
								end_time = GETDATE(),
								[status] = 'SUCCESS'
						WHERE process_id = @PROCESS_ID
							  AND table_name = @TABLE_NAME
							  AND [action] = @ERR_ACTION

						UPDATE TB_M_TABLE_ARCHIVING_CONFIG
							SET now_archieving = 'DONE',
								last_process_status = 'SUCCESS',
								last_archieved = GETDATE(),
								next_archieving = NULL --DATEADD(DAY, 7, ISNULL(last_archieved, GETDATE()))
						WHERE table_name = @TABLE_NAME
					END

					IF((@IS_TRUNCATE = 'Y')) --AND ((@IS_BACKUP = 'Y' AND @ROW_PROCESSED >= 0) OR (@IS_BACKUP = 'N')))
					BEGIN
						SET @ROW_PROCESSED = 0
						SET @ERR_ACTION = 'TRUNCATE'
						SET @QUERY = 'TRUNCATE TABLE ' + @TABLE_NAME

						SET @st = GETDATE()
						INSERT INTO TB_H_TABLE_ARCHIEVE_PROGRESS
							(
								process_id,
								table_name,
								[action],
								start_time,
								query
							)
						SELECT
							@PROCESS_ID,
							@TABLE_NAME,
							@ERR_ACTION,
							GETDATE(),
							@QUERY
						
						IF(@check IS NULL)
						BEGIN
							EXEC sp_executesql @QUERY
						END

						SET @EXEC_TIME = CONVERT(BIGINT, RTRIM(CAST(DATEDIFF(MILLISECOND, @st, GETDATE()) AS VARCHAR(MAX))))
						UPDATE TB_H_TABLE_ARCHIEVE_PROGRESS
							SET rows_processed = NULL,
								execution_time = @EXEC_TIME,
								end_time = GETDATE(),
								[status] = 'SUCCESS'
						WHERE process_id = @PROCESS_ID
							  AND table_name = @TABLE_NAME
							  AND [action] = @ERR_ACTION

						UPDATE TB_M_TABLE_ARCHIVING_CONFIG
							SET now_archieving = 'DONE',
								last_process_status = 'SUCCESS',
								next_archieving = NULL, --DATEADD(DAY, 7, CONVERT(DATE, ISNULL(last_archieved, GETDATE()))),
								last_archieved = GETDATE()
						WHERE table_name = @TABLE_NAME
					END
				END
				ELSE
				BEGIN
					SET @ERR_ACTION = 'SKIPPED'
					SET @ERR_MESSAGE = 'Table ' + @TABLE_NAME + ' Skipped Because its References Not Yet Processed or Error on Process.'
					RAISERROR(@ERR_MESSAGE, 16, 1)
				END

				SET @ERR_ACTION = 'FINISHING'
				UPDATE TB_M_TABLE_ARCHIVING_CONFIG
					SET now_archieving = 'DONE',
						last_process_status = 'SUCCESS',
						last_archieved = GETDATE(),
						next_archieving = NULL --DATEADD(DAY, 7, ISNULL(last_archieved, GETDATE()))
				WHERE table_name = @TABLE_NAME

			END TRY
			BEGIN CATCH
				SET @ERR_MESSAGE = ERROR_MESSAGE()
				--SET @FINAL_STATUS = CASE WHEN @ERR_ACTION = 'SKIPPED' THEN 'WARNING' ELSE 'ERROR' END
									--ELSE ( CASE WHEN @IS_ZERO = 'Y' THEN 'SUCCESS' ELSE 'ERROR' END) END

				IF(NOT EXISTS(SELECT 'x' FROM TB_H_TABLE_ARCHIEVE_PROGRESS 
							  WHERE process_id = @PROCESS_ID 
								    AND table_name = @TABLE_NAME 
									AND [action] = @ERR_ACTION))
				BEGIN
					INSERT INTO TB_H_TABLE_ARCHIEVE_PROGRESS
						(
							process_id,
							table_name,
							[action],
							start_time,
							query,
							[error_message],
							[status]
						)
					SELECT
						@PROCESS_ID,
						@TABLE_NAME,
						@ERR_ACTION,
						GETDATE(),
						@QUERY,
						@ERR_MESSAGE,
						CASE WHEN @ERR_ACTION = 'SKIPPED' THEN 'WARNING' ELSE (CASE WHEN @IS_ZERO = 'Y' THEN 'SUCCESS' ELSE 'ERROR' END) END
				END
				ELSE
				BEGIN
					UPDATE TB_H_TABLE_ARCHIEVE_PROGRESS
						SET end_time = GETDATE(),
							query = @QUERY,
							[error_message] = @ERR_MESSAGE,
							[status] = CASE WHEN @ERR_ACTION = 'SKIPPED' THEN 'WARNING' ELSE (CASE WHEN @IS_ZERO = 'Y' THEN 'SUCCESS' ELSE 'ERROR' END) END
					WHERE process_id = @PROCESS_ID 
						  AND table_name = @TABLE_NAME 
						  AND [action] = @ERR_ACTION
				END

				UPDATE TB_M_TABLE_ARCHIVING_CONFIG
					SET now_archieving = CASE WHEN @IS_ZERO = 'Y' THEN 'DONE' ELSE 'SKIPPED' END,
						last_process_status = CASE WHEN @ERR_ACTION = 'SKIPPED' THEN 'WARNING' ELSE (CASE WHEN @IS_ZERO = 'Y' THEN 'SUCCESS' ELSE 'ERROR' END) END,
						last_archieved = GETDATE(),
						last_process_id = @PROCESS_ID
				WHERE table_name = @TABLE_NAME
			END CATCH
		END
		SET @PROCESS_SEQ = @PROCESS_SEQ + 1
	END

	UPDATE TB_M_TABLE_ARCHIVING_CONFIG
		SET now_archieving = 'NOT YET'

	DECLARE @MAIL_PROFILE VARCHAR(50) = (SELECT TOP 1 SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE')
	DECLARE @MAIL_TO VARCHAR(MAX) = 'muliya.fathullah@toyota.co.id;yanes.sui@toyota.co.id;yoni.efriyatna@toyota.co.id'
	DECLARE @MAIL_CC VARCHAR(MAX) = 'cytta.rizka@toyota.co.id;istd.app13@toyota.co.id'
	DECLARE @MAIL_BCC VARCHAR(MAX) = 'fid.reggy@toyota.co.id'
	DECLARE @MAIL_CONTENT VARCHAR(MAX) = ''
	DECLARE @MAIL_SUBJECT VARCHAR(255) = 'Database ' + DB_NAME() + ' Archiving Summary'
 
	SELECT
		@TABLES = COUNT(DISTINCT table_name),
		@SUC_BACKUP = SUM(CASE WHEN [action] = 'BACKUP' AND [status] = 'SUCCESS' THEN 1 ELSE 0 END),
		@ERR_BACKUP = SUM(CASE WHEN [action] = 'BACKUP' AND [status] = 'ERROR' THEN 1 ELSE 0 END),
		@SUC_DELETED = SUM(CASE WHEN [action] = 'DELETE' AND [status] = 'SUCCESS' THEN 1 ELSE 0 END),
	 	@ERR_DELETED = SUM(CASE WHEN [action] = 'DELETE' AND [status] = 'ERROR' THEN 1 ELSE 0 END),
		@SUC_TRUNCATED = SUM(CASE WHEN [action] = 'TRUNCATE' AND [status] = 'SUCCESS' THEN 1 ELSE 0 END),
		@ERR_TRUNCATED = SUM(CASE WHEN [action] = 'TRUNCATE' AND [status] = 'ERROR' THEN 1 ELSE 0 END),
		@SKIPPED = SUM(CASE WHEN [action] = 'SKIPPED' AND [status] = 'SKIPPED' THEN 1 ELSE 0 END),
		@EXCEPTION = SUM(CASE WHEN [action] = 'EXCEPTION' THEN 1 ELSE 0 END)
	FROM TB_H_TABLE_ARCHIEVE_PROGRESS
	WHERE process_id = @PROCESS_ID

	IF((@ERR_BACKUP > 0) OR (@ERR_DELETED > 0) OR (@ERR_TRUNCATED > 0) OR (@EXCEPTION > 0))
		SET @FINAL_STATUS = 'ERROR'
	ELSE IF (@SKIPPED > 0)
		SET @FINAL_STATUS = 'WARNING'
	ELSE
		SET @FINAL_STATUS = 'SUCCESS'

	SET @MAIL_CONTENT = '<div style="font-size:10pt"><p>Database Archieve ' + DB_NAME() + ' summary : </p>
						<br/>
						<table style="border:2px solid white">
							<tr>
								<td>Table Processed</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @TABLES) + '</td>
							</tr>
							<tr>
								<td>Success Backup</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @SUC_BACKUP) + '</td>
							</tr>
							<tr>
								<td><strong>Error Backup</strong></td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @ERR_BACKUP) + '</td>
							</tr>
							<tr>
								<td>Success Delete</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @SUC_DELETED) + '</td>
							</tr>
							<tr>
								<td><strong>Error Delete</strong></td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @ERR_DELETED) + '</td>
							</tr>
							<tr>
								<td>Success Truncate</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @SUC_TRUNCATED) + '</td>
							</tr>
							<tr>
								<td><strong>Error Truncate</strong></td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @ERR_TRUNCATED) + '</td>
							</tr>
							<tr>
								<td>Skipped</td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @SKIPPED) + '</td>
							</tr>
							<tr>
								<td><strong>Exception</strong></td>
								<td>:</td>
								<td>' + CONVERT(VARCHAR(20), @EXCEPTION) + '</td>
							</tr>
						</table>
						<br/>
						<p>With OVERALL STATUS : <span style="color:' + CASE @FINAL_STATUS WHEN 'ERROR' THEN 'red' ELSE 'black' END + 
						'"><strong>' + @FINAL_STATUS + '</strong></span></p>
						<p>Please See Detail using Process ID : <strong>' + CONVERT(VARCHAR(MAX), @PROCESS_ID) + '</strong></p>' +
						'<p>[TB_H_TABLE_ARCHIEVE_PROGRESS]</p></div>
						'
						
	EXEC msdb.dbo.sp_send_dbmail
					@from_address = '"Archive Master" <archive-master@toyota.co.id>',
					@profile_name = @MAIL_PROFILE,
					@recipients = @MAIL_TO,
					@copy_recipients = @MAIL_CC,
					@subject = @MAIL_SUBJECT, 
					@body = @MAIL_CONTENT,
					@body_format = 'HTML'; 
END

