-- =============================================
-- Author:		fid.salman
-- Create date: 09.09.2013
-- Description:	Insert Goods Receipt ICS
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_InsertPostingFileGR_rahmat] 
	-- Add the parameters for the stored procedure here
	@processId BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @sqlQuery AS VARCHAR(MAX);
	
	-- ============================================
	-- PHASE 1 : GET DATA FROM FINISHED ICS PROCESS
	-- ============================================
	
	CREATE TABLE #ICS_FINISH
	(
		PROCESS_ID VARCHAR(MAX) ,
		SYSTEM_SOURCE VARCHAR(MAX) ,
		USR_ID VARCHAR(MAX) ,
		POSTING_DT DATETIME ,
		PROCESS_DT DATETIME ,
		REF_NO VARCHAR(MAX) ,
		MOVEMENT_TYPE VARCHAR(MAX) ,
		KANBAN_ORDER_NO VARCHAR(MAX) ,
		PROD_PURPOSE_CD VARCHAR(MAX) ,
		SOURCE_TYPE VARCHAR(MAX) ,
		ORI_MAT_NO VARCHAR(MAX) ,
		RECEIVING_AREA_CD VARCHAR(MAX) ,
		ORI_SUPP_CD VARCHAR(MAX) ,
		PO_NO VARCHAR(MAX) ,
		PLANT_CD VARCHAR(MAX) ,
		SLOC_CD VARCHAR(MAX) ,
		ERR_STS VARCHAR(MAX) ,
		MAT_DOC_NO VARCHAR(MAX) ,
		MAT_DOC_YEAR VARCHAR(MAX) ,
		OTHER_PROCESS_ID VARCHAR(MAX) ,
		ERR_CD VARCHAR(MAX) ,
		ERR_DESC VARCHAR(MAX) ,
		ERR_MESSAGE VARCHAR(MAX) 
	)	
	
	SET @sqlQuery = 'INSERT INTO #ICS_FINISH SELECT * FROM OPENQUERY([IPPCS_TO_ICS] , ''select * from tb_t_finished_good_receive_kbs where other_process_id = ' + CONVERT(VARCHAR, @processID) + ''')';
	EXEC (@sqlQuery);
	
	DECLARE @icsProcessId AS BIGINT;	
	SELECT @icsProcessId = PROCESS_ID FROM #ICS_FINISH WHERE OTHER_PROCESS_ID = @processId;
			
	DECLARE 
		@process AS VARCHAR(MAX)
		,@function AS VARCHAR(MAX)
		,@movementType AS VARCHAR(MAX);
	SELECT @movementType = RIGHT(MOVEMENT_TYPE, 1) FROM #ICS_FINISH WHERE OTHER_PROCESS_ID = @processId;
	
	IF @movementType = '1'
	BEGIN
		SET @process = 'Approve';
		SET @function = '42001';
	END
	ELSE
	BEGIN
		SET @process = 'Cancel';
		SET @function = '42002';
	END
	
	EXEC sp_PutLog 
		'Get Data From Finished ICS Process'
		, 'System'
		, 'SP_ICS_InsertPostingFileGR'
		, @processId
		, 'MPCS00004INF'
		, ''
		, '4'
		, @function
		, 0
		
	INSERT INTO TB_T_LOCK
           ([FUNCTION_NO]
           ,[LOCK_REFF]
           ,[CREATED_BY]
           ,[CREATED_DT])
    VALUES
		(@function
		,@processId
		,'System'
		,GETDATE())
	
	-- ==============================================
	-- PHASE 2 : INSERT INTO TB_T_GRTP_RESULT_POSTING
	-- ==============================================
	
	EXEC sp_PutLog 
		'Insert Into TB_T_GRTP_RESULT_POSTING'
		, 'System'
		, 'SP_ICS_InsertPostingFileGR'
		, @processId
		, 'MPCS00004INF'
		, ''
		, '4'
		, @function
		, 0
	
	INSERT INTO [TB_T_GRTP_RESULT_POSTING]
           ([system_source]
           ,[usr_id]
           ,[movement_type]
           ,[process_dt]
           ,[posting_dt]
           ,[manifest_no]	-- REF_NO
           ,[kanban_order_no]
           ,[prod_purpose_cd]
           ,[source_type]
           ,[Ori_Mat_No]
           ,[ori_supp_cd]
           ,[receiving_area_cd]
           ,[plant_cd]
           ,[sloc_cd]
           ,[mat_doc_no]
           ,[mat_doc_year]
           ,[po_no]
           ,[err_sts]
           ,[err_cd]
           ,[error_desc]
           ,[err_msg]
           ,[PROCESS_ID])
	SELECT 
		SYSTEM_SOURCE
		,USR_ID
		,MOVEMENT_TYPE
		,PROCESS_DT
		,POSTING_DT
		,REF_NO
		,KANBAN_ORDER_NO
		,PROD_PURPOSE_CD
		,SOURCE_TYPE
		,ORI_MAT_NO
		,ORI_SUPP_CD
		,RECEIVING_AREA_CD
		,PLANT_CD
		,SLOC_CD
		,MAT_DOC_NO
		,MAT_DOC_YEAR
		,PO_NO
		,ERR_STS
		,ERR_CD
		,ERR_DESC
		,ERR_MESSAGE
		,OTHER_PROCESS_ID
	FROM #ICS_FINISH
		
	-- SELECT * FROM [TB_T_GRTP_RESULT_POSTING];
	
	-- ==========================================
	-- PHASE 3 : INSERT INTO TB_R_LOG_TRANSACTION
	-- ==========================================
	
	EXEC sp_PutLog 
		'Insert Into TB_R_LOG_TRANSACTION'
		, 'System'
		, 'SP_ICS_InsertPostingFileGR'
		, @processId
		, 'MPCS00004INF'
		, ''
		, '4'
		, @function
		, 0
	
	DECLARE 
		@manifestNo AS VARCHAR(MAX)
		,@usrId AS VARCHAR(MAX)
		,@errSts AS VARCHAR(MAX)
		
		,@newRowId AS INT	
		,@rowIndex AS INT
		,@rowCount AS INT;
	
	SELECT
		ROW_NUMBER( ) OVER ( ORDER BY [MANIFEST_NO] ) AS ROW_NUM, * 
	INTO #TEMP_GRTP_RESULT_POSTING
	FROM TB_T_GRTP_RESULT_POSTING
	WHERE 
		PROCESS_ID = @processId
	ORDER BY [MANIFEST_NO] ASC;
	
	--SELECT * FROM #TEMP_GRTP_RESULT_POSTING;
	
	SELECT @rowCount = COUNT(*) FROM #TEMP_GRTP_RESULT_POSTING;
	SELECT @rowIndex = 1;
	WHILE @rowIndex <= @rowCount
	BEGIN
		SELECT 
			@manifestNo = [manifest_no]
			,@usrID = [usr_id] 
			,@errSts = [err_sts]
		FROM #TEMP_GRTP_RESULT_POSTING 
		WHERE ROW_NUM = @rowIndex;
		
		IF @errSts = '1'
		BEGIN
			IF NOT EXISTS(
				SELECT *
				FROM TB_R_LOG_TRANSACTION_H
				WHERE
					PROCESS_ID = @processId
					AND MANIFEST_NO = @manifestNo
			)
			BEGIN
				-- INSERT LOG HEADER TABLE
				INSERT INTO TB_R_LOG_TRANSACTION_H
							([PROCESS_ID]
							,[PROCESS_ID_ICS]
							,[MANIFEST_NO]
							,[CREATED_BY]
							,[CREATED_DATE]
							,[CHANGED_BY]
							,[CHANGED_DATE])
				VALUES (
					@processId,
					@icsProcessId,
					@manifestNo,
					@usrId,
					GETDATE(),
					NULL,
					NULL);
								
				SET @newRowId = SCOPE_IDENTITY();
			END
			ELSE
			BEGIN
				SELECT @newRowId = LOG_ID_H
				FROM TB_R_LOG_TRANSACTION_H
				WHERE
					PROCESS_ID = @processID
					AND MANIFEST_NO = @manifestNo;
			END
	
			-- INSERT LOG DETAIL TABLE
			INSERT INTO TB_R_LOG_TRANSACTION_D
						([LOG_ID_H]
						,[SEQ_NO]
						,[SYSTEM_SOURCE]
						,[USR_ID]
						,[MOVEMENT_TYPE]
						,[PROCESS_DT]
						,[POSTING_DT]
						,[MANIFEST_NO]
						,[KANBAN_ORER_NO]
						,[PROD_PURPOSE_CD]
						,[SOURCE_TYPE]
						,[PART_NO]
						,[SUPP_CD]
						,[DOCK_CD]
						,[PLANT_CD]
						,[SLOC_CD]
						,[LOCATION]
						,[ERR_CD]
						,[ERR_MESSAGE]
						,[CREATED_BY]
						,[CREATED_DT]
						,[CHANGED_BY]
						,[CHANGED_DT])
			SELECT
				@newRowId
				,@rowIndex
				,[system_source]
				,[usr_id]
				,[movement_type]
				,[process_dt]
				,[posting_dt]
				,[manifest_no]	-- REF_NO
				,[kanban_order_no]
				,[prod_purpose_cd]
				,[source_type]				
				,[Ori_Mat_No]
				,[ori_supp_cd]
				,[receiving_area_cd]
				,[plant_cd]
				,[sloc_cd]
				,''
				,[err_cd]
				,[err_msg]
				,@usrId
				,GETDATE()
				,NULL
				,NULL
			FROM #TEMP_GRTP_RESULT_POSTING
			WHERE
				ROW_NUM = @rowIndex;
		END
	
		SET @rowIndex = @rowIndex + 1;
	END

	--SELECT * FROM TB_R_LOG_TRANSACTION_H WHERE PROCESS_ID = @processId;

	-- ========================================================
	-- PHASE 4 : UPDATE GOODS RECEIPT INQUIRY HEADER AND DETAIL
	-- ========================================================
	
	EXEC sp_PutLog 
		'Update Goods Receipt Inquiry Header and Detail'
		, 'System'
		, 'SP_ICS_InsertPostingFileGR'
		, @processId
		, 'MPCS00004INF'
		, ''
		, '4'
		, @function
		, 0
	
	DECLARE 
		@poNo AS VARCHAR(MAX)
		,@createdDt AS DATETIME = GETDATE();
	
	SELECT 
		ROW_NUMBER( ) OVER ( ORDER BY [MANIFEST_NO] ) AS ROW_NUM
		,manifest_no
		,po_no 
		,usr_id
	INTO #TEMP_GROUP_GRTP_RESULT_POSTING
	FROM TB_T_GRTP_RESULT_POSTING 
	WHERE process_id = @processId 
	GROUP BY manifest_no, po_no, usr_id;
	
	SELECT @rowCount = COUNT(*) FROM #TEMP_GROUP_GRTP_RESULT_POSTING;
	SELECT @rowIndex = 1;
	WHILE @rowIndex <= @rowCount
	BEGIN
		SELECT 
			@manifestNo = manifest_no
			,@poNo = po_no 
			,@usrId = usr_id
		FROM #TEMP_GROUP_GRTP_RESULT_POSTING 
		WHERE ROW_NUM = @rowIndex;
		
		EXEC SP_UpdateGoodReceipt @manifestNo, @processId, @icsProcessId, @usrId, @process, @poNo;
				
		SET @rowIndex = @rowIndex + 1;
	END
	
	EXEC SP_UpdateGoodReceiptDetail @processId, @usrId, @createdDt;

	-- ======================================
	-- PHASE 5 : DELETE LOG TRANSACTION TABLE
	-- ======================================

	EXEC sp_PutLog 
		'Delete Log Transaction Table'
		, 'System'
		, 'SP_ICS_InsertPostingFileGR'
		, @processId
		, 'MPCS00004INF'
		, ''
		, '4'
		, @function
		, 0

	SELECT @rowCount = COUNT(*) FROM #TEMP_GROUP_GRTP_RESULT_POSTING;
	SELECT @rowIndex = 1;
	WHILE @rowIndex <= @rowCount
	BEGIN
		SELECT 
			@manifestNo = manifest_no
		FROM #TEMP_GROUP_GRTP_RESULT_POSTING 
		WHERE ROW_NUM = @rowIndex;
		
		BEGIN TRY

			BEGIN TRANSACTION DELETE_LOG_TABLE_DETAIL;

			DELETE FROM TB_R_LOG_TRANSACTION_D 
			WHERE 
				LOG_ID_H IN (
					SELECT LOG_ID_H
					FROM TB_R_LOG_TRANSACTION_H
					WHERE MANIFEST_NO = @manifestNo AND PROCESS_ID <> @processId
					)

			COMMIT TRANSACTION DELETE_LOG_TABLE_DETAIL;
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION DELETE_LOG_TABLE_DETAIL;
		END CATCH
		
		BEGIN TRY

			BEGIN TRANSACTION DELETE_LOG_TABLE_HEADER;

			DELETE FROM TB_R_LOG_TRANSACTION_H WHERE MANIFEST_NO = @manifestNo AND PROCESS_ID <> @processId;

			COMMIT TRANSACTION DELETE_LOG_TABLE_HEADER;
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION DELETE_LOG_TABLE_HEADER;
		END CATCH
		
		BEGIN TRY

			BEGIN TRANSACTION DELETE_GRTP_RESULT_POSTING;

			DELETE FROM TB_T_GRTP_RESULT_POSTING WHERE MANIFEST_NO = @manifestNo AND PROCESS_ID = @processId;

			COMMIT TRANSACTION DELETE_GRTP_RESULT_POSTING;
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION DELETE_GRTP_RESULT_POSTING;
		END CATCH
		
		SET @rowIndex = @rowIndex + 1;
	END	

	DELETE FROM [TB_T_LOCK]
	WHERE
		[FUNCTION_NO] = @function
        AND [LOCK_REFF] = @processId;

	UPDATE TB_R_ICS_QUEUE
	SET
		PROCESS_STATUS = 2
	WHERE
		IPPCS_MODUL = '4'
		AND IPPCS_FUNCTION = @function
		AND PROCESS_ID = @processId
		AND PROCESS_STATUS = '1'
		
		
	

	-- ============================================
	--             CLEAR TEMPORARY TABLE
	-- ============================================
	
	IF OBJECT_ID('tempdb..#TEMP_GROUP_GRTP_RESULT_POSTING') IS NOT NULL 
		DROP TABLE #TEMP_GROUP_GRTP_RESULT_POSTING
	IF OBJECT_ID('tempdb..#TEMP_GRTP_RESULT_POSTING') IS NOT NULL 
		DROP TABLE #TEMP_GRTP_RESULT_POSTING
	IF OBJECT_ID('tempdb..#ICS_FINISH') IS NOT NULL 
		DROP TABLE #ICS_FINISH
END

