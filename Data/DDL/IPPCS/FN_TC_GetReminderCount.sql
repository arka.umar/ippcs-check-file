CREATE FUNCTION [dbo].[FN_TC_GetReminderCount] (
	@ri_n_remain bigint -- in seconds
)
RETURNS smallint
AS
BEGIN
	if @ri_n_remain > (60 * 10) -- >10 minutes
	begin
		return null
	end
	else if @ri_n_remain > (60 * 5) -- >5 minutes
	begin
		return 1
	end
	else if @ri_n_remain > 0 -- >0 minutes
	begin
		return 2
	end
	--else return 3

	return 3
END

