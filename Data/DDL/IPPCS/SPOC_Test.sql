-- =============================================
-- Author:		FID.Deny
-- Create date: 2015-09-10
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SPOC_Test] 
	@dateFrom varchar(15),
	@dateTo varchar(15),
	@type varchar(15)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @i_dateFrom varchar(15) = @datefrom,
			@i_dateTo varchar(15) = @dateto,
			@i_type varchar(15) = @type

		SELECT k.PRODUCTION_DATE, k.ARRIVAL_SEQ, i.DOCK_CD, i.SUPPLIER_CD + '-' + i.SUPPLIER_PLANT AS SUPPLIER, i.PART_NO, i.IPPCS_ORDER_QTY, k.ORDER_QTY_KCI				
		FROM (                                					
				select a.ORDER_NO, 			
					a.DOCK_CD, 		
					a.SUPPLIER_CD,		
					a.SUPPLIER_PLANT,		
					b.PART_NO, 				
					SUM(b.ORDER_QTY) IPPCS_ORDER_QTY                             		
				from IPPCS_QA.dbo.TB_R_DAILY_ORDER_MANIFEST a, 			
					 IPPCS_QA.dbo.TB_R_DAILY_ORDER_PART b                                		
				where 
					CAST(LEFT(a.ORDER_NO, 8) AS DATE) BETWEEN @i_dateFrom AND @i_dateTo	
				AND a.MANIFEST_NO = b.MANIFEST_NO                                			
				GROUP BY a.ORDER_NO, a.DOCK_CD, a.SUPPLIER_CD,a.SUPPLIER_PLANT, b.PART_NO, a.PROCESS_FLAG,a.CREATED_DT			
			 ) i                                				
		INNER JOIN (					
				SELECT PRODUCTION_DATE, ARRIVAL_SEQ, SUPP_CD, 			
					SUPP_PLANT, 		
					PART_NO, 		
					DOCK_CD, 		
					LEFT(CONVERT(VARCHAR, PRODUCTION_DATE, 112) +                                		
						CASE WHEN LEN(CONVERT(VARCHAR, ARRIVAL_SEQ)) = 1 THEN    	
							'0'+ CONVERT(VARCHAR, ARRIVAL_SEQ)
							ELSE CONVERT(VARCHAR, ARRIVAL_SEQ) END, 12) ORDER_NO, 
					SUM(ORDER_QTY) ORDER_QTY_KCI		
				FROM KCI_DB.dbo.TB_R_ORDER_MOD                                 			
				WHERE PRODUCTION_DATE BETWEEN @i_dateFrom AND @i_dateTo                                			
				GROUP BY SUPP_CD, SUPP_PLANT, PART_NO, DOCK_CD, PRODUCTION_DATE, ARRIVAL_SEQ			
			) k                                 				
		ON k.SUPP_CD = i.SUPPLIER_CD 					
		AND k.SUPP_PLANT = i.SUPPLIER_PLANT                                					
		AND k.PART_NO = i.PART_NO 					
		AND k.DOCK_CD = i.DOCK_CD 					
		AND k.ORDER_NO = i.ORDER_NO
		

		AND ((k.ORDER_QTY_KCI <> i.IPPCS_ORDER_QTY AND @i_type = 'difference') OR
			 (k.ORDER_QTY_KCI = i.IPPCS_ORDER_QTY AND @i_type = 'match') OR
			 (@i_type = 'all')
			)

		ORDER BY K.PRODUCTION_DATE, K.ARRIVAL_SEQ
		



END

