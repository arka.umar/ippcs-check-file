
/****** Object:  StoredProcedure [dbo].[SP_ICS_PostingGRtoICS]    Script Date: 04/05/2021 10:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           niit.yudha
-- Create date: 3-Mei-2013
-- Description:      Generate Goods Receipt ICS
-- Update	: FID.Ridwan : 10-Sep-2020
--			  => Send to new ICS (GR Only)
--			  => Add price validation
--			  => Add register to manifest detail
-- =============================================
ALTER PROCEDURE [dbo].[SP_ICS_PostingGRtoICS]
	@UserID VARCHAR(20)
	,@ManifestNo VARCHAR(MAX)
	,@SupplierCode VARCHAR(MAX)
	,@RvcPlantCode VARCHAR(MAX)
	,@DockCode VARCHAR(MAX)
	,@L_N_PROCESS_ID BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PROC_ID VARCHAR(12)
		,@PROCESS_ID BIGINT
		,@LAST_PROCESS_ID VARCHAR(12)
		,@TODAY VARCHAR(8)
		,@NOW DATETIME
		,@CURR_NO INT
		,@CURR_PROCESS_ID VARCHAR(12)
		,@SEQ_NO_LOG BIGINT
		,@LOCATION VARCHAR(100)
		,@PROC_STAT TINYINT
		,@L_N_COUNT_MANIFEST INT
		,@L_N_COUNT_ERR INT
		,@ERR_FLAG BIT

		DECLARE @Iserror CHAR(1) = 'N';
			DECLARE @Param VARCHAR(MAX)
				,@MSG_TXT AS VARCHAR(MAX)
				,@MSG_TYPE AS VARCHAR(MAX)
				,@N_ERR AS INT = 0
				,@LOG_LOCATION AS VARCHAR(MAX) = 'SP Posting GR to ICS'

			
		DECLARE @cnerror INT = 0, @cnloop INT = 1, @Man VARCHAR(30), @Mat VARCHAR(50);

		DECLARE @TEMP_ERR_ICS TABLE (ROW_NO INT, MANIFEST_NO VARCHAR(50), MAT_NO VARCHAR(50))


	SET @LOCATION = 'SP ICS GeneratePosting GR';
	SET @SEQ_NO_LOG = 1;
	SET @ERR_FLAG = 0;
	SET @PROC_STAT = 0;
	SET NOCOUNT ON

	DECLARE @TP_SUPPLIER_CODE VARCHAR(50)
		,@FLTR_DATETIME_VALUE AS VARCHAR(50)
		,@FLTR_LOGICAL_VALUE AS VARCHAR(50)
		,@FLTR_FLAG_VALUE AS VARCHAR(50)
		,@IS_VALID_FILTER CHAR(1)
		,@TYPE_CODE INT
		,@RECEIVE_NO VARCHAR(10)
		,@SOURCE_TYPE_GR CHAR(1)
		,@SOURCE_TYPE_TP CHAR(1)
		,@SYS_SOURCE CHAR(20)
		,@PROD_PURPOSE_CD CHAR(5)
		,@URI_UOM CHAR(5)
		,@SEND_FLAG VARCHAR(1)
	DECLARE @dockServicePart VARCHAR(20) = ''
		,@prodPurposeServicePart VARCHAR(20) = ''
		
	BEGIN TRY
	SET @Param = 'System Master Checking'

	EXEC dbo.CommonGetMessage 'MPCS00002INF'
		,@MSG_TXT OUTPUT
		,@N_ERR OUTPUT
		,@Param

	INSERT INTO TB_R_LOG_D (
		PROCESS_ID
		,SEQUENCE_NUMBER
		,MESSAGE_ID
		,MESSAGE_TYPE
		,[MESSAGE]
		,LOCATION
		,CREATED_BY
		,CREATED_DATE
		)
	SELECT @L_N_PROCESS_ID
		,(
			SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
			FROM dbo.TB_R_LOG_D
			WHERE PROCESS_ID = @L_N_PROCESS_ID
			)
		,'MPCS00002INF'
		,'INF'
		,'MPCS00002INF : ' + @MSG_TXT
		,@LOG_LOCATION
		,@UserID
		,GETDATE()

	SELECT @dockServicePart = SYSTEM_VALUE
	FROM TB_M_SYSTEM
	WHERE SYSTEM_CD = 'DOCK_SERVICE_PART'

	SELECT @prodPurposeServicePart = SYSTEM_VALUE
	FROM TB_M_SYSTEM
	WHERE SYSTEM_CD = 'PROD_PURPOSE_GR_SERVICE_PART'

	DECLARE @dockServicePartTable AS TABLE (DOCK VARCHAR(2))

	INSERT INTO @dockServicePartTable (DOCK)
	EXEC dbo.SplitString @dockServicePart
		,','

	SELECT @TYPE_CODE = 42001

	--GET VALUE FROM SYS TABLE                                                                                                        
	SELECT @TP_SUPPLIER_CODE = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'TP_SPLR_CODE') + '%'

	SELECT @RECEIVE_NO = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'RECEIVE_NO_GR')

	SELECT @SOURCE_TYPE_GR = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'SOURCE_TYPE_GR')

	SELECT @SOURCE_TYPE_TP = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'SOURCE_TYPE_TP')

	SELECT @SYS_SOURCE = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'SYSTEM_SOURCE_GR')

	SELECT @PROD_PURPOSE_CD = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'PROD_PURPOSE_CD_GR')

	SELECT @URI_UOM = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'ORI_UNIT_MEASU_CD_GR')

	SELECT @SEND_FLAG = dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'SEND_FLAG')

	--GET FILTER PARAMETERS                                                                                                           
	SELECT @FLTR_DATETIME_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'POST_TIME_FLTR')))

	SELECT @FLTR_LOGICAL_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'POST_LGCL_FLTR')))

	SELECT @FLTR_FLAG_VALUE = LTRIM(RTRIM(dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'POST_FLG_FLTR')))

	-- END OF GET FILTER PARAMETERS                                                                                                          
	-- CHECK FILTER VALIDITY                                                                                                          
	IF (@FLTR_FLAG_VALUE = 'Y')
	BEGIN
		IF (
				@FLTR_LOGICAL_VALUE IN (
					'='
					,'>'
					,'<'
					,'>='
					,'<='
					)
				)
			AND (LEN(@FLTR_DATETIME_VALUE) = 14) -- AND ( ISDATE(CHAR14TODATE(@FLTR_DATETIME_VALUE)) = 1 )
		BEGIN
			SET @IS_VALID_FILTER = 'Y'
		END
		ELSE
		BEGIN
			SET @IS_VALID_FILTER = 'N'
		END
	END
	ELSE
	BEGIN
		IF (@FLTR_FLAG_VALUE = 'N')
			SET @IS_VALID_FILTER = 'Y'
		ELSE
			SET @IS_VALID_FILTER = 'N'
	END

	-- END OF CHECK FILTER VALIDITY                                                                                                          
	CREATE TABLE #TempManifestNo (ManifestNo VARCHAR(100))

	CREATE TABLE #tempSupplierCode (SupplierCode VARCHAR(100))

	CREATE TABLE #tempRvcPlantCode (RvcPlantCode VARCHAR(100))

	CREATE TABLE #tempDockCode (DockCode VARCHAR(100))

	IF (
			SELECT CHARINDEX(';', @ManifestNo)
			) > 0
	BEGIN
		INSERT INTO #TempManifestNo (ManifestNo)
		EXEC dbo.SplitString @ManifestNo
			,';'
	END
	ELSE
	BEGIN
		INSERT INTO #TempManifestNo (ManifestNo)
		SELECT @ManifestNo
	END

	IF (
			SELECT CHARINDEX(';', @SupplierCode)
			) > 0
	BEGIN
		INSERT INTO #tempSupplierCode (SupplierCode)
		EXEC dbo.SplitString @SupplierCode
			,';'
	END
	ELSE
	BEGIN
		INSERT INTO #tempSupplierCode (SupplierCode)
		SELECT @SupplierCode
	END

	IF (
			SELECT CHARINDEX(';', @RvcPlantCode)
			) > 0
	BEGIN
		INSERT INTO #tempRvcPlantCode (RvcPlantCode)
		EXEC dbo.SplitString @RvcPlantCode
			,';'
	END
	ELSE
	BEGIN
		INSERT INTO #tempRvcPlantCode (RvcPlantCode)
		SELECT @RvcPlantCode
	END

	IF (
			SELECT CHARINDEX(';', @DockCode)
			) > 0
	BEGIN
		INSERT INTO #tempDockCode (DockCode)
		EXEC dbo.SplitString @DockCode
			,';'
	END
	ELSE
	BEGIN
		INSERT INTO #tempDockCode (DockCode)
		SELECT @DockCode
	END

	-- Added by imans, 07-08-2013
	---> EXECUTE SP UPDATE SHIPPING DOCK STAMPING & CASTING
	EXEC [dbo].[SP_UPDATE_SHIPPING_DOCK] @MANIFEST = @ManifestNo

	---
	IF (@IS_VALID_FILTER = 'Y')
	BEGIN
		CREATE TABLE #INVOICENO_GR (INVOICENO VARCHAR(25))

		CREATE TABLE #ICS_POSTING (
			REC_ID INT IDENTITY(1, 1)
			,MANIFEST_NO VARCHAR(25)
			,ITEM_NO INT
			,SUPPLIER_CODE VARCHAR(4)
			,SUPPLIER_PLANT CHAR(1)
			,SHIPPING_DOCK VARCHAR(3)
			,COMPANY_CODE VARCHAR(4)
			,RCV_PLANT_CODE CHAR(1)
			,DOCK_CODE VARCHAR(2)
			,ORDER_NO VARCHAR(12)
			,CAR_FAMILY_CODE VARCHAR(4)
			,RE_EXPORT_CODE VARCHAR(1)
			,PART_NO VARCHAR(12)
			,PART_NAME VARCHAR(100)
			,KNBN_PRN_ADDRESS VARCHAR(10)
			,ORDER_TYPE CHAR(1)
			,MOV_TYPE CHAR(3)
			,DROP_STATUS_DATE DATETIME
			,RECEIVED_QTY INT NULL
			,DN_COMPLETE_FLAG CHAR(1)
			,DATA_TYPE CHAR(2)
			,ORDER_RELEASE_DT DATETIME
			)

		/* ===========================================               
                                  CREATE DATA LIST FROM ORDER                    
                              ===========================================*/
		BEGIN
			--GR                                                                                     
			INSERT INTO #INVOICENO_GR
			SELECT A.MANIFEST_NO
			FROM (
				SELECT DISTINCT (c.MANIFEST_NO)
					,MAX(b.ORDER_QTY) AS ORDER_QTY
					,c.RCV_PLANT_CD
					,c.DOCK_CD
					,c.ICS_FLAG
					,c.DROP_STATUS_FLAG
					,c.DROP_STATUS_DT
					,c.fst_x_plant_cd AS FST_X_PLANT_CODE
					,c.fst_x_shipping_dock_cd AS FST_X_SHIPPING_DOCK_CODE
					,c.SUPPLIER_CD AS SUPPLIER_CODE
				FROM TB_R_DAILY_ORDER_MANIFEST c
				INNER JOIN (
					SELECT DISTINCT (MANIFEST_NO)
						,MAX(ORDER_QTY) AS ORDER_QTY
					FROM TB_R_DAILY_ORDER_PART
					WHERE MANIFEST_NO IN (
							SELECT *
							FROM #TempManifestNo
							)
					GROUP BY (MANIFEST_NO)
					) AS b ON c.MANIFEST_NO = b.MANIFEST_NO
				--
				WHERE c.PROBLEM_FLAG = '0'
					AND c.CANCEL_FLAG = '0'
					AND c.MANIFEST_RECEIVE_FLAG IN (
						'2'
						,'3'
						,'5'
						)
					AND c.MANIFEST_NO IN (
						SELECT *
						FROM #TempManifestNo
						)
					AND c.SUPPLIER_CD NOT LIKE @TP_SUPPLIER_CODE
				--            
				GROUP BY c.MANIFEST_NO
					,c.RCV_PLANT_CD
					,c.DOCK_CD
					,c.ICS_FLAG
					,c.DROP_STATUS_FLAG
					,c.DROP_STATUS_DT
					,c.fst_x_plant_cd
					,c.fst_x_shipping_dock_cd
					,c.SUPPLIER_CD
				) AS A
			WHERE ISNULL(A.ICS_FLAG, '0') LIKE '0'
				AND A.ORDER_QTY > 0
			GROUP BY A.MANIFEST_NO
			HAVING MIN(A.DROP_STATUS_FLAG) = 1
		END

		--GR                                                                                            
		INSERT INTO #ICS_POSTING (
			MANIFEST_NO
			,ITEM_NO
			,SUPPLIER_CODE
			,SUPPLIER_PLANT
			,SHIPPING_DOCK
			,COMPANY_CODE
			,RCV_PLANT_CODE
			,DOCK_CODE
			,ORDER_NO
			,CAR_FAMILY_CODE
			,RE_EXPORT_CODE
			,PART_NO
			,PART_NAME
			,KNBN_PRN_ADDRESS
			,ORDER_TYPE
			,MOV_TYPE
			,DROP_STATUS_DATE
			,RECEIVED_QTY
			,DN_COMPLETE_FLAG
			,DATA_TYPE
			,ORDER_RELEASE_DT
			)
		SELECT A.MANIFEST_NO
			,A.ITEM_NO
			,A.SUPPLIER_CODE
			,A.SUPPLIER_PLANT
			,A.SHIPPING_DOCK
			,A.COMPANY_CD
			,A.RCV_PLANT_CODE
			,A.DOCK_CODE
			,A.ORDER_NO
			,A.CAR_FAMILY_CD
			,A.RE_EXPORT_CD
			,A.PART_NO
			,MAX(A.PART_NAME) PART_NAME
			,'' AS KNBN_PRN_ADDRESS
			,-- was : A.KNBN_PRN_ADDRESS , not send to ics
			A.ORDER_TYPE
			,A.MOVE_TYPE
			,A.DROP_STATUS_DT
			,SUM(A.ORDER_QTY) ORDER_QTY
			,A.DN_COMPLETE_FLAG
			,A.DATA_TYPE
			,A.ORDER_RELEASE_DT
		FROM (
			SELECT DISTINCT (c.MANIFEST_NO)
				,c.SUPPLIER_CD AS SUPPLIER_CODE
				,c.SUPPLIER_PLANT
				,c.SHIPPING_DOCK
				,c.RCV_PLANT_CD AS RCV_PLANT_CODE
				,c.DOCK_CD AS DOCK_CODE
				,c.ICS_FLAG
				,C.COMPANY_CD
				,c.CAR_FAMILY_CD
				,c.RE_EXPORT_CD
				,SUM(b.ORDER_QTY) ORDER_QTY
				,c.ORDER_NO
				,c.DROP_STATUS_FLAG
				,c.DROP_STATUS_DT
				,b.PART_NO
				,B.ITEM_NO
				,MAX(b.PART_NAME) PART_NAME
				,'' AS KNBN_PRN_ADDRESS
				,--was : b.KANBAN_PRINT_ADDRESS AS KNBN_PRN_ADDRESS ,
				c.ORDER_TYPE
				,
				/*CASE LEFT(c.SUPPLIER_CD, 3)
                                          WHEN '807' THEN '301'
                                          ELSE '101'
                                        END AS MOVE_TYPE ,*/
				CASE LEFT(c.SUPPLIER_CD, 3)
					WHEN '807'
						THEN CASE 
								WHEN (
										SELECT 1
										WHERE c.DOCK_CD IN (
												SELECT DOCK
												FROM @dockServicePartTable
												)
										) = 1
									THEN (
											SELECT SYSTEM_VALUE
											FROM TB_M_SYSTEM
											WHERE SYSTEM_CD = 'MOVEMENT_TYPE_SERVICE_PART'
											)
								ELSE '301'
								END
					ELSE '101'
					END AS MOVE_TYPE
				,dbo.GET_VALUE_FROM_M_SYSTEM(@TYPE_CODE, 'DN_COMPLETE_FLG_GR') AS DN_COMPLETE_FLAG
				,'GR' AS DATA_TYPE
				,
				/*
                                        Modified by imans, 14-11-2013
                                        -- Requested by pa Agung, Vandy : 
                                           email Different Posting Date from Scan Date --- CONFIRMED
                                        -- was : c.ORDER_RELEASE_DT
                                        */
				-- FID.Ridwan: 20210401 -> source from order release as request ISTD.Yanes
				--c.ARRIVAL_PLAN_DT AS ORDER_RELEASE_DT
				c.ORDER_RELEASE_DT AS ORDER_RELEASE_DT
			FROM TB_R_DAILY_ORDER_MANIFEST c
			INNER JOIN (
				SELECT MANIFEST_NO
					,SUM(ORDER_QTY) ORDER_QTY
					,RCV_PLANT_CD
					,PART_NO
					,ITEM_NO
					,MAX(PART_NAME) PART_NAME
					,KANBAN_PRINT_ADDRESS
				FROM TB_R_DAILY_ORDER_PART
				WHERE ORDER_QTY > 0
					AND MANIFEST_NO IN (
						SELECT INVOICENO
						FROM #INVOICENO_GR
						)
				GROUP BY (MANIFEST_NO)
					,PART_NO
					,RCV_PLANT_CD
					,PART_NO
					,ITEM_NO
					,KANBAN_PRINT_ADDRESS
				) AS b ON c.MANIFEST_NO = b.MANIFEST_NO
			WHERE c.PROBLEM_FLAG = '0'
				AND c.CANCEL_FLAG = '0'
				AND c.MANIFEST_RECEIVE_FLAG IN (
					'2'
					,'3'
					,'5'
					)
				AND c.MANIFEST_NO IN (
					SELECT INVOICENO
					FROM #INVOICENO_GR
					)
			GROUP BY c.MANIFEST_NO
				,SUPPLIER_CD
				,SUPPLIER_PLANT
				,c.SHIPPING_DOCK
				,C.COMPANY_CD
				,c.CAR_FAMILY_CD
				,c.RE_EXPORT_CD
				,c.RCV_PLANT_CD
				,DOCK_CD
				,ORDER_NO
				,c.DROP_STATUS_FLAG
				,c.DROP_STATUS_DT
				,PART_NO
				,ITEM_NO
				,
				-- was : b.KANBAN_PRINT_ADDRESS ,
				ORDER_TYPE
				,c.ICS_FLAG
				,c.ORDER_RELEASE_DT
			) AS A
		WHERE A.MANIFEST_NO IN (
				SELECT INVOICENO
				FROM #INVOICENO_GR
				)
		GROUP BY A.MANIFEST_NO
			,A.ITEM_NO
			,A.SUPPLIER_CODE
			,A.SUPPLIER_PLANT
			,A.SHIPPING_DOCK
			,A.COMPANY_CD
			,A.RCV_PLANT_CODE
			,A.DOCK_CODE
			,A.ORDER_NO
			,A.CAR_FAMILY_CD
			,A.RE_EXPORT_CD
			,A.PART_NO
			,
			-- was : A.KNBN_PRN_ADDRESS ,
			A.ORDER_TYPE
			,A.MOVE_TYPE
			,A.DROP_STATUS_DT
			,A.DN_COMPLETE_FLAG
			,A.DATA_TYPE
			,A.ORDER_RELEASE_DT
		HAVING MIN(A.DROP_STATUS_FLAG) = 1

		UPDATE #ICS_POSTING
		SET DROP_STATUS_DATE = X.DROP_STATUS_DATE
		FROM #ICS_POSTING
			,(
				SELECT MANIFEST_NO
					,MAX(DROP_STATUS_DATE) AS DROP_STATUS_DATE
				FROM #ICS_POSTING
				GROUP BY MANIFEST_NO
				) X
		WHERE #ICS_POSTING.MANIFEST_NO = X.MANIFEST_NO

		CREATE TABLE #ICS_FILE (
			REC_ID BIGINT IDENTITY(1, 1)
			,--1
			SYS_SOURCE CHAR(20)
			,--2
			[USER_ID] CHAR(20)
			,--3
			MOV_TYPE CHAR(4)
			,--4
			PROCESS_DATE DATETIME
			,--5
			POSTING_DATE DATETIME
			,--6
			REF_NO CHAR(16)
			,--7
			KANBAN_ORDER_NO CHAR(16)
			,--8
			PROD_PURPOSE_CD CHAR(5)
			,--9
			SOURCE_TYPE CHAR(1)
			,--10
			ORI_MAT_NO CHAR(30)
			,PART_NAME VARCHAR(100)
			,ITEM_NO INT
			,--11
			POSTING_QUANTITY CHAR(10)
			,--12
			ORI_UNIT_OF_MEASURE_CD CHAR(5)
			,--13
			ORI_SUPP_CD CHAR(6)
			,--14
			RECEIVING_PLANT_CD CHAR(4)
			,--15
			RECEIVING_AREA_CD CHAR(6)
			,--16
			INT_KANBAN_FLAG CHAR(1)
			,--17
			DN_COMPLETE_FLAG CHAR(1)
			,--18
			MAT_COMPLETE_FLAG CHAR(1)
			,--19
			RECEIVE_NO CHAR(10)
			,--20
			SEND_FLAG CHAR(1)
			,--21
			PROCESS_ID BIGINT
			,--22
			ARRIVAL_PLAN_DT DATETIME,
			POST_STS INT,
			IS_COMPLETE INT,
			IS_TD INT
			)

		INSERT INTO #ICS_FILE
		SELECT @SYS_SOURCE AS systemSource
			,@UserID AS userID
			,A.MOV_TYPE AS movementType
			,GETDATE() AS processDate
			,CONVERT(DATE, CONVERT(VARCHAR(20), A.DROP_STATUS_DATE, 112)) AS postingDate
			,A.MANIFEST_NO AS refNo
			,A.ORDER_NO AS kanbanOrderNo
			,
			--@PROD_PURPOSE_CD AS prodPurpose ,
			/*CASE LEFT(A.SUPPLIER_CODE, 3)
                      WHEN '807' THEN @PROD_PURPOSE_CD
                      ELSE
                                         CASE (A.DOCK_CODE)  
                                                WHEN @dockServicePart THEN @prodPurposeServicePart
                                                ELSE
                                                       @PROD_PURPOSE_CD
                                                END
                                    END AS prodPurpose,*/
			/*CASE LEFT(A.SUPPLIER_CODE, 3)
                                         WHEN '807' THEN @PROD_PURPOSE_CD
                                  ELSE
                                         CASE WHEN (SELECT 1 WHERE A.DOCK_CODE IN (SELECT DOCK FROM @dockServicePartTable)) = 1 
                                  THEN @prodPurposeServicePart
                                  ELSE
                                         @PROD_PURPOSE_CD
                                  END
                                  END AS prodPurpose, */
			CASE 
				WHEN (
						SELECT 1
						WHERE A.DOCK_CODE IN (
								SELECT DOCK
								FROM @dockServicePartTable
								)
						) = 1
					THEN @prodPurposeServicePart
				ELSE @PROD_PURPOSE_CD
				END AS prodPurpose
			,CASE LEFT(A.SUPPLIER_CODE, 3)
				WHEN '807'
					THEN @SOURCE_TYPE_TP
				ELSE @SOURCE_TYPE_GR
				END AS sourceType
			,A.PART_NO AS oriMatNo
			,A.PART_NAME
			,A.ITEM_NO
			,RECEIVED_QTY AS postingQty
			,@URI_UOM AS oriUnitOfMeasure
			,CASE LEFT(A.SUPPLIER_CODE, 3)
				WHEN '807'
					THEN A.SHIPPING_DOCK
				ELSE A.SUPPLIER_CODE
				END AS oriSuppCode
			,A.RCV_PLANT_CODE AS receivingPlantCode
			,A.DOCK_CODE AS receivingPlantArea
			,(
				CASE 
					WHEN (
							SELECT COUNT(1)
							FROM TB_M_PART_INFO x
							WHERE x.SUPPLIER_CD = A.SUPPLIER_CODE
								AND x.SUPPLIER_PLANT = A.SUPPLIER_PLANT
								AND x.DOCK_CD = A.DOCK_CODE
								AND x.PART_NO = A.PART_NO
								AND A.DROP_STATUS_DATE BETWEEN x.start_dt
									AND x.end_dt
							) > 0
						THEN 'Y'
					ELSE 'N'
					END
				) AS intKanbanFlag
			,A.DN_COMPLETE_FLAG AS dnCompleteFlag
			,NULL AS matCompleteFlag
			,@RECEIVE_NO AS receiveNo
			,@SEND_FLAG AS sendFlag
			,--- Y/N 
			@PROCESS_ID AS processID
			,A.ORDER_RELEASE_DT AS orderReleaseDate
			,0 POST_STS
			,1 IS_COMPLETE
			,0 IS_TD
		FROM #ICS_POSTING A

		-- 1. UPDATE FLAG
		UPDATE #ICS_FILE
		SET INT_KANBAN_FLAG = 'N'
		-- USED IN OCTOBER   ,ORI_SUPP_CD = CAST(LTRIM(B.SUPPLIER_CVRT) AS CHAR(6))
		FROM #ICS_FILE A
			,TB_M_SUPPLIER_CVRT B
		WHERE LTRIM(RTRIM(A.RECEIVING_PLANT_CD)) = LTRIM(RTRIM(B.RECEIVING_PLANT))
			AND LTRIM(RTRIM(A.RECEIVING_AREA_CD)) = LTRIM(RTRIM(B.RECEIVING_AREA))

		/*                                                                                                    
                     =================================================                                                                                                     
                           END OF CREATE DATA LIST FROM ORDER                                                                                          
                     =================================================                                                                                                     
                     */
		
		/* Remark FID.Ridwan : 2020-09-10
		INSERT OPENQUERY (
			IPPCS_TO_ICS
			,
			'SELECT  
                     PROCESS_ID, 
                     SYSTEM_SOURCE, 
                     USR_ID, 
                     POSTING_DT,
                     PROCESS_DT,
                     REF_NO,
                     MOVEMENT_TYPE,
                     KANBAN_ORDER_NO, 
                     PROD_PURPOSE_CD, 
                     SOURCE_TYPE, 
                     ORI_MAT_NO, 
                     UNIT_OF_MEASURE_CD, 
                     RECEIVING_PLANT_CD, 
                     RECEIVING_AREA_CD, 
                     INT_KANBAN_FLAG, 
                     ORI_SUPP_CD, 
                     DN_COMPLETE_FLAG, 
                     CREATED_BY, 
                     CREATED_DT, 
                     POSTING_QUANTITY,  
                     RECEVING_PLANT_CD, 
                     ORI_UNIT_OF_MEASURE_CD, 
                     ARRIVAL_PLAN_DT, 
                     OTHER_PROCESS_ID  
                     FROM 
                     TB_T_GOOD_RECEIVE_KBS'
			)
			*/

			--* Validation *--
			DECLARE @DOC_TYPE VARCHAR(2),
				@DOC_TD VARCHAR(MAX),
				@ret CHAR(1) = 'N',
				@msgiD VARCHAR(100);

			SELECT @DOC_TYPE = SYSTEM_VALUE
			FROM TB_M_SYSTEM 
			WHERE FUNCTION_ID = 'COMMON_PO'
				AND SYSTEM_CD = 'PO_LOCAL_REGULER_DOC_TYPE';

			SELECT @DOC_TD = SYSTEM_VALUE 
			FROM TB_M_SYSTEM 
			WHERE FUNCTION_ID = 'DOCK_TD' 
				AND SYSTEM_CD = 'DOCK_TD'
			
			UPDATE A
			SET A.IS_TD = 1
			FROM #ICS_FILE A
			WHERE RECEIVING_AREA_CD IN (SELECT * FROM DBO.FN_SPLIT(@DOC_TD,','))

			--FID.Ridwan : 20210417 -> list manifest with error
			CREATE TABLE #Temp_Manifest_Error
			(
				MANIFEST_NO VARCHAR(20)
			)

			IF EXISTS(SELECT TOP 1 1 
				FROM #ICS_FILE A
				LEFT JOIN TB_M_DOCK_CD B ON A.RECEIVING_AREA_CD = B.DOCK_CD
				AND CAST(A.ARRIVAL_PLAN_DT AS DATE) BETWEEN CAST(B.VALID_DT_FR AS DATE) AND CAST(B.VALID_DT_TO AS DATE) 
				WHERE ISNULL(B.PLANT_CD, '') = '')
			BEGIN
				SET @Iserror = 'Y';

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT D.PROCESS_ID
					,MAX(D.SEQUENCE_NUMBER) + M.SEQ_NO AS SEQUENCE_NUMBER
					,M.MESSAGE_ID
					,M.MESSAGE_TYPE
					,'MSPT00001ERR :' + REPLACE(M.MESSAGE_TEXT, '{0}', 'Plant Code and Sloc Code for Dock Code = ' + ISNULL(M.DOCK_CD,'') + ', Manifest No = ' + ISNULL(M.MANIFEST_NO,'') + ' and Item No = ' + ISNULL(CONVERT(VARCHAR, M.ITEM_NO),'') + ', doesn''t exists in Master Dock Code') AS [MESSAGE]
					,@LOG_LOCATION AS [LOCATION]
					,@UserID AS CREATED_BY
					,CURRENT_TIMESTAMP AS CREATED_DATE
				FROM TB_R_LOG_D D
					,(
						SELECT ROW_NUMBER() OVER (
								ORDER BY A.REF_NO
								) AS SEQ_NO
							,A.RECEIVING_AREA_CD DOCK_CD
							,A.REF_NO MANIFEST_NO
							,A.ITEM_NO
							,MSG.MESSAGE_ID
							,MSG.MESSAGE_TYPE
							,MSG.MESSAGE_TEXT
						FROM #ICS_FILE A
						LEFT JOIN TB_M_DOCK_CD B ON A.RECEIVING_AREA_CD = B.DOCK_CD
						AND CAST(A.ARRIVAL_PLAN_DT AS DATE) BETWEEN CAST(B.VALID_DT_FR AS DATE) AND CAST(B.VALID_DT_TO AS DATE) 
							,dbo.TB_M_MESSAGE MSG
						WHERE ISNULL(B.PLANT_CD, '') = ''
							AND MSG.MESSAGE_ID = 'MSPT00001ERR'
						) M
				WHERE D.PROCESS_ID = @L_N_PROCESS_ID
				GROUP BY D.PROCESS_ID
					,M.MANIFEST_NO
					,M.ITEM_NO
					,M.DOCK_CD
					,M.SEQ_NO
					,M.MESSAGE_ID
					,M.MESSAGE_TYPE
					,M.MESSAGE_TEXT

				UPDATE A
				SET A.POST_STS = 1
				FROM #ICS_FILE A
				LEFT JOIN TB_M_DOCK_CD B ON A.RECEIVING_AREA_CD = B.DOCK_CD
				AND CAST(A.ARRIVAL_PLAN_DT AS DATE) BETWEEN CAST(B.VALID_DT_FR AS DATE) AND CAST(B.VALID_DT_TO AS DATE) 
				WHERE ISNULL(B.PLANT_CD, '') = ''

				--register to manifest detail
				DELETE FROM @TEMP_ERR_ICS

				INSERT INTO @TEMP_ERR_ICS
				SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 1)) ROWNO, REF_NO, ORI_MAT_NO
				FROM #ICS_FILE WHERE POST_STS = 1

				SET @cnerror = (SELECT COUNT(1) FROM @TEMP_ERR_ICS)

				WHILE @cnloop <= @cnerror
				BEGIN
					SELECT @Man = MANIFEST_NO,
						@Mat = MAT_NO
					FROM @TEMP_ERR_ICS
					WHERE ROW_NO = @cnloop

					EXEC [dbo].[SP_COMMON_GR_MANIFEST_LOG] @UserID
						,@Man
						,@Mat
						,1
						,'Plant Code and Sloc Code not found in Master Dock Code'

					SET @cnloop = @cnloop +1;
				END
			END
			
			IF EXISTS(SELECT TOP 1 1 
				FROM #ICS_FILE A
				LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE
					AND POH.SUPPLIER_CD = RTRIM(ORI_SUPP_CD)
					AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.ARRIVAL_PLAN_DT, 112)
				WHERE ISNULL(POH.PO_NO, '') = ''
					AND IS_TD = 0)
			BEGIN
				SET @Iserror = 'Y';

				-- remark FID.Ridwan --> manfiest doesnt have po still processing but complete flag = N
				--INSERT INTO TB_R_LOG_D (
				--	PROCESS_ID
				--	,SEQUENCE_NUMBER
				--	,MESSAGE_ID
				--	,MESSAGE_TYPE
				--	,[MESSAGE]
				--	,LOCATION
				--	,CREATED_BY
				--	,CREATED_DATE
				--	)
				--SELECT D.PROCESS_ID
				--	,MAX(D.SEQUENCE_NUMBER) + M.SEQ_NO AS SEQUENCE_NUMBER
				--	,M.MESSAGE_ID
				--	,M.MESSAGE_TYPE
				--	,'MSPT00001ERR :' + REPLACE(M.MESSAGE_TEXT, '{0}', 'PO No for Period: ' + M.ARRIVAL_PLAN_DT + ', Supplier Cd:' + M.ORI_SUPP_CD + ', Source Type' + M.SOURCE_TYPE + ' And Material No: ' + M.ORI_MAT_NO + ' not exists') AS [MESSAGE]
				--	,@LOG_LOCATION AS [LOCATION]
				--	,@UserID AS CREATED_BY
				--	,CURRENT_TIMESTAMP AS CREATED_DATE
				--FROM TB_R_LOG_D D
				--	,(
				--		SELECT ROW_NUMBER() OVER (
				--				ORDER BY A.REF_NO
				--				) AS SEQ_NO
				--			,CONVERT(VARCHAR(6), A.ARRIVAL_PLAN_DT, 112) ARRIVAL_PLAN_DT
				--			,RTRIM(ORI_SUPP_CD) ORI_SUPP_CD
				--			,RTRIM(A.SOURCE_TYPE) SOURCE_TYPE
				--			,LEFT(RTRIM(ORI_MAT_NO), 10) ORI_MAT_NO
				--			,MSG.MESSAGE_ID
				--			,MSG.MESSAGE_TYPE
				--			,MSG.MESSAGE_TEXT
				--		FROM #ICS_FILE A
				--		LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE
				--			AND POH.SUPPLIER_CD = RTRIM(ORI_SUPP_CD)
				--			AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.ARRIVAL_PLAN_DT, 112)
				--			,dbo.TB_M_MESSAGE MSG
				--		WHERE ISNULL(POH.PO_NO, '') = ''
				--			AND MSG.MESSAGE_ID = 'MSPT00001ERR'
				--		) M
				--WHERE D.PROCESS_ID = @L_N_PROCESS_ID
				--GROUP BY D.PROCESS_ID
				--	,M.ARRIVAL_PLAN_DT
				--	,M.ORI_SUPP_CD
				--	,M.SOURCE_TYPE
				--	,M.ORI_MAT_NO
				--	,M.SEQ_NO
				--	,M.MESSAGE_ID
				--	,M.MESSAGE_TYPE
				--	,M.MESSAGE_TEXT

				--UPDATE A
				--SET A.POST_STS = 1
				--FROM #ICS_FILE A
				--LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE
				--	AND POH.SUPPLIER_CD = RTRIM(ORI_SUPP_CD)
				--	AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.ARRIVAL_PLAN_DT, 112)
				--WHERE ISNULL(POH.PO_NO, '') = ''

				UPDATE A
				SET A.IS_COMPLETE = 0
				FROM #ICS_FILE A
				LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE
					AND POH.SUPPLIER_CD = RTRIM(ORI_SUPP_CD)
					AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.ARRIVAL_PLAN_DT, 112)
				WHERE ISNULL(POH.PO_NO, '') = ''
					AND IS_TD = 0

				--register to manifest detail
				DELETE FROM @TEMP_ERR_ICS

				INSERT INTO @TEMP_ERR_ICS
				SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 1)) ROWNO, REF_NO, ORI_MAT_NO
				FROM #ICS_FILE WHERE IS_COMPLETE = 0

				SET @cnerror = (SELECT COUNT(1) FROM @TEMP_ERR_ICS)

				WHILE @cnloop <= @cnerror
				BEGIN
					SELECT @Man = MANIFEST_NO,
						@Mat = MAT_NO
					FROM @TEMP_ERR_ICS
					WHERE ROW_NO = @cnloop

					EXEC [dbo].[SP_COMMON_GR_MANIFEST_LOG] @UserID
						,@Man
						,@Mat
						,3
						,'PO Data doesnt exist for the passing input parameters'

					SET @cnloop = @cnloop +1;
				END

				INSERT INTO #Temp_Manifest_Error
				SELECT MANIFEST_NO FROM @TEMP_ERR_ICS

			END

		IF OBJECT_ID('tempdb..#TB_T_GOOD_RECEIVE_TEMP') IS NOT NULL
		BEGIN
			DROP TABLE #TB_T_GOOD_RECEIVE_TEMP
		END;

		CREATE TABLE #TB_T_GOOD_RECEIVE_TEMP (
			ROWNO INT
			,PROCESS_KEY VARCHAR(63)
			,PROCESS_ID BIGINT
			,SYSTEM_SOURCE VARCHAR(20)
			,USR_ID VARCHAR(20)
			,SEQ_NO NUMERIC(12)
			,POSTING_DT DATETIME
			,DOC_DT DATETIME
			,PROCESS_DT DATETIME
			,REF_NO VARCHAR(16)
			,MOVEMENT_TYPE VARCHAR(4)
			,MAT_DOC_DESC VARCHAR(255)
			,KANBAN_ORDER_NO VARCHAR(16)
			,PROD_PURPOSE_CD VARCHAR(5)
			,SOURCE_TYPE VARCHAR(1)
			,MAT_NO VARCHAR(23)
			,ORI_MAT_NO VARCHAR(23)
			,MAT_DESC VARCHAR(40)
			,POSTING_QUANTITY INT
			,PART_COLOR_SFX VARCHAR(2)
			,PACKING_TYPE VARCHAR(2)
			,UNIT_OF_MEASURE_CD VARCHAR(6)
			,ORI_UNIT_OF_MEASURE_CD VARCHAR(6)
			,PLANT_CD VARCHAR(4)
			,SLOC_CD VARCHAR(6)
			,DOCK_CD VARCHAR(6)
			,MAT_CURR VARCHAR(3)
			,ACTUAL_EXCHANGE_RATE NUMERIC(7, 2)
			,SUPP_CD VARCHAR(6)
			,ORI_SUPP_CD VARCHAR(6)
			,PO_NO VARCHAR(10)
			,PO_DOC_TYPE VARCHAR(4)
			,PO_DOC_DT DATETIME
			,PO_ITEM_NO VARCHAR(5)
			,PO_MAT_PRICE NUMERIC(16, 5)
			,PO_CURR VARCHAR(3)
			,PO_EXCHANGE_RATE NUMERIC(13, 2)
			,GR_ORI_AMOUNT NUMERIC(16, 5)
			,GR_LOCAL_AMOUNT NUMERIC(16, 5)
			,PO_UNLIMITED_FLAG VARCHAR(1)
			,PO_TOLERANCE_PERCENTAGE NUMERIC(5, 2)
			,PO_TAX_CD VARCHAR(2)
			,PO_INV_WO_GR_FLAG VARCHAR(1)
			,MAT_COMPLETE_FLAG VARCHAR(1)
			,DN_COMPLETE_FLAG VARCHAR(1)
			,INT_KANBAN_FLAG VARCHAR(1)
			,ARRIVAL_PLAN_DT DATETIME
			,OTHER_PROCESS_ID BIGINT
			,COMPLETE_FLAG VARCHAR(1)
			,CREATED_BY VARCHAR(20)
			,CREATED_DT DATETIME
			,INSERT_FLAG INT
			,IS_COMPLETE INT
			,IS_TD INT
			)

		--IF @Iserror <> 'Y'
		IF EXISTS(SELECT TOP 1 1 FROM #ICS_FILE WHERE POST_STS = 0)
		BEGIN
			DECLARE @PROCESS_KEY VARCHAR(63)
				,@PROCESS_ID_PAR BIGINT
				,@SYSTEM_SOURCE VARCHAR(20)
				,@USR_ID VARCHAR(20)
				,@SEQ_NO NUMERIC(12)
				,@POSTING_DT DATETIME
				,@DOC_DT DATETIME
				,@PROCESS_DT DATETIME
				,@REF_NO VARCHAR(16)
				,@MOVEMENT_TYPE VARCHAR(4)
				,@MAT_DOC_DESC VARCHAR(255)
				,@KANBAN_ORDER_NO VARCHAR(16)
				,@PROD_PURPOSE_CD_PAR VARCHAR(5)
				,@SOURCE_TYPE VARCHAR(1)
				,@MAT_NO VARCHAR(23)
				,@ORI_MAT_NO VARCHAR(23)
				,@MAT_DESC VARCHAR(40)
				,@POSTING_QUANTITY INT
				,@PART_COLOR_SFX VARCHAR(2)
				,@PACKING_TYPE VARCHAR(2)
				,@UNIT_OF_MEASURE_CD VARCHAR(6)
				,@ORI_UNIT_OF_MEASURE_CD VARCHAR(6)
				,@PLANT_CD VARCHAR(4)
				,@SLOC_CD VARCHAR(6)
				,@DOCK_CD VARCHAR(6)
				,@MAT_CURR VARCHAR(3)
				,@ACTUAL_EXCHANGE_RATE NUMERIC(7, 2)
				,@SUPP_CD VARCHAR(6)
				,@ORI_SUPP_CD VARCHAR(6)
				,@PO_NO VARCHAR(10)
				,@PO_DOC_TYPE VARCHAR(4)
				,@PO_DOC_DT DATETIME
				,@PO_ITEM_NO VARCHAR(5)
				,@PO_MAT_PRICE NUMERIC(16, 5)
				,@PO_CURR VARCHAR(3)
				,@PO_EXCHANGE_RATE NUMERIC(13, 2)
				,@GR_ORI_AMOUNT NUMERIC(16, 5)
				,@GR_LOCAL_AMOUNT NUMERIC(16, 5)
				,@PO_UNLIMITED_FLAG VARCHAR(1)
				,@PO_TOLERANCE_PERCENTAGE NUMERIC(5, 2)
				,@PO_TAX_CD VARCHAR(2)
				,@PO_INV_WO_GR_FLAG VARCHAR(1)
				,@MAT_COMPLETE_FLAG VARCHAR(1)
				,@DN_COMPLETE_FLAG VARCHAR(1)
				,@INT_KANBAN_FLAG VARCHAR(1)
				,@ARRIVAL_PLAN_DT DATETIME
				,@OTHER_PROCESS_ID BIGINT
				,@COMPLETE_FLAG VARCHAR(1)
				,@CREATED_BY VARCHAR(20)
				,@CREATED_DT DATETIME
				,@INSERT_FLAG INT
				,@IS_COMPLETE INT
				,@IS_TD INT
				,@CN INT = 1
				,@CN_ALL INT = 0

			INSERT INTO #TB_T_GOOD_RECEIVE_TEMP (
				ROWNO
				,PROCESS_KEY
				,PROCESS_ID
				,SYSTEM_SOURCE
				,USR_ID
				,SEQ_NO
				,POSTING_DT
				,DOC_DT
				,PROCESS_DT
				,REF_NO
				,MOVEMENT_TYPE
				,MAT_DOC_DESC
				,KANBAN_ORDER_NO
				,PROD_PURPOSE_CD
				,SOURCE_TYPE
				,MAT_NO
				,ORI_MAT_NO
				,MAT_DESC
				,POSTING_QUANTITY
				,PART_COLOR_SFX
				,PACKING_TYPE
				,UNIT_OF_MEASURE_CD
				,ORI_UNIT_OF_MEASURE_CD
				,PLANT_CD
				,SLOC_CD
				,DOCK_CD
				,MAT_CURR
				,ACTUAL_EXCHANGE_RATE
				,SUPP_CD
				,ORI_SUPP_CD
				,PO_NO
				,PO_DOC_TYPE
				,PO_DOC_DT
				,PO_ITEM_NO
				,PO_MAT_PRICE
				,PO_CURR
				,PO_EXCHANGE_RATE
				,GR_ORI_AMOUNT
				,GR_LOCAL_AMOUNT
				,PO_UNLIMITED_FLAG
				,PO_TOLERANCE_PERCENTAGE
				,PO_TAX_CD
				,PO_INV_WO_GR_FLAG
				,MAT_COMPLETE_FLAG
				,DN_COMPLETE_FLAG
				,INT_KANBAN_FLAG
				,ARRIVAL_PLAN_DT
				,OTHER_PROCESS_ID
				,COMPLETE_FLAG
				,CREATED_BY
				,CREATED_DT
				,INSERT_FLAG
				,IS_COMPLETE
				,IS_TD
				)
			SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 1)) ROWNO 
				,TB.PROCESS_KEY
				,TB.PROCESS_ID
				,TB.SYSTEM_SOURCE
				,TB.USR_ID
				,ROW_NUMBER() OVER (
					ORDER BY (
							SELECT 1
							)
					) SEQ_NO
				,TB.POSTING_DT
				,TB.DOC_DT
				,TB.PROCESS_DT
				,TB.REF_NO
				,TB.MOVEMENT_TYPE
				,TB.MAT_DOC_DESC
				,TB.KANBAN_ORDER_NO
				,TB.PROD_PURPOSE_CD
				,TB.SOURCE_TYPE
				,TB.MAT_NO
				,TB.ORI_MAT_NO
				,TB.MAT_DESC
				,TB.POSTING_QUANTITY
				,TB.PART_COLOR_SFX
				,TB.PACKING_TYPE_CD
				,TB.UNIT_OF_MEASURE_CD
				,TB.ORI_UNIT_OF_MEASURE_CD
				,TB.PLANT_CD
				,TB.SLOC_CD
				,TB.DOCK_CD
				,TB.MAT_CURR
				,TB.ACTUAL_EXCHANGE_RATE
				,TB.SUPP_CD
				,TB.ORI_SUPP_CD
				,TB.PO_NO
				,TB.PO_DOC_TYPE
				,TB.PO_DOC_DT
				,TB.PO_ITEM_NO
				,TB.PO_MAT_PRICE
				,TB.PO_CURR
				,TB.PO_EXCHANGE_RATE
				,TB.GR_ORI_AMOUNT
				,TB.GR_LOCAL_AMOUNT
				,TB.PO_UNLIMITED_FLAG
				,TB.PO_TOLERANCE_PERCENTAGE
				,TB.PO_TAX_CD
				,TB.PO_INV_WO_GR_FLAG
				,TB.MAT_COMPLETE_FLAG
				,TB.DN_COMPLETE_FLAG
				,TB.INT_KANBAN_FLAG
				,TB.ARRIVAL_PLAN_DT
				,TB.OTHER_PROCESS_ID
				,TB.COMPLETE_FLAG
				,TB.CREATED_BY
				,TB.CREATED_DT
				,TB.INSERT_FLAG
				,TB.IS_COMPLETE
				,TB.IS_TD
			FROM (
			SELECT DISTINCT 'IPPCS' + RTRIM(REF_NO) + LEFT(RTRIM(ORI_MAT_NO), 10) + RTRIM(A.PROD_PURPOSE_CD) + RTRIM(A.SOURCE_TYPE) + CONVERT(VARCHAR, A.ITEM_NO) PROCESS_KEY
				,@L_N_PROCESS_ID PROCESS_ID
				,'IPPCS' SYSTEM_SOURCE
				,[USER_ID] USR_ID
				,POSTING_DATE POSTING_DT
				,ARRIVAL_PLAN_DT DOC_DT
				,PROCESS_DATE PROCESS_DT
				,RTRIM(REF_NO) REF_NO
				,RTRIM(MOV_TYPE) MOVEMENT_TYPE
				,'Good Receive IPPCS' MAT_DOC_DESC
				,RTRIM(KANBAN_ORDER_NO) KANBAN_ORDER_NO
				,RTRIM(A.PROD_PURPOSE_CD) PROD_PURPOSE_CD
				,RTRIM(A.SOURCE_TYPE) SOURCE_TYPE
				,LEFT(RTRIM(ORI_MAT_NO), 10) MAT_NO
				,RTRIM(ORI_MAT_NO) ORI_MAT_NO
				,PART_NAME MAT_DESC
				,POSTING_QUANTITY
				,RIGHT(RTRIM(ORI_MAT_NO), 2) PART_COLOR_SFX
				-- 20210102 - FID.Ridwan --> if not exist in packing indicator than set default 0 as request ISTD.Hesti
				,ISNULL(MPI.PACKING_TYPE_CD, '0') PACKING_TYPE_CD
				,RTRIM(ORI_UNIT_OF_MEASURE_CD) UNIT_OF_MEASURE_CD
				,RTRIM(ORI_UNIT_OF_MEASURE_CD) ORI_UNIT_OF_MEASURE_CD
				--,RTRIM(RECEIVING_PLANT_CD) PLANT_CD
				,B.PLANT_CD
				,B.SLOC_CD
				,RTRIM(RECEIVING_AREA_CD) DOCK_CD
				,'IDR' MAT_CURR
				,'1' ACTUAL_EXCHANGE_RATE
				,RTRIM(ORI_SUPP_CD) SUPP_CD
				,RTRIM(ORI_SUPP_CD) ORI_SUPP_CD
				,POH.PO_NO
				,POH.DOC_TYPE PO_DOC_TYPE
				,POH.PO_DATE PO_DOC_DT
				,POD.PO_ITEM_NO PO_ITEM_NO
				,POD.PO_MAT_PRICE
				,POH.PO_CURR
				,POH.EXCHANGE_RATE PO_EXCHANGE_RATE
				,POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE GR_ORI_AMOUNT
				,POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE * POH.EXCHANGE_RATE GR_LOCAL_AMOUNT
				,'N' PO_UNLIMITED_FLAG
				,'0.00' PO_TOLERANCE_PERCENTAGE
				,'' PO_TAX_CD
				,'N' PO_INV_WO_GR_FLAG
				,'Y' MAT_COMPLETE_FLAG
				,DN_COMPLETE_FLAG
				,INT_KANBAN_FLAG
				,ARRIVAL_PLAN_DT
				,@L_N_PROCESS_ID OTHER_PROCESS_ID
				,'Y' COMPLETE_FLAG
				,[USER_ID] CREATED_BY
				,GETDATE() CREATED_DT
				,0 INSERT_FLAG
				,IS_COMPLETE
				,IS_TD
			FROM #ICS_FILE A
			JOIN TB_M_DOCK_CD B ON A.RECEIVING_AREA_CD = B.DOCK_CD
				AND CAST(A.ARRIVAL_PLAN_DT AS DATE) BETWEEN CAST(B.VALID_DT_FR AS DATE)
					AND CAST(B.VALID_DT_TO AS DATE)
			LEFT JOIN TB_M_PACKING_INDICATOR MPI ON A.RECEIVING_AREA_CD = MPI.DOCK_CD
				AND RIGHT(RTRIM(ORI_MAT_NO), 2) = MPI.PART_COLOR_SFX
				AND LEFT(RTRIM(ORI_MAT_NO), 10) = MPI.MAT_NO
				AND RTRIM(A.PROD_PURPOSE_CD) = MPI.PROD_PURPOSE_CD
				AND CAST(A.ARRIVAL_PLAN_DT AS DATE) BETWEEN CAST(MPI.VALID_DT_FR AS DATE)
					AND CAST(MPI.VALID_DT_TO AS DATE)
			LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE --POH.PO_NO = POD.PO_NO
				AND POH.SUPPLIER_CD = RTRIM(ORI_SUPP_CD)
				AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.ARRIVAL_PLAN_DT, 112)
			LEFT JOIN TB_R_PO_ITEM POD ON POH.PO_NO = POD.PO_NO
				AND POD.MAT_NO = LEFT(RTRIM(ORI_MAT_NO), 10)
				AND POD.PROD_PURPOSE_CD = RTRIM(A.PROD_PURPOSE_CD)
				AND POD.SOURCE_TYPE = RTRIM(A.SOURCE_TYPE)
				AND POD.PART_COLOR_SFX = RIGHT(RTRIM(ORI_MAT_NO), 2)
				AND POD.PLANT_CD = B.PLANT_CD
				AND POD.SLOC_CD = B.SLOC_CD
				AND POD.PACKING_TYPE = ISNULL(MPI.PACKING_TYPE_CD, '0')
			WHERE A.POST_STS = 0
			) TB

			SELECT @CN_ALL = COUNT(1) FROM #TB_T_GOOD_RECEIVE_TEMP

			WHILE @CN <= @CN_ALL
			BEGIN
				SELECT @PROCESS_ID_PAR = PROCESS_ID
					,@CREATED_BY = CREATED_BY
					,@PO_NO = PO_NO
					,@PO_ITEM_NO = PO_ITEM_NO
					,@MAT_NO = MAT_NO
					,@SOURCE_TYPE = SOURCE_TYPE
					,@PROD_PURPOSE_CD = PROD_PURPOSE_CD
					,@PACKING_TYPE = PACKING_TYPE
					,@PLANT_CD = PLANT_CD
					,@SLOC_CD = SLOC_CD
					,@ARRIVAL_PLAN_DT = ARRIVAL_PLAN_DT
					,@POSTING_QUANTITY = POSTING_QUANTITY
					,@PART_COLOR_SFX = PART_COLOR_SFX
					,@REF_NO = REF_NO
					,@IS_COMPLETE = IS_COMPLETE
					,@IS_TD = IS_TD
				FROM #TB_T_GOOD_RECEIVE_TEMP
				WHERE ROWNO = @CN

				IF @IS_COMPLETE = 0
				BEGIN
					SET @Param = 'Manifest No: ' + @REF_NO + ' and Material No: ' + @MAT_NO + ' is not complete yet';

					EXEC dbo.CommonGetMessage 'MPCS00001INF'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@Param

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @L_N_PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @L_N_PROCESS_ID
							)
						,'MPCS00001INF'
						,'INF'
						,'MPCS00001INF : ' + @MSG_TXT
						,@LOG_LOCATION
						,@UserID
						,GETDATE()
				END ELSE IF @IS_TD = 1
				BEGIN
					SET @Param = 'Manifest No: ' + @REF_NO + ' and Material No: ' + @MAT_NO + ' is a TD manifest, no need for PO calculations';

					EXEC dbo.CommonGetMessage 'MPCS00001INF'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@Param

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @L_N_PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @L_N_PROCESS_ID
							)
						,'MPCS00001INF'
						,'INF'
						,'MPCS00001INF : ' + @MSG_TXT
						,@LOG_LOCATION
						,@UserID
						,GETDATE()
				END ELSE
				BEGIN
					DELETE FROM TB_S_RESULT_PO WHERE PROCESS_ID = @L_N_PROCESS_ID

					SET @Param = 'Processing PO for Manifest No: ' + @REF_NO + ' and Material No: ' + @MAT_NO;

					EXEC dbo.CommonGetMessage 'MPCS00002INF'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@Param

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @L_N_PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @L_N_PROCESS_ID
							)
						,'MPCS00002INF'
						,'INF'
						,'MPCS00002INF : ' + @MSG_TXT
						,@LOG_LOCATION
						,@UserID
						,GETDATE()

					EXEC  [dbo].[SP_COMMON_GR_UPDATE_PO] @CREATED_BY
						,@PROCESS_ID_PAR
						,@REF_NO
						,@PO_NO
						,@MAT_NO
						,@SOURCE_TYPE
						,@PROD_PURPOSE_CD
						,@PACKING_TYPE
						,@PLANT_CD
						,@SLOC_CD
						,@ARRIVAL_PLAN_DT 
						,@POSTING_QUANTITY 
						,@PART_COLOR_SFX 
						,''

					IF EXISTS(SELECT 1 FROM TB_S_RESULT_PO WHERE Result = 'Y' AND PROCESS_ID = @L_N_PROCESS_ID)
					BEGIN
						SET @Iserror = 'Y';
						SET @ret = 'Y';
						SET @msgiD = 'MPCS00123ERR';

						UPDATE A
						SET A.IS_COMPLETE = 0
						FROM #TB_T_GOOD_RECEIVE_TEMP A
						WHERE ROWNO = @CN

						INSERT INTO #Temp_Manifest_Error
						SELECT @REF_NO MANIFEST_NO

						--BREAK;
					END ELSE
					BEGIN
						SET @ret = 'N';
						SET @msgiD = 'MPCS00122INF';

						DECLARE @PO VARCHAR(20), @POITM VARCHAR(10);

						SELECT @PO = PO_NO, @POITM = PO_ITEM_NO FROM TB_S_RESULT_PO WHERE PROCESS_ID = @L_N_PROCESS_ID

						UPDATE A
						SET A.INSERT_FLAG = 0
							,A.PO_NO = ISNULL(@PO, @PO_NO)
							,A.PO_ITEM_NO = ISNULL(@POITM, @PO_ITEM_NO)
						FROM #TB_T_GOOD_RECEIVE_TEMP A
						WHERE ROWNO = @CN
					END
				
					EXEC dbo.CommonGetMessage @msgiD
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@Param

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @L_N_PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @L_N_PROCESS_ID
							)
						,@msgiD
						,'INF'
						,@msgiD + ' : ' + @MSG_TXT
						,@LOG_LOCATION
						,@UserID
						,GETDATE()
				END
				SET @CN = @CN + 1;
			END

			DELETE FROM TB_S_RESULT_PO WHERE PROCESS_ID = @L_N_PROCESS_ID
		END

		--IF @Iserror <> 'Y'
		IF EXISTS(SELECT TOP 1 1 FROM #TB_T_GOOD_RECEIVE_TEMP WHERE INSERT_FLAG = 0)
		BEGIN
			--FID.Ridwan: 2021-05-03 handle parname is null
			IF OBJECT_ID('tempdb..#TEMP_TB_M_PART_INFO') IS NOT NULL
			BEGIN
				DROP TABLE #TEMP_TB_M_PART_INFO
			END;

			SELECT B.PART_NO
				,B.PART_NAME
			INTO #TEMP_TB_M_PART_INFO
			FROM (
				SELECT ROW_NUMBER() OVER (
						PARTITION BY LEFT(A.PART_NO, 10) ORDER BY A.PART_NO ASC
						) NO
					,LEFT(A.PART_NO, 10) PART_NO
					,A.PART_NAME
				FROM TB_M_PART_INFO A
				JOIN #TB_T_GOOD_RECEIVE_TEMP TB ON LEFT(A.PART_NO, 10) = TB.MAT_NO
				) B
			WHERE B.NO = 1

			--FID.Ridwan: 2020-12-04 add logic comp price cd and rate get from tb r po d
			-- with logic if comp price cd more than 1 record then pivot it.
			IF OBJECT_ID('tempdb..#TB_T_PO_D') IS NOT NULL
			BEGIN
				DROP TABLE #TB_T_PO_D
			END;

			CREATE TABLE #TB_T_PO_D (
				PO_NO VARCHAR(50)
				,PO_ITEM VARCHAR(10)
				,COMP_CD_RATE VARCHAR(MAX)
				)

			INSERT INTO #TB_T_PO_D
			SELECT DISTINCT U2.PO_NO
				,U2.PO_ITEM_NO
				,substring((
						SELECT '|' + U1.COMP_PRICE_CD + ';' + CONVERT(VARCHAR, U1.COMP_PRICE_RATE) AS [text()]
						FROM TB_R_PO_D U1
						WHERE u1.PO_NO = u2.PO_NO
							AND u1.PO_ITEM = u2.PO_ITEM_NO
						ORDER BY U1.PO_ITEM
						FOR XML PATH('')
						), 2, 1000)
			FROM #TB_T_GOOD_RECEIVE_TEMP U2
			WHERE INSERT_FLAG = 0

			MERGE 
				TB_T_GOOD_RECEIVE AS [TARGET]
			USING (
					SELECT DISTINCT A.PROCESS_KEY
						,A.PROCESS_ID
						,A.SYSTEM_SOURCE
						,A.USR_ID
						,A.SEQ_NO
						,A.POSTING_DT
						-- 20210427 -- FID.Ridwan as request ISTD
						--,A.DOC_DT
						,DOM.ARRIVAL_ACTUAL_DT DOC_DT
						,A.PROCESS_DT
						,A.REF_NO
						,A.MOVEMENT_TYPE
						,A.MAT_DOC_DESC
						,A.KANBAN_ORDER_NO
						,A.PROD_PURPOSE_CD
						,A.SOURCE_TYPE
						,A.MAT_NO
						,A.ORI_MAT_NO
						,(CASE WHEN ISNULL(A.MAT_DESC, '') <> '' THEN A.MAT_DESC ELSE ISNULL(TBM.PART_NAME, 'Part name not exist') END) MAT_DESC
						,A.POSTING_QUANTITY
						,A.PART_COLOR_SFX
						,A.PACKING_TYPE
						,A.UNIT_OF_MEASURE_CD
						,A.ORI_UNIT_OF_MEASURE_CD
						,A.PLANT_CD
						,A.SLOC_CD
						,A.DOCK_CD
						,A.MAT_CURR
						,A.ACTUAL_EXCHANGE_RATE
						,A.SUPP_CD
						,A.ORI_SUPP_CD
						,CASE WHEN A.IS_TD = 0 THEN POH.PO_NO ELSE NULL END PO_NO
						,CASE WHEN A.IS_TD = 0 THEN POH.DOC_TYPE ELSE NULL END PO_DOC_TYPE
						,CASE WHEN A.IS_TD = 0 THEN POH.PO_DATE ELSE NULL END PO_DOC_DT
						,POD.PO_ITEM_NO PO_ITEM_NO
						,POD.PO_MAT_PRICE
						,CASE WHEN A.IS_TD = 0 THEN POH.PO_CURR ELSE NULL END PO_CURR
						,CASE WHEN A.IS_TD = 0 THEN POH.EXCHANGE_RATE ELSE NULL END PO_EXCHANGE_RATE
						,CASE WHEN A.IS_TD = 0 THEN POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE ELSE NULL END GR_ORI_AMOUNT
						,CASE WHEN A.IS_TD = 0 THEN POD.PO_QUANTITY_ORIGINAL * POD.PO_MAT_PRICE * POH.EXCHANGE_RATE ELSE NULL END GR_LOCAL_AMOUNT
						,A.PO_UNLIMITED_FLAG
						,CASE WHEN ISNULL(POD.PO_NO, '') <> '' THEN A.PO_TOLERANCE_PERCENTAGE ELSE NULL END PO_TOLERANCE_PERCENTAGE
						,CASE WHEN ISNULL(POD.PO_NO, '') <> '' THEN A.PO_TAX_CD ELSE NULL END PO_TAX_CD
						,A.PO_INV_WO_GR_FLAG
						,A.MAT_COMPLETE_FLAG
						,A.DN_COMPLETE_FLAG
						,A.INT_KANBAN_FLAG
						,A.ARRIVAL_PLAN_DT
						,A.OTHER_PROCESS_ID
						,(CASE WHEN A.IS_COMPLETE = 0 THEN 'N' WHEN A.IS_TD = 1 THEN 'Y' ELSE A.COMPLETE_FLAG END) COMPLETE_FLAG
						,A.CREATED_BY
						,A.CREATED_DT
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 1, 1) ELSE NULL END COMP_PRICE_CD_1
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 1, 2) ELSE NULL END COMP_PRICE_AMOUNT_1
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 2, 1) ELSE NULL END COMP_PRICE_CD_2
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 2, 2) ELSE NULL END COMP_PRICE_AMOUNT_2
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 3, 1) ELSE NULL END COMP_PRICE_CD_3
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 3, 2) ELSE NULL END COMP_PRICE_AMOUNT_3
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 4, 1) ELSE NULL END COMP_PRICE_CD_4
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 4, 2) ELSE NULL END COMP_PRICE_AMOUNT_4
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 5, 1) ELSE NULL END COMP_PRICE_CD_5
						,CASE WHEN A.IS_TD = 0 THEN dbo.[fn_getcomppricecd](B.COMP_CD_RATE, 5, 2) ELSE NULL END COMP_PRICE_AMOUNT_5
					FROM #TB_T_GOOD_RECEIVE_TEMP A
					JOIN TB_R_DAILY_ORDER_MANIFEST DOM ON A.REF_NO = DOM.MANIFEST_NO -- 20210427 -- FID.Ridwan as request ISTD
					LEFT JOIN #TEMP_TB_M_PART_INFO TBM ON A.MAT_NO = TBM.PART_NO --FID.Ridwan: 2021-05-03 handle parname is null
					LEFT JOIN TB_R_PO_H POH ON POH.DOC_TYPE = @DOC_TYPE --POH.PO_NO = POD.PO_NO
						AND POH.SUPPLIER_CD = RTRIM(A.ORI_SUPP_CD)
						AND POH.PRODUCTION_MONTH = CONVERT(VARCHAR(6), A.ARRIVAL_PLAN_DT, 112)
					LEFT JOIN TB_R_PO_ITEM POD ON POH.PO_NO = POD.PO_NO
						AND POD.MAT_NO = LEFT(RTRIM(ORI_MAT_NO), 10)
						AND POD.PROD_PURPOSE_CD = RTRIM(A.PROD_PURPOSE_CD)
						AND POD.SOURCE_TYPE = RTRIM(A.SOURCE_TYPE)
						AND POD.PART_COLOR_SFX = RIGHT(RTRIM(ORI_MAT_NO), 2)
						AND POD.PLANT_CD = A.PLANT_CD
						AND POD.SLOC_CD = A.SLOC_CD
						AND POD.PACKING_TYPE = A.PACKING_TYPE
					LEFT JOIN #TB_T_PO_D B ON POD.PO_NO = B.PO_NO
						AND POD.PO_ITEM_NO = B.PO_ITEM
					WHERE INSERT_FLAG = 0

				) AS [SOURCE]
				  ON [SOURCE].PROCESS_KEY = [TARGET].PROCESS_KEY
			WHEN MATCHED
				  THEN
					UPDATE
					SET [TARGET].PO_NO = [SOURCE].PO_NO
					  ,[TARGET].PO_DOC_TYPE = [SOURCE].PO_DOC_TYPE
					  ,[TARGET].PO_DOC_DT = [SOURCE].PO_DOC_DT
					  ,[TARGET].PO_ITEM_NO = [SOURCE].PO_ITEM_NO
					  ,[TARGET].PO_MAT_PRICE = [SOURCE].PO_MAT_PRICE
					  ,[TARGET].PO_CURR = [SOURCE].PO_CURR
					  ,[TARGET].PO_EXCHANGE_RATE = [SOURCE].PO_EXCHANGE_RATE
					  ,[TARGET].GR_ORI_AMOUNT = [SOURCE].GR_ORI_AMOUNT
					  ,[TARGET].GR_LOCAL_AMOUNT = [SOURCE].GR_LOCAL_AMOUNT
					  ,[TARGET].COMP_PRICE_CD_1 = [SOURCE].COMP_PRICE_CD_1
					  ,[TARGET].COMP_PRICE_AMOUNT_1 = [SOURCE].COMP_PRICE_AMOUNT_1
					  ,[TARGET].COMP_PRICE_CD_2 = [SOURCE].COMP_PRICE_CD_2
					  ,[TARGET].COMP_PRICE_AMOUNT_2 = [SOURCE].COMP_PRICE_AMOUNT_2
					  ,[TARGET].COMP_PRICE_CD_3 = [SOURCE].COMP_PRICE_CD_3
					  ,[TARGET].COMP_PRICE_AMOUNT_3 = [SOURCE].COMP_PRICE_AMOUNT_3
					  ,[TARGET].COMP_PRICE_CD_4 = [SOURCE].COMP_PRICE_CD_4
					  ,[TARGET].COMP_PRICE_AMOUNT_4 = [SOURCE].COMP_PRICE_AMOUNT_4
					  ,[TARGET].COMP_PRICE_CD_5 = [SOURCE].COMP_PRICE_CD_5
					  ,[TARGET].COMP_PRICE_AMOUNT_5 = [SOURCE].COMP_PRICE_AMOUNT_5
					  ,[TARGET].COMPLETE_FLAG = [SOURCE].COMPLETE_FLAG
					  ,[TARGET].SEND_FLAG = NULL
					  ,[TARGET].PROCESS_ID = [SOURCE].PROCESS_ID
					  ,[TARGET].CHANGED_BY = [SOURCE].CREATED_BY
					  ,[TARGET].CHANGED_DT = [SOURCE].CREATED_DT
			WHEN NOT MATCHED
				  THEN
					INSERT (
						PROCESS_KEY
						,PROCESS_ID
						,SYSTEM_SOURCE
						,USR_ID
						,SEQ_NO
						,POSTING_DT
						,DOC_DT
						,PROCESS_DT
						,REF_NO
						,MOVEMENT_TYPE
						,MAT_DOC_DESC
						,KANBAN_ORDER_NO
						,PROD_PURPOSE_CD
						,SOURCE_TYPE
						,MAT_NO
						,ORI_MAT_NO
						,MAT_DESC
						,POSTING_QUANTITY
						,PART_COLOR_SFX
						,PACKING_TYPE
						,UNIT_OF_MEASURE_CD
						,ORI_UNIT_OF_MEASURE_CD
						,PLANT_CD
						,SLOC_CD
						,DOCK_CD
						,MAT_CURR
						,ACTUAL_EXCHANGE_RATE
						,SUPP_CD
						,ORI_SUPP_CD
						,PO_NO
						,PO_DOC_TYPE
						,PO_DOC_DT
						,PO_ITEM_NO
						,PO_MAT_PRICE
						,PO_CURR
						,PO_EXCHANGE_RATE
						,GR_ORI_AMOUNT
						,GR_LOCAL_AMOUNT
						,PO_UNLIMITED_FLAG
						,PO_TOLERANCE_PERCENTAGE
						,PO_TAX_CD
						,PO_INV_WO_GR_FLAG
						,MAT_COMPLETE_FLAG
						,DN_COMPLETE_FLAG
						,INT_KANBAN_FLAG
						,ARRIVAL_PLAN_DT
						,OTHER_PROCESS_ID
						,COMPLETE_FLAG
						,CREATED_BY
						,CREATED_DT
						,COMP_PRICE_CD_1
						,COMP_PRICE_AMOUNT_1
						,COMP_PRICE_CD_2
						,COMP_PRICE_AMOUNT_2
						,COMP_PRICE_CD_3
						,COMP_PRICE_AMOUNT_3
						,COMP_PRICE_CD_4
						,COMP_PRICE_AMOUNT_4
						,COMP_PRICE_CD_5
						,COMP_PRICE_AMOUNT_5
					)
					VALUES (
						[SOURCE].PROCESS_KEY
						,[SOURCE].PROCESS_ID
						,[SOURCE].SYSTEM_SOURCE
						,[SOURCE].USR_ID
						,[SOURCE].SEQ_NO
						,[SOURCE].POSTING_DT
						,[SOURCE].DOC_DT
						,[SOURCE].PROCESS_DT
						,[SOURCE].REF_NO
						,[SOURCE].MOVEMENT_TYPE
						,[SOURCE].MAT_DOC_DESC
						,[SOURCE].KANBAN_ORDER_NO
						,[SOURCE].PROD_PURPOSE_CD
						,[SOURCE].SOURCE_TYPE
						,[SOURCE].MAT_NO
						,[SOURCE].ORI_MAT_NO
						,[SOURCE].MAT_DESC
						,[SOURCE].POSTING_QUANTITY
						,[SOURCE].PART_COLOR_SFX
						,[SOURCE].PACKING_TYPE
						,[SOURCE].UNIT_OF_MEASURE_CD
						,[SOURCE].ORI_UNIT_OF_MEASURE_CD
						,[SOURCE].PLANT_CD
						,[SOURCE].SLOC_CD
						,[SOURCE].DOCK_CD
						,[SOURCE].MAT_CURR
						,[SOURCE].ACTUAL_EXCHANGE_RATE
						,[SOURCE].SUPP_CD
						,[SOURCE].ORI_SUPP_CD
						,[SOURCE].PO_NO
						,[SOURCE].PO_DOC_TYPE
						,[SOURCE].PO_DOC_DT
						,[SOURCE].PO_ITEM_NO
						,[SOURCE].PO_MAT_PRICE
						,[SOURCE].PO_CURR
						,[SOURCE].PO_EXCHANGE_RATE
						,[SOURCE].GR_ORI_AMOUNT
						,[SOURCE].GR_LOCAL_AMOUNT
						,[SOURCE].PO_UNLIMITED_FLAG
						,[SOURCE].PO_TOLERANCE_PERCENTAGE
						,[SOURCE].PO_TAX_CD
						,[SOURCE].PO_INV_WO_GR_FLAG
						,[SOURCE].MAT_COMPLETE_FLAG
						,[SOURCE].DN_COMPLETE_FLAG
						,[SOURCE].INT_KANBAN_FLAG
						,[SOURCE].ARRIVAL_PLAN_DT
						,[SOURCE].OTHER_PROCESS_ID
						,[SOURCE].COMPLETE_FLAG
						,[SOURCE].CREATED_BY
						,[SOURCE].CREATED_DT
						,[SOURCE].COMP_PRICE_CD_1
						,[SOURCE].COMP_PRICE_AMOUNT_1
						,[SOURCE].COMP_PRICE_CD_2
						,[SOURCE].COMP_PRICE_AMOUNT_2
						,[SOURCE].COMP_PRICE_CD_3
						,[SOURCE].COMP_PRICE_AMOUNT_3
						,[SOURCE].COMP_PRICE_CD_4
						,[SOURCE].COMP_PRICE_AMOUNT_4
						,[SOURCE].COMP_PRICE_CD_5
						,[SOURCE].COMP_PRICE_AMOUNT_5
					  );

		END --ELSE

		IF EXISTS(SELECT TOP 1 1 FROM #ICS_FILE WHERE POST_STS = 1)
		BEGIN
			-- roleback if error
			UPDATE OM
			SET 
				OM.MANIFEST_RECEIVE_FLAG = '5', --CASE WHEN @UserID = 'system.autoapprove' THEN '2' ELSE '5' END,
				--OM.APPROVED_BY = NULL,
				--OM.APPROVED_DT = NULL,
				OM.IN_PROGRESS = '3', --CASE WHEN @UserID = 'system.autoapprove' THEN '0' ELSE '3' END,
				OM.CHANGED_BY = @UserID,
				OM.CHANGED_DT = GETDATE()
			FROM TB_R_DAILY_ORDER_MANIFEST OM
			WHERE OM.MANIFEST_NO IN (
				SELECT DISTINCT RTRIM(LTRIM(REF_NO)) FROM #ICS_FILE WHERE POST_STS = 1
				)
		END

		IF EXISTS(SELECT TOP 1 1 FROM #Temp_Manifest_Error)
		BEGIN
			-- roleback if error
			UPDATE OM
			SET 
				OM.MANIFEST_RECEIVE_FLAG = '5', --CASE WHEN @UserID = 'system.autoapprove' THEN '2' ELSE '5' END,
				--OM.APPROVED_BY = NULL,
				--OM.APPROVED_DT = NULL,
				OM.IN_PROGRESS = '3', --CASE WHEN @UserID = 'system.autoapprove' THEN '0' ELSE '3' END,
				OM.CHANGED_BY = @UserID,
				OM.CHANGED_DT = GETDATE()
			FROM TB_R_DAILY_ORDER_MANIFEST OM 
			JOIN (SELECT DISTINCT RTRIM(LTRIM(a.MANIFEST_NO)) REF_NO FROM #Temp_Manifest_Error a) TM ON OM.MANIFEST_NO = TM.REF_NO
		END

		IF EXISTS(SELECT TOP 1 1 FROM #TB_T_GOOD_RECEIVE_TEMP WHERE INSERT_FLAG = 1)
		BEGIN
			UPDATE OM
			SET 
				OM.MANIFEST_RECEIVE_FLAG = '5', --CASE WHEN @UserID = 'system.autoapprove' THEN '2' ELSE '5' END,
				--OM.APPROVED_BY = NULL,
				--OM.APPROVED_DT = NULL,
				OM.IN_PROGRESS = '3', --CASE WHEN @UserID = 'system.autoapprove' THEN '0' ELSE '3' END,
				OM.CHANGED_BY = @UserID,
				OM.CHANGED_DT = GETDATE()
			FROM TB_R_DAILY_ORDER_MANIFEST OM
			WHERE OM.MANIFEST_NO IN (
				SELECT DISTINCT RTRIM(LTRIM(REF_NO)) FROM #TB_T_GOOD_RECEIVE_TEMP WHERE INSERT_FLAG = 1
				)
		END
	END

	-- CHECK ERR
	/*
    SELECT @L_N_COUNT_ERR = COUNT(1) FROM
    TB_R_DAILY_ORDER_MANIFEST
    where MANIFEST_NO in ( SELECT * FROM #TempManifestNo )
    and (ISNULL(ICS_FLAG,1) <> 0
    or ISNULL(CANCEL_FLAG,1) <> 0
    or ISNULL(DROP_STATUS_FLAG,0) <> 1
    or ISNULL(PROBLEM_FLAG,1) <> 0
    or MANIFEST_RECEIVE_FLAG not in ('0','1','2','3','5'));*/
	SELECT @L_N_COUNT_ERR = COUNT(1)
	FROM TB_R_DAILY_ORDER_MANIFEST
	WHERE MANIFEST_NO IN (
			SELECT *
			FROM #TempManifestNo
			)
		AND (
			ISNULL(ICS_FLAG, 1) <> 0
			OR ISNULL(CANCEL_FLAG, 1) <> 0
			OR ISNULL(DROP_STATUS_FLAG, 0) <> 1
			OR ISNULL(PROBLEM_FLAG, 1) <> 0
			OR MANIFEST_RECEIVE_FLAG NOT IN (
				'2'
				,'3'
				,'5'
				)
			);

	-- CHECK NUMBER OF MANIFEST
	SELECT @L_N_COUNT_MANIFEST = COUNT(1)
	FROM #TempManifestNo;

	/* Remark FID.Ridwan : 2020-09-10
	--COMPARE MANIFEST
	IF @L_N_COUNT_ERR <> @L_N_COUNT_MANIFEST
	BEGIN
		INSERT INTO [dbo].[TB_R_ICS_QUEUE] (
			[IPPCS_MODUL]
			,[IPPCS_FUNCTION]
			,[PROCESS_ID]
			,[PROCESS_STATUS]
			,[CREATED_BY]
			,[CREATED_DT]
			)
		VALUES (
			'4'
			,'42001'
			,@L_N_PROCESS_ID
			,0
			,@UserID
			,GETDATE()
			);

			         INSERT OPENQUERY (IPPCS_TO_ICS, 
			                                    'SELECT  
			                                           OTHER_PROCESS_ID,
			                                           STATUS,
			                                           REFERENCE,
			                                           CREATED_BY,
			                                           CREATED_DT
			                                  FROM TB_R_PCS_INTERFACE_STATUS')
			VALUES (@L_N_PROCESS_ID, 0, 'Posting Goods Receipt',@UserID,GETDATE());                                             
	END;*/

	BEGIN
		SELECT c.*
		FROM TB_R_DAILY_ORDER_MANIFEST c
			,TB_R_DAILY_ORDER_PART d
		WHERE c.MANIFEST_NO IN (
				SELECT *
				FROM #TempManifestNo
				)
			AND c.MANIFEST_NO = d.MANIFEST_NO
			AND (
				ISNULL(c.ICS_FLAG, 1) <> 0
				OR ISNULL(c.CANCEL_FLAG, 1) <> 0
				OR ISNULL(c.DROP_STATUS_FLAG, 0) <> 1
				OR ISNULL(c.PROBLEM_FLAG, 1) <> 0
				OR c.MANIFEST_RECEIVE_FLAG NOT IN (
					'0'
					,'1'
					,'2'
					,'3'
					,'5'
					)
				-- added for TP
				OR ISNULL(d.POSTING_STS, '3') = 0
				);
	END;

	IF OBJECT_ID('tempdb..#TempManifestNo') IS NOT NULL
		DROP TABLE #TempManifestNo

	IF OBJECT_ID('tempdb..#tempDockCode') IS NOT NULL
		DROP TABLE #tempDockCode

	IF OBJECT_ID('tempdb..#tempRvcPlantCode') IS NOT NULL
		DROP TABLE #tempRvcPlantCode

	IF OBJECT_ID('tempdb..#tempDockCode') IS NOT NULL
		DROP TABLE #tempDockCode

	IF OBJECT_ID('tempdb..#tempSupplierCode') IS NOT NULL
		DROP TABLE #tempSupplierCode

	IF OBJECT_ID('tempdb..#ICS_FILE') IS NOT NULL
		DROP TABLE #ICS_FILE

	IF OBJECT_ID('tempdb..#ICS_POSTING') IS NOT NULL
		DROP TABLE #ICS_POSTING

	IF OBJECT_ID('tempdb..#INVOICENO_GR') IS NOT NULL
		DROP TABLE #INVOICENO_GR

	IF OBJECT_ID('tempdb..#Temp_Manifest_Error') IS NOT NULL
		DROP TABLE #Temp_Manifest_Error

	END TRY

	BEGIN CATCH
		SET @Param = CONVERT(VARCHAR(1000), ERROR_MESSAGE());
		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@Param

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @L_N_PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @L_N_PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@UserID
			,GETDATE()
	END CATCH

END
