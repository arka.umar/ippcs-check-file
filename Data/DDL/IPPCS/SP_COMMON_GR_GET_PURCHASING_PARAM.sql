
-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	05-Oct-2020
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_COMMON_GR_GET_PURCHASING_PARAM] @USER_ID VARCHAR(20) = 'AGAN'
	,@PROCESS_ID BIGINT = '202009230021'
	,@ri_v_supp_cd VARCHAR(100)
	,@ri_d_ref_date DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MODULE_ID VARCHAR(50) = '4'
		,@FUNCTION_ID VARCHAR(100) = '42005'
		,@IsError CHAR(1) = 'N'
		,@CURRDT DATE = CAST(GETDATE() AS DATE)
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_COMMON_GR_GET_PURCHASING_PARAM'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@RES INT = 0;

	BEGIN TRY
		DECLARE @ro_v_supp_name VARCHAR(100) = ''
			,@ro_v_calculation_schema_cd VARCHAR(100) = ''
			,@ro_v_curr VARCHAR(100) = ''
			,@ro_v_payment_term VARCHAR(100) = ''
			,@ro_v_payment_method VARCHAR(100) = ''
			,@l_v_supp_nm VARCHAR(40) = ''
			,@l_v_curr VARCHAR(40) = ''
			,@l_v_payment_term_cd VARCHAR(40) = ''
			,@l_v_payment_method_cd VARCHAR(40) = ''
			,@l_v_supp_schema_cd VARCHAR(40) = ''
			,@l_n_length INT = 0
			,@l_n_supp_len INT = 0
			,@l_n_null INT = 0
			,@l_v_val_tmp VARCHAR(50)
			,@l_v_val VARCHAR(100)
			,@l_n_count INT = 0;

		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'GET Purchasing Parameters'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		-- Check Input mandatory Parameter Null
		IF ISNULL(@ri_v_supp_cd, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Supplier Code';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Supplier Code';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF ISNULL(@ri_d_ref_date, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Reference Date';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Reference Date';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END;
		END;

		IF @l_n_null > 0
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00003ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,@l_v_val

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00003ERR'
				,'ERR'
				,'MPCS00003ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ' : Mandatory Checking'
				,@USER_ID
				,GETDATE()
		END;

		-- Check Length of Each input parameter
		-- Get Length SUPP_CD from table TB_M_SUPPLIER
		IF LEN(@ri_v_supp_cd) > 10
		BEGIN
			SET @IsError = 'Y';
			SET @PARAM1 = 'Invalid Length of Supplier Code,  the Length should be between 1 and 10 characters.';

			EXEC dbo.CommonGetMessage 'MSPT00001ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,@PARAM1

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00001ERR'
				,'ERR'
				,'MSPT00001ERR : ' + @MSG_TXT
				,@LOG_LOCATION + ' : Length Checking'
				,@USER_ID
				,GETDATE()
		END;

		IF @IsError <> 'Y'
		BEGIN
			-- Get Purchasing Parameters in table TB_M_SUPPLIER
			SELECT @l_v_supp_nm = SUPP_NAME
				,@l_v_curr = ORD_CURR
				,@l_v_payment_term_cd = PAYMENT_TERM_CD
				,@l_v_payment_method_cd = PAYMENT_METHOD_CD
			FROM TB_M_SUPPLIER_ICS
			WHERE SUPP_CD = @ri_v_supp_cd
				AND ISNULL(DELETION_FLAG, 'N') = 'N';

			IF ISNULL(@l_v_supp_nm, '') <> ''
			BEGIN
				SET @ro_v_supp_name = @l_v_supp_nm;
				SET @ro_v_curr = @l_v_curr;
				SET @ro_v_payment_term = @l_v_payment_term_cd;
				SET @ro_v_payment_method = @l_v_payment_method_cd;
			END
			ELSE
			BEGIN
				-- No data found exception for get data in TB_M_SUPPLIER
				-- Check Deletion Flag Suppier Code 'Y'
				SELECT @l_n_count = COUNT(1)
				FROM TB_M_SUPPLIER_ICS
				WHERE SUPP_CD = @ri_v_supp_cd
					AND DELETION_FLAG = 'Y';

				IF @l_n_count > 0
				BEGIN
					SET @IsError = 'Y';
					SET @PARAM1 = 'Supplier ' + @ri_v_supp_cd + ' has found but currently marked as deleted';

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()
				END
				ELSE
				BEGIN
					SET @IsError = 'Y';
					SET @PARAM1 = 'Supplier ' + @ri_v_supp_cd + ' not found';

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()
				END;
			END
		END

		IF @IsError <> 'Y'
		BEGIN
			-- Get Purchasing Parameters in table TB_M_SUPPLIER_SCHEMA
			SELECT @l_v_supp_schema_cd = CALCULATION_SCHEMA_CD
			FROM TB_M_SUPPLIER_SCHEMA
			WHERE SUPP_CD = @ri_v_supp_cd
				AND CONVERT(VARCHAR, VALID_DT_FR, 112) <= CONVERT(VARCHAR, @ri_d_ref_date, 112)
				AND ISNULL(CONVERT(VARCHAR, VALID_DT_TO, 112), '99991231') >= CONVERT(VARCHAR, @ri_d_ref_date, 112);

			SET @ro_v_calculation_schema_cd = @l_v_supp_schema_cd;

			IF ISNULL(@ro_v_calculation_schema_cd, '') = ''
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Supplier ' + @ri_v_supp_cd + ' scheme is not found';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()
			END
		END

		IF @IsError = 'N'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET Purchasing Parameters'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET Purchasing Parameters'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		SELECT @IsError l_n_status
			,@ro_v_supp_name ro_v_supp_name
			,@ro_v_curr ro_v_curr
			,@ro_v_payment_term ro_v_payment_term
			,@ro_v_payment_method ro_v_payment_method
			,@l_v_supp_schema_cd ro_v_calculation_schema_cd
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		SELECT 'Y' l_n_status
			,'' ro_v_supp_name
			,'' ro_v_curr
			,'' ro_v_payment_term
			,'' ro_v_payment_method
			,'' ro_v_calculation_schema_cd
	END CATCH
END
