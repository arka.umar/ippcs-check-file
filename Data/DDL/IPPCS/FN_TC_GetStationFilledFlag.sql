CREATE FUNCTION [dbo].[FN_TC_GetStationFilledFlag] (
	@ri_v_dock_cd		varchar(3),
	@ri_v_station		varchar(3)	
)
RETURNS bit
AS
BEGIN
	if exists(
		select 1
		from TB_R_DELIVERY_CTL_D dcd_1
		where 1 = 1
			and dcd_1.ARRIVAL_ACTUAL_DT is not null
			and dcd_1.ARRIVAL_ACTUAL_DT <= getdate()
			and dcd_1.DEPARTURE_ACTUAL_DT is null
			and dcd_1.DOCK_CD = @ri_v_dock_cd
			--and dcd_1.DOCK_CD = '4N'
			and dcd_1.STATION = @ri_v_station
			--and dcd_1.STATION = '1'
	)
	begin
		return 1
	end

	return 0
END

