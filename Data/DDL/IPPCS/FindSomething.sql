CREATE PROCEDURE [dbo].[FindSomething]
	@Keyword VARCHAR(MAX)
	--@ExcludedWord VARCHAR(MAX) = ''
AS
BEGIN
	DECLARE @p_Keyword VARCHAR(MAX) = @Keyword
	--DECLARE @p_ExcludedWord VARCHAR(MAX) = @ExcludedWord
	DECLARE @sql NVARCHAR(MAX) = ''

	SELECT * FROM (
		SELECT  '[' + ISNULL(s.srvname, '') + '][' + CASE WHEN ISNULL(j.[enabled], 0) = 1 THEN 'Enabled' ELSE 'Disabled' END + '] ' + 
				ISNULL(j.name, '') + ' Step ' + CONVERT(VARCHAR(10), ISNULL(js.step_id, 0)) as [OBJECT_NAME],
				'Job' as [OBJECT_TYPE]
				--js.command as JOB_COMMAND
		FROM    msdb.dbo.sysjobs j
		JOIN    msdb.dbo.sysjobsteps js
				ON      js.job_id = j.job_id 
		JOIN    master.dbo.sysservers s
				ON      s.srvid = j.originating_server_id
		WHERE   CHARINDEX(@p_Keyword, js.command) > 0
				--AND ((CHARINDEX(@p_ExcludedWord, js.command) = 0 
				--		OR ISNULL(@p_ExcludedWord, '') = '' ))

		UNION ALL

		SELECT DISTINCT 
			sysobjects.name AS [OBJECT_NAME] ,
			case 
				 when sysobjects.xtype = 'P' then 'Stored Procedure'
				 when sysobjects.xtype = 'TF' then 'Function'
				 when sysobjects.xtype = 'IF' then 'Function'
				 when sysobjects.xtype = 'TR' then 'Trigger'
				 when sysobjects.xtype = 'V' then 'View'
				 when sysobjects.xtype = 'FN' then 'Function'
				 when sysobjects.xtype = 'U' then 'Table'
				 when sysobjects.xtype = 'FK' then 'Foreign Key'
				 when sysobjects.xtype = 'PK' then 'Primary Key'
			end as [OBJECT_TYPE]
		FROM sysobjects, syscomments
		WHERE sysobjects.id = syscomments.id
			AND sysobjects.type in ('P','TF','TR','V', 'FN', 'IF', 'U', 'PK')
			AND sysobjects.category = 0
			AND CHARINDEX(@p_Keyword, syscomments.text) > 0
				OR CHARINDEX(@p_Keyword, sysobjects.name) > 0
			--AND ((CHARINDEX(@p_ExcludedWord, syscomments.text) = 0 
			--	OR ISNULL(@p_ExcludedWord, '') = '' ))
	)TBL1 ORDER BY [OBJECT_TYPE]
END

