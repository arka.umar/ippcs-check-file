CREATE PROCEDURE [dbo].[CheckForeign]
	@table_name VARCHAR(100) = ''
AS 
BEGIN
	DECLARE @p_table_name VARCHAR(100) = @table_name

	SELECT DISTINCT
		t_parent.name [table_name],
		--t.object_id,
		--fk.name,
		--fk.object_id,
		--fk.parent_object_id,
		--fk.referenced_object_id,
		--fkc.constraint_column_id,
		--c.name,
		--fkc.parent_column_id,
		--c_parent.name [column_name],
		t_reference.name [reference_table]
		--fkc.referenced_column_id,
		--c_reference.name [reference_column]
	FROM sys.tables t_parent
		LEFT JOIN sys.foreign_keys fk
			ON fk.parent_object_id = t_parent.object_id
		--LEFT JOIN sys.foreign_key_columns fkc
		--	ON fkc.constraint_object_id = fk.object_id
		--	   AND fkc.parent_object_id = fk.parent_object_id
		--	   AND fkc.referenced_object_id = fk.referenced_object_id
		--LEFT JOIN sys.columns c
		--	ON c.object_id = t_parent.object_id
		--	   AND c.column_id = fkc.constraint_object_id
		--LEFT JOIN sys.columns c_parent
		--	ON c_parent.object_id = t_parent.object_id
		--	   AND c_parent.column_id = fkc.parent_column_id
		--LEFT JOIN sys.columns c_reference
		--	ON c_reference.object_id = t_parent.object_id
		--	   AND  c_reference.column_id = fkc.referenced_column_id
		LEFT JOIN sys.tables t_reference
			ON t_reference.object_id = fk.referenced_object_id
	WHERE fk.object_id IS NOT NULL --and t_parent.name NOT LIKE '%_M_%'
		  AND (t_parent.name = @p_table_name OR ISNULL(@p_table_name, '') = '')
END

