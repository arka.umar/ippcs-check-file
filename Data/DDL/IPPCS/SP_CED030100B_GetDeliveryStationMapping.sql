-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetDeliveryStationMapping]
AS
BEGIN
	SELECT [DOOR_CD_TLMS]
		,[DOCK_CD]
		,[PHYSICAL_DOCK]
		,CONVERT(VARCHAR, [PHYSICAL_STATION]) [PHYSICAL_STATION]
		,CONVERT(VARCHAR, [USED_FLAG]) [USED_FLAG]
		,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
	FROM [dbo].[TB_T_CED_DELIVERY_STATION_MAPPING]
END

