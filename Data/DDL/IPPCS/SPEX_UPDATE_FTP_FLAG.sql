-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SPEX_UPDATE_FTP_FLAG]
	
AS
BEGIN

	
	DECLARE @l_t_manifest_spex AS TABLE(MANIFEST_NO VARCHAR(20))
	DECLARE @l_t_manifest_not_exists AS TABLE(MANIFEST_NO VARCHAR(20))

	INSERT INTO @l_t_manifest_spex
	SELECT distinct manifest_No FROM TB_R_SPEX_KANBAN 


	insert into @l_t_manifest_not_exists
	SELECT MANIFEST_NO FROM TB_R_DAILY_ORDER_MANIFEST m
	--SET FTP_FLAG=0
	WHERE ORDER_TYPE='3' AND DOCK_CD='S1' AND ISNULL(CANCEL_FLAG,'0') = 0
	AND NOT EXISTS (SELECT '' FROM @l_t_manifest_spex k WHERE K.MANIFEST_NO = M.MANIFEST_NO)

	select * from @l_t_manifest_not_exists


	UPDATE OM
	SET FTP_FLAG = 0
	FROM TB_R_DAILY_ORDER_MANIFEST OM JOIN @l_t_manifest_not_exists e ON OM.MANIFEST_NO = e.MANIFEST_NO

END

