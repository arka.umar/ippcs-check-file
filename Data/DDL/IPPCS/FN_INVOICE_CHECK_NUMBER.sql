-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2020-09-30
-- Description:	This function for check validity number
-- =============================================
CREATE FUNCTION [dbo].[FN_INVOICE_CHECK_NUMBER] (
	@ri_n_number INT
	)
RETURNS INT
AS
BEGIN
	DECLARE @Result INT = 1;

	IF ISNUMERIC(@ri_n_number) = 1
	BEGIN
		SET @Result = 1;
	END
	ELSE
	BEGIN
		SET @Result = 0;
	END

	RETURN @Result;
END
