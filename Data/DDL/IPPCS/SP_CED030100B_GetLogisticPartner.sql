-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetLogisticPartner]
AS
BEGIN
		SELECT [LOG_PARTNER_CD]
		  ,ISNULL(CONVERT(VARCHAR, [TCYMDFROM], 121), '') [TCYMDFROM]
		  ,[LOG_PARTNER_NAME]
		  ,ISNULL(CONVERT(VARCHAR, [ACTIVE_FLAG]), '') [ACTIVE_FLAG]
		  ,ISNULL([LOG_PARTNER_ADDRESS], '') [LOG_PARTNER_ADDRESS]
		  ,ISNULL([POSTAL_CD], '') [POSTAL_CD]
		  ,ISNULL([CITY], '') [CITY]
		  ,ISNULL([ATTENTION], '') [ATTENTION]
		  ,ISNULL([PHONE_NUMBER], '') [PHONE_NUMBER]
		  ,ISNULL([FAX_NUMBER], '') [FAX_NUMBER]
		  ,ISNULL([NPWP], '') [NPWP]
		  ,[DELETION_FLAG]
		  ,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
	  FROM [dbo].[TB_T_CED_LOGISTIC_PARTNER]
END

