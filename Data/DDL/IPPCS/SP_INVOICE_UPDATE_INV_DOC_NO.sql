-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	19-Sept-2020
-- Description	:	Invoice Update
-- Update	: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_INVOICE_UPDATE_INV_DOC_NO] @USER_ID VARCHAR(20) = 'agan'
	,@PROCESS_ID BIGINT = '202009070019'
	,@SupplierInvoiceNo VARCHAR(MAX)
	,@SupplierCode VARCHAR(MAX)
AS
BEGIN
	DECLARE @IsError CHAR(1) = 'N'
	DECLARE @CURRDT DATE = CAST(GETDATE() AS DATE)
	DECLARE @FUNCTION_ID AS VARCHAR(MAX) = '53002'
		,@MODULE_ID AS VARCHAR(MAX) = '5'
		,@FUNTION_NM AS VARCHAR(MAX) = 'SP Invoice Update'
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP Invoice Update'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)

	SET NOCOUNT ON

	BEGIN TRY
		DECLARE @l_v_inv_no BIGINT;

		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Invoice Update'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		--* Step 1. Get Latest Invoice No *--
		SELECT @l_v_inv_no = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'INV_DOC_NO';

		-- catatan perlu sync
		SET @l_v_inv_no = @l_v_inv_no + 1;

		--* Step 2. Update Inv No for All Temporary Tables *--
		-- Update temp invoice header
		UPDATE A
		SET INV_NO = CONVERT(VARCHAR, @l_v_inv_no)
		FROM TB_T_INVOICE_H A
		WHERE PROCESS_ID = @PROCESS_ID
			AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

		-- Update temp invoice detail
		UPDATE A
		SET INV_NO = CONVERT(VARCHAR, @l_v_inv_no)
		FROM TB_T_INVOICE_D A
		WHERE PROCESS_ID = @PROCESS_ID
			AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

		-- Update temp invoice witholding tax
		UPDATE A
		SET INV_NO = CONVERT(VARCHAR, @l_v_inv_no)
		FROM TB_T_INV_WITHOLDING_TAX A
		WHERE PROCESS_ID = @PROCESS_ID
			AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

		-- Update temp invoice GL Account
		UPDATE A
		SET INV_NO = CONVERT(VARCHAR, @l_v_inv_no)
		FROM TB_T_INV_GL_ACCOUNT A
		WHERE PROCESS_ID = @PROCESS_ID
			AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

		-- Update temp invoice tax
		UPDATE A
		SET INV_NO = @l_v_inv_no
		FROM TB_T_INV_TAX A
		WHERE PROCESS_ID = @PROCESS_ID
			AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

		-- Update invoice doc no in table system master
		UPDATE A
		SET SYSTEM_VALUE = CONVERT(VARCHAR, @l_v_inv_no)
			,CHANGED_BY = @USER_ID
			,CHANGED_DT = GETDATE()
		FROM TB_M_SYSTEM A
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'INV_DOC_NO';

		EXEC dbo.CommonGetMessage 'MSPT00005INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Invoice Update'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MSPT00005INF'
			,'INF'
			,'MSPT00005INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		RETURN 0;
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		RETURN 1;
	END CATCH
END
