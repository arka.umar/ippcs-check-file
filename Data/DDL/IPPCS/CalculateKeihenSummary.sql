CREATE PROCEDURE [dbo].[CalculateKeihenSummary]
	@process_id varchar(MAX),
	@userid varchar(20)
AS
BEGIN
	DECLARE @p_userid VARCHAR(20) = @userid
	DECLARE @p_createddt DATETIME = GETDATE()
	DECLARE @p_process_id bigint = @process_id
	DECLARE @p_packmonth varchar(6) = ''
	DECLARE @uploaded_packmonth varchar(6) = ''
	DECLARE @p_error_msg VARCHAR(max) = ''

	IF OBJECT_ID('tempdb..#keihen') IS NOT NULL 
		DROP TABLE #keihen

	IF OBJECT_ID('tempdb..#keihen_summary') IS NOT NULL 
		DROP TABLE #keihen_summary

	IF OBJECT_ID('tempdb..#keihen_old') IS NOT NULL 
		DROP TABLE #keihen_old

	BEGIN TRY
		SELECT @uploaded_packmonth = PACK_MONTH FROM (SELECT TOP 1 PACK_MONTH FROM TB_T_KEIHEN_UPLOAD WHERE PROCESS_ID = @p_process_id) A

		IF(EXISTS(SELECT 'x' FROM TB_R_KEIHEN WHERE PACK_MONTH = @uploaded_packmonth))
		BEGIN
			SELECT * INTO #keihen_old FROM TB_R_KEIHEN WHERE PACK_MONTH = @uploaded_packmonth	
			DELETE FROM TB_R_KEIHEN WHERE PACK_MONTH = @uploaded_packmonth	
		END

		insert into TB_R_KEIHEN
			(
			   PACK_MONTH,
			   PART_NO,
			   DOCK_CD,
			   SUPPLIER_CD,
			   S_PLANT_CD,
			   CAR_CD,
			   N_1_QTY_SIGN,
			   N_1_QTY_GAP,
			   STATUS_CD,
			   APPROVED_BY,
			   APPROVED_DT,
			   PROCESS_ID,
			   CREATED_BY,
			   CREATED_DT
			)
		select
			PACK_MONTH,
			PART_NO,
			DOCK_CD,
			SUPPLIER_CD,
			S_PLANT_CD,
			CAR_CD,
			N_1_QTY_SIGN,
			N_1_QTY_GAP,
			'Received',
			NULL,
			NULL,
			@p_process_id,
			@p_userid,
			@p_createddt
		from TB_T_KEIHEN_UPLOAD
		where PROCESS_ID = @p_process_id

		IF(EXISTS(SELECT 'x' FROM TB_R_KEIHEN WHERE PROCESS_ID = @p_process_id))
		BEGIN
			--select @p_packmonth = MAX(PACK_MONTH) FROM TB_R_KEIHEN WHERE PROCESS_ID = @p_process_id
			select @p_packmonth = MAX(PACK_MONTH) FROM TB_R_LPOP

			IF(EXISTS(SELECT 'x' FROM TB_R_KEIHEN_SUMMARY 
					  WHERE PACK_MONTH = CONVERT(VARCHAR(6), DATEADD(MONTH, 1, CONVERT(DATE, @p_packmonth + '01')), 112)))
			BEGIN
				SELECT * INTO #keihen_summary FROM TB_R_KEIHEN_SUMMARY WHERE PACK_MONTH = CONVERT(VARCHAR(6), DATEADD(MONTH, 1, CONVERT(DATE, @p_packmonth + '01')), 112)
				DELETE FROM TB_R_KEIHEN_SUMMARY WHERE PACK_MONTH = CONVERT(VARCHAR(6), DATEADD(MONTH, 1, CONVERT(DATE, @p_packmonth + '01')), 112)
			END

			insert into TB_R_KEIHEN_SUMMARY
				(
					PACK_MONTH,
					PART_NO,
					DOCK_CD,
					SUPPLIER_CD,
					S_PLANT_CD,
					CAR_CD,
					N_1_ORIGINAL,
					CALCULATION_PROCESS_ID,
					CREATED_BY,
					CREATED_DT,
					PART_NAME
				)
			select
				CONVERT(VARCHAR(6), DATEADD(MONTH, 1, CONVERT(DATE, PACK_MONTH + '01')), 112), 
				PART_NO, 
				DOCK_CD, 
				SUPPLIER_CD, 
				S_PLANT_CD, 
				STUFF(
					(select distinct ', ' + C.CAR_CD
						from TB_R_LPOP C
						where 
						C.PACK_MONTH = Qry.PACK_MONTH
						and C.PART_NO = Qry.PART_NO
						and C.DOCK_CD = Qry.DOCK_CD
						and C.SUPPLIER_CD = Qry.SUPPLIER_CD
						and C.S_PLANT_CD = Qry.S_PLANT_CD
						and C.VERS = Qry.VERS
					for xml path('')), 1, 1, '') as 'CAR_CD', 
				SUM(CONVERT(INT, N_1_VOLUME)) as N_1_ORIGINAL,
				@p_process_id,
				@p_userid,
				@p_createddt,
				MAX(Qry.PART_NAME)
			from tb_r_lpop Qry
			where PACK_MONTH = @p_packmonth 
					AND VERS = 'F'
					AND SRC IN ('1', '3')
			group by PACK_MONTH, PART_NO, DOCK_CD, SUPPLIER_CD, S_PLANT_CD, VERS
			order by PART_NO

			SELECT
				ROW_NUMBER() OVER(ORDER BY K.PACK_MONTH) as [N],
				K.PACK_MONTH,
				K.PART_NO,
				K.DOCK_CD,
				K.SUPPLIER_CD,
				K.S_PLANT_CD,
				SUM(CASE WHEN K.N_1_QTY_SIGN = '-' 
					THEN CONVERT(INT, K.N_1_QTY_SIGN + ISNULL(K.N_1_QTY_GAP, '0')) 
					ELSE CONVERT(INT, ISNULL(K.N_1_QTY_GAP, '0'))
				END) as GAP,
				STUFF(
				(select distinct ', ' + C.CAR_CD
					from TB_R_KEIHEN C
					where 
					C.PACK_MONTH = K.PACK_MONTH
					and C.PART_NO = K.PART_NO
					and C.DOCK_CD = K.DOCK_CD
					and C.SUPPLIER_CD = K.SUPPLIER_CD
					and C.S_PLANT_CD = K.S_PLANT_CD
				for xml path('')), 1, 1, '') as 'CAR_CD',
				@p_process_id as PROCESS_ID
			into #keihen
			from TB_R_KEIHEN K
			WHERE K.PROCESS_ID = @p_process_id
			GROUP BY
				K.PACK_MONTH,
				K.PART_NO,
				K.DOCK_CD,
				K.SUPPLIER_CD,
				K.S_PLANT_CD

			Update KS SET
				KS.N_1_NEW = KS.N_1_ORIGINAL + K.GAP,
				KS.FLUCTUATION =  CASE WHEN KS.N_1_ORIGINAL > 0 THEN
						CONVERT(DECIMAL(30, 2), (CONVERT(DECIMAL(30, 2), K.GAP) / CONVERT(DECIMAL(30, 2), KS.N_1_ORIGINAL)) * 100)
					ELSE NULL END,
				--KS.CAR_CD = CASE WHEN CHARINDEX(K.CAR_CD, KS.CAR_CD) <= 0 THEN KS.CAR_CD + ',' + K.CAR_CD ELSE KS.CAR_CD END
				KS.CAR_CD = dbo.ConcateCarCode(KS.CAR_CD, K.CAR_CD)
			from TB_R_KEIHEN_SUMMARY KS
				JOIN #keihen K
					ON K.PACK_MONTH = KS.PACK_MONTH
					   AND K.PART_NO = KS.PART_NO
					   AND K.DOCK_CD = KS.DOCK_CD
					   AND K.SUPPLIER_CD = KS.SUPPLIER_CD
					   AND K.S_PLANT_CD = KS.S_PLANT_CD
			WHERE KS.CALCULATION_PROCESS_ID = @p_process_id 
		END
		ELSE
		BEGIN
			SET @p_error_msg = 'Keihen Data Does Not Exists'
			RAISERROR(@p_error_msg, 16, 1)
		END

		DELETE FROM TB_T_KEIHEN_UPLOAD where PROCESS_ID = @p_process_id
	END TRY
	BEGIN CATCH
		DELETE FROM TB_R_KEIHEN where PROCESS_ID = @p_process_id
		DELETE FROM TB_R_KEIHEN_SUMMARY where CALCULATION_PROCESS_ID = @p_process_id
		DELETE FROM TB_T_KEIHEN_UPLOAD where PROCESS_ID = @p_process_id
		
		IF OBJECT_ID('tempdb..#keihen_old') IS NOT NULL
		BEGIN
			IF(EXISTS(SELECT 'x' FROM #keihen_old WHERE PACK_MONTH = @uploaded_packmonth))
			BEGIN
				IF(NOT EXISTS(SELECT 'x' FROM TB_R_KEIHEN WHERE PACK_MONTH = @uploaded_packmonth))
				BEGIN
					INSERT INTO TB_R_KEIHEN
					SELECT * FROM #keihen_old
				END
			END
		END

		IF OBJECT_ID('tempdb..#keihen_summary') IS NOT NULL 
		BEGIN
			IF(EXISTS(SELECT 'x' FROM #keihen_summary WHERE PACK_MONTH = CONVERT(VARCHAR(6), DATEADD(MONTH, 1, CONVERT(DATE, @p_packmonth + '01')), 112)))
			BEGIN
				IF(NOT EXISTS(SELECT 'x' FROM TB_R_KEIHEN_SUMMARY WHERE PACK_MONTH = CONVERT(VARCHAR(6), DATEADD(MONTH, 1, CONVERT(DATE, @p_packmonth + '01')), 112)))
				BEGIN
					INSERT INTO TB_R_KEIHEN_SUMMARY
					SELECT * FROM #keihen_summary
				END
			END
		END

		SET @p_error_msg = ERROR_MESSAGE()
		RAISERROR(@p_error_msg, 16, 1)
	END CATCH
END

