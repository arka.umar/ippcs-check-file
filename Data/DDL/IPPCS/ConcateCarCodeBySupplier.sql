CREATE FUNCTION [dbo].[ConcateCarCodeBySupplier] (@PACK_MONTH VARCHAR(6), @SUPP_CD VARCHAR(4), @SUPP_PLANT VARCHAR(2))
RETURNS VARCHAR(400)
AS
BEGIN
DECLARE @CAR VARCHAR(MAX) = ''
DECLARE @OUT VARCHAR(MAX) = ''

SELECT @CAR = STUFF(
(select distinct ', ' + C.CAR_CD
	from TB_R_KEIHEN_FEEDBACK C
	where 
	C.PACK_MONTH = @PACK_MONTH
	and C.SUPPLIER_CD = @SUPP_CD
	and C.S_PLANT_CD = @SUPP_PLANT
for xml path('')), 1, 1, '')

SELECT distinct @OUT = STUFF(
		(select distinct ', ' + LTRIM(RTRIM(s)) 
			from dbo.fnSplitString(@CAR, ',') f2
		for xml path('')), 1, 1, '')
FROM dbo.fnSplitString(@CAR, ',') f1

RETURN @OUT

END

