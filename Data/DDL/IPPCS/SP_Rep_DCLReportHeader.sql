CREATE PROCEDURE [dbo].[SP_Rep_DCLReportHeader] 
	@PickupDateFrom varchar (10),
	@PickupDateTo varchar (10),
	@LPCode varchar(100),
	@Route varchar(100),
	@SupplierCode varchar(100),
	@DockCode varchar(100),
	@DeliveryStatus varchar(20),
	@DeliveryAchievement varchar(50)
AS
BEGIN
	Declare @sql nvarchar(max)
	
	CREATE TABLE #tmpSplitString
	( 
		column1 VARCHAR(10)
	)

	INSERT  INTO #tmpSplitString(column1)
	EXEC dbo.SplitString @LPCode, ''

	INSERT  INTO #tmpSplitString(column1)
	EXEC dbo.SplitString @Route, ''

	INSERT  INTO #tmpSplitString(column1)
	EXEC dbo.SplitString @SupplierCode, ''
  
	INSERT  INTO #tmpSplitString(column1)
	EXEC dbo.SplitString @DockCode, ''	

set @sql = '
  SELECT DISTINCT h.PICKUP_DT
	, r.SUPPLIER_NAME
  FROM
    TB_R_DELIVERY_CTL_SUPPLIERDOCK r
    INNER JOIN TB_R_DELIVERY_CTL_H h
      ON h.DELIVERY_NO = r.DELIVERY_NO
  WHERE
    (h.PICKUP_DT BETWEEN ''' + @PickupDateFrom + ''' AND ''' + @PickupDateTo + ''')
    AND ((h.DELIVERY_STS = ''' + @DeliveryStatus + '''
    AND isnull(''' + @DeliveryStatus + ''', '''') <> '''')
    OR (isnull(''' + @DeliveryStatus + ''', '''') = ''''))
    AND ((h.DELIVERY_ACHIEVEMENT_STA = ''' + @DeliveryAchievement + '''
    AND isnull(''' + @DeliveryAchievement + ''', '''') <> '''')
    OR (isnull(''' + @DeliveryAchievement + ''', '''') = ''''))
'

  IF ( @LPCode <> '' ) 
		BEGIN
			SET @sql = @sql + ' AND (r.LP_CD IN (SELECT column1 FROM #tmpSplitString))'
		END

  IF ( @Route <> '' ) 
		BEGIN
			SET @sql = @sql + ' AND (r.ROUTE IN (SELECT column1 FROM #tmpSplitString))'
		END

 IF ( @SupplierCode <> '' ) 
		BEGIN
			SET @sql = @sql + ' AND (r.SUPPLIER_CD + ''-'' + r.SUPP_PLANT_CD) IN (SELECT column1 FROM #tmpSplitString)'
		END
		
  
  IF ( @DockCode <> '' ) 
		BEGIN
			SET @sql = @sql + ' AND (r.DOCK_CD IN (SELECT column1 FROM #tmpSplitString))'
		END
	
	--print (@sql)
	exec (@sql)
	
     DROP TABLE #tmpSplitString
     DROP TABLE #tmpSplitString2
 
END

