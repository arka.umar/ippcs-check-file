-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	05-Oct-2020
-- Description	:	Preparation Goods Receipt to ICS
-- Update		: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_COMMON_GR_GET_PO_BY_SUPPLIER] @USER_ID VARCHAR(20) = 'AGAN'
	,@PROCESS_ID BIGINT = '202009230021'
	,@ri_v_supp_cd VARCHAR(50) = 'AGAN'
	,@ri_d_prod_month DATE = '202009230021'
	,@r_v_document_type VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MODULE_ID VARCHAR(50) = '4'
		,@FUNCTION_ID VARCHAR(100) = '42005'
		,@IsError CHAR(1) = 'N'
		,@CURRDT DATE = CAST(GETDATE() AS DATE)
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP_COMMON_GR_GET_PO_BY_SUPPLIER'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)
		,@RES INT = 0;

	IF OBJECT_ID('tempdb..#lCur_GetPo') IS NOT NULL
	BEGIN
		DROP TABLE #lCur_GetPo
	END;

	CREATE TABLE #lCur_GetPo (
		PO_NO VARCHAR(1000)
		,SUPP_CD VARCHAR(1000)
		,PROD_MONTH VARCHAR(1000)
		,STATUS INT
		)

	BEGIN TRY
		DECLARE @l_n_count INT
			,@l_v_po_no VARCHAR(2000)
			,@l_n_length INT
			,@l_n_supp_len INT
			,@l_v_val VARCHAR(100) = ''
			,@l_v_val_tmp VARCHAR(50) = ''
			,@l_n_null INT = 0
			,@l_n_rec_po_no INT
			,@vDocType VARCHAR(100)
			,@l_v_doctype VARCHAR(100);

		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'GET PO By Supplier'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		-- Check Input mandatory Parameter Null
		IF ISNULL(@ri_v_supp_cd, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Supplier Code';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Supplier Code';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END
		END

		IF ISNULL(@ri_d_prod_month, '') = ''
		BEGIN
			IF ISNULL(@l_v_val, '') = ''
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val = 'Production Month';
			END
			ELSE
			BEGIN
				SET @l_n_null = @l_n_null + 1;
				SET @l_v_val_tmp = ', Production Month';
				SET @l_v_val = @l_v_val + @l_v_val_tmp;
			END
		END

		IF @l_n_null > 0
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00003ERR'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,@l_v_val

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00003ERR'
				,'ERR'
				,'MPCS00003ERR : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			INSERT INTO #lCur_GetPo
			SELECT '' PO_NO
				,'' SUPP_CD
				,'' PROD_MONTH
				,1 STATUS
		END;

		IF @IsError <> 'Y'
		BEGIN
			-- Check Length of Each input parameter
			IF LEN(@ri_v_supp_cd) > 6
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invalid Length of Supplier Code,  the Length should be between 1 and 6 characters.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()

				INSERT INTO #lCur_GetPo
				SELECT '' PO_NO
					,'' SUPP_CD
					,'' PROD_MONTH
					,1 STATUS
			END;
		END

		IF @IsError <> 'Y'
		BEGIN
			/* Check Document Type on Input Parameter */
			SELECT @l_v_doctype = SYSTEM_VALUE
			FROM TB_M_SYSTEM
			WHERE FUNCTION_ID = 'COMMON_PO'
				AND SYSTEM_CD = 'PO_LOCAL_REGULER_DOC_TYPE';

			IF (ISNULL(@l_v_doctype, '') = '')
			BEGIN
				SET @IsError = 'Y';

				EXEC dbo.CommonGetMessage 'MPCS00004INF'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,'(PO_LOCAL_REGULER_DOC_TYPE)'

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MPCS00004INF'
					,'INF'
					,'MPCS00004INF : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()

				INSERT INTO #lCur_GetPo
				SELECT '' PO_NO
					,'' SUPP_CD
					,'' PROD_MONTH
					,1 STATUS
			END
			ELSE
			BEGIN
				IF ISNULL(@r_v_document_type, '') = ''
				BEGIN
					SET @vDocType = @l_v_doctype;
				END
				ELSE
				BEGIN
					SET @vDocType = @r_v_document_type;
				END;
			END
		END

		IF @IsError <> 'Y'
		BEGIN
			/* Count the data to check whether is equal to 0 or > 0 */
			SELECT @l_n_count = COUNT(1)
			FROM TB_R_PO_H
			WHERE ISNULL(DELETION_FLAG, '0') = '0'
				AND SUPPLIER_CD = @ri_v_supp_cd
				AND PRODUCTION_MONTH = CONVERT(VARCHAR(6), @ri_d_prod_month, 112)

			--AND    doc_type = @vDocType 
			-- count value for check whether the data selected is NULL
			IF @l_n_count = 0
			BEGIN
				SELECT @l_n_count = COUNT(1)
				FROM TB_R_PO_H
				WHERE ISNULL(DELETION_FLAG, '0') = '1'
					AND SUPPLIER_CD = @ri_v_supp_cd
					AND PRODUCTION_MONTH = CONVERT(VARCHAR(6), @ri_d_prod_month, 112)

				--AND    doc_type = @vDocType 
				IF @l_n_count = 0
				BEGIN
					SET @IsError = 'Y';
					SET @PARAM1 = 'PO Data doesnt exist for passing Supplier Code: ' + @ri_v_supp_cd + ' and Production Month: ' + CONVERT(VARCHAR, @ri_d_prod_month, 112);

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()

					INSERT INTO #lCur_GetPo
					SELECT '' PO_NO
						,'' SUPP_CD
						,'' PROD_MONTH
						,1 STATUS
				END
				ELSE
				BEGIN
					SELECT DISTINCT @l_v_po_no = substring((
								SELECT ', ' + U1.po_no AS [text()]
								FROM tb_r_po_h U1
								WHERE ISNULL(U1.DELETION_FLAG, '0') = '1'
									AND U1.SUPPLIER_CD = @ri_v_supp_cd
									AND U1.PRODUCTION_MONTH = CONVERT(VARCHAR, @ri_d_prod_month, 112)
								ORDER BY U1.po_no
								FOR XML PATH('')
								), 2, 1000)
					FROM tb_r_po_h U2
					WHERE ISNULL(U2.DELETION_FLAG, '0') = '1'
						AND U2.SUPPLIER_CD = @ri_v_supp_cd
						AND U2.PRODUCTION_MONTH = CONVERT(VARCHAR, @ri_d_prod_month, 112)

					SET @IsError = 'Y';
					SET @PARAM1 = 'PO Number : ' + @l_v_po_no + ' which have Supplier Code: ' + @ri_v_supp_cd + ' and Production Month: ' + CONVERT(VARCHAR, @ri_d_prod_month, 112) + ' are marked as deleted';

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()

					INSERT INTO #lCur_GetPo
					SELECT '' PO_NO
						,'' SUPP_CD
						,'' PROD_MONTH
						,1 STATUS
				END
			END
			ELSE
			BEGIN
				-- else the data selected is contain data needed
				-- GET PO BY SUPPLIER
				INSERT INTO #lCur_GetPo
				SELECT PO_NO
					,SUPPLIER_CD
					,PRODUCTION_MONTH
					,0 STATUS
				FROM TB_R_PO_H
				WHERE ISNULL(DELETION_FLAG, '0') = '0'
					AND SUPPLIER_CD = @ri_v_supp_cd
					AND PRODUCTION_MONTH = CONVERT(VARCHAR(6), @ri_d_prod_month, 112)
				--AND    doc_type = @vDocType 
				ORDER BY PO_NO DESC;
			END
		END

		IF @IsError = 'N'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET PO By Supplier'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'GET PO By Supplier'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		SELECT *
		FROM #lCur_GetPo
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		INSERT INTO #lCur_GetPo
		SELECT '' PO_NO
			,'' SUPP_CD
			,'' PROD_MONTH
			,2 STATUS

		SELECT *
		FROM #lCur_GetPo
	END CATCH
END
