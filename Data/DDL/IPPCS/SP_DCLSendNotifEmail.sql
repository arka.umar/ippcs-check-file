CREATE PROCEDURE [dbo].[SP_DCLSendNotifEmail]
@dateFrom varchar(10),
@dateTo varchar(10)
AS
BEGIN

DECLARE @text varchar(max), 
@delivery_no varchar(30),
@route varchar(10),
@rate varchar(10),
@pickup_dt varchar(20),
@download_dt varchar(20),
@download_by varchar(20)

DECLARE @email varchar(1000)
SELECT @email = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_TLMS' AND SYSTEM_CD = 'DCL';

BEGIN TRY
SET @text = 'TLMS Synchronize Notification </br>'
SET @text = @text + 'System found some data with <b>Downloaded</b> status in the given date.'
SET @text = @text + '</br></br>';
SET @text = @text + '<table>
 <tr><td>Delivery No &nbsp&nbsp&nbsp</td>
<td>Route&nbsp&nbsp&nbsp</td>
<td>Rate&nbsp&nbsp&nbsp</td>
<td>Pickup Date&nbsp&nbsp&nbsp</td>
<td>Download Date&nbsp&nbsp&nbsp</td>
<td>Download By&nbsp&nbsp&nbsp</td>
</tr>';

--prepare for get the data
DECLARE data_cursor CURSOR FOR
SELECT 
ISNULL(h.DELIVERY_NO, '-'),
ISNULL(h.ROUTE, '-'),
ISNULL(h.RATE, '-'),
h.PICKUP_DT,
ISNULL(h.DOWNLOAD_DT, 0),
ISNULL(h.DOWNLOAD_BY, '-')
 FROM TB_R_DELIVERY_CTL_H h
WHERE h.PICKUP_DT BETWEEN @dateFrom AND @dateTo
AND h.DELIVERY_STS = 'Downloaded'
AND h.DELIVERY_NO like 'R%'


--read the data
OPEN data_cursor
FETCH NEXT FROM  data_cursor INTO @delivery_no, @route, @rate, @pickup_dt, @download_dt, @download_by

WHILE @@FETCH_STATUS = 0
BEGIN
SET @text = @text + '<tr><td>'+@delivery_no + ' &nbsp&nbsp&nbsp</td><td>'+@route+' </td><td>'+@rate+' </td><td>'+@pickup_dt+' </td><td>'+ @download_dt+ ' &nbsp&nbsp&nbsp</td><td>'+@download_by+'</td></tr>'

FETCH NEXT FROM data_cursor INTO @delivery_no, @route, @rate,@pickup_dt,@download_dt, @download_by
END

CLOSE data_cursor
DEALLOCATE data_cursor


SET @text = @text + '</table></br></br></br>Regards.</br>'
SET @text = @text + 'IPPCS Admin'


EXEC msdb.dbo.sp_send_dbmail
@profile_name = 'IPPCS NOTIFICATION',
@recipients = @email,
@subject = 'DCL Creation - DCL Downloaded Summary', 
@body = @text,
@body_format = 'HTML';

END TRY

--BEGIN CATCH
--print ERROR_MESSAGE()
--END CATCH
Begin Catch
DECLARE @ErrorMessage NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;

SELECT 
@ErrorMessage = ERROR_MESSAGE(),
@ErrorSeverity = ERROR_SEVERITY(),
@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return error
-- information about the original error that caused
-- execution to jump to the CATCH block.
RAISERROR (@ErrorMessage, -- Message text.
   @ErrorSeverity, -- Severity.
   @ErrorState -- State.
   );
return
End Catch
END

