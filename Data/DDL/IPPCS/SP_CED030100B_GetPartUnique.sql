-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetPartUnique]
AS
BEGIN
	SELECT [PART_NO]
		, [SUPPLIER_CD]
		, [SUPPLIER_PLANT_CD]
		,ISNULL([UNIQUE_NO], '') [UNIQUE_NO]
		,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
	FROM [dbo].[TB_T_CED_PART_UNIQUE]
END

