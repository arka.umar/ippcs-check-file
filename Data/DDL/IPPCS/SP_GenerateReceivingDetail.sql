CREATE PROCEDURE [dbo].[SP_GenerateReceivingDetail] 
AS

BEGIN
--Declare variable for logging
DECLARE @pid BIGINT = 0;
DECLARE @DIGITS as varchar(4) = '0000';
DECLARE @x XML;
DECLARE @MODULID int;
DECLARE @what varchar(50);
DECLARE @where varchar(20);
DECLARE @user varchar(10);
--End ofDeclare variable for logging

DECLARE @SourceString as varchar(max) = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'DOCK_QR' AND SYSTEM_CD = 'DQ')
SET @x = '<r>' + REPLACE((SELECT @SourceString FOR XML PATH('')), ',', '</r><r>') + '</r>';
SET @MODULID = (select MODULE_ID from  TB_M_FUNCTION where FUNCTION_ID='44001');
SET @USER = 'AGENT';
	
	
DECLARE
@ORDER_NO as varchar(10),
@MANIFEST_NO as varchar(10),
@PART_NO as varchar(12),
@ITEM_NO as int,
@ORDER_PLAN_QTY_LOT as int,
@P_LANE_SEQ as varchar(2),
@PLANT_CD as varchar(2)


CREATE TABLE #tmpTable(
	KANBAN_ID varchar(40),
	ORDER_NO varchar(10),
	PLANT_CD varchar(2),
	MANIFEST_NO varchar(10),
	PART_NO varchar(20),
	ITEM_NO  varchar(3),
	RECEIVING_FLG varchar(1),
	P_LANE_SEQ varchar(2)
);

BEGIN TRY
--step1
--2015-09-29
	SELECT ORDER_NO, MANIFEST_NO, PART_NO, ITEM_NO, ORDER_PLAN_QTY_LOT, P_LANE_SEQ, RCV_PLANT_CD INTO #loop FROM (
		SELECT m.ORDER_NO, m.MANIFEST_NO, p.PART_NO, p.ITEM_NO, p.ORDER_PLAN_QTY_LOT, m.P_LANE_SEQ, p.RCV_PLANT_CD
		FROM TB_R_DAILY_ORDER_MANIFEST m with(nolock)
		INNER JOIN TB_M_PLANT pt with(nolock) on m.RCV_PLANT_CD = pt.PLANT_CD
		INNER JOIN (
		SELECT PART_NO, MANIFEST_NO, ITEM_NO,ORDER_PLAN_QTY_LOT, RCV_PLANT_CD
		FROM TB_R_DAILY_ORDER_PART with(nolock)
		GROUP BY MANIFEST_NO, PART_NO, ITEM_NO, ORDER_PLAN_QTY_LOT, RCV_PLANT_CD
		) p on m.MANIFEST_NO=p.MANIFEST_NO
		WHERE --m.FTP_FLAG='0' 
		m.CREATED_DT=convert(varchar(10),getdate(),120) 
		--m.CREATED_DT='2015-09-23 00:00:00.000'
		AND m.ORDER_TYPE='1' AND m.DOCK_CD IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol)) AND p.ORDER_PLAN_QTY_LOT > 0
	)tbl ORDER BY ORDER_NO ASC;
	--logging step 1
	set @what='Join table is finished';
	set @where='Generated Kanban ID'
	--exec sp_putLog @what=@what, @user=@user, @where=@where, @pid='', @id='MPCS00010INF',@type='INF',@module=@MODULID,@func ='44001',@sts='1'
	--modify putlog @pid as OUTPUT
	EXEC sp_PutLog @what, @user, @where, @pid OUTPUT, 'MPCS00010INF', 'INF', @MODULID, '44001', 1;

	DECLARE CURSE CURSOR 
	FOR 
	SELECT 
		ORDER_NO, MANIFEST_NO, PART_NO, ITEM_NO, ORDER_PLAN_QTY_LOT, P_LANE_SEQ, RCV_PLANT_CD
	FROM #loop;

	BEGIN TRY
		OPEN CURSE;
		FETCH NEXT FROM CURSE INTO @ORDER_NO,@MANIFEST_NO,@PART_NO,@ITEM_NO,@ORDER_PLAN_QTY_LOT,@P_LANE_SEQ, @PLANT_CD
		WHILE @@FETCH_STATUS = 0  
		BEGIN  

			DECLARE @i as int = 1;
			WHILE @i <= @ORDER_PLAN_QTY_LOT
			BEGIN
				--digits.Substring(0, digits.Length - ItemNo.ToString().Length) + ItemNo.ToString();
				DECLARE @INO as varchar(4) = '';
				DECLARE @SEQ as varchar(4) = '';

				SET @INO = SUBSTRING(@DIGITS, 1, LEN(@DIGITS) - LEN(CAST(@ITEM_NO as varchar(max))))+''+CAST(@ITEM_NO as varchar(max));
				SET @SEQ = SUBSTRING(@DIGITS, 1, LEN(@DIGITS) - LEN(CAST(@i as varchar(max))))+''+CAST(@i as varchar(max));
				--SELECT @INO, @SEQ;
				--SELECT @ORDER_NO as ORDER_NO,@MANIFEST_NO as MANIFEST_NO,@PART_NO as PART_NO,@ITEM_NO as ITEM_NO,@ORDER_PLAN_QTY_LOT as ORDER_PLAN_QTY_LOT, @P_LANE_SEQ as P_LANE_SEQ, @i;
				INSERT INTO #tmpTable 
				SELECT @ORDER_NO+''+@MANIFEST_NO+''+@PART_NO+''+@INO+''+@SEQ, @ORDER_NO, @PLANT_CD, @MANIFEST_NO, @PART_NO, @ITEM_NO, '0', @P_LANE_SEQ
		
				SET @i = @i + 1;
			END
		FETCH NEXT FROM CURSE INTO @ORDER_NO,@MANIFEST_NO,@PART_NO,@ITEM_NO,@ORDER_PLAN_QTY_LOT,@P_LANE_SEQ, @PLANT_CD
		END;
		--exec put_log
			set @what='Generated Kanban is success';
			set @where='Generated Kanban ID';
			--exec sp_putLog @what=@what, @user=@user, @where=@where, @pid='',@id='MPCS00011INF',@type='INF',@module=@MODULID,@func ='44001',@sts='1'
			--modify putlog @pid as OUTPUT
			EXEC sp_PutLog @what, @user, @where, @pid OUTPUT, 'MPCS00011INF', 'INF', @MODULID, '44001', 1;
			
		CLOSE CURSE;
		DEALLOCATE CURSE;
		DROP table #loop
	END TRY
	BEGIN CATCH
			set @what='Generated Kanban is failed';
			set @where='Generated Kanban ID';
			--exec sp_putLog @what=@what, @user=@user, @where=@where, @pid='',@id='MPCS00011ERR',@type='ERR',@module=@MODULID,@func ='44001',@sts='0'
			--modify putlog @pid as OUTPUT
			EXEC sp_PutLog @what, @user, @where, @pid OUTPUT, 'MPCS00011ERR', 'ERR', @MODULID, '44001', 1;
	END CATCH

	--log 1: start insert
	begin try
		
		INSERT INTO TB_R_RECEIVING_D (KANBAN_ID, ORDER_NO, PLANT_CD, MANIFEST_NO, PART_NO, ITEM_NO, RECEIVING_FLG, P_LANE_SEQ,
		CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT)
		SELECT KANBAN_ID, ORDER_NO, PLANT_CD, MANIFEST_NO, PART_NO, ITEM_NO, RECEIVING_FLG, P_LANE_SEQ,
		'SYSTEM',getdate(),'SYSTEM',getdate() FROM #tmpTable;
		--log 2: end s=insert successfully
		set @what='Insert data is success';
		set @where='Generated Kanban ID';
		--exec sp_putLog @what=@what, @user=@user, @where=@where, @pid='' ,@id='MPCS00011INF',@type='INF',@module=@MODULID,@func ='44001',@sts='1'
		--modify putlog @pid as OUTPUT
		EXEC sp_PutLog @what, @user, @where, @pid OUTPUT, 'MPCS00011INF', 'INF', @MODULID, '44001', 1;
	end try
	begin catch
	--log 3: end with error : ..
	end catch

DROP TABLE #tmpTable;
end TRY

BEGIN CATCH
		set @what='Generate processing is failed';
		set @where='Generated Kanban ID';
		--exec sp_putLog @what=@what, @user=@user, @where=@where, @pid='',@id='MPCS00011ERR',@type='ERR',@module=@MODULID,@func ='44001',@sts='0'	
		--modify putlog @pid as OUTPUT
		EXEC sp_PutLog @what, @user, @where, @pid OUTPUT, 'MPCS00011ERR', 'ERR', @MODULID, '44001', 1;	
END CATCH

END

