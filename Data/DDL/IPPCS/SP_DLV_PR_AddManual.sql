/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : PR unplanned manual add
*/

CREATE PROCEDURE [dbo].[SP_DLV_PR_AddManual]

@PROD_MONTH  varchar(50),
@ROUTE_CD varchar(10),
@LP_CD varchar(10),
@SOURCE varchar (15),
@PR_QTY varchar(10),
@CREATED_BY varchar (25)

AS
BEGIN
		
		DECLARE @MODUL_ID INT = 3
		DECLARE @FUNCTION_ID VARCHAR (6) =  '31102'
		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_PR_AddManual'
		DECLARE @step AS VARCHAR(50) = 'init'
		DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)


		DECLARE @CHECK_MODULE INT;

		DECLARE @check_existing int;
		DECLARE @CURR_DATE VARCHAR(6), @NEXT_DATE VARCHAR (7);

		--CREATE LOG
		SET @steps = @na+'.'+@step;
		SET @log = 'Add PR Manual Process started.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
		


		SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID IN (@FUNCTION_ID,'31103') AND PROCESS_END_DT IS NULL );
		IF (@CHECK_MODULE=0) BEGIN
		
				INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
				VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());

		
				--CREATE LOG
				SET @steps = @na+'.'+@step;
				SET @log = 'Checking Production Month.';
				EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

				SET @CURR_DATE = (SELECT REPLACE(CONVERT (VARCHAR (7), GETDATE(),111),'/',''));
				SET @NEXT_DATE = (SELECT REPLACE(CONVERT (VARCHAR (7), DATEADD(MM,1, GETDATE()) ,111),'/',''));

				UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;

				IF (@CURR_DATE = @PROD_MONTH OR @PROD_MONTH = @NEXT_DATE) BEGIN


						--CREATE LOG
						SET @steps = @na+'.'+@step;
						SET @log = 'Check existing data.';
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

						SET @check_existing = (SELECT count (PROD_MONTH) as result FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH = @PROD_MONTH
																	AND LP_CD = @LP_CD 
																	AND ROUTE_CD = @ROUTE_CD
																	AND SOURCE = @SOURCE 
																	);


						IF (@check_existing > 0) BEGIN

								--CREATE LOG
								SET @steps = @na+'.'+@step;
								SET @log = 'Route is already exist, can not add manual PR';
								EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
								
								UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

								RAISERROR ('Route is already exist, can not add manual PR',16,1)
								RETURN
						END
						ELSE BEGIN

								INSERT INTO TB_T_PR_RETRIEVAL(
										PROD_MONTH,
										LP_CD,
										ROUTE_CD,
										PR_QTY,
										SOURCE,
										--ITEM_PR_FLAG,
										PR_STATUS_FLAG,
										PROCESS_ID,
										CREATED_BY,
										CREATED_DT
									)

								VALUES (
										@PROD_MONTH,
										@LP_CD,
										@ROUTE_CD,
										@PR_QTY,
										@SOURCE,
										--0,
										'PR1',
										null,
										@CREATED_BY,
										GETDATE()
									)

								UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

						END
				END
				ELSE BEGIN
							--CREATE LOG
							SET @steps = @na+'.'+@step;
							SET @log = 'Only able to create PR for current or next month';
							EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
								
							UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

							RAISERROR ('Only able to create PR for current or next month',16,1)
							RETURN
				END
				
		END
		ELSE BEGIN
				RAISERROR('Other process is still running by another user.',16,1)
				RETURN
		END

		--CREATE LOG
		SET @steps = @na+'.'+@step;
		SET @log = 'Add PR Manual Process finished.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

END

