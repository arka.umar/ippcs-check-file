-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-05-02
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030200B_PostReceivingManifest_20191125] @MANIFEST_NO VARCHAR(max) = ''
	,@MANIFEST_RECEIVING_DT VARCHAR(max) = ''
	,@CREATED_BY VARCHAR(max) = ''
	,@PROCESS_ID VARCHAR(max) = ''
AS
BEGIN
	DECLARE @MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
	DECLARE @PROCESS_ID_IPPCS AS TABLE (PROCESS_ID_IPPCS BIGINT)

	CREATE TABLE #TB_List_Message (
		Result VARCHAR(MAX)
		,Message VARCHAR(MAX)
		)

	-- Check PID yang masih aktif
	INSERT INTO @PROCESS_ID_IPPCS
	SELECT TOP 1 PROCESS_ID
	FROM TB_T_CED_SEND_RECEIVING
	WHERE ISNULL(FINISH_FLAG, 'N') <> 'Y'
	ORDER BY PROCESS_ID

	IF EXISTS (
			SELECT ''
			FROM @PROCESS_ID_IPPCS
			)
	BEGIN
		SET @PARAM1 = (
				SELECT *
				FROM @PROCESS_ID_IPPCS
				)

		INSERT INTO #TB_List_Message (
			Result
			,Message
			)
		SELECT 'Failed'
			,'Send Data Receiving with Process ID ' + @PARAM1 + ' not finished yet in IPPCS';
	END
	ELSE
	BEGIN
		SELECT *
		INTO #TB_List_ManifestNo
		FROM dbo.fnSplitString(@MANIFEST_NO, '|')

		SELECT *
		INTO #TB_List_ManifestReceiving
		FROM dbo.fnSplitString(@MANIFEST_RECEIVING_DT, '|')

		SELECT *
		INTO #TB_List_CreatedBy
		FROM dbo.fnSplitString(@CREATED_BY, '|')

		SELECT *
		INTO #TB_List_ProcessId
		FROM dbo.fnSplitString(@PROCESS_ID, '|')

		SELECT A.zeroBasedOccurance + 1 AS ROWNO
			,A.s AS ManifestNo
			,B.s AS ManifestReceiving
			,C.s AS CreatedBy
			,D.s AS ProcessId
		INTO #tb_t_list_final
		FROM #TB_List_ManifestNo A
		LEFT JOIN #TB_List_ManifestReceiving B ON A.zeroBasedOccurance = B.zeroBasedOccurance
		LEFT JOIN #TB_List_CreatedBy C ON C.zeroBasedOccurance = B.zeroBasedOccurance
		LEFT JOIN #TB_List_ProcessId D ON D.zeroBasedOccurance = C.zeroBasedOccurance

		DELETE
		FROM TB_T_CED_SEND_RECEIVING

		INSERT INTO TB_T_CED_SEND_RECEIVING (
			MANIFEST_NO
			,MANIFEST_RECEIVING_DT
			,CREATED_BY
			,CREATED_DT
			,PROCESS_ID
			,FINISH_FLAG
			,FINISH_DATE
			)
		SELECT ManifestNo
			,CONVERT(DATETIME, ManifestReceiving) ManifestReceiving
			,CreatedBy
			,GETDATE() CreatedDt
			,ProcessId
			,'N'
			,NULL
		FROM #tb_t_list_final

		-- Update IPPCS.TB_R_DAILY_ORDER_PART
		UPDATE P
		SET P.RECEIVED_STATUS = '1'
			,P.CHANGED_BY = T.CREATED_BY
			,P.CHANGED_DT = GETDATE()
		FROM dbo.TB_R_DAILY_ORDER_PART P
		JOIN (
			SELECT DISTINCT T.MANIFEST_NO
				,T.CREATED_BY
			FROM TB_T_CED_SEND_RECEIVING T
			) T ON T.MANIFEST_NO = P.MANIFEST_NO

		-- Update IPPCS.TB_R_DAILY_ORDER_MANIFEST
		UPDATE M
		SET M.RECEIVE_FLAG = '1'
			,M.RECEIVE_DT = convert(DATETIME, T.MANIFEST_RECEIVING_DT)
			,M.ARRIVAL_ACTUAL_DT = convert(DATETIME, T.MANIFEST_RECEIVING_DT)
			,M.MANIFEST_RECEIVE_FLAG = '2'
			,M.DROP_STATUS_FLAG = '1'
			,M.DROP_STATUS_DT = convert(DATETIME, T.MANIFEST_RECEIVING_DT)
			,M.CHANGED_BY = T.CREATED_BY
			,M.CHANGED_DT = GETDATE()
		FROM dbo.TB_R_DAILY_ORDER_MANIFEST M
		JOIN (
			SELECT DISTINCT T.MANIFEST_NO
				,T.MANIFEST_RECEIVING_DT
				,T.CREATED_BY
			FROM TB_T_CED_SEND_RECEIVING T
			) T ON T.MANIFEST_NO = M.MANIFEST_NO WHERE ISNULL(M.MANIFEST_RECEIVE_FLAG,0) = 0

		--Update Finish Flag
		UPDATE TB_T_CED_SEND_RECEIVING
		SET FINISH_FLAG = 'Y'
			,FINISH_DATE = GETDATE()

		INSERT INTO #TB_List_Message (
			Result
			,Message
			)
		SELECT 'Success' Result
			,'Manifest No updated successfully' Message
	END

	SELECT *
	FROM #TB_List_Message

	IF OBJECT_ID('tempdb..#TB_List_ManifestNo') IS NOT NULL
		DROP TABLE #TB_List_ManifestNo

	IF OBJECT_ID('tempdb..#TB_List_KanbanType') IS NOT NULL
		DROP TABLE #TB_List_KanbanType

	IF OBJECT_ID('tempdb..#TB_List_ManifestReceiving') IS NOT NULL
		DROP TABLE #TB_List_ManifestReceiving

	IF OBJECT_ID('tempdb..#TB_List_CreatedBy') IS NOT NULL
		DROP TABLE #TB_List_CreatedBy

	IF OBJECT_ID('tempdb..#TB_List_ProcessId') IS NOT NULL
		DROP TABLE #TB_List_ProcessId

	IF OBJECT_ID('tempdb..#tb_t_list_final') IS NOT NULL
		DROP TABLE #tb_t_list_final

	IF OBJECT_ID('tempdb..#TB_List_Message') IS NOT NULL
		DROP TABLE #TB_List_Message
END

