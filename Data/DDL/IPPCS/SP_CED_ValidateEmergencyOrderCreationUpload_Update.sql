CREATE PROC [dbo].[SP_CED_ValidateEmergencyOrderCreationUpload_Update]
(
	@functionID VARCHAR(30),
	@prosesID VARCHAR(30)
)
AS
SET NOCOUNT ON

--TRUNCATE TABLE TB_T_ERROR_UPLOAD
--IF object_id('tempdb..#TempUpload') IS NOT NULL DROP TABLE #TempUpload
IF object_id('tempdb..#TempValidate') IS NOT NULL DROP TABLE #TempValidate
--IF object_id('tempdb..#TempResult') IS NOT NULL DROP TABLE #TempResult
IF object_id('tempdb..#TempType') IS NOT NULL DROP TABLE #TempType

DECLARE 
		@uploadRowStartIndex INT ,
		@uploadRowEndIndex INT, 
		
		@validateRowCount INT,
		@validateRowIndex INT,
		
		@validateRowCountType INT,
		@validateRowIndexType INT,
		
		
		@sql NVARCHAR(MAX),
		@condition NVARCHAR(MAX),
		@field VARCHAR(100),
		@Length integer,
		@Type integer,
		@message varchar(MAX),
		@TypeLoop int

--DECLARE @@DOCK_PACKING TABLE (DOCK_CD VARCHAR(4))
DECLARE @@items VARCHAR(MAX)

SELECT @@items = SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'DOCK_PACKING' AND SYSTEM_CD = 'EMERGENCY_ORDER'
--INSERT INTO @@DOCK_PACKING SELECT LTRIM(RTRIM(s)) FROM dbo.[fnSplitString](@@items, ',')


CREATE TABLE #TempType (id_num int IDENTITY(1,1), TYPE int)
CREATE TABLE #TempValidate (SEQ_NO int , FUNCTION_ID int,FIELD varchar(8000),
							CONDITION varchar(8000),MESSAGE varchar(max),LENGTH int, TYPE int)

--insert 	into #TempType([TYPE])
--select distinct TYPE from dbo.TB_M_UPLOAD_VALIDATION
--WHERE FUNCTION_ID = @functionId
--ORDER BY TYPE

--SELECT  @validateRowCountType = COUNT('') from #tempType
--	SET @validateRowIndexType = 1
--	WHILE @validateRowIndexType <= @validateRowCountType
--	begin 
--		SELECT  	@TypeLoop=[TYPE]
--		FROM #TempType 
--		WHERE id_num= @validateRowIndexType
		
delete from #TempValidate
INSERT INTO #TempValidate(SEQ_NO , FUNCTION_ID ,FIELD ,
							CONDITION ,MESSAGE ,LENGTH, TYPE )
SELECT 
		ROW_NUMBER( ) OVER ( ORDER BY [FUNCTION_ID] ) AS SEQ_NO ,
		[FUNCTION_ID],
		[FIELD],
		[CONDITION],
		[MESSAGE],
		[LENGTH],
		[TYPE]
	FROM dbo.TB_M_UPLOAD_VALIDATION
	WHERE FUNCTION_ID = '132' -- Remark FID.Ridwan : 2019-10-19 @functionID --and TYPE=@TypeLoop
	ORDER BY TYPE
								
							
SELECT  @uploadRowStartIndex = MIN(ID)
FROM    TB_T_CED_EMERGENCY_ORDER_UPLOAD WITH(INDEX (PK_TB_T_CED_EMERGENCY_ORDER_UPLOAD))
WHERE [FUNCTION_ID] = @functionID AND [PROCESS_ID] = @prosesID

SELECT  @uploadRowEndIndex = MAX(ID)
FROM    TB_T_CED_EMERGENCY_ORDER_UPLOAD WITH(INDEX (PK_TB_T_CED_EMERGENCY_ORDER_UPLOAD))
WHERE [FUNCTION_ID] = @functionID AND [PROCESS_ID] = @prosesID

SELECT  @validateRowCount = COUNT(SEQ_NO)
FROM    #TempValidate --WITH(INDEX (UIDX_ID))

	SET @validateRowIndex = 1
	WHILE @validateRowIndex <= @validateRowCount
		BEGIN
		
				SELECT  @field = [FIELD],
						@condition = [CONDITION],
						@message = [MESSAGE],
						@Length=[LENGTH],
						@Type=[TYPE]
				FROM #TempValidate --WITH(INDEX (IDX_ID))
				WHERE SEQ_NO = @validateRowIndex 
				
				--check empty
				IF @field = 'SUPPLIER_CD_CHECK_EMPTY' 
				BEGIN
						SET @condition = @condition + ' AND ISNULL(SUPPLIER_CD,'''')='''' AND ISNULL(SUPPLIER_PLANT,'''') ='''' AND (ISNULL(KANBAN_NO,'''')='''' OR ISNULL(REPLACE(PART_NO,''-'',''''),'''')='''') AND ISNULL(ORDER_LOT_SZ,'''')='''' AND ISNULL(DOCK_CD,'''') ='''''
				END
				
				IF @field = 'SUPPLIER_PLANT_CHK_EMPTY' 
				BEGIN
						SET @condition = @condition + ' AND ISNULL(SUPPLIER_PLANT,'''') ='''' AND (ISNULL(KANBAN_NO,'''')='''' OR ISNULL(REPLACE(PART_NO,''-'',''''),'''')='''') AND ISNULL(ORDER_LOT_SZ,'''')='''' AND ISNULL(DOCK_CD,'''') ='''''
				END
				
				IF @field = 'DOCK_CD_CHK_EMPTY' 
				BEGIN
						SET @condition = @condition + ' AND ISNULL(DOCK_CD,'''') ='''' AND  (ISNULL(KANBAN_NO,'''')='''' OR ISNULL(REPLACE(PART_NO,''-'',''''),'''')='''') AND ISNULL(ORDER_LOT_SZ,'''')='''''
				END
				IF @field = 'KANBAN_NO_CHK_EMPTY' 
				BEGIN
						SET @condition = @condition + ' AND (ISNULL(KANBAN_NO,'''')='''' and ISNULL(REPLACE(PART_NO,''-'',''''),'''')='''') AND ISNULL(ORDER_LOT_SZ,'''')=''''' 
				END
				IF @field = 'KANBAN_NO_CHK_EMPTY2' 
				BEGIN
						SET @condition = @condition + ' AND (ISNULL(KANBAN_NO,'''')='''' and ISNULL(REPLACE(PART_NO,''-'',''''),'''')='''') ' 
				END			
				IF @field = 'ORDER_LOT_SZ_CHK_EMPTY' 
				BEGIN
						SET @condition = @condition + ' AND ISNULL(ORDER_LOT_SZ,'''')=''''' 
				END
				
				--check length 
					IF @field = 'SUPPLIER_CD_LENGTH_ALL' 
				BEGIN
						SET @condition = @condition + ' AND not LEN(KANBAN_NO) = 4 AND not LEN(DOCK_CD) = 2 AND not LEN(REPLACE(PART_NO,''-'','''')) = 12 AND not LEN(SUPPLIER_CD) = 4' 
				END
				
				IF @field = 'PART_NO_EXISTS' OR @field = 'KANBAN_NO_EXISTS'
				BEGIN
						SET @condition = @condition + ' AND NOT EXISTS (SELECT 1 FROM TB_M_PART_INFO AS c
														WHERE (
															(c.PART_NO = REPLACE(t.PART_NO,''-'','''') AND ISNULL(t.PART_NO,'''') <> '''' AND ISNULL(t.KANBAN_NO,'''') = '''') OR
															(c.KANBAN_NO = REPLACE(t.KANBAN_NO,''-'','''') AND ISNULL(t.PART_NO,'''') = '''' AND ISNULL(t.KANBAN_NO,'''') <> '''') OR
															(c.PART_NO = REPLACE(t.PART_NO,''-'','''') AND c.KANBAN_NO = REPLACE(t.KANBAN_NO,''-'','''') AND ISNULL(t.PART_NO,'''') <> '''' AND ISNULL(t.KANBAN_NO,'''') <> '''')
															)
															AND c.SUPPLIER_CD = t.SUPPLIER_CD 
															AND c.SUPPLIER_PLANT = t.SUPPLIER_PLANT 
															AND c.DOCK_CD = t.DOCK_CD )'
				set @message='PART NO for Supplier Plant Code '' + t.SUPPLIER_CD + ''-'' + t.SUPPLIER_PLANT + '' - And Dock '' + t.DOCK_CD  + '' is not exist'     
				
				END

				IF @field = 'DOCK_CD_EXISTS' 
				BEGIN
						SET @condition = @condition + ' AND NOT EXISTS (SELECT 1 FROM tb_m_dock AS c
														WHERE c.DOCK_CD = t.DOCK_CD)'
				END
				
				IF @field = 'SUPPLIER_CD' OR @field='SUPPLIER_PLANT'
				BEGIN
						SET @condition = @condition + ' AND NOT EXISTS (SELECT 1 FROM tb_m_supplier AS c
														WHERE c.SUPPLIER_CODE = t.SUPPLIER_CD and c.SUPPLIER_PLANT_CD = t.SUPPLIER_PLANT )'
				END
				
				IF @field = 'KANBAN_NO_ORDER_QTY' 
				BEGIN
						SET @condition = @condition + ' AND (LEN(KANBAN_NO)=0 AND LEN(REPLACE(PART_NO,''-'',''''))=0) OR LEN(ORDER_LOT_SZ)=0'
				END

				IF @field = 'KANBAN_NO_QTY_DOCK_CODE' 
				BEGIN
						SET @condition = @condition + ' AND (LEN(KANBAN_NO)=0 AND LEN(REPLACE(PART_NO,''-'',''''))=0) OR LEN(ORDER_LOT_SZ)=0 OR LEN(DOCK_CD)=0'
				END

				/* Start 2019-02-12 FID.Ridwan : Change Request */
				--IF @field = 'PROD_DATE_CHECK_EMPTY' 
				--BEGIN
				--		SET @condition = @condition + ' AND (ISNULL(t.PART_BARCODE,'''')='''' AND ISNULL(t.PROD_DATE,'''')='''')'
				--END

				IF @field = 'PROD_DATE_CHECK_LENGTH' 
				BEGIN
						SET @condition = @condition + ' AND (NOT LEN(t.PROD_DATE)=10 AND ISNULL(t.PROD_DATE,'''')<>'''')'
				END
				
				IF @field = 'PROD_DATE_FORMAT' 
				BEGIN
						SET @condition = @condition + ' AND (ISNULL(t.PROD_DATE,'''')<>'''' AND ([dbo].[fn_date_validate](t.PROD_DATE)) <> 1)'
				END
				
				IF @field = 'KANBAN_QTY_CHECK' 
				BEGIN
						SET @condition = @condition + ' AND ISNULL(ORDER_LOT_SZ,'''')<>'''' AND ISNULL(PART_BARCODE,'''')<>'''' AND ISNULL(ORDER_LOT_SZ,0)> 1'
				END
				
				IF @field = 'PROD_DATE_DIFF' 
				BEGIN
						SET @condition = @condition + ' AND EXISTS(SELECT SUPPLIER_CD, SUPPLIER_PLANT, DOCK_CD, PROD_DATE FROM TB_T_CED_EMERGENCY_ORDER_UPLOAD S WHERE t.SUPPLIER_CD = s.SUPPLIER_CD AND t.SUPPLIER_PLANT = S.SUPPLIER_PLANT AND t.DOCK_CD = S.DOCK_CD AND t.PROD_DATE <> S.PROD_DATE 
						AND ISNULL(t.SUPPLIER_CD,'''')<>'''' 
						AND ISNULL(t.SUPPLIER_PLANT,'''')<>'''' 
						AND ISNULL(t.DOCK_CD,'''')<>'''' 
						AND ISNULL(t.PROD_DATE,'''')<>'''' 
						AND ISNULL(S.PROD_DATE,'''')<>'''' )'
				END
				
				--IF @field = 'DOCK_CD_DIFF' 
				--BEGIN
				--		SET @condition = @condition + ' AND t.DOCK_CD NOT IN (' + (SELECT LTRIM(RTRIM(s)) AS DOCK_CD FROM dbo.[fnSplitString](@@items, ',')) + ')'
				--END
				/* End 2019-02-12 FID.Ridwan : Change Request */

				--SET @sql = 'INSERT INTO TB_T_CED_ERROR_UPLOAD(
				--					FUNCTION_ID, 
				--					PROCESS_ID, 
				--					FIELD, 
				--					ERRORS_MESSAGE)
				--				SELECT
				--					t.FUNCTION_ID,
				--					t.PROCESS_ID,
				--					''' + LEFT(@field,@length) + ''',
				--					''Data in Row ['' + Convert(varchar,t.ID) + '']'' + '', '' + CONVERT(NVARCHAR(MAX),''' + @message + ''')
				--				FROM TB_T_CED_EMERGENCY_ORDER_UPLOAD t WITH(nolock)
				--					WHERE ID = ' + CONVERT(VARCHAR,@uploadRowStartIndex) + ' AND
				--						FUNCTION_ID = ' + @functionID + ' AND PROCESS_ID = ' + @prosesID + ' AND  (' + @condition + ')								
				--		'				
						
						SET @sql = 'INSERT INTO TB_T_CED_ERROR_UPLOAD(
								FUNCTION_ID, 
								PROCESS_ID, 
								FIELD, 
								ERRORS_MESSAGE)
							SELECT
								t.FUNCTION_ID,
								t.PROCESS_ID,
								''' + LEFT(@field,@length) + ''',
								''Data in Row ['' + Convert(varchar,t.ID) + '']'' + '', '' + CONVERT(NVARCHAR(MAX),''' + @message + ''')
							FROM TB_T_CED_EMERGENCY_ORDER_UPLOAD t WITH(nolock)
								WHERE 
									FUNCTION_ID = ' + @functionID + ' AND PROCESS_ID = ' + @prosesID + ' AND  (' + @condition + ')								
					'				
		
		
					BEGIN TRY
					
					PRINT(@sql)
					EXEC (@sql)
					if exists(SELECT '' from  TB_T_CED_ERROR_UPLOAD where FUNCTION_ID = @functionID AND PROCESS_ID = @prosesID )
					begin
					print('a')
					SELECT * FROM TB_T_CED_ERROR_UPLOAD where FUNCTION_ID=@functionID AND PROCESS_ID=@prosesID 
					return 
					end
					
						--PRINT(@sql)
						--EXEC (@sql)
						--if exists(SELECT '' from  TB_T_CED_ERROR_UPLOAD where FUNCTION_ID = @functionID AND PROCESS_ID = @prosesID AND ID = @uploadRowStartIndex)
						--begin
						--	--SELECT * FROM TB_T_CED_ERROR_UPLOAD where FUNCTION_ID=@functionID AND PROCESS_ID=@prosesID 
						--	--return 
						--	goto NextData;
						--end
						
					END TRY
					
					BEGIN CATCH
						DECLARE @ErrorMessage NVARCHAR(4000);
						DECLARE @ErrorSeverity INT;
						DECLARE @ErrorState INT;

						SELECT 
							@ErrorMessage = ERROR_MESSAGE(),
							@ErrorSeverity = ERROR_SEVERITY(),
							@ErrorState = ERROR_STATE();

						-- Use RAISERROR inside the CATCH block to return error
						-- information about the original error that caused
						-- execution to jump to the CATCH block.
						RAISERROR (@ErrorMessage, -- Message text.
								   @ErrorSeverity, -- Severity.
								   @ErrorState -- State.
								   );
--						return
--					END CATCH
--					SET @validateRowIndex = @validateRowIndex + 1
--			END
			
--	--SET @validateRowIndexType = @validateRowIndexType + 1
--	--END

--NextData:
--SET @uploadRowStartIndex  = @uploadRowStartIndex  + 1
--END
				return
				END CATCH
				SET @validateRowIndex = @validateRowIndex + 1
		END
		
--SET @validateRowIndexType = @validateRowIndexType + 1
--END


SELECT * FROM TB_T_CED_ERROR_UPLOAD where FUNCTION_ID=@functionID AND PROCESS_ID=@prosesID

