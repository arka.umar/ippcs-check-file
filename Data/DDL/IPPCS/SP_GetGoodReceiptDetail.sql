CREATE PROCEDURE [dbo].[SP_GetGoodReceiptDetail] 
	@DockCode VARCHAR(2),
	@RcvPlantCode CHAR(1),
	@ManifestNo VARCHAR(16)
AS
BEGIN
	DECLARE		
		@rowIndex INT,
		@rowCount INT,
		
		@partNo AS VARCHAR(MAX),
		@errCd AS VARCHAR(MAX),
		@area AS VARCHAR(MAX),
		@group AS VARCHAR(MAX),
		@remark AS VARCHAR(MAX),
		
		@sql AS NVARCHAR(MAX)
	
	-- Create temp table for good receipt manifest detail
	IF OBJECT_ID('tempdb..#TempGoodReceiptManifestDetail') IS NOT NULL DROP TABLE #TempGoodReceiptManifestDetail
	
	CREATE TABLE #TempGoodReceiptManifestDetail
    (
		MANIFEST_NO VARCHAR(MAX),
		DOCK_CD VARCHAR(MAX),
		RCV_PLANT_CD VARCHAR(MAX),
    
		PART_NO VARCHAR(MAX),
		PART_NAME VARCHAR(MAX),
		ORDER_QTY INT,
		RECEIVED_QTY INT,
		REMAINING_QTY INT,
		RECEIVED_STATUS VARCHAR(MAX),
		DAMAGE_QTY INT,
		MISSPART_QTY INT,
		SHORTAGE_QTY INT,

		AREA VARCHAR(MAX),
		[GROUP] VARCHAR(MAX),
		REMARK VARCHAR(MAX),

		FD_EXCHANGE_RATE VARCHAR(MAX),
		FD_IM_PERIOD VARCHAR(MAX),
		FD_STANDARD_COST VARCHAR(MAX),
		
		PCD_DOCK_CODE VARCHAR(MAX),
		PCD_MATERIAL_MASTER VARCHAR(MAX),
		PCD_ROUTING VARCHAR(MAX),
		PCD_PART_LIST VARCHAR(MAX),
		PCD_OTHERS VARCHAR(MAX),
		
		PAD_DOCK_CODE VARCHAR(MAX),
		PAD_MATERIAL_MASTER VARCHAR(MAX),
		PAD_MATDOCK VARCHAR(MAX),
		PAD_LOCATION_CODE VARCHAR(MAX),
		PAD_OTHERS VARCHAR(MAX),
		
		PUD_MATERIAL_PRICE VARCHAR(MAX),
		PUD_STANDARD_PRICE VARCHAR(MAX),
		PUD_PO VARCHAR(MAX),
		PUD_SOURCE_LIST VARCHAR(MAX),
		PUD_SUPPLIER_MASTER VARCHAR(MAX),
		PUD_OTHERS VARCHAR(MAX),
		
		PVD_MATERIAL_PRICE VARCHAR(MAX),
		PVD_PACKING_TYPE VARCHAR(MAX),
		PVD_OTHERS VARCHAR(MAX),
		
		ISTD_SYSTEM VARCHAR(MAX),
		ISTD_OTHERS VARCHAR(MAX),
    )		
    
    INSERT INTO #TempGoodReceiptManifestDetail
    (
		MANIFEST_NO,
		DOCK_CD,
		RCV_PLANT_CD,
		
		PART_NO,
		PART_NAME,
		ORDER_QTY,
		RECEIVED_QTY,
		REMAINING_QTY,
		RECEIVED_STATUS,
		DAMAGE_QTY,
		MISSPART_QTY,
		SHORTAGE_QTY
    )
    SELECT DISTINCT
		TRDOP.MANIFEST_NO,
		TRDOP.DOCK_CD,
		TRDOP.RCV_PLANT_CD,
		
		TRDOP.[PART_NO],
		TRDOP.[PART_NAME],
		TRDOP.[ORDER_QTY],
		TRPP.[RECEIVED_QTY],
		(
			TRPP.[RECEIVED_QTY] - (TRPP.[DAMAGE_QTY] + TRPP.[MISSPART_QTY] + TRPP.[SHORTAGE_QTY])
		) AS 'REMAINING_QTY',
		TRDOP.[RECEIVED_STATUS],
		TRPP.[DAMAGE_QTY],
		TRPP.[MISSPART_QTY],
		TRPP.[SHORTAGE_QTY]
	FROM [TB_R_DAILY_ORDER_PART] TRDOP
		LEFT JOIN [TB_R_PROBLEM_PART] TRPP ON
			TRDOP.[MANIFEST_NO] = TRPP.[MANIFEST_NO] AND
			TRDOP.[PART_NO] = TRPP.[PART_NO]
	WHERE
		((TRDOP.[DOCK_CD]= @DockCode AND ISNULL(@DockCode, '') <> '') or (ISNULL(@DockCode, '') = '')) AND 
		((TRDOP.[RCV_PLANT_CD]=@RcvPlantCode AND ISNULL(@RcvPlantCode, '') <> '') or (ISNULL(@RcvPlantCode, '') = '')) AND 
		((TRDOP.[MANIFEST_NO]=@ManifestNo AND ISNULL(@ManifestNo, '') <> '') or (ISNULL(@ManifestNo, '') = '')) 		
	ORDER BY TRDOP.[PART_NO] ASC

	-- Create temp table for log transaction detail
	IF OBJECT_ID('tempdb..#TempLogTransactionDetail') IS NOT NULL DROP TABLE #TempLogTransactionDetail

	CREATE TABLE #TempLogTransactionDetail
    (
		ROW_ID INT IDENTITY(1, 1),
		MANIFEST_NO VARCHAR(MAX),
		PART_NO VARCHAR(MAX),
		DOCK_CD VARCHAR(MAX),
		ERR_CD VARCHAR(MAX),
		AREA VARCHAR(MAX),
		[GROUP] VARCHAR(MAX),
		REMARK VARCHAR(MAX)
    )	
    
    INSERT INTO #TempLogTransactionDetail
    (
		MANIFEST_NO,
		PART_NO,
		DOCK_CD,
		ERR_CD,
		AREA,
		[GROUP],
		REMARK
    )
    SELECT 
		P.MANIFEST_NO
		,P.PART_NO
		,P.DOCK_CD
		,D.ERR_CD
		,E.AREA
		,E.[GROUP]
		,E.REMARK
	FROM TB_R_DAILY_ORDER_PART P
		INNER JOIN TB_R_LOG_TRANSACTION_D D 
			ON P.MANIFEST_NO = D.MANIFEST_NO
			AND P.PART_NO = D.PART_NO
		INNER JOIN TB_M_ERRORMAPPING E
			ON D.ERR_CD = E.MESSAGE_ID
	WHERE
		((P.[MANIFEST_NO]=@ManifestNo AND ISNULL(@ManifestNo, '') <> '') or (ISNULL(@ManifestNo, '') = '')) 		
		AND ((P.[DOCK_CD]= @DockCode AND ISNULL(@DockCode, '') <> '') or (ISNULL(@DockCode, '') = '')) 
	ORDER BY P.PART_NO
	
	-- Update temp table for good receipt manifest detail with related error cd
	SET @rowIndex = 1
	SELECT @rowCount = COUNT(*) FROM #TempLogTransactionDetail
	WHILE @rowIndex <= @rowCount
	BEGIN
		SELECT
			@partNo = PART_NO
			,@errCd = ERR_CD
			,@area = AREA
			,@group = [GROUP]
			,@remark = REMARK
		FROM #TempLogTransactionDetail
		WHERE ROW_ID = @rowIndex
		
		SET @sql = 'UPDATE #TempGoodReceiptManifestDetail
					SET
						' + REPLACE(UPPER(RTRIM(@area)), ' ', '_') + '_' + REPLACE(UPPER(RTRIM(@group)), ' ', '_')  + '=''' + @remark + '''
					WHERE
						PART_NO = ''' + @partNo + '''
						AND MANIFEST_NO = ''' + @ManifestNo + '''
						AND DOCK_CD = ''' + @DockCode + '''
						AND RCV_PLANT_CD = ''' + @RcvPlantCode + ''''
	
		EXEC (@sql)
	
		SET @rowIndex = @rowCount + 1
	END
			

	SELECT * FROM #TempGoodReceiptManifestDetail        
END

