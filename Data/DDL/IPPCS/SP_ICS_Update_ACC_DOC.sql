-- =============================================
-- Author:		fid.Joko
-- Create date: 14.10.2013
-- Description:	Update ACC_DOC_NO, ACC_DOC_YEAR
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_Update_ACC_DOC] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #ACC_DOC_NO_TABLE(INV_NO VARCHAR(16), ACCT_DOC_NO VARCHAR(10), ACCT_DOC_YEAR VARCHAR(4))

	DECLARE @sqlQuery AS VARCHAR(MAX)
	SET @sqlQuery = 'INSERT INTO #ACC_DOC_NO_TABLE 
					SELECT INV_NO, ACCT_DOC_NO, ACCT_DOC_YEAR
					FROM OPENQUERY([IPPCS_TO_ICS] ,''
						SELECT h.INV_NO,th.ACCT_DOC_NO,th.ACCT_DOC_YEAR FROM TB_R_INVOICE_H h
						INNER JOIN TB_R_FI_TRANSACTION_H th ON h.INV_NO = th.DOC_NO
						WHERE TO_CHAR(h.CHANGED_DT, ''''yyyy/mm/dd'''') >= TO_CHAR(SYSDATE-30, ''''yyyy/mm/dd'''')'')'
	EXEC (@sqlQuery)

	UPDATE h SET
		h.ACCT_DOC_NO=ac.ACCT_DOC_NO,
		h.ACCT_DOC_YEAR=ac.ACCT_DOC_YEAR,
		h.PAYMENT_DOC_NO=CASE WHEN ts.CLR_DOC like '20%' or ts.CLR_DOC like '15%' THEN ts.CLR_DOC ELSE h.PAYMENT_DOC_NO END,
		h.TEMP_PAY_DOC_NO=CASE WHEN ts.CLR_DOC like '20%' or ts.CLR_DOC like '15%' THEN h.PAYMENT_DOC_NO ELSE ts.CLR_DOC END,
		h.PAY_ACT_DT= CASE WHEN CLR_DT = '00000000' then NULL ELSE convert(datetime,ts.CLR_DT,112) END,
		h.STATUS_CD=CASE WHEN ISNULL(ac.ACCT_DOC_NO,'') <> '' AND ISNULL(ts.CLR_DOC,'') <> '' AND (ts.CLR_DOC like '20%' or ts.CLR_DOC like '15%') THEN 5 ELSE h.STATUS_CD END
	FROM TB_R_INV_H h
	INNER JOIN #ACC_DOC_NO_TABLE ac ON h.INV_NO=ac.INV_NO
	LEFT JOIN TB_T_SAP_BSEG ts ON ac.ACCT_DOC_NO=ts.DOC_NO AND ac.ACCT_DOC_YEAR=ts.DOC_YEAR
	WHERE h.STATUS_CD <> 5
	
	UPDATE h SET
		h.PAYMENT_DOC_NO=CASE WHEN ts.CLR_DOC like '20%' or ts.CLR_DOC like '15%' THEN ts.CLR_DOC ELSE h.PAYMENT_DOC_NO END,
		h.TEMP_PAY_DOC_NO=CASE WHEN ts.CLR_DOC like '20%' or ts.CLR_DOC like '15%' THEN h.PAYMENT_DOC_NO ELSE ts.CLR_DOC END,
		h.PAY_ACT_DT=ts.CLR_DT,
		h.STATUS_CD=CASE WHEN ISNULL(h.ACCT_DOC_NO,'') <> '' AND ISNULL(ts.CLR_DOC,'') <> '' AND (ts.CLR_DOC like '20%' or ts.CLR_DOC like '15%') THEN 5 ELSE h.STATUS_CD END
	FROM TB_R_INV_H h
	INNER JOIN TB_T_SAP_BSEG ts ON h.TEMP_PAY_DOC_NO=ts.DOC_NO AND h.ACCT_DOC_YEAR=ts.DOC_YEAR
	WHERE h.STATUS_CD <> 5
	
	UPDATE TB_R_DAILY_ORDER_MANIFEST SET
		MANIFEST_RECEIVE_FLAG=6,
		CHANGED_BY='AGENT',
		CHANGED_DT=GETDATE()
	WHERE MANIFEST_NO IN(
		select manifest_no
		from 
		(
			select manifest_no,part_no 
			from tb_r_daily_order_part 
			group by manifest_no,part_no 
		)p
		left join(
			select inv_no,inv_ref_no,part_no=(mat_no+part_color_sfx) 
			from TB_R_INV_D
			where INV_NO IN(SELECT INV_NO FROM #ACC_DOC_NO_TABLE)
		)Qry on p.manifest_no=Qry.inv_ref_no and p.part_no=Qry.part_no
		where p.manifest_no in(select INV_REF_NO from TB_R_INV_D where INV_NO IN(SELECT INV_NO FROM #ACC_DOC_NO_TABLE))
		group by manifest_no
		having SUM(case when Qry.inv_no is null then 1 else 0 end)=0
	)
	
	DROP TABLE #ACC_DOC_NO_TABLE
END

