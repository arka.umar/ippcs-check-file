/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Approve draft route price
*/

CREATE PROCEDURE [dbo].[SP_DLV_ApproveDraftRoutePrice]

@GRID VARCHAR (MAX),
@CREATED_BY VARCHAR (MAX)

AS
BEGIN

		CREATE TABLE #TEMP(
			CHILD_ROUTE VARCHAR(4),
			PARENT_ROUTE VARCHAR (4),
			LP_CD VARCHAR(4),
			VALID_FROM DATE,
			VALID_TO DATE,
			APPROVED_BY VARCHAR (20),
			APPROVED_DT DATETIME

		);

		
		DECLARE @CHECK_AUTH INT, @CHECK_COUNT INT, @CHECK_APPROVED INT, @CHECK_VALID_FROM DATE, @CHECK_M_PRICE INT;

		SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE JOB_FUNCTION_ID = 11 AND USERNAME = @CREATED_BY);
		
		IF (@CHECK_AUTH = 1) BEGIN
		
					-- 2. == VARIABLE TABLE TEMPORARY UNTUK EXPLODING GRID ==
					DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
					DECLARE @MIN_ROW INT;
					DECLARE @ROW_DETAIL VARCHAR(MAX);
					DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
					DECLARE @MIN_FIELD INT;
					-- ================================================================

					-- 3. == Variabel Untuk Menyimpan Data Ke TABLE DETAIL ==
					DECLARE @CHILD_ROUTE_T VARCHAR(4),@PARENT_ROUTE_T VARCHAR (4), @LP_CD_T VARCHAR(3), @VALID_FROM_T DATE, @VALID_TO_T DATE,
					@APPROVED_BY_T VARCHAR(20), @APPROVED_DT_T DATETIME;
					
					-- =================================================================
					INSERT INTO @ROWS
					SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@GRID,';');
					-- ================================================================
					SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);
					-- ========================================================
					WHILE @MIN_ROW IS NOT NULL BEGIN
								-- 6.3.1 == Hapus VARIABLE TABLE @FIELDS jika ada ==
								DELETE FROM @FIELDS;
								
								SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
								INSERT INTO @FIELDS
								SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'|');
								-- ========================================================================================

								-- 6.3.3 == Ambil ITEM Berdasarkan ID untuk dimasukkan kedalam VARIABLE FIELD Detail ==
								SET @CHILD_ROUTE_T = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
								SET @PARENT_ROUTE_T = (SELECT ITEM FROM @FIELDS WHERE ID = 2);
								SET @LP_CD_T = (SELECT ITEM FROM @FIELDS WHERE ID = 3);
								SET @VALID_FROM_T = (SELECT ITEM FROM @FIELDS WHERE ID = 4);
								SET @VALID_TO_T = (SELECT ITEM FROM @FIELDS WHERE ID = 5);
								SET @APPROVED_BY_T = (SELECT ITEM FROM @FIELDS WHERE ID = 6);
								SET @APPROVED_DT_T = (SELECT ITEM FROM @FIELDS WHERE ID = 7);
								
								-- ====================================================================================
								
								INSERT INTO #TEMP VALUES(@CHILD_ROUTE_T, @PARENT_ROUTE_T, @LP_CD_T, @VALID_FROM_T, @VALID_TO_T, @APPROVED_BY_T, @APPROVED_DT_T);

								-- 6.3.5 == Ambil ID dari VARIABLE TABLE @ROWS yg IDnya lebih besar dari @MIN_ROW ==
								SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
								-- =================================================================================

					END --END WHILE
					

					SELECT * FROM #TEMP;
					
					SET @CHECK_COUNT = (SELECT COUNT(*) FROM #TEMP);
					SET @CHECK_APPROVED = (SELECT COUNT(*) FROM #TEMP WHERE APPROVED_BY = ''  AND APPROVED_DT IS NULL );

					

					IF (@CHECK_COUNT = @CHECK_APPROVED) BEGIN
					

								DECLARE @CHILD_ROUTE VARCHAR(4),@PARENT_ROUTE VARCHAR (4), @LP_CD VARCHAR(3), @VALID_FROM DATE, @VALID_TO DATE;
								
								DECLARE BTMP_CUR CURSOR
								FOR
								SELECT CHILD_ROUTE, PARENT_ROUTE, LP_CD, VALID_FROM, VALID_TO
								FROM #TEMP
								;

								OPEN BTMP_CUR;
								FETCH NEXT FROM BTMP_CUR INTO
								@CHILD_ROUTE, @PARENT_ROUTE, @LP_CD, @VALID_FROM, @VALID_TO

								WHILE @@FETCH_STATUS = 0
								BEGIN
											
											SET @CHECK_VALID_FROM = (SELECT VALID_FROM FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @CHILD_ROUTE  AND VALID_TO = '9999-12-31')
											
											SET @CHECK_M_PRICE = (SELECT COUNT(*) FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @CHILD_ROUTE)

											IF (@CHECK_M_PRICE=0) BEGIN
														
														UPDATE TB_M_DLV_DRAFT_ROUTE_PRICE SET APPROVED_BY = @CREATED_BY, APPROVED_DT = GETDATE(), CHANGED_BY = @CREATED_BY, CHANGED_DT = GETDATE()
														WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND ROUTE_CD_PARENT = @PARENT_ROUTE AND LP_CD=@LP_CD;

														INSERT INTO TB_M_ROUTE_PRICE (ROUTE_CD, LP_CD, PRICE,  VALID_FROM, VALID_TO, PRICE_TYPE, CREATED_BY, CREATED_DT)
														VALUES (@CHILD_ROUTE, @LP_CD, (SELECT PRICE FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @PARENT_ROUTE  AND VALID_TO='9999-12-31'), @VALID_FROM, @VALID_TO, 'C', @CREATED_BY, GETDATE())
															
											END
											ELSE BEGIN
													
													IF ( CONVERT (DATE,@VALID_FROM,111) > CONVERT(DATE,@CHECK_VALID_FROM,111)) BEGIN
																	
																	UPDATE TB_M_DLV_DRAFT_ROUTE_PRICE SET APPROVED_BY = @CREATED_BY, APPROVED_DT = GETDATE(), CHANGED_BY = @CREATED_BY, CHANGED_DT = GETDATE()
																	WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND ROUTE_CD_PARENT = @PARENT_ROUTE AND LP_CD=@LP_CD;
																	
																	UPDATE TB_M_ROUTE_PRICE 
																	SET VALID_TO = DATEADD(day, -1, CAST(@VALID_FROM AS DATE)),
																	CHANGED_BY = @CREATED_BY, CHANGED_DT = GETDATE()
																	WHERE ROUTE_CD = @CHILD_ROUTE AND VALID_TO = '9999-12-31'

																	INSERT INTO TB_M_ROUTE_PRICE (ROUTE_CD, LP_CD, PRICE,  VALID_FROM, VALID_TO, PRICE_TYPE, CREATED_BY, CREATED_DT)
																	VALUES (@CHILD_ROUTE, @LP_CD, (SELECT PRICE FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @PARENT_ROUTE  AND VALID_TO='9999-12-31'), @VALID_FROM, @VALID_TO, 'C', @CREATED_BY, GETDATE())

													END
													ELSE BEGIN 
																	RAISERROR ('Valid from master route price cannot less then last valid from.',16,1)
																	RETURN
													END
																	
											END



											

								FETCH NEXT FROM BTMP_CUR INTO
									@CHILD_ROUTE, @PARENT_ROUTE, @LP_CD, @VALID_FROM, @VALID_TO
								END;
								CLOSE BTMP_CUR;
								DEALLOCATE BTMP_CUR;
					END
					ELSE BEGIN
								RAISERROR ('Master draft route price already to approve',16,1);
								RETURN
					END
					
		END
		ELSE BEGIN
					
				RAISERROR('You are not authorized to perform this action.',16,1)
				RETURN
		END

		DROP TABLE #TEMP;
		
		

END

