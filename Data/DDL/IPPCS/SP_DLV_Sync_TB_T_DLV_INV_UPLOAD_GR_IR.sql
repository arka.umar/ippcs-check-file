/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : LIV w/ GR-IR table
*/

CREATE PROCEDURE [dbo].[SP_DLV_Sync_TB_T_DLV_INV_UPLOAD_GR_IR]
   @LPCD as VARCHAR(4),
   @UserId as varchar(20),
   @pid as bigint
AS
BEGIN
	begin try

	--'PYL', 'PYL.TEST','201404020002'
	--DECLARE @LPCD as varchar(4) = 'PYL'
	--DECLARE @UserId as varchar(20) = 'PYL.TEST'
	--DECLARE @pid as bigint = 201404050002
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @process_status AS INT = 0
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_Sync_TB_T_DLV_INV_UPLOAD_GR_IR'
	DECLARE @step AS VARCHAR(50)
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)
	DECLARE @MODULE_ID as varchar(1) = '3'
	DECLARE @FUNCTION_ID as varchar(5) = '31401'
	DECLARE @sts as varchar(2)
	DECLARE @count as int
	DECLARE @count_exist as int
	DECLARE @isError as int = 0
	DECLARE @isError2 as int = 0
	DECLARE @isError3 as int = 0
	DECLARE @isError4 as int = 0
	DECLARE @errorTxt as varchar(max) = '';
	DECLARE @CHECK_TUPLOAD INT, @COUNT_TUPLOAD INT;

	SET @log = 'Sync with GR_IR';
	SET @step = 'InitializeSyncProcess';
	SET @steps = @na+'.'+@step;
	INSERT @T
	EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
	


	SET @COUNT_TUPLOAD = (SELECT COUNT (*) FROM TB_T_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid);

	SET @CHECK_TUPLOAD = (SELECT COUNT(*) FROM TB_T_DLV_INV_UPLOAD WHERE REMARKS = 'Incorrect' AND PROCESS_ID = @pid); --added process_id fid.deny 2015-08-03

	
	IF (@COUNT_TUPLOAD = 0 ) BEGIN 
			SET @log = 'No data in TB_T_DLV_INV_UPLOAD';
			SET @step = 'TerminatingSyncProcess';
			SET @steps = @na+'.'+@step;
			INSERT @T
			EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00010ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;
			
			SET @log = 'LIV process failed';
			SET @step = 'TerminatingSyncProcess';
			SET @steps = @na+'.'+@step;
			INSERT @T
			EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00010ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;
			UPDATE TB_R_DLV_QUEUE set PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
		
	END 
	ELSE BEGIN 
			IF (@CHECK_TUPLOAD > 0 ) BEGIN

					--DELETE FROM TB_T_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid
					UPDATE TB_R_DLV_SERVICE_DOC SET  PROCESS_ID = NULL WHERE PROCESS_ID = @pid
					
					SET @errorTxt = @errorTxt + 'Same data already exist in TB_R_DLV_INV_UPLOAD. ';
					
					SET @log = 'Some data are incorrect, reason : '+@errorTxt;
					SET @step = 'TerminatingSyncProcess';
					SET @steps = @na+'.'+@step;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00010ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;
										
					SET @log = 'LIV process failed';
					SET @step = 'TerminatingSyncProcess';
					SET @steps = @na+'.'+@step;
					INSERT @T
					EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00010ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;
					UPDATE TB_R_DLV_QUEUE set PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
					
					
			END 
			ELSE BEGIN

					SELECT

							DELIVERY_NO, 
							PO_NO,
							PO_ITEM_NO, 
							COMP_PRICE_CD, 
							LP_INV_NO, 
							ROUTE_CD, 
							LP_CD, 
							SERVICE_DOC_NO,
							DOC_DT, 
							QTY, 
							S_QTY, 
							PRICE_AMT, 
							PAYMENT_CURR, 
							REMARKS,
							VALIDATION_TYPE,
							INV_NO,
							INV_DT,
							INV_POSTING_BY,
							INV_POSTING_DT,
							INV_AMT,
							INV_AMT_LOCAL_CURR, 
							CONDITION_CATEGORY,
							CURR_CD,
							PARTNER_BANK_KEY,
							PAYMENT_TERM_CD, 
							PAYMENT_METHOD_CD, 
							INV_TEXT,
							INV_TAX_CD,
							INV_TAX_AMT,
							PROCESS_ID,
							BASELINE_DT,
							CERTIFICATE_ID,
							SUBMIT_BY,
							SUBMIT_DT,
							UOM,
							CALC_TAX_FLAG,
							REVERSE_FLAG,
							IN_PROGRESS,
							STATUS_CD,
							CREATED_BY,
							CREATED_DT

							INTO #T_UPLOAD FROM(

									SELECT 
									DISTINCT b.DELIVERY_NO, 
									b.PO_NO,
									b.PO_ITEM_NO, 
									a.COMP_PRICE_CD, 
									b.LP_INV_NO, 
									b.ROUTE_CD, 
									b.LP_CD, 
									b.SERVICE_DOC_NO,
									b.DOC_DT, 
									b.QTY, 
									'1' as S_QTY, 
									b.PRICE_AMT as PRICE_AMT, 
									b.PAYMENT_CURR, 
									--'correct' as REMARKS,
									-- 28/04/2014 --
									b.REMARKS as REMARKS,
									--===========--
									NULL as VALIDATION_TYPE,
									NULL as INV_NO,
									NULL as INV_DT,
									NULL as INV_POSTING_BY,
									NULL as INV_POSTING_DT,
									(SELECT PO_DETAIL_PRICE FROM TB_R_DLV_GR_IR x WHERE x.PO_NO = b.PO_NO AND x.CONDITION_CATEGORY = 'H' AND x.REFF_NO = b.DELIVERY_NO AND x.PO_ITEM_NO = b.PO_ITEM_NO AND x.SERVICE_DOC_NO = b.SERVICE_DOC_NO AND x.CANCEL_STS IS NULL) as INV_AMT,
									(SELECT PO_DETAIL_PRICE FROM TB_R_DLV_GR_IR x WHERE x.PO_NO = b.PO_NO AND x.CONDITION_CATEGORY = 'H' AND x.REFF_NO = b.DELIVERY_NO AND x.PO_ITEM_NO = b.PO_ITEM_NO AND x.SERVICE_DOC_NO = b.SERVICE_DOC_NO AND x.CANCEL_STS IS NULL) as INV_AMT_LOCAL_CURR, 
									a.CONDITION_CATEGORY,
									a.ITEM_CURR as CURR_CD, 
									1 as PARTNER_BANK_KEY,
									e.PAYMENT_TERM_CD, 
									e.PAYMENT_METHOD_CD, 
									NULL as INV_TEXT,
									(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE TB_M_SYSTEM.SYSTEM_CD = 'DEFAULT_TAX_CODE') as INV_TAX_CD, 
									d.PRICE as INV_TAX_AMT,
									b.PROCESS_ID as PROCESS_ID,
									NULL as BASELINE_DT,
									NULL as CERTIFICATE_ID,
									NULL as SUBMIT_BY,
									NULL as SUBMIT_DT,
									'RET' as UOM,
									NULL as CALC_TAX_FLAG,
									'0' as REVERSE_FLAG,
									NULL as IN_PROGRESS,
									'1' as STATUS_CD,
									@UserId as CREATED_BY,
									GETDATE() as CREATED_DT 
									FROM TB_T_DLV_INV_UPLOAD b
									LEFT JOIN TB_R_DLV_GR_IR a ON a.REFF_NO = b.DELIVERY_NO AND CANCEL_STS IS NULL
									LEFT JOIN TB_M_LOGISTIC_PARTNER c ON c.LOG_PARTNER_CD = a.LP_CD
									LEFT JOIN TB_R_DLV_PO_ITEM d ON d.PO_NO = b.PO_NO AND d.ROUTE_CD = b.ROUTE_CD
									LEFT JOIN TB_R_DLV_PO_H e ON e.PO_NO = b.PO_NO
									WHERE b.LP_CD = @LPCD AND b.PROCESS_ID = @pid AND a.CANCEL_STS IS NULL AND a.CONDITION_CATEGORY = 'H' 
									AND b.REMARKS = 'Correct'
					)tbl;

					DECLARE @DELIVERY_NO as varchar(13);
					DECLARE @PO_NO as varchar(11);
					DECLARE @PO_ITEM_NO as int;
					DECLARE @COMP_PRICE_CD as varchar(4);
					DECLARE @LP_INV_NO  as varchar(15);
					DECLARE @ROUTE_CD as varchar(4);
					DECLARE @LP_CD as varchar(4);
					DECLARE @SERVICE_DOC_NO as varchar(11);
					DECLARE @DOC_DT as date;
					DECLARE @QTY as int; 
					DECLARE @S_QTY as int;
					DECLARE @PRICE_AMT as numeric(15,2);
					DECLARE @PAYMENT_CURR as varchar(3);
					DECLARE @REMARKS as varchar(2000);
					DECLARE @VALIDATION_TYPE as varchar(1);
					DECLARE @INV_NO as varchar(6);
					DECLARE @INV_DT as datetime;
					DECLARE @INV_POSTING_BY as varchar(20);
					DECLARE @INV_POSTING_DT as datetime;
					DECLARE @INV_AMT as numeric(15,2);
					DECLARE @INV_AMT_LOCAL_CURR as numeric(15,2);
					DECLARE @CONDITION_CATEGORY as varchar(1);
					DECLARE @CURR_CD as varchar(3);
					DECLARE @PARTNER_BANK_KEY as varchar(10);
					DECLARE @PAYMENT_TERM_CD as varchar(4); 
					DECLARE @PAYMENT_METHOD_CD as varchar(2); 
					DECLARE @INV_TEXT as varchar(20);
					DECLARE @INV_TAX_CD as varchar(2);
					DECLARE @INV_TAX_AMT as numeric(15,2);
					DECLARE @PROCESS_ID as BIGINT;
					DECLARE @BASELINE_DT as datetime;
					DECLARE @CERTIFICATE_ID as varchar(26);
					DECLARE @SUBMIT_BY as varchar(20);
					DECLARE @SUBMIT_DT as datetime;
					DECLARE @UOM as varchar(3);
					DECLARE @CALC_TAX_FLAG as varchar(1);
					DECLARE @REVERSE_FLAG as varchar(1);
					DECLARE @IN_PROGRESS as varchar(1);
					DECLARE @STATUS_CD as int;
					DECLARE @CREATED_BY as varchar(20);
					DECLARE @CREATED_DT as datetime;
						
						
						
						DECLARE T_UPLOAD_CURS CURSOR 
						FOR 
						SELECT 
						DELIVERY_NO, 
						PO_NO,
						PO_ITEM_NO, 
						COMP_PRICE_CD, 
						LP_INV_NO, 
						ROUTE_CD, 
						LP_CD, 
						SERVICE_DOC_NO,
						DOC_DT, 
						QTY, 
						S_QTY, 
						PRICE_AMT, 
						PAYMENT_CURR, 
						REMARKS,
						VALIDATION_TYPE,
						INV_NO,
						INV_DT,
						INV_POSTING_BY,
						INV_POSTING_DT,
						INV_AMT,
						INV_AMT_LOCAL_CURR, 
						CONDITION_CATEGORY,
						CURR_CD,
						PARTNER_BANK_KEY,
						PAYMENT_TERM_CD, 
						PAYMENT_METHOD_CD, 
						INV_TEXT,
						INV_TAX_CD,
						INV_TAX_AMT,
						PROCESS_ID,
						BASELINE_DT,
						CERTIFICATE_ID,
						SUBMIT_BY,
						SUBMIT_DT,
						UOM,
						CALC_TAX_FLAG,
						REVERSE_FLAG,
						IN_PROGRESS,
						STATUS_CD,
						CREATED_BY,
						CREATED_DT
						FROM #T_UPLOAD;
						OPEN T_UPLOAD_CURS;
						FETCH NEXT FROM T_UPLOAD_CURS INTO 
						@DELIVERY_NO, 
						@PO_NO,
						@PO_ITEM_NO, 
						@COMP_PRICE_CD, 
						@LP_INV_NO, 
						@ROUTE_CD, 
						@LP_CD, 
						@SERVICE_DOC_NO,
						@DOC_DT, 
						@QTY, 
						@S_QTY, 
						@PRICE_AMT, 
						@PAYMENT_CURR, 
						@REMARKS,
						@VALIDATION_TYPE,
						@INV_NO,
						@INV_DT,
						@INV_POSTING_BY,
						@INV_POSTING_DT,
						@INV_AMT,
						@INV_AMT_LOCAL_CURR, 
						@CONDITION_CATEGORY,
						@CURR_CD,
						@PARTNER_BANK_KEY,
						@PAYMENT_TERM_CD, 
						@PAYMENT_METHOD_CD, 
						@INV_TEXT,
						@INV_TAX_CD,
						@INV_TAX_AMT,
						@PROCESS_ID,
						@BASELINE_DT,
						@CERTIFICATE_ID,
						@SUBMIT_BY,
						@SUBMIT_DT,
						@UOM,
						@CALC_TAX_FLAG,
						@REVERSE_FLAG,
						@IN_PROGRESS,
						@STATUS_CD,
						@CREATED_BY,
						@CREATED_DT;
						
						WHILE @@FETCH_STATUS = 0  
						BEGIN  
						
									--SELECT @LP_CD, @PO_NO,@PO_ITEM_NO,@ROUTE_CD, @DELIVERY_NO,@COMP_PRICE_CD,@DOC_DT,@QTY,@PAYMENT_CURR,@PRICE_AMT
									IF(@LP_INV_NO IS NULL OR @LP_INV_NO = '') BEGIN
										SET @isError3 = 1;
										SET @errorTxt = @errorTxt + 'LP_INV_NO IS NULL. ';
									END
									
									
									DECLARE @SAME as int = (
										SELECT COUNT(*) FROM TB_R_DLV_GR_IR a 
										LEFT JOIN TB_R_DLV_SERVICE_DOC b ON b.SERVICE_DOC_NO = a.SERVICE_DOC_NO 
										WHERE 
										a.LP_CD = CAST(@LP_CD as varchar(3))
										AND a.PO_NO = cast(@PO_NO as varchar(13))
										AND a.PO_ITEM_NO = CAST(@PO_ITEM_NO as INT)
										AND b.ROUTE_CD = CAST(@ROUTE_CD as varchar(4))
										AND a.REFF_NO = CAST(@DELIVERY_NO as varchar(13)) 
										AND a.COMP_PRICE_CD = CAST(@COMP_PRICE_CD as varchar(4))
										AND a.DOC_DT=CAST(@DOC_DT as date)
										AND b.TRANSACTION_QTY = cast(@QTY as int)
										AND b.TRANSACTION_CURR = cast(@PAYMENT_CURR as varchar(3))
										AND a.PO_DETAIL_PRICE =CAST(@PRICE_AMT as NUMERIC) 
										AND a.GR_IR_AMT = CAST(@PRICE_AMT as NUMERIC) * CAST(@QTY as int)
									)
									
									--SELECT 'SIMILAR : '+CAST(@SAME as varchar(max));
									
									
									IF(@SAME=0) BEGIN
											SET @isError2 = 1;
											SET @errorTxt = @errorTxt + 'Data Missmatch on DELIVERY_NO : '+@DELIVERY_NO+'. ';
											-- 28/04/2014 --
											UPDATE TB_T_DLV_INV_UPLOAD SET REMARKS = 'Incorrect' WHERE SERVICE_DOC_NO=@SERVICE_DOC_NO AND DELIVERY_NO=@DELIVERY_NO;
											--============--
									END
																		
									EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
										
										IF(@STATUS_CD = 1 OR @STATUS_CD = -1 OR @STATUS_CD = -3) BEGIN
												SET @count_exist = (SELECT COUNT(*) FROM TB_R_DLV_INV_UPLOAD WHERE PO_NO = @PO_NO AND PO_ITEM_NO = @PO_ITEM_NO AND DELIVERY_NO = @DELIVERY_NO AND COMP_PRICE_CD = @COMP_PRICE_CD AND STATUS_CD NOT IN ('1', '-1', '-3'))
												IF(@count_exist>0) BEGIN

														SET @log = 'Data is exist in TB_R_DLV_INV_UPLOAD with : PO_NO '+@PO_NO+', PO_ITEM_NO:'+CAST(@PO_ITEM_NO as varchar(max))+', DELIVERY_NO:'+@DELIVERY_NO+', COMP_PRICE_CD:'+@COMP_PRICE_CD;
														SET @step = 'SyncProcess';
														SET @steps = @na+'.'+@step;
														INSERT @T
														EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00005ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;				
														SET @isError4 = 1;
														SET @errorTxt = @errorTxt + 'Data already exist on TB_R_DLV_INV_UPLOAD on DELIVERY_NO : '+@DELIVERY_NO+', PO_NO : '+@PO_NO+', PO_ITEM_NO : '+CAST(@PO_ITEM_NO as varchar(max))+', COMP_PRICE_CD : '+@COMP_PRICE_CD+'. ';
												END
												ELSE BEGIN
														SET @log = 'Insert into TB_R_DLV_INV_UPLOAD with : PO_NO '+@PO_NO+', PO_ITEM_NO:'+CAST(@PO_ITEM_NO as varchar(max))+', DELIVERY_NO:'+@DELIVERY_NO+', COMP_PRICE_CD:'+@COMP_PRICE_CD;
														SET @step = 'SyncProcess';
														SET @steps = @na+'.'+@step;
														INSERT @T
														EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;				
												END
										END
										ELSE BEGIN
											SET @isError = 1;
											SET @errorTxt = @errorTxt + 'Data already created or STATUS_CD not in 1, -1, -3. '; 
										END
										
									UPDATE TB_R_DLV_GR_IR SET LIV_FLAG = 1 WHERE REFF_NO = @DELIVERY_NO AND CANCEL_STS IS NULL
							
						
							
						FETCH NEXT FROM T_UPLOAD_CURS INTO 
							@DELIVERY_NO, 
							@PO_NO,
							@PO_ITEM_NO, 
							@COMP_PRICE_CD, 
							@LP_INV_NO, 
							@ROUTE_CD, 
							@LP_CD, 
							@SERVICE_DOC_NO,
							@DOC_DT, 
							@QTY, 
							@S_QTY, 
							@PRICE_AMT, 
							@PAYMENT_CURR, 
							@REMARKS,
							@VALIDATION_TYPE,
							@INV_NO,
							@INV_DT,
							@INV_POSTING_BY,
							@INV_POSTING_DT,
							@INV_AMT,
							@INV_AMT_LOCAL_CURR, 
							@CONDITION_CATEGORY,
							@CURR_CD,
							@PARTNER_BANK_KEY,
							@PAYMENT_TERM_CD, 
							@PAYMENT_METHOD_CD, 
							@INV_TEXT,
							@INV_TAX_CD,
							@INV_TAX_AMT,
							@PROCESS_ID,
							@BASELINE_DT,
							@CERTIFICATE_ID,
							@SUBMIT_BY,
							@SUBMIT_DT,
							@UOM,
							@CALC_TAX_FLAG,
							@REVERSE_FLAG,
							@IN_PROGRESS,
							@STATUS_CD,
							@CREATED_BY,
							@CREATED_DT;
						END;
						CLOSE T_UPLOAD_CURS;
						DEALLOCATE T_UPLOAD_CURS;
						drop table #T_UPLOAD;
						

						SELECT @isError2

						IF(@isError=0 AND @isError2=0 AND @isError3=0 AND @isError4=0) BEGIN
								INSERT INTO TB_R_DLV_INV_UPLOAD(DELIVERY_NO, PO_NO,PO_ITEM_NO,  COMP_PRICE_CD, SEQ_NO, LP_INV_NO, ROUTE_CD, LP_CD, SERVICE_DOC_NO,DOC_DT, QTY, S_QTY, PRICE_AMT, PAYMENT_CURR, REMARKS,VALIDATION_TYPE,INV_NO,INV_DT,INV_POSTING_BY,INV_POSTING_DT,INV_AMT,INV_AMT_LOCAL_CURR, CONDITION_CATEGORY, CURR_CD, PARTNER_BANK_KEY,PAYMENT_TERM_CD, PAYMENT_METHOD_CD, INV_TEXT,INV_TAX_CD,INV_TAX_AMT,PROCESS_ID,	BASELINE_DT,CERTIFICATE_ID, SUBMIT_BY,SUBMIT_DT,UOM,CALC_TAX_FLAG,REVERSE_FLAG,IN_PROGRESS,STATUS_CD,CREATED_BY,CREATED_DT)
								SELECT 
								DISTINCT b.DELIVERY_NO, 
								b.PO_NO,
								b.PO_ITEM_NO, 
								a.COMP_PRICE_CD,
								ISNULL((SELECT MAX(si.SEQ_NO) + (CASE WHEN si.STATUS_CD = '-3' THEN 1 ELSE 0 END) AS SEQ_NO FROM TB_R_DLV_INV_UPLOAD si WHERE si.DELIVERY_NO = b.DELIVERY_NO AND si.COMP_PRICE_CD = a.COMP_PRICE_CD GROUP BY si.STATUS_CD), '1'), 
								b.LP_INV_NO, 
								b.ROUTE_CD, 
								b.LP_CD, 
								b.SERVICE_DOC_NO,
								b.DOC_DT, 
								b.QTY, 
								'1' as S_QTY, 
								a.PO_DETAIL_PRICE as PRICE_AMT, 
								b.PAYMENT_CURR,
								b.REMARKS as REMARKS,
								--'correct' as REMARKS,
								NULL as VALIDATION_TYPE,
								NULL as INV_NO,
								NULL as INV_DT,
								NULL as INV_POSTING_BY,
								NULL as INV_POSTING_DT,
								CASE WHEN a.CONDITION_CATEGORY = 'H' THEN (SELECT PO_DETAIL_PRICE FROM TB_R_DLV_GR_IR x WHERE x.PO_NO = b.PO_NO AND x.CONDITION_CATEGORY = 'H' AND x.REFF_NO = b.DELIVERY_NO AND x.PO_ITEM_NO = b.PO_ITEM_NO AND x.SERVICE_DOC_NO = b.SERVICE_DOC_NO AND x.CANCEL_STS IS NULL) ELSE NULL END as INV_AMT,
								CASE WHEN a.CONDITION_CATEGORY = 'H' THEN (SELECT PO_DETAIL_PRICE FROM TB_R_DLV_GR_IR x WHERE x.PO_NO = b.PO_NO AND x.CONDITION_CATEGORY = 'H' AND x.REFF_NO = b.DELIVERY_NO AND x.PO_ITEM_NO = b.PO_ITEM_NO AND x.SERVICE_DOC_NO = b.SERVICE_DOC_NO AND x.CANCEL_STS IS NULL) ELSE NULL END as INV_AMT_LOCAL_CURR, 
								a.CONDITION_CATEGORY,
								a.ITEM_CURR as CURR_CD, 
								1 as PARTNER_BANK_KEY,
								e.PAYMENT_TERM_CD, 
								e.PAYMENT_METHOD_CD, 
								NULL as INV_TEXT,
								CASE WHEN a.CONDITION_CATEGORY = 'H' THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE TB_M_SYSTEM.SYSTEM_CD = 'DEFAULT_TAX_CODE') ELSE NULL END as INV_TAX_CD, 
								CASE WHEN a.CONDITION_CATEGORY = 'H' THEN (
									SELECT PO_DETAIL_PRICE FROM TB_R_DLV_GR_IR x 
									WHERE x.PO_NO = b.PO_NO AND x.CONDITION_CATEGORY = 'D' AND x.REFF_NO = b.DELIVERY_NO AND x.PO_ITEM_NO = b.PO_ITEM_NO 
										AND TAX_CD = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE TB_M_SYSTEM.SYSTEM_CD = 'DEFAULT_TAX_CODE')
										AND x.SERVICE_DOC_NO = b.SERVICE_DOC_NO AND x.CANCEL_STS IS NULL
								) ELSE NULL END as INV_TAX_AMT,
								b.PROCESS_ID as PROCESS_ID,
								NULL as BASELINE_DT,
								NULL as CERTIFICATE_ID,
								NULL as SUBMIT_BY,
								NULL as SUBMIT_DT,
								'RET' as UOM,
								NULL as CALC_TAX_FLAG,
								'0' as REVERSE_FLAG,
								NULL as IN_PROGRESS,
								'1' as STATUS_CD,
								@UserId as CREATED_BY,
								GETDATE() as CREATED_DT 
								FROM TB_T_DLV_INV_UPLOAD b
								LEFT JOIN TB_R_DLV_GR_IR a ON a.REFF_NO = b.DELIVERY_NO AND CANCEL_STS IS NULL
								LEFT JOIN TB_M_LOGISTIC_PARTNER c ON c.LOG_PARTNER_CD = a.LP_CD
								LEFT JOIN TB_R_DLV_PO_ITEM d ON d.PO_NO = b.PO_NO AND d.ROUTE_CD = b.ROUTE_CD
								LEFT JOIN TB_R_DLV_PO_H e ON e.PO_NO = b.PO_NO
								WHERE b.LP_CD = @LPCD AND b.PROCESS_ID = @pid AND a.CANCEL_STS IS NULL	
								AND b.REMARKS = 'Correct'
								
								
								UPDATE TB_R_DLV_QUEUE set PROCESS_STATUS = 'QU3', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
								
								SET @log = 'Verfication completed';
								SET @step = 'TerminatingSyncProcess';
								SET @steps = @na+'.'+@step;
								INSERT @T
								EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODULE_ID, @FUNCTION_ID, 1;
								UPDATE TB_R_DLV_QUEUE set PROCESS_STATUS = 'QU3', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
								UPDATE TB_R_DLV_INV_UPLOAD SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
						END
						ELSE BEGIN
								SET @log = 'Data missmatch, terminating process reason : '+@errorTxt;
								SET @step = 'TerminatingSyncProcess';
								SET @steps = @na+'.'+@step;
								INSERT @T
								EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00010ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;
						
								SET @log = 'LIV process failed';
								SET @step = 'TerminatingSyncProcess';
								SET @steps = @na+'.'+@step;
								INSERT @T
								EXEC dbo.sp_PutLog @log, @UserId, @steps, @pid OUTPUT, 'MPCS00010ERR', 'ERR', @MODULE_ID, @FUNCTION_ID, 1;
								UPDATE TB_R_DLV_QUEUE set PROCESS_STATUS = 'QU4', PROCESS_END_DT = GETDATE(),CHANGED_BY = @UserId, CHANGED_DT = GETDATE() WHERE PROCESS_ID = @pid
						END

						
						
			END
	END
		UPDATE TB_R_DLV_SERVICE_DOC SET  PROCESS_ID = NULL WHERE PROCESS_ID = @pid
		--DELETE FROM TB_T_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid
	end try
	begin catch
	declare @state int =ERROR_STATE(),
			@line int=error_line(),
			@message varchar(max)='GRIRsync: error '+error_message()+' line'+cast(error_line() as varchar(100))
	RAISERROR('GRIRsync: ' ,@state,@line,@message)
	end catch
END

