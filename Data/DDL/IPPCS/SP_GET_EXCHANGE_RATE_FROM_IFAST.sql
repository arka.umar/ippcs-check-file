
/****** Object:  StoredProcedure [dbo].[SP_GET_EXCHANGE_RATE_FROM_IFAST]    Script Date: 30/04/2021 16:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****************************************************************/
/* Revision No      : 1											*/
/* Create by        : FID) Ridwan								*/
/* Create date		: 04 - 04 - 2020							*/
/* Description		: Get Exchange rate from IFAST				*/
/* Modified by		:											*/
/* Modified date	:											*/
/* Description		:											*/
/****************************************************************/
CREATE PROCEDURE [dbo].[SP_GET_EXCHANGE_RATE_FROM_IFAST]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--log header
	DECLARE @FUNCTION_ID AS VARCHAR(10);
	DECLARE @MODULE_ID AS VARCHAR(10);
	DECLARE @PROCESS_STATUS AS TINYINT;
	DECLARE @USER_ID VARCHAR(50) = 'System';
	--log detail
	DECLARE @PROCESS_ID AS varchar(100);
	DECLARE @SEQ_NO AS BIGINT;
	DECLARE @MESSAGE_ID AS VARCHAR(12);
	DECLARE @MESSAGE_TYPE AS VARCHAR(3);
	DECLARE @MESSAGE AS VARCHAR(MAX);
	DECLARE @LOCATION AS VARCHAR(MAX);
	--System Type ICS	
	DECLARE @ICSSERVER VARCHAR(50)
		,@ICSDB VARCHAR(50)
		,@IsError VARCHAR(1) = 'N'
		,@TOT_REC INT
		,@TOT_ERROR INT
		,@TOT_SUCCESS INT
		,@MSG_TXT VARCHAR(MAX)
		,@MSG_TYPE VARCHAR(MAX)
		,@MSG VARCHAR(MAX)
		,@NR_ERR INT
		,@N_ERR AS INT = 0;

	-- Openquery String 
	DECLARE @QUERY VARCHAR(MAX);

	BEGIN TRY
		SET @FUNCTION_ID = '71020';
		SET @MODULE_ID = '7';
		SET @PROCESS_STATUS = 4;
		SET @LOCATION = 'Get Exchange rate from IFAST';

		IF OBJECT_ID('tempdb..#pid') IS NOT NULL 
		DROP TABLE #pid

		CREATE TABLE #pid (pid VARCHAR(20))

		-- Insert Log Header
		INSERT INTO #pid 
		EXEC sp_PutLog 'Start Process Get Exchange rate from IFAST', 'system', 'Get Exchange rate', '',
						'MPCS00004INF', '', @MODULE_ID, @FUNCTION_ID, '0'

		SELECT TOP 1 @PROCESS_ID = pid FROM #pid

		SELECT @ICSSERVER = SYSTEM_VALUE
		FROM [TB_M_SYSTEM]
		WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
			AND SYSTEM_CD = 'LINKED_SERVER';

		SELECT @ICSDB = SYSTEM_VALUE
		FROM [TB_M_SYSTEM]
		WHERE FUNCTION_ID = 'IPPCS_TO_ICS'
			AND SYSTEM_CD = 'DB_NAME';

		/*1. End Insert Log H & D*/
		
		/*INSERT DATA TEMPERORY*/
		SET @QUERY = 'SELECT [CURR_CD],
						VALID_FROM_DT,
						[EXCHANGE_RATE],
						VALID_TO_DT,
						[CREATED_BY],
						[CREATED_DT]
				FROM OPENQUERY(' + @ICSSERVER + ', ''SELECT [CURR_CD],
						VALID_FROM_DT,
						[EXCHANGE_RATE],
						VALID_TO_DT,
						[CREATED_BY],
						[CREATED_DT] FROM ' + @ICSDB + 'TB_M_EXCHANGE_RATE WHERE CONVERT(VARCHAR, VALID_TO_DT, 112) = ''''99991231'''' '') B
				';
				
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Inserting data from IFAST to Temporary'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOCATION
			,@USER_ID
			,GETDATE()

		DELETE FROM TB_T_EXCHANGE_RATE

		INSERT INTO TB_T_EXCHANGE_RATE
		(
			CURRENCY_CD,
			VALID_DT_FROM,
			EXCHANGE_RATE,
			VALID_DT_TO,
			CREATED_BY,
			CREATED_DT
		)
		EXEC (@QUERY)


		/*END INSERT DATA TEMPERORY*/
		IF (
				(
					SELECT COUNT(1)
					FROM TB_T_EXCHANGE_RATE
					) > 0
				)
		BEGIN
			EXEC dbo.CommonGetMessage 'MPCS00002INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Inserting data from Temporary to Master'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00002INF'
				,'INF'
				,'MPCS00002INF : ' + @MSG_TXT
				,@LOCATION
				,@USER_ID
				,GETDATE()

			UPDATE a
			SET a.VALID_DT_TO = dateadd(DAY, -1,b.VALID_DT_FROM)
				,a.CHANGED_BY = @USER_ID
				,a.CHANGED_DT = getdate()
			FROM TB_M_EXCHANGE_RATE a
			JOIN (SELECT ROW_NUMBER() OVER(PARTITION BY AA.CURRENCY_CD ORDER BY AA.CURRENCY_CD, AA.VALID_DT_FROM desc) RNO, AA.CURRENCY_CD, AA.VALID_DT_FROM FROM TB_M_EXCHANGE_RATE AA) TA 
				ON A.CURRENCY_CD = TA.CURRENCY_CD
				AND A.VALID_DT_FROM = TA.VALID_DT_FROM
				AND TA.RNO = 1
			JOIN TB_T_EXCHANGE_RATE b ON a.CURRENCY_CD = b.CURRENCY_CD
				and b.VALID_DT_FROM > a.VALID_DT_FROM

			--	where  convert(date,a.VALID_FROM_DT) = convert(date,getdate()-1)
			/*START INSERT DATA*/
			INSERT INTO TB_M_EXCHANGE_RATE
			(
				CURRENCY_CD
				,VALID_DT_FROM
				,EXCHANGE_RATE
				,VALID_DT_TO
				,FOREX_TYPE
				,RELEASE_FLAG
				,CREATED_BY
				,CREATED_DT
				,CHANGED_BY
				,CHANGED_DT
			)
			SELECT CURRENCY_CD
				,VALID_DT_FROM
				,EXCHANGE_RATE
				,VALID_DT_TO
				,'A' FOREX_TYPE
				,'Y' RELEASE_FLAG
				,@USER_ID CREATED_BY
				,getdate() CREATED_DT
				,@USER_ID CHANGED_BY
				,getdate() CHANGED_DT
			FROM TB_T_EXCHANGE_RATE a
			where (CURRENCY_CD + CONVERT(VARCHAR,VALID_DT_FROM,112)) NOT IN (SELECT (CURRENCY_CD + CONVERT(VARCHAR,VALID_DT_FROM,112)) FROM TB_M_EXCHANGE_RATE)
				/*END INSERT DATA*/
		END
		ELSE
		BEGIN
			SET @IsError = 'Y'
			EXEC dbo.CommonGetMessage 'MPCS00001INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'No data found'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00001INF'
				,'INF'
				,'MPCS00001INF : ' + @MSG_TXT
				,@LOCATION
				,@USER_ID
				,GETDATE()

		END

		IF (@IsError = 'Y')
		BEGIN
			EXEC dbo.CommonGetMessage 'MPCS00001INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Process get data Exchange rate is finished with error'

			SET @LOCATION = 'Get data Finish';

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00001INF'
				,'INF'
				,'MPCS00001INF : ' + @MSG_TXT
				,@LOCATION
				,@USER_ID
				,GETDATE()

			UPDATE TB_R_LOG_H
			SET PROCESS_STATUS = 4
			WHERE PROCESS_ID = @PROCESS_ID
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MPCS00001INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Process get data Exchange rate is finished successfully'

			SET @LOCATION = 'Get Data Finish';

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00001INF'
				,'INF'
				,'MPCS00001INF : ' + @MSG_TXT
				,@LOCATION
				,@USER_ID
				,GETDATE()

			UPDATE TB_R_LOG_H
			SET PROCESS_STATUS = 1
			WHERE PROCESS_ID = @PROCESS_ID
		END
			/*End Here 00. End Of History to Log H&D*/
	END TRY

	BEGIN CATCH
		declare @PARAM1 varchar(1000)
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());

		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOCATION
			,@USER_ID
			,GETDATE()
	END CATCH
END
