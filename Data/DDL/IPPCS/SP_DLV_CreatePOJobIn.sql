/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Register job for PO creation
*/

--exec [sp_ICS_InvoicePostJobIn] 
CREATE PROCEDURE [dbo].[SP_DLV_CreatePOJobIn] 

@PR_PO_MONTH VARCHAR (7),
@PR_NO VARCHAR (MAX),
@CREATED_BY VARCHAR (25)

AS
BEGIN 

	DECLARE @process_status AS INT = 0
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_CreatePOJobIn'
	DECLARE @step AS VARCHAR(50) = 'init'
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)

	DECLARE @MODUL_ID VARCHAR(1) = '3';
	DECLARE @FUNCTION_ID VARCHAR(10) = '31202';

	DECLARE @err_message nvarchar(255);

	DECLARE @CHECK_MODULE INT;



	SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID=@FUNCTION_ID AND PROCESS_END_DT IS NULL );
	

	IF (@CHECK_MODULE=0) BEGIN
			
			--CREATE LOG
			SET @steps = @na+'.'+@step;
			SET @log = 'EXEC SP_DLV_CreatePOJobIn';
			
			EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
			
			INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
			VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());
			
			--CREATE TEMPORARY TABLE UNTUK MENAMPUNG SEMUA PARAMETER DAN MEMBUAT PROCESS ID UNTUK PARAMETER TERSEBUT---------
			--LOOPING BERDASARKAN PO NO
	CREATE TABLE #TEMP(
			PR_NO VARCHAR(MAX),
			PR_STATUS VARCHAR(MAX)
		);


		--==Temporary==--
		--  == VARIABLE TABLE TEMPORARY UNTUK EXPLODING GRID ==
		DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
		DECLARE @MIN_ROW INT;
		DECLARE @ROW_DETAIL VARCHAR(MAX);
		DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
		DECLARE @MIN_FIELD INT;
		-- ================================================================
		--  == Variabel Untuk Menyimpan Data Ke TABLE DETAIL ==
		DECLARE @arr_1 VARCHAR(MAX),@arr_2 VARCHAR(MAX)
			
		INSERT INTO @ROWS
		SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@PR_NO,';');
		-- ================================================================
		--  == Ambil Nilai terkecil dari VARIABEL TABLE @ROW ==
		SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

		WHILE @MIN_ROW IS NOT NULL BEGIN
					-- 6.3.1 == Hapus VARIABLE TABLE @FIELDS jika ada ==
					DELETE FROM @FIELDS;
					
					SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
					INSERT INTO @FIELDS
					SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'|');
					-- ========================================================================================
					-- 6.3.3 == Ambil ITEM Berdasarkan ID untuk dimasukkan kedalam VARIABLE FIELD Detail ==
					SET @arr_1 = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
					SET @arr_2 = (SELECT ITEM FROM @FIELDS WHERE ID = 2);
					INSERT INTO #TEMP VALUES (@arr_1,@arr_2)

					SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
									-- =================================================================================
		END --END WHILE
		--==Temporary==--
		
		--UPDATE TABLE PR DENGAN PROCESS ID---
		UPDATE TB_R_DLV_PR_H SET PROCESS_ID = @pid WHERE PR_NO IN(SELECT PR_NO FROM #TEMP)
		--END---------------------------------
	
			--END------------------------------------------------------------------------------------------------------------

			--SELECT 'registry'
				INSERT INTO TB_R_BACKGROUND_TASK_REGISTRY
				VALUES (
				@pid,--ID
				'DeliveryCreatePOTaskRuntime',--NAME
				'Background Task Process Create PO ',--DESCRIPTION
				'System',	--SUBMITTER
				'PO Creation Process',	--FUNCTION_NAME
				'{&quot;productionMonth&quot;:&quot;'+@PR_PO_MONTH+'&quot;,'+
					'&quot;Username&quot;:&quot;'+@CREATED_BY+'&quot;,'+
					--'&quot;PoNumber&quot;:&quot;'+@PR_NO+'&quot;,'+
				  '&quot;ProcessID&quot;:&quot;' +CAST(@pid as varchar)+ '&quot;}',--PARAMETER
					0,	--TYPE
					0,	--STATUS
				--'D:\ATD\IPPCS App\IPPCS\BackgroundManager\DeliveryCreatePRTaskRuntime\bin\Debug\DeliveryCreatePRTaskRuntime.exe',--COMMAND
				'C:\Background_Task\Tasks\DeliveryCreatePOTaskRuntime.exe',--COMMAND
				0,       --START_DATE
				0,		--END_DATE
				
				4,		--PERIODIC_TYPE
				null,		--INTERVAL
				'',		--EXECUTION_DAYS
				'',	--EXECUTION_MONTHS
				6840000 
				)

			UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2'
			--, PROCESS_END_DT=GETDATE()
			WHERE PROCESS_ID = @pid;
	END
	ELSE BEGIN
			SET @err_message = 'Same process still runnning with process ID <b>'+ CONVERT (varchar (MAX), @pid) +'</b>, please  wait until it finish.';
			RAISERROR(@err_message,16,1)
			RETURN
	END
			
	
END;

