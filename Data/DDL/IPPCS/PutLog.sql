ALTER PROCEDURE [dbo].[PutLog]
    @what VARCHAR(MAX) =null,
    @user VARCHAR(20) = null,
    @where VARCHAR(512) = null,
    @pid BIGINT OUTPUT ,
    @id VARCHAR(12) = 'INF',
    @func VARCHAR(20) = NULL,
    @sts TINYINT = 0,
    @v0 varchar(200) = null,
    @v1 varchar(200) = null,
    @v2 varchar(200) = null,
    @v3 varchar(200) = null,
    @v4 varchar(200) = null

AS
BEGIN TRY
  SET NOCOUNT ON;
  IF (@what IS NULL AND @user IS NULL AND @where IS NULL)
      RETURN;
  declare
    @t VARCHAR(3)=null
  , @u VARCHAR(20)
  , @m VARCHAR(10) = NULL
  , @now DATETIME;
  select
      @now = GETDATE()
    , @u = COALESCE(NULLIF(@user, ''), ORIGINAL_LOGIN())
    , @t = case
         when @id IS NOT NULL AND LEN(@id) >3
         then SUBSTRING(@id, LEN(@id)-2, 3)
         else 'INF'
         end
    , @sts = COALESCE(@sts, 
			     ( -- try to get PROCESS_STATUS from last 3 char of MESSAGE_ID usually on
				  select sts =
                    case when i.stsi > 0
                         then convert(tinyint,round((i.stsi -1 ) /4,0))
                         else 1
                         end              -- 123456789012345678901234567890123456789012345678901
                 from (select CHARINDEX(@t, 'INF_ERR_WRN_SUS.ABO_PRO.END.HOL.XXX.XXY.XYZ.CON_') stsi) i))
    , @m = COALESCE(
			(select top 1 module_id from tb_m_function where function_id = @func)
			,left(@func,1)
			,''
			)

/* 
	process_status 
	0-6 refer to Toyota.Common.Web.Util.ProgressStatus CommonWebLibrary\Util\ProgressStatus 
	7   refer to LogMonitoring status in SupplierPortal\COntrollers\LogMonitoringController

	from current TB_M_MESSAGE.MESSAGE_ID only ends with INF ERR WRN ABO CON 
	 
	process_status abbr  description
	---------------------------------
      0 INF INFO, SUCCESS 
	  1 ERR ERROR 
	  2 WRN (success with) WARNING 
	  3 SUS SUSPENDED 
	  4 ABO ABORTED
	  5 PRO PROCESSING
	  6 END process ENDED 
	  7 HOL On Hold
     11 CON CONsole question 
  */

  IF (@pid IS NULL) OR (@pid < 1)
  BEGIN
    WITH p as (select 10000 MUL),
        q as (
            select (convert(bigint
                , convert(varchar(8), getdate(), 112)) * p.mul) [starten]
              from p),
        r as (select q.starten, (q.starten + p.mul-1) [enden]
              from q join p on 1=1)
        SELECT @PID= COALESCE(
            (SELECT TOP 1 PROCESS_ID
               FROM TB_R_LOG_H
              WHERE PROCESS_ID between r.starten and r.enden
           order by process_id desc)
          , r.starten)+1
          from r;
   END;

    MERGE INTO TB_R_LOG_H h
  USING (select @pid pid)  p 
  on p.pid = h.PROCESS_ID 
  WHEN NOT MATCHED THEN 
    INSERT (PROCESS_ID, [START_DATE], MODULE_ID, FUNCTION_ID, PROCESS_STATUS,  
       CREATED_BY, CREATED_DATE, END_DATE)
    VALUES 
      (P.pid, @now, COALESCE(@m,''), COALESCE(@func,''), @sts, 
	   @u, @now, @now)
  WHEN MATCHED THEN 
    UPDATE SET
          FUNCTION_ID = COALESCE(NULLIF(@func,''), FUNCTION_ID, '')
		, MODULE_ID = COALESCE(NULLIF(@m, ''), MODULE_ID, '') 
        , PROCESS_STATUS = COALESCE(@sts, PROCESS_STATUS, 0)           
        , END_DATE = @now
        , CHANGED_DATE = @now
        , CHANGED_BY = @u; 
        
  INSERT dbo.TB_R_LOG_D (process_id, SEQUENCE_NUMBER, MESSAGE_ID, MESSAGE_TYPE,
    [LOCATION], [MESSAGE], CREATED_DATE, CREATED_BY)
  SELECT x.process_id,
    COALESCE((
      SELECT  MAX(d.SEQUENCE_NUMBER) MAX_SEQ
      FROM dbo.TB_R_LOG_D d WHERE process_id = @pid)
      , 0) + 1 [sequence_number]
   , COALESCE(@id,'MPCS00001INF') [message_id]
   , @t [message_type]
   , x._where [location]
   , coalesce( x.txt, x._what, '?') [message]
   , @now [created_date]
   , @u [created_by]
  FROM (
    SELECT
        process_id = @pid
      , _what   = NULLIF(@what,'')
      , _where  = COALESCE(@where , '')
      , dbo.fn_StringFormat( 
	  COALESCE((
    case when NULLIF(@id,'') is null THEN @what
         when nullif(@id,'') is not null then
            (select top 1 NULLIF(message_text,'{0}') from tb_m_message where message_id = @id)
        else null
        END), @what, @V0, '{0} {1} {2} {3} {4}') , @v0, @v1, @v2, @v3, @v4) txt
  ) x
  WHERE  @pid IS NOT null;
END TRY
BEGIN CATCH
       PRINT COALESCE(ERROR_MESSAGE(),'') + ' @ ' + COALESCE(ERROR_PROCEDURE(),'') + ' '
      + COALESCE(CONVERT(VARCHAR(10), ERROR_LINE()),'');
END CATCH
;

GO