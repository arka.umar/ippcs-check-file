-- ==============================================
-- Created By	: Fid.Joko
-- Created Date : 2013-09-06
-- Created For	: Report Invoice
-- Modified By	: wot.mahfudin
-- Modified Date: 2020-10-10
-- ==============================================
ALTER PROCEDURE [dbo].[SP_RepInvoice_Certificate_Print]
	@InvoiceNos varchar(max) = NULL,
	@InvoiceDates varchar(max) = NULL,
	@SupplierCodes varchar(max)
AS
BEGIN
SET NOCOUNT ON;

DECLARE 
	@plid table (PROCESS_ID BIGINT)

DECLARE @pid BIGINT = 0
	, @wh VARCHAR(25) = 'SP_RepInvoice_Certificate_Print'
	, @@log VARCHAR(MAX);

SET @@log = @wh + ' ''' + ISNULL(@InvoiceNos, '') + ''', ''' 
	+ ISNULL(@InvoiceDates,'') + ''', ''' + 
	+ ISNULL(@SupplierCodes,'') + '''';
	
INSERT @plid (PROCESS_ID) 
EXEC dbo.sp_PutLog @@log, 'trdx', @wh, @pid OUTPUT, 'MPCS00002INF', 'INF', '5', '53001', 0; 

DECLARE @CERTIFICATE_PRINT TABLE (
	SUPP_INV_NO VARCHAR(12),
	INV_DT DATETIME,
	INVOICE_AMOUNT NUMERIC(16,2),
	SUPPLIER_CD VARCHAR(MAX),
	INV_TAX_NO VARCHAR(24),
	CURRENCY_CD VARCHAR(3),
	CERTIFICATE_ID VARCHAR(26),
	CERTIFICATE_ID_ACCEPT VARCHAR(26),
	CERTIFICATE_ID_REJECT VARCHAR(26),
	[INV_TAX_DATE] DATETIME,
	INV_TAX_AMOUNT NUMERIC(16,5),
	TURN_OVER NUMERIC(16,2),
	MANUAL_ENTRY CHAR(1), --add MANUAL_ENTRY
	WITHHOLDING_TAX_AMOUNT NUMERIC(16,5), -- add by wot.mahfudin on date 10.10.2020
	[TRANSACTION_TYPE] [varchar](1) -- add by wot.mahfudin
)
DECLARE @iData int, @cntData int,
	@SUPP_INV_NO VARCHAR(12),
	@INV_DT VARCHAR(25),
	@SUPPLIER_CD VARCHAR(6)

DECLARE @tmpSplitStringInvoice TABLE 
( 
	ID int identity,
	column1 VARCHAR(8000)
)
--CREATE UNIQUE CLUSTERED INDEX Idx_Invoice ON #tmpSplitStringInvoice(ID, column1);

DECLARE @tmpSplitStringInvoiceDate TABLE 
( 
	ID int identity,
	column1 VARCHAR(8000)
)
--CREATE UNIQUE CLUSTERED INDEX Idx_InvoiceDate ON #tmpSplitStringInvoiceDate(ID, column1);

DECLARE @tmpSplitStringSupplierCode TABLE 
( 
	ID int identity,
	column1 VARCHAR(8000)
)
--CREATE UNIQUE CLUSTERED INDEX Idx_SupplierCode ON #tmpSplitStringSupplierCode(ID,column1);

INSERT  INTO @tmpSplitStringInvoice(column1)
EXEC dbo.SplitString @InvoiceNos, ','

INSERT  INTO @tmpSplitStringInvoiceDate(column1)
EXEC dbo.SplitString @InvoiceDates, ','

INSERT  INTO @tmpSplitStringSupplierCode(column1)
EXEC dbo.SplitString @SupplierCodes, ','

SET @iData=1
select @cntData=count('') from @tmpSplitStringInvoice

WHILE @iData <=@cntData
BEGIN
	SELECT @SUPP_INV_NO=column1 FROM @tmpSplitStringInvoice WHERE ID=@iData
	SELECT @INV_DT=column1 FROM @tmpSplitStringInvoiceDate WHERE ID=@iData
	SELECT @SUPPLIER_CD=column1 FROM @tmpSplitStringSupplierCode WHERE ID=@iData
	
	
	INSERT INTO @CERTIFICATE_PRINT
	SELECT 
		SUPP_INV_NO=tu.[SUPP_INV_NO]
		  ,INV_DT=tu.[INV_DT]
		  ,INVOICE_AMOUNT=(CASE WHEN MAX(ISNULL(tu.TRANSACTION_TYPE,NULL)) = '3' THEN MAX(ISNULL(tu.INV_AMT_TOTAL,0)) - MAX(ISNULL(tu.WITHHOLDING_TAX_AMOUNT,0)) ELSE MAX(ISNULL(tu.INV_AMT_TOTAL,0)) END)
		  ,SUPPLIER_CD=MAX(tu.SUPP_CD + ' - ' + si.SUPP_NAME)
		  ,INV_TAX_NO=MAX(tu.[INV_TAX_NO])
		  ,CURRENCY_CD=MAX(tu.CURR_CD)
		  ,CERTIFICATE_ID=MAX(tu.[CERTIFICATE_ID])
		  ,CERTIFICATE_ID_ACCEPT=MAX('A')
		  ,CERTIFICATE_ID_REJECT=MAX('R')
		  ,[INV_TAX_DATE]=MAX(INV_TAX_DT)
		  ,INV_TAX_AMOUNT=MAX(ISNULL(tu.INV_TAX_AMOUNT,0))
		  ,TURN_OVER=MAX(ISNULL(tu.TURN_OVER,0))+ISNULL(MAX(ISNULL(t.INV_AMT,0)),0)
		  ,ISNULL(VAT.MANUAL_ENTRY, 'N')
		  ,MAX(ISNULL(tu.WITHHOLDING_TAX_AMOUNT,0)) -- add by wot.mahfudin
		  ,MAX(ISNULL(tu.TRANSACTION_TYPE,NULL)) -- add by wot.mahfudin
	FROM [dbo].TB_R_INV_UPLOAD tu
	INNER JOIN TB_M_SUPPLIER_ICS si ON tu.SUPP_CD=si.SUPP_CD
	
	INNER JOIN [TMMIN_E_FAKTUR].[dbo].TB_R_VAT_IN_H VAT ON tu.INV_TAX_NO = VAT.SAP_TAX_INVOICE_NO --add fid.deny to get MANUAL_ENTRY value

	LEFT JOIN (
		select SUPP_INVOICE_NO,INV_AMT=SUM(INV_AMT)
		from TB_T_INV_GL_ACCOUNT 
		where TAX_CD IN(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM where SYSTEM_CD = 'DEFAULT_TAX_CODE')
		group by SUPP_INVOICE_NO
	)t ON tu.SUPP_INV_NO=t.SUPP_INVOICE_NO
	WHERE tu.[SUPP_INV_NO]=@SUPP_INV_NO 
		AND 
			(ISNULL(@INV_DT,'')='' 
			OR (ISNULL(@INV_DT,'')<>'' 
				AND CONVERT(varchar, tu.INV_DT, 112) = LTRIM(RTRIM(@inv_dt))
				)
			) 
		AND 
		tu.SUPP_CD=@SUPPLIER_CD
	GROUP BY tu.[SUPP_INV_NO],tu.[INV_DT],tu.SUPP_CD, VAT.MANUAL_ENTRY --add VAT.MANUAL_ENTRY
	
	SET @iData = @iData + 1
END
SELECT	SUPP_INV_NO,INV_DT,INVOICE_AMOUNT,SUPPLIER_CD,
		INV_TAX_NO,CURRENCY_CD,CERTIFICATE_ID,CERTIFICATE_ID_ACCEPT,
		CERTIFICATE_ID_REJECT,INV_TAX_DATE,INV_TAX_AMOUNT,TURN_OVER,
		MANUAL_ENTRY,WITHHOLDING_TAX_AMOUNT, TRANSACTION_TYPE,
		ISNULL((SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '010401' AND SYSTEM_REMARK like 'TRANSACTION_TYPE%' AND SYSTEM_CD=TRANSACTION_TYPE), '-') TRANSACTION_TYPE_DESC
FROM @CERTIFICATE_PRINT;
END

