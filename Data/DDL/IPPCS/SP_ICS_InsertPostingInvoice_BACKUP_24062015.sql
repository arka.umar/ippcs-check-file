-- =============================================
-- Author: fid.Joko
-- Create date: 23.09.2013
-- Description:   Insert Invoice Payment ICS
-- Modification by Reggy at 06/11/2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_ICS_InsertPostingInvoice_BACKUP_24062015]
                -- Add the parameters for the stored procedure here
                @processId BIGINT
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @sqlQuery AS VARCHAR(MAX),
		@function VARCHAR(10),
		@INVOICE_NO VARCHAR(16),
		@na VARCHAR(50), 
		@SupplierInvoiceNo VARCHAR(12),
		@SupplierCode VARCHAR(6),
		@FlagSuccess BIT,
		@stepId INT, 
		@ErrorMessage VARCHAR(MAX);
		
	SET @stepId = 1;
	--Added By Reggy, Check if Exist this #ICS Table
	IF OBJECT_ID('tempdb..#ICS_FINISH') IS NOT NULL BEGIN DROP TABLE #ICS_FINISH END;
	CREATE TABLE #ICS_FINISH(INVOICE_NO VARCHAR(16), SupplierInvoiceNo VARCHAR(12), SupplierCode VARCHAR(6))
	
	SET @function = '53002';
	SET @na = 'SP_ICS_InsertPostingInvoice';
	EXEC sp_PutLog 
		'Get data posting is started'
		, 'System'
		, @na
		, @processId
		, 'MPCS00002INF'
		, ''
		, '5'
		, @function
		, 0
	
	Begin Try
	INSERT INTO TB_T_LOCK
           ([FUNCTION_NO]
           ,[LOCK_REFF]
           ,[CREATED_BY]
           ,[CREATED_DT])
    VALUES
		(@function
		,@processId
		,'System'
		,GETDATE())
		
	BEGIN TRANSACTION IPPCSSP_ICS_InsertPostingInvoice
	-- ============================================
	-- PHASE 1 : GET INVOICE_NO ICS PROCESS
	-- ============================================
	SET @stepId = 2;
	SET @sqlQuery = 'INSERT INTO #ICS_FINISH SELECT inv_no, SUPP_INVOICE_NO, SUPP_CD FROM OPENQUERY([IPPCS_TO_ICS] , ''select * from tb_t_invoice_h where other_process_id = ' + CONVERT(VARCHAR, @processID) + ''')';
	EXEC (@sqlQuery);
	
	SELECT @INVOICE_NO=INVOICE_NO,
		@SupplierInvoiceNo=SupplierInvoiceNo,
		@SupplierCode=SupplierCode
	FROM #ICS_FINISH
	
	EXEC sp_PutLog 
		'Get Invoice_No From Finished ICS Process'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	SET @stepId = 3;
	
	--INSERT INTO TB_T_LOCK
 --          ([FUNCTION_NO]
 --          ,[LOCK_REFF]
 --          ,[CREATED_BY]
 --          ,[CREATED_DT])
 --   VALUES
	--	(@function
	--	,@processId
	--	,'System'
	--	,GETDATE())
	
	-- ==============================================
	-- PHASE 2 : INSERT INTO Invoice Table
	-- ==============================================
	EXEC sp_PutLog 
		'Insert TB_R_INV_H'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 4;

	-- ==============================================
	-- Modified by FID.Reggy
	-- Adding Temporary Table Declaration (#TB_T_INV_H_TEMP)
	-- Insert Query String result into Temporary Table (#TB_T_INV_H_TEMP)
	-- Insert Into TB_R_INV_H from #TB_T_INV_H_TEMP
	-- ==============================================
	/*IF OBJECT_ID('tempdb..#TB_T_INV_H_TEMP') IS NOT NULL BEGIN DROP TABLE #TB_T_INV_H_TEMP END
	CREATE TABLE #TB_T_INV_H_TEMP(
		[INV_NO] [varchar](16) NOT NULL,
		[INV_DT] [datetime] NULL,
		[SUPP_INV_NO] [varchar](12) NULL,
		[SUPPLIER_CD] [varchar](6) NULL,
		[CURRENCY_CD] [varchar](3) NULL,
		[EXCHANGE_RATE] [numeric](7, 2) NULL,
		[INV_AMT] [numeric](16, 2) NULL,
		[INV_LOCAL_AMT] [numeric](16, 2) NULL,
		[INV_TAX_NO] [varchar](24) NULL,
		[INV_TEXT] [varchar](20) NULL,
		[PAYMENT_TERM_CD] [varchar](4) NULL,
		[PAYMENT_METHOD_CD] [varchar](2) NULL,
		[SUPP_BANK_TYPE] [varchar](4) NULL,
		[BALANCE] [numeric](16, 2) NULL,
		[INV_DESC] [varchar](50) NULL,
		[WITHOLDING_TAX_CD] [varchar](2) NULL,
		[BASE_AMT] [numeric](16, 2) NULL,
		[WITHOLDING_TAX_AMT] [numeric](16, 2) NULL,
		[ASSIGNMENT] [varchar](12) NULL,
		[PAY_PLAN_DT] [datetime] NULL,
		[STATUS_CD] [int] NULL,
		[CALC_TAX_FLAG] [varchar](1) NULL,
		[REVERSE_FLAG] [varchar](1) NULL,
		[CREATED_BY] [varchar](20) NULL,
		[CREATED_DT] [datetime] NULL,
		[CHANGED_BY] [varchar](20) NULL,
		[CHANGED_DT] [datetime] NULL,
		[IN_PROGRESS] [char](1) NULL
		);*/

	 SET @sqlQuery = '
			INSERT INTO TB_R_INV_H (
						[INV_NO],
						[INV_DT],
						[SUPP_INV_NO],
						[SUPPLIER_CD],
						[CURRENCY_CD],
						[EXCHANGE_RATE],
						[INV_AMT],
						[INV_LOCAL_AMT],
						[INV_TAX_NO],
						[INV_TEXT],
						[PAYMENT_TERM_CD],
						[PAYMENT_METHOD_CD],
						[SUPP_BANK_TYPE],
						[BALANCE],
						[INV_DESC],
						[WITHHOLDING_TAX_CD],
						[BASE_AMT],
						[WITHHOLDING_TAX_AMT],
						[ASSIGNMENT],
						[PAY_PLAN_DT],
						[STATUS_CD],
						[CALC_TAX_FLAG],
						[REVERSE_FLAG],
						[CREATED_BY],
						[CREATED_DT],
						[CHANGED_BY],
						[CHANGED_DT],
						[IN_PROGRESS]
					)

			SELECT
				INV_NO,
				INV_DT,
				SUPP_INVOICE_NO as SUPP_INV_NO,
				SUPP_CD as SUPPLIER_CD,
				PAYMENT_CURR as CURRENCY_CD,
				EXCHANGE_RATE,
				INV_AMT,
				INV_AMT_LOCAL_CURR as INV_LOCAL_AMT,
				HEADER_TEXT as INV_TAX_NO,
				INV_TEXT,
				PAYMENT_TERM_CD,
				PAYMENT_METHOD_CD,
				PARTNER_BANK_KEY as SUPP_BANK_TYPE,
				BALANCE,
				INV_DESCRIPTION as INV_DESC,
				WITHOLDING_TAX_CD,
				WITHOLDING_TAX_BASE_AMT as BASE_AMT,
				WITHOLDING_TAX_AMT,
				ASSIGNMENT,
				NEXT_BASELINE_DT as PAY_PLAN_DT,
				4 as STATUS_CD,
				CALC_TAX_FLAG,
				REVERSE_FLAG,
				''System'' as CREATED_BY,
				GETDATE() as CREATED_DT,
				NULL as CHANGED_BY,
				NULL as CHANGED_DT,
				2 as IN_PROGRESS
			FROM OPENQUERY([IPPCS_TO_ICS] ,
			''select	h.*,
						wt.WITHOLDING_TAX_CD, 
						h.BASELINE_DT+1 as NEXT_BASELINE_DT,
						wt.WITHOLDING_TAX_BASE_AMT,
						wt.WITHOLDING_TAX_AMT
				from TB_T_INVOICE_H h
				left join TB_T_INV_WITHOLDING_TAX wt ON h.INV_NO=wt.INV_NO
				where h.INV_NO = ''''' + CONVERT(VARCHAR, @INVOICE_NO) + ''''''')';

	--INSERT INTO #TB_T_INV_H_TEMP 
	EXEC (@sqlQuery);

	/*INSERT INTO TB_R_INV_H (
		[INV_NO],
		[INV_DT],
		[SUPP_INV_NO],
		[SUPPLIER_CD],
		[CURRENCY_CD],
		[EXCHANGE_RATE],
		[INV_AMT],
		[INV_LOCAL_AMT],
		[INV_TAX_NO],
		[INV_TEXT],
		[PAYMENT_TERM_CD],
		[PAYMENT_METHOD_CD],
		[SUPP_BANK_TYPE],
		[BALANCE],
		[INV_DESC],
		[WITHHOLDING_TAX_CD],
		[BASE_AMT],
		[WITHHOLDING_TAX_AMT],
		[ASSIGNMENT],
		[PAY_PLAN_DT],
		[STATUS_CD],
		[CALC_TAX_FLAG],
		[REVERSE_FLAG],
		[CREATED_BY],
		[CREATED_DT],
		[CHANGED_BY],
		[CHANGED_DT],
		[IN_PROGRESS]
	)
		SELECT 
			[INV_NO],
			[INV_DT],
			[SUPP_INV_NO],
			[SUPPLIER_CD],
			[CURRENCY_CD],
			[EXCHANGE_RATE],
			[INV_AMT],
			[INV_LOCAL_AMT],
			[INV_TAX_NO],
			[INV_TEXT],
			[PAYMENT_TERM_CD],
			[PAYMENT_METHOD_CD],
			[SUPP_BANK_TYPE],
			[BALANCE],
			[INV_DESC],
			[WITHOLDING_TAX_CD],
			[BASE_AMT],
			[WITHOLDING_TAX_AMT],
			[ASSIGNMENT],
			[PAY_PLAN_DT],
			[STATUS_CD],
			[CALC_TAX_FLAG],
			[REVERSE_FLAG],
			[CREATED_BY],
			[CREATED_DT],
			[CHANGED_BY],
			[CHANGED_DT],
			[IN_PROGRESS]
		FROM #TB_T_INV_H_TEMP WHERE INV_NO = CONVERT(VARCHAR, @INVOICE_NO)
*/
	-- ==============================================
	-- Modified by FID.Reggy
	-- Adding Temporary Table Declaration (#TB_T_INV_D_TEMP)
	-- Insert Query String result into Temporary Table (#TB_T_INV_D_TEMP)
	-- Changed Created_By and Created_Dt Value
	-- Get Packing_Type and Retro_Doc_No from TB_R_INV_UPLOAD using join key
	--		#TB_T_INV_H_TEMP.SUPP_INV_NO, 
	--		#TB_T_INV_H_TEMP.SUPPLIER_CD, and
	--		#TB_T_INV_D_TEMP.MAT_NO 
	-- Update Packing_Type, Retro_Doc_No, Changed_By, and Changed_Dt in #TB_T_INV_D_TEMP
	-- Insert Into TB_R_INV_D from #TB_T_INV_D_TEMP
	-- ==============================================

	EXEC sp_PutLog 
		'Insert TB_R_INV_D'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 5;
	IF OBJECT_ID('tempdb..#TB_T_INV_D_TEMP') IS NOT NULL BEGIN DROP TABLE #TB_T_INV_D_TEMP END
	CREATE TABLE #TB_T_INV_D_TEMP(
		[INV_NO] [varchar](10) NOT NULL,
		[INV_DOC_ITEM] [varchar](5) NOT NULL,
		[PO_NO] [varchar](10) NULL,
		[PO_ITEM_NO] [varchar](5) NULL,
		[COMP_PRICE_CD] [varchar](4) NULL,
		[MAT_DOC_NO] [varchar](10) NULL,
		[MAT_DOC_ITEM] [numeric](4, 0) NULL,
		[MAT_NO_REF] [varchar](23) NULL,
		[INV_REF_NO] [varchar](16) NULL,
		[MAT_NO] [varchar](23) NOT NULL,
		[PROD_PURPOSE_CD] [varchar](5) NOT NULL,
		[SOURCE_TYPE_CD] [varchar](1) NOT NULL,
		[PLANT_CD] [varchar](4) NOT NULL,
		[SLOC_CD] [varchar](6) NOT NULL,
		[PACKING_TYPE] [varchar](1) NULL,
		[PART_COLOR_SFX] [varchar](2) NOT NULL,
		[DOC_DT] [date] NULL,
		[QTY] [numeric](13, 3) NULL,
		[PRICE_AMT] [numeric](16, 5) NULL,
		[SUPP_MANIFEST] [varchar](12) NULL,
		[INV_AMT] [numeric](16, 5) NULL,
		[INV_LOCAL_AMT] [numeric](16, 5) NULL,
		[CONDITION_CATEGORY] [varchar](1) NULL,
		[RETRO_DOC_NO] [varchar](10) NULL,
		[TAX_CD] [varchar](2) NOT NULL,
		[UOM_CD] [varchar](3) NULL,
		[MAT_DESC] [varchar](40) NULL,
		[CREATED_BY] [varchar](20) NULL,
		[CREATED_DT] [datetime] NULL,
		[CHANGED_BY] [varchar](20) NULL,
		[CHANGED_DT] [datetime] NULL)

	SET @sqlQuery = '
				SELECT 
					INV_NO,
					INV_DOC_ITEM,
					PO_NO,
					PO_ITEM_NO,
					COMP_PRICE_CD,
					MAT_DOC_NO,
					MAT_DOC_ITEM,
					ORI_MAT_NO as MAT_NO_REF,
					REFF_NO as INV_REF_NO,
					MAT_NO,
					PROD_PURPOSE_CD,
					SOURCE_TYPE as SOURCE_TYPE_CD,
					ISNULL(PLANT_CD,'''') as PLANT_CD,
					ISNULL(SLOC_CD,'''') as SLOC_CD,
					NULL as PACKING_TYPE,
					PART_COLOR_SFX,
					DOC_DT,
					QUANTITY as QTY,
					PRICE_AMT,
					REFF_NO as SUPP_MANIFEST,
					INV_AMT,
					INV_AMT_LOCAL_CURR as INV_LOCAL_AMT,
					CONDITION_CATEGORY,
					NULL as RETRO_DOC_NO,
					TAX_CD,
					UNIT_OF_MEASURE_CD as UOM_CD,
					MAT_DESC,
					''System'' AS CREATED_BY,
					GETDATE() AS CREATED_DT,
					NULL AS CHANGED_BY,
					NULL AS CHANGED_DT
				FROM OPENQUERY([IPPCS_TO_ICS] , 
				''select d.*,m.MAT_DESC from TB_T_INVOICE_D d
				left join TB_M_MATERIAL m ON d.MAT_NO=m.MAT_NO
				where d.INV_NO = ' + CONVERT(VARCHAR, @INVOICE_NO) + ''')';

	INSERT INTO #TB_T_INV_D_TEMP EXEC (@sqlQuery);
	
	UPDATE #TB_T_INV_D_TEMP SET 
		PACKING_TYPE = C.PACKING_TYPE,
		RETRO_DOC_NO = C.RETRO_DOC_NO,
		CHANGED_BY = 'System',
		CHANGED_DT = GETDATE()
	FROM  #TB_T_INV_D_TEMP A 
		JOIN TB_R_INV_UPLOAD C ON C.SUPP_INV_NO = @SupplierInvoiceNo 
								  AND C.SUPP_CD = @SupplierCode 
								  AND C.MAT_NO = A.MAT_NO

	INSERT INTO TB_R_INV_D
			([INV_NO],
			[INV_DOC_ITEM],
			[PO_NO],
			[PO_ITEM_NO],
			[COMP_PRICE_CD],
			[MAT_DOC_NO],
			[MAT_DOC_ITEM],
			[MAT_NO_REF],
			[INV_REF_NO],
			[MAT_NO],
			[PROD_PURPOSE_CD],
			[SOURCE_TYPE_CD],
			[PLANT_CD],
			[SLOC_CD],
			[PACKING_TYPE],
			[PART_COLOR_SFX],
			[DOC_DT],
			[QTY],
			[PRICE_AMT],
			[SUPP_MANIFEST],
			[INV_AMT],
			[INV_LOCAL_AMT],
			[CONDITION_CATEGORY],
			[RETRO_DOC_NO],
			[TAX_CD],
			[UOM_CD],
			[MAT_DESC],
			[CREATED_BY],
			[CREATED_DT],
			[CHANGED_BY],
			[CHANGED_DT])
		SELECT 
			[INV_NO],
			[INV_DOC_ITEM],
			[PO_NO],
			[PO_ITEM_NO],
			[COMP_PRICE_CD],
			[MAT_DOC_NO],
			[MAT_DOC_ITEM],
			[MAT_NO_REF],
			[INV_REF_NO],
			[MAT_NO],
			[PROD_PURPOSE_CD],
			[SOURCE_TYPE_CD],
			[PLANT_CD],
			[SLOC_CD],
			[PACKING_TYPE],
			[PART_COLOR_SFX],
			[DOC_DT],
			[QTY],
			[PRICE_AMT],
			[SUPP_MANIFEST],
			[INV_AMT],
			[INV_LOCAL_AMT],
			[CONDITION_CATEGORY],
			[RETRO_DOC_NO],
			[TAX_CD],
			[UOM_CD],
			[MAT_DESC],
			[CREATED_BY],
			[CREATED_DT],
			[CHANGED_BY],
			[CHANGED_DT]
		FROM #TB_T_INV_D_TEMP WHERE INV_NO = CONVERT(VARCHAR, @INVOICE_NO)

	-- ==============================================
	-- Modified by FID.Reggy
	-- DROP #TB_T_INV_H_TEMP
	-- DROP #TB_T_INV_D_TEMP
	-- ==============================================

	--DROP TABLE #TB_T_INV_H_TEMP
	DROP TABLE #TB_T_INV_D_TEMP
	
	EXEC sp_PutLog 
		'Insert TB_R_INV_TAX'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	-- ==============================================
	-- Modified by FID.Reggy
	-- Changed Table Target In OpenQuery from TB_R_INV_TAX to TB_T_INV_TAX
	-- Changed Table Target In OpenQuery from TB_R_INV_H to TB_T_INV_H
	-- Add column h.PAYMENT_CURR in OpenQuery
	-- Changed Created_By, Created_Dt, Changed_By, and Changed_Dt Value
	-- ==============================================

	SET @stepId = 6;
	SET @sqlQuery = 'INSERT INTO TB_R_INV_TAX(
						INVOICE_TAX_NO,
						INVOICE_NO,
						TAX_CD,
						CURRENCY_CD,
						TAX_AMOUNT,
						LOCAL_TAX_AMOUNT,
						INVOICE_TAX_DT,
						CREATED_BY,
						CREATED_DT,
						CHANGED_BY,
						CHANGED_DT)
					SELECT 
						HEADER_TEXT,
						INV_NO,
						TAX_CD,
						PAYMENT_CURR as CURR_CD,
						TAX_AMT,
						LOCAL_TAX_AMT,
						TAX_DT,
						''System'' as CREATED_BY,
						GETDATE() as CREATED_DT,
						NULL as CHANGED_BY,
						NULL as CHANGED_DT
					FROM OPENQUERY([IPPCS_TO_ICS] , 
					''select it.*,
							 h.HEADER_TEXT,
							 h.TAX_DT,
							 h.PAYMENT_CURR
					  from TB_T_INV_TAX it
					  inner join TB_T_INVOICE_H h on it.INV_NO=h.INV_NO
					where it.INV_NO = ' + CONVERT(VARCHAR, @INVOICE_NO) + ''')';
	EXEC (@sqlQuery);
	
	EXEC sp_PutLog 
		'Insert TB_R_INV_GL_ACCOUNT'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	SET @stepId = 7;

	-- ==============================================
	-- Modified by FID.Reggy
	-- Changed Table Target In OpenQuery from TB_R_INV_GL_ACCOUNT to TB_T_INV_GL_ACCOUNT
	-- Changed Created_By, Created_Dt, Changed_By, and Changed_Dt Value
	-- ==============================================

	SET @sqlQuery = 'INSERT INTO TB_R_INV_GL_ACCOUNT (
						INV_NO,
						GL_ACCOUNT,
						INV_AMT,
						COST_CENTER,
						TAX_CD,
						CREATED_BY,
						CREATED_DT,
						CHANGED_BY,
						CHANGED_DT)
					SELECT 
						INV_NO,
						GL_ACCOUNT,
						INV_AMT,
						COST_CENTER as COST_CENTER_CD,
						TAX_CD,
						''System'' as CREATED_BY,
						GETDATE() as CREATED_DT,
						NULL as CHANGED_BY,
						NULL as CHANGED_DT
					FROM OPENQUERY([IPPCS_TO_ICS] , ''select * from TB_T_INV_GL_ACCOUNT where INV_NO = ' + CONVERT(VARCHAR, @INVOICE_NO) + ''')';
	EXEC (@sqlQuery);
	
	EXEC sp_PutLog 
		'Update TB_R_INV_H'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
		
	SET @stepId = 8;
	update h set
		h.SUBMIT_DT=tu.SUBMIT_DT,
		h.SUBMIT_BY=tu.SUBMIT_BY,
		h.POSTING_DT=tu.POSTING_DT,
		h.POSTING_BY=tu.POSTING_BY,
		h.TURN_OVER=tu.TURN_OVER,
		h.TOTAL_MANIFEST=tu.TOTAL_MANIFEST,
		h.STAMP_FLAG=tu.STAMP_FLAG,
		h.CERTIFICATE_ID=tu.CERTIFICATE_ID,
		h.CREATED_DT=tu.CREATED_DT,
		h.CREATED_BY=tu.CREATED_BY,
		h.CHANGED_BY=tu.POSTING_BY,
		h.CHANGED_DT=GETDATE()
	FROM TB_R_INV_H h
	INNER JOIN TB_R_INV_UPLOAD tu ON h.SUPP_INV_NO=tu.SUPP_INV_NO AND h.SUPPLIER_CD=tu.SUPP_CD
	WHERE h.INV_NO=@INVOICE_NO
	
	SET @stepId = 9;
	UPDATE gr SET
		gr.STATUS_CD='4',
		gr.CHANGED_BY='AGENT',
		gr.CHANGED_DT=GETDATE()
	FROM TB_R_INV_UPLOAD tu
	INNER JOIN TB_R_GR_IR gr ON gr.PO_NO=tu.PO_NO AND 
								gr.PO_ITEM_NO=tu.PO_ITEM_NO AND
								gr.MAT_DOC_NO=tu.MAT_DOC_NO AND
								gr.ITEM_NO=tu.MAT_DOC_ITEM AND
								gr.COMP_PRICE_CD=tu.COMP_PRICE_CD
	WHERE tu.SUPP_INV_NO=@SupplierInvoiceNo AND tu.SUPP_CD=@SupplierCode

	EXEC sp_PutLog 
		'Insert TB_T_INV_GL_ACCOUNT_LOG'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 10;
	INSERT INTO TB_T_INV_GL_ACCOUNT_LOG
	SELECT * from TB_T_INV_GL_ACCOUNT
	WHERE SUPP_INVOICE_NO=@SupplierInvoiceNo
	
	EXEC sp_PutLog 
		'Insert TB_R_INV_UPLOAD_LOG'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 11;
	INSERT INTO TB_R_INV_UPLOAD_LOG
	SELECT * from TB_R_INV_UPLOAD
	WHERE SUPP_INV_NO=@SupplierInvoiceNo AND SUPP_CD=@SupplierCode
	
	EXEC sp_PutLog 
		'Delete TB_T_INV_GL_ACCOUNT'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 12;
	delete from TB_T_INV_GL_ACCOUNT
	WHERE SUPP_INVOICE_NO=@SupplierInvoiceNo
	      	
	EXEC sp_PutLog 
		'Delete TB_R_INV_UPLOAD'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 13;
	delete from TB_R_INV_UPLOAD
	WHERE SUPP_INV_NO=@SupplierInvoiceNo AND SUPP_CD=@SupplierCode
	
	SET @stepId = 14;
	UPDATE TB_R_DAILY_ORDER_MANIFEST SET
		MANIFEST_RECEIVE_FLAG=6,
		CHANGED_BY='AGENT',
		CHANGED_DT=GETDATE()
	WHERE MANIFEST_NO IN(
		select manifest_no
		from 
		(
			select manifest_no,part_no 
			from tb_r_daily_order_part 
			group by manifest_no,part_no 
		)p
		left join(
			select inv_no,inv_ref_no,part_no=(mat_no+part_color_sfx) 
			from TB_R_INV_D
			where INV_NO=@INVOICE_NO
		)Qry on p.manifest_no=Qry.inv_ref_no and p.part_no=Qry.part_no
		where p.manifest_no in(select INV_REF_NO from TB_R_INV_D where INV_NO=@INVOICE_NO)
		group by manifest_no
		having SUM(case when Qry.inv_no is null then 1 else 0 end)=0
	)
	
	EXEC sp_PutLog 
		'Release Lock and Update TB_R_ICS_QUEUE'
		, 'System'
		, @na
		, @processId
		, 'MPCS00004INF'
		, ''
		, '5'
		, @function
		, 0
	
	SET @stepId = 15;
	DELETE FROM [TB_T_LOCK]
	WHERE
		[FUNCTION_NO] = @function
        AND [LOCK_REFF] = @processId;

	SET @stepId = 16;
	UPDATE TB_R_ICS_QUEUE
	SET
		PROCESS_STATUS = 2,
		ICS_STATUS = 'ICS FINISHED', 
		CHANGED_BY = 'SYSTEM', 
		CHANGED_DT=GETDATE()
	WHERE
		IPPCS_MODUL = '5'
		AND IPPCS_FUNCTION = @function
		AND PROCESS_ID = @processId
		AND PROCESS_STATUS = '1'

	SET @FlagSuccess = 1
	COMMIT TRANSACTION IPPCSSP_ICS_InsertPostingInvoice;	
	End Try
	Begin Catch
		select @ErrorMessage= ISNULL(ERROR_MESSAGE(),'') 
			+ ' @line: ' + ISNULL(CONVERT(VARCHAR, ERROR_LINE()), '') 
			+ ' @step:' + CONVERT(varchar, @stepId);
		
		Rollback Transaction IPPCSSP_ICS_InsertPostingInvoice;
		SET @ErrorMessage = @ErrorMessage + ' [Release Lock and Update TB_R_ICS_QUEUE]';
		
		EXEC sp_PutLog 
		@ErrorMessage
		, 'System'
		, @na
		, @processId
		, 'MPCS00008ERR'
		, ''
		, '5'
		, @function
		, 0
	
	End Catch
	
	Begin Try
	BEGIN TRANSACTION IPPCSSP_ICS_PostingInvoice
	
	DECLARE @Email varchar(1000),
		@fullName VARCHAR(200),
		@body VARCHAR(MAX)
	SELECT @Email=m.EMAIL, @fullName =  m.FULL_NAME
	FROM TB_R_INV_H h
	inner join dbo.TB_R_VW_EMPLOYEE m ON h.POSTING_BY=m.USERNAME
	WHERE INV_NO=@INVOICE_NO
	
	SET @body = 'Dear Mr./Mrs. ' + ISNULL(@fullName, '') + ',' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) +
				'Please be informed that your Invoice with Supplier Invoice No ' + CONVERT(VARCHAR,@SupplierInvoiceNo) + ' is posted with Invoice Document No ' + CONVERT(VARCHAR,@INVOICE_NO) +  CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) +
				'This email is autogenerated by system, please do not reply' +  CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) +
				'Thank you for your attention '
			
	IF @Email <> ''
	BEGIN	
		SET @stepId = 16;
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = 'IPPCS Notification',
			@body = @body,
			@recipients = @Email,
			--@copy_recipients = 'supplierportal6@toyota.co.id;vandy.cahyadi@toyota.co.id',
			--@recipients = 'supplierportal8@toyota.co.id;vandy.cahyadi@toyota.co.id',
			@subject = '[IPPCS] Post Invoice Notification' ;
	END
	
	/* Remarked by imans, 26-10-2013
	-- Move above, Error on mail should not related with sending commit
	COMMIT TRANSACTION IPPCSSP_ICS_InsertPostingInvoice		
	End Try
	Begin Catch
		select ERROR_MESSAGE();
		Rollback Transaction IPPCSSP_ICS_InsertPostingInvoice;
	End Catch
	*/
	
	COMMIT TRANSACTION IPPCSSP_ICS_PostingInvoice		
	End Try
	Begin Catch
		SELECT @ErrorMessage= ISNULL(ERROR_MESSAGE(),'') 
			+ ' @line: ' + ISNULL(CONVERT(VARCHAR, ERROR_LINE()), '') 
			+ ' @step:' + CONVERT(varchar, @stepId);
		
		SET @ErrorMessage = @ErrorMessage + ' [Release Lock and Update TB_R_ICS_QUEUE]';
		
		EXEC sp_PutLog 
		@ErrorMessage
		, 'System'
		, @na
		, @processId
		, 'MPCS00008ERR'
		, ''
		, '5'
		, @function
		, 0
	
	End Catch
	
	--IF @FlagSuccess = 1
	--BEGIN
	--	SET @stepId = 17;
	--	SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Invoice_h WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
	--	SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
	--	EXEC (@sqlQuery)
		
	--	SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Invoice_d WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
	--	SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
	--	EXEC (@sqlQuery)
		
	--	SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Inv_Gl_Account WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
	--	SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
	--	EXEC (@sqlQuery)
		
	--	SET @sqlQuery = 'SELECT SUPP_INVOICE_NO FROM Tb_t_Inv_Witholding_Tax WHERE SUPP_INVOICE_NO=''' + @SupplierInvoiceNo + ''' AND OTHER_PROCESS_ID=''' + CONVERT(VARCHAR(20),@processID) + ''''
	--	SET @sqlQuery = N'DELETE OPENQUERY(IPPCS_TO_ICS, ''' + REPLACE(@sqlQuery, '''', '''''') + ''')'
	--	EXEC (@sqlQuery)
	--END
	
	EXEC sp_PutLog 
		'Get data posting is finished'
		, 'System'
		, @na
		, @processId
		, 'MPCS00003INF'
		, ''
		, '5'
		, @function
		, 0
	
END

