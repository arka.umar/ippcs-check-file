ALTER procedure Lock_process
@function varchar(20),
@lock_key varchar(80),
@process_id BIGINT = 0 output,
@lock_user varchar(20)
as begin
    set nocount on;
    declare @ret int = 0, @lock_reff varchar(1000) = null, @PID  BIGINT = null;
    SELECT top 1 @lock_reff = LOCK_REFF FROM TB_T_lOCK WHERE FUNCTION_NO = @function and LOCK_REFF like @lock_key  + '%';
    WITH R AS(SELECT @lock_reff T)
            SELECT @PID = CASE
                WHEN ISNUMERIC(Q.PID) = 1 THEN CONVERT(BIGINT, Q.PID)
                when q.PID is null then null
                ELSE 0 END
            FROM (
                SELECT LTRIM(RTRIM(SUBSTRING(T, L - CI + 2, CI))) PID
                FROM R
                JOIN (
                SELECT CHARINDEX('_', REVERSE(T)) CI, LEN(T) L
                FROM  R
                )  S ON 1=1
            ) Q;
    if @process_id IS NULL -- DELETE
    BEGIN
        if nullif(@pid,0) is not null
            set @process_id = @pid;
        DELETE FROM TB_T_LOCK WHERE FUNCTION_NO = @function and LOCK_REFF = @lock_reff;
        if @@ROWCOUNT <= 0
            set @ret = -1;
    END
    ELSE
    BEGIN
        if @pid is not null
        begin
            SET @process_id = @pid;
            print @function  + case when @pid != 0 then  ' locked by ' + convert(varchar(20), @pid) else ' is locked' end;
            set @ret  = 1;
        end
        ELSE
        BEGIN
            SET @LOCK_REFF = @lock_key+ CASE WHEN @process_id > 0 THEN '_' + CONVERT(VARCHAR(21), @process_id) ELSE '' END;

            INSERT INTO TB_T_LOCK (FUNCTION_NO, LOCK_REFF, CREATED_BY, CREATED_DT)
            VALUES (@function, @lock_reff, COALESCE(@lock_user,'SYSTEM'), getdate());

            if @@ROWCOUNT <= 0
            BEGIN
                set @ret = 2 ;
            END;
        END
    END
    return @ret;
end;

