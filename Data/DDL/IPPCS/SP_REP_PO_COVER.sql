-- =============================================
-- Author:		Fid.Joko
-- Create date: 2013-11-29
-- Description:	data for PO Cover monthly in PODownload 
-- =============================================
CREATE PROCEDURE [dbo].[SP_REP_PO_COVER]
	@PoNo varchar(max) = NULL
AS
BEGIN

SET NOCOUNT ON;
DECLARE 
		  @fid VARCHAR(20) = '24006', 
		  @pid BIGINT = 0,
		  @log VARCHAR(MAX); 

SELECT 
		  h.PO_NO
		, h.PO_DATE
		, DATEADD(MM, 1, h.PO_DATE) DATEPROD
		, H.RELEASE_DT
		, i.SUPP_CD
		, i.SUPP_NAME
		, i.SUPP_ADDR
		, i.SUPP_CITY
		, i.POSTAL_CD
		, i.SUPP_ATTENTION
		

		, dbo.fn_GetSystemValue(@fid, 'PO_FORM_COMPANY') AS [CNAME]
		, dbo.fn_GetSystemValue(@fid, 'PO_FORM_COMPANY_ADDRESS') + CHAR(13) + CHAR(10) 
			+  dbo.fn_GetSystemValue(@fid, 'PO_FORM_CITY') + ' ' + 
			+  dbo.fn_GetSystemValue(@fid, 'PO_FORM_POSTAL_CODE') + ' - ' + 
			+  dbo.fn_GetSystemValue(@fid, 'PO_FORM_COUNTRY') 
		  AS [CADDR]
		, dbo.fn_GetSystemValue(@fid, 'PO_FORM_PHONE') AS [CPHONE]
		, dbo.fn_GetSystemValue(@fid, 'PO_FORM_FAX') AS [CFAX]
		, dbo.fn_GetSystemValue(@fid, 'PO_NPWP') AS [CNPWP] 
		
		, dbo.fn_GetSystemValue(@fid, 'PO_CONTACT_PERSON') AS [CONTACT_PERSON] 

		, dbo.fn_GetSystemValue('ORDER','PO_USER') AS [USERNAME]
		, dbo.fn_GetSystemValue('ORDER', 'PO_DIVISION') AS [DIVISION]
		, dbo.fn_GetSystemValue('ORDER', 'PO_POSITION') AS [POSITION] 
		, dbo.fn_GetSystemValue('24008', 'PO_PC_FOOTER') AFOOT 
		, (SELECT TOP 1 ds.ACTUAL_DT
		   FROM IPPCS_WORKFLOW.dbo.TB_R_DISTRIBUTION_STATUS ds
		   WHERE FOLIO_NO = @PoNo AND ds.ACTUAL_DT IS NOT NULL
		   ORDER BY STATUS_CD DESC)  AS [RELEASEDATE]
		, case when h.RELEASE_DT is not null then '1' else '0' end as [ISRELEASE]
FROM      TB_R_PO_H h
LEFT JOIN TB_M_SUPPLIER_ICS i 
       ON h.SUPPLIER_CD=i.SUPP_CD

WHERE     h.PO_NO = @PoNo

END

