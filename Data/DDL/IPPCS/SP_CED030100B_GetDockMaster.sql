-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetDockMaster]
AS
BEGIN
	SELECT [DOCK_CD]
      ,[PLANT_CD]
      ,[DOCK_NM]
      ,ISNULL(CONVERT(VARCHAR,[PRODUCTION_DT],121),'') [PRODUCTION_DT]
      ,ISNULL([COMPANY_CD],'') [COMPANY_CD]
      ,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
  FROM [dbo].[TB_T_CED_DOCK]
END

