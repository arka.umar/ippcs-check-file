-- =============================================
-- Author:		<FID.GOLDY>
-- Create date: <7 JANUARY 2014>
-- Description:	<AUDIT TRANSFER POSTING FROM IPPCS TO ICS >
-- Execution : exec CHECK_TRANSFER_POSTING_FROM_IPPCS_TO_ICS '','','','20131231','20140205'
-- =============================================
CREATE PROCEDURE [dbo].[CHECK_TRANSFER_POSTING_FROM_IPPCS_TO_ICS]
(
	--@MANIFEST_RECEIVE_FLAG1 AS VARCHAR(MAX),
	--@MANIFEST_RECEIVE_FLAG2 AS VARCHAR(MAX),
	--@MANIFEST_RECEIVE_FLAG3 AS VARCHAR(MAX),
	@ARRIVAL_PLAN_FROM AS VARCHAR(MAX),
	@ARRIVAL_PLAN_TO AS VARCHAR(MAX)
	
)
AS
BEGIN
 DECLARE @TSQL1 varchar(8000),
 @TSQL2 varchar(8000), 
 @RowStart INT,
 @RowEnd INT,
 @MatDocNo varchar(10);

 --IF object_id('tempdb..#MATDOC_H') IS NOT NULL DROP TABLE #MATDOC_H
 --IF object_id('tempdb..#MATDOC_D') IS NOT NULL DROP TABLE #MATDOC_D
  IF object_id('tempdb..#ICSTP') IS NOT NULL DROP TABLE #ICSTP

--CREATE TABLE #MATDOC_H
--(
--ROW_ID BIGINT,
--MAT_DOC_NO	VARCHAR(10),
--MAT_DOC_YEAR	VARCHAR(4),
--REF_CANCEL_MAT_DOC_NO	VARCHAR(10),
--REF_CANCEL_MAT_DOC_YEAR	VARCHAR(4),
--REF_BACKLOG_GI_MAT_DOC_NO	VARCHAR(10),
--REF_BACKLOG_GI_MAT_DOC_YEAR	VARCHAR(4),
--MAT_DOC_DESC	VARCHAR(255),
--POSTING_DT	DATE,
--ENTRY_DT	DATE,
--REFF_NO	VARCHAR(16),
--DOC_DT	DATE,
--PRINT_DT	DATE,
--KANBAN_ORDER_NO	VARCHAR(16),
--USR_ID	VARCHAR(20),
--DN_COMPLETE_FLAG	VARCHAR(1),
--IM_PERIOD	VARCHAR(2),
--IM_YEAR	INT,
--CANCELED_FLAG	VARCHAR(1),
--SYSTEM_SOURCE	VARCHAR(20),
--TRANSACTION_TYPE	VARCHAR(2),
--DOCK_CD	VARCHAR(6),
--CREATED_BY	VARCHAR(20),
--CREATED_DT	DATE,
--CHANGED_BY	VARCHAR(20),
--CHANGED_DT	DATE,
--DOC_TYPE	VARCHAR(2),
--RAW_MAT_FLAG	VARCHAR(1),
--REMARK_1	VARCHAR(255),
--REMARK_2	VARCHAR(255),
--GR_TYPE	VARCHAR(1)
--)



--CREATE TABLE #MATDOC_D
--(
--MAT_DOC_YEAR	VARCHAR(4),
--MAT_DOC_NO	VARCHAR(10),
--ITEM_NO	INT,
--MAT_NO	VARCHAR(23),
--ORI_MAT_NO	VARCHAR(30),
--SOURCE_TYPE	VARCHAR(1),
--PROD_PURPOSE_CD	VARCHAR(5),
--ITEM_QUANTITY	INT,
--UNIT_OF_MEASURE_CD	VARCHAR(3),
--DIRECTION_OF_MOVEMENT	VARCHAR(1),
--MOVEMENT_TYPE	VARCHAR(4),
--SPECIAL_STOCK_FLAG	VARCHAR(1),
--PLANT_CD	VARCHAR(4),
--SLOC_CD	VARCHAR(6),
--DOCK_CD	VARCHAR(6),
--SUPP_CD	VARCHAR(6),
--ORI_SUPP_CD	VARCHAR(6),
--PO_NO	VARCHAR(10),
--PO_ITEM_NO	VARCHAR(5),
--MAT_PRICE	INT,
--ITEM_CUR_CD	VARCHAR(3),
--INTERNAL_ORDER_CD	VARCHAR(12),
--COST_CENTER_CD	VARCHAR(10),
--MAT_DESC	VARCHAR(40),
--BATCH_NO	VARCHAR(10),
--VALUE_LOCAL_CURR	INT,
--VALUE_DOC_CURR	INT,
--REFF_MAT_NO	VARCHAR(23),
--REFF_SOURCE_TYPE	VARCHAR(1),
--REFF_PROD_PURPOSE	VARCHAR(5),
--REFF_BATCH_NO	VARCHAR(10),
--REFF_PLANT_CD	VARCHAR(4),
--REFF_SLOC_CD	VARCHAR(6),
--AUTOMATIC_CREATE_FLAG	VARCHAR(1),
--REFF_MAT_PRICE	INT,
--REFF_CANCEL_MAT_DOC_NO	VARCHAR(10),
--REFF_CANCEL_MAT_DOC_ITEM	INT,
--REF_CANCEL_MAT_DOC_YEAR	VARCHAR(4),
--SPECIAL_STOCK_VALUATION_FLAG	VARCHAR(1),
--RECEIVE_NO	VARCHAR(10),
--ITEM_COMPLETE_FLAG	VARCHAR(1),
--GR_LEVEL	VARCHAR(1),
--CREATED_BY	VARCHAR(20),
--CREATED_DT	DATETIME,
--CHANGED_BY	VARCHAR(20),
--CHANGED_DT	DATETIME,
--PCC_FLAG	VARCHAR(1),
--CANCEL_STS	VARCHAR(1),
--CASE_EXPLOSION_FLAG	VARCHAR(1),
--REMARK_1	VARCHAR(255),
--REMARK_2	VARCHAR(255)

--)

CREATE TABLE #ICSTP
(
 MAT_DOC_YEAR	VARCHAR(4),
 MAT_DOC_NO	VARCHAR(10),
 ORI_MAT_NO	VARCHAR(23),
 REFF_NO VARCHAR(16),
 ITEM_QUANTITY	INT,
)
SELECT  @TSQL1 = 'select * from openquery(IPPCS_TO_ICS,''
select d.mat_doc_year  ,d.mat_doc_no, d.ORI_MAT_NO  as PART_NUMBER,h.REFF_NO as MANIFEST_NO, d.item_quantity as ICS_QUANTITY
from tb_r_mat_doc_h h join tb_r_mat_doc_d d
on h.MAT_DOC_NO = d.Mat_Doc_No and h.mat_doc_year = d.mat_doc_year
where  h. POSTING_DT > to_date( ''''' +@ARRIVAL_PLAN_FROM + ''''' ,''''YYYYMMDD'''')  
and  h.POSTING_DT <= to_date(''''' +@ARRIVAL_PLAN_TO + ''''',''''YYYYMMDD'''') 
and h.transaction_type = ''''TP''''
and h.system_source = ''''IPPCS''''
and d.direction_of_movement = ''''+''''
order by d.mat_doc_no
'')'
insert into #ICSTP
EXEC (@TSQL1)

select * from (

SELECT
--Y.mat_doc_year  ,
--Y.mat_doc_no, 
X.MANIFEST_NO,
X.SUPPLIER_CD,
X.SUPPLIER_PLANT,
X.DOCK_CD,
--X.mat_doc_no, 
--Y.mat_no as PART_NUMBER,
X.PART_NO as PART_NUMBER,
--Y.REFF_NO as MANIFEST_NO, 

X.TOTAL_IPCCS_TP,
Y.item_quantity as ICS_QUANTITY
FROM 
(
select a.MANIFEST_NO, 
	   P.PART_NO , 
	   a.SUPPLIER_CD,
	   a.SUPPLIER_PLANT,
	   a.DOCK_CD,
	   SUM(P.ORDER_QTY) AS TOTAL_IPCCS_TP
from TB_R_DAILY_ORDER_MANIFEST A inner join TB_R_DAILY_ORDER_PART P
on A.MANIFEST_NO = P.MANIFEST_NO
where
--a.MANIFEST_RECEIVE_FLAG in ('3','2','5','4')
A.DROP_STATUS_DT >  CONVERT(varchar(11),(CONVERT(DATETIME, @ARRIVAL_PLAN_FROM, 112)),106)
and A.DROP_STATUS_DT <= CONVERT(varchar(11),(CONVERT(DATETIME, @ARRIVAL_PLAN_TO, 112)),106)
and a.TOTAL_QTY> 0
and P.ORDER_QTY > 0
and a.SUPPLIER_CD like '807%'
and a.MANIFEST_NO is not null
AND (A.CANCEL_FLAG = 0 or A.CANCEL_FLAG is null)
group by 
a.MANIFEST_NO, P.PART_NO , P.ORDER_QTY , a.SUPPLIER_CD, a.SUPPLIER_PLANT, a.DOCK_CD
) AS X LEFT JOIN  #ICSTP Y on X.MANIFEST_NO = Y.REFF_NO  AND X.PART_NO = Y.ORI_MAT_NO --AND X.MAT_DOC_NO = Y.MAT_DOC_NO
--order by X.MANIFEST_NO asc
) as t --where isnull (t.ICS_QUANTITY,0) <> t.TOTAL_IPCCS_TP


--select CONVERT(varchar(11),'20131231',110)
--SELECT CONVERT(DATETIME, '20131231', 112) -- 2013-05-04 00:00:00.000 

--select CONVERT(varchar(11),(CONVERT(DATETIME, '20131231', 112)),106)


--ROW_NUMBER() OVER (ORDER BY ROWNUM) AS ROW_ID,

--SELECT  @TSQL1 = 'select * from openquery(IPPCS_TO_ICS,''select  
--ROWNUM,
--MAT_DOC_NO,
--MAT_DOC_YEAR,
--REF_CANCEL_MAT_DOC_NO,
--REF_CANCEL_MAT_DOC_YEAR,
--REF_BACKLOG_GI_MAT_DOC_NO,
--REF_BACKLOG_GI_MAT_DOC_YEAR,
--MAT_DOC_DESC,
--POSTING_DT,
--ENTRY_DT,
--REFF_NO	,
--DOC_DT,
--PRINT_DT,
--KANBAN_ORDER_NO	,
--USR_ID,
--DN_COMPLETE_FLAG,
--IM_PERIOD,
--IM_YEAR,
--CANCELED_FLAG,
--SYSTEM_SOURCE,
--TRANSACTION_TYPE,
--DOCK_CD,
--CREATED_BY,
--CREATED_DT,
--CHANGED_BY,
--CHANGED_DT,
--DOC_TYPE,
--RAW_MAT_FLAG,
--REMARK_1,
--REMARK_2,
--GR_TYPE
--from tb_r_mat_doc_h where POSTING_DT > to_date( ''''' +@ARRIVAL_PLAN_FROM + ''''' ,''''YYYYMMDD'''') 
--and  POSTING_DT <= to_date(''''' +@ARRIVAL_PLAN_TO + ''''',''''YYYYMMDD'''') 
--'')'
--insert into #MATDOC_H
--EXEC (@TSQL1)

----select * from #MATDOC_H
----ORDER BY ROW_ID;

--SET @RowStart = 1;


--SELECT  @RowEnd = COUNT (*)
--FROM    #MATDOC_H

----drop table #MATDOC_H

--WHILE @RowStart <= @RowEnd
--BEGIN

--SELECT  @MatDocNo = ISNULL(MAT_DOC_NO, '')
--FROM #MATDOC_H 
--WHERE ROW_ID = @RowStart

--IF( @MatDocNo  <> null or @MatDocNo  <> '')
--BEGIN
--	SELECT  @TSQL2 = 'select * from openquery(IPPCS_TO_ICS,''select  distinct MAT_DOC_YEAR, MAT_DOC_NO, ITEM_QUANTITY  from tb_r_mat_doc_d where mat_doc_no = '''''  + @MatDocNo + ''''' 
--	'')'
--	insert into #MATDOC_D(MAT_DOC_YEAR,MAT_DOC_NO,ITEM_QUANTITY)
--	EXEC (@TSQL2)
--END
--ELSE
--BEGIN
--	select ''
--END



--SET @RowStart = @RowStart + 1
--END

--select * from #MATDOC_D
--print @RowStart;
--print @RowEnd;

---------------------------------------------------------
--Create table #temp(id int identity, columnvalue varchar(max))
--Declare @Manifest VARCHAR(MAX),
--@sqlQuery AS VARCHAR(MAX);
--insert into #temp(columnvalue)
--	select * from dbo.fnsplit(
--	(
--		SELECT
--		STUFF(
--		(
--			SELECT
--			',' + '''' + MANIFEST_NO + ''''
--			FROM TB_R_DAILY_ORDER_MANIFEST A
--			WHERE  
--			A.MANIFEST_RECEIVE_FLAG in (@MANIFEST_RECEIVE_FLAG1,@MANIFEST_RECEIVE_FLAG2,@MANIFEST_RECEIVE_FLAG3)
--			and A.ARRIVAL_PLAN_DT > @ARRIVAL_PLAN_FROM
--			and A.ARRIVAL_PLAN_DT < @ARRIVAL_PLAN_TO
--			and A.TOTAL_QTY>0
--			and A.SUPPLIER_CD like '807%'
--			and A.MAT_DOC_NO is not null
--			FOR XML PATH('')
--			), 1, 1, ''
--		) As MANIFEST_NO 
--	)
	
--	,',')

  --SELECT DATALENGTH(
  --(
		
		
--)--datalenght
--   )--datalenght 169.986
		--print @Manifest;
	--SET @sqlQuery = 'SELECT * FROM OPENQUERY([IPPCS_TO_ICS] , ''select * from tb_r_mat_doc_h h where h.reff_no = '
	-- --+ @Manifest+ 
	 
	--SELECT  
		
	--	STUFF(
	--	(
	--		SELECT
	--		',' + '''' + MANIFEST_NO + ''''
	--		FROM TB_R_DAILY_ORDER_MANIFEST A
	--		WHERE  
	--		A.MANIFEST_RECEIVE_FLAG in (@MANIFEST_RECEIVE_FLAG1,@MANIFEST_RECEIVE_FLAG2,@MANIFEST_RECEIVE_FLAG3)
	--		and A.ARRIVAL_PLAN_DT > @ARRIVAL_PLAN_FROM
	--		and A.ARRIVAL_PLAN_DT < @ARRIVAL_PLAN_TO
	--		and A.TOTAL_QTY>0
	--		and A.SUPPLIER_CD like '807%'
	--		and A.MAT_DOC_NO is not null
	--		FOR XML PATH('')
	--		), 1, 1, ''
	--	) 

	--''')';
	--print @sqlQuery;
	--EXEC (@sqlQuery);

	
	

	
	
		
	
	














  -- 1-1101233258

	--select * from openquery (ippcs_to_ics, 'select * from tb_r_mat_doc_h h where h.reff_no in (''1130050264'') ')
	--select distinct mat_doc_no, mat_no, item_qty from openquery (ippcs_to_ics, 'select * from tb_r_mat_doc_d d where d.mat_doc_no=''4904796756'' order by d.mat_no')
END

