/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Insert draft route price
*/

CREATE PROCEDURE [dbo].[SP_DLV_InsertDraftRoutePrice] 

@CHILD_ROUTE VARCHAR(5),
@PARENT_ROUTE VARCHAR(5),
@LP_CD VARCHAR (4),
@VALID_FROM VARCHAR (20),
@CREATED_BY VARCHAR (20)

AS
BEGIN 

	DECLARE @CHECK_AUTH AS INT, 
		@CHECK_ACTIVE AS INT, 
		@CHECK_VALID_FROM AS DATE, 
		@CHECK_MONTH AS DATE ,
		@CHECK_DRAFT AS INT,		
		@isExists AS INT

	DECLARE @RecipientEmail varchar(max),
			@RecipientCopyEmail varchar(max),
			@RecipientBlindCopyEmail varchar(max),
			@ERR_FLAG varchar(50),
			@body varchar(max),
			@subjek varchar(100),
			@ERROR_DETAIL varchar(max),
			@profile varchar(max),
			@MAIL_QUERY VARCHAR(MAX),
			@ACTION VARCHAR(25)
	SET @profile = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE');
	IF (@profile IS NULL) BEGIN
		SET @profile = 'NotificationAgent';
	END

	SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE JOB_FUNCTION_ID = 6 AND USERNAME = @CREATED_BY);

	SET @CHECK_ACTIVE = (SELECT COUNT (*) FROM TB_M_ROUTE_PRICE WHERE ROUTE_CD = @CHILD_ROUTE AND VALID_TO = '9999-12-31');

	SET @CHECK_DRAFT = (SELECT COUNT(*) FROM TB_M_DLV_DRAFT_ROUTE_PRICE WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND APPROVED_DT IS NULL)

	SET @isExists = (SELECT COUNT(1) AS VISDATA FROM TB_M_DLV_DRAFT_ROUTE_PRICE WHERE ROUTE_CD_CHILD = @CHILD_ROUTE)

	-- Check autentication
	IF (@CHECK_AUTH = 1) BEGIN

		-- Check Parameter
		IF (@CHILD_ROUTE <> @PARENT_ROUTE) BEGIN
		
			-- Check route from table master route price
			--IF (@CHECK_ACTIVE = 0) BEGIN 
			
				-- Check table draft
				IF (@CHECK_DRAFT <> 1 ) BEGIN
				
						--SET @CHECK_MONTH = (SELECT DATEADD(MONTH, 1, CAST (GETDATE() AS DATE)))
						
						--IF (@VALID_FROM >= CAST (GETDATE() AS DATE) ) BEGIN
						
						IF (@VALID_FROM >= (SELECT DATEADD(MONTH, -1, CAST (GETDATE() AS DATE))) ) BEGIN	
							
							IF (@isExists=0) BEGIN
									
									INSERT INTO TB_M_DLV_DRAFT_ROUTE_PRICE (ROUTE_CD_CHILD, ROUTE_CD_PARENT, LP_CD, PROPOSED_BY, PROPOSED_DT, VALID_FROM, VALID_TO, CREATED_BY, CREATED_DT)
									VALUES (@CHILD_ROUTE, @PARENT_ROUTE, @LP_CD, @CREATED_BY, GETDATE(), CAST(@VALID_FROM AS DATE), CAST('9999-12-31' AS DATE), @CREATED_BY, GETDATE())
									
									---=== Notification for approver
									SELECT 
										@RecipientEmail=EMAIL_TO, 
										@RecipientCopyEmail=EMAIL_CC, 
										@RecipientBlindCopyEmail=EMAIL_BCC 
									FROM TB_M_RTP_NOTIFICATION_EMAIL 
									WHERE MODULE_ID = '6'
										AND FUNCTION_ID = '61002' AND ACTION = 'APPROVER_NOTIF'
									
									SELECT  @subjek=NOTIFICATION_SUBJECT,
											@body=NOTIFICATION_CONTENT
									FROM    TB_M_NOTIFICATION_CONTENT
									WHERE   FUNCTION_ID = '61002'
											AND ROLE_ID = '31'
											AND NOTIFICATION_METHOD = '1'
									
									SET @body = REPLACE(@body, '@route', @CHILD_ROUTE)
									SET @body = REPLACE(@body, '@parent', @PARENT_ROUTE)
									
									IF (@RecipientCopyEmail IS NOT NULL)
										SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
										
									IF (@RecipientCopyEmail IS NOT NULL)
										SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
									
									SET @body = ISNULL(@body, '')
									
									SET @RecipientEmail = ISNULL(@RecipientEmail, '')
									SET @RecipientCopyEmail = ISNULL(@RecipientCopyEmail, '')
									SET @RecipientBlindCopyEmail = ISNULL(@RecipientBlindCopyEmail, '')
									
									SET @MAIL_QUERY = '	
														EXEC msdb.dbo.sp_send_dbmail
															@profile_name	= ''' + @profile + ''',
															@body 			= ''' + @body + ''',
															@body_format 	= ''HTML'',
															@recipients 	= ''' + @RecipientEmail + ''',
															' + @RecipientCopyEmail + '
															' + @RecipientBlindCopyEmail + '
															@subject 		= ''' + @subjek + ''''
															
									EXEC (@MAIL_QUERY)
									---=== End notification for approver
									
							END 
							ELSE BEGIN

								SET @CHECK_VALID_FROM = (SELECT VALID_FROM FROM TB_M_DLV_DRAFT_ROUTE_PRICE WHERE ROUTE_CD_CHILD = @CHILD_ROUTE AND VALID_TO = '9999-12-31')
								
								IF ( CONVERT (DATE,@VALID_FROM,111) > CONVERT(DATE,@CHECK_VALID_FROM,111)) BEGIN
								
									UPDATE TB_M_DLV_DRAFT_ROUTE_PRICE
										SET VALID_TO = DATEADD(day, -1, CAST(@VALID_FROM AS DATE)),
											CHANGED_BY = @CREATED_BY,
											CHANGED_DT = GETDATE()
									WHERE ROUTE_CD_CHILD = @CHILD_ROUTE 
									AND VALID_TO = '9999-12-31'
									--AND LP_CD = @LP_CD

									INSERT INTO TB_M_DLV_DRAFT_ROUTE_PRICE (ROUTE_CD_CHILD, ROUTE_CD_PARENT, LP_CD, PROPOSED_BY, PROPOSED_DT, VALID_FROM, VALID_TO, CREATED_BY, CREATED_DT)
									VALUES (@CHILD_ROUTE, @PARENT_ROUTE, @LP_CD, @CREATED_BY, GETDATE(), CAST(@VALID_FROM AS DATE), CAST('9999-12-31' AS DATE), @CREATED_BY, GETDATE())
									
									---=== Notification for approver
									SELECT 
										@RecipientEmail=EMAIL_TO, 
										@RecipientCopyEmail=EMAIL_CC, 
										@RecipientBlindCopyEmail=EMAIL_BCC 
									FROM TB_M_RTP_NOTIFICATION_EMAIL 
									WHERE MODULE_ID = '6'
										AND FUNCTION_ID = '61002' AND ACTION = 'APPROVER_NOTIF'
									
									SELECT  @subjek=NOTIFICATION_SUBJECT,
											@body=NOTIFICATION_CONTENT
									FROM    TB_M_NOTIFICATION_CONTENT
									WHERE   FUNCTION_ID = '61002'
											AND ROLE_ID = '31'
											AND NOTIFICATION_METHOD = '1'
									
									SET @body = REPLACE(@body, '@route', @CHILD_ROUTE)
									SET @body = REPLACE(@body, '@parent', @PARENT_ROUTE)
									
									IF (@RecipientCopyEmail IS NOT NULL)
										SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
										
									IF (@RecipientCopyEmail IS NOT NULL)
										SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
									
									SET @body = ISNULL(@body, '')
									
									SET @RecipientEmail = ISNULL(@RecipientEmail, '')
									SET @RecipientCopyEmail = ISNULL(@RecipientCopyEmail, '')
									SET @RecipientBlindCopyEmail = ISNULL(@RecipientBlindCopyEmail, '')
									
									SET @MAIL_QUERY = '	
														EXEC msdb.dbo.sp_send_dbmail
															@profile_name	= ''' + @profile + ''',
															@body 			= ''' + @body + ''',
															@body_format 	= ''HTML'',
															@recipients 	= ''' + @RecipientEmail + ''',
															' + @RecipientCopyEmail + '
															' + @RecipientBlindCopyEmail + '
															@subject 		= ''' + @subjek + ''''
															
									EXEC (@MAIL_QUERY)
									---=== End notification for approver
									
								END
								
								ELSE BEGIN
									RAISERROR ('Valid from cannot less then last valid from.',16,1)
									RETURN
								END
								
							END
							
						END
						ELSE BEGIN
							RAISERROR ('Valid from cannot less then current month.',16,1)
							RETURN
						END
						
				END
				ELSE BEGIN
					RAISERROR ('Please approved your last proposed child route.',16,1)
					RETURN
				END
				
			--END
			--ELSE BEGIN
				--RAISERROR ('Route Price already created.',16,1)
				--RETURN
			--END
			
		END
		
		ELSE BEGIN
			RAISERROR ('Route child cannot be same with route parent.',16,1)
			RETURN
		END
		
	END

	ELSE BEGIN
		RAISERROR ('You are not authorized to perform this action.',16,1)
		RETURN
	END
			
	
END;

