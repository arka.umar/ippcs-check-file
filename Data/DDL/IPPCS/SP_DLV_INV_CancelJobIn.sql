/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Register job for cancel invoice
*/

CREATE PROCEDURE [dbo].[SP_DLV_INV_CancelJobIn] 
@INV_NO VARCHAR (MAX),
@CREATED_BY VARCHAR (20),
@pid AS BIGINT

AS
BEGIN 

	DECLARE @process_status AS INT = 0
	DECLARE @na AS VARCHAR(50) = 'SP_DLV_INV_CancelJobIn'
	DECLARE @step AS VARCHAR(50) = 'init'
	-- DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)

	DECLARE @MODUL_ID VARCHAR(1) = '3';
	DECLARE @FUNCTION_ID VARCHAR(10) = '31405';

	DECLARE @err_message nvarchar(255);

	DECLARE @CHECK_MODULE INT;


	SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID=@FUNCTION_ID AND PROCESS_END_DT IS NULL );
	

	IF (@CHECK_MODULE=0) BEGIN
			
			DECLARE @x XML;
			SET @x = '<r>' + REPLACE((SELECT @INV_NO FOR XML PATH('')), ',', '</r><r>') + '</r>';

			--CREATE LOG
			SET @steps = @na+'.Process';
			EXEC dbo.sp_PutLog 'Cancel Job In:begin', @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

			INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
			VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());

			UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;
	
			UPDATE TB_R_DLV_INV_UPLOAD SET PROCESS_ID = @pid 
			WHERE LP_INV_NO+CAST(SEQ_NO AS VARCHAR) IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol)) AND STATUS_CD IN ('2', '3', '-2', '-4');
			
			DECLARE @ROW_COUNT INT, @COUNT_STATUS INT;


			SET @ROW_COUNT = (SELECT COUNT(*) FROM TB_R_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid);
			SET @COUNT_STATUS = (SELECT COUNT(*) FROM TB_R_DLV_INV_UPLOAD WHERE PROCESS_ID = @pid AND STATUS_CD IN ('2', '3', '-2', '-4'));

			IF (@ROW_COUNT = @COUNT_STATUS) BEGIN

				-- 20150519 / EFaktur / alira.dita / begin
				EXEC [SP_DLV_INV_Cancel] @CREATED_BY, @pid
				-- 20150519 / EFaktur / alira.dita / end
					
			END 
			ELSE BEGIN
					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4'
					,PROCESS_END_DT=GETDATE()
					WHERE PROCESS_ID = @pid;

					UPDATE TB_R_DLV_INV_UPLOAD SET PROCESS_ID = NULL WHERE LP_INV_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM    @x.nodes('/r') y(XmlCol));

					RAISERROR ('Please select records with status : created or submitted or rejected!', 16,1);
					RETURN
			END

			SET @steps = @na+'.Finish';
			EXEC dbo.sp_PutLog 'Cancel Job In:end', @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
	END
	ELSE BEGIN
			
			RAISERROR('Other process is still running by another user.',16,1)
			RETURN
	END
END

