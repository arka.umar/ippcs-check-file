/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : PR reject
*/

CREATE PROCEDURE [dbo].[SP_DLV_Reject_PR]

@PRNO Varchar (MAX),
@CREATED_BY VARCHAR (25)


AS
BEGIN
		
		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_Reject_PR'
		DECLARE @step AS VARCHAR(50) = 'Init'
		DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)

		DECLARE @MODUL_ID INT = 3
		DECLARE @FUNCTION_ID VARCHAR (6) =  '31105'
		DECLARE @CHECK_MODULE INT
		DECLARE @POSITION_ID INT, @CHECK_AUTH INT;
		DECLARE @CHECKSTS VARCHAR (3), @CHECK_STATUS_FLAG INT;
		DECLARE @STS VARCHAR (3), @POS INT, @LVL INT, @CHECK_APPROVAL INT, @CHECK_PO INT, @CHECK_STS_PO VARCHAR(4);
		

		CREATE TABLE #TEMP(
					PR_NO VARCHAR(12)
		);


		--EXPLODE
		--DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
		--DECLARE @MIN_ROW INT;

		--DECLARE @ROW_DETAIL VARCHAR(MAX);
		--DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
		--DECLARE @MIN_FIELD INT;

		--DECLARE @PRNO_T VARCHAR(12);

		--INSERT INTO @ROWS
		--SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@PRNO,';');
		--EXPLODE

		--==CREATE LOG==--
		SET @steps = @na+'.'+@step;
		SET @log = 'PR Rejection process started.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		--==END CREATE LOG==--
		

		SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID IN (@FUNCTION_ID, '31103') AND PROCESS_END_DT IS NULL );
		IF (@CHECK_MODULE=0) BEGIN

-- Insert QUEUE
				INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
				VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());
				


				--==CREATE LOG==--
				SET @steps = @na+'.Process';
				SET @log = 'Check authorization level';
				EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
				--==END CREATE LOG==--
				
		-- Update QUEUE
				UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;


				


				SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE JOB_FUNCTION_ID = 6 AND POSITION_ID IN (6,3,12) AND USERNAME = @CREATED_BY);
				
				IF (@CHECK_AUTH=1)BEGIN



						
						
						SET @POSITION_ID = (SELECT POSITION_LEVEL  FROM [dbo].[TB_R_VW_EMPLOYEE] WHERE USERNAME = @CREATED_BY);

						IF (@POSITION_ID =50) BEGIN
							SET @POS = 3;
							SET @STS = 'SH';
						END
						ELSE IF (@POSITION_ID =40) BEGIN
							SET @POS = 4;
							SET @STS = 'DpH';
						END
						ELSE BEGIN 
							SET @POS = 5;
							SET @STS = 'DH';
						END
						--ELSE BEGIN
							--SET @POS = 0;
							--SET @STS = 'PR2';
						--END

					--==CREATE LOG==--
					SET @steps = @na+'.Process';
					SET @log = 'Parsing data from parameter';
					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
					--==END CREATE LOG==--
					



					--==Temporary==--
						--  == VARIABLE TABLE TEMPORARY UNTUK EXPLODING GRID ==
						DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
						DECLARE @MIN_ROW INT;
						DECLARE @ROW_DETAIL VARCHAR(MAX);
						DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
						DECLARE @MIN_FIELD INT;
						-- ================================================================
						--  == Variabel Untuk Menyimpan Data Ke TABLE DETAIL ==
						DECLARE @arr_1 VARCHAR(12);
						INSERT INTO @ROWS
						SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@PRNO,';');
						-- ================================================================
						--  == Ambil Nilai terkecil dari VARIABEL TABLE @ROW ==
						SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

						WHILE @MIN_ROW IS NOT NULL BEGIN
									-- 6.3.1 == Hapus VARIABLE TABLE @FIELDS jika ada ==
									DELETE FROM @FIELDS;
									
									SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
									INSERT INTO @FIELDS
									SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'|');
									-- ========================================================================================
									-- 6.3.3 == Ambil ITEM Berdasarkan ID untuk dimasukkan kedalam VARIABLE FIELD Detail ==
									SET @arr_1 = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
									
									INSERT INTO #TEMP VALUES (@arr_1)

									SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
													-- =================================================================================
						END --END WHILE
						--==Temporary==--
						

						
						SET @CHECK_STATUS_FLAG = (SELECT COUNT(PR_STATUS_FLAG) FROM TB_R_DLV_PR_H 
																			JOIN #TEMP ON TB_R_DLV_PR_H.PR_NO=#TEMP.PR_NO
																			-- WHERE TB_R_DLV_PR_H.PR_STATUS_FLAG IN ('PR6','PR5'));
																			WHERE TB_R_DLV_PR_H.PR_STATUS_FLAG = 'PR6' );
						

						IF (@CHECK_STATUS_FLAG > 0) BEGIN
									--==CREATE LOG==--
									SET @steps = @na+'.Process';
									SET @log = 'Failed to reject PR';
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
									--==END CREATE LOG==--
														

									--==CREATE LOG==--
									SET @steps = @na+'.Finish';
									SET @log = 'PR Rejection process has been finished.';
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
									--==END CREATE LOG==--

						
									--== UPDATE QUEUE
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

									RAISERROR('Failed to reject.',16,1)
									RETURN

						END
						ELSE BEGIN
								
								
									DECLARE @PRNO_T varchar(12);

									DECLARE BTMP_CUR2 CURSOR
									FOR

									SELECT PR_NO
									FROM #TEMP;

									OPEN BTMP_CUR2;
									FETCH NEXT FROM BTMP_CUR2 INTO
									@PRNO_T;

									WHILE @@FETCH_STATUS = 0
									BEGIN


												--==CREATE LOG==--
												SET @steps = @na+'.Process';
												SET @log = 'Checking PR_STATUS_FLAG  for PR Number '+@PRNO_T;
												EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
												--==END CREATE LOG==--

												SET @CHECKSTS = (SELECT PR_STATUS_FLAG FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T);

												IF (@CHECKSTS = 'PR2') BEGIN
														SET @LVL = 2;
														SET @CHECK_APPROVAL = 1;
														SET @CHECK_PO = 0;
				
												END
												ELSE IF (@CHECKSTS = 'PR3') BEGIN
														SET @LVL = 3;
														SET @CHECK_PO = 0;
														SET @CHECK_APPROVAL = (SELECT COUNT(*) FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T 
																									 AND DPH_APPROVED_BY IS NULL AND DPH_APPROVED_DT IS NULL 
																									 AND DH_APPROVED_BY IS NULL AND DH_APPROVED_DT IS NULL);

												END
												ELSE IF (@CHECKSTS = 'PR4') BEGIN
														SET @LVL = 4;
														SET @CHECK_PO = 0;
														SET @CHECK_APPROVAL = (SELECT COUNT(*) FROM TB_R_DLV_PR_H WHERE PR_NO = @PRNO_T 
																									 AND DH_APPROVED_BY IS NULL AND DH_APPROVED_DT IS NULL);

												END
												ELSE BEGIN
														SET @LVL = 5;
														SET @CHECK_APPROVAL = 1;
														SET @CHECK_STS_PO = (SELECT PO_STATUS_FLAG FROM TB_R_DLV_PO_H WHERE PO_NO = (
																									SELECT MAX(h.PO_NO) FROM TB_R_DLV_PO_H h
																									JOIN TB_R_DLV_PO_D d
																									ON h.PO_NO=d.PO_NO AND d.PR_NO = @PRNO_T
																									GROUP BY d.PR_NO));
														IF (@CHECK_STS_PO = 'PO5' OR @CHECK_STS_PO IS NULL ) BEGIN
																SET @CHECK_PO = 0;
														END
														ELSE BEGIN
																SET @CHECK_PO = 1;
														END

												END
													
												-- IF (@CHECKSTS = 'PR5') OR (@CHECKSTS = 'PR6') BEGIN
												IF (@CHECKSTS = 'PR6') BEGIN
														--==CREATE LOG==--
														SET @steps = @na+'.Process';
														SET @log = 'Failed to reject PR Number '+@PRNO_T;
														EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
														--==END CREATE LOG==--
														

														--==CREATE LOG==--
														SET @steps = @na+'.Finish';
														SET @log = 'PR Rejection process has been finished.';
														EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
														--==END CREATE LOG==--

														--== UPDATE QUEUE
														UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

														RAISERROR('Failed to reject.',16,1)
														RETURN
												END
												ELSE BEGIN
														
														IF (@POS >= @LVL) AND (@CHECK_APPROVAL = 1) AND (@CHECK_PO = 0) BEGIN

																UPDATE TB_R_DLV_PR_H
																SET 
																REJECTED_BY = @CREATED_BY,
																REJECTED_POSITION = @STS,
																REJECTED_DT =  GETDATE(),
																PR_STATUS_FLAG = 'PR6',
																CHANGED_BY = @CREATED_BY,
																CHANGED_DT =  GETDATE()
																WHERE 
																PR_NO = @PRNO_T;

																--==CREATE LOG==--
																SET @steps = @na+'.Process';
																SET @log = 'Updating TB_R_DLV_PR_H for PR Number '+@PRNO_T+' By '+@STS;
																EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																--==END CREATE LOG==--
																
																DECLARE @PROD_MONTH varchar(12),@LP_CD varchar(4), @ROUTE_CD VARCHAR(4);

																DECLARE BTMP_CUR CURSOR
																FOR
																SELECT  prh.PROD_MONTH AS PROD_MONTH, prh.LP_CD AS LP_CD, prd.ROUTE_CD AS ROUTE_CD 
																FROM TB_R_DLV_PR_H prh JOIN TB_R_DLV_PR_D prd ON prh.PR_NO = prd.PR_NO
																WHERE prh.PR_NO=@PRNO_T;

																OPEN BTMP_CUR;
																FETCH NEXT FROM BTMP_CUR INTO
																@PROD_MONTH,@LP_CD, @ROUTE_CD;

																WHILE @@FETCH_STATUS = 0
																BEGIN
																	
																	--==CREATE LOG==--
																	SET @steps = @na+'.Process';
																	SET @log = 'Updating TB_T_PR_RETRIEVAL with PROD_MONTH = '+@PROD_MONTH+', LP_CD ='+@LP_CD+' AND ROUTE_CD='+@ROUTE_CD;
																	EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																	--==END CREATE LOG==--

																	UPDATE TB_T_PR_RETRIEVAL
																	SET PR_STATUS_FLAG = 'PR6', CHANGED_BY = @CREATED_BY, CHANGED_DT =  GETDATE()
																	WHERE PROD_MONTH =@PROD_MONTH AND LP_CD=@LP_CD AND ROUTE_CD=@ROUTE_CD ;

																FETCH NEXT FROM BTMP_CUR INTO
																	@PROD_MONTH,@LP_CD, @ROUTE_CD
																END;
																CLOSE BTMP_CUR;
																DEALLOCATE BTMP_CUR;
																
																
																---MODIF BY AGUNG 11.07.2014
																--CREATE LOG
																SET @steps = @na+'.Process';
																SET @log = 'Starting sent e-mail.';

																EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;				

																BEGIN TRY
																--===========SEND E-MAIL
																DECLARE @RecipientEmail varchar(max),
																		@RecipientCopyEmail varchar(max),
																		@RecipientBlindCopyEmail varchar(max),
																		@ERR_FLAG varchar(50),
																		@body varchar(max),
																		@subjek varchar(100),
																		@ERROR_DETAIL varchar(max),
																		@profile varchar(max),
																		@MAIL_QUERY VARCHAR(MAX)
										
																SET @profile = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE');
																IF (@profile IS NULL) BEGIN
																		SET @profile = 'NotificationAgent';
																END
								
																SELECT @RecipientEmail=EMAIL_TO, @RecipientCopyEmail=EMAIL_CC, @RecipientBlindCopyEmail=EMAIL_BCC 
																FROM TB_M_RTP_NOTIFICATION_EMAIL 
																WHERE MODULE_ID = CAST(@MODUL_ID AS VARCHAR) 
																	AND FUNCTION_ID = @FUNCTION_ID AND ACTION = 'PR_REJECT'
								
																SELECT  @subjek=NOTIFICATION_SUBJECT,
																		@body=NOTIFICATION_CONTENT
																FROM    TB_M_NOTIFICATION_CONTENT
																WHERE   FUNCTION_ID = @FUNCTION_ID
																		AND ROLE_ID = '34'
																		AND NOTIFICATION_METHOD = '1'
								
																SET @subjek = REPLACE(@subjek, '@ProdMonth', RIGHT(LEFT(@PRNO_T, 9), 6))
																SET @body = REPLACE(@body, '@DateTime', (SELECT CAST(DAY(GETDATE()) AS VARCHAR) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR)))
																SET @body = REPLACE(@body, '@rejectby', @CREATED_BY)
																SET @body = REPLACE(@body, '@prno', @PRNO_T)
																
																
																IF (@RecipientCopyEmail IS NOT NULL)
																	SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
																	
																IF (@RecipientBlindCopyEmail IS NOT NULL)
																	SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
																
																SET @body = ISNULL(@body, '')
																SET @subjek = ISNULL(@subjek, '')
																
																SET @RecipientEmail = ISNULL(@RecipientEmail, '')
																SET @RecipientCopyEmail = ISNULL(@RecipientCopyEmail, '')
																SET @RecipientBlindCopyEmail = ISNULL(@RecipientBlindCopyEmail, '')
								
																SET @MAIL_QUERY = '	
																					EXEC msdb.dbo.sp_send_dbmail
																						@profile_name	= ''' + @profile + ''',
																						@body 			= ''' + @body + ''',
																						@body_format 	= ''HTML'',
																						@recipients 	= ''' + @RecipientEmail + ''',
																						' + @RecipientCopyEmail + '
																						' + @RecipientBlindCopyEmail + '
																						@subject 		= ''' + @subjek + ''''
																
																EXEC (@MAIL_QUERY)
																
																SET @steps	= @na + '.Process';
																SET @log	= 'Email notification has been sent';

																EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																--===========END OF SEND E-MAIL

																	--CREATE LOG
																SET @steps = @na+'.Process';
																SET @log = 'send e-mail has been finished.';

																EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;

																
																--== UPDATE QUEUE
																UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

																--==FINISH==--

																END TRY
																BEGIN CATCH
																		DECLARE @ERROR VARCHAR(MAX)
																		SET @ERROR = ERROR_MESSAGE();
																		--CREATE LOG
																		SET @steps = @na+'.Process';
																		SET @log = 'Email notification failed sent to all PIC.';
																		EXEC dbo.sp_PutLog @ERROR, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
																		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;

																		UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
																		WHERE PROCESS_ID = @pid;


																END CATCH

																--UPDATE TB_T_PR_RETRIEVAL
																--SET PR_STATUS_FLAG = 'PR6', CHANGED_BY = @CREATED_BY, CHANGED_DT =  GETDATE()
																--WHERE PROD_MONTH =PROD_MONTH AND LP_CD=LP_CD AND ROUTE_CD=ROUTE_CD ;
																
																
														END

														ELSE BEGIN

																--== UPDATE QUEUE
																UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
							
																--==CREATE LOG==--
																SET @steps = @na+'.Process';
																SET @log = 'Failed to reject PR Number '+@PRNO_T;
																EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
																--==END CREATE LOG==--
																

																--==CREATE LOG==--
																SET @steps = @na+'.Finish';
																SET @log = 'PR Rejection process has been finished.';
																EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																--==END CREATE LOG==--

																RAISERROR('Failed to reject.',16,1)
																RETURN
														END

												END

										

									FETCH NEXT FROM BTMP_CUR2 INTO
										@PRNO_T
									END;
									CLOSE BTMP_CUR2;
									DEALLOCATE BTMP_CUR2;

						END


					
					--SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

							--WHILE @MIN_ROW IS NOT NULL BEGIN

									--DELETE FROM @FIELDS;
									

									--SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
									--INSERT INTO @FIELDS
									--SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'');
									

									--SET @PRNO_T = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
									-- ========================================================================================
									
									



					--SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
							-- =================================================================================
					
					--END --END WHILE
					
					--==CREATE LOG==--
					SET @steps = @na+'.Finish';
					SET @log = 'PR Rejection process has been finished.';
					EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
					--==END CREATE LOG==--
					
					UPDATE TB_R_DLV_PR_H SET PROCESS_ID = NULL;
					UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;

				END
				ELSE BEGIN
						
						--==CREATE LOG==--
						SET @steps = @na+'.Process';
						SET @log = 'Access denied!';
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID,0;
						--==END CREATE LOG==--
						
						--==CREATE LOG==--
						SET @steps = @na+'.Finish';
						SET @log = 'PR Rejection process has been finished.';
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,0;
						--==END CREATE LOG==--


						--== UPDATE QUEUE
						UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

						RAISERROR('You are not authorized to perform this action.',16,1)
						RETURN

				END

	END
	ELSE BEGIN
			
			RAISERROR('Other process is still running by another user.',16,1)
			RETURN
	END
END

