alter procedure sp_1_3_1_trace
  @trace int = 0
, @prod_month varchar(6) = null
, @pid bigint = NULL
, @username varchar(20) = NULL
, @traces VARCHAR(500) = NULL
, @MAT_NO VARCHAR(23) = NULL
as begin
declare
  @trace_clean         int =     1
, @trace_run           int =     2
, @trace_tmp           int =     4
, @trace_local_order   int =     8
, @trace_po_process    int =    16
, @trace_local_val     int =    32
, @trace_mat_price     int =    64
, @trace_material      int =   128
, @trace_valuation     int =   256
, @trace_sloc_data     int =   512
, @trace_tb_r          int =  1024
, @trace_data_val      int =  4096
, @trace_summary       int = 16384
, @trace_po_sprints    int = 32768
, @trace_log           int = 65536
, @MARK_PLANT_SLOC     INT =     1
, @MARK_MATERIAL       INT =     2
, @MARK_SLOC_MAT       INT =     4
, @MARK_PACK_INDICATOR INT =     8
, @MARK_MATERIAL_PRICE INT =    16
, @MARK_CALC_SCHEMA    INT =    32
, @MARK_SUPP_CD        INT =    64
, @MARK_SUPP_SCHEMA    INT =   128
, @MARK_DOC_TYPE       INT =   256
, @MARK_VALUATION      INT =   512
, @MARK_DOCK           INT =  1024

set nocount on;
if NULLIF(@traces,'') is not null
BEGIN
    select @TRACE=sum(v)
    from (
    SELECt distinct x.v
    FROM (select @traces t) t join
    (
    select n, v from (values
      ('clean'          , 1 )
    , ('run'            , 2 )
    , ('tmp'            , 4 )
    , ('local_order'    , 8 )
    , ('po_process'    , 16 )
    , ('local_val'     , 32 )
    , ('mat_price'     , 64 )
    , ('material'     , 128 )
    , ('tb_r'        , 1024 )
    , ('data_val'    , 4096 )
    , ('summary'    , 16384 )
    , ('po_sprints' , 32768 )
    , ('log'        , 65536 ))
    T (N,V)
    ) x on charindex(x.n, t.t) > 0
    ) a
END;
declare @marks TABLE (
    mark int,
    nm varchar(50)
    primary key (mark)
);

insert into @marks(mark, nm)
select mark, LOWER(rtrim(nm)) nm from (values
 (@MARK_PLANT_SLOC     ,'plant_sloc')
,(@MARK_MATERIAL       ,'material')
,(@MARK_SLOC_MAT       ,'sloc_mat')
,(@MARK_PACK_INDICATOR ,'pack_indicator')
,(@MARK_MATERIAL_PRICE ,'material_price')
,(@MARK_CALC_SCHEMA    ,'calc_schema')
,(@MARK_SUPP_CD        ,'supp_cd')
,(@MARK_SUPP_SCHEMA    ,'supp_schema')
,(@MARK_DOC_TYPE       ,'doc_type')
,(@MARK_VALUATION      ,'valuation')
,(@MARK_DOCK           ,'dock')
) T (mark, nm) ;

IF (select object_id('tempdb..#tb_t_mat')) IS NOT NULL
begin
    DROP TABLE #tb_t_mat;
end;

create TABLE #tb_t_mat (
  mat_no VARCHAR(23) NOT NULL
, mat_desc varchar(40)
, unit_of_measure_cd varchar(3)
, E INT DEFAULT(1) not null
, id int identity(1,1)
);
create index ix_tb_t_mat on #tb_t_mat (mat_no, e);

IF (select object_id('tempdb..#tb_t_val')) IS NOT NULL
begin
    DROP TABLE #tb_t_val;
end;

CREATE TABLE #tb_t_val
(PROD_PURPOSE_CD  varchar(5)      NOT NULL,
MAT_NO           varchar(23)     NOT NULL,
SOURCE_TYPE      varchar(1)      NOT NULL,
PLANT_CD         varchar(4)      NOT NULL,
e                int default(1) not null,
id               int identity(1,1)
);
create index ix_tb_t_val on #tb_t_val(MAT_NO, PROD_PURPOSE_CD,SOURCE_TYPE, PLANT_CD, E);

IF (select object_id('tempdb..#tb_t_sloc')) IS NOT NULL
begin
    DROP TABLE #tb_t_sloc;
end;

CREATE  TABLE #tb_t_sloc
(MAT_NO varchar(23)  not null,
PROD_PURPOSE_CD varchar(5) not null,
SOURCE_TYPE varchar(1) not null,
PLANT_CD varchar(4) not null,
SLOC_CD varchar(6) not null,
e int default(1) not null,
id int identity(1,1)
);
create index ix_tb_t_sloc on #tb_t_sloc (mat_no, prod_purpose_cd, source_type, plant_cd, sloc_cd, e);

declare @llowed_doc_type table(doc_type varchar(10));
    insert @llowed_doc_type(doc_type)
    select convert(varchar(10), items) doc_type
    from dbo.fn_split((
        select TOP 1 system_value
        from tb_m_system where FUNCTION_ID = '10301' and SYSTEM_CD = 'DOC_TYPE'
    ), ',') x;

--* FID.Ridwan: 20210309 -> PO Manual SPRINTS
declare @llowed_doc_type_spr table(doc_type varchar(10));
    insert @llowed_doc_type_spr(doc_type)
    select convert(varchar(10), items) doc_type
    from dbo.fn_split((
        select TOP 1 system_value
        from tb_m_system where FUNCTION_ID = '10301' and SYSTEM_CD = 'DOC_TYPE_SPR'
    ), ',') x;

IF (select object_id('tempdb..#tb_t_po_supp')) IS NULL
    CREATE TABLE #TB_T_PO_SUPP (
        SUPP_CD                     varchar(6)
      , PO_NO                       varchar(10)
      , PROD_MONTH                  numeric(6, 0)
      , DOC_TYPE                    VARCHAR(2)
      , REFID                       INT
      , ID                          INT IDENTITY(1,1)
    );
IF (select object_id('tempdb..#tb_t_po_mat')) IS NULL
    CREATE TABLE #TB_T_PO_MAT (
      PO_NO                       varchar(10)
    , PO_ITEM_NO                  varchar(5)
    , PO_QUANTITY_NEW             numeric(13, 3)
    , PO_QUANTITY_OPEN            numeric(13, 3)
    , PO_MAT_PRICE                numeric(16, 5)
    , DELIVERY_DT                 date
    , REFID                       INT
    , ID                          int IDENTITY(1,1)
    );
IF (select object_id('tempdb..#tb_t_po_h')) IS NULL
    CREATE TABLE #TB_T_PO_H (
      PO_NO                       varchar(10)
    , SUPP_CD                     varchar(6)
    , DOC_DT                      date
    , PO_NOTE                     varchar(100)
    , INV_WO_GR_FLAG              varchar(1)
    , DELETION_FLAG               varchar(1)
    , CALCULATION_SCHEMA_CD       varchar(4)
    , DOC_TYPE                    varchar(2)
    , PROD_MONTH                  numeric(6)
    , PO_EXCHANGE_RATE            numeric(13,2)
    , PO_CURR                     varchar(3)
    , PAYMENT_METHOD_CD           varchar(2)
    , PAYMENT_TERM_CD             varchar(4)
    , REFF_INV_NO                 varchar(20)
    , REFF_INV_DT                 date
    , REFF_DOC                    varchar(20)
    , REFF_DOC_DT                 date
    , RECORD_ID                   INT
    , ID                          INT IDENTITY (1,1)
    );


IF (select object_id('tempdb..#tb_t_po_i')) IS NULL
    CREATE TABLE #TB_T_PO_I (
      PO_NO                        varchar(10)
    , PO_ITEM_NO                   varchar(5)
    , PR_NO                        varchar(8)
    , PR_ITEM                      varchar(5)
    , MAT_NO                       varchar(23)
    , SOURCE_TYPE                  varchar(1)
    , PLANT_CD                     varchar(4)
    , PROD_PURPOSE_CD              varchar(5)
    , SLOC_CD                      varchar(6)
    , PART_COLOR_SFX               varchar(2)
    , UNLIMITED_FLAG               varchar(1)
    , TOLERANCE_PERCENTAGE         numeric(5,2)
    , PO_GR_QUANTITY               numeric(13,3)
    , CONTAINER_NO                 varchar(10)
    , NOTE                         varchar(100)
    , SPECIAL_PROCUREMENT_TYPE_CD  varchar(2)
    , MAT_DESC                     varchar(40)
    , PO_QUANTITY_ORIGINAL         numeric(13,3)
    , PO_QUANTITY_NEW              numeric(13,3)
    , PO_QUANTITY_OPEN             numeric(13,3)
    , UNIT_OF_MEASURE_CD           varchar(3)
    , PO_MAT_PRICE                 numeric(16,5)
    , DELIVERY_DT                  date
    , DELETION_FLAG                varchar(1)
    , TAX_CD                       varchar(2)
    , PACKING_TYPE                 varchar(1)
    , SUBCONTRACT_FLAG             varchar(1)
    , RECORD_ID                    INT
    , ID                           INT IDENTITY(1,1)
    )
IF (select object_id('tempdb..#tb_t_po_d')) IS NULL
    CREATE TABLE #TB_T_PO_D (
      PO_NO                        varchar(10)
    , PO_ITEM_NO                   varchar(5)
    , COMP_PRICE_CD                varchar(4)
    , COMP_PRICE_RATE              numeric(16,5)
    , INVOICE_FLAG                 varchar(1)
    , PO_EXCHANGE_RATE             numeric(13,2)
    , SEQ_NO                       numeric(3)
    , BASE_VALUE_F                 numeric(3)
    , BASE_VALUE_T                 numeric(3)
    , CALCULATION_TYPE             varchar(1)
    , PLUS_MINUS_FLAG              varchar(1)
    , SUPP_CD                      varchar(6)
    , PO_CURR                      varchar(3)
    , PO_DETAIL_PRICE              numeric(16,5)
    , PO_DETAIL_AMT                numeric(16,5)
    , INVENTORY_FLAG               varchar(1)
    , ACCRUAL_POSTING_FLAG         varchar(1)
    , CONDITION_CATEGORY           varchar(1)
    , RECORD_ID                    INT
    , ID                           INT IDENTITY(1,1)
    )

declare @v_prod_date datetime;

select @v_prod_date = CONVERT(DATETIME, @prod_month+'01', 112) ;
declare @v_color_sfx varchar(2) ='00';

SELECT TOP 1 @v_color_sfx = LTRIM(RTRIM(SYSTEM_VALUE))
FROM TB_M_SYSTEM WHERE FUNCTION_ID = '300' AND SYSTEM_CD = 'PART_COLOR_SFX_PRICE';

DECLARE @l_color_sfx varchar(2);
SET @l_color_sfx = COALESCE((
SELECT TOP 1 l_color_sfx = CONVERT(VARCHAR(2), LTRIM(RTRIM(SYSTEM_VALUE)))
FROM TB_M_SYSTEM WHERE FUNCTION_ID = '300' AND SYSTEM_CD = 'PART_COLOR_SFX_LAST'), 'X');


if @trace  & @trace_clean > 0
begin
    declare @anew bigint = @pid
    if not exists( select top 1 1 process_id from tb_r_log_h where process_id = @anew)
    begin
        exec PutLog @what = 'Start trace 1_3_1', @USER=@username, @WHERE = 'sp_1_3_1_trace', @pid = @anew output, @id='MPCS00001INF', @func='10301', @sts = 0
    end
    else
    begin
        delete from tb_r_log_d where process_Id = @pid and SEQUENCE_NUMBER > 1;
        update tb_r_log_h set PROCESS_STATUS =0, CHANGED_BY='sp_1_3_1_trace', CHANGED_DATE = getdate() where process_id = @pid;
    end;

    DECLARE @PID_null BIGINT=null;
    exec Lock_process @function='10301', @LOCK_KEY='', @PROCESS_ID=@PID_null OUTPUT, @LOCK_USER= 'unlock'
    set @pid_null = null;
    exec Lock_process @function='111603', @LOCK_KEY='', @PROCESS_ID=@PID_null OUTPUT, @LOCK_USER= 'unlock'

    declare @rc int =0;
    if exists(select top 1 1 from tb_t_local_val_d)
    begin
        select @rc  =count(1) from tb_t_locaL_val_d;
        print convert(varchar(10), @rc) + ' local_val_d truncate'
        truncate table tb_t_local_val_d
    end;

    if exists(select top 1  1 from tb_t_local_val_h)
    begin
        delete i from tb_r_po_item i join tb_t_local_val_h h on h.po_no = i.po_no where h.po_no is not null;
        delete d from tb_r_po_d d    join tb_t_local_val_h h on h.po_no = d.po_no where h.po_no is not null;
        delete h from tb_r_po_h h    join tb_t_local_val_h a on a.po_no = h.po_no where a.po_no is not null;

        select @rc  =count(1) from tb_t_locaL_val_h;
        print convert(varchar(10), @rc) + ' local_val_h truncate'
        truncate table tb_t_local_val_h
    end;

    if exists(select top 1 1 from tb_t_po_process where system_source = 'LOCAL_ORDER')
    begin
        delete from tb_t_po_process where system_source = 'LOCAL_ORDER'
        print convert(varchar(10), (@@rowcount)) + ' po_process deleted';
    end

	--* FID.Ridwan: 20210309 -> PO Manual SPRINTS
    if exists(select top 1 1 from TB_T_PO_SPRINTS where system_source = 'LOCAL_ORDER')
    begin
        delete from TB_T_PO_SPRINTS where system_source = 'LOCAL_ORDER'
        print convert(varchar(10), (@@rowcount)) + ' po_process_spr deleted';
    end
end;

if @trace & @trace_run >0
begin
    exec sp_1_3_1_CREATE_PO @prod_month = @prod_month, @process_id = @pid output, @username = @username
    print 'pid: ' + coalesce(convert(varchar(15), @pid ),'NULL')
end;

IF @trace & (@trace_data_val | @trace_material | @trace_local_order | @trace_po_process | @trace_po_sprints) > 0
begin
    declare @@mat_Nos varchar(max) = convert(varchar(max), '')
        , @@kodes varchar(max) = convert(varchar(max), '')
        , @@sql varchar(max) = CONVERT(VARCHAR(MAX), '')
        , @@dblink_ics_new varchar(50)
        ;

    if @trace & @trace_local_val>0
    begin
        insert #tb_t_mat (mat_no, e)
        select MAT_NO, 0
        from TB_T_LOCAL_VAL_H
        WHERE (@prod_month IS NULL OR PROD_MONTH = @prod_month)
        AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by mat_no;
    end;

    if @trace & @trace_po_process >0
    begin
        insert #tb_t_mat (mat_no, e)
        select MAT_NO, 0
        from TB_T_PO_PROCESS
        WHERE PROD_MONTH = @prod_month
        AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by mat_no;
    end;

    IF @TRACE & @trace_local_order > 0
    BEGIN
        insert #tb_t_mat (mat_no, e)
        select MAT_NO, 0
        from TB_T_LOCAL_ORDER
        WHERE PROD_MONTH = @prod_month
        AND (@PID IS NULL OR OTHER_PROCESS_ID = @pid)
        AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by mat_no;
    END;
	
	--* FID.Ridwan: 20210309 -> PO Manual SPRINTS
    if @trace & @trace_po_sprints >0
    begin
        insert #tb_t_mat (mat_no, e)
        select MAT_NO, 0
        from TB_T_PO_SPRINTS
        WHERE PROD_MONTH = @prod_month
        AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by mat_no;
    end;


    SELECT @@dblink_ics_new = COALESCE((
    SELECT  CONVERT(VARCHAR(50), SYSTEM_VALUE) dblink_ics_new
      FROM TB_M_SYSTEM
     WHERE FUNCTION_ID='LINKED_SERVER' AND SYSTEM_CD = 'NEW_ICS_DB'
    ), '[BB071_DB]');

    DECLARE @@mid1 int = 1, @@mid2 INT = -1, @@midi int = 300, @@mj int, @@mit int = 0;

    select @@mj = count(1) from #tb_t_mat;

    while @@mid1 <= @@mj
    begin
        set @@mid2 = @@mid1 + @@midi-1;
        set @@mit = @@mit + 1;
        select @@mat_nos= stuff((select CONVERT(VARCHAR(MAX),',''') + mat_no +''''
        from (
        select m.MAT_NO  from #tb_t_mat m
        where id between @@mid1 and @@mid2
        and e = 0
        )  x
        for xml path('')), 1,1,'');

        select @@sql = Convert(varchar(max), '') +
            'SELECT MAT_NO,MAT_DESC,UNIT_OF_MEASURE_CD FROM OPENQUERY('+ @@dblink_ics_new+ ',
            ''  select mat_no,mat_desc,unit_of_measure_cd
                from BB071_DB.dbo.TB_M_MATERIAL m
                where m.MAT_NO IN (
                ' + REPLACE(coalesce( nullif( @@mat_Nos,'') , ''''''), '''', '''''') + '
                );
               '')';

        if (@trace_material & @trace) > 0
        begin
            print @@sql;
        end;
        insert into #tb_t_mat (mat_no, mat_desc, unit_of_measure_cd)
        exec (@@sql);

        set @@mid1 = @@mid1+ @@midi;
    end;

    delete from #tb_t_mat where e = 0;

    if @trace & @trace_material > 0
    begin
        select mat_no, mat_desc from #tb_t_mat;
    end;

    IF (@trace & @trace_local_val) > 0
    begin
        insert #tb_t_val (mat_no, PROD_PURPOSE_CD, SOURCE_TYPE, plant_cd, e)
        select mat_no, coalesce(prod_purpose_cd,''), coalesce(source_type,''), coalesce(plant_cd,''), 0
          from TB_T_LOCAL_VAL_H
        WHERE (@prod_month IS NULL OR PROD_MONTH = @prod_month)
          AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by mat_no, prod_purpose_cd, source_type, plant_cd;
    end;

    if (@trace & @trace_local_order) > 0
    begin
        insert #tb_t_val (mat_no, PROD_PURPOSE_CD, SOURCE_TYPE, plant_cd, e)
        select l.mat_no, coalesce(l.prod_purpose_cd,''), coalesce(l.source_type,''), coalesce(d.plant_cd,''), 0
          from TB_T_LOCAL_ORDER l
          JOIN TB_M_DOCK_CD D
            on d.DOCK_CD = l.DOCK_CD
           and @v_prod_date between d.VALID_DT_FR and d.VALID_DT_TO
         WHERE (@prod_month IS NULL OR PROD_MONTH = @prod_month)
           AND (@pid IS NULL OR l.OTHER_PROCESS_ID = @pid)
           AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by l.mat_no, l.prod_purpose_cd,  l.source_type, d.plant_cd;
    end;

    if (@trace & @trace_po_process) > 0
    begin
        insert #tb_t_val (mat_no, PROD_PURPOSE_CD, SOURCE_TYPE, plant_cd, e)
        select mat_no, coalesce(prod_purpose_cd,''), coalesce(source_type,''), coalesce(plant_cd,''), 0
          from TB_T_PO_PROCESS
        WHERE (@prod_month IS NULL OR PROD_MONTH = @prod_month)
          AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by mat_no, prod_purpose_cd, source_type, plant_cd;
    end;
	
	--* FID.Ridwan: 20210309 -> PO Manual SPRINTS
    if (@trace & @trace_po_sprints) > 0
    begin
        insert #tb_t_val (mat_no, PROD_PURPOSE_CD, SOURCE_TYPE, plant_cd, e)
        select mat_no, coalesce(prod_purpose_cd,''), coalesce(source_type,''), coalesce(plant_cd,''), 0
          from TB_T_PO_SPRINTS
        WHERE (@prod_month IS NULL OR PROD_MONTH = @prod_month)
          AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by mat_no, prod_purpose_cd, source_type, plant_cd;
    end;

    select @@mj = count(1) from #tb_t_val;
    select @@mid1= 1, @@mid2 = -1, @@midi = 100, @@mit = 0;

    while @@mid1 <= @@mj
    begin
        set @@mid2 = @@mid1 + @@midi-1;
        set @@mit = @@mit + 1;

        select @@kodes =
        stuff((SELECT ',(''' + MAT_NO + ''''
        + ',''' + PROD_PURPOSE_CD + ''''
        + ',''' + SOURCE_TYPE + ''''
        + ',''' + PLANT_CD + ''')'
        FROM #tb_t_val a
        where id between @@mid1 and @@mid2 and e = 0
        for xml path('')),1,1,'')

        select @@sql = convert(varchar(max), '')
        +  'SELECT MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD  FROM OPENQUERY(' + @@dblink_ics_new+ ',
        ''SELECT v.MAT_NO, v.PROD_PURPOSE_CD, v.SOURCE_TYPE, v.PLANT_CD
         FROM [BB071_DB].dbo.TB_R_MATERIAL_VALUATION v
         JOIN (VALUES '
         + COALESCE(NULLIF(REPLACE(@@kodes, '''', ''''''), '') , ' ('''''''','''''''','''''''','''''''') ' )+
               ') X (MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD)
         ON X.MAT_NO = V.MAT_NO
         AND X.PROD_PURPOSE_CD = coalesce(V.PROD_PURPOSE_CD, '''''''')
         AND X.SOURCE_TYPE = coalesce(V.SOURCE_TYPE,'''''''')
         AND X.PLANT_CD = coalesce(X.PLANT_CD,'''''''')
         '')
         group by  MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD';
         if @trace & @trace_valuation > 0
            print @@sql;

        insert into #tb_t_val (mat_no, prod_purpose_cd, source_type, plant_cd)
        exec (@@sql);

        set @@mid1 = @@mid1+ @@midi;
    end;

    delete from #tb_t_val where e= 0;

    if @trace &  @trace_valuation > 0
    begin
        select mat_no, prod_purpose_cd, source_type, plant_cd from #tb_t_val;
    end

    if @trace & @trace_local_val > 0
    begin
        INSERT #tb_t_sloc (MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD, e)
        select MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD, 0
        from TB_T_LOCAL_VAL_H
        WHERE PROD_MONTH = @prod_month
        group by MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD;
    end;

    if  @trace & @trace_local_order > 0
    begin
        INSERT #tb_t_sloc (MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD, e)
        select l.MAT_NO, l.PROD_PURPOSE_CD, l.SOURCE_TYPE, d.PLANT_CD, d.SLOC_CD, 0
        from TB_T_LOCAL_ORDER L
        LEFT JOIN TB_M_DOCK_CD D
            on d.DOCK_CD = l.DOCK_CD
           and @v_prod_date between d.VALID_DT_FR and d.VALID_DT_TO
        WHERE PROD_MONTH = @prod_month
        AND (@PID IS NULL OR OTHER_PROCESS_ID = @PID)
        AND (@MAT_NO IS NULL OR MAT_NO = @MAT_NO)
        group by L.MAT_NO, l.PROD_PURPOSE_CD, l.SOURCE_TYPE, d.PLANT_CD, d.SLOC_CD;
    end;

    IF @trace & @trace_po_process > 0
    begin
        INSERT #tb_t_sloc (MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD, e)
        select MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD, 0
        from TB_T_PO_PROCESS
        WHERE PROD_MONTH = @prod_month
        group by MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD;
    end;
	
	--* FID.Ridwan: 20210309 -> PO Manual SPRINTS
    IF @trace & @trace_po_sprints > 0
    begin
        INSERT #tb_t_sloc (MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD, e)
        select MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD, 0
        from TB_T_PO_SPRINTS
        WHERE PROD_MONTH = @prod_month
        group by MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD;
    end;

    select @@mj = count(1) from #tb_t_sloc;
    select @@mid1= 1, @@mid2 = -1, @@midi = 100, @@mit = 0;

    while @@mid1 <= @@mj
    begin
        set @@mid2 = @@mid1 + @@midi-1;
        set @@mit = @@mit + 1;

        select @@kodes =
        stuff((SELECT ',(''' + MAT_NO + ''''
        + ',''' + PROD_PURPOSE_CD + ''''
        + ',''' + SOURCE_TYPE + ''''
        + ',''' + PLANT_CD +  ''''
        + ',''' + SLOC_CD + ''')'
        FROM #tb_t_sloc a
        where id between @@mid1 and @@mid2 and e =0
        for xml path('')),1,1,'')

        select @@sql = convert(varchar(max), '')
        +  'SELECT MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD  FROM OPENQUERY(' + @@dblink_ics_new+ ',
        ''SELECT v.MAT_NO, v.PROD_PURPOSE_CD, v.SOURCE_TYPE, v.PLANT_CD, v.SLOC_CD
            FROM [BB071_DB].dbo.TB_R_SLOC_DATA_MATERIAL v
            JOIN (VALUES '
            + COALESCE(NULLIF(REPLACE(@@kodes, '''', ''''''), '') , ' ('''''''','''''''','''''''','''''''','''''''') ' )+
                ') X (MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PLANT_CD, SLOC_CD)
            ON X.MAT_NO = V.MAT_NO
            AND X.PROD_PURPOSE_CD = coalesce(V.PROD_PURPOSE_CD, '''''''')
            AND X.SOURCE_TYPE = coalesce(V.SOURCE_TYPE,'''''''')
            AND X.PLANT_CD = coalesce(X.PLANT_CD,'''''''')
            and x.SLOC_CD = COALESCE(X.SLOC_CD, '''''''')
           group by v.mat_no, v.prod_purpose_cd, v.source_type, v.plant_cd, v.sloc_cd
            '')
            ';
        if @trace& @trace_sloc_data >0
            print @@sql;
        insert into #tb_t_sloc (mat_no, prod_purpose_cd, source_type, plant_cd, sloc_cd)
        exec (@@sql);

        set @@mid1 = @@mid1+ @@midi;
    end;

    delete from #tb_t_sloc where e = 0;

    if @trace & @trace_sloc_data > 0
    begin
        select mat_no, prod_purpose_cd, source_type, plant_cd, sloc_cd
        from #tb_t_sloc;
    end
end;

if @trace & @trace_local_order > 0
begin
    with v as ( select
        RECORD_ID= ROW_NUMBER() OVER(ORDER BY
         SUPP_CD, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PART_COLOR_SFX)
        , DOCK_CD
        , SUPP_CD
        , MAT_NO
        , PROD_PURPOSE_CD
        , SOURCE_TYPE
        , MAT_DESC
        , PART_COLOR_SFX
        , QTY_N
        , QTY_N1
        , QTY_N2
        , QTY_N3
        , OTHER_PROCESS_ID
        , PROD_MONTH
    from dbo.TB_T_LOCAL_ORDER with (nolock)
    where (nullif(@pid, 0) is null or @pid= OTHER_PROCESS_ID)
    AND (NULLIF(@prod_month, '') IS NULL OR prod_month = @prod_month)
    AND (NULLIF(@MAT_NO, '') IS NULL OR MAT_NO = @MAT_NO )
    ), S AS (SELECT
          V.RECORD_ID, v.SUPP_CD, SI.SUPP_NAME
        , SI.PAYMENT_METHOD_CD
        , SI.PAYMENT_TERM_CD
        , SCH.CALCULATION_SCHEMA_CD
        , [MARK]
          = CASE WHEN si.SUPP_NAME is null then @MARK_SUPP_CD else 0 end
          | case when sch.CALCULATION_SCHEMA_CD is null then @MARK_SUPP_SCHEMA  else 0 end
        , SEQ= ROW_NUMBER() OVER(
            PARTITION BY V.RECORD_ID
            ORDER BY sch.VALID_DT_TO DESC, sch.valid_dt_to DESC, SI.SUPP_NAME
        )
    FROM V
    LEFT JOIN TB_M_SUPPLIER_ICS si on si.SUPP_CD = V.SUPP_CD
    LEFT JOIN TB_M_SUPPLIER_SCHEMA sch
           on sch.SUPP_CD = V.SUPP_CD
          AND @v_prod_date between VALID_DT_FR AND VALID_DT_TO
    ), dock as (select
        v.record_id , d.dock_cd , d.SLOC_CD, d.PLANT_CD,
        seq = row_number() over(
            partition by v.record_id
            order by d.valid_dt_to desc, d.valid_dt_fr desc)
    from v
    JOIN TB_M_DOCK_CD D
       on d.DOCK_CD = v.DOCK_CD
      and @v_prod_date between d.VALID_DT_FR and d.VALID_DT_TO
    ), cp as (select
        s.record_id
        , cp.calculation_schema_cd
        , seq = row_number() over(partition by s.record_id order by cp.seq_no)
    from s
    join TB_M_COMP_PRICE cp on cp.CALCULATION_SCHEMA_CD = s.CALCULATION_SCHEMA_CD
    ) , PACK AS (SELECT
          RECORD_ID
        , i.PACKING_TYPE_CD
        , SEQ = ROW_NUMBER() OVER (
            PARTITION BY V.RECORD_ID
            ORDER BY I.VALID_DT_TO DESC, I.VALID_DT_FR DESC)
    from v
    join TB_M_PACKING_INDICATOR i
      on i.dock_cd = v.DOCK_CD
     and i.MAT_NO = v.MAT_NO
     and i.PROD_PURPOSE_CD = v.PROD_PURPOSE_CD
     and i.SOURCE_TYPE = v.SOURCE_TYPE
     and i.PART_COLOR_SFX = v.PART_COLOR_SFX
     and @v_prod_date between i.VALID_DT_FR and i.VALID_DT_TO
    ), MATPRICE AS (SELECT
        V.RECORD_ID
        , mp.PRICE_AMT
        , mp.CURR_CD
        , SEQ = ROW_NUMBER() over(
            partition by v.record_id
            order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.part_color_sfx = coalesce(v.part_color_sfx, @v_color_sfx)
     and mp.packing_type = coalesce(p.PACKING_TYPE_CD,'0')
     and mp.supp_cd = v.supp_cd
     and mp.approve_sts = 'Y'
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ), MATPRICEL AS (SELECT
        V.RECORD_ID
        , SEQ = ROW_NUMBER() over(
            partition by v.record_id
            order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
        , mp.PRICE_AMT, mp.CURR_CD
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.SEQ = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.PART_COLOR_SFX = @l_color_sfx
     and mp.packing_type = coalesce(p.PACKING_TYPE_CD, '0')
     and mp.supp_cd = v.supp_cd
     and mp.APPROVE_STS ='Y'
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ),MATPRICEV AS (SELECT
        V.RECORD_ID
        , mp.PRICE_AMT, mp.CURR_CD
        , SEQ = ROW_NUMBER() over(
            partition by v.record_id
            order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
    and mp.source_type = v.source_type
    and mp.prod_purpose_cd = v.prod_purpose_cd
    and mp.PART_COLOR_SFX = @v_color_sfx
    and mp.packing_type = coalesce(p.PACKING_TYPE_CD, '0')
    and mp.supp_cd = v.supp_cd
    AND mp.APPROVE_STS ='Y'
	and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ), LO AS (select
        V.RECORD_ID
        , [MARK] =
          CASE WHEN v.SUPP_CD IS NULL THEN @MARK_SUPP_CD ELSE 0 END
        | CASE WHEN d.PLANT_CD is null or d.SLOC_CD is null then @MARK_PLANT_SLOC ELSE 0 END
        | CASE WHEN mm.mat_no is null then @MARK_MATERIAL else 0 end
        | CASE WHEN mv.id is null then @MARK_VALUATION else 0 end
        | CASE WHEN sd.id IS NULL THEN @MARK_SLOC_MAT ELSE 0 END
        | case when p.PACKING_TYPE_CD is null then @MARK_PACK_INDICATOR else 0 end
        | CASE WHEN coalesce(mp.seq, mpv.seq, ml.seq) is null then @MARK_MATERIAL_PRICE else 0 end
        | case when cp.CALCULATION_SCHEMA_CD is null then @MARK_CALC_SCHEMA else 0 end
        , V.DOCK_CD
        , d.PLANT_CD
        , d.SLOC_CD
        , V.SUPP_CD
        , V.MAT_NO
        , V.PROD_PURPOSE_CD
        , V.SOURCE_TYPE
        , mm.MAT_DESC
        , V.PART_COLOR_SFX
        , PACKING_TYPE_CD = COALESCE(p.PACKING_TYPE_CD, '0')
        , V.QTY_N
        , V.QTY_N1
        , V.QTY_N2
        , V.QTY_N3
        , COALESCE(mp.curr_cd, MPV.CURR_cD, ML.CURR_CD) CURR_CD
        , COALESCE(MP.PRICE_AMT, MPV.PRICE_AMT, ML.PRICE_AMT) PRICE_AMT
        , V.OTHER_PROCESS_ID
        , V.PROD_MONTH
        , S.SUPP_NAME
        , S.PAYMENT_METHOD_CD
        , S.PAYMENT_TERM_CD
        , S.CALCULATION_SCHEMA_CD
    from v
    LEFT JOIN s
           on s.RECORD_ID = v.RECORD_ID
          and s.SUPP_CD= v.SUPP_CD
          AND S.SEQ = 1
    left join dock d
           on d.RECORD_ID = v.RECORD_ID
          and d.seq = 1
    LEFT JOIN #tb_t_mat mm
           on mm.mat_no = v.MAT_NO
          and mm.e =1
    left join #tb_t_val mv
           on mv.MAT_NO = v.mat_no
          and mv.PROD_PURPOSE_CD =  v.PROD_PURPOSE_CD
          and mv.PLANT_CD = D.PLANT_CD
          AND MV.SOURCE_TYPE = V.SOURCE_TYPE
          AND MV.E =1
    LEFT JOIN #tb_t_sloc sd
           on sd.MAT_NO = v.mat_no
          and sd.PROD_PURPOSE_CD = v.PROD_PURPOSE_CD
          and sd.SOURCE_TYPE = V.SOURCE_TYPE
          and sd.PLANT_CD = d.PLANT_CD
          and sd.SLOC_CD =  d.SLOC_CD
          and sd.e = 1
    left join pack p
           on p.RECORD_ID = v.RECORD_ID
          and p.seq = 1
    left join matprice mp
           on v.RECORD_ID =  mp.RECORD_ID
          and mp.seq = 1
    LEFT JOIN MATPRICEV mpv
           on v.RECORD_ID =  mpv.RECORD_ID
          AND MPV.SEQ = 1
    LEFT JOIN MATPRiCEL ml
           ON v.RECORD_ID = ml.RECORD_ID
          and ml.SEQ = 1
    left join cp
           on cp.RECORD_ID = v.RECORD_ID
          and cp.seq = 1
    )
    SELECT
        lo.RECORD_ID, MARK, r.err
        , SUPP_CD, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PART_COLOR_SFX, PACKING_TYPE_CD, DOCK_CD, PLANT_CD, SLOC_CD
        , PAYMENT_METHOD_CD, PAYMENT_TERM_CD, CALCULATION_SCHEMA_CD
        , QTY_N, QTY_N1, QTY_N2, QTY_N3
        , CURR_CD, PRICE_AMT
        , SUPP_NAME, MAT_DESC
        , OTHER_PROCESS_ID
        , PROD_MONTH
    FROM LO
    left join (
        select
            la.record_id
          , err = stuff(
            (select ','+ m.nm
            from lo l
            join @marks m on m.mark & l.mark > 0
            where l.record_id = la.record_id
            for xml path(''))
            , 1 ,1, '' )
        from lo la
        group by record_id
    ) r on r.RECORD_ID = lo.RECORD_ID
    order by lo.RECORD_ID

end;

IF @trace & @trace_po_process> 0
begin
    with v as (select
        RECORD_ID= ROW_NUMBER() OVER(
            ORDER BY DOC_TYPE,SUPP_CD, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PART_COLOR_SFX)
        , SEQ_NO
        , DOC_TYPE
        , SUPP_CD
        , DOCK_CODE
        , MAT_NO
        , PROD_PURPOSE_CD
        , SOURCE_TYPE
        , PLANT_CD
        , SLOC_CD
        , PACKING_TYPE_cD
        , PROD_MONTH
        , QUANTITY
        , PART_COLOR_SFX
        , UNLIMITED_FLAG
        , TOLERANCE_PERCENTAGE
        , PR_NO
        , PR_ITEM_NO
        , PROCESS_STATUS
        , PO_NO
        , PO_ITEM_NO
        , OTHER_PROCESS_ID
        , PROCESS_ID
        , SYSTEM_SOURCE
    from dbo.TB_T_PO_PROCESS with (nolock)
    where (NULLIF(@prod_month,'') is null or PROD_MONTH = @prod_month)
    AND (NULLIF(@MAT_NO, '') IS NULL OR @MAT_NO = MAT_NO )
    ) , S AS (SELECT
        V.RECORD_ID, v.SUPP_CD, SI.SUPP_NAME
        , SI.PAYMENT_METHOD_CD
        , SI.PAYMENT_TERM_CD
        , SCH.CALCULATION_SCHEMA_CD
        , [MARK]
        = CASE WHEN si.SUPP_NAME is null then @MARK_SUPP_CD else 0 end
        | case when sch.CALCULATION_SCHEMA_CD is null then @MARK_SUPP_SCHEMA  else 0 end
        , SEQ = ROW_NUMBER() OVER(
            PARTITION BY V.RECORD_ID
            ORDER BY sch.VALID_DT_TO DESC, sch.valid_dt_to DESC, SI.SUPP_NAME)
    FROM V
    LEFT JOIN TB_M_SUPPLIER_ICS si on si.SUPP_CD = V.SUPP_CD
    LEFT JOIN TB_M_SUPPLIER_SCHEMA sch on sch.SUPP_CD = V.SUPP_CD AND @v_prod_date between VALID_DT_FR AND VALID_DT_TO
    ), cp as (select
        s.record_id
        , cp.calculation_schema_cd
        , seq = row_number() over(
            partition by s.record_id
            order by cp.seq_no)
    from s
    join TB_M_COMP_PRICE cp on cp.CALCULATION_SCHEMA_CD = s.CALCULATION_SCHEMA_CD
    ) , dock as (select
        V.RECORD_ID
        , D.DOCK_CD
        , SEQ = Row_number() over(
            partition by v.record_id
            order by d.valid_dt_to desc, d.valid_dt_fr desc)
    from v
    JOIN TB_M_DOCK_CD D
       on d.sloc_CD = v.SLOC_CD and d.PLANT_CD = v.PLANT_CD
      and @v_prod_date between d.VALID_DT_FR and d.VALID_DT_TO
    ), PACK AS (SELECT
        v.RECORD_ID
        , i.PACKING_TYPE_CD
        , SEQ = ROW_NUMBER() OVER (
            PARTITION BY V.RECORD_ID
            ORDER BY I.VALID_DT_TO DESC, I.VALID_DT_FR DESC)
    from v
    join dock d on d.RECORD_ID = v.RECORD_ID and d.seq = 1
    join TB_M_PACKING_INDICATOR i
      on i.dock_cd = d.DOCK_CD
     and i.MAT_NO = v.MAT_NO
     and i.PROD_PURPOSE_CD = v.PROD_PURPOSE_CD
     and i.SOURCE_TYPE = v.SOURCE_TYPE
     and i.PART_COLOR_SFX = v.PART_COLOR_SFX
     and @v_prod_date between i.VALID_DT_FR and i.VALID_DT_TO
    ), MATPRICE AS (SELECT
         V.RECORD_ID
        , mp.PRICE_AMT
        , mp.CURR_CD
        , SEQ = ROW_NUMBER() over(
             partition by v.record_id
             order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.part_color_sfx = coalesce(v.part_color_sfx, @v_color_sfx)
     and mp.packing_type = coalesce(p.PACKING_TYPE_CD,'0')
     and mp.supp_cd = v.supp_cd
     and mp.APPROVE_STS ='Y'
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ), MATPRICEL AS (SELECT
        V.RECORD_ID
       , SEQ = ROW_NUMBER() over(
            partition by v.record_id
            order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
       , mp.PRICE_AMT, mp.CURR_CD
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.PART_COLOR_SFX = @l_color_sfx
     and mp.packing_type = coalesce(p.PACKING_TYPE_CD, '0')
     and mp.supp_cd = v.supp_cd
     AND mp.APPROVE_STS ='Y'
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ),MATPRICEV AS (SELECT
        V.RECORD_ID
       , SEQ = ROW_NUMBER() over(
            partition by v.record_id
            order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
       , mp.PRICE_AMT, mp.CURR_CD
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.PART_COLOR_SFX = @v_color_sfx
     and mp.packing_type = COALESCE(p.PACKING_TYPE_CD,'0')
     and mp.supp_cd = v.supp_cd
     and mp.approve_sts = 'Y'
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ), LO AS (select
        V.RECORD_ID
        , [MARK] =
          CASE WHEN v.SUPP_CD IS NULL THEN @MARK_SUPP_CD ELSE 0 END
        | CASE WHEN d.dock_cd is null then @MARK_DOCK ELSE 0 END
        | CASE WHEN mm.mat_no is null then @MARK_MATERIAL else 0 end
        | case when mv.id is null then @MARK_VALUATION else 0 end
        | CASE WHEN sd.id IS NULL THEN @MARK_SLOC_MAT ELSE 0 END
        -- packing_type_cd not mandatory for po_process        
        | CASE WHEN coalesce(mp.seq, mpv.seq, ml.seq) is null then @MARK_MATERIAL_PRICE else 0 end
        | case when cp.CALCULATION_SCHEMA_CD is null then @MARK_CALC_SCHEMA else 0 end
        | case when a.doc_type is null then @MARK_DOC_TYPE ELSE 0 END
        , d.DOCK_CD
        , v.PLANT_CD
        , v.SLOC_CD
        , V.SUPP_CD
        , V.MAT_NO
        , V.PROD_PURPOSE_CD
        , V.SOURCE_TYPE
        , mm.MAT_DESC
        , V.PART_COLOR_SFX
        , PACKING_TYPE_CD = COALESCE(p.PACKING_TYPE_CD, '0')
        , V.QUANTITY
        , COALESCE(mp.curr_cd, MPV.CURR_cD, ML.CURR_CD) CURR_CD
        , COALESCE(MP.PRICE_AMT, MPV.PRICE_AMT, ML.PRICE_AMT) PRICE_AMT
        , V.OTHER_PROCESS_ID
        , V.PROD_MONTH
        , S.SUPP_NAME
        , S.PAYMENT_METHOD_CD
        , S.PAYMENT_TERM_CD
        , S.CALCULATION_SCHEMA_CD
        , V.PR_NO
        , V.PR_ITEM_NO
        , V.PROCESS_STATUS
        , V.PO_NO
        , V.PO_ITEM_NO
        , V.DOC_TYPE
    from v
    left join @llowed_doc_type a
           on a.doc_type = v.doc_type
    LEFT JOIN DOCK D
           ON D.RECORD_ID = V.RECORD_ID
          AND D.SEQ = 1
    LEFT JOIN s
           on s.RECORD_ID = v.RECORD_ID
          and s.SUPP_CD= v.SUPP_CD AND S.SEQ = 1
    LEFT JOIN #tb_t_mat mm
           on mm.mat_no = v.MAT_NO
          and mm.e =1
    left join #tb_t_val mv
           on mv.MAT_NO = v.mat_no
          and mv.PROD_PURPOSE_CD =  v.PROD_PURPOSE_CD
          and mv.PLANT_CD = v.PLANT_CD
          AND MV.SOURCE_TYPE = V.SOURCE_TYPE
          AND MV.E =1
    LEFT JOIN #tb_t_sloc sd
           on sd.MAT_NO = v.mat_no
          and sd.PROD_PURPOSE_CD = v.PROD_PURPOSE_CD
          AND SD.SOURCE_TYPE = v.SOURCE_TYPE
          and sd.PLANT_CD = v.PLANT_CD
          and sd.SLOC_CD =  v.SLOC_CD
          and sd.e = 1
    left join pack p
           on p.RECORD_ID = v.RECORD_ID
          and p.seq = 1
    left join matprice mp
           on v.RECORD_ID =  mp.RECORD_ID
          and mp.seq = 1
    LEFT JOIN MATPRICEV mpv
           on v.RECORD_ID =  mpv.RECORD_ID
          AND MPV.SEQ = 1
    LEFT JOIN MATPRiCEL ml
           ON v.RECORD_ID = ml.RECORD_ID
          and ml.SEQ = 1
    left join cp
           on cp.RECORD_ID = v.RECORD_ID
          and cp.seq = 1
    )
    SELECT
        lo.RECORD_ID, MARK, r.err
        , DOC_TYPE, SUPP_CD, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PART_COLOR_SFX, PACKING_TYPE_CD, DOCK_CD, PLANT_CD, SLOC_CD
        , PAYMENT_METHOD_CD, PAYMENT_TERM_CD, CALCULATION_SCHEMA_CD
        , QUANTITY
        , CURR_CD, PRICE_AMT
        , PR_NO
        , PR_ITEM_NO
        , PO_NO
        , PO_ITEM_NO
        , SUPP_NAME, MAT_DESC
        , OTHER_PROCESS_ID
        , PROD_MONTH
    FROM LO
    left join (
        select
            la.record_id
          , err = stuff(
            (select ','+ m.nm
            from lo l
            join @marks m on m.mark & l.mark > 0
            where l.record_id = la.record_id
            for xml path(''))
            , 1 ,1, '' )
        from lo la
        group by record_id
    ) r on r.RECORD_ID = lo.RECORD_ID
    ORDER BY LO.RECORD_ID
end;

--* FID.Ridwan: 20210309 -> PO Manual SPRINTS
IF @trace & @trace_po_sprints> 0
begin
    with v as (select
        RECORD_ID= ROW_NUMBER() OVER(
            ORDER BY DOC_TYPE,SUPP_CD, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PART_COLOR_SFX)
        , SEQ_NO
        , DOC_TYPE
        , SUPP_CD
        , DOCK_CODE
        , MAT_NO
        , PROD_PURPOSE_CD
        , SOURCE_TYPE
        , PLANT_CD
        , SLOC_CD
        , PACKING_TYPE_cD
        , PROD_MONTH
        , QUANTITY
        , PART_COLOR_SFX
        , UNLIMITED_FLAG
        , TOLERANCE_PERCENTAGE
        , PR_NO
        , PR_ITEM_NO
        , PROCESS_STATUS
        , PO_NO
        , PO_ITEM_NO
        , OTHER_PROCESS_ID
        , PROCESS_ID
        , SYSTEM_SOURCE
		, (CASE WHEN FLAG_PROCESS = 'Y' THEN SPRINTS_MAT_PRICE ELSE NULL END) MAT_PRICE
		, (CASE WHEN FLAG_PROCESS = 'Y' THEN SPRINTS_CURR_CD ELSE NULL END) CURR_CD
    from dbo.TB_T_PO_SPRINTS with (nolock)
    where (NULLIF(@prod_month,'') is null or PROD_MONTH = @prod_month)
    AND (NULLIF(@MAT_NO, '') IS NULL OR @MAT_NO = MAT_NO )
    ) , S AS (SELECT
        V.RECORD_ID, v.SUPP_CD, SI.SUPP_NAME
        , SI.PAYMENT_METHOD_CD
        , SI.PAYMENT_TERM_CD
        , SCH.CALCULATION_SCHEMA_CD
        , [MARK]
        = CASE WHEN si.SUPP_NAME is null then @MARK_SUPP_CD else 0 end
        | case when sch.CALCULATION_SCHEMA_CD is null then @MARK_SUPP_SCHEMA  else 0 end
        , SEQ = ROW_NUMBER() OVER(
            PARTITION BY V.RECORD_ID
            ORDER BY sch.VALID_DT_TO DESC, sch.valid_dt_to DESC, SI.SUPP_NAME)
    FROM V
    LEFT JOIN TB_M_SUPPLIER_ICS si on si.SUPP_CD = V.SUPP_CD
    LEFT JOIN TB_M_SUPPLIER_SCHEMA sch on sch.SUPP_CD = V.SUPP_CD AND @v_prod_date between VALID_DT_FR AND VALID_DT_TO
    ), cp as (select
        s.record_id
        , cp.calculation_schema_cd
        , seq = row_number() over(
            partition by s.record_id
            order by cp.seq_no)
    from s
    join TB_M_COMP_PRICE cp on cp.CALCULATION_SCHEMA_CD = s.CALCULATION_SCHEMA_CD
    ) , dock as (select
        V.RECORD_ID
        , D.DOCK_CD
        , SEQ = Row_number() over(
            partition by v.record_id
            order by d.valid_dt_to desc, d.valid_dt_fr desc)
    from v
    JOIN TB_M_DOCK_CD D
       on d.sloc_CD = v.SLOC_CD and d.PLANT_CD = v.PLANT_CD
      and @v_prod_date between d.VALID_DT_FR and d.VALID_DT_TO
    ), PACK AS (SELECT
        v.RECORD_ID
        , i.PACKING_TYPE_CD
        , SEQ = ROW_NUMBER() OVER (
            PARTITION BY V.RECORD_ID
            ORDER BY I.VALID_DT_TO DESC, I.VALID_DT_FR DESC)
    from v
    join dock d on d.RECORD_ID = v.RECORD_ID and d.seq = 1
    join TB_M_PACKING_INDICATOR i
      on i.dock_cd = d.DOCK_CD
     and i.MAT_NO = v.MAT_NO
     and i.PROD_PURPOSE_CD = v.PROD_PURPOSE_CD
     and i.SOURCE_TYPE = v.SOURCE_TYPE
     and i.PART_COLOR_SFX = v.PART_COLOR_SFX
     and @v_prod_date between i.VALID_DT_FR and i.VALID_DT_TO
    ), MATPRICE AS (SELECT
         V.RECORD_ID
        , mp.PRICE_AMT
        , mp.CURR_CD
        , SEQ = ROW_NUMBER() over(
             partition by v.record_id
             order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.part_color_sfx = coalesce(v.part_color_sfx, @v_color_sfx)
     and mp.packing_type = coalesce(p.PACKING_TYPE_CD,'0')
     and mp.supp_cd = v.supp_cd
     and mp.APPROVE_STS ='Y'
	 and v.MAT_PRICE is null
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ), MATPRICEL AS (SELECT
        V.RECORD_ID
       , SEQ = ROW_NUMBER() over(
            partition by v.record_id
            order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
       , mp.PRICE_AMT, mp.CURR_CD
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.PART_COLOR_SFX = @l_color_sfx
     and mp.packing_type = coalesce(p.PACKING_TYPE_CD, '0')
     and mp.supp_cd = v.supp_cd
     AND mp.APPROVE_STS ='Y'
	 and v.MAT_PRICE is null
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ),MATPRICEV AS (SELECT
        V.RECORD_ID
       , SEQ = ROW_NUMBER() over(
            partition by v.record_id
            order by mp.valid_dt_to desc, mp.valid_dt_fr desc)
       , mp.PRICE_AMT, mp.CURR_CD
    FROM V
    left JOIN PACK P ON P.RECORD_ID  = V.RECORD_ID and p.seq = 1
    join TB_M_MATERIAL_PRICE mp
      on mp.mat_no = v.mat_no
     and mp.source_type = v.source_type
     and mp.prod_purpose_cd = v.prod_purpose_cd
     and mp.PART_COLOR_SFX = @v_color_sfx
     and mp.packing_type = COALESCE(p.PACKING_TYPE_CD,'0')
     and mp.supp_cd = v.supp_cd
     and mp.approve_sts = 'Y'
	 and v.MAT_PRICE is null
	 and @v_prod_date between mp.VALID_DT_FR and mp.VALID_DT_TO
    ), LO AS (select
        V.RECORD_ID
        , [MARK] =
          CASE WHEN v.SUPP_CD IS NULL THEN @MARK_SUPP_CD ELSE 0 END
        | CASE WHEN d.dock_cd is null then @MARK_DOCK ELSE 0 END
        | CASE WHEN mm.mat_no is null then @MARK_MATERIAL else 0 end
        | case when mv.id is null then @MARK_VALUATION else 0 end
        | CASE WHEN sd.id IS NULL THEN @MARK_SLOC_MAT ELSE 0 END
        -- packing_type_cd not mandatory for po_process        
        | CASE WHEN coalesce(mp.seq, mpv.seq, ml.seq) is null then @MARK_MATERIAL_PRICE else 0 end
        | case when cp.CALCULATION_SCHEMA_CD is null then @MARK_CALC_SCHEMA else 0 end
        | case when a.doc_type is null then @MARK_DOC_TYPE ELSE 0 END
        , d.DOCK_CD
        , v.PLANT_CD
        , v.SLOC_CD
        , V.SUPP_CD
        , V.MAT_NO
        , V.PROD_PURPOSE_CD
        , V.SOURCE_TYPE
        , mm.MAT_DESC
        , V.PART_COLOR_SFX
        , PACKING_TYPE_CD = COALESCE(p.PACKING_TYPE_CD, '0')
        , V.QUANTITY
        , COALESCE(mp.curr_cd, MPV.CURR_cD, ML.CURR_CD) CURR_CD
        , COALESCE(MP.PRICE_AMT, MPV.PRICE_AMT, ML.PRICE_AMT) PRICE_AMT
        , V.OTHER_PROCESS_ID
        , V.PROD_MONTH
        , S.SUPP_NAME
        , S.PAYMENT_METHOD_CD
        , S.PAYMENT_TERM_CD
        , S.CALCULATION_SCHEMA_CD
        , V.PR_NO
        , V.PR_ITEM_NO
        , V.PROCESS_STATUS
        , V.PO_NO
        , V.PO_ITEM_NO
        , V.DOC_TYPE
    from v
    left join @llowed_doc_type_spr a
           on a.doc_type = v.doc_type
    LEFT JOIN DOCK D
           ON D.RECORD_ID = V.RECORD_ID
          AND D.SEQ = 1
    LEFT JOIN s
           on s.RECORD_ID = v.RECORD_ID
          and s.SUPP_CD= v.SUPP_CD AND S.SEQ = 1
    LEFT JOIN #tb_t_mat mm
           on mm.mat_no = v.MAT_NO
          and mm.e =1
    left join #tb_t_val mv
           on mv.MAT_NO = v.mat_no
          and mv.PROD_PURPOSE_CD =  v.PROD_PURPOSE_CD
          and mv.PLANT_CD = v.PLANT_CD
          AND MV.SOURCE_TYPE = V.SOURCE_TYPE
          AND MV.E =1
    LEFT JOIN #tb_t_sloc sd
           on sd.MAT_NO = v.mat_no
          and sd.PROD_PURPOSE_CD = v.PROD_PURPOSE_CD
          AND SD.SOURCE_TYPE = v.SOURCE_TYPE
          and sd.PLANT_CD = v.PLANT_CD
          and sd.SLOC_CD =  v.SLOC_CD
          and sd.e = 1
    left join pack p
           on p.RECORD_ID = v.RECORD_ID
          and p.seq = 1
    left join matprice mp
           on v.RECORD_ID =  mp.RECORD_ID
          and mp.seq = 1
    LEFT JOIN MATPRICEV mpv
           on v.RECORD_ID =  mpv.RECORD_ID
          AND MPV.SEQ = 1
    LEFT JOIN MATPRiCEL ml
           ON v.RECORD_ID = ml.RECORD_ID
          and ml.SEQ = 1
    left join cp
           on cp.RECORD_ID = v.RECORD_ID
          and cp.seq = 1
    )
    SELECT
        lo.RECORD_ID, MARK, r.err
        , DOC_TYPE, SUPP_CD, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, PART_COLOR_SFX, PACKING_TYPE_CD, DOCK_CD, PLANT_CD, SLOC_CD
        , PAYMENT_METHOD_CD, PAYMENT_TERM_CD, CALCULATION_SCHEMA_CD
        , QUANTITY
        , CURR_CD, PRICE_AMT
        , PR_NO
        , PR_ITEM_NO
        , PO_NO
        , PO_ITEM_NO
        , SUPP_NAME, MAT_DESC
        , OTHER_PROCESS_ID
        , PROD_MONTH
    FROM LO
    left join (
        select
            la.record_id
          , err = stuff(
            (select ','+ m.nm
            from lo l
            join @marks m on m.mark & l.mark > 0
            where l.record_id = la.record_id
            for xml path(''))
            , 1 ,1, '' )
        from lo la
        group by record_id
    ) r on r.RECORD_ID = lo.RECORD_ID
    ORDER BY LO.RECORD_ID
end;

if @trace & @trace_local_val > 0
begin
    select
        h.RECORD_ID
        , h.VAL_STS
        , h.MARK
        , r.err
        , h.DOC_TYPE
        , h.DOCK_CD
        , h.SUPP_CD
        , h.MAT_NO
        , h.PROD_PURPOSE_CD
        , h.SOURCE_TYPE
        , h.PART_COLOR_SFX
        , h.CALCULATION_SCHEMA_CD
        , h.PAYMENT_METHOD_CD
        , h.PAYMENT_TERM_CD
        , h.PACKING_TYPE
        , h.QTY_N
        , h.QTY_N1
        , h.QTY_N2
        , h.QTY_N3
        , h.PLANT_CD
        , h.SLOC_CD
        , h.SPECIAL_PROCUREMENT_TYPE_CD
        , h.PO_CURR
        , h.MAT_PRICE
        , h.PO_NO
        , h.PO_ITEM_NO
        , h.PR_NO
        , h.PR_ITEM_NO
        , h.MAT_DESC
        , h.UNIT_OF_MEASURE_CD
        , h.PROD_MONTH
    from dbo.TB_T_LOCAL_VAL_H h with (nolock)
    left join (
        select
            la.record_id
          , err = stuff(
            (select ','+ m.nm
            from tb_t_local_val_h l
            join @marks m on m.mark & l.mark > 0
            where l.record_id = la.record_id
            for xml path(''))
            , 1 ,1, '' )
        from tb_t_local_val_h  la
        where (nullif(@MAT_NO,'') IS NULL or MAT_NO = @mat_no)
        group by record_id
    ) r on r.RECORD_ID = h.RECORD_ID
    where (nullif(@MAT_NO,'') IS NULL or MAT_NO = @mat_no)
    ORDER BY RECORD_ID;
end;


if @trace & @trace_mat_price > 0
begin
    if @trace & @trace_local_val > 0
    begin
        with p as (select
          prod_month = @prod_month
        , prod_date= @v_prod_date
        , v_color_sfx = coalesce((select top 1 v_part_color_sfx = convert(varchar(2), system_value)
            from tb_m_system where function_id = '300' and system_cd = 'PART_COLOR_SFX_PRICE'), '')
        , l_color_sfx = COALESCE((
        SELECT TOP 1 l_color_sfx = CONVERT(VARCHAR(2), LTRIM(RTRIM(SYSTEM_VALUE)))
        FROM TB_M_SYSTEM WHERE FUNCTION_ID = '300' AND SYSTEM_CD = 'PART_COLOR_SFX_LAST'), 'X')
        )
         select h.record_id
            , ERR=
              case when h.supp_cd            != m.supp_cd         then ' supp_cd' ELSE '' END
            + case when h.SOURCE_TYPE        != m.SOURCE_TYPE     then ' SOURCE_TYPE' ELSE '' END
            + case when h.PROD_PURPOSE_CD    != m.prod_purpose_cd then ' prod_purpose_cd' ELSE '' END
            + case when h.part_color_sfx     != m.part_color_sfx
                    and m.part_Color_sfx     != p.v_color_sfx
                    and m.PART_COLOR_SFX     != p.l_color_sfx     then ' part_color_sfx' ELSE '' END
            + case when h.packing_type       != m.packing_type    then ' packing_type' ELSE '' END
            + case when NOT (p.prod_date between m.VALID_DT_FR and m.VALID_DT_TO) then ' prod_date' ELSE '' END
            + CASE WHEN M.APPROVE_STS        != 'Y'               THEN ' approve_sts' else '' end
            , h.supp_cd
            , m.supp_cd m_supp_cd
            , h.mat_no
            , m.mat_no m_mat_no
            , h.source_type
            , m.source_type m_source_type
            , h.prod_purpose_cd
            , m.prod_purpose_cd m_prod_purpose_cd
            , h.part_color_sfx
            , m.part_color_sfx m_part_color_sfx
            , p.v_color_sfx
            , p.l_color_sfx
            , h.packing_type
            , m.packing_type m_packing_type
            , h.MAT_PRICE
            , m.price_amt
            , h.PO_CURR
            , m.curr_cd
            , p.prod_date
            , row_number() over(partition by h.record_id order by m.valid_dt_to desc, m.valid_dt_fr desc) seq
            , m.VALID_DT_FR
            , m.valid_dt_to
            , m.approve_sts
         from  TB_T_LOCAL_VAL_H h
         join p on 1=1
         left join TB_M_MATERIAL_PRICE m
         on m.mat_no = h.mat_no
         -- and m.supp_cd = h.supp_cd
         --and m.source_type = h.source_type
         --and m.prod_purpose_cd = h.prod_purpose_cd
         --and m.part_color_sfx = h.part_color_sfx
         --and m.packing_type = h.packing_type
         --and p.prod_date between m.valid_dt_fr and m.valid_dt_to
         --and m.approve_sts = 'Y'
         order by h.RECORD_ID, M.VALID_DT_TO desc, m.VALID_DT_FR desc
    end;

    if @trace & @trace_local_order > 0
    begin
        with p as (select
          prod_month = @prod_month
        , prod_date= @v_prod_date
        , v_color_sfx = coalesce((select top 1 v_part_color_sfx = convert(varchar(2), system_value)
            from tb_m_system where function_id = '300' and system_cd = 'PART_COLOR_SFX_PRICE'), '')
        , l_color_sfx = COALESCE((
        SELECT TOP 1 l_color_sfx = CONVERT(VARCHAR(2), LTRIM(RTRIM(SYSTEM_VALUE)))
        FROM TB_M_SYSTEM WHERE FUNCTION_ID = '300' AND SYSTEM_CD = 'PART_COLOR_SFX_LAST'), 'X')
        )
         select
              ERR=
              case when h.supp_cd            != m.supp_cd         then ' supp_cd' ELSE '' END
            + case when h.SOURCE_TYPE        != m.SOURCE_TYPE     then ' SOURCE_TYPE' ELSE '' END
            + case when h.PROD_PURPOSE_CD    != m.prod_purpose_cd then ' prod_purpose_cd' ELSE '' END
            + case when h.part_color_sfx     != m.part_color_sfx
                    and m.part_Color_sfx     != p.v_color_sfx
                    and m.PART_COLOR_SFX     != p.l_color_sfx     then ' part_color_sfx' ELSE '' END
            + case when i.PACKING_TYPE_CD    != m.packing_type    then ' packing_type' ELSE '' END
            + case when NOT (p.prod_date between m.VALID_DT_FR and m.VALID_DT_TO) then ' prod_date' ELSE '' END
            + CASE WHEN M.APPROVE_STS        != 'Y'               THEN ' approve_sts' else '' end
            , h.supp_cd
            , m.supp_cd m_supp_cd
            , h.mat_no
            , m.mat_no m_mat_no
            , h.source_type
            , m.source_type m_source_type
            , h.prod_purpose_cd
            , m.prod_purpose_cd m_prod_purpose_cd
            , h.part_color_sfx
            , m.part_color_sfx m_part_color_sfx
            , p.v_color_sfx
            , p.l_color_sfx
            , i.packing_type_cd
            , m.packing_type m_packing_type
            , m.price_amt
            , m.curr_cd
            , p.prod_date
            , h.OTHER_PROCESS_ID
            , row_number() over(partition by h.supp_cd, h.mat_no, h.source_type, h.prod_purpose_cd, h.part_color_sfx, i.packing_type_cd
                order by m.valid_dt_to desc, m.valid_dt_fr desc) seq
            , m.VALID_DT_FR
            , m.valid_dt_to
            , m.approve_sts
         from  TB_T_LOCAL_ORDER h with(nolock)
         join p on 1=1
         left join TB_M_PACKING_INDICATOR i
              on i.dock_cd = h.DOCK_CD
             and i.MAT_NO = h.MAT_NO
             and i.PROD_PURPOSE_CD = h.PROD_PURPOSE_CD
             and i.SOURCE_TYPE = h.SOURCE_TYPE
             and i.PART_COLOR_SFX = h.PART_COLOR_SFX
             and @v_prod_date between i.VALID_DT_FR and i.VALID_DT_TO
         left join TB_M_MATERIAL_PRICE m
         on m.mat_no = h.mat_no
         and m.supp_cd = h.supp_cd
         -- and m.source_type = h.source_type
         -- and m.prod_purpose_cd = h.prod_purpose_cd
         -- and m.part_color_sfx = h.part_color_sfx
         -- and m.packing_type = i.PACKING_TYPE_CD
         -- and p.prod_date between m.valid_dt_fr and m.valid_dt_to
         -- and m.approve_sts = 'Y'
         where (nullif(@pid, 0) is null or @pid= h.OTHER_PROCESS_ID)
           AND (NULLIF(@prod_month, '') IS NULL OR h.prod_month = @prod_month)
           AND (NULLIF(@MAT_NO, '') IS NULL OR @MAT_NO = h.MAT_NO )
         order by h.supp_cd, h.mat_no, h.source_type, h.prod_purpose_cd, h.part_color_sfx, i.packing_type_cd, M.VALID_DT_TO desc, m.VALID_DT_FR desc
    end;
end;

if @trace & @trace_tmp > 0
begin
    IF (select object_id('tempdb..#TB_T_PO_H')) IS NOT NULL
    begin
         SELECT
          RECORD_ID
        , PO_NO
        , SUPP_CD
        , DOC_DT
        , PO_NOTE
        , INV_WO_GR_FLAG
        , DELETION_FLAG
        , CALCULATION_SCHEMA_CD
        , DOC_TYPE
        , PROD_MONTH
        , PO_EXCHANGE_RATE
        , PO_CURR
        , PAYMENT_METHOD_CD
        , PAYMENT_TERM_CD
        , REFF_INV_NO
        , REFF_INV_DT
        , REFF_DOC
        , REFF_DOC_DT
        , ID
        FROM #TB_T_PO_H
    end

    IF (select object_id('tempdb..#TB_T_PO_I')) IS NOT NULL
    begin
        SELECT
          RECORD_ID
        , PO_NO
        , PO_ITEM_NO
        , PR_NO
        , PR_ITEM
        , MAT_NO
        , SOURCE_TYPE
        , PLANT_CD
        , PROD_PURPOSE_CD
        , SLOC_CD
        , PART_COLOR_SFX
        , UNLIMITED_FLAG
        , TOLERANCE_PERCENTAGE
        , PO_GR_QUANTITY
        , CONTAINER_NO
        , NOTE
        , SPECIAL_PROCUREMENT_TYPE_CD
        , MAT_DESC
        , PO_QUANTITY_ORIGINAL
        , PO_QUANTITY_NEW
        , PO_QUANTITY_OPEN
        , UNIT_OF_MEASURE_CD
        , PO_MAT_PRICE
        , DELIVERY_DT
        , DELETION_FLAG
        , TAX_CD
        , PACKING_TYPE
        , SUBCONTRACT_FLAG
        , ID
        FROM #TB_T_PO_I
    END

    IF (select object_id('tempdb..#TB_T_PO_D')) IS NOT NULL
    begin
        select
          RECORD_ID
        , PO_NO
        , PO_ITEM_NO
        , COMP_PRICE_CD
        , COMP_PRICE_RATE
        , INVOICE_FLAG
        , PO_EXCHANGE_RATE
        , SEQ_NO
        , BASE_VALUE_F
        , BASE_VALUE_T
        , CALCULATION_TYPE
        , PLUS_MINUS_FLAG
        , SUPP_CD
        , PO_CURR
        , PO_DETAIL_PRICE
        , PO_DETAIL_AMT
        , INVENTORY_FLAG
        , ACCRUAL_POSTING_FLAG
        , CONDITION_CATEGORY
        , ID
        FROM #TB_T_PO_D
    END

end;

if @trace & @trace_tb_r > 0
begin
   select
       h.PO_NO
     , h.SUPPLIER_CD
     , h.PRODUCTION_MONTH
     , h.PO_DATE
     , h.PO_TYPE
     , h.SUPPLIER_PLANT
     , h.CURRENCY_CD
     , h.EXCHANGE_RATE
     , h.PO_CURR
     , h.TOTAL_AMOUNT
     , h.STATUS_CD
     , h.REQUIRED_DT
     , h.DELETION_FLAG
     , h.REFF_INV_NO
     , h.REFF_INV_DT
     , h.LOCK_STATUS
     , h.CONFIRMATION_FLAG
     , h.DOWNLOADED_BY
     , h.DOWNLOADED_DT
     , h.RELEASE_DT
     , h.CREATED_BY
     , h.CREATED_DT
     , h.CHANGED_BY
     , h.CHANGED_DT
     , h.calculation_schema_cd
     , h.INV_WO_GR_FLAG
     , h.PAYMENT_METHOD_CD
     , h.PAYMENT_TERM_CD
     , h.PO_NOTE
     , h.REFF_DOC
     , h.REFF_DOC_DT
     , h.DOC_TYPE
  from TB_R_PO_H H with (nolock)
  JOIN TB_T_LOCAL_VAL_H V on v.PO_NO = h.PO_NO
  WHERE v.VAL_STS = 'OK' ;

    select
       i.PO_NO
     , i.PO_ITEM_NO
     , i.PR_NO
     , i.PR_ITEM
     , i.MAT_NO
     , i.SOURCE_TYPE
     , i.PLANT_CD
     , i.PROD_PURPOSE_CD
     , i.SLOC_CD
     , i.PART_COLOR_SFX
     , i.UNLIMITED_FLAG
     , i.TOLERANCE_PERCENTAGE
     , i.PO_GR_QUANTITY
     , i.CONTAINER_NO
     , i.NOTE
     , i.SPECIAL_PROCUREMENT_TYPE_CD
     , i.MAT_DESC
     , i.PO_QUANTITY_ORIGINAL
     , i.PO_QUANTITY_NEW
     , i.PO_QUANTITY_OPEN
     , i.UNIT_OF_MEASURE_CD
     , i.PO_MAT_PRICE
     , i.DELIVERY_DT
     , i.DELETION_FLAG
     , i.CREATED_BY
     , i.CREATED_DT
     , i.CHANGED_BY
     , i.CHANGED_DT
     , i.TAX_CD
     , i.PACKING_TYPE
     , i.SUBCONTRACT_FLAG
    from TB_R_PO_ITEM i with (nolock)
    JOIN TB_T_LOCAL_VAL_H  h on h.PO_NO = i.PO_NO AND h.PO_ITEM_NO = i.PO_ITEM_NO
    where h.VAL_STS ='OK'

    select
       D.PO_NO
     , D.PO_ITEM
     , D.PR_NO
     , D.PR_ITEM
     , D.MAT_NO
     , D.MAT_DESC
     , D.UOM
     , D.SOURCE_TYPE
     , D.PROD_PURPOSE_CD
     , D.PLANT_CD
     , D.SLOC_CD
     , D.PART_COLOR_SUFFIX
     , D.PACKING_TYPE
     , D.PO_QUANTITY_NEW
     , D.PO_QTY_ORIGINAL
     , D.PO_MAT_PRICE
     , D.REQUIRED_DT
     , D.CURRENCY_CD
     , D.AMOUNT
     , D.DELIVERY_NAME
     , D.DELIVERY_ADDRESS
     , D.DELIVERY_POSTAL_CD
     , D.DELIVERY_CITY
     , D.NOTE
     , D.CREATED_BY
     , D.CREATED_DT
     , D.CHANGED_BY
     , D.CHANGED_DT
     , D.COMP_PRICE_CD
     , D.COMP_PRICE_RATE
     , D.INVOICE_FLAG
     , D.PO_EXCHANGE_RATE
     , D.SEQ_NO
     , D.BASE_VALUE_F
     , D.BASE_VALUE_T
     , D.CALCULATION_TYPE
     , D.PLUS_MINUS_FLAG
     , D.SUPP_CD
     , D.INVENTORY_FLAG
     , D.ACCRUAL_POSTING_FLAG
     , D.CONDITION_CATEGORY
    from  TB_R_PO_D D with (nolock)
    join tb_t_local_val_h h on h.po_no = d.po_no and h.po_item_no = d.po_item
    where h.val_sts='OK'

end;


if @trace & @trace_summary > 0
begin
    SELECT COALESCE(A.SUPP_CD, B.SUPP_CD) SUPP_CD, PO_COUNT, ITEM_COUNT, OK_COUNT
    FROM (
    SELECT supp_cd, count(distinct po_no) po_count, count(distinct PO_ITEM_NO) item_count
    FROM TB_T_LOCAL_VAL_H
    where VAL_STS ='OK'
    AND PO_NO IS NOT NULL AND PO_ITEM_NO IS NOT NULL
    group by supp_cd) A
    FULL OUTER JOIN (
    select supp_cd, count(1) OK_COUNT
    from tb_t_local_val_h
    where val_sts ='OK'
    GROUP BY SUPP_CD
    ) B ON A.SUPP_CD = B.SUPP_CD
    ORDER BY OK_COUNT DESC, ITEM_COUNT DESC
end;

if @trace & @trace_log > 0
begin
    exec showlog @pid
end

/*

exec sp_1_3_1_trace @traces='data_val local_order', @prod_month = '202012', @pid=202012310001, @username=''
exec sp_1_3_1_trace @traces='data_val po_process', @prod_month = '202012', @pid=202012310001, @username=''

*/
end;