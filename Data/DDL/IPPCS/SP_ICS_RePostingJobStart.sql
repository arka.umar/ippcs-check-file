CREATE PROCEDURE [dbo].[SP_ICS_RePostingJobStart]
AS
BEGIN
DECLARE @@SUPP_INV_NO VARCHAR(12) = '',
		@@SUPP_CD VARCHAR(6) = '',
		@@USERID VARCHAR(20) = 'RePostingAgent',
		@@PROCESS_ID BIGINT = 0,
		@@OLD_PROCESS_ID BIGINT = 0,
		@@LOCATION VARCHAR(100) = 'RePosting.Agent',
		@@MODULE VARCHAR(6) = '5',
		@@FUNCTION VARCHAR(6) = '53002',
		@@MESSAGE VARCHAR(MAX) = '',
		@@SQL NVARCHAR(MAX) = '',
		@@ICS_PROCCESS_ID BIGINT = 0,
		@@ICS_ERR_COUNT INT = 0

DECLARE @@TB_PROCESS_ID TABLE (OTHER_PROCESS_ID BIGINT, PROCESS_ID BIGINT)
DECLARE @@TB_INV_NO TABLE (SUPP_INV_NO VARCHAR(100), SUPP_CD VARCHAR(10))
DECLARE @@LOG_ICS TABLE (ERR INT)

	--EXEC sp_PutLog 
	--		'RePosting Invoice Agent for Error Posting Invoice Started', 
	--		@@USERID, 
	--		@@LOCATION, 
	--		@@PROCESS_ID OUTPUT, 
	--		'INF', 
	--		'INF', 
	--		@@MODULE, 
	--		@@FUNCTION, 
	--		0 --STATUS

	--EXEC sp_PutLog 
	--		'Get Oldest Supplier Invoice No and Supplier Code with Error Posting Status', 
	--		@@USERID, 
	--		@@LOCATION, 
	--		@@PROCESS_ID, 
	--		'INF', 
	--		'INF', 
	--		@@MODULE, 
	--		@@FUNCTION, 
	--		1 --STATUS

	INSERT INTO @@TB_INV_NO
	SELECT
		SUPP_INV_NO,
		SUPP_CD
	FROM (
		SELECT DISTINCT 
			SUPP_INV_NO, 
			SUPP_CD, 
			CONVERT(DATE, CREATED_DT) as [DATE] 
		FROM TB_R_INV_UPLOAD 
		WHERE STATUS_CD = -5 AND SUPP_INV_NO = '1709AR006316'
	) INV 
	ORDER BY [DATE] ASC

	DECLARE cur_inv CURSOR FOR
		SELECT SUPP_INV_NO, SUPP_CD FROM @@TB_INV_NO
	OPEN cur_inv
	FETCH NEXT FROM cur_inv INTO @@SUPP_INV_NO, @@SUPP_CD
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM @@TB_PROCESS_ID
		SET @@PROCESS_ID = NULL
		SET @@OLD_PROCESS_ID = NULL
		SET @@ICS_PROCCESS_ID = NULL

		SET @@MESSAGE = 'Supplier Invoice No ' + ISNULL(@@SUPP_INV_NO, '') + ' and Supplier Code ' + ISNULL(@@SUPP_CD, '') + ' Retrieved'
		EXEC sp_PutLog 
				@@MESSAGE, 
				@@USERID, 
				@@LOCATION, 
				@@PROCESS_ID OUTPUT, 
				'INF', 
				'INF', 
				@@MODULE, 
				@@FUNCTION, 
				1 --STATUS

		IF(NOT EXISTS(SELECT 'x' FROM TB_R_ICS_QUEUE WHERE REMARK = ISNULL(@@SUPP_INV_NO, '') + ';' + ISNULL(@@SUPP_CD, '') AND PROCESS_STATUS < 2))
		BEGIN
			SET @@MESSAGE = 'Checking Balance Status in ICS with Supplier Invoice No ' + ISNULL(@@SUPP_INV_NO, '') + ' and Supplier Code ' + ISNULL(@@SUPP_CD, '')
			EXEC sp_PutLog 
					@@MESSAGE, 
					@@USERID, 
					@@LOCATION, 
					@@PROCESS_ID, 
					'INF', 
					'INF', 
					@@MODULE, 
					@@FUNCTION, 
					1 --STATUS

			SET @@SQL = 'SELECT other_process_id, NULL FROM OPENQUERY([IPPCS_TO_ICS], 
						''select max(other_process_id) as other_process_id from tb_t_invoice_h where supp_invoice_no = ''''' + ISNULL(@@SUPP_INV_NO, '') + 
																''''' and supp_cd = ''''' + ISNULL(@@SUPP_CD, '') + 
																''''' and balance = 0 OR balance is null '')'

			BEGIN TRY

				INSERT INTO @@TB_PROCESS_ID (OTHER_PROCESS_ID, PROCESS_ID)
				EXEC sp_executesql @@SQL

				IF(EXISTS(SELECT OTHER_PROCESS_ID FROM @@TB_PROCESS_ID) AND (SELECT OTHER_PROCESS_ID FROM @@TB_PROCESS_ID) IS NOT NULL)
				BEGIN
					
					SELECT 
						@@OLD_PROCESS_ID = OTHER_PROCESS_ID
					FROM @@TB_PROCESS_ID
					WHERE ISNULL(OTHER_PROCESS_ID, '') <> ''

					SET @@SQL = 'SELECT NULL, process_id FROM OPENQUERY([IPPCS_TO_ICS], 
								''select NULL, process_id from tb_t_invoice_h where other_process_id = ''''' + ISNULL(CONVERT(VARCHAR(MAX), @@OLD_PROCESS_ID), '') + ''''''')'

					INSERT INTO @@TB_PROCESS_ID (OTHER_PROCESS_ID, PROCESS_ID)
					EXEC sp_executesql @@SQL

					SELECT 
						@@ICS_PROCCESS_ID = PROCESS_ID
					FROM @@TB_PROCESS_ID
					WHERE ISNULL(PROCESS_ID, '') <> ''

					SET @@MESSAGE = 'Balancing Status 0 with process id ' + ISNULL(CONVERT(VARCHAR(MAX), @@OLD_PROCESS_ID), '')
					EXEC sp_PutLog 
							@@MESSAGE, 
							@@USERID, 
							@@LOCATION, 
							@@PROCESS_ID, 
							'INF', 
							'INF', 
							@@MODULE, 
							@@FUNCTION, 
							1 --STATUS

					SET @@MESSAGE = 'IPPCS process id ' + ISNULL(CONVERT(VARCHAR(MAX), @@OLD_PROCESS_ID), '')
									 + ' and ICS Process ID ' + ISNULL(CONVERT(VARCHAR(MAX), @@ICS_PROCCESS_ID), '')
					EXEC sp_PutLog 
							@@MESSAGE, 
							@@USERID, 
							@@LOCATION, 
							@@PROCESS_ID, 
							'INF', 
							'INF', 
							@@MODULE, 
							@@FUNCTION, 
							1 --STATUS

					SET @@SQL = 'SELECT count_err FROM OPENQUERY([IPPCS_TO_ICS], 
							   ''select count(1) count_err from tb_r_log_d 
										where process_id = ''''' + ISNULL(CONVERT(VARCHAR(MAX), @@ICS_PROCCESS_ID), '') + 
											  ''''' and msg_id like ''''%ERR%'''''')'

					INSERT INTO @@LOG_ICS (ERR)
					EXEC sp_executesql @@SQL
					SELECT @@ICS_ERR_COUNT = ERR FROM @@LOG_ICS

					SET @@MESSAGE = 'Message Error in ICS.TB_R_LOG_D with ICS Process ID ' + ISNULL(CONVERT(VARCHAR(MAX), @@ICS_PROCCESS_ID), '') + 
									' is ' + ISNULL(CONVERT(VARCHAR(MAX), @@ICS_ERR_COUNT), '')
					EXEC sp_PutLog 
							@@MESSAGE, 
							@@USERID, 
							@@LOCATION, 
							@@PROCESS_ID, 
							'INF', 
							'INF', 
							@@MODULE, 
							@@FUNCTION, 
							1 --STATUS

					IF(@@ICS_ERR_COUNT <= 0)
					BEGIN
						EXEC sp_PutLog 
							'Start Process Re-Posting Invoice', 
							@@USERID, 
							@@LOCATION, 
							@@PROCESS_ID, 
							'INF', 
							'INF', 
							@@MODULE, 
							@@FUNCTION, 
							1 --STATUS

						INSERT INTO [dbo].[TB_R_ICS_QUEUE]
									([IPPCS_MODUL]
									,[IPPCS_FUNCTION]
									,[PROCESS_ID]
									,[PROCESS_STATUS]
									,[REMARK]
									,[CREATED_BY]
									,[CREATED_DT])
								VALUES
									('5'
									,'53002'
									,@@PROCESS_ID
									,0
									,@@SUPP_INV_NO + ';' + @@SUPP_CD
									,@@USERID
									,GETDATE());

						UPDATE TB_R_INV_UPLOAD 
						SET
							POSTING_BY = @@USERID,
							IN_PROGRESS = '1',
							CHANGED_BY = @@USERID,
							CHANGED_DT = GETDATE()
						WHERE SUPP_INV_NO = @@SUPP_INV_NO
							  AND SUPP_CD = @@SUPP_CD

						SET @@MESSAGE = 'Waiting for Invoice Posting Job In with Process ID ' + ISNULL(CONVERT(VARCHAR(MAX), @@PROCESS_ID), '')
						EXEC sp_PutLog 
							@@MESSAGE, 
							@@USERID, 
							@@LOCATION, 
							@@PROCESS_ID, 
							'INF', 
							'INF', 
							@@MODULE, 
							@@FUNCTION, 
							2 --STATUS
					END
					ELSE
					BEGIN
						SET @@MESSAGE = 'There Are Error in ICS.TB_R_LOG_D with ICS Process ID ' + ISNULL(CONVERT(VARCHAR(MAX), @@ICS_PROCCESS_ID), '')
						EXEC sp_PutLog 
							@@MESSAGE, 
							@@USERID, 
							@@LOCATION, 
							@@PROCESS_ID, 
							'INF', 
							'INF', 
							@@MODULE, 
							@@FUNCTION, 
							2 --STATUS
					END
				END
				ELSE
				BEGIN
					SET @@MESSAGE = 'Invoice with Supplier Invoice No ' + ISNULL(@@SUPP_INV_NO, '') + ' and Supplier Code ' + ISNULL(@@SUPP_CD, '') +
									' Balance is Not 0 in ICS'
					EXEC sp_PutLog 
						@@MESSAGE, 
						@@USERID, 
						@@LOCATION, 
						@@PROCESS_ID, 
						'SUC', 
						'SUC', 
						@@MODULE, 
						@@FUNCTION, 
						2 --STATUS
				END
			END TRY
			BEGIN CATCH
				SET @@MESSAGE = ERROR_MESSAGE()
				EXEC sp_PutLog 
					@@MESSAGE, 
					@@USERID, 
					@@LOCATION, 
					@@PROCESS_ID, 
					'ERR', 
					'ERR', 
					@@MODULE, 
					@@FUNCTION, 
					3 --STATUS
			END CATCH
		END
		ELSE
		BEGIN
			SET @@MESSAGE = 'Supplier Invoice No ' + ISNULL(@@SUPP_INV_NO, '') + ' and Supplier Code ' + ISNULL(@@SUPP_CD, '') + ' Already Re-Posting by Someone Manually'
			EXEC sp_PutLog 
					@@MESSAGE, 
					@@USERID, 
					@@LOCATION, 
					@@PROCESS_ID, 
					'INF', 
					'INF', 
					@@MODULE, 
					@@FUNCTION, 
					2 --STATUS
		END

		FETCH NEXT FROM cur_inv INTO @@SUPP_INV_NO, @@SUPP_CD
	END
END

