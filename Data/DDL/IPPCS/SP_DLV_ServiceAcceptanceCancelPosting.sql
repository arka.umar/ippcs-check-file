/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Service acceptance cancel posting
*/

CREATE PROCEDURE [dbo].[SP_DLV_ServiceAcceptanceCancelPosting]
	@pid as BIGINT,
	@MODULE_ID as varchar(1),
	@FUNCTION_ID as varchar(5), -- '31303'
	@UserId as varchar(20)
AS
BEGIN
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @na AS VARCHAR(50)
	DECLARE @step AS VARCHAR(50)
	DECLARE @isPostedError varchar(1) = 'N'
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @steps AS VARCHAR(MAX)
	DECLARE @SP_NAME as varchar(MAX) = 'SP_DLV_ServiceAcceptanceCancelPosting'
	DECLARE @SERVICE_DOC_NO as VARCHAR(11)
	DECLARE @LAST_SERV_DOC as VARCHAR(11)
	DECLARE @TRANSACTION_CD as VARCHAR(100) = ''
	DECLARE @Y as VARCHAR(2)
	DECLARE @M as VARCHAR(2)
	DECLARE @D as VARCHAR(2)
	DECLARE @CURR_Y as VARCHAR(2)
	DECLARE @CURR_M as VARCHAR(2)
	DECLARE @CURR_D as VARCHAR(2)
	DECLARE @DIGIT as VARCHAR(4)
	DECLARE @SEQS as VARCHAR(4)
	DECLARE @ADDS AS VARCHAR(11)
	DECLARE @lastSeq INT;
	DECLARE @postSts as varchar(2)
	DECLARE @DlvSts as varchar(15)	
			
	DECLARE @dlv_no varchar(13);
	DECLARE @svc_no varchar(11);
	DECLARE @lcd varchar(3);
	DECLARE @rcd varchar(4);
	DECLARE @j_status varchar(10) = 'SUCCESS';
	
	DECLARE @pono as varchar(11)
	DECLARE @pin as int
	DECLARE @sdn as varchar(11)
	DECLARE @sdy as varchar(4)
	DECLARE @ino as int
	DECLARE @rn as varchar(13)

	DECLARE @fi_doc_no varchar(100)
	DECLARE @overall_sts varchar(20) = 'SUCCESS'
	DECLARE @grir_temp table(
		PO_NO varchar(11),
		PO_ITEM_NO int,
		SERVICE_DOC_NO varchar(11),
		SERVICE_DOC_YEAR varchar(4),
		ITEM_NO int,
		REFF_NO varchar(13),
		STATUS_CD varchar(10)
	)
	
	--DECLARE @po_period AS VARCHAR(6) = (SELECT CURRENT_IM_YEAR + '' + CURRENT_IM_PERIOD FROM TB_M_IM_PERIOD)

	SET @log = 'Service acceptance cancel posting has been started With Process ID ' + CONVERT(VARCHAR(MAX), @pid);
	INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceCancelPosting', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

	BEGIN TRY
		SET @log = 'Insert data Into TB_T_DLV_SA_RAW from TB_R_DELIVERY_CTL_H';
		INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceCancelPosting', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		
		INSERT INTO TB_T_DLV_SA_RAW (
			DELIVERY_NO, 
			PROCESS_ID, 
			ROUTE_CD, 
			RATE, 
			TRANSACTION_CD, 
			PICKUP_DT, 
			LP_CD, 
			POSTING_DT, 
			OK_FLAG, 
			CREATED_BY, 
			CREATED_DT)
		SELECT DISTINCT 
			a.[DELIVERY_NO], 
			@pid as PROCESS_ID, 
			a.[ROUTE], 
			a.[RATE], 
			CASE WHEN b.[ROUTE_CD] IS NULL THEN '' ELSE '302' END, 
			a.[PICKUP_DT], 
			SUBSTRING(b.PO_NO,1,3), 
			GETDATE() as POSTING_DT, 
			CASE WHEN b.[ROUTE_CD] IS NULL THEN 'NG' ELSE 'OK' END AS OK_FLAG, 
			@UserId, 
			GETDATE()
		FROM TB_R_DELIVERY_CTL_H a
			LEFT JOIN TB_R_DLV_PO_ITEM b ON 
				b.ROUTE_CD = a.ROUTE 
				AND SUBSTRING(b.PO_NO, 1,3) = a.LP_CD
		WHERE a.PROCESS_ID = @pid
	
		UPDATE TB_R_DLV_QUEUE 
			SET PROCESS_STATUS = 'QU2', 
			PROCESS_START_DT = GETDATE() 
		WHERE PROCESS_ID = @pid

		IF OBJECT_ID('tempdb..#cancel_table') IS NOT NULL 
			DROP TABLE #cancel_table

		select 
			dlv_no, 
			svc_no, 
			lcd, 
			rcd 
		into #cancel_table 
		from (
			SELECT 
				a.DELIVERY_NO as dlv_no, 
				(SELECT TOP 1 SERVICE_DOC_NO b 
					 FROM TB_R_DLV_SERVICE_DOC b 
					 WHERE b.REFF_NO = a.DELIVERY_NO 
					 ORDER BY b.SERVICE_DOC_NO DESC) as svc_no, 
				a.LP_CD as lcd, 
				a.ROUTE_CD as rcd 
			FROM TB_T_DLV_SA_RAW a 
			WHERE a.PROCESS_ID = @pid
		)tbl;
	END TRY
	BEGIN CATCH
		SET @j_status = 'ERROR'

		SET @log = 'Error While Insert Data into TB_T_DLV_SA_RAW : ' + ERROR_MESSAGE();
		INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SP_DLV_ServiceAcceptanceCancelPosting', @pid, 'MPCS00002ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;
	END CATCH

	IF(@j_status = 'SUCCESS')
	BEGIN
		SELECT @TRANSACTION_CD = SYSTEM_VALUE 
		FROM TB_M_SYSTEM 
		WHERE FUNCTION_ID = '31303' 
			  AND SYSTEM_CD = 'TRANS_CD_SA_CANCEL_POSTING'

		DECLARE CANCELLATION_CURS CURSOR FOR 
			SELECT dlv_no, svc_no, lcd, rcd FROM #cancel_table
		OPEN CANCELLATION_CURS;
		FETCH NEXT FROM CANCELLATION_CURS INTO @dlv_no, @svc_no, @lcd, @rcd;
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			BEGIN TRY
				SET @log = '[' + @dlv_no + '] SA Cancel Process Started'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				SET @LAST_SERV_DOC = (SELECT TOP 1 SERVICE_DOC_NO FROM TB_R_DLV_GR_IR ORDER BY SERVICE_DOC_NO DESC)
				SET @Y  = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2)
				SET @M  = dbo.FN_VARMONTH(GETDATE())
				SET @D = dbo.FN_VARDAY(GETDATE())
				SET @SERVICE_DOC_NO = @MODULE_ID + '' + @Y;
			
				IF(@LAST_SERV_DOC <> '')
				BEGIN
					SET @CURR_Y = (SELECT SUBSTRING(@LAST_SERV_DOC, 2,2))	
					SET @CURR_M = (SELECT SUBSTRING(@LAST_SERV_DOC, 4,2))
					SET @CURR_D = (SELECT SUBSTRING(@LAST_SERV_DOC, 6,2))
					SET @DIGIT = '0000'
					SET @SEQS = (SELECT SUBSTRING(@LAST_SERV_DOC, 8, LEN(@LAST_SERV_DOC)))
					SET @ADDS = (SELECT CAST(CAST(@SEQS as INT)+1 AS VARCHAR(11)))
					IF(SUBSTRING(@LAST_SERV_DOC,1,7) <> ('3' + @Y + '' + @M + '' + @D))
						SET @SERVICE_DOC_NO = @MODULE_ID + '' + @Y + '' + @M + '' + @D + '0001';
					ELSE
						SET @SERVICE_DOC_NO = @MODULE_ID + '' + @CURR_Y + '' + @CURR_M + '' + @CURR_D + ''+ (SELECT SUBSTRING(@DIGIT, 1, LEN(@DIGIT) - LEN(@ADDS)) + @ADDS);
				END
				ELSE
				BEGIN
					SET @SERVICE_DOC_NO = @MODULE_ID + '' + @Y + '' + @M + '' + @D + '0001';
				END

				SET @log = '[' + @dlv_no + '] Generate new Service Doc. No. ' + ISNULL(@SERVICE_DOC_NO, '')
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				UPDATE TB_R_DLV_SERVICE_DOC 
					SET SA_CANCEL_REFF_NO = @SERVICE_DOC_NO, 
						SA_CANCEL_REFF_YEAR = CAST(YEAR(DATEADD(MM, -3, GETDATE())) as VARCHAR(4)), 
						CHANGED_BY = @UserId, 
						CHANGED_DT = GETDATE() 
					WHERE SERVICE_DOC_NO = @svc_no 
						  AND REFF_NO = @dlv_no

				SET @log = '[' + @dlv_no + '] Success Update SA Cancel Reff No To Service Doc. No. ' + ISNULL(@svc_no, '')
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		
				INSERT INTO TB_R_DLV_SERVICE_DOC (
					SERVICE_DOC_NO, 
					SERVICE_DOC_YEAR, 
					PROCESS_ID, 
					DOC_DT,
					PO_NO,
					PROD_MONTH,
					LP_CD, 
					ROUTE_CD, 
					TRANSACTION_QTY, 
					TRANSACTION_CD, 
					TRANSACTION_SIGN, 
					TRANSACTION_VALUE, 
					TRANSACTION_CURR, 
					REFF_NO, 
					SYSTEM_SOURCE, 
					CREATED_BY, 
					CREATED_DT)
				SELECT TOP 1 
					@SERVICE_DOC_NO, 
					SERVICE_DOC_YEAR, 
					@pid, 
					DOC_DT,
					PO_NO,
					PROD_MONTH,
					LP_CD, 
					ROUTE_CD, 
					TRANSACTION_QTY, 
					@TRANSACTION_CD as TRANSACTION_CD, 
					'-' as TRANSACTION_SIGN, 
					TRANSACTION_VALUE, 
					TRANSACTION_CURR, 
					@dlv_no, 
					SYSTEM_SOURCE, 
					@UserId, 
					GETDATE() 
				FROM TB_R_DLV_SERVICE_DOC 
				WHERE REFF_NO = @dlv_no 
				ORDER BY SERVICE_DOC_NO DESC
	
				SET @log = '[' + @dlv_no + '] Insert new Data into TB_R_DLV_SERVICE_DOC with Service Doc. No. ' + ISNULL(@SERVICE_DOC_NO, '')
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				--IF OBJECT_ID('tempdb..#LoopRaw3') IS NOT NULL 
				--	DROP TABLE #LoopRaw3

				--select 
				--	pono, 
				--	pin, 
				--	sdn, 
				--	sdy, 
				--	ino, 
				--	rn 
				--into #LoopRaw3 from(
				--	SELECT PO_NO as pono, 
				--		   PO_ITEM_NO as pin, 
				--		   SERVICE_DOC_NO as sdn, 
				--		   SERVICE_DOC_YEAR as sdy, 
				--		   ITEM_NO as ino, 
				--		   REFF_NO as rn 
				--	FROM TB_R_DLV_GR_IR 
				--	WHERE REFF_NO = @dlv_no
				--)tbl2;

				--DECLARE BTMP_CURS3 CURSOR FOR 
				--	SELECT pono, pin, sdn, sdy, ino, rn FROM #LoopRaw3;
				--OPEN BTMP_CURS3;
				--FETCH NEXT FROM BTMP_CURS3 INTO @pono, @Pin, @sdn, @sdy, @ino, @rn;
				--WHILE @@FETCH_STATUS = 0  
				--BEGIN  
					INSERT INTO TB_R_DLV_GR_IR ( 
						PO_NO, 
						PO_ITEM_NO, 
						SERVICE_DOC_NO, 
						SERVICE_DOC_YEAR, 
						ITEM_NO, 
						DOC_DT, 
						GR_IR_AMT, 
						LP_CD, 
						COMP_PRICE_CD, 
						ACCRUAL_POSTING_FLAG, 
						REFF_NO, 
						CONDITION_CATEGORY, 
						PO_DETAIL_PRICE, 
						TAX_CD, 
						PLUS_MINUS_FLAG, 
						ITEM_CURR, 
						CANCEL_STS, 
						CREATED_BY, 
						CREATED_DT)
					 SELECT PO_NO, 
							PO_ITEM_NO, 
							@SERVICE_DOC_NO, 
							CAST(YEAR(DATEADD(MM,-3, GETDATE())) as VARCHAR(4)), 
							ITEM_NO, 
							GETDATE(), 
							GR_IR_AMT, 
							LP_CD, 
							COMP_PRICE_CD, 
							ACCRUAL_POSTING_FLAG, 
							REFF_NO, 
							CONDITION_CATEGORY, 
							PO_DETAIL_PRICE, 
							TAX_CD, 
							PLUS_MINUS_FLAG, 
							'IDR', 
							'1', 
							@UserId, 
							GETDATE() 
					  FROM TB_R_DLV_GR_IR 
					  WHERE SERVICE_DOC_NO = @svc_no 
							AND REFF_NO = @dlv_no
						
					SET @log = '[' + @rn + '] Insert New Data Into TB_R_DLV_GR_IR with Service Doc. No. ' + ISNULL(@SERVICE_DOC_NO, '')
					INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

					INSERT INTO @grir_temp
					SELECT
						PO_NO,
						PO_ITEM_NO,
						SERVICE_DOC_NO,
						SERVICE_DOC_YEAR,
						ITEM_NO,
						REFF_NO,
						STATUS_CD
					FROM TB_R_DLV_GR_IR 
					WHERE SERVICE_DOC_NO = @svc_no
						  AND REFF_NO = @dlv_no

				--	FETCH NEXT FROM BTMP_CURS3 INTO @pono, @Pin, @sdn, @sdy, @ino, @rn
				--END;
				--CLOSE BTMP_CURS3;
				--DEALLOCATE BTMP_CURS3;
				--DROP TABLE #LoopRaw3;
				
				UPDATE TB_R_DELIVERY_CTL_H 
					SET SA_CANCEL_REFF_NO = @SERVICE_DOC_NO, 
						SA_CANCEL_REFF_YEAR = CAST(YEAR(DATEADD(MM,-3, GETDATE())) as VARCHAR(4)) 
				WHERE DELIVERY_NO = @dlv_no
		
				SET @log = '[' + @dlv_no + '] Update Cancel Reff. No. to TB_R_DELIVERY_CTL_H'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				UPDATE TB_R_DLV_PO_ITEM 
					SET OPEN_QTY = [OPEN_QTY] + 1, 
						[SA_QTY] = [SA_QTY] - 1 
				WHERE [PO_NO] = (SELECT PO_NO FROM TB_R_DLV_SERVICE_DOC 
								 WHERE REFF_NO = @dlv_no 
									   and SERVICE_DOC_NO = @SERVICE_DOC_NO) 
					  AND [ROUTE_CD] = @rcd

				SET @log = '[' + @dlv_no + '] Update PO Item SA QTY and Open QTY'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
		
				EXEC SP_DLV_POSTING_GR @UserId, @pid, @dlv_no, 'TRANS_CD_SA_CANCEL_POSTING', @j_status OUTPUT
		
				IF(ISNULL(@j_status, '') = 'ERROR')
				BEGIN
					RAISERROR('Error When Posting Journal', 16, 1)
				END

				UPDATE TB_R_DLV_GR_IR 
					SET STATUS_CD = '',
						SA_CANCEL_REFF_YEAR = CAST(YEAR(DATEADD(MM,-3, GETDATE())) as VARCHAR(4)), 
						CANCEL_STS = '1', 
						SA_CANCEL_REFF_NO = @SERVICE_DOC_NO 
				WHERE SERVICE_DOC_NO = @svc_no
						AND REFF_NO = @dlv_no

				SET @log = '[' + @dlv_no + '] Update Cancel Reff. Into TB_R_DLV_GR_IR Service Doc. No. ' + ISNULL(@svc_no, '')
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
				
				UPDATE TB_R_DELIVERY_CTL_H
					SET DELIVERY_STS = 'Canceled'
				WHERE DELIVERY_NO = @dlv_no

				SET @log = '[' + @dlv_no + '] Update Status to Canceled'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

			END TRY
			BEGIN CATCH

				SET @overall_sts = 'ERROR'

				SET @log = '[' + @dlv_no + '] Error : ' + ERROR_MESSAGE()
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;

				IF CURSOR_STATUS('global','BTMP_CURS3') >= -1
				BEGIN
					CLOSE BTMP_CURS3
					DEALLOCATE BTMP_CURS3
				END

				UPDATE TB_R_DELIVERY_CTL_H 
					SET SA_CANCEL_REFF_NO = NULL, 
						SA_CANCEL_REFF_YEAR = NULL,
						DELIVERY_STS = 'Posted'
				WHERE DELIVERY_NO = @dlv_no

				SET @log = '[' + @dlv_no + '] Rollback TB_R_DELIVERY_CTL_H'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				UPDATE TB_R_DLV_PO_ITEM 
					SET OPEN_QTY = [OPEN_QTY] - 1, 
						[SA_QTY] = [SA_QTY] + 1 
				WHERE [PO_NO] = (SELECT PO_NO FROM TB_R_DLV_SERVICE_DOC 
								 WHERE REFF_NO = @dlv_no 
									   and SERVICE_DOC_NO = @SERVICE_DOC_NO) 
					  AND [ROUTE_CD] = @rcd

				SET @log = '[' + @dlv_no + '] Rollback TB_R_DLV_PO_ITEM'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				UPDATE TB_R_DLV_SERVICE_DOC 
					SET SA_CANCEL_REFF_NO = NULL, 
						SA_CANCEL_REFF_YEAR = NULL
					WHERE SERVICE_DOC_NO = @svc_no 
						  AND REFF_NO = @dlv_no

				SET @log = '[' + @dlv_no + '] Rollback TB_R_DLV_SERVICE_DOC'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				UPDATE ir 
					SET ir.STATUS_CD = tir.STATUS_CD
				FROM TB_R_DLV_GR_IR ir
					JOIN @grir_temp tir
						ON tir.PO_NO = ir.PO_NO
						   AND tir.PO_ITEM_NO = ir.PO_ITEM_NO
						   AND tir.ITEM_NO = ir.ITEM_NO
						   AND tir.SERVICE_DOC_NO = ir.SERVICE_DOC_NO
						   AND tir.SERVICE_DOC_YEAR = ir.SERVICE_DOC_YEAR
						   AND tir.REFF_NO = ir.REFF_NO
					
				SET @log = '[' + @dlv_no + '] Rollback TB_R_DLV_GR_IR'
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

				DELETE FROM TB_R_DLV_FI_JOURNAL 
				WHERE DOC_NO IN(SELECT TOP 1 DOC_NO FROM TB_R_DLV_FI_TRANS_H 
								WHERE SUBSTRING(REFF_NO, 1, CASE WHEN CHARINDEX('-', REFF_NO) - 1 <= 0 THEN 0 ELSE CHARINDEX('-', REFF_NO) - 1 END)  = @SERVICE_DOC_NO)

				DELETE FROM TB_R_DLV_FI_TRANS_D 
				WHERE DOC_NO IN (SELECT TOP 1 DOC_NO FROM TB_R_DLV_FI_TRANS_H 
								 WHERE SUBSTRING(REFF_NO, 1, CASE WHEN CHARINDEX('-', REFF_NO) - 1 <= 0 THEN 0 ELSE CHARINDEX('-', REFF_NO) - 1 END) = @SERVICE_DOC_NO)

				DELETE FROM TB_R_DLV_FI_TRANS_H 
					WHERE SUBSTRING(REFF_NO, 1, CASE WHEN CHARINDEX('-', REFF_NO) - 1 <= 0 THEN 0 ELSE CHARINDEX('-', REFF_NO) - 1 END) = @SERVICE_DOC_NO
			    
				DELETE FROM TB_R_DLV_GR_IR WHERE SERVICE_DOC_NO = @SERVICE_DOC_NO
			
				DELETE FROM TB_R_DLV_SERVICE_DOC WHERE SERVICE_DOC_NO = @SERVICE_DOC_NO

				SET @log = '[' + @dlv_no + '] Delete Cancellation Data with Service Doc. No. ' + ISNULL(@SERVICE_DOC_NO, '')
				INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;
			END CATCH
			FETCH NEXT FROM CANCELLATION_CURS INTO @dlv_no, @svc_no, @lcd, @rcd
		END;
		CLOSE CANCELLATION_CURS;
		DEALLOCATE CANCELLATION_CURS;
	END

	drop table #cancel_table;
	
	IF(@overall_sts = 'SUCCESS')
	BEGIN
		SET @log = 'SA Cancel Process Success'
		INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002INF', 'INF', @MODULE_ID, @FUNCTION_ID;

		UPDATE TB_R_DLV_QUEUE 
			set PROCESS_STATUS = 'QU3', 
			PROCESS_END_DT = GETDATE(), 
			CHANGED_BY = @UserId, 
			CHANGED_DT = GETDATE() 
		WHERE PROCESS_ID = @pid
	END
	ELSE
	BEGIN
		SET @log = 'SA Cancel Process Finish With Error'
		INSERT @T EXEC dbo.sp_PutLog @log, @UserId, 'SACancelPostingProcess', @pid, 'MPCS00002ERR', 'ERR', @MODULE_ID, @FUNCTION_ID;

		UPDATE TB_R_DLV_QUEUE 
			set PROCESS_STATUS = 'QU4', 
			PROCESS_END_DT = GETDATE(), 
			CHANGED_BY = @UserId, 
			CHANGED_DT = GETDATE() 
		WHERE PROCESS_ID = @pid
	END
	
	DELETE FROM TB_T_DLV_SA_RAW WHERE PROCESS_ID = @pid			
	UPDATE TB_R_DELIVERY_CTL_H SET PROCESS_ID = NULL WHERE PROCESS_ID = @pid
END

