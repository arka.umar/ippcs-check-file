/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Preview invoice when creation invoice
*/

CREATE PROCEDURE [dbo].[SP_DLV_INV_PrevInvoice]
   @SourceString varchar(max),
   @SourceString2 varchar(max)
AS
BEGIN
--ALS20130861,ALS20130861,ALS20130861
--D31401 (line 472)
--R201302167450,R201302187688,R201302187692
--D31401 (line 473)
--TESTINV
	--DECLARE @SourceString as varchar(max) = 'ALS20130861,ALS20130861,ALS20130861'
	--DECLARE @SourceString2 as Varchar(max) = 'R201302167450,R201302187688,R201302187692'
	DECLARE @x XML;
	SET @x = '<r>' + REPLACE((SELECT @SourceString FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	DECLARE @a XML;
	SET @a = '<r>' + REPLACE((SELECT @SourceString2 FOR XML PATH('')), ',', '</r><r>') + '</r>';
	
	
	
	--IF(@SourceString2='')
	--BEGIN
	--	SELECT a.PO_NO, b.LP_INV_NO, CASE WHEN a.[STATUS_CD] = 2 THEN 'Created' ELSE CASE WHEN a.[STATUS_CD] = 1 THEN 'Outstanding' ELSE 'Canceled' END END as STATUS_CD FROM TB_R_DLV_GR_IR a 
	--	JOIN TB_R_DLV_INV_UPLOAD b ON b.PO_NO = a.PO_NO
	--	WHERE a.PO_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol))
	--	GROUP BY b.LP_INV_NO, a.PO_NO, a.STATUS_CD
	--END
	--ELSE
	--BEGIN
	
	--END
	
	
		--SELECT a.PO_NO, b.LP_INV_NO, CASE WHEN a.[STATUS_CD] = 2 THEN 'Created' ELSE CASE WHEN a.[STATUS_CD] = 1 THEN 'Outstanding' ELSE 'Canceled' END END as STATUS_CD FROM TB_R_DLV_GR_IR a 
		--JOIN TB_R_DLV_INV_UPLOAD b ON b.PO_NO = a.PO_NO
		--WHERE a.PO_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND b.LP_INV_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @a.nodes('/r') y(XmlCol))
		--GROUP BY b.LP_INV_NO, a.PO_NO, a.STATUS_CD
		
		SELECT  MAX(a.PO_NO) AS PO_NO, b.LP_INV_NO, sts.STATUS_NAME as STATUS_CD FROM TB_R_DLV_GR_IR a
		LEFT JOIN TB_R_DLV_INV_UPLOAD b ON b.PO_NO = a.PO_NO AND b.DELIVERY_NO = a.REFF_NO AND B.STATUS_CD IN ('1', '-1')
		LEFT JOIN TB_M_INV_STATUS sts ON sts.STATUS_CD = a.STATUS_CD
		WHERE a.PO_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @x.nodes('/r') y(XmlCol)) AND a.REFF_NO IN (SELECT  y.XmlCol.value('(text())[1]', 'VARCHAR(1000)') AS Value FROM @a.nodes('/r') y(XmlCol)) AND a.STATUS_CD = '1'
		GROUP BY b.LP_INV_NO, sts.STATUS_NAME
	
END

