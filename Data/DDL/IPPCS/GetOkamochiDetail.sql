CREATE PROCEDURE [dbo].[GetOkamochiDetail](@DOCKCD VARCHAR(3), @DATEFROM VARCHAR(15), @DATETO VARCHAR(15), @RTEGRPCD VARCHAR(5), @RUNSEQ VARCHAR(3), @prodDate VARCHAR(15))
AS
BEGIN

DECLARE @TEXT VARCHAR(MAX)
DECLARE @PHYDOCK TABLE (DOCKCD VARCHAR(4))

INSERT INTO @PHYDOCK
	SELECT RCVCOMPDOCKCD [DOCK]
	FROM TB_R_DCL_TLMS_ORDER_ASSIGNMENT  --TB_T_TLMS_EMPTY_JOB_CARD
	WHERE RTEGRPCD = @RTEGRPCD
			AND RUNSEQ = @RUNSEQ 
			--AND RTEDATE >= @DATEFROM 
			--AND RTEDATE <= @DATETO --replacing "--" with --a code fid.deny 2015-04-01
			AND RTEDATE IN(@DATEFROM, @DATETO) --a
			AND RCVCOMPDOCKCD = @DOCKCD
	UNION
	SELECT A.LOGICAL_DOCKCD [DOCK]
	FROM TB_M_PHYSICAL_DOCK_MAPPING A
	INNER JOIN TB_R_DCL_TLMS_ORDER_ASSIGNMENT B --TB_T_TLMS_EMPTY_JOB_CARD
		ON B.RCVCOMPDOCKCD = A.LOGICAL_DOCKCD
			AND RTEGRPCD = @RTEGRPCD
			--AND RTEDATE >= @DATEFROM 
			--AND RTEDATE <= @DATETO --replacing "--" with --a code fid.deny 2015-04-01
			AND RTEDATE IN(@DATEFROM, @DATETO) --a
			AND RUNSEQ = @RUNSEQ
	WHERE A.PHYSICAL_DOCKCD = @DOCKCD

SELECT	DISTINCT RIGHT(REPLICATE('0',4) + CAST(A.SUPPCD AS VARCHAR(4)),4) + ' - ' + A.SUPPPLANTCD SUPPLIERCD, 
		B.SUPPLIER_ABBREVIATION SUPPLIERABBR,
		A.RCVCOMPDOCKCD DOCK,
		--modified fid.deny 2015-05-05
		CASE WHEN ISNULL(@prodDate, '') = '' THEN
			RIGHT(REPLICATE('0',2) + CAST(A.ORDSEQ AS VARCHAR(2)),2)
		ELSE
			RIGHT(REPLICATE('0',2) + CONVERT(VARCHAR(2),DAY(CAST(A.ORDDATE AS DATE))), 2) + RIGHT(REPLICATE('0',2) + CAST(A.ORDSEQ AS VARCHAR(2)),2)
		END
		AS ORD
		--end of modified
FROM TB_R_DCL_TLMS_ORDER_ASSIGNMENT A  --TB_T_TLMS_EMPTY_JOB_CARD
	INNER JOIN TB_M_SUPPLIER B 
		ON B.SUPPLIER_CODE = RIGHT(REPLICATE('0',4) + CAST(A.SUPPCD AS VARCHAR(4)),4) 
		   AND B.SUPPLIER_PLANT_CD = A.SUPPPLANTCD
	INNER JOIN @PHYDOCK C 
		ON A.RCVCOMPDOCKCD = C.DOCKCD
WHERE 
	--A.RTEDATE >= @DATEFROM AND A.RTEDATE <= @DATETO --replacing "--" with --a code fid.deny 2015-04-01
	A.RTEDATE IN (@DATEFROM, @DATETO) --a
	AND A.RTEGRPCD = @RTEGRPCD
	AND A.RUNSEQ = @RUNSEQ
	--AND A.PARTEMPKBTYPECD = '1'
ORDER BY SUPPLIERCD ASC, DOCK ASC, ORD ASC
END

