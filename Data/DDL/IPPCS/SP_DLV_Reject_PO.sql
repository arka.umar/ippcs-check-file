/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : PO reject
*/

CREATE PROCEDURE [dbo].[SP_DLV_Reject_PO]

@PO_NO Varchar (MAX),
@CREATED_BY VARCHAR (25)

AS
BEGIN
		
		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_Reject_PO'
		DECLARE @step AS VARCHAR(50) = 'Init'
		DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)

		DECLARE @MODUL_ID INT = 3
		DECLARE @FUNCTION_ID VARCHAR (6) =  '31203'

		DECLARE @POSITION_ID INT, @CHECK_AUTH INT;
		DECLARE @CHECKSTS VARCHAR (3), @CHECK_APPROVAL VARCHAR(3);
		DECLARE @STS VARCHAR (3), @POS INT, @LVL INT;
		


		--EXPLODE
		DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
		DECLARE @MIN_ROW INT;

		DECLARE @ROW_DETAIL VARCHAR(MAX);
		DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
		DECLARE @MIN_FIELD INT;

		DECLARE @PONO_T VARCHAR(12);

		INSERT INTO @ROWS
		SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@PO_NO,';');
		--EXPLODE

		--==CREATE LOG==--
		SET @steps = @na+'.'+@step;
		SET @log = 'PO Rejection process started.';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		--==END CREATE LOG==--

		--==CREATE LOG==--
		SET @steps = @na+'.Process';
		SET @log = 'Check authorization level';
		EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
		--==END CREATE LOG==--

		SET @CHECK_AUTH = (SELECT COUNT (*) FROM TB_R_VW_EMPLOYEE WHERE DIVISION_ID = 19 AND POSITION_ID IN (6,3,12) AND USERNAME = @CREATED_BY);
		
		IF (@CHECK_AUTH=1)BEGIN
				
				SET @POSITION_ID = (SELECT POSITION_LEVEL  FROM [dbo].[TB_R_VW_EMPLOYEE] WHERE USERNAME = @CREATED_BY);

				IF (@POSITION_ID =50) BEGIN
					SET @POS = 3;
					SET @STS = 'SH';
				END
				ELSE IF (@POSITION_ID =40) BEGIN
					SET @POS = 4;
					SET @STS = 'DpH';
				END
				ELSE BEGIN 
					SET @POS = 5;
					SET @STS = 'DH';
				END
				--ELSE BEGIN
					--SET @POS = 0;
					--SET @STS = 'PR2';
				--END

			--==CREATE LOG==--
			SET @steps = @na+'.Process';
			SET @log = 'Parsing data from parameter';
			EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
			--==END CREATE LOG==--


			SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

					WHILE @MIN_ROW IS NOT NULL BEGIN

							DELETE FROM @FIELDS;
							

							SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
							INSERT INTO @FIELDS
							SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'');
							

							SET @PONO_T = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
							-- ========================================================================================
							
							--==CREATE LOG==--
							SET @steps = @na+'.Process';
							SET @log = 'Checking PO_STATUS_FLAG  for PO Number '+@PONO_T;
							EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
							--==END CREATE LOG==--

							SET @CHECKSTS = (SELECT PO_STATUS_FLAG FROM TB_R_DLV_PO_H WHERE PO_NO = @PONO_T);

									IF (@CHECKSTS = 'PO1') BEGIN
											SET @LVL = 2;
									END
									ELSE IF (@CHECKSTS = 'PO2') BEGIN
										SET @LVL = 3;
									END
									ELSE IF (@CHECKSTS = 'PO3') BEGIN
										SET @LVL = 4;
									END
									ELSE BEGIN
										SET @LVL = 5;
									END
								
							IF (@CHECKSTS = 'PO5') OR (@CHECKSTS = 'PO6') OR (@CHECKSTS = 'PO4') BEGIN
									
									--==CREATE LOG==--
									SET @steps = @na+'.Process';
									SET @log = 'Failed to reject PO Number '+@PONO_T;
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
									--==END CREATE LOG==--
									

									--==CREATE LOG==--
									SET @steps = @na+'.Finish';
									SET @log = 'PO Rejection process has been finished.';
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
									--==END CREATE LOG==--

									RAISERROR('Failed to reject.',16,1)
									RETURN
							END
							ELSE BEGIN


									
									IF ( @POS = @LVL OR @POS > @LVL) BEGIN

											UPDATE TB_R_DLV_PO_H
											SET 
											REJECTED_BY = @CREATED_BY,
											REJECTED_POSITION = @STS,
											REJECTED_DT =  GETDATE(),
											PO_STATUS_FLAG = 'PO5',
											CHANGED_BY = @CREATED_BY,
											CHANGED_DT =  GETDATE(),
											RELEASED_FLAG = 'N'
											WHERE 
											PO_NO = @PONO_T;
											
											UPDATE TB_R_DLV_PR_D
												SET PO_NO = NULL,
													PO_FLAG = '1'
											WHERE PO_NO = @PONO_T;

											--==CREATE LOG==--
											SET @steps = @na+'.Process';
											SET @log = 'Updating TB_R_DLV_PO_H for PO Number '+@PONO_T+' By '+@STS;
											EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
											--==END CREATE LOG==--
						
						
											---MODIF BY AGUNG 11.07.2014
											--CREATE LOG
											SET @steps = @na+'.Process';
											SET @log = 'Starting sent e-mail.';

											EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;				

											BEGIN TRY
											--===========SEND E-MAIL
											DECLARE @RecipientEmail varchar(max),
													@RecipientCopyEmail varchar(max),
													@RecipientBlindCopyEmail varchar(max),
													@ERR_FLAG varchar(50),
													@body varchar(max),
													@subjek varchar(100),
													@ERROR_DETAIL varchar(max),
													@profile varchar(max),
													@MAIL_QUERY VARCHAR(MAX)
					
											SET @profile = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'EMAIL_PROFILE' AND SYSTEM_CD = 'EMAIL_PROFILE');
											IF (@profile IS NULL) BEGIN
													SET @profile = 'NotificationAgent';
											END
			
											SELECT @RecipientEmail=EMAIL_TO, @RecipientCopyEmail=EMAIL_CC, @RecipientBlindCopyEmail=EMAIL_BCC 
											FROM TB_M_RTP_NOTIFICATION_EMAIL 
											WHERE MODULE_ID = CAST(@MODUL_ID AS VARCHAR) 
												AND FUNCTION_ID = @FUNCTION_ID AND ACTION = 'PO_REJECT'
			
											SELECT  @subjek=NOTIFICATION_SUBJECT,
													@body=NOTIFICATION_CONTENT
											FROM    TB_M_NOTIFICATION_CONTENT
											WHERE   FUNCTION_ID = @FUNCTION_ID
													AND ROLE_ID = '31'
													AND NOTIFICATION_METHOD = '1'
			
											SET @subjek = REPLACE(@subjek, '@subjectaction', 'Rejected')
											SET @subjek = REPLACE(@subjek, '@ProdMonth', RIGHT(LEFT(@PONO_T, 9), 6))
											SET @body = REPLACE(@body, '@bodyaction', 'Rejected')
											SET @body = REPLACE(@body, '@approve', @CREATED_BY)
											SET @body = REPLACE(@body, '@DateTime', (SELECT CAST(DAY(GETDATE()) AS VARCHAR) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR)))
											SET @body = REPLACE(@body, '@pono', @PONO_T)
											
											IF (@RecipientCopyEmail IS NOT NULL)
												SET @RecipientCopyEmail = '@copy_recipients = ''' + @RecipientCopyEmail + ''','
												
											IF (@RecipientBlindCopyEmail IS NOT NULL)
												SET @RecipientBlindCopyEmail = '@blind_copy_recipients = ''' + @RecipientBlindCopyEmail + ''','
											
											SET @body = ISNULL(@body, '')
											SET @subjek = ISNULL(@subjek, '')
											
											SET @RecipientEmail = ISNULL(@RecipientEmail, '')
											SET @RecipientCopyEmail = ISNULL(@RecipientCopyEmail, '')
											SET @RecipientBlindCopyEmail = ISNULL(@RecipientBlindCopyEmail, '')
			
											SET @MAIL_QUERY = '	
																EXEC msdb.dbo.sp_send_dbmail
																	@profile_name	= ''' + @profile + ''',
																	@body 			= ''' + @body + ''',
																	@body_format 	= ''HTML'',
																	@recipients 	= ''' + @RecipientEmail + ''',
																	' + @RecipientCopyEmail + '
																	' + @RecipientBlindCopyEmail + '
																	@subject 		= ''' + @subjek + ''''
											
											EXEC (@MAIL_QUERY)
											
											SET @steps	= @na + '.Process';
											SET @log	= 'Email notification has been sent';

											EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
											--===========END OF SEND E-MAIL

												--CREATE LOG
											SET @steps = @na+'.Process';
											SET @log = 'send e-mail has been finished.';

											EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;

											
											--== UPDATE QUEUE
											UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3' ,PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;

											--==FINISH==--

											END TRY
											BEGIN CATCH
													DECLARE @ERROR VARCHAR(MAX)
													SET @ERROR = ERROR_MESSAGE();
													--CREATE LOG
													SET @steps = @na+'.Process';
													SET @log = 'Email notification failed sent to all PIC.';
													EXEC dbo.sp_PutLog @ERROR, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;
													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;

													UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
													WHERE PROCESS_ID = @pid;


											END CATCH
											
											
									END

									ELSE BEGIN
											RAISERROR('Failed to reject.',16,1)
											RETURN
									END
							END



			SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
					-- =================================================================================
			
			END --END WHILE
			
			--==CREATE LOG==--
			SET @steps = @na+'.Finish';
			SET @log = 'PO Rejection process has been finished.';
			EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
			--==END CREATE LOG==--


		END
		ELSE BEGIN
				
				--==CREATE LOG==--
				SET @steps = @na+'.Process';
				SET @log = 'Access denied!';
				EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,0;
				--==END CREATE LOG==--
				
				--==CREATE LOG==--
				SET @steps = @na+'.Finish';
				SET @log = 'PO Rejection process has been finished.';
				EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,0;
				--==END CREATE LOG==--

				RAISERROR('You are not authorized to perform this action.',16,1)
				RETURN

		END
END

