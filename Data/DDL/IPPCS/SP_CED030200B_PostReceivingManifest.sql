-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-05-02
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030200B_PostReceivingManifest] @MANIFEST_NO VARCHAR(max) = ''
	,@MANIFEST_RECEIVING_DT VARCHAR(max) = ''
	,@CREATED_BY VARCHAR(max) = ''
	,@PROCESS_ID VARCHAR(max) = ''
AS
BEGIN
	-- FID.Ridwan : 2019-11-25 apply log monitoring
	DECLARE @T AS TABLE (PID BIGINT)
	DECLARE @pid AS BIGINT = 0
	DECLARE @log AS VARCHAR(MAX)
	DECLARE @MODULE_ID AS VARCHAR(1) = '2'
	DECLARE @FUNCTION_ID AS VARCHAR(5) = '23003'
	DECLARE @R_SEQ_NO INT
	DECLARE @TB_T_LOG_D AS TABLE (
		PROCESS_ID BIGINT
		,SEQUENCE_NUMBER INT
		,MESSAGE_ID VARCHAR(12)
		,MESSAGE_TYPE VARCHAR(3)
		,[MESSAGE] VARCHAR(MAX)
		,LOCATION VARCHAR(MAX)
		,CREATED_BY VARCHAR(20)
		,CREATED_DATE DATETIME
		)

	DECLARE @MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
	DECLARE @PROCESS_ID_IPPCS AS TABLE (PROCESS_ID_IPPCS BIGINT)

	CREATE TABLE #TB_List_Message (
		Result VARCHAR(MAX)
		,Message VARCHAR(MAX)
		)
		
	SET @log = 'Starting [Send Receiving data]';

	INSERT @T
	EXEC dbo.sp_PutLog @log
		,'SYSTEM'
		,'SendReceivingData.init'
		,@pid OUTPUT
		,'MPCS00002INF'
		,'INF'
		,@MODULE_ID
		,@FUNCTION_ID;

	BEGIN TRY
		SET @log = 'Iteration : Check PID yang masih aktif';

		INSERT INTO @TB_T_LOG_D
		SELECT @pid
			,NULL
			,'MPCS00002INF'
			,'INF'
			,@log
			,'SendReceivingData'
			,'SYSTEM'
			,GETDATE()

		-- Check PID yang masih aktif
		INSERT INTO @PROCESS_ID_IPPCS
		SELECT TOP 1 PROCESS_ID
		FROM TB_T_CED_SEND_RECEIVING
		WHERE ISNULL(FINISH_FLAG, 'N') <> 'Y'
		ORDER BY PROCESS_ID

		IF EXISTS (
				SELECT ''
				FROM @PROCESS_ID_IPPCS
				)
		BEGIN
			SET @PARAM1 = (
					SELECT *
					FROM @PROCESS_ID_IPPCS
					)

			INSERT INTO #TB_List_Message (
				Result
				,Message
				)
			SELECT 'Failed'
				,'Send Data Receiving with Process ID ' + @PARAM1 + ' not finished yet in IPPCS';
				
			SET @log = 'Iteration : Send Data Receiving with Process ID ' + @PARAM1 + ' not finished yet in IPPCS';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingData'
				,'SYSTEM'
				,GETDATE()
		END
		ELSE
		BEGIN
			SET @log = 'Iteration : Get list from TVEST';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingData'
				,'SYSTEM'
				,GETDATE()

			SELECT *
			INTO #TB_List_ManifestNo
			FROM dbo.fnSplitString(@MANIFEST_NO, '|')

			SELECT *
			INTO #TB_List_ManifestReceiving
			FROM dbo.fnSplitString(@MANIFEST_RECEIVING_DT, '|')

			SELECT *
			INTO #TB_List_CreatedBy
			FROM dbo.fnSplitString(@CREATED_BY, '|')

			SELECT *
			INTO #TB_List_ProcessId
			FROM dbo.fnSplitString(@PROCESS_ID, '|')

			SELECT A.zeroBasedOccurance + 1 AS ROWNO
				,A.s AS ManifestNo
				,B.s AS ManifestReceiving
				,C.s AS CreatedBy
				,D.s AS ProcessId
			INTO #tb_t_list_final
			FROM #TB_List_ManifestNo A
			LEFT JOIN #TB_List_ManifestReceiving B ON A.zeroBasedOccurance = B.zeroBasedOccurance
			LEFT JOIN #TB_List_CreatedBy C ON C.zeroBasedOccurance = B.zeroBasedOccurance
			LEFT JOIN #TB_List_ProcessId D ON D.zeroBasedOccurance = C.zeroBasedOccurance
			
			SET @log = 'Iteration : Delete IPPCS.TB_T_CED_SEND_RECEIVING';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingData'
				,'SYSTEM'
				,GETDATE()

			DELETE
			FROM TB_T_CED_SEND_RECEIVING
			
			SET @log = 'Iteration : Insert IPPCS.TB_T_CED_SEND_RECEIVING';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingData'
				,'SYSTEM'
				,GETDATE()

			INSERT INTO TB_T_CED_SEND_RECEIVING (
				MANIFEST_NO
				,MANIFEST_RECEIVING_DT
				,CREATED_BY
				,CREATED_DT
				,PROCESS_ID
				,FINISH_FLAG
				,FINISH_DATE
				)
			SELECT ManifestNo
				,CONVERT(DATETIME, ManifestReceiving) ManifestReceiving
				,CreatedBy
				,GETDATE() CreatedDt
				,ProcessId
				,'N'
				,NULL
			FROM #tb_t_list_final

			SET @log = 'Iteration : Update IPPCS.TB_R_DAILY_ORDER_PART';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingData'
				,'SYSTEM'
				,GETDATE()

			-- Update IPPCS.TB_R_DAILY_ORDER_PART
			UPDATE P
			SET P.RECEIVED_STATUS = '1'
				,P.CHANGED_BY = T.CREATED_BY
				,P.CHANGED_DT = GETDATE()
			FROM dbo.TB_R_DAILY_ORDER_PART P
			JOIN (
				SELECT DISTINCT T.MANIFEST_NO
					,T.CREATED_BY
				FROM TB_T_CED_SEND_RECEIVING T
				) T ON T.MANIFEST_NO = P.MANIFEST_NO

			SET @log = 'Iteration : Update IPPCS.TB_R_DAILY_ORDER_MANIFEST';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingData'
				,'SYSTEM'
				,GETDATE()

			-- Update IPPCS.TB_R_DAILY_ORDER_MANIFEST
			UPDATE M
			SET M.RECEIVE_FLAG = '1'
				,M.RECEIVE_DT = convert(DATETIME, T.MANIFEST_RECEIVING_DT)
				,M.ARRIVAL_ACTUAL_DT = convert(DATETIME, T.MANIFEST_RECEIVING_DT)
				,M.MANIFEST_RECEIVE_FLAG = '2'
				,M.DROP_STATUS_FLAG = '1'
				,M.DROP_STATUS_DT = convert(DATETIME, T.MANIFEST_RECEIVING_DT)
				,M.CHANGED_BY = T.CREATED_BY
				,M.CHANGED_DT = GETDATE()
			FROM dbo.TB_R_DAILY_ORDER_MANIFEST M
			JOIN (
				SELECT DISTINCT T.MANIFEST_NO
					,T.MANIFEST_RECEIVING_DT
					,T.CREATED_BY
				FROM TB_T_CED_SEND_RECEIVING T
				) T ON T.MANIFEST_NO = M.MANIFEST_NO WHERE ISNULL(M.MANIFEST_RECEIVE_FLAG,0) = 0
				
			SET @log = 'Iteration : Update Finish Flag IPPCS.TB_T_CED_SEND_RECEIVING';

			INSERT INTO @TB_T_LOG_D
			SELECT @pid
				,NULL
				,'MPCS00002INF'
				,'INF'
				,@log
				,'SendReceivingData'
				,'SYSTEM'
				,GETDATE()

			--Update Finish Flag
			UPDATE TB_T_CED_SEND_RECEIVING
			SET FINISH_FLAG = 'Y'
				,FINISH_DATE = GETDATE()

			INSERT INTO #TB_List_Message (
				Result
				,Message
				)
			SELECT 'Success' Result
				,'Manifest No updated successfully' Message

		END
		
		SET @log = 'Finish [Send Receiving data]';

		INSERT INTO @TB_T_LOG_D
		SELECT @pid
			,NULL
			,'MPCS00002INF'
			,'INF'
			,@log
			,'SendReceivingData'
			,'SYSTEM'
			,GETDATE()

				
		SELECT TOP 1 @R_SEQ_NO = SEQUENCE_NUMBER
		FROM TB_R_LOG_D
		WHERE PROCESS_ID = @pid
		ORDER BY SEQUENCE_NUMBER DESC

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT PROCESS_ID
			,@R_SEQ_NO + ROW_NUMBER() OVER (
				ORDER BY PROCESS_ID
				)
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
		FROM @TB_T_LOG_D

		UPDATE TB_R_LOG_H
		SET PROCESS_STATUS = 1
		WHERE PROCESS_ID = @pid

		SELECT *
		FROM #TB_List_Message

		
	END TRY
	BEGIN CATCH
		SET @log = 'Finish With Error [Send Receiving data]';

		INSERT INTO @TB_T_LOG_D
		SELECT @pid
			,NULL
			,'MPCS00002INF'
			,'INF'
			,@log
			,'SendReceivingData'
			,'SYSTEM'
			,GETDATE()

		SELECT TOP 1 @R_SEQ_NO = SEQUENCE_NUMBER
		FROM TB_R_LOG_D
		WHERE PROCESS_ID = @pid
		ORDER BY SEQUENCE_NUMBER DESC

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT PROCESS_ID
			,@R_SEQ_NO + ROW_NUMBER() OVER (
				ORDER BY PROCESS_ID
				)
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
		FROM @TB_T_LOG_D

		UPDATE TB_R_LOG_H
		SET PROCESS_STATUS = 6
		WHERE PROCESS_ID = @pid

		DECLARE @ErrorMessage NVARCHAR(4000);

		SET @ErrorMessage = ERROR_MESSAGE();

		INSERT INTO #TB_List_Message (
				Result
				,Message
				)
			SELECT 'Failed'
				,'Exception : ' + @ErrorMessage;

				
		SELECT *
		FROM #TB_List_Message
	END CATCH

	IF OBJECT_ID('tempdb..#TB_List_ManifestNo') IS NOT NULL
		DROP TABLE #TB_List_ManifestNo

	IF OBJECT_ID('tempdb..#TB_List_KanbanType') IS NOT NULL
		DROP TABLE #TB_List_KanbanType

	IF OBJECT_ID('tempdb..#TB_List_ManifestReceiving') IS NOT NULL
		DROP TABLE #TB_List_ManifestReceiving

	IF OBJECT_ID('tempdb..#TB_List_CreatedBy') IS NOT NULL
		DROP TABLE #TB_List_CreatedBy

	IF OBJECT_ID('tempdb..#TB_List_ProcessId') IS NOT NULL
		DROP TABLE #TB_List_ProcessId

	IF OBJECT_ID('tempdb..#tb_t_list_final') IS NOT NULL
		DROP TABLE #tb_t_list_final

	IF OBJECT_ID('tempdb..#TB_List_Message') IS NOT NULL
		DROP TABLE #TB_List_Message
	
END

