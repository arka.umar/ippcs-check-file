-- =============================================
-- Author:		FID.Ridwan
-- Create date: 2019-04-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_CED030100B_GetDeliveryCtlManifest]
AS
BEGIN
	SELECT DELIVERY_NO
		,DOCK_CD
		,STATION
		,MANIFEST_NO
		,MANIFEST_TYPE
		,ISNULL(CONVERT(VARCHAR,[SYNC_FLAG]),'0') [SYNC_FLAG]
	FROM TB_T_CED_DELIVERY_CTL_MANIFEST
END

