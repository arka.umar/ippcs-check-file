-- =============================================
-- Author:      wot.agunga (agungpanduan.com)
-- Create date: 2020-10-06
-- Description: validation when uploading master build out
-- =============================================
ALTER PROCEDURE [dbo].[SP_PackingIndicatorMasterkUploadValidate]
    @process_id varchar(20)
AS
BEGIN TRY
    SET NOCOUNT ON;

    DECLARE @continue char(1) = 'Y'
    , @functionId CHAR(5) = '71016'
    , @InValidCOunt int= 0
    , @message varchar(2000)
    , @emsg VARCHAR(MAX) = ''
    , @@sql varchar(max) = null
    , @@linkedsvr varchar(50)=''
    , @@linkeddb varchar(50)=''
    , @pid BIGINT = 0
    ;

    set @pid = convert(bigint, @process_id);

    -- just ignore empty rows
    DELETE FROM TB_T_PACKING_INDICATOR_TEMP
    WHERE PROCESS_ID = @process_id
      and nullif(DOCK_CD        ,'') is null
      and nullif(VALID_DT_FR    ,'') is null
      and nullif(PACKING_TYPE_CD,'') is null
      and nullif(PART_COLOR_SFX ,'') is null
      and nullif(MAT_NO         ,'') is null
      and nullif(PROD_PURPOSE_CD,'') is null
      and nullif(SOURCE_TYPE    ,'') is null;

    DECLARE @I INT = 0;
    WHILE @I<1
    BEGIN
        SET @I = 1;
        IF(not EXISTS (SELECT top 1 1 FROM [TB_T_PACKING_INDICATOR_TEMP] P WHERE P.PROCESS_ID = @process_id))
        BEGIN
            SET @message='No Data Found'
            BREAK;
        END;

        UPDATE T SET
            MSG = CASE WHEN NULLIF(DOCK_CD,'')  IS NULL THEN  'DOCK_CD must be filled. ' ELSE '' END
                + CASE WHEN NULLIF(VALID_DT_FR,'')  IS NULL THEN  'VALID_DT_FR must be filled. ' ELSE ''END
                + CASE WHEN NULLIF(PACKING_TYPE_CD,'')  IS NULL THEN  'PACKING_TYPE_CD must be filled. ' ELSE ''END
                + CASE WHEN NULLIF(PART_COLOR_SFX,'')  IS NULL THEN  'PART_COLOR_SFX must be filled. ' ELSE ''END
                + CASE WHEN NULLIF(MAT_NO,'')  IS NULL THEN  'MAT_NO must be filled. ' ELSE ''END
                + CASE WHEN NULLIF(PROD_PURPOSE_CD,'')  IS NULL THEN  'PROD_PURPOSE_CD must be filled. ' ELSE ''END
                + CASE WHEN NULLIF(SOURCE_TYPE,'')  IS NULL THEN  'SOURCE_TYPE must be filled. ' ELSE ''END
                + CASE WHEN dbo.tryconvertdate(VALID_DT_FR) IS NULL THEN 'VALID_DT_FR must be date format dd.MM.yyyy' else '' end
        FROM TB_T_PACKING_INDICATOR_TEMP t
        WHERE PROCESS_ID = @process_id

         UPDATE t SET MSG= COALESCE(MSG,'')
            + 'Dock Code ' + t.DOCK_CD + ' Not Register in Dock Master. '
        from TB_T_PACKING_INDICATOR_TEMP  t
        left JOIN TB_M_DOCK D ON D.DOCK_CD = T.DOCK_CD
        WHERE t.PROCESS_ID = @process_id AND NULLIF(t.MSG,'') IS NULL and d.DOCK_CD is null;

        IF (select object_id('tempdb..#tb_t_mat')) IS NOT NULL
        begin
            DROP TABLE #tb_t_mat;
        end;

        create TABLE #tb_t_mat (
          mat_no VARCHAR(23) NOT NULL
        , mat_desc varchar(40)
        , e int default(1)
        , id int identity(1,1)
        ,  PRIMARY KEY(mat_no, e));

        with x (servername, dbname) as (
            select
              SERVERNAME= coalesce((SELECT SYSTEM_VALUE FROM [dbo].[TB_M_SYSTEM] WHERE FUNCTION_ID = 'LINKED_SERVER' AND SYSTEM_CD = 'NEW_ICS_DB')
                  ,'[BB071_DB]')
            , DBNAME = COALESCE((SELECT top 1 SYSTEM_VALUE FROM [dbo].[TB_M_SYSTEM] WHERE  FUNCTION_ID = 'DB_NAME'AND SYSTEM_CD = 'ICS_DB_NAME')
                  ,'[BB071_DB]')
        )
        select @@linkedsvr=SERVERNAME, @@linkeddb=dbname
        from x;

        insert #tb_t_mat (mat_no, e)
        SELECT p.MAT_NO, e=0 fROM tb_t_packing_indicator_temp p
        where PROCESS_ID=@process_id AND NULLIF(msg,'') is null
        group by p.mat_no;

        DECLARE @@mid1 int = 1, @@mid2 INT = -1, @@midi int = 300, @@mj int
         , @@mit int = 0, @@mat_nos varchar(max) = '';

        select @@mj = count(1) from #tb_t_mat;

        while @@mid1 <= @@mj
        begin
            set @@mid2 = @@mid1 + @@midi-1;
            set @@mit = @@mit + 1;
            select @@mat_nos= stuff((select CONVERT(VARCHAR(MAX),',''') + mat_no +''''
            from (
            select m.MAT_NO  from #tb_t_mat m
            where id between @@mid1 and @@mid2
            and e = 0
            )  x
            for xml path('')), 1,1,'');

            select @@sql = Convert(varchar(max), '') +
                'SELECT MAT_NO,MAT_DESC FROM OPENQUERY('
                + @@linkedsvr+ ',
                ''  select mat_no,mat_desc
                    from ' + @@linkeddb + '.dbo.TB_M_MATERIAL m
                    where m.MAT_NO IN (
                    ' + REPLACE(coalesce( nullif( @@mat_Nos,'') , ''''''), '''', '''''') + '
                    ) AND COALESCE(DELETION_FLAG,''''N'''') = ''''N'''';
                    '')';
            insert into #tb_t_mat (mat_no, mat_desc)
            exec (@@sql);
            set @@sql = null;
            set @@mid1 = @@mid1+ @@midi;
        end;

        delete from #tb_t_mat where e = 0;

        UPDATE T set msg = coalesce(msg, '')
        + 'Material No "' + t.mat_no + '" is not found or has Deletion Flag in MM Master.'
        from TB_T_PACKING_INDICATOR_TEMP t
        left join #tb_t_mat m
        on m.mat_no = t.mat_no
        where m.mat_no is null;

        UPDATE t SET MSG= COALESCE(MSG,'')
            + CASE WHEN x.SEQ < t.SEQ THEN 'Duplicate with row ' + COALESCE(convert(varchar(15),x.SEQ),'') ELSE '' END
        from TB_T_PACKING_INDICATOR_TEMP  t
        JOIN (
        SELECT DOCK_CD, VALID_DT_FR, VALID_DT_TO, PACKING_TYPE_CD, PART_COLOR_SFX, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, SEQ =MIN(SEQ)
        FROM TB_T_PACKING_INDICATOR_TEMP
        WHERE PROCESS_ID = @process_id
        GROUP BY  DOCK_CD, VALID_DT_FR, VALID_DT_TO, PACKING_TYPE_CD, PART_COLOR_SFX, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE
        ) X ON x.DOCK_CD = t.DOCK_CD
           AND x.VALID_DT_FR = t.VALID_DT_FR
           AND x.VALID_DT_TO = t.VALID_DT_TO
           AND x.PACKING_TYPE_CD = t.PACKING_TYPE_CD
           AND x.PART_COLOR_SFX = t.PART_COLOR_SFX
           AND x.MAT_NO = t.MAT_NO
           AND x.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
           AND x.SOURCE_TYPE = t.SOURCE_TYPE
         WHERE t.PROCESS_ID = @process_id;

         UPDATE t SET MSG= COALESCE(MSG,'')
            + CASE WHEN x.SEQ < t.SEQ THEN 'Duplicate but not same PACKING_TYPE with row ' + COALESCE(convert(varchar(15),x.SEQ),'') ELSE '' END
        from TB_T_PACKING_INDICATOR_TEMP  t
        JOIN (
        SELECT DOCK_CD, VALID_DT_FR, VALID_DT_TO, PART_COLOR_SFX, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE, SEQ =MIN(SEQ)
        FROM TB_T_PACKING_INDICATOR_TEMP
        WHERE PROCESS_ID = @process_id AND NULLIF(MSG,'') IS NULL
        GROUP BY  DOCK_CD, VALID_DT_FR, VALID_DT_TO, PART_COLOR_SFX, MAT_NO, PROD_PURPOSE_CD, SOURCE_TYPE
        ) X ON x.DOCK_CD = t.DOCK_CD
           AND x.VALID_DT_FR = t.VALID_DT_FR
           AND x.VALID_DT_TO = t.VALID_DT_TO
           AND x.PART_COLOR_SFX = t.PART_COLOR_SFX
           AND x.MAT_NO = t.MAT_NO
           AND x.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
           AND x.SOURCE_TYPE = t.SOURCE_TYPE
         WHERE t.PROCESS_ID = @process_id;

        UPDATE t SET MSG= COALESCE(MSG,'')
            + 'Already exists. '
        from TB_T_PACKING_INDICATOR_TEMP  t
        JOIN TB_M_PACKING_INDICATOR x
          ON x.MAT_NO = t.MAT_NO
         AND x.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
         AND x.VALID_DT_FR = dbo.TRYCONVERTDATE( t.VALID_DT_FR)
         AND x.DOCK_CD = t.DOCK_CD
         AND x.SOURCE_TYPE = t.SOURCE_TYPE
         AND x.PART_COLOR_SFX = t.PART_COLOR_SFX
         WHERE t.PROCESS_ID = @process_id
         AND NULLIF(t.MSG,'') IS NULL;

        UPDATE t SET MSG= COALESCE(MSG,'')
            + '"Valid date from" must be later than current valid date. '
        from TB_T_PACKING_INDICATOR_TEMP  t
        JOIN TB_M_PACKING_INDICATOR x
          ON x.MAT_NO = t.MAT_NO
         AND x.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
         AND x.DOCK_CD = t.DOCK_CD
         AND x.SOURCE_TYPE = t.SOURCE_TYPE
         AND x.PART_COLOR_SFX = t.PART_COLOR_SFX
         WHERE t.PROCESS_ID = @process_id AND NULLIF(t.MSG,'') IS NULL
         AND x.VALID_DT_FR > dbo.TRYCONVERTDATE(t.VALID_DT_FR)
         AND x.VALID_DT_TO = CONVERT(DATETIME, '9999-12-31', 121);

        SELECT top 1 @message = MSG, @continue=CASE WHEN MSG IS NOT NULL THEN 'N' ELSE 'Y' END
        FROM TB_T_PACKING_INDICATOR_TEMP V WHERE V.PROCESS_ID = @process_id AND NULLIF(MSG,'') IS NOT NULL;

        IF @continue = 'N'
        BEGIN
            BREAK;
        END;
        IF EXISTS(
            SELECT TOP 1 1
            FROM TB_M_PACKING_INDICATOR x
            JOIN TB_T_PACKING_INDICATOR_TEMP t
             ON x.MAT_NO = t.MAT_NO
             AND x.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
             AND x.DOCK_CD = t.DOCK_CD
             AND x.SOURCE_TYPE = t.SOURCE_TYPE
             AND x.PART_COLOR_SFX = t.PART_COLOR_SFX
             WHERE t.PROCESS_ID = @process_id AND NULLIF(t.MSG,'') IS NULL
             AND x.VALID_DT_TO = CONVERT(DATETIME, '9999-12-31', 121)
             AND DBO.tryconvertdate(t.VALID_DT_FR) > x.VALID_DT_FR
        )
        BEGIN
            -- invalidate old data
            UPDATE X SET VALID_DT_TO = DATEADD(DAY, -1,DBO.tryconvertdate(t.VALID_DT_FR)), CHANGED_BY = T.CREATED_BY, CHANGED_DT = GETDATE()
             FROM TB_M_PACKING_INDICATOR x
            JOIN TB_T_PACKING_INDICATOR_TEMP t
             ON x.MAT_NO = t.MAT_NO
             AND x.PROD_PURPOSE_CD = t.PROD_PURPOSE_CD
             AND x.DOCK_CD = t.DOCK_CD
             AND x.SOURCE_TYPE = t.SOURCE_TYPE
             AND x.PART_COLOR_SFX = t.PART_COLOR_SFX
             WHERE t.PROCESS_ID = @process_id AND NULLIF(t.MSG,'') IS NULL
             AND x.VALID_DT_TO = CONVERT(DATETIME, '9999-12-31', 121)
             AND DBO.tryconvertdate(t.VALID_DT_FR) > x.VALID_DT_FR
        END;
        --- make new data
        INSERT INTO [dbo].[TB_M_PACKING_INDICATOR] (
          [DOCK_CD]
        , [VALID_DT_FR]
        , [PACKING_TYPE_CD]
        , [VALID_DT_TO]
        , [PART_COLOR_SFX]
        , [MAT_NO]
        , [PROD_PURPOSE_CD]
        , [SOURCE_TYPE]
        , [CREATED_BY]
        , [CREATED_DT] )
        SELECT
          t.[DOCK_CD]
        , dbo.tryconvertdate(t.valid_dt_fr)
        , t.[PACKING_TYPE_CD]
        , dbo.tryconvertdate(t.valid_dt_to)
        , t.[PART_COLOR_SFX]
        , t.[MAT_NO]
        , t.[PROD_PURPOSE_CD]
        , t.[SOURCE_TYPE]
        , t.[CREATED_BY]
        , GETDATE()
        FROM [TB_T_PACKING_INDICATOR_TEMP] t
        WHERE PROCESS_ID = @process_id and NULLIF(MSG,'') IS NULL
        and not exists(
            select top 1 1 from TB_M_PACKING_INDICATOR
            where mat_no = T.mat_no
            and PROD_PURPOSE_CD=t.PROD_PURPOSE_CD
            and VALID_DT_FR= dbo.tryconvertdate(t.valid_dt_fr)
            and DOCK_CD=t.DOCK_CD
            and SOURCE_TYPE=t.SOURCE_TYPE
            and PART_COLOR_SFX=t.PART_COLOR_SFX);

        SET @message ='success';

        BREAK;
    END;
    SELECT @message M;
END TRY
BEGIN CATCH
    declare @msgid VARCHAR(20) = 'MPCS11000ERR';
    select @msgid  ;

    if nullif(@emsg,'') IS NULL
    begin
        SELECT  @emsg = coalesce(ERROR_MESSAGE(), '') +
                ' @line:' + coalesce( CONVERT(VARCHAR(15), ERROR_LINE()), '') +
                ' ' + coalesce(CONVERT(VARCHAR(100), ERROR_PROCEDURE()),'') +
                ' err#:' + coalesce(CONVERT(VARCHAR(15), ERROR_NUMBER()), '')
                + case when NULLIF(@@sql,'') is not null THEN
                  char(13)+char(10) + ';' + char(13) +char(10) + coalesce(@@sql, '')
                  ELSE '' END ;

    end

    EXEC PutLog @what= @emsg, @user=@functionId, @where='SP_PackingIndicatorMasterkUploadValidate', @pid= @pid OUTPUT,
        @id=@msgid, @sts=1
    SELECT @emsg E;
END CATCH;