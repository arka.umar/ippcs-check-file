/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Invoice posting
*/

CREATE PROCEDURE [dbo].[SP_DLV_POSTING_INVOICE] 
--@GRID VARCHAR (MAX),
@CREATED_BY VARCHAR (20),
@pid  BIGINT,
@REVERSED VARCHAR(1) = 'N'
AS

DECLARE @CHECK_INV VARCHAR(MAX)

BEGIN
--DEKLARASI MODUL DAN FUNCTION
	DECLARE @MODUL_ID INT, @FUNCTION_ID VARCHAR (6)
	DECLARE @MULTI_DELIVERY INT = 0;
	DECLARE @COUNT_DELIVERY INT = 0;
	DECLARE @CALC_JOURNAL DECIMAL(16,2) = 0;
--,@pid AS BIGINT = 0;
	SET @MODUL_ID = 3;
    SET @FUNCTION_ID = '31406'; 
--END ---------------------------
	
--CREATE LOG 1 ---------------------------------------------------
	 EXEC dbo.sp_PutLog 'Posting Process Started', @CREATED_BY, 'SP_DLV_POSTING_INVOICE.init', @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
--LOG 1 ----------------------------------------------------------

--BEGIN POSTING
CREATE TABLE #TEMP(
			LP_INV_NO VARCHAR(MAX),
			INV_DT VARCHAR(MAX),
			LP_CD VARCHAR(MAX),
			STATUS VARCHAR(MAX),
			SEQ_NO INT
		);
INSERT INTO #TEMP 
SELECT 
	LP_INV_NO,
	INV_DT,
	LP_CD,
	STATUS_CD,
	MAX(SEQ_NO)
FROM 
	TB_R_DLV_INV_UPLOAD 
WHERE 
	PROCESS_ID = @pid
	AND
	STATUS_CD NOT IN ('-3')
GROUP BY 
	LP_INV_NO,
	INV_DT,
	LP_CD, 
	STATUS_CD

		----==Temporary==--

		---- 2. == VARIABLE TABLE TEMPORARY UNTUK EXPLODING GRID ==
		--DECLARE @ROWS TABLE(ID INT,ITEM VARCHAR(MAX));
		--DECLARE @MIN_ROW INT;
		--DECLARE @ROW_DETAIL VARCHAR(MAX);
		--DECLARE @FIELDS TABLE(ID INT,ITEM VARCHAR(MAX));
		--DECLARE @MIN_FIELD INT;
		---- ================================================================
		---- 3. == Variabel Untuk Menyimpan Data Ke TABLE DETAIL ==
		--DECLARE @arr_1 VARCHAR(50),@arr_2 VARCHAR(10), @arr_3 VARCHAR(20), @arr_4 VARCHAR(20);
			
		--INSERT INTO @ROWS
		--SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@GRID,';');
		---- ================================================================
		---- 6.2. == Ambil Nilai terkecil dari VARIABEL TABLE @ROW ==
		--SET @MIN_ROW = (SELECT MIN(ID) FROM @ROWS);

		--WHILE @MIN_ROW IS NOT NULL BEGIN
		--			-- 6.3.1 == Hapus VARIABLE TABLE @FIELDS jika ada ==
		--			DELETE FROM @FIELDS;
					
		--			SET @ROW_DETAIL = (SELECT ITEM FROM @ROWS WHERE ID = @MIN_ROW);
		--			INSERT INTO @FIELDS
		--			SELECT * FROM dbo.FN_DLV_ATD_EXPLODE(@ROW_DETAIL,'|');
		--			-- ========================================================================================
		--			-- 6.3.3 == Ambil ITEM Berdasarkan ID untuk dimasukkan kedalam VARIABLE FIELD Detail ==
		--			SET @arr_1 = (SELECT ITEM FROM @FIELDS WHERE ID = 1);
		--			SET @arr_2 = (SELECT ITEM FROM @FIELDS WHERE ID = 2);
		--			SET @arr_3 = (SELECT ITEM FROM @FIELDS WHERE ID = 3);
		--			SET @arr_4 = (SELECT ITEM FROM @FIELDS WHERE ID = 4);
					
		--			INSERT INTO #TEMP VALUES (@arr_1,@arr_2,@arr_3,@arr_4)

		--			SET @MIN_ROW = (SELECT TOP 1 ID FROM @ROWS WHERE ID > @MIN_ROW ORDER BY ID ASC);
		--			-- =================================================================================
		--END 
		--END WHILE		
		--==Temporary==--
		
		--CREATE LOG 2 ---------------------------------------------------
			 EXEC dbo.sp_PutLog 'Checking Invoice Posting', @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
		--LOG 2 ----------------------------------------------------------
		
		--CHECK INVOICE CREATED
		DECLARE @CHECK_CREATED VARCHAR(20)
		SET @CHECK_CREATED =(SELECT COUNT(LP_INV_NO) FROM #TEMP WHERE STATUS ='2');
		IF (@CHECK_CREATED <> 0) BEGIN -- INCLUDE STATUS CREATED NOT ACCESS
		
			--CREATE LOG 3 ---------------------------------------------------
			 EXEC dbo.sp_PutLog 'Posting Failed, Invoice Status is Created', @CREATED_BY, 'SP_DLV_POSTING_INVOICE.finish', @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
			--LOG 3 ----------------------------------------------------------
			-- UPDATE QUEUE -----
			RAISERROR('Posting Failed, Invoice Status is Created',16,1)
			
			 UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;		
		END
		ELSE BEGIN
			--CHECKING INVOICE POSTED
			DECLARE @CHECK_POSTED VARCHAR(20)
			SET @CHECK_POSTED =(SELECT COUNT(LP_INV_NO) FROM #TEMP WHERE STATUS ='4');
			
			IF (@CHECK_POSTED <> 0) BEGIN --INCLUDE STATUS POSTED NOT ACCCES
				--CREATE LOG 4 ---------------------------------------------------
				 EXEC dbo.sp_PutLog 'Posting Failed, Invoice Status is Posted', @CREATED_BY, 'SP_DLV_POSTING_INVOICE.finish', @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
				--LOG 4 ----------------------------------------------------------
				-- UPDATE QUEUE -----
				RAISERROR('Posting Failed, Invoice Status is Posted',16,1)
				
				 UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
						
			END
			ELSE BEGIN
				--CHECKING INVOICE PAID
				DECLARE @CHECK_PAID VARCHAR(20)
				SET @CHECK_PAID =(SELECT COUNT(LP_INV_NO) FROM #TEMP WHERE STATUS ='5');
				IF (@CHECK_PAID <> 0) BEGIN --INCLUDE STATUS PAID NOT ACCCES
					--CREATE LOG 5 ---------------------------------------------------
					 EXEC dbo.sp_PutLog 'Posting Failed, Invoice Status is Paid', @CREATED_BY, 'SP_DLV_POSTING_INVOICE.finish', @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
					--LOG 5 ----------------------------------------------------------
					-- UPDATE QUEUE -----
					RAISERROR('Posting Failed, Invoice Status is Paid',16,1)
					
					UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT=GETDATE()WHERE PROCESS_ID = @pid;
					
				END
				ELSE BEGIN
						BEGIN TRY
							--START CURSOR 1 --LOOPING BERDASARKAN JUMLAH INVOICE NO YANG DIPILIH
							--=======================================================================================
							DECLARE @LP_INV_NO VARCHAR(MAX), @INV_DT VARCHAR(MAX), @LP_CD VARCHAR(MAX)

							DECLARE BTMP_CUR_1 CURSOR
							FOR
							SELECT LP_INV_NO, INV_DT, LP_CD FROM #TEMP;

							OPEN BTMP_CUR_1;
							FETCH NEXT FROM BTMP_CUR_1 INTO @LP_INV_NO,@INV_DT, @LP_CD;

							WHILE @@FETCH_STATUS = 0
							BEGIN
							
		
							--CURSOR SAVE ================================================================
								
							--== CURSOR TO LOOPING INV UPLOAD WITH ONE ROUTE ==--
							--FIND ME
							
							
											
							--EXECUTED POSTING =========================================
							
							----== GENERATE DOC NO ==---
							DECLARE @DOC_NO VARCHAR(MAX),@DOC_NUMBER VARCHAR(10), @INV_NO VARCHAR(MAX),@INV_NUMBER VARCHAR(10)
							SET @DOC_NO=(SELECT TOP 1 DOC_NO FROM TB_R_DLV_FI_TRANS_H 
									WHERE DOC_NO NOT LIKE 'CS%' AND DOC_NO NOT LIKE 'TWX%' 
											AND SUBSTRING(DOC_NO, 1, 6) = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2) + dbo.FN_VARMONTH(GETDATE()) + dbo.FN_VARDAY(GETDATE())
									ORDER BY DOC_NO DESC);
							IF  @DOC_NO IS NULL BEGIN
								SET @DOC_NUMBER = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2) + dbo.FN_VARMONTH(GETDATE()) + dbo.FN_VARDAY(GETDATE()) + '0001';
							END
							ELSE BEGIN
								DECLARE @SEQS2 VARCHAR(4),@ADDS2 VARCHAR(4),@DIGIT2 VARCHAR(4)
								SET @SEQS2 = (SELECT SUBSTRING(@DOC_NO, 8, LEN(@DOC_NO)))
								SET @ADDS2 = (SELECT CAST(CAST(@SEQS2 as INT)+1 AS VARCHAR(11)))
								SET @DIGIT2 ='0000'
							
								SET @DOC_NUMBER = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2) + dbo.FN_VARMONTH(GETDATE()) + dbo.FN_VARDAY(GETDATE()) + (SELECT SUBSTRING(@DIGIT2, 1, LEN(@DIGIT2)-LEN(@ADDS2))+@ADDS2);			
								
							END
							SET @INV_NUMBER = @DOC_NUMBER
							--==Generate INV_NO ==---
							--DECLARE @INV_NO VARCHAR(MAX),@INV_NUMBER VARCHAR(10)
							--SET @INV_NO=(SELECT TOP 1 INV_NO FROM TB_R_DLV_INV_H ORDER BY INV_NO DESC);
							--IF  @INV_NO IS NULL BEGIN
							--SET @INV_NUMBER = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2) + dbo.FN_VARMONTH(GETDATE()) + dbo.FN_VARDAY(GETDATE()) + '0001';
							--END
							--ELSE BEGIN
							--DECLARE @SEQS VARCHAR(4),@ADDS VARCHAR(4),@DIGIT VARCHAR(4)
							--SET @SEQS = (SELECT SUBSTRING(@INV_NO, 8, LEN(@INV_NO)))
							--SET @ADDS = (SELECT CAST(CAST(@SEQS as INT)+1 AS VARCHAR(11)))
							--SET @DIGIT ='0000'
							
							--SET @INV_NUMBER = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2) + dbo.FN_VARMONTH(GETDATE()) + dbo.FN_VARDAY(GETDATE()) + (SELECT SUBSTRING(@DIGIT, 1, LEN(@DIGIT)-LEN(@ADDS))+@ADDS);			

							--END
							
							--CREATE LOG 6 ---------------------------------------------------
							DECLARE @log6 AS VARCHAR(MAX)
							SET @log6 = 'Create Invoice No ' + @INV_NUMBER;
							EXEC dbo.sp_PutLog @log6, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
							--LOG 6 ----------------------------------------------------------
													
							--== END GENERATE INV NO ==---
							
							----== GENERATE DOC NO ==---
							--DECLARE @DOC_NO VARCHAR(MAX),@DOC_NUMBER VARCHAR(10)
							--SET @DOC_NO=(SELECT TOP 1 DOC_NO FROM TB_R_DLV_FI_TRANS_H WHERE DOC_NO NOT LIKE 'CS%' ORDER BY DOC_NO DESC);
							--IF  @DOC_NO IS NULL BEGIN
							--	SET @DOC_NUMBER = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2) + dbo.FN_VARMONTH(GETDATE()) + dbo.FN_VARDAY(GETDATE()) + '0001';
							--END
							--ELSE BEGIN
							--	DECLARE @SEQS2 VARCHAR(4),@ADDS2 VARCHAR(4),@DIGIT2 VARCHAR(4)
							--	SET @SEQS2 = (SELECT SUBSTRING(@DOC_NO, 8, LEN(@DOC_NO)))
							--	SET @ADDS2 = (SELECT CAST(CAST(@SEQS2 as INT)+1 AS VARCHAR(11)))
							--	SET @DIGIT2 ='0000'
							
							--	SET @DOC_NUMBER = SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 3, 2) + dbo.FN_VARMONTH(GETDATE()) + dbo.FN_VARDAY(GETDATE()) + (SELECT SUBSTRING(@DIGIT2, 1, LEN(@DIGIT2)-LEN(@ADDS2))+@ADDS2);			
							
							--END
						
							  --CREATE LOG 7 ---------------------------------------------------
								DECLARE @log7 AS VARCHAR(MAX)
								SET @log7 = 'Create Doc No ' + @DOC_NUMBER;
								EXEC dbo.sp_PutLog @log7, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
								--LOG 7 ----------------------------------------------------------

								--=====================---
							
								------===INSERT INV HEADER ==-----	
							--CREATE LOG 8 ---------------------------------------------------
							DECLARE @log8 AS VARCHAR(MAX)
							SET @log8 = 'Insert To Table Invoice Header';
							EXEC dbo.sp_PutLog @log8, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
							--LOG 8 ----------------------------------------------------------
							
							IF (@REVERSED = 'N')
							BEGIN											
								INSERT INTO TB_R_DLV_INV_H
														   (INV_NO ,INV_POSTING_DT ,INV_DT ,LP_INV_NO ,LP_CD ,PAYMENT_CURR ,EXCHANGE_RATE ,INV_AMT ,INV_AMT_LOCAL_CURR
														   ,TAX_AMT ,ASSIGNMENT ,HEADER_TEXT ,INV_TEXT ,BASELINE_DT ,PAYMENT_TERM_CD ,PAYMENT_METHOD_CD
														   ,PARTNER_BANK_KEY ,CALC_TAX_FLAG ,REVERSE_FLAG ,TAX_DT ,LIV_BASED ,BALANCE ,INV_DESC ,CREATED_BY
														   ,CREATED_DT ,CHANGED_BY ,CHANGED_DT ,PAY_PLAN_DT ,PAY_ACT_DT ,ACCT_DOC_NO ,ACCT_DOC_YEAR ,SUBMIT_BY ,SUBMIT_DT, PROCESS_ID, INV_NOTES)
															
								SELECT 
								@INV_NUMBER, MAX(INV_POSTING_DT),MAX(INV_DT), LP_INV_NO, MAX(LP_CD),MAX(PAYMENT_CURR),--NULL,
								(SELECT DISTINCT PO_EXCHANGE_RATE FROM TB_R_DLV_PO_H f WHERE f.PO_NO = MAX(a.PO_NO)),
								SUM(a.INV_TAX_AMT), SUM(a.INV_AMT_LOCAL_CURR), (SUM(a.INV_TAX_AMT) - SUM(a.INV_AMT))
								, NULL,MAX(a.INV_TEXT),
								MAX(a.INV_TEXT),MAX(a.BASELINE_DT), MAX(a.PAYMENT_TERM_CD),MAX(a.PAYMENT_METHOD_CD),MAX(PARTNER_BANK_KEY),
								NULL, --calc tax flag diambil dari mana?
								0,MAX(a.INV_TAX_DT), NULL, NULL, NULL, @CREATED_BY, GETDATE(), NULL, NULL, MAX(a.PAY_PLAN_DT),NULL,
								NULL, NULL, MAX(a.SUBMIT_BY), MAX(a.SUBMIT_DT), @pid, MAX(INV_NOTES)
								FROM TB_R_DLV_INV_UPLOAD a 
								WHERE a.LP_INV_NO = @LP_INV_NO AND a.CONDITION_CATEGORY ='H' AND STATUS_CD NOT IN ('-3') 
								--AND a.DELIVERY_NO = @DELIVERY_NO AND ROUTE_CD =@ROUTE_CD
								GROUP BY a.LP_INV_NO
								--=======================-----
							
							
								----===INSERT INV DETAIL ==-----
								--CREATE LOG 9 ---------------------------------------------------
									DECLARE @log9 AS VARCHAR(MAX)
									SET @log9 = 'Insert To Table Invoice Detail';
									EXEC dbo.sp_PutLog @log9, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
								--LOG 9 ----------------------------------------------------------


								INSERT INTO TB_R_DLV_INV_D (INV_NO,INV_DOC_ITEM,PO_NO,PO_ITEM_NO,COMP_PRICE_CD,SERVICE_DOC_NO,
															ROUTE_CD,DELIVERY_NO,DOC_DT,QTY,PRICE_AMT,INV_MT,INV_AMT_LOCAL_CURR,
															CONDITION_CATEGORY,TAX_CD,UOM,CREATED_BY,CREATED_DT
											)

											SELECT
															@INV_NUMBER,ROW_NUMBER() OVER(ORDER BY PO_ITEM_NO,DELIVERY_NO),PO_NO,PO_ITEM_NO,COMP_PRICE_CD,SERVICE_DOC_NO,
															NULL,DELIVERY_NO,DOC_DT,QTY,PRICE_AMT,QTY*PRICE_AMT,QTY*PRICE_AMT,
															CONDITION_CATEGORY,INV_TAX_CD,'RET',@CREATED_BY,GETDATE()
								FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND STATUS_CD NOT IN ('-3') ORDER BY PO_ITEM_NO,DELIVERY_NO
								--=======================-----
							END
							
							----===INSERT FL TRANS H===-----
							--CREATE LOG 10 ---------------------------------------------------
								DECLARE @log10 AS VARCHAR(MAX)
								SET @log10 = 'Insert To Table FI Trans Header ' + (SELECT CASE WHEN @REVERSED = 'Y' THEN '(REVERSED)' ELSE '' END);
								EXEC dbo.sp_PutLog @log10, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
							--LOG 10 ----------------------------------------------------------

							SET @COUNT_DELIVERY = 0;
							----===INSERT FL TRANS H===-----
							INSERT INTO TB_R_DLV_FI_TRANS_H (DOC_NO,DOC_YEAR,DOC_DT,POSTING_DT,DOC_HEADER_TXT,REFF_NO,
															 PAYMENT_TERM_CD,LP_CD,CUSTOMER_CD,PAYMENT_METHOD_CD,PARTNER_BANK_KEY,
															 ACCT_DOC_NO,ACCT_DOC_YEAR,HEADER_TYPE,CREATED_BY,CREATED_DT,PROCESS_ID
										)

							SELECT TOP 1
															 @DOC_NUMBER,
															 --YEAR(DOC_DT),
															 YEAR(GETDATE()),
															 DOC_DT,GETDATE(),INV_TEXT,@LP_INV_NO,
															 PAYMENT_TERM_CD,LP_CD,NULL,PAYMENT_METHOD_CD,PARTNER_BANK_KEY,NULL,NULL,NULL,
															 @CREATED_BY,GETDATE(),@pid
							FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND CONDITION_CATEGORY = 'H' AND STATUS_CD NOT IN ('-3')
							ORDER BY SEQ_NO DESC
							--FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = 'ALSIN00001' AND CONDITION_CATEGORY ='H'
							
							------=======================-----
							--CREATE LOG 11 ---------------------------------------------------
							DECLARE @log11 AS VARCHAR(MAX)
							SET @log11 = 'Insert To Table FI Trans Detail ' + (SELECT CASE WHEN @REVERSED = 'Y' THEN '(REVERSED)' ELSE '' END);
							EXEC dbo.sp_PutLog @log11, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
							--LOG 11 ----------------------------------------------------------
							
							----===INSERT FL TRANS DETAIL ==-----
							DECLARE @TRANS_TYPE VARCHAR(MAX);
							
							IF (@REVERSED = 'Y')
								SET @TRANS_TYPE =(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD ='DEL_CAN_TRANS_TYPE' AND FUNCTION_ID ='31405');
							ELSE
								SET @TRANS_TYPE =(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD ='DEL_TRANS_TYPE' AND FUNCTION_ID ='31405');
							
							SET @MULTI_DELIVERY = ((SELECT COUNT(1) FROM TB_R_DLV_INV_UPLOAD u WHERE u.LP_INV_NO = @LP_INV_NO AND STATUS_CD NOT IN ('-3') AND u.SEQ_NO = (SELECT MAX(si.SEQ_NO) FROM TB_R_DLV_INV_UPLOAD si WHERE si.STATUS_CD NOT IN ('-3') AND si.LP_INV_NO = @LP_INV_NO))/3);
							
							INSERT INTO TB_R_DLV_FI_TRANS_D (DOC_NO,DOC_YEAR,DOC_ITEM_NO,CONDITION_TYPE,GL_ACCOUNT,
															 ROUTE_CD,DOC_QTY,DOC_AMT,DOC_CURR_CD,LOCAL_AMT,LOCAL_CURR_CD,PO_NO,
															 TAX_CD,EXCHANGE_RATE,COST_CENTER_CD,ITEM_TEXT,TRANSACTION_NO,UOM,
															 CREATED_BY,CREATED_DT
										)
													
							SELECT 
								@DOC_NUMBER, YEAR(DOC_DT), ROW_NUMBER() OVER(ORDER BY PO_ITEM_NO,DELIVERY_NO), b.CONDITION_TYPE, 
								d.GL_ACCOUNT, NULL, a.QTY, a.PRICE_AMT, a.CURR_CD, a.QTY * a.PRICE_AMT, a.CURR_CD,
								(CASE WHEN A.CONDITION_CATEGORY = 'H' THEN a.PO_NO ELSE NULL END ),
								(CASE WHEN b.DEBIT_CREDIT_IND = 'D' 
														THEN 
															(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DEFAULT_TAX_CODE') 
														ELSE NULL END),
								(SELECT DISTINCT PO_EXCHANGE_RATE FROM TB_R_DLV_PO_H f WHERE f.PO_NO = a.PO_NO),
								(CASE WHEN A.CONDITION_CATEGORY = 'H' 
														THEN 
															(SELECT y.SYSTEM_VALUE FROM TB_M_SYSTEM y WHERE y.SYSTEM_CD = 'COST_CENTER') 
														ELSE NULL END),
								INV_TEXT, b.TRANS_TYPE_CD, 'RET', @CREATED_BY, GETDATE() 
							FROM TB_R_DLV_INV_UPLOAD a 
								LEFT JOIN TB_M_FI_POSTING_RULE_D b ON b.CONDITION_TYPE = a.COMP_PRICE_CD
								LEFT JOIN TB_M_GL_ACCOUNT_MAPPING d ON d.POSTING_CD = b.POSTING_CD AND d.CONDITION_TYPE = b.CONDITION_TYPE
							WHERE 
								a.LP_INV_NO = @LP_INV_NO 
								AND 
								b.TRANS_TYPE_CD = @TRANS_TYPE 
								AND
								STATUS_CD NOT IN ('-3')
								AND
								a.SEQ_NO = (SELECT MAX(si.SEQ_NO) FROM TB_R_DLV_INV_UPLOAD si WHERE si.LP_INV_NO = @LP_INV_NO AND si.STATUS_CD NOT IN ('-3'))
							ORDER BY a.PO_ITEM_NO,a.DELIVERY_NO
							----=======================-----
																	
							
							--CREATE LOG 12 ---------------------------------------------------
								DECLARE @log12 AS VARCHAR(MAX)
								SET @log12 = 'Insert To Table FI Journal ' + (SELECT CASE WHEN @REVERSED = 'Y' THEN '(REVERSED)' ELSE '' END);
								EXEC dbo.sp_PutLog @log12, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
							--LOG 12 ----------------------------------------------------------


							DECLARE @DOC_ITEM_NO as int = 1 --ini penting
							DECLARE @TRANS_TYPE_CD_2 VARCHAR(MAX);
							
							IF (@REVERSED = 'Y')
								SET @TRANS_TYPE_CD_2 =(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD ='DEL_CAN_TRANS_TYPE' AND FUNCTION_ID ='31405');
							ELSE
								SET @TRANS_TYPE_CD_2 =(SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD ='DEL_TRANS_TYPE' AND FUNCTION_ID ='31405');
							
							---=== Create journal ===---
							SELECT TRANS_TYPE_CD, DEBIT_CREDIT_IND,CONDITION_TYPE, POSTING_CD INTO #tb_rule_d FROM(
								SELECT TRANS_TYPE_CD, DEBIT_CREDIT_IND, CONDITION_TYPE, POSTING_CD FROM TB_M_FI_POSTING_RULE_D
								WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD_2
							)tbl;

							DECLARE 
							@TRANS_TYPE_CD as varchar(3),
							@DEBIT_CREDIT_IND as varchar(1),
							@CONDITION_TYPE as varchar(4),
							@POSTING_CD as varchar(2),
							@GL_ACCOUNT as varchar(max),
							@INV_TEXT  as varchar(max)

							DECLARE RULE_D_CURS CURSOR 
							FOR SELECT TRANS_TYPE_CD, DEBIT_CREDIT_IND, CONDITION_TYPE, POSTING_CD from #tb_rule_d ORDER BY DEBIT_CREDIT_IND DESC;
							OPEN RULE_D_CURS;
							FETCH NEXT FROM RULE_D_CURS INTO @TRANS_TYPE_CD, @DEBIT_CREDIT_IND, @CONDITION_TYPE, @POSTING_CD
							WHILE @@FETCH_STATUS = 0  
							BEGIN
							
							SET @INV_TEXT = (SELECT MAX(INV_TEXT) FROM TB_R_DLV_INV_UPLOAD WHERE STATUS_CD NOT IN ('-3') AND LP_INV_NO = @LP_INV_NO AND CONDITION_CATEGORY = 'H' AND SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND STATUS_CD NOT IN ('-3')))
							SET @GL_ACCOUNT = (SELECT gl.GL_ACCOUNT FROM TB_M_GL_ACCOUNT_MAPPING gl WHERE gl.POSTING_CD = @POSTING_CD AND gl.CONDITION_TYPE = @CONDITION_TYPE)
								
								IF NOT EXISTS(SELECT TOP 1 LP_INV_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND COMP_PRICE_CD = @CONDITION_TYPE AND STATUS_CD NOT IN ('-3'))
								BEGIN
									INSERT INTO TB_R_DLV_FI_JOURNAL (DOC_NO, DOC_YEAR, DOC_ITEM, DOC_DT, TRANSACTION_CD, CONDITION_TYPE, DEBIT_CREDIT_INDICATOR, GL_ACCOUNT, ROUTE_CD, QTY, 
																	 AMT_DOC, DOC_CURR, AMT_LOCAL, LOCAL_CURR, PO_NO, TAX_CD, EXCHANGE_RATE, COST_CENTER_CD, DOC_TYPE, GL_BALANCE_FLAG, 
																	 WBS_NO, UOM, ITEM_TEXT, CREATED_BY, CREATED_DT, PROCESS_ID)
									SELECT 
										@DOC_NUMBER AS DOC_NO,
										YEAR(GETDATE()) AS DOC_YEAR,
										@DOC_ITEM_NO AS DOC_ITEM,
										GETDATE() AS DOC_DT,
										@TRANS_TYPE_CD AS TRANSACTION_CD,
										@CONDITION_TYPE AS CONDITION_TYPE,
										@DEBIT_CREDIT_IND AS DEBIT_CREDIT_INDICATOR,
										(CASE 
											WHEN ISNULL(@GL_ACCOUNT,'') = '' THEN (SELECT VENDOR_SAP_ID FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = U.LP_CD) 
										 ELSE @GL_ACCOUNT END) AS GL_ACCOUNT,
										 NULL AS ROUTE_CD,
										 NULL AS QTY,
										 CASE 
											WHEN @DEBIT_CREDIT_IND = 'C' THEN 
												CASE WHEN (((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))))) > 0 THEN
												((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))))*(-1) 
												ELSE ((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END)))) END
											ELSE 
												CASE WHEN ((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END)))) < 0 THEN
												(SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))) * (-1)
												ELSE (SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))) END
										 END AS AMT_DOC,
										 U.PAYMENT_CURR AS DOC_CURR,
										 CASE 
											WHEN @DEBIT_CREDIT_IND = 'C' THEN 
												CASE WHEN (((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))))) > 0 THEN
												((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))))*(-1) 
												ELSE ((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END)))) END
											ELSE 
												CASE WHEN ((SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END)))) < 0 THEN
												(SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))) * (-1)
												ELSE (SUM((CASE WHEN RD.DEBIT_CREDIT_IND = 'C' THEN U.PRICE_AMT * (-1) ELSE U.PRICE_AMT END))) END
										 END AS AMT_LOCAL,
										 U.PAYMENT_CURR AS LOCAL_CURR,
										 NULL AS PO_NO,
										 NULL AS TAX_CD,
										1 AS EXCHANGE_RATE,
										NULL AS COST_CENTER_CD,
										--(SELECT DOC_TYPE FROM TB_M_FI_POSTING_RULE_H WHERE TRANS_TYPE_CD = TRANS_TYPE_CD) as DOC_TYPE,
										'RE' as DOC_TYPE,
										NULL as GL_BALANCE_FLAG,
										NULL as WBS_NO,
										'RET' as UOM,
										@INV_TEXT AS ITEM_TEXT,
										@CREATED_BY AS CREATED_BY,
										GETDATE() AS CREATED_DT,
										@pid AS PROCESS_ID
									FROM TB_R_DLV_INV_UPLOAD U
										LEFT JOIN TB_M_FI_POSTING_RULE_D RD ON RD.TRANS_TYPE_CD = @TRANS_TYPE_CD_2 AND RD.CONDITION_TYPE = U.COMP_PRICE_CD 
									WHERE U.LP_INV_NO = @LP_INV_NO AND U.COMP_PRICE_CD IN (
										SELECT CONDITION_TYPE FROM TB_M_FI_POSTING_RULE_D
										WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD_2 AND CONDITION_TYPE NOT IN (SELECT @CONDITION_TYPE)
									)AND STATUS_CD NOT IN ('-3') 
									GROUP BY U.PAYMENT_CURR, U.LP_CD, U.PO_NO
								END ELSE BEGIN
									INSERT INTO TB_R_DLV_FI_JOURNAL (DOC_NO, DOC_YEAR, DOC_ITEM, DOC_DT, TRANSACTION_CD, CONDITION_TYPE, DEBIT_CREDIT_INDICATOR, GL_ACCOUNT, ROUTE_CD, QTY, 
																	 AMT_DOC, DOC_CURR, AMT_LOCAL, LOCAL_CURR, PO_NO, TAX_CD, EXCHANGE_RATE, COST_CENTER_CD, DOC_TYPE, GL_BALANCE_FLAG, 
																	 WBS_NO, UOM, ITEM_TEXT, CREATED_BY, CREATED_DT, PROCESS_ID)
									SELECT 
										@DOC_NUMBER AS DOC_NO,
										YEAR(GETDATE()) AS DOC_YEAR,
										@DOC_ITEM_NO AS DOC_ITEM,
										GETDATE() AS DOC_DT,
										@TRANS_TYPE_CD AS TRANSACTION_CD,
										@CONDITION_TYPE AS CONDITION_TYPE,
										@DEBIT_CREDIT_IND AS DEBIT_CREDIT_INDICATOR,
										(CASE 
											WHEN ISNULL(@GL_ACCOUNT,'') = '' THEN (SELECT VENDOR_SAP_ID FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = U.LP_CD) 
										 ELSE @GL_ACCOUNT END) AS GL_ACCOUNT,
										 NULL AS ROUTE_CD,
										 NULL AS QTY,
										 CASE 
											WHEN @DEBIT_CREDIT_IND = 'C' THEN (SUM(U.PRICE_AMT))*(-1) 
											ELSE SUM(U.PRICE_AMT)
										 END AS AMT_DOC,
										 U.PAYMENT_CURR AS DOC_CURR,
										 CASE 
											WHEN @DEBIT_CREDIT_IND = 'C' THEN (SUM(U.PRICE_AMT))*(-1) 
											ELSE SUM(U.PRICE_AMT)
										 END AS AMT_LOCAL,
										 U.PAYMENT_CURR AS LOCAL_CURR,
										 CASE WHEN U.CONDITION_CATEGORY = 'H' THEN U.PO_NO ELSE NULL END AS PO_NO,
										 CASE 
											WHEN @DEBIT_CREDIT_IND = 'D' THEN (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DEFAULT_TAX_CODE') 
										ELSE NULL END AS TAX_CD,
										1 AS EXCHANGE_RATE,
										CASE WHEN U.CONDITION_CATEGORY = 'H' THEN (SELECT TOP 1 SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'COST_CENTER') ELSE NULL END AS COST_CENTER_CD,
										--(SELECT DOC_TYPE FROM TB_M_FI_POSTING_RULE_H WHERE TRANS_TYPE_CD = TRANS_TYPE_CD) as DOC_TYPE,
										'RE' as DOC_TYPE,
										NULL as GL_BALANCE_FLAG,
										NULL as WBS_NO,
										'RET' as UOM,
										@INV_TEXT AS ITEM_TEXT,
										@CREATED_BY AS CREATED_BY,
										GETDATE() AS CREATED_DT,
										@pid AS PROCESS_ID
									FROM TB_R_DLV_INV_UPLOAD U
									WHERE U.LP_INV_NO = @LP_INV_NO AND U.COMP_PRICE_CD = @CONDITION_TYPE AND STATUS_CD NOT IN ('-3')
									GROUP BY U.PAYMENT_CURR, U.LP_CD, U.CONDITION_CATEGORY, U.PO_NO
								END							
								
								SET @DOC_ITEM_NO = @DOC_ITEM_NO + 1 --ini penting

							FETCH NEXT FROM RULE_D_CURS INTO @TRANS_TYPE_CD, @DEBIT_CREDIT_IND, @CONDITION_TYPE, @POSTING_CD
							END;
							CLOSE RULE_D_CURS;
							DEALLOCATE RULE_D_CURS;
							drop table #tb_rule_d;
							---=== End create journal ===---
							
							--SELECT 
							--DOC_HEADER_TXT, 
							--LP_INV_NO,
							--DOC_YEAR, 
							--DOC_DT,
							--POSTING_DT, 
							--REFF_NO, 
							--PAYMENT_TERM_CD,
							--ROUTE_CD,
							--LP_CD,
							--CUSTOMER_CD, 
							--PAYMENT_METHOD_CD, 
							--PARTNER_BANK_KEY, 
							--ACCT_DOC_NO, 
							--ACCT_DOC_YEAR, 
							--HEADER_TYPE,
							--PO_NO,
							--QTY
							--into #loop_upl
							--FROM
							--(
							--	SELECT 
							--	DISTINCT upl.DELIVERY_NO as DOC_HEADER_TXT,
							--	upl.LP_INV_NO as LP_INV_NO,
							--	YEAR(upl.DOC_DT) as DOC_YEAR, 
							--	upl.DOC_DT,
							--	GETDATE() as POSTING_DT,
							--	upl.SERVICE_DOC_NO+'-'+CAST(YEAR(upl.DOC_DT) as varchar(max)) as REFF_NO,
							--	--poh.PAYMENT_TERM_CD,
							--	poh.PAYMENT_TERM_CD as PAYMENT_TERM_CD,
							--	upl.ROUTE_CD,
							--	upl.LP_CD,
							--	NULL as CUSTOMER_CD,
							--	--poh.PAYMENT_METHOD_CD,
							--	poh.PAYMENT_METHOD_CD as PAYMENT_METHOD_CD,
							--	--1 as PARTNER_BANK_KEY,
							--	NULL as PARTNER_BANK_KEY,
							--	NULL as ACCT_DOC_NO,
							--	NULL as ACCT_DOC_YEAR,
							--	NULL as HEADER_TYPE,
							--	upl.PO_NO,
							--	QTY
							--	FROM TB_R_DLV_INV_UPLOAD upl
							--	LEFT JOIN TB_R_DLV_PO_H poh ON poh.PO_NO = upl.PO_NO
							--	WHERE LP_INV_NO = @LP_INV_NO AND CONDITION_CATEGORY='H' AND STATUS_CD IN ('3', '-4', '-5')
							--)tbl;

							--DECLARE 
							--@DOC_HEADER_TXT as varchar(25),
							--@LP_INV_NO2 as varchar(max),
							--@DOC_YEAR2 as varchar(4),
							--@DOC_DT as date, 
							--@POSTING_DT as datetime, 
							--@REFF_NO as varchar(16), 
							--@PAYMENT_TERM_CD as varchar(4), 
							--@ROUTE_CD2 as varchar(4),
							--@LP_CD2 as varchar(4), 
							--@CUSTOMER_CD as varchar(10), 
							--@PAYMENT_METHOD_CD as varchar(4), 
							--@PARTNER_BANK_KEY as varchar(10), 
							--@ACCT_DOC_NO as varchar(10), 
							--@ACCT_DOC_YEAR as varchar(4), 
							--@HEADER_TYPE as varchar(2),
							--@PO_NO2 as varchar(max),
							--@QTY as int

							--DECLARE UPL CURSOR 
							--FOR 
							--SELECT 
							--DOC_HEADER_TXT,
							--LP_INV_NO,
							--DOC_YEAR, 
							--DOC_DT,
							--POSTING_DT, 
							--REFF_NO, 
							--PAYMENT_TERM_CD, 
							--ROUTE_CD,
							--LP_CD, 
							--CUSTOMER_CD, 
							--PAYMENT_METHOD_CD, 
							--PARTNER_BANK_KEY, 
							--ACCT_DOC_NO, 
							--ACCT_DOC_YEAR, 
							--HEADER_TYPE,
							--PO_NO,
							--QTY
							--FROM #loop_upl;

							--OPEN UPL;
							--FETCH NEXT FROM UPL INTO @DOC_HEADER_TXT, @LP_INV_NO2, @DOC_YEAR2,@DOC_DT,@POSTING_DT,@REFF_NO, @PAYMENT_TERM_CD,@ROUTE_CD2, @LP_CD2,@CUSTOMER_CD,@PAYMENT_METHOD_CD,@PARTNER_BANK_KEY,@ACCT_DOC_NO,@ACCT_DOC_YEAR,@HEADER_TYPE, @PO_NO2, @QTY
							--WHILE @@FETCH_STATUS = 0  
							--BEGIN  
								---DETAIL---
								
								--SELECT TRANS_TYPE_CD, DEBIT_CREDIT_IND,CONDITION_TYPE, POSTING_CD INTO #tb_rule_d FROM(
								--	SELECT TRANS_TYPE_CD, DEBIT_CREDIT_IND, CONDITION_TYPE, POSTING_CD FROM TB_M_FI_POSTING_RULE_D
								--	WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD_2
								--)tbl;
								--DECLARE 
								--@TRANS_TYPE_CD as varchar(3),
								--@DEBIT_CREDIT_IND as varchar(1),
								--@CONDITION_TYPE as varchar(4),
								--@POSTING_CD as varchar(2)
								
								--DECLARE RULE_D_CURS CURSOR 
								--FOR SELECT TRANS_TYPE_CD, DEBIT_CREDIT_IND, CONDITION_TYPE, POSTING_CD from #tb_rule_d ORDER BY DEBIT_CREDIT_IND DESC;
								--OPEN RULE_D_CURS;
								--FETCH NEXT FROM RULE_D_CURS INTO @TRANS_TYPE_CD, @DEBIT_CREDIT_IND, @CONDITION_TYPE, @POSTING_CD
								--WHILE @@FETCH_STATUS = 0  
								--BEGIN 
									
								--	SELECT PRICE_AMT, CURR_CD, SEQ_NO INTO #UPL FROM(SELECT PRICE_AMT, CURR_CD, SEQ_NO FROM TB_R_DLV_INV_UPLOAD WHERE DELIVERY_NO = @DOC_HEADER_TXT AND COMP_PRICE_CD=@CONDITION_TYPE AND LP_INV_NO = @LP_INV_NO2 AND SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2))tbl;
								--	SELECT SUMS INTO #UPL2 FROM(SELECT SUM(PRICE_AMT) as SUMS FROM TB_R_DLV_INV_UPLOAD WHERE DELIVERY_NO = @DOC_HEADER_TXT AND LP_INV_NO = @LP_INV_NO2 AND COMP_PRICE_CD IN (SELECT CONDITION_TYPE FROM TB_M_FI_POSTING_RULE_D WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD) AND SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2))tbl;
								--	SELECT GL_ACCOUNT INTO #GL FROM (SELECT gl.GL_ACCOUNT FROM TB_M_GL_ACCOUNT_MAPPING gl WHERE gl.POSTING_CD = @POSTING_CD AND gl.CONDITION_TYPE = @CONDITION_TYPE)tbl;
								--	DECLARE @GL_ACCOUNT as varchar(max) = (SELECT GL_ACCOUNT FROM #GL)
								--	DECLARE @SUMS as numeric(15,2) = (SELECT SUMS FROM #UPL2)
								--	DECLARE @PRICE_AMT as numeric(15,2) = (SELECT PRICE_AMT FROM #UPL)
								--	DECLARE @DOC_CURR_CD as varchar(3) = (SELECT TOP 1 CURR_CD FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2 AND DELIVERY_NO = @DOC_HEADER_TXT AND SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2))
								--	DECLARE @TAX_CD as varchar(2) = (SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD = 'DEFAULT_TAX_CODE')
								--	DECLARE @EXCHANGE_RATE as varchar(max) = (SELECT PO_EXCHANGE_RATE FROM TB_R_DLV_PO_H WHERE PO_NO = @PO_NO2)
								--	DECLARE @COST_CENTER_CD as varchar(max) = (SELECT y.SYSTEM_VALUE FROM TB_M_SYSTEM y WHERE y.SYSTEM_CD = 'COST_CENTER')
								--	DECLARE @INV_TEXT  as varchar(max) = (SELECT MAX(INV_TEXT) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2 AND CONDITION_CATEGORY = 'H' AND DELIVERY_NO = @DOC_HEADER_TXT AND SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2))
								--	DECLARE @CONDITION_CATEGORY as varchar(1)  = (SELECT CONDITION_CATEGORY FROM TB_R_DLV_INV_UPLOAD WHERE @LP_INV_NO2 = @LP_INV_NO2 AND COMP_PRICE_CD = @CONDITION_TYPE AND DELIVERY_NO = @DOC_HEADER_TXT AND SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2))
								--	--CREATE LOG 12 ---------------------------------------------------
								--		DECLARE @log12Z AS VARCHAR(MAX)
								--		SET @log12Z = 'Insert 3';
								--		EXEC dbo.sp_PutLog @log12Z, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
								--	--LOG 12 ----------------------------------------------------------
									
								--	--JOURNAL--
								--	IF (@CONDITION_TYPE = 'ZSPC')
								--	BEGIN
									
								--		SET @COUNT_DELIVERY = @COUNT_DELIVERY + 1
								--		IF ((@MULTI_DELIVERY > 1) AND (@COUNT_DELIVERY < @MULTI_DELIVERY))
								--		BEGIN
											
								--			IF (@PRICE_AMT IS NULL)
								--			BEGIN
								--				SET @CALC_JOURNAL = @CALC_JOURNAL + @SUMS
								--			END ELSE
								--			BEGIN
								--				SET @CALC_JOURNAL = @CALC_JOURNAL + @PRICE_AMT
								--			END
								--		END ELSE
								--		BEGIN
								--			IF (@MULTI_DELIVERY > 1)
								--			BEGIN
											
								--				--IF (@PRICE_AMT IS NULL)
								--				--BEGIN
								--				--	SET @CALC_JOURNAL = @CALC_JOURNAL + @SUMS
								--				--END ELSE
								--				--BEGIN
								--				--	SET @CALC_JOURNAL = @CALC_JOURNAL + @PRICE_AMT
								--				--END
												
								--				SET @CALC_JOURNAL = (SELECT SUM(CASE WHEN D.DEBIT_CREDIT_IND = 'C' THEN (U.PRICE_AMT*(-1)) ELSE U.PRICE_AMT END) AS PRICE_AMT FROM TB_R_DLV_INV_UPLOAD U
								--										INNER JOIN TB_M_FI_POSTING_RULE_D D ON D.TRANS_TYPE_CD = @TRANS_TYPE_CD AND D.CONDITION_TYPE = U.COMP_PRICE_CD
								--									WHERE 
								--									U.LP_INV_NO = @LP_INV_NO2 
								--									AND U.COMP_PRICE_CD IN (SELECT CONDITION_TYPE FROM TB_M_FI_POSTING_RULE_D WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD) 
								--									AND U.SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO2)
								--									)

												
								--				INSERT INTO TB_R_DLV_FI_JOURNAL (DOC_NO, DOC_YEAR, DOC_ITEM, DOC_DT, TRANSACTION_CD, CONDITION_TYPE, DEBIT_CREDIT_INDICATOR, GL_ACCOUNT, ROUTE_CD, QTY, AMT_DOC, DOC_CURR, AMT_LOCAL, LOCAL_CURR, PO_NO, TAX_CD, EXCHANGE_RATE, COST_CENTER_CD, DOC_TYPE, GL_BALANCE_FLAG, WBS_NO, UOM, ITEM_TEXT, CREATED_BY, CREATED_DT, PROCESS_ID)
								--				SELECT
								--				@DOC_NUMBER, 
								--				@DOC_YEAR2, 
								--				@DOC_ITEM_NO, 
								--				@DOC_DT,
								--				@TRANS_TYPE_CD_2,
								--				@CONDITION_TYPE,
								--				@DEBIT_CREDIT_IND,
								--				(CASE WHEN ISNULL(@GL_ACCOUNT,'') = '' THEN (SELECT VENDOR_SAP_ID FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = @LP_CD2) ELSE @GL_ACCOUNT END),
								--				NULL,--@ROUTE_CD2,
								--				@QTY,
												
								--				CASE 
								--					WHEN @PRICE_AMT IS NULL THEN 
								--												CASE 
								--													WHEN @DEBIT_CREDIT_IND = 'C' THEN @CALC_JOURNAL * (-1) 
								--													ELSE @CALC_JOURNAL 
								--												END 
								--					ELSE 
								--						CASE
								--							WHEN @DEBIT_CREDIT_IND = 'C' THEN @CALC_JOURNAL * (-1) 
								--							ELSE @CALC_JOURNAL
								--						END
								--				END ,
												
								--				@DOC_CURR_CD,
												
								--				CASE 
								--					WHEN @PRICE_AMT IS NULL THEN 
								--											CASE 
								--												WHEN @DEBIT_CREDIT_IND = 'C' THEN @CALC_JOURNAL * (-1) 
								--												ELSE @CALC_JOURNAL 
								--											END 
								--					ELSE 
								--						CASE
								--							WHEN @DEBIT_CREDIT_IND = 'C' THEN @CALC_JOURNAL * (-1) 
								--							ELSE @CALC_JOURNAL
								--						END
								--				END,
												
								--				@DOC_CURR_CD,
								--				CASE WHEN @CONDITION_CATEGORY = 'H' THEN @PO_NO2 ELSE NULL END,
								--				CASE WHEN @DEBIT_CREDIT_IND = 'D' THEN @TAX_CD ELSE NULL END,
								--				@EXCHANGE_RATE,
								--				CASE WHEN @CONDITION_CATEGORY = 'H' THEN @COST_CENTER_CD ELSE NULL END,
								--				(SELECT DOC_TYPE FROM TB_M_FI_POSTING_RULE_H WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD_2) as DOC_TYPE,
								--				NULL as GL_BALANCE_FLAG,
								--				NULL as WBS_NO,
								--				'RET' as UOM,
								--				@INV_TEXT,
								--				@CREATED_BY,
								--				GETDATE(),
								--				@pid
								--			END ELSE
								--			BEGIN
								--				INSERT INTO TB_R_DLV_FI_JOURNAL (DOC_NO, DOC_YEAR, DOC_ITEM, DOC_DT, TRANSACTION_CD, CONDITION_TYPE, DEBIT_CREDIT_INDICATOR, GL_ACCOUNT, ROUTE_CD, QTY, AMT_DOC, DOC_CURR, AMT_LOCAL, LOCAL_CURR, PO_NO, TAX_CD, EXCHANGE_RATE, COST_CENTER_CD, DOC_TYPE, GL_BALANCE_FLAG, WBS_NO, UOM, ITEM_TEXT, CREATED_BY, CREATED_DT, PROCESS_ID)
								--				SELECT
								--				@DOC_NUMBER, 
								--				@DOC_YEAR2, 
								--				@DOC_ITEM_NO, 
								--				@DOC_DT,
								--				@TRANS_TYPE_CD_2,
								--				@CONDITION_TYPE,
								--				@DEBIT_CREDIT_IND,
								--				(CASE WHEN ISNULL(@GL_ACCOUNT,'') = '' THEN (SELECT VENDOR_SAP_ID FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = @LP_CD2) ELSE @GL_ACCOUNT END),
								--				NULL,--@ROUTE_CD2,
								--				@QTY,
												
								--				CASE 
								--					WHEN @PRICE_AMT IS NULL THEN 
								--												CASE 
								--													WHEN @DEBIT_CREDIT_IND = 'C' THEN @SUMS*(-1) 
								--													ELSE @SUMS 
								--												END 
								--					ELSE 
								--						CASE
								--							WHEN @DEBIT_CREDIT_IND = 'C' THEN @PRICE_AMT*(-1) 
								--							ELSE @PRICE_AMT
								--						END
								--				END ,
												
								--				@DOC_CURR_CD,
												
								--				CASE 
								--					WHEN @PRICE_AMT IS NULL THEN 
								--											CASE 
								--												WHEN @DEBIT_CREDIT_IND = 'C' THEN @SUMS*(-1) 
								--												ELSE @SUMS 
								--											END 
								--					ELSE 
								--						CASE
								--							WHEN @DEBIT_CREDIT_IND = 'C' THEN @PRICE_AMT*(-1) 
								--							ELSE @PRICE_AMT
								--						END
								--				END,
												
								--				@DOC_CURR_CD,
								--				CASE WHEN @CONDITION_CATEGORY = 'H' THEN @PO_NO2 ELSE NULL END,
								--				CASE WHEN @DEBIT_CREDIT_IND = 'D' THEN @TAX_CD ELSE NULL END,
								--				@EXCHANGE_RATE,
								--				CASE WHEN @CONDITION_CATEGORY = 'H' THEN @COST_CENTER_CD ELSE NULL END,
								--				(SELECT DOC_TYPE FROM TB_M_FI_POSTING_RULE_H WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD_2) as DOC_TYPE,
								--				NULL as GL_BALANCE_FLAG,
								--				NULL as WBS_NO,
								--				'RET' as UOM,
								--				@INV_TEXT,
								--				@CREATED_BY,
								--				GETDATE(),
								--				@pid
								--			END
								--		END
								--	END ELSE
								--	BEGIN
								--		INSERT INTO TB_R_DLV_FI_JOURNAL (DOC_NO, DOC_YEAR, DOC_ITEM, DOC_DT, TRANSACTION_CD, CONDITION_TYPE, DEBIT_CREDIT_INDICATOR, GL_ACCOUNT, ROUTE_CD, QTY, AMT_DOC, DOC_CURR, AMT_LOCAL, LOCAL_CURR, PO_NO, TAX_CD, EXCHANGE_RATE, COST_CENTER_CD, DOC_TYPE, GL_BALANCE_FLAG, WBS_NO, UOM, ITEM_TEXT, CREATED_BY, CREATED_DT, PROCESS_ID)
								--		SELECT
								--		@DOC_NUMBER, 
								--		@DOC_YEAR2, 
								--		@DOC_ITEM_NO, 
								--		@DOC_DT,
								--		@TRANS_TYPE_CD_2,
								--		@CONDITION_TYPE,
								--		@DEBIT_CREDIT_IND,
								--		(CASE WHEN ISNULL(@GL_ACCOUNT,'') = '' THEN (SELECT VENDOR_SAP_ID FROM TB_M_LOGISTIC_PARTNER WHERE LOG_PARTNER_CD = @LP_CD2) ELSE @GL_ACCOUNT END),
								--		NULL,--@ROUTE_CD2,
								--		@QTY,
										
								--		CASE 
								--			WHEN @PRICE_AMT IS NULL THEN 
								--										CASE 
								--											WHEN @DEBIT_CREDIT_IND = 'C' THEN @SUMS*(-1) 
								--											ELSE @SUMS 
								--										END 
								--			ELSE 
								--				CASE
								--					WHEN @DEBIT_CREDIT_IND = 'C' THEN @PRICE_AMT*(-1) 
								--					ELSE @PRICE_AMT
								--				END
								--		END ,
										
								--		@DOC_CURR_CD,
										
								--		CASE 
								--			WHEN @PRICE_AMT IS NULL THEN 
								--									CASE 
								--										WHEN @DEBIT_CREDIT_IND = 'C' THEN @SUMS*(-1) 
								--										ELSE @SUMS 
								--									END 
								--			ELSE 
								--				CASE
								--					WHEN @DEBIT_CREDIT_IND = 'C' THEN @PRICE_AMT*(-1) 
								--					ELSE @PRICE_AMT
								--				END
								--		END,
										
								--		@DOC_CURR_CD,
								--		CASE WHEN @CONDITION_CATEGORY = 'H' THEN @PO_NO2 ELSE NULL END,
								--		CASE WHEN @DEBIT_CREDIT_IND = 'D' THEN @TAX_CD ELSE NULL END,
								--		@EXCHANGE_RATE,
								--		CASE WHEN @CONDITION_CATEGORY = 'H' THEN @COST_CENTER_CD ELSE NULL END,
								--		(SELECT DOC_TYPE FROM TB_M_FI_POSTING_RULE_H WHERE TRANS_TYPE_CD = @TRANS_TYPE_CD_2) as DOC_TYPE,
								--		NULL as GL_BALANCE_FLAG,
								--		NULL as WBS_NO,
								--		'RET' as UOM,
								--		@INV_TEXT,
								--		@CREATED_BY,
								--		GETDATE(),
								--		@pid
								--	END
									
								--	SET @DOC_ITEM_NO = @DOC_ITEM_NO + 1 --ini penting
									
								--	DROP TABLE #UPL
								--	DROP TABLE #UPL2
								--	DROP TABLE #GL
									
								
								
								--FETCH NEXT FROM RULE_D_CURS INTO @TRANS_TYPE_CD, @DEBIT_CREDIT_IND, @CONDITION_TYPE, @POSTING_CD
								--END;
								--CLOSE RULE_D_CURS;
								--DEALLOCATE RULE_D_CURS;
								--drop table #tb_rule_d;
								
								
								
							--FETCH NEXT FROM UPL INTO @DOC_HEADER_TXT, @LP_INV_NO2, @DOC_YEAR2,@DOC_DT,@POSTING_DT,@REFF_NO, @PAYMENT_TERM_CD,@ROUTE_CD2, @LP_CD2,@CUSTOMER_CD,@PAYMENT_METHOD_CD,@PARTNER_BANK_KEY,@ACCT_DOC_NO,@ACCT_DOC_YEAR,@HEADER_TYPE, @PO_NO2, @QTY
							--END;
							--CLOSE UPL;
							--DEALLOCATE UPL;
							--drop table #loop_upl;

							--== END CURSOR ==--------------
								
							--END CURSOR SAVE FI TRANS DETAIL ===========================================================

							--======END CURSOR LOOPING=====================================================================================================
							
							----UPDATE SETATUS-===============================
								IF (@REVERSED = 'N')
								BEGIN
									--UPDATE INVOICE NO IN TB_INV_UPLOAD====
									UPDATE TB_R_DLV_INV_UPLOAD 
										SET INV_NO = (SELECT INV_NO FROM TB_R_DLV_INV_H WHERE INV_NO = @INV_NUMBER),
											STATUS_CD = '4',
											IN_PROGRESS = '1',
											REVERSE_FLAG = NULL,
											INV_POSTING_BY = @CREATED_BY,
											INV_POSTING_DT = GETDATE(),
											PROCESS_ID = @pid
									WHERE 
										LP_INV_NO = @LP_INV_NO 
										AND 
										STATUS_CD NOT IN ('-3')
										AND 
										SEQ_NO = (SELECT MAX(SEQ_NO) FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO = @LP_INV_NO AND STATUS_CD NOT IN ('-3')) 
									--END UPDATE======
								
									--=====UPDATE STATUS INV=======--
									--UPDATE TB_R_DLV_INV_UPLOAD SET STATUS_CD='4',IN_PROGRESS ='1',REVERSE_FLAG = NULL,
									--INV_POSTING_BY = @CREATED_BY,INV_POSTING_DT = GETDATE(),PROCESS_ID = @pid
									--WHERE LP_INV_NO = @LP_INV_NO
									----=======================-----
								
									--UPDATE TBR_DLV_GR_IR-------
									UPDATE TB_R_DLV_GR_IR SET STATUS_CD ='4' WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO =@LP_INV_NO AND STATUS_CD NOT IN ('-3'))
								END
								
								--Empty Process ID=========================================================================================================
								UPDATE TB_R_DLV_INV_UPLOAD SET PROCESS_ID = NULL WHERE LP_INV_NO = @LP_INV_NO AND STATUS_CD NOT IN ('-3') -- INV_UPLOAD
								UPDATE TB_R_DLV_INV_H SET PROCESS_ID = NULL WHERE LP_INV_NO = @LP_INV_NO -- INV_H
								UPDATE TB_R_DLV_FI_TRANS_H SET PROCESS_ID = NULL WHERE DOC_NO = @DOC_NUMBER --FI_TRANS_H
								UPDATE TB_R_DLV_FI_JOURNAL SET PROCESS_ID = NULL WHERE DOC_NO = @DOC_NUMBER --FI_JOURNAL
								--=========================================================================================================================
										
												
							--EXECUTED POSTING =========================================
							
							
							FETCH NEXT FROM BTMP_CUR_1 INTO
							@LP_INV_NO,@INV_DT, @LP_CD
							END;
							CLOSE BTMP_CUR_1;
							DEALLOCATE BTMP_CUR_1;
							
								


								UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3',
								PROCESS_END_DT=GETDATE()
								WHERE PROCESS_ID = @pid;
								
								--CREATE LOG 98 ---------------------------------------------------
								DECLARE @log98 AS VARCHAR(MAX)
								SET @log98 = 'Posting Success';
								EXEC dbo.sp_PutLog @log98, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00004INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
								--end-----
								
																
								--CREATE LOG 100 ---------------------------------------------------
								DECLARE @log100 AS VARCHAR(MAX)
								SET @log100 = 'Posting Process Finished';
								EXEC dbo.sp_PutLog @log100, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.finish', @pid OUTPUT, 'MPCS00003INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;		
								--LOG 100 ----------------------------------------------------------
								
								
						END TRY
						
						BEGIN CATCH
							DECLARE @ERROR_MESSAGE VARCHAR(MAX)
							SET @ERROR_MESSAGE = ERROR_MESSAGE()
							SELECT ERROR_MESSAGE()
							--UPDATE QUEUE===================================================================================
							 UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4',PROCESS_END_DT=GETDATE() WHERE PROCESS_ID = @pid;
							--END UPDATE QUEUE ==============================================================================
							
							--CREATE LOG 100 ---------------------------------------------------
							 EXEC dbo.sp_PutLog @ERROR_MESSAGE, @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;		
							 EXEC dbo.sp_PutLog 'Error Posting', @CREATED_BY, 'SP_DLV_POSTING_INVOICE.process', @pid OUTPUT, 'MPCS00008ERR', 'ERR', @MODUL_ID, @FUNCTION_ID,1;		
							 
							--LOG 100 ----------------------------------------------------------
							
							RAISERROR('Posting Failed',16,1)
							--DO SOMETHING
								UPDATE TB_R_DLV_INV_UPLOAD 
									SET STATUS_CD = '-5',
										INV_POSTING_BY = @CREATED_BY,
										INV_POSTING_DT = GETDATE(),
										PROCESS_ID = @pid
								WHERE LP_INV_NO = @LP_INV_NO AND STATUS_CD IN ('3', '-4', '-5')
								
								UPDATE TB_R_DLV_GR_IR SET STATUS_CD ='-5' WHERE SERVICE_DOC_NO IN (SELECT SERVICE_DOC_NO FROM TB_R_DLV_INV_UPLOAD WHERE LP_INV_NO =@LP_INV_NO AND STATUS_CD IN ('3', '-4', '-5'))
							
								
								DELETE FROM TB_R_DLV_INV_D WHERE INV_NO IN(SELECT INV_NO FROM TB_R_DLV_INV_H WHERE PROCESS_ID = @pid)
								DELETE FROM TB_R_DLV_INV_H WHERE PROCESS_ID = @pid
								
								DELETE FROM TB_R_DLV_FI_TRANS_D WHERE DOC_NO IN(SELECT DOC_NO FROM TB_R_DLV_FI_TRANS_H WHERE PROCESS_ID = @pid)
								DELETE FROM TB_R_DLV_FI_TRANS_H WHERE PROCESS_ID = @pid
								DELETE FROM TB_R_DLV_FI_JOURNAL WHERE PROCESS_ID = @pid
								
								
							--
							
						END CATCH
				END
			END
		END
	
		--CLOSE RULE_D_CURS;
		--DEALLOCATE RULE_D_CURS;
END

