CREATE TABLE [dbo].[MAT_DOC_ICS_TEMP](
	[MAT_DOC_NO] [varchar](10) NOT NULL,
	[MAT_DOC_YEAR] [varchar](4) NOT NULL,
	[KANBAN_ORDER_NO] [varchar](16) NULL,
	[POSTING_DT] [datetime] NOT NULL,
	[DOC_DT] [datetime] NOT NULL,
	[SYSTEM_SOURCE] [varchar](20) NOT NULL,
	[TRANSACTION_TYPE] [varchar](2) NOT NULL,
	[DOC_TYPE] [varchar](2) NULL
) ON [PRIMARY]

