CREATE FUNCTION [dbo].[FN_VARYEAR]
(
	-- Add the parameters for the function here
	@DATE DATETIME
)
RETURNS VARCHAR(10)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @RETURN VARCHAR(MAX);

	-- Add the T-SQL statements to compute the return value here

	SET @RETURN = CONVERT(VARCHAR(4),YEAR(@DATE));

	-- Return the result of the function
	RETURN @RETURN;

END

