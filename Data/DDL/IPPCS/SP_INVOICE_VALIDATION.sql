-- =============================================
-- Author		:	FID.Ridwan
-- Create date	:	19-Sept-2020
-- Description	:	Invoice Validation
-- Update	: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_INVOICE_VALIDATION] @USER_ID VARCHAR(20) = 'agan'
	,@PROCESS_ID BIGINT = '202009070019'
	,@SupplierInvoiceNo VARCHAR(MAX)
	,@SupplierCode VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IsError CHAR(1) = 'N'
	DECLARE @CURRDT DATE = CAST(GETDATE() AS DATE)
	DECLARE @FUNCTION_ID AS VARCHAR(MAX) = '53002'
		,@MODULE_ID AS VARCHAR(MAX) = '5'
		,@FUNTION_NM AS VARCHAR(MAX) = 'SP Invoice Validation'
		,@MSG_TXT AS VARCHAR(MAX)
		,@MSG_TYPE AS VARCHAR(MAX)
		,@LOG_LOCATION AS VARCHAR(MAX) = 'SP Invoice Validation'
		,@L_ERR AS INT = 0
		,@N_ERR AS INT = 0
		,@PARAM1 AS VARCHAR(MAX)
		,@PARAM2 AS VARCHAR(MAX)
		,@PARAM3 AS VARCHAR(MAX)

	SET NOCOUNT ON

	BEGIN TRY
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'Invoice Validation'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		--* Step 1 : System Master Checking *--
		EXEC dbo.CommonGetMessage 'MPCS00002INF'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,'System Master Checking'

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00002INF'
			,'INF'
			,'MPCS00002INF : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		DECLARE @lv_current_im_period VARCHAR(100);
		DECLARE @lv_current_im_year VARCHAR(100);
		DECLARE @lv_previous_im_period VARCHAR(100);
		DECLARE @lv_previous_im_year VARCHAR(100);
		DECLARE @lv_posting_flag VARCHAR(100);

		SELECT @lv_current_im_period = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'CURRENT_IM_PERIOD'

		SELECT @lv_current_im_year = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'CURRENT_IM_YEAR'

		SELECT @lv_previous_im_period = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'PREVIOUS_IM_PERIOD'

		SELECT @lv_previous_im_year = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'PREVIOUS_IM_YEAR'

		SELECT @lv_posting_flag = SYSTEM_VALUE
		FROM TB_M_SYSTEM
		WHERE FUNCTION_ID = 'LIV_POSTING'
			AND SYSTEM_CD = 'ALLOW_POSTING_FLAG'

		IF (ISNULL(@lv_current_im_period, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(CURRENT_IM_PERIOD)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@lv_current_im_year, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(CURRENT_IM_YEAR)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@lv_previous_im_period, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(PREVIOUS_IM_PERIOD)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@lv_previous_im_year, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(PREVIOUS_IM_YEAR)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		IF (ISNULL(@lv_posting_flag, '') = '')
		BEGIN
			SET @IsError = 'Y';

			EXEC dbo.CommonGetMessage 'MPCS00004INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'(ALLOW_POSTING_FLAG)'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MPCS00004INF'
				,'INF'
				,'MPCS00004INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()
		END

		--* Step 2 : get value from table TB_T_INVOICE_H *--
		DECLARE @lv_inv_h_payment_curr VARCHAR(3)
			,@lv_inv_h_partner_bank VARCHAR(10)
			,@lv_inv_h_supp_cd VARCHAR(6)
			,@l_n_exchange_rate NUMERIC(13, 2)
			,@l_n_inv_amt NUMERIC(16, 2)
			,@lv_calc_tax_flag VARCHAR(1)
			,@ld_inv_h_posting_dt DATE
			,@lv_tax_cd_H VARCHAR(2)

		SELECT @lv_inv_h_payment_curr = PAYMENT_CURR
			,@lv_inv_h_partner_bank = PARTNER_BANK_KEY
			,@lv_inv_h_supp_cd = SUPP_CD
			,@l_n_exchange_rate = EXCHANGE_RATE
			,@l_n_inv_amt = INV_AMT
			,@lv_calc_tax_flag = CALC_TAX_FLAG
			,@ld_inv_h_posting_dt = INV_POSTING_DT
			,@lv_tax_cd_H = TAX_CD
		FROM TB_T_INVOICE_H
		WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
			AND PROCESS_ID = @PROCESS_ID;

		IF @IsError = 'N'
		BEGIN
			--* Step 3 : get invoice amount from gl and detail *--
			DECLARE @lv_inv_gl_acct_inv_amt NUMERIC(16, 2)

			SELECT @lv_inv_gl_acct_inv_amt = ISNULL(SUM(INV_AMT), 0)
			FROM TB_T_INV_GL_ACCOUNT
			WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
				AND PROCESS_ID = @PROCESS_ID;

			--* Step 4 : Update Invoice Amount Local Currency *--
			UPDATE d
			SET INV_AMT_LOCAL_CURR = ROUND(INV_AMT * @l_n_exchange_rate, 0)
			FROM TB_T_INVOICE_D d
			WHERE process_id = @PROCESS_ID
				AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

			--* Step 5 : Retrieve Detail Invoice Amount *--
			DECLARE @lv_inv_detail_inv_amt NUMERIC(16, 2)

			SELECT @lv_inv_detail_inv_amt = ISNULL(SUM(INV_AMT), 0)
			FROM TB_T_INVOICE_D
			WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
				AND PROCESS_ID = @PROCESS_ID;

			--* Step 6 : Set Total Amount *--
			DECLARE @l_n_tot_amt NUMERIC(16, 2);

			SET @l_n_tot_amt = @lv_inv_gl_acct_inv_amt + @lv_inv_detail_inv_amt;

			--* Step 7 : Check Open Period *--
			DECLARE @lb_isOpen CHAR(1) = 'N';
			DECLARE @lv_month_year VARCHAR(50);
			CREATE TABLE #l_n_fiscal (RESULT VARCHAR(100))

			SET @lv_month_year = RIGHT(CONVERT(VARCHAR, @ld_inv_h_posting_dt, 104), 7);

			INSERT INTO #l_n_fiscal
			EXEC SP_INVOICE_CONVERT_MONTH_PERIOD @PROCESS_ID
				,2
				,@lv_month_year
				,''
				,''

			IF (SELECT LEFT(RESULT, 1) FROM #l_n_fiscal) <> '1'
			BEGIN
				SET @IsError = 'Y';
				SELECT @PARAM1 = REPLACE(RESULT, LEFT(RESULT, 1), '') FROM #l_n_fiscal

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()
			END ELSE
			BEGIN
				DECLARE @lv_fiscal_year VARCHAR(4);
				DECLARE @lv_fiscal_period VARCHAR(2);
				DECLARE @lv_fiscal VARCHAR(10);

				SELECT DISTINCT @lv_fiscal = FISCAL FROM TB_T_RET_FISCAL WHERE PROCESS_ID = @PROCESS_ID

				SET @lv_fiscal_year   = RIGHT(@lv_fiscal, 4);
				SET @lv_fiscal_period = LEFT(@lv_fiscal, 2);

				IF @lv_fiscal_period = @lv_current_im_period
					AND @lv_fiscal_year = @lv_current_im_year
				BEGIN
					SET @lb_isOpen = 'Y';
				END
				ELSE
					IF @lv_fiscal_period = @lv_previous_im_period
						AND @lv_fiscal_year = @lv_previous_im_year
					BEGIN
						IF @lv_posting_flag = 'Y'
						BEGIN
							SET @lb_isOpen = 'Y';
						END
						ELSE
						BEGIN
							SET @lb_isOpen = 'N';
						END
					END
					ELSE
					BEGIN
						SET @lb_isOpen = 'N';
					END;

				--* Step 8. Delete Temporary Invoice Tax Table *--
				IF @lb_isOpen = 'Y'
				BEGIN
					DELETE
					FROM TB_T_INV_TAX
					WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo;
				END
				ELSE
				BEGIN
					--MICS0281AERR Period [Val1] has been close
					SET @IsError = 'Y';
					SET @PARAM1 = 'Period ' + ISNULL(CONVERT(VARCHAR, @ld_inv_h_posting_dt, 112),'') + ' has been close';

					EXEC dbo.CommonGetMessage 'MSPT00001ERR'
						,@MSG_TXT OUTPUT
						,@N_ERR OUTPUT
						,@PARAM1

					INSERT INTO TB_R_LOG_D (
						PROCESS_ID
						,SEQUENCE_NUMBER
						,MESSAGE_ID
						,MESSAGE_TYPE
						,[MESSAGE]
						,LOCATION
						,CREATED_BY
						,CREATED_DATE
						)
					SELECT @PROCESS_ID
						,(
							SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
							FROM dbo.TB_R_LOG_D
							WHERE PROCESS_ID = @PROCESS_ID
							)
						,'MSPT00001ERR'
						,'ERR'
						,'MSPT00001ERR : ' + @MSG_TXT
						,@LOG_LOCATION
						,@USER_ID
						,GETDATE()
				END
			END

			DELETE FROM TB_T_RET_FISCAL WHERE PROCESS_ID = @PROCESS_ID
			DROP TABLE #l_n_fiscal
		END

		IF @IsError = 'N'
		BEGIN
			--* Step 9. Check Duplicate Data Invoice *--
			-- catatan BEDANYA INV DT sama INV POSTING DT
			DECLARE @checkInv VARCHAR(16);
			DECLARE @checkpostingDt DATE;

			SELECT @checkInv = INV_NO
				,@checkpostingDt = POSTING_DT
			FROM TB_R_INV_H
			WHERE SUPPLIER_CD = @SupplierCode
				AND SUPP_INV_NO = @SupplierInvoiceNo

			IF (ISNULL(@checkInv, '') <> '')
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Invoice No : ' + @SupplierInvoiceNo + ' for Supplier CD : ' + @SupplierCode + ', Already Posted with Invoice Doc No : ' + @checkInv + ' and Invoice Posting Date : ' + CONVERT(VARCHAR, @checkpostingDt, 104);

				EXEC dbo.CommonGetMessage 'MPCS00018ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MPCS00004INF'
					,'INF'
					,'MPCS00004INF : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()
			END
		END

		IF @IsError = 'N'
		BEGIN
			-- MICS3127AERR Partner bank key is not valid for selected supplier code
			--* Step 10. Check Account Currency Payment *--
			IF NOT EXISTS (
					SELECT TOP 1 1
					FROM TB_M_SUPPLIER_BANK
					WHERE SUPP_CD = @lv_inv_h_supp_cd
						AND SUPP_BANK_TYPE = @lv_inv_h_partner_bank
						AND ACCOUNT_CURR = @lv_inv_h_payment_curr
					)
			BEGIN
				SET @IsError = 'Y';
				SET @PARAM1 = 'Partner bank key is not valid for selected supplier code.';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'INF'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()
			END
		END

		IF @IsError = 'N'
		BEGIN
			--* Step 11. Calculate withholding tax *--
			IF EXISTS (
					SELECT count('x')
					FROM TB_T_INV_WITHOLDING_TAX A
					INNER JOIN TB_M_WITHHOLDING_TAX B ON (A.WITHOLDING_TAX_CD = B.WITHHOLDING_TAX_CD)
					WHERE A.SUPP_INVOICE_NO = @SupplierInvoiceNo
						AND A.PROCESS_ID = @PROCESS_ID
					)
			BEGIN
				UPDATE A
				SET WITHOLDING_TAX_AMT = (CAST(ROUND(B.RATE * 1.00 / 100, 4) AS FLOAT)) * A.WITHOLDING_TAX_BASE_AMT
				FROM TB_T_INV_WITHOLDING_TAX A
				INNER JOIN TB_M_WITHHOLDING_TAX B ON (A.WITHOLDING_TAX_CD = B.WITHHOLDING_TAX_CD)
				WHERE PROCESS_ID = @PROCESS_ID
					AND SUPP_INVOICE_NO = @SupplierInvoiceNo;
			END

			--* Step 12. Check Calculate Tax Flag *--
			DECLARE @l_n_inv_d_inv_amt NUMERIC(16, 2) = 0;
			DECLARE @l_n_gl_acct_inv_amt NUMERIC(16, 2) = 0;
			DECLARE @l_n_base_amt NUMERIC(16, 2) = 0;
			DECLARE @l_n_tax_amt NUMERIC(16, 2) = 0;
			DECLARE @l_n_tot_tax_amt NUMERIC(16, 5) = 0;
			DECLARE @l_n_balance NUMERIC(16, 2) = 0;

			IF ISNULL(@lv_calc_tax_flag, '') = 'Y'
			BEGIN
				--* Step 13. Delete Tax Temp Table *--
				DELETE
				FROM TB_T_INV_TAX
				WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo;

				--* Step 14. Update Tax Cd *--
				UPDATE A
				SET TAX_CD = @lv_tax_cd_H
				FROM TB_T_INVOICE_D A
				WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
					AND PROCESS_ID = @PROCESS_ID
					AND TAX_CD IS NULL;

				UPDATE A
				SET TAX_CD = @lv_tax_cd_H
				FROM TB_T_INV_GL_ACCOUNT A
				WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
					AND PROCESS_ID = @PROCESS_ID
					AND TAX_CD IS NULL;

				--* Step 15 -> 19 calculate and insert into tb t inv tax *--
				DECLARE @CountAll INT;
				DECLARE @CnStep INT = 1;
				DECLARE @ACCOUNT_KEY VARCHAR(100);
				DECLARE @TAX_CD VARCHAR(100);
				DECLARE @RATE INT;

				SELECT ROW_NUMBER() OVER (
						ORDER BY (
								SELECT 1
								)
						) NO
					,A.ACCOUNT_KEY
					,A.TAX_CD
					,A.RATE
				INTO #TB_T_LIST_TAX
				FROM TB_R_TAX_ACCOUNT A
				INNER JOIN TB_M_ACCOUNT B ON (
						B.ACCOUNT_KEY = A.ACCOUNT_KEY
						AND ISNULL(B.DEDUCTIBLE_TAX, '') <> 'Y'
						);

				SELECT @CountAll = COUNT(1)
				FROM #TB_T_LIST_TAX

				WHILE @CnStep <= @CountAll
				BEGIN
					SELECT @ACCOUNT_KEY = ACCOUNT_KEY
						,@TAX_CD = TAX_CD
						,@RATE = RATE
					FROM #TB_T_LIST_TAX
					WHERE NO = @CnStep

					SELECT @l_n_inv_d_inv_amt = ISNULL(SUM(INV_AMT), 0)
					FROM TB_T_INVOICE_D
					WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
						AND PROCESS_ID = @PROCESS_ID
						AND ISNULL(CONDITION_CATEGORY, '') <> 'A'
						AND TAX_CD = @TAX_CD;

					SELECT @l_n_gl_acct_inv_amt = ISNULL(SUM(INV_AMT), 0)
					FROM TB_T_INV_GL_ACCOUNT
					WHERE SUPP_INVOICE_NO = @SupplierInvoiceNo
						AND PROCESS_ID = @PROCESS_ID
						AND TAX_CD = @TAX_CD;

					IF (
							@l_n_inv_d_inv_amt <> 0
							OR @l_n_gl_acct_inv_amt <> 0
							)
					BEGIN
						SET @l_n_base_amt = @l_n_inv_d_inv_amt + @l_n_gl_acct_inv_amt;
						SET @l_n_tax_amt = (CAST(ROUND(@RATE * 1.00 / 100, 4) AS FLOAT)) * @l_n_base_amt;
						SET @l_n_tot_tax_amt = @l_n_tot_tax_amt + @l_n_tax_amt;

						INSERT INTO TB_T_INV_TAX (
							PROCESS_ID
							,SUPP_INVOICE_NO
							,TAX_CD
							,TAX_AMT
							,ACCOUNT_KEY
							,LOCAL_TAX_AMT
							)
						VALUES (
							@PROCESS_ID
							,@SupplierInvoiceNo
							,@TAX_CD
							,@l_n_tax_amt
							,@ACCOUNT_KEY
							,ROUND(@l_n_exchange_rate * @l_n_tax_amt, 0)
							);

						-- remark?
						--SET @l_n_tot_tax_amt = 0;
					END

					SET @CnStep = @CnStep + 1;
				END

				DROP TABLE #TB_T_LIST_TAX
			END

			--* Step 20. Calculate Balance *--
			SET @l_n_tot_amt = @l_n_tot_amt + @l_n_tot_tax_amt;
			SET @l_n_balance = @l_n_tot_amt - @l_n_inv_amt;

			--* Step 21 Header Update *--
			UPDATE A
			SET TAX_AMT = @l_n_tot_tax_amt
				,BALANCE = @l_n_balance
				,INV_AMT = @l_n_inv_amt
				,INV_AMT_LOCAL_CURR = ROUND(@l_n_inv_amt * EXCHANGE_RATE, 0)
			FROM TB_T_INVOICE_H A
			WHERE PROCESS_ID = @PROCESS_ID
				AND SUPP_INVOICE_NO = @SupplierInvoiceNo;

			--* Step 22. Check Balance *--
			IF @l_n_balance <> 0
			BEGIN
				-- MICS3040AERR {0}
				SET @IsError = 'Y';
				SET @PARAM1 = 'Unable to post data, Balance is not 0';

				EXEC dbo.CommonGetMessage 'MSPT00001ERR'
					,@MSG_TXT OUTPUT
					,@N_ERR OUTPUT
					,@PARAM1

				INSERT INTO TB_R_LOG_D (
					PROCESS_ID
					,SEQUENCE_NUMBER
					,MESSAGE_ID
					,MESSAGE_TYPE
					,[MESSAGE]
					,LOCATION
					,CREATED_BY
					,CREATED_DATE
					)
				SELECT @PROCESS_ID
					,(
						SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
						FROM dbo.TB_R_LOG_D
						WHERE PROCESS_ID = @PROCESS_ID
						)
					,'MSPT00001ERR'
					,'ERR'
					,'MSPT00001ERR : ' + @MSG_TXT
					,@LOG_LOCATION
					,@USER_ID
					,GETDATE()
			END
		END

		IF @IsError = 'N'
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00005INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Validation'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00005INF'
				,'INF'
				,'MSPT00005INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			RETURN 0;
		END
		ELSE
		BEGIN
			EXEC dbo.CommonGetMessage 'MSPT00006INF'
				,@MSG_TXT OUTPUT
				,@N_ERR OUTPUT
				,'Invoice Validation'

			INSERT INTO TB_R_LOG_D (
				PROCESS_ID
				,SEQUENCE_NUMBER
				,MESSAGE_ID
				,MESSAGE_TYPE
				,[MESSAGE]
				,LOCATION
				,CREATED_BY
				,CREATED_DATE
				)
			SELECT @PROCESS_ID
				,(
					SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
					FROM dbo.TB_R_LOG_D
					WHERE PROCESS_ID = @PROCESS_ID
					)
				,'MSPT00006INF'
				,'INF'
				,'MSPT00006INF : ' + @MSG_TXT
				,@LOG_LOCATION
				,@USER_ID
				,GETDATE()

			RETURN 1;
		END
	END TRY

	BEGIN CATCH
		SET @PARAM1 = CONVERT(VARCHAR(1000), ERROR_MESSAGE());
		EXEC dbo.CommonGetMessage 'MPCS00999ERR'
			,@MSG_TXT OUTPUT
			,@N_ERR OUTPUT
			,@PARAM1

		INSERT INTO TB_R_LOG_D (
			PROCESS_ID
			,SEQUENCE_NUMBER
			,MESSAGE_ID
			,MESSAGE_TYPE
			,[MESSAGE]
			,LOCATION
			,CREATED_BY
			,CREATED_DATE
			)
		SELECT @PROCESS_ID
			,(
				SELECT MAX(ISNULL(SEQUENCE_NUMBER, 0)) + 1
				FROM dbo.TB_R_LOG_D
				WHERE PROCESS_ID = @PROCESS_ID
				)
			,'MPCS00999ERR'
			,'ERR'
			,'MPCS00999ERR : ' + @MSG_TXT
			,@LOG_LOCATION
			,@USER_ID
			,GETDATE()

		RETURN 1;
	END CATCH
END
