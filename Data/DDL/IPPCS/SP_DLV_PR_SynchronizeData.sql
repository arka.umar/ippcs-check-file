/*
	Preparation Date : 2014-09-24
	Preparation By   : Rahmat
	Description      : Synchronize PR data
*/

CREATE PROCEDURE [dbo].[SP_DLV_PR_SynchronizeData]

@PROD_MONTH VARCHAR (7),
@PROD_MONTH_LAST VARCHAR (6),
@CREATED_BY VARCHAR (25),
@pid AS BIGINT


AS
BEGIN
	
		DECLARE @MODUL_ID INT = 3
		DECLARE @FUNCTION_ID VARCHAR (6) =  '31102'
		
		--DECLARE @T AS TABLE (PID BIGINT)
		DECLARE @process_status AS INT = 0
		DECLARE @na AS VARCHAR(50) = 'SP_DLV_PR_SynchronizeData'
		DECLARE @step AS VARCHAR(50) = 'init'
		--DECLARE @pid AS BIGINT = 0
		DECLARE @log AS VARCHAR(MAX)
		DECLARE @steps AS VARCHAR(MAX)

		DECLARE @err_message nvarchar(255);
		DECLARE @CHECK_MODULE INT;
		

		DECLARE @CHECK_RETRIEVAL INT;
		

		SET @CHECK_MODULE = (SELECT COUNT (PROCESS_ID) FROM TB_R_DLV_QUEUE WHERE MODULE_ID =@MODUL_ID AND FUNCTION_ID=@FUNCTION_ID AND PROCESS_END_DT IS NULL );
		
		--IF (@CHECK_MODULE=0) BEGIN 


				CREATE TABLE #TB_T_PR_RETRIEVAL(
						PROD_MONTH varchar(6),
						LP_CD varchar(4),
						ROUTE_CD varchar(4),
						SOURCE varchar(20),
						PR_QTY int,
						PR_STATUS_FLAG varchar(3),
						PROCESS_ID bigint,
						CREATED_DT datetime,
						CREATED_BY varchar(50),
						CHANGED_DT datetime,
						CHANGED_BY varchar(50)
				);



				--CREATE LOG
				SET @steps = @na+'.'+@step;
				SET @log = 'Synchronize process started.';
				--INSERT @T
				EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;
				
				--INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
				--VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());

				BEGIN TRY
					INSERT INTO TB_R_DLV_QUEUE (PROCESS_ID, MODULE_ID, FUNCTION_ID, PROCESS_STATUS, PROCESS_START_DT, CREATED_BY, CREATED_DT)
					VALUES (@pid, @MODUL_ID, @FUNCTION_ID,'QU1', GETDATE(), @CREATED_BY, GETDATE());
				END TRY
				BEGIN CATCH
						UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2' WHERE PROCESS_ID = @pid;
				END CATCH


				INSERT INTO #TB_T_PR_RETRIEVAL 
				(PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, PROCESS_ID, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT)
				SELECT PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, PROCESS_ID, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT 
				FROM TB_T_PR_RETRIEVAL
				WHERE PROD_MONTH = @PROD_MONTH AND PR_STATUS_FLAG= 'PR1' AND SOURCE <> 'Manual';


				BEGIN TRY 
						
						--CREATE LOG
						SET @log = 'Delete data from table  TB_T_PR_RETRIEVAL.';
						SET @step = 'Process';
						SET @steps = @na+'.'+@step;
						--INSERT @T
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
						
						DELETE FROM TB_T_PR_RETRIEVAL WHERE PROD_MONTH = @PROD_MONTH AND PR_STATUS_FLAG= 'PR1' AND SOURCE <> 'Manual';
						
						
						BEGIN TRY
	
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU2', PROCESS_END_DT=GETDATE()
									WHERE PROCESS_ID = @pid;
							
									--CREATE LOG
									SET @log = 'Get data from PR Carry Over started.';
									SET @step = 'Process';
									SET @steps = @na+'.'+@step;
									--INSERT @T
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;

--== CARRY OVER ==--

									INSERT INTO TB_T_PR_RETRIEVAL (PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY,PR_STATUS_FLAG, PROCESS_ID, CREATED_BY, CREATED_DT)
								
									SELECT @PROD_MONTH, prH.LP_CD, prD.ROUTE_CD,'Carry Over', prD.PR_QTY,'PR1', @pid, @CREATED_BY, GETDATE()
									FROM TB_R_DLV_PR_D prD JOIN TB_R_DLV_PR_H prH
									ON prD.PR_NO = prH.PR_NO
									JOIN TB_M_LOGISTIC_PARTNER LP ON prH.LP_CD = LP.LOG_PARTNER_CD

									WHERE LP.ACTIVE_FLAG = 2 
									AND prH.PR_STATUS_FLAG NOT IN ('PR5','PR6')
									AND prH.PROD_MONTH = @PROD_MONTH_LAST;
									
									BEGIN TRY 


											--CREATE LOG
											SET @log = 'Get data from TLMS started.';
											SET @step = 'Process';
											SET @steps = @na+'.'+@step;
											--INSERT @T
											EXEC dbo.sp_PutLog @log,@CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;

--== TLMS ==--

											INSERT INTO TB_T_PR_RETRIEVAL (PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY,PR_STATUS_FLAG, PROCESS_ID, CREATED_BY, CREATED_DT)
											SELECT 
												@PROD_MONTH AS PR_PO_MONTH, 
												MTBL.LOGPARTNERCD as LP_CD,
												MTBL.RTEGRPCD as ROUTE_CD, 
												'TLMS', 
												MTBL.ROUTE_QTY,
												'PR1', 
												@pid, 
												@CREATED_BY, 
												GETDATE()
											FROM (
												SELECT RTEGRPCD, LOGPARTNERCD, COUNT(1) AS ROUTE_QTY FROM (
													SELECT DISTINCT a.RTEDATE, a.RTEGRPCD, RIGHT('00'+a.RUNSEQ,2) AS RUNSEQ, LOGPARTNERCD
													FROM TB_R_DCL_TLMS_ORDER_ASSIGNMENT A WITH(NOLOCK)
													INNER JOIN TB_R_DCL_TLMS_DRIVER_DATA B WITH(NOLOCK) ON a.RTEDATE = b.RTEDATE 
																								AND a.RTEGRPCD = b.RTEGRPCD
																								AND RIGHT('00'+a.RUNSEQ,2) = RIGHT('00'+b.RUNSEQ,2)
													WHERE REPLACE(CONVERT(VARCHAR(7), a.RTEDATE, 120), '-', '') = @PROD_MONTH
												) CHTBL
												GROUP BY RTEGRPCD, LOGPARTNERCD
											) MTBL
												INNER JOIN TB_M_LOGISTIC_PARTNER LP ON MTBL.LOGPARTNERCD = LP.LOG_PARTNER_CD AND LP.ACTIVE_FLAG = 2
												INNER JOIN TB_M_DLV_ROUTE DLV_ROUTE ON DLV_ROUTE.ROUTE_CD = MTBL.RTEGRPCD 
											WHERE LOGPARTNERCD + RTEGRPCD + @PROD_MONTH + 'TLMS' NOT IN
																						(
																							SELECT LP_CD + ROUTE_CD + PROD_MONTH + SOURCE FROM TB_T_PR_RETRIEVAL 
																							WHERE PR_STATUS_FLAG <> 'PR1' AND PROD_MONTH = @PROD_MONTH												
																						)
											ORDER BY MTBL.RTEGRPCD
											--SELECT 
											--			REPLACE(CONVERT(VARCHAR(7), RTEDATE, 120), '-', '') AS PR_PO_MONTH, 
											--			LOGPARTNERCD as LP_CD,
											--			RTEGRPCD as ROUTE_CD, 'TLMS', COUNT (RTEDATE) as ROUTE_QTY,'PR1', @pid, @CREATED_BY, GETDATE()

											--FROM TB_T_DCL_TLMS_DRIVER_DATA tlms

											--INNER JOIN TB_M_LOGISTIC_PARTNER LP ON tlms.LOGPARTNERCD = LP.LOG_PARTNER_CD AND LP.ACTIVE_FLAG = 2

											--INNER JOIN TB_M_DLV_ROUTE DLV_ROUTE ON DLV_ROUTE.ROUTE_CD=tlms.RTEGRPCD 

											--WHERE REPLACE(CONVERT(VARCHAR(7), RTEDATE, 120), '-', '') = @PROD_MONTH
											--AND LOGPARTNERCD != '' 
											--AND LOGPARTNERCD + RTEGRPCD + REPLACE(CONVERT(VARCHAR(7), RTEDATE, 120), '-', '') + 'TLMS' NOT IN
											--(
											--	SELECT LP_CD + ROUTE_CD + PROD_MONTH + SOURCE FROM TB_T_PR_RETRIEVAL 
											--	WHERE PR_STATUS_FLAG <> 'PR1' AND PROD_MONTH = @PROD_MONTH												
											--)
											--GROUP BY
											--REPLACE(CONVERT(VARCHAR(7), RTEDATE, 120), '-', ''), LOGPARTNERCD, RTEGRPCD
											
											BEGIN TRY


													--CREATE LOG
													SET @log = 'Get data from Non-TLMS started.';
													SET @step = 'Process';
													SET @steps = @na+'.'+@step;
													--INSERT @T
													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID;

--== Non TLMS ==--
													
													
													INSERT INTO TB_T_PR_RETRIEVAL (PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, PROCESS_ID, CREATED_BY, CREATED_DT)
													
													SELECT 
													REPLACE(CONVERT(VARCHAR(7), ctlH.PICKUP_DT, 120), '-', '') AS PR_PO_MONTH,
													ctlH.LP_CD,  
													ctlH.ROUTE  AS ROUTE_CD, 
													'Non-TLMS' AS SOURCE, 
													COUNT (ctlH.PICKUP_DT) as ROUTE_QTY,'PR1', @pid,
													@CREATED_BY, GETDATE()
													FROM 
													-- TB_R_DELIVERY_CTL_D ctlD
													-- JOIN 
													TB_R_DELIVERY_CTL_H ctlH 
													-- ON ctlD.DELIVERY_NO = ctlH.DELIVERY_NO
				
													INNER JOIN TB_M_LOGISTIC_PARTNER LP ON ctlH.LP_CD = LP.LOG_PARTNER_CD
													INNER JOIN TB_M_DLV_ROUTE DLV_ROUTE ON DLV_ROUTE.ROUTE_CD=ctlH.ROUTE

													WHERE REPLACE(CONVERT(VARCHAR(7), ctlH.PICKUP_DT, 120), '-', '') =@PROD_MONTH AND ctlH.DELIVERY_NO LIKE 'N%'
													AND ctlH.DELIVERY_STS = 'Initial' AND LP.ACTIVE_FLAG = 2
													AND LP_CD != '' AND 
													LP_CD + ROUTE + REPLACE(CONVERT(VARCHAR(7), ctlH.PICKUP_DT, 120), '-', '') + 'Non-TLMS' NOT IN
													(
														SELECT LP_CD + ROUTE_CD + PROD_MONTH + SOURCE FROM TB_T_PR_RETRIEVAL 
														WHERE PR_STATUS_FLAG <> 'PR1' AND PROD_MONTH = @PROD_MONTH												
													)
													GROUP BY REPLACE(CONVERT(VARCHAR(7), ctlH.PICKUP_DT, 120), '-', ''), ctlH.LP_CD, ctlH.ROUTE

													
													--CREATE LOG
													SET @log = 'Synchronize finished.';
													SET @step = 'Finished';
													SET @steps = @na+'.'+@step;
													--INSERT @T
													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'INF', @MODUL_ID, @FUNCTION_ID,1;
																				
													UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU3', PROCESS_END_DT=GETDATE()
													WHERE PROCESS_ID = @pid;
													
													UPDATE TB_T_PR_RETRIEVAL SET PROCESS_ID = NULL;
													
											END TRY --Non-TLMS
											BEGIN CATCH 
													
													--CREATE LOG
													SET @log = 'Error when Non-TLMS synchronize.';
													SET @step = 'Finished';
													SET @steps = @na+'.'+@step;
													--INSERT @T
													EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
															
													UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
													WHERE PROCESS_ID = @pid;

													DELETE FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid;

													INSERT INTO TB_T_PR_RETRIEVAL 
													(PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT)
													SELECT PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT 
													FROM #TB_T_PR_RETRIEVAL;

												

											END CATCH --Non-TLMS
											

									END TRY --TLMS
									BEGIN CATCH
											
											--CREATE LOG
											SET @log = 'Error when TLMS synchronize.';
											SET @step = 'Finished';
											SET @steps = @na+'.'+@step;
											--INSERT @T
											EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
													
											UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
											WHERE PROCESS_ID = @pid;

											DELETE FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid;

											INSERT INTO TB_T_PR_RETRIEVAL 
											(PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT)
											SELECT PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT 
											FROM #TB_T_PR_RETRIEVAL;

									END CATCH --TLMS
									

						END TRY --Carry over

						BEGIN CATCH
									
									--CREATE LOG
									SET @log = 'Error when carry over synchronize.';
									SET @step = 'Finished';
									SET @steps = @na+'.'+@step;
									--INSERT @T
									EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
											
									UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
									WHERE PROCESS_ID = @pid;

									DELETE FROM TB_T_PR_RETRIEVAL WHERE PROCESS_ID = @pid;

									INSERT INTO TB_T_PR_RETRIEVAL 
									(PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT)
									SELECT PROD_MONTH, LP_CD, ROUTE_CD, SOURCE, PR_QTY, PR_STATUS_FLAG, CREATED_DT, CREATED_BY, CHANGED_BY, CHANGED_DT 
									FROM #TB_T_PR_RETRIEVAL;

									
						END CATCH --Carry over
						

				END TRY --delete
				BEGIN CATCH

						--CREATE LOG
						
						UPDATE TB_R_DLV_QUEUE SET PROCESS_STATUS='QU4', PROCESS_END_DT=GETDATE()
						WHERE PROCESS_ID = @pid;

						SET @log = 'Error when deleting retrieval temporary table.';
						SET @step = 'Finished';
						SET @steps = @na+'.'+@step;
						--INSERT @T
						EXEC dbo.sp_PutLog @log, @CREATED_BY, @steps, @pid OUTPUT, 'MPCS00002INF', 'ERR', @MODUL_ID, @FUNCTION_ID;
						

				END CATCH --delete		

				
				DROP TABLE #TB_T_PR_RETRIEVAL;
				
				

		--END
		

		--ELSE BEGIN
			

			--SET @err_message = 'Same process still runnning with process ID <b>'+ CONVERT (varchar (12), @pid) +'</b>, please  wait until it finish.';
			--RAISERROR(@err_message,16,1)
			--RETURN

		--END
				
END

