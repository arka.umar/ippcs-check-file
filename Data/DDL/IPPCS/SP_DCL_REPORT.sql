-- =============================================
-- Author:		FID.Deny
-- Create date: 2015-03-25
-- Desc: Download DCLInquiry in DCLReport
-- =============================================
CREATE PROCEDURE [dbo].[SP_DCL_REPORT] 
@DateFrom varchar(10),
@DateTo varchar(10)
AS
BEGIN
			select * from 
			(SELECT 
				a.DELIVERY_NO  as DeliveryNo,
				CONVERT(varchar(10),a.PICKUP_DT, 120) as PickupDate,
				a.ROUTE as RouteCode, 
				a.RATE as Rate, 
				a.LP_CD as LPCode,
				c.LOG_PARTNER_NAME as LPName
				from 
				TB_R_DELIVERY_CTL_H as a LEFT OUTER JOIN
				TB_R_DELIVERY_CTL_MANIFEST as b 
				on a.DELIVERY_NO = b.DELIVERY_NO LEFT OUTER JOIN
				TB_M_LOGISTIC_PARTNER as c 
				on a.LP_CD = c.LOG_PARTNER_CD 
				WHERE 
					--a.DELIVERY_NO = 'R201503208547'
					a.PICKUP_DT between convert(date, @DateFrom, 101) and convert(date, @DateTo, 101)
				) as aa 
				
				LEFT JOIN
				
				(
				SELECT 
					RTE.DELIVERY_NO AS DELIVERY_NO,
					CASE WHEN RTE.ORDERNO <> '' AND RTE.ORDERNO IS NOT NULL THEN --milkrun
	 				CASE
						WHEN RTE.SUPPLIER_CD like '8%'
						THEN RTE.SUPPLIER_CD + '-' + RTE.SUPPLIER_PLANT_CD + '-' + RTE.SUPP_DOCK_CD
						ELSE RTE.SUPPLIER_CD + '-' + RTE.SUPPLIER_PLANT_CD 
					END
					ELSE --from real supplier 
					RTE.SUPPLIER_CD + '-' + RTE.SUPPLIER_PLANT_CD + '-' + RTE.DOCK_CD
					END
					AS LogisticPoint,
    
					CASE WHEN RTE.ORDERNO <> '' AND RTE.ORDERNO IS NOT NULL THEN --milkrun
					SP.SUPPLIER_NAME
					ELSE
					'Dock ' + RTE.DOCK_CD + ' (T/S #'
						+ (select d.STATION from TB_R_DELIVERY_CTL_D d
						where d.DELIVERY_NO = rte.DELIVERY_NO AND d.DOCK_CD = rte.DOCK_CD) + ')'
					END
					AS SupplierName,

					CONVERT(varchar(19), RTE.ARRIVAL_PLAN_DT, 120) AS ArrivalPlan,
					CONVERT(varchar(19), RTE.DEPARTURE_PLAN_DT, 120) AS DeparturePlan,
					RTE.ORDERNO AS OrderNo,
     
	 
					CASE WHEN RTE.ORDERNO <> '' AND RTE.ORDERNO IS NOT NULL THEN --milkrun
					RTE.DOCK_CD
					ELSE
					(select d.DOORCD from TB_R_DELIVERY_CTL_D d
						where d.DELIVERY_NO = rte.DELIVERY_NO AND d.DOCK_CD = rte.DOCK_CD)
					END
					AS DockCode
					--,
					--(SELECT count(*) as Jumlah FROM 
					--(SELECT DISTINCT A.SUPPLIER_CODE, 
					--	A.SUPPLIER_NAME SupplierName , A.SUPPLIER_PLANT_CD
					--	FROM TB_M_SUPPLIER A
					--	INNER JOIN TB_R_DELIVERY_CTL_ROUTE B 
					--	on A.SUPPLIER_CODE = B.SUPPLIER_CD AND A.SUPPLIER_PLANT_CD = B.SUPPLIER_PLANT_CD
					--	WHERE B.DELIVERY_NO= RTE.DELIVERY_NO and A.SUPPLIER_CODE not like '8%') as temp) AS JUMLAH

				FROM TB_R_DELIVERY_CTL_ROUTE RTE 
					LEFT OUTER JOIN TB_M_SUPPLIER SP on SP.SUPPLIER_CODE = RTE.SUPPLIER_CD AND SP.SUPPLIER_PLANT_CD = RTE.SUPPLIER_PLANT_CD
				
				) as bb on aa.DeliveryNo = bb.DELIVERY_NO

				ORDER BY aa.PickupDate, aa.RouteCode, aa.Rate, bb.ArrivalPlan, bb.LogisticPoint, bb.OrderNo



END

