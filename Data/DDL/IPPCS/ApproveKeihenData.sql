CREATE PROCEDURE [dbo].[ApproveKeihenData]
	@pack_month varchar(8),
	@userid varchar(20)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @p_userid VARCHAR(20) = @userid
	DECLARE @p_packmonth varchar(6) = @pack_month
	DECLARE @p_error_msg VARCHAR(max) = ''
	DECLARE @process_id bigint = 0
	DECLARE @APPROVED_DT datetime
	DECLARE @APPROVED_BY varchar(20) = ''
	DECLARE @SUPPLIER TABLE (
		ROW_NO INT,
		SUPP_CD VARCHAR(10),
		SUPP_PLANT VARCHAR(5),
		SUPP_NAME VARCHAR(max),
		SUPP_EMAIL VARCHAR(max)
	)

	DECLARE @log varchar(max) = ''
	DECLARE @T TABLE (pid bigint)
	DECLARE @sendmailstatus VARCHAR(MAX) = ''

	BEGIN TRY
		SET @log = 'Initiate Approval Keihen';
		INSERT INTO @T EXEC sp_PutLog @log, @userid, 'Keihen.Approval', @process_id OUTPUT, 'MPCS00008INF', 'INF', '9', '99997', 0;

		IF(ISNULL(@process_id, 0) = 0)
		BEGIN
			SET @p_error_msg = 'Error When Get Process ID. Approval Failed'
			RAISERROR(@p_error_msg, 16, 1)
		END

		IF(NOT EXISTS(SELECT 'x' FROM TB_R_KEIHEN_SUMMARY WHERE PACK_MONTH = @p_packmonth))
		BEGIN
			SET @p_error_msg = 'Keihen Summary Data Does Not Exists'
			RAISERROR(@p_error_msg, 16, 1)
		END

		SELECT TOP 1 
			@APPROVED_DT = MAX(APPROVED_DT),
			@APPROVED_BY = MAX(APPROVED_BY)
		FROM TB_R_KEIHEN_SUMMARY
		WHERE PACK_MONTH = @p_packmonth
		GROUP BY PACK_MONTH

		IF(ISNULL(@APPROVED_BY, '') <> '' OR ISNULL(@APPROVED_DT, '') <> '')
		BEGIN
			SET @p_error_msg = 'Keihen Data with Pack Month ' + ISNULL(@p_packmonth, '') + ' Already Approved at ' + ISNULL(CONVERT(VARCHAR(20), @APPROVED_DT), '') + ' By ' + ISNULL(@APPROVED_BY, '')
			RAISERROR(@p_error_msg, 16, 1)
		END

		IF(EXISTS(SELECT 'x' FROM TB_R_KEIHEN_FEEDBACK WHERE PACK_MONTH = @p_packmonth))
		BEGIN
			SET @p_error_msg = 'Keihen Data with Pack Month ' + ISNULL(@p_packmonth, '') + ' Already Exists in FeedBack Table. Approval Failed'
			RAISERROR(@p_error_msg, 16, 1)
		END

		INSERT INTO TB_R_KEIHEN_FEEDBACK (
			PACK_MONTH,
			PART_NO,
			DOCK_CD,
			SUPPLIER_CD,
			S_PLANT_CD,
			CAR_CD,
			N_1_ORIGINAL,
			N_1_NEW,
			FLUCTUATION,
			CREATED_BY,
			CREATED_DT,
			PROCESS_ID,
			LOCK
		)
		SELECT
			PACK_MONTH,
			PART_NO,
			DOCK_CD,
			SUPPLIER_CD,
			S_PLANT_CD,
			CAR_CD,
			N_1_ORIGINAL,
			N_1_NEW,
			FLUCTUATION,
			@p_userid,
			GETDATE(),
			@process_id,
			0
		FROM TB_R_KEIHEN_SUMMARY
		WHERE PACK_MONTH = @p_packmonth
			  AND N_1_ORIGINAL <> N_1_NEW

		UPDATE TB_R_KEIHEN_SUMMARY
			SET APPROVED_BY = @p_userid,
				APPROVED_DT = GETDATE(),
				CHANGED_BY = @p_userid,
				CHANGED_DT = GETDATE()
		WHERE PACK_MONTH = @p_packmonth

		EXEC [KeihenMail] 'APPROVAL', @p_packmonth, @p_userid, @process_id, NULL, @sendmailstatus OUTPUT

		SELECT @sendmailstatus as SENDMAILSTATUS
	END TRY
	BEGIN CATCH
		DELETE FROM TB_R_KEIHEN_FEEDBACK WHERE PROCESS_ID = @process_id

		UPDATE TB_R_KEIHEN_SUMMARY
			SET APPROVED_BY = NULL,
				APPROVED_DT = NULL
		WHERE PACK_MONTH = @p_packmonth

		SET @p_error_msg = 'Error on Approval : ' + ERROR_MESSAGE()
		RAISERROR(@p_error_msg, 16, 1)
	END CATCH
END

