USE [IPPCS_QA]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Supplier_MasterSynchronization]    Script Date: 10/11/2020 16:11:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Sp_Supplier_MasterSynchronization]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE
	@ro_v_userid	varchar(20) = 'job_Sp_Supplier_Master',
	@Message		varchar(max),
	@MESSAGE_ID			varchar(12),
	@created_by varchar(50) = 'job_Sp_Supplier_Master';


	SET NOCOUNT ON;
	DECLARE
	@Processid  bigint = (select dbo.fn_get_last_process_id()),
	@module varchar(8)='02.02',
	@function varchar(10) = '02.02.06',
	@location varchar(50) = 'Job Sp_Supplier_MasterSynchronization',
	@process_flag varchar(max) = 'start upload',
	@now date = getdate(),
	@MESSAGE_TYPE varchar(5) = 'INF';

	EXEC [dbo].[spInsertLogHeader] @Processid,@function,@module,@now,@now,'5','SYSTEM'
	 
	
	
	SET @MESSAGE_ID = 'MPCS00002INF'
	SET @Message = (select REPLACE(MESSAGE_TEXT,'{0}','job_Sp_Supplier_Master') from TB_M_MESSAGE where MESSAGE_ID = @MESSAGE_ID)
	
	EXEC dbo.SPInsertLogDetail @Processid
			,@MESSAGE_ID
			,@MESSAGE_TYPE
			,@MESSAGE
			,@LOCATION
			,'SYSTEM'
			,null;

	-- Delete Data Supplier Bank
	 DELETE FROM TB_M_SUPPLIER_BANK
	--SET @Message = 'Delete Data Supplier Bank'
	--SET @MESSAGE_ID = 'MSG0000005'
	--EXEC dbo.SPInsertLogDetail @Processid
	--		,@MESSAGE_ID
	--		,@MESSAGE_TYPE
	--		,@MESSAGE
	--		,@LOCATION
	--		,'SYSTEM'
			--,null;


	-- Delete Supplier master
	DELETE FROM TB_M_SUPPLIER_ICS
	--SET @Message = 'Delete Data Supplier Master'
	--SET @MESSAGE_ID = 'MSG0000005'
	--EXEC dbo.SPInsertLogDetail @Processid
	--		,@MESSAGE_ID
	--		,@MESSAGE_TYPE
	--		,@MESSAGE
	--		,@LOCATION
	--		,'SYSTEM'
	--		,null;

	-- Move data from staging to permanent
	begin try
		INSERT INTO TB_M_SUPPLIER_BANK
		SELECT 
			SUPP_CD,
			SUPP_BANK_KEY,
			SUPP_BANK_COUNTRY,
			SUPP_ACCOUNT,
			SUPP_BANK_TYPE,
			ACCOUNT_HOLDER,
			ACCOUNT_CURR,			
			'',
			GETDATE(),
			NULL ,
			NULL		 
		FROM TB_T_SUPPLIER_BANK

		SET @MESSAGE_ID = 'MPCS00120INF'
		SET @Message = (select REPLACE(MESSAGE_TEXT,'{0}','data Supplier Bank from staging to permanent') from TB_M_MESSAGE where MESSAGE_ID = @MESSAGE_ID)
		
		--SET @MESSAGE_ID = 'MSG0000005'
		EXEC dbo.SPInsertLogDetail @Processid
				,@MESSAGE_ID
				,@MESSAGE_TYPE
				,@MESSAGE
				,@LOCATION
				,'SYSTEM'
				,null;

		INSERT INTO TB_M_SUPPLIER_ICS
		SELECT 					  
			SUPP_CD					 ,
			PAYMENT_METHOD_CD		 ,
			SUPP_EMAIL_ADDR1		 ,
			SUPP_EMAIL_ADDR2		 ,
			PAYMENT_TERM_CD			 ,
			SUPP_SEARCH_TERM		 ,
			SUPP_NAME				 ,
			SUPP_ADDR				 ,
			SUPP_CITY				 ,
			POSTAL_CD				 ,
			ORD_CURR				 ,
			FREQUENCIES_OF_GR_SENDING			 ,
			FREQUENCIES_REMARK		 ,
			DELETION_FLAG			 ,
			SUPP_ATTENTION			 ,
			SUPP_ATTENTION_POSITION	 ,
			PHONE_NO				 ,
			FAX_NO					 ,
			NULL			 ,
			SUPP_ATTENTION_2		 ,
			SUPP_ATTENTION_POSITION_2,
			@created_by,
			getdate(),
			null,
			null
		FROM TB_T_SUPPLIER_ICS
		
		SET @MESSAGE_ID = 'MPCS00120INF'
		SET @Message = (select REPLACE(MESSAGE_TEXT,'{0}','data Supplier from staging to permanent') from TB_M_MESSAGE where MESSAGE_ID = @MESSAGE_ID)
						
		EXEC dbo.SPInsertLogDetail @Processid
				,@MESSAGE_ID
				,@MESSAGE_TYPE
				,@MESSAGE
				,@LOCATION
				,'SYSTEM'
				,null;


		SET @MESSAGE_ID = 'MPCS00122INF'
		SET @Message = (select REPLACE(MESSAGE_TEXT,'{0}','job_Sp_Supplier_Master') from TB_M_MESSAGE where MESSAGE_ID = @MESSAGE_ID)
				
		EXEC dbo.SPInsertLogDetail @Processid
				,@MESSAGE_ID
				,@MESSAGE_TYPE
				,@MESSAGE
				,@LOCATION
				,'SYSTEM'
				,null;

	end try
	begin catch
		SET @MESSAGE_ID = 'MPCS00121ERR'
		SET @MESSAGE_TYPE = 'ERR'
		SET @Message = (select REPLACE(MESSAGE_TEXT,'{0}',ERROR_MESSAGE()) from TB_M_MESSAGE where MESSAGE_ID = @MESSAGE_ID)
		
		EXEC dbo.SPInsertLogDetail @Processid
				,@MESSAGE_ID
				,@MESSAGE_TYPE
				,@MESSAGE
				,@LOCATION
				,'SYSTEM'
				,null;

		SET @MESSAGE_ID = 'MPCS00123ERR'
		SET @Message = (select REPLACE(MESSAGE_TEXT,'{0}','job_Sp_Supplier_Master') from TB_M_MESSAGE where MESSAGE_ID = @MESSAGE_ID)
				
		EXEC dbo.SPInsertLogDetail @Processid
				,@MESSAGE_ID
				,@MESSAGE_TYPE
				,@MESSAGE
				,@LOCATION
				,'SYSTEM'
				,null;
	end catch

END
