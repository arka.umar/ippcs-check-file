﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using CreatePOPostingTaskRuntime.Models;
using System.Reflection;
using Toyota.Common.Logging.Sink;

namespace CreatePOPostingTaskRuntime
{
    class CreatePOPostingTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            try
            {
                //DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(CreatePOPostingTaskRuntime)), "CreatePOPostingTaskRuntime.SQLFiles"));
                //IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

                //int isError = 0;
                string prodmonth = string.Empty;
                //string processID = "";
                const int MAX_DATA_TRANSFERRED = 10;

                //String gridId = "August-2013_Firm_lpd.testing10";
                //String username = "FID.Iman";

                /* Get parameter with key */
                //String gridId = parameters.Get("GridId");
                String processId = parameters.Get("ProcessID");
                String username = parameters.Get("Username");

                string ProductionMonth = parameters.Get("ProductionMonth");
                //string Timing = "";
                //string ApproveBy = "";

                //char[] SplitChar = { ';' };
                //string[] ParamUpdate = gridId.Split(SplitChar);

                //List<LPOPPurchaseOrder> tempList;
                //List<LPOPPurchaseOrder> list = new List<LPOPPurchaseOrder>();

                GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
                ServiceResult result;

                ServiceParameters sp = new ServiceParameters();
                string sessionID = processId;
                //sp.Add("state", "init");
                //result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));


                sp.Clear();
                sp.Add("state", "process");
                sp.Add("sessionId", sessionID);
                sp.Add("prodMonth", ProductionMonth);
                sp.Add("userId", username);
                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));

                /* Sorry men, nambahin buat debugging aja */
                DefaultLogSession.WriteLine(new LoggingMessage(string.Format("Process result: Status -> {0}, Value: {1}", result.Status, result.Value)));

                #region Phase 1 : Generate Posting File PO -- Disabled Function

                //DefaultLogSession.WriteLine(new LoggingMessage()
                //{
                //    Message = "Start in generate posting file PO.",
                //    Severity = LoggingSeverity.Info
                //});
                //SetProgress(0);

                //for (int i = 0; i < ParamUpdate.Length; i++)
                //{
                //    try
                //    {
                //        tempList = new List<LPOPPurchaseOrder>();
                //        char[] SplitId = { '_' };
                //        string[] ParamId = ParamUpdate[i].Split(SplitId);
                //        ProductionMonth = Convert.ToDateTime(ParamId[0]).Year.ToString() + Convert.ToDateTime(ParamId[0]).Month.ToString("0#");
                //        Timing = ParamId[1].ToString().Substring(0, 1);
                //        ApproveBy = ParamId[2];

                //        tempList = db.Query<LPOPPurchaseOrder>(DatabaseManager.LoadQuery("GetCreatePOICS"), new object[] { ProductionMonth, Timing, ApproveBy }).ToList<LPOPPurchaseOrder>();

                //        if (tempList.Count == 0)
                //        {
                //            DefaultLogSession.WriteLine(new LoggingMessage()
                //            {
                //                Message = "No data found in generate posting file PO. Production Month : " + ProductionMonth + ", Version : " + Timing,
                //                Severity = LoggingSeverity.Warning
                //            });
                //        }

                //        foreach (LPOPPurchaseOrder lpoppo in tempList)
                //        {
                //            list.Add(lpoppo);
                //        }
                //    }
                //    catch (Exception exc)
                //    {
                //        DefaultLogSession.WriteLine(new LoggingMessage()
                //        {
                //            Message = "Error in generate posting file PO. " + exc.Message,
                //            Severity = LoggingSeverity.Error
                //        });

                //        throw exc;
                //    }
                //}

                //DefaultLogSession.WriteLine(new LoggingMessage()
                //{
                //    Message = "Finish in generate posting file PO.",
                //    Severity = LoggingSeverity.Info
                //});
                //SetProgress(10);

                #endregion

                //if (list.Count == 0)
                //{
                //    DefaultLogSession.WriteLine(new LoggingMessage()
                //    {
                //        Message = "No data found in generate posting file PO.",
                //        Severity = LoggingSeverity.Warning
                //    });
                //}
                //else
                //{
                    //GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
                    //ServiceResult result;

                    #region Phase 2 : Startup IPPCS - ICS Gateway Web Service -- Disabled Function

                    //DefaultLogSession.WriteLine(new LoggingMessage()
                    //{
                    //    Message = "Do process startup ICS web service.",
                    //    Severity = LoggingSeverity.Info
                    //});

                    //bool serviceReady = false;

                    //try
                    //{
                    //    gateway.Open();
                    //    string testStringResult = gateway.execute("Startup", "IntegrityCheck", null);
                    //    if (!string.IsNullOrEmpty(testStringResult))
                    //    {
                    //        result = ServiceResult.Create(testStringResult);
                    //        serviceReady = (result.Status == ServiceResult.STATUS_READY);
                    //    }
                    //}
                    //catch (Exception exc)
                    //{
                    //    gateway.Abort();
                    //    gateway = new GatewayService.WebServiceImplClient();
                    //    gateway.Open();

                    //    DefaultLogSession.WriteLine(new LoggingMessage()
                    //    {
                    //        Message = "Error do process startup ICS web service. " + exc.Message,
                    //        Severity = LoggingSeverity.Error
                    //    });

                    //    throw exc;
                    //}

                    //if (!serviceReady)
                    //{
                    //    DefaultLogSession.WriteLine(new LoggingMessage()
                    //    {
                    //        Message = "Fail do process startup ICS web service.",
                    //        Severity = LoggingSeverity.Warning
                    //    });
                    //}

                    #endregion 

                    #region Phase 3 : Init State ICS Web Service

                    //DefaultLogSession.WriteLine(new LoggingMessage()
                    //{
                    //    Message = "Do process init state ICS web service.",
                    //    Severity = LoggingSeverity.Info
                    //});

                    //string jsonResult;
                    //ServiceParameters sp = new ServiceParameters();
                    //string sessionID;
                    //sp.Add("state", "init");
                    //result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));

                    #endregion

                    //if (result.Status != ServiceResult.STATUS_ERROR)
                    //{
                        #region Phase 4 : Transfer Data to ICS Web Service

                        //DefaultLogSession.WriteLine(new LoggingMessage()
                        //{
                        //    Message = "Start in transfer data to ICS web service.",
                        //    Severity = LoggingSeverity.Info
                        //});

                        //sessionID = result.Value.ToString();

                        //sp.Clear();
                        //sp.Add("state", "transfer");
                        //sp.Add("sessionId", result.Value);
                        //sp.Add("data", String.Empty);

                        //const int MAX_DATA_TRANSFERRED = 10;
                        //int dataLength = list.Count;
                        //int lastIndex = dataLength;
                        //int counter = 0;
                        //int fetchSize;
                        //int indexDifference;
                        //bool looping = true;
                        //bool transferError = false;
                        //while (looping)
                        //{
                        //    indexDifference = (lastIndex - counter);
                        //    if (indexDifference < MAX_DATA_TRANSFERRED)
                        //    {
                        //        fetchSize = indexDifference;
                        //    }
                        //    else
                        //    {
                        //        fetchSize = MAX_DATA_TRANSFERRED;
                        //    }

                        //    tempList = list.GetRange(counter, fetchSize);
                        //    jsonResult = JSON.ToString<List<LPOPPurchaseOrder>>(tempList);

                        //    sp.Remove("data");
                        //    sp.Add("data", jsonResult);
                        //    sp.Add("userId", username);
                        //    result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));

                        //    if (result.Status == ServiceResult.STATUS_SUCCESS)
                        //    {
                        //        counter += (fetchSize);
                        //    }
                        //    else
                        //    {
                        //        transferError = true;
                        //        looping = false;
                        //    }

                        //    if (counter == lastIndex)
                        //    {
                        //        looping = false;
                        //    }

                        //}

                        //DefaultLogSession.WriteLine(new LoggingMessage()
                        //{
                        //    Message = "Finish in transfer data to ICS web service.",
                        //    Severity = LoggingSeverity.Info
                        //});
                        //SetProgress(40);

                        #endregion

                        //if (transferError)
                        //{
                        //    DefaultLogSession.WriteLine(new LoggingMessage()
                        //    {
                        //        Message = "Fail in transfer data to ICS web service.",
                        //        Severity = LoggingSeverity.Warning
                        //    });
                        //    SetProgress(100);
                        //}
                        //else
                        //{
                            #region Phase 5 : Taking Data From ICS Web Service

                            //DefaultLogSession.WriteLine(new LoggingMessage()
                            //{
                            //    Message = "Start in taking data from ICS web service.",
                            //    Severity = LoggingSeverity.Info
                            //});

                            //DefaultLogSession.WriteLine(new LoggingMessage()
                            //{
                            //    Message = "Do process state ICS web service.",
                            //    Severity = LoggingSeverity.Info
                            //});

                            //sp.Clear();
                            //sp.Add("state", "process");
                            //sp.Add("sessionId", sessionID);
                            //sp.Add("prodMonth", ProductionMonth);
                            //sp.Add("userId", username);
                            //sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                            //result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));

                            ///* Sorry men, nambahin buat debugging aja */
                            //DefaultLogSession.WriteLine(new LoggingMessage(string.Format("Process result: Status -> {0}, Value: {1}", result.Status, result.Value)));

                            //if (result.Status != ServiceResult.STATUS_ERROR)
                            //{
                            //    DefaultLogSession.WriteLine(new LoggingMessage()
                            //    {
                            //        Message = "Start in inquiry state ICS web service.",
                            //        Severity = LoggingSeverity.Info
                            //    });

                            //    int totalPage = Convert.ToInt32(result.Value != null ? result.Value : 0);
                            //    sp.Clear();
                            //    sp.Add("state", "inquiry");
                            //    sp.Add("sessionId", sessionID);
                            //    sp.Add("processId", "101");
                            //    sp.Add("prodMonth", ProductionMonth);
                            //    int seqLogDetail = 0;

                            //    for (int i = 1; i <= totalPage; i++)
                            //    {
                            //        try
                            //        {
                            //            sp.Add("currentPage", i);
                            //            sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                            //            sp.Add("prodMonth", ProductionMonth);
                            //            result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));

                            //            /* Sorry men, nambahin buat debugging aja */
                            //            DefaultLogSession.WriteLine(new LoggingMessage(string.Format("Inquiry result: data --> {0}", result.Value)));
                            //        }
                            //        catch (Exception exc)
                            //        {
                            //            if (exc.Message.Contains("Timeout") || exc.Message.Contains("timeout"))
                            //            {
                            //                sp.Add("currentPage", i);
                            //                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                            //                sp.Add("prodMonth", ProductionMonth);
                            //                result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));
                            //            }

                            //            DefaultLogSession.WriteLine(new LoggingMessage()
                            //            {
                            //                Message = "Error do process inquiry state ICS web service. " + exc.Message,
                            //                Severity = LoggingSeverity.Error
                            //            });
                            //            SetProgress(100);

                            //            throw exc;
                            //        }

                            //        if (result.Status != ServiceResult.STATUS_ERROR)
                            //        {
                            //            if (result.Value == null)
                            //            {
                            //                DefaultLogSession.WriteLine(new LoggingMessage()
                            //                {
                            //                    Message = "No data found do process inquiry state ICS web service.",
                            //                    Severity = LoggingSeverity.Warning
                            //                });
                            //            }

                            //            List<LPOPPurchaseOrderFinish> values = new List<LPOPPurchaseOrderFinish>();
                            //            values = JSON.ToObject<List<LPOPPurchaseOrderFinish>>(result.Value.ToString());
                            //            //DateTime postingDate = DateTime.Now;
                            //            DateTime poDate;
                            //            double totalamount;

                            //            DefaultLogSession.WriteLine(new LoggingMessage()
                            //            {
                            //                Message = "Do process inserting data from ICS web service.",
                            //                Severity = LoggingSeverity.Info
                            //            });

                            //            foreach (LPOPPurchaseOrderFinish e in values)
                            //            {
                            //                prodmonth = Convert.ToDateTime(e.prodMonth, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID")).Year.ToString()
                            //                            + Convert.ToDateTime(e.prodMonth, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID")).Month.ToString().PadLeft(2, '0');
                            //                //postingDate = Convert.ToDateTime(e.postingDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));
                            //                totalamount = Convert.ToDouble(string.IsNullOrEmpty(e.qtyN) ? "0" : e.qtyN) * Convert.ToDouble(string.IsNullOrEmpty(e.poMatPrice) ? "0" : e.poMatPrice);
                            //                if (e.poNo != null)
                            //                {
                            //                    poDate = Convert.ToDateTime(e.docDate.Substring(6, 2) + "-" + e.docDate.Substring(4, 2) + "-" + e.docDate.Substring(0, 4), System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));

                            //                    db.Execute(DatabaseManager.LoadQuery("ICSInsertPostingCreatePO"), new object[] { 
                            //                e.poNo, 
                            //                prodmonth, 
                            //                e.suppCode, 
                            //                e.poCurr,
                            //                e.poExchangeRate,
                            //                totalamount,
                            //                e.plantCode,
                            //                username,
                            //                e.poItemNo,
                            //                e.prNo,
                            //                e.prItem,
                            //                e.matNo,
                            //                e.sourceType,
                            //                e.prodPurposeCode,
                            //                e.plantCode,
                            //                e.slocCode,
                            //                e.partColorSfx,
                            //                e.packingType,
                            //                e.poGrQty == null ? "0" : e.poGrQty,
                            //                e.poMatPrice,
                            //                null, // field required_date,
                            //                poDate,
                            //                processID,
                            //                sessionID,
                            //                seqLogDetail++,
                            //            });
                            //                    try
                            //                    {
                            //                        //bool isRegistered = WorkflowFunction.RegisterWorklist(e.poNo, "1", 10, username);
                            //                    }
                            //                    catch (Exception exc)
                            //                    {
                            //                        DefaultLogSession.WriteLine(new LoggingMessage()
                            //                        {
                            //                            Message = "Error do process inserting data from ICS web service. " + exc.Message,
                            //                            Severity = LoggingSeverity.Error
                            //                        });

                            //                        SetProgress(100);

                            //                        throw exc;
                            //                    }
                            //                }
                            //                else
                            //                {
                            //                    db.Execute(DatabaseManager.LoadQuery("InsertLPOPConfirmation_Abnormality_Log"), new object[] { 
                            //                    sessionID,
                            //                    username,
                            //                    seqLogDetail++,
                            //                    e.matNo,
                            //                    e.suppCode,
                            //                    e.dockCode,
                            //                    e.plantCode,
                            //                    e.slocCd,
                            //                    e.errMsg,
                            //                    ProductionMonth,
                            //                    "FCST",
                            //                    Timing
                            //            });

                            //                    db.Execute(DatabaseManager.LoadQuery("InsertLPOPConfirmation_Abnormality"), new object[] { 
                            //                    ProductionMonth, //@PackMonth,
                            //                    "FCST", //@DID,
                            //                    Timing, //@Vers,
                            //                    e.matNo, //@PartNo,
                            //                    e.suppCode, //@SuppCd,
                            //                    e.errCode //@ErrCd
                            //            });
                            //                }
                            //            }
                            //        }
                            //        else
                            //        {
                            //            DefaultLogSession.WriteLine(new LoggingMessage()
                            //            {
                            //                Message = "Fail do process inquiry state ICS web service.",
                            //                Severity = LoggingSeverity.Warning
                            //            });
                            //            SetProgress(100);

                            //            transferError = true;
                            //        }
                            //    }
                            //    DefaultLogSession.WriteLine(new LoggingMessage()
                            //    {
                            //        Message = "Finish in inquiry state ICS web service.",
                            //        Severity = LoggingSeverity.Info
                            //    });
                            //    SetProgress(80);

                            //    DefaultLogSession.WriteLine(new LoggingMessage()
                            //    {
                            //        Message = "Start in finish state ICS web service.",
                            //        Severity = LoggingSeverity.Info
                            //    });

                            //    sp.Clear();
                            //    sp.Add("state", "finish");
                            //    sp.Add("sessionId", sessionID);
                            //    result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));
                            //    sp.Clear();
                            //    if (result.Status == ServiceResult.STATUS_ERROR)
                            //    {
                            //        DefaultLogSession.WriteLine(new LoggingMessage()
                            //        {
                            //            Message = "Fail in finish state ICS web service.",
                            //            Severity = LoggingSeverity.Warning
                            //        });
                            //    }
                            //    DefaultLogSession.WriteLine(new LoggingMessage()
                            //    {
                            //        Message = "Finish in finish state ICS web service.",
                            //        Severity = LoggingSeverity.Info
                            //    });
                            //    SetProgress(100);
                            //}
                            //else
                            //{
                            //    DefaultLogSession.WriteLine(new LoggingMessage()
                            //    {
                            //        Message = "Fail do process state ICS web service.",
                            //        Severity = LoggingSeverity.Warning
                            //    });

                            //    SetProgress(100);
                            //}

                            //DefaultLogSession.WriteLine(new LoggingMessage()
                            //{
                            //    Message = "Finish in taking data from ICS web service.",
                            //    Severity = LoggingSeverity.Info
                            //});

                            #endregion
                        //}
                    //}
                    //else
                    //{
                    //    DefaultLogSession.WriteLine(new LoggingMessage()
                    //    {
                    //        Message = "Fail do process init state ICS web service.",
                    //        Severity = LoggingSeverity.Warning
                    //    });

                    //    SetProgress(100);
                    //}

                    //if (isError == 0)
                    //{
                    //    DefaultLogSession.WriteLine(new LoggingMessage()
                    //    {
                    //        Message = "Do process update create PO.",
                    //        Severity = LoggingSeverity.Info
                    //    });

                    //    db.Execute(DatabaseManager.LoadQuery("UpdateCreatePO"), new object[] { 
                    //                        ProductionMonth,
                    //                        username
                    //                });
                    //}

                    gateway.Close();
                //}

                //db.Close();
            }
            catch (Exception exc) 
            {
                throw exc;
            }            
        }
    }
}
