﻿DECLARE @@PO_NO VARCHAR(30) = @0,
		@@PRODUCTION_MONTH varchar(6) = @1,
		@@SUPPLIER_CD varchar(6) = @2,
		@@CURRENCY_CD varchar(3) = @3,
		@@EXCHANGE_RATE numeric = @4,
		@@TOTAL_AMOUNT numeric = @5,
		@@SUPPLIER_PLANT CHAR(1) = @6,
		@@CREATED_BY varchar(20)  = @7,
		@@PO_ITEM varchar(5) = @8,
        @@PR_NO varchar(8) = @9,
        @@PR_ITEM varchar(5) = @10,
        @@MAT_NO varchar(23) = @11,
        @@SOURCE_TYPE varchar(1) = @12,
        @@PROD_PURPOSE_CD varchar(5) = @13,
        @@PLANT_CD varchar(4) = @14,
        @@SLOC_CD nchar(10) = @15,
        @@PART_COLOR_SUFFIX varchar(2) = @16,
        @@PACKING_TYPE varchar(1) = @17,
        @@PO_QTY_ORIGINAL int = @18,
        @@PO_MAT_PRICE numeric = @19,
        @@REQUIRED_DT datetime = @20, 
		@@PO_DATE datetime = @21
	
	
IF NOT EXISTS(SELECT 1 FROM TB_R_PO_H WHERE PO_NO=@@PO_NO)
BEGIN
	PRINT 'tidak ADA'
	INSERT INTO TB_R_PO_H
	        ( PO_NO ,
	          SUPPLIER_CD ,
	          PRODUCTION_MONTH ,
	          PO_DATE ,
	          PO_TYPE ,
	          SUPPLIER_PLANT ,
	          EXCHANGE_RATE ,
	          PO_CURR ,
	          TOTAL_AMOUNT ,
	          STATUS_CD ,
	          REQUIRED_DT ,
	          --DELETION_FLAG ,
	          --REFF_INV_NO ,
	          --REFF_INV_DT ,
	          CREATED_BY ,
	          CREATED_DT 
	        )
	VALUES  ( @@PO_NO , -- PO_NO - varchar(30)
	          @@SUPPLIER_CD , -- SUPP_CD - varchar(6)
	          @@PRODUCTION_MONTH , -- PRODUCTION_MONTH - varchar(6)
	          @@PO_DATE, -- datetime
	          '1' , -- PO_TYPE - char(1)
	          @@SUPPLIER_PLANT , -- SUPPLIER_PLANT - char(1)	          
	          @@EXCHANGE_RATE , -- EXCHANGE_RATE - numeric
	          @@CURRENCY_CD , -- PO_CURR - nchar(10)
	          @@TOTAL_AMOUNT , -- TOTAL_AMOUNT - numeric
	          10 , -- STATUS_CD - int
	          @@REQUIRED_DT, -- datetime
	          --0 , -- DELETION_FLAG - int
	          --'' , -- REFF_INV_NO - varchar(20)
	          --'2013-06-25 06:30:56' , -- REFF_INV_DT - datetime
	          @@CREATED_BY , -- CREATED_BY - varchar(20)
	          GETDATE() -- CREATED_DT - datetime
	        )
END

INSERT INTO TB_R_PO_D
        ( PO_NO ,
          PO_ITEM ,
          PR_NO ,
          PR_ITEM ,
          MAT_NO ,
          SOURCE_TYPE ,
          PROD_PURPOSE_CD ,
          PLANT_CD ,
          SLOC_CD ,
          PART_COLOR_SUFFIX ,
          PACKING_TYPE ,
          PO_QTY_ORIGINAL ,
          PO_MAT_PRICE ,
          REQUIRED_DT ,
          CURRENCY_CD ,
          AMOUNT ,
          CREATED_BY ,
          CREATED_DT 
        )
VALUES  ( @@PO_NO , -- PO_NO - varchar(30)
          @@PO_ITEM , -- PO_ITEM - varchar(5)
          @@PR_NO , -- PR_NO - varchar(8)
          @@PR_ITEM , -- PR_ITEM - varchar(5)
          @@MAT_NO , -- MAT_NO - varchar(23)
          @@SOURCE_TYPE , -- SOURCE_TYPE - varchar(1)
          @@PROD_PURPOSE_CD , -- PROD_PURPOSE_CD - varchar(5)
          @@PLANT_CD , -- PLANT_CD - varchar(4)
          @@SLOC_CD , -- SLOC_CD - nchar(10)
          @@PART_COLOR_SUFFIX , -- PART_COLOR_SUFFIX - varchar(2)
          @@PACKING_TYPE , -- PACKING_TYPE - varchar(1)
          @@PO_QTY_ORIGINAL , -- PO_QTY_ORIGINAL - int
          @@PO_MAT_PRICE , -- PO_MAT_PRICE - numeric
          @@REQUIRED_DT , -- REQUIRED_DT - datetime
          @@CURRENCY_CD , 
          @@TOTAL_AMOUNT ,         
          @@CREATED_BY , -- CREATED_BY - varchar(20)
          GETDATE() -- CREATED_DT - datetime
        )


DECLARE 
	@@newRowId AS INT		
	,@@manifestNo AS VARCHAR(16) = @5
	,@@processId AS BIGINT = @22
	,@@sessionId AS INT = @23
	,@@userName AS VARCHAR(20) = @7
	,@@process AS VARCHAR(6) = @22
	,@@seqNo AS INT = @23

IF (@16 = '1')
BEGIN
	IF NOT EXISTS(
		SELECT *
		FROM TB_R_LOG_TRANSACTION_H
		WHERE
			PROCESS_ID = @@processId
			AND PROCESS_ID_ICS = @@sessionId
			AND MANIFEST_NO = @@manifestNo
	)
	BEGIN
		-- INSERT LOG HEADER TABLE
		INSERT INTO TB_R_LOG_TRANSACTION_H
					([PROCESS_ID]
					,[PROCESS_ID_ICS]
					,[MANIFEST_NO]
					,[CREATED_BY]
					,[CREATED_DATE]
					,[CHANGED_BY]
					,[CHANGED_DATE])
		VALUES (
			@@processId,
			@@sessionId,
			@@manifestNo,
			@@userName,
			GETDATE(),
			NULL,
			NULL)
						
		SET @@newRowId = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		SELECT @@newRowId = LOG_ID_H
		FROM TB_R_LOG_TRANSACTION_H
		WHERE
			PROCESS_ID = @@processId
			AND PROCESS_ID_ICS = @@sessionId
			AND MANIFEST_NO = @@manifestNo
	END

	-- INSERT LOG DETAIL TABLE
	INSERT INTO TB_R_LOG_TRANSACTION_D
				([LOG_ID_H]
				,[SEQ_NO]
				,[SYSTEM_SOURCE]
				,[USR_ID]
				,[MOVEMENT_TYPE]
				,[PROCESS_DT]
				,[POSTING_DT]
				,[MANIFEST_NO]
				,[KANBAN_ORER_NO]
				,[PROD_PURPOSE_CD]
				,[SOURCE_TYPE]
				,[PART_NO]
				,[SUPP_CD]
				,[DOCK_CD]
				,[PLANT_CD]
				,[SLOC_CD]
				,[LOCATION]
				,[ERR_CD]
				,[ERR_MESSAGE]
				,[CREATED_BY]
				,[CREATED_DT]
				,[CHANGED_BY]
				,[CHANGED_DT])
	VALUES( 
		@@newRowId,
		@@seqNo,
		@0,
		@1,
		@2,
		@3,
		@4,
		@5,
		@6,
		@7,
		@8,
		@9,
		@10,
		@11,
		@12,
		@13,
		NULL,
		@17,
		@19,
		@@userName,
		GETDATE(),
		@@userName,
		GETDATE())

	-- INSERT INTO MANIFEST DETAIL
	
END 