DECLARE
	@@PROC_ID VARCHAR(12),
	@@PROCESS_ID BIGINT,
	@@LAST_PROCESS_ID VARCHAR(12),
	@@TODAY VARCHAR(8),
	@@NOW DATETIME,
	@@CURR_NO INT,
	@@CURR_PROCESS_ID VARCHAR(12),
	@@LOG_ID_H BIGINT
SELECT @@TODAY = CONVERT(VARCHAR(8), GETDATE(), 112);

--GET LAST PROCESS ID
SELECT @@LAST_PROCESS_ID = ISNULL(MAX(PROCESS_ID), '1') FROM TB_T_LPOP_COMPLETENESS_CHECK
WHERE SUBSTRING(CAST(PROCESS_ID AS VARCHAR(12)), 1, 8) = @@TODAY;
IF @@LAST_PROCESS_ID = '1'
BEGIN
	SELECT @@CURR_NO = 1;
END
ELSE
BEGIN
	SELECT @@CURR_NO = CAST(SUBSTRING(@@LAST_PROCESS_ID,9,LEN(@@LAST_PROCESS_ID)) AS INT) + 1;
END;

--GET NEXT PROCESS ID
SELECT @@CURR_PROCESS_ID = RIGHT(REPLICATE('0', 4)+ CAST(@@CURR_NO AS VARCHAR(4)), 4);
SELECT @@PROC_ID = @@TODAY + @@CURR_PROCESS_ID;
SELECT @@PROCESS_ID = CAST(@@PROC_ID AS BIGINT);

SELECT @@PROCESS_ID

INSERT INTO dbo.TB_T_LPOP_COMPLETENESS_CHECK
        ( PROCESS_ID ,
          PROCESS_ID_ICS ,
          SUPP_CD ,
          PROD_MONTH ,
          ORI_MAT_NO ,
          ICS_MAT_NO ,
          DOCK_CD ,
          PROD_PURPOSE_CD ,
          SOURCE_TYPE ,
          PLANT_CD ,
          SLOC_CD ,
          PART_COLOR_SFX ,
          PIC_MODULE ,
          PIC_AREA ,
          CREATED_BY ,
          CREATED_DT ,
          ERR_STATUS
        )
VALUES  ( @@PROCESS_ID , -- PROCESS_ID - bigint
          @0 , -- PROCESS_ID_ICS - bigint
          @1 , -- SUPP_CD - varchar(6)
          @2 , -- PROD_MONTH - varchar(6)
          @3 , -- ORI_MAT_NO - varchar(30)
          @4 , -- ICS_MAT_NO - varchar(23)
          @5 , -- DOCK_CD - varchar(6)
          @6 , -- PROD_PURPOSE_CD - varchar(5)
          @7 , -- SOURCE_TYPE - varchar(1)
          @8 , -- PLANT_CD - varchar(4)
          @9 , -- SLOC_CD - varchar(6)
          @10 , -- PART_COLOR_SFX - varchar(2)
          @11 , -- PIC_MODULE - varchar(20)
          @12 , -- PIC_AREA - varchar(20)
          @13 , -- CREATED_BY - varchar(20)
          GETDATE() , -- CREATED_DT - datetime
          0  -- ERR_STATUS - int
        )