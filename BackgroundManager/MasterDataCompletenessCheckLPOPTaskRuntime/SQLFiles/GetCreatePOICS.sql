IF NOT EXISTS ( SELECT  1
                FROM    TB_R_LPOP_CONFIRMATION
                WHERE   PRODUCTION_MONTH = @0
                        AND PO_CREATION_STATUS = '2' ) 
        BEGIN
                SELECT  SUPPLIER_CD suppCode ,
                        PACK_MONTH prodMonth ,
                        PART_NO partNo ,
                        DOCK_CD dockCode ,
                        sourceType = 1 ,
                        prodPurposeCode = 'D'
                FROM    TB_R_LPOP
                WHERE   PACK_MONTH = @0
                        AND VERS = @1
                GROUP BY SUPPLIER_CD ,
                        PACK_MONTH ,
                        PART_NO ,
                        DOCK_CD
        END