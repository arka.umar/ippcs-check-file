﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;
 
using System.Reflection;
using Toyota.Common.Logging.Sink;
using MasterDataCompletenessCheckLPOPTaskRuntime.Models;
using System.Data;

namespace MasterDataCompletenessCheckLPOPTaskRuntime
{
    class MasterDataCompletenessCheckTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        { 
            //LogProvider logProvider = new LogProvider("101", "111", AuthorizedUser);
            //ILogSession sessionA = logProvider.CreateSession();

            string hasil = "Done";

            List<LPOPPurchaseOrder> tempList;
            List<LPOPPurchaseOrder> list = new List<LPOPPurchaseOrder>();

            //DECLARE VARIABLE AS PARAMETER FOR UPDATE DATA
            string ProductionMonth = parameters.Get("ProdMonth");
            string Timing = parameters.Get("Timing"); // Always Firm
            string Username = parameters.Get("Username");

            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(MasterDataCompletenessCheckTaskRuntime)), "MasterDataCompletenessCheckTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Process Create PO to ICS has been started..!!!",
                Severity = LoggingSeverity.Info
            });

            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Process Taking Data from TB_LPOP has been started..!!!",
                Severity = LoggingSeverity.Info
            }); 
            try
            {
                tempList = new List<LPOPPurchaseOrder>();
                ProductionMonth = Convert.ToDateTime(ProductionMonth).Year.ToString() + Convert.ToDateTime(ProductionMonth).Month.ToString("0#");
                tempList = db.Query<LPOPPurchaseOrder>(DatabaseManager.LoadQuery("GetCreatePOICS"), new object[] { ProductionMonth, Timing, Username }).ToList<LPOPPurchaseOrder>();
                foreach (LPOPPurchaseOrder lpoppo in tempList)
                {
                    list.Add(lpoppo);
                }
            }
            catch (Exception ex)
            {
                hasil += "Production Month " + ProductionMonth + " and Timing " + Timing + ": " + ex.Message + "\n"; 
                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = hasil,
                    Severity = LoggingSeverity.Error
                }); 
            }
            db.Close(); 
            GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
            ServiceResult result;

            try
            {
                gateway.Open();
            }
            catch (Exception ex)
            {
                hasil = string.Empty;
                hasil = ex.Message;

                gateway.Abort();
                gateway = new GatewayService.WebServiceImplClient();
                gateway.Open();
            }


            /* Start Process 2 transfer data */
            string jsonResult;
            ServiceParameters sp = new ServiceParameters();
            string sessionID;
            sp.Add("state", "init");
            result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));
            if (result.Status != ServiceResult.STATUS_ERROR)
            {
                sessionID = result.Value.ToString();

                sp.Clear();
                sp.Add("state", "transfer");
                sp.Add("sessionId", result.Value);
                sp.Add("data", String.Empty);

                const int MAX_DATA_TRANSFERRED = 10;
                int dataLength = list.Count;
                //int lastIndex = dataLength - 1;
                int lastIndex = dataLength;
                int counter = 0;
                int fetchSize;
                int indexDifference;
                bool looping = true;
                bool transferError = false;
                while (looping)
                {
                    indexDifference = (lastIndex - counter);
                    if (indexDifference < MAX_DATA_TRANSFERRED)
                    {
                        fetchSize = indexDifference;
                    }
                    else
                    {
                        fetchSize = MAX_DATA_TRANSFERRED;
                    }

                    tempList = list.GetRange(counter, fetchSize);
                    jsonResult = JSON.ToString<List<LPOPPurchaseOrder>>(tempList);

                    sp.Remove("data");
                    sp.Add("data", jsonResult);
                    sp.Add("userId", Username);
                    result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));
                    if (result.Status == ServiceResult.STATUS_SUCCESS)
                    {
                        counter += (fetchSize);
                    }
                    else
                    {
                        transferError = true;
                        looping = false;
                    }

                    if (counter == lastIndex)
                    {
                        looping = false;
                    }

                }

                /* End Process 2 */
                if (transferError)
                {
                    DefaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = "Looping Data " + result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString(),
                        Severity = LoggingSeverity.Error
                    });  
                    throw new Exception(result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString());
                }

                /* Start Process 3 process*/
                sp.Clear();
                sp.Add("state", "process");
                sp.Add("sessionId", sessionID);
                sp.Add("prodMonth", ProductionMonth);
                sp.Add("userId", Username);
                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));
                /* End Process 3 */
                sp.Clear();
                if (result.Status != ServiceResult.STATUS_ERROR)
                {
                    /* Start Process 4 inquiry */

                    int totalPage = Convert.ToInt32(result.Value != null ? result.Value : 0);
                    sp.Clear();
                    sp.Add("state", "inquiry");
                    sp.Add("sessionId", sessionID);
                    sp.Add("processId", "101");

                    //int seqLogDetail = 0;
                    for (int i = 1; i <= totalPage; i++)
                    {
                        try
                        {
                            sp.Add("currentPage", i);
                            sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                            sp.Add("prodMonth", ProductionMonth);
                            result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("Timeout") || ex.Message.Contains("timeout"))
                            {
                                sp.Add("currentPage", i);
                                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                                sp.Add("prodMonth", ProductionMonth);
                                result = ServiceResult.Create(gateway.execute("creation", "purchaseOrder", sp.ToString()));
                            }
                        }

                        //sp.Clear();
                        if (result.Status != ServiceResult.STATUS_ERROR)
                        {
                            if (result.Value == null)
                            {
                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "ICS Doesn't return data",
                                    Severity = LoggingSeverity.Error
                                }); 
                                throw new NoNullAllowedException("ICS doesn't return data.");
                            }

                            List<LPOPPurchaseOrderFinish> values = new List<LPOPPurchaseOrderFinish>();
                            values = JSON.ToObject<List<LPOPPurchaseOrderFinish>>(result.Value.ToString()); 
                            string prodmonth = string.Empty;
                            foreach (LPOPPurchaseOrderFinish e in values)
                            {
                                prodmonth = Convert.ToDateTime(e.prodMonth, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID")).Year.ToString()
                                    + Convert.ToDateTime(e.prodMonth, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID")).Month.ToString().PadLeft(2, '0');

                                db.Execute(DatabaseManager.LoadQuery("InsertLPOPCompletenessCheck"), new object[] { 
                                            sessionID, 
                                            e.suppCode, 
                                            prodmonth, 
                                            null, // partno
                                            e.matNo,
                                            e.dockCode,
                                            e.prodPurposeCode,
                                            e.sourceType,
                                            e.plantCode,
                                            e.slocCode,
                                            e.partColorSfx,
                                            null, //picmodule
                                            null, //picarea
                                            Username
                                    });
                            }
                        }
                        else
                        {
                            DefaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString(),
                                Severity = LoggingSeverity.Error
                            });  
                            throw new Exception(result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString());
                        }

                        if (result.Status != ServiceResult.STATUS_SUCCESS)
                        {
                            transferError = true;
                        }
                    }
                    /* End Process 4 */

                    // NANTI DILANJUTIN 
                    /* Start Process 5 finish */
                    //sp.Clear();
                    //sp.Add("state", "finish");
                    //sp.Add("sessionId", sessionID);
                    //result = ServiceResult.Create(gateway.Execute("creation", "goodsReceive", sp.ToString()));
                    //sp.Clear();
                    //if (result.Status == ServiceResult.STATUS_ERROR)
                    //{
                    //    sessionA.Write("MPCS00002ERR", new string[] { result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString(), "", "", "" });
                    //    sessionA.SetStatus(Toyota.Common.Web.Util.ProgressStatus.PROCESS_STATUS_ERROR);
                    //    sessionA.Commit();

                    //    throw new Exception(result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString());
                    //}
                    /* End Process 5 */
                }
                else
                {
                    DefaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString(),
                        Severity = LoggingSeverity.Error
                    }); 
                    throw new Exception(result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString());
                }
                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Process taking data from ICS has been finished..!!!",
                    Severity = LoggingSeverity.Info
                });
            }
            else
            {
                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString(),
                    Severity = LoggingSeverity.Error
                }); 
                throw new Exception(result.MappedValues[ServiceResult.MAPPING_KEY_ERROR].ToString());
            }
            db.Close();
            gateway.Close();
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Process sending data to ICS has been finished..!!!",
                Severity = LoggingSeverity.Info
            });  
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Process Create PO to ICS has been finished..!!!",
                Severity = LoggingSeverity.Info
            });
            SetProgress(100);
        }
    }
}
