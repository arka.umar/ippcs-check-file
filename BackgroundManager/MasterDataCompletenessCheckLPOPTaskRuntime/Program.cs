﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;
 
using System.Reflection;

namespace CreatePOPostingTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            MasterDataCompletenessCheckLPOPTaskRuntime.MasterDataCompletenessCheckTaskRuntime rtm = new MasterDataCompletenessCheckLPOPTaskRuntime.MasterDataCompletenessCheckTaskRuntime();

            rtm.ExecuteExternal(args);

            #region New passing parameter method
            //BackgroundTaskParameter parameters = new BackgroundTaskParameter();
            //parameters.Add("ProdMonth", "08.2013");
            //parameters.Add("Timing", "Firm");
            //parameters.Add("Username", "FID.Iman");
            //rtm.ExecuteExternal(new String[] { parameters.ToString() });            
            #endregion
        }
    }
}
