﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterDataCompletenessCheckLPOPTaskRuntime.Models
{
    public class LPOPPurchaseOrderFinish
    {
        public string docType { set; get; }
        public string matNo { set; get; }
        public string poMatPrice { set; get; }
        public string paymentMethodCode { set; get; }
        public string prodPurposeCode { set; get; }
        public string prodMonth { set; get; }
        public string containerNo { set; get; }
        public string errDesc { set; get; }
        public string prNo { set; get; }
        public string errMsg { set; get; }
        public string specialProcurementTypeCode { set; get; }
        public string poExchangeRate { set; get; }
        public string suppCode { set; get; }
        public string qtyN3 { set; get; }
        public string reffDoc { set; get; }
        public string subcontractFlag { set; get; }
        public string slocCode { set; get; }
        public string poCurr { set; get; }
        public string calculationSchemaCode { set; get; }
        public string reffDocDate { set; get; }
        public string slocCd { set; get; }
        public string poItemNo { set; get; }
        public string poGrQty { set; get; }
        public string sourceType { set; get; }
        public string taxCode { set; get; }
        public string invWoGrFlag { set; get; }
        public string qtyN { set; get; }
        public string reffInvNo { set; get; }
        public string unlimitedFlag { set; get; }
        public string poQtyNew { set; get; }
        public string packingType { set; get; }
        public string message { set; get; }
        public string plantCode { set; get; }
        public string poQtyOriginal { set; get; }
        public string qtyN2 { set; get; }
        public string errCode { set; get; }
        public string dockCode { set; get; }
        public string valStatus { set; get; }
        public string qtyN1 { set; get; }
        public string poNo { set; get; }
        public string matDesc { set; get; }
        public string paymentTermCode { set; get; }
        public string deliveryDate { set; get; }
        public string prItem { set; get; }
        public string unitOfMeasureCode { set; get; }
        public string note { set; get; }
        public string tolerancePercentage { set; get; }
        public string partColorSfx { set; get; }
        public string poNote { set; get; }
        public string reffInvDate { set; get; }
        public string docDate { set; get; }
    }
}
