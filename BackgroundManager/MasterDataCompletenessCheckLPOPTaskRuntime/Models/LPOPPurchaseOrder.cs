﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterDataCompletenessCheckLPOPTaskRuntime.Models
{
    class LPOPPurchaseOrder
    {
        public string suppCode { set; get; }
        public string prodMonth { set; get; }
        public string partNo { set; get; }
        public string dockCode { set; get; }
        public string sourceType { set; get; }
        public string prodPurposeCode { set; get; } 
    }
}
