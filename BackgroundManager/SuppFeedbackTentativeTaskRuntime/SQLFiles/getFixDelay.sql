﻿declare @@SupCode varchar(10) =@0
declare @@ProdMonth varchar(10)=@1
declare @@Supplant varchar(10)=@2


SELECT 	SUPPLIER_CD as suplierCode ,
		a.PRODUCTION_MONTH as productionMonth, 
		b.FEEDBACK_DUE_DATE as tentativeDate,
		a.SUPPLIER_PLANT as supplant
 
FROM 	TB_R_MONTHLY_FORECAST a 
		INNER  JOIN dbo.TB_R_LPOP_CONFIRMATION b 
		ON a.PRODUCTION_MONTH=b.PRODUCTION_MONTH
		
WHERE 	F_FEEDBACK_DT is null 
		AND a.PRODUCTION_MONTH=@@ProdMonth
		AND a.SUPPLIER_CD=@@SupCode 
		AND a.SUPPLIER_PLANT=@@Supplant
		AND b.LPOP_TIMING='F'
