﻿DECLARE @@ProdMonth VARCHAR(10)=@0
SELECT  supplier_cd as 'suplierCode' , 
		SUPPLIER_PLANT as supplierplant 
FROM 	TB_R_MONTHLY_FORECAST 
WHERE 
		T_FEEDBACK_DT is null 
		and PRODUCTION_MONTH= @@ProdMonth
