﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using SuppFeedbackTentativeTaskRuntime.Model;
using System.Reflection;
using Toyota.Common.Logging.Sink;
using System.IO;
using System.Configuration;
using Toyota.Common.Web.Notifcation.JobMonitoring;
using Toyota.Common.Web.Ioc;
using System.Globalization;


namespace SuppFeedbackTentativeTaskRuntime
{
    public class SuppFeedbackTentativeTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(SuppFeedbackTentativeTaskRuntime)), "SuppFeedbackTentativeTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            IBootstrap bootstrap = Bootstrap.GetInstance();
            bootstrap.RegisterProvider<Toyota.Common.Web.Database.IDBContextManager>(typeof(Toyota.Common.Web.Database.Petapoco.PetaPocoContextManager), true);

            string productionMonth = parameters.Get("productionMonth");
            string timing = parameters.Get("timing");
            if (productionMonth == "" || timing == "")
            {
                Console.WriteLine("args is null"); // Check for null array
            }
            else
            {
                if (timing == "T")
                {
                    SetProgress(5);
                    List<listSupplier> listSuplier = new List<listSupplier>();
                    List<listTentative> listtentative = new List<listTentative>();

                    string dateExpired;
                    string attachID = "";
                    string supplierCode;
                    string supplierplant = "";
                    string forecastvol;

                    string functionID = ConfigurationManager.AppSettings["functionID"];
                    string contentID = ConfigurationManager.AppSettings["contentID"];
                    string fileLocation = ConfigurationManager.AppSettings["fileLocation"].ToString();
                    ArgumentParameter param = new ArgumentParameter();
                    string format = "dd MMM yyyy";

                    listSuplier = db.Query<listSupplier>(DatabaseManager.LoadQuery("GetListSuplier"), new object[] { productionMonth }).ToList<listSupplier>();

                    foreach (listSupplier str in listSuplier)
                    {
                        listtentative = db.Query<listTentative>(DatabaseManager.LoadQuery("getTentativeDelay"), new object[] { str.suplierCode, productionMonth, str.supplierplant }).ToList<listTentative>();

                        foreach (listTentative dt in listtentative)
                        {
                            productionMonth = dt.productionMonth;
                            dateExpired = dt.tentativeDate.ToString(format);
                            supplierCode = dt.suplierCode;
                            supplierplant = dt.supplant;

                            DateTime enteredDate = DateTime.Parse((productionMonth.Substring(0, 4)) + " - " + (productionMonth.Substring(4, 2)) + "- 01");
                            string prodmonth = enteredDate.ToString("MMM yyyy", CultureInfo.InvariantCulture);
                            DateTime plusone = enteredDate.AddMonths(1);
                            DateTime plusfour = enteredDate.AddMonths(4);
                            forecastvol = plusone.ToString("MMM yyyy", CultureInfo.InvariantCulture) + "- " + plusfour.ToString("MMM yyyy", CultureInfo.InvariantCulture);


                            param.Clear();
                            param.Add("@suppCode", supplierCode);
                            param.Add("@production_month", prodmonth);
                            param.Add("@date", dateExpired);
                            param.Add("@plantcd", supplierplant);
                            param.Add("@forecastvol", forecastvol);


                            ParamArray keyParam = new ParamArray();
                            keyParam.Add("functionid", functionID);
                            keyParam.Add("roleid", supplierCode);
                            keyParam.Add("contentid", contentID);
                            keyParam.Add("attachmentid", attachID);
                            DirectJobProvider.Execute(keyParam, param);
                            System.Threading.Thread.Sleep(25);
                        }
                    }
                    db.Close();
                    System.Threading.Thread.Sleep(100);
                    SetProgress(100);
                }

                else 
                {
                    SetProgress(5);
                    List<listSupplier> listSuplier = new List<listSupplier>();
                    List<listFix> listtentative = new List<listFix>();

                    string dateExpired;
                    string attachID = "";
                    string SupplierCode = "";
                    string supplierplant = "";
                    string forecastvol;

                    string functionID = ConfigurationManager.AppSettings["functionID"];
                    string contentID = ConfigurationManager.AppSettings["contentID"];
                    string fileLocation = ConfigurationManager.AppSettings["fileLocation"].ToString();
                    ArgumentParameter param = new ArgumentParameter();
                    //ArgumentParameter keyParam = new ArgumentParameter();
                    string format = "dd MMM yyyy";
                    listSuplier = db.Query<listSupplier>(DatabaseManager.LoadQuery("GetListSuplierfirm"), new object[] { productionMonth }).ToList<listSupplier>();

                    //SuppFeedbackFirmEmail email = new SuppFeedbackFirmEmail();
                    foreach (listSupplier str in listSuplier)
                    {
                        listtentative = db.Query<listFix>(DatabaseManager.LoadQuery("getFixDelay"), new object[] { str.suplierCode, productionMonth, str.supplierplant }).ToList<listFix>();
                        foreach (listFix dt in listtentative)
                        {
                            productionMonth = dt.productionMonth;
                            dateExpired = dt.tentativeDate.ToString(format);
                            SupplierCode = dt.suplierCode;
                            supplierplant = dt.supplant;

                            DateTime enteredDate = DateTime.Parse((productionMonth.Substring(0, 4)) + " - " + (productionMonth.Substring(4, 2)) + "- 01");
                            string prodmonth = enteredDate.ToString("MMM yyyy", CultureInfo.InvariantCulture);
                            DateTime plusone = enteredDate.AddMonths(1);
                            DateTime plusfour = enteredDate.AddMonths(4);
                            forecastvol = plusone.ToString("MMM yyyy", CultureInfo.InvariantCulture) + "- " + plusfour.ToString("MMM yyyy", CultureInfo.InvariantCulture);


                            param.Clear();
                            param.Add("@suppCode", SupplierCode);
                            param.Add("@production_month", prodmonth);
                            param.Add("@date", dateExpired);
                            param.Add("@plantcd", supplierplant);
                            param.Add("@forecastvol", forecastvol);

                            ParamArray keyParam = new ParamArray();
                            //keyParam.Clear();
                            keyParam.Add("functionid", functionID);
                            keyParam.Add("roleid", SupplierCode);
                            keyParam.Add("contentid", "31");
                            keyParam.Add("attachmentid", attachID);

                            DirectJobProvider.Execute(keyParam, param);
                            //email.Send(keyParam, param, null);
                            System.Threading.Thread.Sleep(25);
                        }
                    }
                    db.Close();
                    System.Threading.Thread.Sleep(100);
                    SetProgress(100);
                }





            }
        }

    }
}

