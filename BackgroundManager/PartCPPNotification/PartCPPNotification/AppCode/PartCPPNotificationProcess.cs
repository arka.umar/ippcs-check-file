﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PartCPPNotification.Models;
using PartCPPNotification.Helper.DBConfig;
using PartCPPNotification.Helper.Base;
using System.IO;
using System.Reflection;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.IO.Compression;
using PartCPPNotification.Helper.Util;
using System.Configuration;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.DDF;
using PartCPPNotification.Helper.Report;
using System.Net;
using System.Net.Mail;
using PartCPPNotification.Helper.FTP;

namespace PartCPPNotification.AppCode
{
    public class PartCPPNotificationProcess : BaseBatch
    {
        public static ConnectionDescriptor connDesc = DBContextHelper.Instance.getConfiguration();

        public override void ExecuteBatch()
        {
            Console.WriteLine("start process Part CPP Notification");

            #region Variabel
            string loc = "Part PCC";
            string module = "7";
            string function = "71023";
            int flag = 0;

            SqlConnection connect = new SqlConnection(connDesc.ConnectionString);
            Int32 count = 0;
            Common getProc = new Common();

            string resultReport = string.Empty;
            string resultEmail = string.Empty;
            string[] message;

            #endregion

            string fileLogPath = ConfigurationManager.AppSettings["logpath"];
            string fileLogPathWithFilename = string.Concat(fileLogPath, string.Concat("LogPartCPPNotif", DateTime.Now.ToString("yyyyMMddHHmmss")));
            string fileContent = "";

            fileContent = string.Concat(DateTime.Now.ToString(), " -- Start log");

            #region cek DB
            try
            {
                // Connect to database.
                connect.Open();
                Console.WriteLine("Database connection successful.");
                SqlCommand comm1 = new SqlCommand("SELECT COUNT(1) FROM TB_M_DRAFT_MATERIAL_PRICE WHERE CPP_FLAG ='Y' AND PC_NO IS NULL AND DRAFT_DF = 'N'", connect);
                count = Convert.ToInt32(comm1.ExecuteScalar());
                connect.Close();

                fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- Database connection successful. ");

            }
            catch (Exception e)
            {
                // Confirm unsuccessful connection and stop program execution.
                Console.WriteLine("Database connection unsuccessful.");
                fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- Database connection unsuccessful. ");
                System.Environment.Exit(0);

            }
            #endregion

            getProc.MSG_TXT = "Start Part CPP Notification";
            getProc.LOCATION = loc;
            getProc.PID = 0;
            getProc.MSG_ID = "MTOCSTD024I";
            getProc.MSG_TYPE = "I";
            getProc.MODULE_ID = module;
            getProc.FUNCTION_ID = function;
            getProc.USER_ID = "SYSTEM";
            getProc.PROCESS_STS = 0;
            Int64 PID = CommonDBHelper.Instance.CreateLog(getProc);

            try
            {
                IDBContext db = dbManager.GetContext();

                if (count > 0)
                {
                    fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- Fetching Part CPP data ");
                    getProc.MSG_TXT = "Fetching Part CPP data";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    resultReport = this.DownloadData();

                    if (resultReport.StartsWith("Error"))
                    {
                        flag = 1;
                        message = resultReport.Split('|');

                        getProc.MSG_TXT = message[1].ToString();
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                    }
                    else
                    {
                        getProc.FUNCTION_ID = "PART_CPP_NOTIF";
                        getProc.SYSTEM_CD = "MAIL_TO";
                        string mail_to = CommonDBHelper.Instance.getSystemVal(getProc);

                        getProc.FUNCTION_ID = "PART_CPP_NOTIF";
                        getProc.SYSTEM_CD = "MAIL_CC";
                        string mail_cc = CommonDBHelper.Instance.getSystemVal(getProc);

                        getProc.FUNCTION_ID = "PART_CPP_NOTIF";
                        getProc.SYSTEM_CD = "MAIL_SUBJECT";
                        string mail_subject = CommonDBHelper.Instance.getSystemVal(getProc);

                        getProc.FUNCTION_ID = "PART_CPP_NOTIF";
                        getProc.SYSTEM_CD = "MAIL_BODY";
                        string mail_body = CommonDBHelper.Instance.getSystemVal(getProc);

                        getProc.MSG_TXT = "Send Email proccess is started";
                        getProc.LOCATION = loc;
                        getProc.PID = PID;
                        getProc.MSG_ID = "MTOCSTD024I";
                        getProc.MSG_TYPE = "I";
                        getProc.MODULE_ID = module;
                        getProc.FUNCTION_ID = function;
                        getProc.PROCESS_STS = 0;
                        getProc.USER_ID = "SYSTEM";
                        PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                        Object[] obParams1 = new Object[5];
                        obParams1[0] = mail_to;
                        obParams1[1] = mail_cc;
                        obParams1[2] = mail_subject;
                        obParams1[3] = mail_body;
                        obParams1[4] = resultReport;
                        resultEmail = this.SendMail(obParams1);

                        if (resultEmail != "SUCCESS")
                        {
                            flag = 1;

                            getProc.MSG_TXT = resultEmail;
                            getProc.LOCATION = loc;
                            getProc.PID = PID;
                            getProc.MSG_ID = "MTOCSTD024I";
                            getProc.MSG_TYPE = "I";
                            getProc.MODULE_ID = module;
                            getProc.FUNCTION_ID = function;
                            getProc.PROCESS_STS = 0;
                            getProc.USER_ID = "SYSTEM";
                            PID = CommonDBHelper.Instance.CreateLogDetail(getProc);

                            fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- Error has occured : ", resultEmail);

                        }

                    }

                }
                else
                {
                    flag = 1;

                    getProc.MSG_TXT = "Part CPP data not found";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogDetail(getProc);
                }

                if (flag == 0)
                {
                    getProc.MSG_TXT = "Part CPP Notification Process is finished successfully";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 0;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                    Console.WriteLine("process is finished succesfully");
                    fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- Process is finished succesfully -- ");
                }
                else
                {
                    getProc.MSG_TXT = "Part CPP Notification Process is finished with error";
                    getProc.LOCATION = loc;
                    getProc.PID = PID;
                    getProc.MSG_ID = "MTOCSTD024I";
                    getProc.MSG_TYPE = "I";
                    getProc.MODULE_ID = module;
                    getProc.FUNCTION_ID = function;
                    getProc.PROCESS_STS = 1;
                    getProc.USER_ID = "SYSTEM";
                    PID = CommonDBHelper.Instance.CreateLogFinish(getProc);

                    Console.WriteLine("process is finished with error");
                    fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- Process is finished with error -- ");
                }
            }
            catch (Exception ex)
            {
                fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- Error has occured : ", ex.Message);
            }
            finally {
                Common getFileLoc = new Common();

                getFileLoc.FUNCTION_ID = "PART_CPP_NOTIF";
                getFileLoc.SYSTEM_CD = "FILE_PATH";
                string DownloadDirectory = CommonDBHelper.Instance.getSystemVal(getFileLoc);

                DirectoryHelper.Instance.DropAllFiles(DownloadDirectory);
            }


            fileContent = string.Concat(fileContent, "\r\n", DateTime.Now.ToString(), " -- End log");
            File.WriteAllText(@fileLogPathWithFilename, fileContent);

            Console.WriteLine("end process Part CPP Notification");

        }

        public string DownloadData()
        {
            string result = String.Empty;
            try
            {

                string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Template\PartCPPNotification.xls";

                FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                HSSFWorkbook workbook = new HSSFWorkbook(stream);
                HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Data");
                ICellStyle style = workbook.CreateCellStyle();
                IFont font = workbook.CreateFont();

                IEnumerable<CPPNotifObj> DownloadListDetail = CommonDBHelper.Instance.DownloadData().ToList();

                int startrow = 5;
                int rowindex = startrow;
                int noData = 1;

                foreach (var row in DownloadListDetail)
                {
                    if (noData > 1)
                    {
                        NPOIHelper.CopyRow(sheet, startrow, rowindex);
                    }

                    NPOIHelper.SetCellValueNumeric(sheet, rowindex, 1, row.ROW_NUM);
                    NPOIHelper.SetCellValue(sheet, rowindex, 2, row.MC_CODE);
                    NPOIHelper.SetCellValue(sheet, rowindex, 3, row.MAT_NO);
                    NPOIHelper.SetCellValue(sheet, rowindex, 4, row.MAT_DESC);
                    NPOIHelper.SetCellValue(sheet, rowindex, 5, row.MINOR_CODE);
                    NPOIHelper.SetCellValue(sheet, rowindex, 6, row.PART_COLOR_SFX);
                    NPOIHelper.SetCellValue(sheet, rowindex, 7, row.PACKING_TYPE);
                    NPOIHelper.SetCellValue(sheet, rowindex, 8, row.PART_CATEGORY);
                    NPOIHelper.SetCellValue(sheet, rowindex, 9, row.PRICE_STATUS);
                    NPOIHelper.SetCellValue(sheet, rowindex, 10, row.SUPP_CD);
                    NPOIHelper.SetCellValue(sheet, rowindex, 11, row.SUPPLIER_PLANT_CD);
                    NPOIHelper.SetCellValue(sheet, rowindex, 12, row.SUPP_NAME);
                    NPOIHelper.SetCellValue(sheet, rowindex, 13, row.VALID_DT_FR);
                    NPOIHelper.SetCellValue(sheet, rowindex, 14, row.CURR_CD);
                    NPOIHelper.SetCellValue(sheet, rowindex, 15, row.PRICE_AMT);
                    NPOIHelper.SetCellValue(sheet, rowindex, 16, row.UOM);
                    NPOIHelper.SetCellValue(sheet, rowindex, 17, row.WARP_REF_NO);
                    NPOIHelper.SetCellValue(sheet, rowindex, 18, row.WARP_REF_COMMENT);
                    NPOIHelper.SetCellValue(sheet, rowindex, 19, row.WARP_REF_DATE);
                    NPOIHelper.SetCellValue(sheet, rowindex, 20, row.ECI_NO);
                    NPOIHelper.SetCellValue(sheet, rowindex, 21, row.CPP_FLAG);
                    NPOIHelper.SetCellValue(sheet, rowindex, 22, row.WARP_BUYER_CD);
                    NPOIHelper.SetCellValue(sheet, rowindex, 23, row.BUYER_NAME);
                    NPOIHelper.SetCellValue(sheet, rowindex, 24, row.ADD_DT);
                    NPOIHelper.SetCellValue(sheet, rowindex, 25, row.UPDATE_DT);
                    rowindex++;
                    noData++;
                }

                string reportName = "IPPCS_CPP_PART";
                string dateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                reportName = reportName + "_" + dateTime + ".xls";

                stream.Close();
                using (var exportData = new MemoryStream()) //binding to stream reader
                {
                    Common getFileLoc = new Common();

                    getFileLoc.FUNCTION_ID = "PART_CPP_NOTIF";
                    getFileLoc.SYSTEM_CD = "FILE_PATH";
                    string DownloadDirectory = CommonDBHelper.Instance.getSystemVal(getFileLoc);

                    var pathfile = Path.Combine(DownloadDirectory, reportName);

                    workbook.Write(exportData);
                    FileStream files = new FileStream(pathfile, FileMode.Create, FileAccess.Write);
                    exportData.WriteTo(files);
                    files.Close();


                    result = pathfile;
                }
            }
            catch (SqlException e)
            {
                result = "Error|" + e.Message.ToString();
            }
            catch (Exception e)
            {
                result = "Error|" + e.Message.ToString();
            }

            return result;
        }

        public string SendMail(Object[] d1)
        {
            string result = "SUCCESS";
            Common systemMaster = new Common();

            try
            {
                string SMTP_SERVER = ConfigurationManager.AppSettings["MailHost"];
                string SENDER = ConfigurationManager.AppSettings["MailSender"];

                string V_MAIL_TO = d1[0].ToString();
                string V_MAIL_CC = d1[1].ToString();
                string V_MAIL_SUBJECT = d1[2].ToString();
                string V_MAIL_BODY = d1[3].ToString();
                string V_ATTACH_FILE = d1[4].ToString();

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient(SMTP_SERVER);

                mail.From = new MailAddress(SENDER);
                mail.To.Add(V_MAIL_TO);
                if (V_MAIL_CC != "" && V_MAIL_CC != null)
                {
                    mail.CC.Add(V_MAIL_CC);
                }
                mail.Subject = V_MAIL_SUBJECT;
                mail.Body = V_MAIL_BODY;

                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(V_ATTACH_FILE);
                mail.Attachments.Add(attachment);

                //client.Port = 25; //gmail 587
                //client.Credentials = new System.Net.NetworkCredential(user, pwd);
                client.EnableSsl = false;

                client.Send(mail);
            }
            catch (SmtpException e)
            {
                result = e.Message.ToString();
            }
            catch (SqlException e)
            {
                result = e.Message.ToString();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }

            return result;
        }

        public override List<Common> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
