﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PartCPPNotification.Models;
using PartCPPNotification.Helper.DBConfig;
using PartCPPNotification.Helper.FTP;
using PartCPPNotification.Helper.Base;
using System.IO;
using System.Reflection;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.IO;
using System.IO.Compression;
using PartCPPNotification.Helper.Util;
using System.Configuration;
using System.Data.SqlClient;

namespace PartCPPNotification.Models
{
    public class CSVCreationBypass : BaseRepository<Common>
    {
        
        #region Singleton
        private CSVCreationBypass() { }
        private static CSVCreationBypass instance = null;
        public static CSVCreationBypass Instance
        {
            get
            {
                if (instance == null)
                {
                    SetConfig();
                    instance = new CSVCreationBypass();
                }

                return instance;
            }
        }
        #endregion

        public static ConnectionDescriptor connDesc = DBContextHelper.Instance.getConfiguration();

        public string generateCSV(string type)
        {
            string result = "";

            // Database connection variable.
            SqlConnection connect = new SqlConnection(connDesc.ConnectionString);

            try
            {
                // Connect to database.
                connect.Open();

                Console.WriteLine("Database connection successful.");
            }
            catch (Exception e)
            {
                // Confirm unsuccessful connection and stop program execution.
                Console.WriteLine("Database connection unsuccessful.");
                System.Environment.Exit(0);

            }

            // Export path and file.
            string fileFormatNm = CommonDBHelper.Instance.getFilename(type);

            string DownloadDirectory = "Temp/GR/" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "/";
            string exportPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DownloadDirectory);
            string exportCsv = fileFormatNm + ".csv";

            System.IO.Directory.CreateDirectory(exportPath);

            // Stream writer for CSV file.
            StreamWriter csvFile = null;

            // Check to see if the file path exists.
            if (Directory.Exists(exportPath))
            {
                try
                {
                    // Query text.
                    string sqlText =
                        " SELECT [PROCESS_KEY] "
                        + " ,convert(varchar, [PROCESS_ID]) [PROCESS_ID]"
                        + " ,[SYSTEM_SOURCE]"
                        + " ,[USR_ID]"
                        + " ,convert(varchar, [SEQ_NO]) [SEQ_NO]"
                        + " ,convert(varchar, [TRANS_ID]) [TRANS_ID]"
                        + " ,convert(varchar, [HEADER_INDEX]) [HEADER_INDEX]"
                        + " ,convert(varchar, [POSTING_DT], 104) [POSTING_DT]"
                        + " ,convert(varchar, [DOC_DT], 104) [DOC_DT]"
                        + " ,convert(varchar, [PROCESS_DT], 104) [PROCESS_DT]"
                        + " ,[REF_NO]"
                        + " ,[MOVEMENT_TYPE]"
                        + " ,[SPECIAL_STOCK_TYPE]"
                        + " ,[MAT_DOC_DESC]"
                        + " ,[IM_PERIOD]"
                        + " ,[IM_YEAR]"
                        + " ,[KANBAN_ORDER_NO]"
                        + " ,[PROD_PURPOSE_CD]"
                        + " ,[SOURCE_TYPE]"
                        + " ,[MAT_NO]"
                        + " ,[ORI_MAT_NO]"
                        + " ,[MAT_DESC]"
                        + " ,convert(varchar, [POSTING_QUANTITY]) [POSTING_QUANTITY]"
                        + " ,[PART_COLOR_SFX]"
                        + " ,[PACKING_TYPE]"
                        + " ,[UNIT_OF_MEASURE_CD]"
                        + " ,[ORI_UNIT_OF_MEASURE_CD]"
                        + " ,[BATCH_NO]"
                        + " ,[COIL_MAT_NO]"
                        + " ,convert(varchar, [COIL_QTY]) [COIL_QTY]"
                        + " ,[PLANT_CD]"
                        + " ,[SLOC_CD]"
                        + " ,[DOCK_CD]"
                        + " ,[MAT_CURR]"
                        + " ,[VALUATION_CLASS]"
                        + " ,[PRICE_CALCULATION_TYPE]"
                        + " ,convert(varchar, [ACTUAL_EXCHANGE_RATE]) [ACTUAL_EXCHANGE_RATE]"
                        + " ,[CASE_EXPLOSION_FLAG]"
                        + " ,[SUPP_CD]"
                        + " ,[ORI_SUPP_CD]"
                        + " ,[PO_NO]"
                        + " ,[PO_DOC_TYPE]"
                        + " ,convert(varchar, [PO_DOC_DT], 104) [PO_DOC_DT]"
                        + " ,[PO_ITEM_NO]"
                        + " ,convert(varchar, [PO_MAT_PRICE]) [PO_MAT_PRICE]"
                        + " ,[PO_CURR]"
                        + " ,convert(varchar, [PO_EXCHANGE_RATE]) [PO_EXCHANGE_RATE]"
                        + " ,convert(varchar, [GR_ORI_AMOUNT]) [GR_ORI_AMOUNT]"
                        + " ,convert(varchar, [GR_LOCAL_AMOUNT]) [GR_LOCAL_AMOUNT]"
                        + " ,[PO_UNLIMITED_FLAG]"
                        + " ,convert(varchar, [PO_TOLERANCE_PERCENTAGE]) [PO_TOLERANCE_PERCENTAGE]"
                        + " ,[PO_TAX_CD]"
                        + " ,[PO_INV_WO_GR_FLAG]"
                        + " ,[LIV_SPECIAL_TYPE]"
                        + " ,[LIV_SPECIAL_MAT_NO]"
                        + " ,[SPECIAL_STOCK_VALUATION_FLAG]"
                        + " ,[DELETION_FLAG]"
                        + " ,[GR_LEVEL]"
                        + " ,[RECEIVE_NO]"
                        + " ,[MAT_COMPLETE_FLAG]"
                        + " ,[AUTO_CREATED_FLAG]"
                        + " ,[REF_PROD_PURPOSE_CD]"
                        + " ,[REF_SOURCE_TYPE]"
                        + " ,[REF_MAT_NO]"
                        + " ,[REF_PLANT_CD]"
                        + " ,[REF_SLOC_CD]"
                        + " ,[SPECIAL_PROCUREMENT_TYPE_CD]"
                        + " ,[RAW_MATERIAL_FLAG]"
                        + " ,[DN_COMPLETE_FLAG]"
                        + " ,[REF_CANCEL_MAT_DOC_NO]"
                        + " ,[REF_CANCEL_MAT_DOC_YEAR]"
                        + " ,convert(varchar, [REF_CANCEL_MAT_DOC_ITEM_NO]) [REF_CANCEL_MAT_DOC_ITEM_NO]"
                        + " ,[INT_KANBAN_FLAG]"
                        + " ,convert(varchar, [ARRIVAL_PLAN_DT], 104) [ARRIVAL_PLAN_DT]"
                        + " ,convert(varchar, [OTHER_PROCESS_ID]) [OTHER_PROCESS_ID]"
                        + " ,[COMP_PRICE_CD_1]"
                        + " ,convert(varchar, [COMP_PRICE_AMOUNT_1]) [COMP_PRICE_AMOUNT_1]"
                        + " ,[COMP_PRICE_CD_2]"
                        + " ,convert(varchar, [COMP_PRICE_AMOUNT_2]) [COMP_PRICE_AMOUNT_2]"
                        + " ,[COMP_PRICE_CD_3]"
                        + " ,convert(varchar, [COMP_PRICE_AMOUNT_3]) [COMP_PRICE_AMOUNT_3]"
                        + " ,[COMP_PRICE_CD_4]"
                        + " ,convert(varchar, [COMP_PRICE_AMOUNT_4]) [COMP_PRICE_AMOUNT_4]"
                        + " ,[COMP_PRICE_CD_5]"
                        + " ,convert(varchar, [COMP_PRICE_AMOUNT_5]) [COMP_PRICE_AMOUNT_5]"
                        + " ,[COMPLETE_FLAG]"
                        + " ,[REMARK_1]"
                        + " ,[REMARK_2]"
                        + " ,[PROCESS_STS]"
                        + " ,convert(varchar, [STANDARD_PRICE]) [STANDARD_PRICE]"
                        + " ,[VIN]"
                        + " ,[CREATED_BY]"
                        + " ,convert(varchar, [CREATED_DT], 104) [CREATED_DT]"
                        + " ,[CHANGED_BY]"
                        + " ,convert(varchar, [CHANGED_DT], 104) [CHANGED_DT]"
                        + " FROM [dbo].[TB_T_GOOD_RECEIVE]"
                        + " WHERE ISNULL(SEND_FLAG, 'N') = 'N'"
                        + " AND SYSTEM_SOURCE = '" + type + "'";

                    // Query text incorporated into SQL command.
                    SqlCommand sqlSelect = new SqlCommand(sqlText, connect);

                    // Execute SQL and place data in a reader object.
                    SqlDataReader reader = sqlSelect.ExecuteReader();

                    // Stream writer for CSV file.
                    csvFile = new StreamWriter(@exportPath + exportCsv);

                    // Add the headers to the CSV file.
                    csvFile.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20}," +
                        "{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45}," +
                        "{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57},{58},{59},{60},{61},{62},{63},{64},{65},{66},{67},{68},{69},{70}," +
                        "{71},{72},{73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},{86},{87},{88},{89},{90},{91},{92},{93},{94}",
                        reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4), reader.GetName(5),
                        reader.GetName(6), reader.GetName(7), reader.GetName(8), reader.GetName(9), reader.GetName(10), reader.GetName(11),
                        reader.GetName(12), reader.GetName(13), reader.GetName(14), reader.GetName(15), reader.GetName(16), reader.GetName(17),
                        reader.GetName(18), reader.GetName(19), reader.GetName(20), reader.GetName(21), reader.GetName(22), reader.GetName(23),
                        reader.GetName(24), reader.GetName(25), reader.GetName(26), reader.GetName(27), reader.GetName(28), reader.GetName(29),
                        reader.GetName(30), reader.GetName(31), reader.GetName(32), reader.GetName(33), reader.GetName(34), reader.GetName(35),
                        reader.GetName(36), reader.GetName(37), reader.GetName(38), reader.GetName(39), reader.GetName(40), reader.GetName(41),
                        reader.GetName(42), reader.GetName(43), reader.GetName(44), reader.GetName(45), reader.GetName(46), reader.GetName(47),
                        reader.GetName(48), reader.GetName(49), reader.GetName(50), reader.GetName(51), reader.GetName(52), reader.GetName(53),
                        reader.GetName(54), reader.GetName(55), reader.GetName(56), reader.GetName(57), reader.GetName(58), reader.GetName(59),
                        reader.GetName(60), reader.GetName(61), reader.GetName(62), reader.GetName(63), reader.GetName(64), reader.GetName(65),
                        reader.GetName(66), reader.GetName(67), reader.GetName(68), reader.GetName(69), reader.GetName(70), reader.GetName(71),
                        reader.GetName(72), reader.GetName(73), reader.GetName(74), reader.GetName(75), reader.GetName(76), reader.GetName(77),
                        reader.GetName(78), reader.GetName(79), reader.GetName(80), reader.GetName(81), reader.GetName(82), reader.GetName(83),
                        reader.GetName(84), reader.GetName(85), reader.GetName(86), reader.GetName(87), reader.GetName(88), reader.GetName(89),
                        reader.GetName(90), reader.GetName(91), reader.GetName(92), reader.GetName(93), reader.GetName(94)));

                    // Construct CSV file data rows.
                    while (reader.Read())
                    {
                        // Add line from reader object to new CSV file.
                        csvFile.WriteLine(String.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\",\"{19}\",\"{20}\"," +
                            "\"{21}\",\"{22}\",\"{23}\",\"{24}\",\"{25}\",\"{26}\",\"{27}\",\"{28}\",\"{29}\",\"{30}\",\"{31}\",\"{32}\",\"{33}\",\"{34}\",\"{35}\",\"{36}\",\"{37}\",\"{38}\",\"{39}\",\"{40}\",\"{41}\",\"{42}\",\"{43}\",\"{44}\",\"{45}\"," +
                            "\"{46}\",\"{47}\",\"{48}\",\"{49}\",\"{50}\",\"{51}\",\"{52}\",\"{53}\",\"{54}\",\"{55}\",\"{56}\",\"{57}\",\"{58}\",\"{59}\",\"{60}\",\"{61}\",\"{62}\",\"{63}\",\"{64}\",\"{65}\",\"{66}\",\"{67}\",\"{68}\",\"{69}\",\"{70}\"," +
                            "\"{71}\",\"{72}\",\"{73}\",\"{74}\",\"{75}\",\"{76}\",\"{77}\",\"{78}\",\"{79}\",\"{80}\",\"{81}\",\"{82}\",\"{83}\",\"{84}\",\"{85}\",\"{86}\",\"{87}\",\"{88}\",\"{89}\",\"{90}\",\"{91}\",\"{92}\",\"{93}\",\"{94}\"",
                            reader[0], reader[1], reader[2], reader[3], reader[4], reader[5],
                            reader[6], reader[7], reader[8], reader[9], reader[10], reader[11],
                            reader[12], reader[13], reader[14], reader[15], reader[16], reader[17],
                            reader[18], reader[19], reader[20], reader[21], reader[22], reader[23],
                            reader[24], reader[25], reader[26], reader[27], reader[28], reader[29],
                            reader[30], reader[31], reader[32], reader[33], reader[34], reader[35],
                            reader[36], reader[37], reader[38], reader[39], reader[40], reader[41],
                            reader[42], reader[43], reader[44], reader[45], reader[46], reader[47],
                            reader[48], reader[49], reader[50], reader[51], reader[52], reader[53],
                            reader[54], reader[55], reader[56], reader[57], reader[58], reader[59],
                            reader[60], reader[61], reader[62], reader[63], reader[64], reader[65],
                            reader[66], reader[67], reader[68], reader[69], reader[70], reader[71],
                            reader[72], reader[73], reader[74], reader[75], reader[76], reader[77],
                            reader[78], reader[79], reader[80], reader[81], reader[82], reader[83],
                            reader[84], reader[85], reader[86], reader[87], reader[88], reader[89],
                            reader[90], reader[91], reader[92], reader[93], reader[94]));

                    }

                    // Message stating export successful.
                    Console.WriteLine("Data export successful.");
                }
                catch (Exception e)
                {
                    // Message stating export unsuccessful.
                    Console.WriteLine("Data export unsuccessful.");
                    System.Environment.Exit(0);
                }
                finally
                {
                    // Close the database connection and CSV file.
                    connect.Close();
                    csvFile.Close();
                }

            }
            else
            {
                // Display a message stating file path does not exist.
                Console.WriteLine("File path does not exist.");
            }

            var pathfile = Path.Combine(exportPath, fileFormatNm + ".csv");

            result = exportPath + "|" + fileFormatNm + "|" + pathfile;

            return result;
        }

        public override List<Common> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
