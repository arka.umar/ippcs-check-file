﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PartCPPNotification.Models
{
    public class CPPNotifObj
    {
        public Int32 ROW_NUM { get; set; }
        public String MC_CODE { get; set; }
        public String MAT_NO { get; set; }
        public String MAT_DESC { get; set; }
        public String MINOR_CODE { get; set; }
        public String PART_COLOR_SFX { get; set; }
        public String PACKING_TYPE { get; set; }
        public String PART_CATEGORY { get; set; }
        public String PRICE_STATUS { get; set; }
        public String SUPP_CD { get; set; }
        public String SUPPLIER_PLANT_CD { get; set; }
        public String SUPP_NAME { get; set; }
        public String VALID_DT_FR { get; set; }
        public String CURR_CD { get; set; }
        public String PRICE_AMT { get; set; }
        public String UOM { get; set; }
        public String WARP_REF_NO { get; set; }
        public String WARP_REF_COMMENT { get; set; }
        public String WARP_REF_DATE { get; set; }
        public String ECI_NO { get; set; }
        public String CPP_FLAG { get; set; }
        public String WARP_BUYER_CD { get; set; }
        public String BUYER_NAME { get; set; }
        public String ADD_DT { get; set; }
        public String UPDATE_DT { get; set; }
    }


}
