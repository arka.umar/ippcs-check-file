﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PartCPPNotification.Models
{
    public class Common
    {
        public string FUNCTION_ID { get; set; }
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string SYSTEM_REMARK { get; set; }

        public string MESSAGE_ID { get; set; }
        public string MESSAGE_TEXT { get; set; }
        public string MESSAGE_TYPE { get; set; }

        public Int64 PID { get; set; }
        public string MSG_TXT { get; set; }
        public string USER_ID { get; set; }
        public string LOCATION { get; set; }
        public string MSG_ID { get; set; }
        public string MSG_TYPE { get; set; }
        public string MODULE_ID { get; set; }
        public int PROCESS_STS { get; set; }

        public Int32 ROW_NUM { get; set; }
        public string ITEM { get; set; }
        public string PROCESS_ID { get; set; }
        public string PROCESS_NAME { get; set; }
        public string ErrorEx { get; set; }
        public string MSG_TEXT { get; set; }
        public string MSG_TEXT2 { get; set; }
        public string MSG_TEXT3 { get; set; }
        public string ERR_LOC { get; set; }


        
    }

    public class FTPCredential
    {
        public String HOST_NAME { get; set; }
        public String USER_NAME { get; set; }
        public String PASSWORD { get; set; }
        public String HOST_NAME_SUCCESS { get; set; }
        public String HOST_NAME_FAILED { get; set; }
        public string ITEM { get; set; }
    }
    
}
