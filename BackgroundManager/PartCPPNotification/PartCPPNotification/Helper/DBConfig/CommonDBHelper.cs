﻿using PartCPPNotification.Helper.Base;
using PartCPPNotification.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace PartCPPNotification.Helper.DBConfig
{
    public class CommonDBHelper : BaseRepository<Common>
    {

        #region Singleton
        private CommonDBHelper() { }
        private static CommonDBHelper instance = null;
        public static CommonDBHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    SetConfig();
                    instance = new CommonDBHelper();
                }

                return instance;
            }
        }
        #endregion

        #region FTP
        public List<FTPCredential> GetFtpCredential(string ID, string sql)
        {
            IDBContext db = dbManager.GetContext();
            var Result = db.Fetch<FTPCredential>(sql, new { ID = ID, PARAM = "" });
            db.Close();
            return Result.ToList();
        }

        public List<FTPCredential> GetFtpCredentialSucc(string param, string ID, string sql)
        {
            IDBContext db = dbManager.GetContext();
            var Result = db.Fetch<FTPCredential>(sql, new { ID = ID, PARAM = param });
            db.Close();
            return Result.ToList();
        }
        #endregion

        #region Create Log
        public Int64 CreateLog(Common model)
        {
            IDBContext db = dbManager.GetContext();

            Int64 result = db.SingleOrDefault<Int64>("CreateLog", model);
            db.Close();
            return result;
        }

        public Int64 CreateLogDetail(Common model)
        {
            IDBContext db = dbManager.GetContext();

            Int64 result = db.SingleOrDefault<Int64>("CreateLogDetail", model);
            db.Close();
            return result;
        }

        public Int64 CreateLogFinish(Common model)
        {
            IDBContext db = dbManager.GetContext();

            Int64 result = db.SingleOrDefault<Int64>("CreateLogFinish", model);
            db.Close();
            return result;
        }
        #endregion

        #region master
        public string getSystemVal(Common model)
        {
            IDBContext db = dbManager.GetContext();
            var result = db.Fetch<string>("GetSystemVal", model);
            db.Close();
            return result.FirstOrDefault();
        }

        public string getFilename(string type)
        {
            IDBContext db = dbManager.GetContext();

            var result = db.Fetch<string>("getFilename", new { type = type });
            db.Close();
            return result.FirstOrDefault();
        }
        #endregion

        #region transaction
        public IEnumerable<CPPNotifObj> DownloadData()
        {
            IDBContext db = dbManager.GetContext();
            var Result = db.Fetch<CPPNotifObj>("DownloadData", new
            { 

            });
            db.Close();
            return Result.ToList();
        }


        #endregion

        public override List<Common> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
