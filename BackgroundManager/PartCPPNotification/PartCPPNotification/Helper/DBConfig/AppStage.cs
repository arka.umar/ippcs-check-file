﻿using System;

namespace PartCPPNotification.Helper.DBConfig
{
    internal class AppStage
    {
        internal const String Development = "DEV";
        internal const String QualityAssurance = "QA";
        internal const String Production = "PRD";
        internal const String None = "";
    }
}
