SELECT  SUPPLIER_CD suppCode ,
        ORDER_RELEASE_DT orderReleaseDate ,
        PART_NO partNo , 
        a.DOCK_CD dockCode ,
        sourceType = 1 ,
        prodPurposeCode = 'D'
FROM    dbo.TB_R_DAILY_ORDER_MANIFEST a
        INNER	JOIN dbo.TB_R_DAILY_ORDER_PART b ON a.MANIFEST_NO = b.MANIFEST_NO
WHERE   ORDER_RELEASE_DT = @0
GROUP BY SUPPLIER_CD ,
        ORDER_RELEASE_DT ,
        PART_NO ,
        a.DOCK_CD 