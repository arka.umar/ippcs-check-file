﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterDataCompletenessCheckGRTaskRuntime.Models
{
    class LPOPPurchaseOrder
    {
        public string suppCode { set; get; }
        public string orderReleaseDate { set; get; }
        public string partNo { set; get; }
        public string dockCode { set; get; }
        public string sourceType { set; get; }
        public string prodPurposeCode { set; get; }  
    }
}
