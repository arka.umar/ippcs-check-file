﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Toyota.Common.SimpleConfig
{
    /// <summary>
    /// SimpleTree delegate executed while iterating through nested SimpleTree
    /// using SimpleTreeHelper
    /// </summary>
    /// <param name="s"></param>
    public delegate void Chop(SimpleTree s);

    /// <summary>
    ///  Class for storing simple hierarchical structure 
    ///  the childless nodes considered to be 'leaf' 
    /// </summary>
    public class SimpleTree
    {
        /// they say encapsulate - hide your privates 
        /// but I prefer it this way - no useless getter setter for simple field
        public SimpleTree prev = null;
        public SimpleTree next = null;
        public SimpleTree sup = null;
        public SimpleTree sub = null;

        public int ID = 0;
        public string Text;
        public int Depth = 0;

        /// <summary>
        /// Default shorthand for creating 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="id"></param>
        public SimpleTree(string text = null, int id = 0)
        {
            Depth = 0;
            ID = id;
            Text = text;
        }


        /// <summary>
        /// Get Root node 
        /// </summary>
        public SimpleTree Root
        {
            get
            {
                SimpleTree x = this;
                while (x.sup != null)
                {
                    x = x.sup;
                }
                return x;
            }
        }

        /// <summary>
        /// Get First element of the same level  
        /// </summary>
        public SimpleTree First
        {
            get
            {
                SimpleTree x = this;
                while (x.prev != null)
                {
                    x = x.prev;
                }
                return x;
            }
        }

        /// <summary>
        ///  Get Last Element of the same level 
        /// </summary>
        public SimpleTree Last
        {
            get
            {
                SimpleTree x = this;
                while (x.next != null)
                {
                    x = x.next;
                }
                return x;
            }
        }

        /// <summary>
        /// Search element by text  
        /// </summary>
        /// <param name="text">string to find</param>
        /// <returns>Node containing Text</returns>
        public SimpleTree ByText(string text)
        {
            if (string.Compare(this.Text, text, true) == 0)
            {
                
                return this.sub;
            }
            else
            {
                SimpleTree x = this.First;
                bool found = false;
                while (x != null && !found)
                {
                    if (string.Compare(x.Text, text, true) != 0)
                    {
                        x = x.next;
                    }
                    else
                    {
                        found = true;
                        x = x.sub;
                        
                        break;
                    }
                }

                
                return x;
            }
        }

        /// <summary>
        /// find element by path  
        /// </summary>
        /// <param name="path">string path</param>
        /// <param name="fromRoot">traverse from root or from current node</param>
        /// <returns>node containing text in path separated by "/" slash</returns>
        public SimpleTree Find(string path, bool fromRoot = false)
        {
            SimpleTree x = null;
            if (fromRoot) x = Root; else x = this;

            string[] p = path.Split(new string[] { "/" }, StringSplitOptions.None);
            int i = 0;
            if (p.Length > 0 && p[i].isEmpty())
            {
                x = x.sub;
                ++i;
            }
            while (x != null && i < p.Length)
            {
                
                x = x.ByText(p[i]);
                i++;
            }
            

            return x;
        }


        /// <summary>
        /// find text contained in path 
        /// </summary>
        /// <param name="path">path of search value inside nodes separated by "/" slash</param>
        /// <param name="fromRoot">traverse from root whentrue or from current node when false</param>
        /// <returns>value of last leaf</returns>
        public string GetValue(string path, bool fromRoot = false)
        {
            SimpleTree x = null;
            if (fromRoot) x = Root; else x = this;

            x = Find(path, fromRoot);

            return (x != null) ? x.Text : null;
        }


        /// <summary>
        /// get child node of current node  
        /// </summary>
        /// <returns></returns>
        public List<SimpleTree> Subs()
        {
            List<SimpleTree> b = null;
            SimpleTree x = sub;
            if (x != null)
                b = new List<SimpleTree>();
            while (x != null)
            {
                b.Add(x);
                x = x.next;
            }
            return b;
        }


        /// <summary>
        /// get all sibling including current node  
        /// </summary>
        /// <returns></returns>
        public List<SimpleTree> Kin()
        {
            SimpleTree y = First;
            List<SimpleTree> b = new List<SimpleTree>();
            while (y != null)
            {
                b.Add(y);
                y = y.next;
            }
            return b;
        }


        /// <summary>
        ///  add sibling
        /// </summary>
        /// <param name="x"></param>
        public void Add(SimpleTree x)
        {
            SimpleTree z = Last;

            if (z.next == null)
            {
                z.next = x;

                x.prev = z;
                x.sup = z.sup;
                x.Depth = z.Depth;
            }

        }


        /// <summary>
        ///  Multiply, to reproduce, to make children
        ///  add child node
        /// </summary>
        /// <param name="x"></param>
        public void Mul(SimpleTree x)
        {
            if (sub == null)
            {
                sub = x;
                x.sup = this;

            }
            else
            {
                sub.Add(x);
            }
            x.Depth = this.Depth + 1;
        }


        /// <summary>
        /// Shorthand for object Adding   
        /// </summary>
        /// <param name="text">Node Text</param>
        /// <param name="id">Node ID, if not given, generated from next available ID </param>
        /// <returns>Added Node</returns>
        public SimpleTree Add(string text, int id = 0)
        {
            int newID = (id < 1) ? MaxID + 1 : id;
            SimpleTree x = new SimpleTree(text, newID);
            Add(x);
            return x;
        }

        /// <summary>
        /// Shorthand for object Multiplying
        /// </summary>
        /// <param name="text">Node Text</param>
        /// <param name="id">Node ID, when not given, generated from next available ID</param>
        /// <returns>Multiplied Node</returns>
        public SimpleTree Mul(string text, int id = 0)
        {
            int newID = (id < 1) ? MaxID + 1 : id;
            SimpleTree x = new SimpleTree(text, newID);
            Mul(x);
            return x;
        }


        /// <summary>
        /// get Maximum ID of entire tree collection   
        /// </summary>
        public int MaxID
        {
            get
            {
                SimpleTreeHelper h = new SimpleTreeHelper();
                SimpleTreeHelper.Walk(this, h.GetMax);
                return h.MaxID;

                //int num = 0;
                //Stack<SimpleTree> s = new Stack<SimpleTree>();
                //s.Push(Root);
                //SimpleTree x = null;
                //while (s.Count > 0)
                //{
                //    x = s.Pop();
                //    if (num < x.ID)
                //        num = x.ID;
                //    if (x.sub != null)
                //    {
                //        s.Push(x.sub);
                //    }
                //    if (x.next != null)
                //    {
                //        s.Push(x.next);
                //    }                      
                //}
                //return num;
            }
        }

        /// <summary>
        ///  next ID from entire collection
        /// </summary>
        public int NextID
        {
            get
            {
                return MaxID + 1;
            }
        }
    } // class SimpleTree


    /// <summary>
    /// for traversing through nested SimpleTree
    /// </summary>
    public class SimpleTreeHelper
    {
        public int MaxID = 0;
        public void GetMax(SimpleTree x)
        {
            if (x.ID > MaxID) MaxID = x.ID;
        }

        /// <summary>
        ///  traverse every node from root 
        /// </summary>
        /// <param name="metode">send delegate to execute for every traversed node</param>
        public static void Walk(SimpleTree t, Chop metode)
        {
            Stack<SimpleTree> s = new Stack<SimpleTree>();
            s.Push(t.Root);

            SimpleTree x = null;

            while (s.Count > 0)
            {
                x = s.Pop();
                metode(x);
                SimpleTree l;
                l = x.next;
                if (l != null)
                    s.Push(l);
                l = x.sub;
                if (l != null)
                    s.Push(l);
            }
        }
    } // class SimpleTreeHelper


    /// <summary>
    /// static class extension methods for string 
    /// </summary>
    public static class StringManipulator
    {
        /* take string x and Multiply it n times */
        public static string Mul(this string x, int n)
        {
            StringBuilder s = new StringBuilder("");
            for (int i = 0; i < n; i++)
            {
                s.Append(x);
            }
            return s.ToString();
        }

        public static string CleanNameOf(this string fn)
        {
            while (!string.IsNullOrEmpty(fn))
            {
                int lastDot = fn.LastIndexOf('.');

                if (lastDot > 0)
                    fn = fn.Substring(0, lastDot);
                else
                    break;
            }
            return fn;
        }

        public static bool isEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static List<string> explode(this string s, string separator = ";")
        {
            List<string> b = new List<string>();
            Regex x = new Regex("([\"]([^\"]+)[\"]|([^" + separator + "]+)|)[\\" + separator + "]?");
            MatchCollection c = x.Matches(s);
            if (c.Count > 0)
            {
                for (int i = 0; i < c.Count; i++)
                {
                    if (c[i].Groups.Count > 1 && c[i].Groups[2].Value.Length > 0)
                        b.Add(c[i].Groups[2].Value);
                    else if (c[i].Groups.Count > 0 && c[i].Groups[1].Value.Length > 0)
                        b.Add(c[i].Groups[1].Value);
                    else
                        b.Add("");
                }
            }
            return b;
        }

        public static decimal Dec(this string o, decimal v = 0m)
        {
            decimal x = 0;

            if (o == null || !decimal.TryParse(o.ToString(), out x))
            {
                x = v;
            }
            return x;
        }

        public static string YMD(this string s)
        {
            if (!string.IsNullOrEmpty(s) && s.Length > 9 && s.IndexOf(".") == 2 && s.LastIndexOf(".") == 5)
            {
                string t = s.Substring(6, 4) + s.Substring(3, 2) + s.Substring(0, 2);
                return t;
            }
            else
                return "";
        }

        public static string UnQuote(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return s;

            string a = s.Substring(0, 1);
            string b = s.Substring(s.Length - 1, 1);
            if (s.Length > 2 && a.Equals(b)
                && (a.Equals("\"") || a.Equals("'") || a.Equals("`"))
                )
                s = s.Substring(1, s.Length - 2);
            else if (s.Length > 2 && a.Equals("'") )
                s = s.Substring(1, s.Length-1);
            else if (a.Equals("=")) {
                s = UnQuote(s.Substring(1, s.Length-1));
            }
            return s;
        }

    } // class StringManipulator


    /// <summary>
    /// interface for reading config 
    /// </summary>
    public interface IConfigReader
    {
        string Get(string key);
        string Set(string key, string value);

        bool Read(string filename);
    }

    public class XmlConfigReader : IConfigReader
    {
        SimpleTree t = new SimpleTree() { Text = "", ID = 0, Depth = 0 };

        public int IntVal(string key, int defaultValue = 0)
        {
            string x = t.GetValue(key);
            int r = defaultValue;

            if (!string.IsNullOrEmpty(x) && int.TryParse(x, out r))
            {
                return r;
            }
            else
                return defaultValue;
        }

        public string Val(string key, string defaultValue = "")
        {
            string x = t.GetValue(key);
            if (string.IsNullOrEmpty(x))
                return defaultValue;
            else
                return x;
        }

        public string Get(string key)
        {
            return t.GetValue(key);
        }

        public string Set(string key, string value)
        {
            throw new NotImplementedException();
        }

        public bool Read(string filename)
        {
            if (File.Exists(filename))
            {
                XmlTextReader x = new XmlTextReader(filename);
                Stack<SimpleTree> s = new Stack<SimpleTree>();

                int LastDepth = -1; /// indeterminate depth
                while (x.Read())
                {
                    switch (x.NodeType)
                    {
                        case XmlNodeType.Element:
                            SimpleTree tx = null;
                            if (s.Count < 1 && t.MaxID == 0)
                            {
                                t.Text = x.Name;
                                t.ID = 0;
                                t.prev = null;
                                t.next = null;
                                s.Push(t);
                                LastDepth = 0;
                                tx = t;

                            }
                            else
                            {
                                tx = s.Peek();
                                if (LastDepth < x.Depth)
                                {
                                    LastDepth = x.Depth;
                                    SimpleTree txa = tx.Mul(x.Name);
        
                                    s.Push(txa);
                                    tx = txa;
                                }
                                else
                                {
                                    SimpleTree z = tx.Add(x.Name);
                                    s.Pop();
                                    s.Push(z);
        
                                }
                            }
        

                            if (x.HasAttributes)
                            {
                                x.MoveToFirstAttribute();
                                for (int i = 0; i < x.AttributeCount; i++)
                                {
                                    SimpleTree txx = tx.Mul(x.Name);
                                    txx.Mul(x.Value);

                                    x.MoveToNextAttribute();
                                }
                            }
                            // begin 
                            break;
                        case XmlNodeType.Text:
                            // fill
                            if (s.Count > 0)
                            {
                                SimpleTree txt = s.Peek();
                                txt.Mul(x.Value);
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (LastDepth > x.Depth && s.Count > 0)
                            {
                                s.Pop();
                                LastDepth = x.Depth;
                            }
                            else
                            {

                            }
                            break;

                        /// list of ignored elements 
                        case XmlNodeType.Attribute:
                        case XmlNodeType.Comment:
                        case XmlNodeType.Whitespace:
                            break;

                        default:
                            break;
                    }
                }

                return true;
            }
            return false;
        }


        public string this[string index]
        {
            get
            {
                string v = t.GetValue(index, true);
                return v;
            }
        }

    }

    /// <summary>
    /// logging extension for any object, log to text file 
    /// </summary>
    public static class ObjectLogExtension
    {
        public static string LogFileName = null;
        public static DateTime LastLog = new DateTime(1753, 12, 31);

        public static void Log(this object o, string w, params object[] x)
        {
            if (string.IsNullOrEmpty(LogFileName))
            {
                LogFileName = GetLogFileName(o);

            }
            string what = string.Format(w, x);

            DateTime d = DateTime.Now;
            string ts = new string(' ', 9);
            bool mark = (string.IsNullOrEmpty(w) || w.StartsWith("\r\n"));

            if (Math.Floor((d - LastLog).TotalSeconds) > 0 || mark)
            {
                LastLog = d;
                ts = d.ToString("HH:mm:ss ");
            }
            try
            {
                File.AppendAllText(LogFileName, ts + string.Format(w, x) + "\r\n");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        /// <summary>
        /// Get Log File based on under %ALLUSERSPROFILE% or Common Application Data 
        /// using asssemby company attribute or 'toyota' as default company 
        /// log resides in with current year, month and day sub directory 
        /// and name based on class calling these log
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string GetLogFileName(this object o)
        {
            DateTime n = DateTime.Now;
            AssemblyCompanyAttribute aca =
                AssemblyCompanyAttribute.GetCustomAttribute(
                    Assembly.GetExecutingAssembly()
                    , typeof(AssemblyCompanyAttribute))
                as AssemblyCompanyAttribute;

            string company = "toyota";
            if (aca != null)
            {
                company = aca.Company;
            }
            string logFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
                , company
                , System.AppDomain.CurrentDomain.FriendlyName.CleanNameOf()
                , "log"
                , n.Year.ToString()
                , n.Month.ToString()
                , n.Day.ToString());

            try
            {
                if (!Directory.Exists(logFile))
                {

                    Directory.CreateDirectory(logFile);
                }

                logFile = Path.Combine(logFile, o.GetType().Name + ".log");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                logFile = null;
            }
            return logFile;
        }

        /// <summary>
        /// for resetting log file with filename 'logFile' when rename existing file
        /// </summary>
        /// <param name="o">object</param>
        /// <param name="logFile">File name to Reset</param>
        /// <returns>operation success</returns>
        public static bool ResetLog(this object o, string logFile)
        {
            bool ok = true;
            LastLog = new DateTime(1753, 12, 31);

            if (!string.IsNullOrEmpty(logFile) && File.Exists(logFile))
            {
                string tmpFile = logFile;
                int j = tmpFile.LastIndexOf(".");

                try
                {
                    if (j > 0)
                    {
                        DateTime d = File.GetLastWriteTime(logFile);
                        int SEQ = 0;
                        do
                        {
                            int seq = SEQ + ((int)(DateTime.Now - DateTime.Today).TotalMilliseconds % 1000);
                            tmpFile = logFile.Insert(j, d.ToString(".HHmmss." + seq.ToString().PadRight(3, '0')));
                            SEQ++;
                        } 
                        while (File.Exists(tmpFile));
                    }

                    File.Move(logFile, tmpFile);
                }
                catch (Exception e)
                {
                    ok = false;
                    Debug.WriteLine(e);
                }
            }
            else
            {
                ok = true;
            }
            if (ok)
                LogFileName = logFile;

            return ok;
        }
    } // class ObjectLogExtension


}