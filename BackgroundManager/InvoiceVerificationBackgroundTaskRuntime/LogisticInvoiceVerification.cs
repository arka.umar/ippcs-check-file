﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using InvoiceVerificationBackgroundTaskRuntime.Models;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.SimpleConfig;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Util.Text;
using Toyota.Common.Web.Service;
using Ionic.Zip;

namespace InvoiceVerificationBackgroundTaskRuntime
{
    partial class LogisticInvoiceVerification : ExternalBackgroundTaskRuntime
    {
        private string DateFmt = "dd.MM.yyyy";
        private DateTime lastReport = new DateTime(1753, 12, 31);
        private int lastProgress = 0;
        private XmlConfigReader conf;
        private readonly int[] steps = new int[] { 0, 5, 20, 40, 70, 80, 90, 100 }; 
        private readonly string WsModule = "validation";
        private readonly string WsAction = "invoiceVerificationUpload";
        private const string ModuleId = "5";
        private const string FunctionId = "52001";
        private string stopMark = "";
        private const int Reading = 1;
        private const int Validating = 2;
        private const int Transfering = 3;
        private const int Processing = 4;
        private const int Inquiry = 5;
        private const int Feedback = 6;
        private const int Finishing = 7;
        List<List<string>> data = new List<List<string>>();
        private int UpdateDelayMs = 137;
        private int CHUNK = 100;
        private long processID = -1;
        private int sid = 0;
        private string suppCode = "";
        private string userName = null;
        private string author = "livDaemon";

        private const int LOG_ICS_RAW = 4;
        private const int LOG_ICS_RESPONSE = 2;
        private const int LOG_ICS_SENT = 1;
        private List<List<string>> lst;

        int SKIP_STEP = 0;
        int SKIP_CSV_VALIDATE = 1;
        int skipLine = 1;
        int skipped = 0;
        private int LogVerbosity = (LOG_ICS_SENT | LOG_ICS_RESPONSE);

        public LogisticInvoiceVerification()
        {
            this.Name = "LIV" + Math.Round(
                                    (DateTime.Now - DateTime.Today)
                                    .TotalMilliseconds
                                ,0).ToString() ;
        }

        private IDBContext db = null;
        protected IDBContext DB
        {
            get
            {
                if (db == null)
                {
                    db = DatabaseManager.GetContext(DefaultConnectionDescription.Name);
                }
                return db;
            }
        }

        private Regex rx = null;
        protected Regex RX
        {
            get
            {
                if (rx == null)
                {
                    string separator = conf.Val("/csv/separator", ";");

                    rx = new Regex("([\"]([^\"]+)[\"]|([^" + separator + "]+)|)[\\" + separator + "]?");
                }
                return rx;
            }
        }

        private int DayPid(long pid)
        {
            return (int)(pid % 10000);
        }

        private List<ColumnSpec> colSpec = null;

        /// <summary>
        /// Logistic Invoice Verification process
        /// read csv file sent 
        /// fetch into memory 
        /// do validation 
        /// write validation error
        /// when no error, call ICS service         
        /// </summary>

        /// <param name="parameters">
        /// 0 - process id
        /// 1 - userID 
        /// 2 - Supplier Code
        /// 3 - FileName
        /// 
        /// program will try to find input file in ftp folder structured: 
        /// /FtpDirectory/UserID/FileName
        /// FtpDirectory : local or shared directory 
        /// SupplierCode: from parameter    
        /// UserID: executing user 
        /// </param>
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(
                    new AssemblyFileQueryLoader(this.GetType().Assembly, "InvoiceVerificationBackgroundTaskRuntime.Queries"));
            this.DefaultLogSession.AutoFlush = true;
            
            int errParam = 0;
            string[] paramkeys = new string[] { 
                "ProcessID", 
                "UserID", 
                "SuppCode", 
                "FileName" };
            
            string[] paramv = new string[4] { "", "", "", "" };
            StringBuilder sbParam = new StringBuilder("Parameters: ");
            for (int pi = 0; pi < paramkeys.Length; pi++)
            {
                string v = parameters.Get(paramkeys[pi]);

                paramv[pi] = v.UnQuote();
                sbParam.AppendFormat("[{0}]=\"{1}\" ", paramkeys[pi], paramv[pi]);
                
            }
            sbParam.Append("\r\n");
            if (!long.TryParse(paramv[0], out processID))
            {
                processID = int.MinValue;
            }
            if (processID < 0)
            {
                LogErr("ProcessID is required parameter and must be positive non-zero number.");
                ++errParam;
            }

            say(sbParam.ToString());
            
            userName = paramv[1];

            suppCode = paramv[2];

            string filename = paramv[3];

            if (userName.isEmpty())
            {
                LogErr("UserID parameter is empty.");
                ++errParam;
            }

            if (suppCode.isEmpty())
            {
                LogErr("SuppCode parameter is empty.");
                ++errParam;
            }

            if (filename.isEmpty())
            {
                this.Warn("FileName parameter is empty.");
            }

            if (errParam > 0)
            {
                SetStatus(TaskStatus.Error);
                if (!userName.isEmpty())
                    AddNotification("Error LIV", "LIV Background Task parameter error or incomplete");
                return;
            }

            string confFile = Path.Combine(
                    AppDomain.CurrentDomain.BaseDirectory,
                    System.AppDomain.CurrentDomain.FriendlyName.CleanNameOf() + ".xml");
            
            if (!File.Exists(confFile))
            {
                LogErr("Configuration file : {0} not found", confFile);
                SetStatus(TaskStatus.Error);
                AddNotification("Error LIV", "LIV Background Task Configuration File not found");
                return;
            }
            
            conf = new XmlConfigReader();
            conf.Read(confFile);
            LogVerbosity = conf.IntVal("/LogVerbosity", LogVerbosity);
            UpdateDelayMs = conf.IntVal("/UpdateDelay", UpdateDelayMs);
            author = conf.Val("/author", author);
            // open csv file sent 
            string FtpDir = conf["/ftp/dir"];
            bool dbInput = false;
            if (FtpDir.isEmpty())
            {
                // this.Warn("No CSV directory " + FtpDir + " mentioned in setting, read from TB_R_INVOICE_UPLOAD_FILE instead.");
                dbInput = true;
            }
            stopMark = conf["/stopState"];
            string ext;
            string csvFile = "";
            CHUNK = conf.IntVal("/transfer/chunk", CHUNK);
            if (!dbInput)
            {
                string csvDir = Path.Combine(FtpDir, userName);
                if (!Directory.Exists(csvDir))
                {
                    LogErr("Input File Directory {0} not found.", csvDir);
                    // AddNotification("Error LIV", "Process " + processID + " Input Directory not found");
                    return;
                }
                ext = conf.Val("/ftp/ext", ".csv");
                csvFile = Path.Combine(csvDir, filename + ext);
                if (!File.Exists(csvFile))
                {
                    LogErr("Input file: \"{0}\" not found.", filename + ext);
                    return;
                }
            }

            string fieldSpecs = conf["/csv/fields"];
            if (fieldSpecs.isEmpty())
            {
                LogErr("No Fields specification in setting file, fields specification expected in setting/csv/fields xml setting file.");
                AddNotification("Error LIV", "Field setting not found");
                return;
            }
            DateFmt = conf.Val("/csv/DateFormat", DateFmt);
            colSpec = GetSpecs(fieldSpecs);
            // List<ColumnSpec> columnSpec = 

            
            skipLine = conf.IntVal("/csv/skipLine", skipLine);

            if (dbInput)
            {
                /// fetch each row into memory 
                /// 
                ReadDb();
            }
            else
            {
                ReadCSV(csvFile);
            }

            SetProgress(steps[Reading]);
            if ((SKIP_STEP & SKIP_CSV_VALIDATE) == 0)
                say("Finish Reading Input.\r\n");

            if ((SKIP_STEP & SKIP_CSV_VALIDATE)==0)
                ValidateData();

            SetProgress(steps[Transfering]);

            Process();

            // AddNotification("Upload LIV Success", "Process " + processID + " FINISHED!"); -- moved to JobOut
        }

        void ValidateData()
        {
            say("Start Validate Input.");
            int errors = Validate(colSpec, data);
            int erri = colSpec.Count + 1;

            /// generate error 

            if (errors > 0)
            {
                say("Report Errors.");
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].Count > erri && !data[i][erri].isEmpty())
                    {
                        LogErr("@line " + (i + 1) + ": " + data[i][erri]);
                    }
                }
                ClearTemp();
                SetStatus(TaskStatus.Error);
                SaveData(data);
                AddNotification("Error LIV", "LIV Validation has " + errors + " errors.");
                return;
            }
            SetProgress(steps[Validating]);
            say("Finish Validate.\r\n");
        }

        void ReadCSV(string csvFile)
        {
            string row = null;
            int rownum = 0;
            StreamReader csv = new StreamReader(csvFile);
            FileInfo fi = new FileInfo(csvFile);
            int fsize = (int)(fi.Length % int.MaxValue);
            int reads = 0;
            say("Start Reading Input file: {0} to memory.", csvFile);
            if (skipLine > 0)
            {
                for (int i = 0; i < skipLine; i++)
                {
                    row = csv.ReadLine();
                    if (row == null)
                        break;
                }
            }
            while ((row = csv.ReadLine()) != null)
            {
                rownum++;
                reads += row.Length;
                ShowProgress(Reading, reads, fsize);
                // process line into array
                data.Add(explode(row));
            }
        }
        void ReadDb() 
        {
            string sql = SQL("GetUploadInvoice");
            say("GetUploadInvoice {0}", processID);
            InvoiceUploadFile uploaded = DB.SingleOrDefault<InvoiceUploadFile>(sql, new object[] { processID });
            string[] csvLines = new string[] { };
            string body = "";
            if (uploaded != null)
            {
                string dir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    "toyota",
                    this.GetType().Name);

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                if (uploaded.FileSize == 0) // no transfer file to background job - bulk insert 
                {
                    say("Bulk Insert assumed");
                    SKIP_STEP = SKIP_CSV_VALIDATE;
                    return;
                }
                say("Start Reading Input from DB to memory.");
                if (uploaded.FileName.ToLower().EndsWith(".zip"))
                {
                    ZipFile z = new ZipFile();
                    MemoryStream m = new MemoryStream();
                    byte[] bx = Convert.FromBase64String(uploaded.Body);
                    string zipFileName = Path.Combine(dir, uploaded.FileName);

                    ReMoveFile(zipFileName);
                    zipFileName = string.Format("{0}-{1}.zip", suppCode, processID);
                    this.Say("Zip: {0}", zipFileName);
                    FileStream fx = new FileStream(Path.Combine(dir, zipFileName), FileMode.Create);

                    m.Seek(0, SeekOrigin.Begin);
                    m.Write(bx, 0, bx.Length);
                    fx.Write(bx, 0, bx.Length);

                    MemoryStream x = new MemoryStream();
                    m.Seek(0, SeekOrigin.Begin);
                    z = ZipFile.Read(m);
                    foreach (ZipEntry zz in z.Entries)
                    {
                        string zFile = zz.FileName.ToLower();
                        this.Say("Entry : {0}", zFile);

                        if (zFile.EndsWith(".csv") || zFile.EndsWith(".txt"))
                        {
                            zz.Extract(x);
                            x.Seek(0, SeekOrigin.Begin);
                            x.SetLength(zz.UncompressedSize);
                            StreamReader g = new StreamReader(x);
                            body = g.ReadToEnd();

                            string fn = String.Format("{0}-{1}-{2}", suppCode, processID, Path.GetFileName(zFile));
                            this.Say("UnZip To: {0}", fn);

                            fn = Path.Combine(dir, fn);
                            ReMoveFile(fn);

                            FileStream f = new FileStream(fn, FileMode.Create);
                            x.Seek(0, SeekOrigin.Begin);
                            bx = x.GetBuffer();
                            f.Write(bx, 0, bx.Length);
                            f.Close();

                            break;
                        }
                    }
                    m.Close();
                }
                else
                    body = uploaded.Body;
                csvLines = body.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                say("Uploaded file not there, Bulk Insert assumed");
                SKIP_STEP = SKIP_CSV_VALIDATE;
            }

            if ((SKIP_STEP & SKIP_CSV_VALIDATE) == 0)
            {
                for (int i = 0; i < csvLines.Length; i++)
                {
                    string csvRow = csvLines[i];
                    if (skipped < skipLine)
                    {
                        skipped++;
                        continue;
                    }
                    ShowProgress(Reading, i, csvLines.Length);
                    data.Add(explode(csvRow));
                }


                /// delete before transfer 
                ClearTemp();

                /// update TB_T_INV_UPLOAD 
                SaveToTemp(data);
            }
        }

        void ws(string x, bool? ok = null)
        {
            if (ok == null)
                say("ICS Start   '" + x + "'.");
            else
            {
                if (!ok.Value)
                {
                    this.Warn("ICS Finish  '" + x + "'.\r\n");
                }
                else
                {
                    say("ICS Finish  '" + x + "'.\r\n");
                }
            }
        }      

        /// <summary>
        /// Transfer rows directly to ICS TB_T_INVOICE_UPLOAD 
        /// from IPPCS TB_T_INV_UPLOAD
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        bool LinkedTransfer()
        {
            bool ok = true;
            
            say("DeleteICSInvoiceUploadTemp {0}", suppCode); 
            DB.Execute(SQL("DeleteICSInvoiceUploadTemp"), new object[] { suppCode });

            /// insert list data to tb_t_invoice_upload
            say("InsertICSInvoiceUploadTemp {0}", processID);
            DB.Execute(SQL("InsertICSInvoiceUploadTemp"), new object[] { processID });
            return ok;
        }

        /// <summary>
        /// Update Rows in IPPCS TB_T_INV_UPLOAD 
        /// based on result acquired in TB_T_INV_UPLOAD 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        bool LinkedUpdate()
        {
            bool ok = true;

            try
            {
                say("UpdateInvoiceUploadTempFromICS {0}", processID);
                db.Execute(SQL("UpdateInvoiceUploadTempFromICS"), new object[] { processID });
            }
            catch (Exception e)
            {
                say("Error : {0}\r\n{1}", e.Message, e.StackTrace);
                ok = false;
            }

            return ok;
        }

        bool Process()
        {
            
            say("ICS Open WebService Gateway : " + WsModule + " " + WsAction);
            int totalPage = 0;
            GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
            bool useLinked = conf.IntVal("/transfer/useLinked", 1) != 0;
            ServiceResult result;

            ServiceParameters sp = new ServiceParameters();

            string pX = null;
            bool ok = false;
            string sessionID = null;
            do
            {
                pX = "process";
                if (pX.Equals(stopMark)) break;
                ws(pX);
                sp.Clear();
                sp.Add("state", pX);
                sp.Add("sessionId", processID.ToString());

                sp.Add("userId", userName);
                sp.Add("suppCode", suppCode);

                ListServiceParameter(sp);

                result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
                LogResult(result);
                ok = result.Status != ServiceResult.STATUS_ERROR;
                ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
                ws(pX, ok);

                if (!ok)
                    break;

                /// process Inquiry
                int.TryParse((string)result.Value, out totalPage);

                SetProgress(steps[Processing]);

                pX = "finish";

                ws(pX);
                sp.Clear();
                sp.Add("state", "finish");
                sp.Add("sessionId", sessionID);
                ListServiceParameter(sp);
                result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
                LogResult(result);
                sp.Clear();
                ok = !(result.Status == ServiceResult.STATUS_ERROR);
                ws(pX, ok);
                ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
            } while (false);
            
            say("ICS Gateway Close");
            gateway.Close();

            return ok;
        }

        void Upload()
        {
            say("ICS Open WebService Gateway : " + WsModule + " " + WsAction);
            int totalPage = 0;
            GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
            bool useLinked = conf.IntVal("/transfer/useLinked", 1) != 0; 
            ServiceResult result;
           
            ServiceParameters sp = new ServiceParameters();
            
            string pX = null;
            bool ok = false;
            string sessionID = null;

            do
            {
                sessionID = processID.ToString();
                int.TryParse(sessionID, out sid);

                UpdateInterfaceStatus(0, "Logistic Invoice Verification"); 
                                
                /// delete previous supplier process id tb_t_invoice_upload through linked server 
                say("DeleteICSInvoiceUploadTemp {0}", suppCode);
                DB.Execute(SQL("DeleteICSInvoiceUploadTemp"), new object[] { suppCode });
                
                ok = LinkedTransfer();

                SetProgress(steps[Transfering]);

                if (!ok)
                    break;
            
                pX = "process";
                if (pX.Equals(stopMark)) break;
                ws(pX);
                sp.Clear();
                sp.Add("state", pX);
                sp.Add("sessionId", sessionID);
                
                sp.Add("userId", userName);
                sp.Add("suppCode", suppCode);
                
                ListServiceParameter(sp);
                
                result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
                LogResult(result);
                ok = result.Status != ServiceResult.STATUS_ERROR;
                ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
                ws(pX, ok);

                if (!ok) 
                    break;
                
                /// process Inquiry
                int.TryParse((string)result.Value, out totalPage);
                
                SetProgress(steps[Processing]);
                
                ok=Wait();

                SetProgress(steps[Inquiry]);
                LinkedUpdate();
                SetProgress(steps[Feedback]);

                say("UpdateIcsProcessId {0}", processID);
                DB.Execute(SQL("UpdateIcsProcessId"), new object[] { processID });                

            } while (false);

            pX = "finish";
            
            ws(pX);
            sp.Clear();
            sp.Add("state", "finish");
            sp.Add("sessionId", sessionID);
            ListServiceParameter(sp);
            result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
            LogResult(result);
            sp.Clear();
            ok = !(result.Status == ServiceResult.STATUS_ERROR);
            ws(pX, ok);
            
            ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
            say("ICS Gateway Close");
            gateway.Close();
        }


        int LenOf(string x)
        {
            int commaPos = x.LastIndexOf(',');
            int V = 0;
            if (commaPos > 0)
                int.TryParse(x.Substring(0, commaPos), out V);
            else
                int.TryParse(x, out V);
            return V;
        }

        int DecOf(string x)
        {
            int commaPos = x.LastIndexOf(',');
            int V = 0;
            if (commaPos > 0)
                int.TryParse(x.Substring(commaPos, x.Length - commaPos), out V);
            return V;
        }

        void ListServiceParameter(ServiceParameters sp)
        {
            if (sp == null || !((LogVerbosity & LOG_ICS_SENT) > 0)) return;
            StringBuilder w = new StringBuilder("\r\n");
            foreach (var k in sp.GetKeys())
            {
                object o = sp.Get(k);
                string v = "";
                if (o != null)
                    v = o.ToString();
                if (v.Length > 80) {
                    w.AppendFormat("    [{0}] = \r\n{1}\r\n", k, v);
                } else {
                    w.AppendFormat("    [{0}] = {1}\r\n", k, v);
                }
            }
            say(w.ToString());
        }

        void ListValues(string e, IDictionary<string, object> mv, LoggingSeverity severity)
        {
            if ((LogVerbosity & LOG_ICS_RESPONSE) == 0)
                return;

            if (mv.Keys.Count > 0)
            {
                this.LogSession(severity, e + ":");
                foreach (var k in mv.Keys)
                {
                    object o = mv[k];
                    string v = "";
                    if (o!=null) v = o.ToString();
                    if (v.Length > 80)
                        v = "\r\n" + v + "\r\n";
                    this.LogSession(severity, "    {0} = {1}.", k, v);
                }
            }
        }

        /// validation routine based on field specs 

        List<ColumnSpec> GetSpecs(string fielddesc)
        {
            List<ColumnSpec> l = new List<ColumnSpec>();
            string[] a = fielddesc.Split(new char[] { ';', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            Regex rx = new Regex("([\\w_]+)\\s+([\\w])\\s?\\(\\s?([^\\)]+)\\s?\\)\\s?(.*)?");
            StringBuilder bCols = new StringBuilder("\r\nColumns Spec");
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i].isEmpty() || a[i].Trim().isEmpty())
                    continue;
                MatchCollection g = rx.Matches(a[i]);

                if (g.Count > 0 && g[0].Groups.Count > 0)
                {
                    bool req = false;
                    string desc = "";
                    if (g[0].Groups.Count > 4)
                    {
                        desc = g[0].Groups[4].Value;
                        int isbOpen = desc.IndexOf("[");
                        int isbClose = desc.IndexOf("]");
                        int iAsterisk = desc.IndexOf("*");
                        if (isbOpen >= 0 && isbClose > isbOpen)
                        {
                            desc = desc.Substring(isbOpen + 1, isbClose - isbOpen - 1);
                        }
                        req = (iAsterisk >= 0);
                        if (req & isbOpen < 0)
                            desc = desc.Replace('*', ' ').Trim();
                        else
                            desc = desc.Trim();
                    }

                    ColumnSpec c = new ColumnSpec()
                    {
                        Name = g[0].Groups[1].Value,
                        FType = g[0].Groups[2].Value.ToUpper(),
                        Len = LenOf(g[0].Groups[3].Value),
                        Dec = DecOf(g[0].Groups[3].Value),
                        Desc = desc,
                        Required = req
                    };
                    bCols.AppendFormat("{0,2} {1,-35} {2} ({3,2}) {4} {5}\r\n", i, c.Name, c.FType, c.Len, req ? "*" : " ", desc);

                    l.Add(c);
                }
            }
            if ((LogVerbosity & LOG_ICS_RAW) > 0)
                this.Say(bCols.ToString());
            return l;
        }

        int ShowProgress(int step, int pro = 1, int all = 1)
        {
            int p = steps[step - 1]
                        + (pro / ((all > pro) ? all : (pro > 0 ? pro : 1)))
                           * (steps[step] - steps[step - 1]);

            TimeSpan t = (DateTime.Now - lastReport);
            if (t.TotalMilliseconds > UpdateDelayMs)
            {
                SetProgress(p);
                lastReport = DateTime.Now;
                lastProgress = p;
            }
            return p;
        }

        int Validate(List<ColumnSpec> l, List<List<string>> data)
        {
            List<string> r = new List<string>();
            int errIndex = l.Count;
            int errs = 0;
            int rowCount = data.Count;
            DateTime lastTick = DateTime.Now;
            for (int i = 0; i < data.Count; i++)
            {
                StringBuilder err = new StringBuilder("");
                bool valid = true;
                
                if ((i+1) % CHUNK == 0 && Math.Round((DateTime.Now - lastTick).TotalSeconds) > 0)
                {
                    say("Validating ... {0} of {1} : {2}%", (i + 1), data.Count, ((i + 1) * 100 / data.Count));
                    lastTick = DateTime.Now;
                }

                if (data[i].Count < l.Count)
                {
                    LogErr("Row " + (i + 1) + " columns less than " + l.Count + ".");
                    continue;
                }
                for (int j = 0; j < l.Count; j++)
                {
                    string v = data[i][j];
                    string vname = (l[j].Desc.isEmpty()) ? l[j].Name : l[j].Desc;

                    switch (l[j].FType)
                    {
                        case "C":
                            if (!v.isEmpty() && v.Length > l[j].Len && l[j].Len > 0)
                            {
                                err.Append(string.Format(" col {0,2} '{1}' Maximum {2} characters.", j + 1, vname, l[j].Len));
                                valid = false;
                            }
                            break;
                        case "D":
                            DateTime dt;

                            if (!DateTime.TryParseExact(v, DateFmt, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dt))
                            {
                                err.Append(string.Format(" col {0,2} '{1}' invalid DATE Format({2}).", j + 1, vname, DateFmt));
                                valid = false;
                            }
                            break;
                        case "N":
                            long n;
                            if (!v.isEmpty())
                            {
                                v = v.Trim();
                                if (!long.TryParse(v, out n))
                                {
                                    err.Append(string.Format(" col {0,2} '{1}' is not numeric.", j + 1, vname));
                                    valid = false;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    if (string.IsNullOrEmpty(v) && l[j].Required)
                    {
                        err.Append(string.Format(" col {0,2} '{1}' is required.", j + 1, vname));
                        valid = false;
                    }
                }
                if (!valid)
                {
                    FillErr(data, i + 1, err.ToString(), " ");

                    errs++;
                }
                ShowProgress(Validating, i, rowCount);
            }

            return errs;
        }

        List<string> explode(string s)
        {
            List<string> b = new List<string>();

            MatchCollection c = RX.Matches(s);
            if (c.Count > 0)
            {
                for (int i = 0; i < c.Count; i++)
                {
                    if (c[i].Groups.Count > 1 && c[i].Groups[2].Value.Length > 0)
                        b.Add(c[i].Groups[2].Value);
                    else if (c[i].Groups.Count > 0 && c[i].Groups[1].Value.Length > 0)
                        b.Add(c[i].Groups[1].Value);
                    else
                        b.Add("");
                }
            }
            return b;
        }

        protected override void OnAfterRuntimeExecution(BackgroundTaskParameter parameters)
        {
            ShowProgress(Finishing, 1, 1);
            DB.Close();
            base.OnAfterRuntimeExecution(parameters);
        }

        private Dictionary<string, string> _sql = new Dictionary<string, string>();

        private string SQL(string filename)
        {
            string sql = "";

            if (_sql.ContainsKey(filename) && !_sql[filename].isEmpty())
                return _sql[filename];

            sql = sqlFromFile(filename);
            if (!sql.isEmpty())
            {
                _sql[filename] = sql;
                return sql;
            }

            sql = DatabaseManager.LoadQuery(filename);
            if (!sql.isEmpty())
            {
                _sql[filename] = sql;
                return sql;
            }
            return "";
        }

        private string sqlFromFile(string filename)
        {
            string fn = Path.Combine(Path.GetDirectoryName(this.GetType().Assembly.Location), "Queries", filename + ".sql");

            if (File.Exists(fn))
            {
                string sql = File.ReadAllText(fn);
                return sql;
            }
            else
            {
                // throw new Exception("SQL File " + filename + " not found");
                return null;
            }
        }

        private void say(string w, params object [] x)
        {
            string word = string.Format(w, x);
            this.Say(word);
            PutLog(word, userName, "LIV", processID, "MPCS00004INF", "INF", ModuleId, FunctionId);
        }

        private long PutLog(string msg, string uid, string loc = "",
            long pid = 0, string id = "INF", string typ = "INF",
            string module = ModuleId, string func = FunctionId, int sts = 0)
        {
            long r = DB.SingleOrDefault<long>(SQL("PutLog"),
                    new object[] { msg, uid, loc, pid
                    , id, typ, module, func, sts}
                );
            return r;
        }

        private void LogErr(string w, params object[] x)
        {
            this.Err(w, x);
            PutLog(string.Format(w, x), userName, "LogisticInvoiceVerification", processID);
        }
        
        private void FillErr(List<List<string>> l, int seq, string remark, string valType)
        {
            //if ((LogVerbosity & LOG_ICS_RAW) > 0)
            //    this.Say("FillErr pid={0}\t seq={1}\t\"{2}\"\t{3}", processID, seq, remark, valType);

            int ivaltype = colSpec.Count;
            int iremarks = colSpec.Count + 1;

            if (remark.Equals("null"))
                remark = "";

            if (seq > 0 && l.Count >= seq)
            {
                List<string> x = l[seq - 1];
                if (x.Count <= ivaltype)
                {
                    // this.Say("Added row#{0} Validation type {1}", seq, valType);
                    x.Add(valType);
                }
                else
                    x[ivaltype] = valType;

                if (x.Count <= iremarks)
                {
                    // this.Say("Added row#{0} remark {1}", seq, remark);
                    x.Add(remark);
                }
                else
                {
                    string prevRemark = x[iremarks];
                    remark = prevRemark + remark;

                    x[iremarks] = remark;
                }

                l[seq - 1] = x;
            }
            else
            {
                say("Row Num {0} not found", seq);
            }
        }

        void LogResult(ServiceResult r)
        {
            if ((LogVerbosity & LOG_ICS_RAW) > 0 && r!=null && r.Value!=null)
            {
                say("Result: {0}", r.Value);
            }
        }

        int SaveToTemp(List<List<string>> data)
        {
            string sql = SQL("ReplaceInvoiceUploadTemp");
            int saved = 0;
            int errCols = colSpec.Count + 1;
            DateTime lastTick = DateTime.Now;

            for (int i = 0; i < data.Count; i++)
            {
                List<string> d = data[i];
                if (((i + 1) % CHUNK == 0 
                      && Math.Round((DateTime.Now - lastTick).TotalSeconds) > 0) 
                    || (i == data.Count - 1))
                {
                    say("Write TB_T_INV_UPLOAD {0} of {1} : {2}%", 
                        (i + 1), data.Count, ((i + 1) * 100 / data.Count));
                    lastTick = DateTime.Now;
                }

                object[] x = new object[28];
                
                for (int j = 1; j <= 26; j++)
                {
                    x[j + 1] = null;
                    if (d.Count < (j + 1))
                    {
                        continue;
                    }

                    if (j < colSpec.Count)
                        switch (colSpec[j].FType)
                        {
                            case "D":
                                x[j + 1] = d[j].YMD();
                                break;
                            case "N":
                                x[j + 1] = d[j].Dec();
                                break;
                            default:
                                x[j + 1] = d[j];
                                break;

                        }
                    else
                        x[j + 1] = d[j];
                }

                DB.Execute(sql, new object[] { 
                    processID, 
                    i + 1, 
                    x[ 2], x[ 3], x[ 4], x[ 5],
                    x[ 6], x[ 7], x[ 8], x[ 9], x[10],
                    x[11], x[12], x[13], x[14], x[15],
                    x[16], x[17], x[18], x[19], x[20],
                    x[21], x[22], x[23], x[24], x[25],
                    x[26], x[27]});
                
                if ((LogVerbosity & LOG_ICS_RAW) > 0)
                {
                    StringBuilder b = new StringBuilder("");
                    b.Append(i + 1);
                    for (int irow = 2; irow <= 26; irow++)
                    {
                        b.Append(";");
                        b.Append(x[irow]);
                    }
                    this.Say(b.ToString());
                }

                saved += 1;
            }

            return saved;
        }

        void ClearTemp()
        {
            say("DeleteInvoiceUploadTemp {0}", suppCode);
            DB.Execute(SQL("DeleteInvoiceUploadTemp"), new object[] { suppCode });
        }

        void SaveData(List<List<string>> data)
        {
            say("UpdateIcsProcessId {0}", processID);
            DB.Execute(SQL("UpdateIcsProcessId"), new object[] { processID });

            say("Saving {0} Invoice upload rows", data.Count);
            SaveToTemp(data); 
        }

        void AddNotification(string title, string content)
        {
            this.Say("AddNotification \"{0}\", \"{1}\", \"{2}\", \"{3}\"",
                title, content, userName, conf["/author"]);
            DB.Execute(SQL("AddNotification"), new object[] 
                { title, content, userName, conf["/author"] });
        }

        private void UpdateInterfaceStatus(int status, string refer)
        {
            say("UpdateICSInterfaceStatus {0} {1} {2} {3}",
                processID, status, refer, userName);
            DB.Execute(SQL("UpdateICSInterfaceStatus"), new object[] { processID, status, refer, userName });
        }

        private InterfaceStatus GetInterfaceStatus()
        {
            return DB.Fetch<InterfaceStatus>(SQL("GetICSInterfaceStatus"), 
                    new object[] { processID }).FirstOrDefault();
        }

        public string Numerize(int x)
        {
            string n = "._-=>!*#";
            string r = "";
            int i = 0;
            while (x > 0 && i < n.Length)
            {
                int m = x % 10;
                r = r + new string(n.Substring(i, 1)[0], m);
                x = x / 10;
                i++;
            }
            return r;
        }

        private bool Wait()
        {
            DateTime Started = DateTime.Now;
            int wait = conf.IntVal("/WaitMaxLoop", 3600);
            int waitMs = conf.IntVal("/WaitMs", 1000);

            InterfaceStatus i = new InterfaceStatus() { Status = 9 };
            say("Waiting for ICS process to complete ...");
            int j = 0;
            do {
                i = GetInterfaceStatus();
                Thread.Sleep(waitMs);
                j++;
                this.Say(Numerize(j));
                if (j > wait)
                    break;
            } while(i.Status == 2 || i.Status == 0);
            say("Waiting complete, last Status = {0} - {1} ", i.Status, ProcessStatus(i.Status));
            return (i.Status == 3);
        }

        private string ProcessStatus(int status)
        {
            string r = "";
            switch (status)
            {
                case 0: r = "Started"; break;
                case 1: r = "Error"; break;
                case 2: r = "Running"; break;
                case 3: r = "Done"; break;
                default: 
                    r = "Wait Timeout Exit";
                    UpdateInterfaceStatus(status, sid.ToString());
                    break;
            }
            return r;
        }

        private  byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public static string Base52(long l)
        {
            const byte BASE = 52;
            const string a = "etaoinshrdlucmfwypvbgkjqxzETAOINSHRDLUCMFWYPVBGKJQXZ";
            StringBuilder x = new StringBuilder("");
            byte n;

            do
            {
                n = (byte)(l % BASE);
                x.Insert(0, a.Substring(n, 1));
                l = (l - n) / BASE;
            }
            while (l > 0);
            return x.ToString();
        }

        public static void ReMoveFile(string n)
        {
            if (File.Exists(n))
            {
                int i = 0;
                string newname;
                do
                {
                    string ss = "_"
                        + Base52(
                            (int)Math.Round(DateTime.Now.TimeOfDay.TotalSeconds))
                        + Base52(
                            (int)Math.Round((double)DateTime.Now.Millisecond / 38)
                            )
                        + Base52(i++);
                    newname = Path.Combine(Path.GetDirectoryName(n), Path.GetFileNameWithoutExtension(n) + ss + Path.GetExtension(n));
                } while (File.Exists(newname));
                File.Move(n, newname);
            }
        }

    }
}
