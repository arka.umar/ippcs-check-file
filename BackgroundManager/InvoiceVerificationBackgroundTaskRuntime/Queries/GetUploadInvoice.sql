-- GetUploadInvoice

SELECT TOP 1
	PROCESS_ID AS [ProcessId] 
	, ICS_PROCESS_ID AS [IcsProcessId]
	, USERNAME AS [UserName] 
	, [FILENAME] AS [FileName]
	, [FILESIZE] AS [FileSize]
	, [BODY] AS [Body]
	, [CREATED_BY] AS [CreatedBy]
	, [CREATED_DT] AS [CreatedDate] 
	, [CHANGED_BY] AS [ChangedBy]
	, [CHANGED_DT] AS [ChangedDate] 
FROM TB_T_INV_UPLOAD_FILE
WHERE PROCESS_ID = @0;