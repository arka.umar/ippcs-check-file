-- ReplaceInvoiceUploadTemp

DECLARE 
    @@PROCESS_ID BIGINT,
	@@ROW_NO INT,
	@@SUPP_CD varchar(6) ,
	@@PO_NO varchar(10) ,
	@@PO_ITEM_NO varchar(5) ,
	@@ORI_MAT_NO varchar(23) ,
	@@MAT_NO varchar(23) ,
	@@PROD_PURPOSE_CD varchar(5) ,
	@@SOURCE_TYPE varchar(1) ,
	@@PACKING_TYPE varchar(1) ,
	@@PART_COLOR_SFX varchar(2) ,
	@@MAT_TEXT varchar(40) ,
	@@MAT_DOC_NO varchar(10) ,
	@@MAT_DOC_YEAR VARCHAR(4), 
	@@MAT_DOC_ITEM varchar(4) ,
	@@COMP_PRICE_CD varchar(4) ,
	@@SUPP_MANIFEST varchar(16) ,
	@@DOC_DT date ,
	@@QUANTITY numeric(13, 3) ,
	@@ORDER_NO varchar(10) ,
	@@DOCK_CD varchar(2) ,
	@@SUPP_INV_NO varchar(12) ,
	@@S_QUANTITY numeric(13, 3) ,
	@@PRICE_AMT numeric(16, 5) ,
	@@PAYMENT_CURR varchar(3) ,
	@@AMMOUNT numeric(16, 5) ,
	@@VALIDATION_TYPE varchar(1) ,
	@@REMARKS varchar(8000);
	

SET @@PROCESS_ID = @0;
SET @@ROW_NO=@1;
SET @@SUPP_CD=@2;
SET @@PO_NO=@3;
SET @@PO_ITEM_NO=@4;
SET @@ORI_MAT_NO=@5;
SET @@MAT_NO=@6;
SET @@PROD_PURPOSE_CD=@7;
SET @@SOURCE_TYPE=@8;
SET @@PACKING_TYPE=@9;
SET @@PART_COLOR_SFX=@10;
SET @@MAT_TEXT=@11;
SET @@MAT_DOC_NO=@12;
SET @@MAT_DOC_YEAR=@13;
SET @@MAT_DOC_ITEM=@14;
SET @@COMP_PRICE_CD=@15;
SET @@SUPP_MANIFEST=@16;
SET @@DOC_DT=@17;
SET @@QUANTITY=@18;
SET @@ORDER_NO=@19;
SET @@DOCK_CD=@20;
SET @@SUPP_INV_NO=@21;
SET @@S_QUANTITY=@22;
SET @@PRICE_AMT=@23;
SET @@PAYMENT_CURR=@24;
SET @@AMMOUNT=@25;
SET @@VALIDATION_TYPE=@26;
SET @@REMARKS=@27;


	
	
IF EXISTS(SELECT TOP 1 INV_REF_NO FROM TB_T_INV_UPLOAD 
          WHERE PROCESS_ID = @@PROCESS_ID AND ROW_NO = @@ROW_NO) 
BEGIN 
	UPDATE TB_T_INV_UPLOAD 
	SET 
	    SUPP_CD             = @@SUPP_CD
	  , PO_NO               = @@PO_NO             
	  , PO_ITEM_NO          = @@PO_ITEM_NO        
	  , MAT_NO_REF          = @@ORI_MAT_NO        
	  , MAT_NO              = @@MAT_NO            
	  , PROD_PURPOSE_CD     = @@PROD_PURPOSE_CD   
	  , SOURCE_TYPE_CD      = @@SOURCE_TYPE       
	  , PACKING_TYPE        = @@PACKING_TYPE      
	  , PART_COLOR_SFX      = @@PART_COLOR_SFX    
	  , MAT_TEXT            = @@MAT_TEXT          
	  , MAT_DOC_NO          = @@MAT_DOC_NO        
	  , MAT_DOC_YEAR        = @@MAT_DOC_YEAR
	  , MAT_DOC_ITEM        = @@MAT_DOC_ITEM      
	  , COMP_PRICE_CD       = @@COMP_PRICE_CD     
	  , INV_REF_NO          = @@SUPP_MANIFEST     
	  , DOC_DT              = @@DOC_DT            
	  , QTY                 = @@QUANTITY          
	  , ORDER_NO            = @@ORDER_NO          
	  , DOCK_CD             = @@DOCK_CD           
	  , SUPP_INV_NO         = @@SUPP_INV_NO       
	  , S_QTY               = @@S_QUANTITY        
	  , PRICE_AMT           = @@PRICE_AMT         
	  , PAY_CURR            = @@PAYMENT_CURR      
	  , AMT                 = @@AMMOUNT           
	  , VALIDATION_TYPE     = @@VALIDATION_TYPE   
	  , REMARKS             = @@REMARKS           
	WHERE PROCESS_ID = @@PROCESS_ID AND ROW_NO = @@ROW_NO;
END 
ELSE 
BEGIN

	INSERT INTO dbo.TB_T_INV_UPLOAD
	(
	  PROCESS_ID
	  , ROW_NO
	  , SUPP_CD
	  , PO_NO
	  , PO_ITEM_NO
	  , MAT_NO_REF
	  , MAT_NO
	  , PROD_PURPOSE_CD
	  , SOURCE_TYPE_CD
	  , PACKING_TYPE
	  , PART_COLOR_SFX
	  , MAT_TEXT
	  , MAT_DOC_NO
	  , MAT_DOC_YEAR 
	  , MAT_DOC_ITEM
	  , COMP_PRICE_CD
	  , INV_REF_NO
	  , DOC_DT
	  , QTY
	  , ORDER_NO
	  , DOCK_CD
	  , SUPP_INV_NO
	  , S_QTY
	  , PRICE_AMT
	  , PAY_CURR
	  , AMT
	  , VALIDATION_TYPE
	  , REMARKS
	)
	VALUES
	(
	  @@PROCESS_ID
	  , @@ROW_NO
	  , @@SUPP_CD
	  , @@PO_NO
	  , @@PO_ITEM_NO
	  , @@ORI_MAT_NO
	  , @@MAT_NO
	  , @@PROD_PURPOSE_CD
	  , @@SOURCE_TYPE
	  , @@PACKING_TYPE
	  , @@PART_COLOR_SFX
	  , @@MAT_TEXT
	  , @@MAT_DOC_NO
	  , @@MAT_DOC_YEAR
	  , @@MAT_DOC_ITEM
	  , @@COMP_PRICE_CD
	  , @@SUPP_MANIFEST
	  , @@DOC_DT
	  , @@QUANTITY
	  , @@ORDER_NO
	  , @@DOCK_CD
	  , @@SUPP_INV_NO
	  , @@S_QUANTITY
	  , @@PRICE_AMT
	  , @@PAYMENT_CURR
	  , @@AMMOUNT
	  , @@VALIDATION_TYPE
	  , @@REMARKS
	);
END;