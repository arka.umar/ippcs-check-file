-- DeleteICSInvoiceUploadTemp

DECLARE @@suppCode VARCHAR(12);
SET @@suppCode = @0;

DECLARE @@SQL VARCHAR(MAX);
SET @@SQL = 'DELETE OPENQUERY(IPPCS_TO_ICS, '
+  '''SELECT * ' 
+ ' FROM TB_T_INVOICE_UPLOAD ' 
+ ' WHERE SUPP_CD = ''''' +  @@suppCode + ''''''');';

EXEC(@@SQL);