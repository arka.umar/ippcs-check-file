﻿-- PutLog

declare @@process_id BIGINT;

SET @@process_id = @3;

EXEC	[dbo].[sp_PutLog]
		@@what = @0,
		@@user = @1,
		@@where = @2,
		@@pid = @@process_id OUTPUT,
		@@id = @4,
		@@type = @5,
		@@module = @6,
		@@func = @7,
		@@sts = @8;

SELECT @@process_id PROCESS_ID;
