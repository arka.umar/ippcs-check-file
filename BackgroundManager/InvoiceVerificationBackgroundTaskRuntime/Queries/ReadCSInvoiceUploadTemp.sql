-- ReadICSInvoiceUploadTemp

DECLARE 
	@@PROCESS_ID BIGINT, 
	@@ICS_PROCESS_ID INT, 
	@@SUPP_CD VARCHAR(6),
	@@SQL VARCHAR(MAX);
	
SET @@PROCESS_ID = @0; 
SET @@SUPP_CD = @1;
SET @@SQL= 'SELECT OPENQUERY(IPPCS_TO_ICS,''' + 
	'SELECT  PROCESS_ID, ROW_NO, VALIDATION_TYPE, REMARKS ' + 
	' FROM TB_T_INVOICE_UPLOAD WHERE SUPP_CD = ''' + @@SUPP_CD + '''' + 
	' AND OTHER_PROCESS_ID = ' + @@PROCESS_ID + ''');';
EXEC(@@SQL);