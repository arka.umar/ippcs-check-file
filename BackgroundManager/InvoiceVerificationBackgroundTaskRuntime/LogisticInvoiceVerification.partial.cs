﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InvoiceVerificationBackgroundTaskRuntime.Models;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.SimpleConfig;
using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Util.Text;
using Toyota.Common.Web.Service;

namespace InvoiceVerificationBackgroundTaskRuntime
{
    partial class LogisticInvoiceVerification : ExternalBackgroundTaskRuntime
    {
        #region unused codes

        /// <summary>
        /// not used in LIV
        /// </summary>
        void ProcessInquiry()
        {
            //Info("Start in inquiry state ICS web service.");

            //int totalPage = Convert.ToInt32(result.Value != null ? result.Value : 0);
            //sp.Clear();
            //sp.Add("state", "inquiry");
            //sp.Add("sessionId", sessionID);
            //sp.Add("processId", processID);
            //int seq_no = 1;

            //for (int i = 1; i <= totalPage; i++)
            //{
            //    try
            //    {
            //        sp.Add("currentPage", i);
            //        sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
            //        result = ServiceResult.Create(gateway.execute(p0, p1, sp.ToString()));
            //    }
            //    catch (Exception exc)
            //    {
            //        if (exc.Message.Contains("Timeout") || exc.Message.Contains("timeout"))
            //        {
            //            sp.Add("currentPage", i);
            //            sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
            //            result = ServiceResult.Create(gateway.execute(p0, p1, sp.ToString()));
            //        }
            //        this.Err("Error do process inquiry state ICS web service. " + exc.Message);


            //        throw;
            //    }

            //    if (result.Status != ServiceResult.STATUS_ERROR)
            //    {
            //        if (result.Value == null)
            //        {
            //            Warn("No data found do process inquiry state ICS web service.");
            //        }

            //        //List<GoodsReceiptICSFinish> values = new List<GoodsReceiptICSFinish>();
            //        //values = JSON.ToObject<List<GoodsReceiptICSFinish>>(result.Value.ToString());
            //        Nullable<DateTime> postingDate = null;
            //        Nullable<DateTime> processDate = null;

            //        // clear transaction log before process
            //        Info("Do process delete log transaction table by manifest no.");

            //        //DB.Execute(DatabaseManager.LoadQuery("DeleteLogTransactionByManifest"), new object[] { values[0].oriMatNo });
            //        Info("Do process inserting data from ICS web service.");
            //        //foreach (GoodsReceiptICSFinish e in values)
            //        //{
            //        //    if (!String.IsNullOrEmpty(e.postingDate))
            //        //        postingDate = Convert.ToDateTime(e.postingDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));
            //        //    if (!String.IsNullOrEmpty(e.processDate))
            //        //        processDate = Convert.ToDateTime(e.processDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));

            //        //    DB.ExecuteScalar<string>(DatabaseManager.LoadQuery("ICSInsertPostingFileGR"), new object[] { e.systemSource, e.userId, e.movementType, 
            //        //                postingDate, processDate, e.refNo, e.kanbanOrderNo, e.prodPurpose, 
            //        //                e.sourceType, e.oriMatNo, e.oriSuppCode, e.receivingPlantArea,
            //        //                e.plantCode, e.slocCode, e.matDocNo, e.matDocYear, e.errorStatus, e.errorCode, 
            //        //                e.errorDesc, e.errorMsg, processID, e.sessionId, username, process, seq_no});

            //        //    seq_no++;
            //        //}

            //        //if (values[0].errorStatus == "0")
            //        //{
            //        //    // clear transaction log if process is success (error status is 0).
            //        //    Info("Do process delete log transaction table by manifest no.");
            //        //    DB.Execute(DatabaseManager.LoadQuery("DeleteLogTransactionByManifest"), new object[] { values[0].oriMatNo });
            //        //}
            //        //if (values[0].errorStatus == "1")
            //        //{
            //        //    Warn("Fail do process inserting data from ICS web service.");
            //        //}

            //        // clear result posting log after process
            //        Info("Do process delete log posting result table by process id.");

            //        DB.Execute(DatabaseManager.LoadQuery("DeleteLogResultPostingByProcessId"), new object[] { processID });

            //        // update daily order manifest detail with error result (if exists)
            //        Info("Do process update daily order manifest detail table.");
            //        //DB.Execute(DatabaseManager.LoadQuery("UpdateDailyOrderManifestDetail"), new object[] { values[0].receivingPlantArea, "", values[0].refNo, username, DateTime.Now });
            //    }
            //    else
            //    {
            //        Warn("Fail do process inquiry state ICS web service.");


            //        transferError = true;
            //    }
            //}
            //Info("Finish in inquiry state ICS web service.");
        }

        void DoStartUpIntegrityCheck()
        {
            /* 
            try
            {
                gateway.Open();
                string testStringResult = gateway.execute("Startup", "IntegrityCheck", null);
                if (!string.IsNullOrEmpty(testStringResult))
                {
                    result = ServiceResult.Create(testStringResult);
                    serviceReady = (result.Status == ServiceResult.STATUS_READY);
                }
            }
            catch (Exception exc)
            {
                gateway.Abort();
                gateway = new GatewayService.WebServiceImplClient();
                gateway.Open();

                this.Err("Error do process startup ICS web service. {0}" + exc.Message);

                throw ;
            }
            
            if (!serviceReady)
            {
                Warn("Fail do process startup ICS web service.");
            }
            */
        }



        bool WsTransfer(GatewayService.WebServiceImplClient gateway, List<List<string>> list, ref int totalPage)
        {
            string pX;
            bool ok = false;
            ServiceParameters sp = new ServiceParameters();
            ServiceResult result;

            string jsonResult;
            bool transferError = true;

            pX = "transfer";
            if (pX.Equals(stopMark))
                return !transferError;
            ws(pX);
            sp.Clear();
            sp.Add("state", pX);
            sp.Add("sessionId", sid.ToString());
            sp.Add("data", String.Empty);

            int rowCount = list.Count;
            int lastIndex = rowCount;
            int counter = 0;
            bool proceed = true;

            int rowNum = 0;
            this.Say("maxDataPerPage = {0}.", CHUNK);
            totalPage = (int)Math.Ceiling((decimal)rowCount / CHUNK);
            while (rowNum < rowCount && proceed)
            {
                int chunkrow = 0;
                StringBuilder b = new StringBuilder("");
                List<InvoiceVerificationTransfer> t = new List<InvoiceVerificationTransfer>();

                while (rowNum < rowCount && chunkrow < CHUNK && proceed)
                {
                    List<string> d = list[rowNum];
                    t.Add(new InvoiceVerificationTransfer()
                    {
                        suppCode = d[1],
                        poNo = d[2],
                        poItemNo = d[3],
                        oriMatNo = d[4],
                        matNo = d[5],
                        prodPurposeCode = d[6],
                        sourceType = d[7],
                        packingType = d[8],
                        partColorSfx = d[9],
                        matText = d[10],
                        matDocNo = d[11],
                        matDocItem = d[12],
                        compPriceCode = d[13],
                        suppManifest = d[14],
                        docDate = d[15].YMD(),
                        quantity = d[16].Dec(),
                        orderNo = d[17],
                        dockCode = d[18],
                        suppInvNo = d[19],
                        sQuantity = d[20].Dec(),
                        priceAmt = d[21].Dec(),
                        paymentCurr = d[22],
                        amount = d[23].Dec()
                    });

                    rowNum++;

                    chunkrow++;
                }

                jsonResult = JSON.ToString<List<InvoiceVerificationTransfer>>(t);
                sp.Remove("data");
                sp.Add("userId", userName);
                sp.Add("sessionId", sid.ToString());
                sp.Add("data", (object)jsonResult);
                ShowProgress(Transfering, rowNum, rowCount);
                this.Say(pX + "# {0} of {1}.", ++counter, totalPage);

                result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
                LogResult(result);
                ListServiceParameter(sp);

                ok = (result.Status == ServiceResult.STATUS_SUCCESS);
                ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
                ws(pX, ok);

                if (!ok)
                {
                    transferError = true;
                    proceed = false;
                }

            }

            return !transferError;
        }


        bool WsInquiry(GatewayService.WebServiceImplClient gateway, List<List<string>> list, ref int totalPage)
        {
            string pX;
            bool ok = false;
            ServiceParameters sp = new ServiceParameters();
            ServiceResult result;

            pX = "inquiry";
            if (pX.Equals(stopMark))
                return false;

            ws(pX);
            sp.Clear();
            sp.Add("state", pX);
            sp.Add("sessionId", sid.ToString());
            sp.Add("userId", userName);
            sp.Add("suppCode", suppCode);
            sp.Add("maxDataPerPage", CHUNK);

            for (int activePage = 1; activePage <= totalPage; activePage++)
            {
                sp.Add("currentPage", activePage);
                this.Say(pX + " #{0} of {1}", activePage, totalPage);
                ListServiceParameter(sp);
                result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
                LogResult(result);
                ok = result.Status != ServiceResult.STATUS_ERROR;
                ws(pX, ok);
                ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
                if (ok)
                {
                    /// process Inquiry
                    SetProgress(steps[Inquiry]);
                    LogResult(result);
                    List<InvoiceUploadResult> values = JSON.ToObject<List<InvoiceUploadResult>>(result.Value.ToString());
                    int vCount = (values != null) ? values.Count : 0;
                    this.Say(pX + " #{0}: {1} rows", activePage, vCount);

                    for (int j = 0; j < values.Count; j++)
                    {
                        InvoiceUploadResult r = values[j];
                        if ((LogVerbosity & LOG_ICS_RAW) > 0)
                            this.Say("FillErr seq={0}\tremark=\"{1}\"\tvalType={2}", r.rowNo, r.remarks, r.valType);

                        FillErr(list, r.rowNo, r.remarks, r.valType);
                    }
                }
                else
                {
                    break;
                }
            }

            return ok;

        }

        /// change ICS interfacing design renders old code unusable hence this section
        void WSUpload(List<List<string>> list)
        {


            bool transferError = false;
            this.Say("ICS Open WebService Gateway : " + WsModule + " " + WsAction);
            int totalPage = 0;
            GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
            bool useLinked = conf.IntVal("/transfer/useLinked", 1) != 0;
            ServiceResult result;

            CHUNK = conf.IntVal("/transfer/chunk", CHUNK);

            DoStartUpIntegrityCheck();
            ServiceParameters sp = new ServiceParameters();

            string pX = null;
            bool ok = false;
            string sessionID = null;

            string jsonResult;

            pX = "init";
            ws(pX);
            sp.Add("state", pX);
            ListServiceParameter(sp);
            result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
            LogResult(result);
            ok = (result.Status != ServiceResult.STATUS_ERROR);
            ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
            ws(pX, ok);

            do
            {
                if (!ok)
                {
                    break;
                }
                sessionID = result.Value.ToString();
                int.TryParse(sessionID, out sid);
                this.Say("sessionID = {0}", sid);

                UpdateInterfaceStatus(0, sid.ToString());

                /// delete previous supplier process id tb_t_invoice_upload through linked server 
                /// insert ics tb_t_invoice_upload using web service 
                ok = WsTransfer(gateway, list, ref totalPage);

                SetProgress(steps[Transfering]);

                if (transferError)
                    break;

                pX = "process";
                if (pX.Equals(stopMark)) break;
                ws(pX);
                sp.Clear();
                sp.Add("state", pX);
                sp.Add("sessionId", sessionID);

                sp.Add("userId", userName);
                sp.Add("suppCode", suppCode);
                sp.Add("maxDataPerPage", CHUNK);
                ListServiceParameter(sp);

                result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
                LogResult(result);
                ok = result.Status != ServiceResult.STATUS_ERROR;
                ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
                ws(pX, ok);

                if (ok)
                {
                    /// process Inquiry
                    int.TryParse((string)result.Value, out totalPage);
                    this.Say("totalPage = {0}", totalPage);
                    SetProgress(steps[Processing]);
                }
                else
                {
                    break;
                }
                if (useLinked)
                    ok = Wait();


                WsInquiry(gateway, list, ref totalPage);
                SetProgress(steps[Inquiry]);

                SaveData(list);
                SetProgress(steps[Feedback]);

            } while (false);


            pX = "finish";

            ws(pX);
            sp.Clear();
            sp.Add("state", "finish");
            sp.Add("sessionId", sessionID);
            ListServiceParameter(sp);
            result = ServiceResult.Create(gateway.execute(WsModule, WsAction, sp.ToString()));
            LogResult(result);
            sp.Clear();
            ok = !(result.Status == ServiceResult.STATUS_ERROR);
            ws(pX, ok);

            ListValues(pX, result.MappedValues, ok ? LoggingSeverity.Info : LoggingSeverity.Error);
            this.Say("ICS Gateway Close");
            gateway.Close();
        }

        #endregion unused codes

    }
}
