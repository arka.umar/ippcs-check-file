﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceVerificationBackgroundTaskRuntime.Models
{
    public class InvoiceUploadResult
    {
        public long processId  { get; set; }
        public int rowNo{ get; set; }
        public string valType { get; set; }
        public string remarks { get; set; } 
    }
}
