﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceVerificationBackgroundTaskRuntime.Models
{
    public class InterfaceStatus
    {
        public long ProcessId { get; set; }
        public int Status { get; set; }
        public string Reference { get; set; } 
    }
}
