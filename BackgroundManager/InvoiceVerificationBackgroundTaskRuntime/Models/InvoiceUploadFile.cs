﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceVerificationBackgroundTaskRuntime.Models
{
    public class InvoiceUploadFile
    {
        public long ProcessId { get; set; }
        public long? IcsProcessId { get; set; }
        public string UserName { get; set; }
        public string FileName { get; set; }
        public int? FileSize { get; set; }
        public string Body { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ChangedBy { get; set; } 
        public DateTime? ChangedDate { get; set; }
    }
}

