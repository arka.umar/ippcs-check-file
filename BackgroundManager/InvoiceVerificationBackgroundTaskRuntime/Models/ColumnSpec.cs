﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceVerificationBackgroundTaskRuntime
{
    public class ColumnSpec
    {
        public string Name { get; set; }
        public string FType { get; set; }
        public int Len { get; set; }
        public int Dec { get; set; }
        public string Desc { get; set; }
        public bool Required { get; set; } 
    }
}
