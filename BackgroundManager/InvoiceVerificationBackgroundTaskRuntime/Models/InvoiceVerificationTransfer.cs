﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceVerificationBackgroundTaskRuntime.Models
{
    public class InvoiceVerificationTransfer
    {
        public string suppCode { get; set; }
        public string poNo { get; set; }
        public string poItemNo { get; set; }
        public string oriMatNo { get; set; }
        public string matNo { get; set; }
        public string prodPurposeCode { get; set; }
        public string sourceType { get; set; }
        public string packingType { get; set; }
        public string partColorSfx { get; set; }
        public string matText { get; set; }
        public string matDocNo { get; set; }
        public string matDocItem { get; set; }
        public string compPriceCode { get; set; }
        public string suppManifest { get; set; }
        public string docDate { get; set; }
        public decimal quantity { get; set; }
        public string orderNo { get; set; }
        public string dockCode { get; set; }
        public string suppInvNo { get; set; }
        public decimal sQuantity { get; set; }
        public decimal priceAmt { get; set; }
        public string paymentCurr { get; set; }
        public decimal amount { get; set; }
    }
}
