﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Toyota.Common.Task;
using Toyota.Common.SimpleConfig;

namespace InvoiceVerificationBackgroundTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            LogisticInvoiceVerification job = new LogisticInvoiceVerification();

            try
            {
                if (args.Length == 4) /// specific for command line invocation 
                {
                    BackgroundTaskParameter p = new BackgroundTaskParameter();
                    p.Add("ProcessID", args[0]);
                    p.Add("UserID", args[1]);
                    p.Add("SuppCode", args[2]);
                    p.Add("FileName", args[3]);

                    job.Execute(p.ToString());
                }
                else
                {
                    /// regular invocation through Background job

                    if (args != null && args.Length > 0)
                    {
                        job.ResetLog(job.GetLogFileName());
                    }
                    job.ExecuteExternal(args);
                    job.SetStatus(TaskStatus.Finished);
                }
            }
            catch (Exception ex)
            {
                job.SetStatus(TaskStatus.Error);
                job.Die("Exception {0}\r\n{1}\r\n{2}", ex.Message, ex.InnerException, ex.StackTrace);
                job.Log("{0}\r\n{1}", ex.Message, ex.StackTrace);
            }
            job.SetProgress(100);

        }

        static void TestXml()
        {
            string xFile = Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory,
                System.AppDomain.CurrentDomain.FriendlyName.CleanNameOf() + ".xml");
            XmlConfigReader conf = new XmlConfigReader();
            conf.Read(xFile);

            int UpdateDelayMs = conf.IntVal("/UpdateDelay", 500);
            string transferFields = conf["/transfer/fields"];
        }
    }
}
