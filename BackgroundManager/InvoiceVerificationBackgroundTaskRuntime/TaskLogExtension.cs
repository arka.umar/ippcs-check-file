﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Task.External;
using Toyota.Common.Logging;

namespace InvoiceVerificationBackgroundTaskRuntime
{
    public static class TaskLogExtension
    {
        public static void LogSession(this ExternalBackgroundTaskRuntime o, LoggingSeverity l, string w, params object[] x)
        {
            o.DefaultLogSession.WriteLine(new LoggingMessage() { Severity = l, Message = string.Format(w, x) });            
        }        

        public static void Die(this ExternalBackgroundTaskRuntime o, string w, params object[] x)
        {
            LogSession(o, LoggingSeverity.Fatal, w, x);
        }

        public static void Err(this ExternalBackgroundTaskRuntime o, string w, params object[] x)
        {
            LogSession(o, LoggingSeverity.Error, w, x);
        }

        public static void Warn(this ExternalBackgroundTaskRuntime o, string w, params object[] x)
        {
            LogSession(o, LoggingSeverity.Warning, w, x);
        }

        public static void Say(this ExternalBackgroundTaskRuntime o, string w, params object[] x)
        {
            LogSession(o, LoggingSeverity.Info, w, x);
        }
    }
}
