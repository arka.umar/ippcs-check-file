﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LPOPCompletenessCheck
{
    class MailData
    {
        public List<string> MailRecipient { get; set; }
        public List<string> MailCarbonCopy { get; set; }
        public List<string> MailBlindCarbonCopy { get; set; }
        public List<string> MailAttachmentPath { get; set; }
        public List<Byte[]> Attachment { get; set; }
        public string AttachmentName { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }

        public bool usingCarbonCopy { get; set; }
        public bool usingBlindCarbonCopy { get; set; }
        public bool withAttachment { get; set; }
        public bool useHTMLBody { get; set; }
    }

    class MailConfig
    {
        public string MailHost { get; set; }
        public int MailPort { get; set; }
        public string MailSenderAliases { get; set; }
        public string MailSender { get; set; }
        public string MailSenderPassword { get; set; }
        public bool useAnonymousCredential { get; set; }
        public bool enableSLL { get; set; }
        public int maxAttachment { get; set; }
        public bool useDefaultCredential { get; set; }
    }
}
