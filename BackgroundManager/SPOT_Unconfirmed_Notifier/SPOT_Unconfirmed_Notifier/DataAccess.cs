﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace SPOT_Unconfirmed_Notifier
{
    class DataAccess
    {
        public string SQLPathTo(string sqlfilename)
        {
            string SQLPath = AppDomain.CurrentDomain.BaseDirectory + "SQLFiles/";
            string SQLExtension = ".sql";
            return SQLPath + sqlfilename + SQLExtension;
        }

        public string Query(string SQLFileName)
        {
            string query = "";
            try
            {
                query = File.ReadAllText(this.SQLPathTo(SQLFileName));
            }
            catch (Exception e)
            {
                throw e;
            }
            return query;
        }

        public DbConnection dbconnection()
        {
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["PCS"];
            string connectionString = connectionStringSettings.ConnectionString;
            return DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
        }

        public string connectionstring()
        {
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["PCS"];
            return connectionStringSettings.ConnectionString;
        }

        public Dictionary<string, string> ConnectionStrings()
        {
            Dictionary<string, string> connstr = new Dictionary<string, string>();
            XmlDocument XDoc = new XmlDocument();
            XDoc.Load(AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["ConnectionStringsConfig"]);

            for (int i = 0; i < XDoc.DocumentElement.ChildNodes.Count; i++)
            {
                if (XDoc.DocumentElement.ChildNodes[i].Name.Equals("connectionStrings"))
                {
                    foreach (XmlNode xnode in XDoc.DocumentElement.ChildNodes[i])
                    {
                        XmlElement xelement = (XmlElement)xnode;
                        if (xelement.Attributes["isActive"].Value.Equals("true"))
                        {
                            connstr.Add(xelement.Attributes["name"].Value, xelement.Attributes["connectionString"].Value);
                        }
                    }
                }
            }
            return connstr;
        }

        public string SingleConnection(string serverName)
        {
            string connectionString = "";

            Dictionary<string, string> connections = ConnectionStrings();
            
            foreach(KeyValuePair<string, string> k in connections)
            {
                if (serverName.Equals(k.Key))
                {
                    connectionString = k.Value;
                    break;
                }
            }

            return connectionString;
        }
    }
}
