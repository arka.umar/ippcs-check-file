﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using SuppFeedbackFirmTaskRuntime.Model;
using System.Reflection;

namespace SuppFeedbackFirmTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {

            SuppFeedbackFirmTaskRuntime grRuntime = new SuppFeedbackFirmTaskRuntime();
            
            grRuntime.ExecuteExternal(args);

            #region New passing parameter method
            BackgroundTaskParameter parameters = new BackgroundTaskParameter();
            ////parameters.Add("GridId", "August-2013_Firm_lpd.testing10");
            //parameters.Add("productionMonth", args[0]);
            //grRuntime.ExecuteExternal(new String[] { parameters.ToString() });
            #endregion
             
        }
    }
}
