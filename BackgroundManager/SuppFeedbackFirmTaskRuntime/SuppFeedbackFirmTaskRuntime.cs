﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using SuppFeedbackFirmTaskRuntime.Model;
using System.Reflection;
using Toyota.Common.Logging.Sink;
using System.IO;
using System.Configuration;
//using Toyota.Common.Web.Notifcation.JobMonitoring;
//using Toyota.Common.Web.Ioc;
using Toyota.Notification.Email;


namespace SuppFeedbackFirmTaskRuntime
{
    public class SuppFeedbackFirmTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(SuppFeedbackFirmTaskRuntime)), "SuppFeedbackFirmTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            //IBootstrap bootstrap = Bootstrap.GetInstance();
            //bootstrap.RegisterProvider<Toyota.Common.Web.Database.IDBContextManager>(typeof(Toyota.Common.Web.Database.Petapoco.PetaPocoContextManager), true);

            String productionMonth = parameters.Get("productionMonth");
            
            if (productionMonth == "")
            {
                Console.WriteLine("args is null"); // Check for null array
            }
            else
            {
                SetProgress(5);
                List<listsupplier> listSuplier = new List<listsupplier>();
                List<listFix> listtentative = new List<listFix>();

                string dateExpired;
                string attachID = "";
                string SupplierCode = "";
                string functionID = ConfigurationManager.AppSettings["functionID"];
                string contentID = ConfigurationManager.AppSettings["contentID"];
                string fileLocation = ConfigurationManager.AppSettings["fileLocation"].ToString();
                ArgumentParameter param = new ArgumentParameter();
                ArgumentParameter keyParam = new ArgumentParameter();
                string format = "dd MMM yyyy";

                listSuplier = db.Query<listsupplier>(DatabaseManager.LoadQuery("GetListSuplier"), new object[] { productionMonth }).ToList<listsupplier>();
                SuppFeedbackFirmEmail email = new SuppFeedbackFirmEmail();
                foreach (listsupplier str in listSuplier)
                {
                    listtentative = db.Query<listFix>(DatabaseManager.LoadQuery("getFixDelay"), new object[] { str.suplierCode, productionMonth, str.supplierplant }).ToList<listFix>();
                    foreach (listFix dt in listtentative)
                    {
                        productionMonth = dt.productionMonth;
                        dateExpired = dt.tentativeDate.ToString(format);
                        SupplierCode = dt.suplierCode;
                        param.Clear();
                        param.Add("@suppCode", SupplierCode);
                        param.Add("@production_month", productionMonth);
                        param.Add("@date", dateExpired);
                        //ParamArray keyParam = new ParamArray();
                        keyParam.Clear();
                        keyParam.Add("functionid", functionID);
                        keyParam.Add("roleid", SupplierCode);
                        keyParam.Add("contentid", contentID);
                        keyParam.Add("attachid",attachID);
                        //DirectJobProvider.Execute(keyParam, param);
                        email.Send(keyParam, param, null);
                        System.Threading.Thread.Sleep(500);
                    }
                }
                db.Close();
                System.Threading.Thread.Sleep(500);
                SetProgress(100);
           }
            
        }
    }

}
