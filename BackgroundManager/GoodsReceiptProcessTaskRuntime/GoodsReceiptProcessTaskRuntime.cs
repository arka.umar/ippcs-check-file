﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using System.Reflection;
using Toyota.Common.Logging.Sink;

namespace GoodsReceiptProcessTaskRuntime
{
    class GoodsReceiptProcessTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
            ServiceResult result = new ServiceResult();

            /* Get parameter with key */
            String processId = Id;
            String username = parameters.Get("Username");
            
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Process ID : " + processId,
                Severity = LoggingSeverity.Warning
            });

            try
            {
                SetProgress(5);

                ServiceParameters sp = new ServiceParameters();

                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Start in process state ICS web service.",
                    Severity = LoggingSeverity.Info
                });

                String sendFlag = "N";
                const Int32 MAX_DATA_TRANSFERRED = 10;

                sp.Clear();
                sp.Add("state", "process");
                sp.Add("sessionId", processId);
                sp.Add("sendFlag", sendFlag);
                sp.Add("userId", username);
                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                if (result.Status != ServiceResult.STATUS_ERROR)
                {
                }
                else
                {
                }

                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Finish in process state ICS web service.",
                    Severity = LoggingSeverity.Info
                });
            }
            catch (Exception exc)
            {
                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Error in do process id " + processId + ". Message : " + exc.Message,
                    Severity = LoggingSeverity.Error
                });

                throw exc;
            }
            finally
            {
                gateway.Close();
                SetProgress(100);
            }
        }
    }
}
