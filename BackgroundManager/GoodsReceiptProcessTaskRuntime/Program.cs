﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using System.Reflection;

namespace GoodsReceiptProcessTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            GoodsReceiptProcessTaskRuntime grRuntime = new GoodsReceiptProcessTaskRuntime();

            #region Server Deploy
            grRuntime.ExecuteExternal(args);
            #endregion

            #region Local Testing
            //BackgroundTaskParameter parameters = new BackgroundTaskParameter();
            //parameters.Add("DataList", new List<IDictionary<string, string>>(), typeof(List<IDictionary<string, string>>));
            //parameters.Add("Process", "");
            //parameters.Add("Username", "");
            //grRuntime.ExecuteExternal(new String[] { parameters.ToString() });
            #endregion
        }
    }
}
