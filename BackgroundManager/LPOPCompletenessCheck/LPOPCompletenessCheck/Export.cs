﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace LPOPCompletenessCheck
{
    class Export
    {
        public void generateExcel()
        {
            Logging log = new Logging();
            DataAccess dao = new DataAccess();
            string sheetName = "CompletenessCheck";
            try
            {
                using (ExcelPackage p = new ExcelPackage())
                {
                    p.Workbook.Worksheets.Add(sheetName);
                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                    ws.Name = sheetName;
                    ws.Cells.Style.Font.Size = 11;
                    ws.Cells.Style.Font.Name = "Calibri";

                    int rows = 3;
                    int datacounter = 0;
                    List<string> SQLColumn = new List<string>();
                    //string[] ExcelColumn = null;
                    int column = 1;

                    #region getdata
                    using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                    {
                        conn.Open();

                        using (SqlCommand command = new SqlCommand("EXEC [dbo].[LPOP_GetCompletenessCheckData]", conn))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                SQLColumn = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                                foreach (var c in SQLColumn)
                                {
                                    ws.Cells[2, column].Value = c;
                                    column++;
                                }

                                while (reader.Read())
                                {
                                    column = 1;
                                    foreach (var h in SQLColumn)
                                    {
                                        ws.Cells[rows, column].Value = reader.GetString(reader.GetOrdinal(h)).ToString();
                                        column++;
                                    }

                                    rows++;
                                    datacounter++;
                                }
                            }
                        }
                        conn.Close();
                    }
                    #endregion

                    log.CreateLog("Retrieved " + datacounter + " Rows Data");

                    if (datacounter > 0)
                    {
                        for (int i = 1; i <= SQLColumn.Count; i++)
                        {
                            ws.Cells[2, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[2, i].AutoFitColumns(15, 300);
                        }

                        ws.Cells[1, 1].Value = "LPOP Completeness Check Data";
                        ws.Cells[1, 1, 1, SQLColumn.Count].Merge = true;
                        ws.Cells[1, 1, 1, SQLColumn.Count].Style.Font.Bold = true;
                        ws.Cells[1, 1, 1, SQLColumn.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        ws.Cells[1, 1, rows - 1, SQLColumn.Count].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        ws.Cells[1, 1, rows - 1, SQLColumn.Count].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        ws.Cells[1, 1, rows - 1, SQLColumn.Count].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        ws.Cells[1, 1, rows - 1, SQLColumn.Count].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        string unique = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
                        Byte[] file = p.GetAsByteArray();
                        string name = "LPOPCompletenessCheck_" + unique + ".xlsx";
                        //string fileDirectory = AppDomain.CurrentDomain.BaseDirectory + "Content\\" + name;
                        //System.IO.File.WriteAllBytes(fileDirectory, file);

                        if (file.Length > 0)
                        {
                            log.CreateLog("File " + name + " successfully Created and attemp to send by Email");
                            AttachToEmail(file, name);
                            log.CreateLog("Email Send");
                        }
                    }
                    else
                        log.CreateLog("No Data Exists, Email not Send");
                }
            }
            catch (Exception e)
            {
                log.CreateLog("Process Terminated : " + e.Message);
            }
        }

        private void AttachToEmail(Byte[] attachment, string filename)
        {
            DataAccess dao = new DataAccess();
            MailData data = new MailData();
            MailHelper mail = new MailHelper();
            data.MailRecipient = new List<string>();
            data.MailCarbonCopy = new List<string>();
            data.MailBlindCarbonCopy = new List<string>();
            data.MailAttachmentPath = new List<string>();
            data.Attachment = new List<Byte[]>();


            string[] mail_to = null;
            string[] mail_cc = null;
            string[] mail_bcc = null;

            using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("EXEC [dbo].[LPOP_GetCompletenessCheckMailConfig]", conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            mail_to = reader.GetString(reader.GetOrdinal("TO")).ToString().Split(',');
                            mail_cc = reader.GetString(reader.GetOrdinal("CC")).ToString().Split(',');
                            mail_bcc = reader.GetString(reader.GetOrdinal("BCC")).ToString().Split(',');

                            if (mail_to.Length > 0)
                            {
                                foreach (var to in mail_to)
                                {
                                    if (to.Length > 0)
                                        data.MailRecipient.Add(to);
                                }
                            }

                            if (mail_cc.Length > 0)
                            {
                                foreach (var cc in mail_cc)
                                {
                                    if (cc.Length > 0)
                                        data.MailCarbonCopy.Add(cc);
                                }
                            }

                            if (mail_bcc.Length > 0)
                            {
                                foreach (var bcc in mail_bcc)
                                {
                                    if (bcc.Length > 0)
                                        data.MailBlindCarbonCopy.Add(bcc);
                                }
                            }

                            //data.usingCarbonCopy = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("USE_CC")).ToString());
                            //data.usingBlindCarbonCopy = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("USE_BCC")).ToString());
                            //data.withAttachment = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("WITH_ATTCH")).ToString()); ;
                            data.MailSubject = reader.GetString(reader.GetOrdinal("SUBJECT")).ToString();
                            data.MailBody = reader.GetString(reader.GetOrdinal("BODY")).ToString();
                        }
                    }
                }

                //mail_to = ConfigurationManager.AppSettings["MAIL_TO"].ToString().Split(',');
                //mail_cc = ConfigurationManager.AppSettings["MAIL_CC"].ToString().Split(',');
                //mail_bcc = ConfigurationManager.AppSettings["MAIL_BCC"].ToString().Split(',');

                //if (mail_to.Length > 0)
                //{
                //    foreach (var to in mail_to)
                //    {
                //        if (to.Length > 0)
                //            data.MailRecipient.Add(to);
                //    }
                //}

                //if (mail_cc.Length > 0)
                //{
                //    foreach (var cc in mail_cc)
                //    {
                //        if (cc.Length > 0)
                //            data.MailCarbonCopy.Add(cc);
                //    }
                //}

                //if (mail_bcc.Length > 0)
                //{
                //    foreach (var bcc in mail_bcc)
                //    {
                //        if (bcc.Length > 0)
                //            data.MailBlindCarbonCopy.Add(bcc);
                //    }
                //}

                data.usingCarbonCopy = Convert.ToBoolean(ConfigurationManager.AppSettings["USE_CC"].ToString());
                data.usingBlindCarbonCopy = Convert.ToBoolean(ConfigurationManager.AppSettings["USE_BCC"].ToString());
                data.withAttachment = Convert.ToBoolean(ConfigurationManager.AppSettings["WITH_ATTCH"].ToString()); ;
                //data.MailSubject = ConfigurationManager.AppSettings["MAIL_SUBJECT"].ToString();
                //data.MailBody = ConfigurationManager.AppSettings["MAIL_BODY"].ToString();

                //DateTime d = DateTime.Now;
                //string prodmonth = d.ToString("yyyyMM");

                //data.MailSubject = data.MailSubject.Replace("[PROD_MONTH]", prodmonth);

                data.Attachment.Add(attachment);
                data.AttachmentName = filename;

                conn.Close();
            }

            mail.sendEmail(data);
        }
    }
}
