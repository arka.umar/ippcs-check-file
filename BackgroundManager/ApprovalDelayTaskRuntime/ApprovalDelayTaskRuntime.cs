﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using ApprovalDelayTaskRuntime.Model;
using System.Reflection;
using Toyota.Common.Logging.Sink;
using System.IO;
using System.Configuration;
using Toyota.Common.Web.Notifcation.JobMonitoring;
using Toyota.Common.Web.Ioc;


namespace ApprovalDelayTaskRuntime
{
    public class ApprovalDelayTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(ApprovalDelayTaskRuntime)), "ApprovalDelayTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            IBootstrap bootstrap = Bootstrap.GetInstance();
            bootstrap.RegisterProvider<Toyota.Common.Web.Database.IDBContextManager>(typeof(Toyota.Common.Web.Database.Petapoco.PetaPocoContextManager), true);

            List<DailyManifest> lstman = new List<DailyManifest>();
            List<RoleId> listRoleId = new List<RoleId>();
            List<string> lstdock = new List<string>();
            List<string> lstCount = new List<string>();

            string attachID;
            string fileLoc = string.Empty;
            string functionID = ConfigurationManager.AppSettings["functionID"];
            string contentID = ConfigurationManager.AppSettings["contentID"];
            string RecipientCode = ConfigurationManager.AppSettings["RecipientCode"];
            FileStream fs1;
            AttachmentProvider attach = new AttachmentProvider();
            ArgumentParameter param = new ArgumentParameter(); 

            lstdock = db.Query<string>(DatabaseManager.LoadQuery("GetDailyDockCode"), new object[] { }).ToList<string>();
            lstCount = db.Query<string>(DatabaseManager.LoadQuery("GetCountApproval"), new object[] { }).ToList<string>();

            bool isExists = System.IO.Directory.Exists(ConfigurationManager.AppSettings["fileLocation"]);
            if (!isExists)
                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["fileLocation"]);

            try
            {
                SetProgress(5);
                List<string> manifestNos;
                foreach (string str in lstdock)
                {
                    fileLoc = ConfigurationManager.AppSettings["fileLocation"] + str + ".txt";
                    fs1 = new FileStream(fileLoc, FileMode.OpenOrCreate, FileAccess.Write);
                    lstman = db.Query<DailyManifest>(DatabaseManager.LoadQuery("GetDailyApproval"), new string[] { str }).ToList<DailyManifest>();

                    manifestNos = new List<string>();
                    using (StreamWriter sw = new StreamWriter(fs1))
                    {
                        sw.Write("SuplierCode \t");
                        sw.Write("DockCode \t");
                        sw.WriteLine("ManifestNo");
                        sw.WriteLine("==========================================");
                        foreach (DailyManifest dm in lstman)
                        {
                            manifestNos.Add(dm.ManifestNo);
                            sw.Write(dm.SupplierCode + "\t" + "\t");
                            sw.Write(dm.DockCode + "\t" + "\t");
                            sw.WriteLine(dm.ManifestNo);
                        }
                    }
                    fs1.Close();
                    listRoleId = db.Query<RoleId>(DatabaseManager.LoadQuery("GetRoleIdPerDock"), new object[] { str }).ToList<RoleId>();
                    foreach (RoleId role in listRoleId)
                    {
                        attach.Create(functionID, contentID);
                        attach.Write(fileLoc);
                        attachID = attach.Flush();
                        param.Clear();
                        param.Add("@Dock", str);
                        param.Add("@number_manifest", lstman.Count().ToString());
                        ParamArray keyParam = new ParamArray();
                        keyParam.Add("functionid", functionID);
                        keyParam.Add("roleid", contentID);
                        keyParam.Add("contentid", contentID);
                        keyParam.Add("attachmentid", attachID);
                        keyParam.Add("recipientcode", RecipientCode);
                        DirectJobProvider.Execute(keyParam, param);
                        System.Threading.Thread.Sleep(250);
                    }
                }
                db.Close();
                System.Threading.Thread.Sleep(1000);
                Directory.Delete(ConfigurationManager.AppSettings["fileLocation"].ToString(), true);
            }
            catch (Exception exc)
            {
                db.Close();
                throw exc;
            }
            finally
            {
                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Finally",
                    Severity = LoggingSeverity.Warning
                });
                SetProgress(100);
            }
        }
    }

}
