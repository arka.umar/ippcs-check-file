﻿
--SELECT DISTINCT(ROLE_ID) as Role FROM TB_R_VW_AUTHORIZATION_GROUP WHERE AUTHORIZATION_GROUP_NAME LIKE '%' + @@dockcode + '%'

--select top 1 (a.ROLE_ID) as 'Role' from TB_R_VW_AUTHORIZATION_GROUP a 
--inner join TB_R_VW_USER_ROLE b on a.ROLE_ID=b.ROLE_ID
--inner join TB_R_VW_EMPLOYEE c on b.USERNAME=c.USERNAME
--where AUTHORIZATION_GROUP_NAME like '%' + @@dockcode + '%' and c.EMAIL is not null 
DECLARE @@dockcode varchar(10)=@0
SELECT RECIPIENT_GROUP_ID as 'Role' 
FROM 
	dbo.TB_T_EMAIL_RECIPIENT 
WHERE 
	RECIPIENT_CD= @@dockcode AND 
	RECIPIENT_GROUP_ID is not null