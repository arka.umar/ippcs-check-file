﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApprovalDelayTaskRuntime.Model
{
    public class DailyManifest
    {
        public string ManifestNo { set; get; }
        public string DockCode { set; get; }
        public string SupplierCode { set; get; }
    }
}
