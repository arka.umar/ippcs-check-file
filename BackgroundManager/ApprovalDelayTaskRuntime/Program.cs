﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using  ApprovalDelayTaskRuntime.Model;
using System.Reflection;

namespace ApprovalDelayTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inject parameter using with server deploy block code
            //args = new String[] { "{&quot;tREG-ID&quot;:&quot;FZJNS2O1BLX&quot;,&quot;tID&quot;:&quot;FZJNS2O1BLX&quot;}" };

            ApprovalDelayTaskRuntime grRuntime = new ApprovalDelayTaskRuntime();
            
            grRuntime.ExecuteExternal(args);

            #region New passing parameter method
            //BackgroundTaskParameter parameters = new BackgroundTaskParameter();
            //grRuntime.ExecuteExternal(new String[] { parameters.ToString() });
            #endregion
             
        }
    }
}
