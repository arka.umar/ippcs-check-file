﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace SPOTNotifier
{
    class MailHelper
    {
        public void sendEmail(MailData emailData)
        {
            MailMessage mail = new MailMessage();
            
            try
            {
                MailConfig config = getMailConfig();
                string TempFolder = ConfigurationManager.AppSettings["TempPath"];

                mail.From = new MailAddress(config.MailSender, config.MailSenderAliases);

                if (emailData.MailRecipient.Count > 0)
                {
                    foreach (var recipient in emailData.MailRecipient)
                    {
                        mail.To.Add(recipient);
                    }
                }

                if ((emailData.usingCarbonCopy) && (emailData.MailCarbonCopy != null))
                {
                    foreach (var cc in emailData.MailCarbonCopy)
                    {
                        mail.CC.Add(cc);
                    }
                }

                if ((emailData.usingBlindCarbonCopy) && (emailData.MailBlindCarbonCopy != null))
                {
                    foreach (var bcc in emailData.MailBlindCarbonCopy)
                    {
                        mail.Bcc.Add(bcc);
                    }
                }

                mail.Subject = emailData.MailSubject;
                mail.Body = emailData.MailBody;

                Attachment attach;
                int attachmentCount = 0;
                if (emailData.withAttachment)
                {
                    if(emailData.MailAttachmentPath != null)
                    {
                        foreach (var path in emailData.MailAttachmentPath)
                        {
                            if (attachmentCount < config.maxAttachment)
                            {
                                attach = new Attachment(AppDomain.CurrentDomain.BaseDirectory + TempFolder + path, MediaTypeNames.Application.Octet);
                                mail.Attachments.Add(attach);
                            }
                            else
                                break;

                            attachmentCount++;
                        }
                    }

                    if (emailData.Attachment != null)
                    {
                        foreach (var attc in emailData.Attachment)
                        {
                            if (attachmentCount < config.maxAttachment)
                            {
                                attach = new Attachment(new MemoryStream(attc), emailData.AttachmentName, MediaTypeNames.Application.Octet);
                                mail.Attachments.Add(attach);
                            }
                            else
                                break;

                            attachmentCount++;
                        }
                    }
                }

                SmtpClient client = new SmtpClient(config.MailHost, config.MailPort);

                if (!config.useAnonymousCredential)
                {
                    client.UseDefaultCredentials = config.useDefaultCredential;
                    client.Credentials = new System.Net.NetworkCredential(config.MailSender, config.MailSenderPassword);
                }

                client.EnableSsl = config.enableSLL;
                client.Send(mail);
            }
            catch (Exception e)
            {
                throw new Exception("Mail Not Send because following Error : " + e.Message);
            }
        }

        public MailConfig getMailConfig()
        {
            MailConfig config = new MailConfig();

            try
            {
                bool useDatabaseConfig = Convert.ToBoolean(ConfigurationManager.AppSettings["useDatabaseConfig"]);

                if (!useDatabaseConfig)
                {
                    config.MailHost = ConfigurationManager.AppSettings["MailHost"];
                    config.MailPort = Convert.ToInt32(ConfigurationManager.AppSettings["MailPort"]);
                    config.MailSender = ConfigurationManager.AppSettings["MailSender"];
                    config.MailSenderAliases = ConfigurationManager.AppSettings["MailSenderAliases"];
                    config.MailSenderPassword = ConfigurationManager.AppSettings["MailSenderPassword"] != null ?
                                                ConfigurationManager.AppSettings["MailSenderPassword"] : "";
                    config.maxAttachment = Convert.ToInt32(ConfigurationManager.AppSettings["MaxAttachment"]);
                    config.enableSLL = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                    config.useAnonymousCredential = Convert.ToBoolean(ConfigurationManager.AppSettings["useAnonymousCredential"]);
                    config.useDefaultCredential = Convert.ToBoolean(ConfigurationManager.AppSettings["useDefaultCredential"]);
                }
                else
                {
                    DataAccess dao = new DataAccess();
                    using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                    {
                        conn.Open();
                        using (SqlCommand command = new SqlCommand("EXEC SP_SPOT_GetEmailConfigForNotification", conn))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    config.MailHost = reader.GetString(reader.GetOrdinal("MAIL_HOST")).ToString();
                                    config.MailPort = Convert.ToInt32(reader.GetString(reader.GetOrdinal("MAIL_PORT")).ToString());
                                    config.MailSender = reader.GetString(reader.GetOrdinal("MAIL_SENDER")).ToString();
                                    config.MailSenderAliases = reader.GetString(reader.GetOrdinal("MAIL_ALIAS")).ToString();
                                    config.MailSenderPassword = reader.GetString(reader.GetOrdinal("MAIL_SENDER_PASS")).ToString().Trim() != "" ?
                                                                reader.GetString(reader.GetOrdinal("MAIL_SENDER_PASS")).ToString() : "";
                                    config.maxAttachment = Convert.ToInt32(reader.GetString(reader.GetOrdinal("MAIL_MAX_ATTCH")).ToString());
                                    config.enableSLL = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("MAIL_SSL")).ToString());
                                    config.useAnonymousCredential = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("MAIL_ANONYMOUS")).ToString());
                                    config.useDefaultCredential = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("MAIL_DEFAULT_CRED")).ToString());
                                }
                            }
                        }
                        conn.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw (e);
            }

            return config;
        }
    }
}
