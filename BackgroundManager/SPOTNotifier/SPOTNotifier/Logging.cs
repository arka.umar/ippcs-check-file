﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SPOTNotifier
{
    class Logging
    {
        public void CreateLog(string msg)
        {
            //StreamWriter streamWriter = File.Exists("C://SPOTNotifier/spotnotifier_log.txt") ?
            //                            File.AppendText("C://SPOTNotifier/spotnotifier_log.txt") :
            //                            new StreamWriter("C://SPOTNotifier/spotnotifier_log.txt");

            StreamWriter streamWriter = File.Exists("spotnotifier_log.txt") ?
                                        File.AppendText("spotnotifier_log.txt") :
                                        new StreamWriter("spotnotifier_log.txt");

            if (msg == "break")
                streamWriter.WriteLine();
            else
                streamWriter.WriteLine(DateTime.Now.ToString() + " --> " + msg);
            streamWriter.Close();
        }
    }
}
