﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Configuration;

namespace LPOPCompletenessCheck
{
    class Logging
    {
        public string path = null;
        public string dateFormat = null; 
        public Logging()
        {
            if (string.IsNullOrEmpty(path))
                path = Path.ChangeExtension(System.Reflection.Assembly.GetExecutingAssembly().Location, ".txt");

            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = ConfigurationManager.AppSettings["DateFormat"];
                if (string.IsNullOrEmpty(dateFormat)) dateFormat = "yyyy.MM.dd HH:mm:ss";
            }
        }
        public void CreateLog(string msg)
        {
            File.AppendAllText(path,
              (("break".Equals(msg))
                 ? "\r\n"
                 : (DateTime.Now.ToString(dateFormat) + " --> " + msg)
                )
                + "\r\n"
            ) ;            
        }
    }
}
