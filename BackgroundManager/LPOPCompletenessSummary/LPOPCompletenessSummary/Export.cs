﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace LPOPCompletenessCheck
{
    class Export
    {
        public void CompletenessCheck()
        {
            Logging log = new Logging();
            DataAccess dao = new DataAccess();
            string execute = "NG";

            using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = '11009' AND SYSTEM_CD = 'LPOP_COMP_EXCEL'", conn))
                {
                    execute = command.ExecuteScalar().ToString(); //reader.GetString(reader.GetOrdinal("EXEC_STAT")).ToString();
                }
                conn.Close();
            }

            if (execute == "OK")
            {
                log.CreateLog("Application Running. Generate Excel File");
                generateReport();
            }
            else {
                log.CreateLog("Application Running. No New Data or Not inside Execution Loop");
            }
        }

        public void UpdateSystemFlag()
        {
            Logging log = new Logging();
            DataAccess dao = new DataAccess();

            using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("UPDATE TB_M_SYSTEM SET SYSTEM_VALUE = 'NG' WHERE FUNCTION_ID = '11009' AND SYSTEM_CD = 'LPOP_COMP_EXCEL'", conn))
                {
                    command.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        public void generateReport()
        {
            Logging log = new Logging();
            DataAccess dao = new DataAccess();

            try
            {
                int rows = 11;
                int datacounter = 1;
                int showcount = 0;
                List<string> SQLColumn = new List<string>();
                string[] icsColumns = null;
                string icsColumn = "";
                int column = 1;
                int mergeWeekColumnStart = 0;
                int mergeWeekColumnEnd = 0;
                int mergeHeadColumnStart = 0;
                int mergeHeadColumnEnd = 0;
                int realColumn = 0;
                string sentenceCols = "";
                int TotalPrice_NG = 0;
                string PriceNG_Message = "MICS0232BERR: Material Price doesn't exist on Transaction Price Master";
                int TotalSloc_NG = 0;
                string SlocNG_Message = "MICS0087BERR: Combination of Plant, Sloc and Material doesn't exist on Material Master";
                int TotalMaterial_NG = 0;
                string MaterialNG_Message = "MICS0219BERR: Material doesn't exist on material master";
                int TotalSTDPrice_NG = 0;
                string STDPriceNG_Message = "MICS0232BERR: Released Standar Price doesn't exist on Transaction Standar Price Master";
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                string Price_PackMonth = "";
                string Sloc_PackMonth = "";
                string Material_PackMonth = "";
                string STD_Price_PackMonth = "";
                bool PriceData_Exists = false;
                bool SlocData_Exists = false;
                bool MaterialData_Exists = false;
                bool STDPricedata_Exists = false;
                StringBuilder BodyEmail = new StringBuilder();
                int GrandTotalError = 0;
                List<string> colnote = new List<string>();

                using (ExcelPackage p = new ExcelPackage())
                {
                    log.CreateLog("break");

                    #region Sheet A : Price
                    p.Workbook.Worksheets.Add("Price");
                    ExcelWorksheet SheetA = p.Workbook.Worksheets[1];
                    SheetA.Name = "Price";
                    SheetA.Cells.Style.Font.Size = 11;
                    SheetA.Cells.Style.Font.Name = "Arial";

                    #region Get Completeness Price Data
                    using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                    {
                        conn.Open();

                        using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessPriceSheet]", conn))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                SQLColumn = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                                SheetA.Cells[8, column].Value = "No";
                                SheetA.Cells[8, column, 10, column].Merge = true;
                                SheetA.Cells[8, column, 10, column].Style.WrapText = true;
                                SheetA.Cells[8, column, 10, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetA.Cells[8, column, 10, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;

                                foreach (var c in SQLColumn)
                                {
                                    sentenceCols = c.ToLower();
                                    if (sentenceCols.Split('_')[0] == "week")
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");

                                        mergeWeekColumnStart = column;
                                        SheetA.Cells[9, column].Value = "Packing Type"; column++;
                                        SheetA.Cells[9, column - 1, 10, column - 1].Merge = true;
                                        SheetA.Cells[9, column - 1, 10, column - 1].Style.WrapText = true;
                                        SheetA.Cells[9, column - 1, 10, column - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetA.Cells[9, column - 1, 10, column - 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        mergeHeadColumnStart = column;
                                        SheetA.Cells[10, column].Value = "No"; column++;
                                        SheetA.Cells[10, column].Value = "Release Status"; column++;
                                        SheetA.Cells[10, column].Value = "Valid From"; column++;
                                        mergeHeadColumnEnd = column - 1;
                                        SheetA.Cells[9, mergeHeadColumnStart].Value = "PC Release";
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Merge = true;
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Style.WrapText = true;
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        mergeHeadColumnStart = column;
                                        SheetA.Cells[10, column].Value = "Supplier Code"; column++;
                                        SheetA.Cells[10, column].Value = "Valid From"; column++;
                                        mergeHeadColumnEnd = column - 1;
                                        SheetA.Cells[9, mergeHeadColumnStart].Value = "Source List";
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Merge = true;
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Style.WrapText = true;
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetA.Cells[9, mergeHeadColumnStart, 9, mergeHeadColumnEnd].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        mergeWeekColumnEnd = column - 1;
                                        SheetA.Cells[8, mergeWeekColumnStart].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetA.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Merge = true;
                                        SheetA.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.WrapText = true;
                                        SheetA.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetA.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");
                                        SheetA.Cells[8, column].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetA.Cells[8, column, 10, column].Merge = true;
                                        SheetA.Cells[8, column, 10, column].Style.WrapText = true;
                                        SheetA.Cells[8, column, 10, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetA.Cells[8, column, 10, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        column++;
                                    }
                                }

                                SheetA.Cells[8, column].Value = "Overall Status";
                                SheetA.Cells[8, column, 10, column].Merge = true;
                                SheetA.Cells[8, column, 10, column].Style.WrapText = true;
                                SheetA.Cells[8, column, 10, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetA.Cells[8, column, 10, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;
                                colnote.Clear();
                                realColumn = column - 1;

                                while (reader.Read())
                                {
                                    column = 1;

                                    SheetA.Cells[rows, column].Value = datacounter;
                                    column++;
                                    foreach (var h in SQLColumn)
                                    {
                                        if (h.Split('_')[0] == "WEEK")
                                        {
                                            int oh = reader.GetOrdinal(h);
                                            icsColumn = (oh >= 0 && !reader.IsDBNull(oh)) ? reader.GetString(oh) : "";
                                            if (string.IsNullOrEmpty(icsColumn))
                                                if (!colnote.Contains(h))
                                                    colnote.Add(h);
                                            icsColumns = icsColumn.Split('|');

                                            for (int l = 0; l < icsColumns.Length; l++)
                                            {
                                                SheetA.Cells[rows, column].Value = icsColumns[l];
                                                column++;
                                            }
                                        }
                                        else
                                        {
                                            SheetA.Cells[rows, column].Value = reader.GetString(reader.GetOrdinal(h));
                                            column++;
                                        }
                                    }

                                    PriceData_Exists = true;
                                    rows++;
                                    datacounter++;
                                }
                                if (colnote.Count > 0)
                                    log.CreateLog("Price Column has null values: " + string.Join(",", colnote));
                            }
                        }
                        conn.Close();
                    }
                    #endregion
                    
                    #region Adjusting Header for Price Sheet
                    if (PriceData_Exists)
                    {
                        SheetA.Cells[10, 1, rows - 1, realColumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        SheetA.Cells[10, 1, rows - 1, realColumn].AutoFitColumns(10, 500);
                        SheetA.Cells[8, 1, 10, realColumn].Style.Font.Bold = true;

                        SheetA.Cells[8, 1, 8, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetA.Cells[8, 1, 8, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LawnGreen);

                        SheetA.Cells[9, 1, 9, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetA.Cells[9, 1, 9, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                        SheetA.Cells[10, 1, 10, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetA.Cells[10, 1, 10, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LawnGreen);

                        SheetA.Cells[8, 1, rows - 1, realColumn].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetA.Cells[8, 1, rows - 1, realColumn].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetA.Cells[8, 1, rows - 1, realColumn].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetA.Cells[8, 1, rows - 1, realColumn].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetA.Cells[5, 1].Value = "Production Month";
                        SheetA.Cells[5, 1, 5, 2].Merge = true;
                        SheetA.Cells[5, 1, 5, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetA.Cells[5, 1, 5, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        SheetA.Cells[6, 1].Value = "Created Date";
                        SheetA.Cells[6, 1, 6, 2].Merge = true;
                        SheetA.Cells[6, 1, 6, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetA.Cells[6, 1, 6, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        //SheetA.Cells[5, 3, 5, 4].Merge = true;
                        //SheetA.Cells[6, 3, 6, 4].Merge = true;

                        using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                        {
                            conn.Open();
                            using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessPriceHeader]", conn))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        Price_PackMonth = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        SheetA.Cells[5, 3].Value = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        //SheetA.Cells[6, 3].Value = reader.GetString(reader.GetOrdinal("VALIDATION_STS")).ToString();
                                        //SheetA.Cells[7, 3].Value = PriceNG_Message;
                                        SheetA.Cells[6, 3].Value = DateTime.Now.ToString("dd.MM.yyyy");
                                        TotalPrice_NG = Convert.ToInt32(reader.GetInt32(reader.GetOrdinal("COUNTER_NG")));
                                    }
                                }
                            }
                            conn.Close();
                        }

                        SheetA.Cells[5, 1, 6, 3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetA.Cells[5, 1, 6, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetA.Cells[5, 1, 6, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetA.Cells[5, 1, 6, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetA.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA";
                        SheetA.Cells[1, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        SheetA.Cells[1, 1].Style.Font.Size = 12;
                        SheetA.Cells[1, 1].Style.Font.Bold = true;

                        SheetA.Cells[2, 1].Value = "LPOP INTERFACE RESULT PRICE";
                        SheetA.Cells[2, 1].Style.Font.Bold = true;
                        SheetA.Cells[2, 1].Style.Font.UnderLine = true;

                        showcount = datacounter - 1;
                        log.CreateLog("Retrieved " + showcount + " Rows Price Checking Data (" + Price_PackMonth +")");
                    }
                    #endregion
                    #endregion

                    #region Sheet B : Sloc
                    rows = 10;
                    column = 1;
                    realColumn = 0;
                    datacounter = 1;

                    p.Workbook.Worksheets.Add("Sloc");
                    ExcelWorksheet SheetB = p.Workbook.Worksheets[2];
                    SheetB.Name = "Sloc";
                    SheetB.Cells.Style.Font.Size = 11;
                    SheetB.Cells.Style.Font.Name = "Arial";

                    #region Get Completeness Price Data
                    using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                    {
                        conn.Open();

                        using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessSlocSheet]", conn))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                SQLColumn = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                                SheetB.Cells[8, column].Value = "No";
                                SheetB.Cells[8, column, 9, column].Merge = true;
                                SheetB.Cells[8, column, 9, column].Style.WrapText = true;
                                SheetB.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetB.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;

                                foreach (var c in SQLColumn)
                                {
                                    sentenceCols = c.ToLower();
                                    if (sentenceCols.Split('_')[0] == "week")
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");

                                        mergeWeekColumnStart = column;
                                        SheetB.Cells[9, column].Value = "Sloc Data Material"; column++;
                                        //SheetB.Cells[10, column - 1, 11, column - 1].Merge = true;
                                        SheetB.Cells[9, column - 1, 9, column - 1].Style.WrapText = true;
                                        SheetB.Cells[9, column - 1, 9, column - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetB.Cells[9, column - 1, 9, column - 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        mergeWeekColumnEnd = column - 1;
                                        SheetB.Cells[8, mergeWeekColumnStart].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetB.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Merge = true;
                                        SheetB.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.WrapText = true;
                                        SheetB.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetB.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");
                                        SheetB.Cells[8, column].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetB.Cells[8, column, 9, column].Merge = true;
                                        SheetB.Cells[8, column, 9, column].Style.WrapText = true;
                                        SheetB.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetB.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        column++;
                                    }
                                }

                                SheetB.Cells[8, column].Value = "Overall Status";
                                SheetB.Cells[8, column, 9, column].Merge = true;
                                SheetB.Cells[8, column, 9, column].Style.WrapText = true;
                                SheetB.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetB.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;
                                colnote.Clear();
                                realColumn = column - 1;
                                    
                                while (reader.Read())
                                {
                                    column = 1;

                                    SheetB.Cells[rows, column].Value = datacounter;
                                    column++;
                                    foreach (var h in SQLColumn)
                                    {
                                        if (h.Split('_')[0] == "WEEK")
                                        {
                                            int oc = reader.GetOrdinal(h);
                                            icsColumn = (oc >= 0 && !reader.IsDBNull(oc)) ? reader.GetString(oc) : "";
                                            if (string.IsNullOrEmpty(icsColumn))
                                                if (!colnote.Contains(h))
                                                    colnote.Add(h);
                                                

                                            icsColumns = icsColumn.Split('|');

                                            for (int l = 0; l < icsColumns.Length; l++)
                                            {
                                                SheetB.Cells[rows, column].Value = icsColumns[l];
                                                column++;
                                            }
                                        }
                                        else
                                        {
                                            SheetB.Cells[rows, column].Value = reader.GetString(reader.GetOrdinal(h));
                                            column++;
                                        }
                                    }
                                    

                                    SlocData_Exists = true;
                                    rows++;
                                    datacounter++;
                                }
                                if (colnote.Count > 0)
                                    log.CreateLog("SLOC Column has null values : " + string.Join(",", colnote));
                            }
                        }
                        conn.Close();
                    }
                    #endregion

                    #region Adjusting Header for Price Sheet
                    if (SlocData_Exists)
                    {
                        SheetB.Cells[9, 1, rows - 1, realColumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        SheetB.Cells[9, 1, rows - 1, realColumn].AutoFitColumns(10, 500);
                        SheetB.Cells[8, 1, 9, realColumn].Style.Font.Bold = true;

                        SheetB.Cells[8, 1, 8, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetB.Cells[8, 1, 8, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LawnGreen);

                        SheetB.Cells[9, 1, 9, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetB.Cells[9, 1, 9, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                        SheetB.Cells[8, 1, rows - 1, realColumn].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetB.Cells[8, 1, rows - 1, realColumn].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetB.Cells[8, 1, rows - 1, realColumn].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetB.Cells[8, 1, rows - 1, realColumn].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetB.Cells[5, 1].Value = "Production Month";
                        SheetB.Cells[5, 1, 5, 2].Merge = true;
                        SheetB.Cells[5, 1, 5, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetB.Cells[5, 1, 5, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        SheetB.Cells[6, 1].Value = "Created Date";
                        SheetB.Cells[6, 1, 6, 2].Merge = true;
                        SheetB.Cells[6, 1, 6, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetB.Cells[6, 1, 6, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        //SheetB.Cells[5, 3, 5, 4].Merge = true;
                        //SheetB.Cells[6, 3, 6, 4].Merge = true;
                        //SheetB.Cells[7, 3, 7, 4].Merge = true;
                        //SheetB.Cells[8, 3, 8, 4].Merge = true;

                        using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                        {
                            conn.Open();
                            using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessSlocHeader]", conn))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        Sloc_PackMonth = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        SheetB.Cells[5, 3].Value = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        //SheetB.Cells[6, 3].Value = reader.GetString(reader.GetOrdinal("VALIDATION_STS")).ToString();
                                        //SheetB.Cells[7, 3].Value = SlocNG_Message;
                                        SheetB.Cells[6, 3].Value = DateTime.Now.ToString("dd.MM.yyyy");
                                        TotalSloc_NG = Convert.ToInt32(reader.GetInt32(reader.GetOrdinal("COUNTER_NG")));
                                    }
                                }
                            }
                            conn.Close();
                        }

                        SheetB.Cells[5, 1, 6, 3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetB.Cells[5, 1, 6, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetB.Cells[5, 1, 6, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetB.Cells[5, 1, 6, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetB.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA";
                        SheetB.Cells[1, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        SheetB.Cells[1, 1].Style.Font.Size = 12;
                        SheetB.Cells[1, 1].Style.Font.Bold = true;

                        SheetB.Cells[2, 1].Value = "LPOP INTERFACE RESULT SLOC";
                        SheetB.Cells[2, 1].Style.Font.Bold = true;
                        SheetB.Cells[2, 1].Style.Font.UnderLine = true;

                        showcount = datacounter - 1;
                        log.CreateLog("Retrieved " + showcount + " Rows Sloc Data (" + Sloc_PackMonth + ")");
                    }
                    #endregion
                    #endregion

                    #region Sheet C : Material
                    rows = 10;
                    column = 1;
                    realColumn = 0;
                    datacounter = 1;

                    p.Workbook.Worksheets.Add("Material");
                    ExcelWorksheet SheetC = p.Workbook.Worksheets[3];
                    SheetC.Name = "Material";
                    SheetC.Cells.Style.Font.Size = 11;
                    SheetC.Cells.Style.Font.Name = "Arial";

                    #region Get Completeness Price Data
                    using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                    {
                        conn.Open();

                        using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessMaterialSheet]", conn))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                SQLColumn = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                                SheetC.Cells[8, column].Value = "No";
                                SheetC.Cells[8, column, 9, column].Merge = true;
                                SheetC.Cells[8, column, 9, column].Style.WrapText = true;
                                SheetC.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetC.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;

                                foreach (var c in SQLColumn)
                                {
                                    sentenceCols = c.ToLower();
                                    if (sentenceCols.Split('_')[0] == "week")
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");

                                        mergeWeekColumnStart = column;
                                        SheetC.Cells[9, column].Value = "Material Master"; column++;
                                        //SheetB.Cells[10, column - 1, 11, column - 1].Merge = true;
                                        SheetC.Cells[9, column - 1, 9, column - 1].Style.WrapText = true;
                                        SheetC.Cells[9, column - 1, 9, column - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetC.Cells[9, column - 1, 9, column - 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        SheetC.Cells[9, column].Value = "Material Valuation"; column++;
                                        //SheetB.Cells[10, column - 1, 11, column - 1].Merge = true;
                                        SheetC.Cells[9, column - 1, 9, column - 1].Style.WrapText = true;
                                        SheetC.Cells[9, column - 1, 9, column - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetC.Cells[9, column - 1, 9, column - 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        mergeWeekColumnEnd = column - 1;

                                        SheetC.Cells[8, mergeWeekColumnStart].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetC.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Merge = true;
                                        SheetC.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.WrapText = true;
                                        SheetC.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetC.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");
                                        SheetC.Cells[8, column].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetC.Cells[8, column, 9, column].Merge = true;
                                        SheetC.Cells[8, column, 9, column].Style.WrapText = true;
                                        SheetC.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetC.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        column++;
                                    }
                                }

                                SheetC.Cells[8, column].Value = "Overall Status";
                                SheetC.Cells[8, column, 9, column].Merge = true;
                                SheetC.Cells[8, column, 9, column].Style.WrapText = true;
                                SheetC.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetC.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;

                                realColumn = column - 1;
                                colnote.Clear();
                                while (reader.Read())
                                {
                                    column = 1;
                                    
                                    SheetC.Cells[rows, column].Value = datacounter;
                                    column++;
                                    foreach (var h in SQLColumn)
                                    {
                                        if (h.Split('_')[0] == "WEEK")
                                        {
                                            int oW = reader.GetOrdinal(h);
                                            icsColumn = (oW >= 0 && !reader.IsDBNull(oW)) ? reader.GetString(oW) : "";
                                            if (string.IsNullOrEmpty(icsColumn))
                                                if (!colnote.Contains(h))
                                                    colnote.Add(h);

                                            icsColumns = icsColumn.Split('|');

                                            for (int l = 0; l < icsColumns.Length; l++)
                                            {
                                                SheetC.Cells[rows, column].Value = icsColumns[l];
                                                column++;
                                            }
                                        }
                                        else
                                        {
                                            SheetC.Cells[rows, column].Value = reader.GetString(reader.GetOrdinal(h));
                                            column++;
                                        }
                                    }

                                    MaterialData_Exists = true;
                                    rows++;
                                    datacounter++;
                                }
                                if (colnote.Count > 0)
                                    log.CreateLog("Material Column has null values: " + string.Join(",", colnote));
                            }
                        }
                        conn.Close();
                    }
                    #endregion

                    #region Adjusting Header for Price Sheet
                    if (MaterialData_Exists)
                    {
                        SheetC.Cells[9, 1, rows - 1, realColumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        SheetC.Cells[9, 1, rows - 1, realColumn].AutoFitColumns(10, 500);
                        SheetC.Cells[8, 1, 9, realColumn].Style.Font.Bold = true;

                        SheetC.Cells[8, 1, 8, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetC.Cells[8, 1, 8, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LawnGreen);

                        SheetC.Cells[9, 1, 9, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetC.Cells[9, 1, 9, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                        SheetC.Cells[8, 1, rows - 1, realColumn].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetC.Cells[8, 1, rows - 1, realColumn].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetC.Cells[8, 1, rows - 1, realColumn].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetC.Cells[8, 1, rows - 1, realColumn].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetC.Cells[5, 1].Value = "Production Month";
                        SheetC.Cells[5, 1, 5, 2].Merge = true;
                        SheetC.Cells[5, 1, 5, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetC.Cells[5, 1, 5, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        SheetC.Cells[6, 1].Value = "Created Date";
                        SheetC.Cells[6, 1, 6, 2].Merge = true;
                        SheetC.Cells[6, 1, 6, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetC.Cells[6, 1, 6, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        //SheetC.Cells[5, 3, 5, 4].Merge = true;
                        //SheetC.Cells[6, 3, 6, 4].Merge = true;
                        //SheetC.Cells[7, 3, 7, 4].Merge = true;
                        //SheetC.Cells[8, 3, 8, 4].Merge = true;

                        using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                        {
                            conn.Open();
                            using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessMaterialHeader]", conn))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        Material_PackMonth = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        SheetC.Cells[5, 3].Value = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        //SheetC.Cells[6, 3].Value = reader.GetString(reader.GetOrdinal("VALIDATION_STS")).ToString();
                                        //SheetC.Cells[7, 3].Value = MaterialNG_Message;
                                        SheetC.Cells[6, 3].Value = DateTime.Now.ToString("dd.MM.yyyy");
                                        TotalMaterial_NG = Convert.ToInt32(reader.GetInt32(reader.GetOrdinal("COUNTER_NG")));
                                    }
                                }
                            }
                            conn.Close();
                        }

                        SheetC.Cells[5, 1, 6, 3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetC.Cells[5, 1, 6, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetC.Cells[5, 1, 6, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetC.Cells[5, 1, 6, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetC.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA";
                        SheetC.Cells[1, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        SheetC.Cells[1, 1].Style.Font.Size = 12;
                        SheetC.Cells[1, 1].Style.Font.Bold = true;

                        SheetC.Cells[2, 1].Value = "LPOP INTERFACE RESULT";
                        SheetC.Cells[2, 1].Style.Font.Bold = true;
                        SheetC.Cells[2, 1].Style.Font.UnderLine = true;

                        showcount = datacounter - 1;
                        log.CreateLog("Retrieved " + showcount + " Rows Material Checking Data (" + Material_PackMonth + ")");
                    }
                    #endregion
                    #endregion

                    #region Sheet D : STD Price
                    rows = 10;
                    column = 1;
                    realColumn = 0;
                    datacounter = 1;

                    p.Workbook.Worksheets.Add("STD Price");
                    ExcelWorksheet SheetD = p.Workbook.Worksheets[4];
                    SheetD.Name = "STD Price";
                    SheetD.Cells.Style.Font.Size = 11;
                    SheetD.Cells.Style.Font.Name = "Arial";

                    #region Get Completeness STD Price Data
                    using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                    {
                        conn.Open();

                        using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessSTDPriceSheet]", conn))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                SQLColumn = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                                SheetD.Cells[8, column].Value = "No";
                                SheetD.Cells[8, column, 9, column].Merge = true;
                                SheetD.Cells[8, column, 9, column].Style.WrapText = true;
                                SheetD.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetD.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;

                                foreach (var c in SQLColumn)
                                {
                                    sentenceCols = c.ToLower();
                                    if (sentenceCols.Split('_')[0] == "week")
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");

                                        mergeWeekColumnStart = column;
                                        SheetD.Cells[9, column].Value = "Std Price Amount"; column++;
                                        //SheetD.Cells[10, column - 1, 11, column - 1].Merge = true;
                                        SheetD.Cells[9, column - 1, 9, column - 1].Style.WrapText = true;
                                        SheetD.Cells[9, column - 1, 9, column - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetD.Cells[9, column - 1, 9, column - 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        mergeWeekColumnEnd = column - 1;
                                        SheetD.Cells[8, mergeWeekColumnStart].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetD.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Merge = true;
                                        SheetD.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.WrapText = true;
                                        SheetD.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetD.Cells[8, mergeWeekColumnStart, 8, mergeWeekColumnEnd].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else
                                    {
                                        sentenceCols = sentenceCols.Replace("_", " ");
                                        SheetD.Cells[8, column].Value = textInfo.ToTitleCase(sentenceCols);
                                        SheetD.Cells[8, column, 9, column].Merge = true;
                                        SheetD.Cells[8, column, 9, column].Style.WrapText = true;
                                        SheetD.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        SheetD.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        column++;
                                    }
                                }

                                SheetD.Cells[8, column].Value = "Overall Status";
                                SheetD.Cells[8, column, 9, column].Merge = true;
                                SheetD.Cells[8, column, 9, column].Style.WrapText = true;
                                SheetD.Cells[8, column, 9, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                SheetD.Cells[8, column, 9, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                column++;
                                colnote.Clear();
                                realColumn = column - 1;

                                while (reader.Read())
                                {
                                    column = 1;

                                    SheetD.Cells[rows, column].Value = datacounter;
                                    column++;
                                    foreach (var h in SQLColumn)
                                    {
                                        if (h.Split('_')[0] == "WEEK")
                                        {
                                            int oHh = reader.GetOrdinal(h);
                                            icsColumn = (oHh >= 0 && !reader.IsDBNull(oHh)) ? reader.GetString(oHh) : "";
                                            if (string.IsNullOrEmpty(icsColumn))
                                                if (!colnote.Contains(h))
                                                    colnote.Add(h);

                                            icsColumns = icsColumn.Split('|');

                                            for (int l = 0; l < icsColumns.Length; l++)
                                            {
                                                SheetD.Cells[rows, column].Value = icsColumns[l];
                                                column++;
                                            }
                                        }
                                        else
                                        {
                                            SheetD.Cells[rows, column].Value = reader.GetString(reader.GetOrdinal(h));
                                            column++;
                                        }
                                    }

                                    STDPricedata_Exists = true;
                                    rows++;
                                    datacounter++;
                                }
                                if (colnote.Count > 0)
                                    log.CreateLog("STD Price Column has null values: " + string.Join(",", colnote));
                            }
                        }
                        conn.Close();
                    }
                    #endregion

                    #region Adjusting Header for STD Price Sheet
                    if (STDPricedata_Exists)
                    {
                        SheetD.Cells[9, 1, rows - 1, realColumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        SheetD.Cells[9, 1, rows - 1, realColumn].AutoFitColumns(10, 500);
                        SheetD.Cells[8, 1, 9, realColumn].Style.Font.Bold = true;

                        SheetD.Cells[8, 1, 8, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetD.Cells[8, 1, 8, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LawnGreen);

                        SheetD.Cells[9, 1, 9, realColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetD.Cells[9, 1, 9, realColumn].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                        SheetD.Cells[8, 1, rows - 1, realColumn].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetD.Cells[8, 1, rows - 1, realColumn].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetD.Cells[8, 1, rows - 1, realColumn].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetD.Cells[8, 1, rows - 1, realColumn].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetD.Cells[5, 1].Value = "Production Month";
                        SheetD.Cells[5, 1, 5, 2].Merge = true;
                        SheetD.Cells[5, 1, 5, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetD.Cells[5, 1, 5, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        SheetD.Cells[6, 1].Value = "Created Date";
                        SheetD.Cells[6, 1, 6, 2].Merge = true;
                        SheetD.Cells[6, 1, 6, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        SheetD.Cells[6, 1, 6, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                        //SheetD.Cells[5, 3, 5, 4].Merge = true;
                        //SheetD.Cells[6, 3, 6, 4].Merge = true;
                        //SheetD.Cells[7, 3, 7, 4].Merge = true;
                        //SheetD.Cells[8, 3, 8, 4].Merge = true;

                        using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
                        {
                            conn.Open();
                            using (SqlCommand command = new SqlCommand("EXEC [dbo].[GetLPOPCompletenessSTDPriceHeader]", conn))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        STD_Price_PackMonth = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        SheetD.Cells[5, 3].Value = reader.GetString(reader.GetOrdinal("PACK_MONTH"));
                                        //SheetD.Cells[6, 3].Value = reader.GetString(reader.GetOrdinal("VALIDATION_STS")).ToString();
                                        //SheetD.Cells[7, 3].Value = STDPriceNG_Message;
                                        SheetD.Cells[6, 3].Value = DateTime.Now.ToString("dd.MM.yyyy");
                                        TotalSTDPrice_NG = Convert.ToInt32(reader.GetInt32(reader.GetOrdinal("COUNTER_NG")));
                                    }
                                }
                            }
                            conn.Close();
                        }

                        SheetD.Cells[5, 1, 6, 3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        SheetD.Cells[5, 1, 6, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        SheetD.Cells[5, 1, 6, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        SheetD.Cells[5, 1, 6, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        SheetD.Cells[1, 1].Value = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA";
                        SheetD.Cells[1, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        SheetD.Cells[1, 1].Style.Font.Size = 12;
                        SheetD.Cells[1, 1].Style.Font.Bold = true;

                        SheetD.Cells[2, 1].Value = "LPOP INTERFACE RESULT STD PRICE";
                        SheetD.Cells[2, 1].Style.Font.Bold = true;
                        SheetD.Cells[2, 1].Style.Font.UnderLine = true;

                        showcount = datacounter - 1;
                        log.CreateLog("Retrieved " + showcount + " Rows STD Price Data (" + STD_Price_PackMonth + ")");
                    }
                    #endregion
                    #endregion

                    if (PriceData_Exists && SlocData_Exists && MaterialData_Exists && STDPricedata_Exists)
                    {
                        if ((Price_PackMonth == Sloc_PackMonth)
                            && (Price_PackMonth == Material_PackMonth)
                            && (Sloc_PackMonth == Material_PackMonth)
                            && (Price_PackMonth == STD_Price_PackMonth))
                        {
                            //string unique = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
                            Byte[] file = p.GetAsByteArray();
                            string name = "LPOPCompletenessCheck_" + Price_PackMonth + ".xlsx";

                            string fileDirectory = AppDomain.CurrentDomain.BaseDirectory + "Content\\" + name;
                            if (!System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Content\\"))
                            {
                                System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Content\\");
                            }

                            if (System.IO.File.Exists(fileDirectory))
                            {
                                System.IO.File.Delete(fileDirectory);
                            }

                            System.IO.File.WriteAllBytes(fileDirectory, file);

                            string FtpHost = ConfigurationManager.AppSettings["FTP_HOST"].ToString();
                            string FtpUsername = ConfigurationManager.AppSettings["FTP_USERNAME"].ToString();
                            string FtpPassword = ConfigurationManager.AppSettings["FTP_PASSWORD"].ToString();
                            string FtpFilePath = ConfigurationManager.AppSettings["FTP_FILEPATH"].ToString();
                            //string FtpPath = ConfigurationManager.AppSettings["FTP_LINKPATH"].ToString();

                            log.CreateLog("Send to FTP is started with config host: " + FtpHost + ", username: " + FtpUsername + ", password: " + FtpPassword);
                            FTPHelper ftp = new FTPHelper(FtpHost, FtpUsername, FtpPassword);

                            string[] Ftp_Files = ftp.directoryListSimple(FtpFilePath);
                            for (int y = 0; y < Ftp_Files.Length; y++)
                            {
                                if (Ftp_Files[y] != "")
                                {
                                    if (Ftp_Files[y] == name)
                                    {
                                        ftp.delete(FtpFilePath + name);
                                        break;
                                    }
                                }
                            }

                            ftp.upload(FtpFilePath + name, file);
                            log.CreateLog("Send to FTP is finish successfully and send to " + FtpFilePath + name); 

                            GrandTotalError = TotalMaterial_NG + TotalSloc_NG + TotalPrice_NG + TotalSTDPrice_NG;
                            BodyEmail = BodyEmail.Append("<html><head></head><body>");
                            BodyEmail = BodyEmail.Append("<p>Dear All,</p><br/>");
                            BodyEmail = BodyEmail.Append("<p>Berikut ini kami sampaikan Error LPOP Interface Validation Result untuk Production : " + Price_PackMonth + ".</p>");
                            BodyEmail = BodyEmail.Append("<p>Untuk itu mohon bantuan dari masing-masing PIC untuk segera memaintain data tersebut agar GR processing tidak mengalami error.</p><br/>");
                            BodyEmail = BodyEmail.Append("<table border=\"1\" style=\"border-collapse:collapse\">");
                            BodyEmail = BodyEmail.Append("<tr>");
                            BodyEmail = BodyEmail.Append("<th style=\"background-color:lightblue\">Message Error</th>");
                            BodyEmail = BodyEmail.Append("<th style=\"background-color:lightblue\">Total Error</th>");
                            BodyEmail = BodyEmail.Append("<th style=\"background-color:lightblue\">PIC</th>");
                            BodyEmail = BodyEmail.Append("</tr>");
                            BodyEmail = BodyEmail.Append("<tr>");
                            BodyEmail = BodyEmail.Append("<td>" + PriceNG_Message + "</td>");
                            BodyEmail = BodyEmail.Append("<td>" + TotalPrice_NG.ToString() + "</td>");
                            BodyEmail = BodyEmail.Append("<td>PUD</td>");
                            BodyEmail = BodyEmail.Append("</tr>");
                            BodyEmail = BodyEmail.Append("<tr>");
                            BodyEmail = BodyEmail.Append("<td>" + SlocNG_Message + "</td>");
                            BodyEmail = BodyEmail.Append("<td>" + TotalSloc_NG.ToString() + "</td>");
                            BodyEmail = BodyEmail.Append("<td>PUD</td>");
                            BodyEmail = BodyEmail.Append("</tr>");
                            BodyEmail = BodyEmail.Append("<tr>");
                            BodyEmail = BodyEmail.Append("<td>" + MaterialNG_Message + "</td>");
                            BodyEmail = BodyEmail.Append("<td>" + TotalMaterial_NG.ToString() + "</td>");
                            BodyEmail = BodyEmail.Append("<td>PCD</td>");
                            BodyEmail = BodyEmail.Append("</tr>");
                            BodyEmail = BodyEmail.Append("<tr>");
                            BodyEmail = BodyEmail.Append("<td>" + STDPriceNG_Message + "</td>");
                            BodyEmail = BodyEmail.Append("<td>" + TotalSTDPrice_NG.ToString() + "</td>");
                            BodyEmail = BodyEmail.Append("<td>FD</td>");
                            BodyEmail = BodyEmail.Append("</tr>");
                            BodyEmail = BodyEmail.Append("<tr>");
                            BodyEmail = BodyEmail.Append("<td><strong>GRAND TOTAL</strong></td>");
                            BodyEmail = BodyEmail.Append("<td>" + GrandTotalError.ToString() + "</td>");
                            BodyEmail = BodyEmail.Append("<td></td>");
                            BodyEmail = BodyEmail.Append("</tr>");
                            BodyEmail = BodyEmail.Append("</table><br/>");
                            BodyEmail = BodyEmail.Append("<p>Untuk detail datanya bisa di lihat di file server dengan path sebagai berikut : </p>");
                            BodyEmail = BodyEmail.Append("<a href=\"File:///{FTP_PATH}\">{FTP_PATH}</a><br/>");
                            BodyEmail = BodyEmail.Append("<p>Terima kasih atas bantuan dan kerjasamanya.</p><br/>");
                            BodyEmail = BodyEmail.Append("<p>Regards,</p>");
                            BodyEmail = BodyEmail.Append("<p>ICS Administrator</p><br/><br/><br/>");
                            BodyEmail = BodyEmail.Append("<p>PO Interface Validation Result</p>");
                            BodyEmail = BodyEmail.Append("</body></html>");

                            log.CreateLog("Email sending is started");
                            AttachToEmail(BodyEmail.ToString(), Price_PackMonth);
                            log.CreateLog("Email sending is finish successfully");

                            UpdateSystemFlag();
                        }
                        else 
                        {
                            log.CreateLog("Different Pack Month on Data, Excel Not Created");
                        }
                    }
                    else
                    {
                        log.CreateLog("Data is Not Exists, Excel Not Created");
                    }
                }
            }
            catch (Exception e)
            {
                int eLevel = 0;
                while (e != null && eLevel < 7)
                {
                    log.CreateLog((
                        (eLevel++ < 1) 
                        ? "Process Terminated : " 
                        : "Detail Error :") 
                        + e.Message 
                        + "\r\n" + e.StackTrace);

                    e = e.InnerException;
                }
            }
        }

        private void AttachToEmail(string BodyTable, string ProdMonth)
        {
            string FileServerPath = "";

            DataAccess dao = new DataAccess();
            MailData data = new MailData();
            MailHelper mail = new MailHelper();
            data.MailRecipient = new List<string>();
            data.MailCarbonCopy = new List<string>();
            data.MailBlindCarbonCopy = new List<string>();
            data.MailAttachmentPath = new List<string>();
            data.Attachment = new List<Byte[]>();

            string[] mail_to = null;
            string[] mail_cc = null;
            string[] mail_bcc = null;

            using (SqlConnection conn = new SqlConnection(dao.connectionstring()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("EXEC [dbo].[LPOP_GetCompletenessCheckMailConfig]", conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            mail_to = reader.GetString(reader.GetOrdinal("TO")).Split(',');
                            mail_cc = reader.GetString(reader.GetOrdinal("CC")).Split(',');
                            mail_bcc = reader.GetString(reader.GetOrdinal("BCC")).Split(',');

                            if (mail_to.Length > 0)
                            {
                                foreach (var to in mail_to)
                                {
                                    if (to.Length > 0)
                                        data.MailRecipient.Add(to);
                                }
                            }

                            if (mail_cc.Length > 0)
                            {
                                foreach (var cc in mail_cc)
                                {
                                    if (cc.Length > 0)
                                        data.MailCarbonCopy.Add(cc);
                                }
                            }

                            if (mail_bcc.Length > 0)
                            {
                                foreach (var bcc in mail_bcc)
                                {
                                    if (bcc.Length > 0)
                                        data.MailBlindCarbonCopy.Add(bcc);
                                }
                            }

                            //data.usingCarbonCopy = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("USE_CC")).ToString());
                            //data.usingBlindCarbonCopy = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("USE_BCC")).ToString());
                            //data.withAttachment = Convert.ToBoolean(reader.GetString(reader.GetOrdinal("WITH_ATTCH")).ToString()); ;
                            data.MailSubject = reader.GetString(reader.GetOrdinal("SUBJECT"));
                            //data.MailBody = reader.GetString(reader.GetOrdinal("BODY")).ToString();
                            FileServerPath = reader.GetString(reader.GetOrdinal("FTP_PATH_PARAM"));
                        }
                    }
                }

                data.MailSubject = data.MailSubject.Replace("YYYYMM", ProdMonth);
                data.MailBody = BodyTable.Replace("{FTP_PATH}", FileServerPath);
                //mail_to = ConfigurationManager.AppSettings["MAIL_TO"].ToString().Split(',');
                //mail_cc = ConfigurationManager.AppSettings["MAIL_CC"].ToString().Split(',');
                //mail_bcc = ConfigurationManager.AppSettings["MAIL_BCC"].ToString().Split(',');

                //if (mail_to.Length > 0)
                //{
                //    foreach (var to in mail_to)
                //    {
                //        if (to.Length > 0)
                //            data.MailRecipient.Add(to);
                //    }
                //}

                //if (mail_cc.Length > 0)
                //{
                //    foreach (var cc in mail_cc)
                //    {
                //        if (cc.Length > 0)
                //            data.MailCarbonCopy.Add(cc);
                //    }
                //}

                //if (mail_bcc.Length > 0)
                //{
                //    foreach (var bcc in mail_bcc)
                //    {
                //        if (bcc.Length > 0)
                //            data.MailBlindCarbonCopy.Add(bcc);
                //    }
                //}

                data.usingCarbonCopy = Convert.ToBoolean(ConfigurationManager.AppSettings["USE_CC"].ToString());
                data.usingBlindCarbonCopy = Convert.ToBoolean(ConfigurationManager.AppSettings["USE_BCC"].ToString());
                data.withAttachment = Convert.ToBoolean(ConfigurationManager.AppSettings["WITH_ATTCH"].ToString()); ;
                //data.MailSubject = ConfigurationManager.AppSettings["MAIL_SUBJECT"].ToString();
                //data.MailBody = ConfigurationManager.AppSettings["MAIL_BODY"].ToString();

                //DateTime d = DateTime.Now;
                //string prodmonth = d.ToString("yyyyMM");

                //data.MailSubject = data.MailSubject.Replace("[PROD_MONTH]", prodmonth);

                //data.Attachment.Add(attachment);
                //data.AttachmentName = filename;

                conn.Close();
            }

            mail.sendEmail(data);
        }
    }
}
