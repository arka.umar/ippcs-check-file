﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using  LPOPFeedbackFixTaskRuntime.Model;
using System.Reflection;

namespace LPOPFeedbackFixTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {

            LPOPFeedbackFixTaskRuntime grRuntime = new LPOPFeedbackFixTaskRuntime();
            
            grRuntime.ExecuteExternal(args);
             
        }
    }
}
