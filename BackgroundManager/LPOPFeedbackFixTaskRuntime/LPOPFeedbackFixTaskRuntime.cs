﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using LPOPFeedbackFixTaskRuntime.Model;
using System.Reflection;
using Toyota.Common.Logging.Sink;
using System.IO;
using System.Configuration;
using Toyota.Common.Web.Notifcation.JobMonitoring;


namespace LPOPFeedbackFixTaskRuntime
{
    public class LPOPFeedbackFixTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(LPOPFeedbackFixTaskRuntime)), "LPOPFeedbackFixTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            List<string> listSuplier = new List<string>();
            List<listFix> listtentative = new List<listFix>();

            string productionMonth;
            string dateExpired;
            string attachID = "";
            string functionID = ConfigurationManager.AppSettings["functionID"];
            string contentID = ConfigurationManager.AppSettings["contentID"];
            string fileLocation = ConfigurationManager.AppSettings["fileLocation"].ToString();
            ArgumentParameter param = new ArgumentParameter();
            string format = "dd MMM yyyy";

            //listSuplier = db.Query<string>("GetListSuplier").ToList<string>();
            listSuplier = db.Query<string>(DatabaseManager.LoadQuery("GetListSuplier"), new object[] { }).ToList<string>();
            foreach (string str in listSuplier)
            {
                //listtentative = db.Query<listFix>("getFixDelay", new string[] { str }).ToList<listFix>();
                listtentative = db.Query<listFix>(DatabaseManager.LoadQuery("getFixDelay"), new object[] { str }).ToList<listFix>();
                foreach (listFix dt in listtentative)
                {
                    productionMonth = dt.productionMonth;
                    dateExpired = dt.fixDate.ToString(format);
                    param.Clear();
                    param.Add("@production_month", productionMonth);
                    param.Add("@date", dateExpired);
                    DirectJobProvider.Execute(functionID, contentID, contentID, param, attachID);
                    //System.Threading.Thread.Sleep(250);
                }
            }
            db.Close();
            System.Threading.Thread.Sleep(10000);
            //Directory.Delete(fileLocation, true);

        }
    }

}
