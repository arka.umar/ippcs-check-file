﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LPOPFeedbackFixTaskRuntime.Model
{
    class listFix
    {
        public string suplierCode { get; set; }
        public string feedbackSubmit { get; set; }
        public string productionMonth { get; set; }
        public DateTime fixDate { get; set; }
        public DateTime tentativeDate { get; set; }

    }
}
