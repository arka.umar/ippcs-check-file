﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using GoodsReceiptTransferTaskRuntime.Models;
using System.Reflection;

namespace GoodsReceiptTransferTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            GoodsReceiptTransferTaskRuntime grRuntime = new GoodsReceiptTransferTaskRuntime();

            #region Server Deploy
            //grRuntime.ExecuteExternal(args);
            #endregion

            #region Local Testing
            BackgroundTaskParameter parameters = new BackgroundTaskParameter();
            parameters.Add("DataList", new List<IDictionary<string, string>>(), typeof(List<IDictionary<string, string>>));
            parameters.Add("Process", "");
            parameters.Add("Username", "");
            grRuntime.ExecuteExternal(new String[] { parameters.ToString() });            
            #endregion
        }
    }
}
