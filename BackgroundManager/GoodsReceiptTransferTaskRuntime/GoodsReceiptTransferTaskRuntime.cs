﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using GoodsReceiptTransferTaskRuntime.Models;
using System.Reflection;
using Toyota.Common.Logging.Sink;

namespace GoodsReceiptTransferTaskRuntime
{
    class GoodsReceiptTransferTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(GoodsReceiptTransferTaskRuntime)), "GoodsReceiptTransferTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
            ServiceResult result = new ServiceResult() ;

            #region Testing script get current data from database.
            List<GoodsReceiptInquiryData> goodsReceiptInquiryDataModel = new List<GoodsReceiptInquiryData>();
            goodsReceiptInquiryDataModel = db.Query<GoodsReceiptInquiryData>(
                    DatabaseManager.LoadQuery("GetAllGoodsReceiptInquiry"),
                    new object[] { 
                        DateTime.Now.ToString("yyyy-MM-dd"),   // ARRIVAL TIME FROM
                        DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"),   // ARRIVAL TIME TO
                        "",     // DOCK CD
                        "",     // SUPPLIER CD
                        "",     // RCV PLANT CD
                        "",     // ROUTE RATE
                        "",     // MANIFEST NO
                        "3,5",    // MANIFEST RECEIVE FLAG
                        "",     // ORDER NO
                        ""      // DOCK CD LDAP
                        }).ToList<GoodsReceiptInquiryData>();

            List<IDictionary<string, string>> dataList = new List<IDictionary<string, string>>();
            IDictionary<string, string> dataMap;
            for (int i = 0; i < goodsReceiptInquiryDataModel.Count; i++)
            {
                dataMap = new Dictionary<string, string>();
                dataMap.Add("DockCode", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].DOCK_CD) ? "" : goodsReceiptInquiryDataModel[i].DOCK_CD);
                dataMap.Add("SupplierCode", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].SUPPLIER_CD) ? "" : goodsReceiptInquiryDataModel[i].SUPPLIER_CD);
                dataMap.Add("RecPlantCode", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].RCV_PLANT_CD) ? "" : goodsReceiptInquiryDataModel[i].RCV_PLANT_CD);
                dataMap.Add("ManifestNo", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].MANIFEST_NO) ? "" : goodsReceiptInquiryDataModel[i].MANIFEST_NO);
                dataMap.Add("OrderNo", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].ORDER_NO) ? "" : goodsReceiptInquiryDataModel[i].ORDER_NO);
                dataMap.Add("ApprovedBy", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].APPROVED_BY) ? "" : goodsReceiptInquiryDataModel[i].APPROVED_BY);
                dataList.Add(dataMap);
            }
            String process = "Approve";
            String username = "System";
            #endregion

            db.Close();

            /* Get parameter with indexing */
            //List<IDictionary<string, string>> dataList = JSON.ToObject<List<IDictionary<string, string>>>(parameters[1]);
            //String process = parameters[2];
            //String username = parameters[3];

            /* Get parameter with key */
            //List<IDictionary<string, string>> dataList = parameters.Get<List<IDictionary<string, string>>>("DataList");
            //String process = parameters.Get("Process");
            //String username = parameters.Get("Username");

            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Connection String : " + DatabaseManager.GetDefaultConnectionDescription().ConnectionString,
                Severity = LoggingSeverity.Warning
            });
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Datalist : " + JSON.ToString<List<IDictionary<string, string>>>(dataList),
                Severity = LoggingSeverity.Warning
            });
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Process : " + process,
                Severity = LoggingSeverity.Warning
            });
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Username : " + username,
                Severity = LoggingSeverity.Warning
            });

            try
            {
                bool isValid = true;
                string manifestReceiveFlag = "";

                db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);
                foreach (IDictionary<string, string> map in dataList)
                {
                    manifestReceiveFlag = db.ExecuteScalar<string>(
                                            DatabaseManager.LoadQuery("GetAllGoodsReceiptInquiryStatusByManifest"),
                                            new object[] {
                                                map["ManifestNo"]
                                        });

                    if (!manifestReceiveFlag.Equals("2") && !manifestReceiveFlag.Equals("3") && !manifestReceiveFlag.Equals("5"))
                        isValid = false;
                }
                db.Close();

                if (!isValid)
                {
                    DefaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = process + " Failed! You can only approve manifest(s) which status : Scanned, Approved, Error Posting.",
                        Severity = LoggingSeverity.Warning
                    });
                    SetProgress(100);
                }
                else
                {
                    SetProgress(5);

                    string sendFlag = "";
                    string processID = "";
                    string sourceType = "";

                    IList<GoodsReceiptICSTransfer> tempList;
                    List<GoodsReceiptICSTransfer> list = new List<GoodsReceiptICSTransfer>();
                    List<Dictionary<string, string>> noDataList = new List<Dictionary<string, string>>();

                    #region Phase 1 : Generate Posting File GR

                    db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

                    DefaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = "Start in generate posting file GR.",
                        Severity = LoggingSeverity.Info
                    });

                    foreach (Dictionary<string, string> data in dataList)
                    {
                        try
                        {
                            tempList = db.Fetch<GoodsReceiptICSTransfer>(DatabaseManager.LoadQuery("ICSGeneratePostingFileGR"), new object[] { data["ApprovedBy"], data["ManifestNo"], data["SupplierCode"], data["RecPlantCode"], data["DockCode"] });

                            if ((tempList.Count > 0) && (tempList != null))
                            {
                                list.AddRange(tempList);
                                sendFlag = list[0].sendFlag;
                                processID = list[0].processID;
                                sourceType = list[0].sourceType;
                            }
                            else
                            {
                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "No data found in generate posting file GR. Manifest No : " + data["ManifestNo"],
                                    Severity = LoggingSeverity.Warning
                                });
                            }
                        }
                        catch (Exception exc)
                        {
                            DefaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Error in generate posting file GR. Manifest No : " + data["ManifestNo"] + ". Message : " + exc.Message,
                                Severity = LoggingSeverity.Error
                            });

                            throw exc;
                        }

                    }

                    #region Update error status to no data manifest

                    foreach (Dictionary<string, string> data in noDataList)
                    {
                        db.ExecuteScalar<string>(
                            DatabaseManager.LoadQuery("ApproveGoodsReceiptInquiry"),
                            new object[] {
                            data["DockCode"],   // dockCode
                            data["SupplierCode"],   // supplierCode
                            data["RecPlantCode"],   // rcvPlantCode
                            data["ManifestNo"],   // manifestNo
                            "5",
                            data["OrderNo"],   // orderNo 
                            username
                    });
                    }
                    #endregion

                    DefaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = "Finish in generate posting file GR.",
                        Severity = LoggingSeverity.Info
                    });
                    SetProgress(20);

                    db.Close();

                    #endregion

                    if (list.Count == 0 & sendFlag != "" & processID != "")
                    {
                        DefaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = "No data found in generate posting file GR.",
                            Severity = LoggingSeverity.Warning
                        });
                        SetProgress(100);
                    }
                    else
                    {
                        // Generate process id
                        processID = db.ExecuteScalar<string>(
                            DatabaseManager.LoadQuery("GenerateProcessId"),
                            new object[] { 
                            process,     // what
                            username,     // user
                            "Goods Receipt Inquiry",     // where
                            "",     // pid OUTPUT
                            "",     // id
                            "",     // type
                            "4",     // module
                            process == "Approve" ? "42001" : "42002",     // function
                            "0"      // sts
                        });

                        #region Phase 2 : Startup IPPCS - ICS Gateway Web Service

                        /*
                        * Commented By : fid.salman
                        * Commented Dt : 02.08.2013
                        * Note         : IPPCS - ICS Gateway web service was non-activated cause direct
                        *                communication to ICS web service used in the posting process
                        */

                        //defaultLogSession.WriteLine(new LoggingMessage()
                        //{
                        //    Message = "Do process startup IPPCS - ICS Gateway web service.",
                        //    Severity = LoggingSeverity.Info
                        //});

                        //bool serviceReady = false;

                        //try
                        //{
                        //    gateway.Open();
                        //    string testStringResult = gateway.execute("Startup", "IntegrityCheck", null);
                        //    if (!string.IsNullOrEmpty(testStringResult))
                        //    {
                        //        result = ServiceResult.Create(testStringResult);
                        //        serviceReady = (result.Status == ServiceResult.STATUS_READY);
                        //    }
                        //}
                        //catch (Exception exc)
                        //{
                        //    gateway.Abort();
                        //    gateway = new GatewayService.WebServiceImplClient();
                        //    gateway.Open();

                        //    defaultLogSession.WriteLine(new LoggingMessage()
                        //    {
                        //        Message = "Error do process startup IPPCS - ICS Gateway web service. Message : " + exc.Message,
                        //        Severity = LoggingSeverity.Error
                        //    });

                        //    throw exc;
                        //}

                        //if (!serviceReady)
                        //{
                        //    defaultLogSession.WriteLine(new LoggingMessage()
                        //    {
                        //        Message = "Fail do process startup IPPCS - ICS Gateway web service.",
                        //        Severity = LoggingSeverity.Warning
                        //    });
                        //}

                        #endregion

                        #region Phase 3 : Init State ICS Web Service

                        DefaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = "Do process init state ICS web service.",
                            Severity = LoggingSeverity.Info
                        });

                        string jsonResult;
                        ServiceParameters sp = new ServiceParameters();
                        string sessionID;
                        sp.Add("state", "init");
                        result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                        SetProgress(40);

                        #endregion

                        if (result.Status != ServiceResult.STATUS_ERROR)
                        {
                            #region Phase 4 : Transfer Data to ICS Web Service

                            DefaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Start in transfer data to ICS web service.",
                                Severity = LoggingSeverity.Info
                            });

                            sessionID = result.Value.ToString();

                            sp.Clear();
                            sp.Add("state", "transfer");
                            sp.Add("sessionId", result.Value);
                            sp.Add("data", String.Empty);

                            const int MAX_DATA_TRANSFERRED = 10;
                            int dataLength = list.Count;
                            int lastIndex = dataLength;
                            int counter = 0;
                            int fetchSize;
                            int indexDifference;
                            bool looping = true;
                            bool transferError = false;
                            while (looping)
                            {
                                indexDifference = (lastIndex - counter);
                                if (indexDifference < MAX_DATA_TRANSFERRED)
                                {
                                    fetchSize = indexDifference;
                                }
                                else
                                {
                                    fetchSize = MAX_DATA_TRANSFERRED;
                                }

                                tempList = list.GetRange(counter, fetchSize);
                                jsonResult = JSON.ToString<List<GoodsReceiptICSTransfer>>(tempList);

                                sp.Remove("data");
                                sp.Add("data", (object)jsonResult);
                                sp.Add("userId", username);
                                result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                                if (result.Status == ServiceResult.STATUS_SUCCESS)
                                {
                                    counter += (fetchSize);
                                }
                                else
                                {
                                    transferError = true;
                                    looping = false;

                                    DefaultLogSession.WriteLine(new LoggingMessage(string.Format("Process result: Status -> {0}, Value: {1}", result.Status, result.MappedValues["__error_message__"])));
                                }

                                if (counter == lastIndex)
                                {
                                    looping = false;
                                }
                            }

                            DefaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Finish in transfer data to ICS web service.",
                                Severity = LoggingSeverity.Info
                            });
                            SetProgress(60);

                            #endregion

                            if (transferError)
                            {
                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Fail in transfer data to ICS web service.",
                                    Severity = LoggingSeverity.Warning
                                });
                                SetProgress(100);
                            }
                            else
                            {
                                #region Phase 5 : Taking Data From ICS Web Service

                                db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Start in process state ICS web service.",
                                    Severity = LoggingSeverity.Info
                                });

                                sp.Clear();
                                sp.Add("state", "process");
                                sp.Add("sessionId", sessionID);
                                sp.Add("sendFlag", sendFlag);
                                sp.Add("userId", username);
                                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                                result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                                if (result.Status != ServiceResult.STATUS_ERROR)
                                {
                                }
                                else
                                {
                                }

                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Finish in process state ICS web service.",
                                    Severity = LoggingSeverity.Info
                                });
                                SetProgress(80);

                                db.Close();

                                #endregion

                                #region Phase 6 : Finish State ICS Web Servcie

                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Start in finish state ICS web service.",
                                    Severity = LoggingSeverity.Info
                                });

                                sp.Clear();
                                sp.Add("state", "finish");
                                sp.Add("sessionId", sessionID);
                                result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));
                                sp.Clear();
                                if (result.Status == ServiceResult.STATUS_ERROR)
                                {
                                    DefaultLogSession.WriteLine(new LoggingMessage()
                                    {
                                        Message = "Fail in finish state ICS web service.",
                                        Severity = LoggingSeverity.Warning
                                    });
                                }
                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Finish in finish state ICS web service.",
                                    Severity = LoggingSeverity.Info
                                });
                                SetProgress(100);

                                #endregion
                            }
                        }
                        else
                        {
                            DefaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Fail do process init state ICS web service.",
                                Severity = LoggingSeverity.Warning
                            });
                            SetProgress(100);
                        }

                        gateway.Close();
                    }

                    //db.Close();
                }
            }
            catch (Exception exc)
            {
                gateway.Close();
                db.Close();

                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Error in do process " + process + " by " + username + " id " + Id + ". Message : " + exc.Message,
                    Severity = LoggingSeverity.Error
                });
                SetProgress(100);

                throw exc;
            }
            finally
            {
                // Delete log process lock table
                db.ExecuteScalar<string>(
                        DatabaseManager.LoadQuery("DeleteLogProcessLockByFunctionNo"),
                        new object[] {
                                process
                                ,username
                        });
            }
        }
    }
}
