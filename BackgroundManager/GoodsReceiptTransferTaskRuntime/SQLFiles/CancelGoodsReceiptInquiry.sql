﻿/* Cancel goods receipt inquiry data (tb_r_daily_manifest) fid.salman 09.05.2013 */

DECLARE
    @@dockCode AS VARCHAR(MAX) = @0,
    @@supplierCode AS VARCHAR(MAX) = @1,
    @@rcvPlantCode AS VARCHAR(MAX) = @2,
    @@manifestNo AS VARCHAR(MAX) = @3,
	@@manifestReceiveFlag AS VARCHAR(1) = @4,
	@@orderNo AS VARCHAR(MAX) = @5,
	@@userName AS VARCHAR(MAX) = @6,

	@@sqlQuery AS VARCHAR(MAX)

IF @@manifestReceiveFlag = '8'
BEGIN
	SET @@sqlQuery = 'UPDATE [TB_R_DAILY_ORDER_MANIFEST]
	SET
		--MANIFEST_RECEIVE_FLAG = ''8'',
		IN_PROGRESS = ''1'',
		CHANGED_BY = ''' + @@userName + ''',
		CHANGED_DT = GETDATE()
	WHERE
		[DOCK_CD] = ''' + @@dockCode + ''' AND
		[SUPPLIER_CD] = ''' + @@supplierCode + ''' AND
		[RCV_PLANT_CD] = ''' + @@rcvPlantCode + ''' AND
		[MANIFEST_NO] = ''' + @@manifestNo + ''' AND
		[ORDER_NO] = ''' + @@orderNo + ''''
END

EXEC (@@sqlQuery)

