﻿/* Get all data from goods receipt inquiry table (tb_r_daily_order_manifest) fid.salman 02.05.2013 */

DECLARE
	@@arrivalDateFrom AS VARCHAR(MAX) = @0,
    @@arrivalDateTo AS VARCHAR(MAX) = @1,
    @@dockCode AS VARCHAR(MAX) = @2,
    @@supplierCode AS VARCHAR(MAX) = @3,
    @@rcvPlantCode AS VARCHAR(MAX) = @4,
    @@routeRate AS VARCHAR(MAX) = @5,
    @@manifestNo AS VARCHAR(MAX) = @6,
    @@manifestReceiveFlag AS VARCHAR(MAX) = @7,
    @@orderNo AS VARCHAR(MAX) = @8,
	@@dockCodeLDAP AS VARCHAR(MAX) = @9,

	@@sqlQuery AS VARCHAR(MAX)

IF object_id('tempdb..#TempManifestNo') IS NOT NULL DROP TABLE #TempManifestNo
IF object_id('tempdb..#TempDockCode') IS NOT NULL DROP TABLE #TempDockCode
IF object_id('tempdb..#TempDockCodeLDAP') IS NOT NULL DROP TABLE #TempDockCodeLDAP
IF object_id('tempdb..#TempSupplierCode') IS NOT NULL DROP TABLE #TempSupplierCode
IF object_id('tempdb..#TempManifestReceiveFlag') IS NOT NULL DROP TABLE #TempManifestReceiveFlag

CREATE TABLE #TempManifestNo
( 
	MANIFEST_NO VARCHAR(100)
)

CREATE TABLE #TempDockCode
( 
	DOCK_CODE VARCHAR(100)
)

CREATE TABLE #TempDockCodeLDAP
( 
	DOCK_CODE VARCHAR(100)
)

CREATE TABLE #TempSupplierCode
( 
	SUPPLIER_CODE VARCHAR(100)
)

CREATE TABLE #TempManifestReceiveFlag
( 
	MANIFEST_RECEIVE_FLAG VARCHAR(100)
)

INSERT  INTO #TempManifestNo(MANIFEST_NO)
EXEC dbo.SplitString @@manifestNo, ','

INSERT  INTO #TempDockCode(DOCK_CODE)
EXEC dbo.SplitString @@dockCode, ','

INSERT  INTO #TempDockCodeLDAP(DOCK_CODE)
EXEC dbo.SplitString @@dockCodeLDAP, ','

INSERT  INTO #TempSupplierCode(SUPPLIER_CODE)
EXEC dbo.SplitString @@supplierCode, ','

INSERT  INTO #TempManifestReceiveFlag(MANIFEST_RECEIVE_FLAG)
EXEC dbo.SplitString @@manifestReceiveFlag, ','

SET @@sqlQuery = 'SELECT TOP 150
		TRDOM.[MANIFEST_NO],
		TRDOM.[ORDER_TYPE],
		TRDOM.[SUPPLIER_CD],
		TMS.[SUPPLIER_NAME],
		TRDOM.[RCV_PLANT_CD],
		TRDOM.[DOCK_CD],
		TRDOM.[TOTAL_ITEM],
		TRDOM.[TOTAL_QTY],
		TRDOM.[MANIFEST_RECEIVE_FLAG],
	
		--CASE ISNULL(TRDOM.[DELAY_FLAG], ''0'')
		--	WHEN 0 THEN ''X''
		--	ELSE ''Y''
		--END AS ''ISSUE_DELAY'',	
		--CASE ISNULL(TRDOM.[PROBLEM_FLAG], ''0'')
		--	WHEN 0 THEN ''X''
		--	ELSE ''Y''
		--END AS ''ISSUE_PROBLEM_PART'',
	
		TRDOM.[DELAY_FLAG],
		TRDOM.[PROBLEM_FLAG],

		TRDOM.[ORDER_NO],
		TRDOM.[PO_NO],
	
		(RTRIM(TRDOM.SUB_ROUTE_CD) + ''-'' + RTRIM(TRDOM.[SUB_ROUTE_SEQ])) AS ''ROUTE_RATE'',
	
		TRDOM.[ARRIVAL_PLAN_DT],
		TRDOM.[ARRIVAL_ACTUAL_DT],
		TRDOM.[SCAN_DT],
		TRDOM.[APPROVED_DT],
		TRDOM.[APPROVED_BY],
		TRDOM.[MAT_DOC_NO],
		TRDOM.[ICS_FLAG],
		TRDOM.[ICS_DT],
		TRDOM.[INVOICE_NO],
		TRDOM.[INVOICE_DT],
		TRDOM.[INVOICE_BY],
		TRDOM.[CANCEL_DT],
		TRDOM.[CANCEL_BY],

		TRDOM.[GR_DOCUMENT_DT],
		TRDOM.[CANCEL_FLAG],
		TRDOM.[DELETION_FLAG],
		TRDOM.[IN_PROGRESS]

	FROM [TB_R_DAILY_ORDER_MANIFEST] TRDOM
		LEFT JOIN [TB_M_SUPPLIER] TMS ON
			TRDOM.[SUPPLIER_CD] = TMS.[SUPPLIER_CODE] AND
			TRDOM.[SUPPLIER_PLANT] = TMS.[SUPPLIER_PLANT_CD]
		--LEFT JOIN [TB_R_DELIVERY_CTL_MANIFEST] TRDCM ON
		--	TRDOM.[MANIFEST_NO] = TRDCM.[MANIFEST_NO]
		--LEFT JOIN [TB_R_DELIVERY_CTL_H] TRDCH ON
		--	TRDCM.[DELIVERY_NO] = TRDCH.[DELIVERY_NO]
	WHERE
		--(CONVERT(VARCHAR(10), TRDOM.[ARRIVAL_PLAN_DT], 126) BETWEEN CONVERT(VARCHAR(10), CONVERT(DATETIME, ''' + @@arrivalDateFrom + '''), 126) AND  CONVERT(VARCHAR(10), CONVERT(DATETIME, ''' + @@arrivalDateTo + '''), 126)) AND 
		--((TRDOM.[DOCK_CD] IN (''' + @@dockCode + ''') AND ISNULL(''' + @@dockCode + ''', '''') <> '''') or (ISNULL(''' + @@dockCode + ''', '''') = '''')) AND 
		--((TRDOM.[SUPPLIER_CD] IN (''' + @@supplierCode + ''') AND ISNULL(''' + @@supplierCode + ''', '''') <> '''') or (ISNULL(''' + @@supplierCode + ''', '''') = '''')) AND 
		((TRDOM.[RCV_PLANT_CD]=''' + @@rcvPlantCode + ''' AND ISNULL(''' + @@rcvPlantCode + ''', '''') <> '''') or (ISNULL(''' + @@rcvPlantCode + ''', '''') = '''')) AND 
		(((RTRIM(TRDOM.SUB_ROUTE_CD) + ''-'' + RTRIM(TRDOM.[SUB_ROUTE_SEQ]))=''' + @@routeRate + ''' AND ISNULL(''' + @@routeRate + ''', '''') <> '''') or (ISNULL(''' + @@routeRate + ''', '''') = '''')) AND 
		--((TRDOM.[MANIFEST_NO]=''' + @@manifestNo + ''' AND ISNULL(''' + @@manifestNo + ''', '''') <> '''') or (ISNULL(''' + @@manifestNo + ''', '''') = '''')) AND 
		--((ISNULL(TRDOM.[MANIFEST_RECEIVE_FLAG], ''0'') IN (''' + @@manifestReceiveFlag + ''') AND ISNULL(''' + @@manifestReceiveFlag + ''', '''') <> '''') or (ISNULL(''' + @@manifestReceiveFlag + ''', '''') = '''')) AND 
		TRDOM.[SUPPLIER_CD] NOT LIKE ''807%'' AND 
		--TRDOM.[SHIPPING_DOCK] <> '''' AND
		--TRDOM.[ORDER_RELEASE_DT] BETWEEN ''2013-07-30'' AND ''2013-08-31'' AND
		--TRDOM.[ORDER_RELEASE_DT] > ''2013-08-14'' AND
		((TRDOM.[ORDER_NO]=''' + @@orderNo + ''' AND ISNULL(''' + @@orderNo + ''', '''') <> '''') or (ISNULL(''' + @@orderNo + ''', '''') = '''')) '

IF ( @@manifestNo <> '' ) 
    BEGIN
        SET @@sqlQuery = @@sqlQuery + ' AND (TRDOM.[MANIFEST_NO] IN (SELECT MANIFEST_NO FROM #TempManifestNo))'
    END

IF ( @@dockCode <> '' ) 
    BEGIN
        SET @@sqlQuery = @@sqlQuery + ' AND (TRDOM.[DOCK_CD] IN (SELECT DOCK_CODE FROM #TempDockCode))'
    END

IF ( @@dockCodeLDAP <> '' ) 
    BEGIN
        SET @@sqlQuery = @@sqlQuery + ' AND (TRDOM.[DOCK_CD] IN (SELECT DOCK_CODE FROM #TempDockCodeLDAP))'
    END

IF ( @@supplierCode <> '' ) 
    BEGIN
        SET @@sqlQuery = @@sqlQuery + ' AND (TRDOM.[SUPPLIER_CD] IN (SELECT SUPPLIER_CODE FROM #TempSupplierCode))'
    END
	
IF ( @@manifestReceiveFlag <> '' ) 
    BEGIN
        SET @@sqlQuery = @@sqlQuery + ' AND (ISNULL(TRDOM.[MANIFEST_RECEIVE_FLAG], ''0'') IN (SELECT MANIFEST_RECEIVE_FLAG FROM #TempManifestReceiveFlag))'
    END

SET @@sqlQuery = @@sqlQuery + ' AND TOTAL_QTY > 0 ORDER BY TRDOM.[MANIFEST_NO] ASC'

EXEC (@@sqlQuery)
