﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using GoodsReceiptRePostingTaskRuntime.Models;
using System.Reflection;

namespace GoodsReceiptRePostingTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inject Parameter, using server deploy block
            //args = new String[] { "{&quot;DataList&quot;:&quot;[{\\&quot;DockCode\\&quot;:\\&quot;43\\&quot;,\\&quot;SupplierCode\\&quot;:\\&quot;5165\\&quot;,\\&quot;RecPlantCode\\&quot;:\\&quot;4\\&quot;,\\&quot;ManifestNo\\&quot;:\\&quot;E420130053\\&quot;,\\&quot;OrderNo\\&quot;:\\&quot;2013081631E1\\&quot;}]&quot;,&quot;Process&quot;:&quot;Approve&quot;,&quot;Username&quot;:&quot;FID.Iman&quot;,&quot;tID&quot;:&quot;BKOOD2U22KO&quot;}" };
            
            GoodsReceiptRePostingTaskRuntime grRuntime = new GoodsReceiptRePostingTaskRuntime();

            #region Server Deploy
            //grRuntime.ExecuteExternal(args);
            #endregion
            
            #region Local Testing
            BackgroundTaskParameter parameters = new BackgroundTaskParameter();
            parameters.Add("DataList", new List<IDictionary<string, string>>(), typeof(List<IDictionary<string, string>>));
            parameters.Add("Process", "");
            parameters.Add("Username", "");
            grRuntime.ExecuteExternal(new String[] { parameters.ToString() });            
            #endregion
        }
    }
}
