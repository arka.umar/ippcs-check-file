﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using GoodsReceiptPostingTaskRuntime.Models;
using System.Reflection;

namespace GoodsReceiptPostingTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inject parameter using with server deploy block code
            //args = new String[] { "{&quot;DataList&quot;:&quot;[{\\&quot;DockCode\\&quot;:\\&quot;1R\\&quot;,\\&quot;SupplierCode\\&quot;:\\&quot;807D\\&quot;,\\&quot;RecPlantCode\\&quot;:\\&quot;1\\&quot;,\\&quot;ManifestNo\\&quot;:\\&quot;E100039241\\&quot;,\\&quot;OrderNo\\&quot;:\\&quot;2013071639E1\\&quot;}]&quot;,&quot;Process&quot;:&quot;Approve&quot;,&quot;Username&quot;:&quot;FID.Iman&quot;,&quot;tID&quot;:&quot;QMM1B3NBKZQ&quot;}" };

            GoodsReceiptPostingTaskRuntime grRuntime = new GoodsReceiptPostingTaskRuntime();

            #region Server Deploy
            grRuntime.ExecuteExternal(args);
            #endregion

            #region Local Testing
            //BackgroundTaskParameter parameters = new BackgroundTaskParameter();
            //parameters.Add("DataList", new List<IDictionary<string, string>>(), typeof(List<IDictionary<string, string>>));
            //parameters.Add("Process", "");
            //parameters.Add("Username", "");
            //grRuntime.ExecuteExternal(new String[] { parameters.ToString() });         
            #endregion
        }
    }
}
