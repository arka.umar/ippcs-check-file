﻿/* Get all data from goods receipt inquiry table (tb_r_daily_order_manifest) fid.salman 02.05.2013 */

DECLARE
    @@manifestNo AS VARCHAR(MAX) = @0,

	@@sqlQuery AS VARCHAR(MAX)
	
SET @@sqlQuery = 'SELECT
		TRDOM.[MANIFEST_RECEIVE_FLAG]
	FROM [TB_R_DAILY_ORDER_MANIFEST] TRDOM
	WHERE
		((TRDOM.[MANIFEST_NO]=''' + @@manifestNo + ''' AND ISNULL(''' + @@manifestNo + ''', '''') <> '''') or (ISNULL(''' + @@manifestNo + ''', '''') = ''''))'

EXEC (@@sqlQuery)
