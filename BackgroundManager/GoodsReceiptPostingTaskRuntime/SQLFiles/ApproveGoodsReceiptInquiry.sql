﻿/* Approve goods receipt inquiry data (tb_r_daily_manifest) fid.salman 09.05.2013 */

DECLARE
    @@dockCode AS VARCHAR(2) = @0,
    @@supplierCode AS VARCHAR(6) = @1,
    @@rcvPlantCode AS VARCHAR(1) = @2,
    @@manifestNo AS VARCHAR(16) = @3,
	@@manifestReceiveFlag AS VARCHAR(1) = @4,
	@@orderNo AS VARCHAR(12) = @5,
	@@userName AS VARCHAR(MAX) = @6,

	@@sqlQuery AS VARCHAR(MAX)
	
IF @@manifestReceiveFlag = '3'
BEGIN
	SET @@sqlQuery = '
	UPDATE [TB_R_DAILY_ORDER_MANIFEST]
	SET
		MANIFEST_RECEIVE_FLAG = ''' + @@manifestReceiveFlag + ''',
		--APPROVED_BY = CASE WHEN APPROVED_BY IS NULL
		--				THEN ''' + @@userName + '''
		--				ELSE APPROVED_BY
		--				END,
		--APPROVED_DT = CASE WHEN APPROVED_DT IS NULL
		--				THEN GETDATE()
		--				ELSE APPROVED_DT
		--				END,
		IN_PROGRESS = ''1'',
		CHANGED_BY = ''' + @@userName + ''',
		CHANGED_DT = GETDATE()
	WHERE
		[DOCK_CD] = ''' + @@dockCode + ''' AND
		[SUPPLIER_CD] = ''' + @@supplierCode + ''' AND
		[RCV_PLANT_CD] = ''' + @@rcvPlantCode + ''' AND
		[MANIFEST_NO] = ''' + @@manifestNo + ''' AND
		[ORDER_NO] = ''' + @@orderNo + ''''
END

IF @@manifestReceiveFlag = '5'
BEGIN
	SET @@sqlQuery = '
	UPDATE TB_R_DAILY_ORDER_MANIFEST 
	SET
		MANIFEST_RECEIVE_FLAG = 5,
		IN_PROGRESS = 3,
		CHANGED_BY = ''' + @@userName + ''',
		CHANGED_DT = GETDATE()
	WHERE
		MANIFEST_NO = ''' + @@manifestNo + ''''
END

EXEC (@@sqlQuery)

