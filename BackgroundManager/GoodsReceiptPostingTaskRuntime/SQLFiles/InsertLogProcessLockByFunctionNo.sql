﻿DECLARE
	@@processName AS VARCHAR(7) = @0
	,@@userName AS VARCHAR(20) = @1
	,@@isLocked AS BIT = 0

BEGIN TRAN
IF @@processName = 'Approve'
BEGIN
	IF NOT EXISTS (
		SELECT LOCK_REFF
		FROM TB_T_LOCK
		WHERE LOCK_REFF = '42001' + '_' + @@userName
	)
	BEGIN
		INSERT INTO TB_T_LOCK
		VALUES (
			'42001'
			,'42001' + '_' + @@userName
			,@@userName
			,GETDATE()
		)
		
		SET @@isLocked = 0
	END
	ELSE
	BEGIN
		SET @@isLocked = 1
	END
END
IF @@processName = 'Cancel'
BEGIN
	IF NOT EXISTS (
		SELECT LOCK_REFF
		FROM TB_T_LOCK
		WHERE LOCK_REFF = '42002' + '_' + @@userName
	)
	BEGIN
		INSERT INTO TB_T_LOCK
		VALUES (
			'42002'
			,'42002' + '_' + @@userName
			,@@userName
			,GETDATE()
		)
		
		SET @@isLocked = 0
	END
	ELSE
	BEGIN
		SET @@isLocked = 1
	END
END

IF @@@@ERROR <> 0
BEGIN
	ROLLBACK
END
ELSE
BEGIN
	COMMIT
END

SELECT @@isLocked