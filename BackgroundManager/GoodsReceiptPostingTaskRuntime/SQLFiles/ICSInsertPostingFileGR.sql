﻿
INSERT INTO dbo.TB_T_GRTP_RESULT_POSTING
        ( system_source ,
          usr_id ,
          movement_type ,
          process_dt ,
          posting_dt ,
          manifest_no ,
          kanban_order_no ,
          prod_purpose_cd ,
          source_type ,
          Ori_Mat_No ,
          ori_supp_cd ,
          receiving_area_cd ,
          plant_cd ,
          sloc_cd ,
          mat_doc_no ,
          mat_doc_year ,
		  po_no ,
          err_sts ,
          err_cd ,
          error_desc ,
          err_msg, 
		  PROCESS_ID
        )
VALUES  ( @0 , -- systeSource
          @1 , -- usr_id - varchar(20)
          @2 , -- movement_type - varchar(4)
          @3 , -- process_dt - datetime
          @4 , -- posting_dt - datetime
          @5 , -- manifest_no - varchar(16)
          @6 , -- kanban_order_no - varchar(16)
          @7 , -- prod_purpose_cd - varchar(5)
          @8 , -- source_type - varchar(1)
          @9 , -- Ori_Mat_No - varchar(30)
          @10 , -- ori_supp_cd - varchar(5)
          @11 , -- receiving_area_cd - varchar(6)
          @12 , -- plant_cd - varchar(4)
          @13 , -- sloc_cd - varchar(6)
          @14 , -- mat_doc_no - varchar(10)
          @15 , -- mat_doc_year - char(4)
		  @25 , -- po_no - varchar(10)
          @16 , -- err_sts - char(1)
          @17 , -- err_cd - varchar(20)
          @18 , -- error_desc - varchar(20)
          @19 , -- err_msg - varchar(2000)
		  @20
        )
		
DECLARE 
	@@newRowId AS INT		
	,@@manifestNo AS VARCHAR(16) = @5
	,@@processId AS BIGINT = @20
	,@@sessionId AS INT = @21
	,@@userName AS VARCHAR(20) = @22
	,@@process AS VARCHAR(6) = @23
	,@@seqNo AS INT = @24

IF (@16 = '1')
BEGIN
	IF NOT EXISTS(
		SELECT *
		FROM TB_R_LOG_TRANSACTION_H
		WHERE
			PROCESS_ID = @@processId
			AND PROCESS_ID_ICS = @@sessionId
			AND MANIFEST_NO = @@manifestNo
	)
	BEGIN
		-- INSERT LOG HEADER TABLE
		INSERT INTO TB_R_LOG_TRANSACTION_H
					([PROCESS_ID]
					,[PROCESS_ID_ICS]
					,[MANIFEST_NO]
					,[CREATED_BY]
					,[CREATED_DATE]
					,[CHANGED_BY]
					,[CHANGED_DATE])
		VALUES (
			@@processId,
			@@sessionId,
			@@manifestNo,
			@@userName,
			GETDATE(),
			NULL,
			NULL)
						
		SET @@newRowId = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		SELECT @@newRowId = LOG_ID_H
		FROM TB_R_LOG_TRANSACTION_H
		WHERE
			PROCESS_ID = @@processId
			AND PROCESS_ID_ICS = @@sessionId
			AND MANIFEST_NO = @@manifestNo
	END

	-- INSERT LOG DETAIL TABLE
	INSERT INTO TB_R_LOG_TRANSACTION_D
				([LOG_ID_H]
				,[SEQ_NO]
				,[SYSTEM_SOURCE]
				,[USR_ID]
				,[MOVEMENT_TYPE]
				,[PROCESS_DT]
				,[POSTING_DT]
				,[MANIFEST_NO]
				,[KANBAN_ORER_NO]
				,[PROD_PURPOSE_CD]
				,[SOURCE_TYPE]
				,[PART_NO]
				,[SUPP_CD]
				,[DOCK_CD]
				,[PLANT_CD]
				,[SLOC_CD]
				,[LOCATION]
				,[ERR_CD]
				,[ERR_MESSAGE]
				,[CREATED_BY]
				,[CREATED_DT]
				,[CHANGED_BY]
				,[CHANGED_DT])
	VALUES( 
		@@newRowId,
		@@seqNo,
		@0,
		@1,
		@2,
		@3,
		@4,
		@5,
		@6,
		@7,
		@8,
		@9,
		@10,
		@11,
		@12,
		@13,
		NULL,
		@17,
		@19,
		@@userName,
		GETDATE(),
		@@userName,
		GETDATE())

	-- INSERT INTO MANIFEST DETAIL
END

--EXEC SP_UpdateGoodReceipt @5, @20, @21, @22, @23, @25