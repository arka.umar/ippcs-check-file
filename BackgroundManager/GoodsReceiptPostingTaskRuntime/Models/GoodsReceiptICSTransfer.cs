﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodsReceiptPostingTaskRuntime.Models
{
    public class GoodsReceiptICSTransfer
    {
        public string systemSource { get; set; }
        public string movementType { get; set; }
        public string processDate { get; set; }
        public string postingDate { get; set; }
        public string refNo { get; set; }
        public string kanbanOrderNo { get; set; }
        public string prodPurpose { get; set; }
        public string sourceType { get; set; }
        public string oriMatNo { get; set; }
        public string postingQty { get; set; }
        public string oriUnitOfMeasure { get; set; }
        public string unitOfMeasure { get; set; }
        public string oriSuppCode { get; set; }
        public string receivingPlantCode { get; set; }
        public string receivingPlantArea { get; set; }
        public string intKanbanFlag { get; set; }
        public string dnCompleteFlag { get; set; }
        public string matCompleteFlag { get; set; }
        public string receiveNo { get; set; }
        public string sendFlag { get; set; }
        public string processID { get; set; }

        public string orderReleaseDate { get; set; }
    }
}