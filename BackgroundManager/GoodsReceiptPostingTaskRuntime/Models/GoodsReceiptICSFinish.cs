﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodsReceiptPostingTaskRuntime.Models
{

    public class GoodsReceiptICSResult
    {
        public string status { get; set; }
        public List<GoodsReceiptICSFinish> value { get; set; }
    }

    public class GoodsReceiptICSFinish
    {
        public string systemSource { get; set; }
        public string userId { get; set; }
        public string sessionId { get; set; }
        public string movementType { get; set; }
        public string processDate { get; set; }
        public string postingDate { get; set; }
        public string refNo { get; set; }
        public string kanbanOrderNo { get; set; }
        public string prodPurpose { get; set; }
        public string sourceType { get; set; }
        public string oriMatNo { get; set; }
        public string oriSuppCode { get; set; }
        public string receivingPlantArea { get; set; }
        public string plantCode { get; set; }
        public string slocCode { get; set; }
        public string matDocNo { set; get; }
        public string matDocYear { set; get; }
        public string errorStatus { get; set; }
        public string errorCode { get; set; }
        public string errorDesc { get; set; }
        public string errorMsg { get; set; }

        public string poNo { get; set; }
    }
}