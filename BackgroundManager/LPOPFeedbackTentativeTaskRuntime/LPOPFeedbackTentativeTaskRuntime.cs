﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using LPOPFeedbackTentativeTaskRuntime.Model;
using System.Reflection;
using Toyota.Common.Logging.Sink;
using System.IO;
using System.Configuration;
using Toyota.Common.Web.Notifcation.JobMonitoring;


namespace LPOPFeedbackTentativeTaskRuntime
{
    public class LPOPFeedbackTentativeTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(LPOPFeedbackTentativeTaskRuntime)), "LPOPFeedbackTentativeTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            List<string> listSuplier = new List<string>();
            List<listTentative> listtentative = new List<listTentative>();

            string productionMonth;
            string dateExpired;
            string attachID = "";
            string supplierCode;
            string functionID = ConfigurationManager.AppSettings["functionID"];
            string contentID = ConfigurationManager.AppSettings["contentID"];
            string fileLocation = ConfigurationManager.AppSettings["fileLocation"].ToString();
            ArgumentParameter param = new ArgumentParameter();
            string format = "dd MMM yyyy";

            listSuplier = db.Query<string>("GetListSuplier").ToList<string>();
            foreach (string str in listSuplier)
            {
                listtentative = db.Query<listTentative>("getTentativeDelay", new string[] { str }).ToList<listTentative>();
                foreach (listTentative dt in listtentative)
                {
                    productionMonth = dt.productionMonth;
                    dateExpired = dt.tentativeDate.ToString(format);
                    supplierCode = dt.suplierCode;
                    param.Clear();
                    param.Add("@production_month", productionMonth);
                    param.Add("@date", dateExpired);
                    DirectJobProvider.Execute(functionID, supplierCode, contentID, param, attachID);
                    System.Threading.Thread.Sleep(250);
                }
            }
            db.Close();
            System.Threading.Thread.Sleep(10000);
            Directory.Delete(fileLocation, true);

        }
    }

}
