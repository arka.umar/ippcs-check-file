﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

//using InvoicePostingTaskRuntime.Models;
using System.Reflection;
using Toyota.Common.Logging.Sink;

namespace InvoicePostingTaskRuntime
{
    class InvoicePostingTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            try
            {
                const int MAX_DATA_TRANSFERRED = 10;

                String processId = parameters.Get("ProcessID");
                String username = parameters.Get("Username");

                string SupplierInvoiceNo = parameters.Get("SupplierInvoiceNo");
                string LivBased = parameters.Get("LivBased");
                string SupplierCode = parameters.Get("SupplierCode");
                
                GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
                ServiceResult result;

                ServiceParameters sp = new ServiceParameters();
                string sessionID = processId;
                
                sp.Clear();
                sp.Add("state", "process");
                sp.Add("sessionId", sessionID);
                sp.Add("suppInvNo", SupplierInvoiceNo);
                sp.Add("livBased", LivBased);
                sp.Add("postingDate", "11/06/2009");
                sp.Add("calculateTax", "1");
                sp.Add("suppCode", SupplierCode);
                sp.Add("userId", username);
                sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                result = ServiceResult.Create(gateway.execute("creation", "invoiceSave", sp.ToString()));

                DefaultLogSession.WriteLine(new LoggingMessage(string.Format("Process result: Status -> {0}, Value: {1}", result.Status, result.Value)));

                gateway.Close();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}
