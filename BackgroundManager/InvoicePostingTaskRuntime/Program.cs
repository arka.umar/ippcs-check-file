﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoicePostingTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            InvoicePostingTaskRuntime poRuntime = new InvoicePostingTaskRuntime();

            poRuntime.ExecuteExternal(args);
        }
    }
}
