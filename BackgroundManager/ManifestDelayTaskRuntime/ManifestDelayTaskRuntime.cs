﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Database.Petapoco; 
using ManifestDelayTaskRuntime.Models;
using System.Reflection;
using Toyota.Common.Logging.Sink;
using System.IO;
using System.Configuration;
using Toyota.Common.Web.Notifcation.JobMonitoring;
using Toyota.Common.Web.Ioc;


namespace ManifestDelayTaskRuntime
{
    public class ManifestDelayTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(ManifestDelayTaskRuntime)), "ManifestDelayTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            IBootstrap  bootstrap = Bootstrap.GetInstance();
            bootstrap.RegisterProvider<Toyota.Common.Web.Database.IDBContextManager>(typeof(Toyota.Common.Web.Database.Petapoco.PetaPocoContextManager), true);

            List<DailyManifest> lstman = new List<DailyManifest>();
            List<string> lstdock = new List<string>();
            List<string> lstCount = new List<string>();
            List<RoleId> listRoleId = new List<RoleId>();
            string attachID;
            FileStream fs1;
            string CountManifest;


            //lstdock = db.Query<string>("GetDailyDockCode").ToList<string>();
            //lstCount = db.Query<string>("GetCountManifest").ToList<string>();

            lstdock = db.Query<string>(DatabaseManager.LoadQuery("GetDailyDockCode"), new object[] { }).ToList<string>();
            lstCount = db.Query<string>(DatabaseManager.LoadQuery("GetCountManifest"), new object[] { }).ToList<string>();
            

            AttachmentProvider attach = new AttachmentProvider();
            ArgumentParameter param = new ArgumentParameter();

            string fileLoc = string.Empty;
            bool isExists = System.IO.Directory.Exists(ConfigurationManager.AppSettings["fileLocation"]);

            if (!isExists)
                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["fileLocation"]);

            List<string> manifestNos;
            int i = 0;
            foreach (string str in lstdock)
            {

                fileLoc = ConfigurationManager.AppSettings["fileLocation"] + str + ".txt";
                fs1 = new FileStream(fileLoc, FileMode.OpenOrCreate, FileAccess.Write); 
                lstman = db.Query<DailyManifest>(DatabaseManager.LoadQuery("GetDailyManifest"), new object[] { str }).ToList<DailyManifest>();
            
                CountManifest = lstCount[i];

                manifestNos = new List<string>();
                using (StreamWriter sw = new StreamWriter(fs1))
                {
                    sw.Write("SuplierCode \t");
                    sw.Write("DockCode \t");
                    sw.WriteLine("ManifestNo");
                    sw.WriteLine("==========================================");



                    foreach (DailyManifest dm in lstman)
                    {
                        manifestNos.Add(dm.ManifestNo);
                        //sw.WriteLine(dm.ManifestNo);
                        sw.Write(dm.SupplierCode + "\t" + "\t");
                        sw.Write(dm.DockCode + "\t" + "\t");
                        sw.WriteLine(dm.ManifestNo);

                    }
                }
                fs1.Close();
                 
                listRoleId = db.Query<RoleId>(DatabaseManager.LoadQuery("GetRoleIdPerDock"), new object[] { str }).ToList<RoleId>();
            


                foreach (RoleId role in listRoleId)
                {
                    attach.Create("41001", ConfigurationManager.AppSettings["contentID"]);
                    //attach.Create("41001", str);
                    attach.Write(fileLoc);
                    attachID = attach.Flush();
                    param.Clear();
                    param.Add("@Dock", str);
                    param.Add("@number_manifest", CountManifest);
                    ParamArray keyParam = new ParamArray();
                    keyParam.Add("functionid", "41001");
                    keyParam.Add("roleid", ConfigurationManager.AppSettings["contentID"]);
                    keyParam.Add("contentid", ConfigurationManager.AppSettings["contentID"]);
                    keyParam.Add("recipientcode", ConfigurationManager.AppSettings["RecipientCode"]);
                    keyParam.Add("attachmentid", attachID);
                    DirectJobProvider.Execute(keyParam, param);
                    System.Threading.Thread.Sleep(250);
                }
                i++;
            }
            db.Close();
            System.Threading.Thread.Sleep(1000);
            Directory.Delete(ConfigurationManager.AppSettings["fileLocation"].ToString(), true);
            SetProgress(100);
        
        }
    }

}
