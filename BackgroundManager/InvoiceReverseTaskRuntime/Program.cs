﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceReverseTaskRuntime
{
    class Program
    {
        static void Main(string[] args)
        {
            InvoiceReverseTaskRuntime poRuntime = new InvoiceReverseTaskRuntime();

            poRuntime.ExecuteExternal(args);
        }
    }
}
