﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Service;
using Toyota.Common.Web.Credential;
using Toyota.Common.Web.Util.Converter;
using System.ServiceModel;
using Toyota.Web.Service.Wrapper.SecurityCenter;

namespace WebServiceWrapper_Dev
{
    public class SecurityCenterServiceClient: ServiceClient
    {
        private SecurityCenterClient client;
        private WSHttpBinding binding;

        public SecurityCenterServiceClient(): base("SecurityCenter")
        {            
            binding = ServiceBindings.CreateWSHttpBinding();
            binding.MaxBufferPoolSize = 2147483647;
            binding.MaxReceivedMessageSize = 2147483647;
            binding.MessageEncoding = WSMessageEncoding.Mtom;
            binding.BypassProxyOnLocal = false;
            binding.UseDefaultWebProxy = true;
            binding.AllowCookies = false;
            binding.ReaderQuotas.MaxDepth = 2147483647;
            binding.ReaderQuotas.MaxStringContentLength = 2147483647;
            binding.ReaderQuotas.MaxArrayLength = 2147483647;
            binding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            binding.ReaderQuotas.MaxNameTableCharCount = 2147483647;
            binding.ReliableSession.Enabled = true;
            binding.Security.Mode = SecurityMode.None;

            client = new SecurityCenterClient(binding, new EndpointAddress("http://10.16.25.211:8084/SecurityCenter.svc"));
        }

        public override void Close()
        {
            client.Close();
        }

        public override ServiceResult Execute(string actionName, ServiceParameters parameters)
        {
            return Execute(String.Empty, actionName, parameters);
        }

        public override ServiceResult Execute(string actorName, string actionName, ServiceParameters parameters)
        {
            ServiceResult serviceResult = null;
            switch (actionName)
            {
                case "GetUser":
                    string username = (string) parameters.Get("Username");
                    string password = (string) parameters.Get("Password");
                    string appName = (string) parameters.Get("ApplicationName");

                    string result = client.getRole(username, password, appName);
                    Dictionary<string, List<string>> dataMap = JSON.ToObject<Dictionary<string, List<string>>>(result);
                    User user = User.Cast(dataMap);
                    serviceResult = new ServiceResult();
                    serviceResult.Value = user;

                    break;
                default: break;
            }

            return serviceResult;
        }
    }
}
