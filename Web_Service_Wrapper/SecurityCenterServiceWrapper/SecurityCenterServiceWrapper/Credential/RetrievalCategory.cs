﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Credential
{
    public class RetrievalCategory : SelectableDataModel
    {
        public string GroupID { set; get; }
        public string CategoryID { set; get; }
        public string CategoryName { set; get; }
        public string DatabaseName { set; get; }
        public string TableView { set; get; }
        public override string SelectedKey
        {
            get
            {
                return "CategoryID;";
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Convert.ToString(CategoryID) + ";";
            }
        }
    }
}
