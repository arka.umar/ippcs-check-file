﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    public class AuthorizationDetail
    {
        public String UserName { set; get; }
        public String Area { set; get; }
        public String Object { set; get; }
        public String ScreenChild { set; get; }
        public String Screen { set; get; }
        public String Role { set; get; }        
        public String System { set; get; }
        public String Branch { set; get; }
    }
}
