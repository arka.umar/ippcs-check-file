﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    public class Role
    {
        public int RoleID { set; get; }
        public string Name {  set; get; }
        public int SessionTimeout{ set; get;}
    }
}
