﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Web.Credential
{
    public interface IUserAuthenticator
    {
        bool IsAuthentic(string Username, string Password);
    }
}
