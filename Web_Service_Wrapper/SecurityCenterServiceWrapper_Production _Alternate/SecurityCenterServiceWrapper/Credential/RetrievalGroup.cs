﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.MVC;

namespace Toyota.Common.Web.Credential
{
    public class RetrievalGroup : SelectableDataModel
    {
        public string ID { set; get; }
        public string GroupName { set; get; }

        public override string SelectedKey
        {
            get
            {
                return "ID;";
            }
        }

        public override string SelectedKeyValue
        {
            get
            {
                return Convert.ToString(ID) + ";";
            }
        }
    }
}
