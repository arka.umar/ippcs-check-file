﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Service;
using System.ServiceModel;

namespace WebServiceWrapper
{
    public class ServiceGatewayClient: ServiceClient
    {
        private Toyota.Web.Service.Wrapper.ServiceGateway.WebServiceClient client;
        private BasicHttpBinding binding;

        public ServiceGatewayClient(): base("ServiceGateway")
        {
            binding = ServiceBindings.CreateDefaultBinding();
            client = new Toyota.Web.Service.Wrapper.ServiceGateway.WebServiceClient(binding, new EndpointAddress("http://10.16.25.214:8132/Gateway.svc"));
        }

        public override void Close()
        {
            client.Close();
        }

        public override ServiceResult Execute(string actionName, ServiceParameters parameters)
        {
            return Execute(String.Empty, actionName, parameters); 
        }

        public override ServiceResult Execute(string actorName, string actionName, ServiceParameters parameters)
        {
            string resultString = client.Execute(actorName, actionName, parameters.ToString());
            return ServiceResult.Create(resultString);
        }
    }
}
