﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Data.SQLite;

namespace DCLOffline.Desktop
{
    public partial class FormMain : Form
    {
        string IPAddressIPPCS = System.Configuration.ConfigurationManager.AppSettings["IPAddressIPPCS"];
        string autonumbergeneratorToString;
        DateTime arrivaltimeDT = System.DateTime.Now;
        TimeSpan ts = new TimeSpan();
        string DockCodeVirtu;
        string DeliveryNoVirtu;
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            #region " Then we start for the transaction system eventhandler "

            // set the autorefresh for the gridview
            gridviewautorefreshdatatableeventhandler();

            // start for the controller
            cmdReceive.Enabled = false;
            timer1.Enabled = true;
            if (timer1.Enabled == true)
            {
                label4.Text = "IPPCS IS OFFLINE";
                MetroIdle.BackColor = Color.Red;
                label4.BackColor = Color.Red;
            }

            //simpleButton3.Visible = false;

            #endregion
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("You Are Now Leaving The System, Bye!!", "IPPCS OFFLINE System");
            FormMain me = new FormMain();
            Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblcurrtime.Text = DateTime.Now.ToString("hh:mm:ss");
        }

        private bool CheckDeliveryNoAndLogisticPoint(string deliveryNo, string LogisticPoint)
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            bool hasil = false;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(cs))
                {
                    con.Open();

                    string sqlQuery = "select * from DeliveryScan";

                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                if (rdr["LogisticPoint"].ToString() == txtDOckCode.Text && rdr["DeliveryNo"].ToString() == txtDeliveryNo.Text)
                                {
                                    hasil = true;
                                }
                                else
                                {
                                    hasil = false;
                                }
                            }
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite);
                hasil = false;
            }
            return hasil;
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string connectionstringtofirstsavingdataprocess = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                bool ifExist = CheckDeliveryNoAndLogisticPoint(txtDeliveryNo.Text.Trim(), txtDOckCode.Text.Trim());

                if (ifExist)
                {
                    laksanakanperintahupdate();
                }
                else 
                {
                    laksanakanperintahsimpanbaru(); 
                }
            }
            catch (Exception excumalertfirstsave)
            {
                MessageBox.Show(excumalertfirstsave.Message + excumalertfirstsave.Source + excumalertfirstsave.StackTrace + excumalertfirstsave.TargetSite);
            }
        }

        //private void simpleButton1_Click(object sender, EventArgs e)
        //{
        //    string connectionstringtofirstsavingdataprocess = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
        //    try
        //    {
        //        SQLiteConnection connectionreaddatatablesave = new SQLiteConnection(connectionstringtofirstsavingdataprocess);
        //        SQLiteCommand cmdreaddatatabletosavingcondition = new SQLiteCommand();
        //        cmdreaddatatabletosavingcondition.CommandText = "select * from DeliveryScan";
        //        cmdreaddatatabletosavingcondition.Connection = connectionreaddatatablesave;

        //        connectionreaddatatablesave.Open();

        //        SQLiteDataReader sdrreaddatatabletoconditionsaving = cmdreaddatatabletosavingcondition.ExecuteReader();

        //        if (sdrreaddatatabletoconditionsaving.HasRows == true)
        //        {
        //            while (sdrreaddatatabletoconditionsaving.Read())
        //            {
        //                if (sdrreaddatatabletoconditionsaving["LogisticPoint"].ToString() == txtDOckCode.Text && sdrreaddatatabletoconditionsaving["DeliveryNo"].ToString() == txtDeliveryNo.Text)
        //                {
        //                    //DockCodeVirtu = sdrreaddatatabletoconditionsaving["LogisticPoint"].ToString();
        //                    //DeliveryNoVirtu = sdrreaddatatabletoconditionsaving["DeliveryNo"].ToString();
        //                    sdrreaddatatabletoconditionsaving.Close();
        //                    connectionreaddatatablesave.Close();
        //                    laksanakanperintahupdate();
        //                }
        //                else
        //                {
        //                    //DockCodeVirtu = sdrreaddatatabletoconditionsaving["LogisticPoint"].ToString();
        //                    //DeliveryNoVirtu = sdrreaddatatabletoconditionsaving["DeliveryNo"].ToString();
        //                    sdrreaddatatabletoconditionsaving.Close();
        //                    connectionreaddatatablesave.Close();
        //                    laksanakanperintahsimpanbaru();
        //                }
        //            }
        //        }
        //        else
        //        {
        //            MessageBox.Show("tidak terdapat data pada datatable anda, sistem akan memulai untuk melakukan proses inputan baru");
        //            laksanakanperintahsimpanbaru();
        //        }
        //    }
        //    catch (Exception excumalertfirstsave)
        //    {
        //        MessageBox.Show(excumalertfirstsave.Message + excumalertfirstsave.Source + excumalertfirstsave.StackTrace + excumalertfirstsave.TargetSite);
        //    }
        //}

        //protected void AutonumberIDGeneretor()
        //{
        //    string connectionstringautonumbergenerator = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
        //    try
        //    {
        //        SQLiteConnection connectionstringtoreadhelpgenerator = new SQLiteConnection(connectionstringautonumbergenerator);
        //        SQLiteCommand cmdtohelpreaddatatogeneratorautoid = new SQLiteCommand();
        //        cmdtohelpreaddatatogeneratorautoid.CommandText = "Select ID from deliveryscan order by ID desc";
        //        cmdtohelpreaddatatogeneratorautoid.Connection = connectionstringtoreadhelpgenerator;

        //        connectionstringtoreadhelpgenerator.Open();

        //        int autonumberidnye = Convert.ToInt32(cmdtohelpreaddatatogeneratorautoid.ExecuteScalar());

        //        try
        //        {
        //            autonumberidnye = autonumberidnye + 1;
        //            autonumbergeneratorToString = autonumberidnye.ToString();
        //        }
        //        catch (Exception excum)
        //        {
        //            MessageBox.Show("There was an error on laksanakanautonumberiddulu Event Handler.", "Alert");
        //            MessageBox.Show(excum.Message + excum.Source + excum.StackTrace + excum.TargetSite, "Alert");
        //        }
        //    }
        //    catch (Exception alertautonumbergenerator)
        //    {
        //        MessageBox.Show(alertautonumbergenerator.Message + alertautonumbergenerator.Source + alertautonumbergenerator.StackTrace + alertautonumbergenerator.TargetSite);
        //    }
        //}

        protected void laksanakanperintahsimpanbaru()
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                //string arrivaltimeString = arrivaltimeDT.ToString("dd.MM.yyyy hh:mm:ss");
                string insertthesqldata;
                insertthesqldata = @"insert into DeliveryScan (
                                        DeliveryNo, LogisticPoint, ArrivalTime)
                                        values (
                                        ?, ?, ?)";
               
                int added = 0;
                try
                {
                    
                    using (SQLiteConnection con = new SQLiteConnection(cs))
                    {
                        con.Open();
                        using (SQLiteTransaction mytransaction = con.BeginTransaction())
                        {
                            using (SQLiteCommand cmd = new SQLiteCommand(con))
                            {
                                SQLiteParameter deliveryNo = new SQLiteParameter();
                                SQLiteParameter logisticPoint = new SQLiteParameter();
                                SQLiteParameter arrivalTime = new SQLiteParameter();

                                deliveryNo.DbType = DbType.String;
                                deliveryNo.Size = 13;
                                deliveryNo.Value = txtDeliveryNo.Text;

                                logisticPoint.DbType = DbType.String;
                                logisticPoint.Size = 13;
                                logisticPoint.Value = txtDOckCode.Text;

                                arrivalTime.DbType = DbType.DateTime;
                                arrivalTime.Value = DateTime.Now;


                                cmd.CommandText = insertthesqldata;
                                cmd.Parameters.Add(deliveryNo);
                                cmd.Parameters.Add(logisticPoint);
                                cmd.Parameters.Add(arrivalTime);

                                cmd.ExecuteNonQuery();
                            }
                            mytransaction.Commit();
                        } 
                        con.Close();
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("input data gagal dilakukan, coba cek koding insert anda atau hubungi master developer anda");
                    MessageBox.Show(ex.Message + ex.Source + ex.StackTrace + ex.TargetSite);
                }
                finally
                {
                    txtDOckCode.Text = "";
                    txtDeliveryNo.Text = "";
                    gridviewautorefreshdatatableeventhandler();
                }
                if (added > 0)
                {
                    MessageBox.Show("terimakasih atas kerjasamanya");
                }
            }
            catch (Exception excumalertinsertcommand)
            {
                MessageBox.Show(excumalertinsertcommand.Message + excumalertinsertcommand.Source + excumalertinsertcommand.StackTrace + excumalertinsertcommand.TargetSite);
            }
        }

        protected void laksanakanperintahupdate()
        {
           
            string connectionstringforinsertcommanddata = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                //string arrivaltimeString = arrivaltimeDT.ToString("dd.MM.yyyy hh:mm:ss");
                DateTime departureTime = GetArrivalTime(txtDeliveryNo.Text, txtDOckCode.Text);
                string insertthesqldata;
                insertthesqldata = @"UPDATE DeliveryScan SET
                                        DepartureTime='@DepartureTime',
                                        UnloadingTime='@UnloadingTime'
                                        WHERE DeliveryNo='@DeliveryNo' AND LogisticPoint='@LogisticPoint'";

                SQLiteConnection theconnectioninsertbegin = new SQLiteConnection(connectionstringforinsertcommanddata);
                SQLiteCommand commandtostartinsertdata = new SQLiteCommand(insertthesqldata, theconnectioninsertbegin);

                //commandtostartinsertdata.Parameters.AddWithValue("@ID", autonumbergeneratorToString); // koding sampe sini, error dbnya ngelock cuyy.. gele...  
                commandtostartinsertdata.Parameters.AddWithValue("@DeliveryNo", txtDeliveryNo.Text);
                commandtostartinsertdata.Parameters.AddWithValue("@LogisticPoint", txtDOckCode.Text);
                commandtostartinsertdata.Parameters.AddWithValue("@UnloadingTime", DateTime.Now);
                commandtostartinsertdata.Parameters.AddWithValue("@DepartureTime", (departureTime - DateTime.Now).TotalMinutes);

                int added = 0;
                try
                {
                    theconnectioninsertbegin.Open();
                    added = commandtostartinsertdata.ExecuteNonQuery();
                    MessageBox.Show("Input data berhasil dilakukan");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("input data gagal dilakukan, coba cek koding insert anda atau hubungi master developer anda");
                    MessageBox.Show(ex.Message + ex.Source + ex.StackTrace + ex.TargetSite);
                }
                finally
                {
                    theconnectioninsertbegin.Close();
                    txtDOckCode.Text = "";
                    txtDeliveryNo.Text = "";
                    gridviewautorefreshdatatableeventhandler();
                }
                if (added > 0)
                {
                    MessageBox.Show("terimakasih atas kerjasamanya");
                }
            }
            catch (Exception excumalertinsertcommand)
            {
                MessageBox.Show(excumalertinsertcommand.Message + excumalertinsertcommand.Source + excumalertinsertcommand.StackTrace + excumalertinsertcommand.TargetSite);
            }
        }

        private DateTime GetArrivalTime(string deliveryNo, string logisticPoint)
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            DateTime hasil = DateTime.Now;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(cs))
                {
                    con.Open();

                    string sqlQuery = "select * from DeliveryScan";

                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                if (rdr["LogisticPoint"].ToString() == txtDOckCode.Text && rdr["DeliveryNo"].ToString() == txtDeliveryNo.Text)
                                {
                                    hasil = Convert.ToDateTime(rdr["ArrivalTime"]);
                                }
                                else
                                {
                                    hasil = DateTime.Now;
                                }
                            }
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Error while get Arrival Time Data" + Environment.NewLine + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite);
                hasil = DateTime.Now;
            }
            return hasil;
        }

        protected void gridviewautorefreshdatatableeventhandler()
        {
            dataGridView1.DataSource = null;
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(cs))
                {
                    con.Open();

                    string sqlQuery = "select * from DeliveryScan";

                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            using (SQLiteDataReader rdr = cmd.ExecuteReader())
                            {
                                
                                DataTable dtconbindgridview = new DataTable();
                                dtconbindgridview.Load(rdr);

                                dataGridView1.DataSource = dtconbindgridview;
                                dataGridView1.Refresh();

                                //set datagridview size manually
                                //this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                                this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[0].Width = 75;
                                this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[1].Width = 100;
                                this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[2].Width = 85;
                                this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[3].Width = 128;
                                this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[4].Width = 128;
                                this.dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[5].Width = 108;
                                this.dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[6].Width = 97;
                                this.dataGridView1.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                this.dataGridView1.Columns[7].Width = 125;
                                //this.dataGridView1.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                                //this.dataGridView1.Columns[8].Width = 100;

                                // this is for setting for all header column
                                dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                                //dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                                // this is for setting the position of the content alignment position
                                this.dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.dataGridView1.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                //this.dataGridView1.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                                // this is for sorting the gridview column
                                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                                //dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;

                                // this is for sorting the gridview column
                                dataGridView1.Columns[0].ReadOnly = true;
                                dataGridView1.Columns[1].ReadOnly = true;
                                dataGridView1.Columns[2].ReadOnly = true;
                                dataGridView1.Columns[3].ReadOnly = true;
                                dataGridView1.Columns[4].ReadOnly = true;
                                dataGridView1.Columns[5].ReadOnly = true;
                                dataGridView1.Columns[6].ReadOnly = true;
                                dataGridView1.Columns[7].ReadOnly = true;
                                //dataGridView1.Columns[8].ReadOnly = true;
                            }
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Error while load Data" + Environment.NewLine + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite);
            }
        }

        private void txtDeliveryNo_TextChanged(object sender, EventArgs e)
        {
            cmdReceive.Enabled = true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You Are Now Leaving The System, Bye!!", "DCLRecOffMod System");
            FormMain me = new FormMain();
            Close();
        }

        private void timerPing_Tick(object sender, EventArgs e)
        {

        }
    }
}

