﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DCLOffline.Desktop
{
    class BussinessLayer
    {
        DataLayer dal = new DataLayer();

        public DataSet DisplayData()
        {
            return (dal.DisplayData());
        }


        public DataSet InsertData(DataTransmitter DT)
        {
            return (dal.InsertDataArrivalProcess(DT));
        }

        public DataSet SelectedColumnsDisplay(DataTransmitter DT)
        {
            return (dal.SelectedColumnsDisplay(DT));

        }

        internal DataTable DisplatFloatArrayTable()
        {
            return (dal.DisplatFloatArrayTable());

        }

        internal DataSet SelectedDatesDisplay(DataTransmitter DT)
        {
            return (dal.SelectedDatesDisplay(DT));

        }

        internal DataSet DeleteData(DataTransmitter DT)
        {
            return (dal.DeleteData(DT));
        }


        internal DataSet UpdateData(DataTransmitter DT)
        {
            return (dal.UpdateDataDepartureFinal(DT));
            //throw new NotImplementedException();
        }
    }
}
