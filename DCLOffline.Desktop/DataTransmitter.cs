﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DCLOffline.Desktop
{
    class DataTransmitter
    {
        int rows = 0;
        int fromValue = 0;
        int toValue = 0;
        string selectedValue = "";
        string selectedcolumns = "";
        string selectedcolumn = "";
        long fromdate = 0;
        long todate = 0;
        //  int rowDelete=-1;

        public int Rows
        {
            get { return rows; }
            set { rows = value; }
        }


        public int FromValue
        {
            get { return fromValue; }
            set { fromValue = value; }
        }

        public int ToValue
        {
            get { return toValue; }
            set { toValue = value; }
        }


        public string SelectedValue
        {
            get { return selectedValue; }
            set { selectedValue = value; }
        }

        public string SelectedColumns
        {
            get { return selectedcolumns; }
            set { selectedcolumns = value; }
        }


        public string SelectedColumn
        {
            get { return selectedcolumn; }
            set { selectedcolumn = value; }
        }

        public long FromDate
        {
            get { return fromdate; }
            set { fromdate = value; }
        }

        public long ToDate
        {
            get { return todate; }
            set { todate = value; }
        }

        public int RowDelete { get; set; }
        public char ID { get; set; }
        public char DeliveryNo { get; set; }
        public int LogisticPoint { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string ArrivalTimestring { set; get; }
        public DateTime DepartureTime { get; set; }
        public string DepartureTimestring { set; get; }
        public TimeSpan UnloadingTime { get; set; }
        public string UnloadingTimestring { set; get; }
        public string UploadStatus { get; set; }
        public DateTime UploadTime { get; set; }
        public string UploadTimestring { set; get; }
        public string UploadBy { get; set; }
    }
}
