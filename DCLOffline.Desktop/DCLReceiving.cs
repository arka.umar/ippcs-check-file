﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DCLOffline.Desktop
{
    public class DCLReceiving
    {
        public string DeliveryNo { get; set; }
        public string LogisticPoint { get; set; }
        public string ArrivalTime { get; set; }
        public string DepartureTime { get; set; }
        public string UploadStatus { get; set; }        
    }
}
