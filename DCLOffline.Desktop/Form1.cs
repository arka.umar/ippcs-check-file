﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SQLite;
using System.IO;
using System.Collections;
using System.Runtime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Threading;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Principal;
using System.Xml.Serialization;

namespace DCLOffline.Desktop
{
    public partial class Form1 : Form
    {
        #region " Public Declare Library "
        string IPAddressIPPCS = System.Configuration.ConfigurationManager.AppSettings["IPAddressIPPCS"];
        //string autonumbergeneratorToString;
        DateTime arrivaltimeDT = System.DateTime.Now;
        DateTime theresult = System.DateTime.Now;
        int countdown = 0;
        int countpageload = 0;
        int countonline = 0;
        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                // set the autorefresh for the gridview
                gridviewautorefreshdatatableeventhandler();

                // start for the controller
                cmdReceive.Enabled = false;
                timer1.Enabled = true;
                tmLPFocus.Enabled = true;
                simpleButton3.Visible = false;
                txtDOckCode.Focus();
            }
            catch (Exception alertloaderror)
            {
                lblError.Text = "system Load Page eventhandler error!!" + Environment.NewLine + alertloaderror.Message + alertloaderror.Source + alertloaderror.StackTrace + alertloaderror.TargetSite;
                timer3.Enabled = true;
            }
        }
        //private void Form1_Closing(object sender, FormClosingEventArgs e)
        //{
        //    if (e.CloseReason == CloseReason.WindowsShutDown) return;
            
        //    switch (MessageBox.Show("Are you sure you want to exit?", "DCL Receiving Offline", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
        //    {
        //        case DialogResult.No:
        //            e.Cancel = true;
        //            break;
        //        default:
        //            // Return from the FormClosing method
        //            //return;
        //            break;
        //    }
        //}
        //#region " Eventhandler for closing the apps "
        //private void pictureBox1_Click_1(object sender, EventArgs e)
        //{
        //    MessageBox.Show("You Are Now Leaving The System, Bye!!", "IPPCS OFFLINE System");
        //    Form1 me = new Form1();
        //    Close();
        //}

        //private void pictureBox1_Click(object sender, EventArgs e)
        //{
        //    MessageBox.Show("You Are Now Leaving The System, Bye!!", "DCLRecOffMod System");
        //    Form1 me = new Form1();
        //    Close();
        //}
        //#endregion

        #region " Timer "
        private void timer1_Tick(object sender, EventArgs e)
        {
            // this is eventhandler to show running current timer tjeh
            lblcurrtime.Text = System.DateTime.Now.ToString("hh:mm:ss");
            countpageload = countpageload + 1;
            // here is the ping to IPPCS Server eventhandler start
            try
            {
                // here we declare the function parameter and that will become paramstrig in latter
                if (PingNetwork(IPAddressIPPCS))
                {
                    countonline = 1;
                    label4.Text = "IPPCS IS ONLINE";
                    pictureBox1.BackColor = Color.Green;
                    label4.BackColor = Color.Green;

                    // after that, system automatically
                    txtDOckCode.Enabled = false;
                    txtDeliveryNo.Enabled = false;
                    cmdReceive.Enabled = false;
                    cmdRefresh.Enabled = false;
                    btnEmailNotification.Enabled = true;
                    //txtDOckCode.Focus();
                    if (countdown < 0) countdown = 18;
                    tmCountDown.Enabled = true;
                    timer2.Enabled = true;                    
                }
                else
                {
                    label4.Text = "IPPCS IS OFFLINE";
                    pictureBox1.BackColor = Color.Red;
                    label4.BackColor = Color.Red;
                    if (lblError.Text.Contains("Uploading")) lblError.Text = "";

                    txtDOckCode.Enabled = true;
                    txtDeliveryNo.Enabled = true;
                    cmdReceive.Enabled = true;
                    cmdRefresh.Enabled = true;
                    btnEmailNotification.Enabled = false;
                    tmCountDown.Enabled = false;
                    countdown = 0;
                    timer2.Enabled = false;
                    if (countpageload == 1 || countonline == 1)
                    {
                        txtDOckCode.Focus();
                        countonline = 0;
                    }
                }

            }
            catch (Exception alerterror)
            {
                lblError.Text = alerterror.Message;
                timer3.Enabled = true;
            }
        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            Thread thrRefresh = new Thread(new ThreadStart(RefreshData));
            thrRefresh.Start();
            timer1.Enabled = false;
            timer2.Enabled = false;
            countpageload = 0;
        }
        private void timer3_Tick(object sender, EventArgs e)
        {
            lblError.Text = "";
            timer3.Enabled = false;
        }
        private void tmCountDown_Tick(object sender, EventArgs e)
        {
            lblError.Text = "Uploading data in.." + countdown.ToString() + " second";
            countdown = countdown - 1;
        }
        private void tmLPFocus_Tick(object sender, EventArgs e)
        {
            txtDOckCode.Text = "";
            txtDeliveryNo.Text = "";
            txtDOckCode.Focus();
            //tmLPFocus.Enabled = false;
        }
        private bool PingNetwork(string hostNameOrAddress)
        {
            bool pingStatus = false;

            using (Ping p = new Ping())
            {
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 120;

                try
                {
                    PingReply reply = p.Send(hostNameOrAddress, timeout, buffer);
                    pingStatus = (reply.Status == IPStatus.Success);
                }
                catch (Exception)
                {
                    pingStatus = false;
                }
            }

            return pingStatus;
        }
        #endregion

        #region " private bool CheckDeliveryNoAndLogisticPoint(string deliveryNo, string LogisticPoint) "
        private bool CheckDeliveryNoAndLogisticPoint(string deliveryNo, string LogisticPoint)
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            bool checkresultDeliveryNoAndLogisticPoint = false;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(cs))
                {
                    con.Open();

                    string sqlQuery = "select * from DeliveryScan WHERE DeliveryNo = ? AND LogisticPoint = ?";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter delNo = new SQLiteParameter();
                        SQLiteParameter logisticPoint = new SQLiteParameter();

                        delNo.DbType = DbType.String;
                        delNo.Size = 13;
                        delNo.Value = txtDeliveryNo.Text.ToUpper();

                        logisticPoint.DbType = DbType.String;
                        logisticPoint.Size = 13;
                        logisticPoint.Value = txtDOckCode.Text.ToUpper();

                        cmd.Parameters.Add(delNo);
                        cmd.Parameters.Add(logisticPoint);
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read())
                            {
                                if (rdr["DepartureTime"].ToString() != "" && rdr["UnloadingTime"].ToString() != "")
                                {
                                    lblError.Text = "Already Scanned Depart";
                                    timer3.Enabled = true;
                                    gridviewautorefreshdatatableeventhandler();
                                    txtDOckCode.Focus();
                                    return false;
                                }
                                else if (rdr["DepartureTime"].ToString() == "" && rdr["UnloadingTime"].ToString() == "")
                                {
                                    checkresultDeliveryNoAndLogisticPoint = true;
                                }
                            }
                            else
                            {
                                checkresultDeliveryNoAndLogisticPoint = false;
                            }
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                timer3.Enabled = true;
                checkresultDeliveryNoAndLogisticPoint = false;
            }
            return checkresultDeliveryNoAndLogisticPoint;
        }
        private bool ValidateDelivery(string deliveryNo, string LogisticPoint)
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(cs))
                {
                    con.Open();

                    string sqlQuery = "select * from DeliveryScan WHERE DeliveryNo = ? AND LogisticPoint = ?";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter delNo = new SQLiteParameter();
                        SQLiteParameter logisticPoint = new SQLiteParameter();

                        delNo.DbType = DbType.String;
                        delNo.Size = 13;
                        delNo.Value = txtDeliveryNo.Text.ToUpper();

                        logisticPoint.DbType = DbType.String;
                        logisticPoint.Size = 13;
                        logisticPoint.Value = txtDOckCode.Text.ToUpper();

                        cmd.Parameters.Add(delNo);
                        cmd.Parameters.Add(logisticPoint);
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read())
                            {
                                if (rdr["ArrivalTime"].ToString() != "")
                                {
                                    TimeSpan duration = DateTime.Now.Subtract(DateTime.ParseExact(rdr["ArrivalTime"].ToString(), "dd.MM.yyyy HH:mm:ss", null));

                                    if (duration.TotalSeconds <= 120)
                                    {
                                        lblError.Text = "Gap between Scan Arrival and Departure is 2 minutes, please wait!!";
                                        timer3.Enabled = true;
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                timer3.Enabled = true;
                return false;
            }
            return true;
        }
        #endregion

        #region " Eventhandler "
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ReceiveData();
            txtDOckCode.Text = "";
            txtDeliveryNo.Text = "";
            txtDOckCode.Focus();
        }
        private void cmdRefresh_Click(object sender, EventArgs e)
        {
            //Thread thrRefresh = new Thread(new ThreadStart(RefreshData));
            //thrRefresh.Start();
            txtDeliveryNo.Text = "";
            txtDOckCode.Text = "";
            lblError.Text = "";
            txtDOckCode.Focus();
            gridviewautorefreshdatatableeventhandler();
        }
        private void btnEmailNotification_Click(object sender, EventArgs e)
        {
            btnEmailNotification.Enabled = false;
            try
            {
                Thread thrEmail = new Thread(new ThreadStart(SendEmail));
                thrEmail.Start();
            }
            catch (Exception alertautosystemupload)
            {
                lblError.Text = "Error while System try to send email notification :" + Environment.NewLine + alertautosystemupload.Message + alertautosystemupload.Source + alertautosystemupload.StackTrace + alertautosystemupload.TargetSite;
            }
        }
        #endregion

        #region " private DateTime GetArrivalTime(string deliveryNo, string logisticPoint) "
        private DateTime GetArrivalTime(string deliveryNo, string logisticPoint)
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            //DateTime hasil = System.DateTime.Now;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(cs))
                {
                    con.Open();

                    string sqlQuery = "select ArrivalTime from DeliveryScan WHERE DeliveryNo=? AND LogisticPoint=?";
                    string arrivalTime = "";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter paramdeliveryNo = new SQLiteParameter();
                        SQLiteParameter paramlogisticPoint = new SQLiteParameter();

                        paramdeliveryNo.DbType = DbType.String;
                        paramdeliveryNo.Size = 13;
                        paramdeliveryNo.Value = deliveryNo.ToUpper();

                        paramlogisticPoint.DbType = DbType.String;
                        paramlogisticPoint.Size = 13;
                        paramlogisticPoint.Value = logisticPoint.ToUpper();

                        cmd.Parameters.Add(paramdeliveryNo);
                        cmd.Parameters.Add(paramlogisticPoint);

                        arrivalTime = cmd.ExecuteScalar().ToString();
                        theresult = DateTime.ParseExact(arrivalTime, "dd.MM.yyyy HH:mm:ss", null);
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(@"Error while get Arrival Time Data" + Environment.NewLine + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite);
                //theresult = System.DateTime.Now;
                throw ex;
            }
            return theresult;
        }
        #endregion

        #region " All Transactional Eventhandler "
        protected void ReceiveData()
        {
            lblError.Text = "";
            string connectionstringtofirstsavingdataprocess = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                if (string.IsNullOrEmpty(txtDOckCode.Text))
                {
                    lblError.Text = "Logistic Point required";
                    timer3.Enabled = true;
                    return;
                }
                else if (string.IsNullOrEmpty(txtDeliveryNo.Text))
                {
                    lblError.Text = "Delivery No required";
                    timer3.Enabled = true;
                    return;
                }
                else if (txtDOckCode.TextLength < 2)
                {
                    lblError.Text = "Logistic Point at least have 2 characters";
                    timer3.Enabled = true;
                    return;
                }
                else if (txtDeliveryNo.TextLength < 13)
                {
                    lblError.Text = "Delivery No at least have 13 characters";
                    timer3.Enabled = true;
                    return;
                }
                else
                {
                    bool ifExist = CheckDeliveryNoAndLogisticPoint(txtDeliveryNo.Text.Trim().ToUpper(), txtDOckCode.Text.Trim().ToUpper());
                    bool validate = ValidateDelivery(txtDeliveryNo.Text.Trim().ToUpper(), txtDOckCode.Text.Trim().ToUpper());

                    if (validate)
                    {
                        if (ifExist == true)
                        {
                            doUpdate();
                        }
                        else if (ifExist == false)
                        {
                            doinsert();
                        }
                    }
                }
            }
            catch (Exception excumalertfirstsave)
            {
                lblError.Text = excumalertfirstsave.Message + excumalertfirstsave.Source + excumalertfirstsave.StackTrace + excumalertfirstsave.TargetSite;
                timer3.Enabled = true;
            }
            finally
            {
                txtDOckCode.Text = "";
                txtDeliveryNo.Text = "";
                txtDOckCode.Focus();
            }
        }
        protected void doUpdate()
        {
            string conupdt = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                //using (SQLiteConnection concheck = new SQLiteConnection(conupdt))
                //{
                //    concheck.Open();

                //    string sqlQuery = "select * from DeliveryScan";

                //    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, concheck))
                //    {
                //        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                //        {
                //            while (rdr.Read())
                //            {
                //                //if (rdr["DepartureTime"].ToString() != "")
                //                //{
                //                //    MessageBox.Show("You has enter the data for once, it not alloance for entering twice data at present day", "IPPCS OFFLINE MODE");
                                    
                //                //}
                //                //else if (rdr["DepartureTime"].ToString() == "")
                //                //{
                                    
                //                //}

                //            }
                //        }
                //    }
                //    concheck.Close();
                //}

                string insertthesqldata;
                insertthesqldata = @"UPDATE DeliveryScan SET
                                        DepartureTime=?,
                                        UnloadingTime=?
                                        WHERE DeliveryNo=? AND LogisticPoint=?";

                int added = 0;
                try
                {
                    using (SQLiteConnection con = new SQLiteConnection(conupdt))
                    {
                        con.Open();
                        using (SQLiteTransaction mytransaction = con.BeginTransaction())
                        {
                            using (SQLiteCommand cmd = new SQLiteCommand(con))
                            {
                                SQLiteParameter deliveryNo = new SQLiteParameter();
                                SQLiteParameter logisticPoint = new SQLiteParameter();
                                SQLiteParameter departureTime = new SQLiteParameter();
                                SQLiteParameter unloadingTime = new SQLiteParameter();
                                DateTime TmpDepartureTime = System.DateTime.Now;
                                TimeSpan tsresultdiffer = (TmpDepartureTime - GetArrivalTime(txtDeliveryNo.Text.ToUpper(), txtDOckCode.Text.ToUpper()));
                                
                                deliveryNo.DbType = DbType.String;
                                deliveryNo.Size = 13;
                                deliveryNo.Value = txtDeliveryNo.Text.ToUpper();

                                logisticPoint.DbType = DbType.String;
                                logisticPoint.Size = 13;
                                logisticPoint.Value = txtDOckCode.Text.ToUpper();

                                //departureTime.DbType = DbType.DateTime;
                                //departureTime.Value = System.DateTime.Now;
                                departureTime.DbType = DbType.String;
                                departureTime.Value = TmpDepartureTime.ToString("dd.MM.yyyy HH:mm:ss");

                                unloadingTime.DbType = DbType.String;
                                //unloadingTime.Value = string.Format("{0:D2}:{1:D2}", t.Hours, t.Minutes);
                                //unloadingTime.Value = ts.ToString();
                                //unloadingTime.Value = t.ToString();
                                unloadingTime.Value = tsresultdiffer.ToString().Substring(0, 8);
                                //unloadingTime.Value = converterresulttimespan.ToString("hh:mm");
                                //unloadingTime.Value = xdate.ToString("hh:mm");

                                //DateTime convertarrivaltime = Convert.ToDateTime(GetArrivalTime(txtDeliveryNo.Text, txtDOckCode.Text));
                                //DateTime convertdeparture = Convert.ToDateTime(departureTime.Value);
                                ////TimeSpan ts = (System.DateTime.Now - GetArrivalTime(txtDeliveryNo.Text, txtDOckCode.Text));
                                ////TimeSpan ts = System.DateTime.Now - convertarrivaltime;
                                //TimeSpan ts = convertdeparture - convertarrivaltime;
                                //unloadingTime.Value = ts.ToString();

                                cmd.CommandText = insertthesqldata;

                                cmd.Parameters.Add(departureTime);
                                cmd.Parameters.Add(unloadingTime);
                                cmd.Parameters.Add(deliveryNo);
                                cmd.Parameters.Add(logisticPoint);

                                added = cmd.ExecuteNonQuery();
                            }
                            mytransaction.Commit();
                        }
                        con.Close();
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = "Edit data fail!!" + Environment.NewLine + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                    timer3.Enabled = true;
                }
                finally
                {
                    txtDOckCode.Text = "";
                    txtDeliveryNo.Text = "";
                    gridviewautorefreshdatatableeventhandler();
                }
                if (added > 0)
                {
                    //MessageBox.Show("terimakasih atas kerjasamanya");
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while get Arrival Time Data" + Environment.NewLine + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                timer3.Enabled = true;
                gridviewautorefreshdatatableeventhandler();
            }
        }
        protected void doinsert()
        {
            string consinst = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;

            // first we force sistem to make validation, check if the data want to insert has filled or not
            bool systemvalidator = false;

            try
            {
                using (SQLiteConnection constringvalidator = new SQLiteConnection(consinst))
                {
                    constringvalidator.Open();

                    string SQLQuery = "select * from DeliveryScan WHERE DeliveryNo = ? AND LogisticPoint = ?";

                    using (SQLiteCommand cmdvalidator = new SQLiteCommand(SQLQuery, constringvalidator))
                    {
                        SQLiteParameter delno = new SQLiteParameter();
                        SQLiteParameter logpoi = new SQLiteParameter();

                        delno.DbType = DbType.String;
                        delno.Size = 13;
                        delno.Value = txtDeliveryNo.Text.ToUpper();

                        logpoi.DbType = DbType.String;
                        logpoi.Size = 13;
                        logpoi.Value = txtDOckCode.Text.ToUpper();

                        cmdvalidator.Parameters.Add(delno);
                        cmdvalidator.Parameters.Add(logpoi);

                        using (SQLiteDataReader sdrvalidator = cmdvalidator.ExecuteReader())
                        {
                            if (sdrvalidator["DepartureTime"].ToString() != "" && sdrvalidator["UnloadingTime"].ToString() != "")
                            {
                                systemvalidator = true;
                                constringvalidator.Close();
                                gridviewautorefreshdatatableeventhandler();
                                txtDOckCode.Focus();
                            }
                            else
                            {
                                systemvalidator = false;
                                constringvalidator.Close();
                                string insertthesqldata;
                                insertthesqldata = @"insert into DeliveryScan (
                                        DeliveryNo, LogisticPoint, ArrivalTime)
                                        values (
                                        ?, ?, ?)";

                                int added = 0;
                                try
                                {

                                    using (SQLiteConnection con = new SQLiteConnection(consinst))
                                    {
                                        con.Open();
                                        using (SQLiteTransaction mytransaction = con.BeginTransaction())
                                        {
                                            using (SQLiteCommand cmd = new SQLiteCommand(con))
                                            {
                                                SQLiteParameter deliveryNo = new SQLiteParameter();
                                                SQLiteParameter logisticPoint = new SQLiteParameter();
                                                SQLiteParameter arrivalTime = new SQLiteParameter();

                                                deliveryNo.DbType = DbType.String;
                                                deliveryNo.Size = 13;
                                                deliveryNo.Value = txtDeliveryNo.Text.ToUpper();

                                                logisticPoint.DbType = DbType.String;
                                                logisticPoint.Size = 13;
                                                logisticPoint.Value = txtDOckCode.Text.ToUpper();

                                                arrivalTime.DbType = DbType.String;
                                                //arrivalTime.Value = System.DateTime.Now;
                                                arrivalTime.Value = System.DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");

                                                cmd.CommandText = insertthesqldata;
                                                cmd.Parameters.Add(deliveryNo);
                                                cmd.Parameters.Add(logisticPoint);
                                                cmd.Parameters.Add(arrivalTime);

                                                cmd.ExecuteNonQuery();
                                            }
                                            mytransaction.Commit();
                                        }
                                        con.Close();
                                    }

                                }
                                catch (Exception exinsert)
                                {
                                    lblError.Text = "Error while executed doinsert Eventhandler" + Environment.NewLine + exinsert.Message + exinsert.Source + exinsert.StackTrace + exinsert.TargetSite;
                                    timer3.Enabled = true;
                                }
                                finally
                                {
                                    txtDOckCode.Text = "";
                                    txtDeliveryNo.Text = "";
                                    gridviewautorefreshdatatableeventhandler();
                                }
                                if (added > 0)
                                {
                                    //MessageBox.Show("terimakasih atas kerjasamanya");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception alertorformaininserteventhandler)
            {
                lblError.Text = "Error while System try to do the first validation eventhandler on :" + Environment.NewLine + alertorformaininserteventhandler.Message + alertorformaininserteventhandler.Source + alertorformaininserteventhandler.StackTrace + alertorformaininserteventhandler.TargetSite;
                timer3.Enabled = true;
                gridviewautorefreshdatatableeventhandler();
            }            
        }
        #endregion

        #region " gridviewautorefreshdatatableeventhandler() "
        protected void gridviewautorefreshdatatableeventhandler()
        {
            dataGridView1.DataSource = null;
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(cs))
                {
                    con.Open();

                    string sqlQuery = "select * from DeliveryScan order by arrivaltime desc";

                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        using (DataSet ds = new DataSet())
                        {
                            //using (SQLiteDataReader rdr = cmd.ExecuteReader())
                            //{
                            da.Fill(ds);
                            //DataTable dtconbindgridview = new DataTable();
                            //dtconbindgridview.Load(rdr);

                            dataGridView1.DataSource = ds.Tables[0];
                            dataGridView1.Refresh();

                            //set datagridview size manually
                            //this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[0].Width = 100;
                            this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[1].Width = 75;
                            this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[2].Width = 128;
                            this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[3].Width = 128;
                            this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[4].Width = 128;
                            this.dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[5].Width = 108;
                            this.dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[6].Width = 97;
                            this.dataGridView1.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            this.dataGridView1.Columns[7].Width = 125;
                            //this.dataGridView1.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            //this.dataGridView1.Columns[8].Width = 100;

                            // this is for setting for all header column
                            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                            //dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                            // this is for setting the position of the content alignment position
                            this.dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            this.dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            this.dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            this.dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            this.dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            this.dataGridView1.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            this.dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            this.dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            //this.dataGridView1.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                            // this is for sorting the gridview column
                            dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                            //dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;

                            // this is for sorting the gridview column
                            dataGridView1.Columns[0].ReadOnly = true;
                            dataGridView1.Columns[1].ReadOnly = true;
                            dataGridView1.Columns[2].ReadOnly = true;
                            dataGridView1.Columns[3].ReadOnly = true;
                            dataGridView1.Columns[4].ReadOnly = true;
                            dataGridView1.Columns[5].ReadOnly = true;
                            dataGridView1.Columns[6].ReadOnly = true;
                            dataGridView1.Columns[7].ReadOnly = true;
                            //dataGridView1.Columns[8].ReadOnly = true;
                            //}
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while load Data" + Environment.NewLine + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                timer3.Enabled = true;
            }
        }
        #endregion

        #region " private void txtDeliveryNo_TextChanged "
        private void txtDockCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) txtDeliveryNo.Focus();
        }
        private void txtDeliveryNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReceiveData();
                txtDOckCode.Text = "";
                txtDeliveryNo.Text = "";
                txtDOckCode.Focus();
            }
        }
        private void txtDeliveryNo_TextChanged(object sender, EventArgs e)
        {
            cmdReceive.Enabled = true;
        }
        #endregion

        protected void Autosystemupload()
        {
            try
            {
                Thread thrRefresh = new Thread(new ThreadStart(RefreshData));
                thrRefresh.Start();
            }
            catch (Exception alertautosystemupload)
            {
                lblError.Text = "Error while System try to Upload the Data onlocal into IPPCS Data Server on :" + Environment.NewLine + alertautosystemupload.Message + alertautosystemupload.Source + alertautosystemupload.StackTrace + alertautosystemupload.TargetSite;
                timer3.Enabled = true;
                gridviewautorefreshdatatableeventhandler();
            }
        }

        /// <summary>
        /// Refresh data asynchronous
        /// </summary>
        private void RefreshData()
        {
            string Status="";
            tmCountDown.Enabled = false;
            countdown = 0;
            try
            {
                if (this.InvokeRequired)
                {
                    ThreadStart refresh = new ThreadStart(RefreshData);
                    this.Invoke(refresh, new object[] { });
                }
                else
                {
                    //// do something while thread running
                    string consinst = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
                    List<DCLReceiving> lstDCL = new List<DCLReceiving>();

                    using (WSIPPCS.ServiceSoapClient service = new WSIPPCS.ServiceSoapClient())
                    {
                        using (SQLiteConnection constringvalidator = new SQLiteConnection(consinst))
                        {
                            constringvalidator.Open();

                            string SQLQuery = "select DeliveryNo,LogisticPoint,ArrivalTime,DepartureTime,ifnull(UploadStatus,'') as UploadStatus from DeliveryScan WHERE ifnull(UploadStatus,'') in('','Partial')";
                            
                            using (SQLiteCommand cmdvalidator = new SQLiteCommand(SQLQuery, constringvalidator))
                            {
                                WSIPPCS.ArrayOfString dataupdate = new WSIPPCS.ArrayOfString();
                                WSIPPCS.ArrayOfString data = new WSIPPCS.ArrayOfString();
                                Nullable<DateTime> DEPARTURE_ACTUAL_DT;
                                int i = 0;
                        
                                using (SQLiteDataReader sdrvalidator = cmdvalidator.ExecuteReader())
                                {
                                    while (sdrvalidator.Read())
                                    {
                                        DCLReceiving DCL = new DCLReceiving();
                                        DCL.DeliveryNo = sdrvalidator["DeliveryNo"].ToString();
                                        DCL.LogisticPoint = sdrvalidator["LogisticPoint"].ToString();
                                        DCL.ArrivalTime = sdrvalidator["ArrivalTime"].ToString();
                                        DCL.DepartureTime = sdrvalidator["DepartureTime"].ToString();
                                        DCL.UploadStatus = sdrvalidator["UploadStatus"].ToString();
                                        lstDCL.Add(DCL);
                                    }
                                }

                                if (lstDCL.Any())
                                {
                                    foreach (var dcl in lstDCL)
                                    {
                                        if (i % 5 == 0) Application.DoEvents();
                                        dataupdate = new WSIPPCS.ArrayOfString();
                                        dataupdate.AddRange(new string[] { dcl.DeliveryNo.ToString(), dcl.LogisticPoint.ToString(), 
                                                                    DateTime.ParseExact(dcl.ArrivalTime,"dd.MM.yyyy HH:mm:ss",null).ToString(), 
                                                                    (dcl.DepartureTime == "" ? "" : DateTime.ParseExact(dcl.DepartureTime,"dd.MM.yyyy HH:mm:ss",null).ToString()), 
                                                                    WindowsIdentity.GetCurrent().Name });

                                        data = new WSIPPCS.ArrayOfString();
                                        data.AddRange(new string[] { dcl.DeliveryNo.ToString(), dcl.LogisticPoint.ToString() });

                                        if (dcl.UploadStatus.ToString() == "")
                                        {
                                            if (dcl.DepartureTime.ToString() == "")
                                            {
                                                service.UpdateDCLOffline(dataupdate);
                                                Status = "Partial";
                                                // system start exec update for local sqlite db for update upload status
                                                updatesuccessuploadstatustolocaltable(dcl.DeliveryNo.ToString(), dcl.LogisticPoint.ToString(), Status);
                                            }
                                            else
                                            {
                                                service.UpdateDCLOffline(dataupdate);
                                                Status = "OK";
                                                updatesuccessuploadstatustolocaltable(dcl.DeliveryNo.ToString(), dcl.LogisticPoint.ToString(), Status);
                                            }
                                        }
                                        else
                                        {
                                            if (dcl.DepartureTime.ToString() == "")
                                            {
                                                DEPARTURE_ACTUAL_DT = service.GetDataDCL(data);

                                                if (DEPARTURE_ACTUAL_DT != null && (DateTime.Compare(DEPARTURE_ACTUAL_DT.Value, DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null)) > 0))
                                                {
                                                    Status = "OK";
                                                    updatesuccessuploadstatustolocaltable(dcl.DeliveryNo.ToString(), dcl.LogisticPoint.ToString(), Status);
                                                }
                                            }
                                            else
                                            {
                                                service.UpdateDCLOffline(dataupdate);
                                                Status = "OK";
                                                updatesuccessuploadstatustolocaltable(dcl.DeliveryNo.ToString(), dcl.LogisticPoint.ToString(), Status);
                                            }
                                        }

                                        i++;
                                    }
                                }
                            }
                        }
                        //WSIPPCS.ArrayOfString[] datas = new WSIPPCS.ArrayOfString[dataGridView1.Rows.Count];
                        //int i = 0;
                        //foreach (DataGridViewRow row in this.dataGridView1.Rows)
                        //{
                        //    if (i % 5 == 0) Application.DoEvents();
                        //    datas[i] = new WSIPPCS.ArrayOfString();
                        //    datas[i].AddRange(new string[] { row.Cells[0].Value.ToString(), row.Cells[1].Value.ToString(), 
                        //                                    row.Cells[2].Value.ToString(), row.Cells[3].Value.ToString(), WindowsIdentity.GetCurrent().Name });
                        //    i++;
                        //}
                        //service.UpdateDCLOffline(datas);
                    }
                    //// thread end
                    lblError.Text = "Upload Success";
                    timer1.Enabled = true;
                    timer3.Enabled = true;
                    //Application.Exit();
                }
            }
            catch (Exception alertforuploadeventhandler)
            {
                lblError.Text = "Error while System try to execute the upload main evenhandler  :" + Environment.NewLine + alertforuploadeventhandler.Message + alertforuploadeventhandler.Source + alertforuploadeventhandler.StackTrace + alertforuploadeventhandler.TargetSite;
                timer3.Enabled = true;
                //Application.Exit();
            }
        }
        private void SendEmail()
        {
            try
            {
                if (this.InvokeRequired)
                {
                    ThreadStart email = new ThreadStart(SendEmail);
                    this.Invoke(email, new object[] { });
                }
                else
                {
                    using (WSIPPCS.ServiceSoapClient service = new WSIPPCS.ServiceSoapClient())
                    {
                        WSIPPCS.ArrayOfString data = new WSIPPCS.ArrayOfString();
                        data = new WSIPPCS.ArrayOfString();
                        //data.AddRange(new string[] { dcl.DeliveryNo.ToString(), dcl.LogisticPoint.ToString() });
                        service.SendEmailNotification(data);
                    }
                    //btnEmailNotification.Enabled = true;
                }
            }
            catch (Exception alertforuploadeventhandler)
            {
                lblError.Text = "Error while System try to execute the upload main evenhandler  :" + Environment.NewLine + alertforuploadeventhandler.Message + alertforuploadeventhandler.Source + alertforuploadeventhandler.StackTrace + alertforuploadeventhandler.TargetSite;
            }
        }
        protected void updatesuccessuploadstatustolocaltable(string DeliveryNo, string LogisticPoint, string Status)
        {
            string constringuploadeventhandl = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
            try
            {
                // koding sampe disini om, terusin and selesaikan disini....
                string Queryupdaterlocalupload;
                Queryupdaterlocalupload = @"UPDATE DeliveryScan SET
                                        UploadStatus=?,
                                        UploadTime=?,
                                        UploadBy=?
                                        WHERE DeliveryNo=? AND LogisticPoint=?";

                int added = 0;
                try
                {
                    using (SQLiteConnection construploaderstatus = new SQLiteConnection(constringuploadeventhandl))
                    {
                        construploaderstatus.Open();
                        using (SQLiteTransaction mytransaction = construploaderstatus.BeginTransaction())
                        {
                            using (SQLiteCommand cmd = new SQLiteCommand(construploaderstatus))
                            {
                                SQLiteParameter uploadstatus = new SQLiteParameter();
                                SQLiteParameter uploadtime = new SQLiteParameter();
                                SQLiteParameter uploadby = new SQLiteParameter();
                                SQLiteParameter deliveryno = new SQLiteParameter();
                                SQLiteParameter logisticpoint = new SQLiteParameter();

                                string arrayuploadstatus = Status;
                                string arrayformuploadtime = DateTime.Now.ToString("dd.MM.yyyy HH:mm");
                                string arrayuploadby = WindowsIdentity.GetCurrent().Name;
                                string arraydeliveryno = DeliveryNo.ToUpper();
                                string arraylogisticpoint = LogisticPoint.ToUpper();

                                uploadstatus.DbType = DbType.String;
                                uploadstatus.Size = 13;
                                uploadstatus.Value = arrayuploadstatus;

                                uploadtime.DbType = DbType.String;
                                uploadtime.Size = 13;
                                uploadtime.Value = arrayformuploadtime;

                                uploadby.DbType = DbType.String;
                                uploadby.Size = 14;
                                uploadby.Value = arrayuploadby;

                                logisticpoint.DbType = DbType.String;
                                logisticpoint.Size = 3;
                                logisticpoint.Value = arraylogisticpoint;

                                deliveryno.DbType = DbType.String;
                                deliveryno.Size = 13;
                                deliveryno.Value = arraydeliveryno;

                                cmd.CommandText = Queryupdaterlocalupload;

                                cmd.Parameters.Add(uploadstatus);
                                cmd.Parameters.Add(uploadtime);
                                cmd.Parameters.Add(uploadby);
                                cmd.Parameters.Add(deliveryno);
                                cmd.Parameters.Add(logisticpoint);

                                added = cmd.ExecuteNonQuery();
                            }
                            mytransaction.Commit();
                        }
                        construploaderstatus.Close();
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = "Edit data fail!!" + Environment.NewLine + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                    timer3.Enabled = true;
                }
                finally
                {
                    txtDOckCode.Text = "";
                    txtDeliveryNo.Text = "";
                    gridviewautorefreshdatatableeventhandler();
                }
                if (added > 0)
                {
                    //MessageBox.Show("terimakasih atas kerjasamanya");
                }
            }
            catch (Exception alertforupdatesuccessuploadstatustolocaltable)
            {
                lblError.Text = "Error while System try to update status into local DB eventhandler on  :" + Environment.NewLine + alertforupdatesuccessuploadstatustolocaltable.Message + alertforupdatesuccessuploadstatustolocaltable.Source + alertforupdatesuccessuploadstatustolocaltable.StackTrace + alertforupdatesuccessuploadstatustolocaltable.TargetSite;
                timer3.Enabled = true;
                gridviewautorefreshdatatableeventhandler();
            }
        }
        
        //private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    tmLPFocus.Enabled = true;
        //}
        //private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    tmLPFocus.Enabled = true;
        //}
    }
}
