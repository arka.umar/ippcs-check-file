﻿namespace DCLOffline.Desktop
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelProfessional1 = new UnivercaDotNet.NetUIStyle.LabelProfessional(this.components);
            this.MetroIdle = new UnivercaDotNet.NetUIStyle.ButtonMetro(this.components);
            this.cmdRefresh = new UnivercaDotNet.NetUIStyle.Button();
            this.cmdReceive = new UnivercaDotNet.NetUIStyle.Button();
            this.txtDeliveryNo = new UnivercaDotNet.NetUIStyle.TextBoxFade();
            this.txtDOckCode = new UnivercaDotNet.NetUIStyle.TextBoxFade();
            this.label4 = new UnivercaDotNet.NetUIStyle.Label(this.components);
            this.label3 = new UnivercaDotNet.NetUIStyle.Label(this.components);
            this.label2 = new UnivercaDotNet.NetUIStyle.Label(this.components);
            this.label1 = new UnivercaDotNet.NetUIStyle.Label(this.components);
            this.dataGridView1 = new UnivercaDotNet.NetUIStyle.CustomGrid();
            this.label5 = new System.Windows.Forms.Label();
            this.lblcurrtime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timerPing = new System.Windows.Forms.Timer(this.components);
            this.labelProfessional1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelProfessional1
            // 
            this.labelProfessional1.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.labelProfessional1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProfessional1.BackColor = System.Drawing.Color.Transparent;
            this.labelProfessional1.BackgroundColor = System.Drawing.Color.LavenderBlush;
            this.labelProfessional1.CanvasImage = null;
            this.labelProfessional1.Controls.Add(this.MetroIdle);
            this.labelProfessional1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProfessional1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            this.labelProfessional1.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            this.labelProfessional1.HeaderFont = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProfessional1.HeaderText = "Driver Check List Offline Mode";
            this.labelProfessional1.Image = ((System.Drawing.Image)(resources.GetObject("labelProfessional1.Image")));
            this.labelProfessional1.Location = new System.Drawing.Point(-4, -1);
            this.labelProfessional1.Name = "labelProfessional1";
            this.labelProfessional1.OriginalSize = true;
            this.labelProfessional1.Size = new System.Drawing.Size(993, 132);
            this.labelProfessional1.TabIndex = 0;
            this.labelProfessional1.Text = resources.GetString("labelProfessional1.Text");
            this.labelProfessional1.UseBorder = true;
            // 
            // MetroIdle
            // 
            this.MetroIdle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MetroIdle.BackColor = System.Drawing.Color.Transparent;
            this.MetroIdle.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(143)))), ((int)(((byte)(78)))));
            this.MetroIdle.DescriptionFont = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetroIdle.Font = new System.Drawing.Font("Segoe UI Light", 21.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetroIdle.ForeColor = System.Drawing.Color.White;
            this.MetroIdle.Image = null;
            this.MetroIdle.ImageSize = new System.Drawing.Size(0, 0);
            this.MetroIdle.Location = new System.Drawing.Point(803, 12);
            this.MetroIdle.Name = "MetroIdle";
            this.MetroIdle.Size = new System.Drawing.Size(177, 88);
            this.MetroIdle.TabIndex = 103;
            this.MetroIdle.Text = "Status";
            this.MetroIdle.TextDescription = "IPPCS is Online";
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdRefresh.BackColor = System.Drawing.Color.Transparent;
            this.cmdRefresh.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F);
            this.cmdRefresh.ForeColor = System.Drawing.Color.Black;
            this.cmdRefresh.ImageButtonHot = ((System.Drawing.Bitmap)(resources.GetObject("cmdRefresh.ImageButtonHot")));
            this.cmdRefresh.ImageButtonInactive = ((System.Drawing.Bitmap)(resources.GetObject("cmdRefresh.ImageButtonInactive")));
            this.cmdRefresh.ImageButtonNormal = ((System.Drawing.Bitmap)(resources.GetObject("cmdRefresh.ImageButtonNormal")));
            this.cmdRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cmdRefresh.IntervalFadeOut = 180;
            this.cmdRefresh.Location = new System.Drawing.Point(475, 201);
            this.cmdRefresh.MouseDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(187)))), ((int)(((byte)(111)))));
            this.cmdRefresh.MouseEnterBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(50)))));
            this.cmdRefresh.MouseEnterTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(180)))));
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmdRefresh.Size = new System.Drawing.Size(82, 33);
            this.cmdRefresh.TabIndex = 107;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.UseVisualStyleBackColor = false;
            // 
            // cmdReceive
            // 
            this.cmdReceive.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdReceive.BackColor = System.Drawing.Color.Transparent;
            this.cmdReceive.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F);
            this.cmdReceive.ForeColor = System.Drawing.Color.Black;
            this.cmdReceive.ImageButtonHot = ((System.Drawing.Bitmap)(resources.GetObject("cmdReceive.ImageButtonHot")));
            this.cmdReceive.ImageButtonInactive = ((System.Drawing.Bitmap)(resources.GetObject("cmdReceive.ImageButtonInactive")));
            this.cmdReceive.ImageButtonNormal = ((System.Drawing.Bitmap)(resources.GetObject("cmdReceive.ImageButtonNormal")));
            this.cmdReceive.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cmdReceive.IntervalFadeOut = 180;
            this.cmdReceive.Location = new System.Drawing.Point(379, 201);
            this.cmdReceive.MouseDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(187)))), ((int)(((byte)(111)))));
            this.cmdReceive.MouseEnterBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(50)))));
            this.cmdReceive.MouseEnterTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(180)))));
            this.cmdReceive.Name = "cmdReceive";
            this.cmdReceive.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmdReceive.Size = new System.Drawing.Size(82, 33);
            this.cmdReceive.TabIndex = 105;
            this.cmdReceive.Text = "Receive";
            this.cmdReceive.UseVisualStyleBackColor = false;
            // 
            // txtDeliveryNo
            // 
            this.txtDeliveryNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtDeliveryNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtDeliveryNo.BackColor = System.Drawing.Color.Transparent;
            this.txtDeliveryNo.BorderColorActive = System.Drawing.Color.Maroon;
            this.txtDeliveryNo.BorderColorInactive = System.Drawing.Color.LightSalmon;
            this.txtDeliveryNo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeliveryNo.Location = new System.Drawing.Point(165, 195);
            this.txtDeliveryNo.MaxLength = 32767;
            this.txtDeliveryNo.Name = "txtDeliveryNo";
            this.txtDeliveryNo.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.txtDeliveryNo.ReadOnly = false;
            this.txtDeliveryNo.Size = new System.Drawing.Size(188, 46);
            this.txtDeliveryNo.TabIndex = 104;
            // 
            // txtDOckCode
            // 
            this.txtDOckCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtDOckCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtDOckCode.BackColor = System.Drawing.Color.Transparent;
            this.txtDOckCode.BorderColorActive = System.Drawing.Color.Maroon;
            this.txtDOckCode.BorderColorInactive = System.Drawing.Color.LightSalmon;
            this.txtDOckCode.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDOckCode.Location = new System.Drawing.Point(165, 148);
            this.txtDOckCode.MaxLength = 32767;
            this.txtDOckCode.Name = "txtDOckCode";
            this.txtDOckCode.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.txtDOckCode.ReadOnly = false;
            this.txtDOckCode.Size = new System.Drawing.Size(128, 46);
            this.txtDOckCode.TabIndex = 103;
            // 
            // label4
            // 
            this.label4.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.FontColor1 = System.Drawing.Color.Lavender;
            this.label4.FontColor2 = System.Drawing.Color.Black;
            this.label4.FontLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.label4.HeightLine = 0;
            this.label4.LabelStyle = UnivercaDotNet.NetUIStyle.Label.LabelStyleDesigner.Line;
            this.label4.LineColor1 = System.Drawing.Color.Navy;
            this.label4.LineColor2 = System.Drawing.Color.Indigo;
            this.label4.LineLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.label4.Location = new System.Drawing.Point(146, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 28);
            this.label4.TabIndex = 108;
            this.label4.Text = ":";
            this.label4.UseFadeEffects = false;
            // 
            // label3
            // 
            this.label3.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.FontColor1 = System.Drawing.Color.Lavender;
            this.label3.FontColor2 = System.Drawing.Color.Black;
            this.label3.FontLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.label3.HeightLine = 0;
            this.label3.LabelStyle = UnivercaDotNet.NetUIStyle.Label.LabelStyleDesigner.Line;
            this.label3.LineColor1 = System.Drawing.Color.Navy;
            this.label3.LineColor2 = System.Drawing.Color.Indigo;
            this.label3.LineLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.label3.Location = new System.Drawing.Point(146, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 28);
            this.label3.TabIndex = 106;
            this.label3.Text = ":";
            this.label3.UseFadeEffects = false;
            // 
            // label2
            // 
            this.label2.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.FontColor1 = System.Drawing.Color.Lavender;
            this.label2.FontColor2 = System.Drawing.Color.Black;
            this.label2.FontLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.label2.HeightLine = 0;
            this.label2.LabelStyle = UnivercaDotNet.NetUIStyle.Label.LabelStyleDesigner.Line;
            this.label2.LineColor1 = System.Drawing.Color.Navy;
            this.label2.LineColor2 = System.Drawing.Color.Indigo;
            this.label2.LineLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.label2.Location = new System.Drawing.Point(17, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 28);
            this.label2.TabIndex = 110;
            this.label2.Text = "Delivery No";
            this.label2.UseFadeEffects = false;
            // 
            // label1
            // 
            this.label1.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.FontColor1 = System.Drawing.Color.Lavender;
            this.label1.FontColor2 = System.Drawing.Color.Black;
            this.label1.FontLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.label1.HeightLine = 0;
            this.label1.LabelStyle = UnivercaDotNet.NetUIStyle.Label.LabelStyleDesigner.Line;
            this.label1.LineColor1 = System.Drawing.Color.Navy;
            this.label1.LineColor2 = System.Drawing.Color.Indigo;
            this.label1.LineLinierMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.label1.Location = new System.Drawing.Point(17, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 28);
            this.label1.TabIndex = 109;
            this.label1.Text = "Logistic Point";
            this.label1.UseFadeEffects = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(17, 247);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 25;
            this.dataGridView1.Size = new System.Drawing.Size(956, 227);
            this.dataGridView1.TabIndex = 111;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(828, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 16);
            this.label5.TabIndex = 112;
            this.label5.Text = "Curent Time";
            // 
            // lblcurrtime
            // 
            this.lblcurrtime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblcurrtime.AutoSize = true;
            this.lblcurrtime.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcurrtime.Location = new System.Drawing.Point(776, 190);
            this.lblcurrtime.Name = "lblcurrtime";
            this.lblcurrtime.Size = new System.Drawing.Size(186, 38);
            this.lblcurrtime.TabIndex = 113;
            this.lblcurrtime.Text = "Timerush";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            // 
            // timerPing
            // 
            this.timerPing.Enabled = true;
            this.timerPing.Interval = 1000;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(985, 486);
            this.ControlBox = false;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblcurrtime);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmdRefresh);
            this.Controls.Add(this.cmdReceive);
            this.Controls.Add(this.txtDeliveryNo);
            this.Controls.Add(this.txtDOckCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelProfessional1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DCL Offline";
            this.labelProfessional1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UnivercaDotNet.NetUIStyle.LabelProfessional labelProfessional1;
        private UnivercaDotNet.NetUIStyle.ButtonMetro MetroIdle;
        private UnivercaDotNet.NetUIStyle.Button cmdRefresh;
        private UnivercaDotNet.NetUIStyle.Button cmdReceive;
        private UnivercaDotNet.NetUIStyle.TextBoxFade txtDeliveryNo;
        private UnivercaDotNet.NetUIStyle.TextBoxFade txtDOckCode;
        private UnivercaDotNet.NetUIStyle.Label label4;
        private UnivercaDotNet.NetUIStyle.Label label3;
        private UnivercaDotNet.NetUIStyle.Label label2;
        private UnivercaDotNet.NetUIStyle.Label label1;
        private UnivercaDotNet.NetUIStyle.CustomGrid dataGridView1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblcurrtime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timerPing;
    }
}