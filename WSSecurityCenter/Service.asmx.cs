﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using Toyota.Common.Web.Cryptography;
using Toyota.BussinesLayer;
using Toyota.Common.Web.Credential;

namespace WSSecurityCenter
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        private bool IsRequestValid(byte[] Token)
        {
            byte[] validToken;
            byte[] requestToken;
            bool notValid = false;

            ReleaseSNK();
            using (FileStream fs = new FileStream(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.RelativeSearchPath) + "\\scenter.snk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (SHA512Hash hash = new SHA512Hash())
                {
                    validToken = hash.ComputeHash(fs);
                }
            }
            using (MemoryStream ms = new MemoryStream(Token))
            {
                using (SHA512Hash hash = new SHA512Hash())
                {
                    requestToken = hash.ComputeHash(ms);
                }
            }
            if (requestToken.Length == validToken.Length)
            {
                for (int c = 0; c < requestToken.Length - 1; c++)
                {
                    if (validToken[c] != requestToken[c])
                    {
                        notValid = true;
                        break;
                    }
                }
            }

            if (notValid == true)
                return false;
            else
                return true;
        }

        internal bool ReleaseSNK()
        {
            bool close = false;
            IEnumerator<Dictionary<IntPtr, FileInfo>> list = VmcController.Services.DetectOpenFiles.GetOpenFilesEnumerator(System.Diagnostics.Process.GetCurrentProcess().Id);
            while (list.MoveNext())
            {
                var ret = list.Current.Values.Where(c => c.FullName.Contains("scenter.snk"));
                if (ret.Any() && ret != null)
                {
                    close = VmcController.Services.NativeMethods.CloseHandle(list.Current.Keys.ToList()[0]);
                    break;
                }
            }
            list.Dispose();
            return close;
        }

        [WebMethod]
        public bool Login(string UserName, string Password)
        {
            //if (IsRequestValid(Token))
                return Authentication.Login(UserName, Password);
            //else
            //    return false;
        }

        [WebMethod]
        public string getRole(string UserName, string Password, string App)
        {
            //if (IsRequestValid(Token))
                return Authentication.getRole(UserName, Password,App);
            //else
            //    return null;
        }

        [WebMethod]
        public List<string> appList(string UserName, string Password)
        {
            return Authentication.appList(UserName,Password);
        }

        [WebMethod]
        public bool LogOut(string UserName, string Password)
        {
            return false;
        }

        [WebMethod]
        public string getEmail(string UserName, string Password, List<string> usernames)
        {
            //if (IsRequestValid(Token))
                return Authentication.getEmail(UserName, Password, usernames);
            //else
            //    return null;
        }

        [WebMethod]
        public bool regCreate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            //if (IsRequestValid(Token))
                return Authentication.regCreate(UserName, Password, CompanyName, Address, CompanyAddress, Email, ContactPerson, Telephone, Faximile, MobilePhone);
            //else
            //    return false;
        }

        [WebMethod]
        public bool regUpdate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            //if (IsRequestValid(Token))
                return Authentication.regUpdate(UserName, Password, CompanyName, Address, CompanyAddress, Email, ContactPerson, Telephone, Faximile, MobilePhone);
            //else
            //    return false;
        }

        [WebMethod]
        public bool regDelete(string UserName, string Password, string UserEmail)
        {
            //if (IsRequestValid(Token))
                return Authentication.regDelete(UserName, Password, UserEmail);
            //else
            //    return false;
        }

        [WebMethod]
        public bool regIsExist(string UserName, string Password, string UserEmail)
        {
            //if (IsRequestValid(Token))
                return Authentication.regIsExist(UserName, Password, UserEmail);
            //else
            //    return false;
        }

        [WebMethod]
        public string genNewUser(string UserName, string Password, string Email)
        {
            //if (IsRequestValid(Token))
                return Authentication.genNewUser(UserName, Password, Email);
            //else
            //    return null;
        }

        [WebMethod]
        public string genNewPass(string UserName, string Password, string Email)
        {
            //if (IsRequestValid(Token))
                return Authentication.genNewPass(UserName, Password, Email);
            //else
            //    return null;
        }

        [WebMethod]
        public bool IsDefaultAppExist(string UserName, string Password, string UserEmail)
        {
            //if (IsRequestValid(Token))
                return Authentication.IsDefaultAppExist(UserName, Password, UserEmail);
            //else
            //    return false;
        }

        [WebMethod]
        public bool UpdateDefaultApp(string UserName, string Password, string DefaultApplication)
        {
            //if (IsRequestValid(Token))
                return Authentication.UpdateDefaultApp(UserName, Password,DefaultApplication);
            //else
            //    return false;
        }

        [WebMethod]
        public string getDefaultApp(string UserName, string Password)
        {
            //if (IsRequestValid(Token))
            return Authentication.getDefaultApp(UserName, Password);
            //else
            //    return null;
        }

    }
}