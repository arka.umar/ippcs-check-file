﻿using TransferPostingIPPCStoICS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransferPostingIPPCStoICS.Helper.Base
{
    public abstract class BaseBatch : BaseRepository<Common>
    {
        public abstract void ExecuteBatch();
    }

    public abstract class BaseBatchParam
    {
        public abstract void ExecuteBatch(string[] batchParams);
    }
}
