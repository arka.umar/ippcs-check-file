﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.FTP;
using Ionic.Zip;
using System.IO;
using System.Net.Mail;

namespace TelerikReportGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            long ProcessID;
            int SeqNoLog = 1;
            int ProcStat = 0;
            string Location = "Generate PDF Report";
            string MessageLog = "";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("select dbo.fn_get_last_process_id()", conn))
                {
                    ProcessID = Convert.ToInt64(cmd.ExecuteScalar());
                }
                InsertLogHeader(conn, ProcessID, "21003", "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);
                using (SqlCommand cmd = new SqlCommand("select ORDER_RELEASE_DT,SUPPLIER_CD,SUPPLIER_PLANT_CD,ORDER_TYPE from TB_R_DAILY_ORDER_WEB WHERE FTP_FLAG='0' AND ORDER_TYPE='2'", conn))
                {
                    SqlDataReader dr;
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        string FTPFolderRoot = "";
                        string[] filePathManifestPrePrinteds = new string[] { };
                        string[] filePathManifestPlains = new string[] { };
                        string[] filePathSkids = new string[] { };
                        string[] filePathKanbans = new string[] { };

                        switch (dr["ORDER_TYPE"].ToString())
                        {
                            case "1":
                                FTPFolderRoot = "\\Portal\\DailyOrderPrinting\\";
                                break;
                            case "2":
                                FTPFolderRoot = "\\Portal\\EmergencyOrder\\";
                                break;
                            case "3":
                                FTPFolderRoot = "\\Portal\\ComponentPartOrder\\";
                                break;
                            case "4":
                                FTPFolderRoot = "\\Portal\\JunbikiOrder\\";
                                break;
                            case "5":
                                FTPFolderRoot = "\\Portal\\ProblemPartOrder\\";
                                break;
                        }
                        try
                        {
                            using (SqlCommand cmd2 = new SqlCommand(@"select MANIFEST_NO,TOTAL_QTY=ISNULL(TOTAL_QTY,0),ORDER_TYPE from TB_R_DAILY_ORDER_MANIFEST WHERE FTP_FLAG='0'" +
                                                            " AND CONVERT(VARCHAR(8),ORDER_RELEASE_DT,112)='" + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMdd") + "'" +
                                                            " AND SUPPLIER_CD='" + dr["SUPPLIER_CD"].ToString() + "'" +
                                                            " AND SUPPLIER_PLANT='" + dr["SUPPLIER_PLANT_CD"].ToString() + "'", conn))
                            {
                                SqlDataReader dr2;
                                dr2 = cmd2.ExecuteReader();
                                while (dr2.Read())
                                {
                                    try
                                    {
                                        //string tes = Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyy-MM-dd hh:mm:ss");
                                        DailyOrderPrint(dr2["MANIFEST_NO"].ToString(), Convert.ToDateTime(dr["ORDER_RELEASE_DT"]), dr["SUPPLIER_CD"].ToString(), dr["SUPPLIER_PLANT_CD"].ToString(), dr2["TOTAL_QTY"].ToString(), dr2["ORDER_TYPE"].ToString());
                                        using (SqlCommand cmd22 = new SqlCommand("UPDATE TB_R_DAILY_ORDER_MANIFEST SET FTP_FLAG='1' WHERE MANIFEST_NO='" + dr2["MANIFEST_NO"].ToString() + "'",conn))
                                        {
                                            cmd22.ExecuteNonQuery();
                                        }
                                        MessageLog = "Manifest No " + dr2["MANIFEST_NO"].ToString() + " is successfully generated PDF report";
                                        InsertLogDetail(conn, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                        if (dr["ORDER_TYPE"].ToString() != "1")
                                        {
                                            if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted"))
                                            {
                                                filePathManifestPrePrinteds = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted");
                                            }
                                            if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain"))
                                            {
                                                filePathManifestPlains = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain");
                                            }
                                            if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "Skid"))
                                            {
                                                filePathSkids = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "Skid");
                                            }
                                            if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban"))
                                            {
                                                filePathKanbans = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban");
                                            }
                                            using (ZipFile zip = new ZipFile())
                                            {
                                                foreach (string fileManifest in filePathManifestPrePrinteds)
                                                {
                                                    zip.AddFile(fileManifest, "ManifestPrePrinted");
                                                }
                                                foreach (string fileManifest in filePathManifestPlains)
                                                {
                                                    zip.AddFile(fileManifest, "ManifestPlain");
                                                }
                                                foreach (string fileSkid in filePathSkids)
                                                {
                                                    zip.AddFile(fileSkid, "Skid");
                                                }
                                                foreach (string fileKanban in filePathKanbans)
                                                {
                                                    zip.AddFile(fileKanban, "Kanban");
                                                }
                                                zip.Save(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + dr2["MANIFEST_NO"].ToString() + ".zip");
                                                zip.Dispose();
                                                MessageLog = "Manifest No " + dr2["MANIFEST_NO"].ToString() + " is successfully generated ZIP report";
                                                InsertLogDetail(conn, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ProcStat = 6;
                                        InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                                        UpdateStatusLog(conn, ProcessID, ProcStat);
                                        SendEmail(ex.Message);
                                    }
                                }
                                dr2.Close();
                            }
                            
                            ////DailyOrderDownload(Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyy-MM-dd hh:mm:ss"), dr["SUPPLIER_CD"].ToString() + "-" + dr["SUPPLIER_PLANT_CD"].ToString());

                            if (dr["ORDER_TYPE"].ToString() == "1")
                            {
                                if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\ManifestPrePrinted"))
                                {
                                    filePathManifestPrePrinteds = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\ManifestPrePrinted");
                                }
                                if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\ManifestPlain"))
                                {
                                    filePathManifestPlains = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\ManifestPlain");
                                }
                                if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\Skid"))
                                {
                                    filePathSkids = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\Skid");
                                }
                                if (Directory.Exists(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\Kanban"))
                                {
                                    filePathKanbans = Directory.GetFiles(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\Kanban");
                                }
                                using (ZipFile zip = new ZipFile())
                                {
                                    foreach (string fileManifest in filePathManifestPrePrinteds)
                                    {
                                        zip.AddFile(fileManifest, "ManifestPrePrinted");
                                    }
                                    foreach (string fileManifest in filePathManifestPlains)
                                    {
                                        zip.AddFile(fileManifest, "ManifestPlain");
                                    }
                                    foreach (string fileSkid in filePathSkids)
                                    {
                                        zip.AddFile(fileSkid, "Skid");
                                    }
                                    foreach (string fileKanban in filePathKanbans)
                                    {
                                        zip.AddFile(fileKanban, "Kanban");
                                    }
                                    zip.Save(ConfigurationManager.AppSettings["FTPPath"] + FTPFolderRoot + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "\\" + dr["SUPPLIER_CD"].ToString() + "\\" + dr["SUPPLIER_PLANT_CD"].ToString() + "\\" + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMddhhmm") + "-" + dr["SUPPLIER_PLANT_CD"].ToString() + dr["SUPPLIER_CD"].ToString() + ".zip");
                                    zip.Dispose();
                                    using (SqlCommand cmd11 = new SqlCommand(@"UPDATE TB_R_DAILY_ORDER_WEB SET FTP_FLAG='1' WHERE CONVERT(VARCHAR(8),ORDER_RELEASE_DT,112)='" + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMdd") + "'" +
                                                                " AND SUPPLIER_CD='" + dr["SUPPLIER_CD"].ToString() + "'" +
                                                                " AND SUPPLIER_PLANT_CD='" + dr["SUPPLIER_PLANT_CD"].ToString() + "'", conn))
                                    {
                                        cmd11.ExecuteNonQuery();
                                    }
                                    MessageLog = "Order Release Date " + Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyyMMdd") + "; Supplier Code " + dr["SUPPLIER_CD"].ToString() + "; Supplier Plant " + dr["SUPPLIER_PLANT_CD"].ToString() + " is successfully generated ZIP report";
                                    InsertLogDetail(conn, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcStat = 6;
                            InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                            UpdateStatusLog(conn, ProcessID, ProcStat);
                            SendEmail(ex.Message);
                        }
                    }
                    dr.Close();
                    ProcStat = 1;
                    InsertLogDetail(conn, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                    UpdateStatusLog(conn, ProcessID, ProcStat);
                }
            }

            using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["PCS"].ConnectionString))
            {
                conn2.Open();
                using (SqlCommand cmd3 = new SqlCommand("select MANIFEST_NO,TOTAL_QTY=ISNULL(TOTAL_QTY,0) from TB_R_DAILY_ORDER_MANIFEST WHERE FTP_FLAG='0' AND PROBLEM_FLAG='1'", conn2))
                {
                    SqlDataReader dr3;
                    dr3 = cmd3.ExecuteReader();
                    while (dr3.Read())
                    {
                        try
                        {
                            DailyOrderPrintProblem(dr3["MANIFEST_NO"].ToString(), dr3["TOTAL_QTY"].ToString());
                            using (SqlCommand cmd33 = new SqlCommand("UPDATE TB_R_DAILY_ORDER_MANIFEST SET FTP_FLAG='1' WHERE MANIFEST_NO='" + dr3["MANIFEST_NO"].ToString() + "'", conn2))
                            {
                                cmd33.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            SendEmail(ex.Message);
                        }
                    }
                    dr3.Close();
                }
            }            
        }
        #region DownloadPDF
        public static void DailyOrderDownload(string OrderDate, string SupplierCode)
        {
            string fileName;

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "yyyy-MM-dd hh:mm:ss", null);
            try
            {
                //Manifest
                fileName = "Manifest_" + OrdDate.ToString("yyyyMMddhhmm") + "_" + Supplier[0] + "_" + Supplier[1] + ".pdf";
                PrintManifestReport(OrdDate, Supplier[0], Supplier[1], fileName);

                //Skid
                fileName = "Skid_" + OrdDate.ToString("yyyyMMddhhmm") + "_" + Supplier[0] + "_" + Supplier[1] + ".pdf";
                PrintSkidReport(OrdDate, Supplier[0], Supplier[1], fileName);

                //Kanban
                fileName = "Kanban_" + OrdDate.ToString("yyyyMMddhhmm") + "_" + Supplier[0] + "_" + Supplier[1] + ".pdf";
                PrintKanbanReport(OrdDate, Supplier[0], Supplier[1], fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void DailyOrderPrint(string ManifestNo, DateTime OrderReleaseDate, string SupplierCode, string SupplierPlant, string TotalQty, string OrderType)
        {
            try
            {
                string FileName = ManifestNo + "-";
                string FolderName = "";
                if (OrderType == "1") FolderName = OrderReleaseDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant;

                PrintManifestReport(ManifestNo, "0", FileName, FolderName,OrderType);
                PrintManifestReport(ManifestNo, "1", FileName, FolderName, OrderType);
                //PrintKanbanSkidReport(ManifestNo, FileName);
                if (TotalQty != "0")
                {
                    PrintSelectionKanbanReport(ManifestNo, FileName, FolderName, OrderType);
                    PrintSkidReport(ManifestNo, FileName, FolderName, OrderType);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void DailyOrderPrintProblem(string ManifestNo, string TotalQty)
        {
            try
            {
                string FileName = ManifestNo + "-";
                string FolderName = "DailyOrderProblemPart";

                PrintManifestReport(ManifestNo, "0", FileName, FolderName,"1");
                PrintManifestReport(ManifestNo, "1", FileName, FolderName, "1");
                //PrintKanbanSkidReport(ManifestNo, FileName);
                if (TotalQty != "0")
                {
                    PrintSelectionKanbanReport(ManifestNo, FileName, FolderName, "1");
                    PrintSkidReport(ManifestNo, FileName, FolderName, "1");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static Telerik.Reporting.Report CreateReport(string reportPath,
            string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = DBContextNames.DB_PCS;
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(System.Configuration.ConfigurationManager.AppSettings["AppPath"] + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }
        private static void SavePDFReportToFTP(Telerik.Reporting.Report rpt, string FileName, string FolderName, string OrderType)
        {
            FileName = FileName + ".pdf";

            FTPUpload vFtp = new FTPUpload(false);
            vFtp.Setting.IP = "10.16.20.73";
            vFtp.Setting.UserID = "uuspcs00";
            vFtp.Setting.Password = "uu$pcs00";
            string msg = "";
            string FTPFolderRoot = "";
            switch (OrderType)
            {
                case "1":
                    FTPFolderRoot = "/Portal/DailyOrderPrinting/";
                    break;
                case "2":
                    FTPFolderRoot = "/Portal/EmergencyOrder/";
                    break;
                case "3":
                    FTPFolderRoot = "/Portal/ComponentPartOrder/";
                    break;
                case "4":
                    FTPFolderRoot = "/Portal/JunbikiOrder/";
                    break;
                case "5":
                    FTPFolderRoot = "/Portal/ProblemPartOrder/";
                    break;
            }
            //if (!vFtp.DirectoryList(vFtp.Setting.FtpPath("DailyOrderPrinting")).Contains(FileName))
            //{
                ReportProcessor reportProcess = new ReportProcessor();
                RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                bool resultUpload = vFtp.FtpUpload(FTPFolderRoot + FolderName, FileName, result.DocumentBytes, ref msg);
                rpt.Dispose();
            //}
        }

        private static void PrintManifestReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@PartNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\Rep_SupplierManifest.trdx", "SP_Rep_DailyOrder_Manifest_Print", parameters);

            FTPUpload vFtp = new FTPUpload(false);
            //vFtp.Setting = new FTPSettings(false);
            vFtp.Setting.IP = "10.16.20.73";
            vFtp.Setting.UserID = "uuspcs00";
            vFtp.Setting.Password = "uu$pcs00";
            string msg = "";

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            bool resultUpload = vFtp.FtpUpload("/Portal/DailyOrder/" + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);
            rpt.Dispose();
        }
        private static void PrintSkidReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\Rep_ManifestSkid_Download.trdx", "SP_Rep_DailyOrder_Skid_Print", parameters);

            FTPUpload vFtp = new FTPUpload(false);
            //vFtp.Setting = new FTPSettings(false);
            vFtp.Setting.IP = "10.16.20.73";
            vFtp.Setting.UserID = "uuspcs00";
            vFtp.Setting.Password = "uu$pcs00";
            string msg = "";

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            bool resultUpload = vFtp.FtpUpload("/Portal/DailyOrder/" + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);
            rpt.Dispose();
        }
        private static void PrintKanbanReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@PartNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\Rep_ManifestKanban_Download.trdx", "SP_Rep_DailyOrder_Kanban", parameters);

            FTPUpload vFtp = new FTPUpload(false);
            //vFtp.Setting = new FTPSettings(false);
            vFtp.Setting.IP = "10.16.20.73";
            vFtp.Setting.UserID = "uuspcs00";
            vFtp.Setting.Password = "uu$pcs00";
            string msg = "";

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            bool resultUpload = vFtp.FtpUpload("/Portal/DailyOrder/" + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);
            rpt.Dispose();
        }

        private static void PrintManifestReport(string ManifestNos, string PaperType, string FileName, string FolderName,string OrderType, string ProblemPart=null)
        {
            string reportPath = @"\Report\Rep_SupplierManifest.trdx";
            if (PaperType == "1")
                reportPath = @"\Report\Rep_SupplierManifest_WithoutBorder.trdx";

            string sqlCommand = "SP_Rep_DailyOrder_Manifest_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
            if (ProblemPart == "1") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameters);

            if (PaperType == "1") SavePDFReportToFTP(rpt, FileName + PaperType + "-" + "1", FolderName + "/ManifestPrePrinted",OrderType);
            else SavePDFReportToFTP(rpt, FileName + PaperType + "-" + "1", FolderName + "/ManifestPlain", OrderType);
        }
        private static void PrintSkidReport(string ManifestNos, string FileName, string FolderName, string OrderType, string ProblemPart = null)
        {
            string reportPath = @"\Report\Rep_ManifestSkid_Download.trdx";
            string sqlCommand = "SP_Rep_DailyOrder_Skid_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
            if (ProblemPart == "1") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameters);

            SavePDFReportToFTP(rpt, FileName + "3", FolderName + "/Skid", OrderType);
        }
        //private static void PrintKanbanSkidReport(string ManifestNos, String FileName, string FolderName)
        //{
        //    string kanbanReportPath = @"\Report\Rep_ManifestKanban.trdx";
        //    string kanbanSqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
        //    Telerik.Reporting.SqlDataSourceParameterCollection kanbanParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
        //    kanbanParameters.Add("@ManifestNo", System.Data.DbType.String, "=Parameters.ManifestNo.Value");
        //    kanbanParameters.Add("@PartNo", System.Data.DbType.String, null);
        //    kanbanParameters.Add("@OrderReleaseDate", System.Data.DbType.String, null);
        //    kanbanParameters.Add("@SupplierCode", System.Data.DbType.String, null);
        //    kanbanParameters.Add("@SupplierPlant", System.Data.DbType.String, null);
        //    Telerik.Reporting.Report kanbanReport = CreateReport(kanbanReportPath, kanbanSqlCommand, kanbanParameters);

        //    string skidReportPath = @"\Report\Rep_ManifestSkid.trdx";
        //    string skidSqlCommand = "SP_Rep_DailyOrder_Skid_Print";
        //    Telerik.Reporting.SqlDataSourceParameterCollection skidParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
        //    skidParameters.Add("@ManifestNo", System.Data.DbType.String, "=Parameters.ManifestNo.Value");
        //    Telerik.Reporting.Report skidReport = CreateReport(skidReportPath, skidSqlCommand, skidParameters);

        //    string kanbanSkidReportPath = @"\Report\Rep_KanbanSkid.trdx";
        //    string kanbanSkidSqlCommand = "SP_Rep_DailyOrder_KanbanSkid_Print";
        //    Telerik.Reporting.SqlDataSourceParameterCollection kanbanSkidParameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
        //    kanbanSkidParameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
        //    Telerik.Reporting.Report rpt = CreateReport(kanbanSkidReportPath, kanbanSkidSqlCommand, kanbanSkidParameters);

        //    Telerik.Reporting.SubReport rptSub2 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport1", true)[0];
        //    rptSub2.ReportSource = new Telerik.Reporting.InstanceReportSource { ReportDocument = kanbanReport };
        //    rptSub2.ReportSource.Parameters.Add("ManifestNo", "=Fields.[MANIFEST_NO]");

        //    Telerik.Reporting.SubReport rptSub1 = (Telerik.Reporting.SubReport)rpt.Items.Find("subReport2", true)[0];
        //    rptSub1.ReportSource = new Telerik.Reporting.InstanceReportSource { ReportDocument = skidReport };
        //    rptSub1.ReportSource.Parameters.Add("ManifestNo", "=Fields.[MANIFEST_NO]");

        //    SavePDFReportToFTP(rpt, FileName + "4", FolderName + "/KanbanSkid");
        //}
        private static void PrintSelectionKanbanReport(string ManifestNo, string FileName, string FolderName, string OrderType, string ProblemPart = null, string PartNos = null)
        {
            ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";
            string reportPath = @"\Report\Rep_ManifestKanban_Download.trdx";
            string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
            parameter.Add("@PartNo", System.Data.DbType.String, PartNos);
            parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
            parameter.Add("@SupplierCode", System.Data.DbType.String, null);
            parameter.Add("@SupplierPlant", System.Data.DbType.String, null);
            if (ProblemPart == "1") parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameter);

            SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
        }
        #endregion

        #region Log
        private static void InsertLogHeader(SqlConnection conn,
            long ProcessID, 
            string FunctionID, 
            string ModuleID, 
            DateTime StartDate, 
            DateTime EndDate,
            int ProcessStatus,
            string CreatedBy)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertLogHeader";

            SqlParameter paramProcessID = new SqlParameter("@PROCESS_ID", SqlDbType.BigInt);
            paramProcessID.Value = ProcessID;
            cmd.Parameters.Add(paramProcessID);
            SqlParameter paramFunctionID = new SqlParameter("@FUNCTION_ID", SqlDbType.VarChar);
            paramFunctionID.Value = FunctionID;
            cmd.Parameters.Add(paramFunctionID);
            SqlParameter paramModuleID = new SqlParameter("@MODULE_ID", SqlDbType.VarChar);
            paramModuleID.Value = ModuleID;
            cmd.Parameters.Add(paramModuleID);
            SqlParameter paramStartDate = new SqlParameter("@START_DATE", SqlDbType.DateTime);
            paramStartDate.Value = StartDate;
            cmd.Parameters.Add(paramStartDate);
            SqlParameter paramEndDate = new SqlParameter("@END_DATE", SqlDbType.DateTime);
            paramEndDate.Value = EndDate;
            cmd.Parameters.Add(paramEndDate);
            SqlParameter paramProcessStatus = new SqlParameter("@PROCESS_STATUS", SqlDbType.TinyInt);
            paramProcessStatus.Value = ProcessStatus;
            cmd.Parameters.Add(paramProcessStatus);
            SqlParameter paramCreatedBy = new SqlParameter("@CREATED_BY", SqlDbType.VarChar);
            paramCreatedBy.Value = CreatedBy;
            cmd.Parameters.Add(paramCreatedBy);

            cmd.ExecuteNonQuery();
        }
        private static void InsertLogDetail(SqlConnection conn,
            long ProcessID, 
            string MessageID,
            string MessageType,
            string Message,
            string Location,
            string CreatedBy,
            Nullable<Int32> SeqNo = null
            )
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertLogDetail";

            SqlParameter paramProcessID = new SqlParameter("@PROCESS_ID", SqlDbType.BigInt);
            paramProcessID.Value = ProcessID;
            cmd.Parameters.Add(paramProcessID);
            SqlParameter paramFunctionID = new SqlParameter("@MESSAGE_ID", SqlDbType.VarChar);
            paramFunctionID.Value = MessageID;
            cmd.Parameters.Add(paramFunctionID);
            SqlParameter paramModuleID = new SqlParameter("@MESSAGE_TYPE", SqlDbType.VarChar);
            paramModuleID.Value = MessageType;
            cmd.Parameters.Add(paramModuleID);
            SqlParameter paramStartDate = new SqlParameter("@MESSAGE", SqlDbType.VarChar);
            paramStartDate.Value = Message;
            cmd.Parameters.Add(paramStartDate);
            SqlParameter paramEndDate = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            paramEndDate.Value = Location;
            cmd.Parameters.Add(paramEndDate);
            SqlParameter paramCreatedBy = new SqlParameter("@CREATED_BY", SqlDbType.VarChar);
            paramCreatedBy.Value = CreatedBy;
            cmd.Parameters.Add(paramCreatedBy);
            SqlParameter paramProcessStatus = new SqlParameter("@SEQ_NO", SqlDbType.BigInt);
            paramProcessStatus.Value = SeqNo;
            cmd.Parameters.Add(paramProcessStatus);

            cmd.ExecuteNonQuery();
        }
        private static void UpdateStatusLog(SqlConnection conn, long ProcessID, int ProcessStatus)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE dbo.TB_R_LOG_H SET END_DATE=GETDATE(),PROCESS_STATUS=" + ProcessStatus.ToString() + "WHERE PROCESS_ID = " + ProcessID.ToString();
            cmd.ExecuteNonQuery();
        }
        #endregion
        private static void SendEmail(string message)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("10.16.96.100");

            mail.From = new MailAddress("ippcs-admin@toyota.co.id");
            mail.To.Add("supplierportal8@toyota.co.id");
            mail.Subject = "Error";
            mail.Body = message;

            SmtpServer.Port = 25;
            SmtpServer.Send(mail);
        }
    }
}
