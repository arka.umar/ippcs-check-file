﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatchServer
{
    public class TcpCommandAdapter: TcpCommandListener
    {
        public virtual void StartBatch(string batchId, string submitter, string[] parameter) { }

        public virtual void SuspendBatch(string queueId) { }

        public virtual void ResumeBatch(string queueId) { }

        public virtual void StopBatch(string queueId) { }

        public virtual void StopServer() { }
    }
}
