﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Batch;

namespace BatchServer
{
    public interface RegistryListener
    {
        void ExecuteRegisteredBatch(BatchQueue batchQueue);
    }
}
