﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Toyota.Common.Batch;

namespace BatchServer
{
    public class TcpMonitor: IDisposable, TcpCommandListener
    {
        private TcpListener tcpListener;
        private List<BatchTcpClient> clients;
        private TcpCommandReceiver commandReceiver;
        private bool monitoring;        

        public TcpMonitor(IPAddress address, int port)
        {
            clients = new List<BatchTcpClient>();
            commandReceiver = new TcpCommandReceiver();
            commandReceiver.AddListener(this);
            tcpListener = new TcpListener(address, port);                 
        }

        public void Monitor()
        {
            monitoring = true;
            tcpListener.Start();
            Console.WriteLine("TCP Monitor started.");

            BatchTcpClient client;
            TcpClient tcpClient;
            while (monitoring)
            {
                try
                {
                    tcpClient = tcpListener.AcceptTcpClient();
                    client = new BatchTcpClient(tcpClient, commandReceiver);
                    clients.Add(client);
                    client.Open();
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("wsacancelblockingcall"))
                    {
                        monitoring = false;
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }            
        }
        public void Dispose()
        {
            foreach (BatchTcpClient cl in clients)
            {
                if (!cl.IsClosed)
                {
                    cl.Close();
                }
            }
            tcpListener.Server.Close();
            tcpListener.Stop();
        }

        public void AddCommandListener(TcpCommandListener listener)
        {
            commandReceiver.AddListener(listener);
        }
        public void RemoveCommandListener(TcpCommandListener listener)
        {
            commandReceiver.RemoveListener(listener);
        }

        public void StartBatch(string batchId, string submitter, string[] parameter)
        {
            BatchProcessorDescription batchInfo = BatchRegistry.GetInstance().GetBatchProcessor(batchId);
            if (batchInfo != null)
            {
                StringBuilder paramStringBuilder = new StringBuilder();
                foreach (string p in parameter)
                {
                    paramStringBuilder.Append(" ");
                    paramStringBuilder.Append(p);
                }
                BatchRegistry.GetInstance().Register(batchInfo, submitter, paramStringBuilder.ToString().Trim());
            }
        }
        public void SuspendBatch(string queueId)
        {
            
        }
        public void ResumeBatch(string queueId)
        {
            
        }
        public void StopBatch(string queueId)
        {
            
        }
        public void StopServer()
        {
            
        }
    }
}
