﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting;
using System.Data.SqlClient;
using Toyota.Common.Batch;

namespace BatchServer
{
    public class BatchServer: TcpCommandAdapter
    {        
        private TcpMonitor tcpMonitor;
        private RegistryMonitor registryMonitor;
        private AssemblyLoader assemblyLoader;
        private BatchExecutor batchExecutor;

        public BatchServer(IPAddress address, int port, byte maxExecutedBatch)
        {
            assemblyLoader = new AssemblyLoader();
            batchExecutor = new BatchExecutor(maxExecutedBatch);
            tcpMonitor = new TcpMonitor(address, port);
            tcpMonitor.AddCommandListener(this);
            tcpMonitor.AddCommandListener(batchExecutor);
            registryMonitor = new RegistryMonitor();
            registryMonitor.AddRegistryListener(batchExecutor);
        }

        public void Start()
        {
            assemblyLoader.LoadRequirement();

            Thread thread = new Thread(new ThreadStart(tcpMonitor.Monitor));
            thread.Start();
            thread = new Thread(new ThreadStart(registryMonitor.Monitor));
            thread.Start();
        }
        public void Stop()
        {
            Console.Write("stopping server ... ");            
            tcpMonitor.Dispose();
            registryMonitor.Dispose();
            batchExecutor.Dispose();
            BatchRegistry.GetInstance().Dispose();
            Console.WriteLine("done");
        }

        public override void StopServer()
        {
            Stop();
        }
    }
}
