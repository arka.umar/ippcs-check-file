﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace BatchServer
{
    public class AssemblyLoader
    {
        public void LoadRequirement()
        {
            string commonBatchFile = "CommonBatch.dll";
            AppDomain domain = AppDomain.CurrentDomain;
            AssemblyName assemblyName = AssemblyName.GetAssemblyName(commonBatchFile);
            Assembly[] assemblies = domain.GetAssemblies();
            var existingAssembly = (from a in assemblies
                                    where a.FullName.Equals(assemblyName.FullName)
                                    select a).SingleOrDefault();
            if (existingAssembly == null)
            {
                domain.Load(assemblyName);
            }
        }
    }
}
