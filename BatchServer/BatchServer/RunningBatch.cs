﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Toyota.Common.Batch;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

namespace BatchServer
{
    [Serializable]
    public class RunningBatch
    {
        public AppDomain Domain { set; get; }
        public BatchProcessor Batch { set; get; }
        public BatchProcessorDescription BatchInfo { set; get; }
        public string Submitter { set; get; }
        public string Parameters { set; get; }        
    }
}
