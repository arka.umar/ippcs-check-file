﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace BatchServer
{
    class Program
    {
        static void Main(string[] args)
        {            
            IPAddress address = IPAddress.Parse("127.0.0.1");
            int port = 8132;
            int maxExecutedBatch = 10;
            if (args != null)
            {
                if ((args.Length == 2) || (args.Length == 3))
                {
                    address = IPAddress.Parse(args[0].Trim());
                    port = Convert.ToInt32(args[1].Trim());
                    if (args.Length == 3)
                    {
                        maxExecutedBatch = Convert.ToInt32(args[2].Trim());
                        if (maxExecutedBatch > 254)
                        {
                            Console.WriteLine("Maximum number of batch that can be executed simultaneously is 254");
                            return;
                        }
                    }
                }
            }

            BatchServer server = new BatchServer(address, port, Convert.ToByte(maxExecutedBatch));
            server.Start();            
        }
    }
}
