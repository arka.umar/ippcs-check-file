﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatchServer
{
    public interface TcpCommandListener
    {
        void StartBatch(string batchId, string submitter, string[] parameter);
        void SuspendBatch(string queueId);
        void ResumeBatch(string queueId);
        void StopBatch(string queueId);
        void StopServer();
    }
}
