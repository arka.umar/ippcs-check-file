﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatchServer
{
    public class TcpCommandReceiver
    {
        public const byte PROCESS_CODE_NORMAL = 0;
        public const byte PROCESS_CODE_EXIT = 255;
        private List<TcpCommandListener> listeners;

        public TcpCommandReceiver()
        {
            listeners = new List<TcpCommandListener>();
        }

        public void AddListener(TcpCommandListener listener)
        {
            if (!listeners.Contains(listener))
            {
                listeners.Add(listener);
            }
        }
        public void RemoveListener(TcpCommandListener listener)
        {
            listeners.Remove(listener);
        }

        private void notifyStartBatch(string batchId, string submitter, string[] parameter)
        {
            foreach (TcpCommandListener listener in listeners)
            {
                listener.StartBatch(batchId, submitter, parameter);
            }
        }
        private void notifyStopServer()
        {
            foreach (TcpCommandListener listener in listeners)
            {
                listener.StopServer();
            }
        }        
        private void notifySuspendBatch(string queueId)
        {
            foreach (TcpCommandListener listener in listeners)
            {
                listener.SuspendBatch(queueId);
            }
        }
        private void notifyResumeBatch(string queueId)
        {
            foreach (TcpCommandListener listener in listeners)
            {
                listener.ResumeBatch(queueId);
            }
        }
        private void notifyStopBatch(string queueId)
        {
            foreach (TcpCommandListener listener in listeners)
            {
                listener.StopBatch(queueId);
            }
        }
        private void StopServer()
        {
            notifyStopServer();
        }

        public int Process(string argument) {
            argument = argument.ToLower();
            string[] fractions = argument.Split(' ');
            List<string> arguments = null;
            if ((fractions != null) && (fractions.Length > 0))
            {
                string command = fractions[0];
                command = command.Trim().ToLower();
                if (command.Equals("quit"))
                {
                    notifyStopServer();
                    return PROCESS_CODE_EXIT;
                }
                else if (command.Equals("exec"))
                {
                    if (fractions.Length >= 3)
                    {
                        if (fractions.Length >= 4)
                        {
                            arguments = new List<string>();
                            for (int i = 3; i < fractions.Length; i++)
                            {
                                arguments.Add(fractions[i]);
                            }
                        }
                        notifyStartBatch(fractions[1].Trim(), fractions[2].Trim(), arguments != null ? arguments.ToArray() : new string[] {});                        
                    }
                }
                else if (command.Equals("stop"))
                {
                    if (fractions.Length == 2)
                    {
                        notifyStopBatch(fractions[1].Trim());
                    }
                }
                else if (command.Equals("suspend"))
                {
                    if (fractions.Length == 2)
                    {
                        notifySuspendBatch(fractions[1].Trim());
                    }
                }
                else if (command.Equals("resume"))
                {
                    if (fractions.Length == 2)
                    {
                        notifyResumeBatch(fractions[1].Trim());
                    }
                }
            }

            return PROCESS_CODE_NORMAL;
        }
    }
}
