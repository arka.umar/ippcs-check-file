﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Batch;
using System.Reflection;

namespace BatchServer
{
    public class BatchExecutor: TcpCommandAdapter,IDisposable, RegistryListener
    {
        private byte maxExecutedBatch;
        

        public BatchExecutor(byte maxExecutedBatch)
        {
            this.maxExecutedBatch = maxExecutedBatch;            
        }

        private void Execute(BatchProcessorDescription info, BatchQueue batchQueue)
        {
            if (BatchRegistry.GetInstance().GetRunningBatchCount() >= maxExecutedBatch)
            {
                return;
            }

            AppDomain domain = AppDomain.CreateDomain(info.Name);
            AssemblyName assemblyName = AssemblyName.GetAssemblyName(info.ClassName + ".exe");
            BatchProcessor batch = (BatchProcessor)domain.CreateInstanceAndUnwrap(assemblyName.Name, string.Format("Batch.{0}", info.ClassName));

            StringBuilder paramStringBuilder = new StringBuilder();
            string[] parameters = batchQueue.Parameters.Split(' ');
            foreach (string p in parameters)
            {
                paramStringBuilder.Append(" ");
                paramStringBuilder.Append(p);
            }
            string paramString = paramStringBuilder.ToString().Trim();
            RunningBatch runningBatch = new RunningBatch()
            {
                Batch = batch,
                Domain = domain,
                BatchInfo = info,
                Submitter = batchQueue.Submitter,
                Parameters = paramString
            };
            
            batch.QueueId = batchQueue.QueueId;
            BatchRegistry.GetInstance().RegisterRunningBatch(batchQueue.QueueId, runningBatch);
            batch.Start(parameters);
        }
        public void Execute(BatchQueue batchQueue)
        {
            BatchProcessorDescription info = BatchRegistry.GetInstance().GetBatchProcessor(batchQueue.BatchId);
            if (info != null)
            {
                Execute(info, batchQueue);
            }
        }

        public void Stop(long id)
        {
            BatchRegistry registry = BatchRegistry.GetInstance();
            RunningBatch batch = registry.GetRunningBatch(id);
            if (batch != null)
            {
                batch.Batch.Stop();
                registry.DisposeRunningBatch(id);
            }
        }
        public void Suspend(long id)
        {
            BatchRegistry registry = BatchRegistry.GetInstance();
            RunningBatch batch = registry.GetRunningBatch(id);
            if (batch != null)
            {
                batch.Batch.Suspend();
            }
        }
        public void Resume(long id)
        {
            BatchRegistry registry = BatchRegistry.GetInstance();
            RunningBatch batch = registry.GetRunningBatch(id);
            if (batch != null)
            {
                batch.Batch.Resume();
            }
        }

        public void Dispose()
        {
            foreach (RunningBatch batch in BatchRegistry.GetInstance().GetRunningBatches())
            {
                batch.Batch.Stop();
            }
        }

        public void ExecuteRegisteredBatch(BatchQueue batchQueue)
        {
            Execute(batchQueue);
        }
        public override void StopBatch(string queueId)
        {
            Stop(Convert.ToInt64(queueId));
        }
        public override void SuspendBatch(string queueId)
        {
            Suspend(Convert.ToInt64(queueId));
        }
        public override void ResumeBatch(string queueId)
        {
            Resume(Convert.ToInt64(queueId));
        }
    }
}
