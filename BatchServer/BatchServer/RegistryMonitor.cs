﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Toyota.Common.Batch;

namespace BatchServer
{
    public class RegistryMonitor: IDisposable
    {
        private bool monitoring;
        private List<RegistryListener> listeners;

        public RegistryMonitor()
        {
            WaitingPeriod = 5000;
            listeners = new List<RegistryListener>();
        }

        public void Monitor()
        {
            Console.WriteLine("Registry Monitor started.");
            monitoring = true;
            List<BatchQueue> registeredBatch;
            List<BatchQueue> inactiveBatches;
            BatchRegistry registry = BatchRegistry.GetInstance();
            while (monitoring)
            {
                Thread.Sleep(WaitingPeriod);
                inactiveBatches = registry.GetInactiveBatch();
                if (inactiveBatches != null)
                {
                    registry.RelocateInactiveBatch(inactiveBatches);
                }

                registeredBatch = registry.GetRegisteredBatch();
                if (registeredBatch != null)
                {
                    foreach (BatchQueue batch in registeredBatch)
                    {
                        notifyExecuteRegisteredBatch(batch);                           
                    }
                }
            }
        }
        
        public void AddRegistryListener(RegistryListener listener)
        {
            if (!listeners.Contains(listener))
            {
                listeners.Add(listener);
            }
        }
        public void RemoveRegistryListener(RegistryListener listener)
        {
            listeners.Remove(listener);
        }
        private void notifyExecuteRegisteredBatch(BatchQueue batchQueue)
        {
            foreach (RegistryListener listener in listeners)
            {
                listener.ExecuteRegisteredBatch(batchQueue);
            }
        }

        public int WaitingPeriod { set; get; }        
        public void Dispose()
        {
            monitoring = false;
        }
    }
}
