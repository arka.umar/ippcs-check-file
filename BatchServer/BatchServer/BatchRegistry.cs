﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Toyota.Common.Batch;
using System.Data;

namespace BatchServer
{
    public class BatchRegistry: IDisposable
    {
        private static BatchRegistry instance = new BatchRegistry();
        private Dictionary<long, RunningBatch> runningBatches;

        public const string CONNECTION_STRING_PCS = "Data Source=ARKNABLE\\SQLEXPRESS;Initial Catalog=PCS_DB;User ID=sa;Password=adminadmin;Integrated Security=false;";
        //public const string CONNECTION_STRING_PCS = "Server=10.16.20.230;Database=PCS_DB;User ID=K2;Password=K2pass!;Trusted_Connection=false;";
        private SqlConnection connection;
        private Dictionary<string, BatchProcessorDescription> batchProcessors;
        private Random queueIdGenerator;
        private SqlCommand cmdRegisterBatch;
        private SqlCommand cmdSelectInactiveBatch;
        private SqlCommand cmdCreateHistory;
        private SqlCommand cmdRemoveInactiveBatch;
        private SqlCommand cmdSelectBatchProcessors;
        private SqlCommand cmdSelectRegisteredBatch;

        private BatchRegistry()
        {
            queueIdGenerator = new Random();
            runningBatches = new Dictionary<long, RunningBatch>();

            connection = new SqlConnection(CONNECTION_STRING_PCS);
            connection.Open();
            InitCommands();            

            batchProcessors = GetBatchProcessors();
        }

        public static BatchRegistry GetInstance()
        {
            return instance;
        }

        public void RegisterRunningBatch(long id, RunningBatch batch)
        {
            if (!runningBatches.ContainsKey(id))
            {
                runningBatches.Add(id, batch);
            }
        }
        public void DisposeRunningBatch(long id)
        {
            if (runningBatches.ContainsKey(id))
            {
                runningBatches.Remove(id);
            }
        }
        public int GetRunningBatchCount()
        {
            return runningBatches.Count;
        }
        public RunningBatch GetRunningBatch(long id)
        {
            if (runningBatches.ContainsKey(id))
            {
                return runningBatches[id];
            }
            return null;
        }
        public List<RunningBatch> GetRunningBatches()
        {
            return runningBatches.Values.ToList<RunningBatch>();
        }

        public Dictionary<string, BatchProcessorDescription> GetBatchProcessors()
        {
            if (connection.State != ConnectionState.Open)
            {
                return null;
            }

            SqlDataReader reader = cmdSelectBatchProcessors.ExecuteReader();
            BatchProcessorDescription batchInfo;
            if (reader.HasRows)
            {
                Dictionary<string, BatchProcessorDescription> map = new Dictionary<string, BatchProcessorDescription>();
                while (reader.Read())
                {
                    batchInfo = new BatchProcessorDescription()
                    {
                        Id = Convert.ToString(reader["Id"]).Trim(),
                        Name = Convert.ToString(reader["Name"]).Trim(),
                        ClassName = Convert.ToString(reader["ClassName"]).Trim(),
                        Description = Convert.ToString(reader["Description"]).Trim(),
                        PriorityLevel = Convert.ToByte(reader["PriorityLevel"]),
                        PriorityName = Convert.ToString(reader["PriorityName"]).Trim()
                    };
                    map.Add(batchInfo.Id.ToLower(), batchInfo);
                }
                reader.Close();
                return map;
            }
            return null;
        }
        public BatchProcessorDescription GetBatchProcessor(string id)
        {
            id = id.ToLower();
            if (batchProcessors.ContainsKey(id))
            {
                return batchProcessors[id];
            }

            return null;
        }
        public int Register(BatchProcessorDescription batchInfo, string submitter, string parameters)
        {
            if (connection.State != ConnectionState.Open)
            {
                return -1;
            }

            int queueId = queueIdGenerator.Next(10000);
            SqlParameterCollection commandParams = cmdRegisterBatch.Parameters;
            DateTime today = DateTime.Now;
            commandParams["@batchId"].Value = batchInfo.Id;
            commandParams["@submissionDate"].Value = today;
            commandParams["@submitter"].Value = submitter;
            commandParams["@parameters"].Value = parameters;
            commandParams["@createdBy"].Value = "system";
            commandParams["@createdDate"].Value = today;
            cmdRegisterBatch.ExecuteNonQuery();

            return queueId;
        }
        public List<BatchQueue> GetInactiveBatch()
        {
            if (connection.State != ConnectionState.Open)
            {
                return null;
            }

            SqlDataReader reader = cmdSelectInactiveBatch.ExecuteReader();
            if (reader.HasRows)
            {
                List<BatchQueue> batchList = new List<BatchQueue>();
                while (reader.Read())
                {
                    batchList.Add(new BatchQueue() { 
                        QueueId = Convert.ToInt32(reader["QueueId"]),
                        BatchId = Convert.ToString(reader["BatchId"]),
                        SubmissionDate = Convert.ToDateTime(reader["SubmissionDate"]),
                        Submitter = Convert.ToString(reader["Submitter"]),
                        Progress = Convert.ToByte(reader["Progress"]),
                        Status = Convert.ToByte(reader["Status"]),
                        Parameters = Convert.ToString(reader["Parameters"])
                    });
                }
                reader.Close();

                return batchList;
            }
            reader.Close();

            return null;
        }
        public List<BatchQueue> GetRegisteredBatch()
        {
            if (connection.State != ConnectionState.Open)
            {
                return null;
            }

            SqlDataReader reader = cmdSelectRegisteredBatch.ExecuteReader();
            if (reader.HasRows)
            {
                List<BatchQueue> batchList = new List<BatchQueue>();
                object progress;
                object status;
                object parameters;
                BatchQueue batchQueue;
                while (reader.Read())
                {
                    progress = reader["Progress"];
                    status = reader["Status"];
                    parameters = reader["Parameters"];
                    batchQueue = new BatchQueue()
                    {
                        QueueId = Convert.ToInt32(reader["QueueId"]),
                        BatchId = Convert.ToString(reader["BatchId"]),
                        SubmissionDate = Convert.ToDateTime(reader["SubmissionDate"]),
                        Submitter = Convert.ToString(reader["Submitter"])
                    };
                    batchList.Add(batchQueue);
                    if (Convert.IsDBNull(progress))
                    {
                        batchQueue.Progress = null;
                    }
                    else
                    {
                        batchQueue.Progress = Convert.ToByte(progress);
                    }
                    if (Convert.IsDBNull(parameters))
                    {
                        batchQueue.Parameters = null;
                    }
                    else
                    {
                        batchQueue.Parameters = Convert.ToString(parameters);
                    }
                    if (Convert.IsDBNull(status))
                    {
                        batchQueue.Status = null;
                    }
                    else
                    {
                        batchQueue.Status = Convert.ToByte(status);
                    }
                }
                reader.Close();

                return batchList;
            }
            reader.Close();

            return null;
        }
        public void RelocateInactiveBatch(List<BatchQueue> inactiveBatches)
        {
            if (inactiveBatches == null)
            {
                return;
            }
            if (connection.State != ConnectionState.Open)
            {
                return;
            }

            SqlParameterCollection parameters = cmdCreateHistory.Parameters;            
            DateTime today = DateTime.Now;
            foreach (BatchQueue batch in inactiveBatches)
            {
                parameters["@batchId"].Value = batch.BatchId;
                parameters["@submissionDate"].Value = batch.SubmissionDate;
                parameters["@submitter"].Value = batch.Submitter;
                parameters["@status"].Value = batch.Status;
                parameters["@createdBy"].Value = "system";
                parameters["@createdDate"].Value = today;
                parameters["@params"].Value = batch.Parameters != null ? batch.Parameters : String.Empty;
                cmdCreateHistory.ExecuteNonQuery();

                cmdRemoveInactiveBatch.Parameters["@queueId"].Value = batch.QueueId;
                cmdRemoveInactiveBatch.ExecuteNonQuery();
            }
        }               
        public void Dispose()
        {
            cmdSelectInactiveBatch.Dispose();
            cmdCreateHistory.Dispose();
            cmdRegisterBatch.Dispose();
            connection.Close();
        }

        private void InitCommands()
        {
            StringBuilder commandBuilder = new StringBuilder();
            commandBuilder.AppendLine("insert into tb_r_batch_queue (batch_id, submission_dt, submitter, parameters, created_by, created_dt) ");
            commandBuilder.AppendLine("values (@batchId, @submissionDate, @submitter, @parameters, @createdBy, @createdDate)");
            cmdRegisterBatch = connection.CreateCommand();
            cmdRegisterBatch.CommandText = commandBuilder.ToString();
            SqlParameterCollection parameters = cmdRegisterBatch.Parameters;
            parameters.Add(new SqlParameter("@batchId", SqlDbType.VarChar));
            parameters.Add(new SqlParameter("@submissionDate", SqlDbType.DateTime));
            parameters.Add(new SqlParameter("@submitter", SqlDbType.VarChar));
            parameters.Add(new SqlParameter("@parameters", SqlDbType.VarChar));
            parameters.Add(new SqlParameter("@createdBy", SqlDbType.VarChar));
            parameters.Add(new SqlParameter("@createdDate", SqlDbType.DateTime));

            commandBuilder.Clear();
            commandBuilder.AppendLine("select   batch_id as Id,");
            commandBuilder.AppendLine("         batch_name as Name,");
            commandBuilder.AppendLine("         class_name as ClassName,");
            commandBuilder.AppendLine("         description as Description,");
            commandBuilder.AppendLine("         priority_level as PriorityLevel,");
            commandBuilder.AppendLine("         (select NAME from tb_m_priority where [level] = priority_level) as PriorityName");
            commandBuilder.AppendLine("from tb_m_batch");
            cmdSelectBatchProcessors = connection.CreateCommand();
            cmdSelectBatchProcessors.CommandText = commandBuilder.ToString();

            commandBuilder.Clear();
            commandBuilder.AppendLine("select   queue_id as QueueId,");
            commandBuilder.AppendLine("         batch_id as BatchId,");
            commandBuilder.AppendLine("         submission_dt as SubmissionDate,");
            commandBuilder.AppendLine("         submitter as Submitter,");
            commandBuilder.AppendLine("         parameters as Parameters,");
            commandBuilder.AppendLine("         progress as Progress,");
            commandBuilder.AppendLine("         status as Status ");
            commandBuilder.AppendLine("from tb_r_batch_queue ");
            commandBuilder.AppendLine("where ([status] != @processingStatus) ");
            commandBuilder.AppendLine("      and ([status] != @suspendStatus) ");
            cmdSelectInactiveBatch = connection.CreateCommand();
            cmdSelectInactiveBatch.CommandText = commandBuilder.ToString();
            cmdSelectInactiveBatch.Parameters.Add(new SqlParameter("@processingStatus", SqlDbType.TinyInt) { Value = ProcessStatus.PROCESS_STATUS_PROCESSING });
            cmdSelectInactiveBatch.Parameters.Add(new SqlParameter("@suspendStatus", SqlDbType.TinyInt) { Value = ProcessStatus.PROCESS_STATUS_SUSPENDED });
            
            commandBuilder.Clear();
            commandBuilder.AppendLine("insert into tb_r_batch_history (batch_id, submission_dt, submitter, status, created_by, created_dt, parameters) ");
            commandBuilder.AppendLine("values (@batchId, @submissionDate, @submitter, @status, @createdBy, @createdDate, @params)");
            cmdCreateHistory = connection.CreateCommand();
            cmdCreateHistory.CommandText = commandBuilder.ToString();
            parameters = cmdCreateHistory.Parameters;
            parameters.Add(new SqlParameter("@batchId", SqlDbType.VarChar));
            parameters.Add(new SqlParameter("@submissionDate", SqlDbType.DateTime));
            parameters.Add(new SqlParameter("@submitter", SqlDbType.VarChar));
            parameters.Add(new SqlParameter("@status", SqlDbType.TinyInt));
            parameters.Add(new SqlParameter("@createdBy", SqlDbType.VarChar));
            parameters.Add(new SqlParameter("@createdDate", SqlDbType.DateTime));
            parameters.Add(new SqlParameter("@params", SqlDbType.VarChar));
                        
            cmdRemoveInactiveBatch = connection.CreateCommand();
            cmdRemoveInactiveBatch.CommandText = "delete from tb_r_batch_queue where queue_id = @queueId";
            cmdRemoveInactiveBatch.Parameters.Add(new SqlParameter("@queueId", SqlDbType.Int));

            commandBuilder.Clear();
            commandBuilder.AppendLine("select   queue_id as QueueId,");
            commandBuilder.AppendLine("         batch_id as BatchId,");
            commandBuilder.AppendLine("         submission_dt as SubmissionDate,");
            commandBuilder.AppendLine("         submitter as Submitter,");
            commandBuilder.AppendLine("         parameters as Parameters,");
            commandBuilder.AppendLine("         progress as Progress,");
            commandBuilder.AppendLine("         status as Status ");
            commandBuilder.AppendLine("from tb_r_batch_queue ");
            commandBuilder.AppendLine("where progress is null");
            cmdSelectRegisteredBatch = connection.CreateCommand();
            cmdSelectRegisteredBatch.CommandText = commandBuilder.ToString();
        }
    }
}
