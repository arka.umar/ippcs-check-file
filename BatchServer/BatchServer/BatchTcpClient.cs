﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace BatchServer
{
    public class BatchTcpClient
    {
        private TcpClient client;
        private bool waiting;
        private TcpCommandReceiver commandProcessor;

        public BatchTcpClient(TcpClient client, TcpCommandReceiver commandProcessor)
        {
            this.client = client;
            IsClosed = false;
            this.commandProcessor = commandProcessor;
        }

        public bool IsClosed { set; get; }
        public void Open()
        {
            waiting = true;
            ThreadPool.QueueUserWorkItem(new WaitCallback(Open));
        }

        public void Close()
        {      
            client.Close();
        }

        private void Open(object arg)
        {
            if (client == null)
            {
                return;
            }

            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);

            string command;
            int processCode;
            while (waiting)
            {
                try
                {
                    command = reader.ReadLine();
                    processCode = commandProcessor.Process(command);
                    if (processCode == TcpCommandReceiver.PROCESS_CODE_EXIT)
                    {
                        waiting = false;
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("wsacancelblockingcall"))
                    {
                        waiting = false;
                    }
                    else
                    {
                        throw ex;
                    }
                }                
            }

            reader.Close();
            reader.Dispose();            
            stream.Close();            
            stream.Dispose();            
            IsClosed = true;
        }        
    }
}
