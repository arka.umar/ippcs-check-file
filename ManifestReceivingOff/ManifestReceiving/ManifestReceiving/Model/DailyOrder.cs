﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManifestReceiving.Offline.Model
{
    class DailyOrder
    {
        public string ManifestNo { set; get; }
        public bool ProblemFlag { set; get; }
        public string PartNo { set; get; }
        public int ShortageQty { set; get; }
        public int MisspartQty { set; get; }
        public int DamageQty { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDt { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDt { set; get; }
    }
}
