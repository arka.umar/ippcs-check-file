﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace ManifestReceiving.Offline.Model
{
    public class User
    {
        private static string ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;

        public User()
        {
            MakeUserTable();
        }

        private DataSet _DataSet;
        private string _DataMember = "UserTable";

        public DataSet DataSet
        {
            get { return _DataSet; }
            set { _DataSet = value; }
        }

        public string DataMember
        {
            get { return _DataMember; }
            set
            {
                _DataMember = value;
            }
        }

        private void MakeUserTable()
        {
            DataTable table = new DataTable(_DataMember);
            DataColumn column;
            DataRow row;

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "Username";
            column.AutoIncrement = false;
            column.Caption = "Username";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);


            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "Password";
            column.AutoIncrement = false;
            column.Caption = "Password";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);

            DataSet = new DataSet();
            DataSet.Tables.Add(table);


            try
            {
                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();

                    string sqlQuery = "SELECT * FROM User";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter paramManifestNo = new SQLiteParameter();
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    row = table.NewRow();
                                    row["Username"] = rdr["Username"];
                                    row["Password"] = rdr["Password"];
                                    table.Rows.Add(row);
                                }
                            }

                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        public string Username { set; get; }
        public string Password { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDt { set; get; }
        public string ChangedBy { set; get; }
        public DateTime ChangedDt { set; get; }
    }
}
