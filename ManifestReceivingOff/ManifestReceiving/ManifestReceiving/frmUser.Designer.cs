﻿namespace ManifestReceiving.Offline
{
    partial class frmUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grdUser = new DevExpress.XtraGrid.GridControl();
            this.grdData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcUsername = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cmdAdd = new DevExpress.XtraEditors.SimpleButton();
            this.cmdDelete = new DevExpress.XtraEditors.SimpleButton();
            this.txtUsername = new DevExpress.XtraEditors.TextEdit();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dxValidate = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grdUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidate)).BeginInit();
            this.SuspendLayout();
            // 
            // grdUser
            // 
            this.grdUser.Location = new System.Drawing.Point(7, 105);
            this.grdUser.LookAndFeel.SkinName = "Office 2010 Black";
            this.grdUser.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdUser.MainView = this.grdData;
            this.grdUser.Name = "grdUser";
            this.grdUser.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.grdUser.Size = new System.Drawing.Size(272, 238);
            this.grdUser.TabIndex = 10;
            this.grdUser.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdData});
            // 
            // grdData
            // 
            this.grdData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcUsername,
            this.gcPassword});
            this.grdData.GridControl = this.grdUser;
            this.grdData.Name = "grdData";
            this.grdData.OptionsView.ShowDetailButtons = false;
            this.grdData.OptionsView.ShowGroupPanel = false;
            this.grdData.OptionsView.ShowIndicator = false;
            // 
            // gcUsername
            // 
            this.gcUsername.Caption = "Username";
            this.gcUsername.FieldName = "Username";
            this.gcUsername.Name = "gcUsername";
            this.gcUsername.Visible = true;
            this.gcUsername.VisibleIndex = 0;
            // 
            // gcPassword
            // 
            this.gcPassword.Caption = "Password";
            this.gcPassword.ColumnEdit = this.repositoryItemTextEdit1;
            this.gcPassword.FieldName = "Password";
            this.gcPassword.Name = "gcPassword";
            this.gcPassword.Visible = true;
            this.gcPassword.VisibleIndex = 1;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Location = new System.Drawing.Point(226, 64);
            this.cmdAdd.LookAndFeel.SkinName = "Office 2010 Black";
            this.cmdAdd.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(53, 26);
            this.cmdAdd.TabIndex = 15;
            this.cmdAdd.Text = "&Save";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(167, 64);
            this.cmdDelete.LookAndFeel.SkinName = "Office 2010 Black";
            this.cmdDelete.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(53, 26);
            this.cmdDelete.TabIndex = 14;
            this.cmdDelete.Text = "&Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(111, 12);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Properties.LookAndFeel.SkinName = "Office 2010 Black";
            this.txtUsername.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtUsername.Properties.MaxLength = 16;
            this.txtUsername.Size = new System.Drawing.Size(168, 20);
            this.txtUsername.TabIndex = 16;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(111, 38);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.LookAndFeel.SkinName = "Office 2010 Black";
            this.txtPassword.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtPassword.Properties.MaxLength = 12;
            this.txtPassword.Properties.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(168, 20);
            this.txtPassword.TabIndex = 17;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 14);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 13);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "Username";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 41);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(46, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "Password";
            // 
            // frmUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 347);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.cmdAdd);
            this.Controls.Add(this.cmdDelete);
            this.Controls.Add(this.grdUser);
            this.LookAndFeel.SkinName = "Office 2010 Black";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Master User";
            this.Load += new System.EventHandler(this.frmUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdUser;
        private DevExpress.XtraGrid.Views.Grid.GridView grdData;
        private DevExpress.XtraGrid.Columns.GridColumn gcUsername;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repUsername;
        private DevExpress.XtraGrid.Columns.GridColumn gcPassword;
        private DevExpress.XtraEditors.SimpleButton cmdAdd;
        private DevExpress.XtraEditors.SimpleButton cmdDelete;
        private DevExpress.XtraEditors.TextEdit txtUsername;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}