﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ManifestReceiving.Offline.Model;
using System.Data.SQLite;
using DevExpress.XtraEditors.DXErrorProvider;

namespace ManifestReceiving.Offline
{
    public partial class frmUser : DevExpress.XtraEditors.XtraForm
    {
        private static string ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;

        public frmUser()
        {
            InitializeComponent();
            InitValidationRules();
        }

        private void frmUser_Load(object sender, EventArgs e)
        {
            grdUser.DataSource = GetAllUser();
        }

        private List<User> GetAllUser()
        {
            List<User> result = new List<User>();
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();

                    string sqlQuery = "SELECT * FROM User";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter paramManifestNo = new SQLiteParameter();
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    result.Add(new User
                                    {
                                        Username = rdr["Username"].ToString(),
                                        Password = rdr["Password"].ToString()
                                    });
                                }
                            }
                            
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = new List<User>();
            }

            return result;
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            if (dxValidate.Validate())
            {
                if (CheckUsername(txtUsername.Text.Trim()))
                {
                    UpdateData(new User
                    {
                        Username = txtUsername.Text.Trim()
                    });
                }
                else
                {
                    InsertData(new User
                    {
                        Username = txtUsername.Text.Trim(),
                        Password = txtPassword.Text.Trim()
                    });
                }
                grdUser.DataSource = GetAllUser();
                grdUser.Refresh();
            }
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            if (dxValidate.Validate())
            {
                DeleteData(new User 
                {
                    Username = txtUsername.Text.Trim(),
                    Password = txtPassword.Text.Trim()
                });
                int rowHandle = grdData.GetRowHandle(grdData.DataRowCount);

                grdData.DeleteRow(rowHandle);

                grdUser.DataSource = GetAllUser();
                grdUser.Refresh();
            }
        }

        private void InsertData(User user)
        {
            string sqlInsert = @"insert into User (
                                        Username, 
                                        Password)
                                        values (
                                        ?, ?)";

            using (SQLiteConnection con = new SQLiteConnection(ConnString))
            {
                con.Open();
                using (SQLiteTransaction mytransaction = con.BeginTransaction())
                {

                    using (SQLiteCommand cmd = new SQLiteCommand(con))
                    {
                        SQLiteParameter username = new SQLiteParameter();
                        SQLiteParameter password = new SQLiteParameter();

                        username.DbType = DbType.String;
                        username.Size = 16;
                        username.Value = user.Username;

                        password.DbType = DbType.String;
                        password.Size = 12;
                        password.Value = user.Password;

                        cmd.CommandText = sqlInsert;

                        cmd.Parameters.Add(username);
                        cmd.Parameters.Add(password);

                        cmd.ExecuteNonQuery();
                    }

                    mytransaction.Commit();
                }
                con.Close();
            }
        }

        private void DeleteData(User user)
        {
            string sqlInsert = @"DELETE FROM User WHERE Username = ?";

            using (SQLiteConnection con = new SQLiteConnection(ConnString))
            {
                con.Open();
                using (SQLiteTransaction mytransaction = con.BeginTransaction())
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(con))
                    {
                        SQLiteParameter username = new SQLiteParameter();

                        username.DbType = DbType.String;
                        username.Size = 16;
                        username.Value = user.Username;
                        
                        cmd.CommandText = sqlInsert;

                        cmd.Parameters.Add(username);

                        cmd.ExecuteNonQuery();
                    }
                    mytransaction.Commit();
                }
                con.Close();
            }
        }

        private void UpdateData(User user)
        {
            string sqlInsert = @"UPDATE User SET Password = ? WHERE Username = ?";
            using (SQLiteConnection con = new SQLiteConnection(ConnString))
            {
                con.Open();

                using (SQLiteTransaction mytransaction = con.BeginTransaction())
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(con))
                    {
                        SQLiteParameter username = new SQLiteParameter();
                        SQLiteParameter password = new SQLiteParameter();

                        username.DbType = DbType.String;
                        username.Size = 16;
                        username.Value = user.Username;

                        password.DbType = DbType.String;
                        password.Size = 12;
                        password.Value = user.Password;

                        cmd.CommandText = sqlInsert;

                        cmd.Parameters.Add(username);
                        cmd.Parameters.Add(password);

                        cmd.ExecuteNonQuery();
                    }
                    mytransaction.Commit();
                }
                con.Close();
            }
        }

        private bool CheckUsername(string userName)
        {
            bool result = false;

            using (SQLiteConnection con = new SQLiteConnection(ConnString))
            {
                con.Open();

                string sqlQuery = "SELECT * FROM User WHERE Username=?";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                {
                    SQLiteParameter paramUsername = new SQLiteParameter();

                    paramUsername.DbType = DbType.String;
                    paramUsername.Size = 16;
                    paramUsername.Value = userName;

                    cmd.Parameters.Add(paramUsername);
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }

                    }
                }
                con.Close();
            }

            return result;
        }

        private void InitValidationRules()
        {
            ConditionValidationRule cvr = new ConditionValidationRule();

            cvr.ConditionOperator = ConditionOperator.IsNotBlank;
            cvr.ErrorText = "Value Cannot be null...";

            dxValidate.SetValidationRule(txtUsername, cvr);
            dxValidate.SetValidationRule(txtPassword, cvr);
        }
    }
}