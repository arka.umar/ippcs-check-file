﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Data.SQLite;

namespace ManifestReceiving.Offline
{
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        private static string ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;

        public frmLogin()
        {
            InitializeComponent();
            InitValidationRules();
        }

        private void frmLogin_Resize(object sender, EventArgs e)
        {
            picHeader.Size = new Size(this.Size.Width - 8, 80);
            picHeader.Location = new Point(4, 4);

            picBox.Size = new Size(this.Size.Width - 8, this.Size.Height - 8);
            picBox.Location = new Point(4, 4);            
        }

        private void InitValidationRules()
        {
            ConditionValidationRule cvr = new ConditionValidationRule();

            cvr.ConditionOperator = ConditionOperator.IsNotBlank;
            cvr.ErrorText = "Value Cannot be null...";

            dxValidate.SetValidationRule(txtUsername, cvr);
            dxValidate.SetValidationRule(txtPassword, cvr);
        }

        private void cmdLogin_Click(object sender, EventArgs e)
        {
            if (dxValidate.Validate())
            {
                if (Login(txtUsername.Text.Trim(), txtPassword.Text.Trim()))
                {
                    this.Hide();
                    frmMain frm = new frmMain();
                    frm.Username = txtUsername.Text;
                    frm.Show();
                }
                else
                {
                    XtraMessageBox.Show("Incorrect username or password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool Login(string userName, string password)
        {
            bool result = false;

            using (SQLiteConnection con = new SQLiteConnection(ConnString))
            {
                con.Open();

                string sqlQuery = "SELECT * FROM User WHERE Username=? AND Password=?";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                {
                    SQLiteParameter paramUsername = new SQLiteParameter();
                    SQLiteParameter paramPassword = new SQLiteParameter();

                    paramUsername.DbType = DbType.String;
                    paramUsername.Size = 16;
                    paramUsername.Value = userName;

                    paramPassword.DbType = DbType.String;
                    paramPassword.Size = 25;
                    paramPassword.Value = password;

                    cmd.Parameters.Add(paramUsername);
                    cmd.Parameters.Add(paramPassword);
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }

                    }
                }
                con.Close();
            }
            
            return result;
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}