﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SQLite;
using ManifestReceiving.Offline.Model;
using System.Net.NetworkInformation;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid.Views.Base;

namespace ManifestReceiving.Offline
{
    public partial class frmMain : DevExpress.XtraEditors.XtraForm
    {
        private int countdown = 20;
        private static string ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;
        private static string IPAddressIPPCS = System.Configuration.ConfigurationManager.AppSettings["IPAddressIPPCS"];
        public string Username { set; get; }

        public bool HasLogin { set; get; }
        public frmMain()
        {
            InitializeComponent();
            InitValidationRules();
        }

        private static bool HasBeenExecuted = false;

        private void frmMain_Load(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;

            ClearGrid();
        }

        private void ClearGrid()
        {
            txtManifest.Text = string.Empty;

            DataHelper dh = new DataHelper(txtManifest.Text.Trim());
            dh.ManifestNo = txtManifest.Text.Trim();
            grdManifest.DataSource = dh.DataSet;
            grdManifest.DataMember = dh.DataMember;            
        }

        private bool GetManifestNoProblem(string manifestNo)
        {
            bool result = false;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();

                    string sqlQuery = "SELECT * FROM DailyOrder WHERE ManifestNo=? AND ISEXECUTED <> 'true' AND PROBLEMFLAG <> 'true'";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter paramManifestNo = new SQLiteParameter();

                        paramManifestNo.DbType = DbType.String;
                        paramManifestNo.Size = 16;
                        paramManifestNo.Value = manifestNo;

                        cmd.Parameters.Add(paramManifestNo);

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }

                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                result = false;
            }

            return result;
        }

        private List<DailyOrder> GetAllManifest()
        {
            List<DailyOrder> result = new List<DailyOrder>();
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();

                    string sqlQuery = "SELECT * FROM DailyOrder WHERE IsExecuted <> 'true'";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter paramManifestNo = new SQLiteParameter();
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    result.Add(new DailyOrder {
                                        ManifestNo = rdr["ManifestNo"].ToString(),
                                        PartNo = rdr["PartNo"].ToString(),
                                        ProblemFlag = Convert.ToBoolean(rdr["ProblemFlag"]),
                                        ShortageQty = Convert.ToInt32(rdr["ShortageQty"]),
                                        MisspartQty = Convert.ToInt32(rdr["MisspartQty"]),
                                        DamageQty = Convert.ToInt32(rdr["DamageQty"]),
                                        CreatedBy = rdr["CreatedBy"].ToString(),
                                        CreatedDt = Convert.ToDateTime(rdr["CreatedDt"])
                                    });                                    
                                }
                            }
                            else
                            {
                                lblError.Text = "No rows found.";
                            }
                            
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                result = new List<DailyOrder>();
            }

            return result;
        }

        private List<DailyOrder> GetDistinctManifest()
        {
            List<DailyOrder> result = new List<DailyOrder>();
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();
                    string sqlQuery = "SELECT DISTINCT MANIFESTNO FROM DAILYORDER WHERE ISEXECUTED <> 'true'";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter paramManifestNo = new SQLiteParameter();
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    result.Add(new DailyOrder
                                    {
                                        ManifestNo = rdr["ManifestNo"].ToString()
                                    });
                                }
                            }
                            else
                            {
                                Console.WriteLine("No rows found.");
                            }

                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                result = new List<DailyOrder>();
            }

            return result;
        }        

        private List<DailyOrder> GetManifest(string manifestNo)
        {
            List<DailyOrder> result = new List<DailyOrder>();
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();

                    string sqlQuery = "SELECT * FROM DailyOrder WHERE ManifestNo=? AND ISEXECUTED <> 'true'";
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                    {
                        SQLiteParameter paramManifestNo = new SQLiteParameter();

                        paramManifestNo.DbType = DbType.String;
                        paramManifestNo.Size = 16;
                        paramManifestNo.Value = manifestNo;

                        cmd.Parameters.Add(paramManifestNo);

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    result.Add(new DailyOrder
                                    {
                                        ManifestNo = rdr["ManifestNo"].ToString(),
                                        PartNo = rdr["PartNo"].ToString(),
                                        ProblemFlag = Convert.ToBoolean(rdr["ProblemFlag"]),
                                        ShortageQty = Convert.ToInt32(rdr["ShortageQty"]),
                                        MisspartQty = Convert.ToInt32(rdr["MisspartQty"]),
                                        DamageQty = Convert.ToInt32(rdr["DamageQty"]),
                                        CreatedBy = rdr["CreatedBy"].ToString(),
                                        CreatedDt = Convert.ToDateTime(rdr["CreatedDt"])
                                    });
                                }
                            }
                            else
                            {
                                Console.WriteLine("No rows found.");
                            }

                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                result = new List<DailyOrder>();
            }

            return result;
        }

        private bool CheckManifestPart(string manifestNo, string partNo)
                { 
                    bool result = false;

                    try
                    {
                        using (SQLiteConnection con = new SQLiteConnection(ConnString))
                        {
                            con.Open();

                            string sqlQuery = "SELECT * FROM DailyOrder WHERE ManifestNo=? AND PartNo=?";
                            using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                            {
                                SQLiteParameter paramManifestNo = new SQLiteParameter();
                                SQLiteParameter paramPartNo = new SQLiteParameter();

                                paramManifestNo.DbType = DbType.String;
                                paramManifestNo.Size = 16;
                                paramManifestNo.Value = manifestNo;

                                paramPartNo.DbType = DbType.String;
                                paramPartNo.Size = 12;
                                paramPartNo.Value = partNo;

                                cmd.Parameters.Add(paramManifestNo);
                                cmd.Parameters.Add(paramPartNo);
                                using (SQLiteDataReader rdr = cmd.ExecuteReader())
                                {
                                    if (rdr.Read())
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                    }

                                }
                            }
                            con.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                        result = false;
                    }

                    return result;
                }

        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtManifest.Text.Length > 0)
                {
                    if (XtraMessageBox.Show("Do you want to insert Problem Part??", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (GetManifestNoProblem(txtManifest.Text.Trim()))
                        {
                            DeletePerManifest(txtManifest.Text.Trim());
                        }

                        DataHelper dh = new DataHelper(txtManifest.Text.Trim());

                        dh.ManifestNo = txtManifest.Text.Trim();
                        grdManifest.DataSource = dh.DataSet;
                        grdManifest.DataMember = dh.DataMember;

                        AddNewRow(txtManifest.Text.Trim());

                        int rowHandle = grdData.GetRowHandle(grdData.DataRowCount);
                        grdData.FocusedRowHandle = rowHandle;
                        grdData.FocusedColumn = gcPartNo;
                        grdData.ShowEditor();
                        lblError.Text = "Please input NG Part in grid screen";
                    }
                    else
                    {
                        List<DailyOrder> lst = new List<DailyOrder>();

                        lst.Add(new DailyOrder { 
                            ManifestNo = txtManifest.Text.Trim(), 
                            CreatedBy = Username 
                        });

                        if (!GetManifestNoProblem(txtManifest.Text.Trim()))
                        {
                            InsertData(lst);
                            txtManifest.Text = "";
                            txtManifest.Focus();
                            lblError.Text = "Succes Receive Manifest No without NG Part";
                        }
                        else
                        {
                            txtManifest.Text = "";
                            txtManifest.Focus();
                            lblError.Text = "Manifest has already been received without NG Part";
                        }
                        
                    }
                }
            }
        }

        private void AddNewRow(string manifestNo)
        {
            if (!string.IsNullOrEmpty(manifestNo))
            {
                grdData.AddNewRow();

                int rowHandle = grdData.GetRowHandle(grdData.DataRowCount);
                if (grdData.IsNewItemRow(rowHandle))
                {

                    grdData.SetRowCellValue(rowHandle, gcManifestNo, manifestNo);
                    grdData.SetRowCellValue(rowHandle, gcPartNo, string.Empty);
                    grdData.SetRowCellValue(rowHandle, gcShortage, 0);
                    grdData.SetRowCellValue(rowHandle, gcMisspartQty, 0);
                    grdData.SetRowCellValue(rowHandle, gcDamageQty, 0);
                }
            }
            else
            {
                lblError.Text = "Please fill Manifest No!!";
            }
        }

        #region " Check Online Status "
        private void tmrOnlineStatus_Tick(object sender, EventArgs e)
        {
            lblCurrenTime.Text = System.DateTime.Now.ToString("hh:mm:ss");

            if (PingNetwork(IPAddressIPPCS))
            {
                pnlStatus.BackColor = Color.Lime;
                lblStatus.Text = "Online";
                lblStatus.ForeColor = Color.Black;
                if (countdown < 0) countdown = 20;
                tmrExecuteWS.Start();
                tmrCountDown.Start();
                ControlInForm(false);
            }
            else
            {
                pnlStatus.BackColor = Color.Maroon;
                lblStatus.Text = "Offline";
                lblStatus.ForeColor = Color.Yellow;
                tmrExecuteWS.Stop();
                tmrCountDown.Stop();
                countdown = 20;
                ControlInForm(true);
                HasBeenExecuted = false;
            }
        }

        private bool PingNetwork(string hostNameOrAddress)
        {
            bool pingStatus = false;

            using (Ping p = new Ping())
            {
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 120;

                try
                {
                    PingReply reply = p.Send(hostNameOrAddress, timeout, buffer);
                    pingStatus = (reply.Status == IPStatus.Success);
                }
                catch (Exception)
                {
                    pingStatus = false;
                }
            }

            return pingStatus;
        }
        #endregion
        
        private void InitValidationRules()
        {
            ConditionValidationRule cvr = new ConditionValidationRule();

            cvr.ConditionOperator = ConditionOperator.IsNotBlank;
            cvr.ErrorText = "Value Cannot be null...";

            dxValidate.SetValidationRule(txtManifest, cvr);
        }

        #region " Insert into SQLite "
        private void cmdReceive_Click(object sender, EventArgs e)
        {
            List<DailyOrder> lst = new List<DailyOrder>();
            if (grdData.RowCount > 0)
            {
                int rowHandle; 
                int isProblem = 0;
                int haveProblem = 0;

                string manifestNo = string.Empty;
                string partNo = string.Empty;
                for (int i = 0; i < grdData.RowCount; i++)
                {
                    rowHandle = grdData.GetRowHandle(i);
                    isProblem += (int)grdData.GetRowCellValue(rowHandle, gcShortage) + (int)grdData.GetRowCellValue(rowHandle, gcMisspartQty) + (int)grdData.GetRowCellValue(rowHandle, gcDamageQty);
                    haveProblem = (int)grdData.GetRowCellValue(rowHandle, gcShortage) + (int)grdData.GetRowCellValue(rowHandle, gcMisspartQty) + (int)grdData.GetRowCellValue(rowHandle, gcDamageQty);

                    manifestNo =grdData.GetRowCellValue(rowHandle, gcManifestNo).ToString();
                    partNo = grdData.GetRowCellValue(rowHandle, gcPartNo).ToString();

                    if (!string.IsNullOrEmpty(partNo) || partNo.Trim().Length > 0)
                    {
                        lst.Add(new DailyOrder
                        {
                            ManifestNo = manifestNo,
                            ProblemFlag = haveProblem != 0 ? true : false,
                            PartNo = partNo,
                            ShortageQty = (int)grdData.GetRowCellValue(rowHandle, gcShortage),
                            MisspartQty = (int)grdData.GetRowCellValue(rowHandle, gcMisspartQty),
                            DamageQty = (int)grdData.GetRowCellValue(rowHandle, gcDamageQty),
                            CreatedBy = Username
                        });
                    }
                }

                if (isProblem == 0)
                {
                    if (XtraMessageBox.Show("Receive Manifest without NG Part recorded?", "Information", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        InsertData(lst);
                        ClearGrid();
                        lblError.Text = "Receive Manifest without NG Part recorded";
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    InsertData(lst);
                    ClearGrid();
                }
            }
            else
            {
                lst = new List<DailyOrder>();
                lst.Add(new DailyOrder
                {
                    ManifestNo = txtManifest.Text.Trim(),
                    ProblemFlag = false,
                    PartNo = string.Empty,
                    ShortageQty = 0,
                    MisspartQty = 0,
                    DamageQty = 0,
                    CreatedBy = Username
                });

                InsertData(lst);
            }
        }

        private void InsertData(List<DailyOrder> datasource)
        {
            string sqlInsert = @"insert into DailyOrder (
                                        ManifestNo, 
                                        PartNo, 
                                        ProblemFlag, 
                                        ShortageQty, 
                                        MisspartQty, 
                                        DamageQty, 
                                        CreatedBy, 
                                        CreatedDt)
                                        values (
                                        ?, ?, ?, ?, ?, ?, ?, ?)";

            try
            {

                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();
                    using (SQLiteTransaction mytransaction = con.BeginTransaction())
                    {
                        foreach (DailyOrder daily in datasource)
                        {
                            
                            if (!CheckManifestPart(daily.ManifestNo, daily.PartNo))
                            {
                                using (SQLiteCommand cmd = new SQLiteCommand(con))
                                {
                                    SQLiteParameter manifestNo = new SQLiteParameter();
                                    SQLiteParameter partNo = new SQLiteParameter();
                                    SQLiteParameter problemFlag = new SQLiteParameter();
                                    SQLiteParameter shortageQty = new SQLiteParameter();
                                    SQLiteParameter misspartQty = new SQLiteParameter();
                                    SQLiteParameter damageQty = new SQLiteParameter();
                                    SQLiteParameter createdDt = new SQLiteParameter();
                                    SQLiteParameter createdBy = new SQLiteParameter();

                                    manifestNo.DbType = DbType.String;
                                    manifestNo.Size = 16;
                                    manifestNo.Value = daily.ManifestNo;

                                    partNo.DbType = DbType.String;
                                    partNo.Size = 12;
                                    partNo.Value = daily.PartNo;

                                    problemFlag.DbType = DbType.Boolean;
                                    problemFlag.Value = daily.ProblemFlag;

                                    misspartQty.DbType = DbType.Int32;
                                    misspartQty.Value = daily.MisspartQty;

                                    shortageQty.DbType = DbType.Int32;
                                    shortageQty.Value = daily.ShortageQty;

                                    damageQty.DbType = DbType.Int32;
                                    damageQty.Value = daily.DamageQty;

                                    createdBy.DbType = DbType.String;
                                    createdBy.Value = daily.CreatedBy;

                                    createdDt.DbType = DbType.Date;
                                    createdDt.Value = DateTime.Now;

                                    cmd.CommandText = sqlInsert;

                                    cmd.Parameters.Add(manifestNo);
                                    cmd.Parameters.Add(partNo);
                                    cmd.Parameters.Add(problemFlag);
                                    cmd.Parameters.Add(shortageQty);
                                    cmd.Parameters.Add(misspartQty);
                                    cmd.Parameters.Add(damageQty);
                                    cmd.Parameters.Add(createdBy);
                                    cmd.Parameters.Add(createdDt);

                                    cmd.ExecuteNonQuery();
                                }
                            }
                            else
                            {
                                using (SQLiteCommand cmd = new SQLiteCommand(con))
                                {
                                    SQLiteParameter manifestNo = new SQLiteParameter();
                                    SQLiteParameter partNo = new SQLiteParameter();
                                    SQLiteParameter problemFlag = new SQLiteParameter();
                                    SQLiteParameter shortageQty = new SQLiteParameter();
                                    SQLiteParameter misspartQty = new SQLiteParameter();
                                    SQLiteParameter damageQty = new SQLiteParameter();
                                    SQLiteParameter createdDt = new SQLiteParameter();
                                    SQLiteParameter createdBy = new SQLiteParameter();

                                    manifestNo.DbType = DbType.String;
                                    manifestNo.Size = 16;
                                    manifestNo.Value = daily.ManifestNo;

                                    partNo.DbType = DbType.String;
                                    partNo.Size = 12;
                                    partNo.Value = daily.PartNo;

                                    problemFlag.DbType = DbType.Boolean;
                                    problemFlag.Value = daily.ProblemFlag;

                                    misspartQty.DbType = DbType.Int32;
                                    misspartQty.Value = daily.MisspartQty;

                                    shortageQty.DbType = DbType.Int32;
                                    shortageQty.Value = daily.ShortageQty;

                                    damageQty.DbType = DbType.Int32;
                                    damageQty.Value = daily.DamageQty;

                                    createdBy.DbType = DbType.String;
                                    createdBy.Value = daily.CreatedBy;

                                    createdDt.DbType = DbType.Date;
                                    createdDt.Value = DateTime.Now;

                                    cmd.CommandText = @"UPDATE DailyOrder Set 
                                        ProblemFlag=?, 
                                        ShortageQty=?, 
                                        MisspartQty=?, 
                                        DamageQty=?, 
                                        CreatedBy=?, 
                                        CreatedDt=? WHERE ManifestNo=? AND PartNo=?";

                                    
                                    cmd.Parameters.Add(problemFlag);
                                    cmd.Parameters.Add(shortageQty);
                                    cmd.Parameters.Add(misspartQty);
                                    cmd.Parameters.Add(damageQty);
                                    cmd.Parameters.Add(createdBy);
                                    cmd.Parameters.Add(createdDt);
                                    cmd.Parameters.Add(manifestNo);
                                    cmd.Parameters.Add(partNo);

                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        mytransaction.Commit();
                    }
                    con.Close();
                }

            }
            catch (Exception exinsert)
            {
                lblError.Text = "Error while executed doinsert Eventhandler" + Environment.NewLine + exinsert.Message + exinsert.Source + exinsert.StackTrace + exinsert.TargetSite;
            }
        }

        private void DeletePerManifest(string _manifestNo)
        {
            string sqlDelete = @"DELETE FROM DailyOrder WHERE ManifestNo = ?";

            try
            {

                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();
                    using (SQLiteTransaction mytransaction = con.BeginTransaction())
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(con))
                        {
                            SQLiteParameter manifestNo = new SQLiteParameter();

                            manifestNo.DbType = DbType.String;
                            manifestNo.Size = 16;
                            manifestNo.Value = _manifestNo;                           

                            cmd.CommandText = sqlDelete;

                            cmd.Parameters.Add(manifestNo);

                            cmd.ExecuteNonQuery();
                        }
                        mytransaction.Commit();
                    }
                    con.Close();
                }

            }
            catch (Exception exinsert)
            {
                lblError.Text = "Error while executed doinsert Eventhandler" + Environment.NewLine + exinsert.Message + exinsert.Source + exinsert.StackTrace + exinsert.TargetSite;
            }
        }

        private void DeleteData(DailyOrder daily)
        {
            string sqlInsert = @"DELETE FROM DailyOrder WHERE ManifestNo = ? AND PartNo = ?";

            try
            {

                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();
                    using (SQLiteTransaction mytransaction = con.BeginTransaction())
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(con))
                        {
                            SQLiteParameter manifestNo = new SQLiteParameter();
                            SQLiteParameter partNo = new SQLiteParameter();
                            SQLiteParameter problemFlag = new SQLiteParameter();
                            SQLiteParameter shortageQty = new SQLiteParameter();
                            SQLiteParameter misspartQty = new SQLiteParameter();
                            SQLiteParameter damageQty = new SQLiteParameter();
                            SQLiteParameter createdDt = new SQLiteParameter();
                            SQLiteParameter createdBy = new SQLiteParameter();

                            manifestNo.DbType = DbType.String;
                            manifestNo.Size = 16;
                            manifestNo.Value = daily.ManifestNo;

                            partNo.DbType = DbType.String;
                            partNo.Size = 12;
                            partNo.Value = daily.PartNo;

                            problemFlag.DbType = DbType.Boolean;
                            problemFlag.Value = daily.ProblemFlag;

                            misspartQty.DbType = DbType.Int32;
                            misspartQty.Value = daily.MisspartQty;

                            shortageQty.DbType = DbType.Int32;
                            shortageQty.Value = daily.ShortageQty;

                            damageQty.DbType = DbType.Int32;
                            damageQty.Value = daily.DamageQty;

                            createdBy.DbType = DbType.String;
                            createdBy.Value = daily.CreatedBy;

                            createdBy.DbType = DbType.Date;
                            createdBy.Value = System.DateTime.Now.ToString("dd.MM.yyyy");

                            cmd.CommandText = sqlInsert;

                            cmd.Parameters.Add(manifestNo);
                            cmd.Parameters.Add(problemFlag);
                            cmd.Parameters.Add(partNo);
                            cmd.Parameters.Add(misspartQty);
                            cmd.Parameters.Add(shortageQty);
                            cmd.Parameters.Add(damageQty);
                            cmd.Parameters.Add(createdBy);
                            cmd.Parameters.Add(createdDt);

                            cmd.ExecuteNonQuery();
                        }
                        mytransaction.Commit();
                    }
                    con.Close();
                }

            }
            catch (Exception exinsert)
            {
                lblError.Text = "Error while executed doinsert Eventhandler" + Environment.NewLine + exinsert.Message + exinsert.Source + exinsert.StackTrace + exinsert.TargetSite;
            }
        }

        private void UpdateData(DailyOrder daily)
        {
            string sqlInsert = @"UPDATE DailyOrder SET IsExecuted='true' WHERE ManifestNo = ? AND PartNo = ?";

            try
            {

                using (SQLiteConnection con = new SQLiteConnection(ConnString))
                {
                    con.Open();
                    using (SQLiteTransaction mytransaction = con.BeginTransaction())
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(con))
                        {
                            SQLiteParameter manifestNo = new SQLiteParameter();
                            SQLiteParameter partNo = new SQLiteParameter();

                            manifestNo.DbType = DbType.String;
                            manifestNo.Size = 16;
                            manifestNo.Value = daily.ManifestNo;

                            partNo.DbType = DbType.String;
                            partNo.Size = 12;
                            partNo.Value = daily.PartNo;

                            cmd.CommandText = sqlInsert;

                            cmd.Parameters.Add(manifestNo);
                            cmd.Parameters.Add(partNo);

                            cmd.ExecuteNonQuery();
                        }
                        mytransaction.Commit();
                    }
                    con.Close();
                }

            }
            catch (Exception exinsert)
            {
                lblError.Text = "Error while executed doinsert Eventhandler" + Environment.NewLine + exinsert.Message + exinsert.Source + exinsert.StackTrace + exinsert.TargetSite;
            }
        }
        #endregion

        private void repPartNo_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    int rowHandle = grdData.GetRowHandle(grdData.DataRowCount);

            //    string manifestno = grdData.GetRowCellValue(rowHandle, gcManifestNo).ToString();
            //    string partno = grdData.GetRowCellDisplayText(rowHandle, gcPartNo).ToString();
            //    if (CheckManifestPart(manifestno, partno))
            //    {
            //        string msg = string.Format("PartNo {0} with Manifest No {1}, already exist", partno, manifestno);
            //        XtraMessageBox.Show(msg, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        grdData.FocusedRowHandle = rowHandle;
            //        grdData.FocusedColumn = gcManifestNo;
            //        grdData.ShowEditor();
            //    }
            //}
        }

        private void grdData_KeyDown(object sender, KeyEventArgs e)
        {
            /* when delete is pressed */
            if (e.KeyCode == Keys.Delete)
            {                
                int rowHandle = grdData.GetRowHandle(grdData.DataRowCount);

                grdData.DeleteRow(rowHandle);
            }

            /* when esc is pressed */
            if (e.KeyCode == Keys.Escape)
            {
                txtManifest.Text = string.Empty;
                txtManifest.Focus();
                ClearGrid();
            }
        }

        private void tmrExecuteWS_Tick(object sender, EventArgs e)
        {
            if (!HasBeenExecuted)
            {
                using (WSIPPCS.Service service = new WSIPPCS.Service())
                {
                    string un = string.Empty;
                    List<string> lstParam;// = new List<string>();
                    List<object> paramProblem;
                    List<object> paramManifest;
                    List<DailyOrder> lstDO = GetDistinctManifest();
                    int isProblem = 0;

                    foreach (DailyOrder _do in lstDO)
                    {
                        //isProblem = 0;
                        foreach (DailyOrder dailyorder in GetManifest(_do.ManifestNo))
                        {
                            un = dailyorder.CreatedBy;
                            isProblem += dailyorder.DamageQty + dailyorder.MisspartQty + dailyorder.ShortageQty;
                            lstParam = new List<string>();
                            lstParam.Add(dailyorder.ManifestNo);
                            lstParam.Add(dailyorder.CreatedBy);

                            if (dailyorder.ProblemFlag)
                            {

                                paramProblem = new List<object>();


                                paramProblem.Add(dailyorder.ManifestNo);
                                paramProblem.Add(dailyorder.PartNo);
                                paramProblem.Add(dailyorder.CreatedBy);
                                paramProblem.Add(dailyorder.ShortageQty.ToString());
                                paramProblem.Add(dailyorder.MisspartQty.ToString());
                                paramProblem.Add(dailyorder.DamageQty.ToString());
                                paramProblem.Add(dailyorder.CreatedDt);
                                service.InsertProblemManifest(paramProblem.ToArray());

                            }
                            UpdateData(dailyorder);
                        }

                        paramManifest = new List<object>();
                        paramManifest.Add(_do.ManifestNo);
                        paramManifest.Add(un);
                        paramManifest.Add(DateTime.Now);

                        //int problemManFlag = service.ManifestProblemFLag(_do.ManifestNo);

                        if (isProblem == 0)
                        {
                            service.UpdateManifest(paramManifest.ToArray());
                        }
                        else
                        {
                            service.UpdateManifestProblem(paramManifest.ToArray());
                        }
                    }
                }

                lblError.Text = "Data offline has been uploaded to the server";
                tmrCountDown.Stop();
                HasBeenExecuted = true;

            }
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            AddNewRow(txtManifest.Text);
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            int rowHandle = grdData.GetRowHandle(grdData.DataRowCount);

            grdData.DeleteRow(rowHandle);
        }

        private void ControlInForm(bool value)
        {
            //txtManifest.Enabled = value;
            //grdManifest.Enabled = value;
            //cmdAdd.Enabled = value;
            //cmdReceive.Enabled = value;

            foreach (Control ctl in this.Controls)
            {
                ctl.Enabled = value;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ControlInForm(true);
        }

        private void repDamageQty_KeyDown(object sender, KeyEventArgs e)
        {
            if ((sender is BaseEdit) && (e.KeyCode == Keys.Return))
            {
                grdData.PostEditor();

                AddNewRow(txtManifest.Text);

                int rowHandle = grdData.GetRowHandle(grdData.DataRowCount);
                grdData.FocusedRowHandle = rowHandle;
                grdData.FocusedColumn = gcManifestNo;
                grdData.ShowEditor();

            }
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                frmUser frm = new frmUser();
                frm.ShowDialog();
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void tmrCountDown_Tick(object sender, EventArgs e)
        {
            lblError.Text = "Uploading data in.." + countdown.ToString() + " second";
            countdown = countdown - 1;
        } 
    }
}