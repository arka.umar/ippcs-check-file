﻿namespace ManifestReceiving.Offline
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblCurrenTime = new DevExpress.XtraEditors.LabelControl();
            this.tmrOnlineStatus = new System.Windows.Forms.Timer(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmdReceive = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtManifest = new DevExpress.XtraEditors.TextEdit();
            this.pnlStatus = new DevExpress.XtraEditors.PanelControl();
            this.lblStatus = new DevExpress.XtraEditors.LabelControl();
            this.grdManifest = new DevExpress.XtraGrid.GridControl();
            this.grdData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcManifestNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPartNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repPartNo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcShortage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repShortageQty = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gcMisspartQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repMisspartQty = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gcDamageQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repDamageQty = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repOrderQty = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.lblError = new DevExpress.XtraEditors.LabelControl();
            this.dxValidate = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.tmrExecuteWS = new System.Windows.Forms.Timer(this.components);
            this.cmdDelete = new DevExpress.XtraEditors.SimpleButton();
            this.cmdAdd = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.tmrCountDown = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtManifest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlStatus)).BeginInit();
            this.pnlStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdManifest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPartNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repShortageQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMisspartQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDamageQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repOrderQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidate)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(465, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(216, 24);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Current Time";
            // 
            // lblCurrenTime
            // 
            this.lblCurrenTime.Appearance.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold);
            this.lblCurrenTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblCurrenTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCurrenTime.Location = new System.Drawing.Point(465, 32);
            this.lblCurrenTime.Name = "lblCurrenTime";
            this.lblCurrenTime.Size = new System.Drawing.Size(216, 63);
            this.lblCurrenTime.TabIndex = 4;
            this.lblCurrenTime.Text = "labelControl1";
            // 
            // tmrOnlineStatus
            // 
            this.tmrOnlineStatus.Enabled = true;
            this.tmrOnlineStatus.Interval = 1000;
            this.tmrOnlineStatus.Tick += new System.EventHandler(this.tmrOnlineStatus_Tick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmdReceive);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtManifest);
            this.panelControl1.Location = new System.Drawing.Point(9, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(215, 82);
            this.panelControl1.TabIndex = 6;
            // 
            // cmdReceive
            // 
            this.cmdReceive.Location = new System.Drawing.Point(123, 42);
            this.cmdReceive.LookAndFeel.SkinName = "Office 2010 Black";
            this.cmdReceive.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdReceive.Name = "cmdReceive";
            this.cmdReceive.Size = new System.Drawing.Size(75, 23);
            this.cmdReceive.TabIndex = 10;
            this.cmdReceive.Text = "&Receive";
            this.cmdReceive.Click += new System.EventHandler(this.cmdReceive_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(7, 20);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Manifest No. *";
            // 
            // txtManifest
            // 
            this.txtManifest.Location = new System.Drawing.Point(85, 16);
            this.txtManifest.Name = "txtManifest";
            this.txtManifest.Size = new System.Drawing.Size(113, 20);
            this.txtManifest.TabIndex = 0;
            this.txtManifest.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEdit1_KeyDown);
            // 
            // pnlStatus
            // 
            this.pnlStatus.Appearance.BackColor = System.Drawing.Color.Lime;
            this.pnlStatus.Appearance.Options.UseBackColor = true;
            this.pnlStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlStatus.Controls.Add(this.lblStatus);
            this.pnlStatus.Location = new System.Drawing.Point(230, 11);
            this.pnlStatus.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlStatus.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(137, 83);
            this.pnlStatus.TabIndex = 8;
            // 
            // lblStatus
            // 
            this.lblStatus.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblStatus.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lblStatus.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblStatus.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblStatus.Location = new System.Drawing.Point(0, 20);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(131, 46);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "labelControl1";
            // 
            // grdManifest
            // 
            this.grdManifest.Location = new System.Drawing.Point(9, 129);
            this.grdManifest.LookAndFeel.SkinName = "Office 2010 Black";
            this.grdManifest.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdManifest.MainView = this.grdData;
            this.grdManifest.Name = "grdManifest";
            this.grdManifest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repPartNo,
            this.repOrderQty,
            this.repShortageQty,
            this.repMisspartQty,
            this.repDamageQty});
            this.grdManifest.Size = new System.Drawing.Size(672, 257);
            this.grdManifest.TabIndex = 9;
            this.grdManifest.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdData});
            // 
            // grdData
            // 
            this.grdData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcManifestNo,
            this.gcPartNo,
            this.gcShortage,
            this.gcMisspartQty,
            this.gcDamageQty});
            this.grdData.GridControl = this.grdManifest;
            this.grdData.Name = "grdData";
            this.grdData.OptionsView.ShowDetailButtons = false;
            this.grdData.OptionsView.ShowGroupPanel = false;
            this.grdData.OptionsView.ShowIndicator = false;
            this.grdData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdData_KeyDown);
            // 
            // gcManifestNo
            // 
            this.gcManifestNo.Caption = "Manifest No";
            this.gcManifestNo.FieldName = "ManifestNo";
            this.gcManifestNo.Name = "gcManifestNo";
            this.gcManifestNo.OptionsColumn.AllowEdit = false;
            this.gcManifestNo.OptionsColumn.AllowFocus = false;
            this.gcManifestNo.OptionsColumn.AllowMove = false;
            this.gcManifestNo.Visible = true;
            this.gcManifestNo.VisibleIndex = 0;
            // 
            // gcPartNo
            // 
            this.gcPartNo.Caption = "Part No";
            this.gcPartNo.ColumnEdit = this.repPartNo;
            this.gcPartNo.FieldName = "PartNo";
            this.gcPartNo.Name = "gcPartNo";
            this.gcPartNo.OptionsColumn.AllowMove = false;
            this.gcPartNo.Visible = true;
            this.gcPartNo.VisibleIndex = 1;
            // 
            // repPartNo
            // 
            this.repPartNo.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repPartNo.AutoHeight = false;
            this.repPartNo.MaxLength = 12;
            this.repPartNo.Name = "repPartNo";
            this.repPartNo.NullValuePrompt = "Part No Cannot be null";
            this.repPartNo.NullValuePromptShowForEmptyValue = true;
            this.repPartNo.ValidateOnEnterKey = true;
            this.repPartNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.repPartNo_KeyDown);
            // 
            // gcShortage
            // 
            this.gcShortage.Caption = "Shortage Qty";
            this.gcShortage.ColumnEdit = this.repShortageQty;
            this.gcShortage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcShortage.FieldName = "ShortageQty";
            this.gcShortage.Name = "gcShortage";
            this.gcShortage.Visible = true;
            this.gcShortage.VisibleIndex = 2;
            // 
            // repShortageQty
            // 
            this.repShortageQty.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repShortageQty.AutoHeight = false;
            this.repShortageQty.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repShortageQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repShortageQty.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repShortageQty.Name = "repShortageQty";
            this.repShortageQty.NullText = "0";
            this.repShortageQty.NullValuePrompt = "Shortage Qty Cannot be null";
            this.repShortageQty.NullValuePromptShowForEmptyValue = true;
            this.repShortageQty.ValidateOnEnterKey = true;
            // 
            // gcMisspartQty
            // 
            this.gcMisspartQty.Caption = "Mispart Qty";
            this.gcMisspartQty.ColumnEdit = this.repMisspartQty;
            this.gcMisspartQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcMisspartQty.FieldName = "MisspartQty";
            this.gcMisspartQty.Name = "gcMisspartQty";
            this.gcMisspartQty.Visible = true;
            this.gcMisspartQty.VisibleIndex = 3;
            // 
            // repMisspartQty
            // 
            this.repMisspartQty.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repMisspartQty.AutoHeight = false;
            this.repMisspartQty.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repMisspartQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repMisspartQty.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repMisspartQty.Name = "repMisspartQty";
            this.repMisspartQty.NullValuePrompt = "Misspart Qty Cannot be null";
            this.repMisspartQty.NullValuePromptShowForEmptyValue = true;
            this.repMisspartQty.ValidateOnEnterKey = true;
            // 
            // gcDamageQty
            // 
            this.gcDamageQty.Caption = "Damage Qty";
            this.gcDamageQty.ColumnEdit = this.repDamageQty;
            this.gcDamageQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcDamageQty.FieldName = "DamageQty";
            this.gcDamageQty.Name = "gcDamageQty";
            this.gcDamageQty.Visible = true;
            this.gcDamageQty.VisibleIndex = 4;
            // 
            // repDamageQty
            // 
            this.repDamageQty.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repDamageQty.AutoHeight = false;
            this.repDamageQty.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repDamageQty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repDamageQty.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repDamageQty.Name = "repDamageQty";
            this.repDamageQty.NullValuePrompt = "Damage Qty Cannot be null";
            this.repDamageQty.NullValuePromptShowForEmptyValue = true;
            this.repDamageQty.ValidateOnEnterKey = true;
            this.repDamageQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.repDamageQty_KeyDown);
            // 
            // repOrderQty
            // 
            this.repOrderQty.AutoHeight = false;
            this.repOrderQty.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repOrderQty.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repOrderQty.Name = "repOrderQty";
            // 
            // lblError
            // 
            this.lblError.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblError.Appearance.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblError.Location = new System.Drawing.Point(9, 392);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(94, 13);
            this.lblError.TabIndex = 10;
            this.lblError.Text = "Error Message";
            // 
            // tmrExecuteWS
            // 
            this.tmrExecuteWS.Interval = 1000;
            this.tmrExecuteWS.Tick += new System.EventHandler(this.tmrExecuteWS_Tick);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(37, 100);
            this.cmdDelete.LookAndFeel.SkinName = "Office 2010 Black";
            this.cmdDelete.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(23, 23);
            this.cmdDelete.TabIndex = 12;
            this.cmdDelete.Text = "-";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Location = new System.Drawing.Point(9, 100);
            this.cmdAdd.LookAndFeel.SkinName = "Office 2010 Black";
            this.cmdAdd.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(23, 23);
            this.cmdAdd.TabIndex = 13;
            this.cmdAdd.Text = "+";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(384, 85);
            this.simpleButton1.LookAndFeel.SkinName = "Office 2010 Black";
            this.simpleButton1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "&Execute";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // tmrCountDown
            // 
            this.tmrCountDown.Interval = 1000;
            this.tmrCountDown.Tick += new System.EventHandler(this.tmrCountDown_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 411);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.cmdAdd);
            this.Controls.Add(this.cmdDelete);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.grdManifest);
            this.Controls.Add(this.pnlStatus);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lblCurrenTime);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.LookAndFeel.SkinName = "Office 2010 Black";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtManifest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlStatus)).EndInit();
            this.pnlStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdManifest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPartNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repShortageQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMisspartQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDamageQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repOrderQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblCurrenTime;
        private System.Windows.Forms.Timer tmrOnlineStatus;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtManifest;
        private DevExpress.XtraEditors.PanelControl pnlStatus;
        private DevExpress.XtraGrid.GridControl grdManifest;
        private DevExpress.XtraGrid.Views.Grid.GridView grdData;
        private DevExpress.XtraEditors.LabelControl lblStatus;
        private DevExpress.XtraEditors.SimpleButton cmdReceive;
        private DevExpress.XtraEditors.LabelControl lblError;
        private DevExpress.XtraGrid.Columns.GridColumn gcManifestNo;
        private DevExpress.XtraGrid.Columns.GridColumn gcPartNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repPartNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repOrderQty;
        private DevExpress.XtraGrid.Columns.GridColumn gcShortage;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repShortageQty;
        private DevExpress.XtraGrid.Columns.GridColumn gcMisspartQty;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repMisspartQty;
        private DevExpress.XtraGrid.Columns.GridColumn gcDamageQty;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repDamageQty;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidate;
        private System.Windows.Forms.Timer tmrExecuteWS;
        private DevExpress.XtraEditors.SimpleButton cmdDelete;
        private DevExpress.XtraEditors.SimpleButton cmdAdd;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Timer tmrCountDown;
    }
}