﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ManifestReceiving.Offline.Model;
using System.Data.SQLite;

namespace ManifestReceiving
{
    public class DataHelper
    {
        private static string ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteDb"].ConnectionString;

        public string ManifestNo
        {
            get { return _manifestNo; }
            set { _manifestNo = value; }
        }

        private string _manifestNo;
        
        private DataSet _DataSet;
        private string _DataMember = "DailyOrderTable";

        public DataSet DataSet
        {
            get { return _DataSet; }
            set { _DataSet = value; }
        }

        public string DataMember
        {
            get { return _DataMember; }
            set
            {
                _DataMember = value;
            }
        }

        public DataHelper(string manifestNo)
        {
            _manifestNo = manifestNo;
            MakeDailyTable();
        }

        private void MakeDailyTable()
        {
            DataTable table = new DataTable(_DataMember);
            DataColumn column;
            DataRow row;

            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "ManifestNo";
            column.AutoIncrement = false;
            column.Caption = "Manifest No";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);


            column = new DataColumn();
            column.DataType = typeof(string);
            column.ColumnName = "PartNo";
            column.AutoIncrement = false;
            column.Caption = "Part No";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(int);
            column.ColumnName = "OrderQty";
            column.AutoIncrement = false;
            column.Caption = "Order Qty";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(int);
            column.ColumnName = "ShortageQty";
            column.AutoIncrement = false;
            column.Caption = "Shortage Qty";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(int);
            column.ColumnName = "MisspartQty";
            column.AutoIncrement = false;
            column.Caption = "Misspart Qty";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = typeof(int);
            column.ColumnName = "DamageQty";
            column.AutoIncrement = false;
            column.Caption = "Damage Qty";
            column.ReadOnly = false;
            column.Unique = false;

            table.Columns.Add(column);

            DataSet = new DataSet();
            DataSet.Tables.Add(table);

            using (SQLiteConnection con = new SQLiteConnection(ConnString))
            {
                con.Open();

                string sqlQuery = "SELECT * FROM DailyOrder WHERE ManifestNo=? AND ISEXECUTED <> 'true'";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlQuery, con))
                {
                    SQLiteParameter paramManifestNo = new SQLiteParameter();

                    paramManifestNo.DbType = DbType.String;
                    paramManifestNo.Size = 16;
                    paramManifestNo.Value = _manifestNo;

                    cmd.Parameters.Add(paramManifestNo);

                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                row = table.NewRow();
                                row["ManifestNo"] = rdr["ManifestNo"].ToString();
                                row["PartNo"] = rdr["PartNo"].ToString();
                                row["ShortageQty"] = Convert.ToInt32(rdr["ShortageQty"]);
                                row["MisspartQty"] = Convert.ToInt32(rdr["MisspartQty"]);
                                row["DamageQty"] = Convert.ToInt32(rdr["DamageQty"]);
                                table.Rows.Add(row);
                            }
                        }
                    }
                }
                con.Close();
            }
            
        }

        private List<DailyOrder> GetManifest(string manifestNo)
        {
            List<DailyOrder> result = new List<DailyOrder>();
            try
            {
                
            }
            catch (Exception ex)
            {
                //lblError.Text = "Error while checking Data \n" + ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
                result = new List<DailyOrder>();
            }

            return result;
        }
    }
}
