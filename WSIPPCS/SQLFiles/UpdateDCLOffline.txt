-- niit.mulki 1 mar 2013 


declare @@DELIVERY_NO varchar(50) = @0,
	@@dock_cd varchar(50) = @1,
	@@arrival_actual_dt varchar(50) = @2,
	@@departure_actual_dt varchar(50) = @3,
	@@UserLogin varchar(50)= @4

if not exists(select '' from TB_R_DELIVERY_CTL_D where DELIVERY_NO = @@DELIVERY_NO and DOCK_CD = @@dock_cd)
BEGIN
	INSERT INTO TB_R_DELIVERY_CTL_SYNC_OFFLINE(
		DELIVERY_NO,DOCK_CD,ARRIVAL_ACTUAL_DT,DEPARTURE_ACTUAL_DT,
		CREATED_BY,CREATED_DT,CHANGED_BY,CHANGED_DT)
	SELECT @@DELIVERY_NO,@@dock_cd,@@arrival_actual_dt,case when @@departure_actual_dt='' then null else @@departure_actual_dt end,
		@@UserLogin,GETDATE(),null,null
END
ELSE
BEGIN
	if exists(select '' from TB_R_DELIVERY_CTL_D where DELIVERY_NO = @@DELIVERY_NO and DOCK_CD = @@dock_cd
	 and arrival_actual_dt is null and departure_actual_dt is null)
	BEGIN
		-- Update data Arrival & Departurenya , karena sudah arrived dan departed
		Update dbo.TB_R_DELIVERY_CTL_D
		Set 
			arrival_actual_dt = @@arrival_actual_dt,
			Arrival_Status = 
				(
				case 
				when DATEDIFF(MINUTE, [ARRIVAL_PLAN_DT], @@arrival_actual_dt) <= 0 
				then 3 --Ontime
				else 1  --Delay
				end
				),
			Arrival_Gap = 
				(
				case when @@arrival_actual_dt IS not null 
				then 
					case 
					when @@arrival_actual_dt <= ARRIVAL_PLAN_DT 
					then 0 
					else @@arrival_actual_dt -  [ARRIVAL_PLAN_DT] 
					end
				when @@arrival_actual_dt IS NULL 
				then ARRIVAL_GAP
				end
				),
			departure_actual_dt = case when @@departure_actual_dt='' then null else @@departure_actual_dt end,		
			Departure_Status = case when @@departure_actual_dt='' then Departure_Status else
						(
 						case 
						when DATEDIFF(MINUTE, [DEPARTURE_PLAN_DT], @@departure_actual_dt) <= 0 
						then 3 --Ontime
						else 1  --Delay
						end
						)
					end,
			Departure_Gap = case when @@departure_actual_dt='' then Departure_Gap else
						(
						 case when @@departure_actual_dt IS not null 
						 then 
							case 
							when @@departure_actual_dt <= [DEPARTURE_PLAN_DT] 
							then 0 
							else @@departure_actual_dt - [DEPARTURE_PLAN_DT]
							end
						when @@departure_actual_dt IS NULL 
						then DEPARTURE_GAP
						end
						)
					end,
			CHANGED_BY=@@UserLogin,
			CHANGED_DT=GETDATE()
		Where
			dock_cd = @@dock_cd
			AND delivery_no = @@DELIVERY_NO
	END

	if exists (select '' from TB_R_DELIVERY_CTL_D where DELIVERY_NO = @@DELIVERY_NO and DOCK_CD = @@dock_cd
	 and arrival_actual_dt is  NOT null and departure_actual_dt is null)
	BEGIN
		-- Update data Departurenya saja, karena sudah arrived
		Update dbo.TB_R_DELIVERY_CTL_D
		Set 
			departure_actual_dt = case when @@departure_actual_dt='' then null else @@departure_actual_dt end,
 			Departure_Status = case when @@departure_actual_dt='' then Departure_Status else
						(
 						case 
						when DATEDIFF(MINUTE, [DEPARTURE_PLAN_DT], @@departure_actual_dt) <= 0 
						then 3 --Ontime
						else 1  --Delay
						end
						)
					end,
			Departure_Gap = case when @@departure_actual_dt='' then Departure_Gap else
						(
						 case when @@departure_actual_dt IS not null 
						 then 
							case 
							when @@departure_actual_dt <= [DEPARTURE_PLAN_DT] 
							then 0 
							else @@departure_actual_dt - [DEPARTURE_PLAN_DT]
							end
						when @@departure_actual_dt IS NULL 
						then DEPARTURE_GAP
						end
						)
					end,
			CHANGED_BY=@@UserLogin,
			CHANGED_DT=GETDATE()
		Where
			dock_cd = @@dock_cd
		AND	delivery_no = @@DELIVERY_NO
	END

	if not exists(select '' from TB_R_DELIVERY_CTL_D where DELIVERY_NO=@@DELIVERY_NO and DEPARTURE_ACTUAL_DT is null)
	begin
		Update [TB_R_DELIVERY_CTL_H] set
			DELIVERY_STS='Delivered',
			CHANGED_BY = @@UserLogin,
			CHANGED_DT = getdate()
		where DELIVERY_NO=@@DELIVERY_NO
	end
	else
	begin
		Update [TB_R_DELIVERY_CTL_H] set
			DELIVERY_STS='Arrived',
			CHANGED_BY = @@UserLogin,
			CHANGED_DT = getdate()
		where DELIVERY_NO=@@DELIVERY_NO
	end
END