﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Toyota.DataAccess;
using Toyota.DataAccess.Manifest;

namespace WSIPPCS
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        DCLRepository dcl;
        ManifestRepository mnfst;

        public Service()
        {
            dcl = new DCLRepository();
            mnfst = new ManifestRepository();
        }

        [WebMethod]
        public int UpdateDCLOffline(List<string> args)
        {
            return dcl.UpdateDCL(args);
        }

        [WebMethod]
        public Nullable<DateTime> GetDataDCL(List<string> args)
        {
            return dcl.GetDCL(args);
        }

        [WebMethod]
        public void SendEmailNotification(List<string> args)
        {
            dcl.SendEmailNotification(args);
        }

        [WebMethod]
        public void InsertProblemManifest(List<object> args)
        {
            mnfst.InsertProblemManifest(args);
        }

        [WebMethod]
        public void UpdateManifest(List<object> args)
        {
            mnfst.UpdateManifest(args);
        }

        [WebMethod]
        public void UpdateManifestProblem(List<object> args)
        {
            mnfst.UpdateManifestProblem(args);
        }

        public void UpdateManifestProblemReceived(List<object> args)
        {
            mnfst.UpdateManifestProblemReceived(args);
        }

        [WebMethod]
        public int ManifestProblemFLag(string args)
        {
            return mnfst.ManifestProblemFLag(args);
        }
    }
}