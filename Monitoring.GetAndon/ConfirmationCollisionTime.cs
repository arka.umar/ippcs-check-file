﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitoring.GetAndon
{
    public partial class ConfirmationCollisionTime : Form
    {
        public ConfirmationCollisionTime()
        {
            InitializeComponent();
        }

        private void buttonYes_Click(object sender, EventArgs e)
        {
            AndonParameter.GetInstance().Clear();
            AndonParameter.GetInstance().Add("UserID", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            AndonParameter.GetInstance().Add("Dock", labelDock.Text);
            AndonParameter.GetInstance().Add("Station", labelStation.Text);
            AndonParameter.GetInstance().Add("IntervalUpdate", labelInterval.Text);
            AndonParameter.GetInstance().Add("CollisionFlag", "1");

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void buttonNo_Click(object sender, EventArgs e)
        {
            AndonParameter.GetInstance().Clear();
            AndonParameter.GetInstance().Add("UserID", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            AndonParameter.GetInstance().Add("Dock", labelDock.Text);
            AndonParameter.GetInstance().Add("Station", labelStation.Text);
            AndonParameter.GetInstance().Add("IntervalUpdate", labelInterval.Text);
            AndonParameter.GetInstance().Add("CollisionFlag", "0");

            this.Close();
        }

        private void ConfirmationCollisionTime_Load(object sender, EventArgs e)
        {
            IParameter param = Andon.GetInstance().GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            if (param != null)
            {
                labelDock.Text = param.Get("Dock");
                labelStation.Text = param.Get("Station");
                labelInterval.Text = param.Get("IntervalUpdate");
            }
        }

    }
}
