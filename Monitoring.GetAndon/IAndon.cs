﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitoring.GetAndon
{
    internal interface IAndon
    {
        T CheckOnLoadData<T>();
        T CheckLatestData<T>();
        T GetLatestData<T>();
        int setLatestDataConfirm(IParameter param);
        int setAbandonFlag(IParameter param);
        IParameter GetParam(string UserID);
        int UpdateParam(IParameter param);
        int InsertParam(IParameter param);
    }
}
