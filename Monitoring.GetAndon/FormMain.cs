﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace Monitoring.GetAndon
{
    public partial class FormMain : Form
    {
        Timer MarqueeTimer = new Timer();
        
        public FormMain()
        {
            InitializeComponent();
            MarqueeTimer.Interval = 150;
            MarqueeTimer.Enabled = false;
            MarqueeTimer.Tick += new EventHandler(MarqueeUpdate);
        }

        private IAndon andon;

        private void FormMain_Load(object sender, EventArgs e)
        {
      
            this.Text = "Monitoring Andon" + " - " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            andon = AndonFactory.CreateAndon();
            if (andon.GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name) == null)
            {
                InputParameters input = new InputParameters();
                if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    andon.InsertParam(AndonParameter.GetInstance());
                    timerLatestUpdate.Interval = Convert.ToInt32(andon.GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name).Get("IntervalUpdate"));
                }
            }
            else
            {
                timerLatestUpdate.Interval = Convert.ToInt32(andon.GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name).Get("IntervalUpdate"));
            }

            DCLReceivingModel data = andon.GetLatestData<DCLReceivingModel>();
            _Refresh(data);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            timerLatestUpdate.Start();
        }

        private void timerLatestUpdate_Tick(object sender, EventArgs e)
        {
            GetLastUpdate(0);
        }

        private void GetLastUpdate( int intRefresh)
        {
            try
            {
                DCLReceivingCheckModel checkData = andon.CheckLatestData<DCLReceivingCheckModel>();
                if (checkData.COUNTDATA > 1)
                {
                    timerLatestUpdate.Enabled = false;

                    //set parameter
                    string interval = string.Empty;
                    IParameter param = Andon.GetInstance().GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                    if (param != null)
                    {
                        interval = param.Get("IntervalUpdate");
                    }

                    AndonParameter.GetInstance().Clear();
                    AndonParameter.GetInstance().Add("UserID", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                    AndonParameter.GetInstance().Add("Dock", labelDock.Text);
                    AndonParameter.GetInstance().Add("Station", labelTS.Text);
                    AndonParameter.GetInstance().Add("IntervalUpdate", interval);

                    //--- Trigger running text
                    MarqueeTimer.Enabled = true;
                    lblMarquee.BackColor = System.Drawing.Color.Red;
                    //--- End running text

                    if (intRefresh == 1)
                    {
                        //---=== Update abandon flag and go to next screen
                        AndonParameter.GetInstance().Add("CollisionFlag", "1");
                        andon.setAbandonFlag(AndonParameter.GetInstance());
                        //timerLatestUpdate.Enabled = true;
                    }
                    timerLatestUpdate.Enabled = true;
                }
                else 
                {
                    //--- Trigger running text
                    MarqueeTimer.Enabled = false;
                    lblMarquee.BackColor = System.Drawing.Color.White;
                    //--- End running text
                }

                DCLReceivingModel data = andon.GetLatestData<DCLReceivingModel>();
                _Refresh(data);
            }
            catch (Exception ex)
            {
                timerLatestUpdate.Enabled = false;
                MessageBox.Show(ex.Message);
                Application.Exit();
            }
        }

        private void _Refresh(DCLReceivingModel data)
        {
            double hourGap = 0;
            double minuteGap = 0;
            if (data != null)
            {

                labelTS.Text = data.STATION;
                labelDock.Text = data.DOCK_CD;
                labelRemainingTime.Text = data.REMAINING;
                labelTime.Text = DateTime.Now.ToString("HH:mm");
                labelRoute.Text = data.ROUTE;
                labelRate.Text = data.RATE;
                labelArea.Text = data.REVISE_NO.ToString();
                
                //labelArrivalPlan.Text = data.ARRIVAL_PLAN_DT.ToShortTimeString();
                //labelArrivalActual.Text = data.ARRIVAL_ACTUAL_DT.ToShortTimeString();
                //labelArrivalGap.Text = new DateTime(data.ARRIVAL_GAP.Ticks).ToString("hh:mm");
                //labelDeparturePlan.Text = data.DEPARTURE_PLAN_DT.ToShortTimeString();
                //labelDepartureActual.Text = data.DEPARTURE_ACTUAL_DT.ToString() == "1/1/1900 12:00:00 AM" ? "" : data.DEPARTURE_ACTUAL_DT.ToShortTimeString();
                //labelDepartureGap.Text = data.DEPARTURE_GAP.ToString() == "00:00:00" ? "" : new DateTime(data.DEPARTURE_GAP.Ticks).ToString("hh:mm");

                labelArrivalPlan.Text = data.ARRIVAL_PLAN_DT.ToString("HH:mm");
                labelArrivalActual.Text = data.ARRIVAL_ACTUAL_DT.ToString("HH:mm");
                //labelArrivalGap.Text = new DateTime(data.ARRIVAL_GAP.Ticks).ToString("HH:mm");
                //---=== Modify By Rahmat
                minuteGap = Math.Floor((data.ARRIVAL_ACTUAL_DT - data.ARRIVAL_PLAN_DT).TotalMinutes);
                hourGap = Math.Floor(minuteGap / 60);
                minuteGap = minuteGap - (hourGap * 60);
                labelArrivalGap.Text = ((hourGap < 10) ? "0" + hourGap.ToString() : hourGap.ToString()) + ":" + ((minuteGap < 10) ? "0" + minuteGap.ToString() : minuteGap.ToString());

                labelDeparturePlan.Text = data.DEPARTURE_PLAN_DT.ToString("HH:mm");
                labelDepartureActual.Text = data.DEPARTURE_ACTUAL_DT.ToString() == "1/1/1900 12:00:00 AM" ? "" : data.DEPARTURE_ACTUAL_DT.ToString("HH:mm");
                //labelDepartureGap.Text = data.DEPARTURE_GAP.ToString() == "00:00:00" ? "" : new DateTime(data.DEPARTURE_GAP.Ticks).ToString("HH:mm");
                
                //---=== Modify By Rahmat
                if (data.DEPARTURE_ACTUAL_DT.ToString() == "1/1/1900 12:00:00 AM")
                {
                    labelDepartureGap.Text = "";
                }
                else
                {
                    minuteGap = Math.Floor((data.DEPARTURE_ACTUAL_DT - data.DEPARTURE_PLAN_DT).TotalMinutes);
                    hourGap = Math.Floor(minuteGap / 60);
                    minuteGap = minuteGap - (hourGap * 60);
                    labelDepartureGap.Text = ((hourGap < 10) ? "0" + hourGap.ToString() : hourGap.ToString()) + ":" + ((minuteGap < 10) ? "0" + minuteGap.ToString() : minuteGap.ToString());
                }
                //labelArrivalGap.Text = ((hourGap < 10) ? "0" + hourGap.ToString() : hourGap.ToString()) + ":" + ((minuteGap < 10) ? "0" + minuteGap.ToString() : minuteGap.ToString());
                
                pictureBoxArrival.ImageLocation = string.IsNullOrEmpty(data.ARRIVAL_STATUS_IMAGE) ? "" : AppDomain.CurrentDomain.BaseDirectory + data.ARRIVAL_STATUS_IMAGE.Replace("~", "").Replace("/", @"\").Substring(1);
                pictureBoxDeparture.ImageLocation = string.IsNullOrEmpty(data.DEPARTURE_STATUS_IMAGE) ? "" : AppDomain.CurrentDomain.BaseDirectory + data.DEPARTURE_STATUS_IMAGE.Replace("~", "").Replace("/", @"\").Substring(1);
                lblStatusTextArrival.Text = data.ARRIVAL_STATUS_DESC == "None" ? "" : data.ARRIVAL_STATUS_DESC;
                lblStatusTextDeparture.Text = data.DEPARTURE_STATUS_DESC == "None" ? "" : data.DEPARTURE_STATUS_DESC;
                //labelRemainingTime.Text = data.REMAINING == "00:00" ? data.REMAINING :
                //    (data.DEPARTURE_PLAN_DT.TimeOfDay - data.ARRIVAL_ACTUAL_DT.TimeOfDay).ToString(@"h\:mm");

                string totalhour = data.DEPARTURE_PLAN_DT.Subtract(DateTime.Now).TotalHours.ToString().Split('.')[0].ToString();
                string totalminute = (data.DEPARTURE_PLAN_DT.Subtract(DateTime.Now).TotalMinutes + (data.DEPARTURE_PLAN_DT.Subtract(DateTime.Now).TotalMilliseconds > 0 ? 1 : 0)).ToString().Split('.')[0].ToString();

                labelRemainingTime.Text = (data.DEPARTURE_PLAN_DT <= DateTime.Now) ? "00:00" :
                    (int.Parse(totalhour) > 9 ? "" : "0") + totalhour + ":" + (int.Parse(totalminute) > 9 ? "" : "0") + totalminute;

                if ((data.DEPARTURE_ACTUAL_DT <= data.DEPARTURE_PLAN_DT) && (data.DEPARTURE_ACTUAL_DT.ToString() != "1/1/1900 12:00:00 AM"))
                {
                    labelRemainingTime.Text = "00:00";
                }

                //if (data.ARRIVAL_ACTUAL_DT > data.ARRIVAL_PLAN_DT)
                if (data.DEPARTURE_PLAN_DT <= DateTime.Now)
                {
                    if ((data.DEPARTURE_ACTUAL_DT <= data.DEPARTURE_PLAN_DT) && (data.DEPARTURE_ACTUAL_DT.ToString() != "1/1/1900 12:00:00 AM"))
                    {
                        boxRemainingTime.BackColor = Color.Lime;
                        labelRemainingTime.BackColor = Color.Lime;
                        lblMinutes.BackColor = Color.Lime;
                    }
                    else
                    {
                        boxRemainingTime.BackColor = Color.Red;
                        labelRemainingTime.BackColor = Color.Red;
                        lblMinutes.BackColor = Color.Red;    
                    }
                    timerBlink.Start();
                }
                else
                {
                    boxRemainingTime.BackColor = Color.Lime;
                    labelRemainingTime.BackColor = Color.Lime;
                    lblMinutes.BackColor = Color.Lime;
                }
            }
            else
            {
                labelTS.Text = "X";
                labelDock.Text = "X";
                labelRemainingTime.Text = "XX:XX";
                labelTime.Text = "XX:X";
                labelRoute.Text = "SS:XX";
                labelRate.Text = "XX";
                labelArea.Text = "XX";
                labelArrivalPlan.Text = "XX:XX";
                labelArrivalActual.Text = "XX:XX";
                labelArrivalGap.Text = "XX:XX";
                labelDeparturePlan.Text = "XX:XX";
                labelDepartureActual.Text = "XX:XX";
                labelDepartureGap.Text = "XX:XX";
                pictureBoxArrival.ImageLocation = "";
                pictureBoxDeparture.ImageLocation = "";

                boxRemainingTime.BackColor = Color.Lime;
                labelRemainingTime.BackColor = Color.Lime;
                lblMinutes.BackColor = Color.Lime;
            }
        }

        private void buttonConfigure_Click(object sender, EventArgs e)
        {
            InputParameters input = new InputParameters();
            if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                IParameter param = AndonParameter.GetInstance();
                int ret = andon.UpdateParam(param);
                if (ret == 0)
                {
                    andon.InsertParam(param);
                }
                andon.GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            }
        }

        private void checkBoxFullScreen_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFullScreen.Checked)
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void timerSecond_Tick(object sender, EventArgs e)
        {
            labelSecond.Text = DateTime.Now.ToString("ss");
        }

        private void timerMinute_Tick(object sender, EventArgs e)
        {

        }

        private void pbRefresh_Click(object sender, EventArgs e)
        {
            GetLastUpdate(1);
        }

        private void timerBlink_Tick(object sender, EventArgs e)
        {
            if (boxRemainingTime.BackColor == Color.Red)
            {
                boxRemainingTime.BackColor = Color.Transparent;
                labelRemainingTime.BackColor = Color.Transparent;
                lblMinutes.BackColor = Color.Transparent;
            }
            else if (boxRemainingTime.BackColor == Color.Lime)
            {
                boxRemainingTime.BackColor = Color.Lime;
                labelRemainingTime.BackColor = Color.Lime;
                lblMinutes.BackColor = Color.Lime;
            }
            else
            {
                boxRemainingTime.BackColor = Color.Red;
                labelRemainingTime.BackColor = Color.Red;
                lblMinutes.BackColor = Color.Red;
            }
        }

        #region Marquee Text

        String CurrentText = "                   next truck already come, please beware                   ";

        void MarqueeUpdate(object sender, EventArgs e)
        {
            CurrentText = CurrentText.Substring(1) + CurrentText[0];
            Invalidate();
            lblMarquee.Text = CurrentText;
        }

        #endregion

    }
}
