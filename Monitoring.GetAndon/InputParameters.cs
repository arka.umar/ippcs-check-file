﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitoring.GetAndon
{
    public partial class InputParameters : Form
    {
        public InputParameters()
        {
            InitializeComponent();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxDock.Text) && !string.IsNullOrEmpty(textBoxStation.Text) && !string.IsNullOrEmpty(textBoxIntervalUpdate.Text))
            {
                AndonParameter.GetInstance().Clear();
                AndonParameter.GetInstance().Add("UserID", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                AndonParameter.GetInstance().Add("Dock", textBoxDock.Text);
                AndonParameter.GetInstance().Add("Station", textBoxStation.Text);
                AndonParameter.GetInstance().Add("IntervalUpdate", textBoxIntervalUpdate.Text);
                AndonParameter.GetInstance().Add("CollisionFlag", "0");
                
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Dock and Station cannot be empty.");
            }
        }
         
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InputParameters_Load(object sender, EventArgs e)
        {
            IParameter param = Andon.GetInstance().GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            if (param != null)
            {
                textBoxDock.Text = param.Get("Dock");
                textBoxStation.Text = param.Get("Station");
                textBoxIntervalUpdate.Text = param.Get("IntervalUpdate");
                labelConfirmationFlag.Text = param.Get("CollisionFlag");
            }
        }
         
    }
}
