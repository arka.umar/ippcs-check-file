SELECT	d.DELIVERY_NO ,
        d.STATION ,
        d.DOCK_CD ,
        h.ROUTE ,
        h.RATE ,
        h.REVISE_NO ,
        d.ARRIVAL_ACTUAL_DT ,
        d.ARRIVAL_PLAN_DT ,
        d.ARRIVAL_GAP ,
        d.ARRIVAL_STATUS ,
        ARRIVAL_STATUS_DESC = CASE d.ARRIVAL_STATUS 
							--when 2 then 'Delay Waiting'
							--when 1 then 'Delay LP'
                                WHEN 99 THEN 'ADVANCED'
                                WHEN 3 THEN 'ON TIME'
                                WHEN 2 THEN 'DELAY'
                                WHEN 1 THEN 'DELAY'
                                ELSE 'None'
                              END ,
        ARRIVAL_STATUS_IMAGE = CASE d.ARRIVAL_STATUS
                                 WHEN 99 THEN '~/Content/Images/biggreendot.png'
                                 WHEN 3 THEN '~/Content/Images/biggreendot.png'
                                 WHEN 2 THEN '~/Content/Images/bigreddot2.jpg'
                                 WHEN 1 THEN '~/Content/Images/bigreddot2.jpg'
                                 ELSE '~/Content/Images/biggraydot2.jpg'
                               END ,
        ISNULL(d.DEPARTURE_ACTUAL_DT, '') AS DEPARTURE_ACTUAL_DT ,
        d.DEPARTURE_PLAN_DT ,
        ISNULL(d.DEPARTURE_GAP, '') AS DEPARTURE_GAP ,
        d.DEPARTURE_STATUS ,
        DEPARTURE_STATUS_DESC = CASE d.DEPARTURE_STATUS 
							--when 2 then 'Delay Waiting'
							--when 1 then 'Delay LP'
                                  WHEN 99 THEN 'ADVANCED'
                                  WHEN 3 THEN 'ON TIME'
                                  WHEN 2 THEN 'DELAY'
                                  WHEN 1 THEN 'DELAY'
                                  ELSE 'None'
                                END ,
        DEPARTURE_STATUS_IMAGE = CASE d.DEPARTURE_STATUS
                                   WHEN 99 THEN '~/Content/Images/biggreendot.png'
                                   WHEN 3 THEN '~/Content/Images/biggreendot.png'
                                   WHEN 2 THEN '~/Content/Images/bigreddot2.jpg'
                                   WHEN 1 THEN '~/Content/Images/bigreddot2.jpg'
                                   ELSE '~/Content/Images/biggraydot2.jpg'
                                 END ,
        REMAINING = CASE WHEN ( DATEDIFF(SECOND, ARRIVAL_ACTUAL_DT, DEPARTURE_PLAN_DT) ) < 0 THEN '00:00'
                         ELSE RIGHT('0' + CAST(DATEDIFF(SECOND, ARRIVAL_ACTUAL_DT, DEPARTURE_PLAN_DT) / 3600 AS VARCHAR(3)), 2) + ':' + 
						 RIGHT('0' + 
							CAST(CASE WHEN RIGHT('0' + 
								CAST(( DATEDIFF(SECOND, ARRIVAL_ACTUAL_DT, DEPARTURE_PLAN_DT)
                                % 60 ) AS VARCHAR(2)),
                                2) <> 0
                                THEN ( ( DATEDIFF(SECOND,
                                ARRIVAL_ACTUAL_DT,
                                DEPARTURE_PLAN_DT)
                                % 3600 ) / 60 )
                                + 1
                                ELSE ( DATEDIFF(SECOND,
                                ARRIVAL_ACTUAL_DT,
                                DEPARTURE_PLAN_DT)
                                % 3600 ) / 60
                                END AS VARCHAR(2)),
                                2)
                    END ,
        CurrentTime = ISNULL(d.DEPARTURE_ACTUAL_DT, GETDATE()),
		ABANDON_FLAG,
		CONFIRM_FLAG

--insert into table temporary
INTO #temp
FROM    DBO.[TB_R_DELIVERY_CTL_D] d
		INNER JOIN DBO.[TB_R_DELIVERY_CTL_H] h ON d.DELIVERY_NO = h.DELIVERY_NO 
WHERE   d.[DOCK_CD] = @Dock
	AND STATION = @Station
	AND CONVERT(VARCHAR(8), ARRIVAL_ACTUAL_DT, 112) = CONVERT(VARCHAR(8), GETDATE(),112)
	AND ARRIVAL_ACTUAL_DT IS NOT NULL
	AND (ABANDON_FLAG IS NULL OR ABANDON_FLAG = '0')


--check data is waiting
DECLARE @confirm VARCHAR(10)			
SET @confirm  = (SELECT TOP 1 CONFIRM_FLAG FROM #temp
					WHERE 
						ARRIVAL_ACTUAL_DT IS NOT NULL
						AND DEPARTURE_ACTUAL_DT IS NULL
						AND (CONFIRM_FLAG = '0' OR CONFIRM_FLAG IS NULL)
					OR
						(ARRIVAL_ACTUAL_DT IS NOT NULL
						AND DEPARTURE_ACTUAL_DT IS NULL
						AND CONFIRM_FLAG = '1')
					ORDER BY CONFIRM_FLAG, ARRIVAL_ACTUAL_DT DESC)

--check data is not any ready to waiting
DECLARE @Departured INT
SET @Departured = (SELECT COUNT (1) FROM dbo.TB_R_DELIVERY_CTL_D
					WHERE DOCK_CD = @Dock
						AND STATION = @Station
						AND CONVERT(VARCHAR(8), ARRIVAL_ACTUAL_DT, 112) = CONVERT(VARCHAR(8), GETDATE(),112)
						AND ARRIVAL_ACTUAL_DT IS NOT NULL
						AND DEPARTURE_ACTUAL_DT IS NULL
						AND (ABANDON_FLAG IS NULL OR ABANDON_FLAG = '0'))

-- if truck is waiting
IF (@Departured = 0)
BEGIN
	SELECT TOP 1 * FROM #temp 
	ORDER BY DEPARTURE_ACTUAL_DT DESC
END 

--if any truck waiting n unloading
ELSE
BEGIN
	--if no one truck to wait
	IF (@confirm IS NULL)
	BEGIN
		SELECT TOP 1 * FROM #temp 
		WHERE (CONFIRM_FLAG IS NULL 
			AND DEPARTURE_ACTUAL_DT = '1900-01-01')
		ORDER BY ARRIVAL_ACTUAL_DT ASC
	END
	--if any truck to wait
	ELSE IF (@confirm = '1')
	BEGIN 
		SELECT TOP 1 * FROM #temp 
		WHERE (CONFIRM_FLAG = '1'
			AND  DEPARTURE_ACTUAL_DT = '1900-01-01')
		ORDER BY ARRIVAL_ACTUAL_DT ASC
	END
	ELSE 
	BEGIN
		SELECT TOP 1 * FROM #temp 
		WHERE CONFIRM_FLAG = '1'        
			AND DEPARTURE_ACTUAL_DT = '1900-01-01'
		ORDER BY ARRIVAL_ACTUAL_DT ASC
	END
END


--update data to have normal dataq
IF ((SELECT COUNT (1) FROM #temp) = 1 )
BEGIN
	IF ((SELECT CONFIRM_FLAG FROM #temp) = '1')
	BEGIN
		UPDATE TB_R_DELIVERY_CTL_D 
		SET CONFIRM_FLAG = NULL
		WHERE DELIVERY_NO = (SELECT TOP 1 DELIVERY_NO  FROM #temp
							ORDER BY ARRIVAL_ACTUAL_DT ASC )
			AND DOCK_CD = @Dock 
			AND STATION = @Station
	END
END


DROP TABLE #temp	