UPDATE dbo.TB_R_MONITORING_ANDON
SET Dock = @Dock, 
	Station = @Station, 
	IntervalUpdate = @IntervalUpdate 
WHERE UserID = @UserID