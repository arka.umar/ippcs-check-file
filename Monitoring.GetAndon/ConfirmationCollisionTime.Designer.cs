﻿namespace Monitoring.GetAndon
{
    partial class ConfirmationCollisionTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmationCollisionTime));
            this.labelConfirmation = new System.Windows.Forms.Label();
            this.buttonNo = new UnivercaDotNet.NetUIStyle.Button();
            this.buttonYes = new UnivercaDotNet.NetUIStyle.Button();
            this.labelDock = new System.Windows.Forms.Label();
            this.labelStation = new System.Windows.Forms.Label();
            this.labelInterval = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelConfirmation
            // 
            this.labelConfirmation.AutoSize = true;
            this.labelConfirmation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConfirmation.Location = new System.Drawing.Point(16, 9);
            this.labelConfirmation.Name = "labelConfirmation";
            this.labelConfirmation.Size = new System.Drawing.Size(91, 13);
            this.labelConfirmation.TabIndex = 0;
            this.labelConfirmation.Text = "New truck arrived";
            // 
            // buttonNo
            // 
            this.buttonNo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNo.BackColor = System.Drawing.Color.Transparent;
            this.buttonNo.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F);
            this.buttonNo.ImageButtonHot = ((System.Drawing.Bitmap)(resources.GetObject("buttonNo.ImageButtonHot")));
            this.buttonNo.ImageButtonInactive = ((System.Drawing.Bitmap)(resources.GetObject("buttonNo.ImageButtonInactive")));
            this.buttonNo.ImageButtonNormal = ((System.Drawing.Bitmap)(resources.GetObject("buttonNo.ImageButtonNormal")));
            this.buttonNo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonNo.IntervalFadeOut = 180;
            this.buttonNo.Location = new System.Drawing.Point(150, 114);
            this.buttonNo.MouseDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(187)))), ((int)(((byte)(111)))));
            this.buttonNo.MouseEnterBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(50)))));
            this.buttonNo.MouseEnterTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(180)))));
            this.buttonNo.Name = "buttonNo";
            this.buttonNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonNo.Size = new System.Drawing.Size(72, 24);
            this.buttonNo.TabIndex = 7;
            this.buttonNo.Text = "No";
            this.buttonNo.UseVisualStyleBackColor = false;
            this.buttonNo.Click += new System.EventHandler(this.buttonNo_Click);
            // 
            // buttonYes
            // 
            this.buttonYes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonYes.BackColor = System.Drawing.Color.Transparent;
            this.buttonYes.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F);
            this.buttonYes.ImageButtonHot = ((System.Drawing.Bitmap)(resources.GetObject("buttonYes.ImageButtonHot")));
            this.buttonYes.ImageButtonInactive = ((System.Drawing.Bitmap)(resources.GetObject("buttonYes.ImageButtonInactive")));
            this.buttonYes.ImageButtonNormal = ((System.Drawing.Bitmap)(resources.GetObject("buttonYes.ImageButtonNormal")));
            this.buttonYes.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonYes.IntervalFadeOut = 180;
            this.buttonYes.Location = new System.Drawing.Point(63, 114);
            this.buttonYes.MouseDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(187)))), ((int)(((byte)(111)))));
            this.buttonYes.MouseEnterBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(50)))));
            this.buttonYes.MouseEnterTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(180)))));
            this.buttonYes.Name = "buttonYes";
            this.buttonYes.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonYes.Size = new System.Drawing.Size(72, 24);
            this.buttonYes.TabIndex = 6;
            this.buttonYes.Text = "Yes";
            this.buttonYes.UseVisualStyleBackColor = false;
            this.buttonYes.Click += new System.EventHandler(this.buttonYes_Click);
            // 
            // labelDock
            // 
            this.labelDock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDock.Location = new System.Drawing.Point(83, 35);
            this.labelDock.Name = "labelDock";
            this.labelDock.Size = new System.Drawing.Size(120, 15);
            this.labelDock.TabIndex = 8;
            this.labelDock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStation
            // 
            this.labelStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStation.Location = new System.Drawing.Point(83, 56);
            this.labelStation.Name = "labelStation";
            this.labelStation.Size = new System.Drawing.Size(120, 15);
            this.labelStation.TabIndex = 9;
            this.labelStation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelInterval
            // 
            this.labelInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInterval.Location = new System.Drawing.Point(168, 56);
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.Size = new System.Drawing.Size(35, 15);
            this.labelInterval.TabIndex = 10;
            this.labelInterval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelInterval.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Dock Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Station";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(247, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Do you want to change Andon Monitoring Screen?";
            // 
            // ConfirmationCollisionTime
            // 
            this.AcceptButton = this.buttonYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 150);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelInterval);
            this.Controls.Add(this.labelStation);
            this.Controls.Add(this.labelDock);
            this.Controls.Add(this.buttonNo);
            this.Controls.Add(this.buttonYes);
            this.Controls.Add(this.labelConfirmation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ConfirmationCollisionTime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Confirmation";
            this.Load += new System.EventHandler(this.ConfirmationCollisionTime_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelConfirmation;
        private UnivercaDotNet.NetUIStyle.Button buttonNo;
        private UnivercaDotNet.NetUIStyle.Button buttonYes;
        private System.Windows.Forms.Label labelDock;
        private System.Windows.Forms.Label labelStation;
        private System.Windows.Forms.Label labelInterval;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}