﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database;
using System.Data;
using System.Configuration;
using Dapper;

namespace Monitoring.GetAndon
{
    internal class Andon : IAndon
    {
        private static readonly IAndon instance = new Andon();

        public static IAndon GetInstance()
        {
            return instance;
        }

        private IParameter Parameter { set; get; }


        public Andon()
        { }

        public T CheckOnLoadData<T>()
        {
            IEnumerable<T> result;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Query<T>("CheckOnLoadData", new
                {
                    Dock = Parameter.Get("Dock"),
                    Station = Parameter.Get("Station")
                });
            }
            if (result.Any())
            {
                return result.FirstOrDefault();
            }
            else
                return default(T);
        }

        public T CheckLatestData<T>()
        {
            IEnumerable<T> result;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Query<T>("CheckLatestData", new
                {
                    Dock = Parameter.Get("Dock"),
                    Station = Parameter.Get("Station")
                });
            }
            if (result.Any())
            {
                return result.FirstOrDefault();
            }
            else
                return default(T);
        }

        public T GetLatestData<T>()
        {
            IEnumerable<T> result;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Query<T>("GetLatestDCLReceiving", new
                {
                    Dock = Parameter.Get("Dock"),
                    Station = Parameter.Get("Station")
                });
            }
            if (result.Any())
            {
                return result.LastOrDefault();
            }
            else
                return default(T);
        }

        public int setLatestDataConfirm(IParameter param)
        {
            int result;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Execute("UpdateLatestData", new
                {
                    Dock = param.Get("Dock"),
                    Station = param.Get("Station")
                    //IntervalUpdate = param.Get("IntervalUpdate"),
                    //CollisionFlag = param.Get("CollisionFlag")
                });
            }
            return result;
        }

        public int setAbandonFlag(IParameter param)
        {
            int result;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Execute("UpdateAbandonFlag", new
                {
                    Dock = param.Get("Dock"),
                    Station = param.Get("Station")
                    //IntervalUpdate = param.Get("IntervalUpdate"),
                    //CollisionFlag = param.Get("CollisionFlag")
                });
            }
            return result;
        }

        public IDbConnection OpenConnection()
        {
            return new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["PCS"].ConnectionString);
        }
 
        public IParameter GetParam(string userID)
        {
            if (Parameter != null)
                return Parameter;
             
            IEnumerable<AndonConfiguration> result = null;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Query<AndonConfiguration>("GetConfiguration", new
                {
                    UserID = userID
                });
            }
            if (result.Any())
            {
                AndonConfiguration config = result.FirstOrDefault();
                Parameter = new AndonParameter();
                Parameter.Add("Dock", config.Dock);
                Parameter.Add("Station", config.Station);
                Parameter.Add("UserID", config.UserID);
                Parameter.Add("IntervalUpdate", config.IntervalUpdate);
                Parameter.Add("CollisionFlag", config.CollisionFlag);
                return Parameter;
            }
            else
                return null;
        }

        public int InsertParam(IParameter param)
        {
            int result;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Execute("InsertConfiguration", new
                {
                    UserID = param.Get("UserID"),
                    Dock = param.Get("Dock"),
                    Station = param.Get("Station"),
                    IntervalUpdate = param.Get("IntervalUpdate"),
                    CollisionFlag = param.Get("CollisionFlag")
                });
            }
            Parameter = null;
            return result;
        }

        public int UpdateParam(IParameter param)
        {
            int result;
            using (IQuery con = new DbContext(DatabaseConst.PCS_DB))
            {
                result = con.Execute("UpdateConfiguration", new
                {
                    UserID = param.Get("UserID"),
                    Dock = param.Get("Dock"),
                    Station = param.Get("Station"),
                    IntervalUpdate = param.Get("IntervalUpdate"),
                    CollisionFlag = param.Get("CollisionFlag")
                });
            }
            Parameter = null;
            return result;
        }
    }
}
