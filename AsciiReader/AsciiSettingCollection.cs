﻿// Decompiled with JetBrains decompiler
// Type: AsciiReader.AsciiSettingCollection
// Assembly: AsciiReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 90053FD5-681F-48BD-9587-D40F1B96E466
// Assembly location: D:\Temporary\xxx\xxx\AsciiReader.exe

using System.Collections.Generic;

namespace AsciiReader
{
  public class AsciiSettingCollection
  {
    public string DatabaseName { get; set; }

    public string TableName { get; set; }

    public string FilePath { get; set; }

    public string ApplicationExecuted { get; set; }

    public string ApplicationPath { get; set; }

    public string ApplicationParam { get; set; }

    public string function_id { get; set; }

    public string process_id { get; set; }

    public Dictionary<string, AsciiSetting> Columns { get; set; }

    public AsciiSettingCollection()
    {
      this.Columns = new Dictionary<string, AsciiSetting>();
    }
  }
}
