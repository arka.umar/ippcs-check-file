﻿// Decompiled with JetBrains decompiler
// Type: AsciiReader.AsciiSetting
// Assembly: AsciiReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 90053FD5-681F-48BD-9587-D40F1B96E466
// Assembly location: D:\Temporary\xxx\xxx\AsciiReader.exe

namespace AsciiReader
{
  public class AsciiSetting
  {
    public int Position { get; set; }

    public int Length { get; set; }

    public string DefaultValue { get; set; }

    //Added By FID.Reggy
    public string DataType { get; set; }
  }
}
