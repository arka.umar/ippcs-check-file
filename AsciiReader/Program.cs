﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml;

namespace AsciiReader
{
    internal class Program
    {
        private static int threadCount = 100;
        private static string asciiFile = string.Empty;
        private static ManualResetEvent wait;
        private static AsciiSettingCollection ascSetting;

        public static string AssemblyDirectory
        {
            get
            {
                return Path.GetDirectoryName(Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path));
            }
        }

        private void CreateLog(string msg, string function_id, string location, string processid, string type = "INF", string filename = "")
        {
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            string time = DateTime.Now.ToString("HH:mm:ss.fff");
            string[] filepath = new string[] { };

            msg = msg.Replace("'", "");

            if ((Program.asciiFile.Length > 0) && (function_id.Length > 0))
            {
                filepath = Program.asciiFile.Split('/');
                filename = filepath[filepath.Length - 1];
            }

            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["PCS"];
            string connectionString = connectionStringSettings.ConnectionString;
            DbConnection logconnection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();

            logconnection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
            logconnection.ConnectionString = connectionString;
            using (logconnection)
            {
                try
                {
                    logconnection.Open();
                    using (DbCommand command = logconnection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO TB_R_ASCII_LOG VALUES ('" + processid + "', '" + function_id + "', '" + filename + "', GETDATE(), '" + location + "', '" + msg + "', '" + type + "')";
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception exc)
                {
                    if (!EventLog.SourceExists("AsciiReader", "."))
                        EventLog.CreateEventSource("AsciiReader", "Application");
                    EventLog Lg = new EventLog("Application", ".", "AsciiReader");
                    string Message = "Error Write Log : " + exc.Message;
                    Lg.WriteEntry(Message, EventLogEntryType.Information);

                    if (!EventLog.SourceExists("AsciiReader", "."))
                        EventLog.CreateEventSource("AsciiReader", "Application");
                    Lg = new EventLog("Application", ".", "AsciiReader");
                    Message = "Exception : " + msg;
                    Lg.WriteEntry(Message, EventLogEntryType.Information);

                    Console.WriteLine("###RETURN=1###");
                    Environment.Exit(0);
                }
                finally
                {
                    logconnection.Close();
                }
            }

            Console.WriteLine(date + "|" + time + "|" + function_id + "|" + location + "|" + msg);
        }

        public void Run(object data)
        {
            //Program program = new Program();
            List<string> list = data as List<string>;
            StringBuilder stringBuilder = new StringBuilder();

            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["PCS"];
            string connectionString = connectionStringSettings.ConnectionString;
            DbConnection connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
            
            using (connection)
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                foreach (string str in list)
                {
                    if (str.Trim().Length > 0 && !str.Contains("##"))
                    {
                        stringBuilder = stringBuilder.Append("INSERT INTO " + Program.ascSetting.TableName + "(");
                        string[] strArray1 = new string[Program.ascSetting.Columns.Count];
                        string[] strArray2 = new string[Program.ascSetting.Columns.Count];
                        
                        int index1 = 0;
                        foreach (KeyValuePair<string, AsciiSetting> keyValuePair in Program.ascSetting.Columns)
                        {
                            strArray1[index1] = keyValuePair.Key.ToString();
                            ++index1;
                        }
                        stringBuilder.Append(string.Join(",", strArray1)).Append(",CREATED_BY,CREATED_DT) VALUES (").ToString();
                        
                        int index2 = 0;
                        try
                        {
                            foreach (KeyValuePair<string, AsciiSetting> keyValuePair in Program.ascSetting.Columns)
                            {
                                int num = keyValuePair.Value.Length == 0 ? 1 : (keyValuePair.Value.Position == 0 ? 1 : 0);
                                strArray2[index2] = num != 0 ? "'" + keyValuePair.Value.DefaultValue + "'" : "'" + str.Substring(keyValuePair.Value.Position - 1, keyValuePair.Value.Length) + "'";
                                ++index2;
                            }
                        }
                        catch (Exception ex)
                        {
                            this.CreateLog("Error while reading columns property " + ex.Message, Program.ascSetting.function_id, "Thread Insert", Program.ascSetting.process_id, "ERR");
                            Console.WriteLine("Error while reading columns property " + ex.Message);
                            Console.WriteLine("###RETURN=1###");
                            Environment.Exit(0);
                        }

                        stringBuilder.Append(string.Join(",", strArray2)).Append(",'SYSTEM','" + (object)DateTime.Now + "')").ToString();
                        using (DbCommand command = connection.CreateCommand())
                        {
                            try
                            {
                                command.CommandText = stringBuilder.ToString();
                                if (command.ExecuteNonQuery() == 0)
                                {
                                    this.CreateLog("Error while inserting data on row " + str, Program.ascSetting.function_id, "Thread Insert", Program.ascSetting.process_id, "ERR");
                                    Console.WriteLine("Error while inserting data on row " + str, "", "");
                                    Console.WriteLine("###RETURN=1###");
                                    Environment.Exit(0);
                                }
                            }
                            catch (Exception ex)
                            {
                                this.CreateLog("Error while inserting data on row " + ex.Message, Program.ascSetting.function_id, "Thread Insert", Program.ascSetting.process_id, "ERR");
                                Console.WriteLine("Error while inserting data on row " + ex.Message);
                            }
                        }
                        stringBuilder.Clear();
                    }
                }
            }
            if (Interlocked.Decrement(ref Program.threadCount) != 0)
                return;

            Program.wait.Set();
        }

        private static void Main(string[] args)
        {
            Program program = new Program();
            string message = "";
            string location = "";
            string function_id = "";
            string str2 = string.Empty;

            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["PCS"];
            string connectionString = connectionStringSettings.ConnectionString;
            DbConnection connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();

            string process_id = Guid.NewGuid().ToString("N"); //Convert.ToInt64(DateTime.Now.ToString("ddMMyyyyhhmmssfff"));
            Program.ascSetting = new AsciiSettingCollection();

            try
            {
                //if (!Directory.Exists("AsciiLog"))
                //    Directory.CreateDirectory("AsciiLog");

                Program.ascSetting.process_id = process_id;
                program.CreateLog("Ascii Reader Started", function_id, location, process_id);
                connection.ConnectionString = connectionString;

                Program.wait = new ManualResetEvent(false);
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                location = "Initialization";
                if (args.Length < 1)
                    throw new Exception("Invalid Input Parameters");

                if (args[0] == null || args[0] == string.Empty)
                    throw new Exception("Config File Name is NULL in args[0]");

                if (args[1] == null && args[1] == string.Empty)
                    throw new Exception("File Location Path is NULL in srgs[1]");

                function_id = args[0].Trim();
                Program.ascSetting.function_id = function_id;
                Program.asciiFile = args[1].Trim();
                
                program.CreateLog("Processing File " + Program.asciiFile, function_id, location, process_id);
                location = "Constructed XML Config";
                #region Construct XML Config
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(Program.AssemblyDirectory + ConfigurationManager.AppSettings["ConfigLocation"] + function_id + ".xml");
                Program.ascSetting.DatabaseName = xmlDocument.DocumentElement.ChildNodes[0].Attributes["name"].Value;
                Program.ascSetting.TableName = xmlDocument.DocumentElement.ChildNodes[0].Attributes["table"].Value;
                Program.ascSetting.ApplicationPath = xmlDocument.DocumentElement.ChildNodes[3].Attributes["path"].Value;
                Program.ascSetting.ApplicationExecuted = xmlDocument.DocumentElement.ChildNodes[3].Attributes["name"].Value;
                Program.ascSetting.ApplicationParam = xmlDocument.DocumentElement.ChildNodes[3].Attributes["param"].Value;

                foreach (XmlNode xmlNode1 in xmlDocument.DocumentElement.ChildNodes[1])
                {
                    XmlElement xmlElement = (XmlElement)xmlNode1;
                    if (xmlNode1.ChildNodes.Count == 0)
                    {
                        Console.WriteLine("(No Column Information)");
                    }
                    else
                    {
                        foreach (XmlNode xmlNode2 in xmlNode1.ChildNodes)
                        {
                            if (Program.ascSetting.Columns.ContainsKey(xmlElement.Attributes["name"].Value))
                                Program.ascSetting.Columns[xmlElement.Attributes["name"].Value] = new AsciiSetting()
                                {
                                    Position = Convert.ToInt32(xmlNode2.Attributes["position"].Value),
                                    Length = Convert.ToInt32(xmlNode2.Attributes["length"].Value),
                                    DefaultValue = Convert.ToString(xmlNode2.Attributes["defaultvalue"].Value)
                                };
                            else
                                Program.ascSetting.Columns.Add(xmlElement.Attributes["name"].Value, new AsciiSetting()
                                {
                                    Position = Convert.ToInt32(xmlNode2.Attributes["position"].Value),
                                    Length = Convert.ToInt32(xmlNode2.Attributes["length"].Value),
                                    DefaultValue = Convert.ToString(xmlNode2.Attributes["defaultvalue"].Value)
                                });
                        }
                    }
                }
                #endregion

                location = "Check File Type";
                #region Check Order File
                string IsOrderFile = "";
                connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
                connection.ConnectionString = connectionString;
                using (connection)
                {
                    connection.Open();
                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'ORDER' AND SYSTEM_CD = 'ORDERFILENAME'";
                        IsOrderFile = command.ExecuteScalar().ToString();
                    }
                    connection.Close();
                }
                #endregion

                if (IsOrderFile.Contains(function_id))
                {
                    #region Order Data Bulk Insert
                    string applicationParam = Program.ascSetting.ApplicationParam;
                    IEnumerable<string> data = (IEnumerable<string>)File.ReadAllLines(Program.asciiFile);

                    if (Program.ascSetting.TableName == "TB_T_ORDERPLAN_TMP")
                    {
                        connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
                        connection.ConnectionString = connectionString;
                        using (connection)
                        {
                            connection.Open();
                            using (DbCommand command = connection.CreateCommand())
                            {
                                location = "Delete TB_T_ORDERPLAN_TMP";
                                command.CommandText = "DELETE FROM TB_T_ORDERPLAN_TMP WHERE RCV_PLANT_CODE ='" + applicationParam + "'";
                                int rowsdeleted = command.ExecuteNonQuery();
                                program.CreateLog("Success Delete " + rowsdeleted.ToString() + " rows Where RCV_PLANT_CD " + applicationParam, function_id, location, process_id);

                                location = "Update Interface Status";
                                command.CommandText = "update TB_R_DAILY_ORDER_INTERFACE_FILE set StatusFlag = '1'  WHERE RCV_PLANT_CD ='" + applicationParam + "' and StatusFlag='0' ";
                                int rowsupdated = command.ExecuteNonQuery();
                                program.CreateLog("Success Update Interface Status to 1 for RCV_PLANT_CD " + applicationParam, function_id, location, process_id);
                            }
                            connection.Close();
                        }
                    }

                    location = "Bulk Insert";
                    int rowsbulked = program.BulkTest(data, function_id);
                    if (rowsbulked <= 0)
                        throw new Exception("No Data Inserted into " + Program.ascSetting.TableName);

                    program.CreateLog("Success Insert " + rowsbulked.ToString() + " row(s) for RCV_PLANT_CD " + applicationParam, function_id, location, process_id);

                    connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
                    connection.ConnectionString = connectionString;
                    using (connection)
                    {
                        connection.Open();
                        using (DbCommand command = connection.CreateCommand())
                        {
                            location = "Insert TB_R_DAILY_ORDER_INTERFACE_COUNT";
                            command.CommandText = "INSERT INTO TB_R_DAILY_ORDER_INTERFACE_COUNT (RCV_PLANT_CD, TOTAL, CREATED_DT) VALUES('" + applicationParam + "', " + rowsbulked.ToString() + ", GETDATE())";
                            command.ExecuteNonQuery();
                            program.CreateLog("Success Insert Data", function_id, location, process_id);

                            location = "Insert TB_R_DAILY_ORDER_INTERFACE_FILE";
                            command.CommandText = "INSERT INTO TB_R_DAILY_ORDER_INTERFACE_FILE(RCV_PLANT_CD, StatusFlag) VALUES('" + applicationParam + "', '0')";
                            command.ExecuteNonQuery();
                            program.CreateLog("Success Insert Data", function_id, location, process_id);
                        }
                        connection.Close();
                    }
                    #endregion
                }
                else
                {
                    #region Threading Insert Non-Order Data
                    location = "Check Locking";
                    #region Locking Check
                    connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
                    connection.ConnectionString = connectionString;
                    using (connection)
                    {
                        connection.Open();
                        using (DbCommand queryCommand = connection.CreateCommand())
                        {
                            queryCommand.CommandText = "SELECT COUNT(1) FROM TB_T_LOCK WHERE FUNCTION_NO = '" + function_id + "'";
                            string locked = queryCommand.ExecuteScalar().ToString();

                            if (locked == "1")
                            {
                                message = "Cannot continue to process, previous process with function ID " + function_id + " still running.";
                                throw new CustomException(message);
                            }
                        }
                        connection.Close();
                    }
                    #endregion

                    location = "Locking";
                    #region Adding Lock
                    connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
                    connection.ConnectionString = connectionString;
                    using (connection)
                    {
                        connection.Open();
                        using (DbCommand command = connection.CreateCommand())
                        {
                            command.CommandText = "INSERT INTO TB_T_LOCK VALUES('" + function_id + "', '" + function_id + "' + '_' + REPLACE(convert(varchar, getdate(), 102), '.', ''),'asciiReader', getdate())";
                            command.ExecuteNonQuery();

                            program.CreateLog("Success Locking", function_id, location, process_id);
                        }
                        connection.Close();
                    }
                    #endregion

                    location = "Insert To Temp. Table";
                    #region Insert Into Temp Table
                    string[] strArray = File.ReadAllLines(Program.asciiFile);
                    List<string> list1 = Enumerable.ToList<string>((IEnumerable<string>)strArray);
                    Program.threadCount = Enumerable.Count<string>((IEnumerable<string>)strArray) < 100001 ||
                                          Enumerable.Count<string>((IEnumerable<string>)strArray) > int.MaxValue ?
                                                (Enumerable.Count<string>((IEnumerable<string>)strArray) < 10000 ||
                                                 Enumerable.Count<string>((IEnumerable<string>)strArray) > 100000 ? 4 : 32) : 64;
                    int index1 = 0;
                    int count = list1.Count / Program.threadCount;
                    for (int index2 = 0; index2 < Program.threadCount; ++index2)
                    {
                        List<string> list2 = new List<string>();
                        if (index2 != Program.threadCount - 1)
                            list2.AddRange((IEnumerable<string>)list1.GetRange(index1, count));
                        else
                            list2.AddRange((IEnumerable<string>)list1.GetRange(index1, count + list1.Count % Program.threadCount));

                        Program.wait = new ManualResetEvent(false);
                        index1 += list2.Count;
                        new Thread(new ParameterizedThreadStart(program.Run))
                        {
                            Priority = ThreadPriority.AboveNormal,
                            Name = "thread"
                        }.Start((object)list2);
                    }
                    Program.wait.WaitOne();

                    program.CreateLog("Success Insert " + (strArray.Length - 1).ToString() + " row(s)", function_id, location, process_id);
                    #endregion
                    #endregion
                }
                stopwatch.Stop();

                program.CreateLog("Process Success with Processing Time " + stopwatch.Elapsed, function_id, "Finishing", process_id, "SUC");

                location = "Execute SP";
                if (Program.ascSetting.ApplicationExecuted.Length > 0)
                {
                    program.CreateLog("Execute " + Program.ascSetting.ApplicationExecuted, function_id, location, process_id);
                    string[] separator = new string[1]
                    {
                      ";"
                    };
                    string[] arguments = Program.ascSetting.ApplicationParam.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    if (Program.ascSetting.ApplicationExecuted.Length > 0)
                        program.LaunchApp(Program.ascSetting.ApplicationExecuted, Program.ascSetting.ApplicationPath, arguments, function_id);

                    program.CreateLog("Success Launch " + Program.ascSetting.ApplicationExecuted, function_id, location, process_id, "SUC");
                }

                program.CreateLog("Ascii Reader Finish", "", "", process_id);
                Console.WriteLine("###RETURN=0###");
            }
            catch (CustomException exx)
            {
                program.CreateLog(exx.Message, function_id, location, process_id, "ERR");
                program.CreateLog("Ascii Reader Terminated", "", "", process_id);

                Console.WriteLine("###RETURN=1###");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                program.CreateLog(ex.Message, function_id, location, process_id, "ERR");
                
                #region Delete Lock
                connection = DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
                connection.ConnectionString = connectionString;
                using (connection)
                {
                    location = "Release Lock";
                    connection.Open();
                    using (DbCommand commandd = connection.CreateCommand())
                    {
                        try
                        {
                            commandd.CommandText = "DELETE FROM TB_T_LOCK WHERE FUNCTION_NO = '" + function_id + "'";
                            commandd.ExecuteNonQuery();

                            program.CreateLog("Locked Released", function_id, location, process_id);
                        }
                        catch (Exception exx)
                        {
                            program.CreateLog(exx.Message, function_id, location, process_id, "ERR");
                            Environment.Exit(0);
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
                #endregion

                program.CreateLog("Ascii Reader Terminated", "", "", process_id);
                Console.WriteLine("###RETURN=1###");
                Environment.Exit(0);
            }
            finally
            {
                connection.Close();
            }
        }

        private int BulkTest(IEnumerable<string> data, string function_id)
        {
            DataTable table = new DataTable();
            int rowsbulked = 0;

            foreach (KeyValuePair<string, AsciiSetting> keyValuePair in Program.ascSetting.Columns)
            {
                table.Columns.Add(keyValuePair.Key.ToString());
            }
            table.Columns.Add("CREATED_BY");
            table.Columns.Add("CREATED_DT");
            
            foreach (string str in data)
            {
                if (str.Trim().Length > 0 && !str.Contains("##"))
                {
                    DataRow row = table.NewRow();
                    foreach (KeyValuePair<string, AsciiSetting> keyValuePair in Program.ascSetting.Columns)
                    {
                        int num2 = keyValuePair.Value.Length == 0 ? 1 : (keyValuePair.Value.Position == 0 ? 1 : 0);
                        row[keyValuePair.Key.ToString()] = num2 != 0 ? keyValuePair.Value.DefaultValue : 
                                                str.Substring(keyValuePair.Value.Position - 1, keyValuePair.Value.Length);
                    }
                    row["CREATED_BY"] = "System";
                    row["CREATED_DT"] = DateTime.Now;
                    table.Rows.Add(row);
                }
            }
            
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["PCS"];
            string connectionString = connectionStringSettings.ConnectionString;
            DbProviderFactories.GetFactory(connectionStringSettings.ProviderName).CreateConnection();
            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectionString))
            {
                sqlBulkCopy.DestinationTableName = Program.ascSetting.TableName;

                foreach (KeyValuePair<string, AsciiSetting> keyValuePair in Program.ascSetting.Columns)
                {
                    sqlBulkCopy.ColumnMappings.Add(keyValuePair.Key.ToString(), keyValuePair.Key.ToString());
                }
                sqlBulkCopy.ColumnMappings.Add("CREATED_BY", "CREATED_BY");
                sqlBulkCopy.ColumnMappings.Add("CREATED_DT", "CREATED_DT");

                sqlBulkCopy.BulkCopyTimeout = 120;
                sqlBulkCopy.WriteToServer(table);
                rowsbulked = table.Rows.Count;
            }

            return rowsbulked;
        }

        //private void OnSuccesOrError(string function_id)
        //{
        //    //Program program = new Program();
        //    string[] separator = new string[1]{ ";" };
        //    string[] arguments = Program.ascSetting.ApplicationParam.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        //    if (Program.ascSetting.ApplicationExecuted.Length > 0)
        //        this.LaunchApp(Program.ascSetting.ApplicationExecuted, Program.ascSetting.ApplicationPath, arguments, function_id);
        //    Environment.Exit(0);
        //}

        private void LaunchApp(string appName, string filePath, string[] arguments, string function_id)
        {
            Program program = new Program();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            string str1 = string.Empty;
            
            if (Enumerable.Count<string>((IEnumerable<string>)arguments) > 0)
            {
                string str2 = string.Join(" ", arguments);
                startInfo.Arguments = str2;
            }

            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = true;
            startInfo.FileName = Path.Combine(filePath, appName);
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            using (Process.Start(startInfo)) ;
        }

        public class CustomException : Exception
        {
            public CustomException(String message) : base(message) { }
        }
    }
}
