﻿// Decompiled with JetBrains decompiler
// Type: AsciiReader.ConsoleSpiner
// Assembly: AsciiReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 90053FD5-681F-48BD-9587-D40F1B96E466
// Assembly location: D:\Temporary\xxx\xxx\AsciiReader.exe

using System;

namespace AsciiReader
{
  public class ConsoleSpiner
  {
    public int counter;

    public ConsoleSpiner()
    {
      this.counter = 0;
    }

    public void Turn()
    {
      ++this.counter;
      switch (this.counter % 4)
      {
        case 0:
          Console.Write("/");
          break;
        case 1:
          Console.Write("-");
          break;
        case 2:
          Console.Write("\\");
          break;
        case 3:
          Console.Write("|");
          break;
      }
      Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
    }
  }
}
