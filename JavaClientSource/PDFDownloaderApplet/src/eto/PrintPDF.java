package eto;

import com.google.gson.Gson;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.JApplet;
import javax.swing.JOptionPane;
import netscape.javascript.JSObject;
import org.apache.pdfbox.pdmodel.PDDocument;


/**
 *
 * @author bluespy
 */
public class PrintPDF extends JApplet {  
     Map<String, String> paramValue = new HashMap<String, String>();
     
     //parse the URL parameter
   private void parseParam(String parameters){
 
	   StringTokenizer paramGroup = new StringTokenizer(parameters, "&");
 
	   while(paramGroup.hasMoreTokens()){
 
		   StringTokenizer value = new StringTokenizer(paramGroup.nextToken(), "=");
		   paramValue.put(value.nextToken(), value.nextToken());
 
	   }
   }
    
    public String getPrinterList()
    {
        String out = "";
        for (PrintService printer : PrintServiceLookup.lookupPrintServices(null, null))
        {   
            out += printer.getName() + ";";
        }
        return out.substring(0, out.length()-1);
    }
    
    @Override
    public void init(){
        String pageUrl = getDocumentBase().toString();

        if (pageUrl.indexOf("?") > -1) {
                String paramaters = pageUrl.substring(pageUrl.indexOf("?") + 1);
                parseParam(paramaters);
        }
        
        String successCallback = this.getParameter("successCallback");
        String failureCallback = this.getParameter("failureCallback");
        String getPrinterListCallback = this.getParameter("getPrinterListCallback");
        Boolean onlyGetPrinterList = this.getParameter("onlyGetPrinterList").equalsIgnoreCase("true");
        String printParameter = paramValue.get("printParameter") == null ? this.getParameter("printParameter") : URLDecoder.decode(paramValue.get("printParameter")) ; // "http://localhost:8085/cv.pdf>\\\\tmmin-prt-svr\\S1-ISTD-Project;";
        JSObject window = JSObject.getWindow(this);
        
        window.call(getPrinterListCallback, new String[] { getPrinterList() });
        
        if(onlyGetPrinterList)
        {
            System.exit(0);
        }
        
        Gson gson = new Gson();
        PrintCollection printParameters = gson.fromJson("{\"items\":" +  printParameter + "}", PrintCollection.class);
        
        if (printParameters.items.isEmpty()) {
            window.call(failureCallback, new String[] { "Missing required parameters." });
            error("Missing required parameters");
        } else {
            try {
                for(PrintItem curItem : printParameters.items)
                {
                    // Set printer if available, else use the default printer
                    PrinterJob job = PrinterJob.getPrinterJob();
                    for (PrintService printer : PrintServiceLookup.lookupPrintServices(null, null))
                    {
                        if(printer.getName().equalsIgnoreCase(curItem.targetPrinter))
                        {
                            job.setPrintService(printer);
                        }
                    }

                    PDDocument doc;
                    URL url = new URL(curItem.urlFile);
                    doc = PDDocument.load(url);
                    doc.silentPrint(job);
                    doc.close();
                }               
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL Exception: " + e.getMessage());
                error("Invalid print file URL. Detail: " + e.getMessage());
                window.call(failureCallback, new String[] { "Invalid print file URL." });
            } catch (IOException e) {
                System.out.println("IO Exception: " + e.getMessage());
                error("Print file could not be loaded. Detail: " + e.getMessage());
                window.call(failureCallback, new String[] { "Print file could not be loaded." });
            } catch(PrinterException e) {
                System.out.println("Printer exception: " + e.getMessage());
                error("Printer exception: " + e.getMessage());
                window.call(failureCallback, new String[] { e.getMessage() });
            }
        }
        window.call(successCallback, new String[] { "Done" });
        System.exit(0);
    }
    
    private void error(String message) {
        System.out.println("ERROR: "+message);
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
}

class PrintCollection
{
    ArrayList<PrintItem> items;
    
    public PrintCollection()
    {
        items = new ArrayList<>();
    }
    
    public void add(PrintItem item)
    {
        items.add(item);
    }
}

class PrintItem
{
    public String urlFile = "";
    public String targetPrinter = "";
    
    public PrintItem(String url, String printer)
    {
        urlFile = url;
        targetPrinter = printer;
    }
}
