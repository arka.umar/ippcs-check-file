﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

// niit.yudha - 16 april 2013

namespace Toyota.Email
{
    public class SmtpMailService
    {

        private static readonly HashSet<ISmtpMailService> stored = new HashSet<ISmtpMailService>();
         
        static SmtpMailService()
        {
            Assembly[] ass = AppDomain.CurrentDomain.GetAssemblies();
            Type itype; 
            foreach (Assembly asm in ass)
            {
                try
                {
                    foreach (Type type in asm.GetExportedTypes())
                    {
                        itype = type.GetInterface("ISmtpMailService");
                        if (itype != null)
                        {
                            stored.Add((ISmtpMailService)Activator.CreateInstance(type));
                        }
                    }
                }
                catch (NotSupportedException nse)
                { } 
                catch (Exception ex)
                { 
                    throw ex; 
                } 
            }
        }

        public static ISmtpMailService GetInstanceOf<T>()
        {
            Type type = typeof(T);
            foreach (object obj in stored)
            {
                if (type.IsInstanceOfType(obj))
                { return (ISmtpMailService)obj; }
            }
            return null;
        }

    }
}
