﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

// niit.yudha - 16 april 2013

namespace Toyota.Email
{
    public interface ISmtpMailService
    {
        bool Send(object[] args);
        bool Send(object[] args, ArrayList attachment);
        MailSetting GetMailSetting();
        void Dispose();
    }
}
