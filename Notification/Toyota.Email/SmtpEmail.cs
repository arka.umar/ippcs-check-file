﻿/*
 * Niit.Mulki
 * 27-11-2012
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Collections;
using System.Net.Mime;
using System.Text.RegularExpressions;

namespace Toyota.Email
{
    public class SmtpEmail : IDisposable
    {
        private string[] mailCC;
        public string[] MailCC
        {
            set { mailCC = value; }
            get { return mailCC; }
        }

        private string[] mailTo;
        public string[] MailTo
        {
            set { mailTo = value; }
            get { return mailTo; }
        }

        private string mailFrom;
        public string MailFrom
        {
            set { mailFrom = value; }
            get { return mailFrom; }
        }

        private string subject;
        public string Subject
        {
            set { subject = value; }
            get { return subject; }
        }

        private bool isBodyHtml = false;
        public bool IsBodyHtml
        {
            set { isBodyHtml = value; }
            get { return isBodyHtml; }
        }

        private string body;
        public string Body
        {
            set { body = value; }
            get { return body; }
        }

        private MailSetting setting;
        public MailSetting Setting
        {
            set { setting = value; }
            get { return setting; }
        }

        private bool isCredential;
        public bool IsCredential
        {
            set { isCredential = value; }
            get { return isCredential; }
        }

        public SmtpEmail()
        {
            Setting = new MailSetting(); 
            cred = new NetworkCredential();
        }

        private MailMessage msg;
        private NetworkCredential cred;
         
        /// <summary>
        /// Send Email
        /// </summary>
        /// <returns></returns>
        public bool Send()
        {
            try
            {
                InitMailMessage();
                return _Send();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Send Email with Attachment
        /// </summary>
        public bool Send(ArrayList vAttachment)
        {
            try
            {
                InitMailMessage();
                foreach (string attach in vAttachment)
                {
                    Attachment attached = new Attachment(attach, MediaTypeNames.Application.Octet);
                    msg.Attachments.Add(attached);
                }
                return _Send();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private bool InitMailMessage()
        {
            //if (ValidateEmailAddress(mailFrom))
            //    return false; // "Invalid Sender Email Addres : " + mailFrom;
            msg = new MailMessage();
            foreach (string mailto in mailTo)
            {
                if (ValidateEmailAddress(mailto))
                { msg.To.Add(mailto); }
                else
                {
                    return false; // "Invalid Receipent Email Address : " + mailto; 
                }
            }

            if (mailCC != null)
            { 
                foreach (string cc in mailCC)
                {
                    if (ValidateEmailAddress(cc))
                    { msg.CC.Add(cc); } 
                }
            }
            
            msg.From = new MailAddress(mailFrom);
            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;

            if (isCredential)
            {
                cred = new NetworkCredential(setting.UserName, setting.Password);
            }

            return true;
        }

        private bool _Send()
        {
            try
            {
                SmtpClient client = new SmtpClient(setting.Host, setting.Port) { Credentials = cred, EnableSsl = setting.EnableSSL };
                client.ServicePoint.MaxIdleTime = 0;
                client.ServicePoint.ConnectionLimit = 400;
                client.Send(msg);
                msg.Dispose();
                client.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool ValidateEmailAddress(string emailAddress)
        {
            try
            {
                string TextToValidate = emailAddress;
                Regex expression = new Regex(@"\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}");

                if (expression.IsMatch(TextToValidate))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
                throw new Exception();                
            }
        }

        public void Dispose()
        {
            msg.Dispose();
        }
    }
}
