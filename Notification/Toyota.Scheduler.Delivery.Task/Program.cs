﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Configuration;
using Toyota.Common.Web.Email;
using Toyota.Email;

namespace Toyota.Scheduler.Delivery.Task
{
    class Program
    {
        static void Main(string[] args)
        {
            string folder = ConfigurationManager.AppSettings["FolderTaskScheduler"];
            if (string.IsNullOrEmpty(folder))
                throw new ConfigurationErrorsException("\'FolderTaskScheduler\' cannot be null.");

            string query = ConfigurationManager.AppSettings["Query"];
            if (string.IsNullOrEmpty(query))
                throw new ConfigurationErrorsException("\'Query\' cannot be null.");

            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["PCS"];
            DbProviderFactory factory = DbProviderFactories.GetFactory(cs.ProviderName);
            DbConnection connection = factory.CreateConnection();
            connection.ConnectionString = cs.ConnectionString;
            
            connection.Open();
            DbDataReader result;
            object[] values = new object[0];
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = string.Format(query, "\'" + args[0] + "\'", "\'" + args[1] + "\'");
                result = command.ExecuteReader();
            }
            while (result.Read())
            {
                values = new object[result.FieldCount];
                result.GetValues(values);
            }
            connection.Close();

            if (values.Length > 0)
            {
                ISmtpMailService mail = SmtpMailService.GetInstanceOf<DeliverySmtpMailService>();
                mail.Send(values);
            } 
            Microsoft.Win32.TaskScheduler.TaskService task = new Microsoft.Win32.TaskScheduler.TaskService();
            Microsoft.Win32.TaskScheduler.TaskFolder taskFolder = task.GetFolder(folder);
            taskFolder.DeleteTask(args[1]);
            taskFolder.Dispose();
            task.Dispose();
            
        }
    }
}
