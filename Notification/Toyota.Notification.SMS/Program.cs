﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.Common;
using Database;  

namespace Toyota.Notification.SMS
{
    class Program
    {

        private const string SMSFormat = "ISMS.Send(+param: number={0} +param: message={1})";

        static void Main(string[] args)
        {
            if (args.Length > 3)
            {
                string functionID = args[0];
                string roleID = args[1];
                string method = args[2];
                List<Notification> notif;
                List<Employee> emp;
                try
                {
                    using (IQuery con = new DbContext("PCS"))
                    {
                        notif = con.Query<Notification>("GetNotificationContent", new { FunctionID = functionID, RoleID = roleID, Method = method });
                        emp = con.Query<Employee>("GetUserPhoneEmail", new { RoleID = roleID });
                    }

                    if (notif.Any())
                    {
                        IDictionary<string, string> param = Map(args);
                        string content = Parse(notif.Select(s => s.NotificationContent).SingleOrDefault(), param);
                        string subject = Parse(notif.Select(s => s.NotificationSubject).SingleOrDefault(), param);

                        GSMClient.IClient client = new GSMClient.Client(ConfigurationManager.AppSettings["SMSServerIP"], Convert.ToInt32(ConfigurationManager.AppSettings["SMSServerPort"]));
                        client.Open();
                        foreach (string selected in emp.Select(s => s.Telephone))
                        {
                            client.Send(string.Format(SMSFormat, selected, content));
                        }
                        client.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadKey();
                }
            }
        }

        internal static Dictionary<string, string> Map(string[] args)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            int start, from;
            foreach (string str in args)
            {
                if (str.Contains("@"))
                {
                    start = str.IndexOf("@");
                    from = str.IndexOf("=");
                    param.Add(str.Substring(start, from), str.Substring(from + 1));
                }
            }
            return param;
        }

        internal static string Parse(string input, IDictionary<string, string> param)
        {
            string content = input;
            foreach (string key in param.Keys)
            {
                content = content.Replace(key, param[key]);
            }
            return content;
        }
    }
}
