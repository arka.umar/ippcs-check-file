﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Collections.Concurrent;
using System.Data.Common;
using System.Configuration;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32.TaskScheduler;
using System.Threading;

namespace Toyota.Scheduler.Delivery
{

    public class Delivery : IDisposable
    {

        private Microsoft.Win32.TaskScheduler.TaskService task = new Microsoft.Win32.TaskScheduler.TaskService();
        private Microsoft.Win32.TaskScheduler.TaskFolder taskFolder;
        private string queryGetSchedule;
        private string path;
        private string folder;
        private int threadCounter;
        private ManualResetEvent wait;

        public Delivery()
        {
            folder = ConfigurationManager.AppSettings["TaskSchedulerFolder"];
            if (string.IsNullOrEmpty(folder))
                taskFolder = task.GetFolder(@"\");
            else
                taskFolder = task.GetFolder(folder);
            
            path = ConfigurationManager.AppSettings["ExecuteTaskName"];
            queryGetSchedule = ConfigurationManager.AppSettings["QueryGetSchedule"];

            if (string.IsNullOrEmpty(queryGetSchedule))
                throw new ConfigurationErrorsException("\'Query\' cannot be null.");

            task.BeginInit();
            wait = new ManualResetEvent(false); 
        }
         
        public void ClearAll()
        {
            wait = new ManualResetEvent(false);
            Thread thrCreateTask;
            TaskCollection collection = taskFolder.GetTasks();
            threadCounter = collection.Count;
            foreach (Task item in collection)
            {
                thrCreateTask = new Thread(new ParameterizedThreadStart(_DeleteTask));
                thrCreateTask.Start(item.Name);
            }
            if (threadCounter > 0)
                wait.WaitOne();
        }

        private void _DeleteTask(object name)
        {
            taskFolder.DeleteTask((string)name);
            if (Interlocked.Decrement(ref threadCounter) == 0)
                wait.Set();
        }

        private void CleaningExpiredTask()
        {
            TaskCollection collection = taskFolder.GetTasks();
            foreach (Task item in collection)
            {
                Trigger trg = item.Definition.Triggers.Where(i => i.StartBoundary < DateTime.Now).Select(i => i).SingleOrDefault();
                if (trg != null)
                { taskFolder.DeleteTask(item.Name); }
            }
        }

        public void LoadAndCreate()
        {
            CleaningExpiredTask();
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["PCS"];
            DbProviderFactory factory = DbProviderFactories.GetFactory(cs.ProviderName);
            DbConnection connection = factory.CreateConnection();
            connection.ConnectionString = cs.ConnectionString;
             
            queryGetSchedule = queryGetSchedule.Replace("@DateTimeNow", "\'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\'");
            queryGetSchedule = queryGetSchedule.Replace("@DateTimeNext", "\'" + DateTime.Now.AddHours(12).ToString("yyyy-MM-dd HH:mm:ss") + "\'");
   
            DbDataReader result;
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = queryGetSchedule;
                result = command.ExecuteReader();
            }
            Thread thrCreateTask;
            object[] values;
            while (result.Read())
            {
                threadCounter++;
                values = new object[result.VisibleFieldCount];
                result.GetValues(values);
                thrCreateTask = new Thread(new ParameterizedThreadStart(Create));
                thrCreateTask.Start(values);
            }
            if (result.HasRows) wait.WaitOne();
            connection.Close();
        }
         
        private void Create(object wparam)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("id-ID");
            object[] param = ((object[])wparam);
            DateTime dtData = Convert.ToDateTime(param[1].ToString());
            TimeSpan tsData = TimeSpan.Parse(Convert.ToDateTime(param[1].ToString()).ToString("HH:mm:00"));
            tsData = tsData.Add(TimeSpan.FromMinutes(-1));
            DateTime dtNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy") + " " + tsData.ToString("hh':'mm':00'"));
            
            TimeSpan tsNow = TimeSpan.Parse(DateTime.Now.ToString("HH:mm:00"));
            if (dtData > dtNow)
            {
                Microsoft.Win32.TaskScheduler.TaskDefinition td = task.NewTask();
                td.Settings.Enabled = true;
                td.Settings.Compatibility = Microsoft.Win32.TaskScheduler.TaskCompatibility.V2;
                td.RegistrationInfo.Description = "Task Delivery Truck Check";
                td.RegistrationInfo.Author = "SYSTEM";
                td.Principal.LogonType = TaskLogonType.S4U;
                td.Triggers.Add(new TimeTrigger { StartBoundary = dtNow });
                td.Actions.Add(new ExecAction(AppDomain.CurrentDomain.BaseDirectory + path, dtData.ToString("yyyyMMdd") + " " + param[0].ToString()));
                taskFolder.RegisterTaskDefinition(param[0].ToString(), td);
            } 
            if (Interlocked.Decrement(ref threadCounter) == 0)
                wait.Set();
        }
         
        public void Dispose()
        {
            if (taskFolder != null)
                taskFolder.Dispose();
            if (task != null)
            {
                task.EndInit();
                task.Dispose();
            }
            wait.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
    