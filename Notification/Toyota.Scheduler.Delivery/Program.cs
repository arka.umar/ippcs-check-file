﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Scheduler.Delivery
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> oArgs = args.ToList<string>();
            Delivery del = new Delivery();

            foreach (string arg in oArgs)
            {
                if (arg.Equals("-c", StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.Write("Load data and Creating each task.");
                    del.LoadAndCreate();
                }
                else if (arg.Equals("-d", StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.Write("Deleting all task.");
                    del.ClearAll();
                }
            }
            del.Dispose();
        }
    }
}
