﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

// niit.yudha - 16 april 2013

namespace Toyota.Common.Web.Email
{
    public class NotificationSmtpMailService : ISmtpMailService
    {

        private SmtpEmail mail = new SmtpEmail();
        private string[] mailTo;
        private string mailFrom;
        private string body;
        private string subject;
        private MailSetting setting;

        public NotificationSmtpMailService()
        {
            this.setting = new MailSetting();
            this.GetMailSetting();
        }

        public bool Send(object[] args)
        {
            try
            {
                body = GetBody();
                body = string.Format(body, args[0], args[1], args[2], args[3]);
                mail.IsBodyHtml = true; 
                mail.Setting = setting;
                mail.Subject = string.Format(subject, args[0]);
                mail.MailTo = mailTo;
                mail.MailFrom = mailFrom;
                mail.Body = body; 
                return mail.Send();
            }
            catch (Exception ex)
            { throw (ex); }
        }

        private string GetBody()
        {
            string ret;
            using (StreamReader reader = File.OpenText(AppDomain.CurrentDomain.BaseDirectory + body))
            { ret = reader.ReadToEnd(); }
            return ret;
        } 

        public MailSetting GetMailSetting()
        {
            if (setting != null)
            { 
                string host = System.Configuration.ConfigurationManager.AppSettings["EmailHost"];
                string port = System.Configuration.ConfigurationManager.AppSettings["EmailPort"];
                string mailTos = System.Configuration.ConfigurationManager.AppSettings["EmailTo"];
                subject = System.Configuration.ConfigurationManager.AppSettings["EmailSubject"]; 

                mailFrom = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"]; 
                mailTo = mailTos.Split(';');
                 
                if (string.IsNullOrEmpty(subject))
                    throw new ConfigurationErrorsException("App settings \'EmailSubject\' cannot be null, please check at web.config.");

                if (mailTo.Length == 0)
                    throw new ConfigurationErrorsException("App settings \'EmailTo\' cannot be null, please check at web.config.");

                if (string.IsNullOrEmpty(host) && string.IsNullOrEmpty(port) && string.IsNullOrEmpty(body) && string.IsNullOrEmpty(mailFrom))
                    throw new ConfigurationErrorsException("App settings \'EmailHost\' or \'EmailPort\' or \'EmailBody\' or \'EmailFrom\' cannot be null, please check at web.config.");

                setting.Host = host;
                setting.Port = Convert.ToInt32(port);
            }
            return setting;
        }
    }
}
