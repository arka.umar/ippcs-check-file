﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Email;
using System.Configuration;
using System.Data.Common;
using System.Reflection;

namespace Toyota.Scheduler.Delivery.Email
{
    class Program
    {
        static void Main(string[] args)
        { 
            if (args.Length == 2)
            {
                string query = ConfigurationManager.AppSettings["Query"];
                if (string.IsNullOrEmpty(query))
                    throw new ConfigurationErrorsException("\'Query\' cannot be null.");

                ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["PCS"];
                DbProviderFactory factory = DbProviderFactories.GetFactory(cs.ProviderName);
                DbConnection connection = factory.CreateConnection();
                connection.ConnectionString = cs.ConnectionString;

                connection.Open();
                DbDataReader result;
                object[] values = new object[0];
                using (DbCommand command = connection.CreateCommand())
                {
                    command.CommandText = string.Format(query, "\'" + args[0] + "\'", "\'" + args[1] + "\'");
                    result = command.ExecuteReader();
                }
                while (result.Read())
                {
                    values = new object[result.FieldCount];
                    result.GetValues(values);
                }
                connection.Close();

                ISmtpMailService mail = SmtpMailService.GetInstanceOf<NotificationSmtpMailService>();
                mail.Send(values);
            }
        }
    }
}
