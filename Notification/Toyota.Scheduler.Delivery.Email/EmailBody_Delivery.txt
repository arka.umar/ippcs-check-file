﻿<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            Dear User,
        </td>
    </tr> 
    <tr style="height:60px;">
        <td style="padding-left:50px">     
            Please be informed that this route below has potential delayed on arrival to TMMIN :
        </td>
    </tr>
    <tr>
        <td style="padding-left:50px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">
                        Delivery No
                    </td>
                    <td>:</td>
                    <td>
                        {0}
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">
                        Date
                    </td>
                    <td>:</td>
                    <td>
                        {1}
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">
                        Route  
                    </td>
                    <td>:</td>
                    <td>
                        {2}
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">
                        Rate
                    </td>
                    <td>:</td>
                    <td>
                        {3}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="height:60px;">
        <td style="padding-left:50px">For detail please reffer to Delivery Tracking Screen on IPPCS. </td>
    </tr>
    <tr>
        <td>Thank you,</td>
    </tr>
    <tr><td>Admin IPPCS</td></tr>
</table>