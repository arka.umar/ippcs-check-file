﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// niit.yudha - 16 april 2013

namespace Toyota.Common.Web.Email
{
    public interface ISmtpMailService
    {
        bool Send(object[] args);
        MailSetting GetMailSetting();
    }
}
