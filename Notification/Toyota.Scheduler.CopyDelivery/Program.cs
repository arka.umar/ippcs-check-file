﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Configuration;

namespace Toyota.Scheduler.CopyDelivery
{
    class Program
    {
        static void Main(string[] args)
        {             
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["PCS"];
            DbProviderFactory factory = DbProviderFactories.GetFactory(cs.ProviderName);
            DbConnection connection = factory.CreateConnection();
            connection.ConnectionString = cs.ConnectionString;
            string queryCopyIntoTemp = ConfigurationManager.AppSettings["QueryCopyIntoTemp"];
            string queryDeleteTemporary = ConfigurationManager.AppSettings["QueryDeleteTemporary"];
            if (string.IsNullOrEmpty(queryCopyIntoTemp))
                throw new ConfigurationErrorsException("\'QueryCopyIntoTemp\' cannot be null.");
            if (string.IsNullOrEmpty(queryDeleteTemporary))
                throw new ConfigurationErrorsException("\'QueryDeleteTemporary\' cannot be null.");
            connection.Open();
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = queryCopyIntoTemp;
                command.ExecuteNonQuery();
            } 
            connection.Close();
        }
    }
}
