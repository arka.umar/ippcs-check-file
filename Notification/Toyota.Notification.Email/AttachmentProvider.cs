﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database;

namespace Toyota.Notification.Email
{
    public class AttachmentProvider : Collection
    {
        private string attachID;
        private string functionID;
        private string roleID;
        public int dtAttach { get; set; }

        public void Create(string functionID, string roleID)
        {
            attachID = DateTime.Now.ToString("yyyyMMddhhmmssfff");
            this.functionID = functionID;
            this.roleID = roleID;
        }

        public void Write(string path)
        {
            string fullpath = "";
            if (string.IsNullOrEmpty(path))
                return;

            if (System.IO.Path.GetDirectoryName(path) == "")
                fullpath = AppDomain.CurrentDomain.RelativeSearchPath + @"\" + path;
            else
                fullpath = path;

            if (System.IO.File.Exists(fullpath))
            {
                string filename = System.IO.Path.GetFileNameWithoutExtension(fullpath);
                string extension = System.IO.Path.GetExtension(fullpath);
                byte[] dataInBytes = System.IO.File.ReadAllBytes(fullpath);
                string dataInStr = Convert.ToBase64String(dataInBytes);
                IQuery db = new DbContext("PCS");
                db.Execute("InsertAttachmentContent", new { FunctionID = functionID, RoleID = roleID, AttachID = attachID, Filename = filename, DataInStr = dataInStr, Extension = extension });
                db.Close();
            }
        }

        public string Flush()
        {
            string ret = attachID;
            attachID = null;
            return ret;
        }
    }
}
