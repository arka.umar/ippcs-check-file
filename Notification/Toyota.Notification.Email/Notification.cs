﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Notification.Email
{
    public class Notification
    {
        public string NotificationSubject { get; set; }
        public string NotificationContent { get; set; }
    }
}
