﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Email;
using Database;
using System.Collections;

namespace Toyota.Notification.Email
{
    public abstract class BaseEmail
    {

        public List<Employee> To { get; set; }
        public List<Employee> CC { get; set; }
        public List<Notification> Body { get; set; }
        public List<Attachment> Attachment { get; set; }

        private string functionID, roleID, contentID, attachID, recipientCode;
        
        public virtual void GetAttachment()
        {
            if (Attachment != null) Attachment.Clear();
            using (IQuery con = new DbContext("PCS"))
            {
                if (attachID != null)
                    Attachment = con.Query<Attachment>("GetAttachmentContent", new { FunctionID = functionID, ContentID = contentID, AttachmentID = attachID });
            }
        }

        public virtual void GetBody()
        {
            if (Body != null) Body.Clear();
            using (IQuery con = new DbContext("PCS"))
            {
                Body = con.Query<Notification>("GetNotificationContent", new { FunctionID = functionID, ContentID = contentID, Method = 1 });
            } 
        }

        public virtual void GetCC()
        {
            if (CC != null) CC.Clear();
            using (IQuery con = new DbContext("PCS"))
            {
                CC = con.Query<Employee>("GetMailCC", new { Key = recipientCode });
            } 
        }

        public virtual void GetTo()
        {
            if (To != null) To.Clear();
            using (IQuery con = new DbContext("PCS"))
            {
                To = con.Query<Employee>("GetUserPhoneEmail", new { RoleID = roleID });
            } 
        }

        public virtual void DeleteNotificationAttachment()
        {
            using (IQuery con = new DbContext("PCS"))
            {
                con.Execute("DeleteNotificationAttachment", new { FunctionID = functionID, RoleID = roleID, AttachmentID = attachID });
            }
        }
         
        public void Send(ArgumentParameter keyParameter, ArgumentParameter templateParameter, OptionalParameter optionalParameter)
        {
            if (keyParameter.Count > 2)
            {
                functionID = keyParameter.Get("functionid");
                roleID = keyParameter.Get("roleid");
                contentID = keyParameter.Get("contentid");
                attachID = "";
                recipientCode = "";

                if (optionalParameter != null)
                {
                    if (optionalParameter.ContainsKey("attachid"))
                        attachID = optionalParameter.Get("attachid");

                    if (optionalParameter.ContainsKey("recipientcode"))
                        recipientCode = optionalParameter.Get("recipientcode"); 
                }
                
                Attachment = null;
                ArrayList listAttach = new ArrayList();

                GetTo();
                GetCC();
                GetBody();
                GetAttachment();

                if (Body.Any() && To.Any())
                {
                    IDictionary<string, string> param = templateParameter.ToDictionary();
                    string content = Parse(Body.Select(s => s.NotificationContent).SingleOrDefault(), param);
                    string subject = Parse(Body.Select(s => s.NotificationSubject).SingleOrDefault(), param);

                    if (Attachment != null && Attachment.Any())
                    {
                        byte[] data;
                        string fullpath;
                        foreach (Attachment att in Attachment)
                        {
                            fullpath = AppDomain.CurrentDomain.BaseDirectory + @"\" + att.Filename + att.Extention;
                            data = Convert.FromBase64String(att.Content);
                            System.IO.File.WriteAllBytes(fullpath, data);
                            listAttach.Add(fullpath);
                        }
                    }
                    string mailCC = null;
                    if (CC.Any())
                    {
                        mailCC = CC.Select(s => s.Email).SingleOrDefault();
                    }
                    ISmtpMailService mail = SmtpMailService.GetInstanceOf<NotificationSmtpMailService>();
                    foreach (string email in To.Select(s => s.Email))
                    {
                        if (listAttach.Count == 0)
                            mail.Send(new string[] { email, subject, content, mailCC });
                        else
                            mail.Send(new string[] { email, subject, content, mailCC }, listAttach);
                    }
                    DeleteNotificationAttachment();

                    if (listAttach.Count > 0)
                    {
                        foreach (string file in listAttach)
                        {
                            System.IO.File.SetAttributes(file, System.IO.FileAttributes.Normal);
                            System.IO.File.Delete(file);
                        }
                        listAttach.Clear();
                    }
                }
            }
        }

        internal static Dictionary<string, string> Map(string[] args)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            int start, from;
            foreach (string str in args)
            {
                if (str.Contains("@"))
                {
                    start = str.IndexOf("@");
                    from = str.IndexOf("=");
                    param.Add(str.Substring(start, from), str.Substring(from + 1));
                }
            }
            return param;
        }

        internal static string Parse(string input, IDictionary<string, string> param)
        {
            string content = input;
            foreach (string key in param.Keys)
            {
                content = content.Replace(key, param[key]);
            }
            return content;
        }
    }
}
