﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Notification.Email
{
    public class Employee
    {
        public string Email { get; set; }
        public string Telephone { get; set; }
    }
}
