﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Notification.Email
{
    public class OptionalParameter : Collection
    {
        public OptionalParameter()
        {
            map.Add("recipientcode", "-");
            map.Add("attachmentid", "-"); 
        }

        public virtual bool ContainsKey(string key)
        {
            return map.ContainsKey(key);
        }

        public override void Add(string key, string value)
        {
            if (key.ToLower() == "recipientcode")
                this.map[key] = value;
            else if (key.ToLower() == "attachmentid")
                this.map[key] = value;
            else
                base.Add(key, value);
        }
         
    }
}
