DECLARE @TempMailCC TABLE
        (
          SeqNumber INT ,
          Telephone NVARCHAR(250) ,
          Email VARCHAR(1000)
        )
                 

------------------------------YOUR CODE-----------------------------------      
INSERT  INTO @TempMailCC
SELECT ROW_NUMBER() OVER ( ORDER BY EMAIL_ADDRESS ) AS SeqNumber ,
        '0' AS 'Telephone' ,
        EMAIL_ADDRESS as 'EMAIL'
FROM dbo.TB_T_EMAIL_RECIPIENT
WHERE RECIPIENT_CD = @Key
-----------------------------END YOUR CODE--------------------------------


DECLARE @Phone VARCHAR(250) ,
        @EmailCC VARCHAR(1000) ,
        @Max INT ,
        @i INT ,
        @FinalTelephone VARCHAR(250) ,
        @FinalEmailCC VARCHAR(1000)
		
SELECT  @i = 1 ,
        @Phone = '' ,
        @EmailCC = '' ,
        @FinalTelephone = '' ,
        @FinalEmailCC = ''	

SELECT  @Max = COUNT(1)
FROM    @TempMailCC	


WHILE ( @i < ( @Max + 1 ) ) 
        BEGIN
                SELECT  @FinalTelephone = @FinalTelephone + Telephone + ';' ,
                        @FinalEmailCC = @FinalEmailCC + Email + ';'
                FROM    @TempMailCC
                WHERE   SeqNumber = @i 
               
                SET @i = @i + 1
        END
        
SELECT  @FinalTelephone AS 'Telephone',
        @FinalEmailCC AS 'Email'