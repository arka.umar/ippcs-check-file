@@SEQ_NUMBER INT

    SELECT @@SEQ_NUMBER = ISNULL(MAX(SEQ_NUMBER), 0) + 1 FROM dbo.TB_R_NOTIFICATION_ATTACHMENT
    WHERE FUNCTION_ID = @0
      AND ROLE_ID = @1
      AND ATTACHMENT_ID = @2

    INSERT INTO dbo.TB_R_NOTIFICATION_ATTACHMENT
    ( 
        FUNCTION_ID ,
        ROLE_ID ,
        ATTACHMENT_ID ,
        SEQ_NUMBER ,
        ATTACHMENT_FILENAME ,
        ATTACHMENT_CONTENT ,
        ATTACHMENT_EXTENSION
    )
    VALUES  ( 
        @0 , -- FUNCTION_ID - int
        @1 , -- ROLE_ID - int
        @2 , -- ATTACHMENT_ID - int
        @@SEQ_NUMBER , -- SEQ_NUMBER - int
        @3 , -- ATTACHMENT_FILENAME - varchar(250)
        @4 , -- ATTACHMENT_CONTENT - text
        @5  -- ATTACHMENT_EXTENSION - varchar(5)
    )