﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Notification.Email
{
    public class Attachment
    {
        public string Filename { get; set; }
        public string Content { get; set; }
        public string Extention { get; set; }
    }
}
