﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using Toyota.Email;
using System.Collections;  

// niit.yudha - 16 april 2013

namespace Toyota.Notification.Email
{
    public class NotificationSmtpMailService : ISmtpMailService
    {

        private SmtpEmail mail = new SmtpEmail(); 
        private string mailFrom; 
        private MailSetting setting;

        public NotificationSmtpMailService()
        {
            this.setting = new MailSetting();
            this.GetMailSetting();
        }

        private void InitSend(object[] args)
        { 
            mail.MailTo = new string[] { (string)args[0] };
            mail.Subject = (string)args[1];
            mail.Body = (string)args[2];
            if (!string.IsNullOrEmpty((string)args[3]) || args[3] != null)
            {
                string[] mailCCs = ((string)args[3]).Split(';');
                mail.MailCC = mailCCs;
            }   
            if (args[2].ToString().Contains("/>"))
                mail.IsBodyHtml = true;
            mail.Setting = setting;
            mail.MailFrom = mailFrom; 
        }

        public bool Send(object[] args, ArrayList attachment)
        {
            try
            {
                InitSend(args);
                return mail.Send(attachment);
            }
            catch (Exception ex)
            { throw (ex); }
        }

        public bool Send(object[] args)
        {
            try
            {
                InitSend(args);
                return mail.Send();
            }
            catch (Exception ex)
            { throw (ex); }
        }
         
        public MailSetting GetMailSetting()
        {
            if (setting != null)
            { 
                string host = System.Configuration.ConfigurationManager.AppSettings["EmailHost"];
                string port = System.Configuration.ConfigurationManager.AppSettings["EmailPort"];  

                mailFrom = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"]; 
                
                if (string.IsNullOrEmpty(host) && string.IsNullOrEmpty(port) && string.IsNullOrEmpty(mailFrom))
                    throw new ConfigurationErrorsException("App settings \'EmailHost\' or \'EmailPort\' or \'EmailBody\' or \'EmailFrom\' cannot be null, please check at web.config.");

                setting.Host = host;
                setting.Port = Convert.ToInt32(port);
            }
            return setting;
        } 

        public void Dispose()
        {
            
        }
    }
}
