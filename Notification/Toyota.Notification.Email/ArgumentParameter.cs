﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Notification.Email
{
    public class ArgumentParameter : Collection
    {
        public ArgumentParameter() { }
        public string ToArgument()
        {
            StringBuilder build = new StringBuilder();
            const string formatArgs = " \"{0}={1}\"";
            foreach (string key in this.map.Keys)
            {
                build.Append(string.Format(formatArgs, key, map[key]));
            }
            return build.ToString();
        }
    }
}
