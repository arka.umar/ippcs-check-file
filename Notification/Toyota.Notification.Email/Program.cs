﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.Common;
using System.Reflection;
using Database;  
using Toyota.Email;
using System.Collections;

namespace Toyota.Notification.Email
{
    internal class Program
    {
        static void Main(string[] args)
        { 
            if (args.Length > 2)
            {
                string functionID = args[0];
                string roleID = args[1];
                string contentID = args[2];
                string method = args[3];
                //string attachID = null;
                //string recipientCode = null;
                string attachID = args[4];
                string recipientCode = args[5];


                if (!args[args.Length - 2].Contains("@"))
                    attachID = args[args.Length - 2];

                if (!args[args.Length - 1].Contains("@"))
                    recipientCode = args[args.Length - 1];

                List<Notification> notif;
                List<Employee> employee;
                List<Employee> employeeCC;
                List<Attachment> attach = null;
                ArrayList listAttach = new ArrayList();
                using (IQuery con = new DbContext("PCS"))
                {
                    if (attachID != null)
                        attach = con.Query<Attachment>("GetAttachmentContent", new { FunctionID = functionID, ContentID = contentID, AttachmentID = attachID });

                    notif = con.Query<Notification>("GetNotificationContent", new { FunctionID = functionID, ContentID = contentID, Method = method });
                    employee = con.Query<Employee>("GetUserPhoneEmail", new { RoleID = roleID });

                    employeeCC = con.Query<Employee>("GetMailCC", new { Key = recipientCode });
                }

                if (notif.Any() && employee.Any())
                {
                    IDictionary<string, string> param = Map(args);
                    string content = Parse(notif.Select(s => s.NotificationContent).SingleOrDefault(), param);
                    string subject = Parse(notif.Select(s => s.NotificationSubject).SingleOrDefault(), param);

                    if (attach != null && attach.Any())
                    {
                        byte[] data;
                        string fullpath;
                        foreach (Attachment att in attach)
                        {
                            fullpath = AppDomain.CurrentDomain.BaseDirectory + @"\" + att.Filename + att.Extention;
                            data = Convert.FromBase64String(att.Content);
                            System.IO.File.WriteAllBytes(fullpath, data);
                            listAttach.Add(fullpath);
                        }
                    }
                    string mailCC = null;
                    if (employeeCC.Any())
                    {
                        mailCC = employeeCC.Select(s => s.Email).SingleOrDefault();
                    }
                    ISmtpMailService mail = SmtpMailService.GetInstanceOf<NotificationSmtpMailService>();
                    foreach (string email in employee.Select(s => s.Email))
                    {
                        if (listAttach.Count == 0)
                            mail.Send(new string[] { email, subject, content, mailCC });
                        else
                            mail.Send(new string[] { email, subject, content, mailCC }, listAttach);
                    }
                    using (IQuery con = new DbContext("PCS"))
                    {
                        con.Execute("DeleteNotificationAttachment", new { FunctionID = functionID, RoleID = roleID, AttachmentID = attachID });
                    } 
                    if (listAttach.Count > 0)
                    {
                        foreach (string file in listAttach)
                        {
                            System.IO.File.SetAttributes(file, System.IO.FileAttributes.Normal);
                            System.IO.File.Delete(file);
                        }
                    }
                }
            }
        }

        internal static Dictionary<string, string> Map(string[] args)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            int start, from;
            foreach (string str in args)
            {
                if (str.Contains("@"))
                {
                    start = str.IndexOf("@");
                    from = str.IndexOf("=");
                    param.Add(str.Substring(start, from), str.Substring(from + 1));
                }
            }
            return param;
        }

        internal static string Parse(string input, IDictionary<string, string> param)
        {
            string content = input;
            foreach (string key in param.Keys)
            {
                content = content.Replace(key, param[key]);
            }
            return content;
        }
    }
}
