﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Notification.Email
{
    public class Collection
    {
        protected IDictionary<string, string> map;

        public Collection()
        {
            map = new Dictionary<string, string>();
        }

        public virtual void Add(string key, string value)
        {
            if (!map.ContainsKey(key))
            {
                map.Add(key, value);
            }
        }

        public virtual void Remove(string key)
        {
            if (map.ContainsKey(key))
            {
                map.Remove(key);
            }
        }

        public virtual string Get(string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            return null;
        }

        public virtual void Clear()
        {
            map.Clear();
        }

        public IDictionary<string, string> ToDictionary()
        {
            return map;
        }

        public int Count { get { return map.Count; } }
    }
}
