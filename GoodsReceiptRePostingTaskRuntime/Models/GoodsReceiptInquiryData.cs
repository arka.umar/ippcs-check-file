﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodsReceiptRePostingTaskRuntime.Models
{
    class GoodsReceiptInquiryData
    {
        public String MANIFEST_NO { get; set; }
        public String ORDER_TYPE { get; set; }
        public String SUPPLIER_CD { get; set; }
        public String SUPPLIER_NAME { get; set; }
        public String RCV_PLANT_CD { get; set; }
        public String DOCK_CD { get; set; }
        public Int32 TOTAL_ITEM { get; set; }
        public Int32 TOTAL_QTY { get; set; }
        public String MANIFEST_RECEIVE_FLAG { get; set; }

        public Boolean? DELAY_FLAG { get; set; }
        public Boolean? PROBLEM_FLAG { get; set; }

        public String ORDER_NO { get; set; }
        public String PO_NO { get; set; }

        public String ROUTE_RATE { get; set; }
        public DateTime? ARRIVAL_PLAN_DT { get; set; }
        public DateTime? ARRIVAL_ACTUAL_DT { get; set; }

        public DateTime? SCAN_DT { get; set; }
        public DateTime? APPROVED_DT { get; set; }
        public String APPROVED_BY { get; set; }

        public String MAT_DOC_NO { get; set; }
        public Boolean? ICS_FLAG { get; set; }
        public DateTime? ICS_DT { get; set; }

        public String INVOICE_NO { get; set; }
        public DateTime? INVOICE_DT { get; set; }
        public String INVOICE_BY { get; set; }

        public DateTime? CANCEL_DT { get; set; }
        public String CANCEL_BY { get; set; }

        public DateTime? GR_DOCUMENT_DT { get; set; }
        public Boolean? CANCEL_FLAG { get; set; }
        public String DELETION_FLAG { get; set; }

        public String IN_PROGRESS { get; set; }
    }
}
