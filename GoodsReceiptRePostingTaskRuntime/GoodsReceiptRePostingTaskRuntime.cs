﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Toyota.Common.Task;
using Toyota.Common.Task.External;
using Toyota.Common.Database;
using Toyota.Common.Logging;
using Toyota.Common.Web.Service;
using Toyota.Common.Util.Text;
using Toyota.Common.Database.Petapoco;

using GoodsReceiptRePostingTaskRuntime.Models;
using System.Reflection;
using Toyota.Common.Logging.Sink;

namespace GoodsReceiptRePostingTaskRuntime
{
    class GoodsReceiptRePostingTaskRuntime : ExternalBackgroundTaskRuntime
    {
        protected override void ExecuteProcess(BackgroundTaskParameter parameters)
        {
            List<GoodsReceiptRePostingTaskRuntimeThread> runtimeThreadList = new List<GoodsReceiptRePostingTaskRuntimeThread>();

            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(GoodsReceiptRePostingTaskRuntime)), "GoodsReceiptRePostingTaskRuntime.SQLFiles"));
            IDBContext db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            #region Testing script get current data from database.
            List<GoodsReceiptInquiryData> goodsReceiptInquiryDataModel = new List<GoodsReceiptInquiryData>();
            goodsReceiptInquiryDataModel = db.Query<GoodsReceiptInquiryData>(
                    DatabaseManager.LoadQuery("GetAllGoodsReceiptInquiry"),
                    new object[] { 
                        DateTime.Now.ToString("yyyy-MM-dd"),   // ARRIVAL TIME FROM
                        DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"),   // ARRIVAL TIME TO
                        "",     // DOCK CD
                        "",     // SUPPLIER CD
                        "",     // RCV PLANT CD
                        "",     // ROUTE RATEd
                        "1101241251,1101243764,1101244558,1201300921,1201301572,1201301625,1201304098,1201304489,1201305163,1201306076,1201308121,4002354943,4002354968",     // MANIFEST NO
                        "5",    // MANIFEST RECEIVE FLAG
                        "",     // ORDER NO
                        ""      // DOCK CD LDAP
                        }).ToList<GoodsReceiptInquiryData>();

            List<IDictionary<string, string>> dataList = new List<IDictionary<string, string>>();
            IDictionary<string, string> dataMap;
            for (int i = 0; i < goodsReceiptInquiryDataModel.Count; i++)
            {
                dataMap = new Dictionary<string, string>();
                dataMap.Add("DockCode", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].DOCK_CD) ? "" : goodsReceiptInquiryDataModel[i].DOCK_CD.Trim());
                dataMap.Add("SupplierCode", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].SUPPLIER_CD) ? "" : goodsReceiptInquiryDataModel[i].SUPPLIER_CD.Trim());
                dataMap.Add("RecPlantCode", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].RCV_PLANT_CD) ? "" : goodsReceiptInquiryDataModel[i].RCV_PLANT_CD.Trim());
                dataMap.Add("ManifestNo", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].MANIFEST_NO) ? "" : goodsReceiptInquiryDataModel[i].MANIFEST_NO.Trim());
                dataMap.Add("OrderNo", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].ORDER_NO) ? "" : goodsReceiptInquiryDataModel[i].ORDER_NO.Trim());
                dataMap.Add("ApprovedBy", String.IsNullOrEmpty(goodsReceiptInquiryDataModel[i].APPROVED_BY) ? "" : goodsReceiptInquiryDataModel[i].APPROVED_BY.Trim());
                dataList.Add(dataMap);
            }
            String process = "Approve";
            String username = "System";
            #endregion

            db.Close();

            /* Get parameter with indexing */
            //List<IDictionary<string, string>> dataList = JSON.ToObject<List<IDictionary<string, string>>>(parameters[1]);
            //String process = parameters[2];
            //String username = parameters[3];

            /* Get parameter with key */
            //List<IDictionary<string, string>> dataList = parameters.Get<List<IDictionary<string, string>>>("DataList");
            //String process = parameters.Get("Process");
            //String username = parameters.Get("Username");

            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Connection String : " + DatabaseManager.GetDefaultConnectionDescription().ConnectionString,
                Severity = LoggingSeverity.Warning
            });
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Datalist : " + JSON.ToString<List<IDictionary<string, string>>>(dataList),
                Severity = LoggingSeverity.Warning
            });
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Process : " + process,
                Severity = LoggingSeverity.Warning
            });
            DefaultLogSession.WriteLine(new LoggingMessage()
            {
                Message = "Username : " + username,
                Severity = LoggingSeverity.Warning
            });

            try
            {
                bool isValid = true;
                string manifestReceiveFlag = "";

                db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);
                foreach (IDictionary<string, string> map in dataList)
                {
                    manifestReceiveFlag = db.ExecuteScalar<string>(
                                            DatabaseManager.LoadQuery("GetAllGoodsReceiptInquiryStatusByManifest"),
                                            new object[] {
                                                map["ManifestNo"]
                                        });

                    if (!manifestReceiveFlag.Equals("2") && !manifestReceiveFlag.Equals("3") && !manifestReceiveFlag.Equals("5"))
                        isValid = false;
                }
                db.Close();
                                
                if (!isValid)
                {
                    DefaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = process + " Failed! You can only approve manifest(s) which status : Scanned, Approved, Error Posting.",
                        Severity = LoggingSeverity.Warning
                    });
                }
                else
                {
                    SetProgress(5);

                    const int PROCESS_CAPACITY = 30;

                    int processLastIndex = dataList.Count;
                    int processCounter = 0;
                    int processFetchSize = PROCESS_CAPACITY;
                    int processIndexDifference;
                    int processIndex = 1;
                    bool processLooping = true;

                    // if there is no data pass for posting
                    if (processCounter == processLastIndex)
                    {
                        processLooping = false;
                    }
                    else
                    {
                        processIndexDifference = (processLastIndex - processCounter);
                        if (processIndexDifference < PROCESS_CAPACITY)
                        {
                            processFetchSize = processIndexDifference;
                        }
                        else
                        {
                            processFetchSize = PROCESS_CAPACITY;
                        }
                    }

                    while (processLooping)
                    {
                        DefaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = String.Empty,
                            Severity = LoggingSeverity.Info
                        });
                        DefaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = "Process Counter : " + processIndex,
                            Severity = LoggingSeverity.Info
                        });
                        DefaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = String.Empty,
                            Severity = LoggingSeverity.Info
                        });

                        foreach (IDictionary<string, string> map in dataList.GetRange(processCounter, processFetchSize))
                        {
                            #region Update Initial Status GR

                            DefaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Start in update initial status GR. Manifest No : " + map["ManifestNo"],
                                Severity = LoggingSeverity.Info
                            });

                            try
                            {
                                db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);
                                switch (process)
                                {
                                    case "Approve":
                                        db.ExecuteScalar<string>(
                                            DatabaseManager.LoadQuery("ApproveGoodsReceiptInquiry"),
                                            new object[] {
                                                    map["DockCode"],   // dockCode
                                                    map["SupplierCode"],   // supplierCode
                                                    map["RecPlantCode"],   // rcvPlantCode
                                                    map["ManifestNo"],   // manifestNo
                                                    "3",
                                                    map["OrderNo"],   // orderNo 
                                                    username
                                            });
                                        break;
                                    case "Cancel":
                                        db.ExecuteScalar<string>(
                                            DatabaseManager.LoadQuery("CancelGoodsReceiptInquiry"),
                                            new object[] {
                                                    map["DockCode"],   // dockCode
                                                    map["SupplierCode"],   // supplierCode
                                                    map["RecPlantCode"],   // rcvPlantCode
                                                    map["ManifestNo"],   // manifestNo
                                                    "8",
                                                    map["OrderNo"],   // orderNo 
                                                    username
                                            });
                                        break;
                                }
                                db.Close();
                            }
                            catch (Exception exc)
                            {
                                db.Close();

                                DefaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Error in update initial status GR. Manifest No : " + map["ManifestNo"] + ". Message : " + exc.Message,
                                    Severity = LoggingSeverity.Error
                                });

                                throw exc;
                            }

                            DefaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Finish in update initial status GR. Manifest No : " + map["ManifestNo"],
                                Severity = LoggingSeverity.Info
                            });
                            //SetProgress(10);

                            #endregion
                        }

                        #region Threading Functionality

                        LoggingSession loggingSession = LogManager.CreateSession(
                                "GoodsReceiptPostingSession_" + Id + "_" + processIndex,
                                new LoggingSink[] { 
                                new TextFileLoggingSink(
                                    "GoodsReceiptPostingSession_" + Id + "_" + processIndex, 
                                    ExternalBackgroundTaskRuntime.DEFAULT_LOG_FOLDER
                                    ) ,
                                new CommandPromptLoggingSink(
                                    "GoodsReceiptPostingSession_" + Id + "_" + processIndex
                                    )
                            });
                        loggingSession.EnableMultiSink = true;
                        loggingSession.AutoFlush = true;

                        GoodsReceiptRePostingTaskRuntimeThread runtimeThread = new GoodsReceiptRePostingTaskRuntimeThread(loggingSession);
                        runtimeThreadList.Add(runtimeThread);

                        Dictionary<string, string> runtimeThreadParameter = new Dictionary<string, string>();
                        runtimeThreadParameter.Add("DataList", JSON.ToString<List<IDictionary<string, string>>>(dataList.GetRange(processCounter, processFetchSize)));
                        runtimeThreadParameter.Add("Process", process);
                        runtimeThreadParameter.Add("Username", username);

                        runtimeThread.Start(runtimeThreadParameter);
                        #endregion

                        processCounter += processFetchSize;

                        if (processCounter == processLastIndex)
                        {
                            processLooping = false;
                        }
                        else
                        {
                            processIndexDifference = (processLastIndex - processCounter);
                            if (processIndexDifference < PROCESS_CAPACITY)
                            {
                                processFetchSize = processIndexDifference;
                            }
                            else
                            {
                                processFetchSize = PROCESS_CAPACITY;
                            }
                        }

                        processIndex++;
                    }

                    // Check for thread alive status infinitive loop if there still exists active thread.
                    int counter = runtimeThreadList.Count;
                    while (counter > 0)
                    {
                        counter = 0;

                        foreach (GoodsReceiptRePostingTaskRuntimeThread runtimeThread in runtimeThreadList)
                        {
                            if (runtimeThread.IsAlive())
                                counter++;
                        }

                        if (counter > 0)
                            System.Threading.Thread.Sleep(1);
                    }

                    foreach (GoodsReceiptRePostingTaskRuntimeThread runtimeThread in runtimeThreadList)
                    {
                        if (runtimeThread.IsError())
                        {
                            SetStatus(TaskStatus.Error);
                            throw runtimeThread.ErrorException();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                db.Close();

                DefaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Error in do process " + process + " by " + username + " id " + Id + ". Message : " + exc.Message,
                    Severity = LoggingSeverity.Error
                });

                throw exc;
            }
            finally
            {
                // Delege log process lock table
                //db.ExecuteScalar<string>(
                //        DatabaseManager.LoadQuery("DeleteLogProcessLockByFunctionNo"),
                //        new object[] {
                //                process
                //                ,username
                //        });
                SetProgress(100);
            }
        }
    }

    class GoodsReceiptRePostingTaskRuntimeThread : ExternalBackgroundTaskRuntime
    {
        IDBContext db = null;
        LoggingSession defaultLogSession = null;
        //Thread defaultThread = null;

        public GoodsReceiptRePostingTaskRuntimeThread(LoggingSession paramLoggingSession)
        {
            DatabaseManager.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(GoodsReceiptRePostingTaskRuntime)), "GoodsReceiptRePostingTaskRuntime.SQLFiles"));
            //db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

            defaultLogSession = paramLoggingSession;

            isError = false;
            isAlive = true;
        }

        public void Start(Dictionary<string, string> paramThread)
        {
            //defaultThread = new Thread(new ParameterizedThreadStart(RunPostingDelegate));
            //defaultThread.Start(paramThread);

            ThreadPool.QueueUserWorkItem(new WaitCallback(RunPostingDelegate), paramThread);
        }

        public void Stop()
        {
            isAlive = false;

            //if (defaultThread.IsAlive)
            //    defaultThread.Abort();
        }

        private Exception errorException;
        public Exception ErrorException()
        {
            return errorException;
        }

        private bool isError;
        public bool IsError()
        {
            return isError;
        }

        private bool isAlive;
        public bool IsAlive()
        {
            return isAlive;

            //return defaultThread.IsAlive;
        }

        public void Close()
        {
            defaultLogSession.Dispose();
        }

        public void RunPostingDelegate(Object o)
        {
            Dictionary<string, string> map = (Dictionary<string, string>)o;
            RunPosting(JSON.ToObject<List<IDictionary<string, string>>>(map["DataList"]), map["Process"], map["Username"]);
        }

        private void RunPosting(List<IDictionary<string, string>> dataList, String process, String username)
        {
            try
            {
                //throw new Exception();
                
                string sendFlag = "";
                string processID = "";
                string sourceType = "";

                IList<GoodsReceiptICSTransfer> tempList;
                List<GoodsReceiptICSTransfer> list = new List<GoodsReceiptICSTransfer>();
                List<Dictionary<string, string>> noDataList = new List<Dictionary<string, string>>();

                #region Phase 1 : Generate Posting File GR

                db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

                defaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Start in generate posting file GR.",
                    Severity = LoggingSeverity.Info
                });
                //SetProgress(0);

                foreach (Dictionary<string, string> data in dataList)
                {
                    try
                    {
                        tempList = db.Fetch<GoodsReceiptICSTransfer>(DatabaseManager.LoadQuery("ICSGeneratePostingFileGR"), new object[] { data["ApprovedBy"], data["ManifestNo"], data["SupplierCode"], data["RecPlantCode"], data["DockCode"] });

                        if ((tempList.Count > 0) && (tempList != null))
                        {
                            list.AddRange(tempList);
                            sendFlag = list[0].sendFlag;
                            processID = list[0].processID;
                            sourceType = list[0].sourceType;
                        }
                        else
                        {
                            noDataList.Add(data);

                            defaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "No data found in generate posting file GR. Manifest No : " + data["ManifestNo"],
                                Severity = LoggingSeverity.Warning
                            });
                        }
                    }
                    catch (Exception exc)
                    {
                        isError = true;

                        defaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = "Error in generate posting file GR. Manifest No : " + data["ManifestNo"] + ". Message : " + exc.Message,
                            Severity = LoggingSeverity.Error
                        });

                        throw exc;
                    }

                }

                #region Update error status to no data manifest

                foreach (Dictionary<string, string> data in noDataList)
                {
                    db.ExecuteScalar<string>(
                        DatabaseManager.LoadQuery("ApproveGoodsReceiptInquiry"),
                        new object[] {
                            data["DockCode"],   // dockCode
                            data["SupplierCode"],   // supplierCode
                            data["RecPlantCode"],   // rcvPlantCode
                            data["ManifestNo"],   // manifestNo
                            "5",
                            data["OrderNo"],   // orderNo 
                            username
                    });
                }
                #endregion

                defaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Finish in generate posting file GR.",
                    Severity = LoggingSeverity.Info
                });
                //SetProgress(0);

                db.Close();

                #endregion

                if (list.Count == 0 & sendFlag != "" & processID != "")
                {
                    defaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = "No data found in generate posting file GR.",
                        Severity = LoggingSeverity.Warning
                    });
                }
                else
                {
                    // Generate process id
                    processID = db.ExecuteScalar<string>(
                        DatabaseManager.LoadQuery("GenerateProcessId"), 
                        new object[] { 
                            process,     // what
                            username,     // user
                            "Goods Receipt Inquiry",     // where
                            "",     // pid OUTPUT
                            "",     // id
                            "",     // type
                            "4",     // module
                            process == "Approve" ? "42001" : "42002",     // function
                            "0"      // sts
                        });

                    GatewayService.WebServiceImplClient gateway = new GatewayService.WebServiceImplClient();
                    ServiceResult result;

                    #region Phase 2 : Startup IPPCS - ICS Gateway Web Service

                    /*
                    * Commented By : fid.salman
                    * Commented Dt : 02.08.2013
                    * Note         : IPPCS - ICS Gateway web service was non-activated cause direct
                    *                communication to ICS web service used in the posting process
                    */

                    //defaultLogSession.WriteLine(new LoggingMessage()
                    //{
                    //    Message = "Do process startup IPPCS - ICS Gateway web service.",
                    //    Severity = LoggingSeverity.Info
                    //});

                    //bool serviceReady = false;

                    //try
                    //{
                    //    gateway.Open();
                    //    string testStringResult = gateway.execute("Startup", "IntegrityCheck", null);
                    //    if (!string.IsNullOrEmpty(testStringResult))
                    //    {
                    //        result = ServiceResult.Create(testStringResult);
                    //        serviceReady = (result.Status == ServiceResult.STATUS_READY);
                    //    }
                    //}
                    //catch (Exception exc)
                    //{
                    //    gateway.Abort();
                    //    gateway = new GatewayService.WebServiceImplClient();
                    //    gateway.Open();

                    //    defaultLogSession.WriteLine(new LoggingMessage()
                    //    {
                    //        Message = "Error do process startup IPPCS - ICS Gateway web service. Message : " + exc.Message,
                    //        Severity = LoggingSeverity.Error
                    //    });

                    //    throw exc;
                    //}

                    //if (!serviceReady)
                    //{
                    //    defaultLogSession.WriteLine(new LoggingMessage()
                    //    {
                    //        Message = "Fail do process startup IPPCS - ICS Gateway web service.",
                    //        Severity = LoggingSeverity.Warning
                    //    });
                    //}

                    #endregion

                    #region Phase 3 : Init State ICS Web Service

                    defaultLogSession.WriteLine(new LoggingMessage()
                    {
                        Message = "Do process init state ICS web service.",
                        Severity = LoggingSeverity.Info
                    });

                    string jsonResult;
                    ServiceParameters sp = new ServiceParameters();
                    string sessionID;
                    sp.Add("state", "init");
                    result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                    #endregion

                    if (result.Status != ServiceResult.STATUS_ERROR)
                    {
                        #region Phase 4 : Transfer Data to ICS Web Service

                        defaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = "Start in transfer data to ICS web service.",
                            Severity = LoggingSeverity.Info
                        });

                        sessionID = result.Value.ToString();

                        sp.Clear();
                        sp.Add("state", "transfer");
                        sp.Add("sessionId", result.Value);
                        sp.Add("data", String.Empty);

                        const int MAX_DATA_TRANSFERRED = 10;
                        int dataLength = list.Count;
                        int lastIndex = dataLength;
                        int counter = 0;
                        int fetchSize;
                        int indexDifference;
                        bool looping = true;
                        bool transferError = false;
                        while (looping)
                        {
                            indexDifference = (lastIndex - counter);
                            if (indexDifference < MAX_DATA_TRANSFERRED)
                            {
                                fetchSize = indexDifference;
                            }
                            else
                            {
                                fetchSize = MAX_DATA_TRANSFERRED;
                            }

                            tempList = list.GetRange(counter, fetchSize);
                            jsonResult = JSON.ToString<List<GoodsReceiptICSTransfer>>(tempList);

                            sp.Remove("data");
                            sp.Add("data", (object)jsonResult);
                            sp.Add("userId", username);
                            result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                            if (result.Status == ServiceResult.STATUS_SUCCESS)
                            {
                                counter += (fetchSize);
                            }
                            else
                            {
                                transferError = true;
                                looping = false;
                            }

                            if (counter == lastIndex)
                            {
                                looping = false;
                            }

                        }

                        defaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = "Finish in transfer data to ICS web service.",
                            Severity = LoggingSeverity.Info
                        });
                        //SetProgress(0);

                        #endregion

                        if (transferError)
                        {
                            defaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Fail in transfer data to ICS web service.",
                                Severity = LoggingSeverity.Warning
                            });
                            //SetProgress(100);
                        }
                        else
                        {
                            #region Phase 5 : Taking Data From ICS Web Service

                            db = DatabaseManager.GetContext(DatabaseManager.GetDefaultConnectionDescription().Name);

                            defaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Start in taking data from ICS web service.",
                                Severity = LoggingSeverity.Info
                            });

                            defaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Do process state in ICS web service.",
                                Severity = LoggingSeverity.Info
                            });

                            sp.Clear();
                            sp.Add("state", "process");
                            sp.Add("sessionId", sessionID);
                            sp.Add("sendFlag", sendFlag);
                            sp.Add("userId", username);
                            sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                            result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                            /* Sorry men, nambahin buat debugging aja */
                            //defaultLogSession.WriteLine(new LoggingMessage(string.Format("Process result: Status -> {0}, Value: {1}", result.Status, result.Value)));

                            if (result.Status != ServiceResult.STATUS_ERROR)
                            {
                                defaultLogSession.WriteLine(new LoggingMessage(string.Format("Process result: Status -> {0}, Value: {1}", result.Status, result.Value)));

                                defaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Start in inquiry state in ICS web service.",
                                    Severity = LoggingSeverity.Info
                                });

                                int totalPage = Convert.ToInt32(result.Value != null ? result.Value : 0);
                                sp.Clear();
                                sp.Add("state", "inquiry");
                                sp.Add("sessionId", sessionID);
                                sp.Add("processId", processID);
                                int seq_no = 1;

                                for (int i = 1; i <= totalPage; i++)
                                {
                                    try
                                    {
                                        sp.Add("currentPage", i);
                                        sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                                        result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));

                                        /* Sorry men, nambahin buat debugging aja */
                                        defaultLogSession.WriteLine(new LoggingMessage(string.Format("Inquiry result: data --> {0}", result.Value)));
                                    }
                                    catch (Exception exc)
                                    {
                                        isError = true;

                                        if (exc.Message.Contains("Timeout") || exc.Message.Contains("timeout"))
                                        {
                                            sp.Add("currentPage", i);
                                            sp.Add("maxDataPerPage", MAX_DATA_TRANSFERRED);
                                            result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));
                                        }

                                        defaultLogSession.WriteLine(new LoggingMessage()
                                        {
                                            Message = "Error do process inquiry state in ICS web service. Message : " + exc.Message,
                                            Severity = LoggingSeverity.Error
                                        });
                                        //SetProgress(100);

                                        throw exc;
                                    }

                                    if (result.Status != ServiceResult.STATUS_ERROR)
                                    {
                                        if (result.Value == null)
                                        {
                                            defaultLogSession.WriteLine(new LoggingMessage()
                                            {
                                                Message = "No data found do process inquiry state in ICS web service.",
                                                Severity = LoggingSeverity.Warning
                                            });
                                        }

                                        List<GoodsReceiptICSFinish> values = new List<GoodsReceiptICSFinish>();
                                        values = JSON.ToObject<List<GoodsReceiptICSFinish>>(result.Value.ToString());
                                        Nullable<DateTime> postingDate = null;
                                        Nullable<DateTime> processDate = null;

                                        var errorCollection = from value in values
                                                              where value.errorStatus == "1"
                                                              select value;
                                        int errorCollectionTotal = errorCollection.Count();

                                        if (errorCollectionTotal > 0)
                                        {
                                            defaultLogSession.WriteLine(new LoggingMessage()
                                            {
                                                Message = "Do process inserting data from ICS web service into IPPCS with return error.",
                                                Severity = LoggingSeverity.Warning
                                            });
                                        }
                                        else
                                        {
                                            defaultLogSession.WriteLine(new LoggingMessage()
                                            {
                                                Message = "Do process inserting data from ICS web service into IPPCS.",
                                                Severity = LoggingSeverity.Info
                                            });
                                        }

                                        foreach (GoodsReceiptICSFinish e in values)
                                        {
                                            postingDate = null;
                                            processDate = null;

                                            if (!String.IsNullOrEmpty(e.postingDate))
                                                //postingDate = Convert.ToDateTime(e.postingDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));
                                                postingDate = DateTime.ParseExact(e.postingDate, "dd/MM/yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));
                                            if (!String.IsNullOrEmpty(e.processDate))
                                                //processDate = Convert.ToDateTime(e.processDate, System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));
                                                processDate = DateTime.ParseExact(e.processDate, "dd/MM/yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("id-ID"));

                                            db.ExecuteScalar<string>(DatabaseManager.LoadQuery("ICSInsertPostingFileGR"), new object[] { 
                                            e.systemSource
                                            ,e.userId
                                            ,e.movementType
                                            ,processDate
                                            ,postingDate
                                            ,e.refNo
                                            ,e.kanbanOrderNo
                                            ,e.prodPurpose
                                            ,e.sourceType
                                            ,e.oriMatNo
                                            ,e.oriSuppCode
                                            ,e.receivingPlantArea
                                            ,e.plantCode
                                            ,e.slocCode
                                            ,e.matDocNo
                                            ,e.matDocYear
                                            ,e.errorStatus
                                            ,e.errorCode
                                            ,e.errorDesc
                                            ,e.errorMsg
                                            ,processID
                                            ,e.sessionId
                                            ,username
                                            ,process
                                            ,seq_no
                                            ,e.poNo
                                            });

                                            seq_no++;

                                        }

                                        IEnumerable<string> manifestNoList = values.Select(s => s.refNo).Distinct();

                                        foreach (string manifestNo in manifestNoList)
                                        {
                                            if (errorCollectionTotal == 0)
                                            {
                                                // clear transaction log after process
                                                defaultLogSession.WriteLine(new LoggingMessage()
                                                {
                                                    Message = "Do process delete log transaction table. Manifest no : " + manifestNo,
                                                    Severity = LoggingSeverity.Info
                                                });

                                                db.Execute(DatabaseManager.LoadQuery("DeleteLogTransactionByManifest"), new object[] { manifestNo, processID });
                                            }

                                            // clear manifest detail
                                            defaultLogSession.WriteLine(new LoggingMessage()
                                            {
                                                Message = "Do process delete manifest detail table. Manifest no : " + manifestNo,
                                                Severity = LoggingSeverity.Info
                                            });

                                            db.Execute(DatabaseManager.LoadQuery("DeleteManifestDetailByManifest"), new object[] { manifestNo, processID });

                                            // update daily order manifest
                                            defaultLogSession.WriteLine(new LoggingMessage()
                                            {
                                                Message = "Do process update daily order manifest table. Manifest no : " + manifestNo,
                                                Severity = LoggingSeverity.Info
                                            });

                                            db.Execute(DatabaseManager.LoadQuery("UpdateDailyOrderManifest"), new object[] { manifestNo, processID, sessionID, username, process, "" });

                                            // update daily order manifest detail with error result (if exists)
                                            defaultLogSession.WriteLine(new LoggingMessage()
                                            {
                                                Message = "Do process update daily order manifest detail table. Manifest no : " + manifestNo,
                                                Severity = LoggingSeverity.Info
                                            });

                                            db.Execute(DatabaseManager.LoadQuery("UpdateDailyOrderManifestDetail"), new object[] { processID, username, DateTime.Now });
                                        }
                                    }
                                    else
                                    {
                                        defaultLogSession.WriteLine(new LoggingMessage()
                                        {
                                            Message = "Fail do process inquiry state in ICS web service.",
                                            Severity = LoggingSeverity.Warning
                                        });
                                        //SetProgress(100);

                                        transferError = true;
                                    }
                                }

                                // clear result posting log after process
                                defaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Do process delete log posting result table by process id.",
                                    Severity = LoggingSeverity.Info
                                });

                                db.Execute(DatabaseManager.LoadQuery("DeleteLogResultPostingByProcessId"), new object[] { processID });

                                defaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Finish in inquiry state in ICS web service.",
                                    Severity = LoggingSeverity.Info
                                });
                                //SetProgress(0);
                            }
                            else
                            {
                                defaultLogSession.WriteLine(new LoggingMessage(string.Format("Process result: Status -> {0}, Value: {1}", result.Status, result.MappedValues["__error_message__"])));

                                defaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Fail do process state in ICS web service.",
                                    Severity = LoggingSeverity.Warning
                                });

                                //SetProgress(100);
                            }

                            defaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Finish in taking data from ICS web service.",
                                Severity = LoggingSeverity.Info
                            });

                            db.Close();

                            #endregion

                            #region Phase 6 : Finish State ICS Web Servcie

                            defaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Start in finish state ICS web service.",
                                Severity = LoggingSeverity.Info
                            });

                            sp.Clear();
                            sp.Add("state", "finish");
                            sp.Add("sessionId", sessionID);
                            result = ServiceResult.Create(gateway.execute("creation", "goodsReceive", sp.ToString()));
                            sp.Clear();
                            if (result.Status == ServiceResult.STATUS_ERROR)
                            {
                                defaultLogSession.WriteLine(new LoggingMessage()
                                {
                                    Message = "Fail in finish state ICS web service.",
                                    Severity = LoggingSeverity.Warning
                                });
                            }
                            defaultLogSession.WriteLine(new LoggingMessage()
                            {
                                Message = "Finish in finish state ICS web service.",
                                Severity = LoggingSeverity.Info
                            });
                            //SetProgress(0);

                            #endregion
                        }
                    }
                    else
                    {
                        defaultLogSession.WriteLine(new LoggingMessage()
                        {
                            Message = "Fail do process init state ICS web service.",
                            Severity = LoggingSeverity.Warning
                        });

                        //SetProgress(100);
                    }

                    gateway.Close();
                }

                //db.Close();
            }
            catch (Exception exc)
            {
                isError = true;

                defaultLogSession.WriteLine(new LoggingMessage()
                {
                    Message = "Error in running thread . Message : " + exc.Message,
                    Severity = LoggingSeverity.Error
                });

                errorException = exc;
                //throw exc;
            }
            finally
            {
                isAlive = false;
            }
        }
    }
}
