﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Batch
{
    public class BatchQueue
    {
        public long QueueId { set; get; }
        public string BatchId { set; get; }
        public DateTime SubmissionDate { set; get; }
        public string Submitter { set; get; }
        public string Parameters { set; get; }
        public byte? Progress { set; get; }
        public byte? Status { set; get; }
    }
}
