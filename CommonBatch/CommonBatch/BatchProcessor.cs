﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

namespace Toyota.Common.Batch
{
    public abstract class BatchProcessor: MarshalByRefObject
    {
        private List<BatchProcessorListener> listeners;
        //public const string CONNECTION_STRING_PCS = "Data Source=ARKNABLE\\SQLEXPRESS;Initial Catalog=PCS_DB;User ID=sa;Password=adminadmin;Integrated Security=false;";
        public const string CONNECTION_STRING_PCS = "Server=10.16.20.230;Database=PCS_DB;User ID=K2;Password=K2pass!;Trusted_Connection=false;";
        private SqlConnection connection;
        private SqlCommand progressUpdateCommand;

        public BatchProcessor()
        {
            Suspended = false;
            Processing = false;
            Resumable = false;
            SwitchingPeriod = 100;
            SuspendPeriod = 3000;
            Iterated = false;
            listeners = new List<BatchProcessorListener>();
        }

        public void AddListener(BatchProcessorListener listener)
        {
            if (!listeners.Contains(listener))
            {
                listeners.Add(listener);
            }
        }

        public void RemoveListener(BatchProcessorListener listener)
        {
            listeners.Remove(listener);
        }

        public void Start(string[] args) {
            Thread thread = new Thread(new ParameterizedThreadStart(__Start));
            thread.Start(args);
        }

        private SqlCommand CreateUpdateCommand(SqlConnection connection)
        {
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "update tb_r_batch_queue set progress = @progressPosition, status = @status, changed_by = @changedBy, changed_dt = @changedDate where queue_id = @queueId";
            command.Parameters.Add(new SqlParameter("@queueId", SqlDbType.Int) { Value = QueueId });
            command.Parameters.Add(new SqlParameter("@progressPosition", SqlDbType.TinyInt) { Value = Progress });
            command.Parameters.Add(new SqlParameter("@changedBy", SqlDbType.VarChar) { Value = GetType().Name });
            command.Parameters.Add(new SqlParameter("@changedDate", SqlDbType.DateTime) { Value = DateTime.Now });
            command.Parameters.Add(new SqlParameter("@status", SqlDbType.TinyInt) { Value = Status });

            return command;
        }

        private void __Start(object args)
        {
            Status = ProcessStatus.PROCESS_STATUS_PROCESSING;
            Processing = true;
            connection = new SqlConnection(CONNECTION_STRING_PCS);
            connection.Open();
            progressUpdateCommand = CreateUpdateCommand(connection);
            while (Processing)
            {
                Status = ProcessStatus.PROCESS_STATUS_PROCESSING;
                try
                {
                    __Processing((string[])args);
                }
                catch (Exception ex)
                {
                    Status = __OnExceptionOccured(ex);
                    if ((Status == ProcessStatus.PROCESS_STATUS_ERROR) || (Status == ProcessStatus.PROCESS_STATUS_ABORTED))
                    {
                        Stop();
                    }
                }
                UpdateProcessState();
                __SuspendCheckPoint();
                Processing = __IsProcessContinued() && Iterated && (Status == ProcessStatus.PROCESS_STATUS_PROCESSING);
                if (Processing)
                {
                    Thread.Sleep(SwitchingPeriod);
                }                
            }
            __OnProcessEnd();

            if (Status == ProcessStatus.PROCESS_STATUS_PROCESSING)
            {
                Status = ProcessStatus.PROCESS_STATUS_SUCCESS;                
            }
            UpdateProcessState();
            
            progressUpdateCommand.Dispose();
            connection.Close();

            foreach (BatchProcessorListener listener in listeners)
            {
                listener.BatchProcessingFinished(Status, this);
            }
        }
        private void UpdateProcessState()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            progressUpdateCommand.Parameters["@progressPosition"].Value = Progress;
            progressUpdateCommand.Parameters["@status"].Value = Status;
            progressUpdateCommand.ExecuteNonQuery();
        }

        public void Stop() {
            Processing = false;
            Status = ProcessStatus.PROCESS_STATUS_ABORTED;
        }
        public void Suspend() {
            Suspended = true;            
        }
        public void Resume() {
            Suspended = false;
        }

        protected abstract void __Processing(string[] args);
        protected abstract bool __IsProcessContinued();
        protected abstract void __OnProcessEnd();
        protected abstract byte __OnExceptionOccured(Exception exception);        
        protected void __SuspendCheckPoint()
        {
            if (!Processing)
            {
                if (!Iterated)
                {
                    UpdateProcessState();
                    __OnProcessEnd();
                    Environment.Exit(1);
                }
            }
            else
            {
                while (Suspended)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        Status = ProcessStatus.PROCESS_STATUS_SUSPENDED;
                        UpdateProcessState();
                        connection.Close();
                    }

                    Console.WriteLine(string.Format("suspending process #{0} ...", QueueId));
                    Thread.Sleep(SuspendPeriod);
                    if (!Suspended)
                    {
                        if (connection.State == ConnectionState.Closed)
                        {
                            connection.Open();
                        }
                        Status = ProcessStatus.PROCESS_STATUS_PROCESSING;
                    }
                }
                Status = ProcessStatus.PROCESS_STATUS_PROCESSING;
                UpdateProcessState();
            }         
        }

        public bool Resumable { set; get; }
        public long QueueId { set; get; }

        protected bool Suspended { private set; get; }
        protected bool Processing { private set; get; }
        protected int SwitchingPeriod { set; get; }
        protected int SuspendPeriod { set; get; }
        protected bool Iterated { set; get; }
        protected byte Status { set; get; }

        private byte progress = 0;
        protected byte Progress { 
            set {
                progress = value;
                UpdateProcessState();
            }

            get { return progress; }
        }
    }
}
