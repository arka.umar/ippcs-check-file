﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Batch
{
    public interface BatchProcessorListener
    {
        void BatchProcessingFinished(byte status, BatchProcessor batch);
    }
}
