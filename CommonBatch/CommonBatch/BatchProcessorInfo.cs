﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Common.Batch
{
    [Serializable]
    public class BatchProcessorInfo
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public string ClassName { set; get; }
        public string Description { set; get; }
        public byte PriorityLevel { set; get; }
        public string PriorityName { set; get; }
    }
}
