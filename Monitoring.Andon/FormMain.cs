﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Toyota.Monitoring.Andon;
using Toyota.Sockets;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using System.Xml;
using Toyota.Monitoring.Andon.Data;

namespace Toyota.Monitoring.Andon
{
    public partial class FormMain : Form
    {

        private TcpClient client;
        private Thread threadRead;
        public int Port;
        private DCLReceivingDb data;
        private System.Diagnostics.Stopwatch watchRemainingTime;

        private delegate void DelegateUpdatePanel(byte[] e);

        public FormMain()
        {
            InitializeComponent();
            string ip = System.Configuration.ConfigurationManager.AppSettings["IPServer"];
            string port = System.Configuration.ConfigurationManager.AppSettings["MonitoringPort"];
            if (string.IsNullOrEmpty(ip))
                throw new NoNullAllowedException("IPServer cannot be null or configuration must contain \'IPServer\' key.");
            if (string.IsNullOrEmpty(port))
                throw new NoNullAllowedException("Port cannot be null or configuration must contain \'Port\' key.");

            client = new TcpClient();
            client.Connect(ip, Convert.ToInt32(port));
            Port = ((IPEndPoint)client.Client.LocalEndPoint).Port;
            threadRead = new Thread(new ParameterizedThreadStart(ListeningServer));
            threadRead.Start(client);
        }

        public T Deserialize<T>(string e)
        {
            e = e.Replace("\0", "");
            XmlSerializer des = new XmlSerializer(typeof(T));
            T ret;
            using (StringReader s = new StringReader(e))
            {
                XmlReader reader = XmlReader.Create(s);
                ret = (T)des.Deserialize(reader);
            }
            return ret;
        }

        public string Serialize<T>(object data)
        {
            XmlSerializer serial = new XmlSerializer(typeof(T));
            StringWriter streamW = new StringWriter();
            XmlWriter writer = XmlWriter.Create(streamW);
            serial.Serialize(writer, data);
            return serial.ToString();
        }

        private void ListeningServer(object target)
        {
            TcpClient client = (TcpClient)target;
            Socket socket = client.Client;
            while (true)
            {
                if (!client.Connected)
                    break;
                byte[] readServer = new byte[socket.ReceiveBufferSize];
                try
                {
                    int receive = socket.Receive(readServer);
                    byte[] b = new byte[receive];
                    Buffer.BlockCopy(readServer, 0, b, 0, receive);
                    if (ASCIIEncoding.ASCII.GetString(b) == "client.close")
                        break;
                    this.UpdatePanel(b);
                }
                catch (Exception ioex)
                { return; }
            }
        }

        private void UpdatePanel(byte[] e)
        {
            if (this.InvokeRequired)
            {
                DelegateUpdatePanel delg = new DelegateUpdatePanel(UpdatePanel);
                this.Invoke(delg, new object[] { e });
            }
            else
            {
                // refresh panel here
                boxRemainingTime.BackColor = Color.Lime;
                data = Deserialize<DCLReceivingDb>(Encoding.GetEncoding("ibm850").GetString(e));
                if (data.ARRIVAL_STATUS == 0 && data.DEPARTURE_STATUS == 0)
                {
                    watchRemainingTime = new System.Diagnostics.Stopwatch();
                    watchRemainingTime.Start();
                    timerRemainingTime.Start();
                }
                else
                {
                    if (timerRemainingTime != null) timerRemainingTime.Stop();
                    if (watchRemainingTime != null) watchRemainingTime.Stop();
                    labelRemainingTime.Text = "00:00";
                }
                labelTS.Text = data.STATION;
                labelDock.Text = data.DOCK_CD;
                labelRemainingTime.Text = data.REMAINING;
                labelTime.Text = data.CurrentTime.ToShortTimeString();
                labelRoute.Text = data.ROUTE;
                labelRate.Text = data.RATE;
                labelArea.Text = data.REVISE_NO.ToString();
                labelArrivalPlan.Text = data.ARRIVAL_PLAN_DT.ToShortTimeString();
                labelArrivalActual.Text = data.ARRIVAL_ACTUAL_DT.ToShortTimeString();
                labelArrivalGap.Text = new DateTime(data.ARRIVAL_GAP.Ticks).ToShortTimeString();
                labelDeparturePlan.Text = data.DEPARTURE_PLAN_DT.ToShortTimeString();
                labelDepartureActual.Text = data.DEPARTURE_ACTUAL_DT.ToShortTimeString();
                labelDepartureGap.Text = new DateTime(data.DEPARTURE_GAP.Ticks).ToShortTimeString();
                pictureBoxArrival.ImageLocation  = AppDomain.CurrentDomain.BaseDirectory + data.ARRIVAL_STATUS_IMAGE.Replace("~", "").Replace("/",@"\").Substring(1);
                pictureBoxDeparture.ImageLocation = AppDomain.CurrentDomain.BaseDirectory + data.DEPARTURE_STATUS_IMAGE.Replace("~", "").Replace("/", @"\").Substring(1);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            client.Close();
            base.OnClosed(e);
        }

        private void timerRemainingTime_Tick(object sender, EventArgs e)
        {
            labelRemainingTime.Text = new DateTime(watchRemainingTime.Elapsed.Ticks).ToString("mm:ss"); 
            if (data != null && watchRemainingTime != null)
            {
                if (watchRemainingTime.Elapsed.Ticks > (data.DEPARTURE_ACTUAL_DT.Ticks - data.ARRIVAL_ACTUAL_DT.Ticks))
                { boxRemainingTime.BackColor = Color.Red; }
            }
        }

    }
}
