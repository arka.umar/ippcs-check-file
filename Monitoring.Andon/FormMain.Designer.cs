﻿using Toyota.UI.Forms;
namespace Toyota.Monitoring.Andon
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTS = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelDock = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelRoute = new System.Windows.Forms.Label();
            this.labelArea = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelArrivalPlan = new System.Windows.Forms.Label();
            this.labelArrivalActual = new System.Windows.Forms.Label();
            this.labelArrivalGap = new System.Windows.Forms.Label();
            this.labelDepartureGap = new System.Windows.Forms.Label();
            this.labelDepartureActual = new System.Windows.Forms.Label();
            this.labelDeparturePlan = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelRemainingTime = new System.Windows.Forms.Label();
            this.labelRate = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.timerRemainingTime = new System.Windows.Forms.Timer(this.components);
            this.box15 = new Toyota.UI.Forms.Box();
            this.box16 = new Toyota.UI.Forms.Box();
            this.box17 = new Toyota.UI.Forms.Box();
            this.box14 = new Toyota.UI.Forms.Box();
            this.box13 = new Toyota.UI.Forms.Box();
            this.box12 = new Toyota.UI.Forms.Box();
            this.box11 = new Toyota.UI.Forms.Box();
            this.box10 = new Toyota.UI.Forms.Box();
            this.box9 = new Toyota.UI.Forms.Box();
            this.box8 = new Toyota.UI.Forms.Box();
            this.box7 = new Toyota.UI.Forms.Box();
            this.boxRemainingTime = new Toyota.UI.Forms.Box();
            this.box5 = new Toyota.UI.Forms.Box();
            this.box4 = new Toyota.UI.Forms.Box();
            this.box3 = new Toyota.UI.Forms.Box();
            this.box1 = new Toyota.UI.Forms.Box();
            this.box2 = new Toyota.UI.Forms.Box();
            this.pictureBoxArrival = new System.Windows.Forms.PictureBox();
            this.pictureBoxDeparture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArrival)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDeparture)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label1.Location = new System.Drawing.Point(97, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 62);
            this.label1.TabIndex = 2;
            this.label1.Text = "TRUCK ARRIVAL\r\nINFORMATION\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label2.Location = new System.Drawing.Point(454, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 31);
            this.label2.TabIndex = 4;
            this.label2.Text = "REMAINING TIME";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label3.Location = new System.Drawing.Point(912, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 31);
            this.label3.TabIndex = 7;
            this.label3.Text = "TIME";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label4.Location = new System.Drawing.Point(41, 121);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 72);
            this.label4.TabIndex = 14;
            this.label4.Text = "TS";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTS
            // 
            this.labelTS.AutoSize = true;
            this.labelTS.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelTS.Location = new System.Drawing.Point(41, 193);
            this.labelTS.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelTS.Name = "labelTS";
            this.labelTS.Size = new System.Drawing.Size(118, 75);
            this.labelTS.TabIndex = 15;
            this.labelTS.Text = "XX";
            this.labelTS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 32F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label5.Location = new System.Drawing.Point(195, 134);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 49);
            this.label5.TabIndex = 16;
            this.label5.Text = "Dock :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDock
            // 
            this.labelDock.AutoSize = true;
            this.labelDock.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelDock.Location = new System.Drawing.Point(208, 183);
            this.labelDock.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelDock.Name = "labelDock";
            this.labelDock.Size = new System.Drawing.Size(118, 75);
            this.labelDock.TabIndex = 17;
            this.labelDock.Text = "XX";
            this.labelDock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTime
            // 
            this.labelTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Arial", 72F, System.Drawing.FontStyle.Bold);
            this.labelTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelTime.Location = new System.Drawing.Point(840, 138);
            this.labelTime.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(334, 111);
            this.labelTime.TabIndex = 18;
            this.labelTime.Text = "XX:XX";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label6.Location = new System.Drawing.Point(862, 286);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 44);
            this.label6.TabIndex = 19;
            this.label6.Text = "RATE";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label7.Location = new System.Drawing.Point(295, 286);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 44);
            this.label7.TabIndex = 20;
            this.label7.Text = "ROUTE";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRoute
            // 
            this.labelRoute.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelRoute.AutoSize = true;
            this.labelRoute.Font = new System.Drawing.Font("Arial", 132F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRoute.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelRoute.Location = new System.Drawing.Point(132, 327);
            this.labelRoute.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelRoute.Name = "labelRoute";
            this.labelRoute.Size = new System.Drawing.Size(553, 201);
            this.labelRoute.TabIndex = 21;
            this.labelRoute.Text = "SSXX";
            this.labelRoute.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelArea
            // 
            this.labelArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelArea.AutoSize = true;
            this.labelArea.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArea.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelArea.Location = new System.Drawing.Point(86, 624);
            this.labelArea.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelArea.Name = "labelArea";
            this.labelArea.Size = new System.Drawing.Size(118, 75);
            this.labelArea.TabIndex = 26;
            this.labelArea.Text = "XX";
            this.labelArea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 32F);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label10.Location = new System.Drawing.Point(45, 575);
            this.label10.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 49);
            this.label10.TabIndex = 25;
            this.label10.Text = "Version";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label11.Location = new System.Drawing.Point(249, 543);
            this.label11.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 33);
            this.label11.TabIndex = 27;
            this.label11.Text = "Arrival";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label12.Location = new System.Drawing.Point(747, 543);
            this.label12.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 33);
            this.label12.TabIndex = 28;
            this.label12.Text = "Departure";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label13.Location = new System.Drawing.Point(266, 586);
            this.label13.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 34);
            this.label13.TabIndex = 29;
            this.label13.Text = "Plan";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label14.Location = new System.Drawing.Point(266, 631);
            this.label14.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 34);
            this.label14.TabIndex = 30;
            this.label14.Text = "Actual";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label15.Location = new System.Drawing.Point(266, 681);
            this.label15.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 34);
            this.label15.TabIndex = 31;
            this.label15.Text = "Gap";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelArrivalPlan
            // 
            this.labelArrivalPlan.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelArrivalPlan.AutoSize = true;
            this.labelArrivalPlan.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArrivalPlan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelArrivalPlan.Location = new System.Drawing.Point(446, 581);
            this.labelArrivalPlan.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelArrivalPlan.Name = "labelArrivalPlan";
            this.labelArrivalPlan.Size = new System.Drawing.Size(101, 34);
            this.labelArrivalPlan.TabIndex = 35;
            this.labelArrivalPlan.Text = "XX:XX";
            this.labelArrivalPlan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelArrivalActual
            // 
            this.labelArrivalActual.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelArrivalActual.AutoSize = true;
            this.labelArrivalActual.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArrivalActual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelArrivalActual.Location = new System.Drawing.Point(446, 631);
            this.labelArrivalActual.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelArrivalActual.Name = "labelArrivalActual";
            this.labelArrivalActual.Size = new System.Drawing.Size(101, 34);
            this.labelArrivalActual.TabIndex = 37;
            this.labelArrivalActual.Text = "XX:XX";
            this.labelArrivalActual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelArrivalGap
            // 
            this.labelArrivalGap.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelArrivalGap.AutoSize = true;
            this.labelArrivalGap.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArrivalGap.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelArrivalGap.Location = new System.Drawing.Point(446, 681);
            this.labelArrivalGap.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelArrivalGap.Name = "labelArrivalGap";
            this.labelArrivalGap.Size = new System.Drawing.Size(101, 34);
            this.labelArrivalGap.TabIndex = 39;
            this.labelArrivalGap.Text = "XX:XX";
            this.labelArrivalGap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDepartureGap
            // 
            this.labelDepartureGap.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelDepartureGap.AutoSize = true;
            this.labelDepartureGap.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDepartureGap.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelDepartureGap.Location = new System.Drawing.Point(954, 681);
            this.labelDepartureGap.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelDepartureGap.Name = "labelDepartureGap";
            this.labelDepartureGap.Size = new System.Drawing.Size(101, 34);
            this.labelDepartureGap.TabIndex = 48;
            this.labelDepartureGap.Text = "XX:XX";
            this.labelDepartureGap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDepartureActual
            // 
            this.labelDepartureActual.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelDepartureActual.AutoSize = true;
            this.labelDepartureActual.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDepartureActual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelDepartureActual.Location = new System.Drawing.Point(954, 631);
            this.labelDepartureActual.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelDepartureActual.Name = "labelDepartureActual";
            this.labelDepartureActual.Size = new System.Drawing.Size(101, 34);
            this.labelDepartureActual.TabIndex = 46;
            this.labelDepartureActual.Text = "XX:XX";
            this.labelDepartureActual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDeparturePlan
            // 
            this.labelDeparturePlan.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelDeparturePlan.AutoSize = true;
            this.labelDeparturePlan.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeparturePlan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelDeparturePlan.Location = new System.Drawing.Point(954, 581);
            this.labelDeparturePlan.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelDeparturePlan.Name = "labelDeparturePlan";
            this.labelDeparturePlan.Size = new System.Drawing.Size(101, 34);
            this.labelDeparturePlan.TabIndex = 44;
            this.labelDeparturePlan.Text = "XX:XX";
            this.labelDeparturePlan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label22.Location = new System.Drawing.Point(769, 681);
            this.label22.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 34);
            this.label22.TabIndex = 42;
            this.label22.Text = "Gap";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label23.Location = new System.Drawing.Point(769, 631);
            this.label23.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(102, 34);
            this.label23.TabIndex = 41;
            this.label23.Text = "Actual";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label24.Location = new System.Drawing.Point(769, 586);
            this.label24.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 34);
            this.label24.TabIndex = 40;
            this.label24.Text = "Plan";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Lime;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label25.Location = new System.Drawing.Point(606, 195);
            this.label25.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(89, 29);
            this.label25.TabIndex = 49;
            this.label25.Text = "minutes";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRemainingTime
            // 
            this.labelRemainingTime.AutoSize = true;
            this.labelRemainingTime.BackColor = System.Drawing.Color.Lime;
            this.labelRemainingTime.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRemainingTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelRemainingTime.Location = new System.Drawing.Point(399, 158);
            this.labelRemainingTime.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelRemainingTime.Name = "labelRemainingTime";
            this.labelRemainingTime.Size = new System.Drawing.Size(225, 75);
            this.labelRemainingTime.TabIndex = 50;
            this.labelRemainingTime.Text = "XX:XX";
            this.labelRemainingTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRate
            // 
            this.labelRate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelRate.AutoSize = true;
            this.labelRate.Font = new System.Drawing.Font("Arial", 132F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.labelRate.Location = new System.Drawing.Point(776, 327);
            this.labelRate.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelRate.Name = "labelRate";
            this.labelRate.Size = new System.Drawing.Size(319, 201);
            this.labelRate.TabIndex = 51;
            this.labelRate.Text = "XX";
            this.labelRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 132F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.label8.Location = new System.Drawing.Point(658, 307);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 201);
            this.label8.TabIndex = 52;
            this.label8.Text = "-";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timerRemainingTime
            // 
            this.timerRemainingTime.Interval = 1000;
            this.timerRemainingTime.Tick += new System.EventHandler(this.timerRemainingTime_Tick);
            // 
            // box15
            // 
            this.box15.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.box15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box15.Location = new System.Drawing.Point(926, 676);
            this.box15.Name = "box15";
            this.box15.Size = new System.Drawing.Size(156, 44);
            this.box15.TabIndex = 47;
            // 
            // box16
            // 
            this.box16.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.box16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box16.Location = new System.Drawing.Point(926, 626);
            this.box16.Name = "box16";
            this.box16.Size = new System.Drawing.Size(156, 44);
            this.box16.TabIndex = 45;
            // 
            // box17
            // 
            this.box17.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.box17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box17.Location = new System.Drawing.Point(926, 576);
            this.box17.Name = "box17";
            this.box17.Size = new System.Drawing.Size(156, 44);
            this.box17.TabIndex = 43;
            // 
            // box14
            // 
            this.box14.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.box14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box14.Location = new System.Drawing.Point(418, 676);
            this.box14.Name = "box14";
            this.box14.Size = new System.Drawing.Size(156, 44);
            this.box14.TabIndex = 38;
            // 
            // box13
            // 
            this.box13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.box13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box13.Location = new System.Drawing.Point(418, 626);
            this.box13.Name = "box13";
            this.box13.Size = new System.Drawing.Size(156, 44);
            this.box13.TabIndex = 36;
            // 
            // box12
            // 
            this.box12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.box12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box12.Location = new System.Drawing.Point(418, 576);
            this.box12.Name = "box12";
            this.box12.Size = new System.Drawing.Size(156, 44);
            this.box12.TabIndex = 32;
            // 
            // box11
            // 
            this.box11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.box11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box11.Location = new System.Drawing.Point(12, 538);
            this.box11.Name = "box11";
            this.box11.Size = new System.Drawing.Size(234, 191);
            this.box11.TabIndex = 24;
            // 
            // box10
            // 
            this.box10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.box10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box10.Location = new System.Drawing.Point(244, 538);
            this.box10.Name = "box10";
            this.box10.Size = new System.Drawing.Size(504, 191);
            this.box10.TabIndex = 23;
            // 
            // box9
            // 
            this.box9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.box9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box9.Location = new System.Drawing.Point(745, 538);
            this.box9.Name = "box9";
            this.box9.Size = new System.Drawing.Size(482, 191);
            this.box9.TabIndex = 22;
            // 
            // box8
            // 
            this.box8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.box8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box8.Location = new System.Drawing.Point(12, 270);
            this.box8.Name = "box8";
            this.box8.Size = new System.Drawing.Size(1215, 270);
            this.box8.TabIndex = 13;
            // 
            // box7
            // 
            this.box7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.box7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box7.Location = new System.Drawing.Point(716, 112);
            this.box7.Name = "box7";
            this.box7.Size = new System.Drawing.Size(511, 160);
            this.box7.TabIndex = 12;
            // 
            // boxRemainingTime
            // 
            this.boxRemainingTime.BackColor = System.Drawing.Color.Lime;
            this.boxRemainingTime.Location = new System.Drawing.Point(352, 112);
            this.boxRemainingTime.Name = "boxRemainingTime";
            this.boxRemainingTime.Size = new System.Drawing.Size(366, 160);
            this.boxRemainingTime.TabIndex = 11;
            // 
            // box5
            // 
            this.box5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box5.Location = new System.Drawing.Point(182, 112);
            this.box5.Name = "box5";
            this.box5.Size = new System.Drawing.Size(172, 160);
            this.box5.TabIndex = 10;
            // 
            // box4
            // 
            this.box4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box4.Location = new System.Drawing.Point(12, 112);
            this.box4.Name = "box4";
            this.box4.Size = new System.Drawing.Size(172, 160);
            this.box4.TabIndex = 9;
            // 
            // box3
            // 
            this.box3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.box3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box3.Location = new System.Drawing.Point(716, 16);
            this.box3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.box3.Name = "box3";
            this.box3.Size = new System.Drawing.Size(511, 98);
            this.box3.TabIndex = 8;
            // 
            // box1
            // 
            this.box1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box1.Location = new System.Drawing.Point(12, 16);
            this.box1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.box1.Name = "box1";
            this.box1.Size = new System.Drawing.Size(342, 98);
            this.box1.TabIndex = 5;
            // 
            // box2
            // 
            this.box2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.box2.Location = new System.Drawing.Point(351, 16);
            this.box2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.box2.Name = "box2";
            this.box2.Size = new System.Drawing.Size(367, 98);
            this.box2.TabIndex = 6;
            // 
            // pictureBoxArrival
            // 
            this.pictureBoxArrival.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBoxArrival.Location = new System.Drawing.Point(580, 581);
            this.pictureBoxArrival.Name = "pictureBoxArrival";
            this.pictureBoxArrival.Size = new System.Drawing.Size(128, 128);
            this.pictureBoxArrival.TabIndex = 53;
            this.pictureBoxArrival.TabStop = false;
            // 
            // pictureBoxDeparture
            // 
            this.pictureBoxDeparture.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBoxDeparture.Location = new System.Drawing.Point(1088, 581);
            this.pictureBoxDeparture.Name = "pictureBoxDeparture";
            this.pictureBoxDeparture.Size = new System.Drawing.Size(128, 128);
            this.pictureBoxDeparture.TabIndex = 54;
            this.pictureBoxDeparture.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.ClientSize = new System.Drawing.Size(1244, 741);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBoxDeparture);
            this.Controls.Add(this.pictureBoxArrival);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelRate);
            this.Controls.Add(this.labelRemainingTime);
            this.Controls.Add(this.labelDepartureGap);
            this.Controls.Add(this.box15);
            this.Controls.Add(this.labelDepartureActual);
            this.Controls.Add(this.box16);
            this.Controls.Add(this.labelDeparturePlan);
            this.Controls.Add(this.box17);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.labelArrivalGap);
            this.Controls.Add(this.box14);
            this.Controls.Add(this.labelArrivalActual);
            this.Controls.Add(this.box13);
            this.Controls.Add(this.labelArrivalPlan);
            this.Controls.Add(this.box12);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.labelArea);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.box11);
            this.Controls.Add(this.box10);
            this.Controls.Add(this.box9);
            this.Controls.Add(this.labelRoute);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelDock);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelTS);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.box8);
            this.Controls.Add(this.box7);
            this.Controls.Add(this.boxRemainingTime);
            this.Controls.Add(this.box5);
            this.Controls.Add(this.box4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.box3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.box1);
            this.Controls.Add(this.box2);
            this.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Andon Monitoring";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArrival)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDeparture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Box box1;
        private Box box2;
        private System.Windows.Forms.Label label3;
        private Box box3;
        private Box box4;
        private Box box5;
        private Box boxRemainingTime;
        private Box box7;
        private Box box8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelTS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelDock;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelRoute;
        private Box box9;
        private Box box10;
        private Box box11;
        private System.Windows.Forms.Label labelArea;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private Box box12;
        private System.Windows.Forms.Label labelArrivalPlan;
        private System.Windows.Forms.Label labelArrivalActual;
        private Box box13;
        private System.Windows.Forms.Label labelArrivalGap;
        private Box box14;
        private System.Windows.Forms.Label labelDepartureGap;
        private Box box15;
        private System.Windows.Forms.Label labelDepartureActual;
        private Box box16;
        private System.Windows.Forms.Label labelDeparturePlan;
        private Box box17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelRemainingTime;
        private System.Windows.Forms.Label labelRate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timerRemainingTime;
        private System.Windows.Forms.PictureBox pictureBoxArrival;
        private System.Windows.Forms.PictureBox pictureBoxDeparture;
    }
}

