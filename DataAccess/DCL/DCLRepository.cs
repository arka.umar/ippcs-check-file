﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.DataAccess
{
    public class DCLRepository : BaseRepository
    {
        public int UpdateDCL(List<string> args)
        {
            int ret = 0;
            ret = Execute("UpdateDCLOffline", args.ToArray()); 
            return ret;
        }

        public Nullable<DateTime> GetDCL(List<string> args)
        {
            object _result;
            Nullable<DateTime> DepartureDT =null;

            _result = ExecuteScalar<object>("SelectDCLData", args.ToArray());
            if (_result != DBNull.Value) DepartureDT = Convert.ToDateTime(_result);
            else DepartureDT = null;

            return DepartureDT;
        }

        public void SendEmailNotification(List<string> args)
        {
            Execute("SendEmailNotification", args.ToArray());
        }
    }
}
