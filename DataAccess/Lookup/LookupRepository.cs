﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.DataAccess;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.DataAccess.Lookup;

namespace Toyota.DataAccess.Lookup
{
    public class LookupRepository : BaseRepository
    {

        public List<Supplier> GetSupplier(string Code, string Name)
        {
            List<Supplier> ret = null;
            Query<Supplier>("GetSupplier",
                                                new object[] { Code, Name },
                                                act =>
                                                {
                                                    ret = new List<Supplier>();
                                                    act.ForEach(item =>
                                                    {
                                                        ret.Add(new Supplier()
                                                        {
                                                            SupplierCD = item.SupplierCD,
                                                            SupplierPlantCD = item.SupplierPlantCD,
                                                            SupplierName = item.SupplierName,
                                                            SupplierAbbreviation = item.SupplierAbbreviation,
                                                            CreatedDate = item.CreatedDate,
                                                            CreatedBy = item.CreatedBy,
                                                            ChangedDate = item.ChangedDate,
                                                            ChangedBy = item.ChangedBy
                                                        });
                                                    });
                                                });
            return ret;
        }

        public List<Currency> GetCurrency(string Code, string Name)
        {
            List<Currency> ret = null;
            Query<Currency>("GetCurrency",
                                                new object[] { Code, Name },
                                                act =>
                                                {
                                                    ret = new List<Currency>();
                                                    act.ForEach(item =>
                                                    {
                                                        ret.Add(new Currency()
                                                        {
                                                            CurrencyCD = item.CurrencyCD,
                                                            CurrencyName = item.CurrencyName,
                                                            CreatedDate = item.CreatedDate,
                                                            CreatedBy = item.CreatedBy,
                                                            ChangedDate = item.ChangedDate,
                                                            ChangedBy = item.ChangedBy
                                                        });
                                                    });
                                                });
            return ret;

        }

    }
}
