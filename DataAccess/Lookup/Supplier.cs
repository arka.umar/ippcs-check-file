﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.DataAccess.Lookup
{
    public class Supplier
    {
        public string SupplierCD { set; get; }
        public string SupplierPlantCD { set; get; }
        public string SupplierAbbreviation { set; get; }
        public string SupplierName { set; get; }
        public string CreatedBy { set; get; }
        public DateTime  CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime? ChangedDate { set; get; }
    }
}
