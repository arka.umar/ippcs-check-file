﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.DataAccess.Lookup
{
    public class Currency
    {
        public string CurrencyCD { set; get; }
        public string CurrencyName { set; get; }
        public string CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public string ChangedBy { set; get; }
        public DateTime? ChangedDate { set; get; }
    }
}
