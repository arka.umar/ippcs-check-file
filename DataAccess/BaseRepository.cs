﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.Ioc;
using Toyota.Common.Web.Database.Petapoco;

namespace Toyota.DataAccess
{
    public class BaseRepository
    {

        public BaseRepository()
        { 
            IBootstrap bootstrap = Bootstrap.GetInstance();
            bootstrap.RegisterProvider<IDBContextManager>(typeof(PetaPocoContextManager), true);
        }
        /// <summary>
        /// Base methods for quering List
        /// </summary>
        /// <typeparam name="T">the type of Model</typeparam>
        /// <param name="QueryName">Name of SQL</param>
        /// <param name="Code">Parameter</param>
        /// <param name="Name"></param>
        /// <param name="Action"></param>
        public void Query<T>(string QueryName, object[] Params, Action<List<T>> Action) 
        {
            IDBContextManager dbManager = ProviderResolver.GetInstance().Get<IDBContextManager>();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            IEnumerable<T> ret = dbContext.Query<T>(QueryName, Params);
            if (ret != null && ret.Any())
            {
                Action.Invoke(ret.ToList());
            }
            dbContext.Close();
        }

        public int Execute(string QueryName, object[] Params)
        {
            IDBContextManager dbManager = ProviderResolver.GetInstance().Get<IDBContextManager>();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            int ret = dbContext.Execute(QueryName, Params);
            dbContext.Close();
            return ret;
        }

        public T ExecuteScalar<T>(string QueryName, object[] Params)
        {
            IDBContextManager dbManager = ProviderResolver.GetInstance().Get<IDBContextManager>();
            IDBContext dbContext = dbManager.GetContext(DBContextNames.DB_PCS);
            T ret = dbContext.ExecuteScalar<T>(QueryName, Params);
            dbContext.Close();
            return ret;
        }
    }
}
