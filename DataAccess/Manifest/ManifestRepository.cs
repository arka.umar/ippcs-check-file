﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.DataAccess.Manifest
{
    public class ManifestRepository : BaseRepository
    {
        public int UpdateManifest(List<object> args)
        {
            int ret = 0;
            ret = Execute("UpdateManifest", args.ToArray());
            return ret;
        }

        public int UpdateManifestProblem(List<object> args)
        {
            int ret = 0;
            ret = Execute("UpdateManifestProblem", args.ToArray());
            return ret;
        }

        public int UpdateManifestProblemReceived(List<object> args)
        {
            int ret = 0;
            ret = Execute("UpdateManifestDelivered", args.ToArray());
            return ret;
        }

        public int InsertProblemManifest(List<object> args)
        {
            int ret = 0;
            ret = Execute("InsertProblemPart", args.ToArray());
            return ret;
        }

        public Nullable<DateTime> GetManifest(List<string> args)
        {
            object _result;
            Nullable<DateTime> DepartureDT = null;

            _result = ExecuteScalar<object>("SelectManifestData", args.ToArray());
            if (_result != DBNull.Value) DepartureDT = Convert.ToDateTime(_result);
            else DepartureDT = null;

            return DepartureDT;
        }

        public int ManifestProblemFLag(string args)
        {
            int _result;
            int manifestPronlemFlag = 0;

            _result = ExecuteScalar<int>("SelectManifestProblemFLag", new string[] { args });
            if (_result != null) manifestPronlemFlag = _result;
            else manifestPronlemFlag = 0;

            return manifestPronlemFlag;
        }


    }
}
