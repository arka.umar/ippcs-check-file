﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitoring.GetAndon
{
    internal class AndonParameter : IParameter
    {

        private static readonly IParameter instance = new AndonParameter();

        public static IParameter GetInstance()
        { return instance; }

        public IDictionary<string, string> param;

        public AndonParameter()
        { param = new Dictionary<string, string>(); }

        public string Get(string key)
        {
            if (param.ContainsKey(key))
                return param[key];
            else
                return "";
        }

        public void Add(string key, string value)
        { param.Add(key, value); }

        public void Remove(string key)
        { param.Remove(key); }

        public void Clear()
        { param.Clear(); }

        public int GetCount()
        { return param.Count; }
    }
}
