﻿namespace Monitoring.GetAndon
{
    partial class InputParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputParameters));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDock = new UnivercaDotNet.NetUIStyle.TextBoxFade();
            this.textBoxStation = new UnivercaDotNet.NetUIStyle.TextBoxFade();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonSave = new UnivercaDotNet.NetUIStyle.Button();
            this.textBoxIntervalUpdate = new UnivercaDotNet.NetUIStyle.TextBoxFade();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonCancel = new UnivercaDotNet.NetUIStyle.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Station";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Dock";
            // 
            // textBoxDock
            // 
            this.textBoxDock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.textBoxDock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.textBoxDock.BackColor = System.Drawing.Color.Transparent;
            this.textBoxDock.BorderColorActive = System.Drawing.Color.DarkBlue;
            this.textBoxDock.BorderColorInactive = System.Drawing.Color.MediumSeaGreen;
            this.textBoxDock.Location = new System.Drawing.Point(123, 12);
            this.textBoxDock.MaxLength = 32767;
            this.textBoxDock.Name = "textBoxDock";
            this.textBoxDock.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.textBoxDock.ReadOnly = false;
            this.textBoxDock.Size = new System.Drawing.Size(185, 30);
            this.textBoxDock.TabIndex = 1;
            // 
            // textBoxStation
            // 
            this.textBoxStation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.textBoxStation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.textBoxStation.BackColor = System.Drawing.Color.Transparent;
            this.textBoxStation.BorderColorActive = System.Drawing.Color.DarkBlue;
            this.textBoxStation.BorderColorInactive = System.Drawing.Color.MediumSeaGreen;
            this.textBoxStation.Location = new System.Drawing.Point(123, 42);
            this.textBoxStation.MaxLength = 32767;
            this.textBoxStation.Name = "textBoxStation";
            this.textBoxStation.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.textBoxStation.ReadOnly = false;
            this.textBoxStation.Size = new System.Drawing.Size(185, 30);
            this.textBoxStation.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(10, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(378, 1);
            this.label7.TabIndex = 26;
            this.label7.Text = "label7";
            // 
            // buttonSave
            // 
            this.buttonSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSave.BackColor = System.Drawing.Color.Transparent;
            this.buttonSave.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F);
            this.buttonSave.ImageButtonHot = ((System.Drawing.Bitmap)(resources.GetObject("buttonSave.ImageButtonHot")));
            this.buttonSave.ImageButtonInactive = ((System.Drawing.Bitmap)(resources.GetObject("buttonSave.ImageButtonInactive")));
            this.buttonSave.ImageButtonNormal = ((System.Drawing.Bitmap)(resources.GetObject("buttonSave.ImageButtonNormal")));
            this.buttonSave.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonSave.IntervalFadeOut = 180;
            this.buttonSave.Location = new System.Drawing.Point(128, 141);
            this.buttonSave.MouseDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(187)))), ((int)(((byte)(111)))));
            this.buttonSave.MouseEnterBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(50)))));
            this.buttonSave.MouseEnterTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(180)))));
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonSave.Size = new System.Drawing.Size(83, 35);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // textBoxIntervalUpdate
            // 
            this.textBoxIntervalUpdate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.textBoxIntervalUpdate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.textBoxIntervalUpdate.BackColor = System.Drawing.Color.Transparent;
            this.textBoxIntervalUpdate.BorderColorActive = System.Drawing.Color.DarkBlue;
            this.textBoxIntervalUpdate.BorderColorInactive = System.Drawing.Color.MediumSeaGreen;
            this.textBoxIntervalUpdate.Location = new System.Drawing.Point(123, 71);
            this.textBoxIntervalUpdate.MaxLength = 32767;
            this.textBoxIntervalUpdate.Name = "textBoxIntervalUpdate";
            this.textBoxIntervalUpdate.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.textBoxIntervalUpdate.ReadOnly = false;
            this.textBoxIntervalUpdate.Size = new System.Drawing.Size(185, 30);
            this.textBoxIntervalUpdate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Interval Update";
            // 
            // buttonCancel
            // 
            this.buttonCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancel.BackColor = System.Drawing.Color.Transparent;
            this.buttonCancel.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F);
            this.buttonCancel.ImageButtonHot = ((System.Drawing.Bitmap)(resources.GetObject("buttonCancel.ImageButtonHot")));
            this.buttonCancel.ImageButtonInactive = ((System.Drawing.Bitmap)(resources.GetObject("buttonCancel.ImageButtonInactive")));
            this.buttonCancel.ImageButtonNormal = ((System.Drawing.Bitmap)(resources.GetObject("buttonCancel.ImageButtonNormal")));
            this.buttonCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonCancel.IntervalFadeOut = 180;
            this.buttonCancel.Location = new System.Drawing.Point(234, 140);
            this.buttonCancel.MouseDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(187)))), ((int)(((byte)(111)))));
            this.buttonCancel.MouseEnterBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(50)))));
            this.buttonCancel.MouseEnterTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(180)))));
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonCancel.Size = new System.Drawing.Size(67, 35);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // InputParameters
            // 
            this.AcceptButton = this.buttonSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(402, 190);
            this.ControlBox = false;
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textBoxIntervalUpdate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxStation);
            this.Controls.Add(this.textBoxDock);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputParameters";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Parameters";
            this.Load += new System.EventHandler(this.InputParameters_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private UnivercaDotNet.NetUIStyle.TextBoxFade textBoxDock;
        private UnivercaDotNet.NetUIStyle.TextBoxFade textBoxStation;
        private System.Windows.Forms.Label label7;
        private UnivercaDotNet.NetUIStyle.Button buttonSave;
        private UnivercaDotNet.NetUIStyle.TextBoxFade textBoxIntervalUpdate;
        private System.Windows.Forms.Label label3;
        private UnivercaDotNet.NetUIStyle.Button buttonCancel;

    }
}