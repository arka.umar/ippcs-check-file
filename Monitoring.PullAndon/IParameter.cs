﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitoring.GetAndon
{
    internal interface IParameter
    {
        int GetCount();
        string Get(string key);
        void Add(string key, string value);
        void Remove(string key);
        void Clear();
    }
}
