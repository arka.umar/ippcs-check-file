﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitoring.GetAndon
{
    internal class AndonFactory
    {
        public static IAndon CreateAndon()
        {
            return new Andon();
        }
    }
}
