﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitoring.GetAndon
{
    internal class AndonConfiguration
    {
        public string UserID { get; set; }
        public string Dock { get; set; }
        public string Station { get; set; }
        public string IntervalUpdate { get; set; }
    }
}
