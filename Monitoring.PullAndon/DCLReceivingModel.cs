﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitoring.GetAndon
{ 
    public class DCLReceivingModel
    {
        public string DELIVERY_NO { set; get; }
        public string STATION { set; get; }
        public string DOCK_CD { set; get; }
        public string ROUTE { get; set; }
        public string RATE { get; set; }
        public DateTime ARRIVAL_ACTUAL_DT { set; get; }
        public DateTime ARRIVAL_PLAN_DT { set; get; }
        public TimeSpan ARRIVAL_GAP { set; get; }
        public long ARRIVAL_GAP_TICKS
        {
            set { ARRIVAL_GAP = TimeSpan.FromTicks(value); }
            get { return ARRIVAL_GAP.Ticks; }
        }
        public int ARRIVAL_STATUS { set; get; }
        public string ARRIVAL_STATUS_DESC { get; set; }
        public string ARRIVAL_STATUS_IMAGE { get; set; }
        public DateTime DEPARTURE_ACTUAL_DT { set; get; }
        public DateTime DEPARTURE_PLAN_DT { set; get; }
        public TimeSpan DEPARTURE_GAP { set; get; }
        public long DEPARTURE_GAP_TICKS
        {
            set { DEPARTURE_GAP = TimeSpan.FromTicks(value); }
            get { return DEPARTURE_GAP.Ticks; }
        }
        public int DEPARTURE_STATUS { set; get; }
        public string DEPARTURE_STATUS_DESC { get; set; }
        public string DEPARTURE_STATUS_IMAGE { get; set; }
        public DateTime CurrentTime { set; get; }
        public string REMAINING { set; get; }
        public int REVISE_NO { set; get; }
    }
}
