﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitoring.GetAndon
{
    internal interface IAndon
    {
        T GetLatestData<T>();
        IParameter GetParam(string UserID);
        int UpdateParam(IParameter param);
        int InsertParam(IParameter param);
    }
}
