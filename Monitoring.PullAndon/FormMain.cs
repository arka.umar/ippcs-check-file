﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace Monitoring.GetAndon
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private IAndon andon;

        private void FormMain_Load(object sender, EventArgs e)
        {
            this.Text = "Monitoring Andon" + " - " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            andon = AndonFactory.CreateAndon();
            if (andon.GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name) == null)
            {
                InputParameters input = new InputParameters();
                if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    andon.InsertParam(AndonParameter.GetInstance());
                    timerLatestUpdate.Interval = Convert.ToInt32(andon.GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name).Get("IntervalUpdate"));
                }
            }
            else
            {
                timerLatestUpdate.Interval = Convert.ToInt32(andon.GetParam(System.Security.Principal.WindowsIdentity.GetCurrent().Name).Get("IntervalUpdate"));
            }
            DCLReceivingModel data = andon.GetLatestData<DCLReceivingModel>();
            _Refresh(data);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            timerLatestUpdate.Start();
        }

        private void timerLatestUpdate_Tick(object sender, EventArgs e)
        {
            DCLReceivingModel data = andon.GetLatestData<DCLReceivingModel>();
            _Refresh(data);
        }

        private void _Refresh(DCLReceivingModel data)
        {
            if (data != null)
            {
                labelTS.Text = data.STATION;
                labelDock.Text = data.DOCK_CD;
                labelRemainingTime.Text = data.REMAINING;
                labelTime.Text = DateTime.Now.ToString("hh:mm");
                labelRoute.Text = data.ROUTE;
                labelRate.Text = data.RATE;
                labelArea.Text = data.REVISE_NO.ToString();
                labelArrivalPlan.Text = data.ARRIVAL_PLAN_DT.ToShortTimeString();
                labelArrivalActual.Text = data.ARRIVAL_ACTUAL_DT.ToShortTimeString();
                labelArrivalGap.Text = new DateTime(data.ARRIVAL_GAP.Ticks).ToShortTimeString();
                labelDeparturePlan.Text = data.DEPARTURE_PLAN_DT.ToShortTimeString();
                labelDepartureActual.Text = data.DEPARTURE_ACTUAL_DT.ToShortTimeString();
                labelDepartureGap.Text = new DateTime(data.DEPARTURE_GAP.Ticks).ToShortTimeString();
                pictureBoxArrival.ImageLocation = AppDomain.CurrentDomain.BaseDirectory + data.ARRIVAL_STATUS_IMAGE.Replace("~", "").Replace("/", @"\").Substring(1);
                pictureBoxDeparture.ImageLocation = AppDomain.CurrentDomain.BaseDirectory + data.DEPARTURE_STATUS_IMAGE.Replace("~", "").Replace("/", @"\").Substring(1);
                labelRemainingTime.Text = (data.DEPARTURE_PLAN_DT.TimeOfDay - data.ARRIVAL_ACTUAL_DT.TimeOfDay).ToString(@"h\:mm");
            }
        }

        private void buttonConfigure_Click(object sender, EventArgs e)
        {
            InputParameters input = new InputParameters();
            if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                IParameter param = AndonParameter.GetInstance();
                int ret = andon.UpdateParam(param);
                if (ret == 0)
                {
                    andon.InsertParam(param);
                } 
            }
        }

        private void checkBoxFullScreen_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFullScreen.Checked)
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void timerSecond_Tick(object sender, EventArgs e)
        {
            labelSecond.Text = DateTime.Now.ToString("ss");
        }

        private void timerMinute_Tick(object sender, EventArgs e)
        {

        }
          
    }
}
