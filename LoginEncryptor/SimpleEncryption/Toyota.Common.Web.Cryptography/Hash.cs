using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;

namespace Toyota.Common.Web.Cryptography
{
	public class Hash : IHash, IDisposable
	{
		private HashAlgorithm hashAlgorithm;

		public Hash(HashAlgorithm Algorithm)
		{
			if (Algorithm != null)
			{
				hashAlgorithm = Algorithm;
			}
		}

		[DebuggerStepThrough]
		public byte[] ComputeHash(Stream stream)
		{
			return hashAlgorithm.ComputeHash(stream);
		}

		[DebuggerStepThrough]
		public byte[] ComputeHash(string File)
		{
			using (Stream stream = new FileStream(File, FileMode.Open))
			{
				return ComputeHash(stream);
			}
		}

        [DebuggerStepThrough]
        public void Dispose()
        {
            hashAlgorithm.Dispose();
        }
	}
}
