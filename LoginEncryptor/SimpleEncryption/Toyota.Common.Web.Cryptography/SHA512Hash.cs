using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;

namespace Toyota.Common.Web.Cryptography
{
	public class SHA512Hash : IHash, IDisposable
	{
		private Hash hash;

		public SHA512Hash()
		{
			hash = new Hash(new SHA512Managed());
		}

		[DebuggerStepThrough]
		public byte[] ComputeHash(Stream stream)
		{
			return hash.ComputeHash(stream);
		}

		[DebuggerStepThrough]
		public byte[] ComputeHash(string File)
		{
			return hash.ComputeHash(File);
		}

		[DebuggerStepThrough]
		public void Dispose()
		{
			hash.Dispose();
		}
	}
}
