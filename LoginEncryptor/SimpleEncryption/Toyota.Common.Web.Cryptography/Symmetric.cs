using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Toyota.Common.Web.Cryptography
{
	[DebuggerNonUserCode]
	public class Symmetric : IDisposable
	{
		private readonly SymmetricAlgorithm symAlgo;

		private byte[] keyBytes;

		private byte[] encrypted;

		private byte[] target;

		private readonly Encoding enc;

		private DeriveBytes derified;

		public byte[] Key
		{
			[DebuggerStepThrough]
			get
			{
				return keyBytes;
			}
			[DebuggerStepThrough]
			set
			{
				byte[] bytes = enc.GetBytes(new string(value.ToString().ToCharArray().Reverse()
					.ToArray()));
				derified = new Rfc2898DeriveBytes(value, bytes, 4);
				keyBytes = derified.GetBytes(32);
				symAlgo.Key = keyBytes;
				symAlgo.IV = derified.GetBytes(16);
			}
		}

		public byte[] IV
		{
			[DebuggerStepThrough]
			get
			{
				return symAlgo.IV;
			}
			[DebuggerStepThrough]
			set
			{
				symAlgo.IV = value;
			}
		}

		[SecuritySafeCritical]
		public Symmetric()
		{
			enc = Encoding.GetEncoding("ibm850");
			symAlgo = new RijndaelManaged();
		}

		[DebuggerStepThrough]
		public byte[] Encrypt(byte[] Data)
		{
			encrypted = null;
			target = Data;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (CryptoStream cryptoStream = new CryptoStream(memoryStream, symAlgo.CreateEncryptor(), CryptoStreamMode.Write))
				{
					cryptoStream.Write(target, 0, target.Length);
					cryptoStream.FlushFinalBlock();
				}
				encrypted = memoryStream.ToArray();
			}
			return encrypted;
		}

		[DebuggerStepThrough]
		public byte[] Decrypt(byte[] Data)
		{
			target = Data;
			encrypted = new byte[target.Length];
			using (MemoryStream memoryStream = new MemoryStream(target))
			{
				using (CryptoStream cryptoStream = new CryptoStream(memoryStream, symAlgo.CreateDecryptor(), CryptoStreamMode.Read))
				{
					int num = cryptoStream.Read(target, 0, target.Length);
					encrypted = new byte[num];
					memoryStream.Position = 0L;
					memoryStream.Read(encrypted, 0, encrypted.Length);
				}
			}
			return encrypted;
		}

		public void Dispose()
		{
			if (derified != null)
			{
				derified.Dispose();
			}
			symAlgo.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}
