using System;
using System.IO;

namespace Toyota.Common.Web.Cryptography
{
	public interface IHash : IDisposable
	{
		byte[] ComputeHash(Stream stream);

		byte[] ComputeHash(string file);
	}
}
