using System;
using System.Configuration;
using System.Reflection;
using System.Linq;
using System.IO;

namespace Toyota.Common.Web.Cryptography
{
	public class ConfigKeySym : IKeySym
	{
		public byte[] GetKey()
		{
			byte[] pubkeys = null;//
			byte[] aPublicKey = null;

			string configPublicKeys = ConfigurationManager.AppSettings["PublicKey"];

			/* added to extract PublicKey from config and integrated it into executable to make it 'self contained' */

			if (!string.IsNullOrEmpty(configPublicKeys))
			{
				pubkeys = configPublicKeys.Split(
							new char[] { ' ', '\r', '\n', ',' }
							, StringSplitOptions.RemoveEmptyEntries)
							.Select(a => Convert.ToByte(a)).ToArray();
			}
			if (pubkeys == null || pubkeys.Length <= 2)
			{
				pubkeys = ResourceBytes("PublicKey");
			}
			aPublicKey = new byte[pubkeys.Length - 1];
			// File.WriteAllBytes("PublicKey", pubkeys);
			Array.Copy(pubkeys, aPublicKey, pubkeys.Length - 1);
			/* ends of added code */
			return aPublicKey;
		}

		public static string ResourceString(string name)
		{
			var assembly = Assembly.GetExecutingAssembly();
			string resourcePath = name;
			resourcePath = assembly.GetManifestResourceNames().Single(str=>str.EndsWith(name));
			if (string.IsNullOrEmpty(resourcePath))
			{
				throw new Exception("No Resource: " + name);
			}
			using (Stream stream = assembly.GetManifestResourceStream(resourcePath))
			using (StreamReader reader = new StreamReader(stream))
			{
				return reader.ReadToEnd();
			}
		}

		public static byte[] ResourceBytes(string name)
		{
			var assembly = Assembly.GetExecutingAssembly();
			string resourcePath = name;
			resourcePath = assembly.GetManifestResourceNames().Single(str => str.EndsWith(name));
			if (string.IsNullOrEmpty(resourcePath))
			{
				throw new Exception("No Resource: " + name);
			}
			using (Stream stream = assembly.GetManifestResourceStream(resourcePath))
			using (BinaryReader reader = new BinaryReader(stream))
			{
				return reader.ReadBytes((int) stream.Length);
			}
		}
	}
}
