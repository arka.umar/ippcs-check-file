namespace Toyota.Common.Web.Cryptography
{
	public interface IKeySym
	{
		byte[] GetKey();
	}
}
