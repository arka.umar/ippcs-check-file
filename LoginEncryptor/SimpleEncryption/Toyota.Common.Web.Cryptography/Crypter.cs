using System;
using System.Text;

namespace Toyota.Common.Web.Cryptography
{
	public class Crypter
	{
		private IKeySym keySym;

		public Crypter(IKeySym KeySym)
		{
			if (KeySym == null)
			{
				throw new ArgumentNullException("'KeySym' cannot be null.");
			}
			keySym = KeySym;
		}

		public string Decrypt(string data)
		{
			string result = string.Empty;
			using (Symmetric symmetric = new Symmetric())
			{
				symmetric.Key = keySym.GetKey();
				result = Encoding.ASCII.GetString(symmetric.Decrypt(Convert.FromBase64String(data)));
			}
			return result;
		}

		public string Encrypt(string data)
		{
			string result = string.Empty;
			using (Symmetric symmetric = new Symmetric())
			{
				symmetric.Key = keySym.GetKey();
				result = Convert.ToBase64String(symmetric.Encrypt(Encoding.ASCII.GetBytes(data)));
			}
			return result;
		}
	}
}
