using System;
using System.Windows.Forms;
using Toyota.Common.Web.Cryptography;

namespace SimpleEncryption
{
	internal class Program
	{
		[STAThread]
		private static void Main(string[] args)
		{
			Crypter crypter = new Crypter(new ConfigKeySym());
			string format = "{0}{1}_{2}_{3}";
			string text = "sgFLLt3hWGGowd2S62zoRcFk+I7iJExrthJzL4W5048=";
			string uid, pwd;
			if (args.Length < 1)
			{
				Console.WriteLine("SimpleEncryption username password [username password ... [username password]]");
				Console.Write("User Name: ");
				uid = Console.ReadLine();
				uid = crypter.Encrypt(uid);
				Console.Write("Password: ");
				pwd = Console.ReadLine();
				pwd = crypter.Encrypt(pwd);
				string data3 = string.Format(format, "home?btrck=", text, uid, pwd);
				Clipboard.SetData(DataFormats.StringFormat, data3);
				Console.WriteLine();
				Console.WriteLine("Data has been copied to Clipboard");
			}
			else if (args.Length == 1)
			{
				Console.WriteLine(crypter.Decrypt(args[0]));
			}
			else
			{
				int ia = 0;
				while (ia < args.Length)
				{
					if (ia + 1 >= args.Length)
					{
						break;
					}

					string f = string.Format(format, "home?btrck=", text, crypter.Encrypt(args[ia]), crypter.Encrypt(args[ia + 1]));

					Console.WriteLine("{0}\t{1}", args[ia], f);
					Clipboard.SetData(DataFormats.StringFormat, f);

					ia += 2;
				}
			}
		}
	}
}
