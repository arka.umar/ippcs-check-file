﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.BussinesLayer
{
    class UserInfo
    {
        

        public string USERNAME { get; set; }
        public string CompanyName { get; set; }
        public int ActiveDirectoryFlaq { get; set; }
        public int LockFlag { get; set; }
        public string BranchName { get; set; }
        public int IsActiveFlag { get; set; }
        public string Email { get; set; }
        public string NoReg { get; set; }
        public string FullName { get; set; }
        public string TelNo { get; set; }
        public string MobileNo { get; set; }
        public string ClassId { get; set; }
        public int JobFunctionId { get; set; }
        public string CCCode { get; set; }
        public int LocationId { get; set; }
        public int IsProduction { get; set; }
        public string DefaultApp { get; set; }
    }
}
