﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.BussinesLayer
{
    public class Employee
    {
        public string Username { get; set; }
        public string ClassId { get; set; }
        public string Email { get; set; }
        public string IsActive { get; set; }
        public string LockFlag { get; set; }
        public string MobileNo { get; set; }
        public string ccCode { get; set; }
        public string DefaultApp { get; set; }
        public string Password { get; set; }
        public string PasswordMustBeChanged { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PositionId { get; set; }
        public string PositionLevel { get; set; }
        public string PositionName { get; set; }
        public string SectionId { get; set; }
        public string DepartmentId { get; set; }
        public string DivisionId { get; set; }
        public string DirectorateId { get; set; }
        public string BranchId { get; set; }
        public string LineId { get; set; }
        public string SectionName { get; set; }
        public string DepartmentName { get; set; }
        public string DivisionName { get; set; }
        public string DirectorateName { get; set; }
    }
}
