﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.BussinesLayer
{
    class RoleBussines
    {
        public int AuthorizationID { get; set; }
        public string AuthorizationName { get; set; }
        public int BranchId { get; set; }
        public int FileSentMaxSize { get; set; }
        public int RoleSessionTimeout { get; set; }
        public List<string> AuthorizationDetails { get; set; }
       
    }
}
