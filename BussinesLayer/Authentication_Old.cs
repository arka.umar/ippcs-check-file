﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Threading;
using System.Net;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using System.Reflection;
using Toyota.Common.Web.Credential;
using Newtonsoft.Json;
using Toyota.Common.Web.Util;
using Toyota.Common.Web.Util.Converter;
using Toyota.Common.Database;
using Toyota.Common.Web.Database.Petapoco;
using Toyota.Common.Util.Text;

namespace Toyota.BussinesLayer
{
    public class Authentication_Old
    {
        private static IDBContextManager idbcm = null;

        public static IDBContextManager databaseManager
        {
                get { 
                    if(idbcm == null)
                    {
                         //ConnectionDescription cd = new ConnectionDescription("scToLDAP", "Server=10.16.25.111;Database=OpenLDAP;User ID=webportal;Password=Portal@2012;Trusted_Connection=false;MultipleActiveResultSets=true;Max Pool Size=1000");
                        ConnectionDescription cd = new ConnectionDescription("scToLDAP", "Server=10.16.25.112\\SQLEXPRESS;Database=OpenLDAP;User ID=sa;Password=Adm!nPC5LD4P;Trusted_Connection=false;MultipleActiveResultSets=true;Max Pool Size=1000");
                         idbcm  = new Toyota.Common.Database.Petapoco.PetaPocoContextManager(new ConnectionDescription[] {cd});
                         idbcm.AddQueryLoader(new AssemblyFileQueryLoader(Assembly.GetAssembly(typeof(Authentication_Old)),"Toyota.BussinesLayer.SQLFiles"));

                    }
                    return Authentication_Old.idbcm; 
                
                }
  
        }
        
       
        

        public static bool Login(string UserName, string Password)
        {
            string Query = databaseManager.LoadQuery("ldapAuthentication");
            string Query2 = databaseManager.LoadQuery("ldapAuthencticationNotInAD");
            IDBContext db = databaseManager.GetContext("scToLDAP");
            IList<int> x = db.Fetch<int>(Query, new { username = UserName} );
             IList<string>  y = db.Fetch<string>(Query2, new { username = UserName, password = Password });

            if (x[0] == 1)
                return UserProvider.GetInstance().IsAuthenticAD(UserName, Password);
            else if (y.Count == 0)
                return false;
            else
                return true;

                //return UserProvider.GetInstance().IsAuthenticate(UserName, Password);
        }

        public static string getUserInfo(string UserName, string Password, string app)
        {
            string retLogin = "";
            UserAD user;

            //get userinfo from OpenLDAP
            //user = UserProvider.GetInstance().GetUser(UserName, Password, app);
            #region Get OpenLDAP Data

            string Query = databaseManager.LoadQuery("ldapUserInfo");
            string Query2 = databaseManager.LoadQuery("ldapPositionDetails");
            IDBContext db = databaseManager.GetContext("scToLDAP");

            IList<UserInfo> x = db.Fetch<UserInfo>(Query, new { username = UserName});
            IList<PositionDetails> y = db.Fetch<PositionDetails>(Query2, new { username = UserName });
 
            user = new UserAD();
            user.properties.Add("Username", new List<string> { x[0].USERNAME });
            user.properties.Add("CompanyName", new List<string> { x[0].COMPANY_NAME });
            user.properties.Add("ActiveDirectoryFlag", new List<string> { x[0].ACTIVE_DIRECTORY_FLAG.ToString() });
            user.properties.Add("LockFlag", new List<string> { x[0].LOCK_FLAG.ToString() });
            user.properties.Add("BranchName", new List<string> { x[0].BRANCH_NAME });
            user.properties.Add("IsActive", new List<string> { x[0].ISACTIVE_FLAG.ToString() });
            user.properties.Add("email", new List<string> { x[0].EMAIL });
            user.properties.Add("NoReg", new List<string> { x[0].NO_REG.ToString() });
            user.properties.Add("FullName", new List<string> { x[0].FULL_NAME });
            user.properties.Add("TelNo", new List<string> { x[0].TEL_NO.ToString() });
            user.properties.Add("mobileNo", new List<string> { x[0].MOBILE_NO.ToString() });
            user.properties.Add("classId", new List<string> { x[0].CLASS_ID.ToString() });
            user.properties.Add("JobFunction", new List<string> { x[0].JOB_FUNCTION_ID.ToString() });
            user.properties.Add("ccCode", new List<string> { x[0].CC_CODE.ToString() });
            user.properties.Add("LocationID", new List<string> { x[0].LOCATION_ID.ToString() });
            user.properties.Add("IsProduction", new List<string> { x[0].ISPRODUCTION.ToString() });
            user.properties.Add("DefaultApp", new List<string> { x[0].DEFAULT_APP.ToString() });

            //we map our position details here
            List<string> positiondetails= new List<string>();
            foreach(var positionDetail in y){
                positiondetails.Add(positionDetail.POSITIONDETAILS);
            }
            user.properties.Add("PositionDetails" , positiondetails);
            
            #endregion
            string json = "";
            if (user != null)
            {
                //json = JsonConvert.SerializeObject(user, Formatting.None);
                /* @Lufty: Dicomment dulu gara-gara CPU ngamuk 
                json = JsonParsing.DicToJson(user); */
                json = JSON.ToString<Dictionary<string, List<string>>>(user.properties);
                /*
                using (Symmetric sym = new Symmetric())
                {
                    FileStream fs = System.IO.File.Open(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.RelativeSearchPath) + "\\scenter.snk", FileMode.Open);
                    StrongNameKeyPair k = new StrongNameKeyPair(fs);
                    sym.Key = ASCIIEncoding.ASCII.GetString(k.PublicKey);
                    retLogin = sym.Encrypt(checkJson(json.ToString()));
                    fs.Close();
                    sym.Dispose();
                }
                 * */
            }
            db.Close();
            return json;
        }

        public static string getRole(string UserName, string Password, string app)
        {
            string retLogin = "";
            UserAD user;

            //get userinfo from OpenLDAP
            //user = UserProvider.GetInstance().GetUser(UserName, Password, app);
            #region Get OpenLDAP Data

            string Query = databaseManager.LoadQuery("ldapUserInfo");
            string Query2 = databaseManager.LoadQuery("ldapPositionDetails");
            IDBContext db = databaseManager.GetContext("scToLDAP");

            IList<UserInfo> x = db.Fetch<UserInfo>(Query, new { username = UserName });
            IList<PositionDetails> y = db.Fetch<PositionDetails>(Query2, new { username = UserName });

            user = new UserAD();
            user.properties.Add("Username", new List<string> { x[0].USERNAME });
            user.properties.Add("CompanyName", new List<string> { x[0].COMPANY_NAME });
            user.properties.Add("ActiveDirectoryFlag", new List<string> { x[0].ACTIVE_DIRECTORY_FLAG.ToString() });
            user.properties.Add("LockFlag", new List<string> { x[0].LOCK_FLAG.ToString() });
            user.properties.Add("BranchName", new List<string> { x[0].BRANCH_NAME });
            user.properties.Add("IsActive", new List<string> { x[0].ISACTIVE_FLAG.ToString() });
            user.properties.Add("email", new List<string> { x[0].EMAIL });
            user.properties.Add("NoReg", new List<string> { x[0].NO_REG.ToString() });
            user.properties.Add("FullName", new List<string> { x[0].FULL_NAME });
            user.properties.Add("TelNo", new List<string> { x[0].TEL_NO.ToString() });
            user.properties.Add("mobileNo", new List<string> { x[0].MOBILE_NO.ToString() });
            user.properties.Add("classId", new List<string> { x[0].CLASS_ID.ToString() });
            user.properties.Add("JobFunction", new List<string> { x[0].JOB_FUNCTION_ID.ToString() });
            user.properties.Add("ccCode", new List<string> { x[0].CC_CODE.ToString() });
            user.properties.Add("LocationID", new List<string> { x[0].LOCATION_ID.ToString() });
            user.properties.Add("IsProduction", new List<string> { x[0].ISPRODUCTION.ToString() });
            user.properties.Add("DefaultApp", new List<string> { x[0].DEFAULT_APP.ToString() });

            //we map our position details here
            List<string> positiondetails = new List<string>();
            foreach (var positionDetail in y)
            {
                positiondetails.Add(positionDetail.POSITIONDETAILS);
            }
            user.properties.Add("PositionDetails", positiondetails);

            #endregion
            string json = "";
            if (user != null)
            {
                //json = JsonConvert.SerializeObject(user, Formatting.None);
                /* @Lufty: Dicomment dulu gara-gara CPU ngamuk 
                json = JsonParsing.DicToJson(user); */
                json = JSON.ToString<Dictionary<string, List<string>>>(user.properties);
                /*
                using (Symmetric sym = new Symmetric())
                {
                    FileStream fs = System.IO.File.Open(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.RelativeSearchPath) + "\\scenter.snk", FileMode.Open);
                    StrongNameKeyPair k = new StrongNameKeyPair(fs);
                    sym.Key = ASCIIEncoding.ASCII.GetString(k.PublicKey);
                    retLogin = sym.Encrypt(checkJson(json.ToString()));
                    fs.Close();
                    sym.Dispose();
                }
                 * */
            }
            db.Close();
            return json;
        }

        public static bool chgEmployeeAttr(string UserName, string Password,string attr, string OldVal, string NewVal)
        {
            return UserProvider.GetInstance().ChgEmployeeAttr(UserName, Password, "TOYOTA", attr, OldVal, NewVal);
        }

        public static int crtEmployeeAttr(string UserName, string Password, string user)
        {
            return UserProvider.GetInstance().CrtEmployeeAttr(UserName, Password, "TOYOTA", user);
        }

        public static bool delEmployee(string UserName, string Password, string user)
        {
            return UserProvider.GetInstance().DelEmployee(UserName, Password, "TOYOTA", user);
        }

        public static List<string> appList(string UserName, string Password)
        {
            return UserProvider.GetInstance().AppList(UserName, Password);
        }

        public static string getEmail(string UserName, string Password, List<string> usernames)
        {
            string retEmail = "";
            UserAD emailList;
            emailList = UserProvider.GetInstance().GetEmailList(UserName, Password, usernames);
            string json = JsonParsing.DicToJson(emailList);
            return json;
        }

        public static bool regCreate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            return true;//UserProvider.GetInstance().RegisterCreate(UserName, Password, CompanyName, Address, CompanyAddress, Email, ContactPerson, Telephone, Faximile, MobilePhone);
        }

        public static bool regUpdate(string UserName, string Password, string CompanyName, string Address, string CompanyAddress, string Email, string ContactPerson, string Telephone, string Faximile, string MobilePhone)
        {
            return true;//UserProvider.GetInstance().RegisterUpdate(UserName, Password, CompanyName, Address, CompanyAddress, Email, ContactPerson, Telephone, Faximile, MobilePhone);
        }

        public static bool regDelete(string UserName, string Password, string UserEmail)
        {
            return true;//UserProvider.GetInstance().RegisterDelete(UserName, Password, UserEmail);
        }

        public static bool regIsExist(string UserName, string Password, string UserEmail)
        {
            return true;//UserProvider.GetInstance().RegisterIsExist(UserName, Password, UserEmail);
        }

        public static string genNewUser(string UserName, string Password, string Email)
        {
            
            //string retResult = "";
            //UserAD result;
            //result = UserProvider.GetInstance().GenerateNewUsername(UserName, Password, Email);
            //if (result != null)
            //{
            //    //string json = JsonConvert.SerializeObject(result, Formatting.None);
            //    string json = JsonParsing.DicToJson(result);
            //    using (Symmetric sym = new Symmetric())
            //    {
            //        FileStream fs = System.IO.File.Open(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.RelativeSearchPath) + "\\scenter.snk", FileMode.Open);
            //        StrongNameKeyPair k = new StrongNameKeyPair(fs);
            //        sym.Key = ASCIIEncoding.ASCII.GetString(k.PublicKey);
            //        retResult = sym.Encrypt(checkJson(json.ToString()));
            //        fs.Close();
            //        sym.Dispose();
            //    }
            //}
            //return retResult;
            return "";
        }

        public static string genNewPass(string UserName, string Password, string Email)
        {
            //string retResult = "";
            //UserAD result;
            //result = UserProvider.GetInstance().GenerateNewPassword(UserName, Password, Email);
            //if (result != null)
            //{
            //    //string json = JsonConvert.SerializeObject(result, Formatting.None);
            //    string json = JsonParsing.DicToJson(result);
            //    using (Symmetric sym = new Symmetric())
            //    {
            //        FileStream fs = System.IO.File.Open(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.RelativeSearchPath) + "\\scenter.snk", FileMode.Open);
            //        StrongNameKeyPair k = new StrongNameKeyPair(fs);
            //        sym.Key = ASCIIEncoding.ASCII.GetString(k.PublicKey);
            //        retResult = sym.Encrypt(checkJson(json.ToString()));
            //        fs.Close();
            //        sym.Dispose();
            //    }
            //}
            //return retResult;
            return "";
        }

        public static bool IsDefaultAppExist(string UserName, string Password, string UserEmail)
        {
            return true;//return UserProvider.GetInstance().IsDefaultApplicationExist(UserName, Password, UserEmail);
        }

        public static bool UpdateDefaultApp(string UserName, string Password,string DefaulatApplication)
        {
            return UserProvider.GetInstance().UpdateDefaultApplication(UserName, Password, DefaulatApplication);
        }

        public static String getDefaultApp(string UserName, string Password)
        {
            string defaultApp=""; 
            UserAD user = UserProvider.GetInstance().GetDefaultApp(UserName, Password);
            if (user != null)
            {
                defaultApp = user.properties["app"][0].ToString();
            }
            return defaultApp;
        }

        public static string GetEmployeesData(string values)
        {
            List<UserAD> user;
            user = UserProvider.GetInstance().GetEmployeesData(values);
            string json = "";
            if (user != null)
            {
                json = JsonConvert.SerializeObject(user);
            }
            return json;
        }

        public static string getUserRole(string username, string app)
        {
            List<UserAD> role;
           

            #region GET DATA FROM SQL
            //role = UserProvider.GetInstance().GetUserRole(username, app);


            string Query3 = databaseManager.LoadQuery("ldapRoleID");
            string Query2 = databaseManager.LoadQuery("ldapDetails");
            string Query = databaseManager.LoadQuery("ldap");
            

            IDBContext db = databaseManager.GetContext("scToLDAP");
              
            
            
           
            IList<int> x =  db.Fetch<int>(Query3, new { username = username, aplikasi = app });
            role = new List<UserAD>();
	        #endregion
           
            foreach (var item in x)
	        {
                IList<RoleBussines> ldapRole = db.Fetch<RoleBussines>(Query, new { roleID = item });
                IList<RoleDetails> ldapRoleDetails = db.Fetch<RoleDetails>(Query2, new { aplikasi = app , roleID = item });

               

                UserAD user = new UserAD();
                //user.properties.Add("ObjectClass", new List<string> { "role" });
                user.properties.Add("AuthorizationID", new List<string> { ldapRole[0].ROLE_ID.ToString()});
                user.properties.Add("AuthorizationName", new List<string> { ldapRole[0].ROLE_NAME.ToString() });
                user.properties.Add("branchId", new List<string> { ldapRole[0].BRANCH_ID.ToString()});
                user.properties.Add("fileSentMaxSize", new List<string> { ldapRole[0].ROLE_SENT_MAX_FILESIZE.ToString() });
                user.properties.Add("roleSessionTimeOut", new List<string> { ldapRole[0].ROLE_SESSION_TIMEOUT.ToString()});

                List<string> valueAuth = new List<string>();
                foreach (var AuthDetail in ldapRoleDetails)
	            {
		            valueAuth.Add(AuthDetail.DETAILS.ToString());
	            }
                
                user.properties.Add("authorizationdetails", valueAuth);
                    
                
                role.Add(user);

	        }
            
            
            

            string json = "";
            if (role != null && role.Count != 0)
            {
                json = JsonConvert.SerializeObject(role);
            }

            db.Close();
            
            return json;
        }

        public static string GetEmployeesData(List<string> values)
        {
            List<UserAD> user;
            user = UserProvider.GetInstance().GetEmployeesData(values);
            string json = "";
            if (user != null)
            {
                json = JsonConvert.SerializeObject(user);
            }
            return json;
        }

        public static string GetEmployeesNumber()
        {
            return UserProvider.GetInstance().getEmployeesNumber();
        }

        public static string getUserLogged(string username)
        {
            List<UserAD> info;
            info = UserProvider.GetInstance().GetUserLoggedInfo(username);
            string json = "";
            if (info.Count > 0)
            {
                json = JsonConvert.SerializeObject(info);
            }
            return json;
        }

        public static bool crUserLogged(string user, string password, string info)
        {
            return UserProvider.GetInstance().crUserLoggedInfo(user,password, info);
        }

        public static bool delUserLogged(string user, string password, string username)
        {
            return UserProvider.GetInstance().delUserLoggedInfo(user, password, username);
        }

        public static bool chUserLogged(string user, string password, string changes)
        {
            return UserProvider.GetInstance().chUserLoggedInfo(user, password, changes);
        }

        public static string getRolePR(string UserName, string Password, string app, string AuthId)
        {
            UserAD user;
            user = UserProvider.GetInstance().GetUser(UserName, Password, app);

            List<string> org = new List<string>();
            List<string> structInfo = new List<string>();

            string json = null;
            List<UserAD> Children = UserProvider.GetInstance().GetRoleChunk(UserName, Password, "AuthorizationId", AuthId);

            //we initiate the first entry for node.
            List<UserAD> treeCollection = new List<UserAD>();
            List<List<UserAD>> loopIndex = new List<List<UserAD>>();
            user = UserProvider.GetInstance().GetOrganizationStruct(UserName, Password, "AuthorizationId", AuthId)[0];
            loopIndex.Add(UserProvider.GetInstance().GetOrganizationStruct(UserName, Password, "ParentId", AuthId));
            treeCollection.Add(user);
            user.properties["AuthorizationId"].Add(user.properties["AuthorizationId"][0].ToString());

            //We Looking for Children Nodes over here :)
            for (int i = treeCollection.Count - 1; i < treeCollection.Count; i++)
            {
                if (i < 0)
                    break;
                else
                {
                    string unitID = treeCollection[i].properties["AuthorizationId"][0].ToString();
                    List<UserAD> Node = loopIndex[i];
                    if (Node != null && Node.Count != 0)
                    {
                        int amountChild = 0;
                        //whose children?
                        foreach (UserAD nodeLevel in Node)
                        {
                            treeCollection.Add(nodeLevel);
                            string parentNode = treeCollection[i].properties["authorizationname"][0].ToString() + "Id=" + treeCollection[i].properties["AuthorizationId"][1].ToString() + "," + nodeLevel.properties["authorizationname"][0].ToString() + "Id=" + nodeLevel.properties["AuthorizationId"][0].ToString();
                            structInfo.Add(parentNode);
                            org.Add(parentNode);
                            nodeLevel.properties["AuthorizationId"].Add(parentNode);
                            List<UserAD> CheckNode = UserProvider.GetInstance().GetOrganizationStruct(UserName, Password, "ParentId", nodeLevel.properties["AuthorizationId"][0].ToString());
                            loopIndex.Add(CheckNode);
                            amountChild++;
                        }
                        treeCollection.RemoveAt(treeCollection.Count - (amountChild + 1));
                        loopIndex.RemoveAt(loopIndex.Count - (amountChild + 1));
                    }
                    else
                    {
                        int count = loopIndex[loopIndex.Count - 1].Count + 1;
                        for (int z = 0; z < count; z++)
                        {
                            treeCollection.RemoveAt(treeCollection.Count - 1);
                        }
                        loopIndex.RemoveAt(loopIndex.Count - 1);
                    }
                    i = treeCollection.Count - 2;
                }
            }
            return json;
        }

        public static string getOrganizationStruct(string Username, string Password, string values, int level)
        {
            string json = "";
            UserAD organizationStruct;
            UserAD organizationStructResult = new UserAD();
            List<string> org = new List<string>();
            List<UserAD> orgCapsul = new List<UserAD>();
            List<string> structInfo = new List<string>();

            //Flag for Children or Parent Look up For
            int PARENT = 0;
            int CHILDREN = 1;
            int LOOK_FOR = 100;

            Dictionary<String, List<String>> parameters = null;//JSON.ToObject<Dictionary<string, List<string>>>(values);

            //we initiate the first entry for node.
            string key = parameters.Keys.ElementAt(0);
            string val = parameters[key][0].ToString();
            List<UserAD> treeCollection = new List<UserAD>();
            List<List<UserAD>> loopIndex = new List<List<UserAD>>();
            organizationStruct = UserProvider.GetInstance().GetOrganizationStruct(Username, Password, "UnitId", val)[0];
            loopIndex.Add(UserProvider.GetInstance().GetOrganizationStruct(Username, Password, "ParentId", val));
            treeCollection.Add(organizationStruct);
            organizationStruct.properties["unitid"].Add(organizationStruct.properties["unitname"][0].ToString() +"Id= "+ organizationStruct.properties["unitid"][0].ToString());

            //we do level checking over here
            int currUnitId = Convert.ToInt32(organizationStruct.properties["levelid"][0]);
            int LookingForLv = level;
            if (currUnitId > LookingForLv)
                LOOK_FOR = 0;
            else if (currUnitId < LookingForLv)
                LOOK_FOR = 1;


            if(LOOK_FOR == CHILDREN){

                //We Looking for Children Nodes over here :)
                for (int i = treeCollection.Count - 1; i < treeCollection.Count; i++)
                {
                    if (i < 0)
                        break;
                    else
                    {
                        string unitID = treeCollection[i].properties["unitid"][0].ToString();
                        List<UserAD> Node = loopIndex[i];
                        if (Node != null && Node.Count != 0)
                        {
                            int amountChild = 0;
                            //whose children?
                            foreach (UserAD nodeLevel in Node)
                            {
                                //do we have reach the level we want?
                                if (level >= Convert.ToInt32(nodeLevel.properties["levelid"][0]))
                                {
                                    treeCollection.Add(nodeLevel);
                                    string parentNode = treeCollection[i].properties["unitid"][1].ToString() + "," + nodeLevel.properties["levelname"][0].ToString() + "Id=" + nodeLevel.properties["unitid"][0].ToString();
                                    structInfo.Add(parentNode);
                                    org.Add(parentNode);
                                    nodeLevel.properties["unitid"].Add(parentNode);
                                    List<UserAD> CheckNode = UserProvider.GetInstance().GetOrganizationStruct(Username, Password, "ParentId", nodeLevel.properties["unitid"][0].ToString());
                                    loopIndex.Add(CheckNode);
                                    amountChild++;
                                }
                            }
                            treeCollection.RemoveAt(treeCollection.Count - (amountChild + 1));
                            loopIndex.RemoveAt(loopIndex.Count - (amountChild + 1));
                        }
                        else
                        {
                            int count = loopIndex[loopIndex.Count - 1].Count + 1;
                            for (int z = 0; z < count; z++)
                            {
                                treeCollection.RemoveAt(treeCollection.Count - 1);
                            }
                            loopIndex.RemoveAt(loopIndex.Count - 1);
                        }
                        i = treeCollection.Count - 2;
                    }
                }
            }

            if (LOOK_FOR == PARENT)
            {
                //we reset the org struct over here
                organizationStruct = UserProvider.GetInstance().GetOrganizationStruct(Username, Password, "UnitId", val)[0];

                //We looking for Parent node here
                int flagPar = 0;
                do
                {
                    if (organizationStruct.properties["parentid"][0].ToString().CompareTo("0") == 1)
                    {
                        string ParentId = organizationStruct.properties["parentid"][0].ToString();
                        organizationStruct = UserProvider.GetInstance().GetOrganizationStruct(Username, Password, "UnitId", ParentId)[0];
                        org.Add(organizationStruct.properties["levelname"][0].ToString() + "Id=" + organizationStruct.properties["unitname"][0].ToString());
                    }
                    else
                    {
                        flagPar = 1;
                    }
                } while (flagPar == 0);
            }

            organizationStructResult.properties.Add("Org", org);
            orgCapsul.Add(organizationStructResult);
            json = JsonConvert.SerializeObject(orgCapsul, Formatting.None);
            return json;
        }
    }
}