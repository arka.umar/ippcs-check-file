select 
	username as "Username", 
	[password] as "Password", 
	password_must_be_changed as "PasswordMustBeChanged"
from tb_m_employee 
where ([password] = @password) and (username = @username)