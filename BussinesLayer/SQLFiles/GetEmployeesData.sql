SELECT 
	E.USERNAME [Username] 
	, e.EMAIL [Email]
	, e.CLASS_ID [ClassId]
	, e.NO_REG [NoReg]
	, e.ISACTIVE_FLAG [IsActive]
	, e.LOCK_FLAG [LockFlag]
	, e.MOBILE_NO [MobileNo]
	, e.CC_CODE [ccCode]
	, e.DEFAULT_APP [DefaultApp]
	, e.COMPANY_ID [CompanyId]
	, e.PASSWORD [Password]
	, e.PASSWORD_MUST_BE_CHANGED [PasswordMustBeChanged]
	, E.FIRST_NAME [FirstName]
	, E.LAST_NAME [LastName]
	, m.POSITION_ID [PositionId]
	, P.POSITION_LEVEL [PositionLevel]
	, p.POSITION_NAME [PositionName]
	, m.SECTION_ID [SectionId]
	, m.DEPARTMENT_ID [DepartmentId]
	, m.DIVISION_ID [DivisionId]
	, m.DIRECTORATE_ID [DirectorateId]
	, m.BRANCH_ID [BranchId]
	, m.LINE_ID [LineId]
	, s.UNIT_NAME [SectionName]
	, dept.UNIT_NAME [DepartmentName]
	, dept.UNIT_NAME [DivisionName]
	, dir.UNIT_NAME [DirectorateName]
	
FROM TB_M_EMPLOYEE E
JOIN TB_M_USER_POSITION_MAP m on e.USERNAME = m.USERNAME
LEFT JOIN TB_M_POSITION p ON m.POSITION_ID = P.POSITION_ID
LEFT JOIN TB_M_ORGANIZATION_STRUCTURE s ON s.UNIT_ID = m.SECTION_ID 
LEFT JOIN TB_M_ORGANIZATION_STRUCTURE dept ON dept.UNIT_ID = m.DEPARTMENT_ID
LEFT JOIN TB_M_ORGANIZATION_STRUCTURE div ON div.UNIT_ID = m.DIVISION_ID
LEFT JOIN TB_M_ORGANIZATION_STRUCTURE dir ON dir.UNIT_ID = m.DIRECTORATE_ID