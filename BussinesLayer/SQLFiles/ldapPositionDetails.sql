

select		
'Username='+pos_map.username +',Line='+ISNULL((select UNIT_NAME from TB_M_ORGANIZATION_STRUCTURE where UNIT_ID=pos_map.LINE_ID),'NULL')+
',Position='+ISNULL((select POSITION_NAME from TB_M_POSITION where POSITION_ID=pos_map.POSITION_ID),'NULL')+
',Group='+ISNULL((select UNIT_NAME from TB_M_ORGANIZATION_STRUCTURE where UNIT_ID=pos_map.GROUP_ID),'NULL')+
',Section='+ISNULL((select UNIT_NAME from TB_M_ORGANIZATION_STRUCTURE where UNIT_ID=pos_map.SECTION_ID),'NULL')+
',Department='+ISNULL((select UNIT_NAME from TB_M_ORGANIZATION_STRUCTURE where UNIT_ID=pos_map.DEPARTMENT_ID),'NULL')+
',Division='+ISNULL((select UNIT_NAME from TB_M_ORGANIZATION_STRUCTURE where UNIT_ID=pos_map.DIVISION_ID),'NULL')+
',Directorate='+ISNULL((select UNIT_NAME from TB_M_ORGANIZATION_STRUCTURE where UNIT_ID=pos_map.DIRECTORATE_ID),'NULL')+
',Branch='+b.BRANCH_NAME AS POSITIONDETAILS
from persons P 
INNER JOIN TB_M_USER_POSITION_MAP as pos_map on pos_map.USERNAME = P.cn
INNER JOIN TB_M_BRANCH as b on b.BRANCH_ID = pos_map.BRANCH_ID
where P.cn = @username