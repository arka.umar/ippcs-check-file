﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Toyota.Monitoring.AppMonitorManager
{

    public class Monitor : IDisposable
    {
        public string ID { get; private set; }
        public string Path { get; set; }
        public string TypeName { get; set; }
        public int Port { get; private set; }
        public System.Windows.Forms.Form Form { get; set; }
        public System.Windows.Forms.Screen Screen { get; set; }
        public int Index { get; set; }

        public event EventHandler Closed;
        public event EventHandler Opened;

        private Assembly assm;
         
        public Monitor(string path, string typeName, System.Windows.Forms.Screen screen)
        { Init(path, typeName, screen, -1); }

        public Monitor(string path, string typeName, System.Windows.Forms.Screen screen, int index)
        { Init(path, typeName, screen, index); }

        private void Init(string path, string typeName, System.Windows.Forms.Screen screen, int index)
        {
            ID = screen.DeviceName;
            Path = path;
            TypeName = typeName;
            Screen = screen;
            Index = index;
        }

        private void SetDefaultProperty()
        {
            Form.Location = Screen.WorkingArea.Location;
            Form.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            Form.ShowInTaskbar = false;
            Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        }

        public void CreateInstance()
        {
            if (assm == null)
            {
                assm = Assembly.LoadFile(Path);
                Form = (System.Windows.Forms.Form)assm.CreateInstance(TypeName);
                Form.WindowState = System.Windows.Forms.FormWindowState.Normal;
                Form.FormClosed += Form_FormClosed;
                Form.Show();
                FieldInfo portInfo = Form.GetType().GetField("Port", BindingFlags.GetField | BindingFlags.Instance | BindingFlags.Public);
                 
                if (portInfo != null)
                { Port = (int)portInfo.GetValue(Form); }
                else
                {  throw new EntryPointNotFoundException("Application not intended for Monitoring Application, because doesn\'t have \'Port\' Field."); }
 
                OnOpened(this, new EventArgs());
            }
            else
            {
                Form.Invalidate();
            }
        }

        void Form_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            assm = null;
            OnClosed(this, new EventArgs());
        }

        protected object InvokeMember(string member, BindingFlags flags)
        {
            return Form.GetType().InvokeMember(member, flags, null, Form, new object[] { });
        }

        protected void OnClosed(object sender, EventArgs e)
        {
            if (Closed != null)
                Closed(sender, e);
        }

        protected void OnOpened(object sender, EventArgs e)
        {
            SetDefaultProperty();
            if (Opened != null)
                Opened(sender, e);
        }

        public void Dispose()
        {
            assm = null;
            if (Form != null) Form.Dispose();
            GC.SuppressFinalize(this);
        }

    }

}
