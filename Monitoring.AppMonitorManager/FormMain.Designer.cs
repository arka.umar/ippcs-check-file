﻿namespace Toyota.Monitoring.AppMonitorManager
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPath = new UnivercaDotNet.NetUIStyle.TextBoxFade();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.toolTipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.flatButtonFile = new UnivercaDotNet.NetUIStyle.FlatButton(this.components);
            this.openFileDialogApp = new System.Windows.Forms.OpenFileDialog();
            this.flatButtonSave = new UnivercaDotNet.NetUIStyle.FlatButton(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.timerScreens = new System.Windows.Forms.Timer(this.components);
            this.comboBoxTypeName = new System.Windows.Forms.ComboBox();
            this.labelInformation = new System.Windows.Forms.Label();
            this.propertyGridForm = new System.Windows.Forms.PropertyGrid();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.checkBoxAll = new System.Windows.Forms.CheckBox();
            this.ListViewMonitors = new System.Windows.Forms.ListView();
            this.labelSelectedObject = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.flatButtonClear = new UnivercaDotNet.NetUIStyle.FlatButton(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label1.Location = new System.Drawing.Point(12, 328);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Assembly";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(66, 335);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(653, 1);
            this.label2.TabIndex = 14;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label3.Location = new System.Drawing.Point(30, 354);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Path";
            // 
            // textBoxPath
            // 
            this.textBoxPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.textBoxPath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.textBoxPath.BackColor = System.Drawing.Color.Transparent;
            this.textBoxPath.BorderColorActive = System.Drawing.Color.DarkBlue;
            this.textBoxPath.BorderColorInactive = System.Drawing.Color.MediumSeaGreen;
            this.textBoxPath.Location = new System.Drawing.Point(113, 347);
            this.textBoxPath.MaxLength = 32767;
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.textBoxPath.ReadOnly = false;
            this.textBoxPath.Size = new System.Drawing.Size(190, 30);
            this.textBoxPath.TabIndex = 1;
            this.toolTipInfo.SetToolTip(this.textBoxPath, "Loads an assembly given the long form of its name.");
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(104, 354);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = ":";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(104, 384);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = ":";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label6.Location = new System.Drawing.Point(30, 384);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Form";
            // 
            // flatButtonFile
            // 
            this.flatButtonFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flatButtonFile.BackColor = System.Drawing.Color.Green;
            this.flatButtonFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatButtonFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(239)))), ((int)(((byte)(220)))));
            this.flatButtonFile.Location = new System.Drawing.Point(309, 352);
            this.flatButtonFile.MouseEnterColor = System.Drawing.Color.Silver;
            this.flatButtonFile.MouseLeaveColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(239)))), ((int)(((byte)(220)))));
            this.flatButtonFile.Name = "flatButtonFile";
            this.flatButtonFile.Size = new System.Drawing.Size(52, 20);
            this.flatButtonFile.TabIndex = 2;
            this.flatButtonFile.Text = "Browse";
            this.flatButtonFile.Click += new System.EventHandler(this.flatButtonFile_Click);
            // 
            // openFileDialogApp
            // 
            this.openFileDialogApp.Filter = "Application (*.exe)|*.exe";
            this.openFileDialogApp.Title = "Choose file";
            // 
            // flatButtonSave
            // 
            this.flatButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flatButtonSave.BackColor = System.Drawing.Color.Green;
            this.flatButtonSave.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatButtonSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(239)))), ((int)(((byte)(220)))));
            this.flatButtonSave.Location = new System.Drawing.Point(611, 432);
            this.flatButtonSave.MouseEnterColor = System.Drawing.Color.Silver;
            this.flatButtonSave.MouseLeaveColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(239)))), ((int)(((byte)(220)))));
            this.flatButtonSave.Name = "flatButtonSave";
            this.flatButtonSave.Size = new System.Drawing.Size(107, 50);
            this.flatButtonSave.TabIndex = 4;
            this.flatButtonSave.Text = "Apply";
            this.flatButtonSave.Click += new System.EventHandler(this.flatButtonApply_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(15, 419);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(703, 1);
            this.label7.TabIndex = 25;
            this.label7.Text = "label7";
            // 
            // timerScreens
            // 
            this.timerScreens.Interval = 2500;
            this.timerScreens.Tick += new System.EventHandler(this.timerScreens_Tick);
            // 
            // comboBoxTypeName
            // 
            this.comboBoxTypeName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxTypeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTypeName.FormattingEnabled = true;
            this.comboBoxTypeName.Location = new System.Drawing.Point(119, 381);
            this.comboBoxTypeName.Name = "comboBoxTypeName";
            this.comboBoxTypeName.Size = new System.Drawing.Size(214, 21);
            this.comboBoxTypeName.TabIndex = 3;
            // 
            // labelInformation
            // 
            this.labelInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelInformation.AutoSize = true;
            this.labelInformation.Location = new System.Drawing.Point(401, 451);
            this.labelInformation.Name = "labelInformation";
            this.labelInformation.Size = new System.Drawing.Size(0, 13);
            this.labelInformation.TabIndex = 27;
            // 
            // propertyGridForm
            // 
            this.propertyGridForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGridForm.Location = new System.Drawing.Point(3, 22);
            this.propertyGridForm.Name = "propertyGridForm";
            this.propertyGridForm.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGridForm.Size = new System.Drawing.Size(205, 297);
            this.propertyGridForm.TabIndex = 32;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(5, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.checkBoxAll);
            this.splitContainer1.Panel1.Controls.Add(this.ListViewMonitors);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.labelSelectedObject);
            this.splitContainer1.Panel2.Controls.Add(this.label8);
            this.splitContainer1.Panel2.Controls.Add(this.label10);
            this.splitContainer1.Panel2.Controls.Add(this.propertyGridForm);
            this.splitContainer1.Size = new System.Drawing.Size(721, 322);
            this.splitContainer1.SplitterDistance = 501;
            this.splitContainer1.TabIndex = 29;
            // 
            // checkBoxAll
            // 
            this.checkBoxAll.AutoSize = true;
            this.checkBoxAll.Location = new System.Drawing.Point(9, 10);
            this.checkBoxAll.Name = "checkBoxAll";
            this.checkBoxAll.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAll.TabIndex = 38;
            this.checkBoxAll.UseVisualStyleBackColor = true;
            this.checkBoxAll.CheckedChanged += new System.EventHandler(this.checkBoxAll_CheckedChanged);
            // 
            // ListViewMonitors
            // 
            this.ListViewMonitors.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.ListViewMonitors.AllowColumnReorder = true;
            this.ListViewMonitors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ListViewMonitors.CheckBoxes = true;
            this.ListViewMonitors.FullRowSelect = true;
            this.ListViewMonitors.GridLines = true;
            this.ListViewMonitors.Location = new System.Drawing.Point(3, 3);
            this.ListViewMonitors.Name = "ListViewMonitors";
            this.ListViewMonitors.Size = new System.Drawing.Size(495, 316);
            this.ListViewMonitors.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.ListViewMonitors.TabIndex = 37;
            this.ListViewMonitors.UseCompatibleStateImageBehavior = false;
            this.ListViewMonitors.View = System.Windows.Forms.View.Details;
            this.ListViewMonitors.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.ListViewMonitors_ColumnWidthChanging);
            this.ListViewMonitors.ItemActivate += new System.EventHandler(this.ListViewMonitors_ItemActivate);
            this.ListViewMonitors.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ListViewMonitors_ItemChecked);
            // 
            // labelSelectedObject
            // 
            this.labelSelectedObject.AutoSize = true;
            this.labelSelectedObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectedObject.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.labelSelectedObject.Location = new System.Drawing.Point(114, 6);
            this.labelSelectedObject.Name = "labelSelectedObject";
            this.labelSelectedObject.Size = new System.Drawing.Size(0, 13);
            this.labelSelectedObject.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label8.Location = new System.Drawing.Point(98, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label10.Location = new System.Drawing.Point(5, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "Selected Monitor";
            // 
            // flatButtonClear
            // 
            this.flatButtonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flatButtonClear.BackColor = System.Drawing.Color.RoyalBlue;
            this.flatButtonClear.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatButtonClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(239)))), ((int)(((byte)(220)))));
            this.flatButtonClear.Location = new System.Drawing.Point(487, 432);
            this.flatButtonClear.MouseEnterColor = System.Drawing.Color.Silver;
            this.flatButtonClear.MouseLeaveColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(239)))), ((int)(((byte)(220)))));
            this.flatButtonClear.Name = "flatButtonClear";
            this.flatButtonClear.Size = new System.Drawing.Size(107, 50);
            this.flatButtonClear.TabIndex = 30;
            this.flatButtonClear.Text = "Clear";
            this.flatButtonClear.Click += new System.EventHandler(this.flatButtonClear_Click);
            // 
            // FormMain
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(728, 494);
            this.Controls.Add(this.flatButtonClear);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.labelInformation);
            this.Controls.Add(this.comboBoxTypeName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.flatButtonSave);
            this.Controls.Add(this.flatButtonFile);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "App Monitor Manager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UnivercaDotNet.NetUIStyle.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private UnivercaDotNet.NetUIStyle.TextBoxFade textBoxPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolTip toolTipInfo;
        private UnivercaDotNet.NetUIStyle.FlatButton flatButtonFile;
        private System.Windows.Forms.OpenFileDialog openFileDialogApp;
        private UnivercaDotNet.NetUIStyle.FlatButton flatButtonSave;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer timerScreens;
        private System.Windows.Forms.ComboBox comboBoxTypeName;
        private System.Windows.Forms.Label labelInformation;
        private System.Windows.Forms.PropertyGrid propertyGridForm;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox checkBoxAll;
        private System.Windows.Forms.ListView ListViewMonitors;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelSelectedObject;
        private UnivercaDotNet.NetUIStyle.FlatButton flatButtonClear;


    }
}

