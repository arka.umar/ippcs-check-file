﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Sockets;
using Toyota.Monitoring.AppMonitorManager.OperationContracts;

namespace Toyota.Monitoring.AppMonitorManager
{

    [RequestFilter]
    public class Server : TcpMonitor, IDisposable
    {
        public List<ActionInvoker> Action { get; private set; }

        public Server(int port)
            : base("", port)
        {
            Init("", port);
        }

        public Server(string ipAddress, int port)
            : base(ipAddress, port)
        {
            Init(ipAddress, port);
        }

        private void Init(string ipAddress, int port)
        {
            Action = new List<ActionInvoker>();
            Action.Add(new ActionInvoker(this, Type.GetType("Toyota.Monitoring.AppMonitorManager.OperationContracts.IAndonReceived")));
            base.PacketReceived += OnPacketReceived;
            base.Connected += OnClientConnected;
            base.Disconnected += OnClientDisconnect;
            base.Closed += OnClosed;
            base.Open += OnOpen;
        }

        private void OnPacketReceived(object sender, PacketEventArgs e)
        {
            // implement actions individually
            //Action[0].Invoke("Received", new object[] { this, new object[]{ e }});
        }

        private void OnClientConnected(object sender, SocketEventArgs e)
        {
            // notif when client connected
        }

        private void OnClientDisconnect(object sender, SocketEventArgs e)
        {
            // notif when client disconnected
        }

        private void OnClosed(object sender, EventArgs e)
        {
            // notif when server close connection

        }

        private void OnOpen(object sender, EventArgs e)
        {
            // notif when server open connectiontimerRead.Start();
        }

        public void Dispose()
        {
            this.Dispose();
            GC.SuppressFinalize(this);
        }

    }
}
