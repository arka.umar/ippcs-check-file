﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toyota.Sockets;
using Toyota.Monitoring.AppMonitorManager.OperationContracts;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;

namespace Toyota.Monitoring.AppMonitorManager
{

    //[ActionWhenAndonReceived]
    public partial class FormMain : Form
    {
        private IBaseContract contract;

        private Dictionary<string, Monitor> listMonitors;
        private bool checkBoxAllState;
        private DataSetMonitor.MonitorDataTable dataSetMonitor;

        private Dictionary<string, Screen> listTempScreens;
        internal Server serverClient;
        internal Server serverIPPCS;

        public FormMain()
        {
            InitializeComponent();
            string IPPCSPort = System.Configuration.ConfigurationManager.AppSettings["IPPCSPort"];
            string MonitoringPort = System.Configuration.ConfigurationManager.AppSettings["MonitoringPort"];
            if (string.IsNullOrEmpty(IPPCSPort))
                throw new NoNullAllowedException("IPServer cannot be null or configuration must contain \'IPServer\' key.");
            if (string.IsNullOrEmpty(MonitoringPort))
                throw new NoNullAllowedException("Port cannot be null or configuration must contain \'Port\' key.");

            listTempScreens = new Dictionary<string, Screen>();

            contract = new BaseContract();
            serverIPPCS = new Server(Convert.ToInt32(IPPCSPort));
            serverIPPCS.Connected += OnClientConnectedServer;
            serverIPPCS.Disconnected += OnClientDisconnectServer;
            serverIPPCS.PacketReceived += OnPacketReceivedServer;
            serverIPPCS.Start();

            serverClient = new Server(Convert.ToInt32(MonitoringPort));
            serverClient.Connected += OnClientConnectedClient;
            serverClient.Disconnected += OnClientDisconnectClient;
            serverClient.PacketReceived += OnPacketReceivedClient; 
            serverClient.Start();
            this.InitAction();
        }

        private void InitAction()
        {
            serverIPPCS.Action.Add(new ActionInvoker(this, Type.GetType("Toyota.Monitoring.AppMonitorManager.OperationContracts.IAndonReceived")));
        }

        private void OnPacketReceivedServer(object sender, PacketEventArgs e)
        {
            // notif when client received data
            string dataReceived = Encoding.GetEncoding("ibm850").GetString(e.Data);
            Match match = new Regex(@"<STATION>(\d+)").Match(dataReceived);
            string station = "";
            if (match.Success)
            {
                station = match.Groups[1].Value;
                station = station.Replace("0", "");
                if (this.listMonitors.Count > 0)
                {
                    string DISPLAY = System.Configuration.ConfigurationManager.AppSettings["CONST_DISPLAY_TAG"];
                    Monitor mon = this.listMonitors.Where(item => item.Key == @"\\.\" + DISPLAY + station).Select(x => x.Value).SingleOrDefault();
                    if (mon != null)
                    {
                        Socket client = this.serverClient.Clients[mon.Port];
                        client.SendBufferSize = e.Data.Length;
                        client.Send(e.Data); 
                    }
                }
            }
        }

        private void OnClientConnectedServer(object sender, SocketEventArgs e)
        {
            // notif when client connected
        }

        private void OnClientDisconnectServer(object sender, SocketEventArgs e)
        {
            // notif when client disconnected
        }

        private void OnPacketReceivedClient(object sender, PacketEventArgs e)
        {
            // notif when client received data
        }

        private void OnClientConnectedClient(object sender, SocketEventArgs e)
        {
            // notif when client connected
        }

        private void OnClientDisconnectClient(object sender, SocketEventArgs e)
        {
            // notif when client disconnected
        }

        private void InitializeListView()
        {
            this.ListViewMonitors.Clear();
            this.ListViewMonitors.Groups.Add(new ListViewGroup("List of Monitor"));
            this.ListViewMonitors.Columns.Add("", 25);
            this.ListViewMonitors.Columns.Add("Device Name", 120);
            this.ListViewMonitors.Columns.Add("Working Area", 220);
            this.ListViewMonitors.Columns.Add("Status", 290);
            foreach (Screen scr in Screen.AllScreens)
            {
                ListViewItem lvi = new ListViewItem(new string[] { "", scr.DeviceName, scr.WorkingArea.ToString(), Monitoring.AppMonitorManager.Properties.Resources.NotUsed });
                lvi.Name = scr.DeviceName;
                lvi.Tag = scr;
                lvi.Group = this.ListViewMonitors.Groups[0];
                this.ListViewMonitors.Items.Add(lvi);
            }
        }

        private void SyncListView()
        {
            List<string> keys = new List<string>();
            foreach (ListViewItem lvi in this.ListViewMonitors.Items)
            { keys.Add(lvi.Name); }

            /// find monitor on ListMonitors.Items that doesnt exist in Screen.AllScreens
            IEnumerable<string> indexRemoveKey = keys.Where(x => !Screen.AllScreens.Select(s => s.DeviceName).Contains(x));
            /// find monitor on Screen.AllScreens that doesnt exist in ListMonitors
            IEnumerable<string> indexAddKey = Screen.AllScreens.Where(s => !keys.Contains(s.DeviceName)).Select(x => x.DeviceName);

            if (indexRemoveKey.Any() && indexRemoveKey != null)
            {
                indexRemoveKey.ToList().ForEach(act => this.ListViewMonitors.Items.RemoveByKey(act));
            }

            if ((indexAddKey.Any() && indexAddKey != null))
            {
                foreach (string removeKey in indexAddKey)
                {
                    Screen scr = Screen.AllScreens.Where(s => s.DeviceName == removeKey).SingleOrDefault();
                    ListViewItem lviNew = new ListViewItem(new string[] { "", scr.DeviceName, scr.WorkingArea.ToString(), Monitoring.AppMonitorManager.Properties.Resources.NotUsed });
                    lviNew.Name = scr.DeviceName; lviNew.Group = this.ListViewMonitors.Groups[0]; lviNew.Tag = scr;
                    this.ListViewMonitors.Items.Add(lviNew);
                }
            }
        }

        private void SyncListMonitors()
        {
            List<string> keys = new List<string>();
            foreach (string key in this.listMonitors.Keys)
            { keys.Add(key); }

            ListView.CheckedListViewItemCollection checkd = ListViewMonitors.CheckedItems;
            IEnumerable<ListViewItem> castLvi = checkd.Cast<ListViewItem>();
             
            /// find monitor on Screen.AllScreens that doesnt exist in ListMonitors
            IEnumerable<string> indexAddKey = castLvi.Where(s => !keys.Contains(s.Name)).Select(x => x.Name);
            
            if ((indexAddKey.Any() && indexAddKey != null))
            {
                foreach (string removeKey in indexAddKey)
                {
                    ListViewItem lvi = castLvi.Where(s => s.Name == removeKey).SingleOrDefault();
                    if (!this.listMonitors.ContainsKey(((Screen)lvi.Tag).DeviceName))
                    {
                        this.listMonitors.Add(((Screen)lvi.Tag).DeviceName, new Monitor(this.textBoxPath.Text, this.comboBoxTypeName.Text, (Screen)lvi.Tag, lvi.Index));
                        this.listMonitors[((Screen)lvi.Tag).DeviceName].Closed += OnClosed;
                        this.listMonitors[((Screen)lvi.Tag).DeviceName].Opened += OnOpened;
                    }
                }
            }
        }

        private void Save()
        {
            this.dataSetMonitor.Rows.Clear();
            foreach (string ID in this.listMonitors.Keys)
            {
                Monitor mon = this.listMonitors[ID];
                if (this.dataSetMonitor.Rows.Find(mon.ID) == null)
                    this.dataSetMonitor.AddMonitorRow(mon.ID, mon.TypeName, mon.Path);
            }
            this.dataSetMonitor.WriteXml(Toyota.Monitoring.AppMonitorManager.Properties.Settings.Default.SavedFile);
        }

        private void Clear()
        {
            if (this.ListViewMonitors.CheckedItems.Count > 0)
            {
                foreach (ListViewItem lvi in this.ListViewMonitors.CheckedItems)
                {
                    if (this.listMonitors.ContainsKey(lvi.Name))
                    {
                        Monitor mon = this.listMonitors[lvi.Name];
                        OnClosed(mon, new EventArgs());
                        mon.Dispose();
                        lvi.Checked = false;
                        lvi.Tag = null;
                        lvi.SubItems[3].Text = Monitoring.AppMonitorManager.Properties.Resources.NotUsed;
                    }
                }

                List<string> keys = new List<string>();
                foreach (string key in this.listMonitors.Keys)
                { keys.Add(key); }

                /// find monitor on Screen.AllScreens that doesnt exist in ListMonitors
                IEnumerable<Screen> indexAddKey = Screen.AllScreens.Where(s => !keys.Contains(s.DeviceName));
                foreach (ListViewItem lvi in this.ListViewMonitors.Items)
                { lvi.Tag = indexAddKey.Where(x => x.DeviceName == lvi.Name).SingleOrDefault<Screen>(); }

                this.checkBoxAll.Checked = false;
            }
            else
            {
                MessageBox.Show(Monitoring.AppMonitorManager.Properties.Resources.ClearPleaseSelect);
                return;
            }
            if (this.ListViewMonitors.CheckedItems.Count == Screen.AllScreens.Length)
                InitializeListView();
        }

        protected void OnClosed(object sender, EventArgs e)
        {
            this.listMonitors[((Monitor)sender).ID].Dispose();
            ListViewItem ClosedMonitor = this.ListViewMonitors.Items[((Monitor)sender).ID];
            if (ClosedMonitor != null)
            {
                Screen scr = listTempScreens[ClosedMonitor.Name];
                if (scr != null)
                { ClosedMonitor.Tag = scr; }
                this.textBoxPath.Text = "";
                this.comboBoxTypeName.Items.Clear();
                this.propertyGridForm.SelectedObject = null;
                this.labelSelectedObject.Text = "";

                ClosedMonitor.BackColor = Monitoring.AppMonitorManager.Properties.Settings.Default.ClosedMonitorBackColor;
                ClosedMonitor.ForeColor = Monitoring.AppMonitorManager.Properties.Settings.Default.ClosedMonitorFontColor;
                ClosedMonitor.SubItems[3].Text = Monitoring.AppMonitorManager.Properties.Resources.NotUsed;
            }
            
            this.listMonitors.Remove(((Monitor)sender).ID);
        }

        protected void OnOpened(object sender, EventArgs e)
        {
            ListViewItem OpenedMonitor = this.ListViewMonitors.Items[((Monitor)sender).ID];
            if (OpenedMonitor != null)
            {
                if (!listTempScreens.ContainsKey(OpenedMonitor.Name))
                { listTempScreens.Add(OpenedMonitor.Name, (Screen)OpenedMonitor.Tag); }
                OpenedMonitor.Tag = sender;
                OpenedMonitor.BackColor = Monitoring.AppMonitorManager.Properties.Settings.Default.OpenedMonitorBackColor;
                OpenedMonitor.ForeColor = Monitoring.AppMonitorManager.Properties.Settings.Default.OpenedMonitorFontColor;
                OpenedMonitor.SubItems[3].Text = Monitoring.AppMonitorManager.Properties.Resources.InUsed;
            }
        }
         
        private void FormMain_Load(object sender, EventArgs e)
        {
            this.InitializeListView();
            this.listMonitors = new Dictionary<string, Monitor>();
            this.dataSetMonitor = new DataSetMonitor.MonitorDataTable();
            this.dataSetMonitor.ReadXml(Toyota.Monitoring.AppMonitorManager.Properties.Settings.Default.SavedFile);
            for (int i = 0; i < this.dataSetMonitor.Rows.Count; i++)
            {
                DataSetMonitor.MonitorRow row = (DataSetMonitor.MonitorRow)this.dataSetMonitor.Rows[i];
                Screen scr = Screen.AllScreens.Where(item => item.DeviceName == row.ID).SingleOrDefault();
                if (scr != null)
                {
                    Monitor mon = new Monitor(row.Path, row.TypeName, scr);
                    try
                    {
                        this.listMonitors.Add(row.ID, mon);
                        this.listMonitors[row.ID].Closed += OnClosed;
                        this.listMonitors[row.ID].Opened += OnOpened;
                        mon.CreateInstance();
                    }
                    catch (Exception ex)
                    {
                        this.listMonitors[row.ID].Closed -= OnClosed;
                        this.listMonitors[row.ID].Opened -= OnOpened;
                        this.listMonitors.Remove(row.ID);
                        if (ex.InnerException != null)
                            MessageBox.Show(ex.InnerException.Message, this.Text);
                        else
                            MessageBox.Show(ex.Message, this.Text);
                        mon.Dispose();
                    }
                }
            }

            this.timerScreens.Start();
        }

        private void flatButtonFile_Click(object sender, EventArgs e)
        {
            if (openFileDialogApp.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.comboBoxTypeName.Items.Clear();
                string str = openFileDialogApp.FileName;
                this.textBoxPath.Text = str;
                Assembly assFile = Assembly.LoadFile(str);
                foreach (Type type in assFile.GetExportedTypes())
                {
                    if (type.BaseType == typeof(Form))
                        this.comboBoxTypeName.Items.Add(type.FullName);
                }
                if (this.comboBoxTypeName.Items.Count > 0)
                    this.comboBoxTypeName.SelectedIndex = 0;
                else
                {
                    MessageBox.Show(Monitoring.AppMonitorManager.Properties.Resources.NotContainsForm, this.Text);
                    this.textBoxPath.Text = "";
                }
            }
        }

        private void timerScreens_Tick(object sender, EventArgs e)
        {
            this.SyncListView();
        }

        private void flatButtonApply_Click(object sender, EventArgs e)
        {
            if ((string.IsNullOrEmpty(this.textBoxPath.Text)) || (this.comboBoxTypeName.Items.Count == 0))
            { MessageBox.Show("Path or Type Name cannot be empty.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }

            if (this.ListViewMonitors.CheckedItems.Count == 0)
            { MessageBox.Show("Please select monitor.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
             
            this.SyncListMonitors();

            toolTipInfo.Show(Monitoring.AppMonitorManager.Properties.Resources.AppliedSucces, labelInformation, 5000);
            foreach (Monitor mon in listMonitors.Values)
            {
                try
                {
                    mon.CreateInstance();
                    this.propertyGridForm.SelectedObject = mon.Form;
                    this.checkBoxAll.Checked = false;
                    this.textBoxPath.Text = "";
                    this.comboBoxTypeName.Items.Clear();
                    this.propertyGridForm.SelectedObject = null;
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        MessageBox.Show(ex.InnerException.Message, this.Text);
                    else
                        MessageBox.Show(ex.Message, this.Text);
                    mon.Dispose();
                }
            }
            this.Save();
        }

        private void checkBoxAll_CheckedChanged(object sender, EventArgs e)
        {
            this.checkBoxAllState = true;
            if (this.checkBoxAll.CheckState != CheckState.Indeterminate)
            {
                foreach (ListViewItem lvi in this.ListViewMonitors.Items)
                { lvi.Checked = checkBoxAll.Checked; }
            }
            this.checkBoxAllState = false;
        }

        private void ListViewMonitors_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            // force column checkBox to width 25
            if (e.ColumnIndex == 0)
            { e.Cancel = true; e.NewWidth = 25; }
        }

        private void ListViewMonitors_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (checkBoxAllState) return;
            if (!e.Item.Checked)
            {
                if (this.ListViewMonitors.CheckedItems.Count < this.ListViewMonitors.Items.Count && this.ListViewMonitors.CheckedItems.Count > 0)
                    this.checkBoxAll.CheckState = CheckState.Indeterminate;
                else if (this.ListViewMonitors.CheckedItems.Count == 0)
                    this.checkBoxAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                if (this.ListViewMonitors.CheckedItems.Count == this.ListViewMonitors.Items.Count)
                {
                    this.checkBoxAll.CheckState = CheckState.Checked;
                    this.checkBoxAll.Checked = true;
                }
                else
                    this.checkBoxAll.CheckState = CheckState.Indeterminate;
            }


        }

        private void ListViewMonitors_ItemActivate(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            ListViewItem lvi = lv.SelectedItems[0];
            if (lvi.Tag != null)
            {
                if (lvi.Tag.GetType() == typeof(Monitor))
                {
                    Monitor mon = (Monitor)lvi.Tag;
                    this.propertyGridForm.SelectedObject = mon.Form;
                    this.labelSelectedObject.Text = mon.ID;
                    this.textBoxPath.Text = mon.Path;
                    this.comboBoxTypeName.Items.Clear();
                    this.comboBoxTypeName.Items.Add(mon.TypeName);
                    this.comboBoxTypeName.SelectedIndex = 0;
                }
            }
        }

        private void flatButtonClear_Click(object sender, EventArgs e)
        {
            this.Clear();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            // signal all client
            Socket socket;
            foreach (int port in serverClient.Clients.Keys)
            {
                socket = serverClient.Clients[port];
                socket.Send(ASCIIEncoding.ASCII.GetBytes("client.close"));
            }
            this.Save();
        }
    }
}