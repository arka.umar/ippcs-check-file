﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toyota.Sockets;
using Toyota.Cryptography;
using System.Configuration;
using System.Text.RegularExpressions;
using Toyota.Common.Web.Cryptography;

namespace Toyota.Monitoring.AppMonitorManager.OperationContracts
{
    public class RequestFilterAttribute : Attribute, IRequestFilter
    {

        private IBaseContract contract;

        public RequestFilterAttribute()
        { contract = new BaseContract(); }

        public bool OnRequesting(PacketEventArgs requestFilter)
        {
            //// filtering data
            //// 1. decrypt using rijndael
            //// 2. is contains "<request-andon>>"
            //// 3. encode using ibm850
            if (requestFilter.Data.Length == 0)
            { return false; }

            string dataFilter;
            byte[] decrypt;
            Crypter sym = new Crypter(new ConfigKeySym());
            decrypt = ASCIIEncoding.ASCII.GetBytes(sym.Decrypt(ASCIIEncoding.ASCII.GetString(requestFilter.Data)));
            dataFilter = Encoding.GetEncoding("ascii").GetString(decrypt);
            Match match = new Regex(@"<DCLReceivingDb").Match(dataFilter);
            if (match.Success)
            {
                requestFilter.Data = decrypt;
                return true;
            }
            else
                return false;
        }

    }
}
