﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Monitoring.AppMonitorManager.OperationContracts
{
    public interface IBaseContract
    {
        T Deserialize<T>(string e);
        string Reverse(string text);
    }
}
