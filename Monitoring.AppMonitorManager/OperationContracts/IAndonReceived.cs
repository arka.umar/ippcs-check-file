﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Toyota.Monitoring.AppMonitorManager.OperationContracts
{
    public interface IAndonReceived
    {
        void Received(object sender, object[] data);
    }
}
