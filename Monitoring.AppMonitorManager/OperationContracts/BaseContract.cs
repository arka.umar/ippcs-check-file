﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace Toyota.Monitoring.AppMonitorManager.OperationContracts
{
    public class BaseContract : IBaseContract
    {
        public T Deserialize<T>(string e)
        {
            e = e.Replace("\0", "");
            XmlSerializer des = new XmlSerializer(typeof(T));
            T ret;
            using (StringReader s = new StringReader(e))
            {
                XmlReader reader = XmlReader.Create(s);
                ret = (T)des.Deserialize(reader);
            }
            return ret;
        }

        public string Serialize<T>(object data)
        {
            XmlSerializer serial = new XmlSerializer(typeof(T));
            StringWriter streamW = new StringWriter();
            XmlWriter writer = XmlWriter.Create(streamW);
            serial.Serialize(writer, data);
            return serial.ToString();
        }

        public string Reverse(string text)
        {
            char[] cArray = text.ToCharArray();
            string reverse = String.Empty;
            for (int i = cArray.Length - 1; i > -1; i--)
            {
                reverse += cArray[i];
            }
            return reverse;
        }
    }
}
