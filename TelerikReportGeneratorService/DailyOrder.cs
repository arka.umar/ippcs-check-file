﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelerikReportGeneratorService
{
    class DailyOrder
    {
        public string OrderRelease;
        public DateTime OrderReleaseDate;
        public string SupplierCode;
        public string SupplierPlant;
        public string RcvPlantCode;
        public string PlantName;
        public string OrderType;
        public string SupplierName;
    }
}
