﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace TelerikReportGeneratorService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
                { 
                    new TelerikReportGenerator() 
                };
            ServiceBase.Run(ServicesToRun);

            //#if(!DEBUG)
            //            ServiceBase[] ServicesToRun;
            //            ServicesToRun = new ServiceBase[] 
            //                { 
            //                    new TelerikReportGenerator() 
            //                };
            //            ServiceBase.Run(ServicesToRun);

            //#else
            //TelerikReportGenerator svc = new TelerikReportGenerator();
            //svc.ProcessData("1");
            //            //svc.TesEmail();
            //#endif
        }
    }
}
