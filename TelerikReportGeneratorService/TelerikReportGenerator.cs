﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using Toyota.Common.Web.Database;
using Toyota.Common.Web.FTP;
using Ionic.Zip;
using System.IO;
using System.Net.Mail;
using Database;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

//new
using System.Drawing;
using System.Drawing.Imaging;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using ThoughtWorks.QRCode.Codec.Util;
//end

namespace TelerikReportGeneratorService
{
    public partial class TelerikReportGenerator : ServiceBase
    {
        public TelerikReportGenerator()
        {
            InitializeComponent();
        }

        public void TesEmail()
        {
            MailMessage mail = new MailMessage();
            string MailCC = "vandy.cahyadi@toyota.co.id;supplierportal8@toyota.co.id";
            string[] MailCC_Collection = null;
            MailCC_Collection = MailCC.Split(';');
            foreach (string mailCC in MailCC_Collection)
            {
                mail.CC.Add(mailCC);
            }
        }

        public void Benchmarking(string OrderRelease, string SupplierCode, string SupplierPlant, string RcvPlantCode,
                                 string ManifestNo)
        {
            string ProcessType = "Manifest Paralel";

            using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
            {
                conn.Open();
                using (SqlCommand Benchamarking = new SqlCommand(@"INSERT INTO TB_T_TELERIK_PROCESS
                                                                   VALUES(@EXECUTION_ID,
                                                                          @ORDER_RELEASE_DT,
                                                                          @SUPP_CD,
                                                                          @SUPP_PLANT_CD,
                                                                          @RCV_PLANT_CD,
                                                                          @MANIFEST_NO,
                                                                          GETDATE())", conn))
                {
                    Benchamarking.Parameters.Add(new SqlParameter("EXECUTION_ID", ProcessType));
                    Benchamarking.Parameters.Add(new SqlParameter("ORDER_RELEASE_DT", OrderRelease));
                    Benchamarking.Parameters.Add(new SqlParameter("SUPP_CD", SupplierCode));
                    Benchamarking.Parameters.Add(new SqlParameter("SUPP_PLANT_CD", SupplierPlant));
                    Benchamarking.Parameters.Add(new SqlParameter("RCV_PLANT_CD", RcvPlantCode));
                    Benchamarking.Parameters.Add(new SqlParameter("MANIFEST_NO", ManifestNo));

                    Benchamarking.ExecuteScalar();
                }

                conn.Close();
            }
        }

        public void ProcessData(string RcvPlantCode)
        {
            long ProcessID = 0;
            string FunctionID = "";
            int SeqNoLog = 1;
            int ProcStat = 0;
            string Location = "Generate PDF Report";
            string MessageLog = "";
            string FTPFolderRoot = "";
            string SubjectMail = "";
            string RcvPlantName = "";
            string BodyMail = "";
            string MailCC = "";
            StringBuilder sb = new StringBuilder();

            int ThreadOrder = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadOrder"]);
            int ThreadManifest = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadManifest"]);

            try
            {
                #region Sub Manifest
                FTPFolderRoot = "\\Portal\\SubManifest\\";
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();
                    try
                    {
                        List<Task> tasks = new List<Task>();
                        using (SqlCommand cmd2 = new SqlCommand(@" SELECT MANIFEST_NO FROM TB_R_DAILY_ORDER_MANIFEST WHERE 1=1 AND ISNULL(FTP_FLAG,0)=0 AND ORDER_TYPE = '3' GROUP BY MANIFEST_NO", conn))
                        {
                            SqlDataReader dr2;
                            dr2 = cmd2.ExecuteReader();
                            if (dr2.HasRows)
                            {
                                using (SqlCommand cmd11 = new SqlCommand("SELECT dbo.fn_get_last_process_id()", conn))
                                {
                                    ProcessID = Convert.ToInt64(cmd11.ExecuteScalar());
                                }
                                using (SqlCommand cmd_cc = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID='21003' AND SYSTEM_CD='EML_CC' ", conn))
                                {
                                    MailCC = cmd_cc.ExecuteScalar().ToString();
                                }
                                SeqNoLog = 1;

                                InsertLogHeader(conn, ProcessID, "21003", "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                                InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);

                                List<Manifest> lstManifest = new List<Manifest>();

                                while (dr2.Read())
                                {
                                    Manifest manifest = new Manifest();
                                    manifest.ManifestNo = dr2["MANIFEST_NO"].ToString();
                                    lstManifest.Add(manifest);
                                }
                                dr2.Close();

                                Parallel.ForEach<Manifest>(lstManifest,
                                    new ParallelOptions { MaxDegreeOfParallelism = ThreadManifest },
                                    currentElement =>
                                    {
                                        List<string> MailTo = new List<string>();
                                        using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                        {
                                            conn22.Open();
                                            try
                                            {
                                                DailyPrintSubManifest(currentElement.ManifestNo);

                                                MessageLog = "Sub Manifest " + currentElement.ManifestNo + " is successfully generated PDF report";
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                                bool SubManifestPdf = false;


                                                using (ZipFile zip = new ZipFile())
                                                {
                                                    if (Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentElement.ManifestNo))
                                                    {
                                                        zip.AddDirectory(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentElement.ManifestNo);
                                                        SubManifestPdf = true;
                                                    }
                                                    
                                                    if (SubManifestPdf)
                                                    {
                                                        zip.Save(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentElement.ManifestNo + ".zip");

                                                        MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated ZIP report";
                                                        InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                                    }
                                                    else
                                                    {
                                                        throw new Exception("Failed to Generate ZIP for " + currentElement.ManifestNo);
                                                    }
                                                }

                                                using (SqlCommand cmd22 = new SqlCommand("UPDATE TB_R_DAILY_ORDER_MANIFEST SET FTP_FLAG = '1' WHERE MANIFEST_NO = '" + currentElement.ManifestNo + "'", conn22))
                                                {
                                                    cmd22.ExecuteNonQuery();
                                                }
                                                /*
                                                string NotificationMail = "";
                                                NotificationMail = ConfigurationSettings.AppSettings["NotificationMail"];
                                                if (NotificationMail == "1")
                                                {
                                                    using (SqlCommand cmd22 = new SqlCommand("SELECT DISTINCT EMAIL FROM TB_M_SUPPLIER_CONTACT_PERSON with(nolock) WHERE AREA_CD = '2' AND POSITION_CD >= 6 AND SUPPLIER_CD='" + currentElement.SupplierCode + "'", conn22))
                                                    {
                                                        SqlDataReader dr3;
                                                        dr3 = cmd22.ExecuteReader();

                                                        if (dr3.HasRows)
                                                        {
                                                            MailTo = new List<string>();
                                                            while (dr3.Read())
                                                            {
                                                                if (dr3["EMAIL"].ToString().Trim() != "") MailTo.Add(dr3["EMAIL"].ToString());
                                                            }
                                                        }
                                                    }
                                                    if (MailTo.Count > 0)
                                                    {
                                                        SubjectMail = "[IPPCS] Emergency Order TMMIN " + currentElement.PlantName + " Notification";
                                                        sb = new StringBuilder();
                                                        sb.AppendLine("Dear Bapak / Ibu,");
                                                        sb.AppendLine("");
                                                        sb.AppendLine("Data emergency order " + currentElement.ManifestNo + @" untuk tanggal rilis " + currentElement.OrderRelease + @" sudah selesai diproses.");
                                                        sb.AppendLine("Silakan mengakses IPPCS pada menu “Order Inquiry” untuk mendownload data order, dan menu “Order Printing” untuk mencetak order dengan mengakses link di bawah ini:");
                                                        sb.AppendLine("https://portal.toyota.co.id/ippcs-app/DailyOrder?ManifestNos=" + currentElement.ManifestNo + "&TabActive=2");
                                                        sb.AppendLine("");
                                                        sb.AppendLine("Salam,");
                                                        sb.AppendLine("IPPCS Admin");

                                                        BodyMail = sb.ToString();
                                                        SendEmail(BodyMail, SubjectMail, MailTo, MailCC);
                                                    }
                                                }*/
                                            }
                                            catch (SmtpException exs)
                                            {
                                                InsertLogDetail(conn22, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                                            }
                                            catch (Exception ex)
                                            {
                                                ProcStat = 6;
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                                            }
                                        }
                                    });

                                using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                {
                                    conn22.Open();
                                    if (ProcStat == 0) ProcStat = 1;
                                    InsertLogDetail(conn22, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                                    UpdateStatusLog(conn22, ProcessID, ProcStat);
                                }
                            }
                        }
                    }
                    catch (SmtpException exs)
                    {
                        InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                    }
                    catch (Exception ex)
                    {
                        ProcStat = 6;
                        InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                        UpdateStatusLog(conn, ProcessID, ProcStat);
                    }
                }
                #endregion

                #region Regular Order
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();

                    // fid.intan
                    // add flag checking in TB_M_SYSTEM
                    string flag = "";
                    using (SqlCommand TelerixFlag = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE SYSTEM_CD LIKE '%_TELERIX_FLAG' AND SYSTEM_CD LIKE '%" + RcvPlantCode + "%'", conn))
                    {
                        flag = TelerixFlag.ExecuteScalar().ToString();
                    }

                    if (flag == "Y")
                    {
                        using (SqlCommand LastProcessID = new SqlCommand("select dbo.fn_get_last_process_id()", conn))
                        {
                            ProcessID = Convert.ToInt64(LastProcessID.ExecuteScalar());
                        }
                        using (SqlCommand EmailCC = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID='21003' AND SYSTEM_CD='EML_CC' ", conn))
                        {
                            MailCC = EmailCC.ExecuteScalar().ToString();
                        }
                        SeqNoLog = 1;

                        if (RcvPlantCode == "1") FunctionID = "21010";
                        else if (RcvPlantCode == "4") FunctionID = "21013";
                        else if (RcvPlantCode == "5") FunctionID = "21016";
                        else if (RcvPlantCode == "6") FunctionID = "21019";

                        InsertLogHeader(conn, ProcessID, FunctionID, "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                        InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);


                        List<DailyOrder> lstDailyOrder = new List<DailyOrder>();
                        
                        using (SqlCommand GetWebs = new SqlCommand(@"select 
                                                                    w.ORDER_RELEASE_DT,
                                                                    w.SUPPLIER_CD,
                                                                    w.SUPPLIER_PLANT_CD,
                                                                    w.RCV_PLANT_CD,
                                                                    p.PLANT_NM,
                                                                    w.ORDER_TYPE,
                                                                    s.SUPPLIER_NAME
                                                                FROM TB_R_DAILY_ORDER_WEB w with(nolock) 
                                                                    INNER JOIN TB_M_SUPPLIER s with(nolock) 
                                                                        on w.SUPPLIER_CD=s.SUPPLIER_CODE 
                                                                           AND w.SUPPLIER_PLANT_CD=s.SUPPLIER_PLANT_CD
                                                                    INNER JOIN TB_M_PLANT p 
                                                                        on w.RCV_PLANT_CD=p.PLANT_CD
                                                                WHERE w.FTP_FLAG='0' 
                                                                      AND w.RCV_PLANT_CD = @RCV_PLANT_CD
                                                                ORDER BY w.ORDER_RELEASE_DT", conn))
                        {
                            GetWebs.Parameters.Add(new SqlParameter("RCV_PLANT_CD", RcvPlantCode));

                            SqlDataReader WebDataReader;
                            WebDataReader = GetWebs.ExecuteReader();
                            while (WebDataReader.Read())
                            {
                                DailyOrder dailyOrder = new DailyOrder();
                                dailyOrder.OrderRelease = Convert.ToDateTime(WebDataReader["ORDER_RELEASE_DT"]).ToString("yyyyMMdd");
                                dailyOrder.OrderReleaseDate = Convert.ToDateTime(WebDataReader["ORDER_RELEASE_DT"]);
                                dailyOrder.SupplierCode = WebDataReader["SUPPLIER_CD"].ToString();
                                dailyOrder.SupplierPlant = WebDataReader["SUPPLIER_PLANT_CD"].ToString();
                                dailyOrder.RcvPlantCode = WebDataReader["RCV_PLANT_CD"].ToString();
                                dailyOrder.PlantName = WebDataReader["PLANT_NM"].ToString();
                                dailyOrder.OrderType = WebDataReader["ORDER_TYPE"].ToString();
                                dailyOrder.SupplierName = WebDataReader["SUPPLIER_NAME"].ToString().TrimEnd();
                                lstDailyOrder.Add(dailyOrder);
                            }
                            WebDataReader.Close();

                            if (lstDailyOrder.Count > 0)
                            {
                                //To Be Deleted
                            }

                            #region Master Validation
                            using (SqlCommand SupplierMasterValidate = new SqlCommand(@"SELECT 
                                                                            w.ORDER_RELEASE_DT,
                                                                            w.SUPPLIER_CD,
                                                                            w.SUPPLIER_PLANT_CD,
                                                                            w.ORDER_TYPE,
                                                                            s.SUPPLIER_NAME
                                                                        FROM TB_R_DAILY_ORDER_WEB w with(nolock) 
                                                                            LEFT JOIN TB_M_SUPPLIER s with(nolock) 
                                                                                on w.SUPPLIER_CD=s.SUPPLIER_CODE 
                                                                                   AND w.SUPPLIER_PLANT_CD=s.SUPPLIER_PLANT_CD
                                                                        WHERE w.FTP_FLAG='0' 
                                                                              AND w.RCV_PLANT_CD = @RCV_PLANT_CD
                                                                              AND s.SUPPLIER_CODE IS NULL
                                                                        ORDER BY w.ORDER_RELEASE_DT", conn))
                            {
                                SupplierMasterValidate.Parameters.Add(new SqlParameter("RCV_PLANT_CD", RcvPlantCode));

                                SqlDataReader SupplierMasterValidateReader;
                                SupplierMasterValidateReader = SupplierMasterValidate.ExecuteReader();
                                while (SupplierMasterValidateReader.Read())
                                {
                                    InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Supplier Code " + 
                                                        SupplierMasterValidateReader["SUPPLIER_CD"].ToString() + " and Supplier Plant " + 
                                                        SupplierMasterValidateReader["SUPPLIER_PLANT_CD"].ToString() + 
                                                        " is not exists in table master", Location, "Agent");
                                }

                                SupplierMasterValidateReader.Close();
                            }

                            using (SqlCommand PlantMasterValidation = new SqlCommand(@"SELECT 
                                                                            w.ORDER_RELEASE_DT,
                                                                            w.SUPPLIER_CD,
                                                                            w.SUPPLIER_PLANT_CD,
                                                                            w.RCV_PLANT_CD,
                                                                            w.ORDER_TYPE
                                                                        FROM TB_R_DAILY_ORDER_WEB w with(nolock) 
                                                                            LEFT JOIN TB_M_PLANT p 
                                                                                on w.RCV_PLANT_CD=p.PLANT_CD
                                                                        WHERE w.FTP_FLAG='0' 
                                                                              AND w.RCV_PLANT_CD = @RCV_PLANT_CD 
                                                                              AND p.PLANT_CD IS NULL
                                                                        ORDER BY w.ORDER_RELEASE_DT", conn))
                            {
                                PlantMasterValidation.Parameters.Add(new SqlParameter("RCV_PLANT_CD", RcvPlantCode));

                                SqlDataReader PlantMasterValidationReader;
                                PlantMasterValidationReader = PlantMasterValidation.ExecuteReader();
                                while (PlantMasterValidationReader.Read())
                                {
                                    InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Plant Code " + 
                                                        PlantMasterValidationReader["RCV_PLANT_CD"].ToString() + 
                                                        " is not exists in table master", Location, "Agent");
                                }
                                PlantMasterValidationReader.Close();
                            }
                            #endregion

                            List<Task> tasksOrder = new List<Task>();

                            Parallel.ForEach<DailyOrder>(lstDailyOrder,
                                new ParallelOptions { MaxDegreeOfParallelism = ThreadOrder },
                                currentOrder =>
                                //foreach (var currentOrder in lstDailyOrder)
                                {
                                string[] filePathManifestPrePrinteds = new string[] { };
                                string[] filePathManifestPlains = new string[] { };
                                string[] filePathSkids = new string[] { };
                                string[] filePathKanbans = new string[] { };
                                List<string> MailTo = new List<string>();

                                FTPFolderRoot = "\\Portal\\DailyOrderPrinting\\";
                                bool errorManifest = false;
                                string errorManifestNo = "";

                                try
                                {
                                    using (SqlCommand SelectManifests = new SqlCommand(@"SELECT 
                                                                                    m.MANIFEST_NO,
                                                                                    TOTAL_QTY=ISNULL(p.TOTAL_QTY,0),
                                                                                    m.RCV_PLANT_CD,
                                                                                    pt.PLANT_NM,
                                                                                    m.ORDER_TYPE
                                                                            FROM TB_R_DAILY_ORDER_MANIFEST m with(nolock)
                                                                                INNER JOIN TB_M_PLANT pt with(nolock) 
                                                                                    on m.RCV_PLANT_CD=pt.PLANT_CD
                                                                                INNER JOIN (
                                                                                   SELECT 
                                                                                       MANIFEST_NO,
                                                                                       TOTAL_QTY=SUM(ORDER_QTY)
                                                                                   FROM TB_R_DAILY_ORDER_PART with(nolock)
                                                                                   GROUP BY MANIFEST_NO
                                                                                 ) p on m.MANIFEST_NO=p.MANIFEST_NO
                                                                             WHERE m.FTP_FLAG='0' 
                                                                                   AND m.ORDER_TYPE='1'
                                                                                   AND CONVERT(VARCHAR(8),m.CREATED_DT,112) = @ORDER_RELEASE_DT
                                                                                   AND m.SUPPLIER_CD = @SUPP_CD
                                                                                   AND m.SUPPLIER_PLANT = @SUPP_PLANT_CD
                                                                                   AND m.RCV_PLANT_CD = @RCV_PLANT_CD", conn))
                                    {
                                        SelectManifests.Parameters.Add(new SqlParameter("ORDER_RELEASE_DT", currentOrder.OrderRelease));
                                        SelectManifests.Parameters.Add(new SqlParameter("SUPP_CD", currentOrder.SupplierCode));
                                        SelectManifests.Parameters.Add(new SqlParameter("SUPP_PLANT_CD", currentOrder.SupplierPlant));
                                        SelectManifests.Parameters.Add(new SqlParameter("RCV_PLANT_CD", currentOrder.RcvPlantCode));

                                        SqlDataReader ManifestReader;
                                        ManifestReader = SelectManifests.ExecuteReader();
                                        List<Manifest> lstManifest = new List<Manifest>();

                                        while (ManifestReader.Read())
                                        {
                                            Manifest manifest = new Manifest();
                                            manifest.ManifestNo = ManifestReader["MANIFEST_NO"].ToString();
                                            manifest.OrderType = ManifestReader["ORDER_TYPE"].ToString();
                                            manifest.PlantName = ManifestReader["PLANT_NM"].ToString();
                                            manifest.TotalQty = Convert.ToInt32(ManifestReader["TOTAL_QTY"]);
                                            lstManifest.Add(manifest);
                                        }
                                        ManifestReader.Close();

                                        Parallel.ForEach<Manifest>(lstManifest,
                                            new ParallelOptions { MaxDegreeOfParallelism = ThreadManifest },
                                            currentProcess =>
                                        //foreach (var currentProcess in lstManifest)
                                        {
                                            try
                                            {
                                                DailyOrderPrint(currentProcess.ManifestNo, 
                                                                currentOrder.OrderReleaseDate, 
                                                                currentOrder.SupplierCode, 
                                                                currentOrder.SupplierPlant, 
                                                                currentProcess.TotalQty.ToString(), 
                                                                currentProcess.OrderType, 
                                                                currentOrder.RcvPlantCode, conn);

                                                MessageLog = "Manifest No " + currentProcess.ManifestNo + " is successfully generated PDF report";
                                                InsertLogDetail(conn, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                                RcvPlantName = currentProcess.PlantName;

                                                using (SqlCommand ManifestFTPFlag = new SqlCommand(@"UPDATE TB_R_DAILY_ORDER_MANIFEST 
                                                                                                SET FTP_FLAG = '1' 
                                                                                           WHERE MANIFEST_NO = @MANIFEST_NO", conn))
                                                {
                                                    ManifestFTPFlag.Parameters.Add(new SqlParameter("MANIFEST_NO", currentProcess.ManifestNo));
                                                    ManifestFTPFlag.ExecuteNonQuery();
                                                }

                                                //Benchmarking(currentOrder.OrderRelease, currentOrder.SupplierCode, currentOrder.SupplierPlant,
                                                //             currentOrder.RcvPlantCode, currentProcess.ManifestNo);
                                            }
                                            //catch (SmtpException exs)
                                            //{
                                            //    InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", currentProcess.ManifestNo + "(" + exs.Message + ")", Location, "Agent");
                                            //}
                                            catch (Exception ex)
                                            {
                                                errorManifest = true;
                                                errorManifestNo = errorManifestNo + currentProcess.ManifestNo + ", ";
                                                ProcStat = 6;
                                                InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", currentProcess.ManifestNo + "(" + ex.Message + ")", Location, "Agent");
                                            }
                                        });
                                        //}
                                    }

                                    if (errorManifest)
                                    {
                                        throw new Exception("Error While Create PDF for Order Release DT for Manifest No " + errorManifestNo);
                                    }

                                    if (currentOrder.OrderType == "1")
                                    {
                                        if (Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\ManifestPrePrinted"))
                                        {
                                            filePathManifestPrePrinteds = Directory.GetFiles(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\ManifestPrePrinted");
                                        }
                                        if (Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\ManifestPlain"))
                                        {
                                            filePathManifestPlains = Directory.GetFiles(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\ManifestPlain");
                                        }
                                        if (Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\Skid"))
                                        {
                                            filePathSkids = Directory.GetFiles(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\Skid");
                                        }
                                        if (Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\Kanban"))
                                        {
                                            filePathKanbans = Directory.GetFiles(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + currentOrder.SupplierCode + "\\" + currentOrder.SupplierPlant + "\\" + currentOrder.RcvPlantCode + "\\Kanban");
                                        }

                                        //Added checking for zipped file by FID.Reggy
                                        bool ManifestPrePrintedExists = false;
                                        bool ManifestPlainExists = false;
                                        bool SkidExists = false;
                                        bool KanbanExists = false;

                                        using (ZipFile zip = new ZipFile())
                                        {
                                            foreach (string fileManifest in filePathManifestPrePrinteds)
                                            {
                                                zip.AddFile(fileManifest, "ManifestPrePrinted");
                                                ManifestPrePrintedExists = true;
                                            }
                                            foreach (string fileManifest in filePathManifestPlains)
                                            {
                                                zip.AddFile(fileManifest, "ManifestPlain");
                                                ManifestPlainExists = true;
                                            }
                                            foreach (string fileSkid in filePathSkids)
                                            {
                                                zip.AddFile(fileSkid, "Skid");
                                                SkidExists = true;
                                            }
                                            foreach (string fileKanban in filePathKanbans)
                                            {
                                                zip.AddFile(fileKanban, "Kanban");
                                                KanbanExists = true;
                                            }

                                            if (ManifestPrePrintedExists || ManifestPlainExists || SkidExists || KanbanExists)
                                            {
                                                zip.Save(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + 
                                                         currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "\\" + 
                                                         currentOrder.SupplierCode + "\\" + 
                                                         currentOrder.SupplierPlant + "\\" + 
                                                         currentOrder.RcvPlantCode + "\\" + 
                                                         currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "-" + 
                                                         currentOrder.SupplierPlant + currentOrder.SupplierCode + "-" + 
                                                         currentOrder.RcvPlantCode + ".zip");
                                            }
                                            else
                                            {
                                                throw new Exception("Failed to Generate ZIP for " + currentOrder.OrderReleaseDate.ToString("yyyyMMddhhmm") + "-" +
                                                                                                    currentOrder.SupplierPlant + "-" +
                                                                                                    currentOrder.SupplierCode + "-" +
                                                                                                    currentOrder.RcvPlantCode);
                                            }
                                        }

                                        MessageLog = "Order Release Date " + currentOrder.OrderRelease + "; Supplier Code " + currentOrder.SupplierCode + "; Supplier Plant " + currentOrder.SupplierPlant + "; Rcv Plant Code " + currentOrder.RcvPlantCode + " is successfully generated ZIP report";
                                        InsertLogDetail(conn, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                        using (SqlCommand WebFTPFlag = new SqlCommand(@"UPDATE TB_R_DAILY_ORDER_WEB SET FTP_FLAG = 1 WHERE NOT EXISTS(
                                                                                        SELECT 'x' FROM TB_R_DAILY_ORDER_MANIFEST 
			                                                                            WHERE FTP_FLAG = 0
				                                                                            AND SUPPLIER_CD = @SUPP_CD
                                                                                            AND SUPPLIER_PLANT = @SUPP_PLANT_CD 
                                                                                            AND CONVERT(VARCHAR(8),CREATED_DT,112) = @ORDER_RELEASE_DT 
                                                                                            AND RCV_PLANT_CD = @RCV_PLANT_CD) 
                                                                                     AND SUPPLIER_CD = @SUPP_CD
                                                                                     AND SUPPLIER_PLANT_CD = @SUPP_PLANT_CD 
                                                                                     AND CONVERT(VARCHAR(8),ORDER_RELEASE_DT,112) = @ORDER_RELEASE_DT 
                                                                                     AND RCV_PLANT_CD = @RCV_PLANT_CD;
                                                                                     IF(@@ROWCOUNT = 0)
                                                                                     BEGIN                                                                                 
					                                                                      RAISERROR('No FTP Flag Updated', 16, 1);
                                                                                     END;", conn))
                                        {
                                            WebFTPFlag.Parameters.Add(new SqlParameter("SUPP_CD", currentOrder.SupplierCode));
                                            WebFTPFlag.Parameters.Add(new SqlParameter("SUPP_PLANT_CD", currentOrder.SupplierPlant));
                                            WebFTPFlag.Parameters.Add(new SqlParameter("RCV_PLANT_CD", currentOrder.RcvPlantCode));
                                            WebFTPFlag.Parameters.Add(new SqlParameter("ORDER_RELEASE_DT", currentOrder.OrderRelease));

                                            WebFTPFlag.ExecuteNonQuery();
                                        }

                                        //Benchmarking(currentOrder.OrderRelease, currentOrder.SupplierCode, currentOrder.SupplierPlant,
                                        //                     currentOrder.RcvPlantCode, "DONE");

                                        #region Mail Release Notification
                                        string NotificationMail = "";
                                        NotificationMail = ConfigurationSettings.AppSettings["NotificationMail"];
                                        if (NotificationMail == "1")
                                        {
                                            using (SqlCommand MailRelease = new SqlCommand(@"SELECT DISTINCT EMAIL 
                                                                                       FROM TB_M_SUPPLIER_CONTACT_PERSON with(nolock) 
                                                                                       WHERE AREA_CD = '2' 
                                                                                             AND POSITION_CD >= 6 
                                                                                             AND SUPPLIER_CD='" + currentOrder.SupplierCode + "'", conn))
                                            {
                                                SqlDataReader MailReleaseReader;
                                                MailReleaseReader = MailRelease.ExecuteReader();

                                                if (MailReleaseReader.HasRows)
                                                {
                                                    MailTo = new List<string>();
                                                    while (MailReleaseReader.Read())
                                                    {
                                                        if (MailReleaseReader["EMAIL"].ToString().Trim() != "") MailTo.Add(MailReleaseReader["EMAIL"].ToString());
                                                    }
                                                }
                                            }

                                            if (MailTo.Count > 0)
                                            {
                                                SubjectMail = "[IPPCS] Daily Order TMMIN " + RcvPlantName + " Notification";
                                                sb = new StringBuilder();
                                                sb.AppendLine("Dear Bapak / Ibu,");
                                                sb.AppendLine("");
                                                sb.AppendLine("Data order " + currentOrder.SupplierCode + " – " + currentOrder.SupplierPlant + " " + currentOrder.SupplierName + " " + currentOrder.RcvPlantCode + @" untuk tanggal rilis " + currentOrder.OrderReleaseDate.ToString("dd/MM/yyyy") + @" sudah selesai diproses.");
                                                sb.AppendLine("Silakan mengakses IPPCS pada menu “Order Inquiry” untuk mendownload data order, dan menu “Order Printing” untuk mencetak order dengan mengakses link di bawah ini:");
                                                sb.AppendLine("https://portal.toyota.co.id");
                                                sb.AppendLine("");
                                                sb.AppendLine("Salam,");
                                                sb.AppendLine("IPPCS Admin");

                                                BodyMail = sb.ToString();
                                                SendEmail(BodyMail, SubjectMail, MailTo, MailCC);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                catch (SmtpException exs)
                                {
                                    InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", "Order Release Date " + 
                                                            currentOrder.OrderRelease + "; Supplier Code " + 
                                                            currentOrder.SupplierCode + "; Supplier Plant " + 
                                                            currentOrder.SupplierPlant + "; Rcv Plant Code " + 
                                                            currentOrder.RcvPlantCode + "(" + exs.Message + ")", Location, "Agent");
                                }
                                catch (Exception se)
                                {
                                    using (SqlCommand RollbackFTPFlag = new SqlCommand(@"UPDATE TB_R_DAILY_ORDER_WEB SET FTP_FLAG = 0 WHERE  
                                                                             SUPPLIER_CD = @SUPP_CD 
                                                                             AND SUPPLIER_PLANT_CD = @SUPP_PLANT_CD 
                                                                             AND CONVERT(VARCHAR(8),ORDER_RELEASE_DT,112) = @ORDER_RELEASE_DT 
                                                                             AND RCV_PLANT_CD = @RCV_PLANT_CD; 

                                                                            UPDATE TB_R_DAILY_ORDER_MANIFEST SET FTP_FLAG = 0 WHERE  
                                                                             SUPPLIER_CD = @SUPP_CD 
                                                                             AND SUPPLIER_PLANT = @SUPP_PLANT_CD 
                                                                             AND CONVERT(VARCHAR(8),CREATED_DT,112) = @ORDER_RELEASE_DT 
                                                                             AND RCV_PLANT_CD =  @RCV_PLANT_CD", conn))
                                    {
                                        RollbackFTPFlag.Parameters.Add(new SqlParameter("SUPP_CD", currentOrder.SupplierCode));
                                        RollbackFTPFlag.Parameters.Add(new SqlParameter("SUPP_PLANT_CD", currentOrder.SupplierPlant));
                                        RollbackFTPFlag.Parameters.Add(new SqlParameter("RCV_PLANT_CD", currentOrder.RcvPlantCode));
                                        RollbackFTPFlag.Parameters.Add(new SqlParameter("ORDER_RELEASE_DT", currentOrder.OrderRelease));

                                        RollbackFTPFlag.ExecuteNonQuery();
                                    }

                                    string exception = se.Message;
                                    if (se.InnerException != null)
                                    {
                                        exception = se.InnerException.Message;
                                    }

                                    ProcStat = 6;
                                    InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", "Order Release Date " + currentOrder.OrderRelease + 
                                                        "; Supplier Code " + currentOrder.SupplierCode + 
                                                        "; Supplier Plant " + currentOrder.SupplierPlant + 
                                                        "; Rcv Plant Code " + currentOrder.RcvPlantCode + 
                                                        "(" + exception + ")", Location, "Agent");
                                }
                            });
                            //}

                            //Added Update Hold Telerik in TB_M_SYSTEM by FID.Reggy
                            using (SqlCommand SystemUpdate = new SqlCommand(@"UPDATE TB_M_SYSTEM 
                                                                             SET 
                                                                                SYSTEM_VALUE = 'N', 
                                                                                CHANGED_BY = 'System', 
                                                                                CHANGED_DT = GETDATE()  
                                                                             WHERE SYSTEM_CD LIKE '%_TELERIX_FLAG' 
                                                                                   AND SYSTEM_CD LIKE '%" + RcvPlantCode + "%';", conn))
                            {
                                try
                                {
                                    SystemUpdate.Parameters.Add(new SqlParameter("RCV_PLANT_CD", RcvPlantCode));

                                    SystemUpdate.ExecuteScalar();
                                }
                                catch (Exception e)
                                {
                                    ProcStat = 6;
                                    InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", "Failed to Update Telerix Flag for RCV_PLANT_CD" + 
                                                                            RcvPlantCode + " : " + e.Message, "Update TB_M_SYSTEM", "Agent");
                                }
                            }

                            if (lstDailyOrder.Count > 0)
                            {
                                if (ProcStat == 0) ProcStat = 1;
                                InsertLogDetail(conn, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                                UpdateStatusLog(conn, ProcessID, ProcStat);
                            }
                        }
                        conn.Close();
                    }
                    else conn.Close();
                }
                #endregion

                #region Emergency Order
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();
                    try
                    {
                        FTPFolderRoot = "\\Portal\\EmergencyOrder\\";
                        List<Task> tasks = new List<Task>();
                        using (SqlCommand cmd2 = new SqlCommand(@"SELECT m.MANIFEST_NO, 
                                                                            m.SUPPLIER_CD,
                                                                            m.RCV_PLANT_CD,
                                                                            m.ORDER_RELEASE_DT,
                                                                            TOTAL_QTY=ISNULL(p.TOTAL_QTY,0),m.ORDER_TYPE,pt.PLANT_NM
                                                                    FROM TB_R_DAILY_ORDER_MANIFEST m with(nolock) 
                                                                    INNER JOIN TB_M_PLANT pt ON m.RCV_PLANT_CD=pt.PLANT_CD
                                                                    INNER JOIN (
                                                                        SELECT MANIFEST_NO,
                                                                            TOTAL_QTY=SUM(ORDER_QTY)
                                                                        FROM TB_R_DAILY_ORDER_PART with(nolock)
                                                                        GROUP BY MANIFEST_NO
                                                                    ) p ON m.MANIFEST_NO=p.MANIFEST_NO
                                                                    WHERE m.FTP_FLAG='0' AND m.RCV_PLANT_CD='" + RcvPlantCode + @"' AND (m.ORDER_TYPE='2' /*OR m.ORDER_TYPE = '3'*/) ORDER BY m.ORDER_RELEASE_DT", conn))
                        {
                            SqlDataReader dr2;
                            dr2 = cmd2.ExecuteReader();
                            if (dr2.HasRows)
                            {
                                using (SqlCommand cmd11 = new SqlCommand("select dbo.fn_get_last_process_id()", conn))
                                {
                                    ProcessID = Convert.ToInt64(cmd11.ExecuteScalar());
                                }
                                using (SqlCommand cmd_cc = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID='21003' AND SYSTEM_CD='EML_CC' ", conn))
                                {
                                    MailCC = cmd_cc.ExecuteScalar().ToString();
                                }
                                SeqNoLog = 1;

                                InsertLogHeader(conn, ProcessID, "21003", "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                                InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);

                                List<Manifest> lstManifest = new List<Manifest>();

                                while (dr2.Read())
                                {
                                    Manifest manifest = new Manifest();
                                    manifest.ManifestNo = dr2["MANIFEST_NO"].ToString();
                                    manifest.OrderType = dr2["ORDER_TYPE"].ToString();
                                    manifest.TotalQty = Convert.ToInt32(dr2["TOTAL_QTY"]);
                                    manifest.OrderRelease = Convert.ToDateTime(dr2["ORDER_RELEASE_DT"]).ToString("dd/MM/yyyy");
                                    manifest.OrderReleaseDate = Convert.ToDateTime(dr2["ORDER_RELEASE_DT"]);
                                    manifest.SupplierCode = dr2["SUPPLIER_CD"].ToString();
                                    manifest.RcvPlantCode = dr2["RCV_PLANT_CD"].ToString();
                                    manifest.PlantName = dr2["PLANT_NM"].ToString();
                                    lstManifest.Add(manifest);
                                }
                                dr2.Close();

                                Parallel.ForEach<Manifest>(lstManifest,
                                    new ParallelOptions { MaxDegreeOfParallelism = ThreadManifest },
                                    currentElement =>
                                {
                                    FTPFolderRoot = currentElement.OrderType == "3" ? "\\Portal\\ServicePartExport\\" : FTPFolderRoot;

                                    List<string> MailTo = new List<string>();
                                    using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                    {
                                        conn22.Open();
                                        try
                                        {
                                            //string tes = Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyy-MM-dd hh:mm:ss");
                                            DailyOrderPrint(currentElement.ManifestNo, DateTime.Now, "", "", currentElement.TotalQty.ToString(), currentElement.OrderType, currentElement.RcvPlantCode, conn22);

                                            MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated PDF report";
                                            InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                            //Added checking for zipped file by FID.Reggy
                                            bool ManifestPrePrintedExists = false;
                                            bool ManifestPlainExists = false;
                                            bool SkidExists = false;
                                            bool KanbanExists = false;

                                            using (ZipFile zip = new ZipFile())
                                            {
                                                if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf"))
                                                {
                                                    zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf", "ManifestPrePrinted");
                                                    ManifestPrePrintedExists = true;
                                                }
                                                if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf"))
                                                {
                                                    zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf", "ManifestPlain");
                                                    ManifestPlainExists = true;
                                                }
                                                if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf"))
                                                {
                                                    zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf", "Skid");
                                                    SkidExists = true;
                                                }
                                                if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf"))
                                                {
                                                    zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf", "Kanban");
                                                    KanbanExists = true;
                                                }

                                                if (ManifestPrePrintedExists || ManifestPlainExists || SkidExists || KanbanExists)
                                                {
                                                    zip.Save(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentElement.ManifestNo + ".zip");

                                                    MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated ZIP report";
                                                    InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                                }
                                                else
                                                {
                                                    throw new Exception("Failed to Generate ZIP for " + currentElement.ManifestNo);
                                                }
                                            }

                                            using (SqlCommand cmd22 = new SqlCommand("UPDATE TB_R_DAILY_ORDER_MANIFEST SET FTP_FLAG='1' WHERE MANIFEST_NO='" + currentElement.ManifestNo + "'", conn22))
                                            {
                                                cmd22.ExecuteNonQuery();
                                            }

                                            string NotificationMail = "";
                                            NotificationMail = ConfigurationSettings.AppSettings["NotificationMail"];
                                            if (NotificationMail == "1")
                                            {
                                                using (SqlCommand cmd22 = new SqlCommand("SELECT DISTINCT EMAIL FROM TB_M_SUPPLIER_CONTACT_PERSON with(nolock) WHERE AREA_CD = '2' AND POSITION_CD >= 6 AND SUPPLIER_CD='" + currentElement.SupplierCode + "'", conn22))
                                                {
                                                    SqlDataReader dr3;
                                                    dr3 = cmd22.ExecuteReader();

                                                    if (dr3.HasRows)
                                                    {
                                                        MailTo = new List<string>();
                                                        while (dr3.Read())
                                                        {
                                                            if (dr3["EMAIL"].ToString().Trim() != "") MailTo.Add(dr3["EMAIL"].ToString());
                                                        }
                                                    }
                                                }

                                                if (MailTo.Count > 0)
                                                {
                                                    SubjectMail = "[IPPCS] Emergency Order TMMIN " + currentElement.PlantName + " Notification";
                                                    sb = new StringBuilder();
                                                    sb.AppendLine("Dear Bapak / Ibu,");
                                                    sb.AppendLine("");
                                                    sb.AppendLine("Data emergency order " + currentElement.ManifestNo + @" untuk tanggal rilis " + currentElement.OrderRelease + @" sudah selesai diproses.");
                                                    sb.AppendLine("Silakan mengakses IPPCS pada menu “Order Inquiry” untuk mendownload data order, dan menu “Order Printing” untuk mencetak order dengan mengakses link di bawah ini:");
                                                    sb.AppendLine("https://portal.toyota.co.id/ippcs-app/DailyOrder?ManifestNos=" + currentElement.ManifestNo + "&TabActive=2");
                                                    sb.AppendLine("");
                                                    sb.AppendLine("Salam,");
                                                    sb.AppendLine("IPPCS Admin");

                                                    BodyMail = sb.ToString();
                                                    SendEmail(BodyMail, SubjectMail, MailTo, MailCC);
                                                }
                                            }
                                        }
                                        catch (SmtpException exs)
                                        {
                                            InsertLogDetail(conn22, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                                        }
                                        catch (Exception ex)
                                        {
                                            ProcStat = 6;
                                            InsertLogDetail(conn22, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                                        }
                                    }
                                });

                                using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                {
                                    conn22.Open();
                                    if (ProcStat == 0) ProcStat = 1;
                                    InsertLogDetail(conn22, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                                    UpdateStatusLog(conn22, ProcessID, ProcStat);
                                }
                            }
                        }
                    }
                    catch (SmtpException exs)
                    {
                        InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                    }
                    catch (Exception ex)
                    {
                        ProcStat = 6;
                        InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                        UpdateStatusLog(conn, ProcessID, ProcStat);
                    }
                }
                #endregion

                #region Problem Part
                using (SqlConnection conn2 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn2.Open();
                    FTPFolderRoot = "\\Portal\\ProblemPartOrder\\";
                    using (SqlCommand cmd3 = new SqlCommand(@"SELECT m.MANIFEST_NO,
                                                                    m.SUPPLIER_CD,
                                                                    m.RCV_PLANT_CD,
                                                                    m.ORDER_RELEASE_DT,
                                                                    TOTAL_QTY=ISNULL(p.TOTAL_QTY,0),
                                                                    m.ORDER_TYPE,pt.PLANT_NM
                                                                FROM TB_R_DAILY_ORDER_MANIFEST m with(nolock) 
                                                                INNER JOIN TB_M_PLANT pt ON m.RCV_PLANT_CD=pt.PLANT_CD
                                                                INNER JOIN (
                                                                    SELECT MANIFEST_NO,
                                                                        TOTAL_QTY=SUM(ORDER_QTY)
                                                                    FROM TB_R_DAILY_ORDER_PART with(nolock)
                                                                    GROUP BY MANIFEST_NO
                                                                ) p ON m.MANIFEST_NO=p.MANIFEST_NO 
                                                                WHERE FTP_FLAG='0' AND m.RCV_PLANT_CD='" + RcvPlantCode + @"' AND PROBLEM_FLAG='1' ORDER BY ORDER_RELEASE_DT", conn2))
                    {
                        SqlDataReader dr3;
                        dr3 = cmd3.ExecuteReader();
                        if (dr3.HasRows)
                        {
                            using (SqlCommand cmd11 = new SqlCommand("select dbo.fn_get_last_process_id()", conn2))
                            {
                                ProcessID = Convert.ToInt64(cmd11.ExecuteScalar());
                            }
                            using (SqlCommand cmd_cc = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID='21003' AND SYSTEM_CD='EML_CC' ", conn2))
                            {
                                MailCC = cmd_cc.ExecuteScalar().ToString();
                            }
                            SeqNoLog = 1;

                            InsertLogHeader(conn2, ProcessID, "21003", "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                            InsertLogDetail(conn2, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);

                            List<Manifest> lstManifest = new List<Manifest>();
                            while (dr3.Read())
                            {
                                Manifest manifest = new Manifest();
                                manifest.ManifestNo = dr3["MANIFEST_NO"].ToString();
                                manifest.OrderType = dr3["ORDER_TYPE"].ToString();
                                manifest.TotalQty = Convert.ToInt32(dr3["TOTAL_QTY"]);
                                manifest.OrderRelease = Convert.ToDateTime(dr3["ORDER_RELEASE_DT"]).ToString("dd/MM/yyyy");
                                manifest.OrderReleaseDate = Convert.ToDateTime(dr3["ORDER_RELEASE_DT"]);
                                manifest.SupplierCode = dr3["SUPPLIER_CD"].ToString();
                                manifest.RcvPlantCode = dr3["RCV_PLANT_CD"].ToString();
                                manifest.PlantName = dr3["PLANT_NM"].ToString();
                                lstManifest.Add(manifest);
                            }
                            dr3.Close();

                            Parallel.ForEach<Manifest>(lstManifest,
                                    new ParallelOptions { MaxDegreeOfParallelism = ThreadManifest },
                                    currentElement =>
                                    {
                                        using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                        {
                                            conn22.Open();

                                            try
                                            {
                                                DailyOrderPrintProblem(currentElement.ManifestNo, currentElement.TotalQty.ToString());
                                                MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated PDF report";
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                                using (ZipFile zip = new ZipFile())
                                                {
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf", "ManifestPrePrinted");
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf", "ManifestPlain");
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf", "Skid");
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf", "Kanban");
                                                    }
                                                    zip.Save(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentElement.ManifestNo + ".zip");
                                                    //zip.Dispose();
                                                    MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated ZIP report";
                                                    InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                                }

                                                using (SqlCommand cmd33 = new SqlCommand("UPDATE TB_R_DAILY_ORDER_MANIFEST SET FTP_FLAG='1' WHERE MANIFEST_NO='" + currentElement.ManifestNo + "'", conn22))
                                                {
                                                    cmd33.ExecuteNonQuery();
                                                }
                                            }
                                            catch (SmtpException exs)
                                            {
                                                InsertLogDetail(conn22, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                                            }
                                            catch (Exception ex)
                                            {
                                                ProcStat = 6;
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                                            }
                                        }
                                    });
                            using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                            {
                                conn22.Open();
                                if (ProcStat == 0) ProcStat = 1;
                                InsertLogDetail(conn22, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                                UpdateStatusLog(conn22, ProcessID, ProcStat);
                            }
                        }
                    }
                }
                #endregion


				#region SPEX Order
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();
                    try
                    {
                        FTPFolderRoot = "\\Portal\\ServicePartExport\\";
                        List<Task> tasks = new List<Task>();
                        using (SqlCommand cmd2 = new SqlCommand(@"SELECT m.MANIFEST_NO
	                                                                  ,m.SUPPLIER_CD
	                                                                  ,m.RCV_PLANT_CD
	                                                                  ,m.ORDER_RELEASE_DT
	                                                                  ,TOTAL_QTY = ISNULL(p.TOTAL_QTY, 0)
	                                                                  ,m.ORDER_TYPE
	                                                                  ,pt.PLANT_NM
                                                                  FROM TB_R_PRA_DAILY_ORDER_MANIFEST m WITH (NOLOCK)
                                                                  INNER JOIN TB_M_PLANT pt ON m.RCV_PLANT_CD = pt.PLANT_CD
                                                                  INNER JOIN (
	                                                                  SELECT MANIFEST_NO
		                                                                  ,TOTAL_QTY = SUM(ORDER_QTY)
	                                                                  FROM TB_R_PRA_DAILY_ORDER_PART WITH (NOLOCK)
	                                                                  GROUP BY MANIFEST_NO
	                                                                  ) p ON m.MANIFEST_NO = p.MANIFEST_NO
                                                                  WHERE m.FTP_FLAG = '0'
	                                                                  AND m.RCV_PLANT_CD = '" + RcvPlantCode + @"'
	                                                                  AND m.ORDER_TYPE = '3'
                                                                  ORDER BY m.ORDER_RELEASE_DT", conn))
                        {
                            SqlDataReader dr2;
                            dr2 = cmd2.ExecuteReader();
                            if (dr2.HasRows)
                            {
                                using (SqlCommand cmd11 = new SqlCommand("SELECT dbo.fn_get_last_process_id()", conn))
                                {
                                    ProcessID = Convert.ToInt64(cmd11.ExecuteScalar());
                                }
                                using (SqlCommand cmd_cc = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID='21003' AND SYSTEM_CD='EML_CC' ", conn))
                                {
                                    MailCC = cmd_cc.ExecuteScalar().ToString();
                                }
                                SeqNoLog = 1;

                                InsertLogHeader(conn, ProcessID, "21003", "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                                InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);

                                List<Manifest> lstManifest = new List<Manifest>();

                                while (dr2.Read())
                                {
                                    Manifest manifest = new Manifest();
                                    manifest.ManifestNo = dr2["MANIFEST_NO"].ToString();
                                    manifest.OrderType = dr2["ORDER_TYPE"].ToString();
                                    manifest.TotalQty = Convert.ToInt32(dr2["TOTAL_QTY"]);
                                    manifest.OrderRelease = Convert.ToDateTime(dr2["ORDER_RELEASE_DT"]).ToString("dd/MM/yyyy");
                                    manifest.OrderReleaseDate = Convert.ToDateTime(dr2["ORDER_RELEASE_DT"]);
                                    manifest.SupplierCode = dr2["SUPPLIER_CD"].ToString();
                                    manifest.RcvPlantCode = dr2["RCV_PLANT_CD"].ToString();
                                    manifest.PlantName = dr2["PLANT_NM"].ToString();
                                    lstManifest.Add(manifest);
                                }
                                dr2.Close();

                                Parallel.ForEach<Manifest>(lstManifest,
                                    new ParallelOptions { MaxDegreeOfParallelism = ThreadManifest },
                                    currentElement =>
                                    {
                                        List<string> MailTo = new List<string>();
                                        using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                        {
                                            conn22.Open();
                                            try
                                            {
                                                DailyOrderSPEXPrint(currentElement.ManifestNo, DateTime.Now, "", "", currentElement.TotalQty.ToString(), currentElement.OrderType, currentElement.RcvPlantCode, conn22);

                                                MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated PDF report";
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                                bool ManifestPrePrintedExists = false;
                                                bool ManifestPlainExists = false;
                                                bool SkidExists = false;
                                                bool KanbanExists = false;

                                                using (ZipFile zip = new ZipFile())
                                                {
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf", "ManifestPrePrinted");
                                                        ManifestPrePrintedExists = true;
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf", "ManifestPlain");
                                                        ManifestPlainExists = true;
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf", "Skid");
                                                        SkidExists = true;
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf", "Kanban");
                                                        KanbanExists = true;
                                                    }

                                                    if (ManifestPrePrintedExists || ManifestPlainExists || SkidExists || KanbanExists)
                                                    {
                                                        zip.Save(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentElement.ManifestNo + ".zip");

                                                        MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated ZIP report";
                                                        InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                                    }
                                                    else
                                                    {
                                                        throw new Exception("Failed to Generate ZIP for " + currentElement.ManifestNo);
                                                    }
                                                }

                                                using (SqlCommand cmd22 = new SqlCommand("UPDATE TB_R_PRA_DAILY_ORDER_MANIFEST SET FTP_FLAG='1' WHERE MANIFEST_NO='" + currentElement.ManifestNo + "'", conn22))
                                                {
                                                    cmd22.ExecuteNonQuery();
                                                }

                                                //string NotificationMail = "";
                                                //NotificationMail = ConfigurationSettings.AppSettings["NotificationMail"];
                                                //if (NotificationMail == "1")
                                                //{
                                                //    using (SqlCommand cmd22 = new SqlCommand("SELECT DISTINCT EMAIL FROM TB_M_SUPPLIER_CONTACT_PERSON with(nolock) WHERE AREA_CD = '2' AND POSITION_CD >= 6 AND SUPPLIER_CD='" + currentElement.SupplierCode + "'", conn22))
                                                //    {
                                                //        SqlDataReader dr3;
                                                //        dr3 = cmd22.ExecuteReader();

                                                //        if (dr3.HasRows)
                                                //        {
                                                //            MailTo = new List<string>();
                                                //            while (dr3.Read())
                                                //            {
                                                //                if (dr3["EMAIL"].ToString().Trim() != "") MailTo.Add(dr3["EMAIL"].ToString());
                                                //            }
                                                //        }
                                                //    }

                                                //    if (MailTo.Count > 0)
                                                //    {
                                                //        SubjectMail = "[IPPCS] Emergency Order TMMIN " + currentElement.PlantName + " Notification";
                                                //        sb = new StringBuilder();
                                                //        sb.AppendLine("Dear Bapak / Ibu,");
                                                //        sb.AppendLine("");
                                                //        sb.AppendLine("Data emergency order " + currentElement.ManifestNo + @" untuk tanggal rilis " + currentElement.OrderRelease + @" sudah selesai diproses.");
                                                //        sb.AppendLine("Silakan mengakses IPPCS pada menu “Order Inquiry” untuk mendownload data order, dan menu “Order Printing” untuk mencetak order dengan mengakses link di bawah ini:");
                                                //        sb.AppendLine("https://portal.toyota.co.id/ippcs-app/DailyOrder?ManifestNos=" + currentElement.ManifestNo + "&TabActive=2");
                                                //        sb.AppendLine("");
                                                //        sb.AppendLine("Salam,");
                                                //        sb.AppendLine("IPPCS Admin");

                                                //        BodyMail = sb.ToString();
                                                //        SendEmail(BodyMail, SubjectMail, MailTo, MailCC);
                                                //    }
                                                //}
                                            }
                                            catch (SmtpException exs)
                                            {
                                                InsertLogDetail(conn22, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                                            }
                                            catch (Exception ex)
                                            {
                                                ProcStat = 6;
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                                            }
                                        }
                                    });

                                using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                {
                                    conn22.Open();
                                    if (ProcStat == 0) ProcStat = 1;
                                    InsertLogDetail(conn22, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                                    UpdateStatusLog(conn22, ProcessID, ProcStat);
                                }
                            }
                        }
                    }
                    catch (SmtpException exs)
                    {
                        InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                    }
                    catch (Exception ex)
                    {
                        ProcStat = 6;
                        InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                        UpdateStatusLog(conn, ProcessID, ProcStat);
                    }
                }
                #endregion

                #region SPEX Problem Report Done
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();
                    try
                    {
                        List<Task> tasks = new List<Task>();
                        using (SqlCommand cmd2 = new SqlCommand(@"SELECT PR_CD FROM TB_R_SPEX_PROBLEM_REPORT WHERE ISNULL(PRINT_FLAG, '0') = '0' GROUP BY PR_CD", conn))
                        {
                            SqlDataReader dr2;
                            dr2 = cmd2.ExecuteReader();
                            if (dr2.HasRows)
                            {
                                using (SqlCommand cmd11 = new SqlCommand("SELECT dbo.fn_get_last_process_id()", conn))
                                {
                                    ProcessID = Convert.ToInt64(cmd11.ExecuteScalar());
                                }
                                using (SqlCommand cmd_cc = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID='21003' AND SYSTEM_CD='EML_CC' ", conn))
                                {
                                    MailCC = cmd_cc.ExecuteScalar().ToString();
                                }
                                SeqNoLog = 1;

                                InsertLogHeader(conn, ProcessID, "21003", "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                                InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);

                                List<Manifest> lstManifest = new List<Manifest>();

                                while (dr2.Read())
                                {
                                    Manifest manifest = new Manifest();
                                    manifest.ManifestNo = dr2["PR_CD"].ToString();
                                    lstManifest.Add(manifest);
                                }
                                dr2.Close();

                                Parallel.ForEach<Manifest>(lstManifest,
                                    new ParallelOptions { MaxDegreeOfParallelism = ThreadManifest },
                                    currentElement =>
                                    {
                                        List<string> MailTo = new List<string>();
                                        using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                        {
                                            conn22.Open();
                                            try
                                            {
                                                DailyProblemPrint(currentElement.ManifestNo);

                                                MessageLog = "PR Code " + currentElement.ManifestNo + " is successfully generated PDF report";
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                                using (SqlCommand cmd22 = new SqlCommand("UPDATE TB_R_SPEX_PROBLEM_REPORT SET PRINT_FLAG = '1' WHERE PR_CD = '" + currentElement.ManifestNo + "'", conn22))
                                                {
                                                    cmd22.ExecuteNonQuery();
                                                }

                                                //string NotificationMail = "";
                                                //NotificationMail = ConfigurationSettings.AppSettings["NotificationMail"];
                                                //if (NotificationMail == "1")
                                                //{
                                                //    using (SqlCommand cmd22 = new SqlCommand("SELECT DISTINCT EMAIL FROM TB_M_SUPPLIER_CONTACT_PERSON with(nolock) WHERE AREA_CD = '2' AND POSITION_CD >= 6 AND SUPPLIER_CD='" + currentElement.SupplierCode + "'", conn22))
                                                //    {
                                                //        SqlDataReader dr3;
                                                //        dr3 = cmd22.ExecuteReader();

                                                //        if (dr3.HasRows)
                                                //        {
                                                //            MailTo = new List<string>();
                                                //            while (dr3.Read())
                                                //            {
                                                //                if (dr3["EMAIL"].ToString().Trim() != "") MailTo.Add(dr3["EMAIL"].ToString());
                                                //            }
                                                //        }
                                                //    }

                                                //    if (MailTo.Count > 0)
                                                //    {
                                                //        SubjectMail = "[IPPCS] Emergency Order TMMIN " + currentElement.PlantName + " Notification";
                                                //        sb = new StringBuilder();
                                                //        sb.AppendLine("Dear Bapak / Ibu,");
                                                //        sb.AppendLine("");
                                                //        sb.AppendLine("Data emergency order " + currentElement.ManifestNo + @" untuk tanggal rilis " + currentElement.OrderRelease + @" sudah selesai diproses.");
                                                //        sb.AppendLine("Silakan mengakses IPPCS pada menu “Order Inquiry” untuk mendownload data order, dan menu “Order Printing” untuk mencetak order dengan mengakses link di bawah ini:");
                                                //        sb.AppendLine("https://portal.toyota.co.id/ippcs-app/DailyOrder?ManifestNos=" + currentElement.ManifestNo + "&TabActive=2");
                                                //        sb.AppendLine("");
                                                //        sb.AppendLine("Salam,");
                                                //        sb.AppendLine("IPPCS Admin");

                                                //        BodyMail = sb.ToString();
                                                //        SendEmail(BodyMail, SubjectMail, MailTo, MailCC);
                                                //    }
                                                //}
                                            }
                                            catch (SmtpException exs)
                                            {
                                                InsertLogDetail(conn22, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                                            }
                                            catch (Exception ex)
                                            {
                                                ProcStat = 6;
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                                            }
                                        }
                                    });

                                using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                {
                                    conn22.Open();
                                    if (ProcStat == 0) ProcStat = 1;
                                    InsertLogDetail(conn22, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                                    UpdateStatusLog(conn22, ProcessID, ProcStat);
                                }
                            }
                        }
                    }
                    catch (SmtpException exs)
                    {
                        InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                    }
                    catch (Exception ex)
                    {
                        ProcStat = 6;
                        InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                        UpdateStatusLog(conn, ProcessID, ProcStat);
                    }
                }
                #endregion
				
                //Component Part Order (CPO)
                #region Component Part Order (CPO)
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();
                    try
                    {
                        FTPFolderRoot = "\\Portal\\ComponentPartOrder\\";
                        List<Task> tasks = new List<Task>();
                        using (SqlCommand cmd2 = new SqlCommand(@"SELECT m.MANIFEST_NO, 
                                                                            m.SUPPLIER_CD,
                                                                            m.RCV_PLANT_CD,
                                                                            m.ORDER_RELEASE_DT,
                                                                            TOTAL_QTY=ISNULL(p.TOTAL_QTY,0),m.ORDER_TYPE,pt.PLANT_NM
                                                                    FROM TB_R_DAILY_ORDER_MANIFEST m with(nolock) 
                                                                    INNER JOIN TB_M_PLANT pt ON m.RCV_PLANT_CD=pt.PLANT_CD
                                                                    INNER JOIN (
                                                                        SELECT MANIFEST_NO,
                                                                            TOTAL_QTY=SUM(ORDER_QTY)
                                                                        FROM TB_R_DAILY_ORDER_PART with(nolock)
                                                                        GROUP BY MANIFEST_NO
                                                                    ) p ON m.MANIFEST_NO=p.MANIFEST_NO
                                                                    WHERE m.FTP_FLAG='0' AND m.RCV_PLANT_CD='" + RcvPlantCode + @"' AND m.ORDER_TYPE='6' ORDER BY m.ORDER_RELEASE_DT", conn))
                        {
                            SqlDataReader dr2;
                            dr2 = cmd2.ExecuteReader();
                            if (dr2.HasRows)
                            {
                                using (SqlCommand cmd11 = new SqlCommand("select dbo.fn_get_last_process_id()", conn))
                                {
                                    ProcessID = Convert.ToInt64(cmd11.ExecuteScalar());
                                }
                                using (SqlCommand cmd_cc = new SqlCommand("SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID='21003' AND SYSTEM_CD='EML_CC' ", conn))
                                {
                                    MailCC = cmd_cc.ExecuteScalar().ToString();
                                }
                                SeqNoLog = 1;

                                InsertLogHeader(conn, ProcessID, "21003", "2", DateTime.Now, DateTime.Now, 5, "AGENT");
                                InsertLogDetail(conn, ProcessID, "MPCS00002INF", "INF", "Process generate PDF report is started", Location, "Agent", SeqNoLog);

                                List<Manifest> lstManifest = new List<Manifest>();

                                while (dr2.Read())
                                {
                                    Manifest manifest = new Manifest();
                                    manifest.ManifestNo = dr2["MANIFEST_NO"].ToString();
                                    manifest.OrderType = dr2["ORDER_TYPE"].ToString();
                                    manifest.TotalQty = Convert.ToInt32(dr2["TOTAL_QTY"]);
                                    manifest.OrderRelease = Convert.ToDateTime(dr2["ORDER_RELEASE_DT"]).ToString("dd/MM/yyyy");
                                    manifest.OrderReleaseDate = Convert.ToDateTime(dr2["ORDER_RELEASE_DT"]);
                                    manifest.SupplierCode = dr2["SUPPLIER_CD"].ToString();
                                    manifest.RcvPlantCode = dr2["RCV_PLANT_CD"].ToString();
                                    manifest.PlantName = dr2["PLANT_NM"].ToString();
                                    lstManifest.Add(manifest);
                                }
                                dr2.Close();

                                Parallel.ForEach<Manifest>(lstManifest,
                                    new ParallelOptions { MaxDegreeOfParallelism = ThreadManifest },
                                    currentElement =>
                                    {
                                        FTPFolderRoot = currentElement.OrderType == "6" ? "\\Portal\\ComponentPartOrder\\" : FTPFolderRoot;

                                        List<string> MailTo = new List<string>();
                                        using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                        {
                                            conn22.Open();
                                            try
                                            {
                                                //string tes = Convert.ToDateTime(dr["ORDER_RELEASE_DT"]).ToString("yyyy-MM-dd hh:mm:ss");
                                                DailyOrderPrint(currentElement.ManifestNo, DateTime.Now, "", "", currentElement.TotalQty.ToString(), currentElement.OrderType, currentElement.RcvPlantCode, conn22);

                                                MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated PDF report";
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");

                                                //Added checking for zipped file by FID.Reggy
                                                bool ManifestPrePrintedExists = false;
                                                bool ManifestPlainExists = false;
                                                bool SkidExists = false;
                                                bool KanbanExists = false;

                                                using (ZipFile zip = new ZipFile())
                                                {
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPrePrinted\\" + currentElement.ManifestNo + "-1-1.pdf", "ManifestPrePrinted");
                                                        ManifestPrePrintedExists = true;
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "ManifestPlain\\" + currentElement.ManifestNo + "-0-1.pdf", "ManifestPlain");
                                                        ManifestPlainExists = true;
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Skid\\" + currentElement.ManifestNo + "-3.pdf", "Skid");
                                                        SkidExists = true;
                                                    }
                                                    if (File.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf"))
                                                    {
                                                        zip.AddFile(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + "Kanban\\" + currentElement.ManifestNo + "-2.pdf", "Kanban");
                                                        KanbanExists = true;
                                                    }

                                                    if (ManifestPrePrintedExists || ManifestPlainExists || SkidExists || KanbanExists)
                                                    {
                                                        zip.Save(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + currentElement.ManifestNo + ".zip");

                                                        MessageLog = "Manifest No " + currentElement.ManifestNo + " is successfully generated ZIP report";
                                                        InsertLogDetail(conn22, ProcessID, "MPCS00005INF", "INF", MessageLog, Location, "Agent");
                                                    }
                                                    else
                                                    {
                                                        throw new Exception("Failed to Generate ZIP for " + currentElement.ManifestNo);
                                                    }
                                                }

                                                using (SqlCommand cmd22 = new SqlCommand("UPDATE TB_R_DAILY_ORDER_MANIFEST SET FTP_FLAG='1' WHERE MANIFEST_NO='" + currentElement.ManifestNo + "'", conn22))
                                                {
                                                    cmd22.ExecuteNonQuery();
                                                }

                                                string NotificationMail = "";
                                                NotificationMail = ConfigurationSettings.AppSettings["NotificationMail"];
                                                if (NotificationMail == "1")
                                                {
                                                    using (SqlCommand cmd22 = new SqlCommand("SELECT DISTINCT EMAIL FROM TB_M_SUPPLIER_CONTACT_PERSON with(nolock) WHERE AREA_CD = '2' AND POSITION_CD >= 6 AND SUPPLIER_CD='" + currentElement.SupplierCode + "'", conn22))
                                                    {
                                                        SqlDataReader dr3;
                                                        dr3 = cmd22.ExecuteReader();

                                                        if (dr3.HasRows)
                                                        {
                                                            MailTo = new List<string>();
                                                            while (dr3.Read())
                                                            {
                                                                if (dr3["EMAIL"].ToString().Trim() != "") MailTo.Add(dr3["EMAIL"].ToString());
                                                            }
                                                        }
                                                    }

                                                    if (MailTo.Count > 0)
                                                    {
                                                        SubjectMail = "[IPPCS] Component Part Order TMMIN " + currentElement.PlantName + " Notification";
                                                        sb = new StringBuilder();
                                                        sb.AppendLine("Dear Bapak / Ibu,");
                                                        sb.AppendLine("");
                                                        sb.AppendLine("Data Component Part Order " + currentElement.ManifestNo + @" untuk tanggal rilis " + currentElement.OrderRelease + @" sudah selesai diproses.");
                                                        sb.AppendLine("Silakan mengakses IPPCS pada menu “Daily Order CPO” untuk mendownload data order, dan menu “Daily Order Printing CPO” untuk mencetak order dengan mengakses link di bawah ini:");
                                                        sb.AppendLine("https://portal.toyota.co.id/ippcs-app/DailyOrderCPO?ManifestNos=" + currentElement.ManifestNo);
                                                        sb.AppendLine("");
                                                        sb.AppendLine("Salam,");
                                                        sb.AppendLine("IPPCS Admin");

                                                        BodyMail = sb.ToString();
                                                        SendEmail(BodyMail, SubjectMail, MailTo, MailCC);
                                                    }
                                                }
                                            }
                                            catch (SmtpException exs)
                                            {
                                                InsertLogDetail(conn22, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                                            }
                                            catch (Exception ex)
                                            {
                                                ProcStat = 6;
                                                InsertLogDetail(conn22, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                                            }
                                        }
                                    });

                                using (SqlConnection conn22 = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                                {
                                    conn22.Open();
                                    if (ProcStat == 0) ProcStat = 1;
                                    InsertLogDetail(conn22, ProcessID, "MPCS00003INF", "INF", "Process generate PDF report is finished", Location, "Agent");
                                    UpdateStatusLog(conn22, ProcessID, ProcStat);
                                }
                            }
                        }
                    }
                    catch (SmtpException exs)
                    {
                        InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                    }
                    catch (Exception ex)
                    {
                        ProcStat = 6;
                        InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                        UpdateStatusLog(conn, ProcessID, ProcStat);
                    }
                }
                #endregion
            }
            catch (SmtpException exs)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();
                    InsertLogDetail(conn, ProcessID, "MPCS00001WRN", "WRN", exs.Message, Location, "Agent");
                }
            }
            catch (Exception ex)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
                {
                    conn.Open();

                    ProcStat = 6;
                    InsertLogDetail(conn, ProcessID, "MPCS00005ERR", "ERR", ex.Message, Location, "Agent");
                    UpdateStatusLog(conn, ProcessID, ProcStat);
                    //MailTo.Add("supplierportal8@toyota.co.id");
                    //SendEmail(ex.Message.ToString() ,"Error Generate PDF", MailTo, "supplierportal8@toyota.co.id");
                }
            }
        }

        public void HouseKeepingFTP()
        {
            string FolderName = "";
            int HouseKeepingTime = 0;

            HouseKeepingTime = Convert.ToInt32(ConfigurationSettings.AppSettings["HouseKeepingDays"]);

            //Regular Order
            for (int i = HouseKeepingTime; i < HouseKeepingTime * 2; i++)
            {
                FolderName = DateTime.Now.AddDays(-1 * i).ToString("yyyyMMdd") + "1200";
                if (Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + "\\Portal\\DailyOrderPrinting\\" + FolderName))
                {
                    Directory.Delete(ConfigurationSettings.AppSettings["FTPPath"] + "\\Portal\\DailyOrderPrinting\\" + FolderName, true);
                }
            }

            //Emergency Order
            foreach (var path in Directory.EnumerateFiles(ConfigurationSettings.AppSettings["FTPPath"] + "\\Portal\\EmergencyOrder\\"))
            {
                FileInfo file = new FileInfo(path);
                if (file.CreationTime <= DateTime.Now.AddDays(-1 * HouseKeepingTime))
                    file.Delete();
            }

            //ProblemPart Order
            foreach (var path in Directory.EnumerateFiles(ConfigurationSettings.AppSettings["FTPPath"] + "\\Portal\\ProblemPartOrder\\"))
            {
                FileInfo file = new FileInfo(path);
                if (file.CreationTime <= DateTime.Now.AddDays(-1 * HouseKeepingTime))
                    file.Delete();
            }
        }

        #region DownloadPDF
        public static void DailyOrderDownload(string OrderDate, string SupplierCode)
        {
            string fileName;

            char[] splitchar = { '-' };
            string[] Supplier = SupplierCode.Split(splitchar);
            DateTime OrdDate = DateTime.ParseExact(OrderDate, "yyyy-MM-dd hh:mm:ss", null);
            try
            {
                //Manifest
                fileName = "Manifest_" + OrdDate.ToString("yyyyMMddhhmm") + "_" + Supplier[0] + "_" + Supplier[1] + ".pdf";
                PrintManifestReport(OrdDate, Supplier[0], Supplier[1], fileName);

                //Skid
                fileName = "Skid_" + OrdDate.ToString("yyyyMMddhhmm") + "_" + Supplier[0] + "_" + Supplier[1] + ".pdf";
                PrintSkidReport(OrdDate, Supplier[0], Supplier[1], fileName);

                //Kanban
                fileName = "Kanban_" + OrdDate.ToString("yyyyMMddhhmm") + "_" + Supplier[0] + "_" + Supplier[1] + ".pdf";
                PrintKanbanReport(OrdDate, Supplier[0], Supplier[1], fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static void DailyOrderPrint(string ManifestNo, DateTime OrderReleaseDate, string SupplierCode, string SupplierPlant, string TotalQty, string OrderType, string RcvPlantCode, SqlConnection strConn)
        {
            try
            {
                string FileName = ManifestNo + "-";
                string FolderName = "";
                if (OrderType == "1")
                    FolderName = OrderReleaseDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

                if (OrderType == "6")
                {
                    PrintManifestReport(ManifestNo, "0", FileName, FolderName, OrderType);
                    PrintManifestReport(ManifestNo, "1", FileName, FolderName, OrderType);

                    if (TotalQty != "0")
                    {
                        PrintSelectionKanbanReportCPO(ManifestNo, FileName, FolderName, OrderType);
                        PrintSkidReport(ManifestNo, FileName, FolderName, OrderType);
                        PrintKanbanHeaderReport(ManifestNo, OrderReleaseDate, SupplierCode, SupplierPlant, RcvPlantCode, FolderName, strConn);
                    }
                }
                else
                {
                    PrintManifestReport(ManifestNo, "0", FileName, FolderName, OrderType);
                    PrintManifestReport(ManifestNo, "1", FileName, FolderName, OrderType);

                    if (TotalQty != "0")
                    {
                        PrintSelectionKanbanReport(ManifestNo, FileName, FolderName, OrderType);
                        PrintSkidReport(ManifestNo, FileName, FolderName, OrderType);
                        PrintKanbanHeaderReport(ManifestNo, OrderReleaseDate, SupplierCode, SupplierPlant, RcvPlantCode, FolderName, strConn);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static void DailyOrderPrintProblem(string ManifestNo, string TotalQty)
        {
            try
            {
                string FileName = ManifestNo + "-";
                string FolderName = "";

                PrintManifestReport(ManifestNo, "0", FileName, FolderName, "5");
                PrintManifestReport(ManifestNo, "1", FileName, FolderName, "5");
                //PrintKanbanSkidReport(ManifestNo, FileName);
                if (TotalQty != "0")
                {
                    PrintSelectionKanbanReport(ManifestNo, FileName, FolderName, "5");
                    PrintSkidReport(ManifestNo, FileName, FolderName, "5");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        private static Telerik.Reporting.Report CreateReport(string reportPath, string sqlCommand,
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = null,
            Telerik.Reporting.SqlDataSourceCommandType commandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        )
        {
            string connString = ConfigurationSettings.AppSettings["PCS"];
            Telerik.Reporting.Report rpt;

            XmlReaderSettings settings;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            Telerik.Reporting.SqlDataSource sqlDataSource;

            settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(ConfigurationSettings.AppSettings["AppPath"] + reportPath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            sqlDataSource = (Telerik.Reporting.SqlDataSource)rpt.DataSource;

            sqlDataSource.ConnectionString = connString;
            sqlDataSource.SelectCommandType = commandType;
            sqlDataSource.SelectCommand = sqlCommand;
            //sqlDataSource.CommandTimeout = 600;
            if (parameters != null)
                sqlDataSource.Parameters.AddRange(parameters);

            return rpt;
        }
        
        private static void SavePDFReportToFTP(Telerik.Reporting.Report rpt, string FileName, string FolderName, string OrderType)
        {
            FileName = FileName + ".pdf";

            string FTPFolderRoot = "";
            switch (OrderType)
            {
                case "1":
                    FTPFolderRoot = "/Portal/DailyOrderPrinting/";
                    break;
                case "2":
                    FTPFolderRoot = "/Portal/EmergencyOrder/";
                    break;
                //case "3":
                //    FTPFolderRoot = "/Portal/ComponentPartOrder/";
                //    break;
                case "3":
                    FTPFolderRoot = "/Portal/ServicePartExport/";
                    break;
                case "4":
                    FTPFolderRoot = "/Portal/JunbikiOrder/";
                    break;
                case "5":
                    FTPFolderRoot = "/Portal/ProblemPartOrder/";
                    break;
                case "6":
                    FTPFolderRoot = "/Portal/ComponentPartOrder/";
                    break;
            }

            if (!Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName))
            {
                Directory.CreateDirectory(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName);
            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            using (System.IO.FileStream fs = new System.IO.FileStream(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName + "/" + FileName, System.IO.FileMode.Create))
            {
                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }
            rpt.Dispose();
        }

        private static void PrintManifestReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@PartNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\Rep_SupplierManifest.trdx", "SP_Rep_DailyOrder_Manifest_Print", parameters);

            FTPUpload vFtp = new FTPUpload(false);
            //vFtp.Setting = new FTPSettings(false);
            vFtp.Setting.IP = ConfigurationSettings.AppSettings["FTPServer"];
            vFtp.Setting.UserID = ConfigurationSettings.AppSettings["FTPUserID"];
            vFtp.Setting.Password = ConfigurationSettings.AppSettings["FTPPassword"];
            string msg = "";

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            bool resultUpload = vFtp.FtpUpload("/Portal/DailyOrder/" + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);
            rpt.Dispose();
        }
        
        private static void PrintSkidReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, null);
            parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
            parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
            parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
            Telerik.Reporting.Report rpt = CreateReport(@"\Report\Rep_ManifestSkid_Download.trdx", "SP_Rep_DailyOrder_Skid_Print", parameters);

            FTPUpload vFtp = new FTPUpload(false);
            //vFtp.Setting = new FTPSettings(false);
            vFtp.Setting.IP = ConfigurationSettings.AppSettings["FTPServer"];
            vFtp.Setting.UserID = ConfigurationSettings.AppSettings["FTPUserID"];
            vFtp.Setting.Password = ConfigurationSettings.AppSettings["FTPPassword"];
            string msg = "";

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            bool resultUpload = vFtp.FtpUpload("/Portal/DailyOrder/" + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);
            rpt.Dispose();
        }
        
        private static void PrintKanbanReport(DateTime OrderDate, string SupplierCode, string SupplierPlant, string Filename)
        {
            int cnt = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(@"SELECT MANIFEST_NO FROM TB_R_DAILY_ORDER_MANIFEST WHERE CONVERT(VARCHAR(8), ORDER_RELEASE_DT, 112)='" + Convert.ToDateTime(OrderDate).ToString("yyyyMMdd") + "' AND SUPPLIER_CD='" + SupplierCode + "' AND SUPPLIER_PLANT='" + SupplierPlant + "'", conn))
                {
                    SqlDataReader dr;
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        try
                        {
                            using (SqlCommand cmd2 = new SqlCommand(@"SELECT KANBAN_ID FROM TB_R_RECEIVING_D WHERE MANIFEST_NO='" + dr["MANIFEST_NO"].ToString() + "'", conn))
                            {
                                SqlDataReader dr2;
                                dr2 = cmd2.ExecuteReader();
                                while (dr2.Read())
                                {
                                    try
                                    {
                                        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
                                        qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
                                        qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                                        Image image;
                                        String data = Filename;
                                        image = qrCodeEncoder.Encode(dr2["KANBAN_ID"].ToString());
                                        image.Save(ConfigurationSettings.AppSettings["QRPath"] + dr2["KANBAN_ID"].ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    cnt = cnt + 1;
                                }
                                if (cnt > 0)
                                {
                                    try
                                    {
                                        Telerik.Reporting.ReportParameterCollection reportParameters = new Telerik.Reporting.ReportParameterCollection();
                                        Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
                                        parameters.Add("@ManifestNo", System.Data.DbType.String, null);
                                        parameters.Add("@ItemNo", System.Data.DbType.String, null);
                                        parameters.Add("@OrderReleaseDate", System.Data.DbType.String, OrderDate.ToString("yyyyMMdd"));
                                        parameters.Add("@SupplierCode", System.Data.DbType.String, SupplierCode);
                                        parameters.Add("@SupplierPlant", System.Data.DbType.String, SupplierPlant);
                                        parameters.Add("@IsProblemPlant", System.Data.DbType.String, null);
                                        parameters.Add("@ORDER_PLAN_QTY_LOT", System.Data.DbType.String, null);
                                        reportParameters.Add("QRpath", Telerik.Reporting.ReportParameterType.String, ConfigurationSettings.AppSettings["QRPath"]);
                                        Telerik.Reporting.Report rpt = CreateReport(@"\Report\Rep_ManifestKanban_Download.trdx", "SP_Rep_DailyOrder_Kanban_Print", parameters);
                                        rpt.ReportParameters.AddRange(reportParameters);

                                        FTPUpload vFtp = new FTPUpload(false);
                                        //vFtp.Setting = new FTPSettings(false);
                                        /*
                                        vFtp.Setting.IP = "10.16.20.73";
                                        vFtp.Setting.UserID = "uuspcs00";
                                        vFtp.Setting.Password = "uu$pcs00";
                                         */
                                        vFtp.Setting.IP = ConfigurationSettings.AppSettings["FTPServer"];
                                        vFtp.Setting.UserID = ConfigurationSettings.AppSettings["FTPUserID"];
                                        vFtp.Setting.Password = ConfigurationSettings.AppSettings["FTPPassword"];
                                        string msg = "";

                                        ReportProcessor reportProcess = new ReportProcessor();
                                        RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
                                        bool resultUpload = vFtp.FtpUpload("/Portal/DailyOrder/" + OrderDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant, Filename, result.DocumentBytes, ref msg);
                                        rpt.Dispose();
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("PrintKanbanReport : Exception [" + e.Message + "]");
                                    }
                                    string[] array1 = Directory.GetFiles(@"" + ConfigurationSettings.AppSettings["QRPath"]);
                                    Console.WriteLine(ConfigurationSettings.AppSettings["QRPath"] + "*.jpg");
                                    File.Delete(ConfigurationSettings.AppSettings["QRPath"] + "*.jpg");
                                }

                                //foreach (string name in array1)
                                //{
                                //    Console.WriteLine("Deleting file : " + name);
                                //    File.Delete(name);
                                //}
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
                conn.Close();
            }
        }

        private static void PrintManifestReport(string ManifestNos, string PaperType, string FileName, string FolderName, string OrderType, string ProblemPart = null)
        {
            string reportPath = @"\Report\Rep_SupplierManifest.trdx";
            if (PaperType == "1")
                reportPath = @"\Report\Rep_SupplierManifest_WithoutBorder.trdx";

            string sqlCommand = "SP_Rep_DailyOrder_Manifest_Print";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);

            if (ProblemPart == "1") 
                parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameters);

            if (PaperType == "1") 
                SavePDFReportToFTP(rpt, FileName + PaperType + "-" + "1", FolderName + "/ManifestPrePrinted", OrderType);
            else 
                SavePDFReportToFTP(rpt, FileName + PaperType + "-" + "1", FolderName + "/ManifestPlain", OrderType);
        }
        
        private static void PrintSkidReport(string ManifestNos, string FileName, string FolderName, string OrderType, string ProblemPart = null)
        {
            ManifestNos = ManifestNos.Trim().IndexOf("'") >= 0 ? ManifestNos : "'" + ManifestNos + "'";

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]);
            conn.Open();

            string dock = GetData(conn, ManifestNos);

            if (dock == "1")
            {
                string reportPath = @"\Report\Rep_ManifestSkid_Download_Sunter.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Skid_Print_Sunter";
                Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos.Replace("'", ""));
                //parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
                if (ProblemPart == "1") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameters);

                SavePDFReportToFTP(rpt, FileName + "3", FolderName + "/Skid", OrderType);
            }
            else
            {
                string reportPath = @"\Report\Rep_ManifestSkid_Download.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Skid_Print";
                Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos.Replace("'", ""));
                //parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);
                if (ProblemPart == "1") parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameters);

                SavePDFReportToFTP(rpt, FileName + "3", FolderName + "/Skid", OrderType);
            }
        }
        
        private static void PrintSelectionKanbanReport(string ManifestNo, string FileName, string FolderName, string OrderType, string ProblemPart = null, string PartNos = null)
        {
            ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]);
            conn.Open();

            string dock = GetData(conn, ManifestNo);

            if (dock == "1")
            {
                string reportPath = @"\Report\Rep_ManifestKanban_Sunter.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print_Sunter";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                //parameter.Add("@ItemNo", System.Data.DbType.String, PartNos);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);

                if (ProblemPart == "1") 
                    parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
            else
            {
                //remarks by alira.agi 2015-06-08 qr code
                #region remarks QR Code
                /*
                using (SqlConnection conns = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]))
				{
                    int cnt = 0;
					conns.Open();
					try
					{
                        using (SqlCommand cmd2 = new SqlCommand(@"SELECT KANBAN_ID FROM TB_R_RECEIVING_D WHERE MANIFEST_NO=" + ManifestNo, conns))
						{
							SqlDataReader dr2;
							dr2 = cmd2.ExecuteReader();
                            
							while (dr2.Read())
							{
								try
								{
									QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
									qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
									qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
									Image image;

									image = qrCodeEncoder.Encode(dr2["KANBAN_ID"].ToString());
                                    image.Save(ConfigurationSettings.AppSettings["QRPath"] + dr2["KANBAN_ID"].ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
								}
								catch (Exception e)
								{
									Console.WriteLine(e.Message);
								}
                                cnt = cnt + 1;
							}

                            if(cnt>0)
                            {
                                try
                                {

                                    ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";
                                    string reportPath = @"\Report\Rep_ManifestKanban_Download.trdx";
                                    string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
                                    Telerik.Reporting.ReportParameterCollection reportParameters = new Telerik.Reporting.ReportParameterCollection();
                                    Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                                    parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                                    parameter.Add("@PartNo", System.Data.DbType.String, PartNos);
                                    parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                                    parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                                    parameter.Add("@SupplierPlant", System.Data.DbType.String, null);
                                    reportParameters.Add("QRpath", Telerik.Reporting.ReportParameterType.String, ConfigurationSettings.AppSettings["QRPath"]);

                                    if (ProblemPart == "1") parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                                    Telerik.Reporting.Report rpt = CreateReport(reportPath,
                                        sqlCommand,
                                        parameter);

                                    rpt.ReportParameters.AddRange(reportParameters);
                                    SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("PrintSelectionKanbanReport : Exception [" + e.Message + "]");
                                }


                                //Console.WriteLine("PrintSelectionKanbanReport : Deleting QR CODE Files...");
                                //string[] array1 = Directory.GetFiles(@"" + ConfigurationSettings.AppSettings["QRPath"]);
                                //foreach (string name in array1)
                                //{
                                //    Console.WriteLine("Deleting file : " + name);
                                //    File.Delete(name);
                                //}
                                Console.WriteLine(ConfigurationSettings.AppSettings["QRPath"] + "*.jpg");
                                File.Delete(ConfigurationSettings.AppSettings["QRPath"] + "*.jpg");
                            }


						}
					}
					catch (Exception e)
					{
						Console.WriteLine(e.Message);
					}
				}
                */
                #endregion
                // add by alira.agi 2015-06-08 back to barcode
                
                string reportPath = @"\Report\Rep_ManifestKanban_Download.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                //parameter.Add("@ItemNo", System.Data.DbType.String, PartNos);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);

                if (ProblemPart == "1") 
                    parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
        }

        private static void PrintKanbanHeaderReport(string Manifest_No, DateTime OrderReleaseDT, string SupplierCode, string SupplierPlant, string RcvPlantCode, string strFolderName, SqlConnection sqlConn)
        {

            using (SqlCommand cmd3 = new SqlCommand(@"SELECT 
                                                        m.MANIFEST_NO,
                                                        TOTAL_MANIFEST=ISNULL(p.TOTAL_MANIFEST,0),
                                                        m.RCV_PLANT_CD,
                                                        pt.PLANT_NM,
                                                        m.ORDER_TYPE
                                                    FROM TB_R_DAILY_ORDER_MANIFEST m with(nolock)
                                                    INNER JOIN TB_M_PLANT pt with(nolock) 
                                                        on m.RCV_PLANT_CD=pt.PLANT_CD
                                                    INNER JOIN (
                                                        SELECT MANIFEST_NO,
                                                            TOTAL_MANIFEST=COUNT(MANIFEST_NO)
                                                        FROM TB_R_DAILY_ORDER_PART with(nolock)
                                                        WHERE PROCESS_TYPE = 'PROCESS_N' 
                                                        GROUP BY MANIFEST_NO
                                                        ) p on m.MANIFEST_NO=p.MANIFEST_NO
                                                    WHERE m.FTP_FLAG='0' 
                                                          AND m.ORDER_TYPE='1'
                                                    AND m.MANIFEST_NO=@MANIFEST_NO", sqlConn))
            {
                cmd3.Parameters.Add(new SqlParameter("@MANIFEST_NO", Manifest_No));


                SqlDataReader dr3;
                dr3 = cmd3.ExecuteReader();

                while (dr3.Read())
                {
                    if (dr3.HasRows)
                    {
                        string strManifestNo = dr3["MANIFEST_NO"].ToString();
                        string strOrderType = dr3["ORDER_TYPE"].ToString();
                        string currOrderReleaseDT = OrderReleaseDT.ToString("yyyy-MM-dd");
                        PrintHeaderKanbanReport(strManifestNo, SupplierCode, SupplierPlant, RcvPlantCode, currOrderReleaseDT, strOrderType, strFolderName);
                    }
                }
                dr3.Close();
            }

        }

        private static void PrintHeaderKanbanReport(string ManifestNo, string SupplierCode, string SupplierPlant, string RcvPlantCode, string OrderReleaseDT = null, string OrderType = null, string FolderName = null)
        {
            DateTime OrderReleaseDate = DateTime.Now;
            string FileName = ManifestNo + "-Header";
            //if (OrderType  == "1") FolderName = OrderReleaseDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

            ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]);
            conn.Open();

            string reportPath = @"\Report\Rep_ManifestKanban_Header.trdx";
            string sqlCommand = "SP_Rep_GetDailyOrder_Part_Print_Header";
            string strOrderReleaseDate = DateTime.Now.ToShortDateString();

            Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameter.Add("@SupplierCode", System.Data.DbType.String, SupplierCode.Replace("'", ""));
            parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
            parameter.Add("@SupplierPlantCode", System.Data.DbType.String, SupplierPlant.Replace("'", ""));
            parameter.Add("@RcvPlantCode", System.Data.DbType.String, RcvPlantCode.Replace("'", ""));
            parameter.Add("@OrderReleaseDT", System.Data.DbType.String, OrderReleaseDT);

            Telerik.Reporting.Report rpt = CreateReport(reportPath,
                sqlCommand,
                parameter);

            SavePDFReportToFTP(rpt, FileName, FolderName + "/Kanban", OrderType);
        }

        public static void DailyOrderSPEXPrint(string ManifestNo, DateTime OrderReleaseDate, string SupplierCode, string SupplierPlant, string TotalQty, string OrderType, string RcvPlantCode, SqlConnection strConn)
        {
            try
            {
                string FileName = ManifestNo + "-";
                string FolderName = "";
                if (OrderType == "1")
                    FolderName = OrderReleaseDate.ToString("yyyyMMddhhmm") + "/" + SupplierCode + "/" + SupplierPlant + "/" + RcvPlantCode;

                PrintManifestSPEXReport(ManifestNo, "0", FileName, FolderName, OrderType);
                PrintManifestSPEXReport(ManifestNo, "1", FileName, FolderName, OrderType);

                if (TotalQty != "0")
                {
                    PrintSelectionKanbanSPEXReport(ManifestNo, FileName, FolderName, OrderType);
                    PrintSkidSPEXReport(ManifestNo, FileName, FolderName, OrderType);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void PrintManifestSPEXReport(string ManifestNos, string PaperType, string FileName, string FolderName, string OrderType, string ProblemPart = null)
        {
            string reportPath = @"\Report\Rep_SupplierManifest.trdx";
            if (PaperType == "1")
                reportPath = @"\Report\Rep_SupplierManifest_WithoutBorder.trdx";

            string sqlCommand = "SP_Rep_DailyOrder_Manifest_Print_SPEX2";
            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos);

            if (ProblemPart == "1")
                parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

            Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);

            if (PaperType == "1")
                SavePDFReportToFTP(rpt, FileName + PaperType + "-" + "1", FolderName + "/ManifestPrePrinted", OrderType);
            else
                SavePDFReportToFTP(rpt, FileName + PaperType + "-" + "1", FolderName + "/ManifestPlain", OrderType);
        }

		private static void PrintSelectionKanbanReportCPO(string ManifestNo, string FileName, string FolderName, string OrderType, string ProblemPart = null, string PartNos = null)
        {
            ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]);
            conn.Open();

            string dock = GetData(conn, ManifestNo);

            if (dock == "1")
            {
                string reportPath = @"\Report\Rep_ManifestKanban_CPO.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print_CPO";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                //parameter.Add("@ItemNo", System.Data.DbType.String, null);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);

                if (ProblemPart == "1")
                    parameter.Add("@ORDER_PLAN_QTY_LOT", System.Data.DbType.String, null);
                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
            else
            {

                string reportPath = @"\Report\Rep_ManifestKanban_Download.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                //parameter.Add("@ItemNo", System.Data.DbType.String, PartNos);
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);

                if (ProblemPart == "1")
                    parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath,
                    sqlCommand,
                    parameter);

                SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
        }
        #endregion
        private static void PrintSelectionKanbanSPEXReport(string ManifestNo, string FileName, string FolderName, string OrderType, string ProblemPart = null, string PartNos = null)
        {
            ManifestNo = ManifestNo.Trim().IndexOf("'") >= 0 ? ManifestNo : "'" + ManifestNo + "'";

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]);
            conn.Open();

            string dock = GetDataSpex(conn, ManifestNo);

            if (dock == "1")
            {
                string reportPath = @"\Report\Rep_ManifestKanban_Sunter_SPEX2.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print_Sunter_SPEX2";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);

                if (ProblemPart == "1")
                    parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameter);

                SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
            else
            {
                string reportPath = @"\Report\Rep_ManifestKanban_Download_SPEX2.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Kanban_Print_SPEX2";
                Telerik.Reporting.SqlDataSourceParameterCollection parameter = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameter.Add("@ManifestNo", System.Data.DbType.String, ManifestNo.Replace("'", ""));
                parameter.Add("@OrderReleaseDate", System.Data.DbType.String, null);
                parameter.Add("@SupplierCode", System.Data.DbType.String, null);
                parameter.Add("@SupplierPlant", System.Data.DbType.String, null);

                if (ProblemPart == "1")
                    parameter.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameter);

                SavePDFReportToFTP(rpt, FileName + "2", FolderName + "/Kanban", OrderType);
            }
        }

        private static void PrintSkidSPEXReport(string ManifestNos, string FileName, string FolderName, string OrderType, string ProblemPart = null)
        {
            ManifestNos = ManifestNos.Trim().IndexOf("'") >= 0 ? ManifestNos : "'" + ManifestNos + "'";

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["PCS"]);
            conn.Open();

            string dock = GetDataSpex(conn, ManifestNos);

            if (dock == "1")
            {
                string reportPath = @"\Report\Rep_ManifestSkid_Download_Sunter.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Skid_Print_Sunter_SPEX2";
                Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos.Replace("'", ""));
                
                if (ProblemPart == "1")
                    parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);

                SavePDFReportToFTP(rpt, FileName + "3", FolderName + "/Skid", OrderType);
            }
            else
            {
                string reportPath = @"\Report\Rep_ManifestSkid_Download.trdx";
                string sqlCommand = "SP_Rep_DailyOrder_Skid_Print_SPEX2";
                Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
                parameters.Add("@ManifestNo", System.Data.DbType.String, ManifestNos.Replace("'", ""));

                if (ProblemPart == "1")
                    parameters.Add("@IsProblemPart", System.Data.DbType.Boolean, true);

                Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);

                SavePDFReportToFTP(rpt, FileName + "3", FolderName + "/Skid", OrderType);
            }
        }

        private static void DailyProblemPrint(string PR_CD)
        {
            try
            {
                string FileName = PR_CD + ".pdf";
                string FolderName = "";
                PrintProblemReport(PR_CD, FileName, FolderName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region ProblemReport
        private static void PrintProblemReport(string PR_CD, string FileName, string FolderName)
        {
            string reportPath = @"\Report\Rep_ProblemReport.trdx";
            string sqlCommand = "SP_Rep_Problem_Report";

            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@PROBLEM_SHEET", System.Data.DbType.String, PR_CD);

            Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);

            string FTPFolderRoot = "/Portal/ProblemReport/";
            if (!Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName))
            {
                Directory.CreateDirectory(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName);
            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            using (System.IO.FileStream fs = new System.IO.FileStream(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName + "/" + FileName, System.IO.FileMode.Create))
            {
                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }
            rpt.Dispose();
        }
        #endregion


        #region submanifest
        private static void DailyPrintSubManifest(string SUB_MANIFEST_NO)
        {
            try
            {
                string FileName = SUB_MANIFEST_NO + ".pdf";
                string FolderName = "";
                PrintSubManifestSkid(SUB_MANIFEST_NO, FileName, FolderName);
                PrintSubManifest(SUB_MANIFEST_NO, FileName, FolderName,"0");
                PrintSubManifest(SUB_MANIFEST_NO, FileName, FolderName,"1");
                PrintSubManifestKanban(SUB_MANIFEST_NO, FileName, FolderName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void PrintSubManifestSkid(string SUB_MANIFEST_NO, string FileName, string FolderName)
        {
            string reportPath = @"\Report\Rep_SubManifestSkid.trdx";
            string sqlCommand = "SP_Rep_SubManifest_Skid";

            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, SUB_MANIFEST_NO);

            Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);

            string FTPFolderRoot = "/Portal/SubManifest/" + SUB_MANIFEST_NO + "/Skid/";
            if (!Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName))
            {
                Directory.CreateDirectory(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName);
            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            using (System.IO.FileStream fs = new System.IO.FileStream(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName + "/" + FileName, System.IO.FileMode.Create))
            {
                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }
            rpt.Dispose();
        }

        private static void PrintSubManifest(string SUB_MANIFEST_NO, string FileName, string FolderName,string type)
        {
            string reportPath = @"\Report\Rep_SubManifest_Spex.trdx";
            
            if(type == "1")
                reportPath = @"\Report\Rep_SubManifest_Spex_WithoutBorder.trdx";
            
            string sqlCommand = "SP_Rep_SubManifest_Print";

            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@ManifestNo", System.Data.DbType.String, SUB_MANIFEST_NO);

            Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);


            string FTPFolderRoot = "/Portal/SubManifest/" + SUB_MANIFEST_NO + "/SubManifest/";

            if (type == "1")
                FTPFolderRoot = "/Portal/SubManifest/" + SUB_MANIFEST_NO + "/SubManifestPlain/";
                        
            if (!Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName))
            {
                Directory.CreateDirectory(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName);
            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            using (System.IO.FileStream fs = new System.IO.FileStream(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName + "/" + FileName, System.IO.FileMode.Create))
            {
                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }
            rpt.Dispose();
        }

        private static void PrintSubManifestKanban(string SUB_MANIFEST_NO, string FileName, string FolderName)
        {
            string reportPath = @"\Report\Rep_SubManifest_Kanban_Spex.trdx";
            string sqlCommand = "SP_Rep_SubManifest_Kanban";

            Telerik.Reporting.SqlDataSourceParameterCollection parameters = new Telerik.Reporting.SqlDataSourceParameterCollection();
            parameters.Add("@SubManifestNo", System.Data.DbType.String, SUB_MANIFEST_NO);

            Telerik.Reporting.Report rpt = CreateReport(reportPath, sqlCommand, parameters);

            string FTPFolderRoot = "/Portal/SubManifest/" + SUB_MANIFEST_NO + "/Kanban/";
            if (!Directory.Exists(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName))
            {
                Directory.CreateDirectory(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName);
            }

            ReportProcessor reportProcess = new ReportProcessor();
            RenderingResult result = reportProcess.RenderReport("pdf", rpt, null);
            using (System.IO.FileStream fs = new System.IO.FileStream(ConfigurationSettings.AppSettings["FTPPath"] + FTPFolderRoot + FolderName + "/" + FileName, System.IO.FileMode.Create))
            {
                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }
            rpt.Dispose();
        }
        #endregion


        #region Log
        private static void InsertLogHeader(SqlConnection conn,
            long ProcessID,
            string FunctionID,
            string ModuleID,
            DateTime StartDate,
            DateTime EndDate,
            int ProcessStatus,
            string CreatedBy)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertLogHeader";

            SqlParameter paramProcessID = new SqlParameter("@PROCESS_ID", SqlDbType.BigInt);
            paramProcessID.Value = ProcessID;
            cmd.Parameters.Add(paramProcessID);
            SqlParameter paramFunctionID = new SqlParameter("@FUNCTION_ID", SqlDbType.VarChar);
            paramFunctionID.Value = FunctionID;
            cmd.Parameters.Add(paramFunctionID);
            SqlParameter paramModuleID = new SqlParameter("@MODULE_ID", SqlDbType.VarChar);
            paramModuleID.Value = ModuleID;
            cmd.Parameters.Add(paramModuleID);
            SqlParameter paramStartDate = new SqlParameter("@START_DATE", SqlDbType.DateTime);
            paramStartDate.Value = StartDate;
            cmd.Parameters.Add(paramStartDate);
            SqlParameter paramEndDate = new SqlParameter("@END_DATE", SqlDbType.DateTime);
            paramEndDate.Value = EndDate;
            cmd.Parameters.Add(paramEndDate);
            SqlParameter paramProcessStatus = new SqlParameter("@PROCESS_STATUS", SqlDbType.TinyInt);
            paramProcessStatus.Value = ProcessStatus;
            cmd.Parameters.Add(paramProcessStatus);
            SqlParameter paramCreatedBy = new SqlParameter("@CREATED_BY", SqlDbType.VarChar);
            paramCreatedBy.Value = CreatedBy;
            cmd.Parameters.Add(paramCreatedBy);

            cmd.ExecuteNonQuery();
        }
        private static void InsertLogDetail(SqlConnection conn,
            long ProcessID,
            string MessageID,
            string MessageType,
            string Message,
            string Location,
            string CreatedBy,
            Nullable<Int32> SeqNo = null
            )
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertLogDetail";

            SqlParameter paramProcessID = new SqlParameter("@PROCESS_ID", SqlDbType.BigInt);
            paramProcessID.Value = ProcessID;
            cmd.Parameters.Add(paramProcessID);
            SqlParameter paramFunctionID = new SqlParameter("@MESSAGE_ID", SqlDbType.VarChar);
            paramFunctionID.Value = MessageID;
            cmd.Parameters.Add(paramFunctionID);
            SqlParameter paramModuleID = new SqlParameter("@MESSAGE_TYPE", SqlDbType.VarChar);
            paramModuleID.Value = MessageType;
            cmd.Parameters.Add(paramModuleID);
            SqlParameter paramStartDate = new SqlParameter("@MESSAGE", SqlDbType.VarChar);
            paramStartDate.Value = Message;
            cmd.Parameters.Add(paramStartDate);
            SqlParameter paramEndDate = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            paramEndDate.Value = Location;
            cmd.Parameters.Add(paramEndDate);
            SqlParameter paramCreatedBy = new SqlParameter("@CREATED_BY", SqlDbType.VarChar);
            paramCreatedBy.Value = CreatedBy;
            cmd.Parameters.Add(paramCreatedBy);
            SqlParameter paramProcessStatus = new SqlParameter("@SEQ_NO", SqlDbType.BigInt);
            paramProcessStatus.Value = SeqNo;
            cmd.Parameters.Add(paramProcessStatus);

            cmd.ExecuteNonQuery();
        }
        private static void UpdateStatusLog(SqlConnection conn, long ProcessID, int ProcessStatus)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE dbo.TB_R_LOG_H SET END_DATE=GETDATE(),PROCESS_STATUS=" + ProcessStatus.ToString() + "WHERE PROCESS_ID = " + ProcessID.ToString();
            cmd.ExecuteNonQuery();
        }
        #endregion

        private static void SendEmail(string message, string subject, List<string> MailTo, string MailCC)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail-relay.toyota.co.id");
            string[] MailCC_Collection = null;

            string MailCC_IsActive = "";
            MailCC_IsActive = ConfigurationSettings.AppSettings["MailCC_IsActive"];

            mail.From = new MailAddress("ippcs-admin@toyota.co.id");
            foreach (string mailTo in MailTo)
            {
                mail.To.Add(mailTo);
            }
            if (MailCC != "" && MailCC_IsActive == "1")
            {
                MailCC_Collection = MailCC.Split(';');
                foreach (string mailCC in MailCC_Collection)
                {
                    mail.CC.Add(mailCC);
                }
            }
            
            mail.Subject = subject;
            mail.Body = message;

            SmtpServer.Port = 25;
            SmtpServer.Send(mail);
        }

        private System.Threading.Timer IntervalTimer;
        private volatile Boolean _AvoidDoubleWork;
        
        protected override void OnStart(string[] args)
        {
            _AvoidDoubleWork = false;
            //TGTimer.Enabled = true;
            //TGTimer.Start();
            int hours = Convert.ToInt32(ConfigurationSettings.AppSettings["TimerHour"]);
            int minutes = Convert.ToInt32(ConfigurationSettings.AppSettings["TimerMinute"]);
            int seconds = Convert.ToInt32(ConfigurationSettings.AppSettings["TimerSecond"]);
            TimeSpan tsInterval = new TimeSpan(hours, minutes, seconds);
            IntervalTimer = new System.Threading.Timer(
                new System.Threading.TimerCallback(IntervalTimer_Elapsed)
                , null, tsInterval, tsInterval);
        }

        protected override void OnStop()
        {
            //TGTimer.Stop();
            //TGTimer.Enabled = false;
            IntervalTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            IntervalTimer.Dispose();
            IntervalTimer = null;
        }

        private void IntervalTimer_Elapsed(object state)
        {   // Do the thing that needs doing every few minutes...
            // (Omitted for simplicity is sentinel logic to prevent re-entering
            //  DoWork() if the previous "tick" has for some reason not completed.)
            if (!_AvoidDoubleWork)
            {
                _AvoidDoubleWork = true;
                ProcessData("1");
                ProcessData("4");
                ProcessData("5");

                ProcessData("6"); //handling latest plant fid.deny 2015-07-03
                _AvoidDoubleWork = false;
            }

            if (DateTime.Now.Hour == 5)
            {
                if (DateTime.Now.Minute >= 0 && DateTime.Now.Minute <= 59)
                {
                    //HouseKeepingFTP();
                }
            }
        }
       
        private static string GetData(SqlConnection conn, string Manifest)
        {
            string statusDock = "";

            SqlCommand cmd4 = new SqlCommand();
            cmd4.Connection = conn;
            cmd4.CommandType = CommandType.Text;
            cmd4.CommandText = "SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'DOCK_PACKING' AND SYSTEM_CD = 'DAILY_ORDER'";
            //modified by FID.reggy
            //SqlDataReader dr4;
            //dr4 = cmd4.ExecuteReader();
            //while (dr4.Read())
            //{
            string DOCK_LIST = cmd4.ExecuteScalar().ToString();
                try
                {
                    //string[] Dock = new string[12];
                    //Dock = dr4["SYSTEM_VALUE"].ToString().Split(',');
                    List<string> Dock = DOCK_LIST.Split(',').ToList();

                    SqlCommand cmd44 = new SqlCommand();
                    cmd44.Connection = conn;
                    cmd44.CommandType = CommandType.Text;
                    cmd44.CommandText = "SELECT DOCK_CD FROM TB_R_DAILY_ORDER_MANIFEST WHERE MANIFEST_NO =" + Manifest;
                    //SqlDataReader dr44;
                    //dr44 = cmd44.ExecuteReader();
                    //while (dr44.Read())
                    //{
                    string DOCK_CD = cmd44.ExecuteScalar().ToString();
                        for (int i = 0; i < Dock.Count; i++)
                        {
                            //if (Dock[i].Trim() == dr44["DOCK_CD"].ToString())
                            if(Dock[i].Trim() == DOCK_CD)
                            {
                                statusDock = "1";
                                break;
                            }
                            else
                            {
                                statusDock = "2";
                            }
                        }
                    //}
                    //dr44.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            //}
            //dr4.Close();
            return statusDock;
        }

        private static string GetDataSpex(SqlConnection conn, string Manifest)
        {
            string statusDock = "";

            SqlCommand cmd4 = new SqlCommand();
            cmd4.Connection = conn;
            cmd4.CommandType = CommandType.Text;
            cmd4.CommandText = "SELECT SYSTEM_VALUE FROM TB_M_SYSTEM WHERE FUNCTION_ID = 'DOCK_PACKING' AND SYSTEM_CD = 'DAILY_ORDER'";
            //modified by FID.reggy
            //SqlDataReader dr4;
            //dr4 = cmd4.ExecuteReader();
            //while (dr4.Read())
            //{
            string DOCK_LIST = cmd4.ExecuteScalar().ToString();
            try
            {
                //string[] Dock = new string[12];
                //Dock = dr4["SYSTEM_VALUE"].ToString().Split(',');
                List<string> Dock = DOCK_LIST.Split(',').ToList();

                SqlCommand cmd44 = new SqlCommand();
                cmd44.Connection = conn;
                cmd44.CommandType = CommandType.Text;
                cmd44.CommandText = "SELECT DOCK_CD FROM TB_R_PRA_DAILY_ORDER_MANIFEST WHERE MANIFEST_NO =" + Manifest;
                //SqlDataReader dr44;
                //dr44 = cmd44.ExecuteReader();
                //while (dr44.Read())
                //{
                string DOCK_CD = cmd44.ExecuteScalar().ToString();
                for (int i = 0; i < Dock.Count; i++)
                {
                    //if (Dock[i].Trim() == dr44["DOCK_CD"].ToString())
                    if (Dock[i].Trim() == DOCK_CD)
                    {
                        statusDock = "1";
                        break;
                    }
                    else
                    {
                        statusDock = "2";
                    }
                }
                //}
                //dr44.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //}
            //dr4.Close();
            return statusDock;
        }

    }
}
