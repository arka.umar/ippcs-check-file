﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelerikReportGeneratorService
{
    class Manifest
    {
        public string ManifestNo;
        public string SupplierCode;
        public string RcvPlantCode;
        public string OrderRelease;
        public DateTime OrderReleaseDate;
        public Int32 TotalQty;
        public string OrderType;
        public string PlantName;
    }
}
